*** Settings ***
Resource    ../init.robot
Documentation    Tests New Feature Settings > Local Accounts > Add username = user.name through the CLI
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Force Tags         CLI
Default Tags        CLI     SSH

Suite Setup     CLI:Open
Suite Teardown  CLI:Close Connection

*** Test Cases ***
Test adding user with a dot in their username
        CLI:Enter Path  /settings/local_accounts/
        ${RESULT}=	    CLI:Check If User Exists       user.name
        Run Keyword If  ${RESULT}       CLI:Delete Users      user.name
        CLI:Add
        CLI:Set Field   username    user.name
        CLI:Set Field   password    username
        CLI:Commit
        CLI:Test Show Command  user.name
        CLI:Delete Users  user.name