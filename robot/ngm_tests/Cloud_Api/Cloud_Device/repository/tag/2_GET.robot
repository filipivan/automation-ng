*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	NEED-CREATION
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

repository_tag_list (status_code = 200)
	[Documentation]	This is the endpoint which returns the cloud platform version. Status code 200 is expected. 
	...	Endpoint: /repository/tag

	${response}=   CLOUD::API::Send Get Request    /repository/tag
	Should Be Equal   ${response.status_code}   ${200}

repository_tag_list (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which get the company details. Status code 403 is expected.
	...	Endpoint: /repository/tag

	${response}=    CLOUD::API::Send Get Request Error 403    /repository/tag
	Should Be Equal   ${response.status_code}   ${403}

repository_tag_list (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which get the company details. Status code 404 is expected.
	...	Endpoint: /repository/tags

	${response}=    CLOUD::API::Send Get Request Error 404    /repository/tags
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session