*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API   NON-CRITICAL
Default Tags

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***
device_access_list (status_code = 200)
	[Documentation]	|-. Status code 200 is expected.
	...	Endpoint: /device/access

	${response}=   CLOUD::API::Send Get Request    /device/access
	Should Be Equal   ${response.status_code}   ${200}

device_available_create (status_code = 200)
    [Tags]      BUG_CLOUD-9675
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /device/available

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	device_ids=${device_ids}

	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/device/available
	Should Be Equal   ${response.status_code}   ${200}

device_available_create (status_code = 400) with Invalid body
	[Documentation]	>-. Status code 400 is expected.
	...	Endpoint: /device/available

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	key_enabled=${device_ids}

	${response}=    CLOUD::API::Send Post Request Error 400    PAYLOAD=${PAYLOAD}    PATH=/device/available
	Should Be Equal   ${response.status_code}   ${400}

device_available_create (status_code = 403) with wrong credntials
	[Documentation]	>-. Status code 403 is expected.
	...	Endpoint: /device/available

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	device_ids=${device_ids}

	${response}=    CLOUD::API::Send Post Request Error 403    PAYLOAD=${PAYLOAD}    PATH=/device/available
	Should Be Equal   ${response.status_code}   ${403}

device_available_create (status_code = 404) with wrong endpoint
	[Documentation]	>-. Status code 404 is expected.
	...	Endpoint: /device/availables

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	device_ids=${device_ids}

	${response}=    CLOUD::API::Send Post Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/device/availables
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session