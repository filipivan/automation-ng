*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../../init.robot
Force Tags    API	NON-CRITICAL
Default Tags

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***

device_transfer_execute_create (status_code = 200)
    [Tags]      BUG_CLOUD-9706
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /device/transfer/execute

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/device/transfer/execute
	Should Be Equal   ${response.status_code}   ${200}

device_transfer_execute_create (status_code = 400) with Invalid body
	[Documentation]	>-. Status code 400 is expected.
	...	Endpoint: /device/transfer/execute

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request Error 400    PAYLOAD=${PAYLOAD}    PATH=/device/transfer/execute
	Should Be Equal   ${response.status_code}   ${400}

device_transfer_execute_create (status_code = 403) with wrong credntials
	[Documentation]	>-. Status code 403 is expected.
	...	Endpoint: /device/transfer/execute

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request Error 403    PAYLOAD=${PAYLOAD}    PATH=/device/transfer/execute
	Should Be Equal   ${response.status_code}   ${403}

device_transfer_execute_create (status_code = 404) with wrong endpoint
	[Documentation]	>-. Status code 404 is expected.
	...	Endpoint: /device/transfer/executes

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/device/transfer/executes
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session