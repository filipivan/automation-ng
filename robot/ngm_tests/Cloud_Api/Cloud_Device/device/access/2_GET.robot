*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At
Resource    ../../../../init.robot
Force Tags    API
Default Tags

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

device_access_list (status_code = 200)
	[Documentation]	|-. Status code 200 is expected. 
	...	Endpoint: /device/access

	${response}=   CLOUD::API::Send Get Request    /device/access
	Should Be Equal   ${response.status_code}   ${200}
	${get_list}=	    Get From Dictionary	${response.json()}    list
	${get_id}=	    Get From List  	${get_list}    0
    ${device_ids}=	    Get From Dictionary  	${get_id}    id
    Set Global Variable     ${device_ids}

device_access_list (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which get the company details. Status code 403 is expected.
	...	Endpoint: /device/access

	${response}=    CLOUD::API::Send Get Request Error 403    /device/access
	Should Be Equal   ${response.status_code}   ${403}

device_access_list (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which get the company details. Status code 404 is expected.
	...	Endpoint: /devices/access

	${response}=    CLOUD::API::Send Get Request Error 404    /devices/access
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session