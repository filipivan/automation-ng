*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../../init.robot
Force Tags    API	NON-CRITICAL
Default Tags

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
${device_id}        ${device_ids}

*** Test Cases ***

Get device_logs_list details
    [Tags]      BUG_CLOUD-9675
	[Documentation]	This is the endpoint which get the company details. Status code 200 is expected.
	...	Endpoint: /device/${device_id}/logs

	${response}=    CLOUD::API::Send Get Request    /device/${device_id}/logs
	Should Be Equal   ${response.status_code}   ${200}

device_logs_list (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which get the company details. Status code 403 is expected.
	...	Endpoint: /device/${device_id}/logs

	${response}=    CLOUD::API::Send Get Request Error 403    /device/${device_id}/logs
	Should Be Equal   ${response.status_code}   ${403}

device_logs_list (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which get the company details. Status code 404 is expected.
	...	Endpoint: /device/${device_id}/logss

	${response}=    CLOUD::API::Send Get Request Error 404    /device/${device_id}/logss
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session