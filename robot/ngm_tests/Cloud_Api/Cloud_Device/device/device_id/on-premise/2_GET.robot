*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../../init.robot
Force Tags    API	NEED-CREATION
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
${device_id}        ${device_ids}

*** Test Cases ***

Get device_on-premise_list details
	[Documentation]	This is the endpoint which get the company details. Status code 200 is expected.
	...	Endpoint: /device/${device_id}/on-premise

	${response}=    CLOUD::API::Send Get Request Error 403    /device/${device_id}/on-premise
	Should Be Equal   ${response.status_code}   ${403}

device_on-premise_list (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which get the company details. Status code 403 is expected.
	...	Endpoint: /device/${device_id}/on-premise

	${response}=    CLOUD::API::Send Get Request Error 403    /device/${device_id}/on-premise
	Should Be Equal   ${response.status_code}   ${403}

device_on-premise_list (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which get the company details. Status code 404 is expected.
	...	Endpoint: /device/${device_id}/on-premises

	${response}=    CLOUD::API::Send Get Request Error 404    /device/${device_id}/on-premises
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session