*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	NEED-CREATION
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

profile_read (status_code = 200)
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /profile/${profile_id}

	${response}=   CLOUD::API::Send Get Request    /profile/${profile_id}
	Should Be Equal   ${response.status_code}   ${200}

profile_read (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which get the company details. Status code 403 is expected.
	...	Endpoint: /profile/${profile_id}

	${response}=    CLOUD::API::Send Get Request Error 403    /profile/${profile_id}
	Should Be Equal   ${response.status_code}   ${403}

profile_read (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which get the company details. Status code 404 is expected.
	...	Endpoint: /profiles/${profile_id}

	${response}=    CLOUD::API::Send Get Request Error 404    /profiles/${profile_id}
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session