*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../init.robot
Force Tags    API	NEED-CREATION
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***
#profile_delete (status_code = 204)#	[Documentation]	>-. Status code 204 is expected. 
#	...	Endpoint: /profile/{profile_id}
#	${PAYLOAD}=	Create Dictionary
##	${response}=   CLOUD::API::Send Delete Request    PAYLOAD=${PAYLOAD}    PATH=/profile/{profile_id}#	Should Be Equal   ${response.status_code}   ${204}
#profile_delete (status_code = 400)#	[Documentation]	>-. Status code 400 is expected. 
#	...	Endpoint: /profile/{profile_id}
#	${PAYLOAD}=	Create Dictionary
##	${response}=   CLOUD::API::Send Delete Request    PAYLOAD=${PAYLOAD}    PATH=/profile/{profile_id}#	Should Be Equal   ${response.status_code}   ${400}
#profile_delete (status_code = 403)#	[Documentation]	>-. Status code 403 is expected. 
#	...	Endpoint: /profile/{profile_id}
#	${PAYLOAD}=	Create Dictionary
##	${response}=   CLOUD::API::Send Delete Request    PAYLOAD=${PAYLOAD}    PATH=/profile/{profile_id}#	Should Be Equal   ${response.status_code}   ${403}
#profile_delete (status_code = 404)#	[Documentation]	>-. Status code 404 is expected. 
#	...	Endpoint: /profile/{profile_id}
#	${PAYLOAD}=	Create Dictionary
##	${response}=   CLOUD::API::Send Delete Request    PAYLOAD=${PAYLOAD}    PATH=/profile/{profile_id}#	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session