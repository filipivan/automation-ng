*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../init.robot
Force Tags    API	NON-CRITICAL
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
${PROFILE_NAME}        Test_profile
${DESCRIPTION}      Testing on API
${Prof_type}      CONFIGURATION
${Prof_def}      false
${Prof_dyn}      false
${Prof_un_custom}      false
${Prof_custom}
${Prof_is_custom}      false
${Prof_file}      profile.cfg

*** Test Cases ***

#profile_create (status_code = 200)
#	[Documentation]	>-. Status code 200 is expected.
#	...	Endpoint: /profile

#	${PAYLOAD}=	Evaluate    {'name':(None,${PROFILE_NAME}), 'description':(None,${DESCRIPTION}), 'type':(None,${Prof_type}), 'dynamic':(None,${Prof_dyn}), 'undefined_custom_field':(None,${Prof_un_custom}), 'custom_command_name':(None,${Prof_custom}), 'is_custom_command_enabled':(None,${Prof_is_custom}), 'file':${Prof_file}}
#    ${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	name=${PROFILE_NAME}
#	Set To Dictionary	${payload}	description=${DESCRIPTION}
#	Set To Dictionary	${payload}	type=${Prof_type}
#	Set To Dictionary	${payload}	default=${Prof_def}
#	Set To Dictionary	${payload}	dynamic=${Prof_dyn}
#	Set To Dictionary	${payload}	undefined_custom_field=${Prof_un_custom}
#	Set To Dictionary	${payload}	custom_command_name=${Prof_custom}
#	Set To Dictionary	${payload}	is_custom_command_enabled=${Prof_is_custom}
#	Set To Dictionary	${payload}	file=${Prof_file}

#	${response}=    CLOUD::API::Send Post Request Formdata    PAYLOAD=${PAYLOAD}    PATH=/profile
#	Should Be Equal   ${response.status_code}   ${200}

profile_create (status_code = 400) with Invalid body
	[Documentation]	>-. Status code 400 is expected.
	...	Endpoint: /profile

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	names=${PROFILE_NAME}
	Set To Dictionary	${payload}	description=${DESCRIPTION}
	Set To Dictionary	${payload}	type=${Prof_type}
	Set To Dictionary	${payload}	default=${Prof_def}
	Set To Dictionary	${payload}	dynamic=${Prof_dyn}
	Set To Dictionary	${payload}	undefined_custom_field=${Prof_un_custom}
	Set To Dictionary	${payload}	custom_command_name=${Prof_custom}
	Set To Dictionary	${payload}	iscustomcommand_enabled=${Prof_is_custom}
	Set To Dictionary	${payload}	file=${Prof_file}

	${response}=    CLOUD::API::Send Post Request Error 400    PAYLOAD=${PAYLOAD}    PATH=/profile
	Should Be Equal   ${response.status_code}   ${400}

profile_create (status_code = 403) with wrong credntials
	[Documentation]	>-. Status code 403 is expected.
	...	Endpoint: /profile

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${PROFILE_NAME}
	Set To Dictionary	${payload}	description=${DESCRIPTION}
	Set To Dictionary	${payload}	type=${Prof_type}
	Set To Dictionary	${payload}	default=${Prof_def}
	Set To Dictionary	${payload}	dynamic=${Prof_dyn}
	Set To Dictionary	${payload}	undefined_custom_field=${Prof_un_custom}
	Set To Dictionary	${payload}	custom_command_name=${Prof_custom}
	Set To Dictionary	${payload}	is_custom_command_enabled=${Prof_is_custom}
	Set To Dictionary	${payload}	file=${Prof_file}

	${response}=    CLOUD::API::Send Post Request Error 403    PAYLOAD=${PAYLOAD}    PATH=/profile
	Should Be Equal   ${response.status_code}   ${403}

profile_create (status_code = 404) with wrong endpoint
	[Documentation]	>-. Status code 404 is expected.
	...	Endpoint: /profiles

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${PROFILE_NAME}
	Set To Dictionary	${payload}	description=${DESCRIPTION}
	Set To Dictionary	${payload}	type=${Prof_type}
	Set To Dictionary	${payload}	default=${Prof_def}
	Set To Dictionary	${payload}	dynamic=${Prof_dyn}
	Set To Dictionary	${payload}	undefined_custom_field=${Prof_un_custom}
	Set To Dictionary	${payload}	custom_command_name=${Prof_custom}
	Set To Dictionary	${payload}	is_custom_command_enabled=${Prof_is_custom}
	Set To Dictionary	${payload}	file=${Prof_file}

	${response}=    CLOUD::API::Send Post Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/profiles
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session