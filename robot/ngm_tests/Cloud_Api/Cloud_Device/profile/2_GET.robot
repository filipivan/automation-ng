*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../init.robot
Force Tags    API	NEED-CREATION
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

profile_list (status_code = 200)
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /profile

	${response}=   CLOUD::API::Send Get Request    /profile
	Should Be Equal   ${response.status_code}   ${200}
	${profile_list}=	    Get From Dictionary	${response.json()}    list
	${profile_id}=	    Get From List  	${profile_list}    0
    ${profile_id}=	    Get From Dictionary  	${profile_id}    id
    Set Global Variable     ${profile_id}

profile_list (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which get the company details. Status code 403 is expected.
	...	Endpoint: /profile

	${response}=    CLOUD::API::Send Get Request Error 403    /profile
	Should Be Equal   ${response.status_code}   ${403}

profile_list (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which get the company details. Status code 404 is expected.
	...	Endpoint: /profiles

	${response}=    CLOUD::API::Send Get Request Error 404    /profiles
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session