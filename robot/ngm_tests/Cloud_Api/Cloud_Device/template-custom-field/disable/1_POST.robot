*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	NON-CRITICAL
Default Tags

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
@{STATIC_ID}        ${custom_field_ids}
@{DYN_ID}

*** Test Cases ***

template-custom-field_disable_create (status_code = 200)
	[Documentation]	>-. Status code 200 is expected.
	...	Endpoint: /template-custom-field/disable

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	static_ids=@{STATIC_ID}
	Set To Dictionary	${payload}	dynamic_ids=@{DYN_ID}

	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/template-custom-field/disable
	Should Be Equal   ${response.status_code}   ${200}

template-custom-field_disable_create (status_code = 400) with Invalid body
    [Tags]      BUG_CLOUD-9766
	[Documentation]	>-. Status code 400 is expected.
	...	Endpoint: /template-custom-field/disable

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	staticids=@{STATIC_ID}
	Set To Dictionary	${payload}	dynamicids=@{DYN_ID}

	${response}=    CLOUD::API::Send Post Request Error 400    PAYLOAD=${PAYLOAD}    PATH=/template-custom-field/disable
	Should Be Equal   ${response.status_code}   ${400}

template-custom-field_disable_create (status_code = 403) with wrong credntials
	[Documentation]	>-. Status code 403 is expected.
	...	Endpoint: /template-custom-field/disable

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	static_ids=@{STATIC_ID}
	Set To Dictionary	${payload}	dynamic_ids=@{DYN_ID}

	${response}=    CLOUD::API::Send Post Request Error 403    PAYLOAD=${PAYLOAD}    PATH=/template-custom-field/disable
	Should Be Equal   ${response.status_code}   ${403}

template-custom-field_disable_create (status_code = 404) with wrong endpoint
	[Documentation]	>-. Status code 404 is expected.
	...	Endpoint: /template-custom-field/disables

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	static_ids=@{STATIC_ID}
	Set To Dictionary	${payload}	dynamic_ids=@{DYN_ID}

	${response}=    CLOUD::API::Send Post Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/template-custom-field/disables
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session