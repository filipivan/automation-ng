*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../init.robot
Force Tags    API	NEED-CREATION
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
${TEMP_CUS_NAME}        Test_temp
${SCOPE}        global
${VALUE_TEMP}       99

*** Test Cases ***

Put template-custom-field_create
	[Documentation]	This is the endpoint which change the company details. Status code 200 is expected.
	...	Endpoint: /template-custom-field

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${TEMP_CUS_NAME}
	Set To Dictionary	${payload}	scope=${SCOPE}
	Set To Dictionary	${payload}	value=${VALUE_TEMP}
	Set To Dictionary	${payload}	reference=${null}

	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/template-custom-field
	Should Be Equal   ${response.status_code}   ${201}

template-custom-field_create (status_code = 400) with invalid body
	[Documentation]	This is the endpoint which change the company details. Status code 400 is expected.
	...	Endpoint: /template-custom-field

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	namse=${TEMP_CUS_NAME}
	Set To Dictionary	${payload}	scopes=${SCOPE}
	Set To Dictionary	${payload}	values=${VALUE_TEMP}
	Set To Dictionary	${payload}	references=${null}

	${response}=    CLOUD::API::Send Post Request Error 400   PAYLOAD=${PAYLOAD}    PATH=/template-custom-field
	Should Be Equal   ${response.status_code}   ${400}

template-custom-field_create (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which change the company details. Status code 403 is expected.
	...	Endpoint: /company/{company_id}

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${TEMP_CUS_NAME}
	Set To Dictionary	${payload}	scope=${SCOPE}
	Set To Dictionary	${payload}	value=${VALUE_TEMP}
	Set To Dictionary	${payload}	reference=${null}

	${response}=    CLOUD::API::Send Post Request Error 403   PAYLOAD=${PAYLOAD}    PATH=/template-custom-field
	Should Be Equal   ${response.status_code}   ${403}

template-custom-field_create (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which change the company details. Status code 404 is expected.
	...	Endpoint: /template-custom-fields

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${TEMP_CUS_NAME}
	Set To Dictionary	${payload}	scope=${SCOPE}
	Set To Dictionary	${payload}	value=${VALUE_TEMP}
	Set To Dictionary	${payload}	reference=${null}

	${response}=    CLOUD::API::Send Post Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/template-custom-fields
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session