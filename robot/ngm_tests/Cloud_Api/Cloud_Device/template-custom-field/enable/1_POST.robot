*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	NEED-CREATION
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
@{STATIC_ID}        ${custom_field_ids}
${DYN_ID}       false

*** Test Cases ***

template-custom-field_enable_create (status_code = 200)
	[Documentation]	>-. Status code 200 is expected.
	...	Endpoint: /template-custom-field/enable

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	ids=@{STATIC_ID}
	Set To Dictionary	${payload}	dynamic=${DYN_ID}

	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/template-custom-field/enable
	Should Be Equal   ${response.status_code}   ${200}

template-custom-field_enable_create (status_code = 400) with Invalid body
	[Documentation]	>-. Status code 400 is expected.
	...	Endpoint: /template-custom-field/enable

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	ids=@{STATIC_ID}
	Set To Dictionary	${payload}	dynamics=${DYN_ID}

	${response}=    CLOUD::API::Send Post Request Error 400    PAYLOAD=${PAYLOAD}    PATH=/template-custom-field/enable
	Should Be Equal   ${response.status_code}   ${400}

template-custom-field_enable_create (status_code = 403) with wrong credntials
	[Documentation]	>-. Status code 403 is expected.
	...	Endpoint: /template-custom-field/enable

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	ids=@{STATIC_ID}
	Set To Dictionary	${payload}	dynamic=${DYN_ID}

	${response}=    CLOUD::API::Send Post Request Error 403    PAYLOAD=${PAYLOAD}    PATH=/template-custom-field/enable
	Should Be Equal   ${response.status_code}   ${403}

template-custom-field_enable_create (status_code = 404) with wrong endpoint
	[Documentation]	>-. Status code 404 is expected.
	...	Endpoint: /template-custom-field/enables

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	ids=@{STATIC_ID}
	Set To Dictionary	${payload}	dynamic=${DYN_ID}

	${response}=    CLOUD::API::Send Post Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/template-custom-field/enables
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session