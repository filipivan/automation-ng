*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../init.robot
Force Tags    API	NEED-CREATION
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

template-custom-field_list (status_code = 200)
	[Documentation]	Return a list of all custom fields.. Status code 200 is expected. 
	...	Endpoint: /template-custom-field

	${response}=   CLOUD::API::Send Get Request    /template-custom-field
	Should Be Equal   ${response.status_code}   ${200}
	${get_list}=	    Get From Dictionary	${response.json()}    list
	${get_id}=	    Get From List  	${get_list}    0
    ${custom_field_ids}=	    Get From Dictionary  	${get_id}    id
    Set Global Variable     ${custom_field_ids}

template-custom-field_list (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which get the company details. Status code 403 is expected.
	...	Endpoint: /template-custom-field

	${response}=    CLOUD::API::Send Get Request Error 403    /template-custom-field
	Should Be Equal   ${response.status_code}   ${403}

template-custom-field_list (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which get the company details. Status code 404 is expected.
	...	Endpoint: /template-customfield

	${response}=    CLOUD::API::Send Get Request Error 404    /template-customfield
	Should Be Equal   ${response.status_code}   ${404}

*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session