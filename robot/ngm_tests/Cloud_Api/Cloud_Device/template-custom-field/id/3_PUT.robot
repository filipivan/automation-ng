*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	NEED-CREATION
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
${id}       ${custom_field_ids}
${TEMP_CUS_NAME}        Test_temp
${SCOPE}        global
${VALUE_TEMP}       99

*** Test Cases ***

Put template-custom-field_update
	[Documentation]	This is the endpoint which change the company details. Status code 200 is expected.
	...	Endpoint: /template-custom-field/${id}

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${TEMP_CUS_NAME}
	Set To Dictionary	${payload}	scope=${SCOPE}
	Set To Dictionary	${payload}	value=${VALUE_TEMP}
	Set To Dictionary	${payload}	reference=${null}

	${response}=    CLOUD::API::Send Put Request    PAYLOAD=${PAYLOAD}    PATH=/template-custom-field/${id}
	Should Be Equal   ${response.status_code}   ${200}

template-custom-field_update (status_code = 400) with invalid body
	[Documentation]	This is the endpoint which change the company details. Status code 400 is expected.
	...	Endpoint: /template-custom-field/${id}

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${TEMP_CUS_NAME}
	Set To Dictionary	${payload}	scope=${SCOPE}
	Set To Dictionary	${payload}	values=${VALUE_TEMP}
	Set To Dictionary	${payload}	references=${null}

	${response}=    CLOUD::API::Send Put Request Error 400   PAYLOAD=${PAYLOAD}    PATH=/template-custom-field/${id}
	Should Be Equal   ${response.status_code}   ${400}

template-custom-field_update (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which change the company details. Status code 403 is expected.
	...	Endpoint: /company/{company_id}

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${TEMP_CUS_NAME}
	Set To Dictionary	${payload}	scope=${SCOPE}
	Set To Dictionary	${payload}	value=${VALUE_TEMP}
	Set To Dictionary	${payload}	reference=${null}

	${response}=    CLOUD::API::Send Put Request Error 403   PAYLOAD=${PAYLOAD}    PATH=/template-custom-field/${id}
	Should Be Equal   ${response.status_code}   ${403}

template-custom-field_update (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which change the company details. Status code 404 is expected.
	...	Endpoint: /template-custom-field/${id}s

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${TEMP_CUS_NAME}
	Set To Dictionary	${payload}	scope=${SCOPE}
	Set To Dictionary	${payload}	value=${VALUE_TEMP}
	Set To Dictionary	${payload}	reference=${null}

	${response}=    CLOUD::API::Send Put Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/template-custom-field/${id}s
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session