*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	NEED-CREATION
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
${id}       ${custom_field_ids}

*** Test Cases ***

template-custom-field_read (status_code = 200)
	[Documentation]	Returns custom field.. Status code 200 is expected. 
	...	Endpoint: /template-custom-field/${id}

	${response}=   CLOUD::API::Send Get Request    /template-custom-field/${id}
	Should Be Equal   ${response.status_code}   ${200}

template-custom-field_read (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which get the company details. Status code 403 is expected.
	...	Endpoint: /template-custom-field/${id}

	${response}=    CLOUD::API::Send Get Request Error 403    /template-custom-field/${id}
	Should Be Equal   ${response.status_code}   ${403}

template-custom-field_read (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which get the company details. Status code 404 is expected.
	...	Endpoint: /template-customfield/${id}

	${response}=    CLOUD::API::Send Get Request Error 404    /template-customfield/${id}
	Should Be Equal   ${response.status_code}   ${404}

*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session