*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	NEED-CREATION
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
@{Testing}      1450

*** Test Cases ***

template-custom-field_delete (status_code = 200)
	[Documentation]	Delete custom fields from ID list. Status code 200 is expected. 
	...	Endpoint: /template-custom-field

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	ids=@{Testing}

	${response}=   CLOUD::API::Send Delete Request    PAYLOAD=${PAYLOAD}    PATH=/template-custom-field
	Should Be Equal   ${response.status_code}   ${204}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session