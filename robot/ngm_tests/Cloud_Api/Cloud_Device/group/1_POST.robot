*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../init.robot
Force Tags    API	NON-CRITICAL
Default Tags

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
${GROUP_NAME}       Test_group
${ACCESS_LEVEL}       2

*** Test Cases ***

Put group_create
	[Documentation]	This is the endpoint which change the company details. Status code 200 is expected.
	...	Endpoint: /group

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${GROUP_NAME}
	Set To Dictionary	${payload}	access_level=${ACCESS_LEVEL}

	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/group
	Should Be Equal   ${response.status_code}   ${201}

group_create (status_code = 400) with invalid body
    [Documentation]	This is the endpoint which change the company details. Status code 400 is expected.
	...	Endpoint: /group

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	names=${GROUP_NAME}
	Set To Dictionary	${payload}	accesslevel=${ACCESS_LEVEL}

	${response}=    CLOUD::API::Send Post Request Error 400   PAYLOAD=${PAYLOAD}    PATH=/group
	Should Be Equal   ${response.status_code}   ${400}

group_create (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which change the company details. Status code 403 is expected.
	...	Endpoint: /company/{company_id}

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${GROUP_NAME}
	Set To Dictionary	${payload}	access_level=${ACCESS_LEVEL}

	${response}=    CLOUD::API::Send Post Request Error 403   PAYLOAD=${PAYLOAD}    PATH=/group
	Should Be Equal   ${response.status_code}   ${403}

group_create (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which change the company details. Status code 404 is expected.
	...	Endpoint: /groups

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${GROUP_NAME}
	Set To Dictionary	${payload}	access_level=${ACCESS_LEVEL}

	${response}=    CLOUD::API::Send Post Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/groups
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session