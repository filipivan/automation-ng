*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../../../init.robot
Force Tags    API	NON-CRITICAL
Default Tags

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
${device_id}        ${device_ids}

*** Test Cases ***

group_device_create (status_code = 200)
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /group/${group_id}/device/${device_id}

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	device_id=${device_id}
	Set To Dictionary	${payload}	group_id=${group_id}

	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/group/${group_id}/device/${device_id}
	Should Be Equal   ${response.status_code}   ${200}

group_device_create (status_code = 400) with Invalid body
    [Tags]      BUG_CLOUD-9731
	[Documentation]	>-. Status code 400 is expected.
	...	Endpoint: /group/${group_id}/device/${device_id}

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	device_id=${device_id}
	Set To Dictionary	${payload}	groupid=${group_id}

	${response}=    CLOUD::API::Send Post Request Error 400    PAYLOAD=${PAYLOAD}    PATH=/group/${group_id}/device/${device_id}
	Should Be Equal   ${response.status_code}   ${400}

group_device_create (status_code = 403) with wrong credntials
	[Documentation]	>-. Status code 403 is expected.
	...	Endpoint: /group/${group_id}/device/${device_id}

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	device_id=${device_id}
	Set To Dictionary	${payload}	group_id=${group_id}

	${response}=    CLOUD::API::Send Post Request Error 403    PAYLOAD=${PAYLOAD}    PATH=/group/${group_id}/device/${device_id}
	Should Be Equal   ${response.status_code}   ${403}

group_device_create (status_code = 404) with wrong endpoint
	[Documentation]	>-. Status code 404 is expected.
	...	Endpoint: /group/${group_id}/device/${device_id}s

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	device_id=${device_id}
	Set To Dictionary	${payload}	group_id=${group_id}

	${response}=    CLOUD::API::Send Post Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/group/${group_id}/device/${device_id}s
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session