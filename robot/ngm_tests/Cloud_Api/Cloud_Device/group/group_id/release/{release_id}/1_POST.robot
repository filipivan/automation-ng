*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../../../init.robot
Force Tags    API	NEED-CREATION
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***

group_release_create (status_code = 404) with wrong endpoint
	[Documentation]	>-. Status code 404 is expected.
	...	Endpoint: /group/${group_id}/release/{release_id}s

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/group/${group_id}/release/{release_id}s
	Should Be Equal   ${response.status_code}   ${404}

group_release_create (status_code = 404) with wrong endpoint
	[Documentation]	>-. Status code 404 is expected.
	...	Endpoint: /group/${group_id}/release/{release_id}s

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/group/${group_id}/release/{release_id}s
	Should Be Equal   ${response.status_code}   ${404}

group_release_create (status_code = 404) with wrong endpoint
	[Documentation]	>-. Status code 404 is expected.
	...	Endpoint: /group/${group_id}/release/{release_id}s

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/group/${group_id}/release/{release_id}s
	Should Be Equal   ${response.status_code}   ${404}

group_release_create (status_code = 404) with wrong endpoint
	[Documentation]	>-. Status code 404 is expected.
	...	Endpoint: /group/${group_id}/release/{release_id}s

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/group/${group_id}/release/{release_id}s
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session