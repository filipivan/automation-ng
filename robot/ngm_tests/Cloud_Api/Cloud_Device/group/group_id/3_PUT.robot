*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	NON-CRITICAL
Default Tags

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
${ACCESS_LEVEL}     2

*** Test Cases ***

Put group_update
	[Documentation]	This is the endpoint which change the company details. Status code 200 is expected.
	...	Endpoint: /group/${group_id}

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${group_id}
	Set To Dictionary	${payload}	access_level=${ACCESS_LEVEL}
	Set To Dictionary	${payload}	is_default=true

	${response}=    CLOUD::API::Send Put Request    PAYLOAD=${PAYLOAD}    PATH=/group/${group_id}
	Should Be Equal   ${response.status_code}   ${200}

group_update (status_code = 400) with invalid body
	[Documentation]	This is the endpoint which change the company details. Status code 400 is expected.
	...	Endpoint: /group/${group_id}

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${group_id}
	Set To Dictionary	${payload}	access_level=${ACCESS_LEVEL}
	Set To Dictionary	${payload}	isdefault=true

	${response}=    CLOUD::API::Send Put Request Error 400   PAYLOAD=${PAYLOAD}    PATH=/group/${group_id}
	Should Be Equal   ${response.status_code}   ${400}

group_update (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which change the company details. Status code 403 is expected.
	...	Endpoint: /company/${company_id}

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${group_id}
	Set To Dictionary	${payload}	access_level=${ACCESS_LEVEL}
	Set To Dictionary	${payload}	is_default=true

	${response}=    CLOUD::API::Send Put Request Error 403   PAYLOAD=${PAYLOAD}    PATH=/group/${group_id}
	Should Be Equal   ${response.status_code}   ${403}

group_update (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which change the company details. Status code 404 is expected.
	...	Endpoint: /group/${group_id}s

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${group_id}
	Set To Dictionary	${payload}	access_level=${ACCESS_LEVEL}
	Set To Dictionary	${payload}	is_default=true

	${response}=    CLOUD::API::Send Put Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/group/${group_id}s
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session