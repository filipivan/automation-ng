*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../../../init.robot
Force Tags    API	NEED-CREATION
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***
user_list (status_code = 200)
	[Documentation]	>-. Status code 200 is expected.
	...	Endpoint: /user

	${response}=    CLOUD::API::Send Get Request    /user
	Should Be Equal   ${response.status_code}   ${200}
	${user_list}=	    Get From Dictionary	${response.json()}    list
	${user_id}=	    Get From List  	${user_list}    0
    ${user_id}=	    Get From Dictionary  	${user_id}    id
    Set Global Variable     ${user_id}

group_admin_create (status_code = 200)
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /group/${group_id}/admin/${user_id}

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request Error 400    PAYLOAD=${PAYLOAD}    PATH=/group/${group_id}/admin/${user_id}
	Should Be Equal   ${response.status_code}   ${400}

group_admin_create (status_code = 400) with Invalid body
	[Documentation]	>-. Status code 400 is expected.
	...	Endpoint: /group/${group_id}/admin/${user_id}

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request Error 400    PAYLOAD=${PAYLOAD}    PATH=/group/${group_id}/admin/${user_id}
	Should Be Equal   ${response.status_code}   ${400}

group_admin_create (status_code = 403) with wrong credntials
	[Documentation]	>-. Status code 403 is expected.
	...	Endpoint: /group/${group_id}/admin/${user_id}

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request Error 403    PAYLOAD=${PAYLOAD}    PATH=/group/${group_id}/admin/${user_id}
	Should Be Equal   ${response.status_code}   ${403}

group_admin_create (status_code = 404) with wrong endpoint
	[Documentation]	>-. Status code 404 is expected.
	...	Endpoint: /group/${group_id}/admin/${user_id}s

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/group/${group_id}/admin/${user_id}s
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session