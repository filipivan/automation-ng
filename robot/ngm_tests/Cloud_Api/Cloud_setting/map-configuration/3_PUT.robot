*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
${zoom}     0
${lat}      0
${lng}      0
${site}     true
${device}       true

*** Test Cases ***

map-configuration_update (status_code = 200)
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /map-configuration

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	zoom=${zoom}
	Set To Dictionary	${payload}	lat=${lat}
	Set To Dictionary	${payload}	lng=${lng}
	Set To Dictionary	${payload}	site=${site}
	Set To Dictionary	${payload}	device=${device}

	${response}=    CLOUD::API::Send Put Request    PAYLOAD=${PAYLOAD}    PATH=/map-configuration
	Should Be Equal   ${response.status_code}   ${200}

map-configuration_update (status_code = 400) with invalid body
	[Documentation]	This is the endpoint which change the company details. Status code 400 is expected.
	...	Endpoint: /map-configuration

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	zoom=${zoom}
	Set To Dictionary	${payload}	lat=${lat}

	${response}=    CLOUD::API::Send Put Request Error 400   PAYLOAD=${PAYLOAD}    PATH=/map-configuration
	Should Be Equal   ${response.status_code}   ${400}

map-configuration_update (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which change the company details. Status code 403 is expected.
	...	Endpoint: /map-configuration

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	zoom=${zoom}
	Set To Dictionary	${payload}	lat=${lat}
	Set To Dictionary	${payload}	lng=${lng}
	Set To Dictionary	${payload}	site=${site}
	Set To Dictionary	${payload}	device=${device}

	${response}=    CLOUD::API::Send Put Request Error 403   PAYLOAD=${PAYLOAD}    PATH=/map-configuration
	Should Be Equal   ${response.status_code}   ${403}

map-configuration_update (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which change the company details. Status code 404 is expected.
	...	Endpoint: /mapconfiguration

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	zoom=${zoom}
	Set To Dictionary	${payload}	lat=${lat}
	Set To Dictionary	${payload}	lng=${lng}
	Set To Dictionary	${payload}	site=${site}
	Set To Dictionary	${payload}	device=${device}

	${response}=    CLOUD::API::Send Put Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/mapconfiguration
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session