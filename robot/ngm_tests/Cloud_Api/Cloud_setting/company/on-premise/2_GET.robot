*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

company_on-premise_list (status_code = 200)
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /company/on-premise

	${response}=    CLOUD::API::Send Get Request Error 403    /company/on-premise
	Should Be Equal   ${response.status_code}   ${403}

company_on-premise_list (status_code = 403) wrong credentials
	[Documentation]	>-. Status code 403 is expected.
	...	Endpoint: /company/on-premise

	${response}=    CLOUD::API::Send Get Request Error 403    /company/on-premise
	Should Be Equal   ${response.status_code}   ${403}

company_on-premise_list (status_code = 404) wrong endpoint
	[Documentation]	>-. Status code 404 is expected.
	...	Endpoint: /company/on-premise

	${response}=    CLOUD::API::Send Get Request Error 404    /company/onpremise
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session