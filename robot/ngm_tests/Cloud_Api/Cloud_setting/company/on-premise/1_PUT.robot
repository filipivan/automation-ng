*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
${is_active}        false
${url}      https://testing-zpecloud.com
${enrollment_key}       HYW8MT
${customer_code}        Test@123567gfdBGG

*** Test Cases ***

company_on-premise_update (status_code = 200)
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /company/on-premise

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	is_active=${is_active}
	Set To Dictionary	${payload}	url=${url}
	Set To Dictionary	${payload}	enrollment_key=${enrollment_key}
	Set To Dictionary	${payload}	customer_code=${customer_code}

	${response}=    CLOUD::API::Send Put Request    PAYLOAD=${PAYLOAD}    PATH=/company/on-premise
	Should Be Equal   ${response.status_code}   ${200}

company_on-premise_update (status_code = 400) with invalid body
	[Documentation]	>-. Status code 200 is expected.
	...	Endpoint: /company/on-premise

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	is_active=${is_active}
	Set To Dictionary	${payload}	url=${url}

	${response}=    CLOUD::API::Send Put Request Error 400    PAYLOAD=${PAYLOAD}    PATH=/company/on-premise
	Should Be Equal   ${response.status_code}   ${400}

company_on-premise_update (status_code = 403) wrong credentials
	[Documentation]	>-. Status code 200 is expected.
	...	Endpoint: /company/on-premise

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	is_active=${is_active}
	Set To Dictionary	${payload}	url=${url}
	Set To Dictionary	${payload}	enrollment_key=${enrollment_key}
	Set To Dictionary	${payload}	customer_code=${customer_code}

	${response}=    CLOUD::API::Send Put Request Error 403    PAYLOAD=${PAYLOAD}    PATH=/company/on-premise
	Should Be Equal   ${response.status_code}   ${403}

company_on-premise_update (status_code = 404) wrong endpoint
	[Documentation]	>-. Status code 200 is expected.
	...	Endpoint: /company/on-premise

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	is_active=${is_active}
	Set To Dictionary	${payload}	url=${url}
	Set To Dictionary	${payload}	enrollment_key=${enrollment_key}
	Set To Dictionary	${payload}	customer_code=${customer_code}

	${response}=    CLOUD::API::Send Put Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/company/onpremise
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session