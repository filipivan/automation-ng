*** Settings ***
Documentation    Test API Get samples company_ip-address-whitelist_list
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	NON-DEVICE
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get company_ip-address-whitelist_list (status_code = 200)
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /company/ip-address-whitelist

	${response}=    CLOUD::API::Send Get Request    /company/ip-address-whitelist
	Should Be Equal   ${response.status_code}   ${200}

company_ip-address-whitelist_list (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which get the company details. Status code 403 is expected.
	...	Endpoint: /company/ip-address-whitelist

	${response}=    CLOUD::API::Send Get Request Error 403    /company/ip-address-whitelist
	Should Be Equal   ${response.status_code}   ${403}

company_ip-address-whitelist_list (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which get the company details. Status code 404 is expected.
	...	Endpoint: /company/ip-address-whitelist

	${response}=    CLOUD::API::Send Get Request Error 404    /company/ipaddresswhitelist
	Should Be Equal   ${response.status_code}   ${404}

*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session