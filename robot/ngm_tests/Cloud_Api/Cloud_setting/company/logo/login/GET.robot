*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../../init.robot
Force Tags    API	    NON-CRITICAL
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

company_logo_read (status_code = 200)
	[Documentation]	This is the endpoint which get the company logo url. Status code 200 is expected. 
	...	Endpoint: /company/logo/{login}

	${response}=    CLOUD::API::Send Get Request    /company/logo/${EMAIL}
	Should Be Equal   ${response.status_code}   ${200}

company_logo_read (status_code = 403) wrong credentials
    [Tags]      BUG_CLOUD-9675
	[Documentation]	This is the endpoint which get the company details. Status code 403 is expected.
	...	Endpoint: /company/logo/{login}

	${response}=    CLOUD::API::Send Get Request Error 403    /company/logo/${EMAIL}
	Should Be Equal   ${response.status_code}   ${403}

company_logo_read (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which get the company details. Status code 404 is expected.
	...	Endpoint: /company/logo/{login}

	${response}=    CLOUD::API::Send Get Request Error 404    /company/${EMAIL}
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session