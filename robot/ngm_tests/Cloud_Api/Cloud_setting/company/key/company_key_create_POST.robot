*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
${company_key}      Test@123
${enable}       "True"

*** Test Cases ***

company_key_create (status_code = 200)
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /company/key

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	company_key=${company_key}
	Set To Dictionary	${payload}	key_enabled=${enable}

	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/company/key
	Should Be Equal   ${response.status_code}   ${200}

company_key_create (status_code = 400) with Invalid body
	[Documentation]	>-. Status code 400 is expected.
	...	Endpoint: /company/key

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	key_enabled=${enable}

	${response}=    CLOUD::API::Send Post Request Error 400    PAYLOAD=${PAYLOAD}    PATH=/company/key
	Should Be Equal   ${response.status_code}   ${400}

company_key_create (status_code = 403) with wrong credntials
	[Documentation]	>-. Status code 403 is expected.
	...	Endpoint: /company/key

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	company_key=${company_key}
	Set To Dictionary	${payload}	key_enabled=${enable}

	${response}=    CLOUD::API::Send Post Request Error 403    PAYLOAD=${PAYLOAD}    PATH=/company/key
	Should Be Equal   ${response.status_code}   ${403}

company_key_create (status_code = 404) with wrong endpoint
	[Documentation]	>-. Status code 404 is expected.
	...	Endpoint: /company/key

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	company_key=${company_key}
	Set To Dictionary	${payload}	key_enabled=${enable}

	${response}=    CLOUD::API::Send Post Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/company/keys
	Should Be Equal   ${response.status_code}   ${404}

*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session