*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	NON-CRITICAL	CLOUD-9042
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***

*** Test Cases ***

Get company_read details
	[Documentation]	This is the endpoint which get the company details. Status code 200 is expected. 
	...	Endpoint: /company/{company_id}

	${response}=    CLOUD::API::Send Get Request    /company/${company_id}
	Should Be Equal   ${response.status_code}   ${200}

company_read (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which get the company details. Status code 403 is expected.
	...	Endpoint: /company/${company_id}

	${response}=    CLOUD::API::Send Get Request Error 403    /company/${company_id}
	Should Be Equal   ${response.status_code}   ${403}

company_read (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which get the company details. Status code 404 is expected.
	...	Endpoint: /company/{company_id}

	${response}=    CLOUD::API::Send Get Request Error 404    /company/ancd
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session