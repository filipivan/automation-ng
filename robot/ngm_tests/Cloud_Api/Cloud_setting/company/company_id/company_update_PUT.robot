*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	NON-CRITICAL	CLOUD-9042
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
${company_address}      Jenkins-API
${source_ip_verification}       false
${enforce_ip_verification}       false
${enforce_mfa_verification}       false
@{ip_whitelist}=

*** Test Cases ***

Put company_update
	[Documentation]	This is the endpoint which change the company details. Status code 200 is expected. 
	...	Endpoint: /company/${company_id}

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	business_name=${COMPANY}
	Set To Dictionary	${payload}	address=${company_address}
	Set To Dictionary	${payload}	contact_info=${EMAIL}
	Set To Dictionary	${payload}	source_ip_verification=${source_ip_verification}
	Set To Dictionary	${payload}	enforce_ip_verification=${enforce_ip_verification}
	Set To Dictionary	${payload}	enforce_mfa_verification=${enforce_mfa_verification}
	Set To Dictionary	${payload}	ip_whitelist=@{ip_whitelist}

	${response}=    CLOUD::API::Send Put Request    PAYLOAD=${PAYLOAD}    PATH=/company/${company_id}
	Should Be Equal   ${response.status_code}   ${200}

company_update (status_code = 400) with invalid body
	[Documentation]	This is the endpoint which change the company details. Status code 400 is expected.
	...	Endpoint: /company/${company_id}

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	enforce_mfa_verification=${enforce_mfa_verification}
	Set To Dictionary	${payload}	ip_whitelist=${ip_whitelist}

	${response}=    CLOUD::API::Send Put Request Error 400   PAYLOAD=${PAYLOAD}    PATH=/company/${company_id}
	Should Be Equal   ${response.status_code}   ${400}

company_update (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which change the company details. Status code 403 is expected.
	...	Endpoint: /company/{company_id}

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	business_name=${COMPANY}
	Set To Dictionary	${payload}	address=${company_address}
	Set To Dictionary	${payload}	contact_info=${EMAIL}
	Set To Dictionary	${payload}	source_ip_verification=${source_ip_verification}
	Set To Dictionary	${payload}	enforce_ip_verification=${enforce_ip_verification}
	Set To Dictionary	${payload}	enforce_mfa_verification=${enforce_mfa_verification}
	Set To Dictionary	${payload}	ip_whitelist=@{ip_whitelist}

	${response}=    CLOUD::API::Send Put Request Error 403   PAYLOAD=${PAYLOAD}    PATH=/company/${company_id}
	Should Be Equal   ${response.status_code}   ${403}

company_update (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which change the company details. Status code 404 is expected.
	...	Endpoint: /companys/${company_id}

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	business_name=${COMPANY}
	Set To Dictionary	${payload}	address=${company_address}
	Set To Dictionary	${payload}	contact_info=${EMAIL}
	Set To Dictionary	${payload}	source_ip_verification=${source_ip_verification}
	Set To Dictionary	${payload}	enforce_ip_verification=${enforce_ip_verification}
	Set To Dictionary	${payload}	enforce_mfa_verification=${enforce_mfa_verification}
	Set To Dictionary	${payload}	ip_whitelist=@{ip_whitelist}

	${response}=    CLOUD::API::Send Put Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/companys/${company_id}
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session