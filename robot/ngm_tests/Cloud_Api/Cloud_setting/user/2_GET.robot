*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

user_list (status_code = 200)
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /user

	${response}=    CLOUD::API::Send Get Request    /user
	Should Be Equal   ${response.status_code}   ${200}
	${user_list}=	    Get From Dictionary	${response.json()}    list
	${user_id}=	    Get From List  	${user_list}    0
    ${user_id}=	    Get From Dictionary  	${user_id}    id
    Set Global Variable     ${user_id}

user_list (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which get the company details. Status code 403 is expected.
	...	Endpoint: /user

	${response}=    CLOUD::API::Send Get Request Error 403    /user
	Should Be Equal   ${response.status_code}   ${403}

user_list (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which get the company details. Status code 404 is expected.
	...	Endpoint: /usersss

	${response}=    CLOUD::API::Send Get Request Error 404    /usersss
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session