*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
${OLD_PASS}        Test@12345
${NEW_PASS}        APItest@12345

*** Test Cases ***
user_change-password_create (status_code = 200)
	[Documentation]	>-. Status code 200 is expected.
	...	Endpoint: /user/change-password

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	old_password=${OLD_PASS}
	Set To Dictionary	${payload}	new_password=${NEW_PASS}

	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user/change-password
	Should Be Equal   ${response.status_code}   ${200}

user_change-password_create (status_code = 400)
	[Documentation]	>-. Status code 400 is expected.
	...	Endpoint: /user/change-password

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	oldpassword=${OLD_PASS}
	Set To Dictionary	${payload}	newpassword=${NEW_PASS}

	${response}=    CLOUD::API::Send Post Request Error 400    PAYLOAD=${PAYLOAD}    PATH=/user/change-password
	Should Be Equal   ${response.status_code}   ${400}

user_change-password_create (status_code = 403)
	[Documentation]	>-. Status code 403 is expected.
	...	Endpoint: /user/change-password

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	old_password=${OLD_PASS}
	Set To Dictionary	${payload}	new_password=${NEW_PASS}

	${response}=    CLOUD::API::Send Post Request Error 403    PAYLOAD=${PAYLOAD}    PATH=/user/change-password
	Should Be Equal   ${response.status_code}   ${403}

user_change-password_create (status_code = 404)
	[Documentation]	>-. Status code 404 is expected.
	...	Endpoint: /user/changepassword

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	old_password=${OLD_PASS}
	Set To Dictionary	${payload}	new_password=${NEW_PASS}

	${response}=    CLOUD::API::Send Post Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/user/changepassword
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session