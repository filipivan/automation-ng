*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***

user_register_create (status_code = 200)
	[Documentation]	This is the endpoint for register a new user. Status code 200 is expected. 
	...	Endpoint: /user/register
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user/register
#	Should Be Equal   ${response.status_code}   ${200}

#user_register_create (status_code = 400)
#	[Documentation]	This is the endpoint for register a new user. Status code 400 is expected. 
#	...	Endpoint: /user/register

#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user/register
#	Should Be Equal   ${response.status_code}   ${400}

#user_register_create (status_code = 401)
#	[Documentation]	This is the endpoint for register a new user. Status code 401 is expected. 
#	...	Endpoint: /user/register

#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user/register
#	Should Be Equal   ${response.status_code}   ${401}

#user_register_create (status_code = 403)
#	[Documentation]	This is the endpoint for register a new user. Status code 403 is expected. 
#	...	Endpoint: /user/register

#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user/register
#	Should Be Equal   ${response.status_code}   ${403}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session