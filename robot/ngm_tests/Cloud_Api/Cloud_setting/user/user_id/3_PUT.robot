*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***

user_update (status_code = 200)
	[Documentation]	This is the endpoint which update user detail. Status code 200 is expected. 
	...	Endpoint: /user/{user_id}
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Put Request    PAYLOAD=${PAYLOAD}    PATH=/user/{user_id}
#	Should Be Equal   ${response.status_code}   ${200}

user_update (status_code = 403)
#	[Documentation]	This is the endpoint which update user detail. Status code 403 is expected. 
#	...	Endpoint: /user/{user_id}
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Put Request    PAYLOAD=${PAYLOAD}    PATH=/user/{user_id}
#	Should Be Equal   ${response.status_code}   ${403}

user_update (status_code = 404)
#	[Documentation]	This is the endpoint which update user detail. Status code 404 is expected. 
#	...	Endpoint: /user/{user_id}
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Put Request    PAYLOAD=${PAYLOAD}    PATH=/user/{user_id}
#	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session