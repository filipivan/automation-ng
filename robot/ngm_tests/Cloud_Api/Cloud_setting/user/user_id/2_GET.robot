*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

user_read (status_code = 200)
	[Documentation]	This is the endpoint which get user detail. Status code 200 is expected. 
	...	Endpoint: /user/{user_id}

	${response}=    CLOUD::API::Send Get Request    /user/${user_id}
	Should Be Equal   ${response.status_code}   ${200}

user_read (status_code = 403)
	[Documentation]	This is the endpoint which get user detail. Status code 403 is expected.
	...	Endpoint: /user/{user_id}

	${response}=    CLOUD::API::Send Get Request Error 403     /user/${user_id}
	Should Be Equal   ${response.status_code}   ${403}

user_read (status_code = 404)
	[Documentation]	This is the endpoint which get user detail. Status code 404 is expected.
	...	Endpoint: /user/{user_id}s

	${response}=    CLOUD::API::Send Get Request Error 404     /user/${user_id}s
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session