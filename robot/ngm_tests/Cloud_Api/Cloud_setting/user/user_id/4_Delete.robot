*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***

user_delete (status_code = 204)
	[Documentation]	This is the endpoint which delete user. Status code 204 is expected.
	...	Endpoint: /user/${user_id}

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Delete Request    PAYLOAD=${PAYLOAD}    PATH=/user/${user_id}
	Should Be Equal   ${response.status_code}   ${204}

user_delete (status_code = 403)
	[Documentation]	This is the endpoint which delete user. Status code 403 is expected.
	...	Endpoint: /user/${user_id}
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Delete Request Error 403     PAYLOAD=${PAYLOAD}    PATH=/user/${user_id}
#	Should Be Equal   ${response.status_code}   ${403}

user_delete (status_code = 404)
	[Documentation]	This is the endpoint which delete user. Status code 404 is expected.
	...	Endpoint: /userss/${user_id}
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Delete Request Error 404     PAYLOAD=${PAYLOAD}    PATH=/userss/${user_id}
#	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session