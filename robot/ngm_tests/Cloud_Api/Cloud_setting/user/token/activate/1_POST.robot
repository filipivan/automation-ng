*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***

user_activate_create (status_code = 200)
	[Documentation]	This is the endpoint for active a current user. Status code 200 is expected. 
	...	Endpoint: /user/{token}/activate
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user/{token}/activate
#	Should Be Equal   ${response.status_code}   ${200}

#user_activate_create (status_code = 404)
#	[Documentation]	This is the endpoint for active a current user. Status code 404 is expected. 
#	...	Endpoint: /user/{token}/activate

#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user/{token}/activate
#	Should Be Equal   ${response.status_code}   ${404}

#user_activate_create (status_code = 500)
#	[Documentation]	This is the endpoint for active a current user. Status code 500 is expected. 
#	...	Endpoint: /user/{token}/activate

#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user/{token}/activate
#	Should Be Equal   ${response.status_code}   ${500}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session