*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***

user_forgot-password_create (status_code = 200)
	[Documentation]	This is the endpoint for user receive email with link for reset password. Status code 200 is expected. 
	...	Endpoint: /user/forgot-password
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user/forgot-password
#	Should Be Equal   ${response.status_code}   ${200}

user_forgot-password_create (status_code = 400)
#	[Documentation]	This is the endpoint for user receive email with link for reset password. Status code 400 is expected. 
#	...	Endpoint: /user/forgot-password
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user/forgot-password
#	Should Be Equal   ${response.status_code}   ${400}

user_forgot-password_create (status_code = 404)
#	[Documentation]	This is the endpoint for user receive email with link for reset password. Status code 404 is expected. 
#	...	Endpoint: /user/forgot-password
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user/forgot-password
#	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session