*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At
Resource    ../../../../init.robot
Force Tags    API
Default Tags

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
${USER_EMAIL}       test_api@gmail.com
${USER_PASS}       Test@12345

*** Test Cases ***
user_auth_create (status_code = 200)
	[Documentation]	>-. Status code 200 is expected.
	...	Endpoint: /user/auth

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	email=${USER_EMAIL}
	Set To Dictionary	${payload}	password=${USER_PASS}

	${response}=    CLOUD::API::Send Post Request Error 400    PAYLOAD=${PAYLOAD}    PATH=/user
	Should Be Equal   ${response.status_code}   ${400}

user_auth_create (status_code = 400)
	[Documentation]	>-. Status code 400 is expected.
	...	Endpoint: /user/auth

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	emails=${USER_EMAIL}
	Set To Dictionary	${payload}	passwords=${USER_PASS}

	${response}=    CLOUD::API::Send Post Request Error 400    PAYLOAD=${PAYLOAD}    PATH=/user
	Should Be Equal   ${response.status_code}   ${400}

user_auth_create (status_code = 403)
	[Documentation]	>-. Status code 403 is expected.
	...	Endpoint: /user/auth

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	email=${USER_EMAIL}
	Set To Dictionary	${payload}	password=${USER_PASS}

	${response}=    CLOUD::API::Send Post Request Error 403    PAYLOAD=${PAYLOAD}    PATH=/user/auth
	Should Be Equal   ${response.status_code}   ${403}

user_auth_create (status_code = 404)
	[Documentation]	>-. Status code 404 is expected.
	...	Endpoint: /user/auths

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	email=${USER_EMAIL}
	Set To Dictionary	${payload}	password=${USER_PASS}

	${response}=    CLOUD::API::Send Post Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/user/auths
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session