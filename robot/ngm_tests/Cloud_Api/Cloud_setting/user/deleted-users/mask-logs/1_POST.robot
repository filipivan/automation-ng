*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***

user_deleted-users_mask-logs_create (status_code = 200)
	[Documentation]	This is the endpoint which delete or mask logs from user(s) deleted. Status code 200 is expected. 
	...	Endpoint: /user/deleted-users/mask-logs
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user/deleted-users/mask-logs
#	Should Be Equal   ${response.status_code}   ${200}

user_deleted-users_mask-logs_create (status_code = 400)
	[Documentation]	This is the endpoint which delete or mask logs from user(s) deleted. Status code 400 is expected.
	...	Endpoint: /user/deleted-users/mask-logs
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user/deleted-users/mask-logs
#	Should Be Equal   ${response.status_code}   ${400}

user_deleted-users_mask-logs_create (status_code = 403)
#	[Documentation]	This is the endpoint which delete or mask logs from user(s) deleted. Status code 403 is expected. 
#	...	Endpoint: /user/deleted-users/mask-logs
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user/deleted-users/mask-logs
#	Should Be Equal   ${response.status_code}   ${403}

user_deleted-users_mask-logs_create (status_code = 500)
#	[Documentation]	This is the endpoint which delete or mask logs from user(s) deleted. Status code 500 is expected. 
#	...	Endpoint: /user/deleted-users/mask-logs
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user/deleted-users/mask-logs
#	Should Be Equal   ${response.status_code}   ${500}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session