*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***

user_logout_create (status_code = 200)
	[Documentation]	This is the endpoint for user logout from the application. Status code 200 is expected. 
	...	Endpoint: /user/logout

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user/logout
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session