*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***

user_generate-recovery-key_create (status_code = 200)
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /user/generate-recovery-key

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user/generate-recovery-key
	Should Be Equal   ${response.status_code}   ${200}

#user_generate-recovery-key_create (status_code = 400)
#	[Documentation]	>-. Status code 400 is expected. 
#	...	Endpoint: /user/generate-recovery-key

#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user/generate-recovery-key
#	Should Be Equal   ${response.status_code}   ${400}

#user_generate-recovery-key_create (status_code = 403)
#	[Documentation]	>-. Status code 403 is expected. 
#	...	Endpoint: /user/generate-recovery-key

#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user/generate-recovery-key
#	Should Be Equal   ${response.status_code}   ${403}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session