*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***

user_update-temprory-password_create (status_code = 200)
	[Documentation]	This is the endpoint for user reset the temporary password. Status code 200 is expected. 
	...	Endpoint: /user/update-temprory-password
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user/update-temprory-password
#	Should Be Equal   ${response.status_code}   ${200}

#user_update-temprory-password_create (status_code = 400)
#	[Documentation]	This is the endpoint for user reset the temporary password. Status code 400 is expected. 
#	...	Endpoint: /user/update-temprory-password

#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user/update-temprory-password
#	Should Be Equal   ${response.status_code}   ${400}

#user_update-temprory-password_create (status_code = 401)
#	[Documentation]	This is the endpoint for user reset the temporary password. Status code 401 is expected. 
#	...	Endpoint: /user/update-temprory-password

#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user/update-temprory-password
#	Should Be Equal   ${response.status_code}   ${401}

#user_update-temprory-password_create (status_code = 404)
#	[Documentation]	This is the endpoint for user reset the temporary password. Status code 404 is expected. 
#	...	Endpoint: /user/update-temprory-password

#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user/update-temprory-password
#	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session