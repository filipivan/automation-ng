*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../init.robot
Force Tags    API	    NON-CRITICAL
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
${USER_EMAIL}       test_api@gmail.com
${USER_FIRST_NAME}       test_api
${USER_LAST_NAME}       api
${USER_PH_NO}       14454354354
@{USER_GROUP}       382

*** Test Cases ***

user_create (status_code = 200)
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /user

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	email=${USER_EMAIL}
	Set To Dictionary	${payload}	first_name=${USER_FIRST_NAME}
	Set To Dictionary	${payload}	last_name=${USER_LAST_NAME}
	Set To Dictionary	${payload}	phone_number=${USER_PH_NO}
	Set To Dictionary	${payload}	groups=${USER_GROUP}

	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user
	Should Be Equal   ${response.status_code}   ${200}

user_create (status_code = 400)
    [Tags]      BUG_CLOUD-9824
	[Documentation]	>-. Status code 400 is expected.
	...	Endpoint: /user

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	emails=${USER_EMAIL}
	Set To Dictionary	${payload}	first_name=${USER_FIRST_NAME}
	Set To Dictionary	${payload}	lastname=${USER_LAST_NAME}
	Set To Dictionary	${payload}	phonenumber=${USER_PH_NO}
	Set To Dictionary	${payload}	groups=${USER_GROUP}

	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/user
	Should Be Equal   ${response.status_code}   ${400}

user_create (status_code = 403)
	[Documentation]	>-. Status code 403 is expected.
	...	Endpoint: /user

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	email=${USER_EMAIL}
	Set To Dictionary	${payload}	first_name=${USER_FIRST_NAME}
	Set To Dictionary	${payload}	last_name=${USER_LAST_NAME}
	Set To Dictionary	${payload}	phone_number=${USER_PH_NO}
	Set To Dictionary	${payload}	groups=${USER_GROUP}

	${response}=    CLOUD::API::Send Post Request Error 403    PAYLOAD=${PAYLOAD}    PATH=/user
	Should Be Equal   ${response.status_code}   ${403}

user_create (status_code = 404)
	[Documentation]	>-. Status code 404 is expected.
	...	Endpoint: /user

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	email=${USER_EMAIL}
	Set To Dictionary	${payload}	first_name=${USER_FIRST_NAME}
	Set To Dictionary	${payload}	last_name=${USER_LAST_NAME}
	Set To Dictionary	${payload}	phone_number=${USER_PH_NO}
	Set To Dictionary	${payload}	groups=${USER_GROUP}

	${response}=    CLOUD::API::Send Post Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/users
	Should Be Equal   ${response.status_code}   ${404}



*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session