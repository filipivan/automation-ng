*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

sso_list (status_code = 200)
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /sso

	${response}=    CLOUD::API::Send Get Request    /sso
	Should Be Equal   ${response.status_code}   ${200}

sso_list (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which get the company details. Status code 403 is expected.
	...	Endpoint: /sso

	${response}=    CLOUD::API::Send Get Request Error 403    /sso
	Should Be Equal   ${response.status_code}   ${403}

sso_list (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which get the company details. Status code 404 is expected.
	...	Endpoint: /ssos

	${response}=    CLOUD::API::Send Get Request Error 404    /ssos
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session