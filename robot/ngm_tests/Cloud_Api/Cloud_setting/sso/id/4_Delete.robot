*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***

sso_delete (status_code = 200)
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /sso/{id}
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Delete Request    PAYLOAD=${PAYLOAD}    PATH=/sso/{id}
#	Should Be Equal   ${response.status_code}   ${200}

sso_delete (status_code = 403)
#	[Documentation]	>-. Status code 403 is expected.
#	...	Endpoint: /sso/{id}
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Delete Request    PAYLOAD=${PAYLOAD}    PATH=/sso/{id}
#	Should Be Equal   ${response.status_code}   ${403}

sso_delete (status_code = 404)
#	[Documentation]	>-. Status code 404 is expected.
#	...	Endpoint: /sso/{id}
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Delete Request    PAYLOAD=${PAYLOAD}    PATH=/sso/{id}
#	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session