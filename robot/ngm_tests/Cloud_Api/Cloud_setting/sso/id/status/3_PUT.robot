*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***

sso_status_update (status_code = 200)
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /sso/{id}/status
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Put Request    PAYLOAD=${PAYLOAD}    PATH=/sso/{id}/status
#	Should Be Equal   ${response.status_code}   ${200}

sso_status_update (status_code = 400)
#	[Documentation]	>-. Status code 400 is expected.
#	...	Endpoint: /sso/{id}/status
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Put Request    PAYLOAD=${PAYLOAD}    PATH=/sso/{id}/status
#	Should Be Equal   ${response.status_code}   ${400}

sso_status_update (status_code = 403)
#	[Documentation]	>-. Status code 403 is expected.
#	...	Endpoint: /sso/{id}/status
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Put Request    PAYLOAD=${PAYLOAD}    PATH=/sso/{id}/status
#	Should Be Equal   ${response.status_code}   ${403}

sso_status_update (status_code = 404)
#	[Documentation]	>-. Status code 404 is expected.
#	...	Endpoint: /sso/{id}/status
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Put Request    PAYLOAD=${PAYLOAD}    PATH=/sso/{id}/status
#	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session