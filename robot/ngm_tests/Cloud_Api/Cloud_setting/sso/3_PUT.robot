*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***

sso_update (status_code = 200)
	[Documentation]	This is the endpoint which update a SSO for user authenticated. Status code 200 is expected. 
	...	Endpoint: /sso
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Put Request    PAYLOAD=${PAYLOAD}    PATH=/sso
#	Should Be Equal   ${response.status_code}   ${200}

sso_update (status_code = 400)
#	[Documentation]	This is the endpoint which update a SSO for user authenticated. Status code 400 is expected.
#	...	Endpoint: /sso
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Put Request    PAYLOAD=${PAYLOAD}    PATH=/sso
#	Should Be Equal   ${response.status_code}   ${400}

sso_update (status_code = 404)
#	[Documentation]	This is the endpoint which update a SSO for user authenticated. Status code 404 is expected.
#	...	Endpoint: /sso
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Put Request    PAYLOAD=${PAYLOAD}    PATH=/sso
#	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session