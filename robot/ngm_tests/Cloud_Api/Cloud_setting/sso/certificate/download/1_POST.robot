*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***

sso_certificate_download_create (status_code = 200)
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /sso/certificate/download
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/sso/certificate/download
#	Should Be Equal   ${response.status_code}   ${200}

sso_certificate_download_create (status_code = 400) with Invalid body
	[Documentation]	>-. Status code 400 is expected.
	...	Endpoint: /sso/certificate/download
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	key_enabled=${enable}
#
#	${response}=    CLOUD::API::Send Post Request Error 400    PAYLOAD=${PAYLOAD}    PATH=/sso/certificate/download
#	Should Be Equal   ${response.status_code}   ${400}

sso_certificate_download_create (status_code = 403) with wrong credntials
	[Documentation]	>-. Status code 403 is expected.
	...	Endpoint: /sso/certificate/download
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	company_key=${company_key}
#	Set To Dictionary	${payload}	key_enabled=${enable}
#
#	${response}=    CLOUD::API::Send Post Request Error 403    PAYLOAD=${PAYLOAD}    PATH=/sso/certificate/download
#	Should Be Equal   ${response.status_code}   ${403}

sso_certificate_download_create (status_code = 404) with wrong endpoint
	[Documentation]	>-. Status code 404 is expected.
	...	Endpoint: /sso/certificates/downloads
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	company_key=${company_key}
#	Set To Dictionary	${payload}	key_enabled=${enable}
#
#	${response}=    CLOUD::API::Send Post Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/sso/certificates/downloads
#	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session