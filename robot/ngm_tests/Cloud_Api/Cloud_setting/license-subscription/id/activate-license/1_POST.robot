*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
${operation}        now
${schedule_date}        2023-02-04T06:27:47Z
${id}       1

*** Test Cases ***

license-subscription_activate-license_create (status_code = 200)
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /license-subscription/${id}/activate-license

    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	operation=${operation}
#	Set To Dictionary	${payload}	schedule_date=${schedule_date}
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/license-subscription/${id}/activate-license
#	Should Be Equal   ${response.status_code}   ${200}

license-subscription_activate-license_create (status_code = 400) with Invalid body
	[Documentation]	>-. Status code 400 is expected.
	...	Endpoint: /license-subscription/${id}/activate-license

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	schedule_date=${schedule_date}

	${response}=    CLOUD::API::Send Post Request Error 400    PAYLOAD=${PAYLOAD}    PATH=/license-subscription/${id}/activate-license
	Should Be Equal   ${response.status_code}   ${400}

license-subscription_activate-license_create (status_code = 403) with wrong credntials
	[Documentation]	>-. Status code 403 is expected.
	...	Endpoint: /license-subscription/${id}/activate-license

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	operation=${operation}
	Set To Dictionary	${payload}	schedule_date=${schedule_date}

	${response}=    CLOUD::API::Send Post Request Error 403    PAYLOAD=${PAYLOAD}    PATH=/license-subscription/${id}/activate-license
	Should Be Equal   ${response.status_code}   ${403}

license-subscription_activate-license_create (status_code = 404) with wrong endpoint
	[Documentation]	>-. Status code 404 is expected.
	...	Endpoint: /license-subscription/${id}/activate-license

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	operation=${operation}
	Set To Dictionary	${payload}	schedule_date=${schedule_date}

	${response}=    CLOUD::API::Send Post Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/licensesubscription/${id}/activatelicense
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session