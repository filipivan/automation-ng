*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***

license-subscription_delete (status_code = 200)
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /license-subscription/{id}

#	${PAYLOAD}=	Create Dictionary
    GUI::Basic::Spinner Should Be Invisible
#	${response}=    CLOUD::API::Send Delete Request    PAYLOAD=${PAYLOAD}    PATH=/license-subscription/{id}
#	Should Be Equal   ${response.status_code}   ${200}

license-subscription_delete (status_code = 400)
	[Documentation]	>-. Status code 400 is expected.
	...	Endpoint: /license-subscription/{id}

#	${PAYLOAD}=	Create Dictionary
    GUI::Basic::Spinner Should Be Invisible
#	${response}=    CLOUD::API::Send Delete Request    PAYLOAD=${PAYLOAD}    PATH=/license-subscription/{id}
#	Should Be Equal   ${response.status_code}   ${400}

license-subscription_delete (status_code = 403)
	[Documentation]	>-. Status code 403 is expected.
	...	Endpoint: /license-subscription/{id}
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Delete Request    PAYLOAD=${PAYLOAD}    PATH=/license-subscription/{id}
#	Should Be Equal   ${response.status_code}   ${403}

license-subscription_delete (status_code = 404)
	[Documentation]	>-. Status code 404 is expected.
	...	Endpoint: /license-subscription/{id}
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Delete Request    PAYLOAD=${PAYLOAD}    PATH=/license-subscription/{id}
#	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session