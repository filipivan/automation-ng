*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
${id}
${subscription}     nodegrid
${comment}          -

*** Test Cases ***

request-license-renew_create (status_code = 200)
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /request-license-renew/${company_id}
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	subscription=${subscription}
#	Set To Dictionary	${payload}	comment=${comment}
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/request-license-renew/${company_id}
#	Should Be Equal   ${response.status_code}   ${200}

request-license-renew_create (status_code = 400) with Invalid body
	[Documentation]	>-. Status code 400 is expected.
	...	Endpoint: /request-license-renew/${company_id}

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	comment=${comment}

	${response}=    CLOUD::API::Send Post Request Error 400    PAYLOAD=${PAYLOAD}    PATH=/request-license-renew/${company_id}
	Should Be Equal   ${response.status_code}   ${400}

request-license-renew_create (status_code = 403) with wrong credntials
	[Documentation]	>-. Status code 403 is expected.
	...	Endpoint: /request-license-renew/${company_id}

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	subscription=${subscription}
	Set To Dictionary	${payload}	comment=${comment}

	${response}=    CLOUD::API::Send Post Request Error 403    PAYLOAD=${PAYLOAD}    PATH=/request-license-renew/${company_id}
	Should Be Equal   ${response.status_code}   ${403}

request-license-renew_create (status_code = 404) with wrong endpoint
	[Documentation]	>-. Status code 404 is expected.
	...	Endpoint: /requestlicenserenew/${company_id}

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	subscription=${subscription}
	Set To Dictionary	${payload}	comment=${comment}

	${response}=    CLOUD::API::Send Post Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/requestlicenserenew/${company_id}
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session