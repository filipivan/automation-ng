*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***

email-server-configuration_create (status_code = 200)
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /email-server-configuration/
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	configuration=${enable}
#	Set To Dictionary	${payload}	host=${company_key}
#	Set To Dictionary	${payload}	port=${enable}
#	Set To Dictionary	${payload}	username=${company_key}
#	Set To Dictionary	${payload}	password=${enable}
#	Set To Dictionary	${payload}	sender=${company_key}
#	Set To Dictionary	${payload}	timeout=${company_key}
#	Set To Dictionary	${payload}	use_tls=${enable}
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/email-server-configuration/
#	Should Be Equal   ${response.status_code}   ${200}

email-server-configuration_create (status_code = 400)
	[Documentation]	>-. Status code 400 is expected.
	...	Endpoint: /email-server-configuration/
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	configuration=${enable}
#	Set To Dictionary	${payload}	host=${company_key}
#	Set To Dictionary	${payload}	port=${enable}
#	Set To Dictionary	${payload}	username=${company_key}
#	Set To Dictionary	${payload}	password=${enable}
#	Set To Dictionary	${payload}	sender=${company_key}
#	Set To Dictionary	${payload}	timeout=${company_key}
#	Set To Dictionary	${payload}	use_tls=${enable}
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/email-server-configuration/
#	Should Be Equal   ${response.status_code}   ${400}

email-server-configuration_create (status_code = 403)
	[Documentation]	>-. Status code 403 is expected.
	...	Endpoint: /email-server-configuration/
    GUI::Basic::Spinner Should Be Invisible
#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	configuration=${enable}
#	Set To Dictionary	${payload}	host=${company_key}
#	Set To Dictionary	${payload}	port=${enable}
#	Set To Dictionary	${payload}	username=${company_key}
#	Set To Dictionary	${payload}	password=${enable}
#	Set To Dictionary	${payload}	sender=${company_key}
#	Set To Dictionary	${payload}	timeout=${company_key}
#	Set To Dictionary	${payload}	use_tls=${enable}
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/email-server-configuration/
#	Should Be Equal   ${response.status_code}   ${403}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session