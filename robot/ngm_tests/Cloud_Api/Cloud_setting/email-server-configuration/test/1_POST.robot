*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***

email-server-configuration_test_create (status_code = 200)
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /email-server-configuration/test

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/email-server-configuration/test
	Should Be Equal   ${response.status_code}   ${200}

#email-server-configuration_test_create (status_code = 400)
#	[Documentation]	>-. Status code 400 is expected. 
#	...	Endpoint: /email-server-configuration/test

#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/email-server-configuration/test
#	Should Be Equal   ${response.status_code}   ${400}

#email-server-configuration_test_create (status_code = 403)
#	[Documentation]	>-. Status code 403 is expected. 
#	...	Endpoint: /email-server-configuration/test

#	${PAYLOAD}=	Create Dictionary
#
#	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/email-server-configuration/test
#	Should Be Equal   ${response.status_code}   ${403}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session