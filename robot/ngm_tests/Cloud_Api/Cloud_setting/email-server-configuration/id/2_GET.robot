*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***

*** Test Cases ***

email-server-configuration_read (status_code = 200)
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /email-server-configuration/${company_id}/
    GUI::Basic::Spinner Should Be Invisible
#	${response}=    CLOUD::API::Send Get Request    /email-server-configuration/${company_id}/
#	Should Be Equal   ${response.status_code}   ${200}

email-server-configuration_read (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which get the company details. Status code 403 is expected.
	...	Endpoint: //email-server-configuration/${company_id}/

	${response}=    CLOUD::API::Send Get Request Error 403    /email-server-configuration/${company_id}/
	Should Be Equal   ${response.status_code}   ${403}

email-server-configuration_read (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which get the company details. Status code 404 is expected.
	...	Endpoint: /email-server-configuration/${company_id}/

	${response}=    CLOUD::API::Send Get Request Error 404    /emailserverconfiguration/${company_id}/
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session