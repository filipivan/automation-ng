*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

users_download_list (status_code = 200)
	[Documentation]	>-. Status code 200 is expected. 
	...	Endpoint: /users/download
    GUI::Basic::Spinner Should Be Invisible
#	${response}=    CLOUD::API::Send Get Request    /users/download
#	Should Be Equal   ${response.status_code}   ${200}

users_download_list (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which get the company details. Status code 403 is expected.
	...	Endpoint: /users/download

	${response}=    CLOUD::API::Send Get Request Error 403    /users/download
	Should Be Equal   ${response.status_code}   ${403}

users_download_list (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which get the company details. Status code 404 is expected.
	...	Endpoint: /users/downloadss

	${response}=    CLOUD::API::Send Get Request Error 404    /users/downloadss
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session