*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	
Default Tags    

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
${device_list}      true
${job_list}     false

*** Test Cases ***

diagnostic-logs_generate_create (status_code = 200)
	[Documentation]	''. Status code 200 is expected.
	...	Endpoint: /diagnostic-logs/generate

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	device_list=${device_list}
	Set To Dictionary	${payload}	job_list=${job_list}

	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/diagnostic-logs/generate
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
    CLOUD::API::Post::Session

SUITE:Teardown
    CLOUD::API::Delete::Session