*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	NON-CRITICAL    BUG_CLOUD-9767
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***

jobs_cancel_create (status_code = 200)
	[Documentation]	>-. Status code 200 is expected.
	...	Endpoint: /jobs/cancel/

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/jobs/cancel/
	Should Be Equal   ${response.status_code}   ${200}

jobs_cancel_create (status_code = 400) with Invalid body
	[Documentation]	>-. Status code 400 is expected.
	...	Endpoint: /jobs/cancel/

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request Error 400    PAYLOAD=${PAYLOAD}    PATH=/jobs/cancel/
	Should Be Equal   ${response.status_code}   ${400}

jobs_cancel_create (status_code = 403) with wrong credntials
	[Documentation]	>-. Status code 403 is expected.
	...	Endpoint: /jobs/cancel/

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request Error 403    PAYLOAD=${PAYLOAD}    PATH=/jobs/cancel/
	Should Be Equal   ${response.status_code}   ${403}

jobs_cancel_create (status_code = 404) with wrong endpoint
	[Documentation]	>-. Status code 404 is expected.
	...	Endpoint: /jobs/cancel/s

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/jobs/cancel/s
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session