*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	NEED-CREATION
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

profiles_ike_list (status_code = 200)
	[Documentation]	GET request for the IKE profile.. Status code 200 is expected. 
	...	Endpoint: /sd-wan/profiles/ike/

	${response}=   CLOUD::API::Send Get Request    /sd-wan/profiles/ike/
	Should Be Equal   ${response.status_code}   ${200}

profiles_ike_list (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which get the company details. Status code 403 is expected.
	...	Endpoint: /sd-wan/profiles/ike/

	${response}=    CLOUD::API::Send Get Request Error 403    /sd-wan/profiles/ike/
	Should Be Equal   ${response.status_code}   ${403}

profiles_ike_list (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which get the company details. Status code 404 is expected.
	...	Endpoint: /sd-wan/profiles/ikes/

	${response}=    CLOUD::API::Send Get Request Error 404    /sd-wan/profiles/ikes/
	Should Be Equal   ${response.status_code}   ${404}
*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session