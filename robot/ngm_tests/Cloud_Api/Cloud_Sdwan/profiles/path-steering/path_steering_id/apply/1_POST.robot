*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../../../init.robot
Force Tags    API	NON-CRITICAL    BUG_CLOUD-9767
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***

profiles_path-steering_apply_create (status_code = 200)
	[Documentation]	>-. Status code 200 is expected.
	...	Endpoint: /profiles/path-steering/{path_steering_id}/apply/

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/profiles/path-steering/{path_steering_id}/apply/
	Should Be Equal   ${response.status_code}   ${200}

profiles_path-steering_apply_create (status_code = 400) with Invalid body
	[Documentation]	>-. Status code 400 is expected.
	...	Endpoint: /profiles/path-steering/{path_steering_id}/apply/

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request Error 400    PAYLOAD=${PAYLOAD}    PATH=/profiles/path-steering/{path_steering_id}/apply/
	Should Be Equal   ${response.status_code}   ${400}

profiles_path-steering_apply_create (status_code = 403) with wrong credntials
	[Documentation]	>-. Status code 403 is expected.
	...	Endpoint: /profiles/path-steering/{path_steering_id}/apply/

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request Error 403    PAYLOAD=${PAYLOAD}    PATH=/profiles/path-steering/{path_steering_id}/apply/
	Should Be Equal   ${response.status_code}   ${403}

profiles_path-steering_apply_create (status_code = 404) with wrong endpoint
	[Documentation]	>-. Status code 404 is expected.
	...	Endpoint: /profiles/path-steering/{path_steering_id}/apply/s

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/profiles/path-steering/{path_steering_id}/apply/s
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session