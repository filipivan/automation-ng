*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	NON-CRITICAL    BUG_CLOUD-9767
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***

profiles_link_delete (status_code = 200)
	[Documentation]	DELETE request for the link profile endpoint.. Status code 200 is expected. 
	...	Endpoint: /profiles/link/

	${PAYLOAD}=	Create Dictionary

	${response}=   CLOUD::API::Send Delete Request    PAYLOAD=${PAYLOAD}    PATH=/profiles/link/
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session