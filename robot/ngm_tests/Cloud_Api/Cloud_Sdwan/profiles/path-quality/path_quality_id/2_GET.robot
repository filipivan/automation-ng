*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../../init.robot
Force Tags    API	NON-CRITICAL    BUG_CLOUD-9767
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

profiles_path-quality_read (status_code = 200)
	[Documentation]	GET request for the Profile Path Quality endpoint.. Status code 200 is expected. 
	...	Endpoint: /profiles/path-quality/${path_quality_id}/

	${response}=   CLOUD::API::Send Get Request    /profiles/path-quality/${path_quality_id}/
	Should Be Equal   ${response.status_code}   ${200}

profiles_path-quality_read (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which get the company details. Status code 403 is expected.
	...	Endpoint: /profiles/path-quality/${path_quality_id}/

	${response}=    CLOUD::API::Send Get Request Error 403    /profiles/path-quality/${path_quality_id}/
	Should Be Equal   ${response.status_code}   ${403}

profiles_path-quality_read (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which get the company details. Status code 404 is expected.
	...	Endpoint: /profiles/path-quality/${path_quality_id}/

	${response}=    CLOUD::API::Send Get Request Error 404    /profiles/path-quality/{path_quality_id}/
	Should Be Equal   ${response.status_code}   ${404}

*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session