*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	NON-CRITICAL    BUG_CLOUD-9767
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***

Put profiles_path-quality_update
	[Documentation]	This is the endpoint which change the company details. Status code 200 is expected.
	...	Endpoint: /profiles/path-quality/

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Put Request    PAYLOAD=${PAYLOAD}    PATH=/profiles/path-quality/
	Should Be Equal   ${response.status_code}   ${200}

profiles_path-quality_update (status_code = 400) with invalid body
	[Documentation]	This is the endpoint which change the company details. Status code 400 is expected.
	...	Endpoint: /profiles/path-quality/

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Put Request Error 400   PAYLOAD=${PAYLOAD}    PATH=/profiles/path-quality/
	Should Be Equal   ${response.status_code}   ${400}

profiles_path-quality_update (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which change the company details. Status code 403 is expected.
	...	Endpoint: /company/{company_id}

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Put Request Error 403   PAYLOAD=${PAYLOAD}    PATH=/profiles/path-quality/
	Should Be Equal   ${response.status_code}   ${403}

profiles_path-quality_update (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which change the company details. Status code 404 is expected.
	...	Endpoint: /profiles/path-quality/s

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Put Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/profiles/path-quality/s
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session