*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	NON-CRITICAL    BUG_CLOUD-9767
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

topologies_read (status_code = 200)
	[Documentation]	''. Status code 200 is expected. 
	...	Endpoint: /topologies/${topology_id}/

	${response}=   CLOUD::API::Send Get Request    /topologies/${topology_id}/
	Should Be Equal   ${response.status_code}   ${200}

topologies_read (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which get the company details. Status code 403 is expected.
	...	Endpoint: /topologies/${topology_id}/

	${response}=    CLOUD::API::Send Get Request Error 403    /topologies/${topology_id}/
	Should Be Equal   ${response.status_code}   ${403}

topologies_read (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which get the company details. Status code 404 is expected.
	...	Endpoint: /topologie/${topology_id}/

	${response}=    CLOUD::API::Send Get Request Error 404    /topologie/${topology_id}/
	Should Be Equal   ${response.status_code}   ${404}
*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session