*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	NEED-CREATION
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
${device_id}        ${device_ids}

*** Test Cases ***

devices_read (status_code = 200)
	[Documentation]	Get complete data from specified ZPE Device regarding SD-WAN App. Status code 200 is expected. 
	...	Endpoint: /sd-wan/devices/${device_id}/

	${response}=   CLOUD::API::Send Get Request    /sd-wan/devices/${device_id}/
	Should Be Equal   ${response.status_code}   ${200}

devices_read (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which get the company details. Status code 403 is expected.
	...	Endpoint: /sd-wan/devices/${device_id}/

	${response}=    CLOUD::API::Send Get Request Error 403    /sd-wan/devices/${device_id}/
	Should Be Equal   ${response.status_code}   ${403}

devices_read (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which get the company details. Status code 404 is expected.
	...	Endpoint: /sd-wan/devices/${device_id}/

	${response}=    CLOUD::API::Send Get Request Error 404    /sd-wan/devices/${device_id}s/
	Should Be Equal   ${response.status_code}   ${404}

*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session