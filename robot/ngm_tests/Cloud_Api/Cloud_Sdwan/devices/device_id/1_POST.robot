*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	NON-CRITICAL    BUG_CLOUD-9767
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
${device_id}        ${device_ids}

*** Test Cases ***

devices_create (status_code = 200)
	[Documentation]	>-. Status code 200 is expected.
	...	Endpoint: /sd-wan/devices/${device_id}/

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request    PAYLOAD=${PAYLOAD}    PATH=/sd-wan/devices/${device_id}/
	Should Be Equal   ${response.status_code}   ${200}

devices_create (status_code = 400) with Invalid body
	[Documentation]	>-. Status code 400 is expected.
	...	Endpoint: /sd-wan/devices/${device_id}/

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request Error 400    PAYLOAD=${PAYLOAD}    PATH=/sd-wan/devices/${device_id}/
	Should Be Equal   ${response.status_code}   ${400}

devices_create (status_code = 403) with wrong credntials
	[Documentation]	>-. Status code 403 is expected.
	...	Endpoint: /sd-wan/devices/${device_id}/

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request Error 403    PAYLOAD=${PAYLOAD}    PATH=/devices/${device_id}/
	Should Be Equal   ${response.status_code}   ${403}

devices_create (status_code = 404) with wrong endpoint
	[Documentation]	>-. Status code 404 is expected.
	...	Endpoint: /sd-wan/devices/${device_id}/s

	${PAYLOAD}=	Create Dictionary

	${response}=    CLOUD::API::Send Post Request Error 404    PAYLOAD=${PAYLOAD}    PATH=/sd-wan/devices/${device_id}/s
	Should Be Equal   ${response.status_code}   ${404}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session