*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	NON-CRITICAL    BUG_CLOUD-9767
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

monitoring_device_list (status_code = 200)
	[Documentation]	Return a list of devices and tunnels. Status code 200 is expected. 
	...	Endpoint: /sd-wan/monitoring/device/

	${response}=   CLOUD::API::Send Get Request    /sd-wan/monitoring/device/
	Should Be Equal   ${response.status_code}   ${200}

company_read (status_code = 403) wrong credentials
	[Documentation]	This is the endpoint which get the company details. Status code 403 is expected.
	...	Endpoint: /sd-wan/monitoring/device/

	${response}=    CLOUD::API::Send Get Request Error 403    /sd-wan/monitoring/device/
	Should Be Equal   ${response.status_code}   ${403}

company_read (status_code = 404) wrong endpoint
	[Documentation]	This is the endpoint which get the company details. Status code 404 is expected.
	...	Endpoint: /sd-wan/monitoring/devices/

	${response}=    CLOUD::API::Send Get Request Error 404    /sd-wan/monitoring/devices/
	Should Be Equal   ${response.status_code}   ${404}

*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session