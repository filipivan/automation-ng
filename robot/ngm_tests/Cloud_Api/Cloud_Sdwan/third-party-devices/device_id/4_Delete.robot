*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	NON-CRITICAL    BUG_CLOUD-9767
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***


*** Test Cases ***

#third-party-devices_delete (status_code = 204)
#	[Documentation]	''. Status code 204 is expected. 
#	...	Endpoint: /third-party-devices/{device_id}/

#	${PAYLOAD}=	Create Dictionary
#
#	${response}=   CLOUD::API::Send Delete Request    PAYLOAD=${PAYLOAD}    PATH=/third-party-devices/{device_id}/
#	Should Be Equal   ${response.status_code}   ${204}


*** Keywords ***
SUITE:Setup
   CLOUD::API::Post::Session

SUITE:Teardown
   CLOUD::API::Delete::Session