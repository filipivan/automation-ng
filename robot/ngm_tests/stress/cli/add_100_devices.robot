*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Devices... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	SSH

Suite Setup	Setup
Suite Teardown	Teardown

*** Variables ***
${FIVE_THOUSAND_DEVICES_ACCESS_LICENSE}	${FIVE_THOUSAND_DEVICES_ACCESS_LICENSE}

*** Test Cases ***
Test add 100 devices
	:FOR	${n}	IN RANGE	5000
	\	Add And Remove 100 Devices

*** Keyword ***
Add And Remove 100 Devices
	CLI:Enter Path	/settings/devices
	${OUTPUT}=	CLI:Test Show Command
	:FOR	${n}	IN RANGE	100
	\	CLI:Add Device	${n}_${DUMMY_DEVICE_CONSOLE_NAME}	ilo
	\	${OUTPUT}=	CLI:Test Show Command
	\	Should Contain	${OUTPUT}	${DUMMY_DEVICE_CONSOLE_NAME}
	Delete All Devices

Setup
	CLI:Open
	Delete All Devices
	Add License	${FIVE_THOUSAND_DEVICES_ACCESS_LICENSE}

Teardown
	Delete All Devices
	Delete License	${FIVE_THOUSAND_DEVICES_ACCESS_LICENSE}
	CLI:Close Connection

Get Ls Values
	${output}=	CLI:Ls
	${lines}=	Split To Lines	${OUTPUT}	0	-1
	Remove Values From List	${lines}	${EMPTY}
	${filter_lines} =	Create List
	:FOR	${line}	IN	@{lines}
	\	${value} =	Get Substring	${line}	0	-1
	\	${value} =	Strip String	${value}
	\	${ret} =	Run Keyword And Return Status	Should Start With	${value}	tty
	\	Continue For Loop If	${ret}
	\	${ret} =	Run Keyword And Return Status	Should Start With	${value}	usb
	\	Continue For Loop If	${ret}
	\	Append To List	${filter_lines}	${value}
	Log Variables	DEBUG
	[Return]	${filter_lines}

Delete All Devices
	CLI:Enter Path	/settings/devices
	${devices}=	Get Ls Values
	CLI:Delete Devices	@{devices}

Add License
	[Arguments]	${LICENSE}
	Write	cd /settings/license
	${OUTPUT}=	Read Until Prompt
	Write	add
	${OUTPUT}=	Read Until	{license}]#
	Write	set license_key=${LICENSE}
	${OUTPUT}=	Read Until	{license}]#
	Write	show
	${OUTPUT}=	Read Until	{license}]#
	Should contain	${OUTPUT}	license_key = ${LICENSE}
	Write	commit
	${OUTPUT}=	Read Until Prompt
	Write	show
	${OUTPUT}=	Read Until Prompt

Delete License
	[Arguments]	${LICENSE}
	${LICENSE}=	Escape License	${LICENSE}
	Write	cd /settings/license
	${OUTPUT}=	Read Until Prompt
	Write	delete ${LICENSE}
	${OUTPUT}=	Read Until Prompt
