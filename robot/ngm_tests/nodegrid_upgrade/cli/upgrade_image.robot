*** Settings ***
Resource    ../init.robot
Documentation	Downgrade the system to latest released image, then upgrade the nodegrid image with the latest available image from ftp server
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Force Tags         CLI
Default Tags        SSH

Suite Setup     CLI:Connect As Root
Suite Teardown  Close Connection

*** Keywords ***
SUITE:Try Yesterday's Image
	Set Suite Variable    ${NAME}       (\\S+\\s+){8}nodegrid-genericx86-64-${DATE_YESTERDAY}\\d+_Branch-NG_4.0_\\d+_R\\d+-r\\d+.iso$
	Run Keyword If  '${NGVERSION}' > '4.0'  Set Suite Variable    ${NAME}     (\\S+\\s+){8}nodegrid-genericx86-64-${DATE4.1_YESTERDAY}\\d+_Branch-NG_4.1_\\d+_R\\d+-r\\d+.iso$
    Write	ls
	${OUTPUT}=	Read Until	ftp>
	${LINES}=	Split To Lines	${OUTPUT}
	${FOUND_IMAGE}=	Set Variable	False
	${IMAGE_LINE}=	Set Variable	${EMPTY}
	${LOOK_FOR}=	Set Variable If	'${NGVERSION}'>='4.0'   ${NAME}
	:FOR	${LINE}	IN	@{LINES}
	\	${FOUND_IMAGE}=	    Run Keyword And Return Status	Should Match Regexp	    ${LINE} 	${NAME}
	\	${IMAGE_LINE}=	Set Variable	${LINE}
	\	Run Keyword If	${FOUND_IMAGE}	Exit For Loop
	Run Keyword Unless	${FOUND_IMAGE}	Fail	The ${LOOK_FOR} file was not found and the latest image could not be downloaded, exiting!
	[Return]    ${IMAGE_LINE}

*** Test Cases ***
Find The Latest Release Image
    #Pass Execution If   '${NGVERSION}' > '4.0'  No release image for 4.1
    CLI:Write   cd /var/sw
    Write	ftp 192.168.2.201 21
	Read Until	Name (192.168.2.201:root):
	Write	anonymous
	Read Until	Password:
	Write	anonymous
	Read Until	ftp>
	Write	cd releases/2019
	Read Until	ftp>
	Write   ls

	${OUTPUT}=	Read Until	ftp>
	${LINES}=	Split To Lines	${OUTPUT}
	${FOUND_IMAGE}=	Set Variable	False
	${4.0_IMAGE}=  Set Variable	False
	${REAL_RELEASE_FILE}=	Set Variable	${EMPTY}
	${IMAGE_LINE}=	Set Variable	${EMPTY}
	${LOOK_FOR}=	Set Variable If	'${NGVERSION}'>='4.0'   virtualization_package_Branch-NG
	${LINE_NUM}=    Set Variable   0
	${REAL_NUM}=    Set Variable   0
	:FOR	${LINE}	IN	@{LINES}
	\	${FOUND_IMAGE}=	    Run Keyword And Return Status	Should Contain	    ${LINE} 	virtualization_package_Branch-NG
	\   ${REAL_NUM}=    Evaluate    ${LINE_NUM}-2
	\   Run Keyword If  ${FOUND_IMAGE}  Set Suite Variable  ${REAL_LINE}     @{LINES}[${REAL_NUM}]
	\	Run Keyword If	${FOUND_IMAGE}	Exit For Loop
	\   ${LINE_NUM}=    Evaluate    ${LINE_NUM}+1
	Run Keyword Unless	${FOUND_IMAGE}	Fail	The latest release file was not found and the latest release image could not be downloaded, exiting!

    :FOR  ${i}  IN RANGE    1   8
	\   ${LINE_SPLITTED_BY_SPACE}=	Split String	@{LINES}[${REAL_NUM}]
	\   ${4.0_IMAGE}=  Run Keyword And Return Status   Should Match Regexp  @{LINE_SPLITTED_BY_SPACE}[8]   nodegrid-genericx86-64-\\d+_Branch-NG_4.0_\\d+_R\\d+-r\\d+.iso$
	\   Run Keyword If      ${4.0_IMAGE}      Set Suite Variable	${REAL_RELEASE_FILE}	@{LINE_SPLITTED_BY_SPACE}[8]
	\   Run Keyword If      ${4.0_IMAGE}	    Exit For Loop
	\   ${REAL_NUM}=    Evaluate    ${REAL_NUM}-1
	Run Keyword Unless  ${4.0_IMAGE}   Fail        The latest release file was not found and the latest release image could not be downloaded, exiting!
	Log     ${REAL_RELEASE_FILE}        INFO       console=yes

Downgrade To The Latest Release Image
    #Pass Execution If   '${NGVERSION}' > '4.0'  No release image for 4.1
	CLI:Open
	CLI:Enter Path	/system/toolkit/
	CLI:Write	software_upgrade
	CLI:Write	set image_location=remote_server
	CLI:Write	set url=ftp://192.168.2.201:21/releases/2019/${REAL_RELEASE_FILE}
	CLI:Write	set username=anonymous
	CLI:Write	set password=anonymous
	Run Keyword If  '${NGVERSION}' == '4.0'		CLI:Write	set if_downgrading=apply_factory_default_configuration
	...	ELSE	CLI:Write 	set if_downgrading=restore_configuration_saved_on_version_upgrade
	Write	commit\n
	Set Client Configuration  timeout=5 seconds
	${STATUS}=  Run Keyword And Return Status  read until prompt
	Run Keyword If  ${STATUS}   Fail    Failed to download image.
	Sleep	100s
	Wait Until Keyword Succeeds	    10x	    50s	    CLI:Open

Find The Latest Daily Image
    Set Tags	NON-CRITICAL
	CLI:Connect As Root
	CLI:Write	cd /var/sw

	${HOUR}=    CLI:Write   date "+%H%M"
	${LINES}=	Split To Lines	      ${HOUR}	0	-1
	#If test is running before 4am PST, then it gets yesterday's image, else it gets the day's image
	${DATE}=    Run Keyword If  '@{LINES}[0]' < '1200'    CLI:Write  date --date="1 days ago" "+%Y%m%d"  ELSE    CLI:Write  date "+%Y%m%d"
	${DATE_YESTERDAY}=    Run Keyword If  '@{LINES}[0]' < '1200'    CLI:Write  date --date="2 days ago" "+%Y%m%d"  ELSE    CLI:Write  date --date="1 days ago" "+%Y%m%d"
	#Trunk's image is done at ~4am PST. Trunk's automation usually starts at ~9am.
	${DATE_4.1}=    CLI:Write  date "+%Y%m%d"
	${DATE4.1_YESTERDAY}=  CLI:Write  date --date="1 days ago" "+%Y%m%d"
	${LINES}=	Run Keyword If  '${NGVERSION}' == '4.0'     Split To Lines	      ${DATE}	            0	-1  ELSE    Split To Lines	      ${DATE_4.1}	0	-1
	Set Suite Variable    ${DATE}         @{LINES}[0]
	Set Suite Variable    ${DATE_4.1}     @{LINES}[0]
	${LINES}=	Run Keyword If  '${NGVERSION}' == '4.0'     Split To Lines	      ${DATE_YESTERDAY}	    0	-1  ELSE    Split To Lines	      ${DATE4.1_YESTERDAY}	0	-1
	Set Suite Variable    ${DATE_YESTERDAY}         @{LINES}[0]
	Set Suite Variable    ${DATE4.1_YESTERDAY}      @{LINES}[0]

	Set Suite Variable    ${NAME}       (\\S+\\s+){8}nodegrid-genericx86-64-${DATE}\\d+_Branch-NG_4.0_\\d+_R\\d+-r\\d+.iso$
	Run Keyword If  '${NGVERSION}' > '4.0'  Set Suite Variable    ${NAME}     (\\S+\\s+){8}nodegrid-genericx86-64-${DATE_4.1}\\d+_Branch-NG_4.1_\\d+_R\\d+-r\\d+.iso$

	Write	ftp 192.168.2.201 21
	Read Until	Name (192.168.2.201:root):
	Write	anonymous
	Read Until	Password:
	Write	anonymous
	Read Until	ftp>
	Write	ls
	${OUTPUT}=	Read Until	ftp>
	${LINES}=	Split To Lines	${OUTPUT}
	${FOUND_IMAGE}=	Set Variable	False
	${IMAGE_LINE}=	Set Variable	${EMPTY}
	${LOOK_FOR}=	Set Variable If	'${NGVERSION}'>='4.0'   ${NAME}
	:FOR	${LINE}	IN	@{LINES}
	\	${FOUND_IMAGE}=	    Run Keyword And Return Status	Should Match Regexp	    ${LINE} 	${NAME}
	\	${IMAGE_LINE}=	Set Variable	${LINE}
	\	Run Keyword If	${FOUND_IMAGE}	Exit For Loop

	#If failed, try to get yesterday's image
	${RETRY}=   Run Keyword Unless	${FOUND_IMAGE}	SUITE:Try Yesterday's Image

	${LINE_SPLITTED_BY_SPACE}=	Run Keyword If   ${FOUND_IMAGE}   Split String	    ${IMAGE_LINE}   ELSE   Split String     ${RETRY}
	Set Suite Variable	${REAL_FILE}	@{LINE_SPLITTED_BY_SPACE}[8]
	Log     ${REAL_FILE}        INFO       console=yes
	CLI:Close Current Connection

#Checking if format_partition=no is not changing the configuration
Add Script To Old Image
	CLI:Connect as Root
	CLI:Write	> /etc/scripts/custom_commands/config_test.py
	CLI:Write	/bin/echo "def CustomTest(dev):" > /etc/scripts/custom_commands/config_test.py
	CLI:Write	/bin/echo " print('hello')" >> /etc/scripts/custom_commands/config_test.py
	CLI:Close Current Connection

Upgrade To The Latest Daily Image
    Set Tags	NON-CRITICAL
	CLI:Open
	CLI:Enter Path	/system/toolkit/
	CLI:Write	software_upgrade
	CLI:Write	set image_location=remote_server
	CLI:Write	set url=ftp://192.168.2.201:21/${REAL_FILE}
	CLI:Write	set username=anonymous
	CLI:Write	set password=anonymous
	CLI:Write	set format_partitions_before_upgrade=no
	Write	commit\n
	Set Client Configuration  timeout=5 seconds
	${STATUS}=  Run Keyword And Return Status  read until prompt
	Run Keyword If  ${STATUS}   Fail    Failed to download image.
	Sleep	180s
	Wait Until Keyword Succeeds	    20x	    50s	    CLI:Open

#Checking if format_partition=no is not changing the configuration
Check If Script Is Still There
    Set Tags	NON-CRITICAL 	#Latest Release Image has bug. Won't pass until it is upgraded (2/7/19)
	CLI:Connect As Root
	CLI:Write  cd /etc/scripts/custom_commands/
	${OUTPUT}=		CLI:Write  	ls
	Should Contain 		${OUTPUT} 	config_test.py
	${CONTENT}=		CLI:Write  	/bin/cat config_test.py
	Should Contain 		${CONTENT} 	def CustomTest(dev):
	CLI:Write 		rm config_test.py

#Version should not be the official version
Check if upgrade was successful
	Set Tags	NON-CRITICAL
	${V}=	CLI:Write  cat /software
	Should Not Contain 	${V} 	VERSION=4.0.13