*** Settings ***
Resource    ../init.robot
Documentation	Upgrades the nodegrid image with the latest available image from ftp server
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Force Tags         CLI
Default Tags        SSH

Suite Setup     CLI:Connect As Root
Suite Teardown  Close Connection

*** Test Cases ***

Find The Latest Image
	Write	ftp 192.168.2.201 21
	Read Until	Name (192.168.2.201:root):
	Write	anonymous
	Read Until	Password:
	Write	anonymous
	Read Until	ftp>
	Write	ls
	${OUTPUT}=	Read Until	ftp>
	${LINES}=	Split To Lines	${OUTPUT}
	${FOUND_IMAGE}=	Set Variable	False
	${IMAGE_LINE}=	Set Variable	${EMPTY}
	${LOOK_FOR}=	Set Variable If	'${NGVERSION}'=='4.0'	nodegrid_Trunk.iso	nodegrid_Branch-NG_3.2_20160225_R2938.iso
	:FOR	${LINE}	IN	@{LINES}
	\	${FOUND_IMAGE}=	Run Keyword And Return Status	Should Contain	${LINE}	${LOOK_FOR}
	\	${IMAGE_LINE}=	Set Variable	${LINE}
	\	Run Keyword If	${FOUND_IMAGE}	Exit For Loop
	Run Keyword Unless	${FOUND_IMAGE}	Fail	The ${LOOK_FOR} file was not found, exiting!
	${LINE_SPLITTED_BY_SPACE}=	Split String	${IMAGE_LINE}
	${REAL_FILE}=	Get Substring	@{LINE_SPLITTED_BY_SPACE}[10]	10
	Set Suite Variable	${REAL_FILE}	${REAL_FILE}
	CLI:Write	quit
	Close Connection

Upgrade
	CLI:Open
	CLI:Enter Path	/system/toolkit/
	CLI:Write	software_upgrade
	CLI:Write	set image_location=remote_server
	CLI:Write	set url=ftp://192.168.2.201:21/${REAL_FILE}
	CLI:Write	set username=anonymous
	CLI:Write	set password=anonymous
	CLI:Write	set format_partitions_before_upgrade=yes
	Write	commit\n
	Sleep	100s
	Wait Until Keyword Succeeds	6x	50s	CLI:Open
