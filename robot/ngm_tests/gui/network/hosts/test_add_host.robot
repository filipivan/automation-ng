*** Settings ***
Documentation	Testing adding Host under the Network tab
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	GUI

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test Invalid IP
	GUI::Basic::Add
	Input Text	//*[@id="hostip"]	123
	Input Text	//*[@id="name"]	Dummy
	Input Text	//*[@id="alias"]	Dummy
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Invalid IP address.
	Input Text	//*[@id="hostip"]	127.0.0.0
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Test Duplicate IP
	GUI::Basic::Add
	Input Text	//*[@id="hostip"]	127.0.0.0
	Input Text	//*[@id="name"]	Dummy
	Input Text	//*[@id="alias"]	Dummy
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Entry already exists.
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible

Test Delete Host
	Page Should Contain Element	//*[@id="127.0.0.0"]
	Click Element	//*[@id="127.0.0.0"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	Page Should Not Contain Element	//*[@id="127.0.0.0"]

Test Fields Add Host
	GUI::Basic::Add
	Gui::Basic::Auto Input Tests	hostip  	not.valid.ip.add	345.345.345.345	234.234.234.234
	GUI::Basic::Spinner Should Be Invisible
	Gui::Basic::Auto Input Tests	name	    n[]name	not/valid	@name	valid.name
	GUI::Basic::Spinner Should Be Invisible

	Click Link  //*[@id="234.234.234.234"]
	GUI::Basic::Spinner Should Be Invisible
	Gui::Basic::Auto Input Tests	alias	n[]name	not/valid	@name	valid.name
	GUI::Basic::Spinner Should Be Invisible

Test Case to Delete Added field host
	Page Should Contain Element     //*[@id="234.234.234.234"]/td[1]/input
	Click Element       						//*[@id="234.234.234.234"]/td[1]/input
	GUI::Basic::Delete

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	
	GUI::Basic::Network::Hosts::Open Tab
	Element Should Be Visible	jquery=#addButton
	
SUITE:Teardown
	GUI::Basic::Logout
	Close all Browsers
