*** Settings ***
Documentation	Testing dhcp support for ipv6 validation
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	NON-CRITICAL	NEED-REVIEW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${Client_interface}	ETH1
${Server_interface}	ETH1
${PREFIX}	2001:0db8:cafe::
${LENGTH}	64
${iplo}	2001:0db8:cafe::
${iphi}	2001:0db8:cafe::ffff
${hostname}	nodegrid
${hw_Address}	e4:1a:2c:00:71:62
${host_ipadd}	2001:db8:cafe::fffb

*** Test Cases ***
Test case to configure client interface
	Skip If	not ${ETH1_CONNECTION_BETWEEN_HOST_AND_SHARED}	dhcpv6 not configured/available for this build
	Page Should Contain	${Client_interface}
	Click Link	${Client_interface}
	GUI::Basic::Spinner Should Be Invisible
	SUITE:configure client interface

Test case to configure server interface
	Skip If	not ${ETH1_CONNECTION_BETWEEN_HOST_AND_SHARED}	dhcpv6 not configured/available for this build
	SUITE:Setup1
	Page Should Contain	${Server_interface}
	Click Link	${Server_interface}
	GUI::Basic::Spinner Should Be Invisible
	SUITE:configure Server interface

Test case to add dhcp server configuration
	Skip If	not ${ETH1_CONNECTION_BETWEEN_HOST_AND_SHARED}	dhcpv6 not configured/available for this build
	Run Keyword If	'${NGVERSION}' <= '5.4'	GUI::Basic::Network::DHCP Server::Open Tab
	Run Keyword If	'${NGVERSION}' >= '5.6'	Run Keywords	GUI::Basic::Network::DHCP::Open Tab	AND	Click Element	//span[normalize-space()='DHCP Server']
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	Click Element	xpath=(//input[@id='dhcpserverprotocol'])[2]
	Input Text	//*[@id="dhcpd6prefix"]	${PREFIX}
	Input Text	//*[@id="dhcpd6length"]	${LENGTH}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Test case to add Network Range
	Skip If	not ${ETH1_CONNECTION_BETWEEN_HOST_AND_SHARED}	dhcpv6 not configured/available for this build
	Click Element	xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[2]/a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=/html/body/div[7]/div[1]/a[2]/span
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	jquery=#dhcpdrangeTable
	Click Element	jquery=#addButton
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add Valid IP Range	${iplo}	${iphi}

Test case to Add servers host
	Skip If	not ${ETH1_CONNECTION_BETWEEN_HOST_AND_SHARED}	dhcpv6 not configured/available for this build
	Click Element	xpath=/html/body/div[7]/div[1]/a[3]/span
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	jquery=#dhcpdhostTable
	Click Element	jquery=#addButton
	GUI::Basic::Spinner Should Be Invisible
	Sleep	10s
	Input Text	//*[@id="dhcpdhostname"]	${hostname}
	Input Text	//*[@id="dhcpdhosthwadd"]	${hw_Address}
	Input Text	//*[@id="dhcpdhostipadd"]	${host_ipadd}
	GUI::Basic::Save

Test case to check the client interface received ip configured on server host
	Skip If	not ${ETH1_CONNECTION_BETWEEN_HOST_AND_SHARED}	dhcpv6 not configured/available for this build
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	NON-CRITICAL	NEED-REVIEW	#BUG_NG_6911
	Switch Browser	1
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${host_ipadd}

Test case to check for DHCPv6 in Tracking page
	GUI::Basic::Tracking::Network::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//span[normalize-space()='DHCP']
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	//*[@id="thead"]/tr/th[2]

Test case to delete the servers configuration
	Skip If	not ${ETH1_CONNECTION_BETWEEN_HOST_AND_SHARED}	dhcpv6 not configured/available for this build
	Switch Browser	2
	GUI::Basic::Return
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${Client_interface}
	Click Link	${Client_interface}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=.radio:nth-child(2) #method
	Click Element	css=.radio:nth-child(1) #method6
	GUI::Basic::Save
	Run Keyword If	'${NGVERSION}' <= '5.4'	GUI::Basic::Network::DHCP Server::Open Tab
	Run Keyword If	'${NGVERSION}' >= '5.6'	Run Keywords	GUI::Basic::Network::DHCP::Open Tab	AND	Click Element	//span[normalize-space()='DHCP Server']
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="thead"]/tr/th[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

Test case to Take off Clients interface configuration
	Skip If	not ${ETH1_CONNECTION_BETWEEN_HOST_AND_SHARED}	dhcpv6 not configured/available for this build
	Switch Browser	1
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${Client_interface}
	Click Link	${Client_interface}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=.radio:nth-child(2) #method
	Click Element	css=.radio:nth-child(1) #method6
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	Skip If	not ${ETH1_CONNECTION_BETWEEN_HOST_AND_SHARED}	dhcpv6 not configured/available for this build
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Setup1
	Skip If	not ${ETH1_CONNECTION_BETWEEN_HOST_AND_SHARED}	dhcpv6 not configured/available for this build
	GUI::Basic::Open Nodegrid	${HOMEPAGESHARED}	${BROWSER}	${NGVERSION}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::login
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Skip If	not ${ETH1_CONNECTION_BETWEEN_HOST_AND_SHARED}	dhcpv6 not configured/available for this build
	Close All Browsers

SUITE:Add Valid IP Range
	[Arguments]	${IPO}	${IPT}
	Sleep	10s
	Input Text	jquery=#dhcpdrangelo	${IPO}
	Input Text	jquery=#dhcpdrangehi	${IPT}
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

SUITE:Add valid Host
	[Arguments]	${HOSTNAME}	${HW_ADDRESS}	${HOST_IPADD}
	Input Text	//*[@id="dhcpdhostname"]	${HOSTNAME}
	Input Text	//*[@id="dhcpdhosthwadd"]	${HW_ADDRESS}
	Input Text	//*[@id="dhcpdhostipadd"]	${HOST_IPADD}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:configure client interface
	Click Element	//*[@id="netmngConnAddForm"]/div[2]/div[2]/div/div[7]/div/div[3]/label
	Click Element	css=.radio:nth-child(3) #method6
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:configure Server interface
	Click Element	//*[@id="method"]
	Click Element	css=.radio:nth-child(4) #method6
	Input Text	//*[@id="address6"]	2001:db8:cafe::dddd:101
	Input Text	//*[@id="netmask6"]	64
	Input Text	//*[@id="gateway6"]	2001:db8:cafe::dddd:100
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
