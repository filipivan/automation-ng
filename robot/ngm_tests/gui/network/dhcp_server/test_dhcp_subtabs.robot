*** Settings ***
Documentation	Testing adding DHCP Server under the Network tab
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	GUI

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Add DHCP Server
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	  jquery=#dhcpdsubnet		 123.123.123.0
	Input Text	  jquery=#dhcpdnetmask		255.255.255.0
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Test Add IP Range
	#Select the DHCP created on function "Add DHCP Server"
	Click Link	xpath=//*[@id="123.123.123.0/255.255.255.0"]
	
	GUI::Network::DHCP Server::Network Range::Open Tab
	Element Should Be Visible   jquery=#dhcpdrangeTable

	GUI::Basic::Add
	Input Text	//*[@id="dhcpdrangelo"]	Invalid IP range.
	Input Text	//*[@id="dhcpdrangehi"]	Invalid IP range.
	GUI::Basic::Save

	Page Should Not Contain	//*[@id="globalerr"]/div/div/p
	SUITE:Add Valid IP Range	  123.123.123.8   123.123.123.10

Test Duplicate IP Range
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	
	SUITE:Add Valid IP Range	  123.123.123.8   123.123.123.10
	#This error apears when you already have this range created
	Wait Until Element Contains	jquery=#globalerr	Invalid IP range.

Test Add Second IP Range
	SUITE:Add Valid IP Range	  123.123.123.46  123.123.123.122
	Page Should Contain Element	 xpath=//*[@id="tbody"]/tr[2]

Test Delete IP Ranges
	Select Checkbox	 xpath=//*[@id="thead"]/tr/th[1]/input
	GUI::Basic::Delete
	Sleep  10s
	Page Should Not Contain Element	 xpath=//*[@id="tbody"]/tr

Test Add Host
	GUI::Network::DHCP Server::Hosts::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	  jquery=#dhcpdhostname   hostname
	GUI::Basic::Auto Input Tests	dhcpdhosthwadd  string  1.1.1.1	 00:26:DD:14:C4:EE
	GUI::Basic::Auto Input Tests	dhcpdhostipadd  string  ::2c	1.1.1.1
	GUI::Basic::Spinner Should Be Invisible

Test Add Duplicated DHCP Host on Same Server
	[Documentation]	It tries to add a second host with same name the DHCP Server. It was created after BUG_NG_9086
	Skip If	'${NGVERSION}' <= '5.0'	Fix only implemented on 5.2+
	
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	  jquery=#dhcpdhostname   hostname
	GUI::Basic::Auto Input Tests	dhcpdhosthwadd  string  1.1.1.1	 00:26:DD:14:C4:EE
	GUI::Basic::Auto Input Tests	dhcpdhostipadd  string  ::2c	1.1.1.1
	Wait Until Element Contains	id=errormsg	Entry already exists.

	[Teardown]	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible

Test Add Duplicated DHCP Host on Another Server
	[Documentation]	It tries to add a host with same name to a second DHCP Server. It was created after BUG_NG_9086
	Skip If	'${NGVERSION}' <= '5.0'	Fix only implemented on 5.2+
	Run Keyword If	'${NGVERSION}' <= '5.4'	GUI::Basic::Network::DHCP Server::Open Tab
	Run Keyword If	'${NGVERSION}' >= '5.6'	Run Keywords	GUI::Basic::Network::DHCP::Open Tab	AND	Click Element	//span[normalize-space()='DHCP Server']
	GUI::Basic::Spinner Should Be Invisible
	Sleep	15s
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	  jquery=#dhcpdsubnet		 123.123.0.0
	Input Text	  jquery=#dhcpdnetmask		255.255.0.0
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	
	Click Element   xpath=//*[@id="tbody"]/tr[2]/td[2]/a
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::DHCP Server::Hosts::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	  jquery=#dhcpdhostname   hostname
	Input Text	dhcpdhosthwadd		00:26:DD:14:C4:EE
	Input Text	dhcpdhostipadd		1.1.1.1
	Click Element   id=saveButton
	Wait Until Element Contains	id=errormsg	Entry already exists.
	[Teardown]	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible

Test Delete Host
	Run Keyword If	'${NGVERSION}' <= '5.4'	GUI::Basic::Network::DHCP Server::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' >= '5.6'	GUI::Basic::Network::DHCP::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' >= '5.6'	Click Element	xpath=//span[contains(.,'DHCP Server')]
	GUI::Basic::Spinner Should Be Invisible

	Click Element   xpath=//*[@id="tbody"]/tr/td[2]/a
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::DHCP Server::Hosts::Open Tab
	Element Should Be Visible	   jquery=#dhcpdhostTable
	Select Checkbox	 //*[@id="hostname"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Does Not Contain	 xpath=//*[@id="tbody"]/tr

Return and Delete DHCP
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Return

	Select Checkbox	 xpath=//*[@id="thead"]/tr/th[1]/input
	GUI::Basic::Delete

	GUI::Basic::Spinner Should Be Invisible
	Page Should Not Contain Element	 xpath=//*[@id="tbody"]/tr

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' <= '5.4'	GUI::Basic::Network::DHCP Server::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' >= '5.6'	GUI::Basic::Network::DHCP::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' >= '5.6'	Click Element	xpath=//span[contains(.,'DHCP Server')]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	15s

SUITE:Teardown
	Close All Browsers

SUITE:Add Valid IP Range
	[Arguments]	 ${IPO}  ${IPT}
	Input Text  //*[@id="dhcpdrangelo"]	 ${IPO}
	Input Text  //*[@id="dhcpdrangehi"]	${IPT}
	Click Element   jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible