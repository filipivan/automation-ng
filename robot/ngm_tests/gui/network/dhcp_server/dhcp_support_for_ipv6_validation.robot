*** Settings ***
Documentation	Testing dhcp support for ipv6 validation 
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${prefix}	2001:0db8:cafe::
${length}	64
${ipo}	2001:0db8:cafe::
${ipt}	2001:0db8:cafe::fffa
${invalid_len1}	invalid
${invalid_len2}	129
${invalid_prefix}	192.168.2.1
${hostname}	nodegrid
${hw_Address}	e4:1a:2c:00:71:62
${host_ipadd}	2001:db8:cafe::ffff
${invalid_hostip}	1.2.3.5
${inv_lo1}	2001:0db8:cafe::ffff
${inv_lo2}	192.168.2.3
${inv_hi}	2001:0db8:cafe::fffa
${ipo1}	2001:0db8:cafe::fffa
${ipt1}	2001:0db8:cafe::ffff

*** Test Cases ***
Test case to check Add Tab
	Run Keyword If	'${NGVERSION}' <= '5.4'	GUI::Basic::Network::DHCP Server::Open Tab
	Run Keyword If	'${NGVERSION}' >= '5.6'	Run Keywords	GUI::Basic::Network::DHCP::Open Tab	AND	Click Element	//span[normalize-space()='DHCP Server']
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	DHCP4
	Page Should Contain	SubNet:
	Page Should Contain	Netmask:
	Page Should Contain	Optional Parameters
	Page Should Contain	Domain:
	Page Should Contain	Domain Name Servers
	Page Should Contain	Router IP:
	Page Should Contain	Lease Time (s):
	Page Should Contain	DHCP6
	Click Element	xpath=(//input[@id='dhcpserverprotocol'])[2]
	Page Should Contain	Prefix:
	Page Should Contain	Length:

Test case to check Invalid prefix/length
	Input Text	//*[@id="dhcpd6prefix"]	${invalid_len1}
	Input Text	//*[@id="dhcpd6length"]	${invalid_len1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Input Text	//*[@id="dhcpd6prefix"]	${invalid_prefix}
	Input Text	//*[@id="dhcpd6length"]	${invalid_len2}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.

Test case To add Valid dhcpv6
	Input Text	//*[@id="dhcpd6prefix"]	${prefix}
	Input Text	//*[@id="dhcpd6length"]	${length}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

test case to Drilldown and Select Network Range
	[Tags]	NON-CRITICAL	NEED-REVIEW	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	Click Element	xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[2]/a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=/html/body/div[7]/div[1]/a[2]/span
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	jquery=#dhcpdrangeTable

Test case to Add IP Range
	[Tags]	NON-CRITICAL	NEED-REVIEW	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	Click Element	jquery=#addButton
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="dhcpdrangelo"]	${inv_lo1}
	Input Text	//*[@id="dhcpdrangehi"]	${inv_hi}
	GUI::Basic::Save
	Wait Until Page Contains Element	//*[@id="globalerr"]/div/div/p
	Input Text	//*[@id="dhcpdrangelo"]	${inv_lo2}
	Input Text	//*[@id="dhcpdrangehi"]	${inv_hi}
	GUI::Basic::Save
	Wait Until Page Contains Element	//*[@id="globalerr"]/div/div/p
	SUITE:Add Valid IP Range	${ipo}	${ipt}

Test case to check Duplicate IP Range
	[Tags]	NON-CRITICAL	NEED-REVIEW	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=#addButton
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add Valid IP Range	${ipo}	${ipt}
	Page Should Contain Element	xpath=/html/body/div[7]/div[2]/div[2]/form/div[1]/div/div/p

Test case to Add Second IP Range
	[Tags]	NON-CRITICAL	NEED-REVIEW	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	SUITE:Add Valid IP Range	${ipo1}	${ipt1}
	Page Should Contain Element	xpath=//*[@id="tbody"]/tr[2]

Test case to validate Hosts
	[Tags]	NON-CRITICAL	NEED-REVIEW	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	Click Element	xpath=/html/body/div[7]/div[1]/a[3]/span
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	jquery=#dhcpdhostTable
	Click Element	jquery=#addButton
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="dhcpdhostname"]	${hostname}
	Input Text	//*[@id="dhcpd6hostduidadd"]	${hw_Address}
	Input Text	//*[@id="dhcpdhostipadd"]	${invalid_hostip}
	GUI::Basic::Save
	Wait Until Page Contains Element	//*[@id="globalerr"]/div/div/p

Test case to Add Valid Host
	[Tags]	NON-CRITICAL	NEED-REVIEW	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	SUITE:Add valid Host	${hostname}	${hw_Address}	${host_ipadd}
	GUI::Basic::Spinner Should Be Invisible
	Sleep	10s
	Page Should Contain	nodegrid

Test Case to Delete Host
	[Tags]	NON-CRITICAL	NEED-REVIEW	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	#BUG_NG_6900
	Select Checkbox	//*[@id="nodegrid"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

Test Case to add duplicate dhcpv6
	[Tags]	NON-CRITICAL	NEED-REVIEW	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	Run Keyword If	'${NGVERSION}' <= '5.4'	GUI::Basic::Network::DHCP Server::Open Tab
	Run Keyword If	'${NGVERSION}' >= '5.6'	Run Keywords	GUI::Basic::Network::DHCP::Open Tab	AND	Click Element	//span[normalize-space()='DHCP Server']
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	Click Element	xpath=(//input[@id='dhcpserverprotocol'])[2]
	Input Text	//*[@id="dhcpd6prefix"]	${prefix}
	Input Text	//*[@id="dhcpd6length"]	${length}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="globalerr"]/div/div/p
	GUI::Basic::Cancel

Test case to delete ip ranges
	[Tags]	NON-CRITICAL	NEED-REVIEW	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	Run Keyword If	'${NGVERSION}' <= '5.4'	GUI::Basic::Network::DHCP Server::Open Tab
	Run Keyword If	'${NGVERSION}' >= '5.6'	Run Keywords	GUI::Basic::Network::DHCP::Open Tab	AND	Click Element	//span[normalize-space()='DHCP Server']
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//a[contains(text(),'2001:db8:cafe::/64')]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="range"]/span
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="thead"]/tr/th[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

Test case to delete dhcp server
	[Tags]	NON-CRITICAL	NEED-REVIEW	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	Run Keyword If	'${NGVERSION}' <= '5.4'	GUI::Basic::Network::DHCP Server::Open Tab
	Run Keyword If	'${NGVERSION}' >= '5.6'	Run Keywords	GUI::Basic::Network::DHCP::Open Tab	AND	Click Element	//span[normalize-space()='DHCP Server']
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="thead"]/tr/th[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Close All Browsers

SUITE:Add Valid IP Range
	[Arguments]	${IPO}	${IPT}
	Input Text	jquery=#dhcpdrangelo	${IPO}
	Input Text	jquery=#dhcpdrangehi	${IPT}
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

SUITE:Add valid Host
	[Arguments]	${HOSTNAME}	${HW_ADDRESS}	${HOST_IPADD}
	Input Text	//*[@id="dhcpdhostname"]	${HOSTNAME}
	Input Text	//*[@id="dhcpd6hostduidadd"]	${HW_ADDRESS}
	Input Text	//*[@id="dhcpdhostipadd"]	${HOST_IPADD}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible