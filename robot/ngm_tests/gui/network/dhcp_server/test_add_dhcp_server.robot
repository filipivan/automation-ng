*** Settings ***
Documentation	Testing adding DHCP Server under the Network tab
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test NonAccess Controls
	Page Should Contain Element	//*[@id="addButton"]
	Page Should Contain Element	//*[@id="delButton"]

Test Add Tab
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	SubNet:
	Page Should Contain	Netmask:
	Page Should Contain	Optional Parameters
	Page Should Contain	Domain:
	Page Should Contain	Domain Name Servers
	Page Should Contain	Router IP:

Test Invalid Subnet/Netmask
	Input Text	//*[@id="dhcpdsubnet"]	invalid
	Input Text	//*[@id="dhcpdnetmask"]	invalid
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Test Subnet and Netmask
	Input Text	//*[@id="dhcpdnetmask"]	255.255.255.0
	GUI::Basic::Auto Input Tests	dhcpdsubnet	not.valid.sub.net	a.b.c.d	 192.168.158.0
	GUI::Basic::Spinner Should Be Invisible

	Click element	xpath=//a[contains(text(),'192.168.158.0/255.255.255.0')]

Test Domian Name
	GUI::Basic::Auto Input Tests	domainname	a,a	b,b	qwe.qwe
	Input Text	//*[@id="domainname"]	asd

Test Domain Name Server
	GUI::Basic::Auto Input Tests	domainnameserver	not.val.ip.add	335.1.1.1	 1.1.1.1
	Input Text	//*[@id="domainnameserver"]	123.1.1.1

Test Routers
	GUI::Basic::Auto Input Tests	routers	not.val.ip.add	555.1.1.1	 2.2.2.2
	GUI::Basic::Return
	GUI::Basic::Spinner Should Be Invisible

	Select Checkbox	//*[@id="thead"]/tr/th[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

Test add Dhcp Server
	Run Keyword If	'${NGVERSION}' <= '5.4'	GUI::Basic::Network::DHCP Server::Open Tab
	Run Keyword If	'${NGVERSION}' >= '5.6'	Run Keywords	GUI::Basic::Network::DHCP::Open Tab	AND	Click Element	//span[normalize-space()='DHCP Server']
	GUI::Basic::Spinner Should Be Invisible
	Sleep	15s
	GUI::Basic::Add
	Input Text	//*[@id="dhcpdsubnet"]	192.168.2.0
	Input Text	//*[@id="dhcpdnetmask"]	255.255.255.0
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Test leaving hw_address and circuit_id as blank
	[Documentation]	Test set both hw_address and agent_circuit_id as blank and checks that is not a valid configuration
	...	and the proper error message should be raised.
	[Tags]	NON-CRITICAL	EXCLUDEIN3_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4
	GUI::Basic::Network::DHCP::Open Tab
	Click Element	//span[normalize-space()='DHCP Server']
	GUI::Basic::Spinner Should Be Invisible
	Click Link	//*[@id="192.168.2.0/255.255.255.0"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::DHCP Server::Hosts::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	Input Text	dhcpdhostname	test
	Input Text	dhcpdhosthwadd	${EMPTY}
	Input Text	dhcpdhostcircuitid	${EMPTY}
	Input Text	dhcpdhostipadd	1.1.1.1
	GUI::Basic::Save
	Page Should Contain	At least one of the fields (HW Address or Agent Circuit ID) must be present.
	[Teardown]	GUI::Basic::Cancel

Test to add duplicate server
	Run Keyword If	'${NGVERSION}' <= '5.4'	GUI::Basic::Network::DHCP Server::Open Tab
	Run Keyword If	'${NGVERSION}' >= '5.6'	Run Keywords	GUI::Basic::Network::DHCP::Open Tab	AND	Click Element	//span[normalize-space()='DHCP Server']
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="dhcpdsubnet"]	192.168.2.0
	Input Text	//*[@id="dhcpdnetmask"]	255.255.255.0
	GUI::Basic::Save
	Wait Until Page Contains Element	//*[@id="globalerr"]/div/div/p
	GUI::Basic::Cancel

Test case to delete server
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' <= '5.4'	GUI::Basic::Network::DHCP Server::Open Tab
	Run Keyword If	'${NGVERSION}' >= '5.6'	Run Keywords	GUI::Basic::Network::DHCP::Open Tab	AND	Click Element	//span[normalize-space()='DHCP Server']
	GUI::Basic::Spinner Should Be Invisible
	Sleep	15s
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="thead"]/tr/th[1]/input
	GUI::Basic::Delete

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' <= '5.4'	GUI::Basic::Network::DHCP Server::Open Tab
	Run Keyword If	'${NGVERSION}' >= '5.6'	Run Keywords	GUI::Basic::Network::DHCP::Open Tab	AND	Click Element	//span[normalize-space()='DHCP Server']
	GUI::Basic::Spinner Should Be Invisible
	Sleep	15s

SUITE:Teardown
	Close All Browsers
