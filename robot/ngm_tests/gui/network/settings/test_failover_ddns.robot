*** Settings ***
Documentation	Testing the DNS options for the Network:Settings tab
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	GUI

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
@{String_tests}	not/valid	a'm'i'	sec/nonam][	test#works	a\\s\\	no..work	thisvalid

*** Test Cases ***
Enable DDNS
    ${CHK}=     Run Keyword And Return Status               Checkbox Should Be Selected     jquery=#failover
    Run Keyword Unless      ${CHK}      Select Checkbox     jquery=#failover
    ${CHK}=     Run Keyword And Return Status               Checkbox Should Be Selected     jquery=#ddns
    Run Keyword Unless      ${CHK}      Select Checkbox     jquery=#ddns

Test DDNS Server Name
    GUI::Basic::Auto Input Tests     ddnsMaster  @{String_tests}

Test DDNS Port
    ${RAND}=    Evaluate    random.randint(55,255)   modules=random
    @{TESTS}=   Create List     12.4  .   aaaa     2\\24     24/1    ${RAND}
    GUI::Basic::Auto Input Tests     ddnsPort  @{TESTS}
    GUI::Basic::Spinner Should Be Invisible

Test DDNS Zone
    GUI::Basic::Auto Input Tests     ddnsZone  @{String_tests}
    GUI::Basic::Spinner Should Be Invisible

Test Failover Host Name
    GUI::Basic::Auto Input Tests     ddnsFailHostname  @{String_tests}
    GUI::Basic::Spinner Should Be Invisible

Test User Name
    GUI::Basic::Auto Input Tests     ddnsKeyUser  @{String_tests}
    GUI::Basic::Spinner Should Be Invisible

Test Key Algorithm Dropdown Contents
    @{CHK}=     Create List     HMAC-MD5    HMAC-SHA1    HMAC-SHA224    HMAC-SHA256    HMAC-SHA384    HMAC-SHA512
    @{ALG_DC}=  Get List Items  jquery=#ddnsKeyAlg
    Should Be Equal     ${CHK}  ${ALG_DC}

Test DDNS Key Size
    ${RAND}=    Evaluate    random.randint(2,63)    modules=random
    @{TESTS}=   Create List     12.4  .   aaaa     2\\24     24/1    ${RAND}
    GUI::Auditing::Auto Input Tests     ddnsKeySize  @{TESTS}
    GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Close all Browsers
	SUITE:Setup
	Input text	jquery=#ddnsMaster	${EMPTY}
	Input Text	jquery=#ddnsPort	53
	input Text	jquery=#ddnsZone	${EMPTY}
	input Text	jquery=#ddnsFailHostname	${EMPTY}
	input Text	jquery=#ddnsKeyUser	${EMPTY}
	Input Text	jquery=#ddnsKeySize	512
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	Unselect Checkbox	jquery=#ddns
	GUI::Basic::Spinner Should Be Invisible
	Click element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout
	Close all Browsers