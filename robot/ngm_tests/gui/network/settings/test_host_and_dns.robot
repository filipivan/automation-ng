*** Settings ***
Documentation	Testing the Host and DNS options for the Network:Settings tab
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	GUI

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
@{TESTS}=	..notvalid	not/valid	st'llnot	not.va/lid	valid

*** Test Cases ***
Check Visible Address and Search
	Element Should Be Visible	jquery=#dnsAddress
	Element Should Be Visible	jquery=#dnsSearch

Test Hostname Input Field
	GUI::Basic::Auto Input Tests	hostname	@{TESTS}

Test Domainname Input Field
	GUI::Basic::Auto Input Tests	domainname	@{TESTS}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Input Text	jquery=#hostname	nodegrid
	Input Text	jquery=#domainname	localdomain
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout
	Close all Browsers