*** Settings ***
Documentation	Testing the Failover options for the Network:Settings tab
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Enable Failover
	${CHK}=	Run Keyword And Return Status	Checkbox Should Be Selected	jquery=#failover
	Run Keyword Unless	${CHK}	Select Checkbox	jquery=#failover

Check Connections Contents
	[Tags]	NON-CRITICAL	NEED-REVIEW
#	Backplane 1 is missing on the device 4.2v and Backplane 0 is missing on 5.8v
	@{EXPECTEDS}=	Create List	BACKPLANE0	BACKPLANE1	ETH0	ETH1	hotspot
	@{PRIMARYCON}=	Get List Items	jquery=#primaryConn
	@{SECONDARYCON}=	Get List Items	jquery=#secondaryConn
	FOR	${EXPECTED}	IN	@{EXPECTEDS}
		List Should Contain Value	${PRIMARYCON}	${EXPECTED}
		List Should Contain Value	${SECONDARYCON}	${EXPECTED}
	END

Test Trigger IP Address
	Element Should Not Be Visible	jquery=#address
	Select Radio button	trigger	ipaddress
	Element Should Be Visible	jquery=#address
	@{VALIDS}=	Create List	12.12.12.12
	@{INVALIDS}=	Create List	1..245.2.1	.	12.53.12.53/2	12/25\\12#24
	GUI::Basic::Input Text::Validation	address	${VALIDS}	${INVALIDS}
	Select Radio Button	trigger	ipv4gw
	Element Should Not Be Visible	jquery=#address

Test Number Retries
	[Tags]	NON-CRITICAL	NEED-REVIEW
	${RAND}=	Evaluate	random.randint(6,256)	modules=random
	@{VALIDS}=	Create List	${RAND}
	@{INVALIDS}=	Create List	12.2431	.	53/2	12/25\\12#24	12-12
	GUI::Basic::Input Text::Validation	fail_retries	${VALIDS}	${INVALIDS}
	GUI::Basic::Input Text::Validation	success_retries	${VALIDS}	${INVALIDS}
	GUI::Basic::Input Text::Validation	interval_retries	${VALIDS}	${INVALIDS}
	Input Text	jquery=#fail_retries	3
	Input Text	jquery=#success_retries	1
	Input Text	jquery=#interval_retries	5
	GUI::Basic::Button::Save
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Static Routes::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${CONTAINS}=	Run Keyword And Return Status	Page Should Contain	Mobile Broadband GSM
	Run Keyword If	${CONTAINS}	GUI::Basic::Select Rows In Table Containing Value	peerTable	gsm-eth0
	Run Keyword If	${CONTAINS}	GUI::Basic::Delete
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Logout And Close Nodegrid

