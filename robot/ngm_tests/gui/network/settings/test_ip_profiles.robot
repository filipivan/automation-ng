*** Settings ***
Documentation	Testing the IPv4 and IPv6 Profiles for Network:Settings
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	GUI

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${Orig_loopback4}

*** Test Cases ***
Check IPv4 Box
	SUITE:Check Box	ipv4-ipforward	${FALSE}

Check IPv6 Box
	SUITE:Check Box	ipv6-forward	${FALSE}

Test Loopback Address
	${RAND}=	Evaluate	random.randint(1,255)	modules=random
	@{TESTS}=	Create List	165.124.456.123	not./valid	124.35.32.12/124	234]124[12\\12	12.12.12.${RAND}
	${Loopback4}=	Get Value	//*[@id="loopback4"]
	Set Global Variable	${Orig_loopback4}	${Loopback4}
	GUI::Basic::Auto Input Tests	loopback4	@{TESTS}

Check Dropdown List
	@{COMPARE}=	Create List	Disabled	Strict Mode	Loose Mode
	@{FILTER}=	Get List Items	jquery=#rpfilter
	Should Be Equal	${COMPARE}	${FILTER}
	GUI::Basic::Spinner Should Be Invisible

Check Routing Table Box
	SUITE:Check Box	multiroute	${TRUE}

Notice Visibility test
    GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	jquery=#notice

Restore Original IPV4 Loopback Address
	Input Text	//*[@id="loopback4"]	${Orig_loopback4}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout
	Close all Browsers

SUITE:Check Box
	[Arguments]	${LOCATION}	${CHK}
	[Documentation]	CHK should be true if the checkbox being passed is already checked
	Execute Javascript	document.getElementById("${LOCATION}").scrollIntoView(true);
	Click Element	jquery=#${LOCATION}
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	${CHK}==${FALSE}	Checkbox Should Be Selected	jquery=#${LOCATION}
	...	ELSE	Checkbox Should Not Be Selected	jquery=#${LOCATION}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	Execute Javascript	document.getElementById("${LOCATION}").scrollIntoView(true);
	Click Element	jquery=#${LOCATION}
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	${CHK}==${FALSE}	Checkbox Should Not Be Selected	jquery=#${LOCATION}
	...	ELSE	Checkbox Should Be Selected	jquery=#${LOCATION}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible