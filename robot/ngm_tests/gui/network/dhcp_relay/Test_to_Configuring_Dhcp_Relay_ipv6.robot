*** Settings ***
Documentation	Test to Configuring Dhcp Relay ipv6
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4


*** Variables ***
${Port_host1}	${SWITCH_NETPORT_HOST_INTERFACE1}
${Port_host2}	${SWITCH_NETPORT_HOST_INTERFACE2}
${Port_hostshared1}	${SWITCH_NETPORT_SHARED_INTERFACE1}
${Port_hostshared2}	${SWITCH_NETPORT_SHARED_INTERFACE2}
${COORD_HOMEPAGE}	${HOMEPAGE}
${PEER_HOMEPAGE}	${HOMEPAGESHARED}
${Vlan1_id}	100
${Vlan2_id}	200
${Vlan_Name1}	vlan100
${Vlan_Name2}	vlan200
${ip_v6_address_1}	2001:db8:caff::1
${ip_v6_address_2}	2001:db8:cafe::2
${ip_v6_Relay}	2001:db8:cafe::1
${Prefix_length}	64
${Dhcp_Prefix}	2001:db8:caff::
${Dhcp_length}	64
${Dhcp_Prefix1}	2001:db8:cafe::
${Ip_Start}	2001:db8:caff::100
${Ip_End}	2001:db8:caff::200
${Dhcp_Relay_Server}	backplane1.200
${Dhcp_Relay_Client}	backplane0.100
${Static_Destination_Ip}	2001:db8:caff::
${Static_Destination_Bit_Mask}	64
${Static_Destination_Gatewy_Ip}	2001:db8:cafe::1
${IPV6_address}

*** Test Cases ***
Test Case to Configure Vlan
	SUITE:To Configure Vlan on Host Side
	SUITE:To Config Vlan on Hostshared Side

Test Case to Config Server on Host side
	SUITE:To Config NG Server Connection on Host side

Test Case to Config NG-Relay on Host shared Side
	SUITE:To Config NG-Relay on Host Shared Side

Test Case to Config Client Connection on Host Side
	SUITE:To Config NG-Client Connection on HostSide

Test Case to Add Dhcp Server on Host Side
	SUITE:To Add Dhcp Server on Host Side
	SUITE:To Add Static Routes on NG-Server Host Side

Test Case to Add Dhcp Relay on Host Shared Side
	SUITE:To Add Dhcp Relay on HostShared Side

Test Case to Up the Connection on Host Side
	SUITE:TO Up the Connection on Server host side

Test Case to Delete Vlan on Host Side and Dhcp Server Config
	SUITE:To Delete Vlan on Host Side
	SUITE:To Delete Dhcp Server Config

Test Case to Delete Vlan on Host Shared Side and Dhcp Relay Config
	SUITE:To Delete Vlan on HostShared Side
	SUITE:To Delete Dhcp Relay Config

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:To Configure Vlan on Host Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select checkbox	xpath=(//input[@type='checkbox'])[10]
	Run Keyword And Continue on Failure	select checkbox	xpath=(//input[@type='checkbox'])[11]
	Run Keyword And Continue on Failure	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select from list by value	//*[@id="status"]	enabled
	Run Keyword And Continue on Failure	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=vlan
	Input Text	//*[@id="vlan"]	${Vlan1_id}
	Click Element	//div[@id="memberTagged"]//option[@value="backplane0"]
	Click Element	//div[@id="memberTagged"]//option[@value="${Port_host1}"]
	Click Element	//*[@id="memberTagged"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	sleep	3s
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=vlan
	Input Text	//*[@id="vlan"]	${Vlan2_id}
	Click Element	//div[@id="memberTagged"]//option[@value="backplane1"]
	Click Element	//div[@id="memberTagged"]//option[@value="${Port_host2}"]
	Click Element	//*[@id="memberTagged"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Config NG Server Connection on Host side
	SUITE:Setup
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="connName"]	${Vlan_Name2}
	Select From List By value	//*[@id="connType"]	vlan
	Select From List By value	//*[@id="connItfName"]	backplane1
	Click Element	xpath=//input[@id='method']
	click element	css=.form-group:nth-child(9) .radio:nth-child(5) > label
	wait until element is visible	css=.form-group:nth-child(6)
	Click Element	css=.form-group:nth-child(6) #address6
	sleep	5s
	Input Text	//*[@id="address6"]	${ip_v6_address_2}
	Click Element	xpath=//input[@id='netmask6']
	Input Text	xpath=//input[@id='netmask6']	${Prefix_length}
	Input Text	//*[@id="vlanId"]	${Vlan2_id}
	Click Element	//*[@id="method6"]
	wait until element is visible	//*[@id="saveButton"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Config Vlan on Hostshared Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select checkbox	xpath=(//input[@type='checkbox'])[10]
	Run Keyword And Continue on Failure	select checkbox	xpath=(//input[@type='checkbox'])[11]
	Run Keyword And Continue on Failure	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select from list by value	//*[@id="status"]	enabled
	Run Keyword And Continue on Failure	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=vlan
	Input Text	//*[@id="vlan"]	${Vlan1_id}
	Click Element	//div[@id="memberTagged"]//option[@value="backplane0"]
	Click Element	//div[@id="memberTagged"]//option[@value="${Port_hostshared1}"]
	Click Element	//*[@id="memberTagged"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	sleep	3s
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=vlan
	Input Text	//*[@id="vlan"]	${Vlan2_id}
	Click Element	//div[@id="memberTagged"]//option[@value="backplane1"]
	Click Element	//div[@id="memberTagged"]//option[@value="${Port_hostshared2}"]
	Click Element	//*[@id="memberTagged"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Config NG-Relay on Host Shared Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="connName"]	${Vlan_Name1}
	Select From List By value	//*[@id="connType"]	vlan
	Select From List By value	//*[@id="connItfName"]	backplane0
	Click Element	css=.radio:nth-child(3) #method
	Click Element	xpath=//input[@id='method']
	click element	css=.form-group:nth-child(9) .radio:nth-child(5) > label
	wait until element is visible	css=.form-group:nth-child(6)
	Click Element	css=.form-group:nth-child(6) #address6
	sleep	5s
	Input Text	//*[@id="address6"]	${ip_v6_address_1}
	Click Element	xpath=//input[@id='netmask6']
	Input Text	xpath=//input[@id='netmask6']	${Prefix_length}
	Input Text	//*[@id="vlanId"]	${Vlan1_id}
	Click Element	//*[@id="method6"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="connName"]	${Vlan_Name2}
	Select From List By value	//*[@id="connType"]	vlan
	Select From List By value	//*[@id="connItfName"]	backplane1
	Click Element	css=.radio:nth-child(3) #method
	Click Element	xpath=//input[@id='method']
	click element	css=.form-group:nth-child(9) .radio:nth-child(5) > label
	wait until element is visible	css=.form-group:nth-child(6)
	Click Element	css=.form-group:nth-child(6) #address6
	sleep	5s
	Input Text	//*[@id="address6"]	${ip_v6_Relay}
	Click Element	xpath=//input[@id='netmask6']
	Input Text	xpath=//input[@id='netmask6']	${Prefix_length}
	Input Text	//*[@id="vlanId"]	${Vlan2_id}
	Click Element	//*[@id="method6"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Config NG-Client Connection on HostSide
	SUITE:Setup
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="connName"]	${Vlan_Name1}
	Select From List By value	//*[@id="connType"]	vlan
	Select From List By value	//*[@id="connItfName"]	backplane0
	click element	xpath=//label[contains(.,'No IPv4 Address')]
	sleep	3s
	wait until element is visible	xpath=(//input[@id='method6'])[4]
	click element	xpath=(//input[@id='method6'])[4]
	Input Text	//*[@id="vlanId"]	${Vlan1_id}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	sleep	2s
	click element	xpath=//tr[@id='vlan100']/td/input
	click element	css=.btn:nth-child(4)
	GUI::Basic::Spinner Should Be Invisible
	sleep	10s

SUITE:To Add Dhcp Server on Host Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//a[@id='submenu-drop']/span[2]
	click element	xpath=//span[contains(.,'DHCP Server')]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//label[contains(.,'DHCP6')]
	input text	xpath=//input[@id='dhcpd6prefix']	${Dhcp_Prefix}
	input text	xpath=//input[@id='dhcpd6length']	${Dhcp_length}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	sleep	3s
	click element	xpath=//a[contains(text(),'2001:db8:caff::/64')]
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//span[contains(.,'Network Range')]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	input text	xpath=//input[@id='dhcpdrangelo']	${Ip_Start}
	input text	xpath=//input[@id='dhcpdrangehi']	${Ip_End}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	sleep	3s
	click element	xpath=//a[@id='submenu-drop']/span[2]
	click element	xpath=//span[contains(.,'DHCP Server')]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//input[@id='dhcpserverprotocol']
	click element	xpath=//label[contains(.,'DHCP6')]
	input text	xpath=//input[@id='dhcpd6prefix']	${Dhcp_Prefix1}
	input text	xpath=//input[@id='dhcpd6length']	${Dhcp_length}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Add Static Routes on NG-Server Host Side
	SUITE:Setup
	GUI::Basic::Network::Static Routes::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	select from list by value	xpath=//select[@id='AddconnName']	vlan200
	click element	xpath=(//input[@id='type'])[2]
	input text	xpath=//input[@id='destIp']	${Static_Destination_Ip}
	input text	xpath=//input[@id='gateway']	${Static_Destination_Gatewy_Ip}
	input text	xpath=//input[@id='destMask']	${Static_Destination_Bit_Mask}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Add Dhcp Relay on HostShared Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//a[@id='submenu-drop']/span[2]
	click element	xpath=//span[contains(.,'DHCP Relay')]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//label[contains(.,'DHCPv6')]
	input text	xpath=//input[@id='dhcp6RelayServerItfs']	${Dhcp_Relay_Server}
	input text	xpath=//input[@id='dhcp6RelayClientItfs']	${Dhcp_Relay_Client}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:TO Up the Connection on Server host side
	SUITE:Setup
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	click element	//*[@id="vlan100"]/td[1]/input
	sleep	5s
	click element	xpath=//div[@id='nonAccessControls']/input[3]
	GUI::Basic::Spinner Should Be Invisible
	sleep	70s
	${IPV6_address}=		 Get Text	xpath=//tr[@id='vlan100']
	${IPV6_address}	 @{MATCHES}=	Should Match Regexp	${IPV6_address}	(([0-9a-fA-F]{0,4}:){1,7}[0-9a-fA-F]{0,4})
	Set Suite Variable	${IPV6_address}

SUITE:To Delete Vlan on HostShared Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	select checkbox	xpath=(//input[@type='checkbox'])[10]
	select checkbox	xpath=(//input[@type='checkbox'])[11]
	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	select from list by value	//*[@id="status"]	disabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${Vlan1_id}"]/td[1]/input
	Select Checkbox	//*[@id="${Vlan2_id}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${Vlan_Name1}"]/td[1]/input
	Select Checkbox	//*[@id="${Vlan_Name2}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Delete Vlan on Host Side
	SUITE:Setup
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	select checkbox	xpath=(//input[@type='checkbox'])[10]
	select checkbox	xpath=(//input[@type='checkbox'])[11]
	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	select from list by value	//*[@id="status"]	disabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${Vlan1_id}"]/td[1]/input
	Select Checkbox	//*[@id="${Vlan2_id}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${Vlan_Name1}"]/td[1]/input
	Select Checkbox	//*[@id="${Vlan_Name2}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Delete Dhcp Server Config
	SUITE:Setup
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//a[@id='submenu-drop']/span[2]
	click element	xpath=//span[contains(.,'DHCP Server')]
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//input[@type='checkbox']
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Delete Dhcp Relay Config
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//a[@id='submenu-drop']/span[2]
	click element	xpath=//span[contains(.,'DHCP Relay')]
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//input[@type='checkbox']
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
