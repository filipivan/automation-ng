*** Settings ***
Documentation	Testing the Network:Switch:VLAN tab
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${NETWORK}	${MODEL}	${VERSION}	CHROME	FIREFOX	EXCLUDEIN4_2	DEPENDENCE_SWITCH	NON-CRITICAL	NEED-REVIEW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variable ***
${USERNAME}	admin
${PASSWORD}	admin
${Delay}	5s
${vlan_10}	10
${vlan_20}	20
${enable}	Enabled
${disable}	Disabled
${vlan_intf}	vlan_intf
${PEER_HOMEPAGE}	${HOMEPAGESHARED}
${COORD_HOMEPAGE}	${HOMEPAGE}

*** Test Cases ***
Configure Boundary value and invalid vlan-id tests and delete configured vlan
	[Tags]	EXCLUDEIN4_2
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Setup
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=vlan
	@{VALIDS}=	Create List	100
	@{INVALIDS}=	Create List	4096	a	4095	4.4	5.0	0
	GUI::Basic::Input Text::Validation	vlan	${VALIDS}	${INVALIDS}
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="switchVlanTable"]/tbody/tr[@id="100"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

Configure Vlan 10 tagged and test traffic on both sides of NSR
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Setup
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select checkbox	//*[@id="sfp0"]/td[1]/input
	Run Keyword And Continue on Failure	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select from list by value	//*[@id="status"]	enabled
	Run Keyword And Continue on Failure	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
	Click element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=vlan
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=vlan	${vlan_10}
	Click Element	//div[@id="memberTagged"]//option[@value="backplane0"]
	Click Element	//div[@id="memberTagged"]//option[@value="sfp0"]
	Click Element	//*[@id="memberTagged"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="connName"]	${vlan_intf}
	Select From List By value	//*[@id="connType"]	vlan
	Select From List By value	//*[@id="connItfName"]	backplane0
	Click Element	id=vlanId
	Input Text	id=vlanId	${vlan_10}
	Click Element	css=.radio:nth-child(3) #method
	Click Element	id=address
	Input Text	id=address	4.4.4.11
	Click Element	id=netmask
	Input Text	id=netmask	24
	Click Element	//*[@id="method6"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select checkbox	//*[@id="sfp0"]/td[1]/input
	Run Keyword And Continue on Failure	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select from list by value	//*[@id="status"]	enabled
	Run Keyword And Continue on Failure	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
	Click element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=vlan
	Input Text	id=vlan	${vlan_10}
	Click Element	//div[@id="memberTagged"]//option[@value="backplane0"]
	Click Element	//div[@id="memberTagged"]//option[@value="sfp0"]
	Click Element	//*[@id="memberTagged"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="connName"]	${vlan_intf}
	Select From List By value	//*[@id="connType"]	vlan
	Select From List By value	//*[@id="connItfName"]	backplane0
	Click Element	id=vlanId
	Input Text	id=vlanId	${vlan_10}
	Click Element	css=.radio:nth-child(3) #method
	Click Element	id=address
	Input Text	id=address	4.4.4.22
	Click Element	id=netmask
	Input Text	id=netmask	24
	Click Element	//*[@id="method6"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::System::Toolkit::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=#icon_NetworkTools
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="ipaddress"]	4.4.4.22
	Click Element	xpath=//input[@value="Ping"]
	Select From List By value	//*[@id="interface"]	backplane0.10
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	Wait Until Page Contains	ping statistics	10
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="switchVlanTable"]/tbody/tr[@id="${vlan_10}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="peerTable"]/tbody/tr[@id="${vlan_intf}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="switchVlanTable"]/tbody/tr[@id="${vlan_10}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="peerTable"]/tbody/tr[@id="${vlan_intf}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#sfp0 input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=status
	Select From List By Label	id=status	${disable}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#sfp0 input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=status
	Select From List By Label	id=status	${disable}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Configure Vlan 20 untagged and test traffic on both sides of NSR
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Setup
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select checkbox	//*[@id="sfp0"]/td[1]/input
	Run Keyword And Continue on Failure	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select from list by value	//*[@id="status"]	enabled
	Run Keyword And Continue on Failure	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=vlan
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=vlan	${vlan_20}
	Click Element	//div[@id="memberUntagged"]//option[@value="backplane0"]
	Click Element	//div[@id="memberUntagged"]//option[@value="sfp0"]
	Click Element	//*[@id="memberUntagged"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="connName"]	${vlan_intf}
	Select From List By value	//*[@id="connType"]	vlan
	Select From List By value	//*[@id="connItfName"]	backplane0
	Click Element	id=vlanId
	Input Text	id=vlanId	${vlan_20}
	Click Element	css=.radio:nth-child(3) #method
	Click Element	id=address
	Input Text	id=address	4.4.4.11
	Click Element	id=netmask
	Input Text	id=netmask	24
	Click Element	//*[@id="method6"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select checkbox	//*[@id="sfp0"]/td[1]/input
	Run Keyword And Continue on Failure	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select from list by value	//*[@id="status"]	enabled
	Run Keyword And Continue on Failure	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=vlan
	Input Text	id=vlan	${vlan_20}
	Click Element	//div[@id="memberUntagged"]//option[@value="backplane0"]
	Click Element	//div[@id="memberUntagged"]//option[@value="sfp0"]
	Click Element	//*[@id="memberUntagged"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=connName	${vlan_intf}
	Click Element	id=connType
	Input Text	//*[@id="connName"]	${vlan_intf}
	Select From List By value	//*[@id="connType"]	vlan
	Select From List By value	//*[@id="connItfName"]	backplane0
	Click Element	id=vlanId
	Input Text	id=vlanId	${vlan_20}
	Click Element	css=.radio:nth-child(3) #method
	Click Element	id=address
	Input Text	id=address	4.4.4.22
	Click Element	id=netmask
	Input Text	id=netmask	24
	Click Element	//*[@id="method6"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::System::Toolkit::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=#icon_NetworkTools
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="ipaddress"]	4.4.4.22
	Click Element	xpath=//input[@value="Ping"]
	Select From List By value	//*[@id="interface"]	backplane0.20
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	Wait Until Page Contains	ping statistics	10
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Network::Switch::open tab
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="switchVlanTable"]/tbody/tr[@id="${vlan_20}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="peerTable"]/tbody/tr[@id="${vlan_intf}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Network::Switch::open tab
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="switchVlanTable"]/tbody/tr[@id="${vlan_20}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="peerTable"]/tbody/tr[@id="${vlan_intf}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Spinner Should Be Invisible
	close all browsers