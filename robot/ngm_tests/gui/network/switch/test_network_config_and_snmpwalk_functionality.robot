*** Settings ***
Documentation	Testing the options for enabling and disabling interface in Network:switch and performing SNMPwalk
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW
Default Tags	DEPENDENCE_SWITCH	EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${COMMUNITY_NAME}	private1
${INTERFACE_DISCRIPTION}	switch_testing
${vlan_id}	100
${root_login}	shell sudo su -
${snmpwalk1}	snmpwalk -v 2c -c ${COMMUNITY_NAME} ${HOST} .1.3.6.1.2.1.4.20
${snmpwalk2}	snmpwalk -v 2c -c ${COMMUNITY_NAME} ${HOST} .1.3.6.1.2.1.2.2.1.2
${exit_session}	exit
${HOSTNAME}	nodegrid
${NETWORK_INTERFACE}	netS1-3

*** Test Cases ***
Test Enabling Status
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Setup
	Click Element	css=#${NETWORK_INTERFACE} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Clear Element Text	id=description
	Select From List By Label	id=status	Enabled
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Element Should Contain	//*[@id="${NETWORK_INTERFACE}"]/td[3]	Enabled
	SUITE:Teardown

Test Enabling jumboFrame
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Setup
	Click Element	css=#${NETWORK_INTERFACE} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	id=jumboFrame	Enabled
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Element Should Contain	//*[@id="${NETWORK_INTERFACE}"]/td[6]	Enabled
	SUITE:Teardown

Test check for configuring speed-10M
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Setup
	Click Element	css=#${NETWORK_INTERFACE} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	id=speed	10M
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Element Should Contain	//*[@id="${NETWORK_INTERFACE}"]/td[4]	10M
	SUITE:Teardown

Test check for configuring speed-100M
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Setup
	Click Element	css=#${NETWORK_INTERFACE} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	id=speed	100M
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Element Should Contain	//*[@id="${NETWORK_INTERFACE}"]/td[4]	100M
	SUITE:Teardown

Test check for configuring speed-1G
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${NETWORK_INTERFACE} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	id=speed	1G
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Element Should Contain	//*[@id="${NETWORK_INTERFACE}"]/td[4]	1G
	SUITE:Teardown

Test check for configuring speed-Auto
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Setup
	Click Element	css=#${NETWORK_INTERFACE} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	id=speed	Auto
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Element Should Contain	//*[@id="${NETWORK_INTERFACE}"]/td[4]	Auto
	SUITE:Teardown

Test Disabling jumboframe
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Setup
	Click Element	css=#${NETWORK_INTERFACE} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	id=jumboFrame	Disabled
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Element Should Contain	//*[@id="${NETWORK_INTERFACE}"]/td[6]	Disabled
	SUITE:Teardown

Test Disabling status
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Setup
	Click Element	css=#${NETWORK_INTERFACE} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	id=status	Disabled
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Element Should Contain	//*[@id="${NETWORK_INTERFACE}"]/td[3]	Disabled
	SUITE:Teardown

Test check for writing description
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Setup
	Click Element	css=#${NETWORK_INTERFACE} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=description	${INTERFACE_DISCRIPTION}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Element Text Should Be	//*[@id="${NETWORK_INTERFACE}"]/td[10]	${INTERFACE_DISCRIPTION}
	SUITE:Teardown

Test check for selecting lldp
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Setup
	Click Element	css=#${NETWORK_INTERFACE} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Clear Element Text	id=description
	Select Checkbox	id=lldp
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${NETWORK_INTERFACE} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Checkbox Should Be Selected	id=lldp
	SUITE:Teardown

Test check for unselecting lldp
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Setup
	Click Element	css=#${NETWORK_INTERFACE} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Unselect Checkbox	id=lldp
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${NETWORK_INTERFACE} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Checkbox Should Not Be Selected	id=lldp
	SUITE:Teardown

Test configuring port vlan id
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Setup
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=vlan	${vlan_id}
	Execute Javascript	document.querySelectorAll("#memberTagged select")[0].value = 'backplane0'
	Click Element	css=#memberTagged .col-md-12:nth-child(1) > .listbuilder_btn
	Execute Javascript	document.querySelectorAll("#memberTagged select")[0].value = '${NETWORK_INTERFACE}'
	Click Element	css=#memberTagged .col-md-12:nth-child(1) > .listbuilder_btn
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	Click Element	css=#${NETWORK_INTERFACE} input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=pvid	${vlan_id}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Element Should Contain	//*[@id="${NETWORK_INTERFACE}"]/td[5]	${vlan_id}
	SUITE:Teardown

Test for performing snmpwalk
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Setup
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${HOSTNAME}=	Get value	id=hostname
	GUI::Basic::Network::SNMP::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=oid	.1
	Input Text	id=community	${COMMUNITY_NAME}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="peer_header"]//a[text()="Console"]
	sleep	5
	Switch Window	title=${HOSTNAME}
	Wait Until Page Contains Element	//iframe[@id='termwindow']
	Select Frame	//iframe[@id='termwindow']
	Sleep	5
	Press Keys	None	RETURN
	Press Keys	None	RETURN

	${OUTPUT}=	SUITE:Console Command Output	${root_login}
	${OUTPUT}=	SUITE:Console Command Output	ssh ${SNMPSERVERUSER}@${SNMPSERVER}
	${OUTPUT}=	SUITE:Console Command Output	yes
	sleep	5s
	${OUTPUT}=	SUITE:Console Command Output	${SNMPSERVERPASSWORD}
	${OUTPUT}=	SUITE:Console Command Output	${snmpwalk1}
	sleep	5s
	${OUTPUT}=	SUITE:Console Command Output	${snmpwalk2}
	Unselect Frame
	${OUTPUT}=	SUITE:Console Command Output	${exit_session}
	${OUTPUT}=	SUITE:Console Command Output	${exit_session}
	${OUTPUT}=	SUITE:Console Command Output	${exit_session}
	Switch Browser	1
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	${VLAN_ID} =	Create List	${vlan_id}
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table If Exists Without Alert	jquery=table\#switchVlanTable	${VLAN_ID}
	GUI::Basic::Network::SNMP::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="community|${COMMUNITY_NAME}~default"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	Close All Browsers