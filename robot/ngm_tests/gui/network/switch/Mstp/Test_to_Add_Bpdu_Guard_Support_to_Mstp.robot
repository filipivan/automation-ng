*** Settings ***
Documentation	Test to Add Bpdu Guard Support to MSTP Feature
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2

*** Variables ***
${COORD_HOMEPAGE}	${HOMEPAGE}
${PEER_HOMEPAGE}	${HOMEPAGESHARED}
${Vlan_id}	10
${Port_host}	${SWITCH_SFP_HOST_INTERFACE1}
${Port_host_shared}	${SWITCH_SFP_SHARED_INTERFACE1}
${Vlan_Name}	vlan10
${ip_address_1}	10.1.1.31
${ip_address_2}	10.1.1.33
${net_mask}	24

*** Test Cases ***
Test Case to Config vlan on Both Sides
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:To Configure Vlan on Host Side
	SUITE:To Configure Vlan on Host Shared Side

Test Case to Enable Mstp on Global Page
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:To Enable Stp and set mode to MSTP on Global Page on Both Sides
	SUITE:To Check Ping Status

Test Case to Set MSTP Status and BPDU Guard on Host Side
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:To Enable Mstp Status and BPDU Guard on Host Side
	SUITE:To Check Ping Status
	SUITE:To Enable Mstp Status on Host Shared Side

Test Case to Check Bpdu Guard Status
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:To Check the Bpdu Guard Status on Tracking Page
	SUITE:To Check Ping Status Should not Work

Test Case to Delete Config
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:To Disable Mstp Status on Host Shared Side
	SUITE:To Disable Mstp Status, BPDU Guard and Interface on Host Side
	SUITE:To Disable Stp on Global Page on Both Sides
	SUITE:To Delete Vlan Config on Both Sides

*** Keywords ***
SUITE:Setup
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:To Configure Vlan on Host Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
#	To enable interface
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select checkbox	//*[@id="sfp0"]/td[1]/input
	Run Keyword And Continue on Failure	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select from list by value	//*[@id="status"]	enabled
	Run Keyword And Continue on Failure	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
#	To Add Vlan
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=vlan
	Input Text	//*[@id="vlan"]	${Vlan_id}
	Click Element	//div[@id="memberTagged"]//option[@value="backplane0"]
	Click Element	//div[@id="memberTagged"]//option[@value="${Port_host}"]
	Click Element	//*[@id="memberTagged"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	sleep	3s
#	To Add Network Connection
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="connName"]	${Vlan_Name}
	Select From List By value	//*[@id="connType"]	vlan
	Select From List By value	//*[@id="connItfName"]	backplane0
	Click Element	css=.radio:nth-child(3) #method
	Click Element	id=address
	Input Text	id=address	${ip_address_1}
	Click Element	id=netmask
	Input Text	id=netmask	${net_mask}
	Input Text	//*[@id="vlanId"]	${Vlan_id}
	Click Element	//*[@id="method6"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s

SUITE:To Configure Vlan on Host Shared Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
#	To enable interface
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select checkbox	//*[@id="sfp1"]/td[1]/input
	Run Keyword And Continue on Failure	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select from list by value	//*[@id="status"]	enabled
	Run Keyword And Continue on Failure	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
#	To Add Vlan
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=vlan
	Input Text	//*[@id="vlan"]	${Vlan_id}
	Click Element	//div[@id="memberTagged"]//option[@value="backplane0"]
	Click Element	//div[@id="memberTagged"]//option[@value="${Port_host_shared}"]
	Click Element	//*[@id="memberTagged"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	sleep	3s
#	To Add Network Connection
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="connName"]	${Vlan_Name}
	Select From List By value	//*[@id="connType"]	vlan
	Select From List By value	//*[@id="connItfName"]	backplane0
	Click Element	css=.radio:nth-child(3) #method
	Click Element	id=address
	Input Text	id=address	${ip_address_2}
	Click Element	id=netmask
	Input Text	id=netmask	${net_mask}
	Input Text	//*[@id="vlanId"]	${Vlan_id}
	Click Element	//*[@id="method6"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s

SUITE:To Enable Stp and set mode to MSTP on Global Page on Both Sides
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="netswitchGlobal"]/span
	GUI::Basic::Spinner Should Be Invisible
	Select From List By value	//*[@id="stp_status"]	enabled
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	//*[@id="stp_mode"]	mstp
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="netswitchGlobal"]/span
	GUI::Basic::Spinner Should Be Invisible
	Select From List By value	//*[@id="stp_status"]	enabled
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	//*[@id="stp_mode"]	mstp
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Check Ping Status
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	click element	css=.submenu li:nth-child(5) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="icon_NetworkTools"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=.fixed-top
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="ipaddress"]	${ip_address_2}
	Select From List By value	//*[@id="interface"]	backplane0.10
	Click Element	xpath=//input[@value="Ping"]
	GUI::Basic::Spinner Should Be Invisible
	sleep	15s
	Wait Until Page Contains	ping statistics
	page should not contain	100% packet loss,
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Enable Mstp Status and BPDU Guard on Host Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
#	To enable mstp and bpdu guard on host interface
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select checkbox	//*[@id="sfp0"]/td[1]/input
	Run Keyword And Continue on Failure	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	//*[@id="rstpStatus"]	enabled
	Select From List By Value	//*[@id="rstpBpdu"]	on
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Enable Mstp Status on Host Shared Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
#	To enable mstp status on host shared interface
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select checkbox	//*[@id="sfp1"]/td[1]/input
	Run Keyword And Continue on Failure	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	//*[@id="rstpStatus"]	enabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Check the Bpdu Guard Status on Tracking Page
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Tracking::Open Network Tab
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//span[contains(.,'MSTP')]
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=(//a[contains(text(),'0')])[2]
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	page should contain element	xpath=//td[contains(.,'Discarding')]
	page should contain	Disabled (BPDU Guard)

SUITE:To Check Ping Status Should not Work
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	click element	css=.submenu li:nth-child(5) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="icon_NetworkTools"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=.fixed-top
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="ipaddress"]	${ip_address_2}
	Select From List By value	//*[@id="interface"]	backplane0.10
	Click Element	xpath=//input[@value="Ping"]
	GUI::Basic::Spinner Should Be Invisible
	sleep	15s
	Wait Until Page Contains	ping statistics
	page should contain	100% packet loss,
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Disable Mstp Status on Host Shared Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
#	To enable mstp status on host shared interface
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select checkbox	//*[@id="sfp1"]/td[1]/input
	Run Keyword And Continue on Failure	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	//*[@id="rstpStatus"]	disabled
	select from list by value	//*[@id="status"]	disabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Disable Mstp Status, BPDU Guard and Interface on Host Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
#	To enable mstp and bpdu guard on host interface
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select checkbox	//*[@id="sfp0"]/td[1]/input
	Run Keyword And Continue on Failure	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	//*[@id="rstpStatus"]	disabled
	Select From List By Value	//*[@id="rstpBpdu"]	off
	select from list by value	//*[@id="status"]	disabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Disable Stp on Global Page on Both Sides
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="netswitchGlobal"]/span
	GUI::Basic::Spinner Should Be Invisible
	Select From List By value	//*[@id="stp_status"]	disabled
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="netswitchGlobal"]/span
	GUI::Basic::Spinner Should Be Invisible
	Select From List By value	//*[@id="stp_status"]	disabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Delete Vlan Config on Both Sides
#	To Delete on Host Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${Vlan_Name}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${Vlan_id}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
#	To Delete on Host Shared Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${Vlan_Name}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${Vlan_id}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
