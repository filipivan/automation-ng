*** Settings ***
Documentation	Test to Config LAG With Static Mode
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2

*** Variables ***
${COORD_HOMEPAGE}	${HOMEPAGE}
${PEER_HOMEPAGE}	${HOMEPAGESHARED}
${Vlan_id}	10
${Port_host}	${SWITCH_NETPORT_HOST_INTERFACE1}
${Port_host_shared}	${SWITCH_NETPORT_SHARED_INTERFACE1}
${Port_host2}	${SWITCH_NETPORT_HOST_INTERFACE2}
${Port_host_shared2}	${SWITCH_NETPORT_SHARED_INTERFACE2}
${Vlan_Name}	vlan10
${ip_address_1}	10.1.1.33
${ip_address_2}	10.1.1.32
${net_mask}	24
${Type}	vlan
${Interface}	backplane0
${Lag_Name}	lag

*** Test Cases ***
Test Case to Config vlan on Both Sides
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:To Configure Vlan on Host Side
	SUITE:To Configure Vlan on Host Shared Side

Test Case to Add Lag with Static Mode and Check Ping Status
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:To Add Lag with Static Mode
	SUITE:To Check Ping Status

Test Case to Set the Load balance and Check Ping Status
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:To Set Load balance for Source and destination IP
	SUITE:To Check Ping Status
	SUITE:To Set Load balance for Source and destination Mac
	SUITE:To Check Ping Status
	SUITE:To Set Load balance for Source and Destination MAC and IP
	SUITE:To Check Ping Status
	SUITE:To Set Load balance for Source and Destination MAC and IP and TCP/UDP Ports
	SUITE:To Check Ping Status

Test Case to Disable one of the Interface and Check Ping Status
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:To Disable one of the Interface
	SUITE:To Check Ping Status

Test Case to Set Default Config
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:To Set Load balance for Source and destination Mac
	SUITE:To Delete Lag Config
	SUITE:To Delete Vlan Config on Both Sides

*** Keywords ***
SUITE:Setup
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:To Configure Vlan on Host Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
#	To enable interface
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select checkbox	//*[@id="${Port_host}"]/td[1]/input
	Run Keyword And Continue on Failure	select checkbox	//*[@id="${Port_host2}"]/td[1]/input
	Run Keyword And Continue on Failure	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select from list by value	//*[@id="status"]	enabled
	Run Keyword And Continue on Failure	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
#	To Add Vlan
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=vlan
	Input Text	//*[@id="vlan"]	${Vlan_id}
	Click Element	//div[@id="memberTagged"]//option[@value="backplane0"]
	Click Element	//*[@id="memberTagged"]/div/div[1]/div[2]/div/div[1]/button
	Click Element	//div[@id="memberTagged"]//option[@value="${Port_host}"]
	Click Element	//*[@id="memberTagged"]/div/div[1]/div[2]/div/div[1]/button
	Click Element	//div[@id="memberTagged"]//option[@value="${Port_host2}"]
	Click Element	//*[@id="memberTagged"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	sleep	3s
#	To Add Network Connection
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="connName"]	${Vlan_Name}
	Select From List By value	//*[@id="connType"]	${Type}
	Select From List By value	//*[@id="connItfName"]	${Interface}
	Click Element	css=.radio:nth-child(3) #method
	Click Element	id=address
	Input Text	id=address	${ip_address_1}
	Click Element	id=netmask
	Input Text	id=netmask	${net_mask}
	Input Text	//*[@id="vlanId"]	${Vlan_id}
	Click Element	//*[@id="method6"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s

SUITE:To Configure Vlan on Host Shared Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
#	To enable interface
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select checkbox	//*[@id="${Port_host_shared}"]/td[1]/input
	Run Keyword And Continue on Failure	select checkbox	//*[@id="${Port_host_shared2}"]/td[1]/input
	Run Keyword And Continue on Failure	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select from list by value	//*[@id="status"]	enabled
	Run Keyword And Continue on Failure	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
#	To Add Vlan
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=vlan
	Input Text	//*[@id="vlan"]	${Vlan_id}
	Click Element	//div[@id="memberTagged"]//option[@value="backplane0"]
	Click Element	//*[@id="memberTagged"]/div/div[1]/div[2]/div/div[1]/button
	Click Element	//div[@id="memberTagged"]//option[@value="${Port_host_shared}"]
	Click Element	//*[@id="memberTagged"]/div/div[1]/div[2]/div/div[1]/button
	Click Element	//div[@id="memberTagged"]//option[@value="${Port_host_shared2}"]
	Click Element	//*[@id="memberTagged"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	sleep	3s
#	To Add Network Connection
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="connName"]	${Vlan_Name}
	Select From List By value	//*[@id="connType"]	${Type}
	Select From List By value	//*[@id="connItfName"]	${Interface}
	Click Element	css=.radio:nth-child(3) #method
	Click Element	id=address
	Input Text	id=address	${ip_address_2}
	Click Element	id=netmask
	Input Text	id=netmask	${net_mask}
	Input Text	//*[@id="vlanId"]	${Vlan_id}
	Click Element	//*[@id="method6"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s

SUITE:To Add Lag with Static Mode
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//span[contains(.,'LAG')]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//input[@id='lagName']
	input text	xpath=//input[@id='lagName']	${Lag_Name}
	click element	xpath=//label[contains(.,'Static')]
	wait until element is visible	//*[@id="lagPorts"]/div/div[1]/div[1]/select/option[9]
	Click Element	//div[@id="lagPorts"]//option[@value="${Port_host}"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	Click Element	//div[@id="lagPorts"]//option[@value="${Port_host2}"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
#	To Add Lag on Host Shared Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//span[contains(.,'LAG')]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//input[@id='lagName']
	input text	xpath=//input[@id='lagName']	${Lag_Name}
	click element	xpath=//label[contains(.,'Static')]
	wait until element is visible	//*[@id="lagPorts"]/div/div[1]/div[1]/select/option[9]
	Click Element	//div[@id="lagPorts"]//option[@value="${Port_host_shared}"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	Click Element	//div[@id="lagPorts"]//option[@value="${Port_host_shared2}"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s

SUITE:To Check Ping Status
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	click element	css=.submenu li:nth-child(5) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="icon_NetworkTools"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=.fixed-top
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="ipaddress"]	${ip_address_2}
	Select From List By value	//*[@id="interface"]	backplane0.10
	Click Element	xpath=//input[@value="Ping"]
	GUI::Basic::Spinner Should Be Invisible
	sleep	15s
	Wait Until Page Contains	ping statistics
	page should not contain	100% packet loss,
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Set Load balance for Source and destination IP
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	click element	//*[@id="netswitchGlobal"]/span
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible	//*[@id="load_balance"]
	select from list by label	id=load_balance	Source and Destination IP
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
#	To Set on Host Shared Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	click element	//*[@id="netswitchGlobal"]/span
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible	//*[@id="load_balance"]
	select from list by label	id=load_balance	Source and Destination IP
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s

SUITE:To Set Load balance for Source and destination Mac
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	click element	//*[@id="netswitchGlobal"]/span
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible	//*[@id="load_balance"]
	select from list by label	id=load_balance	Source and Destination MAC
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
#	To Set on Host Shared Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	click element	//*[@id="netswitchGlobal"]/span
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible	//*[@id="load_balance"]
	select from list by label	id=load_balance	Source and Destination MAC
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s

SUITE:To Set Load balance for Source and Destination MAC and IP
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	click element	//*[@id="netswitchGlobal"]/span
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible	//*[@id="load_balance"]
	select from list by label	id=load_balance	Source and Destination MAC and IP
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
#	To Set on Host Shared Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	click element	//*[@id="netswitchGlobal"]/span
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible	//*[@id="load_balance"]
	select from list by label	id=load_balance	Source and Destination MAC and IP
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s

SUITE:To Set Load balance for Source and Destination MAC and IP and TCP/UDP Ports
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	click element	//*[@id="netswitchGlobal"]/span
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible	//*[@id="load_balance"]
	select from list by label	id=load_balance	Source and Destination MAC and IP and TCP/UDP Ports
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
#	To Set on Host Shared Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	click element	//*[@id="netswitchGlobal"]/span
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible	//*[@id="load_balance"]
	select from list by label	id=load_balance	Source and Destination MAC and IP and TCP/UDP Ports
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s

SUITE:To Disable one of the Interface
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	select checkbox	//*[@id="${Port_host2}"]/td[1]/input
	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	select from list by value	//*[@id="status"]	disabled
	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Delete Lag Config
#	To Delete on Host Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//span[contains(.,'LAG')]
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//input[@type='checkbox']
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
#	To Delete on Host Shared Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//span[contains(.,'LAG')]
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//input[@type='checkbox']
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Delete Vlan Config on Both Sides
#	To Delete on Host Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${Vlan_Name}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${Vlan_id}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	select checkbox	//*[@id="${Port_host}"]/td[1]/input
	select checkbox	//*[@id="${Port_host2}"]/td[1]/input
	Run Keyword And Continue on Failure	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	select from list by value	//*[@id="status"]	disabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
#	To Delete on Host Shared Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${Vlan_Name}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${Vlan_id}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	select checkbox	//*[@id="${Port_host_shared}"]/td[1]/input
	select checkbox	//*[@id="${Port_host_shared2}"]/td[1]/input
	Run Keyword And Continue on Failure	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	select from list by value	//*[@id="status"]	disabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
