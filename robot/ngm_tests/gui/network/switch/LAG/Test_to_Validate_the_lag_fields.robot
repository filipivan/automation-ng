*** Settings ***
Documentation	Test to validate lag and lacp fields
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	DEPENDENCE_SWITCH	NON-CRITICAL	BUG_NG_12073
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2

*** Variables ***
${Port_host}	${SWITCH_NETPORT_HOST_INTERFACE1}
${Lag_Name}	lag
${Lag_Name1}	test
${Lag_Id}	112
${System_Proirity}	65536

*** Test Cases ***
Test Case to validate Static mode with backplane and netports
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:To Add lag with Static mode
	SUITE:To Add backplane with netports
	page should contain	LAG with backplane0 and backplane1 cannot be aggregated with any other port.
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible

Test Case to validate Static mode with sfp's and netports
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:To Add lag with Static mode
	SUITE:To Add lag with sfp's and netports
	page should contain	LAG with SFP0 and SFP1 cannot be aggregated with any other port.
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible

Test Case to add the port more than eight
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:To Add lag with Static mode
	SUITE:To Add more than eight ports
	page should contain	Exceeded number of ports. Maximum number of ports is 8.
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible

Test Case to add port to morethan one lag
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	NON-CRITICAL	NEED-REVIEW
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:To Add lag with Static mode
	SUITE:To Add the port to more than one lag
	page should contain	Port cannot be added to more than one LAG.
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible
	SUITE:To Delete Lag

Test Case to Check High System Priority and ID value
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	NON-CRITICAL	NEED-REVIEW
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:To Check lacp with high system priority and id value
	page should contain	The value must be between 1 and 110.
	page should contain	The value must be between 1 and 65535.
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible

Test Case to Check the backplane0 & backplane1 with lacp
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	NON-CRITICAL	NEED-REVIEW
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Test Case to Check the backplane0 & backplane1 with lacp
	page should contain	LAG with backplane0 and backplane1 cannot be aggregated with any other port.
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:To Add lag with Static mode
	SUITE:Setup
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//span[contains(.,'LAG')]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible	xpath=//label[contains(.,'Static')]
	click element	xpath=//label[contains(.,'Static')]
	click element	xpath=//input[@id='lagName']
	input text	xpath=//input[@id='lagName']	${Lag_Name}

SUITE:To Add lag with sfp's and netports
	Click Element	//div[@id="lagPorts"]//option[@value="sfp0"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	Click Element	//div[@id="lagPorts"]//option[@value="sfp1"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	Click Element	//div[@id="lagPorts"]//option[@value="${Port_host}"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	sleep	3s
	page should contain	Error on page. Please check.

SUITE:To Add backplane with netports
	Click Element	//div[@id="lagPorts"]//option[@value="backplane0"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	Click Element	//div[@id="lagPorts"]//option[@value="backplane1"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	Click Element	//div[@id="lagPorts"]//option[@value="${Port_host}"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	sleep	3s
	page should contain	Error on page. Please check.

SUITE:To Add more than eight ports
	Click Element	//div[@id="lagPorts"]//option[@value="netS1-1"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	Click Element	//div[@id="lagPorts"]//option[@value="netS1-2"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	Click Element	//div[@id="lagPorts"]//option[@value="netS1-3"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	Click Element	//div[@id="lagPorts"]//option[@value="netS1-4"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	Click Element	//div[@id="lagPorts"]//option[@value="netS1-5"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	Click Element	//div[@id="lagPorts"]//option[@value="netS1-6"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	Click Element	//div[@id="lagPorts"]//option[@value="netS1-7"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	Click Element	//div[@id="lagPorts"]//option[@value="netS1-8"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	Click Element	//div[@id="lagPorts"]//option[@value="netS1-9"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	sleep	3s
	page should contain	Error on page. Please check.

SUITE:To Add the port to more than one lag
	Click Element	//div[@id="lagPorts"]//option[@value="netS1-1"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	Click Element	//div[@id="lagPorts"]//option[@value="netS1-2"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	sleep	3s
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//span[contains(.,'LAG')]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible	xpath=//label[contains(.,'Static')]
	click element	xpath=//label[contains(.,'Static')]
	click element	xpath=//input[@id='lagName']
	input text	xpath=//input[@id='lagName']	${Lag_Name1}
	Click Element	//div[@id="lagPorts"]//option[@value="netS1-1"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	Click Element	//div[@id="lagPorts"]//option[@value="netS1-2"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	sleep	3s
	page should contain	Error on page. Please check.

SUITE:To Delete Lag
	click element	xpath=(//input[@type='checkbox'])[2]
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Check lacp with high system priority and id value
	SUITE:Setup
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//span[contains(.,'LAG')]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//input[@id='lagName']
	input text	xpath=//input[@id='lagName']	${Lag_Name}
	input text	//*[@id="lagId"]	${Lag_Id}
	click element	xpath=(//input[@id='lagType'])[2]
	click element	xpath=//input[@id='lagSysPrior']
	input text	xpath=//input[@id='lagSysPrior']	${System_Proirity}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	sleep	3s
	page should contain	Error on page. Please check.

SUITE:Test Case to Check the backplane0 & backplane1 with lacp
	SUITE:Setup
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//span[contains(.,'LAG')]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//input[@id='lagName']
	input text	xpath=//input[@id='lagName']	${Lag_Name}
	click element	xpath=(//input[@id='lagType'])[2]
	Click Element	//div[@id="lagPorts"]//option[@value="backplane0"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	Click Element	//div[@id="lagPorts"]//option[@value="backplane1"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	Click Element	//div[@id="lagPorts"]//option[@value="${Port_host}"]
	Click Element	//*[@id="lagPorts"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	sleep	3s
	page should contain	Error on page. Please check.