*** Settings ***
Documentation	Testing the Network:Switch:Mstp
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${NETWORK}	${MODEL}	${VERSION}	CHROME	FIREFOX	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
...	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	DEPENDENCE_SWITCH	NON-CRITICAL	NEED-REVIEW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variable ***
${USERNAME}	admin
${PASSWORD}	admin
${Delay}	5s
${vlan_10}	10
${vlan_20}	20
${enable}	Enabled
${disable}	Disabled
${vlan_intf}	vlan_intf

*** Test Cases ***
Enabled STP Globally and mode to MSTP in Network :: Switch :: Global.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	Click Element	//*[@id="netswitchGlobal"]/span
	GUI::Basic::Spinner Should Be Invisible
	Select From List By value	//*[@id="stp_status"]	enabled
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Enabled
	GUI::Basic::Spinner Should Be Invisible
	Select From List By value	//*[@id="stp_status"]	disabled
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Disabled
	GUI::Basic::Spinner Should Be Invisible

Configure Hello timer with boundary values(1-10) and check configurations.
	[Tags]	BUG_NG_7584	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
...	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	DEPENDENCE_SWITCH	NON-CRITICAL	NEED-REVIEW
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	gui::auditing::auto input tests	stp_hello	4096	a	-1	11	5.0	0	4
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=stp_hello	2
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Configure forward delay timer with boundary values(4-30) and check configurations.
	[Tags]	BUG_NG_7584	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
...	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	DEPENDENCE_SWITCH	NON-CRITICAL	NEED-REVIEW
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	Click Element	id=stp_delay
	@{VALIDS}=	Create List	4
	@{INVALIDS}=	Create List	4096	a	-1	41	5.0	0
	GUI::Basic::Input Text::Validation	stp_delay	${VALIDS}	${INVALIDS}
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=stp_delay	15
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Configure max age timer with boundary values(6-40) and check configurations.
	[Tags]	BUG_NG_7584	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
...	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	DEPENDENCE_SWITCH	NON-CRITICAL	NEED-REVIEW
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	Click Element	id=stp_age
	@{VALIDS}=	Create List	4
	@{INVALIDS}=	Create List	4096	a	-1	41	5.0	10
	GUI::Basic::Input Text::Validation	stp_age	${VALIDS}	${INVALIDS}
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=stp_age	20
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Configure Transmit Hold Count timer with boundary values(1-10) and check configurations.
	[Tags]	BUG_NG_7584	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
...	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	DEPENDENCE_SWITCH	NON-CRITICAL	NEED-REVIEW
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	Click Element	id=stp_count
	@{VALIDS}=	Create List	4
	@{INVALIDS}=	Create List	4096	a	-1	41	5.0	1
	GUI::Basic::Input Text::Validation	stp_count	${VALIDS}	${INVALIDS}
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=stp_count	5
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Configure Different region names.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=mstp_region	45454
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=mstp_region	asadsafds
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=mstp_region	avb213243kdsj
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Configure different revisions.
	[Tags]	BUG_NG_7584	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
...	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	DEPENDENCE_SWITCH	NON-CRITICAL	NEED-REVIEW
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	Click Element	id=mstp_revision
	@{VALIDS}=	Create List	4
	@{INVALIDS}=	Create List	a	-1	0.1	1
	GUI::Basic::Input Text::Validation	mstp_revision	${VALIDS}	${INVALIDS}
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=mstp_revision	0
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Enable STP globally and Configure MSTP instances 1-7 in Network :: Switch :: MSTP.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Spinner Should Be Invisible
	Select From List By value	//*[@id="stp_status"]	enabled
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="netswitchStp"]/span
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=mstpInstance	1
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=mstpInstance	2
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=mstpInstance	3
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=mstpInstance	4
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=mstpInstance	5
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=mstpInstance	6
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=mstpInstance	7
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Delete MSTP instances 2-7 in Network :: Switch :: MSTP.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	Click Element	//*[@id="2"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="3"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="4"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="5"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="6"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="7"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

Configure MSTP instance 1 with vlans and default priority.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	Click Element	//*[@id="1"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="editButton"]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=mstpVlan	1
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

check boundary value test for priority(0-61440 such that its multiples 4096).
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	Click Element	//div[@id="mstpPriority"]//option[@value="0"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@id="mstpPriority"]//option[@value="61440"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@id="mstpPriority"]//option[@value="45056"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@id="mstpPriority"]//option[@value="4096"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Enabled MSTP status on switch interface in Network :: Switch :: Switch Interfaces.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="netswitchItfs"]/span
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="sfp0"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@id="rstpStatus"]//option[@value="enabled"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Disable MSTP status on switch interface in Network :: Switch :: Switch Interfaces.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="netswitchItfs"]/span
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="sfp0"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@id="rstpStatus"]//option[@value="disabled"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Enabled BPDU status on switch interface in Network :: Switch :: Switch Interfaces.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="netswitchItfs"]/span
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="sfp0"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@id="rstpBpdu"]//option[@value="on"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Disable BPDU status on switch interface in Network :: Switch :: Switch Interfaces.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="netswitchItfs"]/span
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="sfp0"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@id="rstpBpdu"]//option[@value="off"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="netswitchStp"]/span
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="1"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout and Close Nodegrid