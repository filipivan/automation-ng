*** Settings ***
Documentation	Testing the options for switching configuration
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW
Default Tags	DEPENDENCE_SWITCH	EXCLUDEIN4_2
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${INTERFACE1}	netS1-1
${INTERFACE2}	netS1-1
${VLAN_ID}	10
${CONNECTION_NAME}	VLAN_10
${CONNECTION_TYPE}	VLAN
${speed1}	1G
${speed2}	10M
${speed3}	100M
${speed4}	Auto
${interface_description}	switching_configuration_1
${backplane}	backplane0
${interface_ip1}	3.3.3.11
${interface_ip2}	3.3.3.12
${data_tx_msg}	5 packets transmitted, 5 received, 0% packet loss

*** Test Cases ***
Test case for Enable the interface via GUI
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Setup
	Click Element	css=#${INTERFACE1} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=status
	Select From List By Label	id=status	Enabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${INTERFACE1} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Enabled
	SUITE:Teardown

Test case for Disable the interface via GUI
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${INTERFACE1} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=status
	Select From List By Label	id=status	Disabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${INTERFACE1} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Disabled
	SUITE:Teardown

Test case to Configure some description via GUI
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${INTERFACE1} input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=description
	Input Text	id=description	${interface_description}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Setup
	Page Should Contain	${interface_description}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${INTERFACE1} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=description
	Clear Element Text	id=description
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Setup
	Page Should Not Contain	${interface_description}
	SUITE:Teardown

Test case to Configure and verify the speed of the interface (10M,100M,1G & 10G)
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${INTERFACE1} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=speed
	Select From List By Label	id=speed	${speed1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Setup
	Page Should Contain	${speed1}
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${INTERFACE1} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=speed
	Select From List By Label	id=speed	${speed2}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Setup
	Page Should Contain	${speed2}
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${INTERFACE1} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=speed
	Select From List By Label	id=speed	${speed3}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Setup
	Page Should Contain	${speed3}
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${INTERFACE1} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=speed
	Select From List By Label	id=speed	${speed4}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Setup
	Page Should Contain	${speed4}
	SUITE:Teardown

Tast case to Enable/Disable Jumbo frame via GUI
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Setup
	Click Element	css=#${INTERFACE1} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	id=jumboFrame	Enabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Setup
	Click Element	css=#${INTERFACE1} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	id=jumboFrame	Disabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Teardown

Test Case to Enable/Disable lldp on the interface via GUI
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Setup
	Click Element	css=#${INTERFACE1} input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Clear Element Text	id=description
	Select Checkbox	id=lldp
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	Click Element	css=#${INTERFACE1} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Checkbox Should Be Selected	id=lldp
	SUITE:Setup
	Click Element	css=#${INTERFACE1} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Unselect Checkbox	id=lldp
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	Click Element	css=#${INTERFACE1} input
	GUI::Basic::Edit
	Checkbox Should Not Be Selected	id=lldp
	SUITE:Teardown

Test case to configure vlan at s1
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Setup
	Click Element	css=#${INTERFACE1} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=status
	Select From List By Label	id=status	Enabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${INTERFACE1} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Enabled
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Vlan configuration at switch 1
	SUITE:Teardown

Test case to configure vlan at s2
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Open NodeGrid	${HOMEPAGEB}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login	${USERNAME}	${PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${INTERFACE1} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=status
	Select From List By Label	id=status	Enabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=vlan	${vlan_id}
	Execute Javascript	document.querySelectorAll("#memberTagged select")[0].value = '${backplane}'
	Click Element	css=#memberTagged .col-md-12:nth-child(1) > .listbuilder_btn
	Execute Javascript	document.querySelectorAll("#memberTagged select")[0].value = '${INTERFACE2}'
	Click Element	css=#memberTagged .col-md-12:nth-child(1) > .listbuilder_btn
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${INTERFACE2} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=pvid
	Clear Element Text	id=pvid
	Input Text	id=pvid	${VLAN_ID}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Element Should Contain	//*[@id="${INTERFACE2}"]/td[5]	${VLAN_ID}
	GUI::Basic::Open NodeGrid	${HOMEPAGEB}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login	${USERNAME}	${PASSWORD}
	GUI::Basic::Network::Connections::Open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=connName	${CONNECTION_NAME}
	Click Element	id=connType
	Select From List By Label	id=connType	${CONNECTION_TYPE}
	Click Element	id=connItfName
	Select From List By Label	jquery=#connItfName	${backplane}
	Click Element	id=vlanId
	Input Text	id=vlanId	${VLAN_ID}
	Click Element	css=.radio:nth-child(3) #method
	Click Element	id=address
	Input Text	id=address	${interface_ip2}
	Click Element	id=netmask
	Input Text	id=netmask	24
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open NodeGrid	${HOMEPAGEB}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login	${USERNAME}	${PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${CONNECTION_NAME}

Test case to Send traffic via the port and check for RX/TX statistics
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Open NodeGrid	${HOMEPAGEB}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login	${USERNAME}	${PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Toolkit::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="icon_NetworkTools"]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="ipaddress"]	${interface_ip1}
	Click Element	//*[@id="networktools"]/div[2]/div[1]/div/input[1]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${data_tx_msg}
	SUITE:Teardown

Test case to delete vlan connections
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Network::connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${CONNECTION_NAME}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should not Contain	${CONNECTION_NAME}
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="10"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${INTERFACE1} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=status
	Select From List By Label	id=status	Disabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open NodeGrid	${HOMEPAGEB}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${CONNECTION_NAME}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should not Contain	${CONNECTION_NAME}
	GUI::Basic::Open NodeGrid	${HOMEPAGEB}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="10"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${INTERFACE1} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=status
	Select From List By Label	id=status	Disabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

*** Keywords ***
SUITE:Setup
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Setup 2
	SUITE:Setup2
	${HOMEPAGE}=	Set Variable	${HOMEPAGEB}
	${USERNAME}=	Set Variable	admin
	${PASSWORD}=	Set Variable	admin
	GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login	${USERNAME}	${PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible

 SUITE:Vlan configuration at switch 1
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=vlan	${vlan_id}
	Execute Javascript	document.querySelectorAll("#memberTagged select")[0].value = '${backplane}'
	Click Element	css=#memberTagged .col-md-12:nth-child(1) > .listbuilder_btn
	Execute Javascript	document.querySelectorAll("#memberTagged select")[0].value = '${INTERFACE1}'
	Click Element	css=#memberTagged .col-md-12:nth-child(1) > .listbuilder_btn
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${INTERFACE1} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=pvid
	Clear Element Text	id=pvid
	Input Text	id=pvid	${VLAN_ID}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Element Should Contain	//*[@id="${INTERFACE1}"]/td[5]	${VLAN_ID}
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=connName	${CONNECTION_NAME}
	Click Element	id=connType
	Select From List By Label	id=connType	${CONNECTION_TYPE}
	Click Element	id=connItfName
	Select From List By Label	jquery=#connItfName	${backplane}
	Click Element	id=vlanId
	Input Text	id=vlanId	${VLAN_ID}
	Click Element	css=.radio:nth-child(3) #method
	Click Element	id=address
	Input Text	id=address	${interface_ip1}
	Click Element	id=netmask
	Input Text	id=netmask	24
	GUI::Basic::Save
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${CONNECTION_NAME}

SUITE:Teardown
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid







