*** Settings ***
Documentation	Testing the ACL functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW
Default Tags	NO-ENV	EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${acl_name1}	acl_1
${acl_name2}	acl_2
${src_mac1}	ff:ff:ff:ff:ff:ff/32
${dst_mac1}	bb:bb:45:66:77:67/32
${src_mac2}	12:54:bc:aa:aa:aa/16
${dst_mac2}	aa:aa:45:66:77:67/16
${src_ip1}	2.2.2.2
${dst_ip1}	221.1.1.1
${src_ip2}	3.3.3.3
${dst_ip2}	21.1.1.1
${ip_1}	4.4.4.11
${ip_2}	4.4.4.22
${vlan_5}	5
${vlan_10}	10
${USERNAME}	admin
${PASSWORD}	admin
${Delay}	5s
${vlan_20}	20
${enable}	Enabled
${disable}	Disabled
${vlan_intf}	vlan_intf

*** Test Cases ***
Configure ACL ACL1 with ingress direction on S1
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Topo_setup
	switch browser	2
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchAcl > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=aclName	${acl_name1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="${acl_name1}"]
	GUI::Basic::Spinner Should Be Invisible

Assign ACL on S2-S1 interface and send traffic for S1.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${INTERFACEB3_3} input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=status
	Select From List By Label	id=ingress_acl	${acl_name1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	switch browser	1
	SUITE:Pass_Ping

Configure rule 0 with permit and src mac(mac address of S1) and send traffic from S1.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	switch browser	2
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchAcl > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="${acl_name1}"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=ruleSourceMac	${src_mac1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	switch browser	1
	SUITE:Pass_Ping

Delete rule 0
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	switch browser	2
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchAcl > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="${acl_name1}"]
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="0"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	switch browser	1
	SUITE:Pass_Ping

Add new rule 0 with deny and src mac(mac address of S1) and send traffic from S1.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	switch browser	2
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchAcl > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="${acl_name1}"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	id=ruleAction		deny
	Input Text	id=ruleSourceMac	${src_mac1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	switch browser	1
	SUITE:Fail_Ping

Add rule 1 with permit and dest mac(mac address of S3) and send traffic from S1.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	switch browser	2
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchAcl > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="${acl_name1}"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	id=ruleAction		permit
	Input Text	id=ruleDestMac	${dst_mac2}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	switch browser	1
	SUITE:Fail_Ping

Change rule 1 to 0 and vice versa using UP/Down option and send traffic from S1.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	switch browser	2
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchAcl > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="${acl_name1}"]
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="1"]/td[1]/input
	Click Element	//*[@id="upButton"]
	GUI::Basic::Spinner Should Be Invisible
	switch browser	1
	SUITE:Pass_Ping

Delete ACL ACL1 and send traffic from S1.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	switch browser	2
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchAcl > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="0"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	switch browser	1
	SUITE:Pass_Ping

Configure ACL 1 and add rule 0 with action permit and src-ipaddress(S1 ip address), assign on the interface S2-S1 and send traffic from S1.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	switch browser	2
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchAcl > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=aclName	${acl_name1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${INTERFACEB3_3} input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=status
	Select From List By Label	id=ingress_acl	${acl_name1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchAcl > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="${acl_name1}"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=ruleSourceIp	${src_ip2}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	switch browser	1
	SUITE:Pass_Ping

Change rule 0 with action deny, src-ipaddress(S1 ip address) and send traffic from S1.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	switch browser	2
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchAcl > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="${acl_name1}"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="0"]
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	id=ruleAction		deny
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	switch browser	1
	SUITE:Fail_Ping

Change rule 0 with action permit, dst-ipaddress(S3 ip address) and send traffic from S1.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	switch browser	2
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchAcl > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="${acl_name1}"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="0"]
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	id=ruleAction		permit
	Input Text	id=ruleDestIp	${dst_ip1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	switch browser	1
	SUITE:Pass_Ping

Change rule 0 with action deny, dst-ipaddress(S3 ip address) and send traffic from S1.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	switch browser	2
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchAcl > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="${acl_name1}"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="0"]
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	id=ruleAction		deny
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	switch browser	1
	SUITE:Fail_Ping

Change rule 0 with action permit, vlan 10 and send traffic from S1.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	switch browser	2
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchAcl > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="${acl_name1}"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="0"]
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	id=ruleAction		permit
	Input Text	id=vlan	${vlan_10}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	switch browser	1
	SUITE:Pass_Ping

Change rule 0 with action deny, vlan 10 and send traffic from S1.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	switch browser	2
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchAcl > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="${acl_name1}"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="0"]
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	id=ruleAction		deny
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	switch browser	1
	SUITE:Fail_Ping

Remove ACL1 from ingress option of the interface.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	switch browser	2
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${INTERFACEB3_3} input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=status
	Select From List By Label	id=ingress_acl	None
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	switch browser	1
	SUITE:Pass_Ping
	SUITE:Teardown

*** Keywords ***
SUITE:Setup
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Setup2
	${HOMEPAGE}=	Set Variable	${HOMEPAGEB}
	${USERNAME}=	Set Variable	admin
	${PASSWORD}=	Set Variable	admin
	GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login	${USERNAME}	${PASSWORD}
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	switch browser	1
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${INTERFACEA3_4} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=status
	Select From List By Label	id=status	${disable}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	switch browser	2
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${INTERFACEB3_4} input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=status
	Select From List By Label	id=status	${disable}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	switch browser	1
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="peerTable"]/tbody/tr[@id="${vlan_intf}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="switchVlanTable"]/tbody/tr[@id="${vlan_10}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	switch browser	2
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="peerTable"]/tbody/tr[@id="${vlan_intf}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="switchVlanTable"]/tbody/tr[@id="${vlan_10}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

SUITE:Topo_setup
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${INTERFACEA3_4} input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	id=status	${enable}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Click element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=vlan	${vlan_10}
	Click Element	//div[@id="memberTagged"]//option[@value="backplane1"]
	Click Element	//div[@id="memberTagged"]//option[@value="${INTERFACEA3_4}"]
	Click Element	//*[@id="memberTagged"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=connName	${vlan_intf}
	Select From List By Value	id=connType		vlan
	Select From List By Value	jquery=#connIeth	backplane1
	Input Text	id=vlanId	${vlan_10}
	Click Element	css=.radio:nth-child(3) #method
	Input Text	id=address	${ip_1}
	Input Text	id=netmask	24
	Click Element	//*[@id="method6"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Setup2
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#${INTERFACEB3_4} input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Edit
	Select From List By Label	id=status	${enable}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Click element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=vlan	${vlan_10}
	Click Element	//div[@id="memberTagged"]//option[@value="backplane1"]
	Click Element	//div[@id="memberTagged"]//option[@value="${INTERFACEB3_4}"]
	Click Element	//*[@id="memberTagged"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=connName	${vlan_intf}
	Select From List By Value	id=connType	vlan
	Select From List By Value	jquery=#connIeth	backplane1
	Input Text	id=vlanId	${vlan_10}
	Click Element	css=.radio:nth-child(3) #method
	Input Text	id=address	${ip_2}
	Input Text	id=netmask	24
	Click Element	//*[@id="method6"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Pass_Ping
	Input Text	//*[@id="ipaddress"]	${ip_2}
	Click Element	xpath=//input[@value="Ping"]
	Wait Until Page Contains	ping statistics	10

SUITE:Fail_ping
	Input Text	//*[@id="ipaddress"]	${ip_2}
	Click Element	xpath=//input[@value="Ping"]
	Wait Until Page Does Not Contain	ping statistics	10