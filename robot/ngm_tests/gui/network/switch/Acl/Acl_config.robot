*** Settings ***
Documentation	Testing the ACL configuration
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${acl_name1}	acl_1
${acl_name2}	acl_2
${src_mac1}	ff:ff:ff:ff:ff:ff/32
${dst_mac1}	bb:bb:45:66:77:67/32
${src_mac2}	12:54:bc:aa:aa:aa/16
${dst_mac2}	aa:aa:45:66:77:67/16
${src_ip1}	2.2.2.2
${dst_ip1}	221.1.1.1
${src_ip2}	3.3.3.3
${dst_ip2}	21.1.1.1
${vlan_5}	5
${vlan_10}	10

*** Test Cases ***
Add new ACL in setting/switch_acl with name and direction
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=aclName	${acl_name1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="${acl_name1}"]

	[Teardown]	Run Keyword And Ignore Error	GUI::Basic::Cancel

Add another ACL with new name and direction (ingress\egress)
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=aclName	${acl_name2}
	Select From List By Value	id=aclDir	egress
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="${acl_name2}"]

	[Teardown]	Run Keyword And Ignore Error	GUI::Basic::Cancel

Configure ACL rule 0 with Action permit, Source/Dest Mac with mask.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	Click Element	//*[@id="${acl_name1}"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=ruleSourceMac	${src_mac1}
	Input Text	id=ruleDestMac	${dst_mac1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="0"]

	[Teardown]	Run Keyword And Ignore Error	GUI::Basic::Cancel

Configure rule 1 with Action deny Source/Dest Mac with mask.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	id=ruleAction	deny
	Input Text	id=ruleSourceMac	${src_mac2}
	Input Text	id=ruleDestMac	${dst_mac2}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="1"]

	[Teardown]	Run Keyword And Ignore Error	GUI::Basic::Cancel

Configure ACL rule 2 with Action permit, Source/Dest ip-address with mask.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	id=ruleAction	permit
	Input Text	id=ruleSourceIp	${src_ip1}
	Input Text	id=ruleDestIp	${dst_ip1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="2"]
	[Teardown]	Run Keyword And Ignore Error	GUI::Basic::Cancel

Configure ACL rule 3 with Action deny, Source/Dest ip-address with mask.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	id=ruleAction	deny
	Input Text	id=ruleSourceIp	${src_ip2}
	Input Text	id=ruleDestIp	${dst_ip2}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="3"]
	[Teardown]	Run Keyword And Ignore Error	GUI::Basic::Cancel

Configure Vlan 5 and then Configure ACL rule 4 with Action permit and vlan 5.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=vlan
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=vlan	${vlan_5}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchAcl > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="${acl_name1}"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	id=ruleAction	deny
	Input Text	id=ruleVid	${vlan_5}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="4"]
	[Teardown]	Run Keyword And Ignore Error	GUI::Basic::Cancel

Configure ACL rule 5 with Action permit and vlan 10.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=vlan
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=vlan	${vlan_10}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchAcl > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="${acl_name1}"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	id=ruleAction	permit
	Input Text	id=ruleVid	${vlan_10}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="5"]
	[Teardown]	Run Keyword And Ignore Error	GUI::Basic::Cancel

Move Rule 5 UP using UP option.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	Select Checkbox	//*[@id="4"]/td[1]/input
	Click Element	//*[@id="upButton"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="0"]
	Page Should Contain Element	//*[@id="1"]
	Page Should Contain Element	//*[@id="2"]
	Page Should Contain Element	//*[@id="3"]
	Page Should Contain Element	//*[@id="4"]
	Page Should Contain Element	//*[@id="5"]
	[Teardown]	Run Keyword And Ignore Error	GUI::Basic::Cancel

Move Rule 4 Down using Down option.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	Select Checkbox	//*[@id="3"]/td[1]/input
	Click Element	//*[@id="downButton"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="0"]
	Page Should Contain Element	//*[@id="1"]
	Page Should Contain Element	//*[@id="2"]
	Page Should Contain Element	//*[@id="3"]
	Page Should Contain Element	//*[@id="4"]
	Page Should Contain Element	//*[@id="5"]
	[Teardown]	Run Keyword And Ignore Error	GUI::Basic::Cancel

Delete Rule 3 and 5.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	Select Checkbox	//*[@id="4"]/td[1]/input
	Select Checkbox	//*[@id="2"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	[Teardown]	Run Keyword And Ignore Error	GUI::Basic::Cancel

Add rules in different ACL name and check configuration.
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchAcl > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="${acl_name2}"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=ruleSourceMac	${src_mac1}
	Input Text	id=ruleDestMac	${dst_mac1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="0"]
	[Teardown]	Run Keyword And Ignore Error	GUI::Basic::Cancel

*** Keywords ***
SUITE:Setup
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

	GUI::Basic::Network::Switch::Open Tab
	Click Element	css=#netswitchAcl > .title_b
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

	#Delete all ACL Created
	GUI::Basic::Network::Switch::open tab
	Click Element	css=#netswitchAcl > .title_b   #Open ACL SubTab
	GUI::Basic::Spinner Should Be Invisible
	
	${present}=    Run Keyword And Return Status    	Page Should Contain Element   //*[@id="${acl_name1}"]
	Run Keyword If    ${present}  Select Checkbox			//tr[@id='0']//input[@type='checkbox']
	${present}=    Run Keyword And Return Status    	Page Should Contain Element   //*[@id="${acl_name2}"]
	Run Keyword If    ${present}  Select Checkbox			//tr[@id='1']//input[@type='checkbox']
	${present}=    Run Keyword And Return Status    	Element Should Be Enabled   //*[@id='delButton']  
	Run Keyword If    ${present}  GUI::Basic::Delete

	#Delete all VLAN Created
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b		#Open VLAN SubTab
	GUI::Basic::Spinner Should Be Invisible

	${present}=    Run Keyword And Return Status    	Page Should Contain Element   //*[@id="${vlan_5}"]
	Run Keyword If    ${present}  Select Checkbox			//table[@id="switchVlanTable"]/tbody/tr[@id="${vlan_5}"]/td[1]/input
	${present}=    Run Keyword And Return Status    	Page Should Contain Element   //*[@id="${vlan_10}"]
	Run Keyword If    ${present}  Select Checkbox			//table[@id="switchVlanTable"]/tbody/tr[@id="${vlan_10}"]/td[1]/input
	${present}=    Run Keyword And Return Status    	Element Should Be Enabled   //*[@id='delButton']  
	Run Keyword If    ${present}  GUI::Basic::Delete

	GUI::Basic::Logout And Close Nodegrid