*** Settings ***
Documentation	Testing Tracking -> Network -> Mac_Table for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2

*** Variable ***
${COORD_HOMEPAGE}	${HOMEPAGE}
${PEER_HOMEPAGE}	${HOMEPAGESHARED}
${Vlan_Name}	vlan_5
${Vlan_id}	5
${ip_address_1}	5.1.1.33
${ip_address_2}	5.1.1.31
${net_mask}	24
${port_33}	${SWITCH_NETPORT_HOST_INTERFACE1}
${port_31}	${SWITCH_SFP_SHARED_INTERFACE1}
${Session_name}	test1
${Destination}	backplane1
${Direction_both}	Both
${Direction_egress}	Egress
${Direction_ingress}	Ingress

*** Test Cases ***
Test Case for Configure Vlan 5 On both NSR
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:To Configure Vlan on Both Sides

Test Case to Ping
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:To Enter the Shell
	SUITE:To Ping from NSR2 to NSR1

Test Case to add port mirroring rule
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Navigate to Port Mirroring Tab
	SUITE:Add Port Mirroring Rule

Test Case to check Egrees traffic
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Navigate to Port Mirroring Tab
	SUITE:To Check Egress Traffic

Test case to Check Ingress traffic
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Navigate to Port Mirroring Tab
	SUITE:To Check Ingress Traffic

Test Case to disable the port mirroring rule
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Navigate to Port Mirroring Tab
	SUITE:To Disable Port Mirroring Rule

Test Case to Rename the port mirroring rule
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:Navigate to Port Mirroring Tab
	SUITE:To Rename the Port Mirroring Rule

Test Case to Delete a Configuration
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	SUITE:To Delete Vlan Configuration
	SUITE:To delete the port mirroring configuration

*** Keywords ***
SUITE:Setup
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Skip If	not ${VLAN_AVAILABLE}	VLAN not available in this automation environment
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:To Enter the Shell
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	wait until page contains element	//*[@id="|nodegrid"]/div[1]/div[2]/a
	Click Element	//*[@id="|nodegrid"]/div[1]/div[2]/a
	Sleep	20s
	Run Keyword If	'${NGVERSION}'=='5.4'	Switch Window	nodegrid
	Run Keyword If	'${NGVERSION}'>'5.4'	Switch Window	nodegrid - Console
	Sleep	15s
	Select Frame	xpath=//*[@id='termwindow']
	Press Keys	//body	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output	shell sudo su -
	Sleep	15s
	Should Contain	${OUTPUT}	root@nodegrid:~#

SUITE:Navigate to Port Mirroring Tab
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchPortMirroring > .title_b
	GUI::Basic::Spinner Should Be Invisible

SUITE:Add Port Mirroring Rule
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	input text	css=#session_name	${Session_name}
	select from list by value	id=destination	${Destination}
	select from list by value	//*[@id="direction"]	${Direction_both}
	select from list by value	//*[@id="status"]	Enabled
	Click Element	//*[@id="sources"]/div/div[1]/div[1]/select/option[1]	###backplane0
	Click Element	css=.col-md-12:nth-child(1) > .listbuilder_btn	###add button
	Click Element	//*[@id="sources"]/div/div[1]/div[1]/select/option[27]	###sfp1
	Click Element	css=.col-md-12:nth-child(1) > .listbuilder_btn
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:To Enter the Shell
	${OUTPUT}=	GUI:Access::Generic Console Command Output	tcpdump --number -c 20 -i backplane1 icmp
	Sleep	15s
	Should Match Regexp	${OUTPUT}	.*request.*

	Sleep	10s

	${OUTPUT}=	GUI:Access::Generic Console Command Output	tcpdump --number -c 20 -i backplane1 icmp
	Sleep	15s
	Should Match Regexp	${OUTPUT}	.*reply.*

SUITE:To Check Egress Traffic
	Click Element	//*[@id="test1"]/td[1]/input
	click element	//*[@id="editButton"]
	GUI::Basic::Spinner Should Be Invisible
	select from list by value	//*[@id="direction"]	${Direction_egress}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:To Enter the Shell
	${OUTPUT}=	GUI:Access::Generic Console Command Output	tcpdump --number -c 20 -i backplane1 icmp
	Sleep	15s
	Should Match Regexp	${OUTPUT}	.*request.*

SUITE:To Check Ingress Traffic
	Click Element	//*[@id="test1"]/td[1]/input
	click element	//*[@id="editButton"]
	GUI::Basic::Spinner Should Be Invisible
	select from list by value	//*[@id="direction"]	${Direction_ingress}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:To Enter the Shell
	${OUTPUT}=	GUI:Access::Generic Console Command Output	tcpdump --number -c 20 -i backplane1 icmp
	Sleep	15s
	Should Match Regexp	${OUTPUT}	.*reply.*

SUITE:To Disable Port Mirroring Rule
	Click Element	//*[@id="test1"]/td[1]/input
	Click Element	//*[@id="disable"]
	GUI::Basic::Spinner Should Be Invisible
	wait until page contains element	xpath=//input[@id='disable']
	SUITE:To Enter the Shell
	${OUTPUT}=	GUI:Access::Generic Console Command Output	tcpdump --number -c 20 -i backplane1 icmp
	Sleep	15s
	Should Not Match Regexp	${OUTPUT}	.*reply.*

SUITE:To Rename the Port Mirroring Rule
	Click Element	//*[@id="test1"]/td[1]/input
	click element	//*[@id="rename"]
	GUI::Basic::Spinner Should Be Invisible
	input text	//*[@id="new_name"]	Renamed
	GUI::Basic::Save
	wait until page contains element	xpath=//a[contains(text(),'Renamed')]
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Configure Vlan on Both Sides
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="connName"]	${Vlan_Name}
	Select From List By value	//*[@id="connType"]	vlan
	Select From List By value	//*[@id="connItfName"]	backplane0
	Click Element	css=.radio:nth-child(3) #method
	Click Element	id=address
	Input Text	id=address	${ip_address_1}
	Click Element	id=netmask
	Input Text	id=netmask	${net_mask}
	Input Text	//*[@id="vlanId"]	${Vlan_id}
	Click Element	//*[@id="method6"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=vlan
	Input Text	//*[@id="vlan"]	${Vlan_id}
	Click Element	//div[@id="memberTagged"]//option[@value="backplane0"]
	Click Element	//div[@id="memberTagged"]//option[@value="${port_33}"]
	Click Element	//*[@id="memberTagged"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select checkbox	//*[@id="${port_33}"]/td[1]/input
	Run Keyword And Continue on Failure	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select from list by value	//*[@id="status"]	enabled
	Run Keyword And Continue on Failure	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="connName"]	${Vlan_Name}
	Select From List By value	//*[@id="connType"]	vlan
	Select From List By value	//*[@id="connItfName"]	backplane0
	Click Element	css=.radio:nth-child(3) #method
	Click Element	id=address
	Input Text	id=address	${ip_address_2}
	Click Element	id=netmask
	Input Text	id=netmask	${net_mask}
	Input Text	//*[@id="vlanId"]	${Vlan_id}
	Click Element	//*[@id="method6"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=vlan
	Input Text	//*[@id="vlan"]	${Vlan_id}
	Click Element	//div[@id="memberTagged"]//option[@value="backplane0"]
	Click Element	//div[@id="memberTagged"]//option[@value="${port_31}"]
	Click Element	//*[@id="memberTagged"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select checkbox	//*[@id="${port_31}"]/td[1]/input
	Run Keyword And Continue on Failure	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select from list by value	//*[@id="status"]	enabled
	Run Keyword And Continue on Failure	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Ping from NSR2 to NSR1
	${OUTPUT}=	GUI:Access::Generic Console Command Output	ping ${ip_address_1}
	Sleep	10s
	Should Match Regexp	${OUTPUT}	.*icmp_seq.*

SUITE:To Delete Vlan Configuration
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${Vlan_Name}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${Vlan_id}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	select checkbox	//*[@id="${port_33}"]/td[1]/input
	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	select from list by value	//*[@id="status"]	disabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${Vlan_Name}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${Vlan_id}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	select checkbox	//*[@id="${port_31}"]/td[1]/input
	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	select from list by value	//*[@id="status"]	disabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:To delete the port mirroring configuration
	SUITE:Navigate to Port Mirroring Tab
	Click Element	//*[@id="Renamed"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible