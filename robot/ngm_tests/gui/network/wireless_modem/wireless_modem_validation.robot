*** Settings ***
Documentation	Testing Wireless Modem under the Network:Wireless Modem Tab
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE	EXCLUDEIN5_0	EXCLUDEIN5_1	EXCLUDEIN5_1	NO-ENV
Default Tags	GUI

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${MODEM}	Channel-B|cdc-wdm0

*** Test Cases ***
Test Table Headings and Buttons
	Skip If	${IS_SR} == False or ${MODEM_EXISTS} == False		Wmodem is only in SR family and wmodem needs to be present
	GUI::Basic::Network::Wireless Modem::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	Element Should Be Disabled      //*[@id="reset"]
	Element Should Be Disabled      //*[@id="firmware"]
	Element Should Contain          //*[@id="thead"]/tr[1]/th[2]      Slot
	Element Should Contain          //*[@id="thead"]/tr[1]/th[3]      Interface
	Element Should Contain          //*[@id="thead"]/tr[1]/th[4]      State
	Element Should Contain          //*[@id="thead"]/tr[1]/th[5]      Firmware Version

Test Firmware Button
	Skip If	${IS_SR} == False or ${MODEM_EXISTS} == False		Wmodem is only in SR family and wmodem needs to be present
	Click Element       //*[@id="${MODEM}"]/td[1]/input
	Element Should Be Enabled      //*[@id="reset"]
	Element Should Be Enabled      //*[@id="firmware"]
	Click Element       //*[@id="firmware"]
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible       jquery=#wmodemGlobalFirmwareTable
	Element Should Be Enabled      //*[@id="returnButton"]
	Element Should Be Disabled      //*[@id="delButton"]
	Element Should Be Enabled      //*[@id="fwupgrade"]

Test Table Headings for Firmware table
	Skip If	${IS_SR} == False or ${MODEM_EXISTS} == False		Wmodem is only in SR family and wmodem needs to be present
	Element Should Be Visible       jquery=#wmodemGlobalFirmwareTable
	Element Should Contain          //*[@id="thead"]/tr[1]/th[2]      Build ID
	Element Should Contain          //*[@id="thead"]/tr[1]/th[3]      Type
	Element Should Contain          //*[@id="thead"]/tr[1]/th[4]      Unique ID

Test Enabling/Disabling Delete Button
	Skip If	${IS_SR} == False or ${MODEM_EXISTS} == False		Wmodem is only in SR family and wmodem needs to be present
	Element Should Be Disabled      //*[@id="delButton"]
	Click Element       //*[@id="${MODEM}|modem0"]/td[1]/input
	Element Should Be Enabled		//*[@id="delButton"]
	Click Element       //*[@id="${MODEM}|modem1"]/td[1]/input
	Element Should Be Disabled		//*[@id="delButton"]

Test Upgrade Button
	Skip If	${IS_SR} == False or ${MODEM_EXISTS} == False		Wmodem is only in SR family and wmodem needs to be present
	Click Element       //*[@id="fwupgrade"]
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible       jquery=#upgradeForm

Test Cancel Button
	Skip If	${IS_SR} == False or ${MODEM_EXISTS} == False		Wmodem is only in SR family and wmodem needs to be present
	Element Should Be Enabled      //*[@id="cancelButton"]
	Element Should Be Disabled      //*[@id="saveButton"]
	Click Element 	//*[@id="cancelButton"]
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible       jquery=#wmodemGlobalFirmwareTable

Test Return Button
	Skip If	${IS_SR} == False or ${MODEM_EXISTS} == False		Wmodem is only in SR family and wmodem needs to be present
	Element Should Be Enabled      //*[@id="returnButton"]
	Click Element 	//*[@id="returnButton"]
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible       jquery=#wmodemGlobalTable

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	${SR FAMILY}=	Create List	Nodegrid Services Router	Nodegrid Gate SR	Nodegrid Link SR	Nodegrid Bold SR
	${SYSTEM}=	GUI4.x::Basic::Get System
	${IS_SR}=	Run Keyword And Return Status	List Should Contain Value	${SR_FAMILY}	${SYSTEM}
	Set Suite Variable 	${IS_SR}
	${MODEM_EXISTS}=	Run Keyword If 	${IS_SR}	SUITE:Check If Modem Exists
	Set Suite Variable 	${MODEM_EXISTS}

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout
	Close all Browsers

SUITE:Check If Modem Exists
    GUI::Basic::Network::Wireless Modem::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	${EXISTS}=	Run Keyword And Return Status 	Table Should Contain	//*[@id="wmodemGlobalTable"]	Channel-B
	[Return]  ${EXISTS}