*** Settings ***
Documentation	Functionality test cases for ipsec tunnel monitoring through GUI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EXCLUDEIN4_2	NON-CRITICAL	BUG_NG_11449

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Test Teardown	SUITE:Test Teardown

*** Variables ***
${LEFT_IP}	${HOST}
${RIGHT_IP}	${HOSTSHARED}
${IPSEC_TUNNEL_NAME_LEFT}	ipsec_tunnel_NGL
${IPSEC_TUNNEL_NAME_RIGHT}	ipsec_tunnel_NGR
${LEFT_ID}	@NGL
${RIGHT_ID}	@NGR
${LEFT_SOURCE_IP}	10.0.0.2
${RIGHT_SOURCE_IP}	10.0.0.3
${IPSEC_SECRET}	${QA_PASSWORD}
${IPV4_LEFT_SUBNET}	${EMPTY}	#	10.0.0.0/24
${IPV4_RIGHT_SUBNET}	${EMPTY}	#	10.0.0.0/24
@{INITIATE_TUNNEL}	Start	On Demand	Ignore
${MONITOR_RETRIES}	3
${MONITOR_INTERVAL}	5
${IPSEC_TUNNEL_NAME_LEFT_FAILOVER}	ipsec_tunnel_NGL_failover
${IPSEC_TUNNEL_NAME_RIGHT_FAILOVER}	ipsec_tunnel_NGR_failover
${LEFT_ID_FAILOVER}	@NGL_failover
${RIGHT_ID_FAILOVER}	@NGR_failover
${LEFT_SOURCE_IP_FAILOVER}	10.0.0.4
${RIGHT_SOURCE_IP_FAILOVER}	10.0.0.5
${IPSEC_TUNNEL_NAME_LEFT_RSA2}	ipsec_tunnel_NGL_RSA2
${IPSEC_TUNNEL_NAME_RIGHT_RSA2}	ipsec_tunnel_NGR_RSA2
${LEFT_ID_RSA2}	@NGL_RSA2
${RIGHT_ID_RSA2}	@NGR_RSA2
${LEFT_SOURCE_IP_RSA2}	10.0.0.6
${RIGHT_SOURCE_IP_RSA2}	10.0.0.7
${LEFT_SOURCE_IPV6}	2001:0db8:cafe::2
${RIGHT_SOURCE_IPV6}	2001:0db8:cafe::3
${LENGTH}	64
${IPV6_LEFT_SUBNET}	${EMPTY}	#	2001:0db8:cafe::0/${LENGTH}
${IPV6_RIGHT_SUBNET}	${EMPTY}	#	2001:0db8:cafe::0/${LENGTH}
${IPSEC_TUNNEL_NAME_LEFT2}	ipsec_tunnel_NGL2
${IPSEC_TUNNEL_NAME_RIGHT2}	ipsec_tunnel_NGR2
${LEFT_ID2}	@NGL2
${RIGHT_ID2}	@NGR2
${LEFT_SOURCE2_IPV6}	2001:0db8:cafe::4
${RIGHT_SOURCE2_IPV6}	2001:0db8:cafe::5
${IKE_PROFILE_NAME}	test-automation-personalized
${IKE_VERSION}	IKEv1
${IKE_MODE}	Aggressive
${IKE_ENCRYPTION}	AES-CTR256
${IKE_AUTHENTICATION}	SHA512
${IKE_DIFFLE}	Group 31 (CURVE25519)
${IKE_LIFETIME}	3600
${IKE_AUTH_PROTO}	ESP
${IKE_ENCRYPTION2}	AES-CTR256
${IKE_AUTHENTICATION2}	SHA512
${IKE_PFS_GROUP}	Group 14 (MODP2048)
${IKE_LIFETIME2}	28800
${IKE_NUMB_RETR}	5
${IKE_INTERVAL}	5
${IKE_ACTION}	hold
${IKE_MTU}	1500
${IKE_CUSTOM_PARAMS}	SEPARATOR=
...	type=tunnel\n
...	initial_contact=no\n
...	keyingtries=%forever\n
...	keyexchange=ike\n
...	nat_keepalive=yes\n
...	fragmentation=yes\n
...	pfs=yes
${DEFAULT_CUSTOM_SCRIPT_NAME}	route_based_vti_updown.sh
${TEST_CUSTOM_SCRIPT_NAME}	test_automation_route_based_vti_updown.sh
${ROUTE_LEFT_SUBNET}	0.0.0.0/0
${ROUTE_RIGHT_SUBNET}	0.0.0.0/0
${VTI_NAME}	vti
${GOOGLE_IP}	8.8.8.8

*** Test Cases ***
#nodegrid
Test case to setup ipsec tunnel using nodegrid IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_LEFT}	nodegrid	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_RIGHT}	nodegrid	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	IKE_PROFILE=nodegrid
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	PEER_ID=${RIGHT_ID}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	IKE_PROFILE=nodegrid
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	PEER_ID=${LEFT_ID}

Test case to ping right from left through IPsec tunnel using nodegrid IKE profile and Pre-Shared Key
	[Tags]	NON-CRITICAL	BUG_NG_11449
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left by root	${RIGHT_SOURCE_IP}	${LEFT_SOURCE_IP}

Test case to ping left from right through IPsec tunnel using nodegrid IKE profile and Pre-Shared Key
	[Tags]	NON-CRITICAL	BUG_NG_11449
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right by root	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}

Test case to delete ipsec tunnel both sides using nodegrid IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_LEFT}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_RIGHT}

Test case to setup ipsec tunnel with monitoring and action failover using nodegrid IKE profile and Pre-Shared Key
#	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	BUG_NG_7982	#<-- BUG CLOSED
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_LEFT_FAILOVER}	nodegrid	${LEFT_ID_FAILOVER}	${RIGHT_ID_FAILOVER}
	...	${LEFT_IP}	${RIGHT_IP}	${LEFT_SOURCE_IP_FAILOVER}	${RIGHT_SOURCE_IP_FAILOVER}
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_LEFT}	nodegrid	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}	MONITORING=Yes	ACTION=Failover
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_RIGHT_FAILOVER}	nodegrid	${LEFT_ID_FAILOVER}	${RIGHT_ID_FAILOVER}
	...	${LEFT_IP}	${RIGHT_IP}	${LEFT_SOURCE_IP_FAILOVER}	${RIGHT_SOURCE_IP_FAILOVER}
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_RIGHT}	nodegrid	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	IKE_PROFILE=nodegrid
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	PEER_ID=${RIGHT_ID}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	IKE_PROFILE=nodegrid
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	PEER_ID=${LEFT_ID}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Reset counters in event list
	SUITE:Turn down IPsec tunnel
	Wait Until Keyword Succeeds	9x	3s	SUITE:Check for an increase in event counters 162 and 163 in the event list

Test case to ping right from left through IPsec tunnel monitoring with failover action and both sides using nodegrid IKE profile and Pre-Shared Key
#	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	BUG_NG_7982	#<-- BUG CLOSED
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left by root	${RIGHT_SOURCE_IP}	${LEFT_SOURCE_IP}

Test case to ping left from right through IPsec tunnel monitoring with failover action and both sides using nodegrid IKE profile and Pre-Shared Key
#	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	BUG_NG_7982	#<-- BUG CLOSED
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right by root	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}

Test case to delete ipsec tunnel monitoring with failover action and both sides using nodegrid IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_LEFT_FAILOVER}
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_LEFT}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_RIGHT_FAILOVER}
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_RIGHT}

Test case to setup ipsec tunnel with monitoring and action restart IPsec using nodegrid IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_LEFT}	nodegrid	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}	MONITORING=Yes	ACTION=Restart IPsec
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_RIGHT}	nodegrid	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	IKE_PROFILE=nodegrid
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	PEER_ID=${RIGHT_ID}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	IKE_PROFILE=nodegrid
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	PEER_ID=${LEFT_ID}
#	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
#	Wait Until Keyword Succeeds	3x	3s	SUITE:Reset counters in event list
#	SUITE:Turn down IPsec tunnel
#	Wait Until Keyword Succeeds	9x	3s	SUITE:Check for an increase in event counters 162 and 163 in the event list

Test case to ping right from left through IPsec tunnel monitoring with Restart IPsec action and both sides using nodegrid IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left by root	${RIGHT_SOURCE_IP}	${LEFT_SOURCE_IP}

Test case to ping left from right through IPsec tunnel monitoring with Restart IPsecr action and both sides using nodegrid IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right by root	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}

Test case to delete ipsec tunnel monitoring with Restart IPsec action and both sides using nodegrid IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_LEFT}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_RIGHT}

Test case to setup ipsec tunnel with monitoring and action restart tunnel using nodegrid IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_LEFT}	nodegrid	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}	MONITORING=Yes	ACTION=Restart Tunnel
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_RIGHT}	nodegrid	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	IKE_PROFILE=nodegrid
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	PEER_ID=${RIGHT_ID}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	IKE_PROFILE=nodegrid
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	PEER_ID=${LEFT_ID}
#	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
#	Wait Until Keyword Succeeds	3x	3s	SUITE:Reset counters in event list
#	SUITE:Turn down IPsec tunnel
#	Wait Until Keyword Succeeds	9x	3s	SUITE:Check for an increase in event counters 162 and 163 in the event list

Test case to ping right from left through IPsec tunnel monitoring with Restart Tunnel action and both sides using nodegrid IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left by root	${RIGHT_SOURCE_IP}	${LEFT_SOURCE_IP}

Test case to ping left from right through IPsec tunnel monitoring with Restart Tunnel action and both sides using nodegrid IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right by root	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}

Test case to delete ipsec tunnel monitoring with Restart Tunnel action and both sides using nodegrid IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_LEFT}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_RIGHT}

Test case to setup ipsec tunnel using nodegrid IKE profile and RSA Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_LEFT}	nodegrid	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}	MONITORING=Yes
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_RIGHT}	nodegrid	${RIGHT_ID}	${LEFT_ID}	${RIGHT_IP}	${LEFT_IP}
	...	${RIGHT_SOURCE_IP}	${LEFT_SOURCE_IP}
	#Edit the configuration on left and right sides to use RSA key
	SUITE:Edit IPsec Tunnel for RSA Key both sides	${IPSEC_TUNNEL_NAME_LEFT}	${IPSEC_TUNNEL_NAME_RIGHT}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	IKE_PROFILE=nodegrid	AUTHENTICATION_METHOD=RSA Key
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	PEER_ID=${RIGHT_ID}	AUTHENTICATION_METHOD=RSA Key
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	IKE_PROFILE=nodegrid	AUTHENTICATION_METHOD=RSA Key
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	PEER_ID=${LEFT_ID}	AUTHENTICATION_METHOD=RSA Key

Test case to ping right from left through IPsec tunnel using nodegrid IKE profile and RSA Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left by root	${RIGHT_SOURCE_IP}	${LEFT_SOURCE_IP}

Test case to ping left from right through IPsec tunnel using nodegrid IKE profile and RSA Key
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right by root	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}

Test case to delete ipsec tunnel both sides using nodegrid IKE profile and RSA Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_LEFT}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_RIGHT}

#Cisco_ASA

Test case to setup ipsec tunnel using Cisco_ASA IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_LEFT}	Cisco_ASA	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_RIGHT}	Cisco_ASA	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	IKE_PROFILE=Cisco_ASA
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	PEER_ID=${RIGHT_ID}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	IKE_PROFILE=Cisco_ASA
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	PEER_ID=${LEFT_ID}

Test case to ping right from left through IPsec tunnel using Cisco_ASA IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left by root	${RIGHT_SOURCE_IP}	${LEFT_SOURCE_IP}

Test case to ping left from right through IPsec tunnel using Cisco_ASA IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right by root	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}

Test case to delete ipsec tunnel both sides using Cisco_ASA IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_LEFT}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_RIGHT}

Test case to setup ipsec tunnel with monitoring and action failover using Cisco_ASA IKE profile and Pre-Shared Key
#	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	BUG_NG_7982	#<-- BUG CLOSED
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_LEFT_FAILOVER}	Cisco_ASA	${LEFT_ID_FAILOVER}	${RIGHT_ID_FAILOVER}
	...	${LEFT_IP}	${RIGHT_IP}	${LEFT_SOURCE_IP_FAILOVER}	${RIGHT_SOURCE_IP_FAILOVER}
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_LEFT}	Cisco_ASA	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}	MONITORING=Yes	ACTION=Failover
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_RIGHT_FAILOVER}	Cisco_ASA	${LEFT_ID_FAILOVER}	${RIGHT_ID_FAILOVER}
	...	${LEFT_IP}	${RIGHT_IP}	${LEFT_SOURCE_IP_FAILOVER}	${RIGHT_SOURCE_IP_FAILOVER}
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_RIGHT}	Cisco_ASA	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	IKE_PROFILE=Cisco_ASA
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	PEER_ID=${RIGHT_ID}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	IKE_PROFILE=Cisco_ASA
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	PEER_ID=${LEFT_ID}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Reset counters in event list
	SUITE:Turn down IPsec tunnel
	Wait Until Keyword Succeeds	9x	3s	SUITE:Check for an increase in event counters 162 and 163 in the event list

Test case to ping right from left through IPsec tunnel monitoring with failover action and both sides using Cisco_ASA IKE profile and Pre-Shared Key
#	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	BUG_NG_7982	#<-- BUG CLOSED
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left by root	${RIGHT_SOURCE_IP}	${LEFT_SOURCE_IP}

Test case to ping left from right through IPsec tunnel monitoring with failover action and both sides using Cisco_ASA IKE profile and Pre-Shared Key
#	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	BUG_NG_7982	#<-- BUG CLOSED
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right by root	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}

Test case to delete ipsec tunnel monitoring with failover action and both sides using Cisco_ASA IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_LEFT_FAILOVER}
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_LEFT}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_RIGHT_FAILOVER}
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_RIGHT}

Test case to setup ipsec tunnel with monitoring and action restart IPsec using Cisco_ASA IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_LEFT}	Cisco_ASA	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}	MONITORING=Yes	ACTION=Restart IPsec
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_RIGHT}	Cisco_ASA	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	IKE_PROFILE=Cisco_ASA
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	PEER_ID=${RIGHT_ID}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	IKE_PROFILE=Cisco_ASA
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	PEER_ID=${LEFT_ID}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Reset counters in event list
	SUITE:Turn down IPsec tunnel
	Wait Until Keyword Succeeds	9x	3s	SUITE:Check for an increase in event counters 162 and 163 in the event list

Test case to ping right from left through IPsec tunnel monitoring with Restart IPsec action and both sides using Cisco_ASA IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left by root	${RIGHT_SOURCE_IP}	${LEFT_SOURCE_IP}

Test case to ping left from right through IPsec tunnel monitoring with Restart IPsecr action and both sides using Cisco_ASA IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right by root	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}

Test case to delete ipsec tunnel monitoring with Restart IPsec action and both sides using Cisco_ASA IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_LEFT}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_RIGHT}

Test case to setup ipsec tunnel with monitoring and action restart tunnel using Cisco_ASA IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_LEFT}	Cisco_ASA	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}	MONITORING=Yes	ACTION=Restart Tunnel
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_RIGHT}	Cisco_ASA	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	IKE_PROFILE=Cisco_ASA
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	PEER_ID=${RIGHT_ID}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	IKE_PROFILE=Cisco_ASA
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	PEER_ID=${LEFT_ID}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Reset counters in event list
	SUITE:Turn down IPsec tunnel
	Wait Until Keyword Succeeds	9x	3s	SUITE:Check for an increase in event counters 162 and 163 in the event list

Test case to ping right from left through IPsec tunnel monitoring with Restart Tunnel action and both sides using Cisco_ASA IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left by root	${RIGHT_SOURCE_IP}	${LEFT_SOURCE_IP}

Test case to ping left from right through IPsec tunnel monitoring with Restart Tunnel action and both sides using Cisco_ASA IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right by root	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}

Test case to delete ipsec tunnel monitoring with Restart Tunnel action and both sides using Cisco_ASA IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_LEFT}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_RIGHT}

Test case to setup ipsec tunnel using Cisco_ASA IKE profile and RSA Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_LEFT}	Cisco_ASA	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}	MONITORING=Yes
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_RIGHT}	Cisco_ASA	${RIGHT_ID}	${LEFT_ID}	${RIGHT_IP}	${LEFT_IP}
	...	${RIGHT_SOURCE_IP}	${LEFT_SOURCE_IP}
	#Edit the configuration on left and right sides to use RSA key
	SUITE:Edit IPsec Tunnel for RSA Key both sides	${IPSEC_TUNNEL_NAME_LEFT}	${IPSEC_TUNNEL_NAME_RIGHT}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	IKE_PROFILE=Cisco_ASA	AUTHENTICATION_METHOD=RSA Key
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	PEER_ID=${RIGHT_ID}	AUTHENTICATION_METHOD=RSA Key
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	IKE_PROFILE=Cisco_ASA	AUTHENTICATION_METHOD=RSA Key
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	PEER_ID=${LEFT_ID}	AUTHENTICATION_METHOD=RSA Key

Test case to ping right from left through IPsec tunnel using Cisco_ASA IKE profile and RSA Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left by root	${RIGHT_SOURCE_IP}	${LEFT_SOURCE_IP}

Test case to ping left from right through IPsec tunnel using Cisco_ASA IKE profile and RSA Key
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right by root	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}

Test case to delete ipsec tunnel both sides using Cisco_ASA IKE profile and RSA Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_LEFT}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_RIGHT}

#PaloAlto

Test case to setup ipsec tunnel using PaloAlto IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_LEFT}	PaloAlto	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_RIGHT}	PaloAlto	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	IKE_PROFILE=PaloAlto
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	PEER_ID=${RIGHT_ID}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	IKE_PROFILE=PaloAlto
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	PEER_ID=${LEFT_ID}

Test case to ping right from left through IPsec tunnel using PaloAlto IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left by root	${RIGHT_SOURCE_IP}	${LEFT_SOURCE_IP}

Test case to ping left from right through IPsec tunnel using PaloAlto IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right by root	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}

Test case to delete ipsec tunnel both sides using PaloAlto IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_LEFT}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_RIGHT}

Test case to setup ipsec tunnel with monitoring and action failover using PaloAlto IKE profile and Pre-Shared Key
#	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	BUG_NG_7982	#<-- BUG CLOSED
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_LEFT_FAILOVER}	PaloAlto	${LEFT_ID_FAILOVER}	${RIGHT_ID_FAILOVER}
	...	${LEFT_IP}	${RIGHT_IP}	${LEFT_SOURCE_IP_FAILOVER}	${RIGHT_SOURCE_IP_FAILOVER}
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_LEFT}	PaloAlto	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}	MONITORING=Yes	ACTION=Failover
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_RIGHT_FAILOVER}	PaloAlto	${LEFT_ID_FAILOVER}	${RIGHT_ID_FAILOVER}
	...	${LEFT_IP}	${RIGHT_IP}	${LEFT_SOURCE_IP_FAILOVER}	${RIGHT_SOURCE_IP_FAILOVER}
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_RIGHT}	PaloAlto	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	IKE_PROFILE=PaloAlto
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	PEER_ID=${RIGHT_ID}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	IKE_PROFILE=PaloAlto
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	PEER_ID=${LEFT_ID}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Reset counters in event list
	SUITE:Turn down IPsec tunnel
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	IKE_PROFILE=PaloAlto
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	PEER_ID=${RIGHT_ID}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	IKE_PROFILE=PaloAlto
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	PEER_ID=${LEFT_ID}
	Wait Until Keyword Succeeds	9x	3s	SUITE:Check for an increase in event counters 162 and 163 in the event list

Test case to ping right from left through IPsec tunnel monitoring with failover action and both sides using PaloAlto IKE profile and Pre-Shared Key
#	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	BUG_NG_7982	#<-- BUG CLOSED
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left by root	${RIGHT_SOURCE_IP}	${LEFT_SOURCE_IP}

Test case to ping left from right through IPsec tunnel monitoring with failover action and both sides using PaloAlto IKE profile and Pre-Shared Key
#	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	BUG_NG_7982	#<-- BUG CLOSED
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right by root	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}

Test case to delete ipsec tunnel monitoring with failover action and both sides using PaloAlto IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_LEFT_FAILOVER}
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_LEFT}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_RIGHT_FAILOVER}
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_RIGHT}

Test case to setup ipsec tunnel with monitoring and action restart IPsec using PaloAlto IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_LEFT}	PaloAlto	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}	MONITORING=Yes	ACTION=Restart IPsec
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_RIGHT}	PaloAlto	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	IKE_PROFILE=PaloAlto
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	PEER_ID=${RIGHT_ID}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	IKE_PROFILE=PaloAlto
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	PEER_ID=${LEFT_ID}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Reset counters in event list
#	SUITE:Turn down IPsec tunnel
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	IKE_PROFILE=PaloAlto
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	PEER_ID=${RIGHT_ID}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	IKE_PROFILE=PaloAlto
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	PEER_ID=${LEFT_ID}
	Wait Until Keyword Succeeds	9x	3s	SUITE:Check for an increase in event counters 162 and 163 in the event list

Test case to ping right from left through IPsec tunnel monitoring with Restart IPsec action and both sides using PaloAlto IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left by root	${RIGHT_SOURCE_IP}	${LEFT_SOURCE_IP}

Test case to ping left from right through IPsec tunnel monitoring with Restart IPsecr action and both sides using PaloAlto IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right by root	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}

Test case to delete ipsec tunnel monitoring with Restart IPsec action and both sides using PaloAlto IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_LEFT}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_RIGHT}

Test case to setup ipsec tunnel with monitoring and action restart tunnel using PaloAlto IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_LEFT}	PaloAlto	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}	MONITORING=Yes	ACTION=Restart Tunnel
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_RIGHT}	PaloAlto	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	IKE_PROFILE=PaloAlto
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	PEER_ID=${RIGHT_ID}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	IKE_PROFILE=PaloAlto
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	PEER_ID=${LEFT_ID}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Reset counters in event list
#	SUITE:Turn down IPsec tunnel
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	IKE_PROFILE=PaloAlto
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	PEER_ID=${RIGHT_ID}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	IKE_PROFILE=PaloAlto
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	PEER_ID=${LEFT_ID}
	Wait Until Keyword Succeeds	9x	3s	SUITE:Check for an increase in event counters 162 and 163 in the event list

Test case to ping right from left through IPsec tunnel monitoring with Restart Tunnel action and both sides using PaloAlto IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left by root	${RIGHT_SOURCE_IP}	${LEFT_SOURCE_IP}

Test case to ping left from right through IPsec tunnel monitoring with Restart Tunnel action and both sides using PaloAlto IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right by root	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}

Test case to delete ipsec tunnel monitoring with Restart Tunnel action and both sides using PaloAlto IKE profile and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_LEFT}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_RIGHT}

Test case to setup ipsec tunnel using PaloAlto IKE profile and RSA Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_LEFT}	PaloAlto	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}	MONITORING=Yes
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_RIGHT}	PaloAlto	${RIGHT_ID}	${LEFT_ID}	${RIGHT_IP}	${LEFT_IP}
	...	${RIGHT_SOURCE_IP}	${LEFT_SOURCE_IP}
	#Edit the configuration on left and right sides to use RSA key
	SUITE:Edit IPsec Tunnel for RSA Key both sides	${IPSEC_TUNNEL_NAME_LEFT}	${IPSEC_TUNNEL_NAME_RIGHT}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	IKE_PROFILE=PaloAlto	AUTHENTICATION_METHOD=RSA Key
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	PEER_ID=${RIGHT_ID}	AUTHENTICATION_METHOD=RSA Key
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	IKE_PROFILE=PaloAlto	AUTHENTICATION_METHOD=RSA Key
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	PEER_ID=${LEFT_ID}	AUTHENTICATION_METHOD=RSA Key

Test case to ping right from left through IPsec tunnel using PaloAlto IKE profile and RSA Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left by root	${RIGHT_SOURCE_IP}	${LEFT_SOURCE_IP}

Test case to ping left from right through IPsec tunnel using PaloAlto IKE profile and RSA Key
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right by root	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}

#Two IPsec tunnels using RSA key - BUG_NG_8086
Test case to setup second ipsec tunnel using nodegrid IKE profile and RSA Key
	[Tags]	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EXCLUDEIN4_2	NON-CRITICAL	NEED-REVIEW	BUG_NG_8086
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_LEFT_RSA2}	nodegrid	${LEFT_ID_RSA2}	${RIGHT_ID_RSA2}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP_RSA2}	${RIGHT_SOURCE_IP_RSA2}	MONITORING=Yes
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_RIGHT_RSA2}	nodegrid	${RIGHT_ID_RSA2}	${LEFT_ID_RSA2}	${RIGHT_IP}	${LEFT_IP}
	...	${RIGHT_SOURCE_IP_RSA2}	${LEFT_SOURCE_IP_RSA2}
	#Edit the configuration on left and right sides to use RSA key
	SUITE:Edit IPsec Tunnel for RSA Key both sides	${IPSEC_TUNNEL_NAME_LEFT_RSA2}	${IPSEC_TUNNEL_NAME_RIGHT_RSA2}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT_RSA2}	IKE_PROFILE=nodegrid	AUTHENTICATION_METHOD=RSA Key
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT_RSA2}	PEER_ID=${RIGHT_ID_RSA2}	AUTHENTICATION_METHOD=RSA Key
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT_RSA2}	IKE_PROFILE=nodegrid	AUTHENTICATION_METHOD=RSA Key
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT_RSA2}	PEER_ID=${LEFT_ID_RSA2}	AUTHENTICATION_METHOD=RSA Key

Test case to ping right from left through Second IPsec tunnel using nodegrid IKE profile and RSA Key
	[Tags]	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EXCLUDEIN4_2		EXCLUDEIN5_0		EXCLUDEIN5_2		EXCLUDEIN5_4		EXCLUDEIN5_6		NON-CRITICAL	NEED-REVIEW	BUG_NG_8086
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping from ${LEFT_SOURCE_IP_RSA2} through IPsec tunnel to ${RIGHT_SOURCE_IP_RSA2}
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left by root	${RIGHT_SOURCE_IP_RSA2}	${LEFT_SOURCE_IP_RSA2}

Test case to ping left from right through Second IPsec tunnel using nodegrid IKE profile and RSA Key
	[Tags]	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EXCLUDEIN4_2	NON-CRITICAL	NEED-REVIEW	BUG_NG_8086
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping from ${RIGHT_SOURCE_IP_RSA2} through IPsec tunnel to ${LEFT_SOURCE_IP_RSA2}
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right by root	${LEFT_SOURCE_IP_RSA2}	${RIGHT_SOURCE_IP_RSA2}

Test case to delete the first ipsec tunnel both sides using PaloAlto IKE profile and RSA Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_LEFT}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_RIGHT}

Test case to delete the second ipsec tunnel both sides using nodegrid IKE profile and RSA Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_LEFT_RSA2}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_RIGHT_RSA2}

#Two IPsec tunnels using one RSA key and the other Pre-Shared Key also both using IPv6
Test case to setup Two IPsec tunnels where one of them is using RSA key and the other Pre-Shared Key both using IPv6
	[Tags]	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EXCLUDEIN4_2	NON-CRITICAL	NEED-REVIEW	BUG_NG_8086
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	${RIGHT_IPV6}	GUI::Network::Get Connection Ipv6	ETH0
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	${LEFT_IPV6}	GUI::Network::Get Connection Ipv6	ETH0
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_LEFT}	nodegrid	${LEFT_ID}	${RIGHT_ID}	${LEFT_IPV6}	${RIGHT_IPV6}
	...	${LEFT_SOURCE_IPV6}	${RIGHT_SOURCE_IPV6}	MONITORING=Yes	LEFT_SUBNET=${IPV6_LEFT_SUBNET}	RIGHT_SUBNET=${IPV6_RIGHT_SUBNET}
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_LEFT2}	nodegrid	${LEFT_ID2}	${RIGHT_ID2}	${LEFT_IPV6}	${RIGHT_IPV6}
	...	${LEFT_SOURCE2_IPV6}	${RIGHT_SOURCE2_IPV6}	MONITORING=Yes	LEFT_SUBNET=${IPV6_LEFT_SUBNET}	RIGHT_SUBNET=${IPV6_RIGHT_SUBNET}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_RIGHT}	nodegrid	${RIGHT_ID}	${LEFT_ID}	${RIGHT_IPV6}	${LEFT_IPV6}
	...	${RIGHT_SOURCE_IPV6}	${LEFT_SOURCE_IPV6}	LEFT_SUBNET=${IPV6_LEFT_SUBNET}	RIGHT_SUBNET=${IPV6_RIGHT_SUBNET}
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_RIGHT2}	nodegrid	${RIGHT_ID2}	${LEFT_ID2}	${RIGHT_IPV6}	${LEFT_IPV6}
	...	${RIGHT_SOURCE2_IPV6}	${LEFT_SOURCE2_IPV6}	LEFT_SUBNET=${IPV6_LEFT_SUBNET}	RIGHT_SUBNET=${IPV6_RIGHT_SUBNET}
	#Edit the configuration on left and right sides to use RSA key for the first tunnel
	SUITE:Edit IPsec Tunnel for RSA Key both sides	${IPSEC_TUNNEL_NAME_LEFT}	${IPSEC_TUNNEL_NAME_RIGHT}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	IKE_PROFILE=nodegrid	AUTHENTICATION_METHOD=RSA Key
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	PEER_ID=${RIGHT_ID}	AUTHENTICATION_METHOD=RSA Key
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT2}	IKE_PROFILE=nodegrid
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT2}	PEER_ID=${RIGHT_ID2}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	IKE_PROFILE=nodegrid	AUTHENTICATION_METHOD=RSA Key
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	PEER_ID=${LEFT_ID}	AUTHENTICATION_METHOD=RSA Key
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT2}	IKE_PROFILE=nodegrid
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT2}	PEER_ID=${LEFT_ID2}

Test case to ping right from left through First IPsec tunnel using nodegrid IKE profile and RSA Key using IPv6
	[Tags]	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EXCLUDEIN4_2	EXCLUDEIN_OFFICIAL		EXCLUDEIN5_6		NON-CRITICAL	NEED-REVIEW	BUG_NG_8086
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping from ${LEFT_SOURCE_IPV6} through IPsec tunnel to ${RIGHT_SOURCE_IPV6}
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left by root	${RIGHT_SOURCE_IPV6}	${LEFT_SOURCE_IPV6}	SUITE_IPV6=Yes

Test case to ping left from right through First IPsec tunnel using nodegrid IKE profile and RSA Key using IPv6
	[Tags]	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EXCLUDEIN4_2	EXCLUDEIN_OFFICIAL		EXCLUDEIN5_6		NON-CRITICAL	NEED-REVIEW	BUG_NG_8086
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping from ${RIGHT_SOURCE_IPV6} through IPsec tunnel to ${LEFT_SOURCE_IPV6}
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right by root	${LEFT_SOURCE_IPV6}	${RIGHT_SOURCE_IPV6}	SUITE_IPV6=Yes

Test case to ping right from left through Second IPsec tunnel using nodegrid IKE profile and Pre-shared Key using IPv6
	[Tags]	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EXCLUDEIN4_2	EXCLUDEIN_OFFICIAL		EXCLUDEIN5_6		NON-CRITICAL	NEED-REVIEW	BUG_NG_8086
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping from ${LEFT_SOURCE2_IPV6} through IPsec tunnel to ${RIGHT_SOURCE2_IPV6}
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping right from left by root	${RIGHT_SOURCE2_IPV6}	${LEFT_SOURCE2_IPV6}	SUITE_IPV6=Yes

Test case to ping left from right through Second IPsec tunnel using nodegrid IKE profile and Pre-shared Key using IPv6
	[Tags]	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EXCLUDEIN4_2	EXCLUDEIN_OFFICIAL		EXCLUDEIN5_6		NON-CRITICAL	NEED-REVIEW	BUG_NG_8086
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	#ping through interfaces is available to v5.2+ by GUI
	Run Keyword If	'${NGVERSION}' >= '5.2'	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping from ${RIGHT_SOURCE2_IPV6} through IPsec tunnel to ${LEFT_SOURCE2_IPV6}
	...	ELSE	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping left from right by root	${LEFT_SOURCE2_IPV6}	${RIGHT_SOURCE2_IPV6}	SUITE_IPV6=Yes

Test case to delete the first ipsec tunnel both sides using PaloAlto IKE profile and RSA Key using IPv6
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_LEFT}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_RIGHT}

Test case to delete the second ipsec tunnel both sides using nodegrid IKE profile and RSA Key using IPv6
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_LEFT2}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_RIGHT2}
	[Teardown]	SUITE:Test Teardown	RE-LOGIN=Yes

# ipsec route based tunnel with personalized IKE-profile
Test case to setup ipsec tunnel with ipsec route based tunnel script using nodegrid IKE profile and Pre-Shared Key
	[Tags]	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL	BUG_NG_9567	BUG_NG_9724
	SUITE:Copy route based vti script
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Setup IKE Profile	${IKE_PROFILE_NAME}	${IKE_VERSION}	${IKE_MODE}	${IKE_ENCRYPTION}	${IKE_AUTHENTICATION}
	...	${IKE_DIFFLE}	${IKE_LIFETIME}	${IKE_AUTH_PROTO}	${IKE_ENCRYPTION2}	${IKE_AUTHENTICATION2}	${IKE_PFS_GROUP}
	...	${IKE_LIFETIME2}	${IKE_NUMB_RETR}	${IKE_INTERVAL}	${IKE_ACTION}	${IKE_MTU}	${IKE_CUSTOM_PARAMS}
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_LEFT}	${IKE_PROFILE_NAME}	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	Null	Null	LEFT_SUBNET=${ROUTE_LEFT_SUBNET}	RIGHT_SUBNET=${ROUTE_RIGHT_SUBNET}	CUSTOM_SCRIPT=${TEST_CUSTOM_SCRIPT_NAME}
	SUITE:Add Static Route
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Setup IKE Profile	${IKE_PROFILE_NAME}	${IKE_VERSION}	${IKE_MODE}	${IKE_ENCRYPTION}	${IKE_AUTHENTICATION}
	...	${IKE_DIFFLE}	${IKE_LIFETIME}	${IKE_AUTH_PROTO}	${IKE_ENCRYPTION2}	${IKE_AUTHENTICATION2}	${IKE_PFS_GROUP}
	...	${IKE_LIFETIME2}	${IKE_NUMB_RETR}	${IKE_INTERVAL}	${IKE_ACTION}	${IKE_MTU}	${IKE_CUSTOM_PARAMS}
	SUITE:Setup IPsec Tunnel	${IPSEC_TUNNEL_NAME_RIGHT}	${IKE_PROFILE_NAME}	${RIGHT_ID}	${LEFT_ID}	${RIGHT_IP}	${LEFT_IP}
	...	Null	Null	RIGHT_SUBNET=${ROUTE_RIGHT_SUBNET}	LEFT_SUBNET=${ROUTE_LEFT_SUBNET}	CUSTOM_SCRIPT=${TEST_CUSTOM_SCRIPT_NAME}
	SUITE:Add Static Route
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	IKE_PROFILE=${IKE_PROFILE_NAME}
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	PEER_ID=${RIGHT_ID}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	IKE_PROFILE=${IKE_PROFILE_NAME}
	SUITE:Check IPsec tunnel in Tracking::Network::IPsec	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_RIGHT}	PEER_ID=${LEFT_ID}

Test case to ping left from right through IPsec tunnel with ipsec route based tunnel and Pre-Shared Key
	[Tags]	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL	BUG_NG_9567	BUG_NG_9724
	Wait Until Keyword Succeeds	9x	3s	SUITE:Ping left from right through IPsec tunnel with ipsec route based tunnel and Pre-Shared Key

Test case to ping right from left through IPsec tunnel with ipsec route based tunnel and Pre-Shared Key
	[Tags]	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL	BUG_NG_9567	BUG_NG_9724
	Wait Until Keyword Succeeds	9x	3s	SUITE:Ping right from left through IPsec tunnel with ipsec route based tunnel and Pre-Shared Key

Test case to delete ipsec tunnel both sides with ipsec route based and Pre-Shared Key
	[Tags]	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL	BUG_NG_9567	BUG_NG_9724
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Delete Static Route If Exists
	SUITE:Delete IKE Profile If Exists	${IKE_PROFILE_NAME}
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_LEFT}
	SUITE:Disable Network::IPsec::Global if enabled
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	Wait Until Keyword Succeeds	3x	3s	SUITE:Delete IPsec Tunnel If Exist	${IPSEC_TUNNEL_NAME_RIGHT}
	SUITE:Delete Static Route If Exists
	SUITE:Delete IKE Profile If Exists	${IKE_PROFILE_NAME}
	SUITE:Disable Network::IPsec::Global if enabled
	#CLI
	SUITE:Delete Routes and Restart Interface

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid	ALIAS=HOST_BROWSER_LEFT
	SUITE:Delete All IPsec tunnels If Exist
	SUITE:Delete Static Route If Exists
	SUITE:Delete IKE Profile If Exists	${IKE_PROFILE_NAME}
	SUITE:Disable Network::IPsec::Global if enabled
#	SUITE:Disable IP forward and enable reverse path filtering to strict mode
#	SUITE:Enable IP forward and disable reverse path filtering
	GUI::Basic::Open And Login Nodegrid	PAGE=${HOMEPAGESHARED}	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Delete All IPsec tunnels If Exist
	SUITE:Delete Static Route If Exists
	SUITE:Delete IKE Profile If Exists	${IKE_PROFILE_NAME}
	SUITE:Disable Network::IPsec::Global if enabled
#	SUITE:Disable IP forward and enable reverse path filtering to strict mode
#	SUITE:Enable IP forward and disable reverse path filtering
	#CLI
	SUITE:Delete Routes and Restart Interface
SUITE:Teardown
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid	ALIAS=HOST_BROWSER_LEFT
	SUITE:Delete All IPsec tunnels If Exist
	SUITE:Delete Static Route If Exists
	SUITE:Delete IKE Profile If Exists	${IKE_PROFILE_NAME}
#	SUITE:Disable IP forward and enable reverse path filtering to strict mode
	SUITE:Disable Network::IPsec::Global if enabled
	GUI::Basic::Logout And Close Nodegrid
	GUI::Basic::Open And Login Nodegrid	PAGE=${HOMEPAGESHARED}	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Delete All IPsec tunnels If Exist
	SUITE:Delete Static Route If Exists
	SUITE:Delete IKE Profile If Exists	${IKE_PROFILE_NAME}
#	SUITE:Disable IP forward and enable reverse path filtering to strict mode
	SUITE:Disable Network::IPsec::Global if enabled
	GUI::Basic::Logout And Close Nodegrid
	#CLI
	SUITE:Delete Routes and Restart Interface

SUITE:Disable IP forward and enable reverse path filtering to strict mode
	GUI::Basic::Network::Settings::open tab
	${STATUS}	Run Keyword And Return Status	Checkbox Should Not Be Selected	id=ipv4-ipforward
	Run Keyword If	 not ${STATUS}	SUITE:Unselect Checkbox ipv4 ip forward
	${STATUS}	Run Keyword And Return Status	Checkbox Should Not Be Selected	id=ipv6-forward
	Run Keyword If	 not ${STATUS}	SUITE:Unselect Checkbox ipv6 ip forward
	${STATUS}	Run Keyword And Return Status	List Selection Should Be	//*[@id="rpfilter"]	Strict Mode
	Run Keyword If	 not ${STATUS}	SUITE:Set path filtering to strict mode
	GUI::Basic::Spinner Should Be Invisible

SUITE:Set path filtering to strict mode
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::open tab
	Click Element	//*[@id="rpfilter"]
	Select From List By Label	id=rpfilter	Strict Mode
	GUI::Basic::Save

SUITE:Unselect Checkbox ipv4 ip forward
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::open tab
	Unselect Checkbox	id=ipv4-ipforward
	GUI::Basic::Save

SUITE:Unselect Checkbox ipv6 ip forward
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::open tab
	Unselect Checkbox	id=ipv6-forward
	GUI::Basic::Save

SUITE:Enable IP forward and disable reverse path filtering
	GUI::Basic::Network::Settings::open tab
	Select Checkbox	id=ipv4-ipforward
	Select Checkbox	id=ipv6-forward
	Click Element	//*[@id="rpfilter"]
	Select From List By Label	id=rpfilter	Disabled
	Checkbox Should Be Selected	id=ipv4-ipforward
	Checkbox Should Be Selected	id=ipv6-forward
	Page Should Contain	Disabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Setup IPsec Tunnel
	[Arguments]	${IPSEC_TUNNEL_NAME}	${IKE_PROFILE}	${LEFT_ID}	${RIGHT_ID}	${LEFT_IP}	${RIGHT_IP}
	...	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}	${MONITORING}=No	${ACTION}=Restart IPsec
	...	${LEFT_SUBNET}=${IPV4_LEFT_SUBNET}	${RIGHT_SUBNET}=${IPV4_RIGHT_SUBNET}	${CUSTOM_SCRIPT}=Null
	SUITE:Setup Network::IPsec::Global
	SUITE:Check Network::IPsec::IKE Profile
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	Click Element	//*[@id="ipsecName"]
	Input Text	//*[@id="ipsecName"]	${IPSEC_TUNNEL_NAME}
	Click Element	//*[@id="ipsecInitiate"]
	${OUTPUT}	Get List Items	//*[@id="ipsecInitiate"]
	CLI:Should Contain All	${OUTPUT}	${INITIATE_TUNNEL}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ipsecInitiate"]
	Select From List By Label	id=ipsecInitiate	Start
	Click Element	//*[@id="ipsecProfile"]
	Select From List By Label	id=ipsecProfile	${IKE_PROFILE}
	IF	'${NGVERSION}' >= '5.6' and '${CUSTOM_SCRIPT}' != 'Null'
		Click Element	//select[@id='ipsecUpdownScript']
		Select From List By Label	id=ipsecUpdownScript	${CUSTOM_SCRIPT}
	END
	Input Text	id=ipsecLeftId	${LEFT_ID}
	Input Text	id=ipsecRightId	${RIGHT_ID}
	IF	'${NGVERSION}' >= '5.6' and '${CUSTOM_SCRIPT}' != 'Null'
		Click Element	//*[@id="ipsecLeftAddr"]
		Select From List By Label	//*[@id="ipsecLeftAddr"]	%eth0
		SUITE:Enable VTI
	ELSE
		Click Element	//*[@id="ipsecLeftAddr"]
		Select From List By Label	//*[@id="ipsecLeftAddr"]	IP Address
		Input Text	id=ipsecLeftIpAddr	${LEFT_IP}
		Input Text	id=ipsecLeftSourceIpAddr	${LEFT_SOURCE_IP}
		Input Text	id=ipsecRightSourceIpAddr	${RIGHT_SOURCE_IP}
	END
	Input Text	id=ipsecRightAddr	${RIGHT_IP}
	Input Text	id=ipsecLeftSubnet	${LEFT_SUBNET}
	Input Text	id=ipsecRightSubnet	${RIGHT_SUBNET}
	Input Text	//*[@id="ipsecSecret"]	${IPSEC_SECRET}
	Run Keyword If	'${MONITORING}' == 'Yes'	SUITE:Enable IPsec Tunnel with monitoring	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}	${ACTION}
	GUI::Basic::Save

SUITE:Enable VTI
	Select Checkbox	id=ipsecVtiEnabled
	Input Text	//input[@id='ipsecVtiMark']	-1
	Input Text	//input[@id='ipsecVtiItf']	${VTI_NAME}
	Unselect Checkbox	id=ipsecVtiRoute
	Select Checkbox	id=ipsecVtiShare

SUITE:Enable IPsec Tunnel with monitoring
	[Arguments]	${LEFT_SOURCE_IP}	${RIGHT_SOURCE_IP}	${ACTION}=Restart IPsec
	GUI::Basic::Spinner Should Be Invisible
	${STATUS}	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='ipsecMonitor']
	Run Keyword If	not ${STATUS}	Select Checkbox	//input[@id='ipsecMonitor']
	Input Text	//input[@id='ipsecMonitorSrcIp']	${LEFT_SOURCE_IP}
	Input Text	//input[@id='ipsecMonitorDstIp']	${RIGHT_SOURCE_IP}
	Input Text	//input[@id='ipsecMonitorRetries']	${MONITOR_RETRIES}
	Input Text	//input[@id='ipsecMonitorInterval']	${MONITOR_INTERVAL}
	Click Element	//select[@id='ipsecMonitorAction']
	Select From List By Label	//select[@id='ipsecMonitorAction']	${ACTION}
	Run Keyword If	'${ACTION}' == 'Failover'	Run Keywords	Click Element	//select[@id='ipsecMonitorTunnel']
	...	AND	Select From List By Label	//select[@id='ipsecMonitorTunnel']	${IPSEC_TUNNEL_NAME_LEFT_FAILOVER}
	Input Text	//*[@id="ipsecSecret"]	${IPSEC_SECRET}

#	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
#	GUI::Network::VPN::Open IPsec
#	Click Element	//a[@id='${IPSEC_TUNNEL_NAME_RIGHT}']
#	GUI::Basic::Spinner Should Be Invisible
#	Input Text	//*[@id="ipsecSecret"]	${IPSEC_SECRET}
#	GUI::Basic::Save
#
#SUITE:Disable monitoring and start IPsec tunnel
#	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
#	GUI::Network::VPN::Open IPsec
#	Click Element	//a[@id='${IPSEC_TUNNEL_NAME_LEFT}']
#	GUI::Basic::Spinner Should Be Invisible
#	${STATUS}	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='ipsecMonitor']
#	Run Keyword If	${STATUS}	Unselect Checkbox	//input[@id='ipsecMonitor']
#	GUI::Basic::Save
#	Select Checkbox	css=#${IPSEC_TUNNEL_NAME_LEFT} input
#	GUI::Basic::Spinner Should Be Invisible
#	Click Element	//input[@id='tunnelUp']
#	GUI::Basic::Spinner Should Be Invisible
#	Page Should Contain	Up

SUITE:Edit IPsec Tunnel for RSA Key both sides
	[Arguments]	${SUITE_IPSEC_TUNNEL_NAME_LEFT}	${SUITE_IPSEC_TUNNEL_NAME_RIGHT}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	GUI::Network::VPN::Open IPsec
	Click Element	//a[@id='${SUITE_IPSEC_TUNNEL_NAME_LEFT}']
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//label[normalize-space()='RSA Key']//input[@id='ipsecAuthMethod']
	Click Element	//input[@value='Generate Left Public Key']
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[4]//div[1]//span[1]//button[1]//i[1]
	GUI::Basic::Spinner Should Be Invisible
	${LEFT_PUBLIC_KEY}	Wait Until Keyword Succeeds	5x	3s	SUITE:Get Left RSA key
	Log	\nLEFT_PUBLIC_KEY: \n${LEFT_PUBLIC_KEY}	INFO	console=yes

	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	GUI::Network::VPN::Open IPsec
	Click Element	//a[@id='${SUITE_IPSEC_TUNNEL_NAME_RIGHT}']
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//label[normalize-space()='RSA Key']//input[@id='ipsecAuthMethod']
	Click Element	//input[@value='Generate Left Public Key']
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[4]//div[1]//span[1]//button[1]//i[1]
	GUI::Basic::Spinner Should Be Invisible
	${RIGHT_PUBLIC_KEY}	Wait Until Keyword Succeeds	5x	3s	SUITE:Get Left RSA key
	Log	\nRIGHT_PUBLIC_KEY: \n${RIGHT_PUBLIC_KEY}	INFO	console=yes
	Input Text	//input[@id='ipsecAuthRsaRightKey']	${LEFT_PUBLIC_KEY}
	GUI::Basic::Save

	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	Input Text	//input[@id='ipsecAuthRsaRightKey']	${RIGHT_PUBLIC_KEY}
	GUI::Basic::Save

SUITE:Get Left RSA key
	Click Element	//input[@id='ipsecAuthRsaLeftKey']
	${LEFT_PUBLIC_KEY}=	Get Value	//input[@id='ipsecAuthRsaLeftKey']
	Should Not Be Empty	${LEFT_PUBLIC_KEY}
	[Return]	${LEFT_PUBLIC_KEY}

SUITE:Setup Network::IPsec::Global
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=ipsecGlobal
	GUI::Basic::Spinner Should Be Invisible
	${STATUS}	Run Keyword And Return Status	Checkbox Should Be Selected	id=ipsecGlobalVtiEnabled
	Run Keyword If	 not ${STATUS}	SUITE:Select Checkbox Enable Virtual Tunnel Interface
	${STATUS}	Run Keyword And Return Status	Checkbox Should Be Selected	id=ipsecGlobalLogEnabled
	Run Keyword If	 not ${STATUS}	SUITE:Select Enable Logging
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Reload

SUITE:Select Checkbox Enable Virtual Tunnel Interface
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=ipsecGlobal
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	id=ipsecGlobalVtiEnabled
	GUI::Basic::Save

SUITE:Select Enable Logging
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=ipsecGlobal
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	id=ipsecGlobalLogEnabled
	GUI::Basic::Save

SUITE:Disable Network::IPsec::Global if enabled
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=ipsecGlobal
	GUI::Basic::Spinner Should Be Invisible
	${STATUS}	Run Keyword And Return Status	Checkbox Should Not Be Selected	id=ipsecGlobalVtiEnabled
	Run Keyword If	 not ${STATUS}	SUITE:Unselect Checkbox Enable Virtual Tunnel Interface
	${STATUS}	Run Keyword And Return Status	Checkbox Should Not Be Selected	id=ipsecGlobalLogEnabled
	Run Keyword If	 not ${STATUS}	SUITE:Unselect Enable Logging
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Reload

SUITE:Unselect Checkbox Enable Virtual Tunnel Interface
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=ipsecGlobal
	GUI::Basic::Spinner Should Be Invisible
	Unselect Checkbox	id=ipsecGlobalVtiEnabled
	GUI::Basic::Save

SUITE:Unselect Enable Logging
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=ipsecGlobal
	GUI::Basic::Spinner Should Be Invisible
	Unselect Checkbox	id=ipsecGlobalLogEnabled
	GUI::Basic::Save

SUITE:Check Network::IPsec::IKE Profile
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=ipsecIke
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Cisco_ASA
	Page Should Contain	PaloAlto
	Page Should Contain	nodegrid
	Page Should Contain	IKEv1
	Page Should Contain	IKEv2
	Page Should Contain	Not Applicable
	Page Should Contain	Main
	Page Should Contain	ESP
	Page Should Contain	Authentication Protocol
	Page Should Contain	Mode
	Page Should Contain	IKE Version
	Page Should Contain	Profile Name
	Page Should Contain Button	id=addButton
	Page Should Contain Button	id=delButton
	Page Should Contain	Reload

SUITE:Check ping from ${SOURCE_IP} through IPsec tunnel to ${DESTINATION_IP}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Toolkit::open tab
	Click Element	id=icon_NetworkTools
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	//*[@id="ipaddress"]
	Element Should Be Visible	//*[@id="interface"]
	Input Text	//*[@id="ipaddress"]	${DESTINATION_IP}
	Click Element	id=interface
	Select From List By Label	//*[@id="interface"]	Source IP Address
	Input Text	//*[@id="interfaceIp"]	${SOURCE_IP}
	Click Element	//*[@id="networktools"]/div[2]/div[1]/div/input[1]
	GUI::Basic::Spinner Should Be Invisible
	${COMMAND_OUTPUT}=	Wait Until Keyword Succeeds	1m	3s	SUITE:Get text in command output
	Should Match Regexp	${COMMAND_OUTPUT}	5 packets transmitted, 5 received, 0% packet loss
	GUI::Basic::Cancel

SUITE:Test Teardown
	[Arguments]	${RE-LOGIN}=No
	GUI::Basic::Spinner Should Be Invisible
	${HAS_CANCEL_BUTTON}	Run Keyword And Return Status	Page Should Contain Button	//input[@id='cancelButton']
	Run Keyword If	${HAS_CANCEL_BUTTON}	Run Keyword And Ignore Error	GUI::Basic::Button::Cancel
	IF	'${RE-LOGIN}' == 'Yes'
		Close All Browsers
		Wait until Keyword Succeeds	3x	3s	GUI::Basic::Open And Login Nodegrid	ALIAS=HOST_BROWSER_LEFT
		GUI::Basic::Spinner Should Be Invisible
		Wait until Keyword Succeeds	3x	3s	GUI::Basic::Open And Login Nodegrid	PAGE=${HOMEPAGESHARED}	ALIAS=PEER_BROWSER_RIGHT
		GUI::Basic::Spinner Should Be Invisible
	END

SUITE:Get text in command output
	${COMMAND_OUTPUT}=	Get Text	//*[@id="log"]
	Log	\nCommand Output contents in System::Toolkit::Network Tools: \n${COMMAND_OUTPUT}	INFO	console=yes
	Should Not Match Regexp	${COMMAND_OUTPUT}	Running...wait.
	[Return]	${COMMAND_OUTPUT}

SUITE:Ping right from left
	SUITE:Check ping from ${LEFT_SOURCE_IP} through IPsec tunnel to ${RIGHT_SOURCE_IP}

SUITE:Ping left from right
	SUITE:Check ping from ${RIGHT_SOURCE_IP} through IPsec tunnel to ${LEFT_SOURCE_IP}

SUITE:Delete IPsec Tunnel If Exist
	[Arguments]	${IPSEC_TUNNEL_NAME}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Spinner Should Be Invisible
	${STATUS}	Run Keyword And Return Status	Page Should Contain	${IPSEC_TUNNEL_NAME}
	Run Keyword If	${STATUS}	SUITE:Select Element In The Table And Delete It	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME}
	GUI::Basic::Spinner Should Be Invisible

SUITE:Select Element In The Table And Delete It
	[Arguments]	${IPSEC_TUNNEL_NAME}
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	css=#${IPSEC_TUNNEL_NAME} input
	GUI::Basic::Delete
	Element Should Not Be Visible	css=#${IPSEC_TUNNEL_NAME}
	GUI::Basic::Spinner Should Be Invisible

SUITE:Wait until IPsec tunnel is UP
	[Arguments]	${IPSEC_TUNNEL_NAME}	${IKE_PROFILE}	${AUTHENTICATION_METHOD}=Pre-Shared Key
	Wait Until Keyword Succeeds	30s	3s	SUITE:Check If IPsec tunnel is UP	${IPSEC_TUNNEL_NAME}	${IKE_PROFILE}	${AUTHENTICATION_METHOD}

SUITE:Check If IPsec tunnel is UP
	[Arguments]	${IPSEC_TUNNEL_NAME}	${IKE_PROFILE}	${AUTHENTICATION_METHOD}=Pre-Shared Key
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Reload
	${TABLE_CONTENTS}=	Get Text	ipsecTunnelTable
	Log	\nTable contents in Network::IPsec::Tunnel: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	${IPSEC_TUNNEL_NAME}+\\s+${AUTHENTICATION_METHOD}+.*${IKE_PROFILE}+\\s+Up

SUITE:Check IPsec tunnel in Tracking::Network::IPsec
	[Arguments]	${IPSEC_TUNNEL_NAME}	${PEER_ID}	${AUTHENTICATION_METHOD}=Pre-Shared Key
	GUITracking::Network::Open IPsec
	${TABLE_CONTENTS}=	Get Text	//table[@id='trackingIpsecTable']
	Log	\nTable contents in Tracking::Network::IPsec: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	${IPSEC_TUNNEL_NAME}+.*${PEER_ID}

SUITE:Turn down IPsec tunnel
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::VPN::Open IPsec
	Click Element	//span[normalize-space()='Tunnel']
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	css=#${IPSEC_TUNNEL_NAME_LEFT} input
	Click Element	//input[@id='tunnelDown']
	Wait Until Page Contains	Down	timeout=4m

SUITE:Turn Up IPsec tunnel
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::VPN::Open IPsec
	Click Element	//span[normalize-space()='Tunnel']
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	css=#${IPSEC_TUNNEL_NAME_LEFT} input
	Click Element	//input[@id='tunnelUp']
	Wait Until Page Contains	Up	timeout=4m

SUITE:Reset counters in event list
#	SUITE:Restart IPsec By Root
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Tracking::Event List::Open Tab
	Select Checkbox	//*[@id="thead"]/tr/th[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Enabled	//*[@id="nonAccessControls"]/input	error=Cannot reset counters because Reset Counters button is disable/not found
	Click Element	//*[@id="nonAccessControls"]/input
	GUI::Basic::Spinner Should Be Invisible
	${TABLE_CONTENTS}=	Get Text	eventListTable
	Log	\nTable contents in Tracking::Event List::Statistics: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Not Match Regexp	${TABLE_CONTENTS}	...+.*[1-9]\\d*\\s+\\w+\\s+(\\w+\\s+Event|Event)

SUITE:Restart IPsec By Root
	CLI:Connect As Root	session_alias=admin_left	HOST_DIFF=${HOST}
	CLI:Write	/etc/init.d/ipsec restart
	CLI:Connect As Root	session_alias=admin_right	HOST_DIFF=${HOSTSHARED}
	CLI:Write	/etc/init.d/ipsec restart
	CLI:Close Connection

SUITE:Check for an increase in event counters 162 and 163 in the event list
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Tracking::Event List::Open Tab
	${TABLE_CONTENTS}=	Get Text	eventListTable
	Log	\nTable contents in Tracking::Event List::Statistics: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	162+.*[1-9]\\d*\\s+\\w+\\s+(\\w+\\s+Event|Event)
	Should Match Regexp	${TABLE_CONTENTS}	163+.*[1-9]\\d*\\s+\\w+\\s+(\\w+\\s+Event|Event)

SUITE:Ping right from left by root
	[Arguments]	${SUITE_RIGHT_SOURCE_IP}	${SUITE_LEFT_SOURCE_IP}=${EMPTY}	${SUITE_IPV6}=No	${NO_STICKY}=No
	CLI:Open	session_alias=admin_left	HOST_DIFF=${HOST}
	CLI:Test Ping	${SUITE_RIGHT_SOURCE_IP}	NUM_PACKETS=5	TIMEOUT=30	SOURCE_INTERFACE=${SUITE_LEFT_SOURCE_IP}	IPV6=${SUITE_IPV6}	NO_STICKY=${NO_STICKY}
	CLI:Close Connection

SUITE:Ping left from right by root
	[Arguments]	${SUITE_LEFT_SOURCE_IP}	${SUITE_RIGHT_SOURCE_IP}=${EMPTY}	${SUITE_IPV6}=No	${NO_STICKY}=No
	CLI:Open	session_alias=admin_right	HOST_DIFF=${HOSTSHARED}
	CLI:Test Ping	${SUITE_LEFT_SOURCE_IP}	NUM_PACKETS=5	TIMEOUT=30	SOURCE_INTERFACE=${SUITE_RIGHT_SOURCE_IP}	IPV6=${SUITE_IPV6}	NO_STICKY=${NO_STICKY}
	CLI:Close Connection

SUITE:Setup IKE Profile
	[Arguments]	${SUITE_IKE_PROFILE_NAME}	${SUITE_IKE_VERSION}	${SUITE_IKE_MODE}	${SUITE_IKE_ENCRYPTION}
	...	${SUITE_IKE_AUTHENTICATION}	${SUITE_IKE_DIFFLE}	${SUITE_IKE_LIFETIME}	${SUITE_IKE_AUTH_PROTO}
	...	${SUITE_IKE_ENCRYPTION2}	${SUITE_IKE_AUTHENTICATION2}	${SUITE_IKE_PFS_GROUP}	${SUITE_IKE_LIFETIME2}
	...	${SUITE_IKE_NUMB_RETR}	${SUITE_IKE_INTERVAL}	${SUITE_IKE_ACTION}	${SUITE_IKE_MTU}	${SUITE_IKE_CUSTOM_PARAMS}
	GUI::Network::VPN::Open IPsec::IKE Profile
	GUI::Basic::Add
	Input Text	//input[@id='ipsecIkeName']	${SUITE_IKE_PROFILE_NAME}
	Click Element	//select[@id='ipsecIkeVersion']
	Select From List By Label	id=ipsecIkeVersion	${SUITE_IKE_VERSION}
	#Phase 1
	Click Element	//select[@id='ipsecIkeMode']
	Select From List By Label	id=ipsecIkeMode	${SUITE_IKE_MODE}
	Click Element	//select[@id='ipsecIkeEncryption']
	Select From List By Label	id=ipsecIkeEncryption	${SUITE_IKE_ENCRYPTION}
	Click Element	//select[@id='ipsecIkeAuth']
	Select From List By Label	id=ipsecIkeAuth	${SUITE_IKE_AUTHENTICATION}
	Click Element	//select[@id='ipsecIkeDhgroup']
	Select From List By Label	id=ipsecIkeDhgroup	${SUITE_IKE_DIFFLE}
	Input Text	//input[@id='ipsecIkeLifetime']	${SUITE_IKE_LIFETIME}
	#Phase 2
	Click Element	//select[@id='ipsecIkeAuthProtocol']
	Select From List By Label	id=ipsecIkeAuthProtocol	${SUITE_IKE_AUTH_PROTO}
	Select From List By Label	//div[@id='ipsecIkeEncryption2']//select[@class='selectbox groupFrom']	${SUITE_IKE_ENCRYPTION2}
	Click Element	//div[@id='ipsecIkeEncryption2']//button[@class='listbuilder_btn'][contains(text(),'Add ►')]
	Select From List By Label	//div[@id='ipsecIkeAuth2']//select[@class='selectbox groupFrom']	${SUITE_IKE_AUTHENTICATION2}
	Click Element	//div[@id='ipsecIkeAuth2']//button[@class='listbuilder_btn'][contains(text(),'Add ►')]
	Click Element	//select[@id='ipsecIkeDhgroup2']
	Select From List By Label	id=ipsecIkeDhgroup2	${SUITE_IKE_PFS_GROUP}
	Input Text	//input[@id='ipsecIkeLifetime2']	${SUITE_IKE_LIFETIME2}
	#Advanced Settings
	Set Focus To Element	//input[@id='ipsecIkeDpdEnabled']
	Checkbox Should Not Be Selected	//input[@id='ipsecIkeDpdEnabled']
	Select Checkbox	//input[@id='ipsecIkeDpdEnabled']
	Input Text	//input[@id='ipsecIkeDpdRetry']	${SUITE_IKE_NUMB_RETR}
	Input Text	//input[@id='ipsecIkeDpdInterval']	${SUITE_IKE_INTERVAL}
	Click Element	//select[@id='ipsecIkeDpdAction']
	Select From List By Label	id=ipsecIkeDpdAction	${SUITE_IKE_ACTION}
	Input Text	//input[@id='ipsecIkeMtu']	${SUITE_IKE_MTU}
	Input Text	//textarea[@id='ipsecIkeExtraParams']	${SUITE_IKE_CUSTOM_PARAMS}
	GUI::Basic::Save

SUITE:Delete IKE Profile If Exists
	[Arguments]	${IKE_NAME}
	GUI::Network::VPN::Open IPsec::IKE Profile
	${STATUS}	Run Keyword And Return Status	Page Should Contain	${IKE_NAME}
	Run Keyword If	${STATUS}	SUITE:Select Element In The Table And Delete It	${IKE_NAME}
	GUI::Basic::Spinner Should Be Invisible

SUITE:Add Static Route
	GUI::Basic::Network::Static Routes::open tab
	GUI::Basic::Add
	Select Checkbox	//input[@id='resolveFqdn']
	Input Text	//input[@id='destIp']	zpecloud.com
	Input Text	//input[@id='gateway']	${GATEWAY}
	GUI::Basic::Save

SUITE:Delete Static Route If Exists
	GUI::Basic::Network::Static Routes::open tab
	GUI::Basic::Delete All Rows In Table If Exists	\#netmnt_StroutTable	False

SUITE:Ping from ${SOURCE_IP} through IPsec tunnel to ${DESTINATION_IP} expecting no received packets due route based tunnel
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Toolkit::open tab
	Click Element	id=icon_NetworkTools
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	//*[@id="ipaddress"]
	Element Should Be Visible	//*[@id="interface"]
	Input Text	//*[@id="ipaddress"]	${DESTINATION_IP}
	Click Element	id=interface
	Select From List By Label	//*[@id="interface"]	${VTI_NAME}

	Click Element	//*[@id="networktools"]/div[2]/div[1]/div/input[1]
	GUI::Basic::Spinner Should Be Invisible
	${COMMAND_OUTPUT}=	Wait Until Keyword Succeeds	2m	3s	SUITE:Get text in command output
	Should Match Regexp	${COMMAND_OUTPUT}	5 packets transmitted, 0 received, 100% packet loss, time
	GUI::Basic::Cancel

SUITE:Check on Tracking::Network::IPsec if it has received Bytes
	[Arguments]	${IPSEC_TUNNEL_NAME}	${PEER_ID}
	GUITracking::Network::Open IPsec
	${TABLE_CONTENTS}=	Get Text	//table[@id='trackingIpsecTable']
	Log	\nTable contents in Tracking::Network::IPsec: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	${IPSEC_TUNNEL_NAME}+\\s+ESP+\\s+\\w+\\s+\\w+\\s+\\d+\\s+\\d+:+\\d+:+\\d+\\s+\\d+\\s+420+\\s+0+\\s+${PEER_ID}

SUITE:Check on Tracking::Network::IPsec if it has sent Bytes
	[Arguments]	${IPSEC_TUNNEL_NAME}	${PEER_ID}
	GUITracking::Network::Open IPsec
	sleep		30s
	${TABLE_CONTENTS}=	Get Text	//table[@id='trackingIpsecTable']
	Log	\nTable contents in Tracking::Network::IPsec: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	${IPSEC_TUNNEL_NAME}+\\s+ESP+\\s+\\w+\\s+\\w+\\s+\\d+\\s+\\d+:+\\d+:+\\d+\\s+\\d+\\s+0+\\s+420+\\s+${PEER_ID}

SUITE:Check on Tracking::Network::IPsec if it has no sent and received Bytes
	[Arguments]	${IPSEC_TUNNEL_NAME}	${PEER_ID}
	GUITracking::Network::Open IPsec
	${TABLE_CONTENTS}=	Get Text	//table[@id='trackingIpsecTable']
	Log	\nTable contents in Tracking::Network::IPsec: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	${IPSEC_TUNNEL_NAME}+.*0+\\s+0+\\s+${PEER_ID}

SUITE:Delete All IPsec tunnels If Exist
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Delete All Rows In Table If Exists	\#ipsecTunnelTable	False

SUITE:Copy route based vti script
	CLI:Connect As Root	session_alias=admin_left	HOST_DIFF=${HOST}
	CLI:Write	cp /etc/scripts/ipsec/${DEFAULT_CUSTOM_SCRIPT_NAME} /etc/scripts/ipsec/${TEST_CUSTOM_SCRIPT_NAME}
	CLI:Connect As Root	session_alias=admin_right	HOST_DIFF=${HOSTSHARED}
	CLI:Write	cp /etc/scripts/ipsec/${DEFAULT_CUSTOM_SCRIPT_NAME} /etc/scripts/ipsec/${TEST_CUSTOM_SCRIPT_NAME}
	CLI:Close Connection

SUITE:Delete Routes and Restart Interface
	CLI:Connect As Root	session_alias=admin_left	HOST_DIFF=${HOST}
	CLI:Write	ip link del ${VTI_NAME}
	CLI:Write	nmcli c up ETH0
	CLI:Write	rm /etc/scripts/ipsec/${TEST_CUSTOM_SCRIPT_NAME}
	CLI:Connect As Root	session_alias=admin_right	HOST_DIFF=${HOSTSHARED}
	CLI:Write	ip link del ${VTI_NAME}
	CLI:Write	nmcli c up ETH0
	CLI:Write	rm /etc/scripts/ipsec/${TEST_CUSTOM_SCRIPT_NAME}
	CLI:Close Connection

SUITE:Ping through IPsec tunnel expecting no received packets due route based tunnel
	[Arguments]	${HOST_DIFF}	${IP_ADDRESS}	${NUM_PACKETS}=5	${TIMEOUT}=30	${SOURCE_INTERFACE}=${EMPTY}
	...	${IPV6}=No	${NO_STICKY}=No
	CLI:Open	session_alias=admin_right	HOST_DIFF=${HOST_DIFF}

	${PING_COMMAND}=	Set Variable	ping ${IP_ADDRESS} -W ${TIMEOUT} -c ${NUM_PACKETS} -B
	${PING_COMMAND}=	Run Keyword If	'${NO_STICKY}' != 'No'
	...	Set Variable	ping ${IP_ADDRESS} -W ${TIMEOUT} -c ${NUM_PACKETS}
	${PING_COMMAND}=	Run Keyword If	'${SOURCE_INTERFACE}' != '${EMPTY}'
	...	Catenate	${PING_COMMAND}	${SPACE}-I ${SOURCE_INTERFACE}
	...	ELSE	Set Variable	${PING_COMMAND}
	${PING_COMMAND}=	Run Keyword If	'${IPV6}' != 'No'
	...	Catenate	${PING_COMMAND}	${SPACE}-6
	...	ELSE	Set Variable	${PING_COMMAND}

	Set Client Configuration	prompt=#
	CLI:Write	shell sudo su -
	Set Client Configuration	timeout=120s
	${OUTPUT}=	CLI:Write	${PING_COMMAND}
	Set Client Configuration	prompt=]#
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Write	exit
	CLI:Close Connection
	Should Contain	${OUTPUT}	5 packets transmitted, 0 received, 100% packet loss

SUITE:Ping right from left through IPsec tunnel with ipsec route based tunnel and Pre-Shared Key
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	BUG_NG_9567	BUG_NG_9724
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Turn down IPsec tunnel
	SUITE:Turn Up IPsec tunnel
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	IKE_PROFILE=${IKE_PROFILE_NAME}
	#ping through interfaces is available to v5.2+ by GUI
	#First ping lost packets due handshake
	Run Keyword And Ignore Error	SUITE:Ping right from left by root	${HOSTSHARED}	NO_STICKY=Yes
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	Run Keyword And Ignore Error	SUITE:Ping left from right by root	${HOST}	NO_STICKY=Yes
#	Different behavior through GUI network tools it might be a bug
#	SUITE:Ping from ${HOST} through IPsec tunnel to ${HOSTSHARED} expecting no received packets due route based tunnel
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Ping right from left by root	${HOSTSHARED}	NO_STICKY=Yes
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Check on Tracking::Network::IPsec if it has no sent and received Bytes	${IPSEC_TUNNEL_NAME_RIGHT}	${LEFT_ID}
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
#	Different behavior through GUI network tools it might be a bug
#	SUITE:Ping from ${HOST} through IPsec tunnel to ${GOOGLE_IP} expecting no received packets due route based tunnel
	SUITE:Ping through IPsec tunnel expecting no received packets due route based tunnel	${HOST}	${GOOGLE_IP}	NO_STICKY=Yes
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	SUITE:Check on Tracking::Network::IPsec if it has received Bytes	${IPSEC_TUNNEL_NAME_RIGHT}	${LEFT_ID}

SUITE:Ping left from right through IPsec tunnel with ipsec route based tunnel and Pre-Shared Key
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Turn down IPsec tunnel
	SUITE:Turn Up IPsec tunnel
	SUITE:Wait until IPsec tunnel is UP	IPSEC_TUNNEL_NAME=${IPSEC_TUNNEL_NAME_LEFT}	IKE_PROFILE=${IKE_PROFILE_NAME}
	#ping through interfaces is available to v5.2+ by GUI
	#First ping lost packets due handshake
	Run Keyword And Ignore Error	SUITE:Ping right from left by root	${HOSTSHARED}	NO_STICKY=Yes
#	Different behavior through GUI network tools it might be a bug
#	SUITE:Ping from ${HOSTSHARED} through IPsec tunnel to ${HOST} expecting no received packets due route based tunnel
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
	Run Keyword And Ignore Error	SUITE:Ping left from right by root	${HOST}	NO_STICKY=Yes
	SUITE:Ping left from right by root	${HOST}	NO_STICKY=Yes
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Check on Tracking::Network::IPsec if it has no sent and received Bytes	${IPSEC_TUNNEL_NAME_LEFT}	${RIGHT_ID}
	GUI::Basic::Switch Browser	ALIAS=PEER_BROWSER_RIGHT
#	Different behavior through GUI network tools it might be a bug
#	SUITE:Ping from ${HOSTSHARED} through IPsec tunnel to ${GOOGLE_IP} expecting no received packets due route based tunnel
	SUITE:Ping through IPsec tunnel expecting no received packets due route based tunnel	${HOST}	${GOOGLE_IP}	NO_STICKY=Yes
	GUI::Basic::Switch Browser	ALIAS=HOST_BROWSER_LEFT
	SUITE:Check on Tracking::Network::IPsec if it has sent Bytes	${IPSEC_TUNNEL_NAME_LEFT}	${RIGHT_ID}