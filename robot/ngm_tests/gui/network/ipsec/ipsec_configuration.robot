*** Settings ***
Documentation	Configuration test cases for ipsec tunnel through GUI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EXCLUDEIN4_2	NON-CRITICAL	BUG_NG_9570

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${TEST_TUNNEL_NAME}	TEST_AUTOMATION_IPSEC_TUNNEL
${TEST_REMOTE_RIGHT_ADDRESS}	${HOSTPEER}
@{TEST_ALL_TUNNEL_NAMES}	${TEST_TUNNEL_NAME}	${WORD}	${WORD_AND_NUMBER}	${WORDS_UNDERSCORE}
@{TEST_IPSEC_INITIATE_VALID_VALUES}	Start	On Demand	Ignore
@{TEST_IPSEC_IKE_PROFILE_VALID_VALUES}	nodegrid	PaloAlto	Cisco_ASA
@{TEST_IPSEC_LEFT_ADDRESS_VALID_VALUES}	%defaultroute	%any	%eth0	%eth1	%loopback	%loopback0	%sit0	%lo	IP Address
@{TEST_IPSEC_CUSTOM_SCRIPT_VALID_VALUES}	${EMPTY}	route_based_vti_updown.sh
@{VALID_NAMES}	${WORD}	${WORD_AND_NUMBER}	${WORDS_UNDERSCORE}
@{VALID_SECRETS}	${VALID_SECRET_SIZE}	${WORD_AND_NUMBER}	${WORDS_UNDERSCORE}
${VALID_LEFTKEY}	0sAwEAAeDJuaLEtY/XwOKZMLK3STTKDHEUwtVJCv1Cr3F1DG6/iN8Tyhca5Rlxq7n1ERIjGRE/gBRFWP6epIHjbeqU5+qagD2EA8+rAz79eE1U3wqryUnJVHZTiXJTVoVVzXlhVh0YUdKv74Pv6wduKJDBao5IbMWHlZTR1q9OvLGeTVJBtHYn8IgRTNttQuybs9cioZ7Z07h4+1iqMtlLKyUIB1GwIFJVAC+n2DqIqLuou22CAAwVZeaP2ohoe5HXn6sj3U6IxRr9uVBNsL54HaooIKilH2zQJW/T8xzHl4NFnCa7LnNnwhcGHaWJmo+MsUW7g+2MDMh+zjpF+hdlfSs0GYUKnrEUyVBLFQsZgUszeu+rbaNcS33Q1+xFnAGg7+iGLxC86akrIlBadMn9D5pmjah/HRKoq/IgT4GCIZwt+xUVRYzY3xoly+geK2Btut2fFYYU3hU1AmsV7Dp16LAMDcfsfhEzlZVrxn5C6om0cj0qHnYK/hxBo0IWpXQ4FgyUMNk7If4ql8kK1OBxc/WmmW5ah4YyQaMEqyRzISTw6HfsmlXj1g7CM3yqoOevVzYWZ9cBpGa5IrsCkDhXVvHRnHV313h6mqh3WKGVWVUP544qc9pzdQzHehu1APjD
${VALID_RIGHTKEY}	0sAwEAAaZKwGHOGsXzaiMMwpXmBRNgizBj/ujv9XiNrURPag+2ooZ3MZglVPAazcl2FseHT9Tbi1nTus/J+4Z9OQs4QkTEHw7io47XYBcIcd/BNJhfQDZziAZ3BBF2VMEODV2VLn4lhY97RkSozROgjyaEJRYqfmj6uBHbFUdwSkVkBcQUvvuBlrwFGZDlJ6XFGq13bHQJR0ArkXIHAfpCTMKEklVVImJIAErcgTxXd3US9Puuh0jKM7YKY+3rvHWTVVWiq8JGPqvvFPDEbYL9yuP7LuB6bw4b8EQYRIZDTpjHemDMn4oN9KZVQ9VjabunwEsv8jEvVvtSiipzQgevIXWheCB/EG9vRIglAykpzvcYrM4ju0zXtjnsfnW4WtmBt/KQEYws/JsaaNEeU8mjswloHxPZyKzDmGldALWreAWQwbFx9IEXVVS9e7EyRBgF1NfHhD3VinbTxy5V1arpb34ZXiC+ByKXi3+IVc81teUnBJPyL5XBRuT3A6Z50n3brdkrHcm8UiZiQw==
@{VALID_LEFTIDS}	${EMPTY}	${ONE_POINTS}	${WORDS_WITH_BARS}
@{VALID_RIGHTIDS}	${POINTS}	${WORDS_SPACES}	${ELEVEN_POINTS}
@{VALID_LEFTIPS}	192.168.2.37	192.168.7.68	12.250.123.40
@{VALID_RIGHTIPS}	192.168.6.97	192.168.3.200	10.254.166.30
@{VALID_LEFTSOURCEIPS}	10.0.0.0	45.30.254.255	1.4.1.1
@{VALID_RIGHTSOURCEIPS}	90.90.90.90	255.200.100.1	5.56.1.39
@{VALID_LEFTSUBNETS}	10.0.0.0/24	45.30.254.255/30	1.4.1.1/15
@{VALID_RIGHTSUBNETS}	90.90.90.0/23	255.200.100.0/32	5.56.1.0/10

*** Test Cases ***
Test case using valid values for fields and tunnel must be Down
	FOR	${INDEX}	${VALID_NAME}	IN ENUMERATE	@{VALID_NAMES}
		GUI::Network::VPN::Open IPsec
		GUI::Basic::Add
		Input Text	//input[@id='ipsecRightAddr']	${TEST_REMOTE_RIGHT_ADDRESS}
		Input Text	//input[@id='ipsecName']	${VALID_NAME}
		Click Element	//select[@id='ipsecInitiate']
		Select From List By Label	id=ipsecInitiate	${TEST_IPSEC_INITIATE_VALID_VALUES}[${INDEX}]
		Click Element	//select[@id='ipsecProfile']
		Select From List By Label	id=ipsecProfile	${TEST_IPSEC_IKE_PROFILE_VALID_VALUES}[${INDEX}]
		IF	'${NGVERSION}' >= '5.6' and '${INDEX}' <= '1'
			Click Element	//select[@id='ipsecUpdownScript']
			Select From List By Label	id=ipsecUpdownScript	${TEST_IPSEC_CUSTOM_SCRIPT_VALID_VALUES}[${INDEX}]
		END
		Input Text	//input[@id='ipsecLeftId']	${VALID_LEFTIDS}[${INDEX}]
		Input Text	//input[@id='ipsecRightId']	${VALID_RIGHTIDS}[${INDEX}]
		Click Element	//select[@id='ipsecLeftAddr']
		Select From List By Label	id=ipsecLeftAddr	IP Address
		Input Text	//input[@id='ipsecLeftIpAddr']	${VALID_LEFTIPS}[${INDEX}]
		Input Text	//input[@id='ipsecRightAddr']	${VALID_RIGHTIPS}[${INDEX}]
		Input Text	//input[@id='ipsecLeftSourceIpAddr']	${VALID_LEFTSOURCEIPS}[${INDEX}]
		Input Text	//input[@id='ipsecRightSourceIpAddr']	${VALID_RIGHTSOURCEIPS}[${INDEX}]
		Input Text	//input[@id='ipsecLeftSubnet']	${VALID_LEFTSUBNETS}[${INDEX}]
		Input Text	//input[@id='ipsecRightSubnet']	${VALID_RIGHTSUBNETS}[${INDEX}]
		Input Text	//input[@id='ipsecSecret']	${VALID_SECRETS}[${INDEX}]
		IF	'${VALID_NAME}' == '${WORDS_UNDERSCORE}'
			Click Element	//label[normalize-space()='RSA Key']//input[@id='ipsecAuthMethod']
			Input Text	//input[@id='ipsecAuthRsaLeftKey']	${VALID_LEFTKEY}
			Input Text	//input[@id='ipsecAuthRsaRightKey']	${VALID_RIGHTKEY}
		END
		GUI::Basic::Save
		GUI::Basic::Page Should Not Contain Error
	END
	${TABLE_CONTENTS}=	Get Text	ipsecTunnelTable
	Log	\nTable contents in Tracking::Event List::Statistics: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	${WORD}+\\s+Pre-Shared Key+\\s+.*nodegrid+\\s+Down
	Should Match Regexp	${TABLE_CONTENTS}	${WORD_AND_NUMBER}+\\s+Pre-Shared Key+\\s+.*PaloAlto+\\s+Down
	Should Match Regexp	${TABLE_CONTENTS}	${WORDS_UNDERSCORE}+\\s+RSA Key+\\s+.*Cisco_ASA+\\s+Down
	[Teardown]	SUITE:Teardown For Add Tunnel Page	@{TEST_ALL_TUNNEL_NAMES}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid

SUITE:Teardown
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table If Exists	ipsecTunnelTable	${TEST_ALL_TUNNEL_NAMES}	HAS_ALERT=${FALSE}
	GUI::Basic::Logout And Close Nodegrid

SUITE:Teardown For Add Tunnel Page
	[Arguments]	@{TUNNEL_NAMES}
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Ignore Error	GUI::Basic::Button::Cancel
	GUI::Basic::Delete Rows In Table If Exists	ipsecTunnelTable	${TUNNEL_NAMES}	HAS_ALERT=${FALSE}