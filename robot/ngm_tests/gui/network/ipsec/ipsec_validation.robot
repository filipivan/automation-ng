*** Settings ***
Documentation	Validation test cases for ipsec tunnel through GUI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EXCLUDEIN4_2	NON-CRITICAL	NEED-REVIEW	BUG_NG_9570

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${TEST_TUNNEL_NAME}	TEST_AUTOMATION_IPSEC_TUNNEL
${TEST_PSK_SECRET}	${QA_PASSWORD}
${TEST_SOURCE_LEFT_ADDRESS}	${HOST}
${TEST_REMOTE_RIGHT_ADDRESS}	${HOSTPEER}
@{TEST_ALL_TUNNEL_NAMES}	${TEST_TUNNEL_NAME}	${WORD}	${WORD_AND_NUMBER}	${WORDS_UNDERSCORE}
@{TEST_IPSEC_INITIATE_VALID_VALUES}	Start	On Demand	Ignore
@{TEST_IPSEC_IKE_PROFILE_VALID_VALUES}	nodegrid	PaloAlto	Cisco_ASA
@{TEST_IPSEC_LEFT_ADDRESS_VALID_VALUES}	%defaultroute	%any	%eth0	%eth1	%loopback	%loopback0	%sit0	%lo	IP Address
@{TEST_IPSEC_CUSTOM_SCRIPT_VALID_VALUES}	${EMPTY}	route_based_vti_updown.sh
@{TEST_IPSEC_ACTION_VALID_VALUES}	Restart IPsec	Restart Tunnel	Failover
${TEST_VTI_INTERFACE}	TEST_AUTOMATION_VTI_INTERFACE_NAME
@{TEST_IPSEC_IKE_VERSION_VALID_VALUES}	IKEv1	IKEv2
${TEST_IKE_PROFILE_NAME}	TEST_AUTOMATION_IKE_PROFILE_NAME
@{TEST_IKE_PROFILE_NAMES}	${TEST_IKE_PROFILE_NAME}	${WORD}	${WORD_AND_NUMBER}	${WORDS_UNDERSCORE}
@{TEST_IPSEC_IKE_MODE_VALID_VALUES}	Aggressive	Main
@{TEST_IPSEC_IKE_ENCRYPTION_VALID_VALUES}	3DES	AES	AES192	AES256	AES-CBC	AES-CBC192	AES-CBC256	AES-CTR	AES-CTR192	AES-CTR256	AES-GCM	AES-GCM192	AES-GCM256
@{TEST_IPSEC_IKE_AUTHENTICATION_VALID_VALUES}	SHA1	SHA256	SHA384	SHA512	MD5
@{TEST_IPSEC_IKE_DIFFLE_VALID_VALUES}	Group 2	Group 5	Group 14	Group 15	Group 16	Group 17	Group 18	Group 19	Group 20	Group 21	Group 31
@{TEST_IPSEC_IKE_DIFFLE_VALID_VALUES_5_4+}	Group 2 (MODP1024)	Group 5 (MODP1536)	Group 14 (MODP2048)	Group 15 (MODP3072)	Group 16 (MODP4096)	Group 17 (MODP6144)	Group 18 (MODP8192)	Group 19 (ECP256)	Group 20 (ECP384)	Group 21 (ECP521)	Group 31 (CURVE25519)
@{TEST_IPSEC_IKE_AUTH_PROTO_VALID_VALUES}	ESP	AH
@{TEST_IPSEC_IKE_ENCRYPTION2_VALID_VALUES}	@{TEST_IPSEC_IKE_ENCRYPTION_VALID_VALUES}	AES-CCM	AES-CCM192	AES-CCM256
@{TEST_IPSEC_IKE_AUTHENTICATION2_VALID_VALUES}	@{TEST_IPSEC_IKE_AUTHENTICATION_VALID_VALUES}	Null
@{TEST_IPSEC_IKE_FPS_GROUP_VALID_VALUES}	None	@{TEST_IPSEC_IKE_DIFFLE_VALID_VALUES}
@{TEST_IPSEC_IKE_FPS_GROUP_VALID_VALUES_5_4+}	None	@{TEST_IPSEC_IKE_DIFFLE_VALID_VALUES_5_4+}
@{TEST_IPSEC_IKE_ACTION_VALID_VALUES}	hold	clear	restart
@{TEST_IPSEC_LEFT_ID}	@NGL	@NGIPSECL2
@{TEST_IPSEC_RIGHT_ID}	@NGR	@NGIPSECR2

*** Test Cases ***
Test case for multiple tunnels with the same name
	@{TEST_TUNNEL_NAMES}=	Create List	${TEST_TUNNEL_NAME}
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	Input Text	//input[@id='ipsecSecret']	${TEST_PSK_SECRET}
	Input Text	//input[@id='ipsecRightAddr']	${TEST_REMOTE_RIGHT_ADDRESS}
	Input Text	//input[@id='ipsecName']	${TEST_TUNNEL_NAME}
	Input Text	//input[@id='ipsecLeftId']	${TEST_IPSEC_LEFT_ID}[0]
	Input Text	//input[@id='ipsecRightId']	${TEST_IPSEC_RIGHT_ID}[0]
	GUI::Basic::Save
	Page Should Contain Element	ipsecTunnelTable
	Page Should Contain	${TEST_TUNNEL_NAME}
	GUI::Basic::Add
	Input Text	//input[@id='ipsecSecret']	${TEST_PSK_SECRET}
	Input Text	//input[@id='ipsecRightAddr']	${TEST_REMOTE_RIGHT_ADDRESS}
	Input Text	//input[@id='ipsecName']	${TEST_TUNNEL_NAME}
	Input Text	//input[@id='ipsecLeftId']	${TEST_IPSEC_LEFT_ID}[1]
	Input Text	//input[@id='ipsecRightId']	${TEST_IPSEC_RIGHT_ID}[1]
	Click Element	//*[@id='saveButton']
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Entry already exists.
	[Teardown]	SUITE:Teardown For Add Tunnel Page	${TEST_TUNNEL_NAME}

Test case invalid values for field=Name
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	Input Text	//input[@id='ipsecSecret']	${TEST_PSK_SECRET}
	Input Text	//input[@id='ipsecRightAddr']	${TEST_REMOTE_RIGHT_ADDRESS}
	Input Text	//input[@id='ipsecLeftId']	${TEST_IPSEC_LEFT_ID}[0]
	Input Text	//input[@id='ipsecRightId']	${TEST_IPSEC_RIGHT_ID}[0]
	SUITE:Input invalid value and check error message	//input[@id='ipsecName']	${EMPTY}	Field must not be empty.
	SUITE:Input invalid value and check error message	//input[@id='ipsecName']	${EXCEEDED}	Exceeded the maximum size for this field.
	SUITE:Input invalid value and check error message	//input[@id='ipsecName']	${NUMBER_AND_POINTS}	This field contains invalid characters.
	[Teardown]	SUITE:Teardown For Add Tunnel Page	@{TEST_ALL_TUNNEL_NAMES}

Test case valid values for field=Initiate Tunnel
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	FOR	${TEST_IPSEC_INITIATE_VALID}	IN	@{TEST_IPSEC_INITIATE_VALID_VALUES}
		Click Element	//*[@id="ipsecInitiate"]
		Select From List By Label	id=ipsecInitiate	${TEST_IPSEC_INITIATE_VALID}
		GUI::Basic::Page Should Not Contain Error
	END
	[Teardown]	SUITE:Teardown For Add Tunnel Page	@{TEST_ALL_TUNNEL_NAMES}

Test case valid values for field=IKE Profile
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	FOR	${TEST_IPSEC_IKE_PROFILE_VALID}	IN	@{TEST_IPSEC_IKE_PROFILE_VALID_VALUES}
		Click Element	//select[@id='ipsecProfile']
		Select From List By Label	id=ipsecProfile	${TEST_IPSEC_IKE_PROFILE_VALID}
		GUI::Basic::Page Should Not Contain Error
	END
	[Teardown]	SUITE:Teardown For Add Tunnel Page	@{TEST_ALL_TUNNEL_NAMES}

Test case valid values for field=Custom Up/Down Script
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	FOR	${TEST_IPSEC_CUSTOM_SCRIPT_VALID}	IN	@{TEST_IPSEC_CUSTOM_SCRIPT_VALID_VALUES}
		Click Element	//select[@id='ipsecUpdownScript']
		Select From List By Label	id=ipsecUpdownScript	${TEST_IPSEC_CUSTOM_SCRIPT_VALID}
		GUI::Basic::Page Should Not Contain Error
		Page Should Contain	Scripts are located in: /etc/scripts/ipsec
	END
	[Teardown]	SUITE:Teardown For Add Tunnel Page	@{TEST_ALL_TUNNEL_NAMES}

Test case valid values for field=Left Address
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	FOR	${TEST_IPSEC_LEFT_ADDRESS_VALID}	IN	@{TEST_IPSEC_LEFT_ADDRESS_VALID_VALUES}
		Click Element	//select[@id='ipsecLeftAddr']
		Select From List By Label	id=ipsecLeftAddr	${TEST_IPSEC_LEFT_ADDRESS_VALID}
		GUI::Basic::Page Should Not Contain Error
	END
	[Teardown]	SUITE:Teardown For Add Tunnel Page	@{TEST_ALL_TUNNEL_NAMES}

Test case invalid values for field=Secret
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	Input Text	//input[@id='ipsecName']	${TEST_TUNNEL_NAME}
	Input Text	//input[@id='ipsecRightAddr']	${TEST_REMOTE_RIGHT_ADDRESS}
	Input Text	//input[@id='ipsecLeftId']	${TEST_IPSEC_LEFT_ID}[0]
	Input Text	//input[@id='ipsecRightId']	${TEST_IPSEC_RIGHT_ID}[0]
	SUITE:Input invalid value and check error message	//input[@id='ipsecSecret']	${EMPTY}	Field must not be empty.
	SUITE:Input invalid value and check error message	//input[@id='ipsecSecret']	${EXCEEDED}	Validation error.	#Exceeded the maximum size for this field.
	[Teardown]	SUITE:Teardown For Add Tunnel Page	@{TEST_ALL_TUNNEL_NAMES}

Test case invalid values for field=Left Public Key and Right Public Key
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	Input Text	//input[@id='ipsecName']	${TEST_TUNNEL_NAME}
	Input Text	//input[@id='ipsecRightAddr']	${TEST_REMOTE_RIGHT_ADDRESS}
	Input Text	//input[@id='ipsecLeftId']	${TEST_IPSEC_LEFT_ID}[0]
	Input Text	//input[@id='ipsecRightId']	${TEST_IPSEC_RIGHT_ID}[0]
	Click Element	//label[normalize-space()='RSA Key']//input[@id='ipsecAuthMethod']
	SUITE:Input invalid value and check error message	//input[@id='ipsecAuthRsaLeftKey']	${EXCEEDED}	Validation error.	RSA_KEY=Yes	2_RSA_LOCATOR=//input[@id='ipsecAuthRsaRightKey']
	SUITE:Input invalid value and check error message	//input[@id='ipsecAuthRsaLeftKey']	${NUMBER_AND_POINTS}	Validation error.	RSA_KEY=Yes	2_RSA_LOCATOR=//input[@id='ipsecAuthRsaRightKey']
	[Teardown]	SUITE:Teardown For Add Tunnel Page	@{TEST_ALL_TUNNEL_NAMES}

Test case invalid values for field=Left ID
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	Input Text	//input[@id='ipsecName']	${TEST_TUNNEL_NAME}
	Input Text	//input[@id='ipsecRightAddr']	${TEST_REMOTE_RIGHT_ADDRESS}
	Input Text	//input[@id='ipsecSecret']	${TEST_PSK_SECRET}
	Input Text	//input[@id='ipsecLeftId']	${TEST_IPSEC_LEFT_ID}[0]
	Input Text	//input[@id='ipsecRightId']	${TEST_IPSEC_RIGHT_ID}[0]
	SUITE:Input invalid value and check error message	//input[@id='ipsecLeftId']	${EXCEEDED}	Exceeded the maximum size for this field.
	[Teardown]	SUITE:Teardown For Add Tunnel Page	@{TEST_ALL_TUNNEL_NAMES}

Test case invalid values for field=Right ID
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	Input Text	//input[@id='ipsecName']	${TEST_TUNNEL_NAME}
	Input Text	//input[@id='ipsecRightAddr']	${TEST_REMOTE_RIGHT_ADDRESS}
	Input Text	//input[@id='ipsecSecret']	${TEST_PSK_SECRET}
	Input Text	//input[@id='ipsecLeftId']	${TEST_IPSEC_LEFT_ID}[0]
	Input Text	//input[@id='ipsecRightId']	${TEST_IPSEC_RIGHT_ID}[0]
	SUITE:Input invalid value and check error message	//input[@id='ipsecRightId']	${EXCEEDED}	Exceeded the maximum size for this field.
	[Teardown]	SUITE:Teardown For Add Tunnel Page	@{TEST_ALL_TUNNEL_NAMES}

Test case invalid values for field=Left IP Address
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	Input Text	//input[@id='ipsecName']	${TEST_TUNNEL_NAME}
	Input Text	//input[@id='ipsecRightAddr']	${TEST_REMOTE_RIGHT_ADDRESS}
	Input Text	//input[@id='ipsecSecret']	${TEST_PSK_SECRET}
	Input Text	//input[@id='ipsecLeftId']	${TEST_IPSEC_LEFT_ID}[0]
	Input Text	//input[@id='ipsecRightId']	${TEST_IPSEC_RIGHT_ID}[0]
	Click Element	//select[@id='ipsecLeftAddr']
	Select From List By Label	id=ipsecLeftAddr	IP Address
	SUITE:Input invalid value and check error message	//input[@id='ipsecLeftIpAddr']	${EMPTY}	Validation error.	#Field must not be empty.
	SUITE:Input invalid value and check error message	//input[@id='ipsecLeftIpAddr']	${EXCEEDED}	Validation error.	#Exceeded the maximum size for this field.
	SUITE:Input invalid value and check error message	//input[@id='ipsecLeftIpAddr']	${NUMBER_AND_POINTS}	Validation error.	#This field contains invalid characters.
	[Teardown]	SUITE:Teardown For Add Tunnel Page	@{TEST_ALL_TUNNEL_NAMES}

Test case invalid values for field=Right Address
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	Input Text	//input[@id='ipsecName']	${TEST_TUNNEL_NAME}
	Input Text	//input[@id='ipsecSecret']	${TEST_PSK_SECRET}
	Input Text	//input[@id='ipsecLeftId']	${TEST_IPSEC_LEFT_ID}[0]
	Input Text	//input[@id='ipsecRightId']	${TEST_IPSEC_RIGHT_ID}[0]
	SUITE:Input invalid value and check error message	//input[@id='ipsecRightAddr']	${EMPTY}	Validation error.	#Field must not be empty.
	SUITE:Input invalid value and check error message	//input[@id='ipsecRightAddr']	${EXCEEDED}	Validation error.	#Exceeded the maximum size for this field.
	SUITE:Input invalid value and check error message	//input[@id='ipsecRightAddr']	${NUMBER_AND_POINTS}	Validation error.	#This field contains invalid characters.
	[Teardown]	SUITE:Teardown For Add Tunnel Page	@{TEST_ALL_TUNNEL_NAMES}

Test case invalid values for field=Left Source IP Address
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	Input Text	//input[@id='ipsecName']	${TEST_TUNNEL_NAME}
	Input Text	//input[@id='ipsecRightAddr']	${TEST_REMOTE_RIGHT_ADDRESS}
	Input Text	//input[@id='ipsecSecret']	${TEST_PSK_SECRET}
	Input Text	//input[@id='ipsecLeftId']	${TEST_IPSEC_LEFT_ID}[0]
	Input Text	//input[@id='ipsecRightId']	${TEST_IPSEC_RIGHT_ID}[0]
	SUITE:Input invalid value and check error message	//input[@id='ipsecLeftSourceIpAddr']	${EXCEEDED}	Invalid IP address.	#Exceeded the maximum size for this field.
	SUITE:Input invalid value and check error message	//input[@id='ipsecLeftSourceIpAddr']	${NUMBER_AND_POINTS}	Invalid IP address.	#This field contains invalid characters.
	[Teardown]	SUITE:Teardown For Add Tunnel Page	@{TEST_ALL_TUNNEL_NAMES}

Test case invalid values for field=Right Source IP Address
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	Input Text	//input[@id='ipsecName']	${TEST_TUNNEL_NAME}
	Input Text	//input[@id='ipsecRightAddr']	${TEST_REMOTE_RIGHT_ADDRESS}
	Input Text	//input[@id='ipsecSecret']	${TEST_PSK_SECRET}
	Input Text	//input[@id='ipsecLeftId']	${TEST_IPSEC_LEFT_ID}[0]
	Input Text	//input[@id='ipsecRightId']	${TEST_IPSEC_RIGHT_ID}[0]
	SUITE:Input invalid value and check error message	//input[@id='ipsecRightSourceIpAddr']	${EXCEEDED}	Invalid IP address.	#Exceeded the maximum size for this field.
	SUITE:Input invalid value and check error message	//input[@id='ipsecRightSourceIpAddr']	${NUMBER_AND_POINTS}	Invalid IP address.	#This field contains invalid characters.
	[Teardown]	SUITE:Teardown For Add Tunnel Page	@{TEST_ALL_TUNNEL_NAMES}

Test case invalid values for field=Left Subnet
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	Input Text	//input[@id='ipsecName']	${TEST_TUNNEL_NAME}
	Input Text	//input[@id='ipsecRightAddr']	${TEST_REMOTE_RIGHT_ADDRESS}
	Input Text	//input[@id='ipsecSecret']	${TEST_PSK_SECRET}
	Input Text	//input[@id='ipsecLeftId']	${TEST_IPSEC_LEFT_ID}[0]
	Input Text	//input[@id='ipsecRightId']	${TEST_IPSEC_RIGHT_ID}[0]
	SUITE:Input invalid value and check error message	//input[@id='ipsecLeftSubnet']	${EXCEEDED}	Invalid subnet	#Exceeded the maximum size for this field.
	SUITE:Input invalid value and check error message	//input[@id='ipsecLeftSubnet']	${NUMBER_AND_POINTS}	Invalid subnet	#This field contains invalid characters.
	[Teardown]	SUITE:Teardown For Add Tunnel Page	@{TEST_ALL_TUNNEL_NAMES}

Test case invalid values for field=Right Subnet
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	Input Text	//input[@id='ipsecName']	${TEST_TUNNEL_NAME}
	Input Text	//input[@id='ipsecRightAddr']	${TEST_REMOTE_RIGHT_ADDRESS}
	Input Text	//input[@id='ipsecSecret']	${TEST_PSK_SECRET}
	Input Text	//input[@id='ipsecLeftId']	${TEST_IPSEC_LEFT_ID}[0]
	Input Text	//input[@id='ipsecRightId']	${TEST_IPSEC_RIGHT_ID}[0]
	SUITE:Input invalid value and check error message	//input[@id='ipsecRightSubnet']	${EXCEEDED}	Invalid subnet	#Exceeded the maximum size for this field.
	SUITE:Input invalid value and check error message	//input[@id='ipsecRightSubnet']	${NUMBER_AND_POINTS}	Invalid subnet	#This field contains invalid characters.
	[Teardown]	SUITE:Teardown For Add Tunnel Page	@{TEST_ALL_TUNNEL_NAMES}

Test case invalid values for monitoring field=Source IP Address
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	Input Text	//input[@id='ipsecName']	${TEST_TUNNEL_NAME}
	Input Text	//input[@id='ipsecRightAddr']	${TEST_REMOTE_RIGHT_ADDRESS}
	Input Text	//input[@id='ipsecSecret']	${TEST_PSK_SECRET}
	Input Text	//input[@id='ipsecLeftId']	${TEST_IPSEC_LEFT_ID}[0]
	Input Text	//input[@id='ipsecRightId']	${TEST_IPSEC_RIGHT_ID}[0]
	Select Checkbox	//input[@id='ipsecMonitor']
	Input Text	//input[@id='ipsecMonitorDstIp']	${TEST_REMOTE_RIGHT_ADDRESS}
	SUITE:Input invalid value and check error message	//input[@id='ipsecMonitorSrcIp']	${EMPTY}	Invalid IP address.	#Field must not be empty.
	SUITE:Input invalid value and check error message	//input[@id='ipsecMonitorSrcIp']	${EXCEEDED}	Invalid IP address.	#Exceeded the maximum size for this field.
	SUITE:Input invalid value and check error message	//input[@id='ipsecMonitorSrcIp']	${NUMBER_AND_POINTS}	Invalid IP address.	#This field contains invalid characters.
	[Teardown]	SUITE:Teardown For Add Tunnel Page	@{TEST_ALL_TUNNEL_NAMES}

Test case invalid values for monitoring field=Destination IP Address
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	Input Text	//input[@id='ipsecName']	${TEST_TUNNEL_NAME}
	Input Text	//input[@id='ipsecRightAddr']	${TEST_REMOTE_RIGHT_ADDRESS}
	Input Text	//input[@id='ipsecSecret']	${TEST_PSK_SECRET}
	Input Text	//input[@id='ipsecLeftId']	${TEST_IPSEC_LEFT_ID}[0]
	Input Text	//input[@id='ipsecRightId']	${TEST_IPSEC_RIGHT_ID}[0]
	Select Checkbox	//input[@id='ipsecMonitor']
	Input Text	//input[@id='ipsecMonitorSrcIp']	${TEST_SOURCE_LEFT_ADDRESS}
	SUITE:Input invalid value and check error message	//input[@id='ipsecMonitorDstIp']	${EMPTY}	Invalid IP address.	#Field must not be empty.
	SUITE:Input invalid value and check error message	//input[@id='ipsecMonitorDstIp']	${EXCEEDED}	Invalid IP address.	#Exceeded the maximum size for this field.
	SUITE:Input invalid value and check error message	//input[@id='ipsecMonitorDstIp']	${NUMBER_AND_POINTS}	Invalid IP address.	#This field contains invalid characters.
	[Teardown]	SUITE:Teardown For Add Tunnel Page	@{TEST_ALL_TUNNEL_NAMES}

Test case invalid values for monitoring field=Number of Retries
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	Input Text	//input[@id='ipsecName']	${TEST_TUNNEL_NAME}
	Input Text	//input[@id='ipsecRightAddr']	${TEST_REMOTE_RIGHT_ADDRESS}
	Input Text	//input[@id='ipsecSecret']	${TEST_PSK_SECRET}
	Input Text	//input[@id='ipsecLeftId']	${TEST_IPSEC_LEFT_ID}[0]
	Input Text	//input[@id='ipsecRightId']	${TEST_IPSEC_RIGHT_ID}[0]
	Select Checkbox	//input[@id='ipsecMonitor']
	Input Text	//input[@id='ipsecMonitorSrcIp']	${TEST_SOURCE_LEFT_ADDRESS}
	Input Text	//input[@id='ipsecMonitorDstIp']	${TEST_REMOTE_RIGHT_ADDRESS}
	SUITE:Input invalid value and check error message	//input[@id='ipsecMonitorRetries']	${NEGATIVE_FLOAT_NUMBER}	This field only accepts positive integers.
	SUITE:Input invalid value and check error message	//input[@id='ipsecMonitorRetries']	${EMPTY}	This field only accepts positive integers.	#Field must not be empty.
	SUITE:Input invalid value and check error message	//input[@id='ipsecMonitorRetries']	${EXCEEDED}	This field only accepts positive integers.	#Exceeded the maximum size for this field.
	SUITE:Input invalid value and check error message	//input[@id='ipsecMonitorRetries']	${NUMBER_AND_POINTS}	This field only accepts positive integers.	#This field contains invalid characters.
	[Teardown]	SUITE:Teardown For Add Tunnel Page	@{TEST_ALL_TUNNEL_NAMES}

Test case invalid values for monitoring field=Interval (s)
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	Input Text	//input[@id='ipsecName']	${TEST_TUNNEL_NAME}
	Input Text	//input[@id='ipsecRightAddr']	${TEST_REMOTE_RIGHT_ADDRESS}
	Input Text	//input[@id='ipsecSecret']	${TEST_PSK_SECRET}
	Input Text	//input[@id='ipsecLeftId']	${TEST_IPSEC_LEFT_ID}[0]
	Input Text	//input[@id='ipsecRightId']	${TEST_IPSEC_RIGHT_ID}[0]
	Select Checkbox	//input[@id='ipsecMonitor']
	Input Text	//input[@id='ipsecMonitorSrcIp']	${TEST_SOURCE_LEFT_ADDRESS}
	Input Text	//input[@id='ipsecMonitorDstIp']	${TEST_REMOTE_RIGHT_ADDRESS}
	SUITE:Input invalid value and check error message	//input[@id='ipsecMonitorInterval']	${NEGATIVE_FLOAT_NUMBER}	This field only accepts positive integers.
	SUITE:Input invalid value and check error message	//input[@id='ipsecMonitorInterval']	${EMPTY}	This field only accepts positive integers.	#Field must not be empty.
	SUITE:Input invalid value and check error message	//input[@id='ipsecMonitorInterval']	${EXCEEDED}	This field only accepts positive integers.	#Exceeded the maximum size for this field.
	SUITE:Input invalid value and check error message	//input[@id='ipsecMonitorInterval']	${NUMBER_AND_POINTS}	This field only accepts positive integers.	#This field contains invalid characters.
	[Teardown]	SUITE:Teardown For Add Tunnel Page	@{TEST_ALL_TUNNEL_NAMES}

Test case valid values for monitoring field=Action
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	Input Text	//input[@id='ipsecName']	${TEST_TUNNEL_NAME}
	Input Text	//input[@id='ipsecRightAddr']	${TEST_REMOTE_RIGHT_ADDRESS}
	Input Text	//input[@id='ipsecSecret']	${TEST_PSK_SECRET}
	Input Text	//input[@id='ipsecLeftId']	${TEST_IPSEC_LEFT_ID}[0]
	Input Text	//input[@id='ipsecRightId']	${TEST_IPSEC_RIGHT_ID}[0]
	Select Checkbox	//input[@id='ipsecMonitor']
	Input Text	//input[@id='ipsecMonitorSrcIp']	${TEST_SOURCE_LEFT_ADDRESS}
	Input Text	//input[@id='ipsecMonitorDstIp']	${TEST_REMOTE_RIGHT_ADDRESS}
	FOR	${TEST_IPSEC_ACTION_VALID}	IN	@{TEST_IPSEC_ACTION_VALID_VALUES}
		Click Element	//select[@id='ipsecMonitorAction']
		Select From List By Label	id=ipsecMonitorAction	${TEST_IPSEC_ACTION_VALID}
		GUI::Basic::Page Should Not Contain Error
	END
	[Teardown]	SUITE:Teardown For Add Tunnel Page	@{TEST_ALL_TUNNEL_NAMES}

Test case invalid values for Virtual Tunnel Interface field=Mark
	SUITE:Go to IPsec::Global and enable virtual tunnel interface if disabled
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	Input Text	//input[@id='ipsecName']	${TEST_TUNNEL_NAME}
	Input Text	//input[@id='ipsecRightAddr']	${TEST_REMOTE_RIGHT_ADDRESS}
	Input Text	//input[@id='ipsecSecret']	${TEST_PSK_SECRET}
	Input Text	//input[@id='ipsecLeftId']	${TEST_IPSEC_LEFT_ID}[0]
	Input Text	//input[@id='ipsecRightId']	${TEST_IPSEC_RIGHT_ID}[0]
	Select Checkbox	//input[@id='ipsecVtiEnabled']
	Input Text	//input[@id='ipsecVtiItf']	${TEST_VTI_INTERFACE}
	#Accepting all values even the EXCEEDED value
	SUITE:Input invalid value and check error message	//input[@id='ipsecVtiLeftAddr']	${NEGATIVE_FLOAT_NUMBER}	Invalid IP address.
	SUITE:Input invalid value and check error message	//input[@id='ipsecVtiLeftAddr']	${EXCEEDED}	Invalid IP address.	#Exceeded the maximum size for this field.
	SUITE:Input invalid value and check error message	//input[@id='ipsecVtiLeftAddr']	${NUMBER_AND_POINTS}	Invalid IP address.	#This field contains invalid characters.
	Checkbox Should Be Selected	//input[@id='ipsecVtiRoute']
	Checkbox Should Not Be Selected	//input[@id='ipsecVtiShare']
	[Teardown]	SUITE:Teardown For Add Tunnel Page	@{TEST_ALL_TUNNEL_NAMES}

Test case invalid values for Virtual Tunnel Interface field=Address
	SUITE:Go to IPsec::Global and enable virtual tunnel interface if disabled
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	Input Text	//input[@id='ipsecName']	${TEST_TUNNEL_NAME}
	Input Text	//input[@id='ipsecRightAddr']	${TEST_REMOTE_RIGHT_ADDRESS}
	Input Text	//input[@id='ipsecSecret']	${TEST_PSK_SECRET}
	Input Text	//input[@id='ipsecLeftId']	${TEST_IPSEC_LEFT_ID}[0]
	Input Text	//input[@id='ipsecRightId']	${TEST_IPSEC_RIGHT_ID}[0]
	Select Checkbox	//input[@id='ipsecVtiEnabled']
	Input Text	//input[@id='ipsecVtiItf']	${TEST_VTI_INTERFACE}
	SUITE:Input invalid value and check error message	//input[@id='ipsecVtiLeftAddr']	${NEGATIVE_FLOAT_NUMBER}	Invalid IP address.
	SUITE:Input invalid value and check error message	//input[@id='ipsecVtiLeftAddr']	${EXCEEDED}	Invalid IP address.	#Exceeded the maximum size for this field.
	SUITE:Input invalid value and check error message	//input[@id='ipsecVtiLeftAddr']	${NUMBER_AND_POINTS}	Invalid IP address.	#This field contains invalid characters.
	SUITE:Input invalid value and check error message	//input[@id='ipsecVtiLeftAddr']	${TEST_REMOTE_RIGHT_ADDRESS}	Invalid mask.
	Checkbox Should Be Selected	//input[@id='ipsecVtiRoute']
	Checkbox Should Not Be Selected	//input[@id='ipsecVtiShare']
	[Teardown]	SUITE:Teardown For Add Tunnel Page	@{TEST_ALL_TUNNEL_NAMES}

Test case invalid values for Virtual Tunnel Interface field=Interface
	SUITE:Go to IPsec::Global and enable virtual tunnel interface if disabled
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Add
	Input Text	//input[@id='ipsecName']	${TEST_TUNNEL_NAME}
	Input Text	//input[@id='ipsecRightAddr']	${TEST_REMOTE_RIGHT_ADDRESS}
	Input Text	//input[@id='ipsecSecret']	${TEST_PSK_SECRET}
	Input Text	//input[@id='ipsecLeftId']	${TEST_IPSEC_LEFT_ID}[0]
	Input Text	//input[@id='ipsecRightId']	${TEST_IPSEC_RIGHT_ID}[0]
	Select Checkbox	//input[@id='ipsecVtiEnabled']
	SUITE:Input invalid value and check error message	//input[@id='ipsecVtiItf']	${EMPTY}	Field must not be empty.
	SUITE:Input invalid value and check error message	//input[@id='ipsecVtiItf']	${NUMBER_AND_POINTS}	This field is empty or it contains invalid characters.	#This field contains invalid characters.
	Checkbox Should Be Selected	//input[@id='ipsecVtiRoute']
	Checkbox Should Not Be Selected	//input[@id='ipsecVtiShare']
	[Teardown]	SUITE:Teardown For Add Tunnel Page	@{TEST_ALL_TUNNEL_NAMES}

Test case invalid values for IKE Profile field=Profile Name
	GUI::Network::VPN::Open IPsec::IKE Profile
	GUI::Basic::Add
	Select From List By Label	id=ipsecIkeVersion	${TEST_IPSEC_IKE_VERSION_VALID_VALUES}[0]
	SUITE:Input invalid value and check error message	//input[@id='ipsecIkeName']	${EMPTY}	Field must not be empty.
	SUITE:Input invalid value and check error message	//input[@id='ipsecIkeName']	${NUMBER_AND_POINTS}	Validation error.	#This field contains invalid characters.
	[Teardown]	SUITE:Teardown For Add IKE Profile Page	@{TEST_IKE_PROFILE_NAMES}

Test case valid values for IKE Profile field=IKE Version
	GUI::Network::VPN::Open IPsec::IKE Profile
	GUI::Basic::Add
	Input Text	//input[@id='ipsecIkeName']	${TEST_IKE_PROFILE_NAME}
	FOR	${TEST_IPSEC_IKE_VERSION_VALID}	IN	@{TEST_IPSEC_IKE_VERSION_VALID_VALUES}
		Click Element	//select[@id='ipsecIkeVersion']
		Select From List By Label	id=ipsecIkeVersion	${TEST_IPSEC_IKE_VERSION_VALID}
		GUI::Basic::Page Should Not Contain Error
	END
	[Teardown]	SUITE:Teardown For Add IKE Profile Page	@{TEST_IKE_PROFILE_NAMES}

Test case valid values for IKE Profile IKEv1 field=Mode
	GUI::Network::VPN::Open IPsec::IKE Profile
	GUI::Basic::Add
	Select From List By Label	id=ipsecIkeVersion	${TEST_IPSEC_IKE_VERSION_VALID_VALUES}[0]
	Input Text	//input[@id='ipsecIkeName']	${TEST_IKE_PROFILE_NAME}
	FOR	${TEST_IPSEC_IKE_MODE_VALID}	IN	@{TEST_IPSEC_IKE_MODE_VALID_VALUES}
		Click Element	//select[@id='ipsecIkeMode']
		Select From List By Label	id=ipsecIkeMode	${TEST_IPSEC_IKE_MODE_VALID}
		GUI::Basic::Page Should Not Contain Error
	END
	Select From List By Label	id=ipsecIkeVersion	${TEST_IPSEC_IKE_VERSION_VALID_VALUES}[1]
	Element Should Not Be Visible	//select[@id='ipsecIkeMode']
	[Teardown]	SUITE:Teardown For Add IKE Profile Page	@{TEST_IKE_PROFILE_NAMES}

Test case valid values for IKE Profile IKEv1 and IKEv2 field=Encryption
	GUI::Network::VPN::Open IPsec::IKE Profile
	GUI::Basic::Add
	Select From List By Label	id=ipsecIkeVersion	${TEST_IPSEC_IKE_VERSION_VALID_VALUES}[0]
	Input Text	//input[@id='ipsecIkeName']	${TEST_IKE_PROFILE_NAME}
	FOR	${TEST_IPSEC_IKE_ENCRYPTION_VALID}	IN	@{TEST_IPSEC_IKE_ENCRYPTION_VALID_VALUES}
		Click Element	//select[@id='ipsecIkeEncryption']
		Select From List By Label	id=ipsecIkeEncryption	${TEST_IPSEC_IKE_ENCRYPTION_VALID}
		GUI::Basic::Page Should Not Contain Error
	END
	Select From List By Label	id=ipsecIkeVersion	${TEST_IPSEC_IKE_VERSION_VALID_VALUES}[1]
	FOR	${TEST_IPSEC_IKE_ENCRYPTION_VALID}	IN	@{TEST_IPSEC_IKE_ENCRYPTION_VALID_VALUES}
		Click Element	//select[@id='ipsecIkeEncryption']
		Select From List By Label	id=ipsecIkeEncryption	${TEST_IPSEC_IKE_ENCRYPTION_VALID}
		GUI::Basic::Page Should Not Contain Error
	END
	[Teardown]	SUITE:Teardown For Add IKE Profile Page	@{TEST_IKE_PROFILE_NAMES}

Test case valid values for IKE Profile IKEv1 and IKEv2 field=Authentication
	GUI::Network::VPN::Open IPsec::IKE Profile
	GUI::Basic::Add
	Select From List By Label	id=ipsecIkeVersion	${TEST_IPSEC_IKE_VERSION_VALID_VALUES}[0]
	Input Text	//input[@id='ipsecIkeName']	${TEST_IKE_PROFILE_NAME}
	FOR	${TEST_IPSEC_IKE_AUTHENTICATION_VALID}	IN	@{TEST_IPSEC_IKE_AUTHENTICATION_VALID_VALUES}
		Click Element	//select[@id='ipsecIkeAuth']
		Select From List By Label	id=ipsecIkeAuth	${TEST_IPSEC_IKE_AUTHENTICATION_VALID}
		GUI::Basic::Page Should Not Contain Error
	END
	Select From List By Label	id=ipsecIkeVersion	${TEST_IPSEC_IKE_VERSION_VALID_VALUES}[1]
	FOR	${TEST_IPSEC_IKE_AUTHENTICATION_VALID}	IN	@{TEST_IPSEC_IKE_AUTHENTICATION_VALID_VALUES}
		Click Element	//select[@id='ipsecIkeAuth']
		Select From List By Label	id=ipsecIkeAuth	${TEST_IPSEC_IKE_AUTHENTICATION_VALID}
		GUI::Basic::Page Should Not Contain Error
	END
	[Teardown]	SUITE:Teardown For Add IKE Profile Page	@{TEST_IKE_PROFILE_NAMES}

Test case valid values for IKE Profile IKEv1 and IKEv2 field=Diffie-Hellman Group
	IF	'${NGVERSION}' >= '5.4'
		${TEST_IPSEC_IKE_DIFFLE_VALID_VALUES_THIS_VERSION}	Create List	@{TEST_IPSEC_IKE_DIFFLE_VALID_VALUES_5_4+}
	ELSE
		${TEST_IPSEC_IKE_DIFFLE_VALID_VALUES_THIS_VERSION}	Create List	@{TEST_IPSEC_IKE_DIFFLE_VALID_VALUES}
	END
	GUI::Network::VPN::Open IPsec::IKE Profile
	GUI::Basic::Add
	Select From List By Label	id=ipsecIkeVersion	${TEST_IPSEC_IKE_VERSION_VALID_VALUES}[0]
	Input Text	//input[@id='ipsecIkeName']	${TEST_IKE_PROFILE_NAME}
	FOR	${TEST_IPSEC_IKE_DIFFLE_VALID}	IN	@{TEST_IPSEC_IKE_DIFFLE_VALID_VALUES_THIS_VERSION}
		Click Element	//select[@id='ipsecIkeDhgroup']
		Select From List By Label	id=ipsecIkeDhgroup	${TEST_IPSEC_IKE_DIFFLE_VALID}
		GUI::Basic::Page Should Not Contain Error
	END
	Select From List By Label	id=ipsecIkeVersion	${TEST_IPSEC_IKE_VERSION_VALID_VALUES}[1]
	FOR	${TEST_IPSEC_IKE_DIFFLE_VALID}	IN	@{TEST_IPSEC_IKE_DIFFLE_VALID_VALUES_THIS_VERSION}
		Click Element	//select[@id='ipsecIkeDhgroup']
		Select From List By Label	id=ipsecIkeDhgroup	${TEST_IPSEC_IKE_DIFFLE_VALID}
		GUI::Basic::Page Should Not Contain Error
	END
	[Teardown]	SUITE:Teardown For Add IKE Profile Page	@{TEST_IKE_PROFILE_NAMES}

Test case invalid values for IKE Profile IKEv1 and IKEv2 field=Lifetime (s)
	GUI::Network::VPN::Open IPsec::IKE Profile
	GUI::Basic::Add
	Select From List By Label	id=ipsecIkeVersion	${TEST_IPSEC_IKE_VERSION_VALID_VALUES}[0]
	Input Text	//input[@id='ipsecIkeName']	${TEST_IKE_PROFILE_NAME}
	SUITE:Input invalid value and check error message	//input[@id='ipsecIkeLifetime']	${NEGATIVE_FLOAT_NUMBER}	Validation error.
	SUITE:Input invalid value and check error message	//input[@id='ipsecIkeLifetime']	${EXCEEDED}	Validation error.	#Exceeded the maximum size for this field.
	SUITE:Input invalid value and check error message	//input[@id='ipsecIkeLifetime']	${NUMBER_AND_POINTS}	Validation error.	#This field contains invalid characters.
	Set Focus To Element	//input[@id='ipsecIkeDpdEnabled']
	Checkbox Should Not Be Selected	//input[@id='ipsecIkeDpdEnabled']
	[Teardown]	SUITE:Teardown For Add IKE Profile Page	@{TEST_IKE_PROFILE_NAMES}

Test case valid values for IKE Profile Phase 2 field=Authentication Protocol
	GUI::Network::VPN::Open IPsec::IKE Profile
	GUI::Basic::Add
	Select From List By Label	id=ipsecIkeVersion	${TEST_IPSEC_IKE_VERSION_VALID_VALUES}[0]
	Input Text	//input[@id='ipsecIkeName']	${TEST_IKE_PROFILE_NAME}
	Click Element	//select[@id='ipsecIkeAuthProtocol']
	Select From List By Label	id=ipsecIkeAuthProtocol	${TEST_IPSEC_IKE_AUTH_PROTO_VALID_VALUES}[0]
	GUI::Basic::Page Should Not Contain Error
	Element Should Be Visible	//div[@id='ipsecIkeEncryption2']
	Element Should Be Visible	//div[@id='ipsecIkeAuth2']
	Click Element	//select[@id='ipsecIkeAuthProtocol']
	Select From List By Label	id=ipsecIkeAuthProtocol	${TEST_IPSEC_IKE_AUTH_PROTO_VALID_VALUES}[1]
	GUI::Basic::Page Should Not Contain Error
	Element Should Not Be Visible	//div[@id='ipsecIkeEncryption2']
	Element Should Be Visible	//div[@id='ipsecIkeAuth2']
	[Teardown]	SUITE:Teardown For Add IKE Profile Page	@{TEST_IKE_PROFILE_NAMES}

Test case valid values for IKE Profile Phase 2 field=Encryption
	GUI::Network::VPN::Open IPsec::IKE Profile
	GUI::Basic::Add
	Select From List By Label	id=ipsecIkeVersion	${TEST_IPSEC_IKE_VERSION_VALID_VALUES}[0]
	Input Text	//input[@id='ipsecIkeName']	${TEST_IKE_PROFILE_NAME}
	Click Element	//select[@id='ipsecIkeAuthProtocol']
	Select From List By Label	id=ipsecIkeAuthProtocol	${TEST_IPSEC_IKE_AUTH_PROTO_VALID_VALUES}[0]
	Select All From List	//div[@id='ipsecIkeEncryption2']//select[@class='selectbox groupFrom']
	List Selection Should Be	//div[@id='ipsecIkeEncryption2']//select[@class='selectbox groupFrom']	@{TEST_IPSEC_IKE_ENCRYPTION2_VALID_VALUES}
	Click Element	//div[@id='ipsecIkeEncryption2']//button[@class='listbuilder_btn'][contains(text(),'Add ►')]
	Select All From List	//div[@id='ipsecIkeEncryption2']//select[@class='selectbox groupTo']
	List Selection Should Be	//div[@id='ipsecIkeEncryption2']//select[@class='selectbox groupTo']	@{TEST_IPSEC_IKE_ENCRYPTION2_VALID_VALUES}
	Click Element	//div[@id='ipsecIkeEncryption2']//button[@class='listbuilder_btn'][contains(text(),'◄ Remove')]
	Select All From List	//div[@id='ipsecIkeEncryption2']//select[@class='selectbox groupFrom']
	List Selection Should Be	//div[@id='ipsecIkeEncryption2']//select[@class='selectbox groupFrom']	@{TEST_IPSEC_IKE_ENCRYPTION2_VALID_VALUES}
	[Teardown]	SUITE:Teardown For Add IKE Profile Page	@{TEST_IKE_PROFILE_NAMES}

Test case valid values for IKE Profile Phase 2 field=Authentication
	GUI::Network::VPN::Open IPsec::IKE Profile
	GUI::Basic::Add
	Select From List By Label	id=ipsecIkeVersion	${TEST_IPSEC_IKE_VERSION_VALID_VALUES}[0]
	Input Text	//input[@id='ipsecIkeName']	${TEST_IKE_PROFILE_NAME}
	Click Element	//select[@id='ipsecIkeAuthProtocol']
	Select From List By Label	id=ipsecIkeAuthProtocol	${TEST_IPSEC_IKE_AUTH_PROTO_VALID_VALUES}[0]
	Select All From List	//div[@id='ipsecIkeAuth2']//select[@class='selectbox groupFrom']
	List Selection Should Be	//div[@id='ipsecIkeAuth2']//select[@class='selectbox groupFrom']	@{TEST_IPSEC_IKE_AUTHENTICATION2_VALID_VALUES}
	Click Element	//div[@id='ipsecIkeAuth2']//button[@class='listbuilder_btn'][contains(text(),'Add ►')]
	Select All From List	//div[@id='ipsecIkeAuth2']//select[@class='selectbox groupTo']
	List Selection Should Be	//div[@id='ipsecIkeAuth2']//select[@class='selectbox groupTo']	@{TEST_IPSEC_IKE_AUTHENTICATION2_VALID_VALUES}
	Click Element	//div[@id='ipsecIkeAuth2']//button[@class='listbuilder_btn'][contains(text(),'◄ Remove')]
	Select All From List	//div[@id='ipsecIkeAuth2']//select[@class='selectbox groupFrom']
	List Selection Should Be	//div[@id='ipsecIkeAuth2']//select[@class='selectbox groupFrom']	@{TEST_IPSEC_IKE_AUTHENTICATION2_VALID_VALUES}
	[Teardown]	SUITE:Teardown For Add IKE Profile Page	@{TEST_IKE_PROFILE_NAMES}

Test case valid values for IKE Profile Phase 2 field=PFS Group
	IF	'${NGVERSION}' >= '5.4'
		${TEST_IPSEC_IKE_FPS_GROUP_VALID_VALUES_THIS_VERSION}	Create List	@{TEST_IPSEC_IKE_FPS_GROUP_VALID_VALUES_5_4+}
	ELSE
		${TEST_IPSEC_IKE_FPS_GROUP_VALID_VALUES_THIS_VERSION}	Create List	@{TEST_IPSEC_IKE_FPS_GROUP_VALID_VALUES}
	END
	GUI::Network::VPN::Open IPsec::IKE Profile
	GUI::Basic::Add
	Input Text	//input[@id='ipsecIkeName']	${TEST_IKE_PROFILE_NAME}
	FOR	${TEST_IPSEC_IKE_PFS_GROUP_VALID}	IN	@{TEST_IPSEC_IKE_FPS_GROUP_VALID_VALUES_THIS_VERSION}
		Click Element	//select[@id='ipsecIkeDhgroup2']
		Select From List By Label	id=ipsecIkeDhgroup2	${TEST_IPSEC_IKE_PFS_GROUP_VALID}
		GUI::Basic::Page Should Not Contain Error
	END
	[Teardown]	SUITE:Teardown For Add IKE Profile Page	@{TEST_IKE_PROFILE_NAMES}

Test case invalid values for IKE Profile Phase 2 field=Lifetime (s)
	GUI::Network::VPN::Open IPsec::IKE Profile
	GUI::Basic::Add
	Select From List By Label	id=ipsecIkeVersion	${TEST_IPSEC_IKE_VERSION_VALID_VALUES}[0]
	Input Text	//input[@id='ipsecIkeName']	${TEST_IKE_PROFILE_NAME}
	SUITE:Input invalid value and check error message	//input[@id='ipsecIkeLifetime2']	${NEGATIVE_FLOAT_NUMBER}	Validation error.
	SUITE:Input invalid value and check error message	//input[@id='ipsecIkeLifetime2']	${EXCEEDED}	Validation error.	#Exceeded the maximum size for this field.
	SUITE:Input invalid value and check error message	//input[@id='ipsecIkeLifetime2']	${NUMBER_AND_POINTS}	Validation error.	#This field contains invalid characters.
	Set Focus To Element	//input[@id='ipsecIkeDpdEnabled']
	Checkbox Should Not Be Selected	//input[@id='ipsecIkeDpdEnabled']
	[Teardown]	SUITE:Teardown For Add IKE Profile Page	@{TEST_IKE_PROFILE_NAMES}

Test case invalid values for IKE Profile Advanced Settings field=Number of Retries
	GUI::Network::VPN::Open IPsec::IKE Profile
	GUI::Basic::Add
	Input Text	//input[@id='ipsecIkeName']	${TEST_IKE_PROFILE_NAME}
	Select Checkbox	//input[@id='ipsecIkeDpdEnabled']
	Input Text	//input[@id='ipsecIkeDpdInterval']	3600
	SUITE:Input invalid value and check error message	//input[@id='ipsecIkeDpdInterval']	${EMPTY}	Field must not be empty.
	[Teardown]	SUITE:Teardown For Add IKE Profile Page	@{TEST_IKE_PROFILE_NAMES}

Test case invalid values for IKE Profile Advanced Settings field=Interval (sec)
	GUI::Network::VPN::Open IPsec::IKE Profile
	GUI::Basic::Add
	Input Text	//input[@id='ipsecIkeName']	${TEST_IKE_PROFILE_NAME}
	Select Checkbox	//input[@id='ipsecIkeDpdEnabled']
	Input Text	//input[@id='ipsecIkeDpdRetry']	3
	SUITE:Input invalid value and check error message	//input[@id='ipsecIkeDpdInterval']	${EMPTY}	Field must not be empty.
	SUITE:Input invalid value and check error message	//input[@id='ipsecIkeDpdInterval']	${NEGATIVE_FLOAT_NUMBER}	Validation error.
	SUITE:Input invalid value and check error message	//input[@id='ipsecIkeDpdInterval']	${EXCEEDED}	Validation error.	#Exceeded the maximum size for this field.
	SUITE:Input invalid value and check error message	//input[@id='ipsecIkeDpdInterval']	${NUMBER_AND_POINTS}	Validation error.	#This field contains invalid characters.
	[Teardown]	SUITE:Teardown For Add IKE Profile Page	@{TEST_IKE_PROFILE_NAMES}

Test case valid values for IKE Profile Advanced Settings field=Action
	GUI::Network::VPN::Open IPsec::IKE Profile
	GUI::Basic::Add
	Input Text	//input[@id='ipsecIkeName']	${TEST_IKE_PROFILE_NAME}
	Select Checkbox	//input[@id='ipsecIkeDpdEnabled']
	Input Text	//input[@id='ipsecIkeDpdInterval']	3600
	FOR	${TEST_IPSEC_IKE_ACTION_VALID}	IN	@{TEST_IPSEC_IKE_ACTION_VALID_VALUES}
		Click Element	//select[@id='ipsecIkeDpdAction']
		Select From List By Label	id=ipsecIkeDpdAction	${TEST_IPSEC_IKE_ACTION_VALID}
		GUI::Basic::Page Should Not Contain Error
	END
	[Teardown]	SUITE:Teardown For Add IKE Profile Page	@{TEST_IKE_PROFILE_NAMES}

Test case invalid values for IKE Profile Advanced Settings field=MTU
	GUI::Network::VPN::Open IPsec::IKE Profile
	GUI::Basic::Add
	Input Text	//input[@id='ipsecIkeName']	${TEST_IKE_PROFILE_NAME}
	Select Checkbox	//input[@id='ipsecIkeDpdEnabled']
	Input Text	//input[@id='ipsecIkeDpdRetry']	3
	Input Text	//input[@id='ipsecIkeDpdInterval']	3600
	SUITE:Input invalid value and check error message	//input[@id='ipsecIkeMtu']	${NEGATIVE_FLOAT_NUMBER}	Validation error.
	SUITE:Input invalid value and check error message	//input[@id='ipsecIkeMtu']	${EXCEEDED}	Validation error.	#Exceeded the maximum size for this field.
	SUITE:Input invalid value and check error message	//input[@id='ipsecIkeMtu']	${NUMBER_AND_POINTS}	Validation error.	#This field contains invalid characters.
	[Teardown]	SUITE:Teardown For Add IKE Profile Page	@{TEST_IKE_PROFILE_NAMES}

Test case invalid values for IKE Profile Advanced Settings field=Custom Parameters
	GUI::Network::VPN::Open IPsec::IKE Profile
	GUI::Basic::Add
	Input Text	//input[@id='ipsecIkeName']	${TEST_IKE_PROFILE_NAME}
	Select Checkbox	//input[@id='ipsecIkeDpdEnabled']
	Input Text	//input[@id='ipsecIkeDpdInterval']	3600
	#Accepting all values even the EXCEEDED value
	SUITE:Input invalid value and check error message	//input[@id='ipsecIkeMtu']	${NEGATIVE_FLOAT_NUMBER}	Validation error.
	SUITE:Input invalid value and check error message	//input[@id='ipsecIkeMtu']	${EXCEEDED}	Validation error.	#Exceeded the maximum size for this field.
	SUITE:Input invalid value and check error message	//input[@id='ipsecIkeMtu']	${NUMBER_AND_POINTS}	Validation error.	#This field contains invalid characters.
	[Teardown]	SUITE:Teardown For Add IKE Profile Page	@{TEST_IKE_PROFILE_NAMES}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid

SUITE:Teardown
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Network::VPN::Open IPsec
	GUI::Basic::Delete Rows In Table If Exists	ipsecTunnelTable	${TEST_ALL_TUNNEL_NAMES}	HAS_ALERT=${FALSE}
	GUI::Network::VPN::Open IPsec::IKE Profile
	GUI::Basic::Delete Rows In Table If Exists	ipsecIkeTable	${TEST_IKE_PROFILE_NAMES}	HAS_ALERT=${FALSE}
	GUI::Basic::Logout And Close Nodegrid

SUITE:Teardown For Add Tunnel Page
	[Arguments]	@{TUNNEL_NAMES}
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Ignore Error	GUI::Basic::Button::Cancel
	GUI::Basic::Delete Rows In Table If Exists	ipsecTunnelTable	${TUNNEL_NAMES}	HAS_ALERT=${FALSE}

SUITE:Teardown For Add IKE Profile Page
	[Arguments]	@{TUNNEL_NAMES}
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Ignore Error	GUI::Basic::Button::Cancel
	GUI::Basic::Delete Rows In Table If Exists	ipsecIkeTable	${TUNNEL_NAMES}	HAS_ALERT=${FALSE}

SUITE:Input invalid value and check error message
	[Arguments]	${LOCATOR}	${VALUE}	${ERROR_MESSAGE}	${RSA_KEY}=No	${2_RSA_LOCATOR}=${EMPTY}
	Input Text	${LOCATOR}	${VALUE}
	IF	'${RSA_KEY}' == 'Yes'
		Input Text	${2_RSA_LOCATOR}	${VALUE}
		Page Should Contain Button	//input[@value='Generate Left Public Key']
	END
	GUI::Basic::Save
	Page Should Contain	Error on page. Please check.
	Set Focus To Element	${LOCATOR}
	Page Should Contain	${ERROR_MESSAGE}

SUITE:Go to IPsec::Global and enable virtual tunnel interface if disabled
	GUI::Network::VPN::Open IPsec::Global
	${STATUS}	Run Keyword And Return Status	Checkbox Should Not Be Selected	//input[@id='ipsecGlobalVtiEnabled']
	IF	${STATUS}
		Select Checkbox	//input[@id='ipsecGlobalVtiEnabled']
		Select Checkbox	//input[@id='ipsecGlobalLogEnabled']
		GUI::Basic::Save
	END