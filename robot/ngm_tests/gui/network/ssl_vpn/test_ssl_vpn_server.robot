*** Settings ***
Documentation	Testing SSL VPN Server under the Network:SSL VPN Tab
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	GUI

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test NonAccess Controls
	Click Element       css=#sslvpnServer > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Disabled	//*[@id="saveButton"]

Test Fields
	GUI::Auditing::Auto Input Tests	listenIP	dummy	**	123	1.1.1.1
	GUI::Auditing::Auto Input Tests	listenPort	dummy	12345678901	${EMPTY}	1234
	GUI::Auditing::Auto Input Tests	tunnelMtu	dummy	**	${EMPTY}	1234
	GUI::Auditing::Auto Input Tests	maxClients	dummy	**	${EMPTY}	1234
	wait until page contains element		ipAddressing
	Select Radio Button	ipAddressing	net
	Input Text	jquery=#tunnelNetIPv4	1.1.1.1
	Save With Validation Error
	Input text	jquery=#tunnelNetIPv4	${EMPTY}
	Input Text	jquery=#tunnelNetIPv6	1.1.1.1
	Save With Validation Error
	Input Text	jquery=#tunnelNetIPv6	${EMPTY}
	Run Keyword If	'${NGVERSION}' < '5.6'		wait until page contains element		css=.radio:nth-child(4) #ipAddressing
	Run Keyword If	'${NGVERSION}' < '5.6'		click element		css=.radio:nth-child(4) #ipAddressing
	Run Keyword If	'${NGVERSION}' >= '5.6'		click element		xpath=(//input[@id='ipAddressing'])[2]
	GUI::Auditing::Auto Input Tests	localIP	dummy	**	123	127.0.0.1
	GUI::Auditing::Auto Input Tests	remoteIP	dummy	123	**	234.243.243.243
	Input Text	jquery=#localIP	${EMPTY}
	Input Text	jquery=#remoteIP	${EMPTY}
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	Select Radio Button	ipAddressing	p2p-ipv6
	GUI::Auditing::Auto Input Tests	localIPv6	dummy	**	123	::1
	GUI::Auditing::Auto Input Tests	remoteIPv6	dummy	**	123	123:123:123:123::2c
	Input Text	jquery=#localIPv6	${EMPTY}
	Input Text	jquery=#remoteIPv6	${EMPTY}
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

Reset Values
	Select Radio Button	ipAddressing	net
	Input Text	jquery=#listenIP	${EMPTY}
	Input Text	jquery=#listenPort	1194
	Input Text	jquery=#tunnelMtu	1500
	Input Text	jquery=#maxClients	256
	Click Element	jquery=#saveButton

Test Authentication Method TLS Elements
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="authType"]
	Click Element	//*[@id="authType"]/option[1]
	Page Should Contain	CA Certificate:
	Page Should Contain	Server Certificate:
	Page Should Contain	Server Key:
	Page Should Contain	Diffie Hellman:

Test Authentication Method Static Key Elements
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="authType"]
	Click Element	//*[@id="authType"]/option[2]
	Page Should Contain	Secret:
	Page Should Contain	Diffie Hellman:

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' <= '5.2'	GUI::Basic::Network::SSL VPN::open tab
	Run Keyword If	'${NGVERSION}' >= '5.4'	Run Keywords	GUI::Basic::Network::VPN::Open Tab	AND	Click Element	//span[normalize-space()='SSL VPN']
    GUI::Basic::Spinner Should Be Invisible
    Click Element    xpath=//a[@id='sslvpnServer']/span
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout
	Close all Browsers

Save With Validation Error
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Validation error.