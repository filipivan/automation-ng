*** Settings ***
Documentation	Testing SSL VPN Client under the Network:SSL VPN Tab
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	GUI

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${Emp}

*** Test Cases ***
Test NonAccess Controls
	Element Should Be Enabled	//*[@id="addButton"]
	Element Should Be Disabled	//*[@id="delButton"]
	Element Should Be Disabled	//*[@id="vpnUp"]
	Element Should Be Disabled	//*[@id="vpnDown"]

Test Fields
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	jquery=#vpnName	!@#$%
	GUI::Basic::Auto Input Tests	gwIpAddr	${EMPTY}	**	123
	GUI::Basic::Auto Input Tests	gwTCPPort	${EMPTY}	Dummy	123
	GUI::Basic::Auto Input Tests	tunnelMtu	${EMPTY}	Dummy	**	123
	Click Element	//*[@id="authType"]
	Click Element	//*[@id="authType"]/option[2]
	GUI::Basic::Auto Input Tests	authLocalIP	123	**	Dummy	1.1.1.1
	GUI::Basic::Auto Input Tests	authRemoteIP	123	**	Dummy	123.123.123.123

Test Client Add Invalid Char Name
	#GUI::Basic::Add
	Input Text	//*[@id="vpnName"]	**
	Input Text	//*[@id="gwIpAddr"]	123
	SUITE:Save With Validation Error

Test Client Add Invalid Num Name
	Input Text	//*[@id="vpnName"]	123
	SUITE:Save With Validation Error

Test Client Add Empty IP Addr
	Input Text	//*[@id="vpnName"]	Dummy123
	Input Text	//*[@id="gwIpAddr"]	${Emp}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Field must not be empty.


Test Client Add Invalid IP Addr
	Input Text	//*[@id="gwIpAddr"]	**
	SUITE:Save With Invalid IP Address

Test Client Add Empty Port
	Input Text	//*[@id="gwIpAddr"]	123
	Input Text	//*[@id="gwTCPPort"]	${Emp}
	SUITE:Save With Validation Error

Test Client Add String Port
	Input Text	//*[@id="gwTCPPort"]	Dummy
	SUITE:Save With Validation Error

Test Client Add Invalid Port
	Input Text	//*[@id="gwTCPPort"]	**
	SUITE:Save With Validation Error

Test Client Add Empty Tunnel
	Input Text	//*[@id="gwTCPPort"]	1234
	Input Text	//*[@id="tunnelMtu"]	${Emp}
	SUITE:Save With Validation Error

Test Client Add String Tunnel
	Input Text	//*[@id="tunnelMtu"]	Dummy
	SUITE:Save With Validation Error

Test Client Add Invalid Tunnel
	Input Text	//*[@id="tunnelMtu"]	**
	SUITE:Save With Validation Error

Test Client Add Invalid Num Local Endpoint
	Click Element	//*[@id="authType"]
	Click Element	//*[@id="authType"]/option[2]
	Input Text	//*[@id="tunnelMtu"]	123
	Input Text	//*[@id="authLocalIP"]	123
	SUITE:Save With Invalid IP Address

Test Client Add Invalid String Local Endpoint
	Input Text	//*[@id="authLocalIP"]	sad
	SUITE:Save With Invalid IP Address

Test Client Add Invalid Char Local Endpoint
	Input Text	//*[@id="authLocalIP"]	**
	SUITE:Save With Invalid IP Address

Test Client Add Invalid Num Remote Endpoint
	Input Text	//*[@id="authRemoteIP"]	123
	Input Text	//*[@id="authLocalIP"]	1.1.1.1
	SUITE:Save With Invalid IP Address

Test Client Add Invalid String Remote Endpoint
	Input Text	//*[@id="authRemoteIP"]	sad
	SUITE:Save With Invalid IP Address

Test Client Add Invalid Char Remote Endpoint
	Input Text	//*[@id="authRemoteIP"]	**
	SUITE:Save With Invalid IP Address

Test Authentication Method TLS Elemets
	Click Element	//*[@id="authType"]
	Click Element	//*[@id="authType"]/option[1]
	GUI::Basic::Spinner Should Be Invisible
	#Page Should Contain	TLS Authentication Key:
	Page Should Contain	TLS Authentication Direction:
	Page Should Contain	CA Certificate:
	Page Should Contain	Client Certificate:
	Page Should Contain	Client Private Key:

Test Authentication Method Password Elemets
	Click Element	//*[@id="authType"]
	Click Element	//*[@id="authType"]/option[3]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Username:
	Page Should Contain	Password:
	Page Should Contain	CA Certificate:

Test Authentication Method Password Plus TLS Elemets
	Click Element	//*[@id="authType"]
	Click Element	//*[@id="authType"]/option[4]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Username:
	Page Should Contain	Password:
	Page Should Contain	TLS Authentication Key:
	Page Should Contain	TLS Authentication Direction:
	Page Should Contain	CA Certificate:
	Page Should Contain	Client Certificate:
	Page Should Contain	Client Private Key:

Test Authentication Method Static Key Elemets
	Click Element	//*[@id="authType"]
	Click Element	//*[@id="authType"]/option[2]
	Page Should Contain	Secret:
	Page Should Contain	Local Endpoint (Local IP):
	Page Should Contain	Remote Endpoint (Remote IP):

Test Add Valid Client
	Input Text	//*[@id="authRemoteIP"]	1.2.3.4
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Table Should Contain	//*[@id="sslvpnClientTable"]	Dummy123

Test Delete Client
	Click Element	//*[@id="Dummy123"]/td[1]/input
	Element Should Be Enabled	//*[@id="delButton"]
	Element Should Be Enabled	//*[@id="vpnUp"]
	Element Should Be Enabled	//*[@id="vpnDown"]
	Click Element	//*[@id="delButton"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Not Contain	Dummy123

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' <= '5.2'	GUI::Basic::Network::SSL VPN::open tab
	Run Keyword If	'${NGVERSION}' >= '5.4'	Run Keywords	GUI::Basic::Network::VPN::Open Tab	AND	Click Element	//span[normalize-space()='SSL VPN']
    GUI::Basic::Spinner Should Be Invisible
    Click Element    xpath=//a[@id='sslvpnClient']/span
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout
	Close all Browsers

SUITE:Save With Invalid IP Address
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Invalid IP address.

SUITE:Save With Validation Error
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Validation error.
