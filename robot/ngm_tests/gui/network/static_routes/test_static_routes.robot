*** Settings ***
Documentation	Testing the Network:Static Routes tab
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Check Connection Dropdown Option
	@{EXPECTEDS}=	Create List     ETH0
	@{CONNECTIONS}=	Get List Items	jquery=#AddconnName
	FOR	${EXPECTED}	IN	@{EXPECTEDS}
		List Should Contain Value	${CONNECTIONS}	${EXPECTED}
	END

Test IPv4 Settings
	Select Radio Button	type	ipv4

Test IPv4 Destination IP
	[Tags]	NON-CRITICAL	BUG_NG_8307
	@{VALIDS}=	Create List	12.12.12.12
	@{INVALIDS}=	Create List	12..1244	267.266.267.267	12.35.23.124/24
	GUI::Basic::Input Text::Validation	destIp	${VALIDS}	${INVALIDS}

Check Bitmask
	Element Should Be Visible	destMask

Test IPv4 Gateway IP
	@{VALIDS}=	Create List	12.12.12.12
	@{INVALIDS}=	Create List	12..1244	267.266.267.267	12.35.23.124/24
	GUI::Basic::Input Text::Validation	gateway	${VALIDS}	${INVALIDS}

Test IPv4 Metric
	Input Text	destIp	12..1244
	@{VALIDS}=	Create List	12345
	@{INVALIDS}=	Create List	test	12.12	1..11	12/12
	GUI::Basic::Input Text::Validation	metric	${VALIDS}	${INVALIDS}

Test IPv6 Settings
	Select Radio Button	type	ipv6

Test IPv6 Destination IP
	[Tags]	NON-CRITICAL	BUG_NG_8307
	@{VALIDS}=	Create List	002c::002c
	@{INVALIDS}=	Create List	00cd::2314::234c	267.266.267.267	12.35.23.124/24	12.12.12.12	2341::dfga
	GUI::Basic::Input Text::Validation	destIp	${VALIDS}	${INVALIDS}

Test IPv6 Gateway IP
	@{VALIDS}=	Create List	002c::002c
	@{INVALIDS}=	Create List	00cd::2314::234c	267.266.267.267	12.35.23.124/24	12.12.12.12	2341::dfga
	GUI::Basic::Input Text::Validation	gateway	${VALIDS}	${INVALIDS}

Test IPv6 Metric
	@{VALIDS}=	Create List	12345
	@{INVALIDS}=	Create List	test	12.12	1..11	12/12
	GUI::Basic::Input Text::Validation	metric	${VALIDS}	${INVALIDS}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Static Routes::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Run Keyword And Continue On Failure	GUI::Basic::Button::Cancel
	GUI::Basic::Logout And Close Nodegrid

