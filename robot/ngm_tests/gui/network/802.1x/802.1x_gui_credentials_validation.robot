*** Settings ***
Resource        ../../init.robot
Documentation   Tests Settings > Credentials validation file about 802.1x Feature through the GUI. Keep IS_NSR = True in br settings file for the test to execute and pass
Metadata        Version	1.0
Metadata        Executed At	${HOST}
Force Tags      GUI    EXCLUDEIN3_2    EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
@{CRED_AUTH_METHOD}     MD5   TLS    PEAP    TTLS
@{CRED_INNER_AUTH}      MD5   PAP    CHAP    MSCHAP    MSCHAPV2
@{CRED_NAMES}           test_cred_md5  test_cred_tls  test_cred_peap_md5  test_cred_peap_mschapv2  test_cred_ttls_md5
												...  test_cred_ttls_pap  test_cred_ttls_chap  test_cred_ttls_mschap  test_cred_ttls_mschapv2
@{CRED_ANONYMOUS}       test_cred_anonymous_0   test_cred_anonymous_1   test_cred_anonymous_2   test_cred_anonymous_3
												...  test_cred_anonymous_4  test_cred_anonymous_5  test_cred_anonymous_6
@{VALID_IPS}            192.168.15.60  192.168.16.85  192.168.17.55  0.0.0.0  255.255.255.255
@{INVALID_IPS}          20.20   300.300.300.300   -1.-1.-1.-1    0,5.0,5.0,5.0,5  0.00.00.00
@{VALID_PASSWORDS}      .NGAutom!2#     .ZPE5ystems!2020    .ZPE5ystems!2021    .ZPE5ystems!2020.ZPE5ystems!2021
@{VALID_COUNTRY_CODES}  US  BR  GB  IN
@{VALID_STATES}         CA  SC  IRE  KA
@{VALID_LOCALITIES}     Fremont  Dublin  Blumenau  Bangalore
@{VALID_ORGANIZATIONS}  US Corporate HQ  ZPE Europe  ZPE Brazil  ZPE India (APAC)
@{VALID_EMAIL_ADDRESS}  testeUS@zpesystems.com  testeBR@zpesystems.com  testeGB@zpesystems.com  testeIN@zpesystems.com
@{CERTIFICATE_INFOR}    client certificate:  Certificate:  Data:  Subject Public Key Info:  Signature Algorithm:
												...  BEGIN CERTIFICATE  END CERTIFICATE  BEGIN ENCRYPTED PRIVATE KEY  END ENCRYPTED PRIVATE KEY
@{ALL_VALUES}=          ${WORD}     ${ONE_WORD}     ${TWO_WORD}     ${THREE_WORD}
												...     ${SEVEN_WORD}   ${EIGHT_WORD}   ${ELEVEN_WORD}  ${NUMBER}   ${ONE_NUMBER}
												...     ${TWO_NUMBER}   ${THREE_NUMBER}     ${SEVEN_NUMBER}     ${EIGHT_NUMBER}
												...     ${ELEVEN_NUMBER}    ${WORD_AND_NUMBER}  ${NUMBER_AND_WORD}  ${POINTS}
												...     ${ONE_POINTS}   ${TWO_POINTS}   ${THREE_POINTS}	${SEVEN_POINTS}
												...     ${EIGHT_POINTS}     ${ELEVEN_POINTS}    ${WORD_AND_POINTS}  ${NUMBER_AND_POINTS}
												...     ${POINTS_AND_NUMBER}    ${POINTS_AND_WORD}

*** Test Cases ***
Test Values For Field=username
		# Skipping this test if the device is not a NSR
		Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
		${VALID_USERNAMES}=	Create List	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
		...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
		...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
		...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}
		# Opens 802.1x tab as the device is switched device
		GUI::Network::802.1x::Open Credentials Tab
		# Doing a basic test for validating the username field
		FOR  ${VALID_USERNAME}  IN  @{VALID_USERNAMES}
				GUI::Basic::Add
				GUI::Network::802.1x::Create An Credentials   CRED_NAME=${VALID_USERNAME}  CRED_AUTH_METHOD=${CRED_AUTH_METHOD}[0]
				...  CRED_ANONYMOUS=${CRED_ANONYMOUS}[0]  CRED_INNER_AUTH=${CRED_INNER_AUTH}[0]

				GUI::Basic::Cancel
				GUI::Network::802.1x::Delete Credentials If Exists  ${VALID_USERNAME}
		END

		${INVALID_USERNAMES}=   Create List     ${POINTS}   ${ONE_POINTS}   ${TWO_POINTS}
		...     ${THREE_POINTS}     ${SEVEN_POINTS}   ${WORD_AND_POINTS}    ${NUMBER_AND_POINTS}    ${POINTS_AND_NUMBER}
		...     ${POINTS_AND_WORD}   ${EIGHT_POINTS}

		SUITE:Add 802.1x Credentials Without Commit  ""  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]
				...  ${CRED_INNER_AUTH}[0]
		# Inoutting an invalid username and testing the error message
		FOR  ${INVALID_USERNAME}  IN  @{INVALID_USERNAMES}
			Input Text          id=dot1xUsersName     ${INVALID_USERNAME}
			Click Element       jquery=#saveButton
			Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
			Wait Until Element Contains	jquery=#errormsg	This field is empty or it contains invalid characters.

			Input Text          id=dot1xUsersName     ""
		END

		GUI::Basic::Cancel

Test Values For Field=password
		# Skipping this test if the device is not a NSR
	Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
		# Doing a basic test for validating the password field
	FOR    ${VALID_PASSWORD}   IN  @{VALID_PASSWORDS}
		SUITE:Add 802.1x Credentials Without Commit  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]
		...  ${CRED_INNER_AUTH}[0]
		Input Text	id=dot1xUsersPassword         ${VALID_PASSWORD}
		Input Text	id=dot1xUsersPasswordAgain    ${VALID_PASSWORD}
		GUI::Basic::Save
		# Deleting all the credentials if they exists on the UI
		GUI::Network::802.1x::Delete Credentials If Exists  ${CRED_NAMES}[0]
	END
		# Creaing a list of invalid passwords for the password field verification
	${INVALID_PASSWORDS}     Create List     ${WORD}    ${EIGHT_WORD}   ${ELEVEN_WORD}  ${NUMBER}
											...     ${EIGHT_NUMBER}  ${ELEVEN_NUMBER}    ${WORD_AND_NUMBER}  ${NUMBER_AND_WORD}  ${POINTS}
											...     ${EIGHT_POINTS}     ${ELEVEN_POINTS}    ${WORD_AND_POINTS}  ${NUMBER_AND_POINTS}
											...     ${POINTS_AND_NUMBER}    ${POINTS_AND_WORD}
	SUITE:Add 802.1x Credentials Without Commit  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]
	...  ${CRED_INNER_AUTH}[0]
		#Inputting a different password value for the field and checking the error messages
	FOR    ${INVALID_PASSWORD}     IN   @{INVALID_PASSWORDS}
		Input Text    dot1xUsersPassword  			${INVALID_PASSWORD}
		Input Text    dot1xUsersPasswordAgain		${INVALID_PASSWORD}
		Click Element       jquery=#saveButton
		Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
		Wait Until Element Contains	jquery=#errormsg	Password minimum uppercase characters requirement: 1

		Input Text          id=dot1xUsersPassword     ""
		Input Text          id=dot1xUsersPasswordAgain     ""
	END
		# Creating invalid passwords list
	${INVALID_PASSWORDS}     Create List     ${ONE_WORD}   ${TWO_WORD}   ${THREE_WORD}   ${SEVEN_WORD}   ${ONE_NUMBER}
											...     ${TWO_NUMBER}   ${THREE_NUMBER}     ${SEVEN_NUMBER}
											...     ${ONE_POINTS}   ${TWO_POINTS}   ${THREE_POINTS}	${SEVEN_POINTS}

	FOR    ${INVALID_PASSWORD}     IN   @{INVALID_PASSWORDS}
		Input Text    dot1xUsersPassword  ${INVALID_PASSWORD}
		Input Text    dot1xUsersPasswordAgain  ${INVALID_PASSWORD}

		Click Element       jquery=#saveButton
		Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
		Wait Until Element Contains	jquery=#errormsg	The password is too short. Minimum length: 8.

		Input Text          id=dot1xUsersPassword     ""
		Input Text          id=dot1xUsersPasswordAgain     ""
	END
		# Validating the error messages on the page for invalid passwords.
	Input Text    dot1xUsersPassword  ${INVALID_PASSWORD}
	Input Text    dot1xUsersPasswordAgain  ${EMPTY}

	Click Element       jquery=#saveButton
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Password mismatch.
	# Clicking oncael button
	GUI::Basic::Cancel

Test Values For Field=authentication_method
		# Skipping this test if the device is not a NSR
	Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
	# ADding the credentials without commiting the changes
	SUITE:Add 802.1x Credentials Without Commit  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]  ${CRED_INNER_AUTH}[0]
	# Inoutting the fields
	${COMBOBOX_VALUES}=		Get Text    		id=dot1xUsersAuthMethod
	@{COMBOBOX_VALUES}=		Split String    ${COMBOBOX_VALUES}   \n
	${UPPER1}=		Evaluate     "${COMBOBOX_VALUES}".upper()
	${UPPER2}=		Evaluate     "${CRED_AUTH_METHOD}".upper()
	Should Contain     ${UPPER1}     ${UPPER2}
	 # Clickign on cancel button
	GUI::Basic::Cancel

Test Values For Field=anonymous_identity
		# Skipping this test if the device is not a NSR
	Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
	# Creating list of all values, empty values and so on
	${VALID_IDENTITYS}=     Create List     @{ALL_VALUES}   ${EMPTY}   @{VALID_IPS}
	#  Giving different values in a loop
	FOR    ${VALID_IDENTITY}   IN  @{VALID_IDENTITYS}
		SUITE:Add 802.1x Credentials Without Commit  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[2]  ${CRED_ANONYMOUS}[0]
		...  ${CRED_INNER_AUTH}[0]
		# Giving the imput texts and saving the changes
		Input Text  				id=dot1xUsersAnonId     ${VALID_IDENTITY}
		GUI::Basic::Save
		 # Deleting the crdentials if they exists
		GUI::Network::802.1x::Delete Credentials If Exists  ${CRED_NAMES}[0]
	END

Test Values For Field=inner_authentication
		# Skipping this test if the device is not a NSR
	Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
	# Adding the credentials without commiting the changes
	SUITE:Add 802.1x Credentials Without Commit  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[2]  ${CRED_ANONYMOUS}[0]
		...  ${CRED_INNER_AUTH}[0]
	# Inputting the values and validating
	${COMBOBOX_VALUES}=		Get Text    		id=dot1xUsersAuthMethod2
	@{COMBOBOX_VALUES}=		Split String    ${COMBOBOX_VALUES}   \n
	${UPPER1}=		Evaluate     "${COMBOBOX_VALUES}".upper()
	${UPPER2}=		Evaluate     "${CRED_INNER_AUTH}".upper()
	Should Contain     ${UPPER1}     ${UPPER2}
	# Clicking on cancel button
	GUI::Basic::Cancel
	# Delete the credentials if exists
	GUI::Network::802.1x::Delete Credentials If Exists  ${CRED_NAMES}[0]

Test Values For Differents Field=authentication_methods
		# Skipping this test if the device is not a NSR
	Skip If    ${IS_NSR} == ${FALSE}    System is not NSR

	GUI::Network::802.1x::Open Credentials Tab
	SUITE:Add 802.1x Credentials Without Commit  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[2]  ${CRED_ANONYMOUS}[0]
	...  ${CRED_INNER_AUTH}[0]
	# Entering the inout values for validation
	FOR    ${CRED_INNER}     IN   @{CRED_INNER_AUTH}
		IF  '${CRED_INNER}'=='${CRED_INNER_AUTH}[1]'
			Click Element    id=dot1xUsersAuthMethod2
			Select From List By Value			id=dot1xUsersAuthMethod2  ${CRED_INNER}
			Click Element       jquery=#saveButton
			Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
			Wait Until Element Contains	jquery=#errormsg	PEAP only accepts MSCHAPV2 and MD5.
		END
		IF  '${CRED_INNER}'=='${CRED_INNER_AUTH}[2]'
			Click Element    id=dot1xUsersAuthMethod2
			Select From List By Value			id=dot1xUsersAuthMethod2  ${CRED_INNER}
			Click Element       jquery=#saveButton
			Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
			Wait Until Element Contains	jquery=#errormsg	PEAP only accepts MSCHAPV2 and MD5.
		END
		IF  '${CRED_INNER}'=='${CRED_INNER_AUTH}[3]'
			Click Element    id=dot1xUsersAuthMethod2
			Select From List By Value			id=dot1xUsersAuthMethod2  ${CRED_INNER}
			Click Element       jquery=#saveButton
			Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
			Wait Until Element Contains	jquery=#errormsg	PEAP only accepts MSCHAPV2 and MD5.
		END
	END

	GUI::Basic::Cancel
	GUI::Network::802.1x::Delete Credentials If Exists  ${CRED_NAMES}[0]

Test Integrity On Certificate Button And Credential Name
		# Skipping this test if the device is not a NSR
	Skip If            ${IS_NSR} == ${FALSE}    System is not NSR
	# Clicking on 802.1x tab as the device is switched
	GUI::Network::802.1x::Open Credentials Tab
	SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[0]

	GUI::Basic::Select Checkbox  		//tr[@id='${CRED_NAMES}[1]']/td/input
	Click Element    id=certificate
	GUI::Basic::Spinner Should Be Invisible

	Click Element								id=dot1xCertCN
	${COMMON_NAME}=  Get Element Attribute		id=dot1xCertCN  value
	Should Be Equal    ${COMMON_NAME}    ${CRED_NAMES}[1]
	GUI::Basic::Cancel
	# Deleting the credentials if they exists
	GUI::Network::802.1x::Delete Credentials If Exists  ${CRED_NAMES}[1]

Test Integrity On Certificate Valid Values For Field=country_code
		# Skipping this test if the device is not a NSR
	Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
	# Clicking on 802.1x tab as the device is switched
	GUI::Network::802.1x::Open Credentials Tab
	SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[0]

	FOR    ${VALID_COUNTRY_CODE}   IN  @{VALID_COUNTRY_CODES}
		GUI::Basic::Select Checkbox  		//tr[@id='${CRED_NAMES}[1]']/td/input
		Click Element    id=certificate
		GUI::Basic::Spinner Should Be Invisible

		GUI::Network::802.1x::Create An Credentials Certificate  CERT_STATE=SC  CERT_LOCALITY=Blumenau
		...  CERT_EMAIL=t@t.com  CERT_INPUT_PASSWORD=1  CERT_OUTPUT_PASSWORD=1  CERT_ORGANIZATION=ZPE Brazil
		...   CERT_COUNTRY=${VALID_COUNTRY_CODE}

		GUI::Basic::Cancel
	END

	${INVALID_NUMBERS}=   Create List     ${POINTS}   ${ONE_POINTS}   ${TWO_POINTS}
	...     ${THREE_POINTS}     ${SEVEN_POINTS}  ${NUMBER_AND_POINTS}    ${POINTS_AND_NUMBER}
	...     ${POINTS_AND_WORD}   ${EIGHT_POINTS}

	GUI::Basic::Select Checkbox  		//tr[@id='${CRED_NAMES}[1]']/td/input
	Click Element    id=certificate
	GUI::Basic::Spinner Should Be Invisible

	GUI::Network::802.1x::Create An Credentials Certificate  CERT_STATE=SC  CERT_LOCALITY=Blumenau
	...  CERT_EMAIL=t@t.com  CERT_INPUT_PASSWORD=1  CERT_OUTPUT_PASSWORD=1  CERT_ORGANIZATION=ZPE Brazil
	...   CERT_COUNTRY=${VALID_COUNTRY_CODE}  CLICK_ON_SAVE=${FALSE}

	FOR    ${INVALID_NUMBER}     IN   @{INVALID_NUMBERS}
		Input Text	id=dot1xCertCountry    ${INVALID_NUMBER}
		Click Element       jquery=#saveButton
		Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
		Wait Until Element Contains	jquery=#errormsg	Illegal Characters. Use i.e. US

		Input Text	id=dot1xCertCountry     ${EMPTY}
	END

	Input Text	id=dot1xCertCountry    ${EMPTY}
	Click Element       jquery=#saveButton
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Field must not be empty.

	GUI::Basic::Cancel
	# Deleting the credentials if exists
	GUI::Network::802.1x::Delete Credentials If Exists  ${CRED_NAMES}[1]

Test Integrity On Certificate Valid Values For Field=state
		# Skipping this test if the device is not a NSR
	Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
	# Clicking on 802.1x tab as the device is switched
	GUI::Network::802.1x::Open Credentials Tab
	SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[0]

	FOR    ${VALID_STATE}   IN  @{VALID_STATES}
		GUI::Basic::Select Checkbox  		//tr[@id='${CRED_NAMES}[1]']/td/input
		Click Element    id=certificate
		GUI::Basic::Spinner Should Be Invisible

		GUI::Network::802.1x::Create An Credentials Certificate  CERT_STATE=${VALID_STATE}  CERT_LOCALITY=Blumenau
		...  CERT_EMAIL=t@t.com  CERT_INPUT_PASSWORD=1  CERT_OUTPUT_PASSWORD=1  CERT_ORGANIZATION=ZPE Brazil
		...   CERT_COUNTRY=BR

		GUI::Basic::Cancel
	END

	${INVALID_STATES}=   Create List     ${POINTS}   ${ONE_POINTS}   ${TWO_POINTS}
	...     ${THREE_POINTS}     ${SEVEN_POINTS}   ${WORD_AND_POINTS}    ${NUMBER_AND_POINTS}    ${POINTS_AND_NUMBER}
	...     ${POINTS_AND_WORD}   ${EIGHT_POINTS}

	GUI::Basic::Select Checkbox  		//tr[@id='${CRED_NAMES}[1]']/td/input
	Click Element    id=certificate
	GUI::Basic::Spinner Should Be Invisible

	GUI::Network::802.1x::Create An Credentials Certificate  CERT_STATE=${EMPTY}  CERT_LOCALITY=Blumenau
	...  CERT_EMAIL=t@t.com  CERT_INPUT_PASSWORD=1  CERT_OUTPUT_PASSWORD=1  CERT_ORGANIZATION=ZPE Brazil
	...   CERT_COUNTRY=BR

	FOR    ${INVALID_STATE}     IN   @{INVALID_STATES}
		Input Text	id=dot1xCertState    ${INVALID_STATE}
		Click Element       jquery=#saveButton
		Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
		Wait Until Element Contains	jquery=#errormsg	Illegal Characters. Use i.e.

		Input Text	id=dot1xCertState     ${EMPTY}
	END

	Input Text	id=dot1xCertState    ${EMPTY}
	Click Element       jquery=#saveButton
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Field must not be empty.

	GUI::Basic::Cancel
		# Deleting the credentials if exists
	GUI::Network::802.1x::Delete Credentials If Exists  ${CRED_NAMES}[1]


	Skip If    ${IS_NSR} == ${FALSE}    System is not NSR

	SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[0]

	FOR    ${VALID_LOCALITY}   IN  @{VALID_LOCALITIES}
		GUI::Basic::Select Checkbox  		//tr[@id='${CRED_NAMES}[1]']/td/input
		Click Element    id=certificate
		GUI::Basic::Spinner Should Be Invisible

		GUI::Network::802.1x::Create An Credentials Certificate  CERT_STATE=SC  CERT_LOCALITY=${VALID_LOCALITY}
		...  CERT_EMAIL=t@t.com  CERT_INPUT_PASSWORD=1  CERT_OUTPUT_PASSWORD=1  CERT_ORGANIZATION=ZPE Brazil
		...  CERT_COUNTRY=BR

		GUI::Basic::Cancel
	END

	${INVALID_LOCALITY}=   Create List     ${POINTS}   ${ONE_POINTS}   ${TWO_POINTS}
	...     ${THREE_POINTS}     ${SEVEN_POINTS}   ${WORD_AND_POINTS}    ${NUMBER_AND_POINTS}    ${POINTS_AND_NUMBER}
	...     ${POINTS_AND_WORD}   ${EIGHT_POINTS}

	GUI::Basic::Select Checkbox  		//tr[@id='${CRED_NAMES}[1]']/td/input
	Click Element    id=certificate
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::802.1x::Create An Credentials Certificate  CERT_STATE=SC  CERT_LOCALITY=${Empty}
	...  CERT_EMAIL=t@t.com  CERT_INPUT_PASSWORD=1  CERT_OUTPUT_PASSWORD=1  CERT_ORGANIZATION=ZPE Brazil
	...  CERT_COUNTRY=BR

	FOR    ${INVALID_LOCALITY}   IN   @{INVALID_LOCALITY}
		Input Text	id=dot1xCertLocality    ${INVALID_LOCALITY}
		Click Element       jquery=#saveButton
		Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
		Wait Until Element Contains	jquery=#errormsg	Illegal Characters. Use i.e Los Angeles

		Input Text	id=dot1xCertLocality     ${EMPTY}
	END

	Input Text	id=dot1xCertLocality    ${EMPTY}
	Click Element       jquery=#saveButton
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Field must not be empty.
	# Clicking on cancel button
	GUI::Basic::Cancel
		# Deleting the credentials if exists
	GUI::Network::802.1x::Delete Credentials If Exists  ${CRED_NAMES}[1]

Test Integrity On Certificate Valid Values For Field=organization
		# Skipping this test if the device is not a NSR
	Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
	# Clicking on 802.1x tab as the device is switched
	GUI::Network::802.1x::Open Credentials Tab
	SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[0]

	FOR    ${VALID_ORGANIZATION}   IN  @{VALID_ORGANIZATIONS}
		GUI::Basic::Select Checkbox  		//tr[@id='${CRED_NAMES}[1]']/td/input
		Click Element    id=certificate
		GUI::Basic::Spinner Should Be Invisible

		GUI::Network::802.1x::Create An Credentials Certificate  CERT_STATE=SC  CERT_LOCALITY=Blumenau
		...  CERT_EMAIL=t@t.com  CERT_INPUT_PASSWORD=1  CERT_OUTPUT_PASSWORD=1  CERT_ORGANIZATION=${VALID_ORGANIZATION}
		...  CERT_COUNTRY=BR

		GUI::Basic::Cancel
	END

	GUI::Basic::Select Checkbox  		//tr[@id='${CRED_NAMES}[1]']/td/input
	Click Element    id=certificate
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::802.1x::Create An Credentials Certificate  CERT_STATE=SC  CERT_LOCALITY=Blumenau
		...  CERT_EMAIL=t@t.com  CERT_INPUT_PASSWORD=1  CERT_OUTPUT_PASSWORD=1  CERT_ORGANIZATION=${EMPTY}
		...  CERT_COUNTRY=BR
	Click Element       jquery=#saveButton
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Field must not be empty.

	GUI::Basic::Cancel
		# Deleting the credentials if exists
	GUI::Network::802.1x::Delete Credentials If Exists  ${CRED_NAMES}[1]

Test Integrity On Certificate Valid Values For Field=email
		# Skipping this test if the device is not a NSR
	Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
	# Clicking on 802.1x tab as the device is switched
	GUI::Network::802.1x::Open Credentials Tab
	SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[0]

	FOR    ${VALID_EMAIL}   IN  @{VALID_EMAIL_ADDRESS}
		GUI::Basic::Select Checkbox  		//tr[@id='${CRED_NAMES}[1]']/td/input
		Click Element    id=certificate
		GUI::Basic::Spinner Should Be Invisible

		GUI::Network::802.1x::Create An Credentials Certificate  CERT_STATE=SC  CERT_LOCALITY=Blumenau
		...  CERT_EMAIL=${VALID_EMAIL}  CERT_INPUT_PASSWORD=1  CERT_OUTPUT_PASSWORD=1  CERT_ORGANIZATION=ZPE Brazil
		...  CERT_COUNTRY=BR

		GUI::Basic::Cancel
	END

	${INVALID_EMAILS}=   Create List     ${POINTS}  ${ONE_POINTS}   ${TWO_POINTS}
	...     ${THREE_POINTS}     ${SEVEN_POINTS}   ${WORD_AND_POINTS}    ${NUMBER_AND_POINTS}    ${POINTS_AND_NUMBER}
	...     ${POINTS_AND_WORD}   ${EIGHT_POINTS}  @{INVALID_IPS}

	GUI::Basic::Select Checkbox  		//tr[@id='${CRED_NAMES}[1]']/td/input
	Click Element    id=certificate
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::802.1x::Create An Credentials Certificate  CERT_STATE=SC  CERT_LOCALITY=Blumenau
	...  CERT_EMAIL=${EMPTY}  CERT_INPUT_PASSWORD=1  CERT_OUTPUT_PASSWORD=1  CERT_ORGANIZATION=ZPE Brazil
	...  CERT_COUNTRY=BR

	FOR    ${INVALID_EMAIL}   IN   @{INVALID_EMAILS}
		Input Text	id=dot1xCertEmail    ${INVALID_EMAIL}
		Click Element       jquery=#saveButton
		Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
		Wait Until Element Contains	jquery=#errormsg	Invalid e-mail address
	END

	Input Text	id=dot1xCertEmail    ${EMPTY}
	Click Element       jquery=#saveButton
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Field must not be empty.
	# Clicking on cancel button
	GUI::Basic::Cancel
		# Deleting the credentials if exists
	GUI::Network::802.1x::Delete Credentials If Exists  ${CRED_NAMES}[1]

Test Integrity On Certificate Valid Values For Field=input_password
		# Skipping this test if the device is not a NSR
	Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
	# Clicking on 802.1x tab as the device is switched
	GUI::Network::802.1x::Open Credentials Tab
	SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[0]

	FOR    ${VALID_PASSWORD}   IN  @{VALID_PASSWORDS}
		GUI::Basic::Select Checkbox  		//tr[@id='${CRED_NAMES}[1]']/td/input
		Click Element    id=certificate
		GUI::Basic::Spinner Should Be Invisible

		GUI::Network::802.1x::Create An Credentials Certificate  CERT_STATE=SC  CERT_LOCALITY=Blumenau
		...  CERT_EMAIL=t@t.com  CERT_INPUT_PASSWORD=${VALID_PASSWORD}  CERT_OUTPUT_PASSWORD=1  CERT_ORGANIZATION=ZPE Brazil
		...  CERT_COUNTRY=BR

		GUI::Basic::Cancel
	END

	${INVALID_PASSWORDS}=   Create List     ${POINTS}  ${ONE_POINTS}  ${TWO_POINTS}
	...     ${THREE_POINTS}  ${SEVEN_POINTS}  ${WORD_AND_POINTS}  ${NUMBER_AND_POINTS}    ${POINTS_AND_NUMBER}
	...     ${POINTS_AND_WORD}  ${EIGHT_POINTS}  @{INVALID_IPS}

	GUI::Basic::Select Checkbox  		//tr[@id='${CRED_NAMES}[1]']/td/input
	Click Element    id=certificate
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::802.1x::Create An Credentials Certificate  CERT_STATE=SC  CERT_LOCALITY=Blumenau
	...  CERT_EMAIL=t@t.com  CERT_INPUT_PASSWORD=${EMPTY}  CERT_OUTPUT_PASSWORD=1  CERT_ORGANIZATION=ZPE Brazil
	...  CERT_COUNTRY=BR

	Click Element       jquery=#saveButton
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Field must not be empty.

	GUI::Basic::Cancel
		# Deleting the credentials if exists
	GUI::Network::802.1x::Delete Credentials If Exists  ${CRED_NAMES}[1]

Test Integrity On Certificate Valid Values For Field=output_password
		# Skipping this test if the device is not a NSR
	Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
	# Clicking on 802.1x tab as the device is switched
	GUI::Network::802.1x::Open Credentials Tab
	SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[0]

	FOR    ${VALID_PASSWORD}   IN  @{VALID_PASSWORDS}
		GUI::Basic::Select Checkbox  		//tr[@id='${CRED_NAMES}[1]']/td/input
		Click Element    id=certificate
		GUI::Basic::Spinner Should Be Invisible

		GUI::Network::802.1x::Create An Credentials Certificate  CERT_STATE=SC  CERT_LOCALITY=Blumenau
		...  CERT_EMAIL=t@t.com  CERT_INPUT_PASSWORD=1  CERT_OUTPUT_PASSWORD=${VALID_PASSWORD}  CERT_ORGANIZATION=ZPE Brazil
		...  CERT_COUNTRY=BR

		GUI::Basic::Cancel
	END

	${INVALID_PASSWORDS}=   Create List     ${POINTS}   ${ONE_POINTS}   ${TWO_POINTS}
	...  ${THREE_POINTS}     ${SEVEN_POINTS}   ${WORD_AND_POINTS}    ${NUMBER_AND_POINTS}    ${POINTS_AND_NUMBER}
	...  ${POINTS_AND_WORD}   ${EIGHT_POINTS}   @{INVALID_IPS}

	GUI::Basic::Select Checkbox  		//tr[@id='${CRED_NAMES}[1]']/td/input
	Click Element    id=certificate
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::802.1x::Create An Credentials Certificate  CERT_STATE=SC  CERT_LOCALITY=Blumenau
	...  CERT_EMAIL=t@t.com  CERT_INPUT_PASSWORD=1  CERT_OUTPUT_PASSWORD=${EMPTY}  CERT_ORGANIZATION=ZPE Brazil
	...  CERT_COUNTRY=BR

	Click Element       jquery=#saveButton
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Field must not be empty.

	GUI::Basic::Cancel
		# Deleting the credentials if exists
	GUI::Network::802.1x::Delete Credentials If Exists  ${CRED_NAMES}[1]

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open and Login Nodegrid
	Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
	SUITE:Delete 802.1x Credentials

SUITE:Teardown
		Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
		SUITE:Delete 802.1x Credentials
		GUI::Basic::Logout and Close Nodegrid
		Close All Browsers

SUITE:Add 802.1x Credential
	[Arguments]     ${CRED_NAME}  ${CRED_AUTH_METHOD}   ${CRED_ANONYMOUS}=${EMPTY}  ${CRED_INNER_AUTH}=${EMPTY}
	GUI::Network::802.1x::Open Credentials Tab
	GUI::Basic::Add
	GUI::Network::802.1x::Create An Credentials  CRED_NAME=${CRED_NAME}  CRED_PASSWORD=${QA_PASSWORD}  CRED_AUTH_METHOD=${CRED_AUTH_METHOD}  CRED_ANONYMOUS=${CRED_ANONYMOUS}
	...  CRED_INNER_AUTH=${CRED_INNER_AUTH}

SUITE:Delete 802.1x Credentials
	GUI::Network::802.1x::Open Credentials Tab

	FOR  ${CRED_NAME}  IN  @{CRED_NAMES}
		GUI::Network::802.1x::Delete Credentials If Exists  ${CRED_NAME}
	END

SUITE:Add 802.1x Credentials Without Commit
	[Arguments]  ${CRED_NAME}  ${CRED_AUTH_METHOD}  ${CRED_ANONYMOUS}  ${CRED_INNER_AUTH}

	GUI::Basic::Add
	GUI::Network::802.1x::Create An Credentials  CRED_NAME=${CRED_NAME}  CRED_PASSWORD=${QA_PASSWORD}  CRED_AUTH_METHOD=${CRED_AUTH_METHOD}  CRED_ANONYMOUS=${CRED_ANONYMOUS}
	...  CRED_INNER_AUTH=${CRED_INNER_AUTH}  CLICK_ON_SAVE=${False}