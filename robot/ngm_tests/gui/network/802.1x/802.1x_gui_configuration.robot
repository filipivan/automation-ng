*** Settings ***
Resource        ../../init.robot
Documentation   Tests Settings > configuration file about 802.1x Feature through the GUI
Metadata        Version	1.0
Metadata        Executed At	${HOST}
Force Tags      GUI   EXCLUDEIN3_2    EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
@{CRED_AUTH_METHOD}     MD5   TLS    PEAP    TTLS
@{CRED_INNER_AUTH}      MD5   PAP    CHAP    MSCHAP    MSCHAPV2
@{CRED_NAMES}           test_cred_md5  test_cred_tls  test_cred_peap_md5  test_cred_peap_mschapv2  test_cred_ttls_md5
                        ...  test_cred_ttls_pap  test_cred_ttls_chap  test_cred_ttls_mschap  test_cred_ttls_mschapv2
@{CRED_ANONYMOUS}       test_cred_anonymous_0   test_cred_anonymous_1   test_cred_anonymous_2   test_cred_anonymous_3
                        ...  test_cred_anonymous_4  test_cred_anonymous_5  test_cred_anonymous_6
@{PROF_TYPES}           internal_eap_server   radius_server  supplicant
@{PROF_NAMES}           test_prof_internal_eap_server  test_prof_radius_server   test_prof_supplicant
${RETRANSMIT_INTERVAL}  15
${NETS_NAME}  netS

*** Test Cases ***
Test Check If Status Is Enabled For Sfp0 Port Using EAP Server
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    ${PROFILE}=    Get From List    ${PROF_NAMES}    0
    SUITE:Enable Interface And 802.1x On Switch Interfaces    sfp0   ${PROFILE}
    GUI::Basic::Network::Switch::Open Tab
    
    SUITE:Check 802.1x Profile Inside Switch Interface  sfp0    ${PROFILE}

Test Check If Status Is Enabled For Net Port Using EAP Server
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    ${PROFILE}=     Get From List    ${PROF_NAMES}    0
    SUITE:Enable Interface And 802.1x On Switch Interfaces    ${NETS_NAME}   ${PROFILE}
    GUI::Basic::Network::Switch::Open Tab

    SUITE:Check 802.1x Profile Inside Switch Interface  ${NETS_NAME}   ${PROFILE}

Test Check If Status Is Enabled For Sfp0 Port Using Radius Server
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    ${PROFILE}=  Get From List    ${PROF_NAMES}    1
    SUITE:Enable Interface And 802.1x On Switch Interfaces    sfp0   ${PROFILE}
    GUI::Basic::Network::Switch::Open Tab
    
    SUITE:Check 802.1x Profile Inside Switch Interface  sfp0   ${PROFILE}

Test Check If Status Is Enabled For Net Port Using Radius Server
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    ${PROFILE}=  Get From List    ${PROF_NAMES}    1
    SUITE:Enable Interface And 802.1x On Switch Interfaces    ${NETS_NAME}   ${PROFILE}
    GUI::Basic::Network::Switch::Open Tab

    SUITE:Check 802.1x Profile Inside Switch Interface  ${NETS_NAME}   ${PROFILE}

Test Check If Status Is Enabled For Sfp0 Port Using Supplicant Profile
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    ${PROFILE}=  Get From List    ${PROF_NAMES}    2
    SUITE:Enable Interface And 802.1x On Switch Interfaces    sfp0   ${PROFILE}
    GUI::Basic::Network::Switch::Open Tab

    SUITE:Check 802.1x Profile Inside Switch Interface  sfp0   ${PROFILE}

Test Check If Status Is Enabled For Net Port Using Supplicant Profile
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    ${PROFILE}=  Get From List    ${PROF_NAMES}    2
    SUITE:Enable Interface And 802.1x On Switch Interfaces    ${NETS_NAME}   ${PROFILE}
    GUI::Basic::Network::Switch::Open Tab

    SUITE:Check 802.1x Profile Inside Switch Interface  ${NETS_NAME}   ${PROFILE}
    
*** Keywords ***
SUITE:Setup
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    GUI::Basic::Open And Login Nodegrid
    SUITE:Get Interface Net's Name
    SUITE:Delete Configurations If Exists To 802.1x Network

    GUI::Network::802.1x::Open Credentials Tab
    SUITE:Add Configurations To 802.1x Network

SUITE:Teardown
    Close all Browsers

SUITE:Delete Configurations If Exists To 802.1x Network
    SUITE:Disable 802.1x On Switch Interfaces    sfp0
    SUITE:Disable 802.1x On Switch Interfaces    ${NETS_NAME}

    GUI::Network::802.1x::Open Credentials Tab
    SUITE:Delete 802.1x Credentials  @{PROF_NAMES}

    GUI::Network::802.1x::Open Profiles Tab
    SUITE:Delete 802.1x Profiles  @{PROF_NAMES}

SUITE:Check 802.1x Profile Inside Switch Interface
    [Arguments]  ${SWITCH_INTERFACE}   ${PROF_NAME}
    
    ${GRID_CONTENT}=  Get Text    id=switchItfTable  
    Should Contain    ${GRID_CONTENT}    ${SWITCH_INTERFACE} Enabled Auto 1 Disabled None None Disabled Enabled
    
	Click Element	//*[@id="${SWITCH_INTERFACE}"]/td[2]/a
    GUI::Basic::Spinner Should Be Invisible

    ${SWITCH_PROFILE}=  Get Selected List Label    id=dot1x
    
    Should Be Equal    ${SWITCH_PROFILE}    ${PROF_NAME}
    GUI::Basic::Cancel

SUITE:Add Configurations To 802.1x Network
    GUI::Network::802.1x::Open Credentials Tab
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[2]  ${CRED_AUTH_METHOD}[2]  ${CRED_ANONYMOUS}[0]  ${CRED_INNER_AUTH}[0]
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[3]  ${CRED_AUTH_METHOD}[2]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[4]
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[4]  ${CRED_AUTH_METHOD}[3]  ${CRED_ANONYMOUS}[2]  ${CRED_INNER_AUTH}[0]
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[5]  ${CRED_AUTH_METHOD}[3]  ${CRED_ANONYMOUS}[3]  ${CRED_INNER_AUTH}[1]
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[6]  ${CRED_AUTH_METHOD}[3]  ${CRED_ANONYMOUS}[4]  ${CRED_INNER_AUTH}[2]
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[7]  ${CRED_AUTH_METHOD}[3]  ${CRED_ANONYMOUS}[5]  ${CRED_INNER_AUTH}[3]
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[8]  ${CRED_AUTH_METHOD}[3]  ${CRED_ANONYMOUS}[6]  ${CRED_INNER_AUTH}[4]
    
    GUI::Network::802.1x::Open Profiles Tab
    SUITE:Add 802.1x Profiles

SUITE:Add 802.1x Credential
    [Arguments]  ${CRED_NAME}  ${CRED_AUTH_METHOD}  ${CRED_ANONYMOUS}=${EMPTY}  ${CRED_INNER_AUTH}=${EMPTY}
    GUI::Basic::Add
    GUI::Network::802.1x::Create An Credentials  ${CRED_NAME}  ${QA_PASSWORD}  ${CRED_AUTH_METHOD}  ${CRED_ANONYMOUS}  ${CRED_INNER_AUTH} 

SUITE:Delete 802.1x Credentials
    [Arguments]  @{PROF_NAMES}

    FOR    ${CRED_NAME}     IN   @{CRED_NAMES}
        GUI::Network::802.1x::Delete Credentials If Exists  ${CRED_NAME}
    END

SUITE:Add 802.1x Profiles
    ${SELECT_USERS}=         Catenate    SEPARATOR=,    @{CRED_NAMES}

    FOR    ${PROF_TYPE}     IN   @{PROF_TYPES}
        GUI::Basic::Add
        IF  '${PROF_TYPE}' == 'internal_eap_server'
            @{TEMP_PROF_USERS}=  Create List  ${CRED_NAMES}[0]
            GUI::Network::802.1x::Create An Profile  PROF_NAME=${PROF_NAMES}[0]  PROF_TYPE=${PROF_TYPE}  
            ...  PROF_USERS=@{TEMP_PROF_USERS}  PROF_TRANSMIT_INTERVAL=${RETRANSMIT_INTERVAL}
        END
        
        IF  '${PROF_TYPE}' == 'radius_server'
            GUI::Network::802.1x::Create An Profile  PROF_NAME=${PROF_NAMES}[1]  PROF_TYPE=${PROF_TYPE}  
            ...  PROF_IP_ADDRESS=${RADIUSSERVER2}  PROF_PORT_NUMBER=${RADIUSSERVER2_PORT}  
            ...  PROF_SHARED_SECRET=${RADIUSSERVER2_SECRET}  PROF_RETRANSMIT_INTERVAL=${RETRANSMIT_INTERVAL}
        END

        IF  '${PROF_TYPE}' == 'supplicant'
            @{TEMP_PROF_USERS}=  Create List  ${CRED_NAMES}[0]
            GUI::Network::802.1x::Create An Profile  PROF_NAME=${PROF_NAMES}[2]  PROF_TYPE=${PROF_TYPE}  
            ...  PROF_USERS=@{TEMP_PROF_USERS}
        END
    END

SUITE:Delete 802.1x Profiles
    [Arguments]  @{PROF_NAMES}
    FOR    ${PROF_NAME}     IN   @{PROF_NAMES}
        Log  ${PROF_NAME}
        GUI::Network::802.1x::Delete Profiles If Exists  ${PROF_NAME}
    END

SUITE:Enable Interface And 802.1x On Switch Interfaces
    [Arguments]  ${SWITCH_INTERFACE}  ${PROF_NAME}
    GUI::Basic::Network::Switch::Open Tab
    
    GUI::Basic::Network::Switch::open tab
	GUI::Basic::Click Element	//span[normalize-space()='Switch Interfaces']
	Select Checkbox	//tr[@id='${SWITCH_INTERFACE}']//input[@type='checkbox']
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//input[@id='editButton']
	GUI::Basic::Click Element	//select[@id='status']
	Select From List By Label	//select[@id='status']	Enabled

    Wait Until Element Is Visible    dot1xEnabled
    Select Checkbox     xpath=//*[@id="dot1xEnabled"]
    Wait Until Element Is Visible    dot1x
    Select From List By Value    xpath=//*[@id="dot1x"]  ${PROF_NAME}  #Select credential to use

    GUI::Basic::Save

SUITE:Disable 802.1x On Switch Interfaces
    [Arguments]  ${INTERFACE}
    GUI::Network::Switch::Switch Interfaces::Enable switch interface  ${INTERFACE}
    #set status=disabled and enable_802.1x=no

SUITE:Get Interface Net's Name
    GUI::Basic::Network::Switch::Open Tab
    ${NETS_NAME}=  Format String    netS1-1
    Set Suite Variable  ${NETS_NAME}