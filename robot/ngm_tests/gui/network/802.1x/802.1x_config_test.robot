*** Settings ***
Documentation	Automated test for 802.1x support-configuration/validation
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX
Default Tags	EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${User1}	admin
${Password1}	Admin@123
${User2}	admin1
${User3}	admin2
${User4}	admin3
${Authentication_method1}	MD5
${Authentication_method2}	TLS
${Authentication_method3}	PEAP
${Authentication_method4}	TTLS

*** Test Cases ***
Test case to add a profile to validate with empty user
	# Login into Nodegrid
	GUI::Basic::Open And Login Nodegrid
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on 802.1x tab as the device is switched and NSR
	GUI::Network::802.1x::Open Profiles Tab
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=dot1xName	profile1
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	# Error validation for empty user
	Page Should Contain	Field must not be empty.

Test case to configure credentials and profile with MD5 mode
	# Login into Nodegrid
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on 802.1x tab as the device is switched and NSR
	GUI::Network::802.1x::Open Credentials Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchDot1xUsers > .title_b
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=dot1xUsersName	${User1}
	Click Element	id=dot1xUsersPassword
	Clear Element Text	id=dot1xUsersPassword
	Input Text	id=dot1xUsersPassword	${Password1}
	Click Element	id=dot1xUsersPasswordAgain
	Clear Element Text	id=dot1xUsersPasswordAgain
	Input Text	id=dot1xUsersPasswordAgain	${Password1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${Authentication_method1}
	Click Element	css=#netDot1x > .title_b
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=dot1xName	profile1
	Click Element	//*[@id="dot1xIntUsers"]/div/div[1]/div[1]/select/option
	Click Element	//*[@id="dot1xIntUsers"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Test case to edit credentials with TLS mode
	Click Element	css=#netswitchDot1xUsers > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${User1}"]/td[1]/input
	Sleep	5s
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=dot1xUsersName	${User2}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=dot1xUsersAuthMethod
	Select From List By Label	id=dot1xUsersAuthMethod	${Authentication_method2}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${Authentication_method2}
	Click Element	css=#netswitchDot1xUsers > .title_b
	GUI::Basic::Spinner Should Be Invisible
	# Editing the credentials and deleting the changes
	Select Checkbox	xpath=//tr[@id='${User2}']/td/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

Test case to edit credentials with PEAP mode
	Click Element	css=#netswitchDot1xUsers > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${User1}"]/td[1]/input
	Sleep	5s
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	# Inputting the values
	Input Text	id=dot1xUsersName	${User3}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=dot1xUsersAuthMethod
	Select From List By Label	id=dot1xUsersAuthMethod	 ${Authentication_method3}
	Click Element	id=dot1xUsersAnonId
	Input Text	id=dot1xUsersAnonId	${Password1}
	Click Element	id=dot1xUsersAuthMethod2
	Select From List By Label	id=dot1xUsersAuthMethod2	PAP
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	PEAP only accepts MSCHAPV2 and MD5.
	Select From List By Label	id=dot1xUsersAuthMethod2	MSCHAP
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	PEAP only accepts MSCHAPV2 and MD5.
	Select From List By Label	id=dot1xUsersAuthMethod2	MD5
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${Authentication_method3}
	 # Clicking the checkbox and delting the changes
	Select Checkbox	xpath=//tr[@id='${User3}']/td/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

Test case to edit credentials with TTLS mode
	Click Element	css=#netswitchDot1xUsers > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${User1}"]/td[1]/input
	Sleep	5s
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	# Inputting the values
	Input Text	id=dot1xUsersName	${User4}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=dot1xUsersAuthMethod
	Select From List By Label	id=dot1xUsersAuthMethod	 ${Authentication_method4}
	Input Text	id=dot1xUsersAnonId	${Password1}
	Click Element	id=dot1xUsersAuthMethod2
	Select From List By Label	id=dot1xUsersAuthMethod2	PAP
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${Authentication_method4}
	 # Clicking the checkbox and delting the changes
	Select Checkbox	xpath=//tr[@id='${User4}']/td/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on 802.1x tab-> Profiles tab as the device is switched and NSR
	GUI::Network::802.1x::Open Profiles Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#profile1 input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on 802.1x tab-> credentials tab as the device is switched and NSR
	GUI::Network::802.1x::Open Credentials Tab
	GUI::Basic::Spinner Should Be Invisible
	# Deleting the changes
	Select Checkbox	//*[@id="admin"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout and Close Nodegrid