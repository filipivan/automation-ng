*** Settings ***
Documentation	Testing the QOS configuration
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${TEST_QOS_RULE_NAME}	Automation_qos_rule.name
${TEST_QOS_RULE_PROTOCOL}	tcp,udp
${TEST_QOS_CLASS_NAME}	Automation_qos_class.name
${TEST_QOS_INTERFACE_NAME}	Automation_qos_interface.test
${TEST_QOS_CLASS_IN/OUTPUT_UNIT}	%
${TEST_QOS_INTERFACE_IN/OUTPUT_B}	500
${TEST_QOS_INTERFACE_IN/OUTPUT_UNIT}	bit/s

*** Test Cases ***
Test case to Check each field in QoS rule Add page
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//span[normalize-space()='Rules']
	GUI::Basic::Add
	GUI::Basic::Click Element	//input[@id='qos_form_rule_name']
	GUI::Basic::Input Text	//input[@id='qos_form_rule_name']	${TEST_QOS_RULE_NAME}
	GUI::Basic::Select Checkbox	//input[@id='qos_form_rule_ipv4']
	GUI::Basic::Unselect Checkbox	//input[@id='qos_form_rule_ipv6']
	GUI::Basic::Unselect Checkbox	//input[@id='qos_form_rule_syn']
	GUI::Basic::Unselect Checkbox	//input[@id='qos_form_rule_ack']
	GUI::Basic::Click Element	//input[@id='qos_form_rule_protocol']
	GUI::Basic::Input Text	//input[@id='qos_form_rule_protocol']	${TEST_QOS_RULE_PROTOCOL}
	GUI::Basic::Click Element	//input[@id='qos_form_rule_src']
	GUI::Basic::Input Text	//input[@id='qos_form_rule_src']	${EMPTY}
	GUI::Basic::Click Element	//input[@id='qos_form_rule_dst']
	GUI::Basic::Input Text	//input[@id='qos_form_rule_dst']	${EMPTY}
	GUI::Basic::Click Element	//input[@id='qos_form_rule_sports']
	GUI::Basic::Input Text	//input[@id='qos_form_rule_sports']	${EMPTY}
	GUI::Basic::Click Element	//input[@id='qos_form_rule_dports']
	GUI::Basic::Input Text	//input[@id='qos_form_rule_dports']	${EMPTY}
	GUI::Basic::Click Element	//input[@id='qos_form_rule_smac']
	GUI::Basic::Input Text	//input[@id='qos_form_rule_smac']	${EMPTY}
	GUI::Basic::Click Element	//input[@id='qos_form_rule_dmac']
	GUI::Basic::Input Text	//input[@id='qos_form_rule_dmac']	${EMPTY}
	GUI::Basic::Click Element	//input[@id='qos_form_rule_extra']
	GUI::Basic::Input Text	//input[@id='qos_form_rule_extra']	${EMPTY}
	GUI::Basic::Click Element	//select[@class='selectbox groupFrom']
	GUI::Basic::Click Element	//button[contains(text(),'Add ►')]
	GUI::Basic::Click Element	//button[contains(text(),'◄ Remove')]
	GUI::Basic::Click Element	//select[@class='selectbox groupTo']
	GUI::Basic::Cancel

Test case to Check each field in QoS classes Add page
	GUI::Basic::Network::QoS::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//span[normalize-space()='Classes']
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//input[@id='qos_form_class_name']
	GUI::Basic::Input Text	//input[@id='qos_form_class_name']	${TEST_QOS_CLASS_NAME}
	GUI::Basic::Click Element	//select[@id='qos_form_priority']
	Select From List By Value	//select[@id='qos_form_priority']	1
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//input[@id='qos_form_extra']
	GUI::Basic::Input Text	//input[@id='qos_form_extra']	test
	GUI::Basic::Click Element	//div[@id='qos_form_rule_table']//select[@class='selectbox groupFrom']
	GUI::Basic::Click Element	//div[@id='qos_form_rule_table']//button[@class='listbuilder_btn'][contains(text(),'Add ►')]
	GUI::Basic::Click Element	//div[@id='qos_form_rule_table']//div[@class='col-xs-2']//div[2]//button[1]
	GUI::Basic::Click Element	//div[@id='qos_form_rule_table']//select[@class='selectbox groupTo']
	GUI::Basic::Click Element	//div[@id='qos_form_interface_table']//select[@class='selectbox groupFrom']
	GUI::Basic::Click Element	//div[@id='qos_form_interface_table']//select[@class='selectbox groupTo']
	GUI::Basic::Click Element	//body//div[@class='content']//div[@id='qos_form_interface_table']//div[@class='row']//div[@class='row']//div[1]//button[1]
	GUI::Basic::Click Element	//div[@id='qos_form_interface_table']//div[@class='col-xs-2']//div[2]//button[1]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//input[@id='qos_form_input_min_rate_value']
	GUI::Basic::Input Text	//input[@id='qos_form_input_min_rate_value']	50
	GUI::Basic::Click Element	//select[@id='qos_form_input_min_rate_unit']
	Select From List By Label	//select[@id='qos_form_input_min_rate_unit']	${TEST_QOS_CLASS_IN/OUTPUT_UNIT}
	GUI::Basic::Click Element	//input[@id='qos_form_output_min_rate_value']
	GUI::Basic::Input Text	//input[@id='qos_form_output_min_rate_value']	100
	GUI::Basic::Click Element	//select[@id='qos_form_output_min_rate_unit']
	Select From List By Label	//select[@id='qos_form_output_min_rate_unit']	${TEST_QOS_CLASS_IN/OUTPUT_UNIT}
	GUI::Basic::Cancel

Test case to Check each field in QoS interfaces Add page
	GUI::Basic::Network::QoS::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//span[normalize-space()='Interfaces']
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//input[@id='qos_form_name']
	GUI::Basic::Input Text	//input[@id='qos_form_name']	${TEST_QOS_INTERFACE_NAME}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//input[@id='qos_form_extra']
	GUI::Basic::Input Text	//input[@id='qos_form_extra']	${TEST_QOS_INTERFACE_NAME}
	GUI::Basic::Click Element	//select[@class='selectbox groupFrom']
	GUI::Basic::Click Element	//button[contains(text(),'Add ►')]
	GUI::Basic::Click Element	//select[@class='selectbox groupTo']
	GUI::Basic::Click Element	//div[@class='col-xs-2']//div[2]//button[1]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//input[@id='qos_form_input_rate_value']
	GUI::Basic::Input Text	//input[@id='qos_form_input_rate_value']	${TEST_QOS_INTERFACE_IN/OUTPUT_B}
	GUI::Basic::Click Element	//select[@id='qos_form_input_rate_unit']
	Select From List By Label	//select[@id='qos_form_input_rate_unit']	${TEST_QOS_INTERFACE_IN/OUTPUT_UNIT}
	GUI::Basic::Click Element	//input[@id='qos_form_output_rate_value']
	GUI::Basic::Input Text	//input[@id='qos_form_output_rate_value']	${TEST_QOS_INTERFACE_IN/OUTPUT_B}
	GUI::Basic::Click Element	//select[@id='qos_form_output_rate_unit']
	Select From List By Label	//select[@id='qos_form_output_rate_unit']	${TEST_QOS_INTERFACE_IN/OUTPUT_UNIT}
	GUI::Basic::Cancel

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::QoS::open tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Close All Browsers