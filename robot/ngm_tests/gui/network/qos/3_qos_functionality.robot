*** Settings ***
Documentation	Testing the QOS configuration
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	EXCLUDEIN4_2	EXCLUDEIN5_0

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${TEST_QOS_RULE_NAME}	Automation_qos_rule.name
${TEST_QOS_CLASS_NAME}	Automation_qos_class.name
${TEST_QOS_INTERFACE_ETH0}	eth0
${TEST_QOS_INTERFACE_ETH1}	eth1
${TEST_QOS_INTERFACE_BACK0}	backplane0
${TEST_QOS_INTERFACE_BACK1}	backplane1
${TEST_CONNECTION_ETH0}	ETH0
${TEST_CONNECTION_ETH1}	ETH1
${TEST_CONNECTION_BACK0}	BACKPLANE0
${TEST_CONNECTION_BACK1}	BACKPLANE1
${TEST_QOS_FORM_PRIORITY_VALUE}	0
${TEST_QOS_CLASS_IN/OUTPUT_RB}	100
${TEST_QOS_CLASS_IN/OUTPUT_UNIT}	Kbit/s
${TEST_QOS_INTERFACE_IN/OUTPUT_B}	10
${TEST_QOS_INTERFACE_IN/OUTPUT_UNIT}	Mbit/s
${STRONG_PASSWORD}	${QA_PASSWORD}

*** Test Cases ***
Test case to add QoS rule
	SUITE:Delete All QoS Rules If exists
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//input[@id='qos_form_rule_name']
	GUI::Basic::Input Text	//input[@id='qos_form_rule_name']	${TEST_QOS_RULE_NAME}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Check Rows In Table	qos_rules_table	1
	Page Should Not Contain	Error

Test case to add QoS classe
	SUITE:Delete All QoS Classes If exists
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//input[@id='qos_form_class_name']
	GUI::Basic::Input Text	//input[@id='qos_form_class_name']	${TEST_QOS_CLASS_NAME}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//select[@id='qos_form_priority']
	Select From List By Value	//select[@id='qos_form_priority']	${TEST_QOS_FORM_PRIORITY_VALUE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//div[@id='qos_form_rule_table']//select[@class='selectbox groupFrom']
	GUI::Basic::Click Element	//option[@value='${TEST_QOS_RULE_NAME}']
	GUI::Basic::Click Element	//div[@id='qos_form_rule_table']//button[@class='listbuilder_btn'][contains(text(),'Add ►')]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//input[@id='qos_form_input_min_rate_value']
	GUI::Basic::Input Text	//input[@id='qos_form_input_min_rate_value']	${TEST_QOS_CLASS_IN/OUTPUT_RB}
	GUI::Basic::Click Element	//select[@id='qos_form_input_min_rate_unit']
	Select From List By Label	//select[@id='qos_form_input_min_rate_unit']	${TEST_QOS_CLASS_IN/OUTPUT_UNIT}
	GUI::Basic::Click Element	//input[@id='qos_form_output_min_rate_value']
	GUI::Basic::Input Text	//input[@id='qos_form_output_min_rate_value']	${TEST_QOS_CLASS_IN/OUTPUT_RB}
	GUI::Basic::Click Element	//select[@id='qos_form_output_min_rate_unit']
	Select From List By Label	//select[@id='qos_form_output_min_rate_unit']	${TEST_QOS_CLASS_IN/OUTPUT_UNIT}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Check Rows In Table	qos_class_table	1
	Page Should Not Contain	Error

Test case to add eth0 QoS interface
	SUITE:Delete All QoS Interfaces If exists
	SUITE:Add QoS Interface	${TEST_QOS_INTERFACE_ETH0}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Check Rows In Table	interfaceTable	1
	Page Should Contain	${TEST_QOS_INTERFACE_ETH0}
	Page Should Not Contain	Error
	${FIREQOS_TABLE_CONTENT}	GUI::Tracking::Get Tracking QoS Table Text
	Should Contain	${FIREQOS_TABLE_CONTENT}	${TEST_QOS_INTERFACE_ETH0}

Test case to add eth1 QoS interface
	${HOST_HAS_ETH1}	GUI::Network::Check If Has Connection	${TEST_CONNECTION_ETH1}
	Set Suite Variable	${HOST_HAS_ETH1}
	GUI::Basic::Switch Browser	ALIAS=PEER_HOMEPAGE
	${PEER_HAS_ETH1}	GUI::Network::Check If Has Connection	${TEST_CONNECTION_ETH1}
	Set Suite Variable	${PEER_HAS_ETH1}
	Skip If	not ${HOST_HAS_ETH1} or not ${PEER_HAS_ETH1}	This automation test environment has no ETH1 connection
	GUI::Basic::Switch Browser	ALIAS=HOST_HOMEPAGE
	GUI::Basic::Network::QoS::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//span[normalize-space()='Interfaces']
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add QoS Interface	${TEST_QOS_INTERFACE_ETH1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${TEST_QOS_INTERFACE_ETH1}
	Page Should Not Contain	Error
	${FIREQOS_TABLE_CONTENT}	GUI::Tracking::Get Tracking QoS Table Text
	Should Contain	${FIREQOS_TABLE_CONTENT}	${TEST_QOS_INTERFACE_ETH1}

Test case to add backplane0 QoS interface
	Skip If	not ${VLAN_AVAILABLE}	This automation test environment has no VLAN available
	GUI::Basic::Switch Browser	ALIAS=SHARED_HOMEPAGE
	${HOSTSHARED_HAS_BACK0}	GUI::Network::Check If Has Connection	BACKPLANE0
	Set Suite Variable	${HOSTSHARED_HAS_BACK0}
	GUI::Basic::Switch Browser	ALIAS=HOST_HOMEPAGE
	${HOST_HAS_BACK0}	GUI::Network::Check If Has Connection	BACKPLANE0
	Set Suite Variable	${HOST_HAS_BACK0}
	Skip If	not ${HOST_HAS_BACK0}	HOST has no BACKPLANE0 connection
	Skip If	not ${HOSTSHARED_HAS_BACK0}	HOSTSHARED has no BACKPLANE0 connection
	GUI::Basic::Network::QoS::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//span[normalize-space()='Interfaces']
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add QoS Interface	${TEST_QOS_INTERFACE_BACK0}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${TEST_QOS_INTERFACE_BACK0}
	Page Should Not Contain	Error
	${FIREQOS_TABLE_CONTENT}	GUI::Tracking::Get Tracking QoS Table Text
	Should Contain	${FIREQOS_TABLE_CONTENT}	${TEST_QOS_INTERFACE_BACK0}

Test case to add backplane1 QoS interface
	Skip If	not ${VLAN_AVAILABLE}	This automation test environment has no VLAN available
	GUI::Basic::Switch Browser	ALIAS=SHARED_HOMEPAGE
	${HOSTSHARED_HAS_BACK1}	GUI::Network::Check If Has Connection	BACKPLANE1
	Set Suite Variable	${HOSTSHARED_HAS_BACK1}
	GUI::Basic::Switch Browser	ALIAS=HOST_HOMEPAGE
	${HOST_HAS_BACK1}	GUI::Network::Check If Has Connection	BACKPLANE1
	Set Suite Variable	${HOST_HAS_BACK1}
	Skip If	not ${HOST_HAS_BACK1}	HOST has no BACKPLANE1 connection
	Skip If	not ${HOSTSHARED_HAS_BACK1}	HOSTSHARED has no BACKPLANE1 connection
	GUI::Basic::Network::QoS::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//span[normalize-space()='Interfaces']
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add QoS Interface	${TEST_QOS_INTERFACE_BACK1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${TEST_QOS_INTERFACE_BACK1}
	Page Should Not Contain	Error
	${FIREQOS_TABLE_CONTENT}	GUI::Tracking::Get Tracking QoS Table Text
	Should Contain	${FIREQOS_TABLE_CONTENT}	${TEST_QOS_INTERFACE_BACK1}

Test case to add GSM QoS interface
	GUI::Basic::Switch Browser	ALIAS=HOST_HOMEPAGE
	${HAS_GSM}	GUI::Tracking::Check If Has GSM Module and Sim Card with Signal
	Set Suite Variable	${HAS_GSM}
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	${INDEX_NUMBER}	GUI::Tracking::Get Index Number With 5G Sim Card Up Otherwise 4G Up
	${ROW_NUMBER}	Evaluate	${INDEX_NUMBER} + 1
	${GSM_CDC_WDMX}	GUI::Tracking::Get Tracking Wireless Modem Table Cell	ROW=${ROW_NUMBER}	COLUMN=2
	Set Suite Variable	${GSM_CDC_WDMX}
	${GSM_OPERATOR_NAME}	GUI::Tracking::Get Tracking Wireless Modem Table Cell	ROW=${ROW_NUMBER}	COLUMN=7
	Set Suite Variable	${GSM_OPERATOR_NAME}
	${TEST_QOS_INTERFACE_GSM}	GUI::Tracking::Rewrite to root GSM interface format	${GSM_CDC_WDMX}
	Set Suite Variable	${TEST_QOS_INTERFACE_GSM}
	GUI::Basic::Network::QoS::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//span[normalize-space()='Interfaces']
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add QoS Interface	${TEST_QOS_INTERFACE_GSM}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${TEST_QOS_INTERFACE_GSM}
	Page Should Not Contain	Error
	${FIREQOS_TABLE_CONTENT}	GUI::Tracking::Get Tracking QoS Table Text
	Should Contain	${FIREQOS_TABLE_CONTENT}	${TEST_QOS_INTERFACE_GSM}
	[Teardown]	SUITE:Test Teardown

Test case to identify HOST and HOSTSHARED systems
	GUI::Basic::Switch Browser	ALIAS=HOST_HOMEPAGE
	${HOST_IS_NSR}	GUI::Basic::Is Net SR
	Set Suite Variable	${HOST_IS_NSR}
	${HOST_IS_GSR}	GUI::Basic::Is Gate SR
	Set Suite Variable	${HOST_IS_GSR}
	GUI::Basic::Switch Browser	ALIAS=SHARED_HOMEPAGE
	${HOSTSHARED_IS_NSR}	GUI::Basic::Is Net SR
	Set Suite Variable	${HOSTSHARED_IS_NSR}
	${HOSTSHARED_IS_GSR}	GUI::Basic::Is Gate SR
	Set Suite Variable	${HOSTSHARED_IS_GSR}

Test case to Setup VLAN and add static IPv4 to backplane0
	Skip If	not ${VLAN_AVAILABLE}	This automation test environment has no VLAN available
	Skip If	not ${HOST_HAS_BACK0}	HOST has no BACKPLANE0 connection
	Skip If	not ${HOSTSHARED_HAS_BACK0}	HOSTSHARED has no BACKPLANE0 connection
	Run Keyword If	not ${HOST_IS_NSR} and not ${HOST_IS_GSR}
	...	Fail	HOST system is not NSR and not GSR, the switch test cases must be updated for the new system
	Run Keyword If	not ${HOSTSHARED_IS_NSR} and not ${HOSTSHARED_IS_GSR}
	...	Fail	HOSTSHARED system is not NSR and not GSR, the switch test cases must be updated for the new system
	# Switch to host browser page
	GUI::Basic::Switch Browser	ALIAS=HOST_HOMEPAGE
	# Network backplane connection to default
	GUI::Network::Setup BACKPLANE Connection to default	BACKPLANE=${TEST_CONNECTION_BACK0}
	# VLAN to default
	Run Keyword If	${HOST_IS_NSR}	GUI::Network::Setup Untagged VLAN to default with sfp0 and sfp1 on switch
	...	ELSE	GUI::Network::Setup Untagged VLAN to default without sfp0 and sfp1 on switch
	# Switch interfaces to default
	Run Keyword If	${HOST_IS_NSR}	GUI::Network::Switch::Switch Interfaces::Setup Switch Interfaces to default with sfp0 and sfp1 on switch
	...	ELSE	GUI::Network::Switch::Switch Interfaces::Setup Switch Interfaces to default without sfp0 and sfp1 on switch
	# Edit VLAN
	Run Keyword If	${HOST_IS_NSR}	GUI::Network::Switch::Add Backplane1 and sfp1 to first Untagged VLAN
	...	ELSE	GUI::Network::Switch::Add Backplane1 and sfp1 to first Untagged VLAN	HAS_SFP1=No
	# Enable switch interface
	GUI::Network::Switch::Switch Interfaces::Enable switch interface	${SWITCH_NETPORT_HOST_INTERFACE2}
	# Setup Backplane with static IPv4 and check if it's Up
	${HOST_BACK0_IP}	Wait Until Keyword Succeeds	12x	3s	GUI::Network::Connections::Setup Backplane with static IPv4 and check if it's Up
	...	${HOST}	${TEST_CONNECTION_BACK0}	${TEST_CONNECTION_BACK0}	${TEST_QOS_INTERFACE_BACK0}	HOST_HOMEPAGE
	Set Suite Variable	${HOST_BACK0_IP}

	# Switch to hostshared browser page
	GUI::Basic::Switch Browser	ALIAS=SHARED_HOMEPAGE
	# Network backplane connection to default
	GUI::Network::Setup BACKPLANE Connection to default	BACKPLANE=${TEST_CONNECTION_BACK0}
	# VLAN to default
	Run Keyword If	${HOSTSHARED_IS_NSR}	GUI::Network::Setup Untagged VLAN to default with sfp0 and sfp1 on switch
	...	ELSE	GUI::Network::Setup Untagged VLAN to default without sfp0 and sfp1 on switch
	# Switch interfaces to default
	Run Keyword If	${HOSTSHARED_IS_NSR}	GUI::Network::Switch::Switch Interfaces::Setup Switch Interfaces to default with sfp0 and sfp1 on switch
	...	ELSE	GUI::Network::Switch::Switch Interfaces::Setup Switch Interfaces to default without sfp0 and sfp1 on switch
	# Edit VLAN
	Run Keyword If	${HOSTSHARED_IS_NSR}	GUI::Network::Switch::Add Backplane1 and sfp1 to first Untagged VLAN
	...	ELSE	GUI::Network::Switch::Add Backplane1 and sfp1 to first Untagged VLAN	HAS_SFP1=No
	# Enable switch interface
	GUI::Network::Switch::Switch Interfaces::Enable switch interface	${SWITCH_NETPORT_SHARED_INTERFACE2}
	# Setup Backplane with static IPv4 and check if it's Up
	${SHARED_BACK0_IP}	Wait Until Keyword Succeeds	12x	3s	GUI::Network::Connections::Setup Backplane with static IPv4 and check if it's Up
	...	${HOSTSHARED}	${TEST_CONNECTION_BACK0}	${TEST_CONNECTION_BACK0}	${TEST_QOS_INTERFACE_BACK0}	SHARED_HOMEPAGE
	Set Suite Variable	${SHARED_BACK0_IP}

Test case to Setup VLAN and add static IPv4 to backplane1
	Skip If	not ${VLAN_AVAILABLE}	This automation test environment has no VLAN available
	Skip If	not ${HOST_HAS_BACK1}	HOST has no BACKPLANE1 connection
	Skip If	not ${HOSTSHARED_HAS_BACK1}	HOSTSHARED has no BACKPLANE1 connection
	Run Keyword If	not ${HOST_IS_NSR} and not ${HOST_IS_GSR}
	...	Fail	HOST system is not NSR and not GSR, the switch test cases must be updated for the new system
	Run Keyword If	not ${HOSTSHARED_IS_NSR} and not ${HOSTSHARED_IS_GSR}
	...	Fail	HOSTSHARED system is not NSR and not GSR, the switch test cases must be updated for the new system
	GUI::Basic::Switch Browser	ALIAS=HOST_HOMEPAGE
	GUI::Network::Setup BACKPLANE Connection to default	BACKPLANE=${TEST_CONNECTION_BACK1}
	Run Keyword If	${HOST_IS_NSR}	GUI::Network::Setup Untagged VLAN to default with sfp0 and sfp1 on switch
	...	ELSE	GUI::Network::Setup Untagged VLAN to default without sfp0 and sfp1 on switch
	Run Keyword If	${HOST_IS_NSR}	GUI::Network::Switch::Add Backplane1 and sfp1 to first Untagged VLAN
	...	ELSE	GUI::Network::Switch::Add Backplane1 and sfp1 to first Untagged VLAN	HAS_SFP1=No
	GUI::Network::Switch::Switch Interfaces::Enable switch interface	${SWITCH_NETPORT_HOST_INTERFACE2}
	${HOST_BACK1_IP}	Wait Until Keyword Succeeds	12x	3s	GUI::Network::Connections::Setup Backplane with static IPv4 and check if it's Up
	...	${HOST}	${TEST_CONNECTION_BACK1}	${TEST_CONNECTION_BACK1}	${TEST_QOS_INTERFACE_BACK1}	HOST_HOMEPAGE
	Set Suite Variable	${HOST_BACK1_IP}
	GUI::Basic::Switch Browser	ALIAS=SHARED_HOMEPAGE
	GUI::Network::Setup BACKPLANE Connection to default	BACKPLANE=${TEST_CONNECTION_BACK1}
	Run Keyword If	${HOSTSHARED_IS_NSR}	GUI::Network::Setup Untagged VLAN to default with sfp0 and sfp1 on switch
	...	ELSE	GUI::Network::Setup Untagged VLAN to default without sfp0 and sfp1 on switch
	Run Keyword If	${HOSTSHARED_IS_NSR}	GUI::Network::Switch::Add Backplane1 and sfp1 to first Untagged VLAN
	...	ELSE	GUI::Network::Switch::Add Backplane1 and sfp1 to first Untagged VLAN	HAS_SFP1=No
	GUI::Network::Switch::Switch Interfaces::Enable switch interface	${SWITCH_NETPORT_SHARED_INTERFACE2}
	${SHARED_BACK1_IP}	Wait Until Keyword Succeeds	12x	3s	GUI::Network::Connections::Setup Backplane with static IPv4 and check if it's Up
	...	${HOSTSHARED}	${TEST_CONNECTION_BACK1}	${TEST_CONNECTION_BACK1}	${TEST_QOS_INTERFACE_BACK1}	SHARED_HOMEPAGE
	Set Suite Variable	${SHARED_BACK1_IP}

Test case to Setup GSM Connection
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	GUI::Basic::Switch Browser	ALIAS=HOST_HOMEPAGE
	${TEST_QOS_CONNECTION_GSM}	Set Variable	gsm-${GSM_CDC_WDMX}
	Set Suite Variable	${TEST_QOS_CONNECTION_GSM}
	SUITE:Delete GSM Connection If exists
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Add
	GUI::Basic::Click Element	//select[@id='connType']
	Select From List By Label	//select[@id='connType']	${GSM_TYPE_GUI}
	GUI::Basic::Click Element	//select[@id='connItfName']
	Select From List By Label	//select[@id='connItfName']	${GSM_CDC_WDMX}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//input[@id='connHealth']
	GUI::Basic::Click Element	//input[@id='connHealthEnsureUp']
	GUI::Basic::Click Element	//input[@id='mobileAPN']
	GUI::Basic::Input Text	//input[@id='mobileAPN']	${GSM_WMODEM_CARRIERS_APN["${GSM_OPERATOR_NAME}"]}
	Log	\nGSM_WMODEM_CARRIERS_APN.keys(${GSM_OPERATOR_NAME}): \n${GSM_WMODEM_CARRIERS_APN["${GSM_OPERATOR_NAME}"]}	INFO	console=yes
	GUI::Basic::Click Element	//input[@id='dataUsageMonitor1']
	GUI::Basic::Click Element	//input[@id='dataUsageLimit1']
	GUI::Basic::Input Text	//input[@id='dataUsageLimit1']	1
	GUI::Basic::Click Element	//input[@id='dataUsageWarning1']
	GUI::Basic::Input Text	//input[@id='dataUsageWarning1']	80
	GUI::Basic::Click Element	//input[@id='gnssStatus']
	GUI::Basic::Click Element	//input[@id='gnssPolltime']
	GUI::Basic::Input Text	//input[@id='gnssPolltime']	1
	GUI::Basic::Save
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	${TEST_QOS_CONNECTION_GSM}+\\s+Connected+\\s+Mobile Broadband GSM+\\s+${GSM_CDC_WDMX}+\\s+Up+\\s+\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}(?=\\/\\d{2})
	[Teardown]	SUITE:Test Teardown

Test case to generate TCP traffic between HOST and HOSTPEER VM through the ETH0 interface and check it in the FireQoS table
	CLI:Open	HOST_DIFF=${HOSTPEER}	session_alias=peer_session
	CLI:Start iPerf3 Server	PORT=5201
	CLI:Open	PASSWORD=${STRONG_PASSWORD}
	${OUTPUT1}	CLI:Test iPerf3 Client	SERVER_IP_ADDRESS=${HOSTPEER}	PORT=5201	SOURCE_INTERFACE_IP=${HOST}
	${OUTPUT2}	CLI:Test iPerf3 Client	SERVER_IP_ADDRESS=${HOSTPEER}	PORT=5201	SOURCE_INTERFACE_IP=${HOST}
	CLI:Switch Connection	peer_session
	CLI:Stop iPerf3 Server
	CLI:Close Connection
	GUI::Basic::Switch Browser	ALIAS=HOST_HOMEPAGE
	${FIREQOS_TABLE_CONTENT}	GUI::Tracking::Get Tracking QoS Table Text
	Should Not Contain	${OUTPUT1}	iperf3: error - unable to connect to server
	Should Not Contain	${OUTPUT2}	iperf3: error - unable to connect to server
	Should Match Regexp	${FIREQOS_TABLE_CONTENT}	${TEST_QOS_INTERFACE_ETH0}+\\s+input+\\s+${TEST_QOS_CLASS_NAME}+\\s+\\d{1,3}\\.\\d{2} KB+\\s+\\d+\\s+\\d+\\s+\\d	#(KB|B)
	Should Match Regexp	${FIREQOS_TABLE_CONTENT}	${TEST_QOS_INTERFACE_ETH0}+\\s+output+\\s+${TEST_QOS_CLASS_NAME}+\\s+\\d{1,3}\\.\\d{2} MB+\\s+\\d+\\s+\\d+\\s+\\d	#(MB|KB|B)

Test case to generate TCP traffic between HOST and HOSTPEER VM through the ETH1 interface and check it in the FireQoS table
	Skip If	not ${HOST_HAS_ETH1} or not ${PEER_HAS_ETH1}	This automation test environment has no ETH1 connection
	${ETH1_HAS_IP}	Run Keyword And Return Status	GUI::Network::Get Connection Ipv4	${TEST_CONNECTION_ETH1}
	Skip If	not ${ETH1_HAS_IP}	This automation test environment has ETH1 connection but it's Down without IP Address
	${ETH1_IP}	GUI::Network::Get Connection Ipv4	${TEST_CONNECTION_ETH1}
	CLI:Open	HOST_DIFF=${HOSTPEER}	session_alias=peer_session
	CLI:Start iPerf3 Server	PORT=5201
	CLI:Open	PASSWORD=${STRONG_PASSWORD}
	${OUTPUT1}	CLI:Test iPerf3 Client	SERVER_IP_ADDRESS=${HOSTPEER}	PORT=5201	SOURCE_INTERFACE_IP=${ETH1_IP}
	${OUTPUT2}	CLI:Test iPerf3 Client	SERVER_IP_ADDRESS=${HOSTPEER}	PORT=5201	SOURCE_INTERFACE_IP=${ETH1_IP}
	CLI:Switch Connection	peer_session
	CLI:Stop iPerf3 Server
	CLI:Close Connection
	GUI::Basic::Switch Browser	ALIAS=HOST_HOMEPAGE
	${FIREQOS_TABLE_CONTENT}	GUI::Tracking::Get Tracking QoS Table Text
	Should Not Contain	${OUTPUT1}	iperf3: error - unable to connect to server	#If it happens need to check if still there are physical connections
	Should Not Contain	${OUTPUT2}	iperf3: error - unable to connect to server	#If it happens need to check if still there are physical connections
	Should Match Regexp	${FIREQOS_TABLE_CONTENT}	${TEST_QOS_INTERFACE_ETH1}+\\s+input+\\s+${TEST_QOS_CLASS_NAME}+\\s+\\d{1,3}\\.\\d{2} KB+\\s+\\d+\\s+\\d+\\s+\\d	#(KB|B)
	Should Match Regexp	${FIREQOS_TABLE_CONTENT}	${TEST_QOS_INTERFACE_ETH1}+\\s+output+\\s+${TEST_QOS_CLASS_NAME}+\\s+\\d{1,3}\\.\\d{2} MB+\\s+\\d+\\s+\\d+\\s+\\d	#(MB|KB|B)

Test case to generate TCP traffic between HOST and HOSTSHARED through the BACKPLANE0 interface and check it in the FireQoS table
	Skip If	not ${VLAN_AVAILABLE}	This automation test environment has no VLAN available
	Skip If	not ${HOST_HAS_BACK0}	HOST has no BACKPLANE0 connection
	Skip If	not ${HOSTSHARED_HAS_BACK0}	HOSTSHARED has no BACKPLANE0 connection
	CLI:Open	HOST_DIFF=${HOSTSHARED}	session_alias=shared_session
	CLI:Start iPerf3 Server	PORT=5201
	CLI:Open	PASSWORD=${STRONG_PASSWORD}
	${OUTPUT1}	CLI:Test iPerf3 Client	SERVER_IP_ADDRESS=${SHARED_BACK0_IP}	PORT=5201	SOURCE_INTERFACE_IP=${HOST_BACK0_IP}
	${OUTPUT2}	CLI:Test iPerf3 Client	SERVER_IP_ADDRESS=${SHARED_BACK0_IP}	PORT=5201	SOURCE_INTERFACE_IP=${HOST_BACK0_IP}
	CLI:Switch Connection	shared_session
	CLI:Stop iPerf3 Server
	CLI:Close Connection
	GUI::Basic::Switch Browser	ALIAS=HOST_HOMEPAGE
	Should Not Contain	${OUTPUT1}	iperf3: error - unable to connect to server	#If it happens need to check if still there are physical connections
	Should Not Contain	${OUTPUT2}	iperf3: error - unable to connect to server	#If it happens need to check if still there are physical connections
	${FIREQOS_TABLE_CONTENT}	GUI::Tracking::Get Tracking QoS Table Text
	Should Match Regexp	${FIREQOS_TABLE_CONTENT}	${TEST_QOS_INTERFACE_BACK0}+\\s+input+\\s+${TEST_QOS_CLASS_NAME}+\\s+\\d{1,3}\\.\\d{2} KB+\\s+\\d+\\s+\\d+\\s+\\d	#(KB|B)
	Should Match Regexp	${FIREQOS_TABLE_CONTENT}	${TEST_QOS_INTERFACE_BACK0}+\\s+output+\\s+${TEST_QOS_CLASS_NAME}+\\s+\\d{1,3}\\.\\d{2} MB+\\s+\\d+\\s+\\d+\\s+\\d	#(MB|KB|B)

Test case to generate TCP traffic between HOST and HOSTSHARED through the BACKPLANE1 interface and check it in the FireQoS table
	[Tags]	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	EXCLUDEIN4_2	EXCLUDEIN5_0	BUG_NG_9911	NON-CRITICAL	NEED-REVIEW
	Skip If	not ${VLAN_AVAILABLE}	This automation test environment has no VLAN available
	Skip If	not ${HOST_HAS_BACK1}	HOST has no BACKPLANE1 connection
	Skip If	not ${HOSTSHARED_HAS_BACK1}	HOSTSHARED has no BACKPLANE1 connection
	CLI:Open	HOST_DIFF=${HOSTSHARED}	session_alias=shared_session
	CLI:Start iPerf3 Server	PORT=5201
	CLI:Open	PASSWORD=${STRONG_PASSWORD}
	${OUTPUT1}	CLI:Test iPerf3 Client	SERVER_IP_ADDRESS=${SHARED_BACK1_IP}	PORT=5201	SOURCE_INTERFACE_IP=${HOST_BACK1_IP}
	${OUTPUT2}	CLI:Test iPerf3 Client	SERVER_IP_ADDRESS=${SHARED_BACK1_IP}	PORT=5201	SOURCE_INTERFACE_IP=${HOST_BACK1_IP}
	CLI:Switch Connection	shared_session
	CLI:Stop iPerf3 Server
	CLI:Close Connection
	GUI::Basic::Switch Browser	ALIAS=HOST_HOMEPAGE
	${FIREQOS_TABLE_CONTENT}	GUI::Tracking::Get Tracking QoS Table Text
	Should Not Contain	${OUTPUT1}	iperf3: error - unable to connect to server	#If it happens need to check if still there are physical connections
	Should Not Contain	${OUTPUT2}	iperf3: error - unable to connect to server	#If it happens need to check if still there are physical connections
	Should Match Regexp	${FIREQOS_TABLE_CONTENT}	${TEST_QOS_INTERFACE_BACK1}+\\s+input+\\s+${TEST_QOS_CLASS_NAME}+\\s+\\d{1,3}\\.\\d{2} KB+\\s+\\d+\\s+\\d+\\s+\\d	#(KB|B)
	Should Match Regexp	${FIREQOS_TABLE_CONTENT}	${TEST_QOS_INTERFACE_BACK1}+\\s+output+\\s+${TEST_QOS_CLASS_NAME}+\\s+\\d{1,3}\\.\\d{2} MB+\\s+\\d+\\s+\\d+\\s+\\d	#(MB|KB|B)

Test case to generate TCP traffic between HOST and remote server of iPerf3 through the GSM interface (wwanX) and check it in the FireQoS table
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	Wait Until Keyword Succeeds	3x	10s	SUITE:Generate TCP traffic between HOST and remote server of iPerf3 through the GSM interface
	[Teardown]	SUITE:Test Teardown

Test case to Delete GSM Connection
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Click Element	//tr[@id='${TEST_QOS_CONNECTION_GSM}']//input[@type='checkbox']
	GUI::Basic::Delete
	Page Should Not Contain	${TEST_QOS_CONNECTION_GSM}

Test case to Setup Untagged VLAN to default
	[Tags]	NON-CRITICAL
#	Checked manuall working fine, the issue was only with switching to switch tab
	Skip If	not ${VLAN_AVAILABLE}	This automation test environment has no VLAN available
	Run Keyword If	not ${HOST_IS_NSR} and not ${HOST_IS_GSR}
	...	Fail	HOST system is not NSR and not GSR, the switch test cases must be updated for the new system
	Run Keyword If	not ${HOSTSHARED_IS_NSR} and not ${HOSTSHARED_IS_GSR}
	...	Fail	HOSTSHARED system is not NSR and not GSR, the switch test cases must be updated for the new system

	GUI::Basic::Switch Browser	ALIAS=HOST_HOMEPAGE
	Run Keyword If	${HOST_IS_NSR}	GUI::Network::Setup Untagged VLAN to default with sfp0 and sfp1 on switch
	...	ELSE	GUI::Network::Setup Untagged VLAN to default without sfp0 and sfp1 on switch
	Run Keyword If	${HOST_IS_NSR}	GUI::Network::Switch::Switch Interfaces::Setup Switch Interfaces to default with sfp0 and sfp1 on switch
	...	ELSE	GUI::Network::Switch::Switch Interfaces::Setup Switch Interfaces to default without sfp0 and sfp1 on switch
	GUI::Network::Setup BACKPLANE Connection to default	BACKPLANE=${TEST_CONNECTION_BACK0}
	GUI::Network::Setup BACKPLANE Connection to default	BACKPLANE=${TEST_CONNECTION_BACK1}
	GUI::Basic::Switch Browser	ALIAS=SHARED_HOMEPAGE
	Run Keyword If	${HOSTSHARED_IS_NSR}	GUI::Network::Setup Untagged VLAN to default with sfp0 and sfp1 on switch
	...	ELSE	GUI::Network::Setup Untagged VLAN to default without sfp0 and sfp1 on switch
	Run Keyword If	${HOSTSHARED_IS_NSR}	GUI::Network::Switch::Switch Interfaces::Setup Switch Interfaces to default with sfp0 and sfp1 on switch
	...	ELSE	GUI::Network::Switch::Switch Interfaces::Setup Switch Interfaces to default without sfp0 and sfp1 on switch
	GUI::Network::Setup BACKPLANE Connection to default	BACKPLANE=${TEST_CONNECTION_BACK0}
	GUI::Network::Setup BACKPLANE Connection to default	BACKPLANE=${TEST_CONNECTION_BACK1}
	[Teardown]	SUITE:Test Teardown

Test Case to Delete QoS Rules, Classes and Interefaces If Exists
	GUI::Basic::Switch Browser	ALIAS=HOST_HOMEPAGE
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Delete All QoS Rules If exists
	SUITE:Delete All QoS Classes If exists
	SUITE:Delete All QoS Interfaces If exists

*** Keywords ***
SUITE:Setup
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid	PAGE=${HOMEPAGEPEER}	ALIAS=PEER_HOMEPAGE
	GUI::Basic::Open And Login Nodegrid	PAGE=${HOMEPAGESHARED}	ALIAS=SHARED_HOMEPAGE
	GUI::Basic::Open And Login Nodegrid	PAGE=${HOMEPAGE}	ALIAS=HOST_HOMEPAGE
	Set Suite Variable	${HAS_ETH1}	${FALSE}
	Set Suite Variable	${HOST_HAS_BACK0}	${FALSE}
	Set Suite Variable	${HOSTSHARED_HAS_BACK0}	${FALSE}
	Set Suite Variable	${HOST_HAS_BACK1}	${FALSE}
	Set Suite Variable	${HOSTSHARED_HAS_BACK1}	${FALSE}
	Set Suite Variable	${HAS_BACK1}	${FALSE}
	Set Suite Variable	${HAS_GSM}	${FALSE}
	Set Suite Variable	${TEST_QOS_CONNECTION_GSM}	${FALSE}
#	CLI: Setup
	CLI:Open
	SUITE:Change passwords of admin and root to strong passwords
	CLI:Close Connection

SUITE:Teardown
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid	PAGE=${HOMEPAGESHARED}	ALIAS=SHARED_HOMEPAGE
	GUI::Basic::Open And Login Nodegrid	PAGE=${HOMEPAGE}	ALIAS=HOST_HOMEPAGE	PASSWORD=${STRONG_PASSWORD}
	SUITE:Delete All QoS Rules If exists
	SUITE:Delete All QoS Classes If exists
	SUITE:Delete All QoS Interfaces If exists
	IF	${HOST_HAS_BACK0} and ${HOST_HAS_BACK1} and ${VLAN_AVAILABLE}
		GUI::Basic::Switch Browser	ALIAS=HOST_HOMEPAGE
		GUI::Network::Setup BACKPLANE Connection to default	BACKPLANE=${TEST_CONNECTION_BACK0}
		GUI::Network::Setup BACKPLANE Connection to default	BACKPLANE=${TEST_CONNECTION_BACK1}
		Run Keyword If	${HOST_IS_NSR}	GUI::Network::Setup Untagged VLAN to default with sfp0 and sfp1 on switch
	...	ELSE	GUI::Network::Setup Untagged VLAN to default without sfp0 and sfp1 on switch
		Run Keyword If	${HOST_IS_NSR}	GUI::Network::Switch::Switch Interfaces::Setup Switch Interfaces to default with sfp0 and sfp1 on switch
	...	ELSE	GUI::Network::Switch::Switch Interfaces::Setup Switch Interfaces to default without sfp0 and sfp1 on switch
	END
	IF	${HOSTSHARED_HAS_BACK0} and ${HOSTSHARED_HAS_BACK1} and ${VLAN_AVAILABLE}
		GUI::Basic::Switch Browser	ALIAS=SHARED_HOMEPAGE
		GUI::Network::Setup BACKPLANE Connection to default	BACKPLANE=${TEST_CONNECTION_BACK0}
		GUI::Network::Setup BACKPLANE Connection to default	BACKPLANE=${TEST_CONNECTION_BACK1}
		Run Keyword If	${HOSTSHARED_IS_NSR}	GUI::Network::Setup Untagged VLAN to default with sfp0 and sfp1 on switch
	...	ELSE	GUI::Network::Setup Untagged VLAN to default without sfp0 and sfp1 on switch
		Run Keyword If	${HOSTSHARED_IS_NSR}	GUI::Network::Switch::Switch Interfaces::Setup Switch Interfaces to default with sfp0 and sfp1 on switch
	...	ELSE	GUI::Network::Switch::Switch Interfaces::Setup Switch Interfaces to default without sfp0 and sfp1 on switch
	END
	IF	${HAS_GSM}
		GUI::Basic::Switch Browser	ALIAS=HOST_HOMEPAGE
		SUITE:Delete GSM Connection If exists
	END
	GUI::Basic::Logout And Close Nodegrid
#	CLI: Teardown
	CLI:Open	PASSWORD=${STRONG_PASSWORD}
	SUITE:Change passwords of admin and root to default passwords
	CLI:Close Connection

SUITE:Test Teardown
	GUI::Basic::Spinner Should Be Invisible
	${HAS_CANCEL_BUTTON}	Run Keyword And Return Status	Page Should Contain Button	//input[@id='cancelButton']
	Run Keyword If	${HAS_CANCEL_BUTTON}	Run Keyword And Ignore Error	GUI::Basic::Button::Cancel
	GUI::Basic::Spinner Should Be Invisible
	${HAS_RETURN_BUTTON}	Run Keyword And Return Status	Page Should Contain Button	//input[@id='returnButton']
	Run Keyword If	${HAS_RETURN_BUTTON}	Run Keyword And Ignore Error	GUI::Basic::Click Element	//input[@id='returnButton']

SUITE:Add QoS Interface
	[Arguments]	${TEST_QOS_INTERFACE_NAME}
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//input[@id='qos_form_name']
	GUI::Basic::Input Text	//input[@id='qos_form_name']	${TEST_QOS_INTERFACE_NAME}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//select[@class='selectbox groupFrom']
	GUI::Basic::Click Element	//option[@value='${TEST_QOS_CLASS_NAME}']
	GUI::Basic::Click Element	//button[contains(text(),'Add ►')]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//input[@id='qos_form_input_rate_value']
	GUI::Basic::Input Text	//input[@id='qos_form_input_rate_value']	${TEST_QOS_INTERFACE_IN/OUTPUT_B}
	GUI::Basic::Click Element	//select[@id='qos_form_input_rate_unit']
	Select From List By Label	//select[@id='qos_form_input_rate_unit']	${TEST_QOS_INTERFACE_IN/OUTPUT_UNIT}
	GUI::Basic::Click Element	//input[@id='qos_form_output_rate_value']
	GUI::Basic::Input Text	//input[@id='qos_form_output_rate_value']	${TEST_QOS_INTERFACE_IN/OUTPUT_B}
	GUI::Basic::Click Element	//select[@id='qos_form_output_rate_unit']
	Select From List By Label	//select[@id='qos_form_output_rate_unit']	${TEST_QOS_INTERFACE_IN/OUTPUT_UNIT}

SUITE:Delete All QoS Rules If exists
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::QoS::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//span[normalize-space()='Rules']
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete All Rows In Table If Exists	\#qos_rules_table	False

SUITE:Delete All QoS Classes If exists
	GUI::Basic::Network::QoS::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//span[normalize-space()='Classes']
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete All Rows In Table If Exists	\#qos_class_table	False

SUITE:Delete All QoS Interfaces If exists
	GUI::Basic::Network::QoS::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//span[normalize-space()='Interfaces']
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete All Rows In Table If Exists	\#interfaceTable	False

SUITE:Delete GSM Connection If exists
	${HAS_GSM_CONNECTION}	GUI::Network::Check If Has Connection	${TEST_QOS_CONNECTION_GSM}
	IF	${HAS_GSM_CONNECTION}
		GUI::Basic::Network::Connections::open tab
		GUI::Basic::Click Element	//tr[@id='${TEST_QOS_CONNECTION_GSM}']//input[@type='checkbox']
		GUI::Basic::Delete
		Page Should Not Contain	${TEST_QOS_CONNECTION_GSM}
	END

SUITE:Change passwords of admin and root to strong passwords
	CLI:Change User Password From Shell	root	${STRONG_PASSWORD}	${ROOT_PASSWORD}
	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${STRONG_PASSWORD}	${STRONG_PASSWORD}
	Log	Changed the passwords of admin and root to: ${STRONG_PASSWORD}	INFO	console=yes

SUITE:Change passwords of admin and root to default passwords
	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}	${STRONG_PASSWORD}
	CLI:Change User Password From Shell	root	${ROOT_PASSWORD}	${STRONG_PASSWORD}
	Log	The passwords of admin and root users came back to default: ${DEFAULT_PASSWORD} and ${ROOT_PASSWORD}	INFO	console=yes

SUITE:Generate TCP traffic between HOST and remote server of iPerf3 through the GSM interface
	GUI::Basic::Switch Browser	ALIAS=HOST_HOMEPAGE
	${GSM_IP}	GUI::Network::Get Connection Ipv4	${TEST_QOS_CONNECTION_GSM}
	CLI:Open	PASSWORD=${STRONG_PASSWORD}
	${OUTPUT1}	CLI:Test iPerf3 Client	SERVER_IP_ADDRESS=${IPERF3_SERVER_IP}	PORT=${IPERF3_SERVER_PORT}	SOURCE_INTERFACE_IP=${GSM_IP}
	${OUTPUT2}	CLI:Test iPerf3 Client	SERVER_IP_ADDRESS=${IPERF3_SERVER_IP}	PORT=${IPERF3_SERVER_PORT}	SOURCE_INTERFACE_IP=${GSM_IP}
	GUI::Basic::Switch Browser	ALIAS=HOST_HOMEPAGE
	${FIREQOS_TABLE_CONTENT}	GUI::Tracking::Get Tracking QoS Table Text
	Should Not Contain	${OUTPUT1}	iperf3: error - unable to connect to server	#If it happens need to check if still there are GSM sim card with signal and iperf3 server is Up
	Should Not Contain	${OUTPUT2}	iperf3: error - unable to connect to server	#If it happens need to check if still there are GSM sim card with signal and iperf3 server is Up
	Should Match Regexp	${FIREQOS_TABLE_CONTENT}	${TEST_QOS_INTERFACE_GSM}+\\s+input+\\s+${TEST_QOS_CLASS_NAME}+\\s+\\d{1,3}\\.\\d{2} KB+\\s+\\d+\\s+\\d+\\s+\\d	#(KB|B)
	Should Match Regexp	${FIREQOS_TABLE_CONTENT}	${TEST_QOS_INTERFACE_GSM}+\\s+output+\\s+${TEST_QOS_CLASS_NAME}+\\s+\\d{1,3}\\.\\d{2} MB+\\s+\\d+\\s+\\d+\\s+\\d	#(MB|KB|B)