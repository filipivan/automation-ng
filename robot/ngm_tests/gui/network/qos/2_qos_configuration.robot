*** Settings ***
Documentation	Testing the QOS configuration
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${interface_eth0}	eth0
${interface_class1}	class1
${interface_rule1}	rule1
${dir_input}	input
${dir_output}	output
${dir_bi}	bidirectional
${bandw_10}	10
${bandw_50}	50
${bandw_100}	100
${ip1}	132.1.1.1
${ip2}	143.1.1.1
${port1}	22
${port2}	28
${smac}	00:0a:95:9d:68:16
${dmac}	10:0a:35:5d:78:17

*** Test Cases ***
Add new interface in QOS
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=qos_form_name	${interface_eth0}
	Input Text	id=qos_form_input_rate_value	${bandw_10}
	Input Text	id=qos_form_output_rate_value	${bandw_10}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="${interface_eth0}"]

Select all 3 directions of QoS directions
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="interfaceTable"]/tbody/tr[@id="${interface_eth0}"]/td[1]/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	jquery=#qos_form_direction	${dir_input}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="interfaceTable"]/tbody/tr[@id="${interface_eth0}"]/td[1]/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	jquery=#qos_form_direction	${dir_output}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="interfaceTable"]/tbody/tr[@id="${interface_eth0}"]/td[1]/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	jquery=#qos_form_direction	${dir_bi}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Enable and disable of the customer parameters and check for the custom parameters input values.
	Select Checkbox	//table[@id="interfaceTable"]/tbody/tr[@id="${interface_eth0}"]/td[1]/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=qos_form_enabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="interfaceTable"]/tbody/tr[@id="${interface_eth0}"]/td[1]/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=qos_form_enabled
	Click Element	id=qos_form_extra
	Input Text	id=qos_form_extra	test
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Check Bandwidth and each bandwidth parameters for input and output.
	Select Checkbox	//table[@id="interfaceTable"]/tbody/tr[@id="${interface_eth0}"]/td[1]/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=qos_form_input_rate_value	${bandw_50}
	Select From List By Value	jquery=#qos_form_input_rate_unit	Gbps
	Input Text	id=qos_form_output_rate_value	${bandw_50}
	Select From List By Value	jquery=#qos_form_output_rate_unit	Gbps
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="interfaceTable"]/tbody/tr[@id="${interface_eth0}"]/td[1]/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=qos_form_input_rate_value	${bandw_10}
	Select From List By Value	jquery=#qos_form_input_rate_unit	Mbps
	Input Text	id=qos_form_output_rate_value	${bandw_10}
	Select From List By Value	jquery=#qos_form_output_rate_unit	Mbps
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="interfaceTable"]/tbody/tr[@id="${interface_eth0}"]/td[1]/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=qos_form_input_rate_value	${bandw_10}
	Select From List By Value	jquery=#qos_form_input_rate_unit	Kbps
	Input Text	id=qos_form_output_rate_value	${bandw_10}
	Select From List By Value	jquery=#qos_form_output_rate_unit	Kbps
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="interfaceTable"]/tbody/tr[@id="${interface_eth0}"]/td[1]/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=qos_form_input_rate_value	${bandw_10}
	Select From List By Value	jquery=#qos_form_input_rate_unit	Bps
	Input Text	id=qos_form_output_rate_value	${bandw_10}
	Select From List By Value	jquery=#qos_form_output_rate_unit	Bps
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="interfaceTable"]/tbody/tr[@id="${interface_eth0}"]/td[1]/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=qos_form_input_rate_value	${bandw_10}
	Select From List By Value	jquery=#qos_form_input_rate_unit	Gbit
	Input Text	id=qos_form_output_rate_value	${bandw_10}
	Select From List By Value	jquery=#qos_form_output_rate_unit	Gbit
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="interfaceTable"]/tbody/tr[@id="${interface_eth0}"]/td[1]/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=qos_form_input_rate_value	${bandw_10}
	Select From List By Value	jquery=#qos_form_input_rate_unit	Mbit
	Input Text	id=qos_form_output_rate_value	${bandw_10}
	Select From List By Value	jquery=#qos_form_output_rate_unit	Mbit
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Create new class in Network :: QoS :: Classes and add these class in Interfaces.
	Click Element	css=#qosClassNav > .title_b
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=qos_form_class_name
	Input Text	id=qos_form_class_name	${interface_class1}
	Click Element	//div[@id="qos_form_interface_table"]//option[@value="${interface_eth0}"]
	Click Element	//*[@id="qos_form_interface_table"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Check Reserved and Max Bandwidth with each bandwidth parameters for input and output
	Select Checkbox	//table[@id="qos_class_table"]/tbody/tr[@id="${interface_class1}"]/td[1]/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=qos_form_input_min_rate_value	${bandw_10}
	Select From List By Value	jquery=#qos_form_input_min_rate_unit	Gbit
	Input Text	id=qos_form_input_max_rate_value	${bandw_100}
	Select From List By Value	jquery=#qos_form_input_max_rate_unit	Gbit
	Input Text	id=qos_form_output_min_rate_value	${bandw_10}
	Select From List By Value	jquery=#qos_form_output_min_rate_unit	Gbit
	Input Text	id=qos_form_output_max_rate_value	${bandw_100}
	Select From List By Value	jquery=#qos_form_output_max_rate_unit	Gbit
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Add new rule with name in Network :: QoS :: Rules
	Click Element	css=#qosRuleNav > .title_b
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=qos_form_rule_name	${interface_rule1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Enable disable ipv4/ipv6 and TCP flags SYN and ACK.
	Select Checkbox	//table[@id="qos_rules_table"]/tbody/tr[@id="${interface_rule1}"]/td[1]/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=qos_form_rule_ipv4
	Click Element	id=qos_form_rule_ipv6
	Click Element	id=qos_form_rule_syn
	Click Element	id=qos_form_rule_ack
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="qos_rules_table"]/tbody/tr[@id="${interface_rule1}"]/td[1]/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=qos_form_rule_ipv4
	Click Element	id=qos_form_rule_ipv6
	Click Element	id=qos_form_rule_syn
	Click Element	id=qos_form_rule_ack
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Add remove protocols, src/dst ip,port and mac address and also custom parameters.
	GUI::Basic::Network::QoS::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#qosRuleNav > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="qos_rules_table"]/tbody/tr[@id="${interface_rule1}"]/td[1]/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=qos_form_rule_src	${ip1}
	Input Text	id=qos_form_rule_dst	${ip2}
	Input Text	id=qos_form_rule_sports	${port1}
	Input Text	id=qos_form_rule_dports	${port2}
	Input Text	id=qos_form_rule_smac	${smac}
	Input Text	id=qos_form_rule_dmac	${dmac}
	Input Text	id=qos_form_rule_extra	${interface_rule1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Assign interface to classes and rules in classes in each section and check..
	Select Checkbox	//table[@id="qos_rules_table"]/tbody/tr[@id="${interface_rule1}"]/td[1]/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@id="qos_form_class_table"]//option[@value="${interface_class1}"]
	Click Element	//*[@id="qos_form_class_table"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Delete and add new interface/Classes/Rules.
	Select Checkbox	//table[@id="qos_rules_table"]/tbody/tr[@id="${interface_rule1}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#qosClassNav > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="qos_class_table"]/tbody/tr[@id="${interface_class1}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::QoS::open tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="interfaceTable"]/tbody/tr[@id="${interface_eth0}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::QoS::open tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Close All Browsers