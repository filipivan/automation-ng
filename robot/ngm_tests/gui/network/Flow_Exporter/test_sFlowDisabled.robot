*** Settings ***
Documentation	Testing Network -> FlowExporter -> sFlow
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	FIREFOX	CHROME
Default Tags	GUI

Suite Setup	SUITE:Setup

*** Variables ***

${PROTOCOL}		sFlow

*** Test Cases ***
Test Enter In Root Mode In Console With Sudo Permissions
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Network FlowExporter

*** Keywords ***
SUITE:Setup
	# Open and login into Nodegrid application
	GUI::Basic::Open And Login Nodegrid

SUITE:Network FlowExporter
	# Navigate to network, click on Flow exporter and fill the form for all the required fields
	SUITE:Navigate to Network and FlowExporter
	# Disabling the sflow
	SUITE:Disable sFlow
	# Deleting the sflow
	SUITE:Teardown


SUITE:Navigate to Network and FlowExporter
	# This page is available for v5.8+
	# Navigating to NetFlow -> Flow Exporter
	GUI::Basic::Network::Flow Exporter::open tab
	# Clicking on Add button
	GUI::Basic::Add
	# Fill the form for sflow
	Click Element	id=flow_form_name
	Input Text	id=flow_form_name	Testsflow
	Click Element	id=flow_form_collector_ip
	Input Text	id=flow_form_collector_ip	${NETFLOW_COLLECTOR_SERVER}
	Click Element	id=flow_form_collector_port
	Input Text	id=flow_form_collector_port		${SFLOW_PORT}
	Select From List By Label	id=flow_form_protocol	${PROTOCOL}
	# Clicking on save button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Disable sFlow
	# Select the checkbox for added sflow
	Click Element	//td[@class='selectable']//input[@type='checkbox']
	Sleep	5s
	# Clicking on disable button
	Click Button	//input[@id='disable']
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	# Verify that page should contain diabled element
	Page Should Contain Element		//td[normalize-space()='Disabled']
	# Get the text from the row to confirm that it got disabled
	Get Text	//td[normalize-space()='Disabled']
	Close Browser

SUITE:Delete sFlow
	# Click on Network tab
	GUI::Basic::Network::Flow Exporter::open tab
	Sleep	5s
	# Select the checkbox for added sflow
	Click Element	//td[@class='selectable']//input[@type='checkbox']
	Sleep	5s
	# Click on delete button
	GUI::Basic::Delete
	# Click Button	id=delButton

SUITE:Teardown
	# Open nidegrid and login into the app.
	GUI::Basic::Open And Login Nodegrid
	# Delet the sflow created as part of teardown
	SUITE:Delete sFlow
	# Logout of the application and close nodegrid
	GUI::Basic::Logout And Close Nodegrid