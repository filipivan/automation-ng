*** Settings ***
Documentation	Testing Network -> FlowExporter -> sFlow
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	FIREFOX	CHROME
Default Tags	GUI

Suite Setup	SUITE:Setup

*** Variable ***

${PROTOCOL}		sFlow

*** Test Cases ***
Test Enter In Root Mode In Console With Sudo Permissions
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Network FlowExporter


*** Keywords ***
SUITE:Setup
	# Open Nodegrid application and login into the app.
	GUI::Basic::Open And Login Nodegrid

SUITE:Network FlowExporter
	# Verifying the sampling rate validations for sflow network
    SUITE:Navigate to Network FlowExporter for Sampling Rate Validations

SUITE:Navigate to Network FlowExporter for Sampling Rate Validations
	# The sflow works for versions v5.8 and greater
	# Navigating to NEtwork -> Flow Exporter
	GUI::Basic::Network::Flow Exporter::open tab
	# Clicking on Add button
	GUI::Basic::Add
	# Filling out the forms with text in sampling rate
	Click Element	id=flow_form_name
	Input Text	id=flow_form_name	Testsflow
	Click Element	id=flow_form_collector_ip
	Input Text	id=flow_form_collector_ip	${NETFLOW_COLLECTOR_SERVER}
	Click Element	id=flow_form_collector_port
	Input Text	id=flow_form_collector_port		${SFLOW_PORT}
	Select From List By Label	id=flow_form_protocol	${PROTOCOL}
	Click Element	id=flow_form_sampling_rate
	Input Text	id=flow_form_sampling_rate	sdasder
	# Click on save button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	# Verify that page contains error message
	Page Should Contain Element		id=errormsg
	Click Element	id=flow_form_sampling_rate
	# Input the sampling rate field with numbers and spaces in between
	Input Text	id=flow_form_sampling_rate	12 54 45
	# Click on save button
	GUI::Basic::Save
	# Verify that there is an error message
	Page Should Contain Element		id=errormsg
	Click Element	id=flow_form_sampling_rate
	# Inout the sampling rate with combination of numbers and text and space in between
	Input Text	id=flow_form_sampling_rate	2686 sdfds
	# Click on save button
	GUI::Basic::Save
	# Verify that there is an error message
	Page Should Contain Element		id=errormsg
	Click Element	id=flow_form_sampling_rate
	# Input the text with numbers and special characters
	Input Text	id=flow_form_sampling_rate	47432%
	# Click on save button
	GUI::Basic::Save
	# Verify that there is an error message
	Page Should Contain Element		id=errormsg
	# Click on cancel button
	GUI::Basic::Cancel
	# Logout of the nodegrid app and close the browser
	GUI::Basic::Logout And Close Nodegrid





