*** Settings ***
Documentation	Automated test for Netflow streaming telemetry support
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${Device_name}	nodegrid
${root_login}	shell sudo su -
${Collector_ip1}	${NETFLOW_COLLECTOR_SERVER}
${Collector_ip2}	11.11.11.11
${Netflow_collector_server_login}	ssh qausers@192.168.2.223
${collector_pass}	${NETFLOW_PASSWD}
${Interface}	eth0
${start_nfdump_service}	sudo systemctl start nfdump
${stop_nfdump_service}	sudo systemctl stop nfdump
${remove_content}	sudo rm /var/cache/nfdump/*
${confirm_nfcapd_running}	ps -ef | grep nfcapd
${nfcapd_listening_with_netstat}	netstat -n --udp --listen
${view_flow_records}	nfdump -R /var/cache/nfdump	-A srcip

*** Test Cases ***
Test case to add a connection
	GUI::Basic::Open And Login Nodegrid
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=(//a[contains(text(),'Flow Exporter')])[2]
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=flow_form_name
	Input Text	id=flow_form_name	Testflow
	Click Element	id=flow_form_interface
	Select From List By Label	id=flow_form_interface	${Interface}
	Click Element	id=flow_form_collector_ip
	Input Text	id=flow_form_collector_ip	${Collector_ip1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=(//a[contains(text(),'Flow Exporter')])[2]
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=flow_form_name
	Input Text	id=flow_form_name	Testflownew
	Click Element	id=flow_form_interface
	Select From List By Label	id=flow_form_interface	${Interface}
	Click Element	id=flow_form_collector_ip
	Input Text	id=flow_form_collector_ip	${Collector_ip2}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Teardown

Test case to validate the fields
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open And Login Nodegrid
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=(//a[contains(text(),'Flow Exporter')])[2]
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=flow_form_name
	Input Text	id=flow_form_name	Testflow2
	Click Element	id=flow_form_collector_ip
	Input Text	id=flow_form_collector_ip	${Collector_ip1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=(//a[contains(text(),'Flow Exporter')])[2]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Testflow2
	Select Checkbox	css=#flow-Testflow2 input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=(//a[contains(text(),'Flow Exporter')])[2]
	Page Should Not Contain	Testflow2
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=(//a[contains(text(),'Flow Exporter')])[2]
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=flow_form_name
	Input Text	id=flow_form_name	Testflow
	Click Element	id=flow_form_collector_ip
	Input Text	id=flow_form_collector_ip	${Collector_ip1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	Page Should Contain	Error on page. Please check.
	GUI::Basic::Open And Login Nodegrid
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=(//a[contains(text(),'Flow Exporter')])[2]
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=flow_form_name
	Input Text	id=flow_form_name	Testflow@123
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Validation error
	GUI::Basic::Open And Login Nodegrid
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=(//a[contains(text(),'Flow Exporter')])[2]
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=flow_form_name
	Input Text	id=flow_form_name	Testflownew
	Click Element	id=flow_form_interface
	Select From List By Label	id=flow_form_interface	${Interface}
	Click Element	id=flow_form_collector_ip
	Input Text	id=flow_form_collector_ip	${Collector_ip2}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Teardown

Test case to add connection with Protocol "Netflowv5" and validate the fields
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=(//a[contains(text(),'Flow Exporter')])[2]
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=flow_form_name
	Input Text	id=flow_form_name	Testflow22
	Click Element	id=flow_form_interface
	Select From List By Label	id=flow_form_interface	${Interface}
	Click Element	id=flow_form_collector_ip
	Input Text	id=flow_form_collector_ip	${Collector_ip1}
	Click Element	id=flow_form_protocol
	Select From List By Label	id=flow_form_protocol	NetFlow v5
	Click Element	id=flow_form_active_timeout
	Input Text	id=flow_form_active_timeout	70
	Click Element	id=flow_form_inactive_timeout
	Input Text	id=flow_form_inactive_timeout	20
	Click Element	id=flow_form_sampling_rate
	Input Text	id=flow_form_sampling_rate	2
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=(//a[contains(text(),'Flow Exporter')])[2]
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=flow_form_name
	Input Text	id=flow_form_name	Testflownew
	Click Element	id=flow_form_interface
	Select From List By Label	id=flow_form_interface	${Interface}
	Click Element	id=flow_form_collector_ip
	Input Text	id=flow_form_collector_ip	${Collector_ip2}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Teardown

Test case to add connection with Protocol "Netflowv9" and validate the fields
	GUI::Basic::Open And Login Nodegrid
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=(//a[contains(text(),'Flow Exporter')])[2]
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=flow_form_name
	Input Text	id=flow_form_name	Testflow3
	Click Element	id=flow_form_interface
	Select From List By Label	id=flow_form_interface	eth0
	Click Element	id=flow_form_collector_ip
	Input Text	id=flow_form_collector_ip	${Collector_ip1}
	Click Element	//*[@id="flow_form_protocol"]
	Select From List By Label	//*[@id="flow_form_protocol"]	NetFlow v9
	Click Element	id=flow_form_active_timeout
	Input Text	id=flow_form_active_timeout	80
	Click Element	id=flow_form_inactive_timeout
	Input Text	id=flow_form_inactive_timeout	30
	Click Element	id=flow_form_sampling_rate
	Input Text	id=flow_form_sampling_rate	3
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=(//a[contains(text(),'Flow Exporter')])[2]
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=flow_form_name
	Input Text	id=flow_form_name	Testflownew
	Click Element	id=flow_form_interface
	Select From List By Label	id=flow_form_interface	${Interface}
	Click Element	id=flow_form_collector_ip
	Input Text	id=flow_form_collector_ip	${Collector_ip2}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Teardown

Test case to Confirm nfcapd/nfdump is running and listen to port 2055 with Protocol "Netflow v5/9
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=(//a[contains(text(),'Flow Exporter')])[2]
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	css=#flow-Testflow input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	css=#flow-Testflow22 input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	css=#flow-Testflow3 input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=(//a[contains(text(),'Flow Exporter')])[2]
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=flow_form_name
	Input Text	id=flow_form_name	Testflownew
	Click Element	id=flow_form_interface
	Select From List By Label	id=flow_form_interface	${Interface}
	Click Element	id=flow_form_collector_ip
	Input Text	id=flow_form_collector_ip	${Collector_ip1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${ret}=	Get value	id=hostname
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="peer_header"]//a[text()="Console"]
	sleep	5
	Switch Window	title=${ret}
	Wait Until Page Contains Element	//iframe[@id='termwindow']
	Select Frame	//iframe[@id='termwindow']
	Sleep	10
	Press Keys	//body	RETURN
	Press Keys	//body	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output	${root_login}
	${OUTPUT}=	GUI:Access::Generic Console Command Output	${Netflow_collector_server_login}
	${OUTPUT}=	GUI:Access::Generic Console Command Output	${collector_pass}
	${OUTPUT}=	GUI:Access::Generic Console Command Output	${remove_content}
	${OUTPUT}=	GUI:Access::Generic Console Command Output	${Collector_pass}
	${OUTPUT}=	GUI:Access::Generic Console Command Output	${start_nfdump_service}
	sleep	5s
	${OUTPUT}=	GUI:Access::Generic Console Command Output	${confirm_nfcapd_running}
	should contain	${OUTPUT}	/var/cache/nfdump -p 2055
	${OUTPUT}=	GUI:Access::Generic Console Command Output	${nfcapd_listening_with_netstat}
	should contain	${OUTPUT}	udp  0  0 0.0.0.0:2055
	sleep 600s
	${OUTPUT}=	GUI:Access::Generic Console Command Output	${view_flow_records}
	should not contain	${OUTPUT}	Empty file list
	${OUTPUT}=	GUI:Access::Generic Console Command Output	${stop_nfdump_service}
	${OUTPUT}=	GUI:Access::Generic Console Command Output	${Collector_pass}
	Unselect Frame
	Press Keys	//body	cw
	Close all browsers

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=(//a[contains(text(),'Flow Exporter')])[2]
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	xpath=(//input[@type='checkbox'])[2]
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

GUI:Access::Generic Console Command Output
	[Arguments]	${COMMAND}	${CONTAINS_HELP}=no
	[Documentation]	Input the given command into the ttyd terminal and retrieve the output after prompt lines
	...	== REQUIREMENTS ==
	...	The console type needs to be one of those:
	...	-	CLI -> ends with "]#"
	...	-	Shell -> ends with "$"
	...	-	Root -> ends with "#"
	...	== ARGUMENTS ==
	...	-	COMMAND = Command to be executed
	...	-	CONTAINS_HELP = [yes/no] Console contains "[Enter '^Ec?' for help]" message
	${CHECK_TAB}=	Run Keyword And Return Status	Should Not Contain  ${COMMAND}  TAB
	Run Keyword If  ${CHECK_TAB}	Press Keys  //body	${COMMAND}  RETURN
	...  ELSE	Press Keys	//body	${COMMAND}
	Sleep	1

	Wait Until Keyword Succeeds	1m	1s	Execute JavaScript  term.selectAll();
	${CONSOLE_CONTENT}=	Wait Until Keyword Succeeds	1m	1s	Execute Javascript	return term.getSelection().trim();
	Should Not Contain  ${CONSOLE_CONTENT}  [error.connection.failure] Could not establish a connection to device
	Run Keyword If	"${CONTAINS_HELP}" == "yes"	Should Contain	${CONSOLE_CONTENT}	[Enter '^Ec?' for help]
	${OUTPUT}=  Output From Last Command	${CONSOLE_CONTENT}
	[Return]	${OUTPUT}
