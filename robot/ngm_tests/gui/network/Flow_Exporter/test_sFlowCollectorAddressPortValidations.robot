*** Settings ***
Documentation	Testing Network -> FlowExporter -> sFlow
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	FIREFOX	CHROME
Default Tags	GUI

Suite Setup	SUITE:Setup

*** Variables ***
${PROTOCOL}		sFlow


*** Test Cases ***
Test Enter In Root Mode In Console With Sudo Permissions
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Network FlowExporter

*** Keywords ***
SUITE:Setup
	# Open and login into Nodegrid application
	GUI::Basic::Open And Login Nodegrid

SUITE:Network FlowExporter
	# Navigate to network, click on Flow exporter and validate collector address and port
	SUITE:Navigate to Network and FlowExporter for Collector Address and Port Validations

SUITE:Navigate to Network and FlowExporter for Collector Address and Port Validations
	# Navigating to Network -> Flow Exporter
	GUI::Basic::Network::Flow Exporter::open tab
	# Clicking on Add button
	GUI::Basic::Add
	# Fill the form for sflow with giving name and protocol
	Click Element	id=flow_form_name
	Input Text	id=flow_form_name	Test
	Select From List By Label	id=flow_form_protocol	${PROTOCOL}
	# Click on save button
	GUI::Basic::Save
	# Verify that page contains the error message
	Page Should Contain Element		id=errormsg
	# Click on cancel button
	GUI::Basic::Cancel
	# logout and close the nodegird application
	GUI::Basic::Logout And Close Nodegrid





