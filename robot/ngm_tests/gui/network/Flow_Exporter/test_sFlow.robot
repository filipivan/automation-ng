*** Settings ***
Documentation	Testing Network -> FlowExporter -> sFlow
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	FIREFOX	CHROME
Default Tags	GUI

Suite Setup	SUITE:Setup

*** Variables ***
${TEST_NETFLOW_DEVICE}	test-dev.ice
${PROTOCOL}		sFlow
${DEVCON_TYPE}	device_console
${DEVCON_USERNAME}	${NETFLOW_USERNAME}
${TEST_NETFLOW_PASSWD}	${NETFLOW_PASSWD}
${DEVCON_ASKPASS}	yes
${DEVCON_STATUS}	enabled
${DEVCON_URL}	http://%IP


*** Test Cases ***
Test Enter In Root Mode In Console With Sudo Permissions
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Network FlowExporter


*** Keywords ***
SUITE:Setup
	# Open and login into Nodegrid application
	GUI::Basic::Open And Login Nodegrid

SUITE:Network FlowExporter
	# Navigate to network, click on Flow exporter and fill the form for all the required fields
	SUITE:Navigate to Network and FlowExporter
	# Adding a console device with sflow server to verify the data packages from console
	SUITE:Add a console device	${TEST_NETFLOW_DEVICE}	${DEVCON_TYPE}	${NETFLOW_COLLECTOR_SERVER}	${NETFLOW_USERNAME}	${DEVCON_ASKPASS}	${DEVCON_STATUS}	${DEVCON_URL}
	# Accessing console of the device and viewing data packages
	SUITE:Access Console and View data packages
	SUITE:Teardown

SUITE:Add a console device
	# Adding a console device with ask password
	[Arguments]	${DEVICE} 	${DEVCON_TYPE}	${NETFLOW_COLLECTOR_SERVER}	${NETFLOW_USERNAME}	${DEVCON_ASKPASS}	${DEVCON_STATUS}	${DEVCON_URL}
	#GUI::Basic::Managed Devices::Devices::Open Tab
	GUI::ManagedDevices::Open Devices tab
	# Clicking on Add button
	GUI::Basic::Add
	Wait Until Element Is Visible	jquery=#saveButton
	Input Text	jquery=#spm_name	${DEVICE}
	Select From List By Value	jquery=#type	${DEVCON_TYPE}
	Input Text	jquery=#phys_addr	${NETFLOW_COLLECTOR_SERVER}
	Run Keyword If	'${DEVCON_TYPE}' == 'virtual_console_vmware'	Run Keywords	Element Should Not Be Visible	id=username
	...	AND	Element Should Not Be Visible	id=askpassword
	...	AND	Element Should Not Be Visible	id=passwordfirst
	...	AND	Element Should Not Be Visible	id=passwordconf
	...	AND	Element Should Be Visible	id=vmmanager
	...	AND	Select From List By Index	id=vmmanager	1
	...	ELSE	Run Keywords	Element Should Be Visible	id=username
	...	AND	Element Should Be Visible	id=askpassword
	...	AND	Element Should Be Visible	id=passwordfirst
	...	AND	Element Should Be Visible	id=passwordconf
	...	AND	Input Text	jquery=#username	${NETFLOW_USERNAME}
	...	AND	Select Radio Button	askpassword	${DEVCON_ASKPASS}
	Select From List By Value	jquery=#status	${DEVCON_STATUS}
	Input Text	jquery=#url	${DEVCON_URL}
	GUI::Basic::Save
	GUI::ManagedDevices::Wait For Managed Devices Table
	Table Should Contain	jquery=#SPMTable	${DEVICE}


SUITE:Navigate to Network and FlowExporter
	# The sflow works for versions v5.8 and greater
	# Navigating to Network-> Flow Exporter
	GUI::Basic::Network::Flow Exporter::open tab
	# Clicking on Add button
	GUI::Basic::Add
	# Fill the forms for sflow
	Click Element	id=flow_form_name
	Input Text	id=flow_form_name	Testsflow
	Click Element	id=flow_form_collector_ip
	Input Text	id=flow_form_collector_ip	${NETFLOW_COLLECTOR_SERVER}
	Click Element	id=flow_form_collector_port
	Input Text	id=flow_form_collector_port		${SFLOW_PORT}
	Select From List By Label	id=flow_form_protocol	${PROTOCOL}
	# Clicking on save button
	GUI::Basic::Save

SUITE:Access Console and View data packages
	# Clicking on Access -> Table tab
	GUI::Basic::Access::Table::Open Tab
	Sleep	5s
	GUI::Basic::Spinner Should Be Invisible
	# Click on console for the added device
	GUI:Access::Table::Access Device Console	${TEST_NETFLOW_DEVICE}
	Sleep	5s
	Press Keys	//body	RETURN
	# Press the enter keys
	${OUTPUT}=	GUI:Access::Generic Console Command Output	${TEST_NETFLOW_PASSWD}
	Press Keys	//body	RETURN
	# Type sfcapd command in the output terminal
	${OUTPUT}=	GUI:Access::Generic Console Command Output	sfcapd -E -p 6363 -l /tmp/nfcap-sflow
	Sleep	5s
	# Verify that the outout contains the packages
	Should Contain	${OUTPUT}	FLOW,127.0.0.1,1073741823,1073741823
	# Click control C to stop the terminal flow
	${OUTPUT}=	GUI:Access::Generic Console Command Output	^C
	${OUTPUT}=	GUI:Access::Generic Console Command Output	^C
	Close Browser

SUITE:Delete sFlow
	# Click on Network tab
	GUI::Basic::Network::Flow Exporter::open tab
	Sleep	5s
	# Select the checkbox for added sflow
	Click Element	//td[@class='selectable']//input[@type='checkbox']
	Sleep	5s
	# Click on delete button
	GUI::Basic::Delete
	#Click Button	id=delButton

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	# Delete the added console device
	GUI::ManagedDevices::Delete Device If Exists	${TEST_NETFLOW_DEVICE}
	# Delete sflow from Network explorer
	SUITE:Delete sFlow
	# logout of the application and close the nodegrid
	GUI::Basic::Logout And Close Nodegrid