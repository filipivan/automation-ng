*** Settings ***
Documentation	Testing Network -> FlowExporter -> sFlow
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	FIREFOX	CHROME
Default Tags	GUI

Suite Setup	SUITE:Setup

*** Variables ***
${PROTOCOL}		sFlow

*** Test Cases ***
Test Enter In Root Mode In Console With Sudo Permissions
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Network FlowExporter

*** Keywords ***
SUITE:Setup
	# Open Nodegrid application and login into the app.
	GUI::Basic::Open And Login Nodegrid

SUITE:Network FlowExporter
	# Verifying Network Flow Exporter with sflow for Name validations
    SUITE:Navigate to Network FlowExporter for Name Validations

SUITE:Navigate to Network FlowExporter for Name Validations
	#The sflow works for versions v5.8 and greater
	# Navigating to Network -> Flow Exporter
	GUI::Basic::Network::Flow Exporter::open tab
	# Clicking on Add button
	GUI::Basic::Add
	# Fill the forms for sflow
	Click Element	id=flow_form_name
	Click Element	id=flow_form_collector_ip
	Input Text	id=flow_form_collector_ip	${NETFLOW_COLLECTOR_SERVER}
	Click Element	id=flow_form_collector_port
	Input Text	id=flow_form_collector_port		${SFLOW_PORT}
	Select From List By Label	id=flow_form_protocol	${PROTOCOL}
	# Click on save button
	GUI::Basic::Save
	# Veirfy for the error message
	Page Should Contain Element		id=errormsg
	Click Element	id=flow_form_name
	Input Text	id=flow_form_name	Test sflow
	# Click on save button
	GUI::Basic::Save
	# Verify that page should contain error message
	Page Should Contain Element		id=errormsg
	# Click on the name field
	Click Element	id=flow_form_name
	# Input the name field with any text and some spaces in between
	Input Text	id=flow_form_name	test 123
	# Click on Save button
	GUI::Basic::Save
	# Verify that page contains error message
	Page Should Contain Element		id=errormsg
	# Click on the name field
	Click Element	id=flow_form_name
	# Inout the name field with special characters
	Input Text	id=flow_form_name	test@123
	# Click on save button
	GUI::Basic::Save
	# Verify that page contains error message
	Page Should Contain Element		id=errormsg
	# Click on cancel button
	GUI::Basic::Cancel
	# Lcgout of the nodegrid app and close the browser
	GUI::Basic::Logout And Close Nodegrid





