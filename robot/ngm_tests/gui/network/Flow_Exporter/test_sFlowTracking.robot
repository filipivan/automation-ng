*** Settings ***
Documentation	Testing Network -> FlowExporter -> sFlow
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	FIREFOX	CHROME
Default Tags	GUI

Suite Setup	SUITE:Setup

*** Variables ***

${PROTOCOL}		sFlow

*** Test Cases ***
Test Enter In Root Mode In Console With Sudo Permissions
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Network FlowExporter

*** Keywords ***
SUITE:Setup
	# Open and login into Nodegrid application
	GUI::Basic::Open And Login Nodegrid

SUITE:Network FlowExporter
	# Navigate to Network -> Flow exporter
	SUITE:Navigate to Network and FlowExporter
	# Navigate to Tracking tab and view the data packages
	SUITE:Access Tracking and View data packages
	SUITE:Teardown


SUITE:Navigate to Network and FlowExporter
# The sflow works for versions v5.8 and greater
	# Navigating to Flow Exporter
	GUI::Basic::Network::Flow Exporter::open tab
	# Clicking on Add button
	GUI::Basic::Add
	# Filling the forms for sFlow
	Click Element	id=flow_form_name
	Input Text	id=flow_form_name	Testsflow
	Click Element	id=flow_form_collector_ip
	Input Text	id=flow_form_collector_ip	${NETFLOW_COLLECTOR_SERVER}
	Click Element	id=flow_form_collector_port
	Input Text	id=flow_form_collector_port		${SFLOW_PORT}
	Select From List By Label	id=flow_form_protocol	${PROTOCOL}
	# Clicking on save button
	GUI::Basic::Save

SUITE:Access Tracking and View data packages
	# Click on Tracking -> Network tab
	GUI::Tracking::Open Network Tab
	# Click on Flow Exporter
	Click Element	//span[normalize-space()='Flow Exporter']
	GUI::Basic::Spinner Should Be Invisible
	# Verify that page contains the table with sflow rate and the packages
	Page Should Contain Element		//div[@id='main_doc']
	# Close the browser
	Close Browser

SUITE:Delete sFlow
	# Click on Network tab
	GUI::Basic::Network::Flow Exporter::open tab
	Sleep	5s
	# Select the checkbox for added sflow
	Click Element	//td[@class='selectable']//input[@type='checkbox']
	Sleep	5s
	# Click on delete button
	GUI::Basic::Delete
	#Click Button	id=delButton

SUITE:Teardown
	# Open Nodegrid and login into the app
	GUI::Basic::Open And Login Nodegrid
	# Delet the sflow that was created
	SUITE:Delete sFlow
	# Logout of the nodegrid app and close the application
	GUI::Basic::Logout And Close Nodegrid