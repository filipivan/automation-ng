*** Settings ***
Documentation	Testing System button for SNMP under the Network:SNMP tab
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	GUI

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test NonAccess Controls
	Page Should Contain Element	//*[@id="nonAccessControls"]/input[1]
	Page Should Contain Element	//*[@id="nonAccessControls"]/input[2]
	Page Should Contain Element	//*[@id="nonAccessControls"]/input[3]

Test System Tab
	Click Element	//*[@id="nonAccessControls"]/input[3]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	SNMP System Information
	Page Should Contain	SNMP Engine ID:
	Page Should Contain	SysName:
	Page Should Contain	SysLocation:
	Page Should Contain	SysContact:

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::SNMP::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout
	Close all Browsers
