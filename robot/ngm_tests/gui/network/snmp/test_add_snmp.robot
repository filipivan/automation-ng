*** Settings ***
Documentation	Testing adding SNMP under the Network:SNMP tab
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test V1/V2
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	jquery=#oid	$%#@!
	Select Radio Button	snmpVersion	v1v2
	GUI::Basic::Auto Input Tests	community	!@#$#	Dummy
	GUI::Basic::Auto Input Tests	source  !@#$	Dummy
	GUI::Basic::Auto Input Tests	oid	123	2c::2c  1.6.3.4.23.4.12.4.2
	
	Select Checkbox	xpath=//*[@id="snmpTable"]/tbody/tr/td[1]/input
	Wait Until Element Is Enabled  id=delButton
  GUI::Basic::Delete

Test V3
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	jquery=#oid	$%#@!
	Select Radio Button	snmpVersion	v3
	GUI::Basic::Auto Input Tests	user_name	${EMPTY}	!@#$#	Dummy
	GUI::Basic::Auto Input Tests	authPassword	Dummy	12345678901234567890123456789012345678901234567890123456789012345	\#1234567890	1234567890
	GUI::Basic::Auto Input Tests	privPassPhrase	12345678901234567890123456789012345678901234567890123456789012345	\#1234567890	1234567890
	GUI::Basic::Cancel

#Test V1/V2 Empty Community
#	GUI::Basic::Add
#	Input Text	//*[@id="source"]	Dummy
#	GUI::Basic::Save
#	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
#	Wait Until Element Contains	jquery=#errormsg	Field must not be empty.
#
#Test V1/V2 Invalid Community
#	Input Text	//*[@id="name"]	%^&
#	GUI::Basic::Save
#	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
#	Wait Until Element Contains	jquery=#errormsg	This field contains invalid characters.
#
#Test V1/V2 Invalid Source
#	Input Text	//*[@id="source"]	%^&
#	Input Text	//*[@id="name"]	Dummy
#	GUI::Basic::Save
#	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
#	Wait Until Element Contains	jquery=#errormsg	Validation error.
#
#Test V1/V2 Invalid OID
#	Input Text	//*[@id="source"]	Dummy
#	Input Text	//*[@id="oid"]	&^%
#	GUI::Basic::Save
#	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
#	Wait Until Element Contains	jquery=#errormsg	Validation error.
#
#Test V1/V2 Number OID
#	Input Text	//*[@id="oid"]	123
#	GUI::Basic::Save
#	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
#	Wait Until Element Contains	jquery=#errormsg	Validation error.
#
#Test V3 Invalid Username
#	Input Text	//*[@id="oid"]	${EMPTY}
#	Select Radio Button	snmpVersion	user
#	Input Text	//*[@id="user_name"]	&^%
#	GUI::Basic::Save
#	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
#	Wait Until Element Contains	jquery=#errormsg	This field contains invalid characters.
#
#Test V3 Authentication Password Too Short
#	Input Text	//*[@id="user_name"]	Dummy
#	Input Text	//*[@id="authPassword"]	Dummy
#	GUI::Basic::Save
#	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
#	Wait Until Element Contains	jquery=#errormsg	This field should have at least 8 characters and less than 64 and should not begin with #.
#
#Test V3 Authentication Password Too Long
#	Input Text	//*[@id="authPassword"]	12345678901234567890123456789012345678901234567890123456789012345
#	GUI::Basic::Save
#	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
#	Wait Until Element Contains	jquery=#errormsg	This field should have at least 8 characters and less than 64 and should not begin with #.
#
#Test V3 Authentication Password Invalid
#	Input Text	//*[@id="authPassword"]	\#1234567890
#	GUI::Basic::Save
#	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
#	Wait Until Element Contains	jquery=#errormsg	This field should have at least 8 characters and less than 64 and should not begin with #.
#
#Test V3 Privacy Password Too Short
#	Input Text	//*[@id="privPassPhrase"]	Dummy
#	Input Text	//*[@id="authPassword"]	1234567890
#	GUI::Basic::Save
#	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
#	Wait Until Element Contains	jquery=#errormsg	This field should have at least 8 characters and less than 64 and should not begin with #.
#
#Test V3 Privacy Password Too Long
#	Input Text	//*[@id="privPassPhrase"]	12345678901234567890123456789012345678901234567890123456789012345
#	GUI::Basic::Save
#	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
#	Wait Until Element Contains	jquery=#errormsg	This field should have at least 8 characters and less than 64 and should not begin with #.
#
#Test V3 Privacy Password Invalid
#	Input Text	//*[@id="privPassPhrase"]	\#1234567890
#	GUI::Basic::Save
#	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
#	Wait Until Element Contains	jquery=#errormsg	This field should have at least 8 characters and less than 64 and should not begin with #.
#
#Test V3 Empty Username
#	Input Text	//*[@id="user_name"]	${EMPTY}
#	GUI::Basic::Save
#	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
#	Wait Until Element Contains	jquery=#errormsg	Field must not be empty.

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::SNMP::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout
	Close all Browsers