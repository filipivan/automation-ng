*** Settings ***
Documentation	Test to add LTE Monitoring via Snmp
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_0

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${Access_Type}	Read and Write
${Community_Name}	test-automation
${OID}	.1.3.6.1.4.1.42518.4.2.1.1.7.2.1
${Modem_Name}	M2 Wireless Modem
${Operator_Value1}
${Operator_Value2}
${Radio_Mode_value1}
${Radio_Mode_value2}
${WARNING_MESSAGE}	...	There is no M2 Wirless Modem on Device

*** Test Cases ***
Test Case Add Snmp Config
	GUI::Basic::Network::SNMP::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	input text	xpath=//input[@id='community']	${Community_Name}
	select from list by label	//*[@id="permission"]	${Access_Type}
	click element	xpath=//label[contains(.,'Enable SNMP for IPv6')]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Test to get operator name
	Sleep    10s
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

	GUI::Basic::Tracking::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="wmodem"]/span  #Aba Wirless Modem
	GUI::Basic::Spinner Should Be Invisible

	sleep	5s
	${Operator_Value1}=	Get Text	xpath=//tr[1]/td[7]
	sleep	5s
	${Radio_Mode_value1}=	Get Text	xpath=//tr[1]/td[8]
	
Test Case to verify logs on snmpwalk cmd
	SUITE:Test Case to Enter the Shell
	${OUTPUT}=	GUI:Access::Generic Console Command Output	snmpwalk -c ${Community_Name} -v 2c ::1 ${OID}
	Sleep	15s
	Should Match Regexp	${OUTPUT}	.*STRING.*

Test Case to Verify Information on Tracking page
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

	GUI::Basic::Tracking::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="wmodem"]/span  #Aba Wirless Modem
	GUI::Basic::Spinner Should Be Invisible

	page should contain	 ${Operator_Value1}
	page should contain	 ${Radio_Mode_value1}

Test Case to Verify Information on Shell Mode
	SUITE:Test Case to Enter the Shell
	${OUTPUT}=	GUI:Access::Generic Console Command Output	snmpwalk -c ${Community_Name} -v 2c ::1 ${OID}.8
	Sleep	15s
	Should Match Regexp	${OUTPUT}	.*${Operator_Value1}.*
	
	SUITE:Test Case to Enter the Shell
	${OUTPUT}=	GUI:Access::Generic Console Command Output	snmpwalk -c ${Community_Name} -v 2c ::1 ${OID}.9
	Sleep	15s
	Should Match Regexp	${OUTPUT}	.*${Radio_Mode_value1}.*

Test Case to delete Snmp Config
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::SNMP::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//input[@type='checkbox']
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

	${IS_NSR}	GUI::Basic::Is Net SR
	Skip If	not ${IS_NSR}

	GUI::Basic::Tracking::Devices::Open Tab
	${HAS_Wirless_modem}	Run Keyword And Return Status	Page Should Contain	USB devices
	Skip If	not ${HAS_Wirless_modem}

	click element	xpath=//span[contains(.,'USB devices')]
	GUI::Basic::Spinner Should Be Invisible
	${HAS_Wirless_modem}	Run Keyword and Return Status	Page Should Contain	 ${Modem_Name}
	Run Keyword If	not ${HAS_Wirless_modem}	Skip	${WARNING_MESSAGE}

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:Test Case to Enter the Shell
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="|nodegrid"]/div[1]/div[2]/a
	Sleep	20s
	Run Keyword If  '${NGVERSION}'<='5.4'	Switch Window	nodegrid
	Run Keyword If  '${NGVERSION}'>'5.4'	Switch Window	nodegrid - Console

	Sleep	10s
	Select Frame	xpath=//*[@id='termwindow']
	Press Keys	None	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output	shell sudo su -
	Sleep	15s
	Should Contain	${OUTPUT}	root@nodegrid:~#