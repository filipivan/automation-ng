*** Settings ***
Documentation	Test for higher encryption for snmp
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${NETWORK}	${MODEL}	${VERSION}	WINDOWS     DEPENDENCE_SWITCH       NON-CRITICAL
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${Usr}     test1
${Authentication_Password}      12345678
${CONNECTION}	ETH0
${TREE_STATIC}	.1.3.6.1.4.1.42518.4.2.1.1.1.1.0
${Version}      v3
${Security_level1}      AuthNoPriv
${Authentication_Alogorithm1}       MD5
${Privacy_Algorithm1}       DES
${Access_Type1}     Read only
${Security_level2}      AuthPriv
${Authentication_Alogorithm2}       SHA-512
${Privacy_Algorithm2}       AES-256
${Access_Type2}     Read and Write
${Tree_Static_cmd}      OID=.1.3.6.1.4.1.42518.4.2.1.1.1.1.0
${IPV6_address}


*** Test Cases ***
Test Case to Enable SNMP on both Sides
    [Tags]      NON-CRITICAL    NEED-REVIEW
    SUITE:Test Case to Enable SNMP
    GUI::Basic::Open And Login Nodegrid     PAGE=${HOMEPAGEPEER}
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Test Case to Enable SNMP

Test Case Check If System Has SNMP Protocol Support On Both Sides
    [Tags]      NON-CRITICAL    NEED-REVIEW
    SUITE:Setup
    SUITE:Test Case to Enter the Shell and Check SNMP Protocol Available on Both Sides
    GUI::Basic::Open And Login Nodegrid     PAGE=${HOMEPAGEPEER}
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Test Case to Enter the Shell and Check SNMP Protocol Available on Both Sides

Test case to configure SNMP on Host Side
    [Tags]      NON-CRITICAL    NEED-REVIEW
    SUITE:Setup
    SUITE:Test Case to Configure SNMP

Test Configure SNMP With MD5 Authentication Protocol And DES Algorithm
    [Tags]      NON-CRITICAL    NEED-REVIEW
    SUITE:GET Ipv6 address
    SUITE:Encrypted SNMP Connection with Peer Using MD5 Authentication algorithm and DES Privacy Algorithm

Test Configure SNMP With SHA-512 Authentication Protocol And AES-256 Algorithm
    [Tags]      NON-CRITICAL    NEED-REVIEW
    SUITE:Test Case to change the Configure of SNMP
    SUITE:GET Ipv6 address
    SUITE:Encrypted SNMP Connection with Peer Using SHA-512 Authentication algorithm and AES-256 Privacy Algorithm

Test Case to Delete the Configuration
    [Tags]      NON-CRITICAL    NEED-REVIEW
    SUITE:Delete the Configuration of SNMP
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Verify the SNMP Status

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
    Close All Browsers

SUITE:Test Case to Enable SNMP
    GUI::Basic::Security::Services::open tab
    GUI::Basic::Spinner Should Be Invisible
    Sleep       10s
    wait until page contains element        css=#snmp
    Run Keyword And Continue On Failure       Select Checkbox       css=#snmp
    Run Keyword And Continue On Failure     Click Element       //*[@id="saveButton"]
    GUI::Basic::Spinner Should Be Invisible

SUITE:Test Case to Configure SNMP
    GUI::Basic::Network::SNMP::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
    Sleep       5s
    click element       xpath=(//input[@id='snmpVersion'])[2]
    input text      user_name       ${Usr}
    select from list by label       //*[@id="authLevel"]        ${Security_level2}
    select from list by label       //*[@id="authType"]     ${Authentication_Alogorithm1}
    select from list by label       //*[@id="privType"]     ${Privacy_Algorithm1}
    select from list by label       //*[@id="permission"]       ${Access_Type1}
    input text      authPassword        ${Authentication_Password}
    input text   privPassPhrase      ${Authentication_Password}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible

SUITE:Test Case to change the Configure of SNMP
    SUITE:Setup
    GUI::Basic::Network::SNMP::Open Tab
	GUI::Basic::Spinner Should Be Invisible
    Click Element       //*[@id="test1"]
    GUI::Basic::Spinner Should Be Invisible
    select from list by label       //*[@id="authLevel"]        ${Security_level1}
    select from list by label       //*[@id="authType"]     ${Authentication_Alogorithm2}
    select from list by label       //*[@id="privType"]     ${Privacy_Algorithm2}
    select from list by label       //*[@id="permission"]       ${Access_Type2}
    input text      authPassword        ${Authentication_Password}
    input text   privPassPhrase      ${Authentication_Password}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible

SUITE:Delete the Configuration of SNMP
    SUITE:Setup
    GUI::Basic::Network::SNMP::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element       //*[@id="user|test1"]/td[1]/input
	Click Element       //*[@id="delButton"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:Verify the SNMP Status
    SUITE:Setup
    GUI::Basic::Security::Services::open tab
    GUI::Basic::Spinner Should Be Invisible
    wait until page contains element        css=#snmp
    checkbox should be selected       css=#snmp
    GUI::Basic::Open And Login Nodegrid     PAGE=${HOMEPAGEPEER}
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Security::Services::open tab
    GUI::Basic::Spinner Should Be Invisible
    wait until page contains element        css=#snmp
    checkbox should be selected       css=#snmp

SUITE:Test Case to Enter the Shell and Check SNMP Protocol Available on Both Sides
    GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element       xpath=//a[contains(text(),'Console')]
    GUI::Basic::Spinner Should Be Invisible
    Sleep       20s
    Run Keyword If  '${NGVERSION}'<='5.4'        Switch Window       nodegrid
    Run Keyword If  '${NGVERSION}'>'5.4'       Switch Window        nodegrid - Console
    Sleep       15s
    Select Frame	xpath=//*[@id='termwindow']
    Press Keys	None	RETURN

    ${OUTPUT}=	GUI:Access::Generic Console Command Output      shell sudo su -
    Sleep       20s
	Should Contain      ${OUTPUT}		root@nodegrid:~#

	${OUTPUT}=	GUI:Access::Generic Console Command Output      snmpget 2>&1 | grep PROTOCOL
	Sleep       20s
    Should Contain      ${OUTPUT}		-a PROTOCOL	set authentication protocol (MD5|SHA|SHA-224|SHA-256|SHA-384|SHA-512)       -x PROTOCOL	set privacy protocol (DES|AES|AES-192|AES-256)

SUITE:GET Ipv6 address
    GUI::Basic::Network::Connections::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    ${IPV6_address}=	    Get Text        //*[@id="ETH0"]/td[8]
    ${IPV6_address}   @{MATCHES}=	Should Match Regexp	${IPV6_address}	(([0-9a-fA-F]{0,4}:){1,7}[0-9a-fA-F]{0,4})
    Set Suite Variable  ${IPV6_address}

SUITE:Encrypted SNMP Connection with Peer Using MD5 Authentication algorithm and DES Privacy Algorithm
    GUI::Basic::Open And Login Nodegrid     PAGE=${HOMEPAGEPEER}
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element       xpath=//a[contains(text(),'Console')]
    GUI::Basic::Spinner Should Be Invisible
    Sleep       20s
    Run Keyword If  '${NGVERSION}'<='5.4'        Switch Window       nodegrid
    Run Keyword If  '${NGVERSION}'>'5.4'       Switch Window        nodegrid - Console
    Sleep       20s
    Select Frame	xpath=//*[@id='termwindow']
    Press Keys	None	RETURN

    ${OUTPUT}=	GUI:Access::Generic Console Command Output      shell sudo su -
    Sleep       20s
	Should Contain      ${OUTPUT}		root@nodegrid:~#

	${OUTPUT}=	GUI:Access::Generic Console Command Output      ${Tree_Static_cmd}
	Sleep       15s
	Should Contain      ${OUTPUT}		root@nodegrid:~#

    ${OUTPUT}=	GUI:Access::Generic Console Command Output      snmpget -v3 -u ${Usr} -a MD5 -A 12345678 -x des -X 12345678 -l AuthPriv ${HOST}:161 $OID
    Sleep       15s
    Should Match Regexp     ${OUTPUT}      .*STRING.*"

	${OUTPUT}=	GUI:Access::Generic Console Command Output      snmpget -v3 -u ${Usr} -a MD5 -A 12345678 -x des -X 12345678 -l AuthPriv ${IPV6_address}:161 $OID
    Sleep       15s
    Should Match Regexp     ${OUTPUT}      .*STRING.*"

SUITE:Encrypted SNMP Connection with Peer Using SHA-512 Authentication algorithm and AES-256 Privacy Algorithm
    GUI::Basic::Open And Login Nodegrid     PAGE=${HOMEPAGEPEER}
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element       xpath=//a[contains(text(),'Console')]
    GUI::Basic::Spinner Should Be Invisible
    Sleep       20s
    Run Keyword If  '${NGVERSION}'<='5.4'       Switch Window           nodegrid
    Run Keyword If  '${NGVERSION}'>'5.4'       Switch Window        nodegrid - Console
    Sleep       20s
    Select Frame	xpath=//*[@id='termwindow']
    Press Keys	None	RETURN

    ${OUTPUT}=	GUI:Access::Generic Console Command Output      shell sudo su -
    Sleep       15s
	Should Contain      ${OUTPUT}		root@nodegrid:~#

	${OUTPUT}=	GUI:Access::Generic Console Command Output      ${Tree_Static_cmd}
	Sleep       15s
	Should Contain      ${OUTPUT}		root@nodegrid:~#

    ${OUTPUT}=	GUI:Access::Generic Console Command Output      snmpget -v3 -u ${Usr} -a sha-512 -A 12345678 -x aes-256 -X 12345678 -l AuthNoPriv ${HOST}:161 $OID
	Sleep       15s
    Should Match Regexp     ${OUTPUT}      .*STRING.*"

	${OUTPUT}=	GUI:Access::Generic Console Command Output      snmpget -v3 -u ${Usr} -a sha-512 -A 12345678 -x aes-256 -X 12345678 -l AuthNoPriv ${IPV6_address}:161 $OID
	Sleep       15s
    Should Match Regexp     ${OUTPUT}      .*STRING.*"