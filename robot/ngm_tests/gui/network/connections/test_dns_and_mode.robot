*** Settings ***
Documentation	Testing the DNS and Mode options for Network:Connections
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CONNECTION}	test

*** Test Cases ***
Test IPv4 DNS
	@{VALIDS}=	Create List	12.12.12.12
	@{INVALIDS}=	Create List	1..245.2.1	.	12.53.12.53/2	12/25\\12#24
	@{MSK_VALIDS}=	Create List	1.1.1.1/12
	@{MSK_INVALIDS}=	Create List	[34	/24	..1	\\12
	GUI::Basic::Spinner Should Be Invisible
	Select Radio Button	method	disabled
	Element Should Not Be Visible	jquery=#address
	Select Radio Button	method	auto
	Element Should Not Be Visible	jquery=#address
	Select Radio Button	method	manual
	Element Should Be Visible	jquery=#address
	Input Text	id=address	${VALIDS}[0]
	Input Text	id=netmask	${MSK_VALIDS}[0]
	Input Text	id=gateway	${VALIDS}[0]
	GUI::Basic::Input Text::Validation	ipv4dns	${VALIDS}	${INVALIDS}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="test"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

Test IPv6 DNS
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	jquery=#connName	${CONNECTION}
	${VALIDS}=	Create List	002c::002c
	@{INVALIDS}=	Create List	20f::3::245f	32:::425v	002g::002c	2d4f:2352d:2453::2ddc
	@{MSK_VALIDS}=	Create List	64
	@{MSK_INVALIDS}=	Create List	[34	/24	..1	\\12
	Select Radio Button	method6	ignore
	Element Should Not Be Visible	jquery=#address6
	Select Radio Button	method6	auto
	Element Should Not Be Visible	jquery=#address6
	Select Radio Button	method6	dhcp
	Element Should Not Be Visible	jquery=#address6
	Select Radio Button	method6	manual
	Element Should Be Visible	jquery=#address6
	Input Text	id=address6	${VALIDS}[0]
	Input Text	id=netmask6	${MSK_VALIDS}[0]
	Input Text	id=gateway6	${VALIDS}[0]
	GUI::Basic::Input Text::Validation	ipv6dns	${VALIDS}	${INVALIDS}
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	${CONNECTIONS}=	Create List	${CONNECTION}
	Set Suite Variable	${CONNECTIONS}	${CONNECTIONS}
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table If Exists	peerTable_wrapper	${CONNECTIONS}
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	jquery=#connName	${CONNECTION}

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	${HAS_CANCEL_BUTTON}	Run Keyword And Return Status	Page Should Contain Button	//input[@id='cancelButton']
	Run Keyword If	${HAS_CANCEL_BUTTON}	Run Keyword And Ignore Error	GUI::Basic::Button::Cancel
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="test"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
