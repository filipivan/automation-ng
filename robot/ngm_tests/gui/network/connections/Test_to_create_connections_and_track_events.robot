*** Settings ***
Documentation	Test to Create a Some of Connections to perform up/down and Track the Events for Each Connections
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags  	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_0

*** Variables ***
${ip_address}		10.0.0.31
${net_mask}		24

*** Test Cases ***
Test Case to Create a Connection for Backplane0 and Check the Events
	[Documentation]  Set the blackplaneo to static ip Do up/down functions, Check the information on network::connection table and track the events
	${HOST_HAS_BACK0}   GUI::Network::Check If Has Connection  BACKPLANE0
	Skip If	not ${HOST_HAS_BACK0}	HOST has no BACKPLANE0 connection
	Set Suite Variable	${HOST_HAS_BACK0}
	SUITE:Edit Backplane0 To Static Ipv4 address
	Wait Until Keyword Succeeds	5x	5s		SUITE:GUI::Network::Table::Details		BACKPLANE0	Connected	Ethernet		backplane0		Up
	Wait Until Keyword Succeeds	5x	5s		SUITE:TO track network connection table and event list on Backplane0 down status
	Wait Until Keyword Succeeds	5x	5s		SUITE:TO track network connection table and event list on Backplane0 up status
	[Teardown]	SUITE: To Set Default Config for Backplane0

Test Case to Add Vlan Connection and Check the Events
	[Documentation]  Configure Vlan Do up/down functions, Check the information on network::connection table and track the events
 	SUITE:To Add VLAN Connection	Vlan10	vlan	backplane0	10.0.0.33	24	10
	Wait Until Keyword Succeeds	5x	5s		SUITE:GUI::Network::Table::Details	Vlan10	Connected	VLAN	backplane0.10	Up
	Wait Until Keyword Succeeds	5x	5s		SUITE:TO track network connection table and event list on vlan down status
	Wait Until Keyword Succeeds	5x	5s		SUITE:TO track network connection table and event list on vlan up status
	[Teardown]	SUITE:TO Delete Connection		Vlan10

Test Case to Add Bridge Connection and Check the Events
	[Documentation]  Configure Bridge Do up/down functions, Check the information on network::connection table and track the events
	SUITE:TO Add Bridge Connection		Test-Bridge		Bridge		backplane0		20.0.0.33		24
	Wait Until Keyword Succeeds	5x	5s		SUITE:GUI::Network::Table::Details	Test-Bridge	Connected	Bridge		br0		Up
	Wait Until Keyword Succeeds	5x	5s		SUITE:TO track network connection table and event list on Bridge down status
	Wait Until Keyword Succeeds	5x	5s		SUITE:TO track network connection table and event list on Bridge up status
	[Teardown]	SUITE:TO Delete Connection		Test-Bridge

Test Case to Add Bonding Connection and Check the Events
	[Documentation]  Configure Bonding Do up/down functions, Check the information on network::connection table and track the events
	SUITE:TO Add Bonding Connection		Test-Bonding		Bonding		Active backup	eth1	backplane0		30.0.0.33		24
	Wait Until Keyword Succeeds	5x	5s		SUITE:GUI::Network::Table::Details	Test-Bonding	Connected	Bonding		eth1		Up
	Wait Until Keyword Succeeds	5x	5s	SUITE:TO track network connection table and event list on Bonding down status
	Wait Until Keyword Succeeds	5x	5s	SUITE:TO track network connection table and event list on Bonding up status
	[Teardown]	SUITE:TO Delete Connection		Test-Bonding

Test Case to Add Loopback Connection and Check the Events
	[Documentation]  Configure Loopback Do up/down functions, Check the information on network::connection table and track the events
	SUITE:TO Add Loopback Connection	Test-Loopback	Loopback	40.0.0.33	24
	Wait Until Keyword Succeeds	5x	5s		SUITE:GUI::Network::Table::Details	Test-Loopback	Connected	Loopback		loopback1		Up
	Wait Until Keyword Succeeds	5x	5s	SUITE:TO track network connection table and event list on Loopback down status
	Wait Until Keyword Succeeds	5x	5s	SUITE:TO track network connection table and event list on Loopback up status
	[Teardown]	SUITE:TO Delete Connection		Test-Loopback

Test Case to Add GSM Connection and Check the Events
	[Documentation]  Configure GSM Do up/down functions, Check the information on network::connection table and track the events
	${HAS_GSM}	GUI::Tracking::Check If Has GSM Module and Sim Card with Signal
	Set Suite Variable	${HAS_GSM}
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	${INDEX_NUMBER}	GUI::Tracking::Get Index Number With 5G Sim Card Up Otherwise 4G Up
	${ROW_NUMBER}	Evaluate	${INDEX_NUMBER} + 1
	${GSM_CDC_WDMX}	GUI::Tracking::Get Tracking Wireless Modem Table Cell	ROW=${ROW_NUMBER}	COLUMN=2
	Set Suite Variable	${GSM_CDC_WDMX}
	SUITE:To Add GSM connection	Test-GSM	Mobile Broadband GSM	${GSM_CDC_WDMX}
	Wait Until Keyword Succeeds	5x	5s		SUITE:GUI::Network::Table::Details	Test-GSM	Connected	Mobile Broadband GSM		${GSM_CDC_WDMX}		Up
	Wait Until Keyword Succeeds	5x	5s	SUITE:TO track network connection table and event list on GSM down status
	Wait Until Keyword Succeeds	5x	5s	SUITE:TO track network connection table and event list on GSM up status
	[Teardown]	SUITE:TO Delete Connection		Test-GSM

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:Edit Backplane0 To Static Ipv4 address
	[Documentation]		Set backplane0 to static Ip
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	click element		xpath=//a[contains(text(),'BACKPLANE0')]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=.radio:nth-child(3) #method
	Click Element	id=address
	Input Text	id=address	${ip_address}
	Click Element	id=netmask
	Input Text	id=netmask	${net_mask}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:GUI::Network::Table::Details
	[Documentation]		Get the table details on network::connection page
	[Arguments]	${CONNECTION}	${STATUS}	${TYPE}	${INTERFACE}	${CARRIER_STATE}
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}		${CONNECTION}+\\s+${STATUS}+\\s+${TYPE}+\\s+${INTERFACE}+\\s+${CARRIER_STATE}+\\s+\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\/\\d{2}

SUITE:GUI::Network::To Down the Connection
	[Documentation]		Set the connection to down status
	[Arguments]	${CONNECTION}
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	wait until page contains element		xpath=//tr[@id='${CONNECTION}']/td/input
	click element		xpath=//tr[@id='${CONNECTION}']/td/input
	click element		xpath=//div[@id='nonAccessControls']/input[4]
	GUI::Basic::Spinner Should Be Invisible

SUITE:GUI::Network::To Up the Connection
	[Documentation]		Set the connection to up status
	[Arguments]	${CONNECTION}
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	wait until page contains element		xpath=//tr[@id='${CONNECTION}']/td/input
	click element		xpath=//tr[@id='${CONNECTION}']/td/input
	click element		xpath=//div[@id='nonAccessControls']/input[3]
	GUI::Basic::Spinner Should Be Invisible

SUITE:TO track network connection table and event list on Backplane0 down status
	[Documentation]		Track the event for backplane0 down status
	SUITE:GUI::Network::To Down the Connection	BACKPLANE0
	SUITE:TO Track Events
	${TABLE_CONTENTS}=	Get Text	eventListTable
	Log	\nTable contents in Tracking::EventsList::Event: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp		${TABLE_CONTENTS}	Network+\\s+Connection+\\s+State\\s+Changed.\\s+Action:+\\s+down.+\\s+Connection:+\\s+BACKPLANE0.+\\s+Interface:+\\s+backplane0.+\\s+Type:+\\s+ethernet.+\\s+User:+\\s+admin.

SUITE:TO track network connection table and event list on Backplane0 up status
	[Documentation]		Track the event for backplane0 up status
	SUITE:GUI::Network::To Up the Connection		BACKPLANE0
	Wait Until Keyword Succeeds	5x	5s		SUITE:GUI::Network::Table::Details	BACKPLANE0	Connected	Ethernet		backplane0		Up
	Wait Until Keyword Succeeds	10x	5s	SUITE:TO Track Events
	${TABLE_CONTENTS}=	Get Text	eventListTable
	Log	\nTable contents in Tracking::EventsList::Event: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp		${TABLE_CONTENTS}	Network+\\s+Connection+\\s+State\\s+Changed.\\s+Action:+\\s+up.+\\s+Connection:+\\s+BACKPLANE0.+\\s+Interface:+\\s+backplane0.+\\s+Type:+\\s+ethernet.+\\s+User:+\\s+admin.


SUITE: To Set Default Config for Backplane0
	[Documentation]	Set to default config for Backplane0
	SUITE:Setup
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	click element		xpath=//a[contains(text(),'BACKPLANE0')]
	GUI::Basic::Spinner Should Be Invisible
	click element		xpath=//form[@id='netmngConnAddForm']/div[2]/div[2]/div/div/div/div/label
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Add VLAN Connection
	SUITE:Setup
	[Arguments]		${VLAN_NAME}	${CONNECTION_TYPE}		${CONNECTION_INTERFACE}		${IPv4_ADDRESS}		${BIT_MASK}		${VLAN_ID}
	[Documentation]	Add Vlan Connection
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible		//*[@id="connName"]		20s
	Input Text	//*[@id="connName"]		${VLAN_NAME}
	sleep		5s
	Select From List By value		//*[@id="connType"]		${CONNECTION_TYPE}
	Select From List By value		//*[@id="connItfName"]	${CONNECTION_INTERFACE}
	sleep		5s
	Wait Until Element Is Visible		css=.radio:nth-child(3)
	Click Element	css=.radio:nth-child(3) #method
	Click Element		id=address
	Input Text		id=address		${IPv4_ADDRESS}
	Click Element		id=netmask
	Input Text		id=netmask	${BIT_MASK}
	Input Text		//*[@id="vlanId"]	${VLAN_ID}
	Click Element		//*[@id="method6"]	#NO IPV6
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain		${VLAN_NAME}

SUITE:TO Track Events
	[Documentation]		To track the events search for the filter 143 id
	GUI::Basic::Tracking::Event List::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//span[contains(.,'Events')]
	wait until page contains element		id=filter_field
	input text	id=filter_field	143
	click element	id=button_search_date
	wait until page contains		143

SUITE:TO track network connection table and event list on vlan down status
	[Documentation]		Track the event for vlan down status
	SUITE:GUI::Network::To Down the Connection		Vlan10
	Wait Until Keyword Succeeds	2x	5s	SUITE:TO Track Events
	${TABLE_CONTENTS}=	Get Text	eventListTable
	Log	\nTable contents in Tracking::EventsList::Event: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp		${TABLE_CONTENTS}	Network+\\s+Connection+\\s+State+\\s+Changed.+\\s+Action:+\\s+down.+Connection:+\\s+Vlan10.+\\s+Type:+\\s+vlan.+\\s+User:+\\s+admin.

SUITE:TO track network connection table and event list on vlan up status
	[Documentation]		Track the event for vlan up status
	SUITE:GUI::Network::To Up the Connection		Vlan10
	Wait Until Keyword Succeeds	5x	5s		SUITE:GUI::Network::Table::Details	Vlan10	Connected	VLAN	backplane0.10	Up
	Wait Until Keyword Succeeds	2x	5s	SUITE:TO Track Events
	${TABLE_CONTENTS}=	Get Text	eventListTable
	Log	\nTable contents in Tracking::EventsList::Event: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp		${TABLE_CONTENTS}	Network+\\s+Connection+\\s+State+\\s+Changed.+\\s+Action:+\\s+up.+Connection:+\\s+Vlan10.+\\s+Type:+\\s+vlan.+\\s+User:+\\s+admin.

SUITE:TO Delete Connection
	[Documentation]		To Delete the network connection
	SUITE:Setup
	[Arguments]		${CONNECTION_NAME}
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	click element		xpath=//tr[@id='${CONNECTION_NAME}']/td/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

SUITE:TO Add Bridge Connection
	SUITE:Setup
	[Arguments]		${CONNECTION_NAME}		${CONNECTION_TYPE}		${Bridge_INTERFACE}		${IPv4_ADDRESS}		${BIT_MASK}
	[Documentation]	Add Bridge Connection
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible		//*[@id="connName"]		20s
	Input Text	//*[@id="connName"]	${CONNECTION_NAME}
	sleep		5s
	GUI::Basic::Click Element	//select[@id='connType']
	Select From List By Label	//select[@id='connType']	${CONNECTION_TYPE}
	GUI::Basic::Input Text	//input[@id='bridgeSlave']	${Bridge_INTERFACE}
	Wait Until Element Is Visible		css=.radio:nth-child(3)
	Click Element	css=.radio:nth-child(3) #method
	Click Element		id=address
	Input Text		id=address		${IPv4_ADDRESS}
	Click Element		id=netmask
	Input Text		id=netmask	${BIT_MASK}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain		${CONNECTION_NAME}

SUITE:TO track network connection table and event list on Bridge down status
	[Documentation]		Track the event for bridge down status
	SUITE:GUI::Network::To Down the Connection		Test-Bridge
	Wait Until Keyword Succeeds	10x	5s	SUITE:TO Track Events
	${TABLE_CONTENTS}=	Get Text	eventListTable
	Log	\nTable contents in Tracking::EventsList::Event: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp		${TABLE_CONTENTS}	Network+\\s+Connection+\\s+State\\s+Changed.\\s+Action:+\\s+down.+\\s+Connection:+\\s+Test-Bridge.+\\s+Interface:+\\s+br0.+\\s+Type:+\\s+bridge.+\\s+User:+\\s+admin.

SUITE:TO track network connection table and event list on Bridge up status
	[Documentation]		Track the event for bridge up status
	SUITE:GUI::Network::To Up the Connection		Test-Bridge
	Wait Until Keyword Succeeds	5x	5s		SUITE:GUI::Network::Table::Details	Test-Bridge	Connected	Bridge		br0		Up
	Wait Until Keyword Succeeds	10x	5s	SUITE:TO Track Events
	${TABLE_CONTENTS}=	Get Text	eventListTable
	Log	\nTable contents in Tracking::EventsList::Event: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp		${TABLE_CONTENTS}	Network+\\s+Connection+\\s+State\\s+Changed.\\s+Action:+\\s+up.+\\s+Connection:+\\s+Test-Bridge.+\\s+Interface:+\\s+br0.+\\s+Type:+\\s+bridge.+\\s+User:+\\s+admin.

SUITE:TO Add Bonding Connection
	[Documentation]		To Add Bonding Connection
	SUITE:Setup
	[Arguments]		${CONNECTION_NAME}		${CONNECTION_TYPE}		${BOND_MODE}	${PRIMARY_VALUE}	${SECONDARY_VALUE}	${IPv4_ADDRESS}		${BIT_MASK}
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible		//*[@id="connName"]		20s
	Input Text	//*[@id="connName"]	${CONNECTION_NAME}
	sleep		5s
	GUI::Basic::Click Element	//select[@id='connType']
	Select From List By Label	//select[@id='connType']	${CONNECTION_TYPE}
	Select From List By label	//select[@id='bondMode']	${BOND_MODE}
	Select From List By label	xpath=//select[@id='bondPrimary']		${PRIMARY_VALUE}
	Select From List By label	xpath=//select[@id='bondSecondary']		${SECONDARY_VALUE}
	Wait Until Element Is Visible		css=.radio:nth-child(3)
	Click Element	css=.radio:nth-child(3) #method
	Click Element		id=address
	Input Text		id=address		${IPv4_ADDRESS}
	Click Element		id=netmask
	Input Text		id=netmask	${BIT_MASK}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep	10s
	Page Should Contain		${CONNECTION_NAME}

SUITE:TO track network connection table and event list on Bonding down status
	[Documentation]		Track the event for bonding down status
	SUITE:GUI::Network::To Down the Connection	Test-Bonding
	SUITE:TO Track Events
	${TABLE_CONTENTS}=	Get Text	eventListTable
	Log	\nTable contents in Tracking::EventsList::Event: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp		${TABLE_CONTENTS}	Network+\\s+Connection+\\s+State\\s+Changed.\\s+Action:+\\s+down.+\\s+Connection:+\\s+Test-Bonding.+\\s+Interface:+\\s+bond0.+\\s+Type:+\\s+bond.+\\s+User:+\\s+admin.

SUITE:TO track network connection table and event list on Bonding up status
	[Documentation]		Track the event for bonding up status
	SUITE:GUI::Network::To Up the Connection		Test-Bonding
	Wait Until Keyword Succeeds	5x	5s		SUITE:GUI::Network::Table::Details	Test-Bonding	Connected	Bonding		eth1		Up
	Wait Until Keyword Succeeds	10x	5s	SUITE:TO Track Events
	${TABLE_CONTENTS}=	Get Text	eventListTable
	Log	\nTable contents in Tracking::EventsList::Event: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp		${TABLE_CONTENTS}	Network+\\s+Connection+\\s+State\\s+Changed.\\s+Action:+\\s+up.+\\s+Connection:+\\s+Test-Bonding.+\\s+Interface:+\\s+bond0.+\\s+Type:+\\s+bond.+\\s+User:+\\s+admin.

SUITE:TO Add Loopback Connection
	SUITE:Setup
	[Arguments]		${CONNECTION_NAME}		${CONNECTION_TYPE}		${IPv4_ADDRESS}		${BIT_MASK}
	[Documentation]	Add Loopback Connection
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible		//*[@id="connName"]		20s
	Input Text	//*[@id="connName"]	${CONNECTION_NAME}
	sleep		5s
	GUI::Basic::Click Element		//select[@id='connType']
	Select From List By Label		//select[@id='connType']	${CONNECTION_TYPE}
	Run Keyword And Continue on Failure		Wait Until Element Is Visible		xpath=//form[@id='netmngConnAddForm']/div[2]/div[2]/div/div[3]/div/div[2]/label
	Run Keyword And Continue on Failure		Click Element	xpath=//form[@id='netmngConnAddForm']/div[2]/div[2]/div/div[3]/div/div[2]/label
	set focus to element		xpath=//form[@id='netmngConnAddForm']/div[2]/div[2]/div/div/div/div[4]/div/input
	execute javascript  document.getElementById("address").scrollIntoView()
	input text		xpath=(//input[@id='address'])[2] 	${IPv4_ADDRESS}
	input text		xpath=(//input[@id='netmask'])[2]		${BIT_MASK}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain		${CONNECTION_NAME}

SUITE:TO track network connection table and event list on Loopback down status
	[Documentation]		Track the event for loopback down status
	SUITE:GUI::Network::To Down the Connection	Test-Loopback
	SUITE:TO Track Events
	${TABLE_CONTENTS}=	Get Text	eventListTable
	Log	\nTable contents in Tracking::EventsList::Event: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp		${TABLE_CONTENTS}	Network+\\s+Connection+\\s+State\\s+Changed.\\s+Action:+\\s+down.+\\s+Connection:+\\s+Test-Loopback.+\\s+Interface:+\\s+loopback1.+\\s+Type:+\\s+dummy.+\\s+User:+\\s+admin.

SUITE:TO track network connection table and event list on Loopback up status
	[Documentation]		Track the event for loopback down status
	SUITE:GUI::Network::To Up the Connection		Test-Loopback
	Wait Until Keyword Succeeds	5x	5s		SUITE:GUI::Network::Table::Details	Test-Loopback	Connected	Loopback		loopback1		Up
	Wait Until Keyword Succeeds	10x	5s	SUITE:TO Track Events
	${TABLE_CONTENTS}=	Get Text	eventListTable
	Log	\nTable contents in Tracking::EventsList::Event: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp		${TABLE_CONTENTS}	Network+\\s+Connection+\\s+State\\s+Changed.\\s+Action:+\\s+up.+\\s+Connection:+\\s+Test-Loopback.+\\s+Interface:+\\s+loopback1.+\\s+Type:+\\s+dummy.+\\s+User:+\\s+admin.

SUITE:To Add GSM connection
	SUITE:Setup
	[Documentation]	Create a new GSM connection
	[Arguments]	${NAME}	${TYPE}	${INTERFACE}
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Add
	GUI::Basic::Input Text	//input[contains(@id,'connName')]	${NAME}
	GUI::Basic::Click Element	//select[@id='connType']
	Select From List By Label	//select[@id='connType']	${TYPE}
	GUI::Basic::Click Element	//select[@id='connItfName']
	Select From List By Label	//select[@id='connItfName']	${INTERFACE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:TO track network connection table and event list on GSM down status
	[Documentation]		Track the event for GSM down status
	SUITE:GUI::Network::To Down the Connection	Test-GSM
	SUITE:TO Track Events
	${TABLE_CONTENTS}=	Get Text	eventListTable
	Log	\nTable contents in Tracking::EventsList::Event: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp		${TABLE_CONTENTS}	Network+\\s+Connection+\\s+State\\s+Changed.\\s+Action:+\\s+down.+\\s+Connection:+\\s+Test-GSM.+\\s+Interface:+\\s+${GSM_CDC_WDMX}.+\\s+Type:+\\s+gsm.+\\s+User:+\\s+admin.

SUITE:TO track network connection table and event list on GSM up status
	[Documentation]		Track the event for GSM up status
	SUITE:GUI::Network::To Up the Connection		Test-GSM
	Wait Until Keyword Succeeds	5x	5s		SUITE:GUI::Network::Table::Details	Test-GSM	Connected	Mobile Broadband GSM		${GSM_CDC_WDMX}		Up
	Wait Until Keyword Succeeds	10x	5s	SUITE:TO Track Events
	${TABLE_CONTENTS}=	Get Text	eventListTable
	Log	\nTable contents in Tracking::EventsList::Event: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp		${TABLE_CONTENTS}	Network+\\s+Connection+\\s+State\\s+Changed.\\s+Action:+\\s+up.+\\s+Connection:+\\s+Test-GSM.+\\s+Interface:+\\s+${GSM_CDC_WDMX}.+\\s+Type:+\\s+gsm.+\\s+User:+\\s+admin.
