*** Settings ***
Documentation    Automated test for a/31 p2p link
Metadata    Version    1.0
Metadata    Executed At    ${HOST}
Metadata    Executed with    ${BROWSER}
Resource    ../../init.robot
Force Tags   PART-3    GUI    ${BROWSER}    ${SYSTEM}    ${MODEL}    ${VERSION}    EXCLUDEIN4_2    EXCLUDEIN5_0

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Variables ***
${USERNAME}    ${DEFAULT_USERNAME}
${PASSWORD}    ${DEFAULT_PASSWORD}
${DELAY}    15s
${ip_T1eth1}    10.0.0.220
${ip_T2eth1}    10.0.0.221

*** Test Cases ***
Test case to add 31 bit bitmask to eth1 of both targets
    Skip If    not ${HAS_HOSTSHARED}   There are no HOST SHARED for this build
    Skip If    not ${HOST_HAS_ETH1_UP} or not ${HOSTSHARED_HAS_ETH1_UP}    The HOST or HOSTSHARED units has no ETH1 connection UP
    SUITE:Addbitmask
    GUI::Basic::Spinner Should Be Invisible

Test case to send traffic
    Skip If    not ${HAS_HOSTSHARED}   There are no HOST SHARED for this build
    Skip If    not ${HOST_HAS_ETH1_UP} or not ${HOSTSHARED_HAS_ETH1_UP}    The HOST or HOSTSHARED units has no ETH1 connection UP
    GUI::Basic::Switch Browser    ALIAS=HOST
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::System::Toolkit::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element    //*[@id="icon_NetworkTools"]
    GUI::Basic::Spinner Should Be Invisible
    Click Element    css=.fixed-top
    GUI::Basic::Spinner Should Be Invisible
    Click Element    //*[@id="ipaddress"]
    GUI::Basic::Spinner Should Be Invisible
    Input Text    //*[@id="ipaddress"]    ${ip_T2eth1}
    Click Element    id=interface
    Select From List By Label    //*[@id="interface"]    eth1
    Click Element    //*[@id="networktools"]/div[2]/div[1]/div/input[1]
    sleep    ${delay}
    Page Should Contain    5 packets transmitted, 5 received, 0% packet loss


*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid    ALIAS=HOST
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Network::Connections::open tab
    GUI::Basic::Spinner Should Be Invisible
    ${HOST_HAS_ETH1_UP}    SUITE:Check If ETH1 Exist And Is UP
    Set Suite Variable    ${HOST_HAS_ETH1_UP}

    Skip If    not ${HAS_HOSTSHARED}   There are no HOST SHARED for this build
    GUI::Basic::Open And Login Nodegrid    PAGE=${HOMEPAGESHARED}    ALIAS=SHARED
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Network::Connections::open tab
    GUI::Basic::Spinner Should Be Invisible
    ${HOSTSHARED_HAS_ETH1_UP}    SUITE:Check If ETH1 Exist And Is UP
    Set Suite Variable    ${HOSTSHARED_HAS_ETH1_UP}
    Run KEyword If    not ${HOST_HAS_ETH1_UP} or not ${HOSTSHARED_HAS_ETH1_UP}    Log
    ...    HOST or HOSTSHARED unit has no ETH1 connection UP    WARN
    Skip If    not ${HOST_HAS_ETH1_UP} or not ${HOSTSHARED_HAS_ETH1_UP}    The HOST or HOSTSHARED units has no ETH1 connection UP

SUITE:Check If ETH1 Exist And Is UP
    ${STATUS}    Run Keyword And Return Status    Page Should contain Element    //*[@id="ETH1"]
    Run Keyword If    not ${STATUS}    Return From Keyword

    ${TABLE_CONTENTS}=    Get Text    peerTable
    Log    \nTable contents in Network :: Connections: \n${TABLE_CONTENTS}    INFO    console=yes
    ${STATUS}    Run Keyword And Return Status    Should Match Regexp    ${TABLE_CONTENTS}    ETH1+\\s+Connected+\\s+Ethernet\\s+eth1+\\s+Up
    [Return]    ${STATUS}

SUITE:Addbitmask
    GUI::Basic::Switch Browser    ALIAS=HOST
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Network::Connections::open tab
    Click Link    ETH1
    GUI::Basic::Spinner Should Be Invisible
    Click Element    css=.radio:nth-child(3) #method
    Click Element    id=address
    Input Text    id=address    ${ip_T1eth1}
    Click Element    id=netmask
    Input Text    id=netmask    31
    GUI::Basic::Save

    GUI::Basic::Switch Browser    ALIAS=SHARED
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Network::Connections::open tab
    Click Link    ETH1
    GUI::Basic::Spinner Should Be Invisible
    Click Element    css=.radio:nth-child(3) #method
    Click Element    id=address
    Input Text    id=address    ${ip_T2eth1}
    Click Element    id=netmask
    Input Text    id=netmask    31
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    Close All Browsers
    Skip If    not ${HAS_HOSTSHARED}   There are no HOST SHARED for this build
    Skip If    not ${HOST_HAS_ETH1_UP} or not ${HOSTSHARED_HAS_ETH1_UP}    The HOST or HOSTSHARED units has no ETH1 connection UP
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Network::Connections::open tab
    GUI::Basic::Spinner Should Be Invisible
    Click Link    ETH1
    GUI::Basic::Spinner Should Be Invisible
    Click Element    css=.radio:nth-child(2) #method
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Save If Configuration Changed
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Logout and Close Nodegrid

    GUI::Basic::Open And Login Nodegrid    PAGE=${HOMEPAGESHARED}    ALIAS=SHARED
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Network::Connections::open tab
    GUI::Basic::Spinner Should Be Invisible
    Click Link    ETH1
    GUI::Basic::Spinner Should Be Invisible
    Click Element    css=.radio:nth-child(2) #method
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Save If Configuration Changed
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Logout and Close Nodegrid