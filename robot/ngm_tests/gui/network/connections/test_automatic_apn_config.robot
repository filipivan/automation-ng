*** Settings ***
Documentation	Testing the automatic APN Configuration
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
...	EXCLUDEIN5_4

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${STRONG_PASSWORD}	${QA_PASSWORD}
${APN_NAME}		vzwinternet

*** Test Cases ***
Test case to get GSM operator name and interface information
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	${INDEX_NUMBER}	GUI::Tracking::Get Index Number With 5G Sim Card Up Otherwise 4G Up
	${ROW_NUMBER}	Evaluate	${INDEX_NUMBER} + 1
	${GSM_CDC_WDMX}	GUI::Tracking::Get Tracking Wireless Modem Table Cell	ROW=${ROW_NUMBER}	COLUMN=2
	Set Suite Variable	${GSM_CDC_WDMX}

Test case to add a new GSM connection type in Network:Connections and select automatic pre APN config.
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	${TEST_CONNECTION_GSM_NAME}	Set Variable	gsm-${GSM_CDC_WDMX}
	Set Suite Variable	${TEST_CONNECTION_GSM_NAME}
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text  id=connName  ${TEST_CONNECTION_GSM_NAME}
	GUI::Basic::Click Element	//select[@id='connType']
	Select From List By Label	//select[@id='connType']	${GSM_TYPE_GUI}
	GUI::Basic::Click Element	//select[@id='connItfName']
	Select From List By Label	//select[@id='connItfName']	${GSM_CDC_WDMX}
	Run Keyword If	'${NGVERSION}'>='5.6'	Select Radio Button	mobileApnConf	automatic
	...	ELSE	Input Text	id=mobileAPN	${APN_NAME}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep  15s

Test case to check the connection should be up
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab

	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections \n${TABLE_CONTENTS}	INFO	console=yes
	Wait Until Keyword Succeeds	5x	3s		Should Match Regexp	${TABLE_CONTENTS}	${TEST_CONNECTION_GSM_NAME}+\\s+Connected+\\s+Mobile Broadband GSM+\\s+${GSM_CDC_WDMX}+\\s+Up+\\s+\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}(?=\\/\\d{2})

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	${HAS_GSM}	GUI::Tracking::Check If Has GSM Module and Sim Card with Signal
	Set Suite Variable	${HAS_GSM}
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	#	CLI: Setup
	CLI:Open
	SUITE:Change passwords of admin and root to strong passwords
	CLI:Close Connection

SUITE:Teardown
	Close All Browsers
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	GUI::Basic::Open And Login Nodegrid	PASSWORD=${STRONG_PASSWORD}
	SUITE:Delete GSM Connection If exists
	GUI::Basic::Logout And Close Nodegrid
#	CLI: Teardown
	CLI:Open	PASSWORD=${STRONG_PASSWORD}
	SUITE:Change passwords of admin and root to default passwords
	CLI:Close Connection

SUITE:Change passwords of admin and root to strong passwords
	CLI:Change User Password From Shell	root	${STRONG_PASSWORD}	${ROOT_PASSWORD}
	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${STRONG_PASSWORD}	${STRONG_PASSWORD}
	Log	Changed the passwords of admin and root to: ${STRONG_PASSWORD}	INFO	console=yes

SUITE:Change passwords of admin and root to default passwords
	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}	${STRONG_PASSWORD}
	CLI:Change User Password From Shell	root	${ROOT_PASSWORD}	${STRONG_PASSWORD}
	Log	The passwords of admin and root users came back to default: ${DEFAULT_PASSWORD} and ${ROOT_PASSWORD}	INFO	console=yes

SUITE:Delete GSM Connection If exists
	${HAS_GSM_CONNECTION}	GUI::Network::Check If Has Connection	${TEST_CONNECTION_GSM_NAME}
	IF	${HAS_GSM_CONNECTION}
		GUI::Basic::Network::Connections::open tab
		GUI::Basic::Click Element	//tr[@id='${TEST_CONNECTION_GSM_NAME}']//input[@type='checkbox']
		GUI::Basic::Delete
		Page Should Not Contain	${TEST_CONNECTION_GSM_NAME}
	END