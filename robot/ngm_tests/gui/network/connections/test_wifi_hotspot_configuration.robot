*** Settings ***
Documentation	Test configuration in Wifi and Hotspot options under Network:Connections
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SSID_NAME}	testWIFI_automation
${PRE-SHARED_KEY}	.ZPE5ystems!2020
${SERVER}	${RADIUSSERVER3}
${SECRET}	${RADIUSSERVER3_SECRET}
${USERNAME}	${RADIUSSERVER2_USER_TEST1}
${PASSWORD}	${RADIUSSERVER2_USER_TEST1_PASSWORD}
${CLIENT_NAME}	hostshared_client
${SECURITY1}	Disabled

*** Test Cases ***
Test hotspot with security disabled with 2.4Ghz
	[Tags]	EXCLUDEIN5_9
	[Documentation]	Test hotspot could be configured with security as disabled and band as 2.4Ghz
	SUITE:Configure Hotspot	none	2_4ghz

Test hotspot with WPA2 Personal 2.4Ghz
	[Tags]	EXCLUDEIN5_9
	[Documentation]	Test hotspot could be configured with security as wpa2_personal and band as 2.4Ghz
	SUITE:Configure Hotspot	wpa-psk		2_4ghz

Test hotspot with WPA2 Enterprise 2.4Ghz
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_9
	[Documentation]	Test hotspot could be configured with security as wpa2_enterprise and band as 2.4Ghz
	SUITE:Configure Hotspot	wpa-eap		2_4ghz

Test hotspot with WPA3 2.4Ghz
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_9
	[Documentation]	Test hotspot could be configured with security as wpa3_personal and band as 2.4Ghz
	Run Keyword If	'${NGVERSION}'>='5.9'	SUITE:Configure Hotspot	sae		2_4ghz

Test hotspot with security disabled with 5Ghz
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_9
	[Documentation]	Test hotspot could be configured with security as disabled and band as 5Ghz
	Run Keyword If	'${NGVERSION}'>='5.9'	SUITE:Configure Hotspot	none	5ghz

Test hotspot with WPA2 Personal 5Ghz
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_9
	[Documentation]	Test hotspot could be configured with security as wpa2_personal and band as 5Ghz
	Run Keyword If	'${NGVERSION}'>='5.9'	SUITE:Configure Hotspot	wpa-eap		5ghz

Test hotspot with WPA2 Enterprise 5Ghz
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_9
	[Documentation]	Test hotspot could be configured with security as wpa2_enterprise and band as 5Ghz
	Run Keyword If	'${NGVERSION}'>='5.9'	SUITE:Configure Hotspot	wpa-eap		5ghz

Test hotspot with WPA3 5Ghz
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_9
	[Documentation]	Test hotspot could be configured with security as wpa3_personal and band as 5Ghz
	Run Keyword If	'${NGVERSION}'>='5.9'	SUITE:Configure Hotspot	sae		5ghz

Test wifi client with security disabled
	[Documentation]	Test wifi client can be configured with security as disabled
	SUITE:Configure WiFi Client	none
	[Teardown]	SUITE:Delete Wifi Client Connection

Test wifi client with WPA2 Personal
	[Documentation]	Test wifi client can be configured with security as wpa2_personal
	SUITE:Configure WiFi Client	wpa-psk
	[Teardown]	SUITE:Delete Wifi Client Connection

Test wifi client with WPA2 Enterprise
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
	[Documentation]	Test wifi client can be configured with security as wpa2_enterprise
	SUITE:Configure WiFi Client	wpa-eap
	[Teardown]	SUITE:Delete Wifi Client Connection

Test wifi client with WPA3
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	[Documentation]	Test wifi client can be configured with security as wpa3_personal
	Run Keyword If	'${NGVERSION}'>='5.9'	SUITE:Configure WiFi Client	WPA3 Personal
	Run Keyword If	'${NGVERSION}'>='5.9'	SUITE:Delete Wifi Client Connection

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

SUITE:Configure Hotspot
	[Documentation]	Keyword configures the hotspot connection and all its necessary fields according to ${SECURITY} option.
	...	Is also possible to specify the desired Wifi band using ${BAND} argument and the region using ${REGION}.
	[Arguments]	${SECURITY}=none	${BAND}=2_4ghz	${REGION}=00
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//a[contains(text(),'hotspot')]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//*[@id="wifiSsid"]
	Input Text	//*[@id="wifiSsid"]		${SSID_NAME}
	Sleep	5s

	IF	'${SECURITY}' == 'wpa-eap'
		Run Keyword If	'${NGVERSION}'>='5.9'	Select From List By Value	//*[@id="wifiSec"]	${SECURITY}
		Run Keyword If	'${NGVERSION}'<'5.9'	Select Radio button		wifiSec		${SECURITY}
		Wait Until Page Contains Element	//*[@id="wifiSecEapRadiusIP"]
		Page Should Contain Element		//*[@id="wifiSecEapRadiusIP"]
		Click Element	//*[@id="wifiSecEapRadiusIP"]
		Input Text	//*[@id="wifiSecEapRadiusIP"]	${SERVER}
		Click Element	//*[@id="wifiSecEapRadiusSecret"]
		Input Text	//*[@id="wifiSecEapRadiusSecret"]	${SECRET}
	ELSE IF	'${SECURITY}' == 'none'
		Run Keyword If	'${NGVERSION}'>='5.9'	Select From List By Value	//*[@id="wifiSec"]	${SECURITY}
		Sleep	10s
		Run Keyword If	'${NGVERSION}'<'5.9'	Select Radio button		wifiSec		${SECURITY}
	ELSE IF	'${SECURITY}' == 'sae'
		Select From List By Value	//*[@id="wifiSec"]	${SECURITY}
		Wait Until Page Contains Element	//*[@id="wifiSecPsk"]
		Sleep	5s
		Input Text	//*[@id="wifiSecPsk"]	${PRE-SHARED_KEY}
	ELSE
		Run Keyword If	'${NGVERSION}'>='5.9'	Select From List By Value	//*[@id="wifiSec"]	${SECURITY}
		Run Keyword If	'${NGVERSION}'<'5.9'	Select Radio button		wifiSec		${SECURITY}
		Wait Until Page Contains Element	//*[@id="wifiSecPsk"]
		Sleep	5s
		Input Text	//*[@id="wifiSecPsk"]	${PRE-SHARED_KEY}
	END
	Run Keyword If	'${NGVERSION}'>='5.9'	Select From List By Value	//*[@id="wifiRegion"]	${REGION}
	Run Keyword If	'${NGVERSION}'>='5.9'	Select Radio Button	wifiBand	${BAND}
	GUI::Basic::Save

SUITE:Configure WiFi Client
	[Documentation]	Keyword configures the wifi connection and all its necessary fields according to ${SECURITY} option.
	[Arguments]	${SECURITY}=Disabled
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="connName"]	${CLIENT_NAME}
	Select From List By Label	//*[@id="connType"]	WiFi
	Wait Until Page Contains	WiFi SSID
	Input Text	//*[@id="wifiSsid"]	${SSID_NAME}
	Input Text	//*[@id="wifiBssid"]	${CLIENT_NAME}
	Unselect Checkbox	//*[@id="connAuto"]
	IF	'${SECURITY}' == 'wpa-eap'
		Run Keyword If	'${NGVERSION}'>='5.9'	Select From List By Value	//*[@id="wifiSec"]	${SECURITY}
		Run Keyword If	'${NGVERSION}'<'5.9'	Select Radio button		wifiSec		${SECURITY}
		Wait Until Page Contains Element	//*[@id="wifiSecEapIdentity"]
		Page Should Contain Element    //*[@id="wifiSecEapIdentity"]
		Click Element    //*[@id="wifiSecEapIdentity"]
		Input Text	//*[@id="wifiSecEapIdentity"]	${USERNAME}
		Input Text	//*[@id="wifiSecEapPassword"]	${PASSWORD}
	ELSE IF	'${SECURITY}' == 'none'
		Run Keyword If	'${NGVERSION}'>='5.9'	Select From List By Value	//*[@id="wifiSec"]	${SECURITY}
		Run Keyword If	'${NGVERSION}'<'5.9'	Select Radio button		wifiSec		${SECURITY}
	ELSE IF	'${SECURITY}' == 'WPA3 Personal'
		Select From List By Label	//*[@id="wifiSec"]	${SECURITY}
		Wait Until Page Contains Element	//*[@id="wifiSecPsk"]
		Input Text	//*[@id="wifiSecPsk"]	${PRE-SHARED_KEY}
	ELSE
		Run Keyword If	'${NGVERSION}'>='5.9'	Select From List By Value	//*[@id="wifiSec"]	${SECURITY}
		Run Keyword If	'${NGVERSION}'<'5.9'	Select Radio button		wifiSec		${SECURITY}
		Wait Until Page Contains Element	//*[@id="wifiSecPsk"]
		Input Text	//*[@id="wifiSecPsk"]	${PRE-SHARED_KEY}
	END
	GUI::Basic::Save

SUITE:Delete Wifi Client Connection
	Select Checkbox	//*[@id="hostshared_client"]/td[1]/input
	GUI::Basic::Delete