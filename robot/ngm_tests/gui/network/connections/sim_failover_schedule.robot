*** Settings ***
Documentation	Testing SIM Failover for Scheduled Failback
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW	EXCLUDEIN5_8

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${sim_conn}	sim_conn1
${conn_type}	gsm
${conn_intf}	cdc-wdm0
${sim_1}	1
${sim_2}	2
${check_sim_1}	SIM 1
${check_sim_2}	SIM 2
${sim_failed}	1
${check_modem}	False
${date_time}	1****
${failback_hr}	8

*** Test Cases ***
Test Modem support
	GUI::Basic::Network::Wireless Modem::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${check_modem}=	run keyword and return status	Table Should Contain	jquery=#wmodemGlobalTable	Channel-A

Add new connection with primary and secondary SIM
	skip if	not ${check_modem}	Without Wireless Modem
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=connName
	Input Text	id=connName	${sim_conn}
	Select From List By Value	jquery=#connType	${conn_type}
	Select From List By Value	jquery=#connItfName	${conn_intf}
	Click Element	id=mobileDualSim
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Bring up the connection
	skip if	not ${check_modem}	Without Wireless Modem
	Click Element	//*[@id="${sim_conn}"]/td[1]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="nonAccessControls"]/input[3]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Wireless Modem::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Table Should Contain	jquery=#wmodemGlobalTable	Channel-A

Enable SIM Failover with Scheduled Failback and verify Failover
	skip if	not ${check_modem}	Without Wireless Modem
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=failover
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	jquery=#primaryConn	${sim_conn}
	Select From List By Value	jquery=#primaryConnSim	${sim_1}
	Select From List By Value	jquery=#secondaryConn	${sim_conn}
	Select From List By Value	jquery=#secondaryConnSim	${sim_2}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	primarySchedule
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=primaryScheduleCron	 ${date_time}
	Input Text	id=primaryScheduleDuration	 ${failback_hr}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	2m	1s	SUITE:Check Cellular Connection

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Check Cellular Connection
	GUI::Basic::Tracking::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#wmodem > .title_b
	GUI::Basic::Spinner Should Be Invisible
	${sim_failed}=	Get Table Cell	xpath=//*[@id="wmodemTable"]	2	5
	should be equal	${sim_failed}	${check_sim_2}

SUITE:Teardown
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	Run Keyword If	not ${check_modem}	GUI::Basic::Logout and Close Nodegrid
	skip If	not ${check_modem}	Without Wireless Modem
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	primarySchedule
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=failover
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table Containing Value	peerTable	${sim_conn}	False
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Table Should Not Contain	peerTable	${sim_conn}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout and Close Nodegrid