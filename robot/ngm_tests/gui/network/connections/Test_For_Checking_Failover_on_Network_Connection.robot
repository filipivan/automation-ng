*** Settings ***
Documentation	Test for Checking Failover on Network Connection
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	NON-CRITICAL
Default Tags	EXCLUDEIN3_2

*** Variables ***
${TEST_CONNECTION_ETH1}		ETH1
${USERNAME}		${DEFAULT_USERNAME}
${PASSWORD}		${DEFAULT_PASSWORD}
${IP_Address}		1.1.1.1
${tcpdump_cmd_eth1}		tcpdump -i eth1 icmp -c 20
${tcpdump_cmd_eth0}		tcpdump -i eth0 icmp -c 20

*** Test Cases ***
Test Case to Enable Network Failover
	SUITE:TO Enable Network Failover

Test Case to Ping IP from both Interface ETH0 and ETH1
	SUITE:To Ping IP from ETH0 Interface
	page should not contain	100% packet loss,
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible
	SUITE:To Ping IP from ETH1 Interface
	page should not contain	100% packet loss,
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible

Test Case to Open ETH1 Ip and add firewall
	SUITE:To Get ipv4 address of ETH1
	SUITE:To Login device with ETH1 Ip
	SUITE:To Add Firewall
	Wait Until Keyword Succeeds	10x	5s		SUITE:To Track Event List

Test Case to Ping IP from both Interface ETH0 and ETH1 After Config Network Failure
	SUITE:Setup
	SUITE:To Ping IP from ETH0 Interface
	page should contain	100% packet loss,
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible
	SUITE:To Ping IP from ETH1 Interface
	page should not contain	100% packet loss,
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible

Test Case to check Routing table metrics under Tracking::Network::Routing Table
	SUITE:To Check Metrics on Routing Table

Test Case to Check tcpdump after setting ETH0 to block state
	SUITE:Test Case to Enter the Shell
	${OUTPUT}=	GUI:Access::Generic Console Command Output	ping ${IP_Address}
	SUITE:Test Case to Enter the Shell
	${OUTPUT}=	GUI:Access::Generic Console Command Output	${tcpdump_cmd_eth1}
	Sleep	15s
	Should Match Regexp	${OUTPUT}	one.one.one.one.*ICMP.*request.*
	Should Match Regexp	${OUTPUT}	one.one.one.one.*ICMP.*reply.*

Test Case to block ETH1 on firewall and Check the Status
	SUITE:To Get ipv4 address of ETH1
	SUITE:To Login device with ETH1 Ip
	SUITE:To Set the Firewall to ETH1
	SUITE:Test Case to Enter the Shell
	${OUTPUT}=	GUI:Access::Generic Console Command Output	ping ${IP_Address}
	SUITE:Test Case to Enter the Shell
	${OUTPUT}=	GUI:Access::Generic Console Command Output	${tcpdump_cmd_eth0}
	Sleep	15s
	Should Match Regexp	${OUTPUT}	one.one.one.one.*ICMP.*request.*
    Should Match Regexp	${OUTPUT}	one.one.one.one.*ICMP.*reply.*

Test Case to Delete the Firewall and Check the Status
	SUITE:To Get ipv4 address of ETH1
	SUITE:To Login device with ETH1 Ip
	SUITE:To Delete the Firewall
	Wait Until Keyword Succeeds	10x	5s	SUITE:To Track Event List after deleting firewall
	SUITE:To Check Metrics on Routing Table after deleting firewall rule

Test Caase to Check route and ifconfig on Shell mode
	SUITE:Test Case to Enter the Shell
	${OUTPUT}=	GUI:Access::Generic Console Command Output	which route
	Sleep	15s
	Should Match Regexp	${OUTPUT}	.*sbin.*route
	${OUTPUT}=	GUI:Access::Generic Console Command Output	which ifconfig
	Sleep	15s
	Should Match Regexp	${OUTPUT}	.*sbin.*ifconfig

Test Case to Disable Network Failover
	SUITE:TO Disable Network Failover

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	SUITE:To Check that the Device has ETH1 is up

SUITE:To Check that the Device has ETH1 is up
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	${HOST_HAS_ETH1}   GUI::Network::Check If Has Connection  ${TEST_CONNECTION_ETH1}
	Set Suite Variable		${HOST_HAS_ETH1}
	Skip If    not ${HOST_HAS_ETH1}		This automation test environment has no ETH1 connection
	${ETH1_HAS_IP}	Run Keyword And Return Status	GUI::Network::Get Connection Ipv4	${TEST_CONNECTION_ETH1}
	Skip If	not ${ETH1_HAS_IP}	This automation test environment has ETH1 connection but it's Down without IP Address

SUITE:To Get ipv4 address of ETH1
	SUITE:Setup
	${ETH1_IPV4}  GUI::Network::Get Connection Ipv4  ETH1
	Set Suite Variable  ${ETH1_IPV4}

SUITE:TO Enable Network Failover
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	set focus to element		id=failover
	select checkbox		id=failover
	select from list by value		xpath=//select[@id='primaryConn']		ETH0
	select from list by value		xpath=//select[@id='secondaryConn']		ETH1
	click element		xpath=(//input[@id='trigger'])[2]
	input text		xpath=//input[@id='address']		${IP_Address}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:TO Disable Network Failover
	SUITE:Setup
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	set focus to element		id=failover
	unselect checkbox		id=failover
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Ping IP from ETH0 Interface
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	click element	css=.submenu li:nth-child(5) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//a[contains(text(),'Network Tools')]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=.fixed-top
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="ipaddress"]	${IP_Address}
	Select From List By value	//*[@id="interface"]	eth0
	Click Element	xpath=//input[@value="Ping"]
	GUI::Basic::Spinner Should Be Invisible
	sleep	15s
	Wait Until Page Contains	ping statistics

SUITE:To Ping IP from ETH1 Interface
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	click element	css=.submenu li:nth-child(5) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//a[contains(text(),'Network Tools')]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=.fixed-top
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="ipaddress"]	${IP_Address}
	Select From List By value	//*[@id="interface"]	eth1
	Click Element	xpath=//input[@value="Ping"]
	GUI::Basic::Spinner Should Be Invisible
	sleep	15s
	Wait Until Page Contains	ping statistics

SUITE:To Login device with ETH1 Ip
	open browser		https://${ETH1_IPV4}
	Wait Until Element Is Visible	password
	Input Text	css=#username	${USERNAME}
	Input Text	password	${PASSWORD}
	Click Element	id=login-btn
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Add Firewall
	GUI::Basic::Security::Firewall::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	click element		xpath=//a[contains(text(),'INPUT')]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	select from list by value		xpath=//select[@id='target']		DROP
	input text		xpath=//input[@id='source']		${IP_Address}
	select from list by value		xpath=//select[@id='inIface']		eth0
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Set the Firewall to ETH1
	GUI::Basic::Security::Firewall::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	click element		xpath=//a[contains(text(),'INPUT')]
	GUI::Basic::Spinner Should Be Invisible
	select checkbox		xpath=(//input[@type='checkbox'])[3]
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	wait until page contains element		xpath=//select[@id='inIface']
	select from list by value		xpath=//select[@id='inIface']		eth1
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Delete the Firewall
	GUI::Basic::Security::Firewall::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	click element		xpath=//a[contains(text(),'INPUT')]
	GUI::Basic::Spinner Should Be Invisible
	select checkbox		xpath=(//input[@type='checkbox'])[3]
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Track Event List
	GUI::Tracking::Open Event List Tab
	GUI::Basic::Spinner Should Be Invisible
	click element		xpath=//span[contains(.,'Events')]
	wait until page contains element		id=filter_field
	input text	id=filter_field	144
	click element	id=button_search_date
	page should contain		144
	wait until page contains	Nodegrid Network Failover Executed

SUITE:To Track Event List after deleting firewall
	GUI::Tracking::Open Event List Tab
	GUI::Basic::Spinner Should Be Invisible
	click element		xpath=//span[contains(.,'Events')]
	wait until page contains element		id=filter_field
	input text	id=filter_field	145
	click element	id=button_search_date
	wait until page contains		145
	wait until page contains	Nodegrid Network Failover Executed

SUITE:To Check Metrics on Routing Table
	GUI::Basic::Spinner Should Be Invisible
   	click element		css=#menu_main_tracking img
   	GUI::Basic::Spinner Should Be Invisible
   	click element		xpath=(//a[contains(text(),'Network')])[2]
   	GUI::Basic::Spinner Should Be Invisible
   	Click Element		//*[@id="trackingRouteTable"]
   	GUI::Basic::Spinner Should Be Invisible
   	Page Should Contain Element     //*[@id="routingTable"]
   	GUI::Basic::Spinner Should Be Invisible
   	${TABLE_CONTENTS}=		Get Text		routingTable
   	Log		\nTable contents in Tracking::Network::Routing Table \n${TABLE_CONTENTS}	INFO	console=yes
   	${TABLE_CONTENTS}=		Convert To String		${TABLE_CONTENTS}
   	@{GET_LINES}=		Split To Lines		${TABLE_CONTENTS}
   	${HAS_ETH1_60}  Set Variable    ${FALSE}
   	${HAS_ETH0_90}  Set Variable    ${FALSE}
   	FOR		${INDEX}		${LINE}		IN ENUMERATE		@{GET_LINES}
        ${STATUS}=		Run Keyword And Return Status		Should Match Regexp		${LINE}		60\\s+eth1\\s+all
        IF  '${STATUS}' == '${TRUE}'
            ${HAS_ETH1_60}=		Set Variable		${TRUE}
        END
        ${STATUS}=		Run Keyword And Return Status		Should Match Regexp		${LINE}		90\\s+eth0\\s+all
        IF  '${HAS_ETH1_60}' == '${TRUE}' and '${STATUS}' == '${TRUE}'
            ${HAS_ETH0_90}=		Set Variable		${TRUE}
            Exit For Loop
        END
        IF  '${HAS_ETH0_90}' == '${TRUE}' and '${HAS_ETH1_60}' == '${FALSE}'
            Fail    The metrics are wrong need to check Failover feature.
            Exit For Loop
        END
    END
    ${METRIC_IS_OK}=    Run Keyword If  ${HAS_ETH0_90}  Set Variable    ${TRUE}
    ...		ELSE		Set Variable		${FALSE}
    Should Be True		${METRIC_IS_OK}

SUITE:To Check Metrics on Routing Table after deleting firewall rule
	GUI::Basic::Spinner Should Be Invisible
   	click element		css=#menu_main_tracking img
   	GUI::Basic::Spinner Should Be Invisible
   	click element		xpath=(//a[contains(text(),'Network')])[2]
   	GUI::Basic::Spinner Should Be Invisible
   	Click Element		//*[@id="trackingRouteTable"]
   	GUI::Basic::Spinner Should Be Invisible
   	Page Should Contain Element     //*[@id="routingTable"]
   	GUI::Basic::Spinner Should Be Invisible
   	${TABLE_CONTENTS}=		Get Text		routingTable
   	Log		\nTable contents in Tracking::Network::Routing Table \n${TABLE_CONTENTS}	INFO	console=yes
   	${TABLE_CONTENTS}=		Convert To String		${TABLE_CONTENTS}
   	@{GET_LINES}=		Split To Lines		${TABLE_CONTENTS}
   	${HAS_ETH0_90}  Set Variable    ${FALSE}
   	${HAS_ETH1_100}  Set Variable    ${FALSE}
   	FOR		${INDEX}		${LINE}		IN ENUMERATE		@{GET_LINES}
        ${STATUS}=		Run Keyword And Return Status		Should Match Regexp		${LINE}		90\\s+eth0\\s+all
        IF  '${STATUS}' == '${TRUE}'
            ${HAS_ETH0_90}=		Set Variable		${TRUE}
        END
        ${STATUS}=		Run Keyword And Return Status		Should Match Regexp		${LINE}		100\\s+eth1\\s+all
        IF  '${HAS_ETH0_90}' == '${TRUE}' and '${STATUS}' == '${TRUE}'
            ${HAS_ETH1_100}=		Set Variable		${TRUE}
            Exit For Loop
        END
        IF  '${HAS_ETH0_90}' == '${TRUE}' and '${HAS_ETH1_100}' == '${TRUE}'
            Fail    The metrics are wrong need to check Failover feature.
            Exit For Loop
        END
    END
    ${METRIC_IS_OK}=    Run Keyword If  ${HAS_ETH0_90}  Set Variable    ${TRUE}
    ...		ELSE		Set Variable		${FALSE}
    Should Be True		${METRIC_IS_OK}

SUITE:Test Case to Enter the Shell
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="|nodegrid"]/div[1]/div[2]/a
	Sleep	10s
	Run Keyword If  '${NGVERSION}'<='5.4'	Switch Window	nodegrid
	Run Keyword If  '${NGVERSION}'>'5.4'	Switch Window	nodegrid - Console
	Sleep	10s
	Select Frame	xpath=//*[@id='termwindow']
	Press Keys	None	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output	shell sudo su -
	Sleep	15s
	Should Contain	${OUTPUT}	root@nodegrid:~#

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	Close All Browsers