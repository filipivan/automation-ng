*** Settings ***
Documentation	Testing Multiple loopback address.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	EXCLUDEIN4_2	NON-CRITICAL	NEED-REVIEW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${LOOP1}	loopback1
${LOOP2}	loopback2
${LOOP3}	loopback3
${LOOP1ADD}	1.1.1.1
${LOOP2ADD}	2.1.1.1
${LOOP3ADD}	3.1.1.1
${LOOP1IPV6ADD}	1:1::1
${LOOP2IPV6ADD}	2:1::2
${LOOP3IPV6ADD}	3:3::3
${MASK}	24
${IPV6MASK}	96

*** Test Cases ***
Configure 1st loopback address
	Input Text	jquery=#connName	${LOOP1}
	Select From List By Value	jquery=#connType	dummy
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text  id=address  ${LOOP1ADD}
	Input Text	//*[@id="netmask"]	${MASK}
	Select Checkbox	id=method6Loopback
	Input Text	//*[@id="address6"]	${LOOP1IPV6ADD}
	Input Text	//*[@id="netmask6"]	${IPV6MASK}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Verify the configured 1st Loopback address's
	Page Should Contain	${LOOP1}
	Page Should Contain	${LOOP1ADD}
	Page Should Contain	${LOOP1IPV6ADD}

Configure 2nd Loopback address
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	jquery=#connName	${LOOP2}
	Select From List By Value	jquery=#connType	dummy
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="address"]	${LOOP2ADD}
	Input Text	//*[@id="netmask"]	${MASK}
	Select Checkbox	id=method6Loopback
	Input Text	//*[@id="address6"]	${LOOP2IPV6ADD}
	Input Text	//*[@id="netmask6"]	${IPV6MASK}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Verify the configured 2nd Loopback address's
	Page Should Contain	${LOOP1}
	Page Should Contain	${LOOP1ADD}
	Page Should Contain	${LOOP1IPV6ADD}
	Page Should Contain	${LOOP2}
	Page Should Contain	${LOOP2ADD}
	Page Should Contain	${LOOP2IPV6ADD}

Configure 3rd Loopback address
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	jquery=#connName	${LOOP3}
	Select From List By Value	jquery=#connType	dummy
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="address"]	${LOOP3ADD}
	Input Text	//*[@id="netmask"]	${MASK}
	Select Checkbox	id=method6Loopback
	Input Text	//*[@id="address6"]	${LOOP3IPV6ADD}
	Input Text	//*[@id="netmask6"]	${IPV6MASK}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Verify the configured 3rd Loopback address's
	Page Should Contain	${LOOP1}
	Page Should Contain	${LOOP1ADD}
	Page Should Contain	${LOOP1IPV6ADD}
	Page Should Contain	${LOOP2}
	Page Should Contain	${LOOP2ADD}
	Page Should Contain	${LOOP2IPV6ADD}
	Page Should Contain	${LOOP3}
	Page Should Contain	${LOOP3ADD}
	Page Should Contain	${LOOP3IPV6ADD}

Delete 2nd Loopback address
	Select Checkbox	//*[@id="${LOOP2}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

Verify Deleted 2nd Loopback address's
	Page Should Contain	${LOOP1}
	Page Should Contain	${LOOP1ADD}
	Page Should Contain	${LOOP1IPV6ADD}
	Page Should Contain	${LOOP3}
	Page Should Contain	${LOOP3ADD}
	Page Should Contain	${LOOP3IPV6ADD}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Select Checkbox	//*[@id="${LOOP1}"]/td[1]/input
	Select Checkbox	//*[@id="${LOOP3}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

