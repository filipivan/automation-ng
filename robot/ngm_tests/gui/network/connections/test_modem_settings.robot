*** Settings ***
Documentation	Testing the Analog Modem options for Network:Connections
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CONNECTION}       Test1
${TYPE}     generic

*** Test Cases ***
Select Modem Type and Name it
	Input Text	//*[@id="connName"]     ${CONNECTION}
	Select From List By Value	jquery=#connType	${TYPE}
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	jquery=#pppTitle

Check Status Dropdown
	@{CHK}=	Create List	Enabled	Disabled
	@{STAT}=	Get List Items	jquery=#pppStatus
	Should Be Equal	${CHK}	${STAT}

Test Device Name
	${VALIDS}=	Create List	te.st
	${INVALIDS}=	Create List	..test	.test	3test	te'st	te[]st	te,st
	GUI::Basic::Input Text::Validation	pppModem	${VALIDS}	${INVALIDS}

Check Speed Dropdown
    SUITE:Setup
	@{CHK}=	Create List	9600	19200	38400	57600	115200
	@{SPEED}=	Get List Items	jquery=#pppSpeed
	Should Be Equal	${CHK}	${SPEED}

Test Dial-Out Phone Number
	SUITE:Setup
	Wait Until Page Contains Element       pppPhoneNumber
	${VALIDS}=	Create List	9999999999
	${INVALIDS}=	Create List	555-555-5555	555 555 5555	phoneNumber	(432)341-1245	+5524521245
	GUI::Basic::Input Text::Validation	pppPhoneNumber	${VALIDS}	${INVALIDS}

Test Chat Presence
    Wait Until Page Contains Element        jquery=#pppInitChat
	Page Should Contain Element     jquery=#pppInitChat

Test Timeout
	SUITE:Setup
	Wait Until Page Contains Element        pppIdleTmo
	${VALIDS}=	Create List	12345678
	${INVALIDS}=	Create List	555-555-5555	string	(432)341-1245	+5524521245	123456789012345678
	GUI::Basic::Input Text::Validation	pppIdleTmo	${VALIDS}	${INVALIDS}

*** Keywords ***
SUITE:Setup
	${CONNECTIONS}=	Create List	${CONNECTION}
	Set Suite Variable	${CONNECTIONS}	${CONNECTIONS}
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table If Exists Without Alert        jquery=table       ${CONNECTIONS}
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="connName"]     ${CONNECTION}
	Select From List By Value	jquery=#connType	${TYPE}

SUITE:Teardown
	Run Keyword And Continue On Failure	GUI::Basic::Button::Cancel
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table If Exists Without Alert     jquery=table       ${CONNECTIONS}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
