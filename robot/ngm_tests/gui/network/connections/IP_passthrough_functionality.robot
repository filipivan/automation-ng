*** Settings ***
Documentation	Automated test for IP Passthrough
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${DELAY}	5s
${Device_name}	nodegrid
${root_login}	shell sudo su -
${gateway_ip}	${GATEWAY}
${CELLULAR_CONNECTION}	cellular
${CONNECTION_TYPE}	Mobile Broadband GSM
${ping_cmd}	ping -I wwan0 8.8.8.8
${Tx_msg}	64 bytes from 8.8.8.8:
${INTERFACE}	cdc-wdm0
${INTERFACE_1}	BACKPLANE0
${APN}	i2gold

*** Test Cases ***
Test case to add cellular connection and enable IP Passthrough
	SUITE:Add Connetion
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Teardown

Test case to ping from nodegrid to outside of the network if it was in IP passthrough mode
	SUITE:Add Connetion
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login	${NGVERSION}	${USERNAME}	${QA_PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::open tab
	GUI::Basic::Spinner Should Be Invisible
	${ret}=	Get value	id=hostname
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="peer_header"]//a[text()="Console"]
	sleep	5
	Switch Window	title=${ret}
	Wait Until Page Contains Element	//iframe[@id='termwindow']
	Select Frame	//iframe[@id='termwindow']
	Sleep	10
	Press Keys	//body	RETURN
	Press Keys	//body	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output	${root_login}
	${OUTPUT}=	GUI:Access::Generic Console Command Output	${ping_cmd}
	Press Keys	//body	CTRL+C
	sleep	5s
	should contain	${OUTPUT}	${Tx_msg}
	Unselect Frame
	Press Keys	//body	cw
	Close all browsers

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Add Connetion
	SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::open tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="ipv4-ipforward"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Local Accounts::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Link	//*[@id="admin"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="uPasswd"]
	Input Text	//*[@id="uPasswd"]	${QA_PASSWORD}
	Click Element	//*[@id="cPasswd"]
	Input Text	//*[@id="cPasswd"]	${QA_PASSWORD}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login	${NGVERSION}	${USERNAME}	${QA_PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=connName
	Input Text	//*[@id="connName"]	${CELLULAR_CONNECTION}
	Click Element	id=connType
	Select From List By Label	//*[@id="connType"]	${CONNECTION_TYPE}
	Click Element	id=connIeth
	Select From List By Label	id=connIeth	${INTERFACE}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Link	 ${CELLULAR_CONNECTION}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="mobileAPN"]
	Clear Element Text	//*[@id="mobileAPN"]
	Input Text	//*[@id="mobileAPN"]	${APN}
	Select Checkbox	//*[@id="ipPass"]
	Select From List By Label	//*[@id="ipPassLanConn"]	${INTERFACE_1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="cellular"]/td[1]/input
	Click Button	//*[@id="nonAccessControls"]/input[3]

SUITE:Teardown
	GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login	${NGVERSION}	${USERNAME}	${QA_PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::open tab
	GUI::Basic::Spinner Should Be Invisible
	Unselect Checkbox	//*[@id="ipv4-ipforward"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login	${NGVERSION}	${USERNAME}	${QA_PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	css=#cellular input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login	${NGVERSION}	${USERNAME}	${QA_PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Local Accounts::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Link	//*[@id="admin"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="uPasswd"]
	Input Text	//*[@id="uPasswd"]	${DEFAULT_PASSWORD}
	Click Element	//*[@id="cPasswd"]
	Input Text	//*[@id="cPasswd"]	${DEFAULT_PASSWORD}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

GUI:Access::Generic Console Command Output
	[Arguments]	${COMMAND}	${CONTAINS_HELP}=no
	[Documentation]	Input the given command into the ttyd terminal and retrieve the output after prompt lines
	...	== REQUIREMENTS ==
	...	The console type needs to be one of those:
	...	-	CLI -> ends with "]#"
	...	-	Shell -> ends with "$"
	...	-	Root -> ends with "#"
	...	== ARGUMENTS ==
	...	-	COMMAND = Command to be executed
	...	-	CONTAINS_HELP = [yes/no] Console contains "[Enter '^Ec?' for help]" message
	${CHECK_TAB}=	Run Keyword And Return Status	Should Not Contain	${COMMAND}	TAB
	Run Keyword If	${CHECK_TAB}	Press Keys	//body	${COMMAND}	RETURN
	...	ELSE	Press Keys	//body	${COMMAND}
	Sleep	1

	Wait Until Keyword Succeeds	1m	1s	Execute JavaScript	term.selectAll();
	${CONSOLE_CONTENT}=	Wait Until Keyword Succeeds	1m	1s	Execute Javascript	return term.getSelection().trim();
	Should Not Contain	${CONSOLE_CONTENT}	[error.connection.failure] Could not establish a connection to device
	Run Keyword If	"${CONTAINS_HELP}" == "yes"	Should Contain	${CONSOLE_CONTENT}	[Enter '^Ec?' for help]
	${OUTPUT}=	Output From Last Command	${CONSOLE_CONTENT}
	[Return]	${OUTPUT}

