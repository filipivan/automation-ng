*** Settings ***
Documentation	Testing Cellular Failover Up Down
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${GSM_CONNECTION_NAME}	${GSM_CELLULAR_CONNECTION}
${TYPE}	${GSM_TYPE_GUI}
${REACHABLE_IP}	8.8.8.8
${STRONG_PASSWORD}	${QA_PASSWORD}
${TEST_CONNECTION_ETH0}	ETH0
${TEST_CONNECTION_ETH1}	ETH1

*** Test Cases ***
Test case to get GSM operator name and interface information
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	${INDEX_NUMBER}	GUI::Tracking::Get Index Number With 5G Sim Card Up Otherwise 4G Up
	${ROW_NUMBER}	Evaluate	${INDEX_NUMBER} + 1
	${GSM_CDC_WDMX}	GUI::Tracking::Get Tracking Wireless Modem Table Cell	ROW=${ROW_NUMBER}	COLUMN=2
	Set Suite Variable	${GSM_CDC_WDMX}
	${GSM_OPERATOR_NAME}	GUI::Tracking::Get Tracking Wireless Modem Table Cell	ROW=${ROW_NUMBER}	COLUMN=7
	Set Suite Variable	${GSM_OPERATOR_NAME}
	${ROOT_GSM_INTERFACE}	GUI::Tracking::Rewrite to root GSM interface format	${GSM_CDC_WDMX}
	Set Suite Variable	${ROOT_GSM_INTERFACE}
	[Teardown]	GUI::Basic::Cancel or return if exist

Test case to add connection with primary SIM and validate secondary one to check cellular UP/Down
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	SUITE:Add New Connection With Primary And Secondary SIM
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Bring Up The Connection
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Bring Down The Connection
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Bring Up The Connection
	[Teardown]	GUI::Basic::Cancel or return if exist

Test case to setup the cellular failover 
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	${HOST_HAS_ETH1}	GUI::Network::Check If Has Connection	${TEST_CONNECTION_ETH1}
	Set Suite Variable	${HOST_HAS_ETH1}
	Skip If	not ${HOST_HAS_ETH1}	This automation test environment has no ETH1 connection
	${ETH1_HAS_IP}	Run Keyword And Return Status	GUI::Network::Get Connection Ipv4	${TEST_CONNECTION_ETH1}
	Set Suite Variable	${ETH1_HAS_IP}
	Skip If	not ${ETH1_HAS_IP}	This automation test environment has ETH1 connection but it's Down without IP Address
#	${ETH1_IP}	GUI::Network::Get Connection Ipv4	${TEST_CONNECTION_ETH1}
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Execute Javascript	window.document.getElementById("failover").scrollIntoView(true);
	Select Checkbox	//input[@id='failover']
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=#primaryConn
	Select From List By Label	jquery=#primaryConn	${TEST_CONNECTION_ETH1}
	Click Element	jquery=#secondaryConn
	Select From List By Label	jquery=#secondaryConn	${GSM_CONNECTION_NAME}
	Click Element	jquery=#secondaryConnSim
	Select From List By Value	jquery=#secondaryConnSim	1
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Select Radio Button	trigger	ipaddress
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=address	${REACHABLE_IP}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Link	${GSM_CONNECTION_NAME}
	Wait Until Keyword Succeeds	2m	1s	SUITE:Check Cellular Connection
	[Teardown]	Run Keyword And Ignore Error	GUI::Basic::Click Element Accept Alert	//li[@role='presentation']//a[contains(text(),'Connections')]	WAIT_SPINNER=${TRUE}	SPINNER_TIMEOUT=1m

Test case to turn down ETH1 and check events 144 and 145
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	Skip If	not ${HOST_HAS_ETH1}	This automation test environment has no ETH1 connection
	Skip If	not ${ETH1_HAS_IP}	This automation test environment has ETH1 connection but it's Down without IP Address
	Wait Until Keyword Succeeds	3x	3s	SUITE:Reset counters in event list
	Wait Until Keyword Succeeds	6x	3s	SUITE:Bring Connection Down and check if it has not IPv4	${TEST_CONNECTION_ETH1}
	Wait Until Keyword Succeeds	12x	3s	SUITE:Check for an increase in event counters 144 in the event list
	Wait Until Keyword Succeeds	6x	3s	SUITE:Bring Connection Up and check if it got IPv4	${TEST_CONNECTION_ETH1}
	Wait Until Keyword Succeeds	12x	3s	SUITE:Check for an increase in event counters 145 in the event list

Test case to disable failover and delete GSM connection
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Execute Javascript	window.document.getElementById("failover").scrollIntoView(true);
	Unselect Checkbox	//input[@id='failover']
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Delete Rows In Table Containing Value	peerTable	${GSM_CONNECTION_NAME}	False
	GUI::Basic::Table Should Not Contain	peerTable	${GSM_CONNECTION_NAME}
	[Teardown]	GUI::Basic::Cancel or return if exist

*** Keywords ***
SUITE:Setup
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	${HAS_GSM}	GUI::Tracking::Check If Has GSM Module and Sim Card with Signal
	Set Suite Variable	${HAS_GSM}
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	#	CLI: Change password
	CLI:Open
	SUITE:Change passwords of admin and root to strong passwords
	CLI:Close Connection

SUITE:Teardown
	Run Keyword If	not ${HAS_GSM}	Close All Browsers
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
#	GUI: Teardown
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid	PASSWORD=${STRONG_PASSWORD}
	GUI::Basic::Logout and Close Nodegrid
#	CLI: Teardown
	CLI:Open	PASSWORD=${STRONG_PASSWORD}
	SUITE:Change passwords of admin and root to default passwords
	CLI:Close Connection

SUITE:Add New Connection With Primary And Secondary SIM
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//input[@id='connName']
	Input Text	//input[@id='connName']	${GSM_CONNECTION_NAME}
	Click Element	//select[@id='connType']
	Select From List By Label	//select[@id='connType']	${TYPE}
	Run Keyword If	'${NGVERSION}' == '4.2'	Select From List By Label	//select[@id='connIeth']	${GSM_CDC_WDMX}
	Run Keyword If	'${NGVERSION}' >= '5.0'	Select From List By Label	//select[@id='connItfName']	${GSM_CDC_WDMX}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=mobileAPN
	Input Text	//*[@id="mobileAPN"]	${GSM_WMODEM_CARRIERS_APN["${GSM_OPERATOR_NAME}"]}
	Log	\nGSM_WMODEM_CARRIERS_APN.keys(${GSM_OPERATOR_NAME}): \n${GSM_WMODEM_CARRIERS_APN["${GSM_OPERATOR_NAME}"]}	INFO	console=yes
	GUI::Basic::Select Checkbox	//input[@id='dataUsageMonitor1']
	GUI::Basic::Input Text	//input[@id='dataUsageLimit1']	1
	GUI::Basic::Input Text	//input[@id='dataUsageWarning1']	80
	Execute Javascript	window.document.getElementById("mobileDualSim").scrollIntoView(true);
	IF	${IS_4G_MODEM}
		GUI::Basic::Click Element	//input[@id='mobileDualSim']
	ELSE
	Log	\n5G modem on NSR has only SIM1, basically this option will be available for 4G or in a HiveSR with 5G or 4G then.\n	INFO	console=yes
	END
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Table Should Contain	jquery=#peerTable	${GSM_CONNECTION_NAME}
	GUI::Basic::Spinner Should Be Invisible

SUITE:Check Cellular Connection
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network :: Connections: \n${TABLE_CONTENTS}	INFO	console=yes
	${STATUS}	Run Keyword And Return Status	Should Match Regexp	${TABLE_CONTENTS}	${GSM_CONNECTION_NAME}+.*Up

SUITE:Bring Up The Connection
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="${GSM_CONNECTION_NAME}"]/td[1]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="nonAccessControls"]/input[3]
	GUI::Basic::Spinner Should Be Invisible	SPINNER_TIMEOUT=2m
	SUITE:Check Cellular Connection

SUITE:Bring Down The Connection
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="${GSM_CONNECTION_NAME}"]/td[1]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="nonAccessControls"]/input[4]
	GUI::Basic::Spinner Should Be Invisible
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network :: Connections: \n${TABLE_CONTENTS}	INFO	console=yes
	${STATUS}	Run Keyword And Return Status	Should Match Regexp	${TABLE_CONTENTS}	${GSM_CONNECTION_NAME}+.*Down

SUITE:Check if has wireless modem support
	${HAS_WMODEM_SUPPORT}=	CLI:Has Wireless Modem Support
	Set Suite Variable	${HAS_WMODEM_SUPPORT}
	Log	Has Wireless Modem Support: ${HAS_WMODEM_SUPPORT}	INFO	console=yes

SUITE:Check if has wireless modem with sim card
	${HAS_WMODEM}	${MODEM_INFO}=	CLI:Get Wireless Modems SIM Card Info	0
	Set Suite Variable	${HAS_WMODEM}
	Log	Has Wireless Modem: ${HAS_WMODEM}	INFO	console=yes
	Set Suite Variable	${MODEM_INFO}
	Log	Modem Info: ${MODEM_INFO}	INFO	console=yes

SUITE:Change passwords of admin and root to strong passwords
	CLI:Change User Password From Shell	root	${STRONG_PASSWORD}	${ROOT_PASSWORD}
	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${STRONG_PASSWORD}	${STRONG_PASSWORD}
	Log	Changed the passwords of admin and root to: ${STRONG_PASSWORD}	INFO	console=yes

SUITE:Change passwords of admin and root to default passwords
	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}	${STRONG_PASSWORD}
	CLI:Change User Password From Shell	root	${ROOT_PASSWORD}	${STRONG_PASSWORD}
	Log	The passwords of admin and root users came back to default: ${DEFAULT_PASSWORD} and ${ROOT_PASSWORD}	INFO	console=yes

SUITE:Check for an increase in event counters 144 in the event list
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Tracking::Event List::Open Tab
	${TABLE_CONTENTS}=	Get Text	eventListTable
	Log	\nTable contents in Tracking::Event List::Statistics: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	144+.*[1-9]\\d*\\s+\\w+\\s+(\\w+\\s+Event|Event)

SUITE:Check for an increase in event counters 145 in the event list
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Tracking::Event List::Open Tab
	${TABLE_CONTENTS}=	Get Text	eventListTable
	Log	\nTable contents in Tracking::Event List::Statistics: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	145+.*[1-9]\\d*\\s+\\w+\\s+(\\w+\\s+Event|Event)

SUITE:Bring Connection Down and check if it has not IPv4
	[Arguments]	${TEST_CONNECTION}
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Click Element	//tr[@id='${TEST_CONNECTION}']//input[@type='checkbox']
	GUI::Basic::Click Element	//input[@value='Down Connection']
	GUI::Basic::Network::Connections::Open Tab
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Not Match Regexp	${TABLE_CONTENTS}	${TEST_CONNECTION}+.*(?:[0-9]{1,3}\\.){3}[0-9]{1,3}

SUITE:Bring Connection Up and check if it got IPv4
	[Arguments]	${TEST_CONNECTION}
	GUI::Basic::Network::Connections::Open Tab
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections: \n${TABLE_CONTENTS}	INFO	console=yes
	${CONNECTION_HAS_IPV4}	Run Keyword And Return Status	Should Match Regexp	${TABLE_CONTENTS}	${TEST_CONNECTION}+.*(?:[0-9]{1,3}\\.){3}[0-9]{1,3}
	Run Keyword If	${CONNECTION_HAS_IPV4}	Return From Keyword
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Click Element	//tr[@id='${TEST_CONNECTION}']//input[@type='checkbox']
	GUI::Basic::Click Element	//input[@value='Up Connection']

SUITE:Reset counters in event list
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Tracking::Event List::Open Tab
	Select Checkbox	//*[@id="thead"]/tr/th[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Enabled	//*[@id="nonAccessControls"]/input	error=Cannot reset counters because Reset Counters button is disable/not found
	Click Element	//*[@id="nonAccessControls"]/input
	GUI::Basic::Spinner Should Be Invisible
	${TABLE_CONTENTS}=	Get Text	eventListTable
	Log	\nTable contents in Tracking::Event List::Statistics: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Not Match Regexp	${TABLE_CONTENTS}	...+.*[1-9]\\d*\\s+\\w+\\s+(\\w+\\s+Event|Event)