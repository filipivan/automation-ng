*** Settings ***
Documentation	Automated test for sim dataplan information
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${DELAY}	5s
${Device_name}	nodegrid
${root_login}	shell sudo su -
${gateway_ip}	${GATEWAY}
${CELLULAR_CONNECTION}	cellular
${CONNECTION_TYPE}	Mobile Broadband GSM
${Data}	17.0GB
${slot}	Channel-A
${INTERFACE}	cdc-wdm0
${curl_cmd}	curl -k --interface wwan0	https://speed.hetzner.de/100MB.bin --output ./test.bin

*** Test Cases ***
Test case to add cellular connection
	SUITE:Add Connetion
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Teardown

Test case to verify data into tracking
	SUITE:Add Connetion
	GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login	${NGVERSION}	${USERNAME}	${QA_PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Tracking::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="wmodem"]/span
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="${slot}"]
	GUI::Basic::Spinner Should Be Invisible
	sleep	${DELAY}
	Page Should Contain	${Data}
	SUITE:Teardown

Test case to push data on sim and verify in tracking
	SUITE:Add Connetion
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login	${NGVERSION}	${USERNAME}	${QA_PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${ret}=	Get value	id=hostname
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="peer_header"]//a[text()="Console"]
	sleep	5
	Switch Window	title=${ret}
	Wait Until Page Contains Element	//iframe[@id='termwindow']
	Select Frame	//iframe[@id='termwindow']
	Sleep	10
	Press Keys	//body	RETURN
	Press Keys	//body	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output	${root_login}
	${OUTPUT}=	GUI:Access::Generic Console Command Output	${curl_cmd}
	sleep	240
	Unselect Frame
	Close all browsers
	GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login	${NGVERSION}	${USERNAME}	${QA_PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Tracking::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="wmodem"]/span
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="Channel-A"]
	GUI::Basic::Spinner Should Be Invisible
	sleep	${DELAY}
	Page Should Contain	${Data}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Add Connetion
	SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Local Accounts::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Link	//*[@id="admin"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="uPasswd"]
	Input Text	//*[@id="uPasswd"]	${QA_PASSWORD}
	Click Element	//*[@id="cPasswd"]
	Input Text	//*[@id="cPasswd"]	${QA_PASSWORD}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login	${NGVERSION}	${USERNAME}	${QA_PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should contain Element	//*[@id="ETH0"]
	Click Link	ETH0
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=.radio:nth-child(3) #method
	Click Element	id=address
	Input Text	id=address	${HOST}
	Click Element	id=netmask
	Input Text	id=netmask	24
	Click Element	//*[@id="gateway"]
	Input Text	//*[@id="gateway"]	${gateway_ip}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=connName
	Input Text	//*[@id="connName"]	${CELLULAR_CONNECTION}
	Click Element	id=connType
	Select From List By Label	//*[@id="connType"]	${CONNECTION_TYPE}
	Click Element	id=connIeth
	Select From List By Label	id=connIeth	${INTERFACE}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Link	${CELLULAR_CONNECTION}
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	id=dataUsageMonitor1
	Click Element	id=dataUsageLimit1
	Input Text	id=dataUsageLimit1	17
	Click Element	id=dataUsageWarning1
	Input text	id=dataUsageWarning1	25
	sleep	${DELAY}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="cellular"]/td[1]/input
	Click Button	//*[@id="nonAccessControls"]/input[3]

SUITE:Teardown
	GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login	${NGVERSION}	${USERNAME}	${QA_PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should contain Element	//*[@id="ETH0"]
	Click Link	ETH0
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=.radio:nth-child(2) #method
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	css=#cellular input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login	${NGVERSION}	${USERNAME}	${QA_PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Local Accounts::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Link	//*[@id="admin"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="uPasswd"]
	Input Text	//*[@id="uPasswd"]	${DEFAULT_PASSWORD}
	Click Element	//*[@id="cPasswd"]
	Input Text	//*[@id="cPasswd"]	${DEFAULT_PASSWORD}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

GUI:Access::Generic Console Command Output
	[Arguments]	${COMMAND}	${CONTAINS_HELP}=no
	[Documentation]	Input the given command into the ttyd terminal and retrieve the output after prompt lines
	...	== REQUIREMENTS ==
	...	The console type needs to be one of those:
	...	-	CLI -> ends with "]#"
	...	-	Shell -> ends with "$"
	...	-	Root -> ends with "#"
	...	== ARGUMENTS ==
	...	-	COMMAND = Command to be executed
	...	-	CONTAINS_HELP = [yes/no] Console contains "[Enter '^Ec?' for help]" message
	${CHECK_TAB}=	Run Keyword And Return Status	Should Not Contain	${COMMAND}	TAB
	Run Keyword If	${CHECK_TAB}	Press Keys	//body	${COMMAND}	RETURN
	...	ELSE	Press Keys	//body	${COMMAND}
	Sleep	1

	Wait Until Keyword Succeeds	1m	1s	Execute JavaScript	term.selectAll();
	${CONSOLE_CONTENT}=	Wait Until Keyword Succeeds	1m	1s	Execute Javascript	return term.getSelection().trim();
	Should Not Contain	${CONSOLE_CONTENT}	[error.connection.failure] Could not establish a connection to device
	Run Keyword If	"${CONTAINS_HELP}" == "yes"	Should Contain	${CONSOLE_CONTENT}	[Enter '^Ec?' for help]
	${OUTPUT}=	Output From Last Command	${CONSOLE_CONTENT}
	[Return]	${OUTPUT}

