*** Settings ***
Documentation	SIM MTU Test
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW	EXCLUDEIN5_8

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${sim_conn}	sim_conn1
${conn_type}	gsm
${conn_intf}	cdc-wdm0
${sim_1}	1
${sim_2}	2
${check_sim_1}	SIM 1
${check_sim_2}	SIM 2
${sim_failed}	1
${check_modem}	False
${date_time}	1****
${failback_hr}	8

*** Test Cases ***
Add new connection with primary SIM
	Skip If	${check_modem}	!= True
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=connName
	Input Text	id=connName	${sim_conn}
	Select From List By Value	jquery=#connType	${conn_type}
	Select From List By Value	jquery=#connItfName	${conn_intf}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Bring up the connection
	Click Element	//*[@id="${sim_conn}"]/td[1]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="nonAccessControls"]/input[3]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Wireless Modem::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Table Should Contain	jquery=#wmodemGlobalTable	Channel-A

Test Boundary values for the MTU
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	@{VALIDS}=	Create List	1	1500	65535
	@{INVALIDS}=	Create List	999999999	string	0	65536
	GUI::Basic::Input Text::Validation	mobileMTU	${VALIDS}	${INVALIDS}

Configure lowest MTU and then send default MTU traffic
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=mobileMTU	1
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Configure 80 MTU and then send traffic with greater payload and with fragmentation set
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=mobileMTU	80
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Configure 500 MTU and then send traffic with greater payload
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=mobileMTU	500
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Wireless Modem::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${check_modem}=	run keyword and return status	Table Should Contain	jquery=#wmodemGlobalTable	Channel-A

SUITE:Check Cellular Connection
	GUI::Basic::Tracking::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#wmodem > .title_b
	GUI::Basic::Spinner Should Be Invisible
	${sim_failed}=	Get Table Cell	xpath=//*[@id="wmodemTable"]	2	5
	should be equal	${sim_failed}	${check_sim_2}

SUITE:Teardown
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table Containing Value	peerTable	${sim_conn}	False
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Table Should Not Contain	peerTable	${sim_conn}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout and Close Nodegrid