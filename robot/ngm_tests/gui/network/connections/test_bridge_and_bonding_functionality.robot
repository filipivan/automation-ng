*** Settings ***
Documentation	Automated functionality test cases for bridge and bonding interfaces
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EXCLUDEIN4_2	EXCLUDEIN5_0

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${TEST_BRIDGE_TYPE}	Bridge
${TEST_BOND_TYPE}	Bonding
${DEFAULT_BRIDGE_ETH0_NAME}	bridge-eth0
${DEFAULT_BOND_ETH0_NAME}	bond-eth0
${DEFAULT_BRIDGE_ETH1_NAME}	bridge-eth1
${DEFAULT_BOND_ETH1_NAME}	bond-eth1
${DEFAULT_BRIDGE_ETH0ETH1_NAME}	bridge-eth0eth1	#It maybe different after changes on BUG_NG_8744 and BUG_NG_8736
${DEFAULT_BOND_ETH0ETH1_NAME}	bond-eth0eth1	#It maybe different after changes on BUG_NG_8744 and BUG_NG_8736
${DEFAULT_BRIDGE_ETH0BOND0_NAME}	bridge-eth0bond0	#It maybe different after changes on BUG_NG_8744 and BUG_NG_8736
${DEFAULT_BOND_BOND0_NAME}	bond-bond0
${TEST_INTERFACE_ETH0}	eth0
${TEST_INTERFACE_ETH1}	eth1
${TEST_INTERFACE_ETH0ETH1}	eth0 eth1
${TEST_INTERFACE_ETH0BOND0}	eth0 bond0
${TEST_BOND_ROUD-ROBIN}	Round-robin
${TEST_CONNECTION_ETH1}	ETH1
${CUSTOM_MAC_1}	00:11:22:33:44:55
${CUSTOM_MAC_2}	00:aa:bb:cc:dd:ee

*** Test Cases ***
Test case to brigde eth0
	${CONNECTION_NAMES}=	Create List	${DEFAULT_BRIDGE_ETH0_NAME}
	SUITE:Setup bridge connection	${TEST_BRIDGE_TYPE}	${TEST_INTERFACE_ETH0}	${DEFAULT_BRIDGE_ETH0_NAME}
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	${DEFAULT_BRIDGE_ETH0_NAME}+\\s+Connected+\\s+Bridge+\\s+br0+\\s+Up+\\s+${HOST}
	[Teardown]	SUITE:Connections Teardown	${CONNECTION_NAMES}

Test case to bond eth0
	${CONNECTION_NAMES}=	Create List	${DEFAULT_BOND_ETH0_NAME}
	SUITE:Setup bonding connection	${TEST_BOND_TYPE}	${TEST_BOND_ROUD-ROBIN}	${TEST_INTERFACE_ETH0}	${DEFAULT_BOND_ETH0_NAME}
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	${DEFAULT_BOND_ETH0_NAME}+\\s+Connected+\\s+Bonding+.*Up+\\s+${HOST}
	[Teardown]	SUITE:Connections Teardown	${CONNECTION_NAMES}

Test case to bridge eth1
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN_OFFICIAL
	${CONNECTION_NAMES}=	Create List	${DEFAULT_BRIDGE_ETH1_NAME}
	${HOST_HAS_ETH1}	GUI::Network::Check If Has Connection	${TEST_CONNECTION_ETH1}
	Set Suite Variable	${HOST_HAS_ETH1}
	Skip If	not ${HOST_HAS_ETH1}	This automation test environment has no ETH1 connection
	${HOST_ETH1_HAS_IP}	Run Keyword And Return Status	GUI::Network::Get Connection Ipv4	${TEST_CONNECTION_ETH1}
	Set Suite Variable	${HOST_ETH1_HAS_IP}
	Skip If	not ${HOST_ETH1_HAS_IP}	This automation test environment has ETH1 connection but it's Down without IP Address

	${HOST_ETH1_IPV4}	GUI::Network::Get Connection Ipv4	${TEST_CONNECTION_ETH1}
	Set Suite Variable	${HOST_ETH1_IPV4}
	SUITE:Setup bridge connection	${TEST_BRIDGE_TYPE}	${TEST_INTERFACE_ETH1}	${DEFAULT_BRIDGE_ETH1_NAME}
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	${DEFAULT_BRIDGE_ETH1_NAME}+\\s+Connected+\\s+Bridge+\\s+br0+\\s+Up+\\s+${HOST_ETH1_IPV4}
	[Teardown]	SUITE:Connections Teardown	${CONNECTION_NAMES}

Test case to bond eth1
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN_OFFICIAL
	${CONNECTION_NAMES}=	Create List	${DEFAULT_BOND_ETH1_NAME}
	Skip If	not ${HOST_HAS_ETH1}	This automation test environment has no ETH1 connection
	Skip If	not ${HOST_ETH1_HAS_IP}	This automation test environment has ETH1 connection but it's Down without IP Address

	SUITE:Setup bonding connection	${TEST_BOND_TYPE}	${TEST_BOND_ROUD-ROBIN}	${TEST_INTERFACE_ETH1}	${DEFAULT_BOND_ETH1_NAME}
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	${DEFAULT_BOND_ETH1_NAME}+\\s+Connected+\\s+Bonding+.*Up+\\s+${HOST_ETH1_IPV4}
	[Teardown]	SUITE:Connections Teardown	${CONNECTION_NAMES}

Test case to bridge eth0 and eth1
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN_OFFICIAL
	${CONNECTION_NAMES}=	Create List	${DEFAULT_BRIDGE_ETH0_NAME}
	Skip If	not ${HOST_HAS_ETH1}	This automation test environment has no ETH1 connection
	Skip If	not ${HOST_ETH1_HAS_IP}	This automation test environment has ETH1 connection but it's Down without IP Address
	Skip If	${IS_VM}	This automation test environment is a NGM, due a critical automation BUG_NG_8927 this test cannot be executed on VMs

	SUITE:Setup bridge connection	${TEST_BRIDGE_TYPE}	${TEST_INTERFACE_ETH0ETH1}	${DEFAULT_BRIDGE_ETH0_NAME}
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	${DEFAULT_BRIDGE_ETH0_NAME}+\\s+Connected+\\s+Bridge+\\s+br0+\\s+Up+\\s+${HOST}
	[Teardown]	SUITE:Connections Teardown	${CONNECTION_NAMES}

Test case to bond eth0 and eth1
	[Tags]	EXCLUDEIN_NIGHTLY	EXCLUDEIN_OFFICIAL
	# WAS CONVERTED IN A FER_350
	${CONNECTION_NAMES}=	Create List	${DEFAULT_BOND_ETH0_NAME}
	Skip If	not ${HOST_HAS_ETH1}	This automation test environment has no ETH1 connection
	Skip If	not ${HOST_ETH1_HAS_IP}	This automation test environment has ETH1 connection but it's Down without IP Address

	SUITE:Setup bonding connection	${TEST_BOND_TYPE}	${TEST_BOND_ROUD-ROBIN}	${TEST_INTERFACE_ETH0ETH1}	${DEFAULT_BOND_ETH0_NAME}
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	${DEFAULT_BOND_ETH0_NAME}+\\s+Connected+\\s+Bonding+.*Up+\\s+${HOST}
	[Teardown]	SUITE:Connections Teardown	${CONNECTION_NAMES}

Test case to bridge eth0 with bond-eth1
	[Tags]	BUG_NG_8927	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN_OFFICIAL
	${CONNECTION_NAMES}=	Create List	${DEFAULT_BOND_ETH1_NAME}
	Skip If	not ${HOST_HAS_ETH1}	This automation test environment has no ETH1 connection
	Skip If	not ${HOST_ETH1_HAS_IP}	This automation test environment has ETH1 connection but it's Down without IP Address
	Skip If	${IS_VM}	This automation test environment is a NGM, due a critical automation BUG_NG_8927 this test cannot be executed on VMs

	SUITE:Setup bonding connection	${TEST_BOND_TYPE}	${TEST_BOND_ROUD-ROBIN}	${TEST_INTERFACE_ETH1}	${DEFAULT_BOND_ETH1_NAME}
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	${DEFAULT_BOND_ETH1_NAME}+\\s+Connected+\\s+Bonding+.*Up+\\s+${HOST_ETH1_IPV4}

	SUITE:Setup bridge connection	${TEST_BRIDGE_TYPE}	${TEST_INTERFACE_ETH0BOND0}	${DEFAULT_BRIDGE_ETH0_NAME}
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	${DEFAULT_BRIDGE_ETH0_NAME}+\\s+Connected+\\s+Bridge+.*Up+\\s+${HOST}
	[Teardown]	SUITE:Connections Teardown	${CONNECTION_NAMES}

Test add bonding connection with custom MAC address
	[Documentation]	Test adds a bonding connection with only ${TEST_CONNECTION_ETH1} as slave and ${CUSTOM_MAC_1} as MAC
	...	address, then check if this custom address was really applied in NG context.
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	${CONNECTION_NAMES}=	Create List	${DEFAULT_BOND_ETH1_NAME}
	SUITE:Setup bonding connection	${TEST_BOND_TYPE}	${TEST_BOND_ROUD-ROBIN}	${TEST_INTERFACE_ETH1}	${DEFAULT_BOND_ETH1_NAME}	${CUSTOM_MAC_1}
	[Teardown]	SUITE:Connections Teardown	${CONNECTION_NAMES}

Test check if custom MAC address is configured correctly with ifconfig in bonding connection
	[Documentation]	Test adds a bridge connection with only ${TEST_CONNECTION_ETH1} as slave and ${CUSTOM_MAC_1} as MAC
	...	address, then check if this custom address was really applied in network context with ifconfig command.
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	${CONNECTION_NAMES}=	Create List	${DEFAULT_BOND_ETH1_NAME}
	SUITE:Setup bonding connection	${TEST_BOND_TYPE}	${TEST_BOND_ROUD-ROBIN}	${TEST_INTERFACE_ETH1}	${DEFAULT_BOND_ETH1_NAME}	${CUSTOM_MAC_1}
	GUI::Access::Table::Access Console
	GUI:Access::Generic Console Command Output	shell sudo su -
	${BOND0_CONFIG}	GUI:Access::Generic Console Command Output	ifconfig bond0
	Should Contain	${BOND0_CONFIG}	ether ${CUSTOM_MAC_1}
	Close Window
	Switch Window	nodegrid - Nodegrid
	[Teardown]	SUITE:Connections Teardown	${CONNECTION_NAMES}

Test edit bonding connection for other custom MAC address and check it changes
	[Documentation]	Test adds a bonding connection with ${CUSTOM_MAC_1} and then edits it to have ${CUSTOM_MAC_2} and
	...	checks that it has the new address and not the old one.
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	${CONNECTION_NAMES}=	Create List	${DEFAULT_BOND_ETH1_NAME}
	SUITE:Setup bonding connection	${TEST_BOND_TYPE}	${TEST_BOND_ROUD-ROBIN}	${TEST_INTERFACE_ETH1}	${DEFAULT_BOND_ETH1_NAME}	${CUSTOM_MAC_1}
	GUI::Network::Open Connections Tab
	Page Should Contain	${DEFAULT_BOND_ETH1_NAME}
	Click Element	xpath=//a[contains(text(),'${DEFAULT_BOND_ETH1_NAME}')]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text	//input[@id='bondMacAddr']	${CUSTOM_MAC_2}
	GUI::Basic::Save
	SUITE:Check Connection With Custom Mac	${DEFAULT_BOND_ETH1_NAME}	${CUSTOM_MAC_2}
	[Teardown]	SUITE:Connections Teardown	${CONNECTION_NAMES}

Test edit bonding connection for failover MAC and primary interface
	[Documentation]	Test adds a bonding connection with ${CUSTOM_MAC_1} and then edits it to have the MAC address from
	...	the first interface. Test checks that it has the address from ${TEST_INTERFACE_ETH1} and not the old one.
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	${CONNECTION_NAMES}=	Create List	${DEFAULT_BOND_ETH1_NAME}
	${ETH1_MAC}	SUITE:Get Interface MAC Address
	SUITE:Setup bonding connection	${TEST_BOND_TYPE}	${TEST_BOND_ROUD-ROBIN}	${TEST_INTERFACE_ETH1}	${DEFAULT_BOND_ETH1_NAME}	${CUSTOM_MAC_1}
	GUI::Network::Open Connections Tab
	Page Should Contain	${DEFAULT_BOND_ETH1_NAME}
	Click Element	xpath=//a[contains(text(),'${DEFAULT_BOND_ETH1_NAME}')]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Select Radio Button	bondMacConf	bond_fail-over-mac
	Select From List By label	//select[@id="bondFailMac"]	Primary Interface
	GUI::Basic::Save
	SUITE:Check Connection With Custom Mac	${DEFAULT_BOND_ETH1_NAME}	${ETH1_MAC}
	[Teardown]	SUITE:Connections Teardown	${CONNECTION_NAMES}

Test add bridge connection with custom MAC address
	[Documentation]	Test adds a bridge connection with only ${TEST_CONNECTION_ETH1} as slave and ${CUSTOM_MAC_1} as MAC
	...	address, then check if this custom address was really applied in NG context.
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	${CONNECTION_NAMES}=	Create List	${DEFAULT_BRIDGE_ETH1_NAME}
	SUITE:Setup bridge connection	${TEST_BRIDGE_TYPE}	${TEST_INTERFACE_ETH1}	${DEFAULT_BRIDGE_ETH1_NAME}	${CUSTOM_MAC_1}
	[Teardown]	SUITE:Connections Teardown	${CONNECTION_NAMES}

Test check if custom MAC address is configured correctly with ifconfig in bridge connection
	[Documentation]	Test adds a bridge connection with only ${TEST_CONNECTION_ETH1} as slave and ${CUSTOM_MAC_1} as MAC
	...	address, then check if this custom address was really applied in network context with ifconfig command.
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	${CONNECTION_NAMES}=	Create List	${DEFAULT_BOND_ETH1_NAME}
	SUITE:Setup bridge connection	${TEST_BRIDGE_TYPE}	${TEST_INTERFACE_ETH1}	${DEFAULT_BRIDGE_ETH1_NAME}	${CUSTOM_MAC_1}
	GUI::Access::Table::Access Console
	GUI:Access::Generic Console Command Output	shell sudo su -
	${BR0_CONFIG}	GUI:Access::Generic Console Command Output	ifconfig br0
	Should Contain	${BR0_CONFIG}	ether ${CUSTOM_MAC_1}
	Close Window
	Switch Window	nodegrid - Nodegrid
	[Teardown]	SUITE:Connections Teardown	${CONNECTION_NAMES}

Test edit bridge connection for other custom MAC address and check it changes
	[Documentation]	Test adds a bridge connection with ${CUSTOM_MAC_1} and then edits it to have ${CUSTOM_MAC_2} and
	...	checks that it has the new address and not the old one.
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	${CONNECTION_NAMES}=	Create List	${DEFAULT_BRIDGE_ETH1_NAME}
	SUITE:Setup bridge connection	${TEST_BRIDGE_TYPE}	${TEST_INTERFACE_ETH1}	${DEFAULT_BRIDGE_ETH1_NAME}	${CUSTOM_MAC_1}
	GUI::Network::Open Connections Tab
	Page Should Contain	${DEFAULT_BRIDGE_ETH1_NAME}
	Click Element	xpath=//a[contains(text(),'${DEFAULT_BRIDGE_ETH1_NAME}')]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text	//input[@id='bridgeMacAddr']	${CUSTOM_MAC_2}
	GUI::Basic::Save
	SUITE:Check Connection With Custom Mac	${DEFAULT_BRIDGE_ETH1_NAME}	${CUSTOM_MAC_2}
	[Teardown]	SUITE:Connections Teardown	${CONNECTION_NAMES}

Test edit bridge connection for failover MAC and primary interface
	[Documentation]	Test adds a bridge connection with ${CUSTOM_MAC_1} and then edits it to have the MAC address from
	...	the first interface. Test checks that it has the address from ${TEST_INTERFACE_ETH1} and not the old one.
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	${CONNECTION_NAMES}=	Create List	${DEFAULT_BRIDGE_ETH1_NAME}
	${ETH1_MAC}	SUITE:Get Interface MAC Address
	SUITE:Setup bridge connection	${TEST_BRIDGE_TYPE}	${TEST_INTERFACE_ETH1}	${DEFAULT_BRIDGE_ETH1_NAME}	${CUSTOM_MAC_1}
	GUI::Network::Open Connections Tab
	Page Should Contain	${DEFAULT_BRIDGE_ETH1_NAME}
	Click Element	xpath=//a[contains(text(),'${DEFAULT_BRIDGE_ETH1_NAME}')]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Select Radio Button	bridgeMacConf	use_mac_from_first_interface
	GUI::Basic::Save
	SUITE:Check Connection With Custom Mac	${DEFAULT_BRIDGE_ETH1_NAME}	${ETH1_MAC}
	[Teardown]	SUITE:Connections Teardown	${CONNECTION_NAMES}

*** Keywords ***
SUITE:Setup
	${ALL_INTERFACES_NAME}=	Create List	${DEFAULT_BRIDGE_ETH0_NAME}	${DEFAULT_BOND_ETH0_NAME}
	...	${DEFAULT_BRIDGE_ETH1_NAME}	${DEFAULT_BOND_ETH1_NAME}	${DEFAULT_BRIDGE_ETH0ETH1_NAME}
	...	${DEFAULT_BRIDGE_ETH0BOND0_NAME}	${DEFAULT_BOND_BOND0_NAME}
	Set Suite Variable	${ALL_INTERFACES_NAME}
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Connections Teardown	${ALL_INTERFACES_NAME}
	#Check If Host is a VM by CLI
	CLI:Open
	${IS_VM}=	CLI:Is VM System
	Set Suite Variable	${IS_VM}
	CLI:Close Connection

SUITE:Teardown
	Close All Browsers
	Wait Until Keyword Succeeds	3x	60s	GUI::Basic::Open And Login Nodegrid
	SUITE:Connections Teardown	${ALL_INTERFACES_NAME}
	GUI::Basic::Logout And Close Nodegrid

SUITE:Connections Teardown
	[Arguments]	${TEST_CONNECTIONS}
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Click Element	//span[@id='refresh-icon']
	GUI::Basic::Delete Rows In Table If Exists	peerTable	${TEST_CONNECTIONS}	HAS_ALERT=${FALSE}
	GUI::Basic::Page Should Not Contain	${TEST_CONNECTIONS}

SUITE:Setup bridge connection
	[Arguments]	${TEST_TYPE}	${TEST_INTERFACE}	${DEFAULT_CONNECTION_NAME}	${MAC_ADDRESS}=default
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Add
	Select From List By label	jquery=#connType	${TEST_TYPE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text	//input[@id='bridgeSlave']	${TEST_INTERFACE}
	IF	'${MAC_ADDRESS}' != 'default'
		GUI::Basic::Select Radio Button	bridgeMacConf	bridge_mac_custom
		GUI::Basic::Input Text	//input[@id='bridgeMacAddr']	${MAC_ADDRESS}
		GUI::Basic::Save	WAIT_SPINNER=${FALSE}
		SUITE:Check Connection With Custom Mac	${DEFAULT_CONNECTION_NAME}	${MAC_ADDRESS}
	ELSE
		GUI::Basic::Save	WAIT_SPINNER=${FALSE}
		Wait Until Keyword Succeeds	6x	10s	SUITE:Should contain network connection name	${DEFAULT_CONNECTION_NAME}
	END
SUITE:Should contain network connection name
	[Arguments]	${DEFAULT_CONNECTION_NAME}
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Click Element	//span[@id='refresh-icon']
	Page Should Contain	${DEFAULT_CONNECTION_NAME}
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	${DEFAULT_CONNECTION_NAME}+\\s+Connected+\\s+.*Up

SUITE:Check Connection With Custom Mac
	[Arguments]	${DEFAULT_CONNECTION_NAME}	${MAC_ADDRESS}
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Click Element	//span[@id='refresh-icon']
	Page Should Contain	${DEFAULT_CONNECTION_NAME}
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections \n${TABLE_CONTENTS}	INFO	console=yes
	Should Contain	${TABLE_CONTENTS}	${MAC_ADDRESS}

SUITE:Setup bonding connection
	[Arguments]	${TEST_TYPE}	${TEST_BOND_MODE}	${TEST_INTERFACE}	${DEFAULT_CONNECTION_NAME}	${MAC_ADDRESS}=default
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Add
	Select From List By label	jquery=#connType	${TEST_TYPE}
	Select From List By label	//select[@id='bondMode']	${TEST_BOND_MODE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text	//input[@id='bondSlave']	${TEST_INTERFACE}
	IF	'${MAC_ADDRESS}' != 'default'
		GUI::Basic::Select Radio Button	bondMacConf	bond_custom_mac
		GUI::Basic::Input Text	//input[@id='bondMacAddr']	${MAC_ADDRESS}
		GUI::Basic::Save	WAIT_SPINNER=${FALSE}
		SUITE:Check Connection With Custom Mac	${DEFAULT_CONNECTION_NAME}	${MAC_ADDRESS}
	ELSE
		GUI::Basic::Save	WAIT_SPINNER=${FALSE}
		Wait Until Keyword Succeeds	6x	10s	SUITE:Should contain network connection name	${DEFAULT_CONNECTION_NAME}
	END

SUITE:Get Interface MAC Address
	CLI:Open
	${MAC_ADDRESS}	CLI:Get Interface MAC Address	${TEST_INTERFACE_ETH1}
	CLI:Close Connection
	[Return]	${MAC_ADDRESS}