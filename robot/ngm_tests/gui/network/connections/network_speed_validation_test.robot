*** Settings ***
Documentation	Automated test Network Speed Validation
Metadata		Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	EXCLUDEIN4_2	EXCLUDEIN5_0	NON-CRITICAL	NEED-REVIEW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${CONNECTION_NAME}	eth0

*** Test Cases ***
Test case to check option for link monitoring
	Click link	//*[@id="ETH0"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Link Mode:
	Click Element	id=ethLinkMode
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	id=ethLinkMode	10M/Full
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	SUITE:Setup
	Click link	//*[@id="ETH0"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	10M/Full
	SUITE:Setup
	Click link	//*[@id="ETH0"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Link Mode:
	Click Element	id=ethLinkMode
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	id=ethLinkMode	100M/Full
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	SUITE:Setup
	Click link	//*[@id="ETH0"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	100M/Full
	SUITE:Setup
	Click link	//*[@id="ETH0"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Link Mode:
	Click Element	id=ethLinkMode
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	id=ethLinkMode	1G/Full
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	SUITE:Setup
	Click link	//*[@id="ETH0"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	1G/Full
	SUITE:Setup
	Click link	//*[@id="ETH0"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Link Mode:
	Click Element	id=ethLinkMode
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	id=ethLinkMode	Auto
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	SUITE:Setup
	Click link	//*[@id="ETH0"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Auto

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Network::Connections::open tab

SUITE:Teardown
	GUI::Basic::Logout and Close Nodegrid
