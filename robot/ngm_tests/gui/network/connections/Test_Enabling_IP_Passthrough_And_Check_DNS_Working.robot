*** Settings ***
Documentation	Test to Enabling Ip-Passthrough And Check DNS Working
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_0

*** Variables ***
${Port_host1}		${SWITCH_NETPORT_HOST_INTERFACE1}
${Port_hostshared1}		${SWITCH_NETPORT_SHARED_INTERFACE1}
${COORD_HOMEPAGE}		${HOMEPAGE}
${PEER_HOMEPAGE}		${HOMEPAGESHARED}
${GSM_Connection}		GSM
${CONNECTION_TYPE}		Mobile Broadband GSM
${INTERFACE}	cdc-wdm8
${IPV4_address}
${DNS_Adress}		www.google.com

*** Test Cases ***
Test Case to Verify if Device has GSM
	${HAS_GSM}	GUI::Tracking::Check If Has GSM Module and Sim Card with Signal
	Set Suite Variable	${HAS_GSM}
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	Log	This device has GSM

Test Case to Configure Vlan on Both Devices
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	SUITE:To Configure Vlan on Host Side
	SUITE:To Configure Vlan on Host Shared Side

Test to Case to Add GSM Connection
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	SUITE:To Add GSM Connection on Host Side
	SUITE:To get ipv4 Address After Adding GSM Connection

Test Case to Enable Ip-Passthrough
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	SUITE:To Enable Ip-passthrough on Host Side

Test Case to check the backplane0 is up and Check the ping Status
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	SUITE:To Up the backplane0 Connection
	SUITE:To Check Ping Status on Host-Shared Side

Test Case to Delete Config
	SUITE:To Set Default Config on Host Side
	SUITE:To Set Default Config on Host Shared Side

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:To Configure Vlan on Host Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open Settings Tab
	GUI::Basic::Spinner Should Be Invisible
	click element		xpath=//label[contains(.,'Enable IPv4 IP Forward')]
	click element		xpath=//select[@id='rpfilter']
	select from list by label		xpath=//select[@id='rpfilter']		Disabled
	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
	sleep		20s
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	click element		xpath=//a[contains(text(),'BACKPLANE0')]
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure		click element		xpath=//label[contains(.,'No IPv4 Address')]
	Run Keyword And Continue on Failure		click element		css=.radio:nth-child(1) #method6
	Run Keyword And Continue on Failure	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	wait until page contains element		xpath=//a[contains(text(),'${Port_host1}')]
	Run Keyword And Continue on Failure		click element		xpath=//a[contains(text(),'${Port_host1}')]
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select from list by value	//*[@id="status"]	enabled
	Run Keyword And Continue on Failure	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	click element		xpath=(//a[contains(text(),'1')])[3]
	GUI::Basic::Spinner Should Be Invisible
	execute javascript  document.getElementById("memberUntagged").scrollIntoView()
	Run Keyword And Continue on Failure		Click Element		//div[@id="memberUntagged"]//option[@value="backplane0"]
	Run Keyword And Continue on Failure		Click Element		//*[@id="memberUntagged"]/div/div[1]/div[2]/div/div[1]/button
	Run Keyword And Continue on Failure		Click Element		//div[@id="memberUntagged"]//option[@value="${Port_host1}"]
	Run Keyword And Continue on Failure		Click Element		//*[@id="memberUntagged"]/div/div[1]/div[2]/div/div[1]/button
	Run Keyword And Continue on Failure		Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Configure Vlan on Host Shared Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open Settings Tab
	GUI::Basic::Spinner Should Be Invisible
	click element		xpath=//label[contains(.,'Enable IPv4 IP Forward')]
	click element		xpath=//select[@id='rpfilter']
	select from list by label		xpath=//select[@id='rpfilter']		Disabled
	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	wait until page contains element		xpath=//a[contains(text(),'${Port_hostshared1}')]
	Run Keyword And Continue on Failure		click element		xpath=//a[contains(text(),'${Port_hostshared1}')]
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Continue on Failure	select from list by value	//*[@id="status"]	enabled
	Run Keyword And Continue on Failure	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	click element		xpath=(//a[contains(text(),'1')])[3]
	GUI::Basic::Spinner Should Be Invisible
	execute javascript  document.getElementById("memberUntagged").scrollIntoView()
	Run Keyword And Continue on Failure	Click Element		//div[@id="memberUntagged"]//option[@value="backplane0"]
	Run Keyword And Continue on Failure	Click Element		//*[@id="memberUntagged"]/div/div[1]/div[2]/div/div[1]/button
	Run Keyword And Continue on Failure	Click Element		//div[@id="memberUntagged"]//option[@value="${Port_hostshared1}"]
	Run Keyword And Continue on Failure	Click Element		//*[@id="memberUntagged"]/div/div[1]/div[2]/div/div[1]/button
	Run Keyword And Continue on Failure		Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Add GSM Connection on Host Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="connName"]		${GSM_Connection}
	Click Element	id=connType
	Select From List By Label	//*[@id="connType"]	${CONNECTION_TYPE}
	wait until page contains element		//*[@id="connItfName"]
	Select From List By Label	//*[@id="connItfName"]		${INTERFACE}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:To get ipv4 Address After Adding GSM Connection
	GUI::Basic::Network::Connections::Open Tab
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections: \n${TABLE_CONTENTS}	INFO	console=yes
	${CONNECTION_HAS_IPV4}	Run Keyword And Return Status	Should Match Regexp	${TABLE_CONTENTS}	${GSM_Connection}+.*(?:[0-9]{1,3}\\.){3}[0-9]{1,3}

SUITE:To Enable Ip-passthrough on Host Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	click element		xpath=//a[contains(text(),'${GSM_Connection}')]
	GUI::Basic::Spinner Should Be Invisible
	execute javascript  document.getElementById("ipPass").scrollIntoView()
	click element		//*[@id="ipPass"]
	click element		//*[@id="ipPassLanConn"]
	select from list by label		//*[@id="ipPassLanConn"]		BACKPLANE0
	wait until page contains element		//*[@id="saveButton"]
	click element		//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
	sleep		10s
	page should contain			IP Passthrough

SUITE:To Check Ping Status on Host-Shared Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	click element	css=.submenu li:nth-child(5) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="icon_NetworkTools"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=.fixed-top
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="ipaddress"]	${DNS_Adress}
	Select From List By value	//*[@id="interface"]	backplane0
	Click Element	xpath=//input[@value="Ping"]
	GUI::Basic::Spinner Should Be Invisible
	sleep	15s
	Wait Until Page Contains	ping statistics
	page should not contain	100% packet loss,
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Set Default Config on Host Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	select checkbox		//*[@id="GSM"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	wait until page contains element		xpath=//a[contains(text(),'${Port_host1}')]
	click element		xpath=//a[contains(text(),'${Port_host1}')]
	GUI::Basic::Spinner Should Be Invisible
	select from list by value	//*[@id="status"]	disabled
	Run Keyword And Continue on Failure	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open Settings Tab
	GUI::Basic::Spinner Should Be Invisible
	wait until page contains element		//*[@id="ipv4-ipforward"]
	unselect checkbox		//*[@id="ipv4-ipforward"]
	click element		xpath=//select[@id='rpfilter']
	select from list by label		xpath=//select[@id='rpfilter']		Strict Mode
	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
	sleep		20s

SUITE:To Set Default Config on Host Shared Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	wait until page contains element		xpath=//a[contains(text(),'${Port_hostshared1}')]
	click element		xpath=//a[contains(text(),'${Port_hostshared1}')]
	GUI::Basic::Spinner Should Be Invisible
	select from list by value	//*[@id="status"]	disabled
	Run Keyword And Continue on Failure	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open Settings Tab
	GUI::Basic::Spinner Should Be Invisible
	wait until page contains element		//*[@id="ipv4-ipforward"]
	unselect checkbox		//*[@id="ipv4-ipforward"]
	click element		xpath=//select[@id='rpfilter']
	select from list by label		xpath=//select[@id='rpfilter']		Strict Mode
	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
	sleep		20s

SUITE:To Up the backplane0 Connection
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	select checkbox		//*[@id="BACKPLANE0"]/td[1]/input
	click element		//*[@id="nonAccessControls"]/input[3]
	GUI::Basic::Spinner Should Be Invisible
	sleep		10s
	${IPV4_address}=	    Get Text        //*[@id="BACKPLANE0"]/td[7]
