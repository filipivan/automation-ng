*** Settings ***
Documentation	Testing the VLAN options for Network:Connections
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CONNECTION}	9./placeholder
${TYPE}	vlan

*** Test Cases ***
Select Vlan Type
	Select From List By Value	jquery=#connType	${TYPE}
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	jquery=#vlanTitle

Test ID Properties
	[Tags]	NON-CRITICAL	NEED-REVIEW
	@{VALIDS}=	Create List	1234
	@{INVALIDS}=	Create List	';./'	vlanid	id1234	12.34.12.34	new id;
	GUI::Basic::Input Text::Validation	vlanId	${VALIDS}	${INVALIDS}

*** Keywords ***
SUITE:Setup
	${CONNECTIONS}=	Create List	${CONNECTION}
	Set Suite Variable	${CONNECTIONS}	${CONNECTIONS}
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table If Exists	peerTable_wrapper	${CONNECTIONS}
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	jquery=#connName	${CONNECTION}

SUITE:Teardown
	Run Keyword And Continue On Failure	GUI::Basic::Button::Cancel
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table If Exists	peerTable_wrapper	${CONNECTIONS}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
