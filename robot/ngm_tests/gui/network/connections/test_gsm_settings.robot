*** Settings ***
Documentation	Testing the Mobile GSM options for Network:Connections
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CONNECTION}	9./placeholder
${TYPE}	gsm

*** Test Cases ***
Select Broadband Type
	Select From List By Value	jquery=#connType	${TYPE}
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	jquery=#mobileTitle

Test SIM1
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Test SIM Configuration	1

Test SIM2
	[Tags]	NON-CRITICAL	NEED-REVIEW
	Select Checkbox	jquery=#mobileDualSim
	Wait Until Element Is Visible	jquery=#mobileActiveSim
	SUITE:Test SIM Configuration	2
	@{EXPECTED}=	Create List	1	2
	@{SIMS}=	Get List Items	jquery=#mobileActiveSim
	Should Be Equal	${SIMS}	${EXPECTED}

*** Keywords ***
SUITE:Setup
	${CONNECTIONS}=	Create List	${CONNECTION}
	Set Suite Variable	${CONNECTIONS}	${CONNECTIONS}
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table If Exists	peerTable_wrapper	${CONNECTIONS}
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	jquery=#connName	${CONNECTION}

SUITE:Teardown
	Run Keyword And Continue On Failure	GUI::Basic::Button::Cancel
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table If Exists	peerTable_wrapper	${CONNECTIONS}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

SUITE:Test SIM Configuration
	[Arguments]	${SIM}=${EMPTY}
	@{VALIDS}=	Create List	124-13	.3112.1	2.23	12;124	124\\124	24-124
	@{INVALIDS}=	Create List	12
	GUI::Basic::Input Text::Validation	mobilePhonen    ${SIM}	${VALIDS}	${INVALIDS}
	GUI::Basic::Input Text::Validation	mobilePIN   ${SIM}	${VALIDS}	${INVALIDS}
	Element Should Be Visible	jquery=#mobileUser  ${SIM}
	Element Should Be Visible	jquery=#mobilePasswd    ${SIM}
	Element Should Be Visible	jquery=#mobileAPN   ${SIM}

