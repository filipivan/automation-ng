*** Settings ***
Documentation	Testing the WIFI options for Network:Connections
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CONNECTION}	9./placeholder
${TYPE}	wifi
${Security}	wpa-psk

*** Test Cases ***
Select Wifi Type
	Select From List By Value	jquery=#connType	${TYPE}
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	jquery=#wifiTitle

Verify SSID is Present
	Element Should Be Visible	jquery=#wifiSsid

Verify BSSID is Present
	Element Should Be Visible	jquery=#wifiBssid

Verify Security Disabled
	Element Should Be Visible    jquery=#wifiSec
	Element Should Not Be Visible	jquery=#wifiSecPsk

Verify Security Enabled
	Run Keyword If	'${NGVERSION}'>='5.9'	Select From List By Value    jquery=#wifiSec	${Security}
	...	ELSE	Select Radio Button		wifiSec	wpa-psk
	Element Should Be Visible	jquery=#wifiSecPsk

Verify Hidden Network Checkbox
	Page Should Contain Element	//*[@id="wifiHidden"]

*** Keywords ***
SUITE:Setup
	${CONNECTIONS}=	Create List	${CONNECTION}
	Set Suite Variable	${CONNECTIONS}	${CONNECTIONS}
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table If Exists	peerTable_wrapper	${CONNECTIONS}
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Run Keyword And Continue On Failure	GUI::Basic::Button::Cancel
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table If Exists	peerTable_wrapper	${CONNECTIONS}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

