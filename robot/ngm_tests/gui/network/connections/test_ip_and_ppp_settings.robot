*** Settings ***
Documentation	Testing the ip and ppp for connections
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CONNECTION}	test
${TYPE}	generic

*** Test Cases ***
Select Analog MODEM Type
	Input Text	jquery=#connName	${CONNECTION}
	Select From List By Value	jquery=#connType	${TYPE}
	GUI::Basic::Spinner Should Be Invisible
	Element Should Not Be Visible	jquery=#connIeth

Test PPP IPv4 Settings
    [Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Test PPP IP Configuration	4

Test PPP IPv6 Settings
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Test PPP IP Configuration	6

Test PPP Auth Settings
	[Tags]	NON-CRITICAL	NEED-REVIEW
	Select Radio Button	pppAuthType	none
	GUI::Basic::Spinner Should Be Invisible
	Element Should Not Be Visible	jquery=#pppAuthProto
	Select Radio Button	pppAuthType	local
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	jquery=#pppAuthProto
	@{CHK}=	Create List	PAP	CHAP	EAP
	@{LST}=	Get List Items	jquery=#pppAuthProto
	Should Be Equal	${CHK}	${LST}
	Select Radio Button	pppAuthType	remote
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	jquery=#pppAuthUser
	Element Should Be Visible	jquery=#pppAuthPasswd

*** Keywords ***
SUITE:Setup
	${CONNECTIONS}=	Create List	${CONNECTION}
	Set Suite Variable	${CONNECTIONS}	${CONNECTIONS}
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table If Exists	peerTable_wrapper	${CONNECTIONS}
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	jquery=#connName	${CONNECTION}

SUITE:Teardown
	Run Keyword And Continue On Failure	GUI::Basic::Button::Cancel
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table If Exists	peerTable_wrapper	${CONNECTIONS}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

SUITE:Test PPP IP Configuration
	[Arguments]	${FOS}
	[Documentation]	FOS is 'four or six' its expecting either 4 or 6
	@{FOS4_VALIDS}=	Create List	12.12.12.12
	@{FOS4_INVALIDS}=	Create List	1..245.2.1	.	12.53.12.53/2	12/25\\12#24
	@{FOS6_VALIDS}=	Create List	::002c
	@{FOS6_INVALIDS}=	Create List	20f3::245f	32:::425v	002c::002c	2d4f:2352:2453::2ddc
	@{FOS_VALIDS}=	Set Variable If	'${FOS}' == '4'	${FOS4_VALIDS}	${FOS6_VALIDS}
	@{FOS_INVALIDS}=	Set Variable If	'${FOS}' == '4'	${FOS4_INVALIDS}	${FOS6_INVALIDS}
	Select Radio Button     pppIP${FOS}type     no
	GUI::Basic::Spinner Should Be Invisible
	Element Should Not Be Visible	jquery=#pppIP${FOS}localaddr
	Select Radio Button	pppIP${FOS}type	local
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	jquery=#pppIP${FOS}localaddr
	GUI::Basic::Input Text::Validation	pppIP${FOS}localaddr	${FOS_VALIDS}	${FOS_INVALIDS}
	GUI::Basic::Input Text::Validation	pppIP${FOS}remaddr	${FOS_VALIDS}	${FOS_INVALIDS}
	Select Radio Button     pppIP${FOS}type     remote
	GUI::Basic::Spinner Should Be Invisible
	Element Should Not Be Visible	jquery=#pppIP${FOS}localaddr
