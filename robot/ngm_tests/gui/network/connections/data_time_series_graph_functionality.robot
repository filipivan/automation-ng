*** Settings ***
Documentation	Automated test for data series
Metadata		Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	admin
${PASSWORD}	zpe#!123
${DELAY}	5s
${Device_name}	nodegrid
${root_login}	shell sudo su -
${gateway_ip}	${GATEWAY}
${CELLULAR_CONNECTION}	cellular
${Data}	17.0GB

*** Test Cases ***
Test case to add cellular connection and enable Data monitoring
	SUITE:configure ETH0
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add Cellular Connection
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Teardown

Test case to verify data into tracking
	SUITE:configure ETH0
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add Cellular Connection
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Tracking::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="wmodem"]/span
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="Channel-A"]
	GUI::Basic::Spinner Should Be Invisible
	sleep	${DELAY}
	Page Should Contain	${Data}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:configure ETH0
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Page Should contain Element	//*[@id="ETH0"]
	Click Link	ETH0
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=.radio:nth-child(3) #method
	Click Element	id=address
	Input Text	id=address	${HOST}
	Click Element	id=netmask
	Input Text	id=netmask	24
	Click Element	//*[@id="gateway"]
	Input Text	//*[@id="gateway"]	${gateway_ip}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Add Cellular Connection
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=connName
	Input Text	//*[@id="connName"]	${CELLULAR_CONNECTION}
	Click Element	id=connType
	Select From List By Label	//*[@id="connType"]	Mobile Broadband GSM
	Click Element	id=connIeth
	Select From List By Label	id=connIeth	cdc-wdm0
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Setup
	Click Link	${CELLULAR_CONNECTION}
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	id=dataUsageMonitor1
	Click Element	id=dataUsageLimit1
	Input Text	id=dataUsageLimit1	17
	Click Element	id=dataUsageWarning1
	Input text	id=dataUsageWarning1	25
	sleep	${DELAY}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="cellular"]/td[1]/input
	Click Button	//*[@id="nonAccessControls"]/input[3]

SUITE:Teardown
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Page Should contain Element	//*[@id="ETH0"]
	Click Link	ETH0
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=.radio:nth-child(2) #method
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	css=#cellular input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid



