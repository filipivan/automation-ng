*** Settings ***
Documentation	Testing ----> Secondary DNS Server
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	BUG_NG_11890
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}

${GATEWAY_IP}	${GATEWAY}
${DNS_SERVER_IPV4_IP}	8.8.8.8
${DNS_SERVER_IPV4_IP2}	75.75.75.75
${DNS_SERVER_HOSTNAME1}	dns.google
${NET_MASK}	24
${DNS_SERVER_IPV6_IP}	123::123
${DNS_SERVER_HOSTNAME2}	cdns.comcast.net

*** Test Cases ***
Test Case To Add And Validate DNS Server
	[Tags]	NON-CRITICAL	BUG_NG_11890
	GUI::Network::Open Settings Tab
	${ret}=	Get value	id=dnsAddress
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Link	ETH1
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
#	Click Element	css=.radio:nth-child(3) #method
#	Input Text	id=address	${HOST}
#	Input Text	id=netmask	${NET_MASK}
#	Input Text	id=gateway	${GATEWAY_IP}
	Input Text	id=ipv4dns	${DNS_SERVER_IPV4_IP},${DNS_SERVER_IPV4_IP2}
	Input Text	id=ipv4dnsSearch	${DNS_SERVER_HOSTNAME1}
	Input Text	id=ipv6dns	${DNS_SERVER_IPV6_IP}
	Input Text	id=ipv6dnsSearch	${DNS_SERVER_HOSTNAME2}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::open tab
	GUI::Basic::Spinner Should Be Invisible
	sleep	10s
	Click Element	//*[@id="refresh-icon"]
	sleep	10s
	Click Element	//*[@id="refresh-icon"]
	sleep		3s
	${DNS_ADDRESS}=	Get Value	id=dnsAddress
	Should Contain Any	${DNS_ADDRESS}	${DNS_SERVER_IPV6_IP}	${DNS_SERVER_IPV4_IP},${DNS_SERVER_IPV4_IP2}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Link	ETH1
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=.radio:nth-child(2) #method
	Clear Element Text	id=ipv4dns
	Clear Element Text	id=ipv4dnsSearch
	Clear Element Text	id=ipv6dns
	Clear Element Text	id=ipv6dnsSearch
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid