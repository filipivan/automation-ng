*** Settings ***
Documentation	Testing the Bonding validation options for Network:Connections
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CONNECTION}	test
${TYPE}	bond

*** Test Cases ***
Select Bonding Type
	Select From List By Value	jquery=#connType	${TYPE}
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	jquery=#bondTitle

Check Bond Primary and Secundary
	IF	${HOST_HAS_ETH1}
	@{INTERFACES}=	Create List	eth0	eth1
	ELSE
	@{INTERFACES}=	Create List	eth0
	END
	@{PRIMARY_CON}=	Get List Items	bondPrimary
	@{SECONDAY_CON}=	Get List Items	bondSecondary
	FOR	${INTERFACE}	IN	@{INTERFACES}
		List Should Contain Value	${PRIMARY_CON}	${INTERFACE}
		List Should Contain Value	${SECONDAY_CON}	${INTERFACE}
	END

Check Bond Mode
	GUI::Basic::Spinner Should Be Invisible
	@{CHK}=	Create List	Round-robin	Active backup	XOR load balancing	Broadcast	802.3ad(LACP)	Adaptive Transmit load balancing	Adaptive load balancing
	@{BOND}=	Get List Items	bondMode
	Should Be Equal	${CHK}	${BOND}

Check Bond Monitor
	GUI::Basic::Spinner Should Be Invisible
	@{CHK}=	Create List	MII	ARP
	@{MONI}=	Get List Items	jquery=#bondMonitor
	Should Be Equal	${CHK}	${MONI}

Test Timeouts
	SUITE:Setup
	@{INVALIDS}=	Create List	313'12	124-13	.3112.1	2.23	12;124	124\\124	24-124
	GUI::Basic::Input Text::Validation      bondFreq		${INVALIDS}
	GUI::Basic::Input Text::Validation	    bondUpdelay     	${INVALIDS}
	GUI::Basic::Input Text::Validation	    bondDowndelay		${INVALIDS}

Test ARP Target
	SUITE:Setup
	@{INVALIDS}=	Create List	.	255	.3112.1	255-255-255-255	12;124	255.255.255.256
	GUI::Basic::Wait Until Element Is Accessible	bondMonitor
	Select From List By Value	bondMonitor	arp
	GUI::Basic::Input Text::Validation      bondArptarg     ${INVALIDS}

Check ARP Value
	SUITE:Setup
	Select From List By Value	bondMonitor	arp
	GUI::Basic::Wait Until Element Is Accessible        //*[@id="bondArpval"]
	@{CHK}=	Create List	None	Active      Backup      All
	@{ARPV}=	Get List Items	jquery=#bondArpval
	Should Be Equal	${CHK}	${ARPV}
	GUI::Basic::Input Text::Validation      bondArptarg     ${CHK}

Check Fail MAC
	GUI::Basic::Spinner Should Be Invisible
	@{CHK}=	Create List	Primary Interface	Current Active Interface	Follow Active Interface
	@{FAIM}=	Get List Items	jquery=#bondFailMac
	Should Be Equal	${CHK}	${FAIM}

*** Keywords ***
SUITE:Setup
	${CONNECTIONS}=	Create List	${CONNECTION}
	Set Suite Variable	${CONNECTIONS}	${CONNECTIONS}
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table If Exists	peerTable_wrapper	${CONNECTIONS}
	${HOST_HAS_ETH1}	GUI::Network::Check If Has Connection	ETH1
	Set Suite Variable	${HOST_HAS_ETH1}
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	jquery=#connName	${CONNECTION}
	Select From List By Value	jquery=#connType	${TYPE}

SUITE:Teardown
	Run Keyword And Continue On Failure	GUI::Basic::Button::Cancel
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table If Exists	peerTable_wrapper	${CONNECTIONS}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

GUI::Basic::Input Text::Validation
	[Arguments]     ${LOCATOR}      ${INVALIDS}
	[Documentation]	locator gets the id of the element to test. You must be on the proper page
	...	tests is the texts to be tested. All must be wrong except for the first input which will be set on the end
	FOR	${INVALID}	IN	@{INVALIDS}
		Run Keyword And Continue On Failure	GUI::Basic::Input Text::Error	${LOCATOR}	${INVALID}	${TRUE}
	END
