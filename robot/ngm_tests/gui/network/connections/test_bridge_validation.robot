*** Settings ***
Documentation	Testing the Bridge validation options for Network:Connections
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CONNECTION}	automation_test_bridge_interface
${TYPE}	bridge

*** Test Cases ***
Select Bridge Type
	Select From List By Value	jquery=#connType	${TYPE}
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	jquery=#bridgeTitle

Check Bridge Interfaces Presence
	Element Should Be Visible	jquery=#bridgeSlave

Check Spanning Tree Protocol Checkbox
	Page Should Contain Element	//*[@id="bridgeSTP"]

Test case about valid values to field=Bridge Interfaces
	[Tags]	NON-CRITICAL	BUG_NG_12965
	@{VALIDS}=	Create List	eth0
	FOR	${VALID}	IN	@{VALIDS}
		SUITE:Test Setup
		Input Text	jquery=#bridgeSlave	${VALID}
		Click Element	saveButton
		GUI::Basic::Spinner Should Be Invisible
		Page Should Contain Element	peerTable
		GUI::Basic::Page Should Not Contain Error
	END

Test case about valid values to field=Bridge Hello
	[Tags]	NON-CRITICAL	BUG_NG_12965
	@{VALIDS}=	Create List	2	6	10
	FOR	${VALID}	IN	@{VALIDS}
		SUITE:Test Setup
		Input Text	jquery=#bridgeHelloTime	${VALID}
		Input Text	jquery=#bridgeSlave		eth1
		Click Element	saveButton
		GUI::Basic::Spinner Should Be Invisible
		Page Should Contain Element	peerTable
		GUI::Basic::Page Should Not Contain Error
	END

Test case about invalid values to field=Bridge Hello
	@{INVALIDS}=	Create List	es	1-	d4	ae;	${EMPTY}	99999999	12.21.21.12	11	0
	FOR	${INVALID}	IN	@{INVALIDS}
		SUITE:Test Setup
		Input Text	jquery=#bridgeHelloTime	${INVALID}
		Click Element	saveButton
		GUI::Basic::Spinner Should Be Invisible
		Page Should Not Contain Element	peerTable
		GUI::Basic::Error Count Should Be	1
		Page Should Contain	Valid value is a number between 1 and 10.
	END

Test case about valid values to field=Bridge Forward
	[Tags]	NON-CRITICAL	BUG_NG_12965
	@{VALIDS}=	Create List	4	15	30
	FOR	${VALID}	IN	@{VALIDS}
		SUITE:Test Setup
		Input Text	jquery=#bridgeForwardDelay	${VALID}
		Input Text	jquery=#bridgeSlave		eth1
		Click Element	saveButton
		GUI::Basic::Spinner Should Be Invisible
		Page Should Contain Element	peerTable
		GUI::Basic::Page Should Not Contain Error
	END

Test case about invalid values to field=Bridge Forward
	@{INVALIDS}=	Create List	es	1-	d4	ae;	${EMPTY}	99999999	12.21.21.12	0
	FOR	${INVALID}	IN	@{INVALIDS}
		SUITE:Test Setup
		Input Text	jquery=#bridgeForwardDelay	${INVALID}
		Click Element	saveButton
		GUI::Basic::Spinner Should Be Invisible
		Page Should Not Contain Element	peerTable
		GUI::Basic::Error Count Should Be	1
		Page Should Contain	Valid value is a number between 4 and 30.
	END

Test case about valid values to field=Bridge Max
	[Tags]	NON-CRITICAL	BUG_NG_12965
	@{VALIDS}=	Create List	6	21	39
	FOR	${VALID}	IN	@{VALIDS}
		SUITE:Test Setup
		Input Text	jquery=#bridgeMaxAge	${VALID}
		Input Text	jquery=#bridgeSlave		eth1
		Click Element	saveButton
		GUI::Basic::Spinner Should Be Invisible
		Page Should Contain Element	peerTable
		GUI::Basic::Page Should Not Contain Error
	END

Test case about invalid values to field=Bridge Max
	@{INVALIDS}=	Create List	es	1-	d4	ae;	${EMPTY}	99999999	12.21.21.12	0
	FOR	${INVALID}	IN	@{INVALIDS}
		SUITE:Test Setup
		Input Text	jquery=#bridgeMaxAge	${INVALID}
		Click Element	saveButton
		GUI::Basic::Spinner Should Be Invisible
		Page Should Not Contain Element	peerTable
		GUI::Basic::Error Count Should Be	1
		Page Should Contain	Valid value is a number between 6 and 40.
	END

*** Keywords ***
SUITE:Setup
	Close All Browsers
	${CONNECTIONS}=	Create List	${CONNECTION}	bridge-eth0
	Set Suite Variable	${CONNECTIONS}	${CONNECTIONS}
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table If Exists	peerTable	${CONNECTIONS}	HAS_ALERT=${FALSE}
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	jquery=#connName	${CONNECTION}

SUITE:Teardown
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table If Exists	peerTable	${CONNECTIONS}	HAS_ALERT=${FALSE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

SUITE:Test Setup
	Run Keyword And Ignore Error	GUI::Basic::Button::Cancel
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table If Exists	peerTable	${CONNECTIONS}	HAS_ALERT=${FALSE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	jquery=#connName	${CONNECTION}
	Select From List By Value	jquery=#connType	${TYPE}
	GUI::Basic::Spinner Should Be Invisible