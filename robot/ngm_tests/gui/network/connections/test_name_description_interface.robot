*** Settings ***
Documentation	Testing the Name/Description options for Network:Connections
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CONNECTION}	validname

*** Test Cases ***
Add Network Connection
	GUI::Basic::Add
	Input Text	jquery=#ipv4dns	.--.--.--.9
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="globalerr"]/div/div/p
	Page Should Contain Element	jquery=#description

Test Network Name
	@{VALIDS}=	Create List	${CONNECTION}
	@{INVALIDS}=	Create List	n\\o	9alsonot	\#24network	..no..	.//./\\##
	GUI::Basic::Input Text::Validation	connName	${VALIDS}	${INVALIDS}

Test Connection Type Dropdown List
	@{CHK}=	Create List	Bonding	Ethernet	Mobile Broadband GSM	VLAN	WiFi	Bridge	Analog MODEM
	@{LST}=	Get List Items	jquery=#connType

*** Keywords ***
SUITE:Setup
	${CONNECTIONS}=	Create List	${CONNECTION}
	Set Suite Variable	${CONNECTIONS}	${CONNECTIONS}
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table If Exists	peerTable_wrapper	${CONNECTIONS}

SUITE:Teardown
	Run Keyword And Continue On Failure	GUI::Basic::Button::Cancel
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table If Exists	peerTable_wrapper	${CONNECTIONS}
	GUI::Basic::Logout And Close Nodegrid
