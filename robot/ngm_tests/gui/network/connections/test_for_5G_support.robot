*** Settings ***
Documentation	Test to verify 5G support
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${ROOT_PASSWORD}	${ROOT_PASSWORD}
${ADMIN_PASSWORD}	${DEFAULT_PASSWORD}
${STRONG_PASSWORD}	${QA_PASSWORD}
${CURRENT_ADMIN_PASSWORD}	${DEFAULT_PASSWORD}
${CONNECTION_NAME}	test5g
${INTERFACE_NAME}	interface
${APN}	apn
${IP_PUBLIC}	8.8.8.8
${CDC-WDM}	cdc-wdm
${WWAN}	wwan
${RESULT_EXPECT_OF_PING}	64 bytes from 8.8.8.8

*** Test Cases ***
Test case to verify if device is a NSR
	[Documentation]	Check if the device is NSR
	CLI:Open	admin	${CURRENT_ADMIN_PASSWORD}
	${SYSTEM_NAME}	CLI:Get System Name
	CLI:Close Connection
	${IS_NSR}	Run Keyword And Return Status	Should Be True	"""Nodegrid Net SR""" in """${SYSTEM_NAME}"""
	Set Suite Variable	${IS_NSR}
	Skip If	not ${IS_NSR}	This device is not a NSR
	Log	This device is a NSR

Test case to verify if device has GSM
	[Documentation]	Checks if the device has GSM module
	${HAS_GSM}	GUI::Tracking::Check If Has GSM Module and Sim Card with Signal
	Set Suite Variable	${HAS_GSM}
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	Log	This device has GSM

Test case to create a GSM Connection
	[Tags]	NON-CRITICAL	NEED-REVIEW
	[Documentation]	Create a new GSM connection using the GSM module
	Skip If	not ${IS_NSR}	This device is not a NSR
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	${NUM_ELEMENTS_OF_TABLE}	Get Element Count	xpath://table[@id='wmodemTable']/tbody/tr
	${REGISTRED_DEVICE}	SUITE:Check if device as registred	${NUM_ELEMENTS_OF_TABLE}
	${INTERFACE}	Get text	xpath://table[@id='wmodemTable']/tbody/tr[${REGISTRED_DEVICE}]/td[2]
	Set Suite Variable	${INTERFACE_NAME}	${INTERFACE}	#CHANGE
	${GSM_OPERATOR_NAME}	Get text	xpath://table[@id='wmodemTable']/tbody/tr[${REGISTRED_DEVICE}]/td[7]
	${APN_SELECTED}	SUITE:Check APN for GSM Operator	${GSM_OPERATOR_NAME}
	Set Suite Variable	${APN}	${APN_SELECTED}
	SUITE:Create a new GSM connection	${CONNECTION_NAME}	${GSM_TYPE_GUI}	${INTERFACE_NAME}	${APN}
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${CONNECTION_NAME}

Test case to validate GSM connectivity
	[Tags]	NON-CRITICAL	NEED-REVIEW
	[Documentation]	Tests GSM connection connectivity to an external IP
	Skip If	not ${IS_NSR}	This device is not a NSR
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	GUI::Basic::Click Element	(//span[contains(.,'System')])[1]
	GUI::Basic::Click Element	(//a[contains(.,'Toolkit')])[2]
	GUI::Basic::Click Element	//a[@id='icon_NetworkTools']
	GUI::Basic::Input Text	//input[@id='ipaddress']	${IP_PUBLIC}
	${NUMBER_OF_INTERFACE}	remove string	${INTERFACE_NAME}	${CDC-WDM}
	Select From List By Label	//select[contains(@id,'interface')]	${WWAN}${NUMBER_OF_INTERFACE}
	GUI::Basic::Click Element	//input[contains(@value,'Ping')]
	Sleep	10s
	Page Should Contain	${RESULT_EXPECT_OF_PING}
	GUI::Basic::Click Element	//input[contains(@id,'cancelButton')]

Test case to Delete GSM Connection
	[Tags]	NON-CRITICAL	NEED-REVIEW
	[Documentation]	Deletes the GSM connection created for the test
	Skip If	not ${IS_NSR}	This device is not a NSR
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	GUI::Basic::Click Element	(//span[contains(.,'Network')])[1]
	GUI::Basic::Click Element	(//a[contains(.,'Connections')])[2]
	GUI::Basic::Click Element	//tr[@id='${CONNECTION_NAME}']//input[@type='checkbox']
	GUI::Basic::Delete
	Page Should Not Contain	${CONNECTION_NAME}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Change passwords of admin and root to strong passwords

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
	Close All Browsers
	SUITE:Change passwords of admin and root to default passwords

### MY SUITES ###
SUITE:Change passwords of admin and root to strong passwords
	[Documentation]	Change admin user and root user password to strong password
	CLI:Open	${USERNAME}	${ADMIN_PASSWORD}
	CLI:Change User Password From Shell	root	${STRONG_PASSWORD}	${ROOT_PASSWORD}
	CLI:Change User Password From Shell	${USERNAME}	${STRONG_PASSWORD}	${STRONG_PASSWORD}
	CLI:Close Connection
	${CURRENT_ADMIN_PASSWORD}	Set Variable	${STRONG_PASSWORD}
	Set Suite Variable	${CURRENT_ADMIN_PASSWORD}

SUITE:Change passwords of admin and root to default passwords
	[Documentation]	Change admin user and root user password to default password
	CLI:Open	admin	${CURRENT_ADMIN_PASSWORD}
	CLI:Change User Password From Shell	root	${ROOT_PASSWORD}	${STRONG_PASSWORD}
	CLI:Change User Password From Shell	admin	${ADMIN_PASSWORD}	${ROOT_PASSWORD}
	CLI:Close Connection
	${CURRENT_ADMIN_PASSWORD}	Set Variable	${ADMIN_PASSWORD}
	Set Suite Variable	${CURRENT_ADMIN_PASSWORD}
	Log	Changed the passwords of admin and root to: ${CURRENT_ADMIN_PASSWORD}	INFO	console=yes

SUITE:Check if device as registred
	[Documentation]	Checks if the GSM device is registered to the network
	[Arguments]	${SIZE_OF_TABLE}=0
	${REGISTRED}	Set Variable	False
	FOR	${ELEMENT}	IN RANGE	${SIZE_OF_TABLE}
		${DEVICE_HAS_REGISTRED}	Get Text	xpath://table[@id='wmodemTable']/tbody/tr[${ELEMENT+1}]/td[4]
		${REGISTRED}	Run Keyword And Return Status	Should Be True	"""Registered""" in """${DEVICE_HAS_REGISTRED}"""
		IF	${REGISTRED}
			RETURN	${ELEMENT+1}
		END
	END

SUITE:Check APN for GSM Operator
	[Documentation]	Check the APN for the specific carrier of the SIM card
	[Arguments]	${OPERATOR}=''
	${APN}	Set Variable	''
	IF	'${OPERATOR}'=='Verizon Wireless'
		${APN}	Set Variable	vzwinternet
	ELSE IF	'${OPERATOR}'=='AT&T'
		${APN}	Set Variable	I2GOLD
	ELSE
		${APN}	Set Variable	''
	END
	return from keyword	${APN}

SUITE:Create a new GSM connection
	[Documentation]	Create a new GSM connection
	[Arguments]	${NAME}	${TYPE}	${INTERFACE}	${APN}
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Add
	GUI::Basic::Input Text	//input[contains(@id,'connName')]	${NAME}
	GUI::Basic::Click Element	//select[@id='connType']
	Select From List By Label	//select[@id='connType']	${TYPE}
	GUI::Basic::Click Element	//select[@id='connItfName']
	Select From List By Label	//select[@id='connItfName']	${INTERFACE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text	//input[@id='mobileAPN']	${APN}
	GUI::Basic::Click Element	//input[@id='gnssStatus']
	GUI::Basic::Click Element	//input[@id='gnssPolltime']
	GUI::Basic::Input Text	//input[@id='gnssPolltime']	1
	GUI::Basic::Save
