*** Settings ***
Documentation	Functionality test cases about heart beat for cellular module
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EXCLUDEIN4_2	EXCLUDEIN5_0

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CELLULAR_CONNECTION}	${GSM_CELLULAR_CONNECTION}
${CELLULAR_CONNECTION_CLONE}	${GSM_CELLULAR_CONNECTION}_clone
${TYPE}	${GSM_TYPE_GUI}
${STRONG_PASSWORD}	${QA_PASSWORD}
${REACHABLE_IP}	8.8.8.8
${NON_REACHABLE_IP}	0.1.2.3
${FAILOVER_PRIMARY_CONNECTION}	ETH0
${FAILOVER_SECONDARY_CONNECTION}	${CELLULAR_CONNECTION}
${DEFAULT_SECONDARY_CONNECTION}	ETH1

*** Test Cases ***
Test case to get GSM operator name and interface information
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	${INDEX_NUMBER}	GUI::Tracking::Get Index Number With 5G Sim Card Up Otherwise 4G Up
	${ROW_NUMBER}	Evaluate	${INDEX_NUMBER} + 1
	${GSM_CDC_WDMX}	GUI::Tracking::Get Tracking Wireless Modem Table Cell	ROW=${ROW_NUMBER}	COLUMN=2
	Set Suite Variable	${GSM_CDC_WDMX}
	${GSM_OPERATOR_NAME}	GUI::Tracking::Get Tracking Wireless Modem Table Cell	ROW=${ROW_NUMBER}	COLUMN=7
	Set Suite Variable	${GSM_OPERATOR_NAME}
	${ROOT_GSM_INTERFACE}	GUI::Tracking::Rewrite to root GSM interface format	${GSM_CDC_WDMX}
	Set Suite Variable	${ROOT_GSM_INTERFACE}

Test case to add new GSM connection with health monitoring and turn it down
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	SUITE:Add GSM connection with health monitoring
	SUITE:Check if task in scheduler is automatically created
	SUITE:Turn GSM Connection Down to Validate Ensure Connection Up Functionality

Test case to decrease the time for the health monitoring and check if GSM connection is up
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	Wait Until Keyword Succeeds	3x	3s	SUITE:Reset counters in event list
	SUITE:Clone task automatically created in scheduler feature
	SUITE:Decrease the time for the health check through CLI as root user
	Wait Until Keyword Succeeds	3x	3s	SUITE:Check if clone task automatically restarts every minute
	SUITE:Check if GSM Connection is Up
	Wait Until Keyword Succeeds	3x	3s	SUITE:Check for an increase in event counters 146 and 140 in the event list

Test case to ping external network using mobile connection in network tools
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	SUITE:Check if GSM Connection is Up
	Wait Until Keyword Succeeds	3x	3s	SUITE:Ping external network using mobile connection in network tools
	[Teardown]	SUITE:Teardown in network tools

Test case to ping non-reachable network using mobile connection in network tools
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	SUITE:Check if GSM Connection is Up
	SUITE:Ping non-reachable network using mobile connection in network tools
	[Teardown]	SUITE:Teardown in network tools

Test case using non-reachable IP Address in health monitoring
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	SUITE:Turn GSM Connection Down to Validate Ensure Connection Up Functionality
	Wait Until Keyword Succeeds	3x	3s	SUITE:Reset counters in event list
	SUITE:Edit GSM Connection with another IP address	${NON_REACHABLE_IP}
	Wait Until Keyword Succeeds	3x	3s	SUITE:Check if clone task automatically restarts every minute
	SUITE:Check for an increase in event counter 147 in the event list

Test case to verify if integration with failover feature is not raising false-positives
	[Tags]	NON-CRITICAL	NEED-REVIEW
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	[Setup]	SUITE:Setup failover
	SUITE:Edit GSM Connection with another IP address	${REACHABLE_IP}
	SUITE:Turn GSM Connection Down to Validate Ensure Connection Up Functionality
	Wait Until Keyword Succeeds	3x	3s	SUITE:Reset counters in event list
	Wait Until Keyword Succeeds	3x	3s	SUITE:Check if clone task automatically restarts every minute
	Wait Until Keyword Succeeds	15x	3s	SUITE:Check for an increase in event counter 140 and 146 in the event list
	Wait Until Keyword Succeeds	3x	3s	SUITE:Check for not increase in event counter 144 and 145 in the event list
	[Teardown]	SUITE:Teardown failover

Test case to delete the GSM connection with health monitoring and clone scheduler task
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid	PASSWORD=${STRONG_PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Delete GSM connection with health monitoring
	SUITE:Check if task in scheduler is automatically deleted
	SUITE:Delete clone scheduler task

*** Keywords ***
SUITE:Setup
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	${HAS_GSM}	GUI::Tracking::Check If Has GSM Module and Sim Card with Signal
	Set Suite Variable	${HAS_GSM}
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	#	CLI: Change password
	CLI:Open
	SUITE:Change passwords of admin and root to strong passwords
	CLI:Close Connection

SUITE:Teardown
	Run Keyword If	not ${HAS_GSM}	Close All Browsers
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
#	GUI: Teardown
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid	PASSWORD=${STRONG_PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout and Close Nodegrid
#	CLI: Teardown
	CLI:Open	PASSWORD=${STRONG_PASSWORD}
	SUITE:Change passwords of admin and root to default passwords
	CLI:Close Connection

SUITE:Check if has wireless modem support
	${HAS_WMODEM_SUPPORT}=	CLI:Has Wireless Modem Support
	Set Suite Variable	${HAS_WMODEM_SUPPORT}
	Log	Has Wireless Modem Support: ${HAS_WMODEM_SUPPORT}	INFO	console=yes

SUITE:Check if has wireless modem with sim card
	${HAS_WMODEM}	${MODEM_INFO}=	CLI:Get Wireless Modems SIM Card Info	0
	Set Suite Variable	${HAS_WMODEM}
	Log	Has Wireless Modem: ${HAS_WMODEM}	INFO	console=yes
	Set Suite Variable	${MODEM_INFO}
	Log	Modem Info: ${MODEM_INFO}	INFO	console=yes

SUITE:Change passwords of admin and root to strong passwords
	CLI:Change User Password From Shell	root	${STRONG_PASSWORD}	${ROOT_PASSWORD}
	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${STRONG_PASSWORD}	${STRONG_PASSWORD}
	Log	Changed the passwords of admin and root to: ${STRONG_PASSWORD}	INFO	console=yes

SUITE:Change passwords of admin and root to default passwords
	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}	${STRONG_PASSWORD}
	CLI:Change User Password From Shell	root	${ROOT_PASSWORD}	${STRONG_PASSWORD}
	Log	The passwords of admin and root users came back to default: ${DEFAULT_PASSWORD} and ${ROOT_PASSWORD}	INFO	console=yes

SUITE:Add GSM connection with health monitoring
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Button::Add
	Click Element	id=connName
	Input Text	//*[@id="connName"]	${CELLULAR_CONNECTION}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=connType
	Select From List By Label	//*[@id="connType"]	${TYPE}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=connItfName
	Select From List By Label	//*[@id="connItfName"]	${GSM_CDC_WDMX}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=mobileAPN
	Input Text	//*[@id="mobileAPN"]	${GSM_WMODEM_CARRIERS_APN["${GSM_OPERATOR_NAME}"]}
	Log	\nGSM_WMODEM_CARRIERS_APN.keys(${GSM_OPERATOR_NAME}): \n${GSM_WMODEM_CARRIERS_APN["${GSM_OPERATOR_NAME}"]}	INFO	console=yes
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=connHealth
	Click Element	id=connHealthEnsureUp
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	//*[@id="connItfName"]
	Element Should Be Visible	//*[@id="description"]
	Element Should Be Visible	//*[@id="connAuto"]
	Element Should Be Visible	//*[@id="primaryConn"]
	Element Should Be Visible	//*[@id="lldp"]
	Element Should Be Visible	//*[@id="connHealthEnsureUp"]
	Element Should Be Visible	//*[@id="connHealthAddress"]
	Element Should Be Visible	//*[@id="connHealthInterval"]
	GUI::Basic::Save

SUITE:Delete GSM connection with health monitoring
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	Select Checkbox	css=#${CELLULAR_CONNECTION} input
	GUI::Basic::Delete
	Element Should Not Be Visible	css=#${CELLULAR_CONNECTION}
	GUI::Basic::Spinner Should Be Invisible

SUITE:Check if task in scheduler is automatically created
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Scheduler::Open Tab
	Page Should Contain	Task Name
	Table Should Contain	taskTable	${CELLULAR_CONNECTION}_healthCheck
	Table Should Contain	taskTable	Enabled

SUITE:Check if task in scheduler is automatically deleted
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Scheduler::Open Tab
	Page Should Contain	Task Name
	Page Should Not Contain	${CELLULAR_CONNECTION}_healthCheck

SUITE:Delete clone scheduler task
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Scheduler::Open Tab
	${STATUS}=	Run Keyword And Return Status	Select Checkbox	css=#${CELLULAR_CONNECTION_CLONE} input
	Run Keyword If	${STATUS}	GUI::Basic::Delete
	Element Should Not Be Visible	css=#${CELLULAR_CONNECTION_CLONE}
	GUI::Basic::Spinner Should Be Invisible

SUITE:Turn GSM Connection Down to Validate Ensure Connection Up Functionality
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	Table Should Contain	peerTable	${CELLULAR_CONNECTION}
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	css=#${CELLULAR_CONNECTION} input
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Enabled	//*[@id="nonAccessControls"]/input[4]	error=Cannot down connection because down connection button is disable
	Click Element	//*[@id="nonAccessControls"]/input[4]
	GUI::Basic::Spinner Should Be Invisible
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	${CELLULAR_CONNECTION}\\s+\\w+\\s+\\w+\\s+${TYPE}\\s+${GSM_CDC_WDMX}\\s+Down

SUITE:Reset counters in event list
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Tracking::Event List::Open Tab
	Select Checkbox	//*[@id="thead"]/tr/th[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Enabled	//*[@id="nonAccessControls"]/input	error=Cannot reset counters because Reset Counters button is disable/not found
	Click Element	//*[@id="nonAccessControls"]/input
	GUI::Basic::Spinner Should Be Invisible
	${TABLE_CONTENTS}=	Get Text	eventListTable
	Log	\nTable contents in Tracking::Event List::Statistics: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Not Match Regexp	${TABLE_CONTENTS}	...+.*[1-9]\\d*\\s+\\w+\\s+(\\w+\\s+Event|Event)

SUITE:Clone task automatically created in scheduler feature
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Scheduler::Open Tab
	Select Checkbox	css=#${CELLULAR_CONNECTION}_healthCheck input
	Wait Until Element Is Enabled	//*[@id="clone_task"]	error=Cannot clone task because Clone button is disable
	Click Element	//*[@id="clone_task"]
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	//*[@id="clone_name"]
	Input Text	//*[@id="clone_name"]	${CELLULAR_CONNECTION_CLONE}
	GUI::Basic::Save

SUITE:Decrease the time for the health check through CLI as root user
	CLI:Connect As Root	PASSWORD=${STRONG_PASSWORD}
	CLI:Write	sed -i 's/^.*root/\\t*\\t*\\t*\\t*\\t*\\troot/' /etc/cron.d/${CELLULAR_CONNECTION_CLONE}
	CLI:Write	/etc/init.d/ccm restart
	CLI:Close Connection

SUITE:Check if clone task automatically restarts every minute
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Tracking::Scheduler::Open Tab
	Wait Until Element Is Enabled	//*[@id="reset_task_log"]	error=Cannot reset the logs because Reset Log button is disable
	Click Element	//*[@id="reset_task_log"]
	GUI::Basic::Spinner Should Be Invisible
	${TABLE_CONTENTS}=	Get Text	ScheduleLogTable
	Log	\nTable contents in Tracking::Scheduler: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Not Match Regexp	${TABLE_CONTENTS}	${CELLULAR_CONNECTION_CLONE}\\s+root\\s+.*Task Started
	Should Not Match Regexp	${TABLE_CONTENTS}	${CELLULAR_CONNECTION_CLONE}\\s+root\\s+.*Task Ended
	Sleep	1m	3s
	Wait Until Element Is Enabled	//*[@id="refresh-icon"]	error=Cannot Reload the page because Reload button is disable/not appears
	Click Element	//*[@id="refresh-icon"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Tracking::Scheduler::Open Tab
	${TABLE_CONTENTS}=	Get Text	ScheduleLogTable
	Log	\nTable contents in Tracking::Scheduler: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	${CELLULAR_CONNECTION_CLONE}\\s+root\\s+.*Task Started
	Should Match Regexp	${TABLE_CONTENTS}	${CELLULAR_CONNECTION_CLONE}\\s+root\\s+.*Task Ended

SUITE:Check if GSM Connection is Up
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	${CELLULAR_CONNECTION}\\s+\\w+\\s+${TYPE}\\s+${GSM_CDC_WDMX}\\s+Up

SUITE:Check for an increase in event counters 146 and 140 in the event list
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Tracking::Event List::Open Tab
	${TABLE_CONTENTS}=	Get Text	eventListTable
	Log	\nTable contents in Tracking::Event List::Statistics: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	140+.*[1-9]\\d*\\s+\\w+\\s+(\\w+\\s+Event|Event)
	Should Match Regexp	${TABLE_CONTENTS}	146+.*[1-9]\\d*\\s+\\w+\\s+(\\w+\\s+Event|Event)

SUITE:Ping external network using mobile connection in network tools
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Toolkit::open tab
	Click Element	id=icon_NetworkTools
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	//*[@id="connection"]
	Element Should Be Visible	//*[@id="ipaddresstest"]
	Click Element	//*[@id="networktools"]/div[4]/div/div/input
	GUI::Basic::Spinner Should Be Invisible
	${COMMAND_OUTPUT}=	Wait Until Keyword Succeeds	1m	3s	GUI::System::Toolkit::Network Tools::Get text in command output
	Should Match Regexp	${COMMAND_OUTPUT}	Ping Result: OK
	Should Match Regexp	${COMMAND_OUTPUT}	Success: Connection ${CELLULAR_CONNECTION} has internet access
	GUI::Basic::Cancel

SUITE:Ping non-reachable network using mobile connection in network tools
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Toolkit::open tab
	Click Element	id=icon_NetworkTools
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	//*[@id="connection"]
	Element Should Be Visible	//*[@id="ipaddresstest"]
	Input Text	//*[@id="ipaddresstest"]	${NON_REACHABLE_IP}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="networktools"]/div[4]/div/div/input
	GUI::Basic::Spinner Should Be Invisible
	${COMMAND_OUTPUT}=	Wait Until Keyword Succeeds	1m	3s	GUI::System::Toolkit::Network Tools::Get text in command output
	Should Match Regexp	${COMMAND_OUTPUT}	Ping Result: NOK
	Should Match Regexp	${COMMAND_OUTPUT}	Diagnostic Result: Connection cannot reach configured IP Address - ping failed
	GUI::Basic::Cancel
	[Teardown]	SUITE:Teardown in network tools

SUITE:Teardown in network tools
	${STATUS}=	Run Keyword And Return Status	Element Should Be Visible	//*[@id="ipaddresstest"]
	Run Keyword If	${STATUS}	GUI::Basic::Cancel

SUITE:Edit GSM Connection with another IP address
	[Arguments]	${IP_ADDRESS}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	Click Link	${CELLULAR_CONNECTION}
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	//*[@id="connHealthAddress"]
	Input Text	//*[@id="connHealthAddress"]	${IP_ADDRESS}
	GUI::Basic::Save

SUITE:Check for an increase in event counter 147 in the event list
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Tracking::Event List::Open Tab
	${TABLE_CONTENTS}=	Get Text	eventListTable
	Log	\nTable contents in Tracking::Event List::Statistics: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	147+.*[1-9]\\d*\\s+\\w+\\s+(\\w+\\s+Event|Event)

SUITE:Setup failover
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::Open Tab
	Wait Until Element Is Visible	//*[@id="failover"]	error=Cannot Select Checkbox "Enable Network Failover" because it is not visible
	Select Checkbox	//*[@id="failover"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="primaryConn"]
	Select From List By Label	//*[@id="primaryConn"]	${FAILOVER_PRIMARY_CONNECTION}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="secondaryConn"]
	Select From List By Label	//*[@id="secondaryConn"]	${FAILOVER_SECONDARY_CONNECTION}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save

SUITE:Teardown failover
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid	PASSWORD=${STRONG_PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::Open Tab
	Wait Until Element Is Visible	//*[@id="primaryConn"]	error=Cannot Click Element "Primary Connection:" because it is not visible
	Click Element	//*[@id="primaryConn"]
	Select From List By Label	//*[@id="primaryConn"]	${FAILOVER_PRIMARY_CONNECTION}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	//*[@id="secondaryConn"]	error=Cannot Click Element "Secondary Connection:" because it is not visible
	Click Element	//*[@id="secondaryConn"]
	Select From List By Label	//*[@id="secondaryConn"]	${DEFAULT_SECONDARY_CONNECTION}
	Wait Until Element Is Visible	//*[@id="failover"]	error=Cannot Unselect Checkbox "Enable Network Failover" because it is not visible
	Unselect Checkbox	//*[@id="failover"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save

SUITE:Check for an increase in event counter 140 and 146 in the event list
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Tracking::Event List::Open Tab
	${TABLE_CONTENTS}=	Get Text	eventListTable
	Log	\nTable contents in Tracking::Event List::Statistics: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	140+.*[1-9]\\d*\\s+\\w+\\s+(\\w+\\s+Event|Event)
	Should Match Regexp	${TABLE_CONTENTS}	146+.*[1-9]\\d*\\s+\\w+\\s+(\\w+\\s+Event|Event)

SUITE:Check for not increase in event counter 144 and 145 in the event list
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Tracking::Event List::Open Tab
	${TABLE_CONTENTS}=	Get Text	eventListTable
	Log	\nTable contents in Tracking::Event List::Statistics: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Not Match Regexp	${TABLE_CONTENTS}	144+.*[1-9]\\d*\\s+\\w+\\s+(\\w+\\s+Event|Event)
	Should Not Match Regexp	${TABLE_CONTENTS}	145+.*[1-9]\\d*\\s+\\w+\\s+(\\w+\\s+Event|Event)