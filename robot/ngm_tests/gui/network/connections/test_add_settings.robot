*** Settings ***
Documentation	Testing the options for adding a connection in Network:Connections
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test Analog Modem Settings Options
	Select From List By Value	jquery=#connType	generic
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' <= '4.2'  Element Should Not Be Visible	//*[@id="connIeth"]
	Run Keyword If	'${NGVERSION}' >= '5.0'  Element Should Not Be Visible	//*[@id="connItfName"]
	Element Should Be Visible	//*[@id="description"]
	Element Should Be Visible	//*[@id="connAuto"]
	Element Should Not Be Visible	//*[@id="primaryConn"]
	Element Should Not Be Visible	//*[@id="lldp"]

Test Bridge Settings Options
	Select From List By Value	jquery=#connType	bridge
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' <= '4.2'  Element Should Not Be Visible	//*[@id="connIeth"]
	Run Keyword If	'${NGVERSION}' >= '5.0'  Element Should Not Be Visible	//*[@id="connItfName"]
	Element Should Be Visible	//*[@id="description"]
	Element Should Be Visible	//*[@id="connAuto"]
	Element Should Be Visible	//*[@id="primaryConn"]
	Element Should Not Be Visible	//*[@id="lldp"]

Test WiFi Settings Options
	Select From List By Value	jquery=#connType	wifi
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' <= '4.2'  Element Should Be Visible	//*[@id="connIeth"]
	Run Keyword If	'${NGVERSION}' >= '5.0'  Element Should Be Visible	//*[@id="connItfName"]
	Element Should Be Visible	//*[@id="description"]
	Element Should Be Visible	//*[@id="connAuto"]
	Element Should Be Visible	//*[@id="primaryConn"]
	Element Should Be Visible	//*[@id="lldp"]

Test VLAN Settings Options
	Select From List By Value	jquery=#connType	vlan
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' <= '4.2'  Element Should Be Visible	//*[@id="connIeth"]
	Run Keyword If	'${NGVERSION}' >= '5.0'  Element Should Be Visible	//*[@id="connItfName"]
	Element Should Be Visible	//*[@id="description"]
	Element Should Be Visible	//*[@id="connAuto"]
	Element Should Be Visible	//*[@id="primaryConn"]
	Element Should Not Be Visible	//*[@id="lldp"]

Test GSM Settings Options
	Select From List By Value	jquery=#connType	gsm
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' <= '4.2'  Element Should Be Visible	//*[@id="connIeth"]
	Run Keyword If	'${NGVERSION}' >= '5.0'  Element Should Be Visible	//*[@id="connItfName"]
	Element Should Be Visible	//*[@id="description"]
	Element Should Be Visible	//*[@id="connAuto"]
	Element Should Be Visible	//*[@id="primaryConn"]
	Element Should Be Visible	//*[@id="lldp"]

Test Ethernet Settings Options
	Select From List By Value	jquery=#connType	ethernet
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' <= '4.2'  Element Should Be Visible	//*[@id="connIeth"]
	Run Keyword If	'${NGVERSION}' >= '5.0'  Element Should Be Visible	//*[@id="connItfName"]
	Element Should Be Visible	//*[@id="description"]
	Element Should Be Visible	//*[@id="connAuto"]
	Element Should Be Visible	//*[@id="primaryConn"]
	Element Should Be Visible	//*[@id="lldp"]

Test Bonding Settings Options
	Select From List By Value	jquery=#connType	bond
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' <= '4.2'  Element Should Not Be Visible	//*[@id="connIeth"]
	Run Keyword If	'${NGVERSION}' >= '5.0'  Element Should Not Be Visible	//*[@id="connItfName"]
	Element Should Be Visible	//*[@id="description"]
	Element Should Be Visible	//*[@id="connAuto"]
	Element Should Be Visible	//*[@id="primaryConn"]
	Element Should Be Visible	//*[@id="lldp"]

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Button::Cancel
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

