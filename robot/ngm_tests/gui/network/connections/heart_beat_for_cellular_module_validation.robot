*** Settings ***
Documentation	Validation test cases about heart beat for cellular module
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EXCLUDEIN4_2	EXCLUDEIN5_0

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CELLULAR_CONNECTION}	${GSM_CELLULAR_CONNECTION}
${TYPE}	${GSM_TYPE_GUI}
${STRONG_PASSWORD}	${QA_PASSWORD}
@{VALID_IP_DNS_ADDRESSES}	google.com	8.8.4.4	zpesystems.com
@{VALID_INTERVALS}	1	20	2
@{INVALID_VALUES}	${EMPTY}	${POINTS}	${ELEVEN_POINTS}	${EXCEEDED}

*** Test Cases ***
Test case to get GSM operator name and interface information
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	${INDEX_NUMBER}	GUI::Tracking::Get Index Number With 5G Sim Card Up Otherwise 4G Up
	${ROW_NUMBER}	Evaluate	${INDEX_NUMBER} + 1
	${GSM_CDC_WDMX}	GUI::Tracking::Get Tracking Wireless Modem Table Cell	ROW=${ROW_NUMBER}	COLUMN=2
	Set Suite Variable	${GSM_CDC_WDMX}
	${GSM_OPERATOR_NAME}	GUI::Tracking::Get Tracking Wireless Modem Table Cell	ROW=${ROW_NUMBER}	COLUMN=7
	Set Suite Variable	${GSM_OPERATOR_NAME}
	${ROOT_GSM_INTERFACE}	GUI::Tracking::Rewrite to root GSM interface format	${GSM_CDC_WDMX}
	Set Suite Variable	${ROOT_GSM_INTERFACE}

Test case to add GSM connection
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	SUITE:Add GSM connection with health monitoring

Test case for valid values on field="IP Address:" in Network::Connections::${CELLULAR_CONNECTION} tab
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	FOR	${VALID_IP_DNS_ADDRESS}	IN	@{VALID_IP_DNS_ADDRESSES}
		SUITE:Edit GSM Connection with valid value	IP_ADDRESS=${VALID_IP_DNS_ADDRESS}
	END

Test case for invalid values on field="IP Address:" in Network::Connections::${CELLULAR_CONNECTION} tab
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	FOR	${INVALID_VALUE}	IN	@{INVALID_VALUES}
		SUITE:Edit GSM Connection with invalid value	Invalid IP address.	IP_ADDRESS=${INVALID_VALUE}
	END

Test case for valid values on field="Interval (hours):" in Network::Connections::${CELLULAR_CONNECTION} tab
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	FOR	${VALID_INTERVAL}	IN	@{VALID_INTERVALS}
		SUITE:Edit GSM Connection with valid value	INTERVAL=${VALID_INTERVAL}
	END

Test case for invalid values on field="Interval (hours):" in Network::Connections::${CELLULAR_CONNECTION} tab
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	FOR	${INVALID_VALUE}	IN	@{INVALID_VALUES}
		SUITE:Edit GSM Connection with invalid value	Interval hour must be between 1 and 24	INTERVAL=${INVALID_VALUE}
	END

Test case for valid and invalid values on field="Connection:" in System::Toolkit::Network Tools tab
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	[Setup]	SUITE:Edit GSM Connection with valid value
	SUITE:Check list selection on field="Connection:" in System::Toolkit::Network Tools tab

Test case for valid values on field="IP Address:" in System::Toolkit::Network Tools tab
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	FOR	${VALID_IP_DNS_ADDRESS}	IN	@{VALID_IP_DNS_ADDRESSES}
		SUITE:Ping external network using mobile connection in network tools	${VALID_IP_DNS_ADDRESS}
	END
	[Teardown]	SUITE:Teardown in network tools

Test case for invalid values on field="IP Address:" in System::Toolkit::Network Tools tab
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	${INVALID_VALUES}	Create List	${POINTS}	${ELEVEN_POINTS}	${EXCEEDED}
	FOR	${INVALID_VALUE}	IN	@{INVALID_VALUES}
		SUITE:Ping non-reachable network using mobile connection in network tools	${INVALID_VALUE}
	END
	[Teardown]	SUITE:Teardown in network tools

*** Keywords ***
SUITE:Setup
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	${HAS_GSM}	GUI::Tracking::Check If Has GSM Module and Sim Card with Signal
	Set Suite Variable	${HAS_GSM}
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	#	CLI: Change password
	CLI:Open
	SUITE:Change passwords of admin and root to strong passwords
	CLI:Close Connection

SUITE:Teardown
	Run Keyword If	not ${HAS_GSM}	Close All Browsers
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
#	GUI: Teardown
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid	PASSWORD=${STRONG_PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Delete GSM connection with health monitoring
	SUITE:Check if task in scheduler is automatically deleted
	GUI::Basic::Logout and Close Nodegrid
#	CLI: Teardown
	CLI:Open	PASSWORD=${STRONG_PASSWORD}
	SUITE:Change passwords of admin and root to default passwords
	CLI:Close Connection

SUITE:Check if has wireless modem support
	${HAS_WMODEM_SUPPORT}=	CLI:Has Wireless Modem Support
	Set Suite Variable	${HAS_WMODEM_SUPPORT}
	Log	Has Wireless Modem Support: ${HAS_WMODEM_SUPPORT}	INFO	console=yes

SUITE:Check if has wireless modem with sim card
	${HAS_WMODEM}	${MODEM_INFO}=	CLI:Get Wireless Modems SIM Card Info	0
	Set Suite Variable	${HAS_WMODEM}
	Log	Has Wireless Modem: ${HAS_WMODEM}	INFO	console=yes
	Set Suite Variable	${MODEM_INFO}
	Log	Modem Info: ${MODEM_INFO}	INFO	console=yes

SUITE:Change passwords of admin and root to strong passwords
	CLI:Change User Password From Shell	root	${STRONG_PASSWORD}	${ROOT_PASSWORD}
	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${STRONG_PASSWORD}	${STRONG_PASSWORD}
	Log	Changed the passwords of admin and root to: ${STRONG_PASSWORD}	INFO	console=yes

SUITE:Change passwords of admin and root to default passwords
	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}	${STRONG_PASSWORD}
	CLI:Change User Password From Shell	root	${ROOT_PASSWORD}	${STRONG_PASSWORD}
	Log	The passwords of admin and root users came back to default: ${DEFAULT_PASSWORD} and ${ROOT_PASSWORD}	INFO	console=yes

SUITE:Add GSM connection with health monitoring
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Button::Add
	Click Element	id=connName
	Input Text	//*[@id="connName"]	${CELLULAR_CONNECTION}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=connType
	Select From List By Label	//*[@id="connType"]	${TYPE}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=connItfName
	Select From List By Label	//*[@id="connItfName"]	${GSM_CDC_WDMX}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=mobileAPN
	Input Text	//*[@id="mobileAPN"]	${GSM_WMODEM_CARRIERS_APN["${GSM_OPERATOR_NAME}"]}
	Log	\nGSM_WMODEM_CARRIERS_APN.keys(${GSM_OPERATOR_NAME}): \n${GSM_WMODEM_CARRIERS_APN["${GSM_OPERATOR_NAME}"]}	INFO	console=yes
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=connHealth
	Click Element	id=connHealthEnsureUp
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	//*[@id="connItfName"]
	Element Should Be Visible	//*[@id="description"]
	Element Should Be Visible	//*[@id="connAuto"]
	Element Should Be Visible	//*[@id="primaryConn"]
	Element Should Be Visible	//*[@id="lldp"]
	Element Should Be Visible	//*[@id="connHealthEnsureUp"]
	Element Should Be Visible	//*[@id="connHealthAddress"]
	Element Should Be Visible	//*[@id="connHealthInterval"]
	GUI::Basic::Save

SUITE:Delete GSM connection with health monitoring
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	Select Checkbox	css=#${CELLULAR_CONNECTION} input
	GUI::Basic::Delete
	Element Should Not Be Visible	css=#${CELLULAR_CONNECTION}
	GUI::Basic::Spinner Should Be Invisible

SUITE:Check if task in scheduler is automatically deleted
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Scheduler::Open Tab
	Page Should Contain	Task Name
	Page Should Not Contain	${CELLULAR_CONNECTION}_healthCheck

SUITE:Edit GSM Connection with valid value
	[Arguments]	${IP_ADDRESS}=8.8.8.8	${INTERVAL}=24
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	Click Link	${CELLULAR_CONNECTION}
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	//*[@id="connHealthAddress"]
	Input Text	//*[@id="connHealthAddress"]	${IP_ADDRESS}
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	//*[@id="connHealthInterval"]
	Input Text	//*[@id="connHealthInterval"]	${INTERVAL}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections: \n${TABLE_CONTENTS}	INFO	console=yes
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	css=#${CELLULAR_CONNECTION} input
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Enabled	//*[@id="nonAccessControls"]/input[3]	error=Cannot up connection because up connection button is disable or not visible
	Click Element	//*[@id="nonAccessControls"]/input[3]
	GUI::Basic::Spinner Should Be Invisible
	Should Match Regexp	${TABLE_CONTENTS}	${CELLULAR_CONNECTION}\\s+\\w+\\s+${TYPE}\\s+${GSM_CDC_WDMX}\\s+Up

SUITE:Edit GSM Connection with invalid value
	[Arguments]	${ERROR_MESSAGE}	${IP_ADDRESS}=8.8.8.8	${INTERVAL}=24
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	Click Link	${CELLULAR_CONNECTION}
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	//*[@id="connHealthAddress"]
	Input Text	//*[@id="connHealthAddress"]	${IP_ADDRESS}
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	//*[@id="connHealthInterval"]
	Input Text	//*[@id="connHealthInterval"]	${INTERVAL}
	GUI::Basic::Save
	Page Should Contain	${ERROR_MESSAGE}
	GUI::Basic::Cancel

SUITE:Check list selection on field="Connection:" in System::Toolkit::Network Tools tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Toolkit::open tab
	Click Element	id=icon_NetworkTools
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	//*[@id="connection"]
	Click Element	id=connection
	List Selection Should Be	//*[@id="connection"]	${CELLULAR_CONNECTION}
	GUI::Basic::Cancel

SUITE:Ping external network using mobile connection in network tools
	[Arguments]	${IP_ADDRESS}=8.8.8.8
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Toolkit::open tab
	Click Element	id=icon_NetworkTools
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	//*[@id="connection"]
	Element Should Be Visible	//*[@id="ipaddresstest"]
	Input Text	//*[@id="ipaddresstest"]	${IP_ADDRESS}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="networktools"]/div[4]/div/div/input
	GUI::Basic::Spinner Should Be Invisible
	${COMMAND_OUTPUT}=	Wait Until Keyword Succeeds	1m	3s	SUITE:Get text in command output
	Should Match Regexp	${COMMAND_OUTPUT}	Ping Result: OK
	Should Match Regexp	${COMMAND_OUTPUT}	Success: Connection ${CELLULAR_CONNECTION} has internet access
	GUI::Basic::Cancel

SUITE:Get text in command output
	${COMMAND_OUTPUT}=	Get Text	//*[@id="log"]
	Log	\nCommand Output contents in System::Toolkit::Network Tools: \n${COMMAND_OUTPUT}	INFO	console=yes
	Should Not Match Regexp	${COMMAND_OUTPUT}	Running...wait.
	[Return]	${COMMAND_OUTPUT}

SUITE:Ping non-reachable network using mobile connection in network tools
	[Arguments]	${IP_ADDRESS}=?
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Toolkit::open tab
	Click Element	id=icon_NetworkTools
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	//*[@id="connection"]
	Element Should Be Visible	//*[@id="ipaddresstest"]
	Input Text	//*[@id="ipaddresstest"]	${IP_ADDRESS}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="networktools"]/div[4]/div/div/input
	GUI::Basic::Spinner Should Be Invisible
	${COMMAND_OUTPUT}=	Wait Until Keyword Succeeds	1m	3s	SUITE:Get text in command output
	Should Match Regexp	${COMMAND_OUTPUT}	Ping Result: NOK
	Should Match Regexp	${COMMAND_OUTPUT}	Diagnostic Result: Connection cannot reach configured IP Address - ping failed
	GUI::Basic::Cancel

SUITE:Teardown in network tools
	${STATUS}=	Run Keyword And Return Status	Element Should Be Visible	//*[@id="ipaddresstest"]
	Run Keyword If	${STATUS}	GUI::Basic::Cancel