*** Settings ***
Documentation	Testing the Wireguard
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	EXCLUDEIN4_2	EXCLUDEIN5_0

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${intf_name1}	intf_1
${intf_add1}	1.1.1.1
${ext_add1}	3.3.3.3
${intf_port1}	455
${type_server}	server
${type_client}	Client
${enabled}	Enabled
${disabled}	Disabled


*** Test Cases ***
Validate fields in page Network :: Wireguard interface address
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=wireguardItfName	${intf_name1}
	Input Text	id=wireguardItfPort	${intf_port1}
	Click Element	id=wireguardItfIntAddr
	@{VALIDS}=	Create List	1.1.1.1/24
	@{INVALIDS}=	Create List	256.1.1.1	2.2.2.2/40	a.b.c.d
	GUI::Basic::Input Text::Validation	wireguardItfIntAddr	${VALIDS}	${INVALIDS}
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="${intf_name1}"]
	Select Checkbox	//*[@id="${intf_name1}"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

Validate fields in page Network :: Wireguard port.
	[Tags]	NON-CRITICAL	BUG_NG_11756
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=wireguardItfName	${intf_name1}
	Input Text	id=wireguardItfIntAddr	${intf_add1}
	Click Element	id=wireguardItfPort
	@{VALIDS}=	Create List	480
	@{INVALIDS}=	Create List	-1	a	65536	65545
	GUI::Basic::Input Text::Validation	wireguardItfPort	${VALIDS}	${INVALIDS}
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element 	//*[@id="${intf_name1}"]
	Select Checkbox	//*[@id="${intf_name1}"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

Validate fields in page Network :: Wireguard external address.
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=wireguardItfName	${intf_name1}
	Input Text	id=wireguardItfIntAddr	${intf_add1}
	Input Text	id=wireguardItfPort	${intf_port1}
	Select From List By Label	id=wireguardItfStatus	${enabled}
	Click Element	//*[@id="wireguardItfExternalAddr"]
	@{VALIDS}=	Create List	3.3.3.3
	@{INVALIDS}=	Create List	-1.1.1-1	313'12	22.2.2.2/36
	GUI::Basic::Input Text::Validation	wireguardItfExternalAddr	${VALIDS}	${INVALIDS}
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="${intf_name1}"]
	Select Checkbox	//*[@id="${intf_name1}"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

Validate fields in page Network :: Wireguard DNS server.
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=wireguardItfName	${intf_name1}
	Input Text	id=wireguardItfIntAddr	${intf_add1}
	Input Text	id=wireguardItfPort	${intf_port1}
	Input Text	id=wireguardItfExternalAddr	${ext_add1}
	Select From List By Label	id=wireguardItfTypeAdd	${type_client}
	Select From List By Label	id=wireguardItfStatus	${enabled}
	Click Element	id=wireguardItfDns
	@{VALIDS}=	Create List	3.3.3.3
	@{INVALIDS}=	Create List	a.b.c.d	256.1.1.1	22.2.2.2/36
	GUI::Basic::Input Text::Validation	wireguardItfDns	${VALIDS}	${INVALIDS}
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="${intf_name1}"]
	Select Checkbox	//*[@id="${intf_name1}"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

Validate fields in page Network :: Wireguard DNS server.2
	[Tags]	NON-CRITICAL	BUG_NG_11756
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=wireguardItfName	${intf_name1}
	Input Text	id=wireguardItfIntAddr	${intf_add1}
	Input Text	id=wireguardItfPort	${intf_port1}
	Input Text	id=wireguardItfExternalAddr	${ext_add1}
	Input Text	id=wireguardItfDns	${ext_add1}
	Select From List By Label	id=wireguardItfTypeAdd	${type_client}
	Select From List By Label	id=wireguardItfStatus	${enabled}
	Click Element	id=wireguardItfMtu
	@{VALIDS}=	Create List	500
	@{INVALIDS}=	Create List	-1	b	65537
	GUI::Basic::Input Text::Validation	wireguardItfMtu	${VALIDS}	${INVALIDS}
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="${intf_name1}"]
	Select Checkbox	//*[@id="${intf_name1}"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' <= '5.2'	GUI::Basic::Network::Wireguard::open tab
	Run Keyword If	'${NGVERSION}' >= '5.4'	Run Keywords	GUI::Basic::Network::VPN::Open Tab	AND	Click Element	//span[normalize-space()='Wireguard']
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' <= '5.2'	GUI::Basic::Network::Wireguard::open tab
	Run Keyword If	'${NGVERSION}' >= '5.4'	Run Keywords	GUI::Basic::Network::VPN::Open Tab	AND	Click Element	//span[normalize-space()='Wireguard']
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid