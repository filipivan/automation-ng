*** Settings ***
Documentation	Editing Local Accounts Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test Open Tab
	GUI::Network::Open
	GUI::Basic::Spinner Should Be Invisible

Test Settings
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible

Test Connections
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible

Test Static Routes
	GUI::Basic::Network::Static Routes::Open Tab
	GUI::Basic::Spinner Should Be Invisible

Test Hosts
	GUI::Basic::Network::Hosts::Open Tab
	GUI::Basic::Spinner Should Be Invisible

Test SNMP
	GUI::Basic::Network::SNMP::Open Tab
	GUI::Basic::Spinner Should Be Invisible

Test DHCP Server
	Run Keyword If	'${NGVERSION}' <= '5.4'	GUI::Basic::Network::DHCP Server::Open Tab
	Run Keyword If	'${NGVERSION}' >= '5.6'	Run Keywords	GUI::Basic::Network::DHCP::Open Tab	AND	Click Element	//span[normalize-space()='DHCP Server']
	GUI::Basic::Spinner Should Be Invisible

Test SSL VPN
	Run Keyword If	'${NGVERSION}' <= '5.2'	GUI::Basic::Network::SSL VPN::open tab
	Run Keyword If	'${NGVERSION}' >= '5.4'	Run Keywords	GUI::Basic::Network::VPN::Open Tab	AND	Click Element	//span[normalize-space()='SSL VPN']
	GUI::Basic::Spinner Should Be Invisible

Test Client Tab
	Run Keyword If	'${NGVERSION}' <= '5.2'	GUI::Basic::Network::SSL VPN::open tab
	Run Keyword If	'${NGVERSION}' >= '5.4'	Run Keywords	GUI::Basic::Network::VPN::Open Tab	AND	Click Element	//span[normalize-space()='SSL VPN']
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//a[@id='sslvpnClient']/span
	GUI::Basic::Spinner Should Be Invisible

Test Server Tab
	Run Keyword If	'${NGVERSION}' <= '5.2'	GUI::Basic::Network::SSL VPN::open tab
	Run Keyword If	'${NGVERSION}' >= '5.4'	Run Keywords	GUI::Basic::Network::VPN::Open Tab	AND	Click Element	//span[normalize-space()='SSL VPN']
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//a[@id='sslvpnServer']/span
	GUI::Basic::Spinner Should Be Invisible


Test Server Status Tab
	Run Keyword If	'${NGVERSION}' <= '5.2'	GUI::Basic::Network::SSL VPN::open tab
	Run Keyword If	'${NGVERSION}' >= '5.4'	Run Keywords	GUI::Basic::Network::VPN::Open Tab	AND	Click Element	//span[normalize-space()='SSL VPN']
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//a[@id='sslvpn_status']/span
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	Sleep	10s

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout
	Close all Browsers
