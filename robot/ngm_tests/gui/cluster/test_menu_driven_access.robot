*** Settings ***
Documentation	Test to Configure Cluster Menu Driven Access Functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup		SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW

*** Variables ***
${PEER_IP}				${HOSTPEER}
${PEER_HOMEPAGE}		${HOMEPAGEPEER}
${COORD_IP}				${HOST}
${COORD_HOMEPAGE}		${HOMEPAGE}
${Name_Co-ordinator}	test-Coordinator
${Name_Peer}			test-Peer
${Type}					device_console
${CLUSTER_LICENSE_1}	${TEN_DEVICES_CLUSTERING_LICENSE}
${DEFAULT_HOSTNAME}		${HOSTNAME_NODEGRID}
${PRE_SHARED_KEY}		admin
${IS_STAR_MODE}			Mesh
${Co-ordinator_addrs}	${HOST}
${License_1}			${TEN_DEVICES_CLUSTERING_LICENSE_2}
${Ip_address}			127.0.0.1
${Usr}					admin
${Passwd}				${DEFAULT_PASSWORD}
${Confm_passwd}			${DEFAULT_PASSWORD}
${Clone_Name}			dummy-dev-coordinator
${Clone_No}				51
${Group_Name}			test_group
${USERNG}				test_group
${PWDNG}				admin
${DUMMY_CLONED}			dummy-dev-coordinator
${HOSTNAME_COORDINATOR}		test-nodegrid-coord
${DOMAINAME_COORDINATOR}	test-localdomain-coord
${HOSTNAME_PEER}		test-nodegrid-peer
${DOMAINAME_PEER}		test-localdomain-peer
${Access_License}		${ONE_THOUSAND_DEVICES_ACCESS_LICENSE}
${Default_DOMAIN_NAME}	localdomain
${HOST_NAME}			nodegrid
#${DEFAULT_USERNAME}	admin

*** Test Cases ***
Test Case to Configure Coordinator Side
	GUI::Basic::Open And Login Nodegrid		PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add Access license
	SUITE:Change Hostname To "${HOSTNAME_COORDINATOR}" And Domain Name To "${DOMAINAME_COORDINATOR}"
	SUITE:Configure Coordinator Side cluster
	SUITE:Add Device on Coordinator Side
	SUITE:Clone device on Coordinator side
	SUITE:Add Authorization
	SUITE:Add Local Accounts
	close all browsers

Test Case to Configure Peer Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Change Hostname To "${HOSTNAME_PEER}" And Domain Name To "${DOMAINAME_PEER}"
	SUITE:Configure Peer Side Cluster
	SUITE:Add Device on Peer Side
	SUITE:Add Authorization
	SUITE:Add Local Accounts
	close all browsers

Test case to Verify the Peer status On Peer Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Cluster::Peers::open tab
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Check the cluster status
	Page Should Contain Element	xpath=//td[contains(.,'Peer')]
	Page Should Contain Element	xpath=//td[contains(.,'${DOMAINAME_PEER}')]
	close all browsers

Test case to Verify the Peer status On Coordinator Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Cluster::Peers::open tab
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Check the cluster status
	Page Should Contain Element	xpath=//td[contains(.,'Peer')]
	Page Should Contain Element	xpath=//td[contains(.,'${DOMAINAME_COORDINATOR}')]
	close all browsers

Test login with menu-driven and check pagination
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible	xpath=//div[@id='|test-nodegrid-coord']/div/div[2]/a
	click element	xpath=//div[@id='|test-nodegrid-coord']/div/div[2]/a
	Sleep	5s
	Run Keyword If	'${NGVERSION}'<='5.4'	Switch Window	test-nodegrid-coord
	Run Keyword If	'${NGVERSION}'>'5.4'	Switch Window	test-nodegrid-coord - Console
	Sleep	5s
	Select Frame	xpath=//*[@id='termwindow']
	Press Keys	None	RETURN
	sleep	5s

	GUI:Access::Generic Console Command Output	n
	${LAST_PAGE}	GUI:Access::Generic Console Command Output	N	#it will show last page & co-ordinator
	sleep	3s
	Should Contain	${LAST_PAGE}	${HOSTNAME_COORDINATOR}.${DOMAINAME_COORDINATOR}
	Should Not Contain	${LAST_PAGE}	${DUMMY_CLONED}

	${FIRST_PAGE}	GUI:Access::Generic Console Command Output	P	#it should move to first page should contain only dummy device not co-ordinator
	sleep	3s
	Should not Contain	${FIRST_PAGE}	${Name_Co-ordinator}
	Should Contain X Times	${FIRST_PAGE}	${DUMMY_CLONED}		51

	${FIRST_AGAIN}	GUI:Access::Generic Console Command Output	P	#tries to do previous page in first page, should not change the output
	sleep	3s
	Should Be Equal	${FIRST_PAGE}	${FIRST_AGAIN}

	${ORDER_BY_ID}	GUI:Access::Generic Console Command Output	i
	sleep	3s
	Should Contain	${ORDER_BY_ID}	${Name_Co-ordinator}
	Should Contain X Times	${ORDER_BY_ID}	${DUMMY_CLONED}		50

	${ORDER_BY_NAME}	GUI:Access::Generic Console Command Output	n	#only to display devices in alphabetical order
	sleep	3s
	Should Contain	${ORDER_BY_NAME}	${DUMMY_CLONED}		50
	Should Not Contain	${ORDER_BY_NAME}	${Name_Co-ordinator}

	close all browsers

Test show local devices in coordinator and peer
	# Coordinator side
	SUITE:Login to coordinator with menu driven crediantials
	sleep	5s
	GUI:Access::Generic Console Command Output	l	#just to go back to initial state
	${ID_ORDER}	GUI:Access::Generic Console Command Output	i
	${LOCAL_BY_ID}	GUI:Access::Generic Console Command Output	l
	Should Be Equal	${ID_ORDER}	${LOCAL_BY_ID}	# l command should not change menu-driven output (BUG NG-8418)
	Should Not Contain	${LOCAL_BY_ID}	${Name_Peer}
	${NAME_ORDER}	GUI:Access::Generic Console Command Output	n
	${LOCAL_BY_NAME}	GUI:Access::Generic Console Command Output	l
	Should Be Equal	${NAME_ORDER}	${LOCAL_BY_NAME}
	Should Not Contain	${LOCAL_BY_NAME}	${Name_Peer}
	close all browsers

	# Peer side
	SUITE:Login to peer side with menu driven crediantials
	sleep	5s
	GUI:Access::Generic Console Command Output	l	#just to go back to initial state
	${ID_ORDER}	GUI:Access::Generic Console Command Output		i
	${LOCAL_BY_ID}	GUI:Access::Generic Console Command Output	l
	Should Be Equal	${ID_ORDER}	${LOCAL_BY_ID}	# l command should not change menu-driven output (BUG NG-8418)
	Should Not Contain	${LOCAL_BY_ID}	${Name_Co-ordinator}
	${NAME_ORDER}	GUI:Access::Generic Console Command Output	n
	${LOCAL_BY_NAME}	GUI:Access::Generic Console Command Output	l
	Should Be Equal	${NAME_ORDER}	${LOCAL_BY_NAME}
	Should Not Contain	${LOCAL_BY_NAME}	${Name_Co-ordinator}
	close all browsers

Test peers in cluster via menu-driven cluster
	SUITE:Login to peer side with menu driven crediantials
	${PEERS_IN_PEER}	GUI:Access::Generic Console Command Output	p
	sleep	5s
	Should Contain	${PEERS_IN_PEER}	${HOSTNAME_COORDINATOR}.${DOMAINAME_COORDINATOR}
	SUITE:Login to coordinator with menu driven crediantials
	${PEERS_IN_COORDINATOR}	GUI:Access::Generic Console Command Output	p
	sleep	5s
	Should Contain	${PEERS_IN_COORDINATOR}	${HOSTNAME_PEER}.${DOMAINAME_PEER}
	close all browsers

Test search devices
	SUITE:Login to coordinator with menu driven crediantials
	GUI:Access::Generic Console Command Output	s
	sleep	5s
	${DEVICES_ITSELF}	GUI:Access::Generic Console Command Output	${Name_Co-ordinator}
	Should Contain	${DEVICES_ITSELF}	${HOSTNAME_COORDINATOR}.${DOMAINAME_COORDINATOR}
	Should Contain	${DEVICES_ITSELF}	${Name_Co-ordinator}

	# Do a search with few results and see "N" and "P" does not work (BUG NG-8418)
	${NEXT_PAGE}	GUI:Access::Generic Console Command Output	N
	${PREVIOUS_PAGE}	GUI:Access::Generic Console Command Output	P
	Should Be Equal	${NEXT_PAGE}	${PREVIOUS_PAGE}

	# Do a blank search and see "n" and "i" command does not change the order of devices (BUG NG-8418)
	GUI:Access::Generic Console Command Output	s
	GUI:Access::Generic Console Command Output	_	#all devices have "_" so the result should list all devices in both coordinator and peer
	${ALL_DEVICES_ID}	GUI:Access::Generic Console Command Output	i
	${ALL_DEVICES_NAME}	GUI:Access::Generic Console Command Output	n
	Should Be Equal	${ALL_DEVICES_ID}	${ALL_DEVICES_NAME}

	close all browsers

Test connect to local device in coordinator
	SUITE:Login to coordinator with menu driven crediantials
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	GUI:Access::Generic Console Command Output	l	#just to go back to initial state
	GUI:Access::Generic Console Command Output	i
	GUI:Access::Generic Console Command Output	n
	${NEXT_PAGE}	GUI:Access::Generic Console Command Output	N
	Should Contain	${NEXT_PAGE}	52: ${Name_Co-ordinator}
	GUI:Access::Generic Console Command Output	52
	sleep	5s
	${OUTPUT}=	GUI:Access::Generic Console Command Output	ENTER
	sleep	5s
	Should Contain	${OUTPUT}	[${DEFAULT_USERNAME}@${HOSTNAME_COORDINATOR} /]#
	close all browsers

Test connect to peer device in coordinator
	SUITE:Login to coordinator with menu driven crediantials
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	GUI:Access::Generic Console Command Output	s
	GUI:Access::Generic Console Command Output	${Name_Peer}
	GUI:Access::Generic Console Command Output	1
	${OUTPUT}=	GUI:Access::Generic Console Command Output	ENTER
	sleep	5s
	Should Contain	${OUTPUT}	[${DEFAULT_USERNAME}@${HOSTNAME_PEER} /]#
	close all browsers

Test connect to local device in peer
	[Tags]	NON-CRITICAL
	SUITE:Login to peer side with menu driven crediantials
	GUI:Access::Generic Console Command Output	l	#just to go back to initial state
	GUI:Access::Generic Console Command Output	1
	Sleep	5s
	${OUTPUT}=	GUI:Access::Generic Console Command Output	ENTER
	sleep	5s
	Should Contain	${OUTPUT}	[${DEFAULT_USERNAME}@${HOSTNAME_PEER} /]#
	close all browsers

Test connect to coordinator device in peer
	SUITE:Login to peer side with menu driven crediantials
	GUI:Access::Generic Console Command Output	s
	GUI:Access::Generic Console Command Output	${Name_Co-ordinator}
	GUI:Access::Generic Console Command Output	1
	Sleep	5s
	${OUTPUT}=	GUI:Access::Generic Console Command Output	ENTER
	sleep	5s
	Should Contain	${OUTPUT}	[${DEFAULT_USERNAME}@${HOSTNAME_COORDINATOR} /]#
	close all browsers

Test command to exit menu-driven cluster
	# Coordinator side
	SUITE:Login to coordinator with menu driven crediantials
	sleep	5s
	GUI:Access::Generic Console Command Output	q
	Close Browser
	# Peer side
	SUITE:Login to peer side with menu driven crediantials
	sleep	5s
	GUI:Access::Generic Console Command Output	q
	close all browsers

*** Keywords ***
SUITE:Setup
	SUITE:Delete configuration on Coordinator side
	SUITE:Delete configuration on Peer side

SUITE:Teardown
	SUITE:Delete configuration on Coordinator side
	SUITE:Delete configuration on Peer side

SUITE:Delete configuration on Coordinator side
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Cluster::Open Settings Tab
    Unselect Checkbox   //*[@id="enabled"]
    Select Checkbox   lps_enabled
    Click Element    //label[normalize-space()='Client']//input[@id='lps_type']
    Unselect Checkbox   management_enabled
    GUI::Basic::Save If Configuration Changed	SPINNER_TIMEOUT=120s
	GUI::Cluster::Delete Licenses
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Input Text	domainname	${Default_DOMAIN_NAME}
	Input Text	hostname	${HOST_NAME}
	GUI::Basic::Save If Configuration Changed
	GUI::ManagedDevices::Delete All Devices
	SUITE:Delete local Account
	SUITE:Delete Authorization
	Close All Browsers

SUITE:Delete configuration on Peer side
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Cluster::Disable Cluster
	GUI::Cluster::Delete Licenses
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Input Text	domainname	${Default_DOMAIN_NAME}
	Input Text	hostname	${HOST_NAME}
	GUI::Basic::Save If Configuration Changed
	GUI::ManagedDevices::Delete All Devices
	SUITE:Delete local Account
	SUITE:Delete Authorization
	Close All Browsers

SUITE:Check the cluster status
	Wait Until Keyword Succeeds	50x	20s	SUITE:Check the status

SUITE:Check the status
	GUI::Basic::Cluster::Peers::open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	xpath=//td[contains(.,'Online')]

SUITE:Login to coordinator with menu driven crediantials
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible	xpath=//div[@id='|test-nodegrid-coord']/div/div[2]/a
	click element	xpath=//div[@id='|test-nodegrid-coord']/div/div[2]/a
	Sleep	5s
	Run Keyword If	'${NGVERSION}'<='5.4'	Switch Window	test-nodegrid-coord
	Run Keyword If	'${NGVERSION}'>'5.4'	Switch Window	test-nodegrid-coord - Console
	Sleep	5s
	Select Frame	xpath=//*[@id='termwindow']
	Press Keys	None	RETURN
	Sleep	5s

SUITE:Login to peer side with menu driven crediantials
	GUI::Basic::Open Nodegrid	${HOMEPAGEPEER}	${BROWSER}	${NGVERSION}
	GUI::Basic::Spinner Should Be Invisible
	Input Text	username	${USERNG}
	Input Text	password	${PWDNG}
	Click Element	login-btn
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible	xpath=//a[contains(.,'Console')]
	click element	xpath=//a[contains(.,'Console')]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	Run Keyword If	'${NGVERSION}'<='5.4'	Switch Window	test-nodegrid-peer
	Run Keyword If	'${NGVERSION}'>'5.4'	Switch Window	test-nodegrid-peer - Console
	Sleep	5s
	Select Frame	xpath=//*[@id='termwindow']
	Press Keys	None	RETURN
	Sleep	5s

SUITE:Add Local Accounts
	GUI::Security::Open Local Accounts tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	input text	xpath=//input[@id='uName']	${Group_Name}
	input text	xpath=//input[@id='uPasswd']	admin
	input text	xpath=//input[@id='cPasswd']	admin
	wait until element is visible	xpath=//div[@id='uGroup']/div/div/div/select
	click element	//*[@id="uGroup"]/div/div[1]/div[1]/select/option[2]
	click element	xpath=//button[contains(.,'Add ►')]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Configure Coordinator Side cluster
	GUI::Basic::System::License::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="licensevalue"]	${CLUSTER_LICENSE_1}
	GUI::Basic::Save If Configuration Changed
	GUI::Basic::Spinner Should Be Invisible
	GUI::Cluster::Open Settings Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	enabled
	select checkbox	//*[@id="lps_enabled"]
	Wait Until Element Is Visible	lps_type
	Select Radio Button	lps_type	server
	Select Radio Button	nodeType	master
	Input Text	//*[@id="preSharedKey"]	${PRE_SHARED_KEY}
	Click Element	//*[@id="starMode"]
	Click Element	//*[@id="cluster_enabled"]
	Select Checkbox	management_enabled
	GUI::Basic::Save	SPINNER_TIMEOUT=120s

SUITE:Configure Peer Side Cluster
	GUI::Cluster::Open Settings Tab
	Select Checkbox	//*[@id="enabled"]
	select checkbox	//*[@id="lps_enabled"]
	Wait Until Element Is Visible	lps_type
	Select Radio Button	lps_type	client
	Click Element	css=.radio:nth-child(3) #nodeType
	Input Text	//*[@id="masterAddr"]	${Co-ordinator_addrs}
	Input Text	//*[@id="preSharedKey2"]	${PRE_SHARED_KEY}
	select checkbox	//*[@id="cluster_enabled"]
	Select Checkbox	management_enabled
	GUI::Basic::Save If Configuration Changed	SPINNER_TIMEOUT=90s

SUITE:Add Device on Coordinator Side
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${Name_Co-ordinator}
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text	//*[@id='spm_name']	${Name_Co-ordinator}
	Select From List By Label	//*[@id="type"]	${Type}
	Input Text	id=phys_addr	${Ip_address}
	Input Text	id=username	${Usr}
	Input Text	id=passwordfirst	${Passwd}
	Input Text	id=passwordconf	${Confm_passwd}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Add Device on Peer Side
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${Name_Peer}
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text	//*[@id='spm_name']	${Name_Peer}
	Select From List By Label	//*[@id="type"]	${Type}
	Input Text	id=phys_addr	${Ip_address}
	Input Text	id=username	${Usr}
	Input Text	id=passwordfirst	${Passwd}
	Input Text	id=passwordconf	${Confm_passwd}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Clone device on Coordinator side
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//tr[@id='test-Coordinator']/td/input
	click element	xpath=//div[@id='nonAccessControls']/input[5]
	GUI::Basic::Spinner Should Be Invisible
	input text	id=spm_name	${Clone_Name}
	input text	id=num_clones	${Clone_No}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Add Authorization
	GUI::Basic::Security::Authorization::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	input text	id=uGroup	${Group_Name}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//a[contains(text(),'test_group')]
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//span[contains(.,'Profile')]
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible	xpath=//label[contains(.,'Menu-driven access to devices')]
	click element	xpath=//label[contains(.,'Menu-driven access to devices')]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Add Access license
	GUI::Basic::System::License::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="licensevalue"]	${Access_License}
	GUI::Basic::Save If Configuration Changed
	GUI::Basic::Spinner Should Be Invisible

SUITE:Change Hostname To "${HOSTNAME}" And Domain Name To "${DOMAIN_NAME}"
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Input Text	hostname	${HOSTNAME}
	Input Text	domainname	${DOMAIN_NAME}
	GUI::Basic::Save If Configuration Changed

SUITE:Delete local Account
	GUI::Security::Open Local Accounts tab
	GUI::Basic::Spinner Should Be Invisible
	${CONTAINS_LOCAL_ACCOUNT}	run keyword and return status	page should contain element  //*[@id="${Group_Name}"]/td[1]/input
	run keyword if	'${CONTAINS_LOCAL_ACCOUNT}'=='${TRUE}'	select checkbox		//*[@id="${Group_Name}"]/td[1]/input
	run keyword if	'${CONTAINS_LOCAL_ACCOUNT}'=='${TRUE}'	click element	//input[contains(@id,'delButton')]
	run keyword if	'${CONTAINS_LOCAL_ACCOUNT}'=='${TRUE}'	handle alert
	sleep	3s
	page should not contain element		xpath=//tr[@id='test_group']/td/input

SUITE:Delete Authorization
	GUI::Basic::Security::Authorization::open tab
	GUI::Basic::Spinner Should Be Invisible
	${CONTAINS_LOCAL_GROUP}		run keyword and return status	page should contain element		xpath=//tr[@id='test_group']/td/input
	run keyword if	'${CONTAINS_LOCAL_GROUP}'=='${TRUE}'	select checkbox		xpath=//tr[@id='test_group']/td/input
	run keyword if	'${CONTAINS_LOCAL_GROUP}'=='${TRUE}'	click element	//input[contains(@id,'delButton')]
	run keyword if	'${CONTAINS_LOCAL_GROUP}'=='${TRUE}'	handle alert
	sleep	3s
	page should not contain element		xpath=//tr[@id='test_group']/td/input
SUITE:Delete devices
	SUITE:Setup
	GUI::Basic::Managed Devices::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'=='4.2'	Input Text	//*[@id="search_expr1"]	dummy
	Run Keyword If	'${NGVERSION}'>'4.2'	Input Text	//*[@id="filter_field"]	dummy
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'=='4.2'	Press Keys	//*[@id="search_expr1"]	ENTER
	Run Keyword If	'${NGVERSION}'>'4.2'	Press Keys	//*[@id="filter_field"]	ENTER
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="thead"]/tr/th[1]/input
	Click Element	//*[@id="delButton"]
	Handle Alert