*** Settings ***
Documentation	Test for SSH Connection to Devices Across All Clusters for IPv4/IPv6 and STATIC IP - MESH and STAR MODE with license pool
Metadata	Version	1
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../init.robot
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${PASSWORD}	${DEFAULT_PASSWORD}
${SHAREDKEY}	shared_key
${MESH_MODE}	no
${STAR_MODE}	yes
${DEVCOORD_NAME}	COORDINATOR
${DEVCOORD_IPADDR}	${HOST}
${DEVPEER_NAME}	PEER
${DEVPEER_IPADDR}	${HOSTPEER}
${HOMEPAGEPEER}	${HOMEPAGEPEER}
${CLUSTER_LICENSE}	${FIVE_DEVICES_CLUSTERING_LICENSE_2}
${STATIC_IPV6}	${IPV6_PREFIX}

*** Test Cases ***
Test Cluster Connection - MESH MODE - IPv4
	GUI::Cluster::Add License	${CLUSTER_LICENSE}
	GUI::Basic::Spinner Should Be Invisible

	GUI::Cluster::Add Coordinator	${DEVCOORD_NAME}	${DEVCOORD_IPADDR}	${SHAREDKEY}	${MESH_MODE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout and Close Nodegrid

	GUI::Basic::Open And Login Nodegrid	PAGE=${HOMEPAGEPEER}
	GUI::Basic::Spinner Should Be Invisible

	GUI::Cluster::Add Peer	${DEVPEER_NAME}	${DEVPEER_IPADDR}	${DEVCOORD_IPADDR}	${SHAREDKEY}
	Wait Until Keyword Succeeds	120	3	SUITE:Check Peers Tab
	GUI::Basic::Logout and Close Nodegrid

	[Teardown]	Close All Browsers

Test access peer device by ssh across cluster - MESH MODE - IPv4
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	
	GUI::Access::Table::Wait Until Cluster Node Is Visible	${HOSTNAME_NODEGRID}.${DOMAINAME_NODEGRID}
	GUI::Access::Table::Access Console	${HOSTNAME_NODEGRID}.${DOMAINAME_NODEGRID}
	Sleep    50
	
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	120	40	SUITE:SSH Peer
	[Teardown]	SUITE:Teardown

Test Cluster Connection - STAR MODE - IPv4
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

	GUI::Cluster::Add License	${CLUSTER_LICENSE}
  GUI::Basic::Spinner Should Be Invisible

	GUI::Cluster::Add Coordinator	${DEVCOORD_NAME}	${DEVCOORD_IPADDR}	${SHAREDKEY}	${STAR_MODE}
	GUI::Basic::Spinner Should Be Invisible

	GUI::Basic::Open And Login Nodegrid	PAGE=${HOMEPAGEPEER}
	GUI::Basic::Spinner Should Be Invisible

	GUI::Cluster::Add Peer	${DEVPEER_NAME}	${DEVPEER_IPADDR}	${DEVCOORD_IPADDR}	${SHAREDKEY}
	GUI::Basic::Spinner Should Be Invisible

	GUI::Basic::Cluster::Peers::Open Tab
  GUI::Basic::Spinner Should Be Invisible

	Page Should Contain	Cluster must be enabled. Cluster Peers is only available for Coordinator nodes in star mode.
  GUI::Basic::Spinner Should Be Invisible

	GUI::Basic::Open And Login Nodegrid

	Wait Until Keyword Succeeds	120	3	SUITE:Check Peers Tab

	[Teardown]	Close All Browsers

Test access peer device by ssh across cluster - STAR MODE - IPv4
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

	GUI::Access::Table::Wait Until Cluster Node Is Visible	${HOSTNAME_NODEGRID}.${DOMAINAME_NODEGRID}
	GUI::Access::Table::Access Console	${HOSTNAME_NODEGRID}.${DOMAINAME_NODEGRID}

	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	120	20	SUITE:SSH Peer

	[Teardown]	SUITE:Teardown

*** Keywords ***
SUITE:Setup
	Sleep	1m
	GUI::Basic::Open And Login Nodegrid

SUITE:Teardown
	Close all Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Cluster::Remove Peer From Coordinator If Exist
	GUI::ManagedDevices::Delete Device If Exists	${DEVCOORD_NAME}
	GUI::Cluster::Delete Licenses
	GUI::Cluster::Disable Cluster
	GUI::Basic::Logout
	Close all Browsers

	GUI::Basic::Open And Login Nodegrid	PAGE=${HOMEPAGEPEER}
	GUI::ManagedDevices::Delete Device If Exists	${DEVPEER_NAME}
	GUI::Cluster::Disable Cluster
	GUI::Basic::Spinner Should Be Invisible
	Close all Browsers

SUITE:Check Peers Tab
	GUI::Cluster::Open Peers Tab
	Page Should Contain	peerTable
	Table Should Contain	peerTable	Peer
	Table Should Contain	peerTable	Coordinator
	Table Should Contain	peerTable	${HOST}
	Table Should Contain	peerTable	${HOSTPEER}
	Element Should Not Contain	peerTable	Offline
	Element Should Not Contain	peerTable	Error
	Click Element	//*[@id="main_menu"]/li[6]

SUITE:SSH Peer	
	Sleep	30
	Press Keys	None	show /settings/network_connections/	ENTER
	Sleep	10
	Press Keys	None	shell sudo ssh ${DEFAULT_USERNAME}:${DEVPEER_NAME}@${DEVCOORD_IPADDR}	ENTER
	Sleep	10
	Press Keys	None	yes	ENTER
	Sleep	10
	Press Keys	None	${PASSWORD}	ENTER
	Sleep	10
	Press Keys	None	exit	ENTER
	Sleep	10

	${CONSOLE_CONTENT}=	SUITE:Get Console Content
	Should Contain	${CONSOLE_CONTENT}	${HOSTPEER}
	Should Contain	${CONSOLE_CONTENT}	[Enter '^Ec?' for help]
	Log	\n++++++++++++++++++++++++++++ Console Content: ++++++++++++++++++++++++++++\n ${CONSOLE_CONTENT}\n	INFO	console=yes

SUITE:Get Console Content
	Wait Until Keyword Succeeds	1m	1s	Execute JavaScript	term.selectAll();
	${CONSOLE_CONTENT}=	Wait Until Keyword Succeeds	1m	1s	Execute Javascript	return term.getSelection().trim();
	Log To Console	${CONSOLE_CONTENT}
	Should Not Contain	${CONSOLE_CONTENT}	[error.connection.failure] Could not establish a connection to device
	[Return]	${CONSOLE_CONTENT}