*** Settings ***
Documentation	Test that the management tab works properly
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../init.robot
Force Tags   PART-1	GUI	${BROWSER}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SHAREDKEY}	shared_key
${MESH_MODE}	no
${DEVCOORD_NAME}	COORDINATOR
${DEVCOORD_IPADDR}	${HOST}

*** Test Cases ***
Test Table Display
	#Table Should Be Displayed
	GUI::Cluster::Add Coordinator	${DEVCOORD_NAME}	${DEVCOORD_IPADDR}	${SHAREDKEY}	${MESH_MODE}
	GUI::Basic::Cluster::Management::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	wait until page contains element	jquery=#peerMngtTable
	Element Should Be Visible	jquery=#peerMngtTable
	${COUNT}=	GUI::Basic::Count Table Rows	jquery=#peerMngtTable
	should be equal	'${COUNT}'	'0'
	Page Should Not Contain Element	xpath=//div[@id="notice"]

#Test Software Upgrade
	#DONT KNOW HOW TO TEST THIS ONE WITHOUT A VALID IP RANGE

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Cluster::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Cluster::Management::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	30	2	SUITE Disable Cluster
	Wait Until Keyword Succeeds	30	2	GUI::ManagedDevices::Delete Device If Exists	${DEVCOORD_NAME}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout
	Close all Browsers

SUITE Disable Cluster
	GUI::Basic::Cluster::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Unselect Checkbox	lps_enabled
	Unselect Checkbox	enabled
	Page Should Not Contain	clusterName
	Wait Until Keyword Succeeds	30	2	GUI::Basic::Save
	Sleep	10s
	GUI::Basic::Spinner Should Be Invisible