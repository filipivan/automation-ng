*** Settings ***
Documentation	Test that the initialization of the settings is correct
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../init.robot

Suite Setup		SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags	PART-1	GUI		${BROWSER}	${MODEL}	${VERSION}

*** Test Cases ***
Test Management Checkbox
    [Documentation]		Test that the checkbox works
    Select Checkbox     jquery=#management_enabled
    Click Element       jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    Checkbox Should Be Selected     jquery=#management_enabled
    Click Element     jquery=#management_enabled
    Wait Until Element is Visible      jquery=#saveButton
    Click Element       jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    Checkbox Should Not Be Selected     jquery=#management_enabled

Test Server and Client Buttons
	[Documentation]		Test that the Server and Client buttons work and text boxes work
    [Tags]	NON-CRITICAL	NEED-REVIEW
    Click Element       //*[@id="enabled"]
    Sleep		5s
    Wait Until Element is Visible		//*[@id="lps_enabled"]
    select checkbox		//*[@id="lps_enabled"]
    Wait Until Element is Visible		jquery=#saveButton
    Click Element		jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain Element		jquery=#renew_time
    Page Should Contain Element		jquery=#lease_time
#    Execute Javascript	window.scrollTo(0,document.body.scrollHeight);

Test Renew Errors
    [Tags]	NON-CRITICAL	NEED-REVIEW
    #try empty
    select checkbox		//*[@id="lps_enabled"]
    click element		//*[@id="lps_type"]
    SUITE:Text Input Error Tests     renew_time      ${TRUE}     ${EMPTY}
    #try 0
    SUITE:Text Input Error Tests     renew_time      ${TRUE}     0
    #try words
    SUITE:Text Input Error Tests     renew_time      ${TRUE}     aaaaa
    #try 8
    SUITE:Text Input Error Tests     renew_time      ${TRUE}     8
    #try 2
    SUITE:Text Input Error Tests     renew_time      ${FALSE}    2

Test Lease Errors
    [Tags]	NON-CRITICAL	NEED-REVIEW
    #try empty
    SUITE:Text Input Error Tests     lease_time      ${TRUE}     ${EMPTY}
    #try 0
    SUITE:Text Input Error Tests     lease_time      ${TRUE}     0
    #try words
    SUITE:Text Input Error Tests     lease_time      ${TRUE}     aaaaa
    #try 8
    SUITE:Text Input Error Tests     lease_time      ${TRUE}     1
    #try 2
    SUITE:Text Input Error Tests     lease_time      ${FALSE}    7

Test Auto Enrollment Settings
    [Documentation]		Test that the text box is working, no errors should be given i dont think. nodegrid-key <--important
    [Tags]	NON-CRITICAL	NEED-REVIEW
    Checkbox Should Be Selected     jquery=#autoEnroll
    Sleep       2s
    Page Should Contain Element     jquery=#autoPreSharedKey
    #does it work?
    SUITE:Text Input Error Tests      autoPreSharedKey    ${FALSE}    .//./.a22sddd
    #reset it
    SUITE:Text Input Error Tests      autoPreSharedKey    ${FALSE}    nodegrid-key

Test Enable Cluster
    [Documentation]		Test settings under the 2_enable cluster. Enable clustering access=????? what do i test? error on no settings picked
    unselect checkbox		//*[@id="enabled"]
    Checkbox Should Not Be Selected		//*[@id="enabled"]
    Click Element		//*[@id="enabled"]
    SUITE:Text Input Error Tests      clusterName     ${FALSE}     myCluster
    SUITE:Text Input Error Tests      clusterName     ${FALSE}     nodegrid
    capture page screenshot

Test Coordinator Base Settings
	[Documentation]		check base coordinator
	[Tags]	NON-CRITICAL	NEED-REVIEW
	click element					//span[@id='refresh-icon']
	GUI::Basic::Spinner Should Be Invisible
	click element					//input[@id='enabled']
	page should contain element		(//label[contains(.,'Type:')])[1]
	page should contain element		//input[@value='master']
	click element					//input[@value='master']
	Click Element					//input[@id='saveButton']
	capture page screenshot
	Page Should Contain Element		xpath=//p[@class="bg-danger text-danger"]
	Checkbox Should Be Selected		//input[@id='allowEnroll']
	Page Should Contain Element		//input[@id='preSharedKey']
	Page Should Contain Element		//label[contains(.,'Star')]
	Page Should Contain Element		//input[@id='pollRate']

Test Coordinator Auto Enroll Settings
    [Documentation]		check enroll
    [Tags]	NON-CRITICAL	NEED-REVIEW
    Wait Until Element is Visible	jquery=#allowEnroll
    GUI::Basic::Select Checkbox		//input[@id='allowEnroll']
    Element Should Be Visible		//input[@id='preSharedKey']
    Click Element					//input[@id='saveButton']
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain Element		xpath=//p[@class="bg-danger text-danger"]
    GUI::Basic::Select Checkbox		//input[@id='allowEnroll']
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Text Input Error Tests	preSharedKey	${FALSE}	thisIsMyKey

Test Coordinator Polling Rate
    [Documentation]		Coordinator: Everything besides polling rate is hunky dory. Check that error if <10 and >600 valid
    [Tags]	NON-CRITICAL	NEED-REVIEW
    SUITE:Text Input Error Tests     pollRate      ${FALSE}     11
    #empty
    SUITE:Text Input Error Tests     pollRate      ${TRUE}     ${EMPTY}
    #<10
    SUITE:Text Input Error Tests     pollRate      ${TRUE}     9
    #>600
    SUITE:Text Input Error Tests     pollRate      ${TRUE}     601
    #WeLikeToHaveFun
    SUITE:Text Input Error Tests     pollRate      ${TRUE}     asdfg
    #valid
    SUITE:Text Input Error Tests     pollRate      ${FALSE}    30

Test Peer Settings
    [Documentation]		Peer Ip must be valid or error thrown
    Wait Until Element is Visible    //label[normalize-space()='Peer']//input[@id='nodeType']
    GUI::Basic::Click Element     //label[normalize-space()='Peer']//input[@id='nodeType']
    #IP
    #input invalid ip address
    SUITE:Text Input Error Tests      masterAddr   ${TRUE}     not.val.idi.pad
    #another invalid
    SUITE:Text Input Error Tests      masterAddr   ${TRUE}     256.2.192.35
    #Valid Ip
    SUITE:Text Input Error Tests      masterAddr   ${TRUE}     565.245.224.13
    #KEYS
    #input whatever
    Wait Until Element is Visible    //label[normalize-space()='Coordinator']//input[@id='nodeType']
    GUI::Basic::Click Element    //label[normalize-space()='Coordinator']//input[@id='nodeType']
    Wait Until Element Is Not Visible   xpath=//*[@id='loading']    timeout=20s
    Wait Until Element is Visible      jquery=#saveButton
    Click Element               jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible

Test Automatic Enrollment Range Tab
	[Documentation]		Test that the tab is open and that the table works. Errors thrown with invalid ips ranges can be whatever you want. Error thrown with duplicate entries.
	[Tags]	NON-CRITICAL	NEED-REVIEW
    GUI::Basic::Cluster::Settings::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element       css=#range > .title_b
    GUI::Basic::Spinner Should Be Invisible
    #test add valid Enrollment Range
    SUITE:AER IP Input Text  12.12.12.12      12.12.12.192    ${FALSE}
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Element is Visible    peerTable
    ${COUNT}=   GUI::Basic::Count Table Rows  peerTable
    Should be Equal		'${COUNT}'		'1'
    #test add blatant
    SUITE:AER IP Input Text  im.dnm.not.val   129.255.12.125  ${TRUE}
    #test add invalid ip
    SUITE:AER IP Input Text  12.12.12.245     124.355.124.21  ${TRUE}
    #test duplicate ip
    SUITE:AER IP Input Text 	12.12.12.12		12.12.12.192	${TRUE}
    #test new ip
    SUITE:AER IP Input Text 	2.5.2.124		12.25.36.124	${FALSE}
    GUI::Basic::Spinner Should Be Invisible
    ${COUNT}=   GUI::Basic::Count Table Rows	peerTable
    log		${COUNT}
    Should be Equal		'${COUNT}'		'2'
    click element		//input[@type='checkbox']
    click element		//input[@id='delButton']
    GUI::Basic::Spinner Should Be Invisible
    capture page screenshot

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Cluster::Settings::Open Tab
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Close all Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	SUITE Disable Cluster
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout
	Close all Browsers

SUITE Disable Cluster
	GUI::Basic::Cluster::Settings::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Element is Visible       lps_enabled
	Unselect Checkbox	lps_enabled
	Wait Until Element is Visible       enabled
	Unselect Checkbox	enabled
	Page Should Not Contain	clusterName
	GUI::Basic::Save If Configuration Changed
	GUI::Basic::Spinner Should Be Invisible

SUITE:Text Input Error Tests
    [Arguments]  ${LOCATOR}     ${EXPECTS}      ${TEXT_ENT}
    [Documentation]     locator wants the id of the element expects wants a t/f of whether an error should be thrown text_ent wants the input
    Execute Javascript  document.getElementById("${LOCATOR}").scrollIntoView(true);
    Input Text			jquery=#${LOCATOR}		${empty}
    Input Text			jquery=#${LOCATOR}		${TEXT_ENT}
    Execute Javascript	window.scrollTo(0,0);
    Wait Until Element Is Not Visible	xpath=//*[@id='loading']	timeout=${GUI_DEFAULT_TIMEOUT}
    Click Element       jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    Run Keyword if		${EXPECTS}==${TRUE}		Page Should Contain Element		xpath=//p[@class="bg-danger text-danger"]


SUITE:AER IP Input Text
    [Arguments]  ${IP_ONE}     ${IP_TWO}      ${EXPECTS}
    [Documentation]     IP_ONE(TWO) expect the input for the two fields. EXPECTS wants a t/f of whether an error should be thrown
    ${LOC}=     run keyword and return status  Page Should Contain Element      jquery=#cancelButton
    Run Keyword If      ${LOC}==${FALSE}    Click Element   jquery=#addButton
    Wait Until Element Is Not Visible   xpath=//*[@id='loading']    timeout=20s
    Input Text      jquery=#clusterRangeLo      ${IP_ONE}
    Input Text      jquery=#clusterRangeHi      ${IP_TWO}
    Click Element   jquery=#saveButton
    Wait Until Element Is Not Visible   xpath=//*[@id='loading']    timeout=20s
    Run Keyword if      ${EXPECTS}==${TRUE}     Page Should Contain Element     xpath=//p[@class="bg-danger text-danger"]
    Page Should Not Contain Element     Page Should Not Contain Element         xpath=//p[@class="bg-danger text-danger"]