*** Settings ***
Documentation	Test that the default settings are correct
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-1	GUI	${BROWSER}	${MODEL}	${VERSION}	CHROME	FIREFOX

*** Test Cases ***
Check Peers
    GUI::Basic::Cluster::Peers::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//div[@id="notice"]
	Page Should Contain	Cluster must be enabled. Cluster Peers is only available for Coordinator nodes in star mode.

Check Default Settings
	GUI::Basic::Cluster::Settings::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	Checkbox Should Be Selected	jquery=#autoEnroll
	unselect checkbox		//*[@id="enabled"]
	Checkbox Should Not Be Selected		//*[@id="enabled"]
	Element Should Be Visible	jquery=#management_enabled
	Checkbox Should Not Be Selected	jquery=#management_enabled
	Element Should Be Visible	jquery=#lps_enabled
	${IS_LPS_ENABLED}=	Run Keyword And Return Status	 Checkbox Should Be Selected 	//*[@id='lps_enabled']
	Run Keyword If	${IS_LPS_ENABLED}==${FALSE}	Fix Checkbox	//*[@id='lps_enabled']
	Select Radio Button	lps_type	client
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

Check Management
	GUI::Basic::Cluster::Management::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//div[@id="notice"]

*** Keywords ***

SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Cluster::Open
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout
	Close all Browsers

Fix Checkbox
	[Arguments]	${LOCATOR}
	Click Element	xpath=${LOCATOR}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible