*** Settings ***
Resource	../init.robot
Documentation	Clear the system configuration before starting cluster test cases.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags   PART-1	CLI	GUI	CHROME	FIREFOX	FACTORY_SETTINGS_CLUSTER

Suite Setup	SUITE:Setup
Suite Teardown	Run Keywords	SUITE:Teardown	${HOMEPAGE}	${HOST}	AND	SUITE:Teardown	${HOMEPAGEPEER}	${HOSTPEER}

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}

*** Test Cases ***
Test to apply Factory Settings that is going to clear host's system
	CLI:Apply Factory Settings
	CLI:QA Device First Settings	${HOST}	${VERSION}
	[Teardown]	SUITE:Teardown	${HOMEPAGE}	${HOST}

Test to apply Factory Settings that is going to clear peer's system
	CLI:Open	session_alias=peer_session	HOST_DIFF=${HOSTPEER}
	CLI:Apply Factory Settings
	CLI:QA Device First Settings	${HOSTPEER}	${VERSION}
	[Teardown]	SUITE:Teardown	${HOMEPAGEPEER}	${HOSTPEER}

*** Keywords ***
SUITE:Setup
	CLI:Open
	${VERSION}=	CLI:Get System Major And Minor Version
	Set Suite Variable	${VERSION}

SUITE:Teardown
	[Arguments]	${HOMEPAGEDIFF}	${HOST_DIFF}
	GUI::Basic::Check If Host Has Factory Settings	PAGE=${HOMEPAGEDIFF}
	Close All Browsers
	${1ST_LOGIN}	Run Keyword And Return Status	GUI::Basic::Open And Login Nodegrid	PAGE=${HOMEPAGEDIFF}
	Close All Browsers
	IF	'${NGVERSION}' >= '5.0' and not ${1ST_LOGIN}
		${STATUS}	Run Keyword And Return Status	GUI::Basic::Open And Login Nodegrid	PAGE=${HOMEPAGEDIFF}	USERNAME=${DEFAULT_USERNAME}	PASSWORD=${QA_PASSWORD}
		Run Keyword If	${STATUS}	GUI::Basic::Enable Security For SSH Root Access By GUI and Change the Passwords By CLI	HOST_DIFF=${HOST_DIFF}
	END
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid	PAGE=${HOMEPAGEDIFF}
	GUI::Basic::Logout And Close Nodegrid