*** Settings ***
Documentation	Test that the peer tab works properly
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-1	GUI	${BROWSER}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW

*** Variables ***
${SHAREDKEY}	shared_key
${MESH_MODE}	no

${DEVCOORD_NAME} 	COORDINATOR
${DEVCOORD_IPADDR}	${HOST}

*** Test Cases ***
Test Peer Table Display
	GUI::Basic::Spinner Should Be Invisible
	GUI::Cluster::Add Coordinator	${DEVCOORD_NAME} 	${DEVCOORD_IPADDR}	${SHAREDKEY}	${MESH_MODE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Cluster::Peers::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	wait until page contains element	jquery=#peerTable
	Element Should Be Visible	jquery=#peerTable
	Page Should Not Contain Element	xpath=//div[@id="notice"]

Test Deleting Main Item
	#Should throw error and complain about not being able to delete NG.localdomain and need to go to the settings or something
	wait until page contains element	xpath=//th[@class="sorting_disabled selectable"]
	Sleep	5s
	Click Element	xpath=//th[@class="sorting_disabled selectable"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=#delButton
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	jquery=#main_doc > div:nth-child(1) > div > div

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Cluster::Peers::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Cluster::Open Automatic Enrollment Range
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//*[@id="thead"]/tr/th[1]/input
	Click Element	jquery=#delButton
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Cluster::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=#enabled
	Click Element	xpath=//div/div[4]/label/input[@id="lps_type"]
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	30	2	SUITE Disable Cluster
	Wait Until Keyword Succeeds	30	2	GUI::ManagedDevices::Delete Device If Exists	${DEVCOORD_NAME}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout
	Close all Browsers

SUITE Disable Cluster
	GUI::Basic::Cluster::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Unselect Checkbox	lps_enabled
	Unselect Checkbox	enabled
	Page Should Not Contain	clusterName
	Wait Until Keyword Succeeds	30	2	GUI::Basic::Save
	Sleep	5s
	GUI::Basic::Spinner Should Be Invisible