*** Settings ***
Documentation	Test target accessing coordinator and peer (local and managed devices)
...	TTYD consoles from Table and Tree views using cluster in star mode
Metadata	Version 1.0
Metadata	Executed At ${HOST}
Metadata	Executed with	${BROWSER}
Resource	../init.robot
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Test Teardown	SUITE:Test Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${PEER_IP}	${HOSTPEER}
${PEER_HOMEPAGE}	${HOMEPAGEPEER}
${COORD_IP}	${HOST}
${COORD_HOMEPAGE}	${HOMEPAGE}
${DEVICE}	test_device
${DEVICE_TYPE}	device_console
${DEVICE_IP}	127.0.0.1
${CLUSTER_LICENSE}	${FIVE_DEVICES_CLUSTERING_LICENSE_2}
${DEFAULT_HOSTNAME}	${HOSTNAME_NODEGRID}
${COORD_HOSTNAME}	test-nodegrid-coord
${PEER_HOSTNAME}	test-nodegrid-peer
${DEV_COORD_NAME}	test-dev-nodegrid-coord
${DEV_PEER_NAME}	test-dev-nodegrid-peer
${DEFAULT_DOMAIN_NAME}	localdomain
${COORD_DOMAIN_NAME}	test-localdomain-coord
${PEER_DOMAIN_NAME}	test-localdomain-peer
${PRE_SHARED_KEY}	test-qa-123
${STAR_MODE}	yes

*** Test Cases ***
Test Access Peer Console From Table View
	[Setup]	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	SUITE:Accessing Console From Table View Should Succeed	${PEER_HOSTNAME}	${PEER_DOMAIN_NAME}

Test Access Peer Managed Device Console From Table View
	[Setup]	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	SUITE:Accessing Managed Device Console From Table View Should Succeed
	...	${PEER_HOSTNAME}	${PEER_DOMAIN_NAME}	${DEV_PEER_NAME}

Test Access Peer Console From Tree View
	[Setup]	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	SUITE:Accessing Console From Tree View Should Succeed	${PEER_HOSTNAME}	${PEER_DOMAIN_NAME}

Test Access Peer Managed Device Console From Tree View
	[Setup]	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	SUITE:Accessing Managed Device Console From Tree View Should Succeed
	...	${PEER_HOSTNAME}	${PEER_DOMAIN_NAME}	${DEV_PEER_NAME}

*** Keywords ***
SUITE:Setup
	Sleep	1m
	SUITE:Coordinator Setup
	SUITE:Peer Setup
	Close All Browsers

SUITE:Teardown
	SUITE:Peer Teardown
	SUITE:Coordinator Teardown
	Close All Browsers

SUITE:Test Teardown
	Close All Browsers

SUITE:Coordinator Setup
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	SUITE:Change Hostname To "${COORD_HOSTNAME}" And Domain Name To "${COORD_DOMAIN_NAME}"
	GUI::ManagedDevices::Delete Device If Exists	${DEV_COORD_NAME}
	GUI::Cluster::Add Coordinator	${DEV_COORD_NAME}	${COORD_IP}	${PRE_SHARED_KEY}	${STAR_MODE}
	GUI::Cluster::Delete Licenses
	GUI::Cluster::Add License	${CLUSTER_LICENSE}

SUITE:Peer Setup
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	SUITE:Change Hostname To "${PEER_HOSTNAME}" And Domain Name To "${PEER_DOMAIN_NAME}"
	GUI::ManagedDevices::Delete Device If Exists	${DEV_PEER_NAME}
	GUI::Cluster::Add Peer	${DEV_PEER_NAME}	${PEER_IP}	${COORD_IP}	${PRE_SHARED_KEY}
	SUITE:Wait For License Lease From Coordinator To Complete

SUITE:Peer Teardown
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Cluster::Remove Peer From Coordinator If Exist
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::ManagedDevices::Delete Device If Exists	${DEV_PEER_NAME}
	GUI::Cluster::Disable Cluster
	SUITE:Change Hostname To "${DEFAULT_HOSTNAME}" And Domain Name To "${DEFAULT_DOMAIN_NAME}"

SUITE:Coordinator Teardown
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::ManagedDevices::Delete Device If Exists	${DEV_COORD_NAME}
	GUI::Cluster::Disable Cluster
	GUI::Cluster::Delete Licenses
	SUITE:Change Hostname To "${DEFAULT_HOSTNAME}" And Domain Name To "${DEFAULT_DOMAIN_NAME}"

SUITE:Change Hostname To "${HOSTNAME}" And Domain Name To "${DOMAIN_NAME}"
	GUI::Network::Open Settings Tab
	GUI::Basic::Spinner Should Be Invisible
	Input Text		xpath=//input[@id='hostname']		${HOSTNAME}
	Input Text		xpath=//input[@id='domainname']		${DOMAIN_NAME}
	GUI::Basic::Save If Configuration Changed

SUITE:Wait For License Lease From Coordinator To Complete
	Wait Until Keyword Succeeds	2m	6s	SUITE:Check License Lease

SUITE:Check License Lease
	GUI::Basic::System::License::Open Tab
	Page Should Contain Element	//table[@id="license_table"]/tbody/tr/td[text()="Leased From"]

SUITE:Accessing Console From Table View Should Succeed
	[Arguments]	${HOSTNAME}	${DOMAIN_NAME}
	GUI::Access::Table::Wait Until Cluster Node Is Visible	${HOSTNAME}.${DOMAIN_NAME}
	GUI::Access::Table::Access Console	${HOSTNAME}.${DOMAIN_NAME}
	Sleep	5
	Wait Until Keyword Succeeds	3x	3s	SUITE:Executing Some Commands On Console Should Succeed	${HOSTNAME}

SUITE:Accessing Console From Tree View Should Succeed
	[Arguments]	${HOSTNAME}	${DOMAIN_NAME}
	GUI::Access::Tree::Wait Until Cluster Node Is Visible	${HOSTNAME}
	GUI::Access::Tree::Access Console	${HOSTNAME}	${DOMAIN_NAME}
	Sleep	5
	Wait Until Keyword Succeeds	3x	3s	SUITE:Executing Some Commands On Console Should Succeed	${HOSTNAME}

SUITE:Accessing Managed Device Console From Table View Should Succeed
	[Arguments]	${HOSTNAME}	${DOMAIN_NAME}	${DEVICE_NAME}
	GUI::Access::Table::Wait Until Cluster Node Is Visible	${HOSTNAME}.${DOMAIN_NAME}

	GUI:Access::Table::Access Device Console	${DEVICE_NAME}	${HOSTNAME}.${DOMAIN_NAME}
	Sleep	5
	Wait Until Keyword Succeeds	3x	3s	SUITE:Executing Some Commands On Console Should Succeed	${HOSTNAME}

SUITE:Accessing Managed Device Console From Tree View Should Succeed
	[Arguments]	${HOSTNAME}	${DOMAIN_NAME}	${DEVICE_NAME}
	GUI::Access::Tree::Wait Until Cluster Node Is Visible	${HOSTNAME}
	GUI:Access::Tree::Access Device Console	${DEVICE_NAME}	${HOSTNAME}
	Sleep	5
	Wait Until Keyword Succeeds	3x	3s	SUITE:Executing Some Commands On Console Should Succeed	${HOSTNAME}

SUITE:Executing Some Commands On Console Should Succeed
	[Arguments]	${HOSTNAME}
	${OUTPUT}=	GUI:Access::Generic Console Command Output	whoami
	Should Contain	${OUTPUT}	${USERNAME}
	${OUTPUT}=	GUI:Access::Generic Console Command Output	hostname
	Should Contain	${OUTPUT}	${HOSTNAME}
	${OUTPUT}=	GUI:Access::Generic Console Command Output	ls
	Should Contain	${OUTPUT}	access/	system/	settings/
	${OUTPUT}=	GUI:Access::Generic Console Command Output	pwd
	Should Contain	${OUTPUT}	/