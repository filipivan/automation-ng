*** Settings ***
Documentation	Test for Configure Cluster And Do Search
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}

*** Variables ***
${DEV_COORD_NAME}         co-ordinator
${DEV_PEER_NAME}          peer
${PEER_IP}                ${HOSTPEER}
${PEER_HOMEPAGE}          ${HOMEPAGEPEER}
${COORD_IP}               ${HOST}
${COORD_HOMEPAGE}         ${HOMEPAGE}
${Name}                 dummy-dev-peer
${Type}            device_console
${CLUSTER_LICENSE_1}        HPI4V-UUKB9-2B7FO-W5PSR
${CLUSTER_LICENSE_2}        F0855-DF7TJ-G112X-EJ48R
${DEFAULT_HOSTNAME}       ${HOSTNAME_NODEGRID}
${PRE_SHARED_KEY}         admin
${IS_STAR_MODE}     Mesh
${DOMAIN_NAME}      co-ordinator
${Co-ordinator_addrs}       ${HOST}
#${License_1}        HOENS-I7UT3-FR1OZ-FBZBR            ###used on running locally
#${License_2}        FF4IH-KJ90E-1ZWR8-8RTVR            ###used on running locally

*** Test Cases ***
Test Case to Configure Co-ordinator Side
    [Tags]	NON-CRITICAL
    GUI::Basic::Open And Login Nodegrid     PAGE=${COORD_HOMEPAGE}
    GUI::Basic::System::License::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="licensevalue"]     ${CLUSTER_LICENSE_1}
	GUI::Basic::Save If Configuration Changed
    GUI::Cluster::Open Settings Tab
    wait until page contains element		//*[@id="enabled"]
	Select Checkbox		//*[@id="enabled"]
	select checkbox     //*[@id="lps_enabled"]
	Select Radio Button	nodeType	master
	Input Text      //*[@id="preSharedKey"]         ${PRE_SHARED_KEY}
	Click Element     //*[@id="starMode"]
	Click Element       //*[@id="cluster_enabled"]
	Wait Until Element is Visible      //*[@id="lps_enabled"]
	select checkbox       //*[@id="lps_enabled"]
	GUI::Basic::Save If Configuration Changed	SPINNER_TIMEOUT=60s
	GUI::Basic::Spinner Should Be Invisible

Test Case to Configure Peer Side
	GUI::Basic::Open And Login Nodegrid     PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::System::License::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="licensevalue"]     ${CLUSTER_LICENSE_2}
	GUI::Basic::Save If Configuration Changed
    GUI::Cluster::Open Settings Tab
    GUI::Basic::Spinner Should Be Invisible
    wait until page contains element		//*[@id="enabled"]
	Select Checkbox		//*[@id="enabled"]
    Click Element       css=.radio:nth-child(3) #nodeType
    Input Text      //*[@id="masterAddr"]       ${Co-ordinator_addrs}
    Input Text      //*[@id="preSharedKey2"]        ${PRE_SHARED_KEY}
    select checkbox     //*[@id="cluster_enabled"]
    GUI::Basic::Save If Configuration Changed	SPINNER_TIMEOUT=60s
    GUI::Basic::Spinner Should Be Invisible

Test case to Verify the Peer status On Peer Side
    GUI::Basic::Open And Login Nodegrid     PAGE=${PEER_HOMEPAGE}
    GUI::Basic::Cluster::Peers::open tab
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Check the cluster status
    Page Should Contain Element     xpath=//td[contains(.,'Peer')]
    Page Should Contain Element     xpath=//td[contains(.,'nodegrid.localdomain')]

Test case to Verify the Peer status On Co-Ordinator Side
    GUI::Basic::Open And Login Nodegrid     PAGE=${COORD_HOMEPAGE}
    GUI::Basic::Cluster::Peers::open tab
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Check the cluster status
    Page Should Contain Element     xpath=//td[contains(.,'Peer')]
    Page Should Contain Element     xpath=//td[contains(.,'nodegrid.localdomain')]

Test Case to Add Managed Device on Peer Side
    GUI::Basic::Open And Login Nodegrid     PAGE=${PEER_HOMEPAGE}
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists	${Name}
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Input Text	    //*[@id='spm_name']	    ${Name}
    Select From List By Label   //*[@id="type"]    ${Type}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible

Test Case to Verify on Co-ordinator Side for Added Managed device
    GUI::Basic::Open And Login Nodegrid     PAGE=${COORD_HOMEPAGE}
    GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    # Run Keyword If  '${NGVERSION}'=='4.2'       Input Text      //*[@id="search_expr1"]    ${Name}
    # Run Keyword If  '${NGVERSION}'>'4.2'       Input Text      //*[@id="filter_field"]     ${Name}
    Input Text      //*[@id="search_expr1"]    ${Name}
    GUI::Basic::Spinner Should Be Invisible
    # Run Keyword If  '${NGVERSION}'=='4.2'       Press Keys    //*[@id="search_expr1"]    ENTER
    # Run Keyword If  '${NGVERSION}'>'4.2'       Press Keys    //*[@id="filter_field"]    ENTER
    Press Keys    //*[@id="search_expr1"]    ENTER
    GUI::Basic::Spinner Should Be Invisible
    Sleep       10s
    page should contain     ${Name}

Test Case to Delete Configuration On Peer Side
    GUI::Basic::Open And Login Nodegrid     PAGE=${PEER_HOMEPAGE}
	 GUI::Basic::Spinner Should Be Invisible
    GUI::Cluster::Disable Cluster
    GUI::Cluster::Delete Licenses
    GUI::ManagedDevices::Delete Device If Exists	${Name}

Test Case to Delete Configuration On Co-ordinator Side
    GUI::Basic::Open And Login Nodegrid     PAGE=${COORD_HOMEPAGE}
    GUI::Basic::Spinner Should Be Invisible
    GUI::Cluster::Disable Cluster
    GUI::Cluster::Delete Licenses

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:Check the cluster status
    Wait Until Keyword Succeeds  50x  20s     SUITE:Check the status

SUITE:Check the status
    GUI::Basic::Cluster::Peers::open tab
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain Element         xpath=//td[contains(.,'Online')]
