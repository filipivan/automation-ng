*** Settings ***
Documentation	Editing managed devices -> types for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Variable ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}


*** Test Cases ***
Test All Device Types Exist
    Page Should Contain Element    //*[@id="cimc_ucs"]
    Page Should Contain Element    //*[@id="console_server_acs"]
    Page Should Contain Element    //*[@id="console_server_acs6000"]
    Page Should Contain Element    //*[@id="console_server_digicp"]
    Page Should Contain Element    //*[@id="console_server_lantronix"]
    Page Should Contain Element    //*[@id="console_server_nodegrid"]
    Page Should Contain Element    //*[@id="console_server_opengear"]
    Page Should Contain Element    //*[@id="device_console"]
    Page Should Contain Element    //*[@id="drac"]
    Page Should Contain Element    //*[@id="idrac6"]
    Page Should Contain Element    //*[@id="ilo"]
    Page Should Contain Element    //*[@id="ilom"]
    Page Should Contain Element    //*[@id="imm"]
    Page Should Contain Element    //*[@id="infrabox"]
    Page Should Contain Element    //*[@id="ipmi_1.5"]
    Page Should Contain Element    //*[@id="ipmi_2.0"]
    Page Should Contain Element    //*[@id="kvm_aten"]
    Page Should Contain Element    //*[@id="kvm_dsr"]
    Page Should Contain Element    //*[@id="kvm_mpu"]
    Page Should Contain Element    //*[@id="kvm_raritan"]
    Page Should Contain Element    //*[@id="netapp"]
    Page Should Contain Element    //*[@id="pdu_apc"]
    Page Should Contain Element    //*[@id="pdu_baytech"]
    Page Should Contain Element    //*[@id="pdu_cyberpower"]
    Page Should Contain Element    //*[@id="pdu_eaton"]
    Page Should Contain Element    //*[@id="pdu_enconnex"]
    Page Should Contain Element    //*[@id="pdu_geist"]
    Page Should Contain Element    //*[@id="pdu_mph2"]
    Page Should Contain Element    //*[@id="pdu_pm3000"]
    Page Should Contain Element    //*[@id="pdu_raritan"]
    Page Should Contain Element    //*[@id="pdu_servertech"]
    Page Should Contain Element    //*[@id="usb"]
    Page Should Contain Element    //*[@id="usb_kvm"]
    Page Should Contain Element    //*[@id="usb_sensor"]
    Page Should Contain Element    //*[@id="virtual_console_kvm"]
    Page Should Contain Element    //*[@id="virtual_console_vmware"]

Test Clone and Delete Buttons Appear
    Click Element       //*[@id="cimc_ucs"]/td[1]/input
    Wait Until Element Is Enabled       //*[@id="nonAccessControls"]/input[1]
    Wait Until Element Is Enabled       //*[@id="delButton"]
    Click Element       //*[@id="console_server_acs"]/td[1]/input
    Element Should Be Enabled       //*[@id="delButton"]
    Element Should Be Disabled       //*[@id="nonAccessControls"]/input[1]
    Click Element       //*[@id="console_server_acs"]/td[1]/input

Test Device Clone
    Select Checkbox     xpath=//tr[@id='cimc_ucs']/td/input
    Click element       css=.button:nth-child(1)
    GUI::Basic::Spinner Should Be Invisible
    gui::auditing::auto input tests     spmtargettype_name      ;   @#$     ,./;'   name
    GUI::Basic::Spinner Should Be Invisible
    gui::basic::delete rows in table    \#SPMTargettypeTable      name

Test Device Drilldown
    Click Element       //*[@id="cimc_ucs"]/td[2]/a
    GUI::Basic::Spinner Should Be Invisible
    @{CHK}=     Create List     SSH     Telnet      IPMI    None
    @{LST}=     Get List Items  jquery=#protocol
    Should Be Equal             ${CHK}  ${LST}
    Page Should Contain     Device Type Name:
    Page Should Contain     Family
    Page Should Contain     Protocol
    Page Should Contain     Login Prompt:
    Page Should Contain     Password Prompt:
    Page Should Contain     Command Prompt:
    Page Should Contain     Console Escape Sequence:
    Select From List By Value       jquery=#protocol    ssh
    Page Should Contain     SSH Options:




*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Types::Open Tab
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
