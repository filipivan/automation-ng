*** Settings ***
Documentation	Testing errors in managed devices -> views for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

*** Test Cases ***
Test Add Tree Name Errors
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	jquery=#aggregation1
	Input Text	jquery=#agg_interval1	Big Number
	GUI::Basic::Auto Input Tests	spm_container_name	${EMPTY}	$$$	12345678901234567890123456789012345678901234567890123456789012345	Dummy Tree

Test Add Tree No Parent Error
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	addButton
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	jquery=#aggregation1
	Input Text	jquery=#agg_interval1	Big Number
	GUI::Basic::Input Text::Error	spm_container_name	$$$

	Unselect Checkbox	jquery=#aggregation1
	Click Element	saveButton
	Sleep	30
	Page Should Contain	Error on page. Please check.

Test Add Tree Aggregation Name Errors
	Select Checkbox		jquery=#aggregation1
	GUI::Basic::Auto Input Tests	agg_name1	${EMPTY}	$$$	12345678901234567890123456789012345678901234567890123456789012345	Big Mistake	Agg1

Test Add Tree Aggregation Interval Errors
	Input Text	jquery=#agg_name1	Wont Work
	GUI::Basic::Auto Input Tests	agg_interval1	${EMPTY}	$$$	12345678901	9	Nahh	20
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible

Test Add Image Name Errors
	GUI::Basic::Wait Until Element Is Accessible	//*[@id="image"]
	Click Element	//*[@id="image"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	jquery=#refresh	Big Number
	GUI::Basic::Auto Input Tests	name	${EMPTY}	$$$	12345678901234567890123456789012345678901234567890123456789012345	Dummy Image

Test Add Image Refresh Errors
	GUI::Basic::Auto Input Tests	refresh	${EMPTY}	$$$	12345678901	4	Nahh	20
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Views::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
