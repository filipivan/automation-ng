*** Settings ***
Documentation	Editing managed devices -> views for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***
Test Tree
    GUI::Basic::Add Button Should Be Visible
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
    Input Text  spm_container_name      Group1
    Click Element  //*[@id="0"]
    Check Aggregation Checkbox      1
    Check Aggregation Checkbox      2
    Check Aggregation Checkbox      3
    Check Aggregation Checkbox      4
    Check Aggregation Checkbox      5
    GUI::Basic::Cancel
    GUI::Basic::Spinner Should Be Invisible

Check No Addition
    ${CHK}=     run keyword and return status       Page Should Contain Element     xpath=//*[@id="tbody"]/tr[4]
    Run Keyword if      ${CHK}      Remove From View

Test Add Image Screen
    GUI::Basic::Wait Until Element Is Accessible    //*[@id="image"]
    Click Element       //*[@id="image"]
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain Element     //*[@id="name"]
    Page Should Contain Element     //*[@id="refresh"]
    GUI::Basic::Select Radio Button     image_sel       image_sel_local
    Page Should Contain Element     //*[@id="image_strip"]
    GUI::Basic::Select Radio Button     image_sel        image_sel_client
    Page Should Contain Element     //*[@id="image_strip_upload"]

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::ManagedDevices::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Views::Open Tab
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

Check Aggregation Checkbox
    [Arguments]     ${agg_num}
    Page Should contain Checkbox    jquery=#aggregation${agg_num}
    Select Checkbox                 jquery=#aggregation${agg_num}
    Page Should Contain Element     jquery=#agg_name${agg_num}
    Page Should Contain Element     jquery=#agg_type${agg_num}
    Page Should Contain Element     jquery=#agg_filter${agg_num}
    Page Should Contain Element     jquery=#agg_interval${agg_num}
    Page Should contain Checkbox    jquery=#agg_sum${agg_num}
    Page Should contain Checkbox    jquery=#agg_avg${agg_num}
    Unselect Checkbox               jquery=#aggregation${agg_num}

Remove From View
    Select Checkbox     xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[4]/td[1]/input
    Click Element       jquery=#delButton
    Handle Alert
    GUI::Basic::Spinner Should Be Invisible