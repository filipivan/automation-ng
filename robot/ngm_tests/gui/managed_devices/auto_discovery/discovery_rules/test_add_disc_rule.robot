*** Settings ***
Documentation	ManagedDevices::Auto Discovery::Discovery Rule
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

*** Test Cases ***
Add discovery rule
	GUI::Basic::Managed Devices::Devices::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	# To add a discovery rule, we need at least one device
	${ret}=	GUI::Basic::Count Table Rows	SPMTable
	Run Keyword If	"${ret}" == "0"	GUI::ManagedDevices::Add Dummy Device Console	${DUMMY_DEVICE_CONSOLE_NAME}_disc_rule	device_console	127.0.0.1	22	root	no	${ROOT_PASSWORD}	ondemand

	GUI::ManagedDevices::Open Discovery Rules
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add Discovery Rule	dummy_disc_rule
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Discovery Rule	dummy_disc_rule
    GUI::Basic::Spinner Should Be Invisible
	${list}=	Create List	${DUMMY_DEVICE_CONSOLE_NAME}_disc_rule
	GUI::ManagedDevices::Delete Devices If Exists	${list}

Test Discovery Rule Name
	GUI::ManagedDevices::Delete All Discovery Rules
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open Discovery Rules
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	operation	discard
	GUI::Basic::Auto Input Tests	discovery_name	**	Dummy

Test MAC Address
	Click Element	xpath=//td[2]/a
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Auto Input Tests	macaddr	**	str	23	23:23:PU:XJ:23:23	12:12:12:12:12:12

Test Console Port List
	Click Element	xpath=//td[2]/a
	GUI::Basic::Spinner Should Be Invisible
	Select Radio Button	source	consoleserver
	GUI::Basic::Auto Input Tests	portlistcas	**	str	100	12

Test KVM Port List
	Click Element		xpath=//td[2]/a
	GUI::Basic::Spinner Should Be Invisible
	Select Radio Button	source	kvmport
	GUI::Basic::Auto Input Tests	portlistkvm	**	str	100	12

Test Delete List
	Select Checkbox		xpath=//thead[@id='thead']/tr/th/input
	Click Element	jquery=#delButton
	Handle Alert
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

SUITE:Add Discovery Rule
	[Arguments]	${DISC_NAME}	${SCREENSHOT}=FALSE
	[Tags]	GUI	MANAGED_DEVICES	DISCOVERY
	Wait Until Element Is Visible	id=SPMDTable
	GUI::Basic::Add
	Wait Until Element Is Visible	jquery=#discovery_name
	Input Text	jquery=#discovery_name	${DISC_NAME}
	GUI::Basic::Save
	GUI::Basic::Table Should Has Row With Id	SPMDTable	${DISC_NAME}