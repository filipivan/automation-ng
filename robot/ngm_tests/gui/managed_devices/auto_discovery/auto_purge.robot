*** Settings ***
Documentation	Auto-purge Inactive Ports
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX		NON-CRITICAL	NEED-REVIEW
Default Tags	EXCLUDEIN4_2
...
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${Perle}	perle
${discoverd_device}	DO_NOT_TURN_OFF_Opengear

*** Test Cases ***
# Adding non-critical tags for all the test cases becuase the perle server is behaving inconsistently.Need to take a look later
Add perle server
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=spm_name
	Input Text	id=spm_name	${Perle}
	Select From List By Value	jquery=#type	console_server_perle
	Click Element	id=phys_addr
	Input Text	id=phys_addr	${PERLE_SCS_SERVER_IP}
	Click Element	id=username
	Input Text	id=username	${PERLE_SCS_SERVER_USERNAME}
	Click Element	id=passwordfirst
	Input Text	id=passwordfirst	${PERLE_SCS_SERVER_PASSWORD}
	Click Element	id=passwordconf
	Input Text	id=passwordconf	${PERLE_SCS_SERVER_PASSWORD}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Add auto discovery Rules
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#spm_discovery > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=discovery_name
	Input Text	id=discovery_name	perle_scs_rule
	Select From List By Value	jquery=#status	enabled
	Select Radio Button	source	consoleserver
	Select From List By Value	jquery=#seed	perle
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Reset the Discovery logs and check Auto Discovery devices
	[Tags]	NON-CRITICAL	NEED-REVIEW
#	Checked manually working, able to see clone action on discovery logs page
	Click Element	css=#spm_discoverylogs > .title_b
	GUI::Basic::Click Element	//*[@id='reset_logs']
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#discovery_now > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="cas|perle"]/td[1]/input
	GUI::Basic::Click Element	//*[@id='discovernow']
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="cas|perle"]/td[1]/input
	GUI::Basic::Click Element	//*[@id='discovernow']
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	30x	5s	SUITE:Check Auto Discovery Devices

Login to Perle and trigger the DUT
	[Tags]	NON-CRITICAL	NEED-REVIEW
	${BROWSER}	Convert To Lower Case	${BROWSER}
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nOpening Browser: ${BROWSER}	INFO	console=yes
	Run Keyword If	'${BROWSER}' == 'chrome' or '${BROWSER}' == 'headlesschrome'	Wait Until Keyword Succeeds	3x	3s	Open Browser	${PERLE_SCS_SERVER_HOMEPAGE}	${BROWSER}	options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors")
	Run Keyword If	'${BROWSER}' == 'firefox' or '${BROWSER}' == 'headlessfirefox'	Wait Until Keyword Succeeds	3x	3s	Open Browser	${PERLE_SCS_SERVER_HOMEPAGE}	${BROWSER}	options=set_preference("moz:dom.disable_beforeunload", "true")
	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\nBrowser ${BROWSER} opened	INFO	console=yes
	Maximize Browser Window
	Wait Until Page Contains Element	//*[@id="username"]
	Wait Until Page Contains Element	//*[@id="password"]
	Input Text	username	${PERLE_SCS_SERVER_USERNAME}
	Input Text	password	${PERLE_SCS_SERVER_PASSWORD}
	Click Element	login
	Wait Until Page Contains Element	//*[@id="sections"]/tbody/tr[3]/td[2]/input
	Click Element	//*[@id="sections"]/tbody/tr[3]/td[2]/input
	Wait Until Page Contains Element	//*[@id="main_tbl"]/tbody/tr[2]/td[2]/input
	Click Element	//*[@id="main_tbl"]/tbody/tr[2]/td[2]/input
	sleep	2s
	Click Element	css=#yui-dt0-bdrow0-cell0 > input[type=checkbox]
	sleep	2s
	Click Element	css=table.main_table:nth-child(3) form.rt02 table.rt02 tbody:nth-child(1) tr:nth-child(1) td:nth-child(1) p:nth-child(2) > input:nth-child(1)
	sleep	5s
	Wait Until Page Contains Element	//*[@id="nav_reboot_btn"]
	Click Element	//*[@id="nav_reboot_btn"]
	sleep	5s
	Handle Alert	action=ACCEPT
	sleep	100s

Verify Auto-Discovery of Devices
	[Tags]	NON-CRITICAL	NEED-REVIEW		NG-9910
	switch browser	1
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#spm_discoverylogs > .title_b
	GUI::Basic::Click Element	//*[@id='reset_logs']
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#discovery_now > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="cas|perle"]/td[1]/input
	GUI::Basic::Click Element	//*[@id='discovernow']
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="cas|perle"]/td[1]/input
	GUI::Basic::Click Element	//*[@id='discovernow']
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	2m	1s	SUITE:Check Auto Discovery Devices Disabled

Test case to enable perle server port
	[Tags]	NON-CRITICAL	NEED-REVIEW
	${BROWSER}	Convert To Lower Case	${BROWSER}
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nOpening Browser: ${BROWSER}	INFO	console=yes
	Run Keyword If	'${BROWSER}' == 'chrome' or '${BROWSER}' == 'headlesschrome'	Wait Until Keyword Succeeds	3x	3s	Open Browser	${PERLE_SCS_SERVER_HOMEPAGE}	${BROWSER}	options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors")
	Run Keyword If	'${BROWSER}' == 'firefox' or '${BROWSER}' == 'headlessfirefox'	Wait Until Keyword Succeeds	3x	3s	Open Browser	${PERLE_SCS_SERVER_HOMEPAGE}	${BROWSER}	options=set_preference("moz:dom.disable_beforeunload", "true")
	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\nBrowser ${BROWSER} opened	INFO	console=yes	Maximize Browser Window
	Wait Until Page Contains Element	//*[@id="username"]
	Wait Until Page Contains Element	//*[@id="password"]
	Input Text	username	${PERLE_SCS_SERVER_USERNAME}
	Input Text	password	${PERLE_SCS_SERVER_PASSWORD}
	Click Element	login
	Wait Until Page Contains Element	//*[@id="sections"]/tbody/tr[3]/td[2]/input
	Click Element	//*[@id="sections"]/tbody/tr[3]/td[2]/input
	Wait Until Page Contains Element	//*[@id="main_tbl"]/tbody/tr[2]/td[2]/input
	Click Element	//*[@id="main_tbl"]/tbody/tr[2]/td[2]/input
	Wait Until Page Contains Element	css=#yui-dt0-bdrow0-cell0 > input[type=checkbox]
	Click Element	css=#yui-dt0-bdrow0-cell0 > input[type=checkbox]
	sleep	2s
	Click Element	css=table.main_table:nth-child(3) form.rt02 table.rt02 tbody:nth-child(1) tr:nth-child(1) td:nth-child(1) p:nth-child(2) > input:nth-child(1)
	Wait Until Page Contains Element	//*[@id="nav_reboot_btn"]
	Click Element	//*[@id="nav_reboot_btn"]
	sleep	5s
	Handle Alert	action=ACCEPT
	sleep	130s

Verify the Auto-discovery
	[Tags]	NON-CRITICAL	NEED-REVIEW
	switch browser	1
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#spm_discoverylogs > .title_b
	GUI::Basic::Click Element	//*[@id='reset_logs']
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#discovery_now > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="cas|perle"]/td[1]/input
	GUI::Basic::Click Element	//*[@id='discovernow']
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	2m	1s	SUITE:Check Auto Discovery Devices None

*** Keywords ***
Suite:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Check Auto Discovery Devices
	Click Element	css=#spm_discoverylogs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	${device_clone}=	Get Table Cell	xpath=//*[@id="DiscoveryLogTable"]	15	5
	should be equal	${device_clone}	Device Cloned

SUITE:Check Auto Discovery Devices Disabled
	Click Element	css=#spm_discoverylogs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	${device_status}=	Get Table Cell	xpath=//*[@id="DiscoveryLogTable"]	3	5
	should be equal	${device_status}	Device Disabled

SUITE:Check Auto Discovery Devices None
	Click Element	css=#spm_discoverylogs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	${device_none}=	Get Table Cell	xpath=//*[@id="DiscoveryLogTable"]	2	5
	should be equal	${device_none}	None

SUITE:Teardown
	switch browser	1
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#spm_discovery > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="perle_scs_rule"]/td[1]/input
	GUI::Basic::Delete With Alert
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${Perle}
	sleep	3s
	GUI::ManagedDevices::Delete Device If Exists	Bold
	sleep	3s
	GUI::ManagedDevices::Delete Device If Exists	Cisco
	sleep	3s
	GUI::ManagedDevices::Delete Device If Exists	NSC-RSA
	sleep	3s
	GUI::ManagedDevices::Delete Device If Exists	${discoverd_device}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout and Close Nodegrid