*** Settings ***
Documentation	Tests fields validation in ManagedDevices::Auto Discovery::Hostname Detection
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test adding new match
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	id=strType	probe
	Input Text	id=strData	\\r\\r\\w
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Page Should Not Contain Error
	GUI::ManagedDevices::Wait For Hostname Detection
	GUI::Basic::Spinner Should Be Invisible
	Table Column Should Contain	id=discNameTable	3	\\r\\r\\w

Test delete new match
	GUI::Basic::Delete Rows In Table Containing Value	discNameTable	\\r\\r\\w

Test Global Settings Timeout
	GUI::Basic::Spinner Should Be Invisible
	click element	jquery=#discNameGlobalSet
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Auto Input Tests	probetmo	123456	**	str	${EMPTY}	11

Test Global Settings Retries
	GUI::Basic::Spinner Should Be Invisible
	click element	jquery=#discNameGlobalSet
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Auto Input Tests	nretries	123456	**	str	${EMPTY}	4

Reset Values
	GUI::Basic::Spinner Should Be Invisible
	click element	jquery=#discNameGlobalSet
	GUI::Basic::Spinner Should Be Invisible
	Input Text	jquery=#probetmo	5
	Input Text	jquery=#nretries	3
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

#Test Global Setting Probe Legth Error
#	click element	jquery=#discNameGlobalSet
#	GUI::Basic::Spinner Should Be Invisible
#	Input Text	//*[@id="probetmo"]	123456
#	GUI::Basic::Save
#	${T}=	Execute Javascript	var ro=document.getElementById('probetmo');if(ro==null){return false;}var er=ro.parentNode.nextSibling;if(er==null){return false;}return true;
#	Should Be True	${T}
#
#Test Global Setting Probe Invalid Error
#	Input Text	//*[@id="probetmo"]	**
#	GUI::Basic::Save
#	${T}=	Execute Javascript	var ro=document.getElementById('probetmo');if(ro==null){return false;}var er=ro.parentNode.nextSibling;if(er==null){return false;}return true;
#	Should Be True	${T}
#
#Test Global Setting Retries Legth Error
#	Input Text	//*[@id="probetmo"]	10
#	Input Text	//*[@id="nretries"]	123456
#	GUI::Basic::Save
#	${T}=	Execute Javascript	var ro=document.getElementById('nretries');if(ro==null){return false;}var er=ro.parentNode.nextSibling;if(er==null){return false;}return true;
#	Should Be True	${T}
#
#Test Global Setting Retries Invalid Error
#	Input Text	//*[@id="nretries"]	**
#	GUI::Basic::Save
#	${T}=	Execute Javascript	var ro=document.getElementById('nretries');if(ro==null){return false;}var er=ro.parentNode.nextSibling;if(er==null){return false;}return true;
#	Should Be True	${T}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Auto Discovery::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element  css=#spm_discoveryname > .title_b
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
