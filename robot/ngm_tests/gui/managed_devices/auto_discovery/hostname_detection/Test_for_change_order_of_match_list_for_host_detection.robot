*** Settings ***
Documentation	Changing priorities.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_2

*** Variables ***
${String_1}	%Htest
${String_2}	\\r\\r\\w
${String_type1}	match
${String_type2}	probe

*** Test Cases ***
Test Case to Check up/down button
	GUI::Basic::Select Rows In Table Containing Value	discNameTable	${String_1}
	Click Element	//*[@id="upButton"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//td[contains(.,'${String_1}')]
	GUI::Basic::Select Rows In Table Containing Value	discNameTable	${String_1}
	Click Element	//*[@id="downButton"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//td[contains(.,'${String_1}')]
	GUI::Basic::Select Rows In Table Containing Value	discNameTable	${String_2}
	Click Element	//*[@id="upButton"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//td[contains(.,'${String_2}')]
	GUI::Basic::Select Rows In Table Containing Value	discNameTable	${String_2}
	Click Element	//*[@id="downButton"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//td[contains(.,'${String_2}')]

Test case to delete configuration about auto discovery
	GUI::Basic::Delete Rows In Table Containing Value	discNameTable	${String_2}
	GUI::Basic::Delete Rows In Table Containing Value	discNameTable	${String_1}
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Auto Discovery::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#spm_discoveryname > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	id=strType	${String_type1}
	Input Text	id=strData	${String_1}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Table Column Should Contain	id=discNameTable	3	${String_1}
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	id=strType	 ${String_type2}
	Input Text	id=strData	 ${String_2}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Page Should Not Contain Error
	GUI::ManagedDevices::Wait For Hostname Detection
	GUI::Basic::Spinner Should Be Invisible
	Table Column Should Contain	id=discNameTable	3	${String_2}

SUITE:Teardown
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Logout and Close Nodegrid
