*** Settings ***
Documentation	Testing HOSTNAME DETECTION
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	admin
${PASSWORD}	admin
${USERNAME_ROOT}	root
${PASSWORD_ROOT}	root
${DEVICE_TYPE}	device_console
${Changing_hostname}	nodegrid-host-device
${backup_of_hostname}	restore_hostname

*** Test Cases ***
Test Case For Changing Hostname For Detection
	GUI::Basic::Open NodeGrid	${HOMEPAGEPEER}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login	${NGVERSION}	${USERNAME}	${PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${ret}=	Get value	id=hostname
	Input Text	id=hostname	${Changing_hostname}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=spm_name	${backup_of_hostname}
	Input Text	id=phys_addr	${ret}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible

Test For Enabling Hostname Detection With Mode Enabled
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=spm_name	test_host_enabled_enable
	Select From List By Label	id=type	${DEVICE_TYPE}
	Input Text	id=phys_addr	${HOSTPEER}
	Input Text	id=username	${USERNAME_ROOT}
	Input Text	id=passwordfirst	${PASSWORD_ROOT}
	Input Text	id=passwordconf	${PASSWORD_ROOT}
	Select Checkbox	id=discname
	Select From List By Value	jquery=#status	enabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open Discovery Now
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=.sorting_disabled > input
	Click Element	id=discovernow
	sleep	5
	Click Element	//*[@id="spm_discoverylogs"]
	sleep	5
	GUI::Access::Open
	sleep	5
	SUITE:Teardown

Test For Checking Function Of Disable Hostname Detection
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=spm_name	test_host_disabled_enable
	Select From List By Label	id=type	${DEVICE_TYPE}
	Input Text	id=phys_addr	${HOSTPEER}
	Input Text	id=username	${USERNAME_ROOT}
	Input Text	id=passwordfirst	${PASSWORD_ROOT}
	Input Text	id=passwordconf	${PASSWORD_ROOT}
	Select From List By Value	jquery=#status	enabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open Discovery Now
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_discoverylogs"]
	sleep	5
	GUI::Access::Open
	sleep	5
	SUITE:Teardown

Test For Enabling Hostname Detection With Mode On-Demand
	SUITE:Setup
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=spm_name	test_host_enabled_ondemand
	Select From List By Label	id=type	${DEVICE_TYPE}
	Input Text	id=phys_addr	${HOSTPEER}
	Input Text	id=username	${USERNAME_ROOT}
	Input Text	id=passwordfirst	${PASSWORD_ROOT}
	Input Text	id=passwordconf	${PASSWORD_ROOT}
	Select Checkbox	id=discname
	Select From List By Value	jquery=#status	ondemand
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open Discovery Now
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=.sorting_disabled > input
	Click Element	id=discovernow
	sleep	5
	Click Element	//*[@id="spm_discoverylogs"]
	sleep	5
	GUI::Access::Open
	sleep	5
	SUITE:Teardown

Test For Removing Changes Made In Hostname
	GUI::Basic::Open NodeGrid	${HOMEPAGEPEER}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login	${NGVERSION}	admin	admin
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${backup_of_hostname}"]/td[1]/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	${ret}=	Get value	id=phys_addr
	GUI::Network::Open Settings Tab
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=hostname	${ret}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${backup_of_hostname}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	test_host_disabled_enable
	GUI::ManagedDevices::Delete Device If Exists	${Changing_hostname}
	GUI::Basic::Logout And Close Nodegrid