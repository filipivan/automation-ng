*** Settings ***
Documentation	Testing ----> Raritan Console Server support
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW
Default Tags	EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${DEVICE_NAME}	raritan
${DEVICE_TYPE}	console_server_raritan
${DISCOVERY_RULE_NAME}	raritan_server
${DEVICE_ACTION}	Device Cloned
${serial_port_1}	Serial_Port_1-opengear1
${serial_port_2}	Serial_Port_2-opengear2

*** Test Cases ***
Test Case To Check Raritan Console Server support
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=spm_name	${DEVICE_NAME}
	Select From List By Label	id=type	${DEVICE_TYPE}
	Input Text	id=phys_addr	${RARITAN_SERVER_IP}
	Input Text	id=username	${RARITAN_SERVER_USERNAME}
	Input Text	id=passwordfirst	${RARITAN_SERVER_PASSWORD}
	Input Text	id=passwordconf	${RARITAN_SERVER_PASSWORD}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_discovery"]/span
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	Input Text	id=discovery_name	${DISCOVERY_RULE_NAME}
	Select From List By Value	id=status	enabled
	Select Radio Button	source	consoleserver
	Select From List By Label	id=operation	Clone (Mode: Enabled)
	Select From List By Value	jquery=#seed	${DEVICE_NAME}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_discoverylogs"]/span
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#discovery_now > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="cas|${DEVICE_NAME}"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//*[@id='discovernow']
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="refresh-icon"]
	Click Element	//*[@id="spm_discoverylogs"]/span
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="refresh-icon"]
	Wait Until Page Contains	${DEVICE_ACTION}
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${serial_port_1}
	Page Should Contain	${serial_port_2}

*** Keywords ***
Suite:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE_NAME}
	GUI::ManagedDevices::Delete Device If Exists	${serial_port_1}
	GUI::ManagedDevices::Delete Device If Exists	${serial_port_2}

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${serial_port_1}
	GUI::ManagedDevices::Delete Device If Exists	${serial_port_2}
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE_NAME}
	GUI::ManagedDevices::Open Discovery Rules
	GUI::ManagedDevices::Delete Discovery Rule	${DISCOVERY_RULE_NAME}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout and Close Nodegrid