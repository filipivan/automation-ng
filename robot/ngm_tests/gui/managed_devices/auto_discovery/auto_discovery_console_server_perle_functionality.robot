*** Settings ***
Documentation	Testing ----> Perle Console Server support
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${PERLE_TYPE}	console_server_perle
${PERLE_SCS}	test-perle-scs
${PERLE_SCS_RULE}	perle_scs_rule
${PERLE_SCR_RULE}	perle_scr_rule
${PERLE_SCR}	test-perle-scr

*** Test Cases ***
Test discover Perle SCS ports
	[Tags]
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Add Device	${PERLE_SCS}	${PERLE_TYPE}
	...	${PERLE_SCS_SERVER_IP}	${PERLE_SCS_SERVER_USERNAME}	no	${PERLE_SCS_SERVER_PASSWORD}	ondemand
	GUI::ManagedDevices::Add Discovery Rule	${PERLE_SCS_RULE}	${PERLE_SCS}	consoleserver
	SUITE:Discover All Devices
	SUITE:Wait For Device To Be Cloned	${PERLE_SCS_SERVER_IP}

Test discover Perle SCS hostname
	SUITE:Enable Hostname Detection	${PERLE_SCS}
	SUITE:Wait For Device Hostname To Be Discovered	${PERLE_SCS_SERVER_IP}

Test discover Perle SCR ports
	[Tags]
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Add Device	${PERLE_SCR}	${PERLE_TYPE}
	...	${PERLE_SCR_SERVER_IP}	${PERLE_SCR_SERVER_USERNAME}	no	${PERLE_SCR_SERVER_PASSWORD}	ondemand
	GUI::ManagedDevices::Add Discovery Rule	${PERLE_SCR_RULE}	${PERLE_SCR}	consoleserver
	SUITE:Discover All Devices
	SUITE:Wait For Device To Be Cloned	${PERLE_SCR_SERVER_IP}

Test discover Perle SCR hostname
	[Tags]
	SUITE:Enable Hostname Detection	${PERLE_SCR}
	SUITE:Wait For Device Hostname To Be Discovered	${PERLE_SCR_SERVER_IP}

*** Keywords ***
Suite:Setup
	CLI:Open
	${TIME_SECONDS}=	CLI:Get Current System Time In Seconds
	CLI:Close Connection
	Set Suite Variable	${TIME_SECONDS}

	GUI::Basic::Open And Login Nodegrid
	SUITE:Reset Discovery Logs
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::ManagedDevices::Delete All Devices

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete All Devices
	GUI::ManagedDevices::Open Discovery Rules
	GUI::Basic::Spinner Should Be Invisible
	Click Element		//*[@id="thead"]/tr/th[1]/input
	Click Element		//*[@id="delButton"]
	Handle Alert
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout and Close Nodegrid

SUITE:Discover All Devices
	Click Element	css=#discovery_now > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//table[@id="discovernowTable"]//input[@type="checkbox"]
	GUI::Basic::Click Element	//*[@id='discovernow']
	GUI::Basic::Spinner Should Be Invisible

SUITE:Wait For Device To Be Cloned
	[Arguments]	${DEVICE_IP}
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	Click Element	//*[@id="spm_discoverylogs"]/span
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	30x	5s	SUITE:Discovery Logs Should Contain	${DEVICE_IP}	Device Cloned

SUITE:Wait For Device Hostname To Be Discovered
	[Arguments]	${DEVICE_IP}
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	Click Element	//*[@id="spm_discoverylogs"]/span
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	60	3s	SUITE:Discovery Logs Should Contain	${DEVICE_IP}	Device Updated

SUITE:Discovery Logs Should Contain
	[Arguments]	${DEVICE_IP}	${TEXT}
	Click Element	//a[@class="icon-refresh"]
	GUI::Basic::Spinner Should Be Invisible
	${DISCOVERY_LOGS}=	GUI::Basic::Get Table Values	DiscoveryLogTable
	FOR	${DISCOVERY_LOG}	IN	@{DISCOVERY_LOGS}
		${IS_SEARCHED_IP}=	Run Keyword And Return Status	Should Be Equal	${DISCOVERY_LOG["IP Address"]}	${DEVICE_IP}
		${SUCCESSFULLY_CLONED}=	Run Keyword And Return Status	Should Be Equal	${DISCOVERY_LOG["Action"]}	${TEXT}
		Return From Keyword If	${IS_SEARCHED_IP} and ${SUCCESSFULLY_CLONED}
	END
	Fail	Perle Port was not cloned

SUITE:Reset Discovery Logs
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	Click Element	//*[@id="spm_discoverylogs"]/span
	GUI::Basic::Spinner Should Be Invisible
	Click Element		//*[@id="reset_logs"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:Enable Hostname Detection
	[Arguments]	${DEVICE_NAME}
	GUI::Basic::Managed Devices::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Enter Device	${DEVICE_NAME}
	Select Checkbox	discname
	GUI::Basic::Save