*** Settings ***
Documentation	Tests fields validation in ManagedDevices::Auto Discovery::VM Managers
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2	CRITICAL

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test NonAccess Controls
	Page Should Contain Element	//*[@id="addButton"]
	Page Should Contain Element	//*[@id="delButton"]

Test Add VM Manager Empty VM Server Error
	${Dummy_Exist}=	Run Keyword And Return Status	Page Should Contain Element	//*[@id="Dummy"]
	Run Keyword If	${Dummy_Exist}	Select Checkbox	//*[@id="Dummy"]/td[1]/input
	Run Keyword If	${Dummy_Exist}	GUI::Basic::Delete	True
	Click element	jquery=#addButton
	GUI::Basic::Spinner Should Be Invisible
	Input Text      //*[@id="username"]	**
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#errormsg	VM Server must be not empty

Test Add VM Manager Invalid VM Server Error
	Input Text      //*[@id="vmmanager"]        **
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#errormsg	Validation error.

Test Add VM Manager Invalid Username Error
	Input Text      //*[@id="vmmanager"]        Dummy
	Input Text      //*[@id="username"]     !@#$
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#errormsg	This field should contain only numbers, letters, and the characters @ . \\ /.

Test Add VM Manager Mismatch Password Error
	Input Text      //*[@id="username"]     Dummy
	Input Text      //*[@id="fpasswd"]      Dummy123
	Input Text      //*[@id="cpasswd"]      Dummy456
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#errormsg	Password mismatch.

Test Add VM Manager Invalid Port Error
	Input Text      //*[@id="mkswebport"]       Dummy
	Input Text      //*[@id="fpasswd"]      Dummy123
	Input Text      //*[@id="cpasswd"]      Dummy123
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#errormsg	Validation error.

Add VM Manager
	Input Text      //*[@id="mkswebport"]       123
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

Drilldown VM Manager
	Click Element	xpath=//a[contains(text(),'Dummy')]
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	jquery=#vmdiscovery
	GUI::Basic::Spinner Should Be Invisible
	gui::auditing::auto input tests	interval	str	-1	12.3	1000

Test Delete VM
    GUI::ManagedDevices::Open VM Manager
	GUI::Basic::Spinner Should Be Invisible
	Click Element       //*[@id="Dummy"]/td[1]
	Click element       //*[@id="delButton"]
	Handle Alert
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open VM Manager
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
