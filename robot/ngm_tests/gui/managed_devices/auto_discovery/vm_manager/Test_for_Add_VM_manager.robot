*** Settings ***
Documentation	Test for Add VM Manager
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

*** Variables ***
${Name}	Centos-Guacamole
${Type}	virtual_console_vmware
${Ip_address}	192.168.2.234
${Vm_Manager_ip}	${VMMANAGER}[ip]
${Vm_Manager_username}	${VMMANAGER}[username]
${Vm_Manager_password}	${VMMANAGER}[password]
${Confm_passwd}	.VMware!123
${FTP_VMRC10}	${FTPSERVER2_URL}${FTPSERVER2_VMRC10_PATH}
${FTP_USERNAME}	${FTPSERVER2_USER}
${FTP_PASSWORD}	${FTPSERVER2_PASSWORD}


*** Test Cases ***
Test Case to Add VM Manager
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_discoverylogs"]/span
	sleep	5s
	Click Element	//*[@id="reset_logs"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="vm_managers"]/span
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="vmmanager"]	${Vm_Manager_ip}
	Input Text	//*[@id="username"]	${Vm_Manager_username}
	Input Text	//*[@id="fpasswd"]	${Vm_Manager_password}
	Input Text	//*[@id="cpasswd"]	${Confm_passwd}
	Click Element	id=vmmanager
	Select From List By Label	//*[@id="type"]	VMware
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Test Case to Add Managed Device
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Setup
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${Name}
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text	//*[@id='spm_name']	${Name}
	Select From List By Label	//*[@id="type"]	${Type}
	Click Element	//*[@id="vmmanager"]
	select from list by value	//*[@id="vmmanager"]	${Vm_Manager_ip}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Table Should Has Row With Id	SPMTable	${Name}
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${Name}

Test Case Add VMRC Package
	SUITE:Setup
	GUI::ManagedDevices::Open VM Manager
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="vmrc_bundle"]
	GUI::Basic::Spinner Should Be Invisible
	Select Radio Button	vmrctypeselection	vmrc_bundle_remote
	Input Text	//*[@id="url"]	${FTP_VMRC10}
	Input Text	//*[@id="userName"]	${FTP_USERNAME}
	Input Text	//*[@id="password"]	${FTP_PASSWORD}
	Select Checkbox	//*[@id="absolutepath"]
	Click Element	jquery=#saveButton
	Wait until Element Is Not Visible	xpath=//*[@id='loading']	timeout=300s

Test Case to Check MKS button
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Setup
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="|Centos-Guacamole"]/td[2]/div/a[1]
	Sleep	15s
	Switch Window	${Name}
	Sleep	10s
	Close All Browsers

Test Case to Delete Configuration
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Setup
	GUI::ManagedDevices::Open VM Manager
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="del_vmrc_bundle"]
	Click Element	//*[@id="del_vmrc_bundle"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="Centos-Guacamole"]/td[1]/input
	Click Element	//*[@id="delButton"]
	Handle Alert
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="vm_managers"]/span
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="${VMMANAGER}[ip]"]/td[1]/input
	Click Element	//*[@id="delButton"]
	Handle Alert
	GUI::Basic::Spinner Should Be Invisible


*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers