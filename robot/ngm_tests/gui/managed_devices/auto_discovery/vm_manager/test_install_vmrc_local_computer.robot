*** Settings ***
Documentation	Tests installing vmrc using local computer in ManagedDevices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${Timeout}

*** Test Cases ***
Test VMRC Not Intalled
	${orig_timeout}=	Set Selenium Timeout	180 seconds
	Set Global Variable	${Timeout}	${orig_timeout}
	GUI::ManagedDevices::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	//*[@id="type"]	virtual_console_vmware
	${Not_installed}=	Run Keyword And Return Status	Page Should Contain Element	//*[@id="typeNotice"]
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	${Not_installed}==False	SUITE:Uninstall VMRC

Test Install VMRC Local Computer
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::ManagedDevices::Open VM Manager
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="vmrc_bundle"]
	Click Element	//*[@id="vmrc_bundle"]
	GUI::Basic::Spinner Should Be Invisible
	Select Radio Button	vmrctypeselection	vmrc_bundle_localcomputer
	Choose File	//*[@id="clientFilename"]	C:\\VMRC\\VMware-Remote-Console.bundle
	Click Element	jquery=#saveButton
	Wait until Element Is Not Visible	xpath=//*[@id='loading']	timeout=300s

Test Confirm Install Local Computer
	[Tags]	NON-CRITICAL	NEED-REVIEW
	Page Should Contain Element	//*[@id="del_vmrc_bundle"]
	GUI::ManagedDevices::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	//*[@id="type"]	virtual_console_vmware
	Page Should Not Contain Element	//*[@id="typeNotice"]
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible

Test Uninstall VMRC Local Computer
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Uninstall VMRC

Test Confirm Uninstall Local Computer
	[Tags]	NON-CRITICAL	NEED-REVIEW
	Set Selenium Timeout	${Timeout}
	Page Should Contain Element	//*[@id="vmrc_bundle"]
	GUI::ManagedDevices::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	//*[@id="type"]	virtual_console_vmware
	Page Should Contain Element	//*[@id="typeNotice"]
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

SUITE:Uninstall VMRC
	GUI::ManagedDevices::Open VM Manager
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="del_vmrc_bundle"]
	Click Element	//*[@id="del_vmrc_bundle"]
	GUI::Basic::Spinner Should Be Invisible