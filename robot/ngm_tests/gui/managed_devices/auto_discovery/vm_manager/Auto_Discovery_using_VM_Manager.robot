*** Settings ***
Documentation	Automated test for Auto Discovery using VM Manager
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	NON-CRITICAL	NEED-REVIEW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${DEVICE_NAME}	esxi
${DISCOVERY_RULE_NAME}	ESXI_server
${DEVICE_TYPE}	virtual_console_vmware

*** Test Cases ***
Test case to add VMmanager
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_discoverylogs"]/span
	sleep	5s
	Click Element	//*[@id="reset_logs"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Not Contain	${VM1}
	Page Should Not Contain	${VM2}
	Page Should Not Contain	${VM3}
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="vm_managers"]/span
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="username"]	${ESXI_SERVER_USERNAME}
	Input Text	//*[@id="fpasswd"]	${ESXI_SERVER_PASSWORD}
	Input Text	//*[@id="cpasswd"]	${ESXI_SERVER_PASSWORD}
	Click Element	id=vmmanager
	Input Text	name=vmmanager	${DEVICE_NAME}
	Select From List By Label	//*[@id="type"]	VMware
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="vm_managers"]/span
	GUI::Basic::Spinner Should Be Invisible
	Click Link	${DEVICE_NAME}
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="vmdiscovery"]
	Click Element	//*[@id="vmDatacenter"]/div[2]/div[1]/select/option
	Click Button	//*[@id="vmDatacenter"]/div[2]/div[3]/button[1]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Test case to add managed device/Rule/and Auto discovery
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=spm_name	${DEVICE_NAME}
	Input Text	//*[@id="phys_addr"]	${ESXI_SERVER_IP}
	Select From List By Label	id=type	${DEVICE_TYPE}
	Select From List by Value	//*[@id="vmmanager"]	${DEVICE_NAME}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_discovery"]/span
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=discovery_name	${DISCOVERY_RULE_NAME}
	Select From List By Value	id=status	enabled
	Select Radio Button	source	vmmanager
	Select From List By Label	id=operation	Clone (Mode: Enabled)
	Select From List By Value	jquery=#seed	${DEVICE_NAME}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="discovery_now"]/span
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="vm|esxi"]/td[1]/input
	Click Button	//*[@id="discovernow"]
	GUI::Basic::Spinner Should Be Invisible
	sleep	20s
	Click Element	//*[@id="spm_discoverylogs"]/span
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	Page Should Contain	${VM1}
	Page Should Contain	${VM2}
	Page Should Contain	${VM3}
	Page Should Contain	${VM4}
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${DEVICE_NAME}
	${BROWSER}	Convert To Lower Case	${BROWSER}
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nOpening Browser: ${BROWSER}	INFO	console=yes
	Run Keyword If	'${BROWSER}' == 'chrome' or '${BROWSER}' == 'headlesschrome'	Wait Until Keyword Succeeds	3x	3s	Open Browser	${ESXI_SERVER_HOMEPAGE}	${BROWSER}	options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors")
	Run Keyword If	'${BROWSER}' == 'firefox' or '${BROWSER}' == 'headlessfirefox'	Wait Until Keyword Succeeds	3x	3s	Open Browser	${ESXI_SERVER_HOMEPAGE}	${BROWSER}	options=set_preference("moz:dom.disable_beforeunload", "true")
	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\nBrowser ${BROWSER} opened	INFO	console=yes
	Maximize Browser Window
	Sleep	60s
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=username	${ESXI_SERVER_USERNAME}
	Input Text	id=password	${ESXI_SERVER_PASSWORD}
	Click Element	id=submit
	sleep	120s
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//div[2]/a/span[2]
	sleep	20s
	Page Should Contain	${VM1}
	Page Should Contain	${VM2}
	Page Should Contain	${VM3}
	Close Browser
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="vm_managers"]/span
	GUI::ManagedDevices::Delete All VM Managers
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
Suite:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE_NAME}

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${VM1}
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${VM2}
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${VM3}
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${VM4}
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE_NAME}
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open Discovery Rules
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Discovery Rule	${DISCOVERY_RULE_NAME}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout and Close Nodegrid

