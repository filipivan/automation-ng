*** Settings ***
Documentation	Tests vmrc Elements in ManagedDevices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2	CRITICAL

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***

Test Install Button Appears
	GUI::ManagedDevices::Open VM Manager
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="vmrc_bundle"]

Test Instal Button Page
	Click Element	//*[@id="vmrc_bundle"]
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Enabled	//*[@id="cancelButton"]
	Element Should Be Disabled	//*[@id="saveButton"]
	Page Should Contain Element	//*[@id="vmrc_bundleForm"]/div[2]/div/div/div/label
	Page Should Contain Element	//*[@id="vmrc_bundleForm"]/div[2]/div/div/div/div

Test Radio Button Elements
	Select Radio Button	vmrctypeselection	vmrc_bundle_local
	Page Should Contain	Filename:
	Page Should Contain Element	//*[@id="localFilename"]
	Page Should Contain Element	//*[@id="notice"]

	Select Radio Button	vmrctypeselection	vmrc_bundle_localcomputer
	Page Should Contain	Filename
	Page Should Contain Element	//*[@id="clientFilename"]

	Select Radio Button	vmrctypeselection	vmrc_bundle_remote
	FOR	${I}	IN RANGE	6	10
		Page Should Contain Element	//*[@id="vmrc_bundleForm"]/div[2]/div/div/div/div/div[${I}]
	END

*** Keywords ***

SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
