*** Settings ***
Documentation	Adding Network Scan to ManagedDevices::Auto Discovery::Network Scan
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

*** Test Cases ***
Add Dummy Device
	GUI::ManagedDevices::Open
	GUI::Basic::Spinner Should Be Invisible
	${count}=	GUI::Basic::Count Table Rows	SPMTable
	Run Keyword If	${count} > 0	Log	"Device already exists"
	Run Keyword If	${count} == 0	Add Missing Device

Add Network Scan
#	Execute Javascript	window.scrollTo(0,document.body.scrollHeight);
#	GUI::Basic::Wait Until Element Is Accessible	jquery=body > div.main_menu > div > ul > li:nth-child(4)
#	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(4)
#	Wait Until Element Is Visible	jquery=#addButton
#	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open Network Scan
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Network Scan	dummy_network
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Add Dummy Network Scan	dummy_network	127.0.0.1	127.0.0.5

Edit Network Scan
	GUI::ManagedDevices::Enter Network Scan	dummy_network
	Input Text	jquery=#ipfirst	127.0.0.6
	Input Text	jquery=#iplast	127.0.0.10
	Input Text	jquery=#portlist	28-30,625
	Input Text	jquery=#interval	10
	GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Enter Network Scan	dummy_network
	GUI::Basic::Wait Until Element Is Accessible	jquery=#interval
	${IPFIRST}=	Get Value	jquery=#ipfirst
	${IPLAST}=	Get Value	jquery=#iplast
	${PORTLIST}=	Get Value	jquery=#portlist
	${INTERVAL}=	Get Value	jquery=#interval
	Should Be Equal	${IPFIRST}	127.0.0.6
	Should Be Equal	${IPLAST}	127.0.0.10
	Should Be Equal	${PORTLIST}	28-30,625
	Should Be Equal	${INTERVAL}	10

Delete Network Scan
	GUI::ManagedDevices::Delete Network Scan	dummy_network
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Add Dummy Network Scan	dummy_network	127.0.0.1	127.0.0.5
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Network Scan	dummy_network
	GUI::Basic::Spinner Should Be Invisible

Delete Network Scans
	GUI::ManagedDevices::Add Dummy Network Scan	dummy_network	127.0.0.1	127.0.0.5
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Add Dummy Network Scan	dummy_network2	127.0.0.6	127.0.0.10
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Network Scan	dummy_network	dummy_network2
	GUI::Basic::Spinner Should Be Invisible
	Page Should Not Contain Element	id=dummy_network
	Page Should Not Contain Element	id=dummy_network2

*** Keywords ***

SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::ManagedDevices::Delete Device If Exists	aaaa
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

Add Missing Device
	Wait Until Element Is Visible	jquery=#addButton
	Click Element	jquery=#addButton
	Wait Until Element Is Visible	jquery=#cancelButton
	Input Text	xpath=//input[@id="spm_name"]	aaaa
	Wait Until Element Is Visible	jquery=#saveButton
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible