*** Settings ***
Documentation	Adding Network Scan to ManagedDevices::Auto Discovery::Network Scan
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${DEVICE_NAME}	Test_scan_device
${DISCOVERY_RULE_NAME}	Test_scan_device
${DEVICE_TYPE}	idrac6
${IP_Range_Start}	192.168.2.225
${IP_Range_End}	192.168.2.234
${SCAN_ID}	1
${device_name1}	192.168.2.229
${device_name2}	192.168.2.227
${device_name3}	192.168.2.228
${device_name4}	192.168.2.232
${device_name5}	192.168.2.233
${device_name6}	192.168.2.230
${device_name7}	192.168.2.231

*** Test Cases ***
Test case to Add Dummy Device of type idrac for discovery
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=spm_name	${DEVICE_NAME}
	Select From List By Label	id=type	${DEVICE_TYPE}
	Input Text	//*[@id="phys_addr"]	${IDRAC9_IP}
	Input Text	//*[@id="username"]	${IDRAC9_USERNAME}
	Input Text	//*[@id="passwordfirst"]	${IDRAC9_PASSWORD}
	Input Text	//*[@id="passwordconf"]	${IDRAC9_PASSWORD}
	GUI::Basic::Save

Test case to validate Scan Interval field
	SUITE:Setup
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_netdiscover"]/span
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	Input Text	//*[@id="netdiscover_id"]	1
	Input Text	//*[@id="ipfirst"]	${IP_Range_Start}
	Input Text	//*[@id="iplast"]	${IP_Range_End}
	Unselect Checkbox	//*[@id="morelikethis"]
	Unselect Checkbox	//*[@id="portscan"]
	Input Text	//*[@id="interval"]	-1
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Error on page. Please check.
	SUITE:Setup
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_netdiscover"]/span
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	Input Text	//*[@id="netdiscover_id"]	${SCAN_ID}
	Input Text	//*[@id="ipfirst"]	${IP_Range_Start}
	Input Text	//*[@id="iplast"]	${IP_Range_End}
	Unselect Checkbox	//*[@id="morelikethis"]
	Unselect Checkbox	//*[@id="portscan"]
	Input Text	//*[@id="interval"]	0
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Error on page. Please check.

Test case to Add Network scan
	SUITE:Setup
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_netdiscover"]/span
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	Input Text	//*[@id="netdiscover_id"]	1
	Input Text	//*[@id="ipfirst"]	${IP_Range_Start}
	Input Text	//*[@id="iplast"]	${IP_Range_End}
	Unselect Checkbox	//*[@id="morelikethis"]
	Unselect Checkbox	//*[@id="portscan"]
	Input Text	//*[@id="interval"]	50
	GUI::Basic::Save
	SUITE:Setup
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_netdiscover"]/span
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${SCAN_ID}

Test case Create discovery rule and auto discovery using network scan
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_discovery"]/span
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=discovery_name	${DISCOVERY_RULE_NAME}
	Select From List By Value	id=status	enabled
	Click Element	xpath=(//input[@id='source'])[8]
	Select From List By Label	id=operation	Clone (Mode: Enabled)
	Select From List By Value	jquery=#seed	${DEVICE_NAME}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="discovery_now"]/span
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="net|${SCAN_ID}"]/td[1]/input
	Click Button	//*[@id="discovernow"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	2m	1s	SUITE:Check Auto Discovery Devices
	GUI:Access::Open Table tab
	Page Should Contain	${DEVICE_NAME}
	SUITE:reset logs

Test case for auto-discovery with port scan enabled
	SUITE:Setup
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_netdiscover"]/span
	GUI::Basic::Spinner Should Be Invisible
	Click Link	//*[@id="${SCAN_ID}"]
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="portscan"]
	GUI::Basic::Save
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="discovery_now"]/span
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="net|${SCAN_ID}"]/td[1]/input
	Click Button	//*[@id="discovernow"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	2m	1s	SUITE:Check Auto Discovery Devices with port scan

Delete configuration for network scan
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_netdiscover"]/span
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="thead"]/tr/th[1]/input
	Click Element	//*[@id="delButton"]
	Handle Alert	timeout=10 s
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${IP_Range_Start}
	GUI::ManagedDevices::Delete Device If Exists	${device_name1}
	GUI::ManagedDevices::Delete Device If Exists	${device_name2}
	GUI::ManagedDevices::Delete Device If Exists	${device_name4}
	GUI::ManagedDevices::Delete Device If Exists	${device_name5}
	GUI::ManagedDevices::Delete Device If Exists	${device_name6}
	GUI::ManagedDevices::Delete Device If Exists	${device_name7}
	GUI::ManagedDevices::Delete Device If Exists	${IP_Range_End}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::ManagedDevices::Delete Device If Exists	Test_scan_device
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	Click Element	//*[@id="spm_discovery"]/span
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="Test_scan_device"]/td[1]/input
	Click Button	//*[@id="delButton"]
	Handle Alert	timeout=10 s
	GUI::Basic::Spinner Should Be Invisible
	SUITE:reset logs
	GUI::Basic::Logout And Close Nodegrid

SUITE:Check Auto Discovery Devices
	Click Element	css=#spm_discoverylogs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	${discovery_method}=	Get Table Cell	xpath=//*[@id="DiscoveryLogTable"]	2	4
	should be equal	${discovery_method}		Network Scan

SUITE:reset logs
	SUITE:Setup
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_discoverylogs"]/span
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="reset_logs"]

SUITE:Check Auto Discovery Devices with port scan
	Click Element	css=#spm_discoverylogs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${device_name1}
	Page Should Contain	${IP_Range_Start}
	Page Should Contain	${device_name2}
	Page Should Contain	${device_name3}


