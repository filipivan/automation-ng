*** Settings ***
Documentation	Tests fields validation in ManagedDevices::Auto Discovery::Network Scan
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Page should not allow empty network scan id
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#netdiscover_id	jquery=#ipfirst	jquery=#iplast
	Input Text	id=ipfirst	x
	GUI::Basic::Wait Until Element Is Accessible	jquery=#saveButton
	GUI::Basic::Save If Configuration Changed
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Error Count Should Be	1
	Input Text	            jquery=#netdiscover_id	!@#$

Test Add Tab Elements
	Select Checkbox	//*[@id="enabled"]
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#morelikethis	jquery=#portscan	jquery=#ping
	Select Checkbox	//*[@id="morelikethis"]
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#seed
	Select Checkbox	//*[@id="portscan"]
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#portlist	jquery=#noticeportlist

Verifies if IP Range Start field is validated
	Input Text	                    jquery=#ipfirst	    asd
	GUI::Basic::Save If Configuration Changed
	Wait Until Element Is Visible	jquery=#globalerr
	GUI::Basic::Auto Input Tests	ipfirst	            string  !@#$%  345.345.345.345  2c::2c::2c  127.0.0.5
	GUI::Basic::Wait Until Element Is Accessible	    jquery=#saveButton
	GUI::Basic::Save If Configuration Changed

Verifies if IP Range End field is validated
	Input Text	                    jquery=#iplast	    asd
	GUI::Basic::Save If Configuration Changed
	Wait Until Element Is Visible	jquery=#globalerr
	GUI::Basic::Auto Input Tests	iplast	            string  !@#$%  345.345.345.345  2c::2c::2c  127.0.0.10
	GUI::Basic::Wait Until Element Is Accessible	    jquery=#saveButton
	GUI::Basic::Save If Configuration Changed
	Capture Page Screenshot
	
Verifies if Port List field is validated
    # [Tags]	NON-CRITICAL  BUG_NG_11155
	Input Text                        //input[@id='netdiscover_id']        test-scan-id
	Input Text	                    jquery=#portlist	        asd
	Click Element	                jquery=#saveButton
	Wait Until Element Is Visible	jquery=#globalerr
	Capture Page Screenshot
	Input Text	//*[@id="ipfirst"]	192.168.2.150
	Input Text	//*[@id="iplast"]	192.168.2.155
	Input Text	                    jquery=#portlist	        29
	GUI::Basic::Auto Input Tests    portlist	         string  !@#$%  1.1.1.1  65536  29
	Capture Page Screenshot
	Select Checkbox		//*[@id="test-scan-id"]/td[1]/input
	Click Button	//*[@id="delButton"]
	Handle Alert	timeout=10 s
	GUI::Basic::Spinner Should Be Invisible

Verifies if Interval field is validated
    # [Tags]	NON-CRITICAL  BUG_NG_11155
    GUI::ManagedDevices::Open Network Scan
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Wait Until Element Is Accessible  jquery=#portlist
	Input Text                        //input[@id='netdiscover_id']        test-scan-id
	Input Text	                    jquery=#portlist	        asd
	Capture Page Screenshot
	GUI::Basic::Save If Configuration Changed
	Wait Until Element Is Visible	jquery=#globalerr
	Input Text	//*[@id="ipfirst"]	192.168.2.150
	Input Text	//*[@id="iplast"]	192.168.2.155
	Input Text	                    jquery=#portlist	        29
	#GUI::Basic::Auto Input Global Error Tests	//*[@id="interval"]	Error on page. Please check.	string  !@#$%  ${EMPTY}  12345678901
	Input Text	//*[@id="interval"]	string
	GUI::Basic::Save
	Page Should Contain	Error on page. Please check.
	Input Text	//*[@id="interval"]	${EMPTY}
	Page Should Contain	Error on page. Please check.
	Input Text	//*[@id="interval"]	!@#$%
	GUI::Basic::Save
	Page Should Contain	Error on page. Please check.
	Input Text	//*[@id="interval"]	123456789012
	GUI::Basic::Save
	Page Should Contain	Error on page. Please check.
	GUI::Basic::Wait Until Element Is Accessible	            jquery=#saveButton
	GUI::Basic::Save If Configuration Changed

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open Network Scan
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
