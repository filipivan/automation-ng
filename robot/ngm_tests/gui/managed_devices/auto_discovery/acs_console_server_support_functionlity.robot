*** Settings ***
Documentation	Automated test case for console server port
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE_NAME}	acs
${DEVICE_TYPE}	console_server_acs6000
${DISCOVERY_RULE_NAME}	acs_server
${serial_port_1}	28-3a-46-p-1
${serial_port_2}	28-3a-46-p-2
${DEVICE_ACTION}	Device Cloned

*** Test Cases ***
Test case to add auto Discovery rule
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=spm_name	${DEVICE_NAME}
	Select From List By Label	id=type	${DEVICE_TYPE}
	Input Text	id=phys_addr	${ACS_IP}
	Input Text	id=username		${ACS_USER}
	Input Text	id=passwordfirst	${ACS_PASSWORD}
	Input Text	id=passwordconf	${ACS_PASSWORD}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_discovery"]/span
	GUI::ManagedDevices::Open Discovery Rules
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="discovery_name"]	${DEVICE_NAME}
	Click Element	css=.radio:nth-child(12) #source
	Select From List By Label	//*[@id="operation"]	Clone (Mode: Enabled)
	Select From List By Label	//*[@id="seed"]	${DEVICE_NAME}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Test Discovery Logs Elements
	[Tags]	NON-CRITICAL	NEED-REVIEW
	Click Element	//*[@id="spm_discoverylogs"]/span
	sleep	5s
	Click Element	//*[@id="reset_logs"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#discovery_now > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="cas|${DEVICE_NAME}"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//*[@id='discovernow']
	GUI::Basic::Spinner Should Be Invisible
	sleep	10s
	Click Element	//*[@id="spm_discoverylogs"]/span
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="refresh-icon"]
	Wait Until Page Contains	${DEVICE_ACTION}
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${serial_port_1}
	Page Should Contain	${serial_port_2}

Test Case to Delete Discovery rule
	GUI::ManagedDevices::Open Discovery Rules
	Page Should Contain	${DEVICE_NAME}
	Select Checkbox	//*[@id="${DEVICE_NAME}"]/td[1]/input
	GUI::Basic::Delete With Alert
	GUI::Basic::Spinner Should Be Invisible
	Page should not Contain	${DEVICE_NAME}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::ManagedDevices::Delete All Devices
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE_NAME}

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete All Devices
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE_NAME}
	GUI::ManagedDevices::Delete Device If Exists	${serial_port_1}
	GUI::ManagedDevices::Delete Device If Exists	${serial_port_2}
	GUI::Basic::Logout and Close Nodegrid
