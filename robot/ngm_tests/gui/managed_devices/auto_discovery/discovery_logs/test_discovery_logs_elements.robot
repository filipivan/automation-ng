*** Settings ***
Documentation	ManagedDevices::Auto Discovery::Discovery Logs
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***

Test Discovery Logs Elements
	Click Element	//*[@id="spm_discoverylogs"]
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Enabled	//*[@id="reset_logs"]
	Page Should Contain Element	//*[@id="DiscoveryLogTable"]

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Auto Discovery::Open Tab
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
