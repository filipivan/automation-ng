*** Settings ***
Documentation	Access Table Tablular View
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2		GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	MAC	CHROME
Default Tags	EXCLUDEIN3_2
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NAME}     Name
${ACTION}   Action
${TYPE}     Type
${MODE}     Mode
${IP_ADD}     IP Address
${NODE_HOST}     Nodegrid Host
${GROUPS}     Groups
${SERIAL_PORT}     Serial Port
${KVM_PORT}     KVM Port
${END_POINT}     End Point
${PORT_ALIAS}     Port Alias
${IP_ALIAS}     IP Alias
${SEC_IP_ALIAS}     Second IP Alias

*** Test Cases ***
Check for the headers by selecting views
	Page Should Contain Element  xpath=//th[contains(.,'Name')]
    Page Should Contain Element  xpath=//th[contains(.,'Action')]
    GUI::Basic::Managed Devices::Preferences::open tab
    Click Element  css=#spm_views_pref > .title_b
    GUI::Basic::Spinner Should Be Invisible
    Click Element   //div[@id="cols"]//option[@value="${TYPE}"]
    Click Element   //div[@id="cols"]//option[@value="${MODE}"]
    Click Element   //div[@id="cols"]//option[@value="${IP_ADD}"]
    Click Element   //div[@id="cols"]//option[@value="${NODE_HOST}"]
    Click Element   //div[@id="cols"]//option[@value="${GROUPS}"]
    Click Element   //div[@id="cols"]//option[@value="${SERIAL_PORT}"]
    Click Element   //div[@id="cols"]//option[@value="${KVM_PORT}"]
    Click Element   //div[@id="cols"]//option[@value="${END_POINT}"]
    Click Element   //div[@id="cols"]//option[@value="${PORT_ALIAS}"]
    Click Element   //div[@id="cols"]//option[@value="${IP_ALIAS}"]
    Click Element   //div[@id="cols"]//option[@value="${SEC_IP_ALIAS}"]
    Click Element		xpath=//button[contains(.,'Add ►')]
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
    Page Should Contain     ${TYPE}
    Page Should Contain     ${MODE}
    Page Should Contain     ${IP_ADD}
    Page Should Contain     ${NODE_HOST}
    Page Should Contain     ${GROUPS}
    Page Should Contain     ${SERIAL_PORT}
    Page Should Contain     ${KVM_PORT}
    Page Should Contain     ${END_POINT}
    Page Should Contain     ${PORT_ALIAS}
    Page Should Contain     ${IP_ALIAS}
    Page Should Contain     ${SEC_IP_ALIAS}
    Page Should Contain Element  xpath=//th[contains(.,'Name')]
    Page Should Contain Element  xpath=//th[contains(.,'Type')]
    Page Should Contain Element  xpath=//th[contains(.,'Mode')]
    Page Should Contain Element  xpath=//th[contains(.,'IP Address')]
    Page Should Contain Element  xpath=//th[contains(.,'Nodegrid Host')]
    Page Should Contain Element  xpath=//th[contains(.,'Groups')]
    Page Should Contain Element  xpath=//th[contains(.,'Serial Port')]
    Page Should Contain Element  xpath=//th[contains(.,'KVM Port')]
    Page Should Contain Element  xpath=//th[contains(.,'End Point')]
    Page Should Contain Element  xpath=//th[contains(.,'Port Alias')]
    Page Should Contain Element  xpath=//th[contains(.,'IP Alias')]
    Page Should Contain Element  xpath=//th[contains(.,'Second IP Alias')]
    Page Should Contain Element  xpath=//th[contains(.,'Action')]

*** Keywords ***
Suite:Setup
    GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Managed Devices::Preferences::open tab
    Click Element  css=#spm_views_pref > .title_b
    GUI::Basic::Spinner Should Be Invisible
    Click Element   //div[@id="cols"]//option[@value="${TYPE}"]
    Click Element   //div[@id="cols"]//option[@value="${MODE}"]
    Click Element   //div[@id="cols"]//option[@value="${IP_ADD}"]
    Click Element   //div[@id="cols"]//option[@value="${NODE_HOST}"]
    Click Element   //div[@id="cols"]//option[@value="${GROUPS}"]
    Click Element   //div[@id="cols"]//option[@value="${SERIAL_PORT}"]
    Click Element   //div[@id="cols"]//option[@value="${KVM_PORT}"]
    Click Element   //div[@id="cols"]//option[@value="${END_POINT}"]
    Click Element   //div[@id="cols"]//option[@value="${PORT_ALIAS}"]
    Click Element   //div[@id="cols"]//option[@value="${IP_ALIAS}"]
    Click Element   //div[@id="cols"]//option[@value="${SEC_IP_ALIAS}"]
    Click Element   //*[@id="cols"]/div/div[1]/div[2]/div/div[2]/button
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain Element  xpath=//th[contains(.,'Name')]
    Page Should Contain Element  xpath=//th[contains(.,'Action')]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Logout and Close Nodegrid