*** Settings ***
Documentation	Verifies field name visibility on drilldown and edit mode
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}

*** Test Cases ***

Verify fields Data Logging and Event Logging visibility on drac in drilldown mode
	GUI::ManagedDevices::Delete Devices	${DUMMY_DEVICE_CONSOLE_NAME}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text	    //*[@id='spm_name']	    ${DUMMY_DEVICE_CONSOLE_NAME}
	Select From List By Value	jquery=#type	        drac
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep       5s
	GUI::ManagedDevices::Wait For Managed Devices Table
	GUI::Basic::Spinner Should Be Invisible
	wait until page contains element        ${DUMMY_DEVICE_CONSOLE_NAME}
	Table Should Contain	    jquery=#SPMTable	${DUMMY_DEVICE_CONSOLE_NAME}
	GUI::ManagedDevices::Enter Device	            ${DUMMY_DEVICE_CONSOLE_NAME}
	Page Should Contain Element	input[@id='databuf']	limit=0
	Page Should Contain Element	input[@id='eventlog']	limit=0

Verify fields Data Logging and Event Logging visibility on drac in edit mode
	GUI::Basic::Managed Devices::Devices::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    wait until page contains element        //*/tr[@id='${DUMMY_DEVICE_CONSOLE_NAME}']/*/input
	GUI::Basic::Select Checkbox	//*/tr[@id='${DUMMY_DEVICE_CONSOLE_NAME}']/*/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Wait Until Elements Are Visible	jquery=#databuf	jquery=#eventlog

Delete dummy_device_console
	GUI::ManagedDevices::Delete Devices	${DUMMY_DEVICE_CONSOLE_NAME}

Verify fields Data Logging and Event Logging visibility on local_serial in drilldown mode
	${ret}=	GUI::Basic::Is Serial Console	${NGVERSION}
	Run Keyword If	'${ret}' == '1'
		...	Run Keywords
		...	GUI::Basic::Click Element	//*/a[@id='ttyS1']
		...	AND	GUI::Basic::Spinner Should Be Invisible
		...	AND	Page Should Contain Element	input[@id='databuf']	limit=0
		...	AND	Page Should Contain Element	input[@id='eventlog']	limit=0
		...	ELSE Log To Console	Disabled in NodeGrid Manager

Verify fields Data Logging and Event Logging visibility on local_serial in edit mode
	${ret}=	Gui::Basic::Is Serial Console	${NGVERSION}
	Run Keyword If	'${ret}' == '1'
		...	Run Keywords
		...	GUI::Basic::Return
		...	AND	GUI::Basic::Spinner Should Be Invisible
		...	AND	GUI::Basic::Wait Until Element Is Accessible	jquery=a#ttyS1
		...	AND	GUI::Basic::Select Checkbox	//*/tr[@id='ttyS1']/*/input
		...	AND	GUI::Basic::Edit
		...	AND	Wait Until Element Is Visible	jquery=#type
		...	AND	GUI::Basic::Spinner Should Be Invisible
		...	AND	GUI::Basic::Wait Until Elements Are Visible	jquery=#databuf
		...	AND	Page Should Contain Element	input[@id='eventlog']	limit=0
		...	ELSE Log To Console	Disabled in NodeGrid Manager

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	Close All Browsers
