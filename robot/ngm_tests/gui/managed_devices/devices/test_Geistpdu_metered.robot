*** Settings ***
Documentation	Test pdu operations for metered/non-switched Geist pdu
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	DEPENDENCE_PDU	MISSING_DEVICE

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${GEIST_METERED_PDU}	test_metered_Geist
${GEIST_METERED_TYPE}	pdu_geist
${GEIST_METERED_USER}	GeistDemo
${GEIST_METERED_IP}	76.79.48.119
${GEIST_METERED_PASSWORD}	powerup
${GEIST_ID}	1
${GEIST_OUTLET_ID}	2
${GEIST_OUTLET1_NAME}	Outlet_1
${OUTLET2}	Outlet_2
${OUTLET3}	Outlet_3
${TEST_DEVICE}	mergeoutlet_meteredgeist
${TEST_DEVICE_TYPE}	device_console
${COMMAND}	Outlet

*** Test Cases ***
Test Case To Add Geist pdu/protocol/SNMPV2
	[Tags]	NON-CRITICAL	NEED-REVIEW	MISSING_DEVICE
	#we need to have the Geist pdu metered on Automation side
	SUITE:Add Geist metered pdu Managed device
	SUITE:Add SNMPV2 community

Test case to check outlets discovery
	[Tags]	NON-CRITICAL	NEED-REVIEW	MISSING_DEVICE
	Auto discovery for device
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	${GEIST_METERED_PDU}
	Click Link	${GEIST_METERED_PDU}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spmoutlets_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	${GEIST_OUTLET1_NAME}
	Page Should Contain	${OUTLET2}
	Page Should Contain	${OUTLET3}

Test case to do pdu operations on Access table page
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If 	'${NGVERSION}' <= '5.6'	Click Element	//*[@id="|${GEIST_METERED_PDU}"]/td[1]/div/a/span
	...	ELSE	click element		//*[@id="|${GEIST_METERED_PDU}|"]/td[1]/div/a/span
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	css=.btn:nth-child(4)
	Click Button	css=.btn:nth-child(4)
	GUI::Basic::Spinner Should Be Invisible
	Page Should not contain	unknown

Test case to add managed for merging outlet
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=spm_name	${TEST_DEVICE}
	Select From List By Label	id=type	${TEST_DEVICE_TYPE}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Test case to merge outlet
	[Tags]	NON-CRITICAL	NEED-REVIEW	MISSING_DEVICE
	Wait Until Page Contains	${TEST_DEVICE}
	Click Link	${TEST_DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spmcommands_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	id=command	${COMMAND}
	GUI::Basic::Spinner Should Be Invisible
	Sleep	10s
	GUI::Basic::Spinner Should Be Invisible
	Double Click Element	//div[@id="outlets"]//option[@value="${GEIST_METERED_PDU}"]
	Sleep	10s
	GUI::Basic::Spinner Should Be Invisible
	Double Click Element	//*[@id="outlets"]/div[2]/div[3]/select/option
	GUI::Basic::Spinner Should Be Invisible
	Double Click Element	//*[@id="outlets"]/div[2]/div[4]/select/option[1]
	Click Button	//*[@id="outlets"]/div[2]/div[5]/button[1]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save

Test to perform merge outlet operations
	[Tags]	NON-CRITICAL	NEED-REVIEW	MISSING_DEVICE
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait until Page Contains	${TEST_DEVICE}
	Click Element	//*[@id="|${TEST_DEVICE}"]/td[1]/div/a/span
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	css=.btn:nth-child(5)
	Click Button	css=.btn:nth-child(5)
	GUI::Basic::Spinner Should Be Invisible
	Sleep	10s
	Wait Until Element Contains	//*[@id="commandresponse"]	Power Cycle in progress
	click element	xpath=//input[@id='targetpowerstatus']
	Sleep	10s
	Wait Until Element Contains	//*[@id="commandresponse"]	Power Status: OFF

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::ManagedDevices::Delete Device If Exists	${GEIST_METERED_PDU}
	GUI::ManagedDevices::Delete Device If Exists	${TEST_DEVICE}

SUITE:Add Geist metered pdu Managed device
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=spm_name	${GEIST_METERED_PDU}
	Select From List By Label	id=type	${GEIST_METERED_TYPE}
	wait until element is visible	//*[@id="phys_addr"]
	Input Text	//*[@id="phys_addr"]	${GEIST_METERED_IP}
	Select From List By Label	id=status	On-demand
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Add SNMPV2 community
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Link	${GEIST_METERED_PDU}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spmmgmt_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="snmp_proto_enabled"]
	Input Text	//*[@id="community2"]	private
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${GEIST_METERED_PDU}
	sleep	5s
	GUI::ManagedDevices::Delete Device If Exists	${TEST_DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	close all browsers

Auto discovery for device
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Auto Discovery::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="discovery_now"]
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Disabled	//*[@id="discovernow"]
	Page Should Contain Element	//*[@id="discovernowTable"]
	Select Checkbox	//*[@id="pdu|${GEIST_METERED_PDU}"]/td[1]/input
	Click Button	//*[@id="discovernow"]
	GUI::Basic::Spinner Should Be Invisible