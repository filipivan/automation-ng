*** Settings ***
Documentation	Testing Rename devices without having to check the box and scroll up
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    EXCLUDEIN4_2

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${DEVICE_1}	device01
${DEVICE_2}	device02
${DEVICE_3}	device03
${DEVICE_4}	device04
${DEVICE_5}	device05
${DEVICE_6}	device06
${DEVICE_7}	device07
${DEVICE_8}	device08
${DEVICE_9}	device09
${DEVICE_10}	device10
${DEVICE_11}	device11
${DEVICE_12}	device12
${DEVICE_13}	device13
${DEVICE_14}	device14
${DEVICE_15}	device15
${DEVICE_16}	device16
${DEVICE_17}	device17
${DEVICE_18}	device18
${DEVICE_19}	device19
${DEVICE_20}	device20
${DEVICE_rename}	devicerename
${Access_License}        XR9R4-274UG-ZNJB1-B28VR

*** Test Cases ***
Test Case to Add License
    GUI::Basic::System::License::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text      //*[@id="licensevalue"]     ${Access_License}
	GUI::Basic::Save If Configuration Changed

Add Devices
    SUITE:Setup
    GUI::ManagedDevices::Add Device	${DEVICE_1}	device_console	127.0.0.1	${USERNAME}	no	${PASSWORD}	enabled
    GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Add Device	${DEVICE_2}	device_console	127.0.0.1	${USERNAME}	no	${PASSWORD}	enabled
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Add Device	${DEVICE_3}	device_console	127.0.0.1	${USERNAME}	no	${PASSWORD}	enabled
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Add Device	${DEVICE_4}	device_console	127.0.0.1	${USERNAME}	no	${PASSWORD}	enabled
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Add Device	${DEVICE_5}	device_console	127.0.0.1	${USERNAME}	no	${PASSWORD}	enabled
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Add Device	${DEVICE_6}	device_console	127.0.0.1	${USERNAME}	no	${PASSWORD}	enabled
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Add Device	${DEVICE_7}	device_console	127.0.0.1	${USERNAME}	no	${PASSWORD}	enabled
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Add Device	${DEVICE_8}	device_console	127.0.0.1	${USERNAME}	no	${PASSWORD}	enabled
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Add Device	${DEVICE_9}	device_console	127.0.0.1	${USERNAME}	no	${PASSWORD}	enabled
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Add Device	${DEVICE_10}	device_console	127.0.0.1	${USERNAME}	no	${PASSWORD}	enabled
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Add Device	${DEVICE_11}	device_console	127.0.0.1	${USERNAME}	no	${PASSWORD}	enabled
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Add Device	${DEVICE_12}	device_console	127.0.0.1	${USERNAME}	no	${PASSWORD}	enabled
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Add Device	${DEVICE_13}	device_console	127.0.0.1	${USERNAME}	no	${PASSWORD}	enabled
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Add Device	${DEVICE_14}	device_console	127.0.0.1	${USERNAME}	no	${PASSWORD}	enabled
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Add Device	${DEVICE_15}	device_console	127.0.0.1	${USERNAME}	no	${PASSWORD}	enabled
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Add Device	${DEVICE_16}	device_console	127.0.0.1	${USERNAME}	no	${PASSWORD}	enabled
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Add Device	${DEVICE_17}	device_console	127.0.0.1	${USERNAME}	no	${PASSWORD}	enabled
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Add Device	${DEVICE_18}	device_console	127.0.0.1	${USERNAME}	no	${PASSWORD}	enabled
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Add Device	${DEVICE_19}	device_console	127.0.0.1	${USERNAME}	no	${PASSWORD}	enabled
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Add Device	${DEVICE_20}	device_console	127.0.0.1	${USERNAME}	no	${PASSWORD}	enabled
    GUI::Basic::Spinner Should Be Invisible
    ${DEVICES} =	Create List	${DEVICE_1}	${DEVICE_2}	${DEVICE_3}	${DEVICE_4}	${DEVICE_5}	${DEVICE_6}	${DEVICE_7}	${DEVICE_8}	${DEVICE_9}	${DEVICE_10}	${DEVICE_11}	${DEVICE_12}	${DEVICE_13}	${DEVICE_14}	${DEVICE_15}	${DEVICE_16}    ${DEVICE_17}	${DEVICE_18}	${DEVICE_19}	${DEVICE_20}
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Table Should Has Row With Id   SPMTable	${DEVICE_1}
	GUI::Basic::Table Should Has Row With Id   SPMTable	${DEVICE_2}
	GUI::Basic::Table Should Has Row With Id   SPMTable	${DEVICE_5}
	GUI::Basic::Table Should Has Row With Id   SPMTable	${DEVICE_10}
	GUI::Basic::Table Should Has Row With Id   SPMTable	${DEVICE_11}
	GUI::Basic::Table Should Has Row With Id   SPMTable	${DEVICE_12}
	GUI::Basic::Table Should Has Row With Id   SPMTable	${DEVICE_15}
	GUI::Basic::Table Should Has Row With Id   SPMTable	${DEVICE_19}
	GUI::Basic::Table Should Has Row With Id   SPMTable	${DEVICE_20}

Rename a device
    SUITE:Setup
    GUI::Basic::Spinner Should Be Invisible
    Click Element     //*[@id="${DEVICE_20}"]/td[1]/input
    GUI::Basic::Click Element	//input[@value='Rename']
    GUI::Basic::Spinner Should Be Invisible
    Input Text	//*[@id="spm_name"]     ${DEVICE_rename}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Table Should Has Row With Id   SPMTable	${DEVICE_rename}
    GUI::Basic::Table Should Not Have Row With Id   SPMTable    ${DEVICE_20}

Delete Devices
    SUITE:Setup
    GUI::ManagedDevices::Delete Devices	${DEVICE_1}	${DEVICE_2}	${DEVICE_3}	${DEVICE_4}	${DEVICE_5}	${DEVICE_6}	${DEVICE_7}	${DEVICE_8}	${DEVICE_9}	${DEVICE_10}	${DEVICE_11}	${DEVICE_12}	${DEVICE_13}	${DEVICE_14}	${DEVICE_15}	${DEVICE_16}    ${DEVICE_17}	${DEVICE_18}	${DEVICE_19}	${DEVICE_rename}
    GUI::Basic::Table Should Not Have Row With Id   SPMTable    ${DEVICE_1}
    GUI::Basic::Table Should Not Have Row With Id   SPMTable    ${DEVICE_rename}
    GUI::Basic::Table Should Not Have Row With Id   SPMTable    ${DEVICE_20}

Test Case delete License
    GUI::Basic::System::License::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Delete All Rows In Table If Exists	\#license_table	False

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Managed Devices::Devices::Open Tab
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	Close All Browsers
