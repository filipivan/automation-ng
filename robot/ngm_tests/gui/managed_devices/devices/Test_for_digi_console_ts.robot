*** Settings ***
Documentation	Test for digi console ts
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Variables ***
${Name}     digi-console-ts
${Type}     console_server_digicp
${Ip_address}       192.168.3.144
${Usr}     root
${Passwd}     dbps
${Confm_passwd}     dbps
${Status}     On-demand

*** Test Cases ***
Test case to add a NSC as Digi Console Ts
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists	${Name}
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text	    //*[@id='spm_name']	    ${Name}
	Select From List By Label   //*[@id="type"]    ${Type}
	Input Text      //*[@id="url"]      https://%IP
	Input Text      id=phys_addr    ${Ip_address}
	Input Text      id=username          ${Usr}
    Input Text      id=passwordfirst     ${Passwd}
    Input Text      id=passwordconf      ${Confm_passwd}
    Select From List By Label    id=status    ${Status}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Table Should Has Row With Id        SPMTable    ${Name}
    GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain    ${Name}

Test case to check added Digi Console Ts is able to access On Console Window
	[Tags]	NON-CRITICAL	NEED-REVIEW
    GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Page Contains Element        xpath=(//a[contains(text(),'Console')])[2]
    Click Element       xpath=(//a[contains(text(),'Console')])[2]
    GUI::Basic::Spinner Should Be Invisible
    Sleep       10s
    Run Keyword If  '${NGVERSION}'<='5.4'        Switch Window       ${Name}
    Run Keyword If  '${NGVERSION}'>'5.4'        Switch Window        digi-console-ts - Console
    Wait Until Page Contains Element        xpath=//*[@id='termwindow']
    Select Frame	xpath=//*[@id='termwindow']
    Sleep       10s
	Press Keys	//body	RETURN
	[Teardown]	SUITE:Teardown

Test case to check added Digi Console Ts is able to access On Web Window
    SUITE:Setup
    GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element       xpath=//a[contains(text(),'Web')]
    GUI::Basic::Spinner Should Be Invisible
    Sleep       30s
    Run Keyword If  '${NGVERSION}'<='5.4'        Switch Window       ${Name}
    Run Keyword If  '${NGVERSION}'>'5.4'       Switch Window        digi-console-ts - Web
	[Teardown]	SUITE:Teardown

Test case to delete added Configuration on managed device
    SUITE:Setup
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element       //*[@id="${Name}"]/td[1]/input
    Click Element       //*[@id='delButton']
    Handle Alert
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	Close All Browsers
