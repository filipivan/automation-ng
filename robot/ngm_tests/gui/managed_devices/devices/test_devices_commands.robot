*** Settings ***
Documentation	Testing Commands Tab on  managed devices -> devices for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Test PDU Tab Elements
    GUI::ManagedDevices::Add Device     PDU             pdu_apc                 ${EMPTY}  ${EMPTY}  no    ${EMPTY}  disabled
    Click Element       //*[@id="PDU"]/td[2]/a
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Pod Menu Must Be	Access	Management	Logging	Custom Fields	Commands	Outlets

Test Commands Tab Elements
    Click Element       //*[@id="spmcommands_nav"]
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be Enabled   //*[@id="returnButton"]
    Element Should Be Enabled   //*[@id="addButton"]

Test Add Commands
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain Element     //*[@id="command"]
    Select Checkbox     //*[@id="enabled"]
    Page Should Contain Element     //*[@id="protocol"]

    FOR    ${I}    IN RANGE    1    11
		Element Should Be Visible     //*[@id="addCommandForm"]/div[2]/div/div/div[4]/div[2]/div[${I}]
		Element Should Be Visible     //*[@id="addCommandForm"]/div[2]/div/div/div[4]/div[2]/div[${I}]/div[1]
		Element Should Be Visible     //*[@id="addCommandForm"]/div[2]/div/div/div[4]/div[2]/div[${I}]/div[2]
		Element Should Be Visible     //*[@id="addCommandForm"]/div[2]/div/div/div[4]/div[2]/div[${I}]/div[2]
	END

Delete Device
    GUI::Basic::Return
    GUI::ManagedDevices::Delete Devices     PDU
    GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
    GUI::ManagedDevices::Open
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
