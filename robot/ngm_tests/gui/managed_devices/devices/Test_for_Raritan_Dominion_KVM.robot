*** Settings ***
Documentation	Test for KVM direct connection to device via RDP(Windows)
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

*** Variables ***
${Name}	raritan
${Type}	kvm_raritan
${Ip_address}	192.168.2.23
${Usr}	admin
${Passwd}	D0mini0n
${Confm_passwd}	D0mini0n
${Status}	On-demand
${Status_kvm-rule}	enabled
${Dominion_port1}	Dominion_KX3_Port1
${Dominion_port2}	Dominion_KX3_Port2
${Operation}	ondemand

*** Test Cases ***
Test case to add a RDP(Windows) configuration
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${Name}
	GUI::ManagedDevices::Delete Device If Exists	${Dominion_port1}
	GUI::ManagedDevices::Delete Device If Exists	${Dominion_port2}
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text	//*[@id='spm_name']	${Name}
	Select From List By Label   //*[@id="type"]	${Type}
	Input Text	id=phys_addr	${Ip_address}
	Input Text	id=username	${Usr}
	Input Text	id=passwordfirst	${Passwd}
	Input Text	id=passwordconf	${Confm_passwd}
	Select From List By Label	id=status	${Status}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Table Should Has Row With Id	SPMTable	${Name}
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${Name}

Test Case to add discovery rules
	SUITE:Setup
	GUI::ManagedDevices::Open Discovery Rules
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Sleep	10s
	Input Text	//*[@id="discovery_name"]	kvm-rule
	wait until element is visible	xpath=//select[@id='status']
	Click Element	xpath=//select[@id='status']
	Click Element	xpath=//label[contains(.,'KVM Ports')]
	Click Element	xpath=//select[@id='operation']
	Select From List by Value	xpath=//select[@id='operation']	${Operation}
	Select From List By Label	//*[@id="seed"]	${Name}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="kvm-rule"]

Test Case to KVM Ports Device
	SUITE:Setup
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="discovery_now"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//tr[@id='kvm|raritan']/td/input
	Click Element	//*[@id="discovernow"]

Test Case to Verify the KVM Ports on Discovery logs page
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Setup
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_discoverylogs"]
	Sleep	15s
	page should contain element	xpath=//td[contains(.,'Device Cloned')]
	page should contain element	xpath=//td[contains(.,'192.168.2.23')]
	page should contain element	xpath=//td[contains(.,'Dominion_KX3_Port1')]
	page should contain element	xpath=//td[contains(.,'KVM Ports')]
	page should contain element	xpath=//td[contains(.,'Dominion_KX3_Port2')]

Test Case Access KVM Ports
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Setup
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep	15s
	Click Element	//*[@id="|raritan"]/td[1]/div/a
	Switch Window
	wait until page contains element	xpath=//input[@id='webSession']
	Click Element	xpath=//input[@id='webSession']
	Sleep	15s
	Switch Window	${Name}

Test Case to delete added discovery rules
	SUITE:Setup
	GUI::ManagedDevices::Open Discovery Rules
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="kvm-rule"]/td[1]/input
	Click Element	//*[@id='delButton']
	Handle Alert
	GUI::ManagedDevices::Delete Device If Exists	${Name}
	GUI::ManagedDevices::Delete Device If Exists	${Dominion_port1}
	GUI::ManagedDevices::Delete Device If Exists	${Dominion_port2}
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers