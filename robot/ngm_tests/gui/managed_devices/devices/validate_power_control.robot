*** Settings ***
Documentation	Test pdu operations
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2        NON-CRITICAL

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${PDU_DEVICE}      test_raritan
${DEVICE_TYPE}     pdu_raritan
${PDU_ID}          My-PDU
${OUTLET_ID1}      1
${OUTLET1_NAME}    Outlet_1
${OUTLET2_NAME}    test-bnu_02
${OUTLET3_NAME}    Outlet_3
${TEST_DEVICE}     test_device
${TEST_DEVICE_TYPE}    device_console
${COMMAND}         Outlet
${NEW_USER}    test1
${NEW_PASS}    test1

*** Test Cases ***
Test Case To Add pdu device/protocol/SNMPV2
    GUI::Basic::Button::Add
    GUI::Basic::Spinner Should Be Invisible
    Input Text  id=spm_name       ${PDU_DEVICE}
    Select From List By Label    id=type    ${DEVICE_TYPE}
    Input Text  id=phys_addr     ${PDU_RARITAN}[ip]
    Select From List By Label     id=status    On-demand
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Managed Devices::Devices::open tab
    GUI::Basic::Spinner Should Be Invisible
    Click Link    ${PDU_DEVICE}
    GUI::Basic::Spinner Should Be Invisible
    Click Element    //*[@id="spmmgmt_nav"]/span
    GUI::Basic::Spinner Should Be Invisible
    Select Checkbox    //*[@id="snmp_proto_enabled"]
    Input Text    //*[@id="community2"]    private
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    Click Element    //*[@id="spmcommands_nav"]/span
    GUI::Basic::Spinner Should Be Invisible
    Click Element    //*[@id="Outlet"]
    GUI::Basic::Spinner Should Be Invisible
    Select From List By Label    //*[@id="protocol"]    SNMP
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    Close Browser

Test case to perform auto discovery and check outlets discoverd on outlets page
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Managed Devices::Auto Discovery::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element	//*[@id="discovery_now"]
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be Disabled	//*[@id="discovernow"]
    Page Should Contain Element	//*[@id="discovernowTable"]
    Select Checkbox    //*[@id="pdu|${PDU_DEVICE}"]/td[1]/input
    Click Button    //*[@id="discovernow"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Managed Devices::Devices::open tab
    GUI::Basic::Spinner Should Be Invisible
    Sleep    15s
    Click Link    ${PDU_DEVICE}
    GUI::Basic::Spinner Should Be Invisible
    Click Element    //*[@id="spmoutlets_nav"]/span
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain    ${OUTLET1_NAME}
    Page Should Contain    ${OUTLET2_NAME}
    Page Should Contain    ${OUTLET3_NAME}
    GUI::Basic::Spinner Should Be Invisible
    Close Browser

Test case to do pdu operations on Access table
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI:Access::Open Table tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element    //*[@id="|${PDU_DEVICE}"]/td[1]/div/a/span
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Page Contains Element   css=.btn:nth-child(4)
    Click Button    css=.btn:nth-child(4)
    GUI::Basic::Spinner Should Be Invisible
    Page Should not contain    unknown
    Select Checkbox    //*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[1]/input
    Click Button    //*[@id="nonAccessControls"]/input[2]
    GUI::Basic::Spinner Should Be Invisible
    Sleep    20s
    Wait Until Element Contains   //*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]    Off
    Select Checkbox    xpath=//tr[@id='test_raritan:My-PDU:1:Outlet_1']/td/input
    Click Button    css=.btn:nth-child(3)
    GUI::Basic::Spinner Should Be Invisible
    Sleep    20s
    Wait Until Element Contains   //*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]    On
    Select Checkbox    //*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[1]/input
    Click Button    css=#nonAccessControls > .btn:nth-child(2)
    GUI::Basic::Spinner Should Be Invisible
    Sleep       30s
    Wait Until Element Contains   //*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]    Off
    Select Checkbox    xpath=//tr[@id='test_raritan:My-PDU:1:Outlet_1']/td/input
    Click Button    css=.button:nth-child(1)
    GUI::Basic::Spinner Should Be Invisible
    Sleep       30s
    Wait Until Element Contains   //*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]   	On
    GUI::Basic::Spinner Should Be Invisible
    Close Browser

Test case to add managed device merge outlets
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Managed Devices::Devices::open tab
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Button::Add
    GUI::Basic::Spinner Should Be Invisible
    Input Text  id=spm_name       ${TEST_DEVICE}
    Select From List By Label    id=type    ${TEST_DEVICE_TYPE}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    Click Link    ${TEST_DEVICE}
    GUI::Basic::Spinner Should Be Invisible
    Click Element    //*[@id="spmcommands_nav"]/span
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
    Select From List By Label    id=command   ${COMMAND}
    GUI::Basic::Spinner Should Be Invisible
    Sleep    20s
    GUI::Basic::Spinner Should Be Invisible
    Double Click Element   //div[@id="outlets"]//option[@value="${PDU_DEVICE}"]
    Sleep    20s
    GUI::Basic::Spinner Should Be Invisible
    Double Click Element    //*[@id="outlets"]/div[2]/div[3]/select/option
    GUI::Basic::Spinner Should Be Invisible
    Double Click Element    //*[@id="outlets"]/div[2]/div[4]/select/option[1]
    Click Button    //*[@id="outlets"]/div[2]/div[5]/button[1]
    GUI::Basic::Spinner Should Be Invisible
    Sleep       20s
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible

Test case to perform power operations
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI:Access::Open Table tab
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Page Contains Element   //*[@id="|test_device"]/td[2]/div/a[1]
    Click Element     xpath=(//a[contains(text(),'Console')])[2]
    sleep    20s
    Run Keyword If  '${NGVERSION}'<='5.4'       Switch Window       ${TEST_DEVICE}
    Run Keyword If  '${NGVERSION}'>'5.4'         Switch Window      ${TEST_DEVICE} - Console
    Wait Until Page Contains Element    //iframe[@id='termwindow']
    Select Frame    //iframe[@id='termwindow']
    Sleep   20s
    Press Keys      //body   RETURN
    Press Keys      //body   RETURN

    ${OUTPUT}=    GUI:Access::Generic Console Command Output    CTRL+O
    Page Should Contain    Options
    Unselect Frame
    Press Keys 	 //body 	cw
    Close all browsers
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI:Access::Open Table tab
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Page Contains Element    //*[@id="|test_device"]/td[1]/div/a/span
    Click Element    //*[@id="|test_device"]/td[1]/div/a/span
    Wait Until Page Contains Element    //*[@id="targetpowerstatus"]
    Wait Until Page Contains Element     //*[@id="targetpowercycle"]
    Wait Until Page Contains Element    //*[@id="targetpoweroff"]
    Wait Until Page Contains Element    //*[@id="targetpoweron"]

Test Case to Delete Managed devices
    SUITE:Delete Managed Devices

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Managed Devices::Devices::open tab
    GUI::ManagedDevices::Delete Device If Exists    ${PDU_DEVICE}
    GUI::ManagedDevices::Delete Device If Exists     ${TEST_DEVICE}
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
    close all browsers

SUITE:Delete Managed Devices
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Managed Devices::Devices::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists     ${PDU_DEVICE}
    sleep       5s
    GUI::ManagedDevices::Delete Device If Exists     ${TEST_DEVICE}
