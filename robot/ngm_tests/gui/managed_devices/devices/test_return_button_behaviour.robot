*** Settings ***
Documentation	Verifies if the return button in commands tab from managed devices works properly
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE 
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

*** Test Cases ***
Check if return button inside Commands tabs returns to device table
    GUI::Basic::Managed Devices::Devices::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Enter Device	${DUMMY_DEVICE_CONSOLE_NAME}
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Devices::Open Commands Menu
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Return Button
	GUI::Basic::Spinner Should Be Invisible
	Table Should Contain	jquery=#SPMTable	${DUMMY_DEVICE_CONSOLE_NAME}

Check if return button after drilldown into a command returns to the commands table
    GUI::Basic::Managed Devices::Devices::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Enter Device	${DUMMY_DEVICE_CONSOLE_NAME}
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Devices::Open Commands Menu
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//*/a[@id='Console']
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Return Button
	GUI::Basic::Spinner Should Be Invisible
	Table Should Contain	    jquery=#SPMCommandsTable	Console

Check if return button after return from a command returns to the devices table
    GUI::Basic::Managed Devices::Devices::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Enter Device	${DUMMY_DEVICE_CONSOLE_NAME}
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Devices::Open Commands Menu
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//*/a[@id='Console']
	GUI::Basic::Return Button
	GUI::Basic::Return Button
	Table Should Contain	    jquery=#SPMTable	${DUMMY_DEVICE_CONSOLE_NAME}

Check if return button when adding command returns to the devices table
    GUI::Basic::Managed Devices::Devices::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Enter Device	${DUMMY_DEVICE_CONSOLE_NAME}
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Devices::Open Commands Menu
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Return Button
	GUI::Basic::Spinner Should Be Invisible
	Table Should Contain	    jquery=#SPMTable	${DUMMY_DEVICE_CONSOLE_NAME}

Check if table values after drilldown a command and return are correct
    GUI::Basic::Managed Devices::Devices::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Enter Device	${DUMMY_DEVICE_CONSOLE_NAME}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Devices::Open Commands Menu
	GUI::Basic::Spinner Should Be Invisible
	${T1} = 	GUI::Basic::Get Table Values        SPMCommandsTable
	GUI::Basic::Drilldown Table Row By Index        \#SPMCommandsTable	1
	GUI::Basic::Return Button
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Wait For Device Commands
	${T2} = 	GUI::Basic::Get Table Values        SPMCommandsTable
	GUI::Basic::Table Values Should Be Equal	${T1}	${T2}
	GUI::ManagedDevices::Delete Device If Exists     ${DUMMY_DEVICE_CONSOLE_NAME}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Delete Suite Data
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Add Dummy Device Console	${DUMMY_DEVICE_CONSOLE_NAME}	device_console	127.0.0.1	22	root	no	${ROOT_PASSWORD}	ondemand

SUITE:Teardown
	SUITE:Delete Suite Data
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:Delete Suite Data
	@{IDS} =	Create List	${DUMMY_DEVICE_CONSOLE_NAME}
	GUI::ManagedDevices::Delete Devices If Exists       ${IDS}
