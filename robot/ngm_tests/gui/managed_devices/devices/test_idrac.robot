*** Settings ***
Documentation	Test Perle console server support
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Variables ***
${TYPE_IDRAC}     drac
${MODE}     On-demand
${NAME_IDRAC}    test_idrac
${IDRAC_IP}     192.168.2.233
${IDRAC_USERNAME}   root
${IDRAC_PASS}   calvin
${COMM_KVM}  KVM
${EXT_TYPE}     idrac.py

*** Test Cases ***
Add idrac8 to the managed devices
    GUI::ManagedDevices::Delete Device If Exists	${NAME_IDRAC}
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Button::Add
    GUI::Basic::Spinner Should Be Invisible
    Input Text  id=spm_name       ${NAME_IDRAC}
    Select From List By Label    id=type    ${TYPE_IDRAC}
    Input Text  id=phys_addr     ${IDRAC_IP}
    Input Text  id=username          ${IDRAC_USERNAME}
    Input Text  id=passwordfirst     ${IDRAC_PASS}
    Input Text  id=passwordconf      ${IDRAC_PASS}
    Select From List By Label    id=status    ${MODE}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Table Should Has Row With Id        SPMTable    ${NAME_IDRAC}

Add device command KVM for idrac8 using script idrac.py
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    Select Checkbox    //*[@id="test_idrac"]/td[1]/input
    Click Element       xpath=//a[contains(text(),'test_idrac')]
    GUI::Basic::Spinner Should Be Invisible
    Sleep       5s
    Click Element       css=#spmcommands_nav > .title_b
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Button::Add
    GUI::Basic::Spinner Should Be Invisible
    Select From List By Label    id=command    ${COMM_KVM}
    Select From List By Label    id=pymod_file    ${EXT_TYPE}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible

Check that the device is created and showing in access table
    GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain    ${NAME_IDRAC}

Test KVM command
	[Tags]	NON-CRITICAL	NEED-REVIEW
    wait until element is visible       xpath=//a[contains(text(),'KVM')]
    Click Element       xpath=//a[contains(text(),'KVM')]
    Sleep      20s
#    idrac  Device  was not able accessible
#    Run Keyword If  '${NGVERSION}'<='5.4'        Switch Window       ${NAME_IDRAC}
#    Run Keyword If  '${NGVERSION}'>'5.4'        Switch Window        ${NAME_IDRAC} - KVM
#    Sleep        5s

Delete Devices
	[Tags]	NON-CRITICAL
    SUITE:Setup
    GUI::ManagedDevices::Delete Devices     ${NAME_IDRAC}
    GUI::Basic::Table Should Not Have Row With Id   SPMTable    ${NAME_IDRAC}

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Managed Devices::Devices::open tab
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	close all browsers