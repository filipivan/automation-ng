*** Settings ***
Documentation	Test for digi console passport
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

*** Variables ***
${Name}     digi-console-Passport
${Type}     console_server_digicp
${Ip_address}       192.168.3.91
${Usr}     root
${Passwd}     dbps
${Confm_passwd}     dbps
${Status}     On-demand

*** Test Cases ***
Test case to add a NSC as Digi Console Passport
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists	${Name}
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text	    //*[@id='spm_name']	    ${Name}
	Select From List By Label   //*[@id="type"]    ${Type}
	Input Text      id=phys_addr    ${Ip_address}
	Input Text      id=username          ${Usr}
    Input Text      id=passwordfirst     ${Passwd}
    Input Text      id=passwordconf      ${Confm_passwd}
    Select From List By Label    id=status    ${Status}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Table Should Has Row With Id        SPMTable    ${Name}
    GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain    ${Name}

Test case to check added Digi Console Passport is able to access On Console Window
	[Tags]	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW	BUG_NG_9567
    GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Page Contains Element        xpath=(//a[contains(text(),'Console')])[2]
    Click Element       xpath=(//a[contains(text(),'Console')])[2]
    GUI::Basic::Spinner Should Be Invisible
    Sleep       30s
    Run Keyword If  '${NGVERSION}'<='5.4'       Switch Window       digi-console-Passport
    Run Keyword If  '${NGVERSION}'>'5.4'         Switch Window      digi-console-Passport - Console
    Wait Until Page Contains Element        xpath=//*[@id='termwindow']
    Select Frame	xpath=//*[@id='termwindow']
    Sleep       20s
	Press Keys      //body      RETURN
    ${OUTPUT}=	GUI:Access::Generic Console Command Output	hostname
	Should Contain	${OUTPUT}	Digi_Passport
	[Teardown]	Close All Browsers

Test case to check added Digi Console Passport is able to access On Web Window
    SUITE:Setup
    GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element       xpath=//a[contains(text(),'Web')]
    GUI::Basic::Spinner Should Be Invisible
    Sleep       30s
    Run Keyword If  '${NGVERSION}'<='5.4'       Switch Window       digi-console-Passport
    Run Keyword If  '${NGVERSION}'>'5.4'       Switch Window        digi-console-Passport - Web
	[Teardown]	Close All Browsers

Test case to delete added Configuration on managed device
    SUITE:Setup
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element       //*[@id="${Name}"]/td[1]/input
    Click Element       //*[@id='delButton']
    Handle Alert
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Close All Browsers
