*** Settings ***
Documentation	Test for Add leased licensed to licenses and managed device page
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX		NON-CRITICAL
Default Tags	EXCLUDEIN4_2

*** Variables ***
${DEV_COORD_NAME}       co-ordinator
${DEV_PEER_NAME}        peer
${PEER_IP}      ${HOSTPEER}
${PEER_HOMEPAGE}        ${HOMEPAGEPEER}
${COORD_IP}     ${HOST}
${COORD_HOMEPAGE}       ${HOMEPAGE}
${DEVICE}       test_device
${DEVICE_TYPE}      device_console
${DEVICE_IP}        127.0.0.1
${CLUSTER_LICENSE_1}        ${TEN_DEVICES_CLUSTERING_LICENSE}
${CLUSTER_LICENSE_2}        ${TEN_DEVICES_CLUSTERING_LICENSE_2}
${DEFAULT_HOSTNAME}     ${HOSTNAME_NODEGRID}
${COORD_DOMAIN_NAME}        co-ordinator
${PEER_DOMAIN_NAME}     peer
${PRE_SHARED_KEY}       admin
${Username}     admin
${Password}     admin
${Confirm_Password}     admin
${IS_STAR_MODE}     Mesh
${DOMAIN_NAME}      co-ordinator
${Co-ordinator_addrs}       ${HOST}
${Name_1}       test_1
${Name_2}       test_2
${Name_3}       test_3
${Name_4}       test_4
${Name_5}       test_5
${Name}     test
${Type}     device_console
${Ip_address_1}     10.0.0.1
${Ip_address_2}     10.0.0.2
${Ip_address_3}     10.0.0.3
${Ip_address_4}     10.0.0.4
${Ip_address_5}     10.0.0.5
${Access_License}       XR9R4-274UG-ZNJB1-B28VR
${Default_DOMAIN_NAME}      localdomain

*** Test Cases ***
Test Case to Configure Co-ordinator Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Input Text      domainname      ${DOMAIN_NAME}
	GUI::Basic::Save If Configuration Changed
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists        ${DEV_COORD_NAME}
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text      //*[@id='spm_name']     ${DEV_COORD_NAME}
	Select From List By Label       //*[@id="type"]     ${DEVICE_TYPE}
	Input Text      id=phys_addr        ${DEVICE_IP}
	Input Text      id=username     ${Username}
	Input Text      id=passwordfirst        ${Password}
	Input Text      id=passwordconf     ${Confirm_Password}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Table Should Has Row With Id	SPMTable	${DEV_COORD_NAME}
	GUI::Cluster::Open Settings Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox     //*[@id="enabled"]
	select checkbox     //*[@id="lps_enabled"]
	Select Radio Button	nodeType	master
	Input Text      //*[@id="preSharedKey"]     ${PRE_SHARED_KEY}
	Click Element       //*[@id="starMode"]
	Click Element       //*[@id="cluster_enabled"]
	Wait Until Element is Visible       //*[@id="lps_enabled"]
	Click element       //*[@id="lps_enabled"]
	click element       //*[@id="lps_enabled"]
	Click Element       //*[@id="lps_type"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::License::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text      //*[@id="licensevalue"]     ${CLUSTER_LICENSE_1}
	GUI::Basic::Save If Configuration Changed
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text      //*[@id="licensevalue"]     ${CLUSTER_LICENSE_2}
	GUI::Basic::Save If Configuration Changed
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text      //*[@id="licensevalue"]     ${Access_License}
	GUI::Basic::Save If Configuration Changed

Test Case to Configure Peer Side
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Input Text	domainname      ${PEER_DOMAIN_NAME}
	GUI::Basic::Save If Configuration Changed
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists        ${PEER_DOMAIN_NAME}
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text	//*[@id='spm_name']     ${PEER_DOMAIN_NAME}
	Select From List By Label	//*[@id="type"]     ${DEVICE_TYPE}
	Input Text      id=phys_addr        ${DEVICE_IP}
	Input Text      id=username     ${Username}
	Input Text      id=passwordfirst        ${Password}
	Input Text      id=passwordconf     ${Confirm_Password}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Table Should Has Row With Id	SPMTable	${PEER_DOMAIN_NAME}
	GUI::Cluster::Open Settings Tab
	GUI::Basic::Spinner Should Be Invisible
	select checkbox     //*[@id="lps_enabled"]
	Select Checkbox     //*[@id="enabled"]
	Click Element       css=.radio:nth-child(3) #nodeType
	wait until element is visible		//*[@id="masterAddr"]
	Input Text      //*[@id="masterAddr"]		${Co-ordinator_addrs}
	Input Text      //*[@id="preSharedKey2"]        ${PRE_SHARED_KEY}
	Click Element       //*[@id="cluster_enabled"]
	select checkbox     //*[@id="cluster_enabled"]
	select checkbox     //*[@id="lps_enabled"]
	Click Element       css=.radio:nth-child(4) > label
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep       10s
	SUITE:Wait For License Lease From Coordinator To Complete

Test Case to add managed device
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists        ${Name_1}
	GUI::ManagedDevices::Delete Device If Exists        ${Name_2}
	GUI::ManagedDevices::Delete Device If Exists        ${Name_3}
	GUI::ManagedDevices::Delete Device If Exists        ${Name_4}
	GUI::ManagedDevices::Delete Device If Exists        ${Name_5}
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text      //*[@id='spm_name']     ${Name_1}
	Select From List By Label       //*[@id="type"]     ${Type}
	Input Text	id=phys_addr        ${Ip_address_1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep		3s
	page should contain        ${Name_1}
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text      //*[@id='spm_name']     ${Name_2}
	Select From List By Label       //*[@id="type"]     ${Type}
	Input Text      id=phys_addr        ${Ip_address_2}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep		3s
	page should contain        ${Name_2}
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text      //*[@id='spm_name']     ${Name_3}
	Select From List By Label       //*[@id="type"]     ${Type}
	Input Text	id=phys_addr        ${Ip_address_3}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep		3s
	page should contain        ${Name_3}
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text      //*[@id='spm_name']     ${Name_4}
	Select From List By Label       //*[@id="type"]     ${Type}
	Input Text      id=phys_addr        ${Ip_address_4}
	GUI::Basic::Save
	page should contain        ${Name_4}
	Sleep		3s
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text      //*[@id='spm_name']     ${Name_5}
	Select From List By Label       //*[@id="type"]     ${Type}
	Input Text      id=phys_addr        ${Ip_address_5}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	page should contain        ${Name_5}

Test Case to Verify leased licenses On Managed device Page
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep	15s
	Page Should Contain Element     xpath=//div[@id='table_status']/span	Access: ( Licensed | Used | Leased | Available ): 159 | 36 | 50 | 73
	SUITE:Wait For Accesss License Lease From Coordinator To Complete

Test Case to Delete Configuration On Peer Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::ManagedDevices::Delete Device If Exists        ${DEV_PEER_NAME}
	GUI::ManagedDevices::Delete Device If Exists        ${Name_1}
	GUI::ManagedDevices::Delete Device If Exists        ${Name_2}
	GUI::ManagedDevices::Delete Device If Exists        ${Name_3}
	GUI::ManagedDevices::Delete Device If Exists        ${Name_4}
	GUI::ManagedDevices::Delete Device If Exists        ${Name_5}
	GUI::Cluster::Disable Cluster
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Input Text	domainname      ${Default_DOMAIN_NAME}
	GUI::Basic::Save If Configuration Changed
	GUI::Basic::Spinner Should Be Invisible

Test Case to Delete Configuration On Co-ordinator Side
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::ManagedDevices::Delete Device If Exists  ${DEV_COORD_NAME}
	GUI::Cluster::Disable Cluster
	GUI::Cluster::Delete Licenses
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Input Text      domainname      ${Default_DOMAIN_NAME}
	GUI::Basic::Save If Configuration Changed
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:Wait For License Lease From Coordinator To Complete
	Wait Until Keyword Succeeds  36x  5s	SUITE:Check License Lease

SUITE:Check License Lease
	GUI::Basic::System::License::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	xpath=//td[contains(.,'Clustering')]

SUITE:Wait For Accesss License Lease From Coordinator To Complete
	Wait Until Keyword Succeeds  24x  5s	SUITE:Access Check License Lease

SUITE:Access Check License Lease
	GUI::Basic::System::License::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element     xpath=//td[contains(.,'Access')]
