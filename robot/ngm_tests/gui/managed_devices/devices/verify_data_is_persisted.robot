*** Settings ***
Documentation	Editing managed devices Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}

*** Test Cases ***

Login And Create Device
	GUI::ManagedDevices::Delete Devices	${DUMMY_DEVICE_CONSOLE_NAME}	${DUMMY_DEVICE_CONSOLE_NAME}2
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Add Dummy Device Console	${DUMMY_DEVICE_CONSOLE_NAME}	device_console	127.0.0.1	22	root	no	${ROOT_PASSWORD}	ondemand

Check IP Address Field Is Persisted
    Sleep       5s
    wait until page contains element        //*/a[@id='${DUMMY_DEVICE_CONSOLE_NAME}']
	GUI::Basic::Click Element	//*/a[@id='${DUMMY_DEVICE_CONSOLE_NAME}']
	Check Element Is Persisted      phys_addr       123.145.135.124

Check Username Field Is Persisted
    Check Element Is Persisted      username       user

Check Enable IP Alias::IP Address Field Is Persisted
	Select Checkbox     //*[@id='inbipEnable']
	GUI::Basic::Spinner Should Be Invisible
    Check Element Is Persisted      inbipAddr       123.145.135.125

Check Allow Telnet Protocol::TCP Socket Port Field Is Persisted
	GUI::Basic::Wait Until Element Is Accessible	jquery=#inbipTeln
	GUI::Basic::Select Checkbox	    //*[@id='inbipTeln']
	GUI::Basic::Spinner Should Be Invisible
    Check Element Is Persisted      inbipTelnPort       34

Check Allow Binary Socket::TCP Socket Port Field Is Persisted
	GUI::Basic::Wait Until Element Is Accessible	jquery=#inbipRaw
	GUI::Basic::Select Checkbox	    //*[@id='inbipRaw']
	GUI::Basic::Spinner Should Be Invisible
    Check Element Is Persisted      inbipRawPort       34

Check Enable Second IP Alias::IP Address Field Is Persisted
	GUI::Basic::Wait Until Element Is Accessible	jquery=#inbSecipEnable
	GUI::Basic::Select Checkbox	    //*[@id='inbSecipEnable']
	GUI::Basic::Spinner Should Be Invisible
    Check Element Is Persisted      inbSecipAddr       123.145.135.126

Check Enable Second IP Alias::Allow Telnet Protocol::TCP Socket Port Field Is Persisted
	GUI::Basic::Wait Until Element Is Accessible	jquery=#inbSecipTeln
	GUI::Basic::Select Checkbox	    //*[@id='inbSecipTeln']
	GUI::Basic::Spinner Should Be Invisible
    Check Element Is Persisted      inbSecipTelnPort       35

Check Enable Second IP Alias::Allow Binary Socket::TCP Socket Port Field Is Persisted
	GUI::Basic::Wait Until Element Is Accessible	jquery=#inbSecipRaw
	GUI::Basic::Select Checkbox	    //*[@id='inbSecipRaw']
	GUI::Basic::Wait Until Element Is Accessible	jquery=#inbSecipRawPort
	# should trigger an error
	GUI::Basic::Input Text	        //*[@id='inbSecipRawPort']	123.145.135.127
	GUI::Basic::Click Element	    //*[@id='saveButton']
	Wait Until Element Is Visible	jquery=#globalerr
    Check Element Is Persisted      inbSecipRawPort       28

Check Address Location Field Is Persisted
    Check Element Is Persisted      addr_location       asdqwe

Check Coordinates Location Field Is Persisted
    Check Element Is Persisted      coordinates       50,50

Check Enable Send Break is Persisted
	GUI::Basic::Wait Until Element Is Accessible	jquery=#enable-break
	Checkbox Should Not Be Selected	    jquery=#enable-break
	GUI::Basic::Select Checkbox	        //*[@id='enable-break']
	GUI::Basic::Click Element	        //*[@id='saveButton']
	GUI::Basic::Wait Until Element Is Accessible	jquery=#break-sequence
	Checkbox Should Be Selected	        jquery=#enable-break
	${BREAK_FIELD_VALUE}=	Get Value	jquery=#break-sequence
	Should Be Equal	${BREAK_FIELD_VALUE}	~break
    Check Element Is Persisted          break-sequence       ~break1
    Check Element Is Persisted          break-sequence       ~break
	GUI::Basic::Unselect Checkbox	    //*[@id='enable-break']
	GUI::Basic::Click Element	        //*[@id='saveButton']
	GUI::Basic::Wait Until Element Is Accessible	jquery=#enable-break
	Checkbox Should Not Be Selected	    jquery=#enable-break

Check Allow SSH protocol::SSH Port Field Is Persisted
	GUI::Basic::Wait Until Element Is Accessible	//*[@id='inbssh']
	GUI::Basic::Select Checkbox	    //*[@id='inbssh']
	GUI::Basic::Wait Until Element Is Accessible	jquery=#inbsshTCP
	# should trigger an error, since ssh port number should be between 2001 and 65000
	GUI::Basic::Input Text	        //*[@id='inbsshTCP']	55
	GUI::Basic::Click Element	    //*[@id='saveButton']
	Wait Until Element Is Visible	jquery=#globalerr
    Check Element Is Persisted      inbsshTCP       2002

Check Allow Telnet protocol::Telnet Port Is Persisted
	GUI::Basic::Wait Until Element Is Accessible	jquery=#inbteln
	GUI::Basic::Select Checkbox	    //*[@id='inbteln']
	GUI::Basic::Wait Until Element Is Accessible	jquery=#inbtelnTCP
	#should trigger an error, this field accept values between 2001 and 65000
	GUI::Basic::Input Text	        //*[@id='inbtelnTCP']	56
	GUI::Basic::Click Element	    //*[@id='saveButton']
	Wait Until Element Is Visible	jquery=#globalerr
    Check Element Is Persisted      inbtelnTCP       2003

Check Allow Binary Socket::TCP Socket Port Is Persisted
	GUI::Basic::Wait Until Element Is Accessible	jquery=#inbraw
	GUI::Basic::Select Checkbox	    //*[@id='inbraw']
	GUI::Basic::Wait Until Element Is Accessible	jquery=#inbrawTCP
	GUI::Basic::Input Text	        //*[@id='inbrawTCP']	57
	GUI::Basic::Click Element	    //*[@id='saveButton']
	Wait Until Element Is Visible	jquery=#globalerr
	Wait Until Element Is Visible	jquery=#inbrawTCP
	GUI::Basic::Input Text	        //*[@id='inbrawTCP']	2004
	${INBRAWTCP_FIELD_VALUE}=	Get Value	jquery=#inbrawTCP
	Should Be Equal	${INBRAWTCP_FIELD_VALUE}	2004

Check Expiration Date Field Is Persisted
	GUI::Basic::Wait Until Element Is Accessible	jquery=#ttlType
	GUI::Basic::Select Radio Button	ttlType	date
    Check Element Is Persisted      ttlDate       2030-03-05

Check If Device Type Change Is Persisted
	GUI::Basic::Wait Until Element Is Accessible	jquery=#type
	${TYPE_FIELD_VALUE}=	Get Value	        jquery=#type
	Should Be Equal	    ${TYPE_FIELD_VALUE}	    device_console

	Select From List By Value	jquery=#type	idrac6
	${TYPE_FIELD_VALUE}=	Get Value	        jquery=#type
	Should Be Equal	    ${TYPE_FIELD_VALUE}	    idrac6

	GUI::Basic::Click Element	        //*[@id='saveButton']
	${TYPE_FIELD_VALUE}=	Get Value	jquery=#type
	Should Be Equal	${TYPE_FIELD_VALUE}	idrac6

	GUI::Basic::Spinner Should Be Invisible

Check Data Loggin Strings Are Persisted
	Sleep	5
	Click Element	xpath=//span[contains(.,'Logging')]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Wait Until Element Is Accessible	jquery=#databuf
	GUI::Basic::Select Checkbox	    //*[@id='databuf']
	GUI::Basic::Wait Until Element Is Accessible	jquery=#alertenable
	GUI::Basic::Select Checkbox	    //*[@id='alertenable']
    Check Element Is Persisted      alertstring1       asdwe
    Check Element Is Persisted      alertstring2       asdwe
    Check Element Is Persisted      alertstring3       asdwe
    Check Element Is Persisted      alertstring4       asdwe
    Check Element Is Persisted      alertstring5       asdwe

Check Enable event logging alerts::Event String Is Persisted
	GUI::Basic::Wait Until Element Is Accessible	jquery=#eventlog
	GUI::Basic::Select Checkbox	    //*[@id='eventlog']
	GUI::Basic::Wait Until Element Is Accessible	jquery=#eventalarm
	GUI::Basic::Select Checkbox	    //*[@id='eventalarm']
    Check Element Is Persisted      eventstring1       aawdas
    Check Element Is Persisted      eventstring2       aawdas
    Check Element Is Persisted      eventstring3       aawdas
    Check Element Is Persisted      eventstring4       aawdas
    Check Element Is Persisted      eventstring5       aawdas

Delete Device Console
	GUI::ManagedDevices::Delete Devices	${DUMMY_DEVICE_CONSOLE_NAME}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

Check Element Is Persisted
    [Arguments]     ${Loc}      ${Val}
	GUI::Basic::Wait Until Element Is Accessible	jquery=#${Loc}
	GUI::Basic::Input Text	    //*[@id='${Loc}']	${Val}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#${Loc}
	${TEMP_VALUE}=	Get Value	jquery=#${Loc}
	Should Be Equal	${TEMP_VALUE}	${Val}