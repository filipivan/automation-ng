*** Settings ***
Documentation	Testing Managment Tab on	managed devices -> devices for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

*** Test Cases ***
Test Managment Tab Elements
#	the device not contain usb sensor In type field...
#	Wait Until Keyword Succeeds	30	2	GUI::ManagedDevices::Add Device	USB_Sensor	usb_sensor	${EMPTY}	${EMPTY}	no	${EMPTY}	disabled
	Wait Until Keyword Succeeds	30	2	GUI::ManagedDevices::Add Device	PDU_APC	pdu_apc	${EMPTY}	${EMPTY}	no	${EMPTY}	disabled
	Wait Until Keyword Succeeds	30	2	GUI::ManagedDevices::Add Device	ILO	ilo	${EMPTY}	${EMPTY}	no	${EMPTY}	disabled
	Wait Until Keyword Succeeds	30	2	GUI::ManagedDevices::Add Device	Dev_Console	device_console	${EMPTY}	${EMPTY}	no	${EMPTY}	disabled
	Wait Until Keyword Succeeds	30	2	GUI::ManagedDevices::Add Device	Con_Serv_acs	console_server_acs	${EMPTY}	${EMPTY}	no	${EMPTY}	disabled

#Test USB Sensor
#	Test Managment Tab	USB_Sensor

Test PDU APC
	Test Managment Tab	PDU_APC

Test ILO
	Test Managment Tab	ILO

Test Device Console
	Test Managment Tab	Dev_Console

Test Console Server ACS
	Test Managment Tab	Con_Serv_acs

Delete Devices
	GUI::ManagedDevices::Delete Devices	USB_Sensor	PDU_APC	ILO	Dev_Console	Con_Serv_acs

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

Test Managment Tab
	[Arguments]	${Dev_Name}
	Click Element	//*[@id="${Dev_Name}"]/td[2]/a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spmmgmt_nav"]
	GUI::Basic::Spinner Should Be Invisible
	Test Device
	Test Protos
	Test Monitoring
	Test Discovery
	Test Discovery Outlets
	Test Scripts
	Click Element	//*[@id="returnButton"]
	GUI::Basic::Spinner Should Be Invisible

Test Device
	Page Should Contain Element	//*[@id="dev_name_title"]
	Page Should Contain Element	//*[@id="spmmgmt_form"]/div[2]/div[1]/div/div[1]/div[2]

Test Protos
	#Test SSH Proto
	${SSH_Exist}=	Run Keyword And Return Status	Page Should Contain Element	//*[@id="ssh_proto_enabled"]
	Run Keyword If	${SSH_Exist}	SSH Proto

	#Test IMPI Proto
	${IMPI_Exist}=	Run Keyword And Return Status	Page Should Contain Element	//*[@id="ipmi_proto_enabled"]
	Run Keyword If	${IMPI_Exist}	IMPI Proto

	#Test SNMP Proto
	${SNMP_Exist}=	Run Keyword And Return Status	Page Should Contain Element	//*[@id="snmp_proto_enabled"]
	Run Keyword If	${SNMP_Exist}	SNMP Proto

Test Monitoring
	#Test Nominal Monitoring
	${Nominal_Exist}=	Run Keyword And Return Status	Page Should Contain Element	//*[@id="nominal_monit_enabled"]
	Run Keyword If	${Nominal_Exist}	Nominal Monit

	#Test IMPI Monitoring
	${IMPI_Exist}=	Run Keyword And Return Status	Page Should Contain Element	//*[@id="ipmi_monit_enabled"]
	Run Keyword If	${IMPI_Exist}	IMPI Monit

	#Test SNMP Monitoring
	${SNMP_Exist}=	Run Keyword And Return Status	Page Should Contain Element	//*[@id="snmp_monit_enabled"]
	Run Keyword If	${SNMP_Exist}	SNMP Monit

	#Test USB Monitoring
	${USB_Exist}=	Run Keyword And Return Status	Page Should Contain Element	//*[@id="usb_sensor_monit_enabled"]
	Run Keyword If	${USB_Exist}	USB

Test Discovery
	${Disc_Exist}=	Run Keyword And Return Status	Page Should Contain Element	//*[@id="discport_title"]
	Run Keyword If	${Disc_Exist}	Disc

Test Discovery Outlets
	${Outlet_Exist}=	Run Keyword And Return Status	Page Should Contain Element	//*[@id="outletdiscovery"]
	Run Keyword If	${Outlet_Exist}	Disc Outlet

Test Scripts
	Page Should Contain Element	//*[@id="script_title"]
	FOR	${I}	IN RANGE	1	5
		Page Should Contain Element	//*[@id="spmmgmt_form"]/div[2]/div[2]/div/div/div[2]/div/div/div[${I}]
	END
USB
	Select Checkbox	//*[@id="usb_sensor_monit_enabled"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="usb_sensor_monit_template"]
	Page Should Contain Element	//*[@id="usb_sensor_monit_interval"]
	Page Should Contain Element	//*[@id="usb_sensor_temp_title"]
	Page Should Contain Element	//*[@id="usb_sensor_hum_title"]

SNMP Monit
	Select Checkbox	//*[@id="snmp_monit_enabled"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="snmp_monit_template"]
	Page Should Contain Element	//*[@id="snmp_monit_interval"]

IMPI Monit
	Select Checkbox	//*[@id="ipmi_monit_enabled"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="ipmi_monit_template"]
	Page Should Contain Element	//*[@id="ipmi_monit_interval"]

Nominal Monit
	Select Checkbox	//*[@id="nominal_monit_enabled"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="nominal_monit_name"]
	Page Should Contain Element	//*[@id="nominal_monit_type"]
	Page Should Contain Element	//*[@id="nominal_monit_value"]
	Page Should Contain Element	//*[@id="nominal_monit_interval"]

Disc
	Select Checkbox	//*[@id="portdiscovery"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="portdiscoveryinterval"]
	Page Should Contain Element	//*[@id="portdiscoverynaming"]

Disc Outlet
	Select Checkbox	//*[@id="outletdiscovery"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="outletdiscoveryinterval"]

SSH Proto
	Select Checkbox	//*[@id="ssh_proto_enabled"]
	Page Should Contain Element	//*[@id="sameasaccess"]

IMPI Proto
	Select Checkbox	//*[@id="ipmi_proto_enabled"]
	Page Should Contain Element	//*[@id="sameasaccessipmi"]

SNMP Proto
	Select Checkbox	//*[@id="snmp_proto_enabled"]
	Page Should Contain Element	//*[@id="snmpVersion"]