*** Settings ***
Documentation	Automated test for Ip alias
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${DELAY}	15s
${Device_name}	nsr
${alias_ip}	192.168.2.246
${HOMEPAGE_alias}	https://192.168.2.246
${root_login}	shell sudo su -
${getDevice_ip}	ifconfig eth0

*** Test Cases ***
Test case to add a device
	SUITE:addDevice
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Teardown

Test case to provide alias ip to managed device
	SUITE:addDevice
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Alias
	SUITE:Teardown

Test case to login to alias
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:addDevice
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Alias
	GUI::Basic::Open Nodegrid	${HOMEPAGE_alias}	${BROWSER}	${NGVERSION}
	Input Text	username	${USERNAME}
	Input Text	password	${PASSWORD}
	Click Element	id=login-btn
	sleep	${DELAY}
	Switch Window	title=alias
	Wait Until Page Contains Element	//iframe[@id='termwindow']
	Select Frame	//iframe[@id='termwindow']
	Sleep	5s
	Press Keys	//body	RETURN
	Press Keys	//body	RETURN
	${OUTPUT}=	GUI:Access::Generic Console Command Output	${DEFAULT_USERNAME}
	${OUTPUT}=	GUI:Access::Generic Console Command Output	${DEFAULT_PASSWORD}

	${OUTPUT}=	SUITE:Console Command Output	${root_login}
	${OUTPUT}=	SUITE:Console Command Output	${getDevice_ip}
	Should Contain	${OUTPUT}	inet addr:${HOSTPEER}
	Unselect Frame
	Press Keys	//body 	cw

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Managed Devices::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:addDevice
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_name"]
	Input Text	//*[@id="spm_name"]	${Device_name}
	Click Element	//*[@id="type"]
	Select From List By Label	//*[@id="type"]	device_console
	Click Element	//*[@id="phys_addr"]
	Input Text	//*[@id="phys_addr"]	${HOSTPEER}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Alias
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="nsr"]/td[2]/a
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id='inbipEnable']
	Click Element	//*[@id="inbipAddr"]
	Clear Element Text	//*[@id="inbipAddr"]
	Input Text	//*[@id="inbipAddr"]	${alias_ip}
	GUI::Basic::Save
	sleep	${DELAY}

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${Device_name}
	GUI::Basic::Logout And Close Nodegrid

SUITE:Console Command Output
	[Arguments]	${COMMAND}
	[Documentation]	Input the given command into the ttyd terminal and retrieve the output after prompt lines
	...	== ARGUMENTS ==
	...	-	COMMAND = Command to be executed
	...	== EXPECTED RESULT ==
	...	Input the command into the ttyd terminal and returns the OUTPUT between the command and the last prompt

	${CHECK_TAB}=	Run Keyword And Return Status	Should Not Contain	${COMMAND}	TAB
	Run Keyword If	${CHECK_TAB}	Press Keys  //body	${COMMAND}	RETURN
	...	ELSE	Press Keys  //body	${COMMAND}
	Sleep	1

	${SELECTION}=	Execute JavaScript	term.selectAll(); return term.getSelection().trim();
	Should Not Contain  ${SELECTION}	[error.connection.failure] Could not establish a connection to device

	${LINES}=	Split String	${SELECTION}	\n
	${INDEXES_OCCURRED}=	Create List
	${LENGTH}=	Get Length	${LINES}
	FOR	${INDEX}	IN RANGE	0	${LENGTH}
		${LINE}=	Get From List	${LINES}	${INDEX}
		${CHECK}=	Run Keyword And Return Status	Should Contain	${LINE}	@
		Run Keyword If	${CHECK}	Append to List	${INDEXES_OCCURRED}	${INDEX}
	END
	${STRING}=	Set Variable
	${END}=	Get From List	${INDEXES_OCCURRED}	-1
	${PREVIOUS}=	Get From List	${INDEXES_OCCURRED}	-2
	FOR	${INDEX}	IN RANGE	${PREVIOUS}	${END}
		${LINE}=	Get From List	${LINES}	${INDEX}
		${STRING}=	Set Variable	${STRING}\n${LINE}
	END
	${STRING}=	Get Substring	${STRING}	1
	[Return]	${STRING}

GUI:Access::Generic Console Command Output
	[Arguments]	${COMMAND}	${CONTAINS_HELP}=no
	[Documentation]	Input the given command into the ttyd terminal and retrieve the output after prompt lines
	...	== REQUIREMENTS ==
	...	The console type needs to be one of those:
	...	-	CLI -> ends with "]#"
	...	-	Shell -> ends with "$"
	...	-	Root -> ends with "#"
	...	== ARGUMENTS ==
	...	-	COMMAND = Command to be executed
	...	-	CONTAINS_HELP = [yes/no] Console contains "[Enter '^Ec?' for help]" message
	${CHECK_TAB}=	Run Keyword And Return Status	Should Not Contain  ${COMMAND}  TAB
	Run Keyword If  ${CHECK_TAB}	Press Keys  //body	${COMMAND}  RETURN
	...  ELSE	Press Keys	//body	${COMMAND}
	Sleep	1
