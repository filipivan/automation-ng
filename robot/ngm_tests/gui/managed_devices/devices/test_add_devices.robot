*** Settings ***
Documentation	Testing Adding a device from managed devices Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

*** Variables ***
${DEVICE}	test-dev.ice
${DEVICE_CLONE}	test-dev.clone
${EXPIRED_LICENSE}	${FIFTY_DEVICES_ACCESS_LICENSE_EXPIRED}

*** Test Cases ***
Test add Devices
	#GUI::ManagedDevices::Delete Devices If Exists	${DUMMY_DEVICE_CONSOLE_NAME}	${DUMMY_DEVICE_CONSOLE_NAME}2
	GUI::ManagedDevices::Delete All Devices
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Table Should Not Have Row With Id	SPMTable	${DUMMY_DEVICE_CONSOLE_NAME}
	GUI::Basic::Table Should Not Have Row With Id	SPMTable	${DUMMY_DEVICE_CONSOLE_NAME}2
	GUI::ManagedDevices::Add Dummy Device Console	${DUMMY_DEVICE_CONSOLE_NAME}	device_console	127.0.0.1	22	root	no	${ROOT_PASSWORD}	ondemand
	GUI::ManagedDevices::Add Dummy Device Console	${DUMMY_DEVICE_CONSOLE_NAME}2	device_console	127.0.0.2	22	root	no	${ROOT_PASSWORD}	disabled
	GUI::Basic::Table Should Has Row With Id		SPMTable	${DUMMY_DEVICE_CONSOLE_NAME}
	GUI::Basic::Table Should Has Row With Id		SPMTable	${DUMMY_DEVICE_CONSOLE_NAME}2

Test delete Devices
	GUI::ManagedDevices::Delete Devices	${DUMMY_DEVICE_CONSOLE_NAME}	${DUMMY_DEVICE_CONSOLE_NAME}2
	GUI::Basic::Table Should Not Have Row With Id	SPMTable	${DUMMY_DEVICE_CONSOLE_NAME}
	GUI::Basic::Table Should Not Have Row With Id	SPMTable	${DUMMY_DEVICE_CONSOLE_NAME}2

Test add device with exceeded licenses
	[Setup]	GUI::ManagedDevices::Delete All Devices
	GUI::ManagedDevices::Add Device	${DEVICE}

	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Select Rows In Table Containing Value	SPMTable	${DEVICE}
	GUI::Basic::Clone
	Input Text	spm_name	${DEVICE_CLONE}
	Input Text	num_clones	${${NUM_LICENSES} - 1}
	GUI::Basic::Save

	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Add
	Input Text	jquery=#spm_name	${DEVICE}-new
	GUI::Basic::Save
	Page Should Contain	No license available
	GUI::Basic::Cancel

Test add device with expiring license
	[Setup]	GUI::ManagedDevices::Delete All Devices
	GUI::ManagedDevices::Add Device	${DEVICE}
	GUI::System::License::Add License	${EXPIRED_LICENSE}

	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Select Rows In Table Containing Value	SPMTable	${DEVICE}
	GUI::Basic::Clone
	Input Text	spm_name	${DEVICE_CLONE}
	Input Text	num_clones	${${NUM_LICENSES} - 1}
	GUI::Basic::Save

	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Add
	Input Text	jquery=#spm_name	${DEVICE}-new
	GUI::Basic::Save
	Page Should Contain	No license available
	GUI::Basic::Cancel


*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::System::License::Delete All Licenses
	GUI::ManagedDevices::Delete All Devices

	CLI:Open
	${NUM_LICENSES}=	CLI:Get Number Of Available Access License Keys Installed
	CLI:Close Connection
	Set Suite Variable	${NUM_LICENSES}

SUITE:Teardown
	GUI::ManagedDevices::Delete All Devices
	GUI::System::License::Delete All Licenses
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
