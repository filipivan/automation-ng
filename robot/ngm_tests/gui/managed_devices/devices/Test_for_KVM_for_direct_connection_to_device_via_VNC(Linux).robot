*** Settings ***
Documentation	Test for KVM direct connection to device via RDP(Windows)
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup		SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags		PART-2		GUI		${BROWSER}		${SYSTEM}		${MODEL}	${VERSION}		CHROME		FIREFOX
Default Tags	EXCLUDEIN3_2

*** Variables ***
${DEVICE_NAME}				linux-VNC
${DEVICE_TYPE}				device_console
${LINUX_IP}					192.168.2.222
${LINUX_USER}				QAusers
${LINUX_PASSWORD}			QAusers!
${DEVICE_SATUS}				On-demand
${DEVICE_COMMAND}			KVM
${COMMAND_PROTOCOL}			VNC
${COMMAND_TYPE_EXTENSION}	vnc.py

*** Test Cases ***
Test case to add a VNC(Linux) configuration
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text			//*[@id='spm_name']		${DEVICE_NAME}
	Select From List By Label		//*[@id="type"]			${DEVICE_TYPE}
	Input Text						id=phys_addr			${LINUX_IP}
	Input Text						id=username				${LINUX_USER}
	Input Text						id=passwordfirst		${LINUX_PASSWORD}
	Input Text						id=passwordconf			${LINUX_PASSWORD}
	Select From List By Label		id=status				${DEVICE_SATUS}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Table Should Has Row With Id		SPMTable		${DEVICE_NAME}
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain				${DEVICE_NAME}

Test case to check added VNC(Linux) is able to access On KVM Linux
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element						//a[@id='${DEVICE_NAME}']
	GUI::Basic::Spinner Should Be Invisible
	Click Element						css=#spmcommands_nav > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label			id=command			${DEVICE_COMMAND}
	Select From List By Label			id=protocol			${COMMAND_PROTOCOL}
	Select From List By Label			id=pymod_file		${COMMAND_TYPE_EXTENSION}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain					${DEVICE_NAME}
	Wait Until Page Contains Element	xpath=//a[contains(text(),'KVM')]

Test case to check KVM Linux
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait until Page Contains Element	xpath=//a[contains(text(),'KVM')]
	Click Element		xpath=//a[contains(text(),'KVM')]
	Sleep		5s
	Run Keyword If		'${NGVERSION}'<='5.4'		Switch Window		${DEVICE_NAME}
	Run Keyword If		'${NGVERSION}'>'5.4'		Switch Window		${DEVICE_NAME} - KVM
	Sleep		10s
	Alert Should Not Be Present
	Close All Browsers

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Delete device

SUITE:Teardown
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Delete device
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:Delete device
	GUI::ManagedDevices::Open Devices tab
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE_NAME}