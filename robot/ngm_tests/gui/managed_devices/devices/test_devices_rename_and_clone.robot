*** Settings ***
Documentation	Testing rename and clone on	managed devices -> devices for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

*** Variables ***
${DEVICE}	test-dev.to-clone
${DEVICE_CLONE}	test-dev.clone
${EXPIRED_LICENSE}	${FIFTY_DEVICES_ACCESS_LICENSE_EXPIRED}

*** Test Cases ***
Test rename device
	Wait Until Keyword Succeeds	30	2	GUI::ManagedDevices::Add Dummy Device Console	Dummy	device_console	${EMPTY}	${EMPTY}	${EMPTY}	no	${EMPTY}	ondemand
	GUI::Basic::Table Should Has Row With Id	SPMTable	Dummy
	Click Element	//*[@id="Dummy"]/td[1]/input
	Click Element	//*[@id="nonAccessControls"]/input[4]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="spm_name"]	RenamedDummy
	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Table Should Has Row With Id	SPMTable	RenamedDummy
	SUITE:Teardown

Test clone device
	SUITE:Setup
	Wait Until Keyword Succeeds	30	2	GUI::ManagedDevices::Add Dummy Device Console	Dummy	device_console	${EMPTY}	${EMPTY}	${EMPTY}	no	${EMPTY}	ondemand
	Click Element	//*[@id="Dummy"]/td[1]/input
	Click Element	//*[@id="nonAccessControls"]/input[5]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="spm_name"]	Clone
	Input Text	//*[@id="num_clones"]	2
	Input Text	//*[@id="phys_addr"]	1.1.1.1
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Table Should Has Row With Id	SPMTable	Clone1
	GUI::Basic::Table Should Has Row With Id	SPMTable	Clone2
	SUITE:Teardown

Delete Devices
	SUITE:Setup
	GUI::ManagedDevices::Delete Devices	RenamedDummy	Dummy	Clone1	Clone2

Test clone device with exceeded licenses
	[Setup]	GUI::ManagedDevices::Delete All Devices
	GUI::ManagedDevices::Add Device	${DEVICE}

	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Select Rows In Table Containing Value	SPMTable	${DEVICE}
	GUI::Basic::Clone
	Input Text	spm_name	${DEVICE_CLONE}
	Input Text	num_clones	${NUM_LICENSES}
	GUI::Basic::Save
	Page Should Contain	No license available
	GUI::Basic::Cancel

Test clone device with expiring license
	[Setup]	GUI::ManagedDevices::Delete All Devices
	GUI::ManagedDevices::Add Device	${DEVICE}
	GUI::System::License::Add License	${EXPIRED_LICENSE}

	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Select Rows In Table Containing Value	SPMTable	${DEVICE}
	GUI::Basic::Clone
	Input Text	spm_name	${DEVICE_CLONE}
	Input Text	num_clones	${NUM_LICENSES}
	GUI::Basic::Save
	Page Should Contain	No license available
	GUI::Basic::Cancel
	[Teardown]	GUI::System::License::Delete All Licenses

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::System::License::Delete All Licenses
	GUI::ManagedDevices::Delete All Devices

	CLI:Open
	${NUM_LICENSES}=	CLI:Get Number Of Available Access License Keys Installed
	CLI:Close Connection
	Set Suite Variable	${NUM_LICENSES}

SUITE:Teardown
	GUI::ManagedDevices::Delete All Devices
	GUI::System::License::Delete All Licenses
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers
