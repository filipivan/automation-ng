*** Settings ***
Documentation	Test for Ilo5
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Variables ***
${Name}     ilo5
${Type}     ${ILO_TYPE}
${Ip_address}       ${ILO5_IP}
${Usr}     ${ILO5_USERNAME}
${Passwd}     ${ILO5_PASSWORD}
${Confm_passwd}     ${ILO5_PASSWORD}
${Status}     On-demand
${cmd}      KVM
${type_extn}        ilo.py

*** Test Cases ***
Test case to add a idrac8 configuration
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists	${Name}
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text	    //*[@id='spm_name']	    ${Name}
	Select From List By Label   //*[@id="type"]    ${Type}
	Input Text      id=phys_addr    ${Ip_address}
	Input Text      id=username          ${Usr}
    Input Text      id=passwordfirst     ${Passwd}
    Input Text      id=passwordconf      ${Confm_passwd}
    Select From List By Label    id=status    ${Status}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Table Should Has Row With Id        SPMTable    ${Name}
    GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain    ${Name}

Test case to check added Idrac8 is able to access On KVM Window
    SUITE:Setup
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    Select Checkbox    //*[@id="ilo5"]/td[1]/input
    Click Element       xpath=//a[contains(text(),'ilo5')]
    Sleep       5s
    Click Element       css=#spmcommands_nav > .title_b
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
    Select From List By Label       id=command      ${cmd}
    Select From List By Label       id=pymod_file      ${type_extn}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain    ${Name}
    Wait Until Page Contains Element     xpath=//a[contains(text(),'KVM')]

Test case to check KVM window
	[Tags]	NON-CRITICAL	NEED-REVIEW
    SUITE:Setup
    GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    Wait until Page Contains Element        xpath=//a[contains(text(),'KVM')]
    Click Element       xpath=//a[contains(text(),'KVM')]
#    ilo Device  was not able accessible
#    Sleep       15s
#    Run Keyword If  '${NGVERSION}'<='5.4'        Switch Window       ilo5
#    Run Keyword If  '${NGVERSION}'>'5.4'       Switch Window        ilo5 - KVM
    Close All Browsers

Test case to delete added Configuration on managed device
	[Tags]	NON-CRITICAL
    SUITE:Setup
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible

    Click Element       //*[@id="ilo5"]/td[1]/input
    Click Element       //*[@id='delButton']
    Handle Alert
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	Close All Browsers