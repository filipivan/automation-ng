*** Settings ***
Documentation	Edit multiple device with ipv4 & ipv6
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Variables ***
${Name_1}       test_1
${Name_2}       test_2
${Name_3}       test_3
${Name}     test
${Type}     device_console
${Ip_address_1}     10.0.0.1
${Ip_address_2}     10.0.0.2
${Ip_address_3}     10.0.0.3
${Ip_address_4}     10.0.0.4
${Ip_address_5}     10.0.0.5
${ipv4}     10.20.30.40
${ipv6}    fe80::1
${Interface}        eth0
${Action}       console


*** Test Cases ***
Test case to add managed device
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists	${Name}
    GUI::ManagedDevices::Delete Device If Exists	${Name_1}
    GUI::ManagedDevices::Delete Device If Exists	${Name_2}
    GUI::ManagedDevices::Delete Device If Exists	${Name_3}
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Input Text	    //*[@id='spm_name']	    ${Name_1}
    Select From List By Label   //*[@id="type"]    ${Type}
    Input Text    id=phys_addr    ${Ip_address_1}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Table Should Has Row With Id        SPMTable    ${Name_1}
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Input Text	    //*[@id='spm_name']	    ${Name_2}
    Select From List By Label   //*[@id="type"]    ${Type}
    Input Text    id=phys_addr    ${Ip_address_2}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Table Should Has Row With Id        SPMTable    ${Name_2}
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Input Text	    //*[@id='spm_name']	    ${Name_3}
    Select From List By Label   //*[@id="type"]    ${Type}
    Input Text    id=phys_addr    ${Ip_address_3}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Table Should Has Row With Id        SPMTable    ${Name_3}
    GUI::Basic::Spinner Should Be Invisible

Test case to edit managed device for ipv4
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    Run Keyword If  '${NGVERSION}'=='4.2'       Input Text      //*[@id="search_expr1"]    ${Name}
    Run Keyword If  '${NGVERSION}'>'4.2'       Input Text      //*[@id="filter_field"]     ${Name}
    GUI::Basic::Spinner Should Be Invisible
    Run Keyword If  '${NGVERSION}'=='4.2'       Press Keys    //*[@id="search_expr1"]    ENTER
    Run Keyword If  '${NGVERSION}'>'4.2'       Press Keys    //*[@id="filter_field"]    ENTER
    GUI::Basic::Spinner Should Be Invisible
    Sleep       5s
    Click Element       //*[@id="thead"]/tr/th[1]/input
    Click Element       //*[@id="editButton"]
    GUI::Basic::Spinner Should Be Invisible
    Sleep       5s
    wait until page contains element        //*[@id="inbipEnable"]
    click element       //*[@id="inbipEnable"]
    Input Text      //*[@id="inbipAddr"]        ${ipv4}
    Select From List By Label       //*[@id="inbInterf"]        ${Interface}
    Run Keyword If  '${NGVERSION}'>'4.2'        wait until page contains element        //*[@id="inbAction"]
    Run Keyword If  '${NGVERSION}'>'4.2'        Select From List By Label       //*[@id="inbAction"]        ${Action}
    Sleep       3s
    wait until page contains element        //*[@id="saveButton"]
    click element       //*[@id="saveButton"]
    GUI::Basic::Spinner Should Be Invisible

Test case to Validate the edited managed device for ipv4
    SUITE:Validate Managed device
    Click Element       xpath=//a[contains(text(),'test_1')]
    Sleep       5s
    page should contain textfield       //*[@id="inbipAddr"]        ${ipv4}
    page should contain checkbox        //*[@id="inbipEnable"]
    SUITE:Validate Managed device
    Click Element       xpath=//a[contains(text(),'test_2')]
    Sleep       5s
    page should contain textfield       //*[@id="inbipAddr"]        ${ipv4}
    page should contain checkbox        //*[@id="inbipEnable"]
    SUITE:Validate Managed device
    Click Element       xpath=//a[contains(text(),'test_3')]
    Sleep       5s
    page should contain textfield       //*[@id="inbipAddr"]        ${ipv4}
    page should contain checkbox        //*[@id="inbipEnable"]

Test case to edit managed device for ipv6
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    Run Keyword If  '${NGVERSION}'=='4.2'       Input Text      //*[@id="search_expr1"]    ${Name}
    Run Keyword If  '${NGVERSION}'>'4.2'       Input Text      //*[@id="filter_field"]     ${Name}
    GUI::Basic::Spinner Should Be Invisible
    Run Keyword If  '${NGVERSION}'=='4.2'       Press Keys    //*[@id="search_expr1"]    ENTER
    Run Keyword If  '${NGVERSION}'>'4.2'       Press Keys    //*[@id="filter_field"]    ENTER
    GUI::Basic::Spinner Should Be Invisible
    Sleep       5s
    Click Element       //*[@id="thead"]/tr/th[1]/input
    Click Element       //*[@id="editButton"]
    GUI::Basic::Spinner Should Be Invisible
    Sleep       5s
    Input Text      //*[@id="inbipAddr"]        ${ipv6}
    Select From List By Label       //*[@id="inbInterf"]        ${Interface}
    Run Keyword If  '${NGVERSION}'>'4.2'        wait until page contains element         //*[@id="inbAction"]
    Run Keyword If  '${NGVERSION}'>'4.2'        Select From List By Label       //*[@id="inbAction"]        ${Action}
    Sleep       3s
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible

Test case to Validate the edited managed device for ipv6
    SUITE:Validate Managed device
    Click Element       xpath=//a[contains(text(),'test_1')]
    Sleep       5s
    page should contain textfield       //*[@id="inbipAddr"]        ${ipv6}
    page should contain checkbox        //*[@id="inbipEnable"]
    SUITE:Validate Managed device
    Click Element       xpath=//a[contains(text(),'test_2')]
    Sleep       5s
    page should contain textfield       //*[@id="inbipAddr"]        ${ipv6}
    page should contain checkbox        //*[@id="inbipEnable"]
    SUITE:Validate Managed device
    Click Element       xpath=//a[contains(text(),'test_3')]
    Sleep       5s
    page should contain textfield       //*[@id="inbipAddr"]        ${ipv6}
    page should contain checkbox        //*[@id="inbipEnable"]

Test case to delete added managed devices
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    Run Keyword If  '${NGVERSION}'=='4.2'       Input Text      //*[@id="search_expr1"]    ${Name}
    Run Keyword If  '${NGVERSION}'>'4.2'       Input Text      //*[@id="filter_field"]     ${Name}
    GUI::Basic::Spinner Should Be Invisible
    Run Keyword If  '${NGVERSION}'=='4.2'       Press Keys    //*[@id="search_expr1"]    ENTER
    Run Keyword If  '${NGVERSION}'>'4.2'       Press Keys    //*[@id="filter_field"]    ENTER
    GUI::Basic::Spinner Should Be Invisible
    Sleep       5s
    Click Element       //*[@id="thead"]/tr/th[1]/input
    Click Element       //*[@id="delButton"]
    Handle Alert

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:Validate Managed device
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    Run Keyword If  '${NGVERSION}'=='4.2'       Input Text      //*[@id="search_expr1"]    ${Name}
    Run Keyword If  '${NGVERSION}'>'4.2'       Input Text      //*[@id="filter_field"]     ${Name}
    GUI::Basic::Spinner Should Be Invisible
    Run Keyword If  '${NGVERSION}'=='4.2'       Press Keys    //*[@id="search_expr1"]    ENTER
    Run Keyword If  '${NGVERSION}'>'4.2'       Press Keys    //*[@id="filter_field"]    ENTER
    GUI::Basic::Spinner Should Be Invisible
    Sleep       5s