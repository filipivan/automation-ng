*** Settings ***
Documentation	Test for Direct access managed device
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}

*** Variables ***
${Name}	test_access
${Ip_address}	${HOST}
${Type}	device_console
${Username}	${DEFAULT_USERNAME}
${Password}	${DEFAULT_PASSWORD}
${Confirm_Password}	${DEFAULT_PASSWORD}
${Url_Web}	https://${HOST}/direct/test_access/web
${Url_Console}	https://${HOST}/direct/test_access/console

*** Test Cases ***
Test case to add managed device
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${Name}
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text	//*[@id='spm_name']	${Name}
	Select From List By Label	//*[@id="type"]	${Type}
	Input Text	id=phys_addr	${Ip_address}
	Input Text	id=username	${Username}
	Input Text	id=passwordfirst	${Password}
	Input Text	id=passwordconf	${Confirm_Password}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Test to Access directly to web
	SUITE:Setup
	GUI::Basic::Open NodeGrid	${Url_Web}
	GUI::Basic::Direct Login	${Username}	 ${Password}
	Sleep	15s
	Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists	test_access - Web
	...	ELSE	GUI:Basic::Wait Until Window Exists	test_access
	Close All Browsers

Test case to access console
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Setup
	GUI::Basic::Open NodeGrid	${Url_Console}
	GUI::Basic::Direct Login	${Username}	 ${Password}
	Sleep	15s
	Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists	test_access - Console
	...	ELSE	GUI:Basic::Wait Until Window Exists	test_access
	Select Frame	xpath=//*[@id='termwindow']
	Sleep	15s
	Press Keys	//body	RETURN
	${OUTPUT}=	GUI:Access::Generic Console Command Output	ls
	Should Contain	${OUTPUT}	access/	system/	settings/

Test Case to delete managed device
	SUITE:Setup
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="test_access"]/td[1]/input
	Click Element	//*[@id="delButton"]
	Handle Alert


*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers