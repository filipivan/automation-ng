*** Settings ***
Documentation	Testing errors on  managed devices -> devices -> add for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

*** Test Cases ***
Test Name Errors
    Select From List By Value   //*[@id="type"]     console_server_nodegrid
    Input Text      //*[@id="inbpowerctlkey"]    *
    GUI::Basic::Auto Input Tests        spm_name    ${EMPTY}    **  Dummy

Test IP Address Errors
    GUI::Basic::Auto Input Tests        phys_addr   **      1234

Test TCP Port Errors
    GUI::Basic::Auto Input Tests        tcpport   Dummy    **  12345678901     1.1     65536   1000

Test Coordinates Errors
    GUI::Basic::Auto Input Tests        coordinates     **      MyHouse      1234    91,181      10,10

Test Mismatch Password Error
	[Tags]	NON-CRITICAL	NEED-REVIEW
    Select Radio Button     askpassword     no
    Input Text      //*[@id="passwordfirst"]    pass123
    Input Text      //*[@id="passwordconf"]     pass456
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    Element Should Contain      //*[@id="errormsg"]     Password mismatch.
    Select Radio Button     askpassword     yes

Test Break Sequence Error
	[Tags]	NON-CRITICAL	NEED-REVIEW
    Select Checkbox     //*[@id="enable-break"]
    Input Text          //*[@id="break-sequence"]  l
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    Element Should Contain      //*[@id="errormsg"]     This field must have at least two characteres.
    Unselect Checkbox     //*[@id="enable-break"]

Test Expiration Date Errors
    Select Radio Button     ttlType     date
    GUI::Basic::Auto Input Tests        ttlDate     **  Today   0000/01/00  0000/01/32  0000/00/01  0000/13/01  5/5/5/5     1000000000      2019/4/20

Test Expiration Days Errors
	[Tags]	NON-CRITICAL	NEED-REVIEW
    Select Radio Button     ttlType     days
    GUI::Basic::Auto Input Tests        ttlDays     Dummy   90-7890     **      500
    Select Radio Button     ttlType     never

Test Enable IP Alias Invalid IP
	[Tags]	NON-CRITICAL	NEED-REVIEW
    Select Checkbox     //*[@id="inbipEnable"]
    GUI::Basic::Auto Input Tests        inbipAddr   **      1234    1.1.1.1

Test Enable IP Alias Invalid Telenet Protocol Port
	[Tags]	NON-CRITICAL	NEED-REVIEW
    Select Checkbox     //*[@id="inbipTeln"]
    GUI::Basic::Auto Input Tests        inbipTelnPort   Dummy    **  12345678901     1.1   1000
    Unselect Checkbox     //*[@id="inbipTeln"]

Test Enable IP Alias Invalid Binary Socket Port
	[Tags]	NON-CRITICAL	NEED-REVIEW
    Select Checkbox     //*[@id="inbipRaw"]
    GUI::Basic::Auto Input Tests        inbipRawPort    Dummy    **  12345678901     1.1   1000
    Unselect Checkbox     //*[@id="inbipRaw"]

Test Enable Second IP Alias Invalid IP
	[Tags]	NON-CRITICAL	NEED-REVIEW
    Select Checkbox     //*[@id="inbSecipEnable"]
    GUI::Basic::Auto Input Tests        inbSecipAddr   **      1234    1.1.1.1

Test Enable Second IP Alias Invalid Telenet Protocol Port
	[Tags]	NON-CRITICAL	NEED-REVIEW
    Select Checkbox     //*[@id="inbSecipTeln"]
    GUI::Basic::Auto Input Tests        inbSecipTelnPort   Dummy    **  12345678901     1.1   1000

Test Enable Second IP Alias Invalid Binary Socket Port
	[Tags]	NON-CRITICAL	NEED-REVIEW
    Select Checkbox     //*[@id="inbSecipRaw"]
    GUI::Basic::Auto Input Tests        inbSecipRawPort    Dummy    **  12345678901     1.1   1000
    Unselect Checkbox     //*[@id="inbSecipEnable"]
    Unselect Checkbox     //*[@id="inbipEnable"]

Test Invalid SSH Port
	[Tags]	NON-CRITICAL	NEED-REVIEW
    Select Checkbox     //*[@id="inbssh"]
    GUI::Basic::Auto Input Tests        inbsshTCP   Dummy   2000    65001   12345678901     **      2500
    Unselect Checkbox   //*[@id="inbssh"]

Test Invalid Telnet Port
	[Tags]	NON-CRITICAL	NEED-REVIEW
    Select Checkbox     //*[@id="inbteln"]
    GUI::Basic::Auto Input Tests        inbtelnTCP   Dummy   2000    65001   12345678901     **      2500
    Unselect Checkbox   //*[@id="inbteln"]

Test Invalid Binary Socket Port
	[Tags]	NON-CRITICAL	NEED-REVIEW
    Select Checkbox     //*[@id="inbraw"]
    GUI::Basic::Auto Input Tests        inbrawTCP    Dummy   2000    65001   12345678901     **      2500
    Unselect Checkbox   //*[@id="inbraw"]

Test Escape Sequence Errors
	[Tags]	NON-CRITICAL	NEED-REVIEW
    GUI::Basic::Auto Input Tests        inbhotkey       Dummy   D   $$$     ^Ec
    Input Text      //*[@id="inbhotkey"]     *

Test Power Control Key Errors
	[Tags]	NON-CRITICAL	NEED-REVIEW
    GUI::Basic::Auto Input Tests        inbpowerctlkey       Dummy   D   $$$     ^O
    GUI::Basic::Cancel
    GUI::Basic::Spinner Should Be Invisible

Test Duplicate Device Error
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
    Input Text      //*[@id="spm_name"]     dupe1
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
    Input Text      //*[@id="spm_name"]     dupe1
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    Element Should Contain      //*[@id="errormsg"]     Entry already exists.
    GUI::Basic::Cancel
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists  dupe1

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
    GUI::ManagedDevices::Open
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
