*** Settings ***
Documentation	Editing Deviced from managed devices Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${DEVICE_1}	device01
${DEVICE_2}	device02
${BASE}	16

*** Test Cases ***
Multi edit should update changed fields only
	GUI::ManagedDevices::Add Dummy Device Console	${DUMMY_DEVICE_CONSOLE_NAME}	device_console	127.0.0.1	22	root	no	${ROOT_PASSWORD}	ondemand
	GUI::ManagedDevices::Add Dummy Device Console	${DUMMY_DEVICE_CONSOLE_NAME}2	device_console	127.0.0.2	22	root	no	${ROOT_PASSWORD}	disabled
	GUI::ManagedDevices::Select Checkboxes	${DUMMY_DEVICE_CONSOLE_NAME}	${DUMMY_DEVICE_CONSOLE_NAME}2
	GUI::Basic::Click Element	//*[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#multiedit_title
	GUI::Basic::Input Text	//*[@id='phys_addr']	127.0.0.3
	GUI::Basic::Click Element	//*[@id='saveButton']
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Wait For Managed Devices Table
	#test changed field
	${IP} =	Get Text	jquery=tr[id="${DUMMY_DEVICE_CONSOLE_NAME}"] > td:nth-child(3)
	Should Be Equal	${IP}	127.0.0.3
	${IP} =	Get Text	jquery=tr[id="${DUMMY_DEVICE_CONSOLE_NAME}2"] > td:nth-child(3)
	Should Be Equal	${IP}	127.0.0.3
	#test unchanged field
	${STATUS} =	Get Text	jquery=tr[id="${DUMMY_DEVICE_CONSOLE_NAME}"] > td:nth-child(5)
	Should Be Equal	${STATUS}	On-demand
	${STATUS} =	Get Text	jquery=tr[id="${DUMMY_DEVICE_CONSOLE_NAME}2"] > td:nth-child(5)
	Should Be Equal	${STATUS}	Disabled
	GUI::ManagedDevices::Delete All Devices

Test IP Alias for IPv6
	GUI::ManagedDevices::Add Device	${DEVICE_1}	device_console	127.0.0.1	${USERNAME}	no	${PASSWORD}	enabled
	GUI::ManagedDevices::Add Device	${DEVICE_2}	device_console	127.0.0.1	${USERNAME}	no	${PASSWORD}	enabled
	${DEVICES} =	Create List	${DEVICE_1}	${DEVICE_2}
	GUI::Basic::Managed Devices::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Select Table Rows	jquery=table\#SPMTable	${DEVICES}
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	inbipEnable
	Select Checkbox	inbipEnable
	Wait Until Page Contains Element	inbipAddr
	Input Text	inbipAddr	${IP_ALIAS_IPV6}
	GUI::Basic::Save
	Sleep	2
	GUI::ManagedDevices::Open Devices tab
	Click Link	${DEVICE_1}
	Wait Until Page Contains Element	inbipAddr
	${TEXT_1}=	Get Value	inbipAddr
	${TEXT_1}=	Fetch From Right	${TEXT_1}	:
	${TEXT_1}=	Convert To Integer	${TEXT_1}	${BASE}
	${TEXT_1}=	 Evaluate	 ${TEXT_1} + 1
	${TEXT_1}=	Convert To Hex	${TEXT_1}

	GUI::Basic::Managed Devices::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Link	${DEVICE_2}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	inbipAddr
	${TEXT_2}=	Get Value	inbipAddr
	${TEXT_2}=	Fetch From Right	${TEXT_2}	:

	${TEXT_1}=	Convert To Lowercase	${TEXT_1}
	${TEXT_2}=	Convert To Lowercase	${TEXT_2}
	Should Be Equal	 ${TEXT_2}	${TEXT_1}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE_1}
	sleep	5s
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE_2}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
