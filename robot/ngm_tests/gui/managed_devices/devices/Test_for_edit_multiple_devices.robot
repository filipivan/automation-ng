*** Settings ***
Documentation	Edit multiple device
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Variables ***
${Name_1}       test_1
${Name_2}       test_2
${Name_3}       test_3
${Name_4}       test_4
${Name_5}       test_5
${Name}     test
${Type}     device_console
${Ip_address_1}     10.0.0.1
${Ip_address_2}     10.0.0.2
${Ip_address_3}     10.0.0.3
${Ip_address_4}     10.0.0.4
${Ip_address_5}     10.0.0.5
${Edited_Ip_address}      20.0.0.4
${Status}     On-demand


*** Test Cases ***
Test case to add managed device
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists	${Name}
    GUI::ManagedDevices::Delete Device If Exists	${Name_1}
    GUI::ManagedDevices::Delete Device If Exists	${Name_2}
    GUI::ManagedDevices::Delete Device If Exists	${Name_3}
    GUI::ManagedDevices::Delete Device If Exists	${Name_4}
    GUI::ManagedDevices::Delete Device If Exists	${Name_5}
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Input Text	    //*[@id='spm_name']	    ${Name_1}
    Select From List By Label   //*[@id="type"]    ${Type}
    Input Text    id=phys_addr    ${Ip_address_1}
    wait until element is visible       //*[@id="saveButton"]
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    Sleep       5s
    GUI::Basic::Table Should Has Row With Id        SPMTable    ${Name_1}
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Add
    GUI::Basic::Input Text	    //*[@id='spm_name']	    ${Name_2}
    Select From List By Label   //*[@id="type"]    ${Type}
    Input Text    id=phys_addr    ${Ip_address_2}
    wait until element is visible       //*[@id="saveButton"]
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    Sleep       5s
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Table Should Has Row With Id        SPMTable    ${Name_2}
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Input Text	    //*[@id='spm_name']	    ${Name_3}
    Select From List By Label   //*[@id="type"]    ${Type}
    Input Text    id=phys_addr    ${Ip_address_3}
    wait until element is visible       //*[@id="saveButton"]
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    Sleep       5s
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Table Should Has Row With Id        SPMTable    ${Name_3}
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Input Text	    //*[@id='spm_name']	    ${Name_4}
    Select From List By Label   //*[@id="type"]    ${Type}
    Input Text    id=phys_addr    ${Ip_address_4}
    wait until element is visible       //*[@id="saveButton"]
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    Sleep       5s
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Table Should Has Row With Id        SPMTable    ${Name_4}
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Input Text	    //*[@id='spm_name']	    ${Name_5}
    Select From List By Label   //*[@id="type"]    ${Type}
    Input Text    id=phys_addr    ${Ip_address_5}
    wait until element is visible       //*[@id="saveButton"]
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    Sleep       5s
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Table Should Has Row With Id        SPMTable    ${Name_5}
    GUI::Basic::Spinner Should Be Invisible

Test case to edit managed device
    [Tags]    NON-CRITICAL    NEED-REVIEW
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    Run Keyword If  '${NGVERSION}'=='4.2'       Input Text      //*[@id="search_expr1"]    ${Name}
    Run Keyword If  '${NGVERSION}'>'4.2'       Input Text      //*[@id="filter_field"]     ${Name}
    GUI::Basic::Spinner Should Be Invisible
    Run Keyword If  '${NGVERSION}'=='4.2'       Press Keys    //*[@id="search_expr1"]    ENTER
    Run Keyword If  '${NGVERSION}'>'4.2'       Press Keys    //*[@id="filter_field"]    ENTER
    GUI::Basic::Spinner Should Be Invisible
    Sleep       2s
    Click Element       //*[@id="thead"]/tr/th[1]/input
    Click Element       //*[@id="editButton"]
    GUI::Basic::Spinner Should Be Invisible
    Sleep       5s
    Input Text      id=phys_addr        ${Edited_Ip_address}
    Select From List By Label    id=status    ${Status}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible

Test case to Validate the edited managed device
    SUITE:Validate Managed device
    Click Element       xpath=//a[contains(text(),'test_1')]
    Sleep       5s
    page should contain list        id=status           ${Status}
    SUITE:Validate Managed device
    Click Element       xpath=//a[contains(text(),'test_2')]
    Sleep       5s
    page should contain list        id=status           ${Status}
    SUITE:Validate Managed device
    Click Element       xpath=//a[contains(text(),'test_3')]
    Sleep       5s
    page should contain list        id=status           ${Status}
    SUITE:Validate Managed device
    Click Element       xpath=//a[contains(text(),'test_4')]
    Sleep       5s
    page should contain list        id=status           ${Status}
    SUITE:Validate Managed device
    Click Element       xpath=//a[contains(text(),'test_5')]
    Sleep       5s
    page should contain list        id=status           ${Status}

Test case to delete added managed devices
    SUITE:Setup
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    Run Keyword If  '${NGVERSION}'=='4.2'       Input Text      //*[@id="search_expr1"]    ${Name}
    Run Keyword If  '${NGVERSION}'>'4.2'       Input Text      //*[@id="filter_field"]     ${Name}
    GUI::Basic::Spinner Should Be Invisible
    Run Keyword If  '${NGVERSION}'=='4.2'       Press Keys    //*[@id="search_expr1"]    ENTER
    Run Keyword If  '${NGVERSION}'>'4.2'       Press Keys    //*[@id="filter_field"]    ENTER
    GUI::Basic::Spinner Should Be Invisible
    Sleep       5s
    Click Element       //*[@id="thead"]/tr/th[1]/input
    click element       //*[@id="delButton"]
    Handle Alert

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:Validate Managed device
	SUITE:Setup
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    Run Keyword If  '${NGVERSION}'=='4.2'       Input Text      //*[@id="search_expr1"]    ${Name}
    Run Keyword If  '${NGVERSION}'>'4.2'       Input Text      //*[@id="filter_field"]     ${Name}
    GUI::Basic::Spinner Should Be Invisible
    Run Keyword If  '${NGVERSION}'=='4.2'       Press Keys    //*[@id="search_expr1"]    ENTER
    Run Keyword If  '${NGVERSION}'>'4.2'       Press Keys    //*[@id="filter_field"]    ENTER
    GUI::Basic::Spinner Should Be Invisible
    Sleep       5s