*** Settings ***
Documentation	Verifies field name visibility on drilldown and edit mode
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}

*** Test Cases ***

Verify field Name visibility on device_console in drilldown mode
	GUI::ManagedDevices::Delete Devices	${DUMMY_DEVICE_CONSOLE_NAME}	${DUMMY_DEVICE_CONSOLE_NAME}2
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text	    //*[@id='spm_name']	    ${DUMMY_DEVICE_CONSOLE_NAME}
	Select From List By Value	jquery=#type	        device_console
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep       5s
	GUI::ManagedDevices::Wait For Managed Devices Table
	GUI::Basic::Spinner Should Be Invisible
	wait until page contains element        ${DUMMY_DEVICE_CONSOLE_NAME}
	GUI::ManagedDevices::Enter Device	${DUMMY_DEVICE_CONSOLE_NAME}
	Wait Until Element Is Visible	    jquery=#spm_name

Verify field Name visibility on device_console in edit mode
	GUI::Basic::Managed Devices::Devices::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Select Checkbox	    //*/tr[@id='${DUMMY_DEVICE_CONSOLE_NAME}']/*/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#spm_name
	Page Should Contain Element 	jquery=#spm_name

Delete dummy_device_console
	GUI::ManagedDevices::Delete Devices	${DUMMY_DEVICE_CONSOLE_NAME}

Verify fields Name and Local Serial Port visibility on local_serial in drilldown mode
	${ret}=	GUI::Basic::Is Serial Console
	Run Keyword If	'${ret}' == '1'
		...	Run Keywords
		...	GUI::Basic::Click Element	//*/a[@id='ttyS1']
		...	AND	GUI::Basic::Wait Until Elements Are Visible	jquery=#spm_name	jquery=#tty
		...	ELSE	Log To Console	Disabled on NodeGrid Manager

Verify fields Name and Local Serial Port visibility on local_serial in edit mode
	${ret}=	GUI::Basic::Is Serial Console
	Run KEYWORD IF	'${ret}' == '1'
		...	RUN KEYWORDS
		...	GUI::Basic::Click Element	//*[@id='returnButton']
		...	AND	GUI::Basic::Wait Until Element Is Accessible	jquery=a#ttyS1
		...	AND	GUI::Basic::Select Checkbox	//*/tr[@id='ttyS1']/*/input
		...	AND	GUI::Basic::Edit
		...	AND	Wait Until Element Is Visible	jquery=#type
		...	AND	Page Should Contain Element	input[@id='spm_name']	0
		...	AND	Page Should Contain Element	input[@id='tty']	0
		...	ELSE	Log To Console	Disabled on NodeGrid Manager

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
