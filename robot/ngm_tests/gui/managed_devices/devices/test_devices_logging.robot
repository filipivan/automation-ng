*** Settings ***
Documentation	Testing Logging Tab on  managed devices -> devices for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Test Logging Tab Elements
    GUI::ManagedDevices::Add Device     ILO     ilo     ${EMPTY}    ${EMPTY}    no  ${EMPTY}    disabled
    Click Element       //*[@id="ILO"]/td[2]/a
    GUI::Basic::Spinner Should Be Invisible
    Click Element       //*[@id="spmextended_nav"]
    GUI::Basic::Spinner Should Be Invisible

Test Data Logging
    Select Checkbox     //*[@id="databuf"]
    Select Checkbox     //*[@id="alertenable"]
    FOR    ${I}    IN RANGE    1    11
		Element Should Be Visible     //*[@id="SPMFormExt"]/div[3]/div[1]/div/div/div[2]/div/div[2]/div[${I}]
	END

Test Event Logging
    Select Checkbox         //*[@id="eventlog"]
    Select Checkbox         //*[@id="eventalarm"]
    Element Should Be Visible     //*[@id="eventintval"]
    Element Should Be Visible     //*[@id="eventintunit"]
    FOR    ${I}    IN RANGE    1    11
		Element Should Be Visible     //*[@id="SPMFormExt"]/div[3]/div[2]/div/div/div[2]/div[1]/div[2]/div[${I}]
	END

Delete Device
    GUI::Basic::Return
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Devices     ILO
    GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Open
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
