*** Settings ***
Documentation	Test for Super Micro
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

*** Variables ***
${Name}	super_micro
${Type}	ipmi_2.0
${Ip_address}	192.168.2.236
${Usr}	ADMIN
${Passwd}	.Sup3rM1cr0!
${Confm_passwd}	.Sup3rM1cr0!
${Status}	On-demand
${cmd}	KVM
${type_extn}	supermicro.py

*** Test Cases ***
Test case to add a super_micro configuration
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${Name}
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text	//*[@id='spm_name']	${Name}
	Select From List By Label	//*[@id="type"]	${Type}
	Input Text	id=phys_addr	${Ip_address}
	Input Text	id=username		${Usr}
	Input Text	id=passwordfirst	${Passwd}
	Input Text	id=passwordconf	${Confm_passwd}
	Select From List By Label	id=status	${Status}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Table Should Has Row With Id	SPMTable	${Name}
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${Name}

Test case to check added super_micro is able to access On KVM Window
	SUITE:Setup
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="super_micro"]/td[1]/input
	Click Element	xpath=//a[contains(text(),'super_micro')]
	Sleep	5s
	Click Element	css=#spmcommands_nav > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	id=command	${cmd}
	Select From List By Label	id=pymod_file	${type_extn}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${Name}
	Wait Until Page Contains Element	xpath=//a[contains(text(),'KVM')]

Test case to check KVM window
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Setup
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait until Page Contains Element	xpath=//a[contains(text(),'KVM')]
	Click Element	xpath=//a[contains(text(),'KVM')]
	Sleep	15s
	Run Keyword If	'${NGVERSION}'<='5.4'	Switch Window	super_micro
	Run Keyword If	'${NGVERSION}'>'5.4'	Switch Window	super_micro - KVM
	Close All Browsers

Test case to delete added Configuration on managed device
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Setup
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible

	Click Element	//*[@id="super_micro"]/td[1]/input
	Click Element	//*[@id='delButton']
	Handle Alert
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers