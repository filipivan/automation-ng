*** Settings ***
Documentation	Test PDU outlets commands ON/OFF/Cicle/Status. 
...             All the devices are created and exported on zpe_pdus.conf file in FTP.
...             Devices made: IDRAC | IDRAC9 | RARITAN METERED | SERVERTECH | RARITAN PX3 | PM3000 | MPH2 |
...             ENCONEEX | CPI METERED | TRIPPLITE ATS | EATON | CYBERPOWER | APC
...             Raming devices: Digital Loggers (loaner from SE)| BAYTEC (we don't have)| ICE (we don't have)| 
...             RITTAL (we don't have) | 
  
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW

*** Test Cases ***
Test Outlets on PDU_APC
    ${RESULT}=	Run Keyword And Return Status	Dictionary Should Contain Item  ${PDU_APC}  execute  True
    Run Keyword If	${RESULT}  SUITE:Test Outlet in ${PDU_APC}[name]

Test Outlets on PDU_GEIST
    ${RESULT}=	Run Keyword And Return Status	Dictionary Should Contain Item  ${PDU_GEIST}  execute  True
    Run Keyword If	${RESULT}  SUITE:Test Outlet in ${PDU_GEIST}[name]

Test Outlets on PDU_EATON
    ${RESULT}=	Run Keyword And Return Status	Dictionary Should Contain Item  ${PDU_EATON}  execute  True
    Run Keyword If	${RESULT}  SUITE:Test Outlet in ${PDU_EATON}[name]

Test Outlets on PDU_CYBERPOWER
    ${RESULT}=	Run Keyword And Return Status	Dictionary Should Contain Item  ${PDU_CYBERPOWER}  execute  True
    Run Keyword If	${RESULT}  SUITE:Test Outlet in ${PDU_CYBERPOWER}[name]

Test Outlets on PDU_IDRAC
    ${RESULT}=	Run Keyword And Return Status	Dictionary Should Contain Item  ${PDU_IDRAC}  execute  True
    Run Keyword If	${RESULT}  SUITE:Test Outlet in ${PDU_IDRAC}[name]

Test Outlets on PDU_IDRAC9
    ${RESULT}=	Run Keyword And Return Status	Dictionary Should Contain Item  ${PDU_IDRAC9}  execute  True
    Run Keyword If	${RESULT}  SUITE:Test Outlet in ${PDU_IDRAC9}[name]

Test Outlets on PDU_SERVERTECH
    ${RESULT}=	Run Keyword And Return Status	Dictionary Should Contain Item  ${PDU_SERVERTECH}  execute  True
    Run Keyword If	${RESULT}  SUITE:Test Outlet in ${PDU_SERVERTECH}[name]

Test Outlets on PDU_RARITAN
    ${RESULT}=	Run Keyword And Return Status	Dictionary Should Contain Item  ${PDU_RARITAN}  execute  True
    Run Keyword If	${RESULT}  SUITE:Test Outlet in ${PDU_RARITAN}[name]

Test Outlets on PDU_RARITAN_METERED
    ${RESULT}=	Run Keyword And Return Status	Dictionary Should Contain Item  ${PDU_RARITAN_METERED}  execute  True
    Run Keyword If	${RESULT}  SUITE:Test Outlet in ${PDU_RARITAN_METERED}[name]

Test Outlets on PDU_RARITAN_PX3
    ${RESULT}=	Run Keyword And Return Status	Dictionary Should Contain Item  ${PDU_RARITAN_PX3}  execute  True
    Run Keyword If	${RESULT}  SUITE:Test Outlet in ${PDU_RARITAN_PX3}[name]

Test Outlets on PDU_PM3000
    ${RESULT}=	Run Keyword And Return Status	Dictionary Should Contain Item  ${PDU_PM3000}  execute  True
    Run Keyword If	${RESULT}  SUITE:Test Outlet in ${PDU_PM3000}[name]

Test Outlets on PDU_MPH2
    ${RESULT}=	Run Keyword And Return Status	Dictionary Should Contain Item  ${PDU_MPH2}  execute  True
    Run Keyword If	${RESULT}  SUITE:Test Outlet in ${PDU_MPH2}[name]

Test Outlets on PDU_ENCONNEX
    ${RESULT}=	Run Keyword And Return Status	Dictionary Should Contain Item  ${PDU_ENCONNEX}  execute  True
    Run Keyword If	${RESULT}  SUITE:Test Outlet in ${PDU_ENCONNEX}[name]

Test Outlets on PDU_CPI_METERED
    ${RESULT}=	Run Keyword And Return Status	Dictionary Should Contain Item  ${PDU_CPI_METERED}  execute  True
    Run Keyword If	${RESULT}  SUITE:Test Outlet in ${PDU_CPI_METERED}[name]

Test Outlets on PDU_TRIPPLITE_ATS
	[Tags]	EXCLUDEIN4_2
    ${RESULT}=	Run Keyword And Return Status	Dictionary Should Contain Item  ${PDU_TRIPPLITE_ATS}  execute  True
    Run Keyword If	${RESULT}  SUITE:Test Outlet in ${PDU_TRIPPLITE_ATS}[name]

*** Keywords ***
SUITE:Setup    
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	
	GUI::ManagedDevices::Delete All Devices
	GUI::System::License::Delete All Licenses

	GUI::System::License::Add License  ${FIVE_THOUSAND_DEVICES_ACCESS_LICENSE}
	GUI::Basic::Logout

	SUITE:Load devices archive

	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open Discovery Now

	#Select all devices
	Click Element	css=#discovery_now > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox  //table[@id="discovernowTable"]//input[@type="checkbox"]
	GUI::Basic::Click Element	//*[@id='discovernow']
	GUI::Basic::Spinner Should Be Invisible
	Sleep    30s
	GUI::Access::Open

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete All Devices
	close all browsers

SUITE:Load devices archive
	CLI:Open
	CLI:Connect As Root
	CLI:Enter Path  /tmp/
	CLI:Write  wget ftp://ftpuser@192.168.2.88/files/zpe_pdus.conf --password ftpuser
	CLI:Close Connection
    
	CLI:Open
	Set Client Configuration  timeout=400
	CLI:Enter Path  /settings/devices
	CLI:Write  import_settings --file /tmp/zpe_pdus.conf
	Sleep		30s
	CLI:Close Connection

SUITE:Check all outlets
	Wait Until Element Is Visible	nonAccessControls
	Wait Until Element Is Visible	//*[@id="thead"]/tr/th[1]/input
	Click Element	//*[@id="thead"]/tr/th[1]/input
	Sleep	5

SUITE:Outlet On button
	Wait Until Element Is Visible	xpath=//input[@value='Outlet On']
	Click Element	xpath=//input[@value='Outlet On']
	Wait Until Keyword Succeeds	20	20	GUI::Basic::Spinner Should Be Invisible
	Sleep	20
	SUITE:Outlet Status button
	Element Should Contain	xpath=//td[6]	On

SUITE:Outlet Off button
	Wait Until Element Is Visible	xpath=//input[@value='Outlet Off']
	Click Element	xpath=//input[@value='Outlet Off']
	Wait Until Keyword Succeeds	20	20	GUI::Basic::Spinner Should Be Invisible
	Sleep	20
	SUITE:Outlet Status button
	Element Should Contain	xpath=//td[6]	Off


SUITE:Outlet Cycle button
	Wait Until Element Is Visible	xpath=//input[@value='Outlet Cycle']
	Click Element	xpath=//input[@value='Outlet Cycle']
	Wait Until Keyword Succeeds	20	20	GUI::Basic::Spinner Should Be Invisible
	Sleep	20
	
SUITE:Outlet Status button
	Wait Until Element Is Visible	nonAccessControls
	Wait Until Element Is Visible	xpath=//input[@value='Outlet Status']
	Click Element	xpath=//input[@value='Outlet Status']
	Wait Until Keyword Succeeds	10	20	GUI::Basic::Spinner Should Be Invisible
	Sleep	10
	Page Should Not Contain	unknown

SUITE:Check outlet line ${ROW}
	Wait Until Element Is Visible	xpath=(//input[@type='checkbox'])[${ROW}]
	Select Checkbox	xpath=(//input[@type='checkbox'])[${ROW}]

SUITE:Test Outlet in ${PDU_NAME}
	SUITE:Test Outlet Single ${PDU_NAME}
	SUITE:Test Outlet Multiple Outlets ${PDU_NAME}
	SUITE:Test Outlet All Outlets ${PDU_NAME}

SUITE:Test Outlet Single ${PDU_NAME}
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${PDU_NAME}
	Click Link	${PDU_NAME}

	SUITE:Outlet Status button
	SUITE:Check outlet line 2
	SUITE:Outlet On button

	SUITE:Check outlet line 2
	SUITE:Outlet Off button

	SUITE:Check outlet line 2
	SUITE:Outlet Cycle button
	SUITE:Outlet Status button
	Wait Until Keyword Succeeds	30	2	Element Should Contain	xpath=//td[6]	On
	[Teardown]	Click Element	xpath=//div[@id='modal']/div/div/div/button/span

SUITE:Test Outlet Multiple Outlets ${PDU_NAME}
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${PDU_NAME}
	Click Link	${PDU_NAME}

	SUITE:Outlet Status button
	SUITE:Check outlet line 2
	SUITE:Check outlet line 3
	SUITE:Check outlet line 4
	SUITE:Check outlet line 5
	SUITE:Outlet On button

	SUITE:Check outlet line 2
	SUITE:Check outlet line 3
	SUITE:Check outlet line 4
	SUITE:Check outlet line 5
	SUITE:Outlet Off button

	SUITE:Check outlet line 2
	SUITE:Check outlet line 3
	SUITE:Check outlet line 4
	SUITE:Check outlet line 5
	SUITE:Outlet Cycle button
	SUITE:Outlet Status button
	Wait Until Keyword Succeeds	30	2	Element Should Contain	xpath=//td[6]	On
	[Teardown]	Click Element	xpath=//div[@id='modal']/div/div/div/button/span

SUITE:Test Outlet All Outlets ${PDU_NAME}
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${PDU_NAME}
	Click Link	${PDU_NAME}    
	SUITE:Outlet Status button
	
	SUITE:Check all outlets
	SUITE:Outlet On button   
	
	SUITE:Check all outlets
	SUITE:Outlet Off button  
	
	SUITE:Check all outlets
	SUITE:Outlet Cycle button
	Sleep    10s
	SUITE:Outlet Status button
	Wait Until Keyword Succeeds	30	2	Element Should Contain	xpath=//td[6]	On
	[Teardown]	Click Element	xpath=//div[@id='modal']/div/div/div/button/span