*** Settings ***
Documentation	Testing Adding a device from managed devices Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}  CHROME  FIREFOX  EDGE  NON-CRITICAL

*** Variables ***
${HOMEPAGE}  ${HOST}

*** Test Cases ***
Try to search devices with filter
    GUI::Basic::Input Text    //*[@id='filter_field']    ttyS
    Press Keys    None  ENTER
    GUI::Basic::Spinner Should Be Invisible
    Table Should Contain	jquery=#SPMTable    ttyS1
    Table Should Contain	jquery=#SPMTable    ttyS8

    GUI::Basic::Input Text    //*[@id='filter_field']    usb
    Press Keys    None  ENTER
    GUI::Basic::Spinner Should Be Invisible
    Table Should Contain	jquery=#SPMTable    usbS1
    Table Should Contain	jquery=#SPMTable    usbS4

    GUI::Basic::Input Text    //*[@id='filter_field']    1
    Press Keys    None  ENTER
    GUI::Basic::Spinner Should Be Invisible
    Table Should Contain	jquery=#SPMTable    ttyS1
    Table Should Contain	jquery=#SPMTable    usbS1

Validate if before logout still with search history
    GUI::Basic::Logout
    GUI::Basic::Login

    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Open

Validate no device founded filter
    GUI::Basic::Input Text    //*[@id='filter_field']    007
    Press Keys    None  ENTER
    GUI::Basic::Spinner Should Be Invisible
    Element Should Contain    //*[@id="search_result_message"]  No results were found. Please try a different search.
    
Validate search history
    Click Element   //*[@id="histShow"]
    Element Should Contain    //*[@id="historyBlock"]	ttyS
    Element Should Contain    //*[@id="historyBlock"]	usb
    Element Should Contain    //*[@id="historyBlock"]	1
    Element Should Contain    //*[@id="historyBlock"]	007

Try to see if the clear and show button desapear wen use clear button
    Click Element    //*[@id="histClear"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Elements Should Not Be Visible  //*[@id="histShow"]
    GUI::Basic::Elements Should Not Be Visible  //*[@id="histClear"]

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Open
    
    ${ISVMSYSTEM}=  GUI::Basic::Is VM System
    Skip If     ${ISVMSYSTEM}  Only runs on phisical devices by dependency of tty

SUITE:Teardown
    GUI::Basic::Logout and Close Nodegrid
    Close All Browsers