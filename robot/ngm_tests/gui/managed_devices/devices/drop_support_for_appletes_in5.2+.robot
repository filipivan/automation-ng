*** Settings ***
Documentation	Test Security:Firewall rules description/comments
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_0

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${device_1}	Raritan_Dominion_KXIII
${device_2}	Intel_BMC
${device_3}	iLO4_v2.70
${device_4}	iLO_5

*** Test Cases ***
Test for Raritan Dominion KXIII
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Add the device Raritan Dominion KXIII
	SUITE:Add discovery rule
	SUITE:Discover KVM ports device
	SUITE:check if Discovered KVM ports of device on discovery logs
	SUITE:Reset discovery Logs

Test case to check intel bmc device
	SUITE:Setup
	SUITE:Add the device as intel_bmc type
	SUITE:Add the KVM Command with the intel_bmc.py extension
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//a[contains(text(),'KVM')]

Test case to check iLO4
	SUITE:Setup
	SUITE:Add the device iLO4
	SUITE:Add the KVM Command with the ilo.py extension
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//a[contains(text(),'KVM')]

Test case to check iLO5
	SUITE:Setup
	SUITE:Add the device iLO5
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	${device_4}
	Click Link	${device_4}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spmcommands_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	id=command	KVM
	Select From List By Label	//*[@id="pymod_file"]	ilo.py
	GUI::Basic::Save
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//a[contains(text(),'KVM')]

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete All Devices
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab

Suite:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${device_1}
	GUI::ManagedDevices::Delete Device If Exists	${device_2}
	GUI::ManagedDevices::Delete Device If Exists	${device_3}
	GUI::ManagedDevices::Delete Device If Exists	${device_4}
	GUI::ManagedDevices::Delete Device If Exists	Dominion_KX3_Port1
	GUI::ManagedDevices::Delete Device If Exists	Dominion_KX3_Port1
	GUI::Basic::Managed Devices::Auto Discovery::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_discovery"]/span
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="kvm_raritan"]/td[1]/input
	Click Button	//*[@id="delButton"]
	Handle Alert
	GUI::Basic::Logout And Close Nodegrid

SUITE:Add the device Raritan Dominion KXIII
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=spm_name	${device_1}
	Select From List By Label	id=type	${RARITAN_KVM_TYPE}
	Input Text	id=phys_addr	${RARITAN_KVM_IP}
	Input Text	//*[@id="username"]	${RARITAN_KVM_USERNAME}
	Input Text	//*[@id="passwordfirst"]	${RARITAN_KVM_PASSWORD}
	Input Text	//*[@id="passwordconf"]	${RARITAN_KVM_PASSWORD}
	Select From List By Label	id=status	On-demand
	GUI::Basic::Save

SUITE:Add the device as intel_bmc type
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=spm_name	${device_2}
	Select From List By Label	id=type	${INTEL_BMC_TYPE}
	Input Text	id=phys_addr	${INTEL_BMC_IP}
	Input Text	//*[@id="username"]	${INTEL_BMC_USERNAME}
	Input Text	//*[@id="passwordfirst"]	${INTEL_BMC_PASSWORD}
	Input Text	//*[@id="passwordconf"]	${INTEL_BMC_PASSWORD}
	Select From List By Label	id=status	On-demand
	GUI::Basic::Save

SUITE:Add the device iLO4
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=spm_name	${device_3}
	Select From List By Label	id=type	${ILO_TYPE}
	Input Text	id=phys_addr	${ILO4_IP}
	Input Text	//*[@id="username"]	${ILO4_USERNAME}
	Input Text	//*[@id="passwordfirst"]	${ILO4_PASSWORD}
	Input Text	//*[@id="passwordconf"]	${ILO4_PASSWORD}
	Select From List By Label	id=status	On-demand
	GUI::Basic::Save

SUITE:Add the device iLO5
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=spm_name	${device_4}
	Select From List By Label	id=type	${ILO_TYPE}
	Input Text	id=phys_addr	${ILO5_IP}
	Input Text	//*[@id="username"]	${ILO5_USERNAME}
	Input Text	//*[@id="passwordfirst"]	${ILO5_PASSWORD}
	Input Text	//*[@id="passwordconf"]	${ILO5_PASSWORD}
	Select From List By Label	id=status	On-demand
	GUI::Basic::Save

SUITE:Add the KVM Command with the intel_bmc.py extension
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	${device_2}
	Click Link	${device_2}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spmcommands_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	id=command	KVM
	Select From List By Label	//*[@id="pymod_file"]	intel_bmc.py
	GUI::Basic::Save

SUITE:Add the KVM Command with the ilo.py extension
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	${device_3}
	Click Link	${device_3}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spmcommands_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	id=command	KVM
	Select From List By Label	//*[@id="pymod_file"]	ilo.py
	GUI::Basic::Save

SUITE:Add discovery rule
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Auto Discovery::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_discovery"]/span
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	Wait Until Page Contains Element	//*[@id="discovery_name"]
	Input Text	//*[@id="discovery_name"]	kvm_raritan
	Click Element	//*[@id="addSPMD_Form"]/div[2]/div/div/div[3]/div/div[15]/label
	Input Text	//*[@id="portlistkvm"]	1,2
	Select From List By Label	//*[@id="seed"]	${device_1}
	GUI::Basic::Save

SUITE:Discover KVM ports device
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Auto Discovery::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="discovery_now"]
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Disabled	//*[@id="discovernow"]
	Page Should Contain Element	//*[@id="discovernowTable"]
	Select Checkbox	//*[@id="kvm|${device_1}"]/td[1]/input
	Click Button	//*[@id="discovernow"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:check if Discovered KVM ports of device on discovery logs
	GUI::Basic::Managed Devices::Auto Discovery::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_discoverylogs"]/span
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Dominion_KX3_Port1
	Page Should Contain	Dominion_KX3_Port2

SUITE:Reset discovery Logs
	GUI::Basic::Managed Devices::Auto Discovery::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_discoverylogs"]/span
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="reset_logs"]
