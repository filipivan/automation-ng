*** Settings ***
Documentation	Testing sending ssh keys to managed Devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX
Default Tags	EXCLUDEIN4_2

*** Variables ***
${USERPASS}	root

*** Test Cases ***

Add Device and check for the device addition.
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="spm_name"]	${HOSTPEER}
	Select From List By Value	id=type	device_console
	Input Text	//*[@id="spm_name"]	${HOSTPEER}
	Input Text	//*[@id="username"]	${USERPASS}
	Input Text	//*[@id="passwordfirst"]	${USERPASS}
	Input Text	//*[@id="passwordconf"]	${USERPASS}
	Click Element	//*[@id="addSPM_Form"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Table Should Has Row With Id	SPMTable	${HOSTPEER}

Generate Ssh keys and then send the SSH keys to the device
	[Tags]	NON-CRITICAL	NEED-REVIEW
	Click Element  //*[@id="${HOSTPEER}"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id='sshkeyaction']
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id='saveButton']
	Click Element	//*[@id='saveButton']
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id='cancelButton']

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox  //*[@id="${HOSTPEER}"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete With Alert
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
