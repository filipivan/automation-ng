*** Settings ***
Documentation	Test pdu operations
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${PDU_DEVICE}	test_raritan
${DEVICE_TYPE}	pdu_raritan
${PDU_ID}	My-PDU
${OUTLET_ID1}	1
${OUTLET1_NAME}	Outlet_1
${OUTLET2_NAME}	test-bnu_02
${OUTLET3_NAME}	Outlet_3
${TEST_DEVICE}	test_device
${TEST_DEVICE_TYPE}	device_console
${COMMAND}	Outlet
${NEW_USER}	test1
${NEW_PASS}	test1

*** Test Cases ***
Test Case To Add pdu device/protocol/SNMPV2
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=spm_name	${PDU_DEVICE}
	Select From List By Label	id=type	${DEVICE_TYPE}
	Input Text	id=phys_addr	${PDU_RARITAN}[ip]
	Select From List By Label	id=status	On-demand
	GUI::Basic::Save
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Link	${PDU_DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spmmgmt_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="snmp_proto_enabled"]
	Input Text	//*[@id="community2"]	private
	GUI::Basic::Save
	Click Element	//*[@id="spmcommands_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="Outlet"]
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	//*[@id="protocol"]	SNMP
	GUI::Basic::Save
	Close Browser

Test case to perform auto discovery and check outlets discoverd on outlets page
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Auto Discovery::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="discovery_now"]
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Disabled	//*[@id="discovernow"]
	Page Should Contain Element	//*[@id="discovernowTable"]
	Select Checkbox	//*[@id="pdu|${PDU_DEVICE}"]/td[1]/input
	Click Button	//*[@id="discovernow"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	Sleep	15s
	Click Link	${PDU_DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spmoutlets_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${OUTLET1_NAME}
	Page Should Contain	${OUTLET2_NAME}
	Page Should Contain	${OUTLET3_NAME}
	Close Browser

Test case to do pdu operations on Access table
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="|${PDU_DEVICE}"]/td[1]/div/a/span
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	css=.btn:nth-child(4)
	Click Button	css=.btn:nth-child(4)
	GUI::Basic::Spinner Should Be Invisible
	Page Should not contain	unknown
	Select Checkbox	xpath=//tr[@id='test_raritan:My-PDU:1:Outlet_1']/td/input
	Click Button	//*[@id="nonAccessControls"]/input[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	Wait Until Element Contains	//*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]	Off
	Select Checkbox	xpath=//tr[@id='test_raritan:My-PDU:1:Outlet_1']/td/input
	Click Button	css=.btn:nth-child(3)
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	Wait Until Element Contains	//*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]	On
	Select Checkbox	xpath=//tr[@id='test_raritan:My-PDU:1:Outlet_1']/td/input
	Click Button	css=#nonAccessControls > .btn:nth-child(2)
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	//*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]	Off
	Select Checkbox	xpath=//tr[@id='test_raritan:My-PDU:1:Outlet_1']/td/input
	Click Button	css=.button:nth-child(1)
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	//*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]		On
	Close Browser

Test case to do pdu operations from tree page
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open And Login Nodegrid
	GUI:Access::Open Tree tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Wait Until Element Is Accessible	//div[@parent[contains(.,"|${HOSTNAME_NODEGRID}")]]//a[@title="${PDU_DEVICE}"]
	Click Element	//div[@parent[contains(.,"|${HOSTNAME_NODEGRID}")]]//a[@title="${PDU_DEVICE}"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	css=.btn:nth-child(4)
	Click Button	css=.btn:nth-child(4)
	GUI::Basic::Spinner Should Be Invisible
	Page Should not contain	unknown
	Select Checkbox	xpath=//tr[@id='test_raritan:My-PDU:1:Outlet_1']/td/input
	Click Button	//*[@id="nonAccessControls"]/input[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	Wait Until Element Contains	//*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]	Off
	Select Checkbox	xpath=//tr[@id='test_raritan:My-PDU:1:Outlet_1']/td/input
	Click Button	css=.btn:nth-child(3)
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	Wait Until Element Contains	//*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]	On
	Select Checkbox	xpath=//tr[@id='test_raritan:My-PDU:1:Outlet_1']/td/input
	Click Button	css=#nonAccessControls > .btn:nth-child(2)
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	//*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]	Off
	Select Checkbox	xpath=//tr[@id='test_raritan:My-PDU:1:Outlet_1']/td/input
	Click Button	css=.button:nth-child(1)
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	//*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]		On
	Close Browser

Test case to create new device user and add Group permission
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=spm_name	${TEST_DEVICE}
	Select From List By Label	id=type	${TEST_DEVICE_TYPE}
	GUI::Basic::Save
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Open Local Accounts tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="uName"]	${NEW_USER}
	Input Text	//*[@id="uPasswd"]	${NEW_PASS}
	Input Text	//*[@id="cPasswd"]	${NEW_PASS}
	GUI::Basic::Save
	GUI::Security::Open Services tab
	Select Checkbox	//*[@id="authorization"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Open Authorization tab
	GUI::Basic::Spinner Should Be Invisible
	Click Link	//*[@id="user"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm"]/span
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	Select From List By Label	xpath=//div[@id='targetSelection']/div/div/div/select	${TEST_DEVICE}
	Click Element	//*[@id="targetSelection"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="saveButton"]
	Handle Alert
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login		${NGVERSION}	${NEW_USER}	${NEW_PASS}
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${TEST_DEVICE}
	GUI::Basic::Open And Login Nodegrid
	GUI::Security::Open Authorization tab
	GUI::Basic::Spinner Should Be Invisible
	Click Link	//*[@id="user"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm"]/span
	GUI::Basic::Spinner Should Be Invisible
	Click Link	//*[@id="test_device"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="rPowerMode"]
	Click Element	//*[@id="saveButton"]
	Handle Alert
	GUI::Basic::Spinner Should Be Invisible
	Page Should Not Contain	control
	GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login		${NGVERSION}	${NEW_USER}	${NEW_PASS}
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${TEST_DEVICE}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::ManagedDevices::Delete Device If Exists	${PDU_DEVICE}
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Security::Open Local Accounts tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="test1"]/td[1]/input
	Click Button	//*[@id="delButton"]
	Handle Alert
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Open Services tab
	UnSelect Checkbox	//*[@id="authorization"]
	GUI::Basic::Save
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${PDU_DEVICE}
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${TEST_DEVICE}
	GUI::Basic::Logout and Close Nodegrid

GUI::Security::Open Services tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]
	...	Opens the NodeGrid Security::Authorization page
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Services page is open and all elements are accessible
	[Tags]	GUI	SECURITY
	GUI::Security::Open Security
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=(//a[contains(text(),'Services')])[2]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#webService
	Wait Until Element Is Visible	jquery=#services
	Wait Until Element Is Visible	jquery=#sshServer
	Wait Until Element Is Visible	jquery=#crypto
	Wait Until Element Is Visible	jquery=#managedDevices
