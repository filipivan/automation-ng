*** Settings ***
Documentation	Automated test for Device Access
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags  DEPENDENCE_SERVERRDP	DEPENDENCE_SERVERVNC

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${DELAY}	15s
${RDP_DEVICE_NAME}	${RDP_VM_NAME}
${VNC_DEVICE_NAME}	${VNC_VM_NAME}
${TEST_RDP_USER}	${RDP_USER}
${TEST_VNC_USER}	${VNC_USER}
${TEST_RDP_PASSWORD}	${RDP_PASSWORD}
${TEST_VNC_PASSWORD}	${VNC_PASSWORD}
${TEST_RDP_TCP_PORT}	${RDP_TCP_PORT}
${TEST_VNC_TCP_PORT}	${VNC_TCP_PORT}
${TEST_RDP_IP}	${RDP_IP}
${TEST_VNC_IP}	${VNC_IP}

*** Test Cases ***
Test case to add a device for RDP
	SUITE:Setup
	GUI::Basic::Add
	Input Text	id=spm_name	${RDP_DEVICE_NAME}
	Select From List By Label	id=type	device_console
	Input Text	id=phys_addr	${TEST_RDP_IP}
	Input Text	id=tcpport	${TEST_RDP_TCP_PORT}
	Input Text	id=username	${TEST_RDP_USER}
	Click Element	id=passwordfirst
	Clear Element Text	//*[@id="passwordfirst"]
	Input Text	id=passwordfirst	${TEST_RDP_PASSWORD}
	Click Element	id=passwordconf
	Clear Element Text	//*[@id="passwordconf"]
	Input Text	id=passwordconf	${TEST_RDP_PASSWORD}
	GUI::Basic::Save
	[Teardown]	SUITE:Teardown

Test case to add kvm command and rdp protocol to device
	SUITE:Setup
	Click Element	//*[@id="${RDP_DEVICE_NAME}"]/td[2]/a
	Sleep	${DELAY}
	Click Element	//*[@id="spmcommands_nav"]
	Sleep	${DELAY}
	GUI::Basic::Add
	Page Should Contain Element	//*[@id="command"]
	Select Checkbox	//*[@id="enabled"]
	Page Should Contain Element	//*[@id="protocol"]
	Click Element	id=command
	Select From List By Label	id=command	KVM
	Select From List By Label	id=protocol	RDP
	Select From List By Label	id=pymod_file	rdp.py
	GUI::Basic::Save
	[Teardown]	SUITE:Teardown

Test case to check the device added with rdp protocol is able to access
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//a[contains(text(),'KVM')]
	Run Keyword If 	'${NGVERSION}' <= '5.6'	Click Element	xpath=//a[contains(text(),'KVM')]
	...	ELSE	click element		xpath=//tr[@id='|${RDP_DEVICE_NAME}|']/td[2]/div/a[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	60s
	Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists	${RDP_DEVICE_NAME} - KVM
	...	ELSE	GUI:Basic::Wait Until Window Exists	${RDP_DEVICE_NAME}
	Page Should Contain	${RDP_DEVICE_NAME}
	Page Should Contain Element	//div[@class='display software-cursor']//div//div//div
	[Teardown]	SUITE:Teardown

Test case to delete the device
	SUITE:Setup
	GUI::ManagedDevices::Delete Device If Exists	${RDP_DEVICE_NAME}
	SUITE:Setup
	Page Should Not Contain	${RDP_DEVICE_NAME}
	[Teardown]	SUITE:Teardown

Test case to add a device for VNC
	SUITE:Setup
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=spm_name	${VNC_DEVICE_NAME}
	Select From List By Label	id=type	device_console
	Input Text	id=phys_addr	${TEST_VNC_IP}
	Input Text	id=tcpport	${TEST_VNC_TCP_PORT}
	Input Text	id=username	${TEST_VNC_USER}
	Click Element	id=passwordfirst
	Clear Element Text	//*[@id="passwordfirst"]
	Input Text	id=passwordfirst	${TEST_VNC_PASSWORD}
	Click Element	id=passwordconf
	Clear Element Text	//*[@id="passwordconf"]
	Input Text	id=passwordconf	${TEST_VNC_PASSWORD}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	[Teardown]	SUITE:Teardown

Test case to add kvm command and vnc protocol to device
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="${VNC_DEVICE_NAME}"]/td[2]/a
	Sleep	${DELAY}
	Click Element	//*[@id="spmcommands_nav"]
	Sleep	${DELAY}
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="command"]
	Select Checkbox	//*[@id="enabled"]
	Page Should Contain Element	//*[@id="protocol"]
	Click Element	id=command
	Select From List By Label	id=command	KVM
	Select From List By Label	id=protocol	VNC
	Select From List By Label	id=pymod_file	vnc.py
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	[Teardown]	SUITE:Teardown

Test case to check the device added with VNC is able to access
	[Tags]	NON-CRITICAL
#	VNC Device is not able accessible & ping too ...Asked Jamie to Recover
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If 	'${NGVERSION}' <= '5.6'		Click Element	//*[@id="|${VNC_DEVICE_NAME}"]/td[3]/div/a[2]
	...	ELSE	click element		//*[@id="|${VNC_DEVICE_NAME}|"]/td[2]/div/a[2]
	Sleep	60s
	Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists	${VNC_DEVICE_NAME} - KVM
	...	ELSE	GUI:Basic::Wait Until Window Exists	${VNC_DEVICE_NAME}
	Page Should Contain Element	//div[@class='display software-cursor']//div//div//div
	[Teardown]	SUITE:Teardown

Test case to delete the device created with VNC protocol
	SUITE:Setup
	GUI::ManagedDevices::Delete Device If Exists	${VNC_DEVICE_NAME}
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Page Should Not Contain	${VNC_DEVICE_NAME}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Close All Browsers