*** Settings ***
Documentation	Testing elements on  managed devices -> devices -> add for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

*** Test Cases ***
Test NonAccess Buttons
	Element Should Be Enabled	//*[@id="cancelButton"]
	Element Should Be Disabled	//*[@id="saveButton"]

Test Name And Types Exist
	Element Should Be Visible	//*[@id="addSPM_Form"]/div[2]/div[1]/div/div[1]/label
	Element Should Be Visible	//*[@id="addSPM_Form"]/div[2]/div[1]/div/div[2]/label
	@{Types}=	Create List	cimc_ucs	console_server_acs  console_server_acs6000  console_server_digicp	console_server_lantronix	console_server_nodegrid  console_server_opengear	device_console	drac	 idrac6	ilo	ilom	imm	infrabox	ipmi_1.5	ipmi_2.0	kvm_aten	kvm_dsr	kvm_mpu	kvm_raritan	netapp  pdu_apc	pdu_baytech	pdu_cyberpower	pdu_eaton	pdu_enconnex	pdu_geist	pdu_mph2	pdu_pm3000	pdu_raritan	pdu_servertech	virtual_console_kvm	virtual_console_vmware
	@{TXT}=	Get List Items	//*[@id="type"]
	FOR	${I}	IN	@{Types}
		Should Contain  ${TXT}	${I}
	END

Test Notice And IP Address Exists
	Select From List By Value	//*[@id="type"]	virtual_console_vmware
	Element Should Be Visible	//*[@id="typeNotice"]
	Element Should Be Visible	//*[@id="addSPM_Form"]/div[2]/div[1]/div/div[4]
	Element Should Be Visible	//*[@id="addSPM_Form"]/div[3]/div[1]/div/div[4]

Test Chassis And Blade ID Exists
	Select From List By Value	//*[@id="type"]	cimc_ucs
	wait until page contains element		//*[@id="chassis"]
	Element Should Be Visible	//*[@id="chassis"]
	Element Should Be Visible	//*[@id="blade"]

Test Port Exists
	Select From List By Value	//*[@id="type"]	device_console
	Element Should Be Visible	//*[@id="addSPM_Form"]/div[2]/div[1]/div/div[5]

Test Device Console Options Exist
	FOR	${I}	IN RANGE	1	4
		Element Should Be Visible	//*[@id="addSPM_Form"]/div[3]/div[1]/div/div[${I}]
	END
	${START_RANGE}=	Set Variable If	'${NGVERSION}' >= '5.0'	6	5
	FOR	${I}	IN RANGE	${START_RANGE}	9
		Element Should Be Visible	//*[@id="addSPM_Form"]/div[3]/div[1]/div/div[${I}]
	END
	FOR	${I}	IN RANGE	1	5
		Element Should Be Visible	//*[@id="addSPM_Form"]/div[3]/div[1]/div/div[2]/div/div[${I}]
	END
Test Inbound Access Left Side Elements Exist
	[Tags]	NON-CRITICAL
	FOR	${INDEX}	IN RANGE	1	8
		Element Should Be Visible	//*[@id="addSPM_Form"]/div[4]/div[1]/div/div[${INDEX}]
	END
	Select Checkbox	//*[@id="inbipEnable"]
	Select Checkbox	//*[@id="inbipTeln"]
	Select Checkbox	//*[@id="inbipRaw"]
	FOR	${I}	IN RANGE	1	5
		Element Should Be Visible	//*[@id="addSPM_Form"]/div[4]/div[1]/div/div[6]/div[2]/div[${I}]
	END
	Select Checkbox	//*[@id="inbSecipEnable"]
	Select Checkbox	//*[@id="inbSecipTeln"]
	Select Checkbox	//*[@id="inbSecipRaw"]
	FOR	${I}	IN RANGE	1	5
		Element Should Be Visible	//*[@id="addSPM_Form"]/div[4]/div[1]/div/div[7]/div[2]/div[${I}]
	END

Test Inbound Access Right Side Elements Exist
	Select Checkbox	//*[@id="inbssh"]
	Select Checkbox	//*[@id="inbteln"]
	Select Checkbox	//*[@id="inbraw"]
	FOR	${I}	IN RANGE	2	6
		Element Should Be Visible	//*[@id="addSPM_Form"]/div[4]/div[2]/div/div[${I}]
	END

Test Location And URL Exist
	FOR	${I}	IN RANGE	1	5
		Element Should Be Visible	//*[@id="addSPM_Form"]/div[2]/div[2]/div/div[${I}]
	END

Test Icon, Expiration, and End Point Exists
	Element Should Be Visible	//*[@id="addSPM_Form"]/div[3]/div[2]/div/div[1]
	Element Should Be Visible	//*[@id="addSPM_Form"]/div[3]/div[2]/div/div[3]
	Select From List By Value	//*[@id="type"]	console_server_nodegrid
	Element Should Be Visible	//*[@id="addSPM_Form"]/div[3]/div[2]/div/div[5]
	Element Should Be Visible	//*[@id="addSPM_Form"]/div[3]/div[2]/div/div[6]

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::ManagedDevices::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
