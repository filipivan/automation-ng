*** Settings ***
Documentation	Test Perle console server support
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

*** Variables ***
${perl_con_ser}	perl_con_ser
${DISCOVERY_RULE_NAME}	perle_server
${serial_port_1}	${PERLE_SCS_AVAILABLE_PORTS[0]}

*** Test Cases ***
Add perle console server to the managed devices
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=spm_name	${perl_con_ser}
	Select From List By Label	id=type	${PERLE_SCS_TYPE}
	Input Text	id=phys_addr	${PERLE_SCS_SERVER_IP}
	Input Text	id=username		${PERLE_SCS_SERVER_USERNAME}
	Input Text	id=passwordfirst	${PERLE_SCS_SERVER_PASSWORD}
	Input Text	id=passwordconf	${PERLE_SCS_SERVER_PASSWORD}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Table Should Has Row With Id	SPMTable	${perl_con_ser}

Check the console server in the access page
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${perl_con_ser}

Verify the auto-discovery for the configured perle server
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_discovery"]/span
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=discovery_name	${DISCOVERY_RULE_NAME}
	Select From List By Value	id=status	enabled
	Select Radio Button	source	consoleserver
	Select From List By Label	id=operation	Clone (Mode: Enabled)
	Select From List By Value	jquery=#seed	${perl_con_ser}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_discoverylogs"]/span
	sleep	5s
	Click Element	//*[@id="reset_logs"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#discovery_now > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="cas|${perl_con_ser}"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//*[@id='discovernow']
	GUI::Basic::Spinner Should Be Invisible
	sleep	20s
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_discoverylogs"]/span
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${serial_port_1}

Delete Devices
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Devices	${perl_con_ser}
	GUI::Basic::Table Should Not Have Row With Id	SPMTable	${perl_con_ser}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${serial_port_1}
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open Discovery Rules
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="thead"]/tr/th[1]/input
	Click Element	//*[@id="delButton"]
	Handle Alert
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid