*** Settings ***
Documentation	Test Managed-Devices-Devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX
Default Tags	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4

Suite Setup		SUITE:Setup

*** Variables ***
${DEVICE}	NSCDevice
${DEVCON_TYPE}	device_console
${DEVCON_IPADDR}	${HOST}
${DEVCON_USERNAME}	${USERNAME}
${DEVCON_ASKPASS}	yes
${DEVCON_STATUS}	enabled
${DEVCON_URL}	http://%IP
${PASSWORD}		${FACTORY_PASSWORD}
${SSHKey}	ecdsa-256

*** Test Cases ***

Test case for Adding Shell to NG
	[Documentation]		Setting shell permissions in the NSC device with serial ports
	GUI::Basic::Security::Authorization::Open Tab
	Click Link		//*[@id="admin"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="profile"]/span
	GUI::Basic::Spinner Should Be Invisible
	${Checked}=	Get Element Attribute	//input[@value='start_appl_shell']	checked
	Run Keyword If	'${Checked}'=='None'	Select Radio Button		start_appl		start_appl_shell
	Run Keyword If	'${Checked}'=='None'	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Close Browser


Test case for Adding a console device on VM
	[Documentation]		Adding a console device (NSC) on the VM
	GUI::Basic::Open NodeGrid	${HOMEPAGEPEER}
	GUI::Basic::Login
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete All Devices
	# Adding a device
	GUI::Basic::Managed Devices::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=#addButton
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#saveButton
	Input Text	jquery=#spm_name	${DEVICE}
	Select From List By Value	jquery=#type	${DEVCON_TYPE}
	Input Text	jquery=#phys_addr	${DEVCON_IPADDR}
	Run Keyword If	'${DEVCON_TYPE}' == 'virtual_console_vmware'	Run Keywords	Element Should Not Be Visible	id=username
	...	AND	Element Should Not Be Visible	id=askpassword
	...	AND	Element Should Not Be Visible	id=passwordfirst
	...	AND	Element Should Not Be Visible	id=passwordconf
	...	AND	Element Should Be Visible	id=vmmanager
	...	AND	Select From List By Index	id=vmmanager	1
	...	ELSE	Run Keywords	Element Should Be Visible	id=username
	...	AND	Element Should Be Visible	id=askpassword
	...	AND	Input Text	jquery=#username	${DEVCON_USERNAME}
	...	AND	Select Radio Button	askpassword	${DEVCON_ASKPASS}
	Select From List By Value	jquery=#status	${DEVCON_STATUS}
	Input Text	jquery=#url	${DEVCON_URL}
	GUI::Basic::Click Element	//*[@id='saveButton']
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Wait For Managed Devices Table
	Table Should Contain	jquery=#SPMTable	${DEVICE}
	Click Link		//*[@id="${DEVICE}"]
	GUI::Basic::Spinner Should Be Invisible
	# Allowing SSHkey support for the device
	Select Checkbox		//*[@id="sshkey"]
	GUI::Basic::Click Element	//*[@id='saveButton']
	# Clicking on SSH Key
	Click Button	//*[@id="sshkeyaction"]
	GUI::Basic::Spinner Should Be Invisible
	# Selecting a particular SSH key from the droopdown
	Select From List By Value	//*[@id="sshkeytype"]		${SSHKey}
	# Clicking on Generate Key pair
	Click Button	//*[@value="Generate Key pair"]
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on Use specific to enter and confirm the password
	Select Radio Button		ssh_password_from_access	use_specific
	Click Element	//*[@id="passwordsshfirst"]
	Input Text		//*[@id="passwordsshfirst"]		${FACTORY_PASSWORD}
	Input Text		//*[@id="passwordsshconf"]		${FACTORY_PASSWORD}
	# Clicking on send public key button
	Click Button	//*[@value="Send Public Key"]
	Sleep	5s
	# Verifying that the key has been installed
	page should contain		Key has been installed
	GUI::Basic::Spinner Should Be Invisible
	Close Browser

Test case for Accessing Device Console
	[Documentation]		Accessing device through console
	GUI::Basic::Open NodeGrid	${HOMEPAGEPEER}
	GUI::Basic::Login
	GUI::Basic::Spinner Should Be Invisible
	# Open and login Nodegrid
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	# Checking for read-write permissions clicking on console button as admin
	GUI:Access::Table::Access Device Console	${DEVICE}
	# Press the enter keys
	Press Keys	//body	RETURN
	Press Keys	//body	RETURN
	# Type pwd command in the output terminal to see if the output asks for password
	${OUTPUT}=  GUI:Access::Generic Console Command Output	pwd
	Should Contain  ${OUTPUT}	/home/admin
	GUI::Basic::Spinner Should Be Invisible
	Close Browser

Test case for Adding cli to NG
	[Documentation]		Adding CLI back to NG
	SUITE:Setup
	GUI::Basic::Security::Authorization::Open Tab
	Click Link		//*[@id="admin"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="profile"]/span
	GUI::Basic::Spinner Should Be Invisible
	${Checked}=	Get Element Attribute	//input[@value='start_appl_cli']	checked
	Run Keyword If	'${Checked}'=='None'	Select Radio Button		start_appl		start_appl_cli
	Run Keyword If	'${Checked}'=='None'	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Close Browser

Test case for Deleting devices on NG
	# Open and login nodegrid application
	GUI::Basic::Open NodeGrid	${HOMEPAGEPEER}
	GUI::Basic::Login
	GUI::Basic::Spinner Should Be Invisible
	# Delete the console device if exists
	GUI::ManagedDevices::Delete All Devices
	GUI::Basic::Spinner Should Be Invisible
	# Close the browser
	Close Browser

*** Keywords ***

SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	[Documentation]		Closing all the browsers
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers