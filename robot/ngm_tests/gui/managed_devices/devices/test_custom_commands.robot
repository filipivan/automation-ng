*** Settings ***
Documentation	Test cases for custom command functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	CLI	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${DEVICE}	test

*** Test Cases ***
Custom Command Functionality
	[Tags]	NON-CRITICAL	NEED-REVIEW
#	On WebUI
	SUITE:Add Console Device
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Enter Device	${DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Devices::Open Commands Menu
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add Custom Commands
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="custom_commands"]/td[1]/input
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Access::Table::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="|${DEVICE}"]/td[1]/div/a/span
	Click Element	spm_customcommand1
	Sleep	2
	Element Should Be Visible	commandresponse
	Element Should Contain	commandresponse	Hello World
	Click Element	spm_customcommand2
	Sleep	2
	Element Should Be Visible	commandresponse
	Element Should Contain	commandresponse	Hello World
	Click Element	//*[@id="modal"]/div/div/div[1]/button/span


*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	CLI:Connect as Root
	CLI:Write	> /etc/scripts/custom_commands/hello.py
	CLI:Write	/bin/echo "#!/usr/bin/env python" >> /etc/scripts/custom_commands/hello.py
	CLI:Write	/bin/echo "print('Hello World')" >> /etc/scripts/custom_commands/hello.py
	CLI:Close Connection

SUITE:Teardown
	Click Element	//*[@id="modal"]/div/div/div[1]/button/span
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE}
	GUI::Basic::Logout And Close Nodegrid
	CLI:Connect as Root
	CLI:Write	rm -f /etc/scripts/custom_commands/hello.py
	CLI:Close Connection

SUITE:Add Console Device
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE}
	GUI::ManagedDevices::Add Device	${DEVICE}	device_console	127.0.0.1

SUITE:Add Custom Commands
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Wait Until Element Is Accessible  script_customcommand1
	Select From List By Value	 script_customcommand1	hello.py

	GUI::Basic::Wait Until Element Is Accessible	script_customcommand2
	Select From List By Value	script_customcommand2	hello.py

	Select Checkbox	enabled_customcommand1
	Select Checkbox	enabled_customcommand2

	Input Text	label_customcommand1	command_1
	Input Text	label_customcommand2	hello
	GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
	Table Should Contain	SPMCommandsTable_wrapper	Custom Commands
