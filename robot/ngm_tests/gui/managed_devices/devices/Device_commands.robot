*** Settings ***
Documentation	Automated test for Device Commands: Add Console-Like Command SSH/Telnet
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	admin
${PASSWORD}	admin
${Delay}	5s
${Device_name}	test_device
${Device_Type}	device_console
${SSH_command}	SSH
${Telnet_command}	Telnet
${hostname}	nodegrid

*** Test Cases ***
Test case to add managed device
	SUITE:Setup
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_name"]
	Input Text	//*[@id="spm_name"]	${Device_name}
	Click Element	//*[@id="type"]
	Select From List By Label	//*[@id="type"]	${Device_Type}
	Click Element	//*[@id="phys_addr"]
	Input Text	//*[@id="phys_addr"]	${HOSTPEER}
	Click Element	id=username
	Input Text	id=username	${USERNAME}
	Click Element	id=passwordfirst
	Clear Element Text	//*[@id="passwordfirst"]
	Input Text	id=passwordfirst	${PASSWORD}
	Click Element	id=passwordconf
	Clear Element Text	id=passwordconf
	Input Text	id=passwordconf	${PASSWORD}
	Select Checkbox	//*[@id="inbssh"]
	Click Element	//*[@id="inbsshTCP"]
	Clear Element Text	//*[@id="inbsshTCP"]
	Input Text	//*[@id="inbsshTCP"]	3049
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	xpath=//input[@id='inbteln']
	Click Element	id=inbtelnTCP
	Clear Element Text	id=inbtelnTCP
	Input Text	id=inbtelnTCP	7049
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Teardown

Test case to add ssh command to managed device and connect to ssh functionality
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:AddDevice
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="${Device_name}"]/td[2]/a
	sleep	${DELAY}
	Click Element	//*[@id="spmcommands_nav"]
	Sleep	20s
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=command
	Select From List By Label	id=command	${SSH_command}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=user
	Clear Element Text	id=user
	Input Text	id=user	${USERNAME}
	Click Element	id=ip_address
	Clear Element Text	id=ip_address
	Input Text	id=ip_address	${HOSTPEER}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="|${Device_name}"]/td[1]/div/a/span
	sleep	10s
	Page Should Contain	${SSH_command}
	Click Element	//*[@id="modal"]/div/div/div[2]/div[1]/div[1]/div[2]/button
	sleep	10s
	Switch Window	title=test_device
	Select Frame	//iframe[@id='termwindow']
	Press Keys 	//body	RETURN
	Press Keys	//body	RETURN
	${OUTPUT}=	SUITE:Console Command Output	${PASSWORD}
	Sleep	5s
	${OUTPUT}=	SUITE:Console Command Output	whoami
	Page Should Contain	${username}
	${OUTPUT}=	SUITE:Console Command Output	hostname
	Page Should Contain	${hostname}
	${OUTPUT}=	SUITE:Console Command Output	exit
	Page Should Contain	${Device_name}
	Unselect Frame
	Press Keys 	//body 	cw
	Close all browsers
	SUITE:Teardown

Test case to Enable Telnet Service to Managed Devices
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Services::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	css=#telnetDev
	Select Checkbox	//*[@id="telnet"]
	GUI::Basic::Save
	GUI::Basic::Open Nodegrid	${HOMEPAGEPEER}	${BROWSER}	${NGVERSION}
	GUI::Basic::login
	GUI::Basic::Security::Services::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	css=#telnetDev
	Select Checkbox	//*[@id="telnet"]
	GUI::Basic::Save
	SUITE:Teardown

Test case to add Telnet command to managed device and connect to Telnet functionality
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:AddDevice
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="${Device_name}"]/td[2]/a
	GUI::Basic::Spinner Should Be Invisible
	sleep	${DELAY}
	Click Element	//*[@id="spmcommands_nav"]
	Sleep	20s
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=command
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	//*[@id="command"]	${Telnet_command}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=.title:nth-child(5) #user
	Clear Element Text	css=.title:nth-child(5) #user
	Input Text	css=.title:nth-child(5) #user	${USERNAME}
	Click Element	css=.title:nth-child(5) #ip_address
	Clear Element Text	css=.title:nth-child(5) #ip_address
	Input Text	css=.title:nth-child(5) #ip_address	${HOSTPEER}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="|${Device_name}"]/td[1]/div/a/span
	sleep	10s
	Page Should Contain	${Telnet_command}
	Click Element	//*[@id="modal"]/div/div/div[2]/div[1]/div[1]/div[2]/button
	sleep	10s
	Switch Window	title=test_device
	Select Frame	//iframe[@id='termwindow']
	Press Keys 	//body	RETURN
	Press Keys	//body	RETURN
	${OUTPUT}=	SUITE:Console Command Output	${PASSWORD}
	Sleep	5s
	${OUTPUT}=	SUITE:Console Command Output	whoami
	Page Should Contain	${username}
	${OUTPUT}=	SUITE:Console Command Output	hostname
	Sleep	5s
	Page Should Contain	${hostname}
	${OUTPUT}=	SUITE:Console Command Output	exit
	Sleep	5s
	Page Should Contain	${Device_name}
	Unselect Frame
	Press Keys 	//body 	cw
	Close all browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Security::Services::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep	${Delay}
	Unselect Checkbox	css=#telnetDev
	Unselect Checkbox	//*[@id="telnet"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open Nodegrid	${HOMEPAGEPEER}	${BROWSER}	${NGVERSION}
	GUI::Basic::login
	GUI::Basic::Security::Services::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Unselect Checkbox	css=#telnetDev
	Unselect Checkbox	//*[@id="telnet"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::ManagedDevices::Open Devices tab

SUITE:Teardown
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::ManagedDevices::Delete Device If Exists	${Device_name}
	GUI::Basic::Logout And Close Nodegrid

SUITE:AddDevice
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_name"]
	Input Text	//*[@id="spm_name"]	${Device_name}
	Click Element	//*[@id="type"]
	Select From List By Label	//*[@id="type"]	${Device_Type}
	Click Element	//*[@id="phys_addr"]
	Input Text	//*[@id="phys_addr"]	${HOSTPEER}
	Click Element	id=username
	Input Text	id=username	${USERNAME}
	Click Element	id=passwordfirst
	Clear Element Text	//*[@id="passwordfirst"]
	Input Text	id=passwordfirst	${PASSWORD}
	Click Element	id=passwordconf
	Clear Element Text	id=passwordconf
	Input Text	id=passwordconf	${PASSWORD}
	Select Checkbox	//*[@id="inbssh"]
	Click Element	//*[@id="inbsshTCP"]
	Clear Element Text	//*[@id="inbsshTCP"]
	Input Text	//*[@id="inbsshTCP"]	3049
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	xpath=//input[@id='inbteln']
	Click Element	id=inbtelnTCP
	Clear Element Text	id=inbtelnTCP
	Input Text	id=inbtelnTCP	7049
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

GUI::Security::Open Services tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]
	...	Opens the NodeGrid Security::Authorization page
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Services page is open and all elements are accessible
	[Tags]	GUI	SECURITY
	GUI::Security::Open Security
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(7) > a
	Wait Until Element Is Visible	jquery=#webService
	Wait Until Element Is Visible	jquery=#services
	Wait Until Element Is Visible	jquery=#sshServer
	Wait Until Element Is Visible	jquery=#crypto
	Wait Until Element Is Visible	jquery=#managedDevices

SUITE:Console Command Output
	[Arguments]	${COMMAND}
	[Documentation]	Input the given command into the ttyd terminal and retrieve the output after prompt lines
	...	== ARGUMENTS ==
	...	-	COMMAND = Command to be executed
	...	== EXPECTED RESULT ==
	...	Input the command into the ttyd terminal and returns the OUTPUT between the command and the last prompt

	${CHECK_TAB}=	Run Keyword And Return Status	Should Not Contain	${COMMAND}	TAB
	Run Keyword If	${CHECK_TAB}	Press Keys  //body	${COMMAND}	RETURN
	...	ELSE	Press Keys  //body	${COMMAND}
	Sleep	1
