*** Settings ***
Documentation	Verifies type field is a combo box and contains all options
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}

*** Test Cases ***
Verify type field contains all options when adding
	GUI::ManagedDevices::Delete Devices	${DUMMY_DEVICE_CONSOLE_NAME}	${DUMMY_DEVICE_CONSOLE_NAME}2
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text	//*[@id='spm_name']	${DUMMY_DEVICE_CONSOLE_NAME}
	@{TYPES}=	Get List Items	jquery=#type
	@{MUST_HAVE_TYPES}=	Create List	ilo	imm	drac	idrac6	ipmi_1.5	ipmi_2.0	ilom	intel_bmc	cimc_ucs	netapp	device_console	nodegrid_ap	infrabox	virtual_console_vmware	virtual_console_kvm	console_server_nodegrid	console_server_acs	console_server_opengear	console_server_acs6000	console_server_digcp	console_server_lantronix	console_server_perle		console_server_raritan	pdu_apc	pdu_baytech	pdu_eaton	pdu_mph2	pdu_pm3000	pdu_cpi		pdu_raritan	pdu_geist	pdu_servertech	pdu_enconnex	pdu_cyberpower	pdu_rittal	pdu_tripplite	kvm_dsr	kvm_mpu	kvm_aten	kvm_raritan
	Select From List By Value	jquery=#type	device_console
	GUI::Basic::Click Element	//*[@id='saveButton']
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	GUI::ManagedDevices::Wait For Managed Devices Table
	GUI::Basic::Spinner Should Be Invisible
	Table Should Contain	jquery=#SPMTable	${DUMMY_DEVICE_CONSOLE_NAME}

Verify type field is combo box on drilldown mode
	GUI::ManagedDevices::Enter Device	${DUMMY_DEVICE_CONSOLE_NAME}
	GUI::Basic::Wait Until Element Is Accessible	jquery=select#type

Verify type field contains all options on drilldown mode
	[Tags]	NON-CRITICAL	NEED-REVIEW
	@{TYPES}=	Get List Items	jquery=#type
	@{MUST_HAVE_TYPES}=	Create List
	${ret}=	GUI::Basic::Is Serial Console
	# only serial console have local serial types
	Run Keyword If	'${ret}' == '1'	Append To List	${MUST_HAVE_TYPES}	local_serial	usb_serial	usb_serialB
	Append to List	 ${MUST_HAVE_TYPES}	 ilo	imm	drac	idrac6	ipmi_1.5	ipmi_2.0	ilom	intel_bmc	cimc_ucs	netapp	device_console	nodegrid_ap	infrabox	virtual_console_vmware	virtual_console_kvm	console_server_nodegrid	console_server_acs	console_server_opengear	console_server_acs6000	console_server_digcp	console_server_lantronix	console_server_perle		console_server_raritan	pdu_apc	pdu_baytech	pdu_eaton	pdu_mph2	pdu_pm3000	pdu_cpi		pdu_raritan	pdu_geist	pdu_servertech	pdu_enconnex	pdu_cyberpower	pdu_rittal	pdu_tripplite	kvm_dsr	kvm_mpu	kvm_aten	kvm_raritan
	Run Keyword If	${NGVERSION} >= 4.0 and '${ret}' == '0'  Append to List	${MUST_HAVE_TYPES}	pdu_cyberpower	usb_sensor
	Sort List	${TYPES}
	Sort List	${MUST_HAVE_TYPES}
	Log	${TYPES}
	Log	${MUST_HAVE_TYPES}
	Lists Should Be Equal	${TYPES}	${MUST_HAVE_TYPES}
	Sleep	5s
	GUI::ManagedDevices::Delete Devices	${DUMMY_DEVICE_CONSOLE_NAME}	${DUMMY_DEVICE_CONSOLE_NAME}2


*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
