*** Settings ***
Documentation	Test ask during login on device
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME_ADMIN}	${DEFAULT_USERNAME}
${PASSWORD_ADMIN}	${DEFAULT_PASSWORD}

${DEV_NAME}	test_ask_during_login
${DEV_IP}	127.0.0.1
${DEV_ASKPASS}	yes
${DEV_TYPE}	device_console
${DEV_STATUS}	enabled
${DEV_ASKPASS}	yes
${DEV_URL}	http://%IP
${PORT}	10001

*** Test Cases ***
Test Ask During Login
	[Setup]	SUITE:Setup
	SUITE:Add Device Ask During Login	${DEV_NAME}	${DEV_TYPE}	${DEV_IP}	${DEV_ASKPASS}	${DEV_STATUS}	${DEV_URL}	${PORT}
	Sleep	5
	GUI::Basic::Access::Tree::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5
	GUI::Basic::Wait Until Element Is Accessible	xpath=//span[@id="|${DEV_NAME}"]/a[@action="spmSession"]
	Click Element	xpath=//span[@id="|${DEV_NAME}"]/a[@action="spmSession"]
	GUI:Basic::Wait Until Window Exists	${DEV_NAME}
	Switch Window	title=${DEV_NAME}
	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
	Select Frame	xpath=//*[@id='termwindow']
	Sleep	10

	Press Keys	None	RETURN
	Press Keys	None	RETURN
	Press Keys	None	RETURN

	Sleep	5
	SUITE:Check Output	${USERNAME_ADMIN}
	Sleep	2
	Close Window
	Sleep	3
	${WINDOW_NAME}=	Get Window Names
	Switch Window	${WINDOW_NAME}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Sleep	3
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${DEV_NAME}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

SUITE:Add Device Ask During Login
	[Arguments]	${DEV_NAME}	${DEV_TYPE}	${DEV_IP}	${DEV_ASKPASS}	${DEV_STATUS}	${DEV_URL}	${PORT}
	GUI::Basic::Managed Devices::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=#addButton
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#saveButton
	Input Text	jquery=#spm_name	${DEV_NAME}
	Select From List By Value	jquery=#type	${DEV_TYPE}
	Input Text	jquery=#phys_addr	${DEV_IP}
	Wait Until Element Is Visible	askpassword
	Select Radio Button	askpassword	${DEV_ASKPASS}
	Select From List By Value	jquery=#status	${DEV_STATUS}
	Input Text	jquery=#url	${DEV_URL}
	Input Text	tcpport	${PORT}
	Sleep	2
	GUI::Basic::Click Element	//*[@id='saveButton']
	GUI::ManagedDevices::Wait For Managed Devices Table
	Table Should Contain	jquery=#SPMTable	${DEV_NAME}

SUITE:Check Output
	[Arguments]	${COMMAND}
	${CHECK_TAB}=	Run Keyword And Return Status	Should Not Contain	${COMMAND}	TAB
	Run Keyword If	${CHECK_TAB}	Press Keys	None	${COMMAND}	RETURN
	...	ELSE	Press Keys	None	${COMMAND}
	Sleep	1
	#${SELECTION}=	Execute JavaScript	term.selectAll(); return term.getSelection().trim();
	#Should Contain	${SELECTION}	[error.connection.failure] Could not establish a connection to device