*** Settings ***
Documentation	Test Skip authentication to access device (on/off regular/remote user)
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW	BUG_NG_9567

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME_ADMIN}	${DEFAULT_USERNAME}
${PASSWORD_ADMIN}	${DEFAULT_PASSWORD}
${DEVICE}	template
${REMOTESERVER_IP}	${TACACSSERVER_IP}
${REMOTESERVER_USERNAME}	${TACACSSERVER_USER1}
${REMOTESERVER_PASSWORD}	${TACACSSERVER_PASSWORD1}
${ID}	TACACS+
${TEST_IP_ALIAS_IPV4}	${IP_ALIAS_IPV4}
${TEST_IP_ALIAS_IPV6}	${IP_ALIAS_IPV6}

*** Test Cases ***
Test Skip authentication to access device - not checked
	GUI::Basic::Managed Devices::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Select Checkboxes	${DEVICE}
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Checkbox Should Not Be Selected	inbauth
	GUI::Basic::Access::Tree::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	GUI::Basic::Wait Until Element Is Accessible	xpath=//span[@id="|${DEVICE}"]/a[@action="spmSession"]
	Click Element	xpath=//span[@id="|${DEVICE}"]/a[@action="spmSession"]
	GUI:Basic::Wait Until Window Exists	${DEVICE}
	Switch Window	title=${DEVICE}
	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
	Select Frame	xpath=//*[@id='termwindow']
	Sleep	10s
	Press Keys	//body	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 ls
	Should Contain	${OUTPUT}	access/	system/	settings/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 pwd
	Should Contain	${OUTPUT}	/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 hostname
	Should Contain	${OUTPUT}	nodegrid

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 whoami
	Should Contain	${OUTPUT}	${DEFAULT_USERNAME}

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 TAB+TAB

	${COMMANDS}=	Create List	apply_settings	cd	change_password	commit
	...	event_system_audit	event_system_clear	exit	factory_settings
	...	hostname	ls	pwd	quit	reboot	revert	save_settings	set
	...	shell	show	shutdown	software_upgrade	system_certificate
	...	system_config_check	whoami
	FOR	${COMMAND}	IN	@{COMMANDS}
		Should Contain	${OUTPUT}	${COMMAND}
	END

	Unselect Frame
	[Teardown]	Close all Browsers

Test Check the box for Skip authentication to access device (NONE authentication)
	[Setup]	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add Console Device
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Select Checkboxes	${DEVICE}
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Checkbox Should Not Be Selected	inbauth

	GUI::Basic::Access::Tree::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Wait Until Element Is Accessible	xpath=//span[@id="|${DEVICE}"]/a[@action="spmSession"]
	Click Element	xpath=//span[@id="|${DEVICE}"]/a[@action="spmSession"]
	GUI:Basic::Wait Until Window Exists	${DEVICE}
	Switch Window	title=${DEVICE}
	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
	Select Frame	xpath=//*[@id='termwindow']
	Sleep	10s
	Press Keys	//body	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 ls
	Should Contain	${OUTPUT}	access/	system/	settings/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 pwd
	Should Contain	${OUTPUT}	/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 hostname
	Should Contain	${OUTPUT}	nodegrid

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 whoami
	Should Contain	${OUTPUT}	${DEFAULT_USERNAME}

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 TAB+TAB

	${COMMANDS}=	Create List	apply_settings	cd	change_password	commit
	...	event_system_audit	event_system_clear	exit	factory_settings
	...	hostname	ls	pwd	quit	reboot	revert	save_settings	set
	...	shell	show	shutdown	software_upgrade	system_certificate
	...	system_config_check	whoami
	FOR	${COMMAND}	IN	@{COMMANDS}
		Should Contain	${OUTPUT}	${COMMAND}
	END
	Unselect Frame
	Close All Browsers

	SUITE:User Login	${USERNAME_ADMIN}	${PASSWORD_ADMIN}
	GUI::ManagedDevices::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Select Checkboxes	${DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	inbauth
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

	GUI::Basic::Access::Tree::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Wait Until Element Is Accessible	xpath=//span[@id="|${DEVICE}"]/a[@action="spmSession"]
	Click Element	xpath=//span[@id="|${DEVICE}"]/a[@action="spmSession"]
	GUI:Basic::Wait Until Window Exists	${DEVICE}
	Switch Window	title=${DEVICE}
	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
	Select Frame	xpath=//*[@id='termwindow']
	Sleep	10s
	Press Keys	//body	RETURN
	Sleep	10s

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 ls
	Should Contain	${OUTPUT}	access/	system/	settings/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 pwd
	Should Contain	${OUTPUT}	/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 hostname
	Should Contain	${OUTPUT}	nodegrid

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 whoami
	Should Contain	${OUTPUT}	${DEFAULT_USERNAME}

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 TAB+TAB

	${COMMANDS}=	Create List	apply_settings	cd	change_password	commit
	...	event_system_audit	event_system_clear	exit	factory_settings
	...	hostname	ls	pwd	quit	reboot	revert	save_settings	set
	...	shell	show	shutdown	software_upgrade	system_certificate
	...	system_config_check	whoami
	FOR	${COMMAND}	IN	@{COMMANDS}
		Should Contain	${OUTPUT}	${COMMAND}
	END
	Unselect Frame
	Close All Browsers

Test Unchecked the box for Skip authentication to access device
	[Setup]	SUITE:Setup
	SUITE:Add Console Device
	GUI::ManagedDevices::Select Checkboxes	${DEVICE}
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	inbauth
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

	GUI::Basic::Access::Tree::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Wait Until Element Is Accessible	xpath=//span[@id="|${DEVICE}"]/a[@action="spmSession"]
	Click Element	xpath=//span[@id="|${DEVICE}"]/a[@action="spmSession"]

	GUI:Basic::Wait Until Window Exists	${DEVICE}
	Switch Window	title=${DEVICE}
	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
	Select Frame	xpath=//*[@id='termwindow']
	Sleep	10s
	Press Keys	//body	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 ls
	Should Contain	${OUTPUT}	access/	system/	settings/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 pwd
	Should Contain	${OUTPUT}	/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 hostname
	Should Contain	${OUTPUT}	nodegrid

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 whoami
	Should Contain	${OUTPUT}	${DEFAULT_USERNAME}

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 TAB+TAB

	${COMMANDS}=	Create List	apply_settings	cd	change_password	commit
	...	event_system_audit	event_system_clear	exit	factory_settings
	...	hostname	ls	pwd	quit	reboot	revert	save_settings	set
	...	shell	show	shutdown	software_upgrade	system_certificate
	...	system_config_check	whoami
	FOR	${COMMAND}	IN	@{COMMANDS}
		Should Contain	${OUTPUT}	${COMMAND}
	END
	Unselect Frame
	Close Browser

	SUITE:User Login	${USERNAME_ADMIN}	${PASSWORD_ADMIN}
	GUI::ManagedDevices::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Select Checkboxes	${DEVICE}
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Unselect Checkbox	inbauth
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Select Checkboxes	${DEVICE}
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Checkbox Should Not Be Selected	inbauth

	GUI::Basic::Access::Tree::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Wait Until Element Is Accessible	xpath=//span[@id="|${DEVICE}"]/a[@action="spmSession"]
	Click Element	xpath=//span[@id="|${DEVICE}"]/a[@action="spmSession"]
	GUI:Basic::Wait Until Window Exists	${DEVICE}
	Switch Window	title=${DEVICE}
	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
	Select Frame	xpath=//*[@id='termwindow']
	Sleep	10s
	Press Keys	//body	RETURN
	Sleep	10s

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 ls
	Should Contain	${OUTPUT}	access/	system/	settings/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 pwd
	Should Contain	${OUTPUT}	/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 hostname
	Should Contain	${OUTPUT}	nodegrid

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 whoami
	Should Contain	${OUTPUT}	${DEFAULT_USERNAME}

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 TAB+TAB

	${COMMANDS}=	Create List	apply_settings	cd	change_password	commit
	...	event_system_audit	event_system_clear	exit	factory_settings
	...	hostname	ls	pwd	quit	reboot	revert	save_settings	set
	...	shell	show	shutdown	software_upgrade	system_certificate
	...	system_config_check	whoami
	FOR	${COMMAND}	IN	@{COMMANDS}
		Should Contain	${OUTPUT}	${COMMAND}
	END

	Unselect Frame
	Close All Browsers

Test Skip authentication to access device not checked - remote user
	[Setup]	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add Console Device
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Select Checkboxes	${DEVICE}
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Checkbox Should Not Be Selected	inbauth
	SUITE:Add TACACS+ Server
	GUI::Basic::Logout

	Wait Until Keyword Succeeds	10	10	SUITE:User Login	${REMOTESERVER_USERNAME}	${REMOTESERVER_PASSWORD}

	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Wait Until Element Is Accessible	//*[@id="|${DEVICE}"]/td[2]/div/a[1]
	Click Element	//*[@id="|${DEVICE}"]/td[2]/div/a[1]
	GUI:Basic::Wait Until Window Exists	${DEVICE}
	Switch Window	title=${DEVICE}
	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
	Select Frame	xpath=//*[@id='termwindow']
	Sleep	10s
	Press Keys	//body	RETURN
	Sleep	10s

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 ls
	Should Contain	${OUTPUT}	access/	system/	settings/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 pwd
	Should Contain	${OUTPUT}	/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 hostname
	Should Contain	${OUTPUT}	nodegrid

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 whoami
	Should Contain	${OUTPUT}	${DEFAULT_USERNAME}

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 TAB+TAB

	${COMMANDS}=	Create List	apply_settings	cd	change_password	commit
	...	event_system_audit	event_system_clear	exit	factory_settings
	...	hostname	ls	pwd	quit	reboot	revert	save_settings	set
	...	shell	show	shutdown	software_upgrade	system_certificate
	...	system_config_check	whoami
	FOR	${COMMAND}	IN	@{COMMANDS}
		Should Contain	${OUTPUT}	${COMMAND}
	END

	Unselect Frame
	Close All Browsers

Test Check the box for Skip authentication to access device (NONE authentication) - remote user
	[Setup]	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add Console Device
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Select Checkboxes	${DEVICE}
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Checkbox Should Not Be Selected	inbauth
	SUITE:Add TACACS+ Server
	GUI::Basic::Logout

	Wait Until Keyword Succeeds	10	10	SUITE:User Login	${REMOTESERVER_USERNAME}	${REMOTESERVER_PASSWORD}

	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	GUI::Basic::Wait Until Element Is Accessible	//*[@id="|${DEVICE}"]/td[2]/div/a[1]
	Click Element	//*[@id="|${DEVICE}"]/td[2]/div/a[1]
	GUI:Basic::Wait Until Window Exists	${DEVICE}
	Switch Window	title=${DEVICE}
	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
	Select Frame	xpath=//*[@id='termwindow']
	Sleep	10s
	Press Keys	 //body	 RETURN
	Sleep	15s

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 ls
	Should Contain	${OUTPUT}	access/	system/	settings/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 pwd
	Should Contain	${OUTPUT}	/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 hostname
	Should Contain	${OUTPUT}	nodegrid

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 whoami
	Should Contain	${OUTPUT}	${DEFAULT_USERNAME}

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 TAB+TAB

	${COMMANDS}=	Create List	apply_settings	cd	change_password	commit
	...	event_system_audit	event_system_clear	exit	factory_settings
	...	hostname	ls	pwd	quit	reboot	revert	save_settings	set
	...	shell	show	shutdown	software_upgrade	system_certificate
	...	system_config_check	whoami
	FOR	${COMMAND}	IN	@{COMMANDS}
		Should Contain	${OUTPUT}	${COMMAND}
	END
	Unselect Frame
	Close Browser

	SUITE:User Login	${USERNAME_ADMIN}	${PASSWORD_ADMIN}
	GUI::ManagedDevices::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Select Checkboxes	${DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	inbauth
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout

	Wait Until Keyword Succeeds	10	10	SUITE:User Login	${REMOTESERVER_USERNAME}	${REMOTESERVER_PASSWORD}

	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	GUI::Basic::Wait Until Element Is Accessible	//*[@id="|${DEVICE}"]/td[2]/div/a[1]
	Click Element	//*[@id="|${DEVICE}"]/td[2]/div/a[1]
	GUI:Basic::Wait Until Window Exists	${DEVICE}
	Switch Window	title=${DEVICE}
	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
	Select Frame	xpath=//*[@id='termwindow']
	Sleep	10s
	Press Keys	//body	RETURN
	Sleep	15s

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 ls
	Should Contain	${OUTPUT}	access/	system/	settings/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 pwd
	Should Contain	${OUTPUT}	/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 hostname
	Should Contain	${OUTPUT}	nodegrid

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 whoami
	Should Contain	${OUTPUT}	${DEFAULT_USERNAME}

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 TAB+TAB

	${COMMANDS}=	Create List	apply_settings	cd	change_password	commit
	...	event_system_audit	event_system_clear	exit	factory_settings
	...	hostname	ls	pwd	quit	reboot	revert	save_settings	set
	...	shell	show	shutdown	software_upgrade	system_certificate
	...	system_config_check	whoami
	FOR	${COMMAND}	IN	@{COMMANDS}
		Should Contain	${OUTPUT}	${COMMAND}
	END

	Unselect Frame
	Close All Browsers

Test Unchecked the box for Skip authentication to access device - remote user
	[Setup]	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add Console Device
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Select Checkboxes	${DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	inbauth
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add TACACS+ Server
	GUI::Basic::Logout

	Wait Until Keyword Succeeds	10	10	SUITE:User Login	${REMOTESERVER_USERNAME}	${REMOTESERVER_PASSWORD}

	GUI::Basic::Access::Tree::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Wait Until Element Is Accessible	xpath=//span[@id="|${DEVICE}"]/a[@action="spmSession"]
	Click Element	xpath=//span[@id="|${DEVICE}"]/a[@action="spmSession"]

	GUI:Basic::Wait Until Window Exists	${DEVICE}
	Switch Window	title=${DEVICE}
	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
	Select Frame	xpath=//*[@id='termwindow']
	Sleep	15s
	Press Keys	//body	RETURN
	Sleep	15s

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 ls
	Should Contain	${OUTPUT}	access/	system/	settings/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 pwd
	Should Contain	${OUTPUT}	/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 hostname
	Should Contain	${OUTPUT}	nodegrid

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 whoami
	Should Contain	${OUTPUT}	${DEFAULT_USERNAME}

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 TAB+TAB

	${COMMANDS}=	Create List	apply_settings	cd	change_password	commit
	...	event_system_audit	event_system_clear	exit	factory_settings
	...	hostname	ls	pwd	quit	reboot	revert	save_settings	set
	...	shell	show	shutdown	software_upgrade	system_certificate
	...	system_config_check	whoami
	FOR	${COMMAND}	IN	@{COMMANDS}
		Should Contain	${OUTPUT}	${COMMAND}
	END

	Unselect Frame
	Close All Browsers

	SUITE:User Login	${USERNAME_ADMIN}	${PASSWORD_ADMIN}
	GUI::ManagedDevices::Open
	GUI::ManagedDevices::Select Checkboxes	${DEVICE}
	GUI::Basic::Edit
	Unselect Checkbox	inbauth
	GUI::Basic::Save

	GUI::ManagedDevices::Select Checkboxes	${DEVICE}
	GUI::Basic::Edit
	Checkbox Should Not Be Selected	inbauth

	SUITE:User Login	${USERNAME_ADMIN}	${PASSWORD_ADMIN}
	GUI::Basic::Logout

	Wait Until Keyword Succeeds	10	10	SUITE:User Login	${REMOTESERVER_USERNAME}	${REMOTESERVER_PASSWORD}

	GUI::Basic::Access::Tree::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Wait Until Element Is Accessible	xpath=//span[@id="|${DEVICE}"]/a[@action="spmSession"]
	Click Element	xpath=//span[@id="|${DEVICE}"]/a[@action="spmSession"]
	GUI:Basic::Wait Until Window Exists	${DEVICE}
	Switch Window	title=${DEVICE}
	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
	Select Frame	xpath=//*[@id='termwindow']
	Sleep	15s
	Press Keys	//body	RETURN
	Sleep	15s

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 ls
	Should Contain	${OUTPUT}	access/	system/	settings/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 pwd
	Should Contain	${OUTPUT}	/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 hostname
	Should Contain	${OUTPUT}	nodegrid

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 whoami
	Should Contain	${OUTPUT}	${DEFAULT_USERNAME}

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 TAB+TAB

	${COMMANDS}=	Create List	apply_settings	cd	change_password	commit
	...	event_system_audit	event_system_clear	exit	factory_settings
	...	hostname	ls	pwd	quit	reboot	revert	save_settings	set
	...	shell	show	shutdown	software_upgrade	system_certificate
	...	system_config_check	whoami
	FOR	${COMMAND}	IN	@{COMMANDS}
		Should Contain	${OUTPUT}	${COMMAND}
	END

	Unselect Frame
	Close All Browsers


Test Skip authentication to access device - checked (IP_ALIAS IPv4)
	[Setup]	SUITE:Setup
	SUITE:User Login	${USERNAME_ADMIN}	${PASSWORD_ADMIN}
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add Console Device
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Select Checkboxes	${DEVICE}

	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Input Text	username	${USERNAME_ADMIN}
	Input Text	passwordfirst	${PASSWORD_ADMIN}
	Input Text	passwordconf	${PASSWORD_ADMIN}
	Select Checkbox	inbauth
	Sleep	5s
	Select Checkbox	inbipEnable
	Input Text	inbipAddr	${TEST_IP_ALIAS_IPV4}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers
	${BROWSER}	Convert To Lower Case	${BROWSER}
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nOpening Browser: ${BROWSER}	INFO	console=yes
	Run Keyword If	'${BROWSER}' == 'chrome' or '${BROWSER}' == 'headlesschrome'	Wait Until Keyword Succeeds	3x	3s	Open Browser	https://${TEST_IP_ALIAS_IPV4}	${BROWSER}	options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors")
	Run Keyword If	'${BROWSER}' == 'firefox' or '${BROWSER}' == 'headlessfirefox'	Wait Until Keyword Succeeds	3x	3s	Open Browser	https://${TEST_IP_ALIAS_IPV4}	${BROWSER}	options=set_preference("moz:dom.disable_beforeunload", "true")
	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\nBrowser ${BROWSER} opened	INFO	console=yes
	Maximize Browser Window
	GUI::Basic::Direct Login
	Sleep	5s

	GUI:Basic::Wait Until Window Exists	${DEVICE}
	Switch Window	 title=${DEVICE}
	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
	Select Frame	xpath=//*[@id='termwindow']
	Sleep	15s
	Press Keys	//body	RETURN
	Sleep	15s

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 ls
	Should Contain	${OUTPUT}	access/	system/	settings/

	Unselect Frame
	Close All Browsers

Test Skip authentication to access device - not checked (IP_ALIAS IPv4)
	[Setup]	SUITE:Setup
	SUITE:User Login	${USERNAME_ADMIN}	${PASSWORD_ADMIN}
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add Console Device
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Select Checkboxes	${DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Input Text	username	${USERNAME_ADMIN}
	Input Text	passwordfirst	${PASSWORD_ADMIN}
	Input Text	passwordconf	${PASSWORD_ADMIN}
	Checkbox Should Not Be Selected	inbauth
	Sleep	5s
	Select Checkbox	inbipEnable
	Input Text	inbipAddr	${TEST_IP_ALIAS_IPV4}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Close Browser
	${BROWSER}	Convert To Lower Case	${BROWSER}
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nOpening Browser: ${BROWSER}	INFO	console=yes
	Run Keyword If	'${BROWSER}' == 'chrome' or '${BROWSER}' == 'headlesschrome'	Wait Until Keyword Succeeds	3x	3s	Open Browser	https://${TEST_IP_ALIAS_IPV4}	${BROWSER}	options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors")
	Run Keyword If	'${BROWSER}' == 'firefox' or '${BROWSER}' == 'headlessfirefox'	Wait Until Keyword Succeeds	3x	3s	Open Browser	https://${TEST_IP_ALIAS_IPV4}	${BROWSER}	options=set_preference("moz:dom.disable_beforeunload", "true")
	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\nBrowser ${BROWSER} opened	INFO	console=yes
	Maximize Browser Window
	GUI::Basic::Direct Login

	GUI:Basic::Wait Until Window Exists	${DEVICE}
	Switch Window	title=${DEVICE}
	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
	Select Frame	xpath=//*[@id='termwindow']
	Sleep	10s
	Press Keys	None	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output	 ls
	Should Contain	${OUTPUT}	access/	system/	settings/

	Unselect Frame
	Close Window
	[Teardown]	SUITE:Teardown

Test Skip authentication to access device - checked (IP_ALIAS IPv6)
	[Setup]	SUITE:Setup
	SUITE:User Login	${USERNAME_ADMIN}	${PASSWORD_ADMIN}
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add Console Device
	GUI::ManagedDevices::Select Checkboxes	${DEVICE}
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Input Text	username	${USERNAME_ADMIN}
	Input Text	passwordfirst	${PASSWORD_ADMIN}
	Input Text	passwordconf	${PASSWORD_ADMIN}
	Select Checkbox	inbauth
	Select Checkbox	inbipEnable
	Sleep	5
	Input Text	inbipAddr	${TEST_IP_ALIAS_IPV6}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Close Browser
	${BROWSER}	Convert To Lower Case	${BROWSER}
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nOpening Browser: ${BROWSER}	INFO	console=yes
	Run Keyword If	'${BROWSER}' == 'chrome' or '${BROWSER}' == 'headlesschrome'	Wait Until Keyword Succeeds	3x	3s	Open Browser	https://[${TEST_IP_ALIAS_IPV6}]/	${BROWSER}	options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors")
	Run Keyword If	'${BROWSER}' == 'firefox' or '${BROWSER}' == 'headlessfirefox'	Wait Until Keyword Succeeds	3x	3s	Open Browser	https://[${TEST_IP_ALIAS_IPV6}]/	${BROWSER}	options=set_preference("moz:dom.disable_beforeunload", "true")
	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\nBrowser ${BROWSER} opened	INFO	console=yes
	Maximize Browser Window
	GUI::Basic::Direct Login
	GUI::Basic::Spinner Should Be Invisible
	GUI:Basic::Wait Until Window Exists	${DEVICE}
	Switch Window	title=${DEVICE}
	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
	Select Frame	xpath=//*[@id='termwindow']
	Sleep	10
	Press Keys	None	RETURN
	${OUTPUT}=	GUI:Access::Console Command Output	ls
	Should Contain	${OUTPUT}	access/	system/	settings/

	Unselect Frame
	Close Window
	[Teardown]	SUITE:Teardown

Test Skip authentication to access device - not checked (IP_ALIAS IPv6)
	[Setup]	SUITE:Setup
	SUITE:User Login	${USERNAME_ADMIN}	${PASSWORD_ADMIN}
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add Console Device
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Select Checkboxes	${DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Input Text	username	${USERNAME_ADMIN}
	Input Text	passwordfirst	${PASSWORD_ADMIN}
	Input Text	passwordconf	${PASSWORD_ADMIN}
	Checkbox Should Not Be Selected	inbauth
	Select Checkbox	inbipEnable
	Sleep	5
	Input Text	inbipAddr	${TEST_IP_ALIAS_IPV6}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Close Browser
	${BROWSER}	Convert To Lower Case	${BROWSER}
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nOpening Browser: ${BROWSER}	INFO	console=yes
	Run Keyword If	'${BROWSER}' == 'chrome' or '${BROWSER}' == 'headlesschrome'	Wait Until Keyword Succeeds	3x	3s	Open Browser	https://[${TEST_IP_ALIAS_IPV6}]/	${BROWSER}	options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors")
	Run Keyword If	'${BROWSER}' == 'firefox' or '${BROWSER}' == 'headlessfirefox'	Wait Until Keyword Succeeds	3x	3s	Open Browser	https://[${TEST_IP_ALIAS_IPV6}]/	${BROWSER}	options=set_preference("moz:dom.disable_beforeunload", "true")
	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\nBrowser ${BROWSER} opened	INFO	console=yes
	Maximize Browser Window
	GUI::Basic::Direct Login

	GUI:Basic::Wait Until Window Exists	${DEVICE}
	Select	title=${DEVICE}
	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
	Select Frame	xpath=//*[@id='termwindow']
	Sleep	10
	Press Keys	None	RETURN
	${OUTPUT}=	GUI:Access::Console Command Output	ls
	Should Contain	${OUTPUT}	access/	system/	settings/

	Unselect Frame
	Close Window
	[Teardown]	SUITE:Teardown

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add Console Device
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add TACACS+ Server
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Local Accounts::Add	dummy

SUITE:Teardown
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Local Accounts::Delete	dummy
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE}
	GUI::Security::Authentication Delete TACACS+ Server If Exists	${ID}
	GUI::Basic::Logout And Close Nodegrid

SUITE:User Login
	[Arguments]	${USERNAME}=${DEFAULT_USERNAME}	${PASSWORD}=${DEFAULT_PASSWORD}
	GUI::Basic::Open And Login Nodegrid	${USERNAME_ADMIN}	${PASSWORD_ADMIN}

SUITE:Add Console Device
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE}
	GUI::ManagedDevices::Add Device	${DEVICE}	device_console	127.0.0.1	${USERNAME_ADMIN}	no	${PASSWORD_ADMIN}	enabled

SUITE:Add TACACS+ Server
	GUI::Security::Authentication Delete TACACS+ Server If Exists	${ID}
	GUI::Security::Authentication Add TACACS+ Server	${REMOTESERVER_IP}
