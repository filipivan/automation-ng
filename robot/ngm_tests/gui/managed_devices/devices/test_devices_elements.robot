*** Settings ***
Documentation	Testing elements on  managed devices -> devices for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE      NON-CRITICAL
Default Tags	EXCLUDEIN3_2

*** Test Cases ***
Test Devices Header
    Run Keyword If  '${NGVERSION}' >= '5.4'   Page Should Contain      Access: ( Licensed | Used | Leased | Available ):    Monitoring: ( Licensed | Used | Leased | Available ):
    Run Keyword If  '${NGVERSION}' >= '5.0' and '${NGVERSION}' < '5.4'  Page Should Contain    Access: ( Licensed | Used | Available ):    Monitoring: ( Licensed | Used | Available ):

Test NonAccess Buttons
    Element Should Be Enabled       //*[@id="addButton"]
	FOR    ${I}    IN RANGE    2    9
        Element Should Be Disabled      //*[@id="nonAccessControls"]/input[${I}]
	END
Test Table Exist
    Page Should Contain Element     //*[@id="SPMTable"]

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Open
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Logout And Close Nodegrid
