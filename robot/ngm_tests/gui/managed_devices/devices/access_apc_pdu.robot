*** Settings ***
Documentation	Test pdu operations
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	DEPENDENCE_PDU	MISSING_DEVICE	NON-CRITICAL	NEED-REVIEW

#Need apc pdu to be available on automation side

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${APC_PDU_DEVICE}	test_apc
${APC_DEVICE_TYPE}	pdu_apc
${APC_USER}	apc
${APC_PASSWORD}	apc
${PDU_ID}	1
${OUTLET_ID}	2
${OUTLET1_NAME}	Outlet_2
${TEST_DEVICE}	mergeoutlet_apc
${TEST_DEVICE_TYPE}	device_console
${COMMAND}	Outlet

*** Test Cases ***
Test Case To Add pdu device/protocol/SNMPV2
	SUITE:Setup
	SUITE:Add apc pdu Managed device
	SUITE:Add SNMPV2 community
	SUITE:Add SNMP protocol

Test case to check outlets discovery
	Auto discovery for device
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	${APC_PDU_DEVICE}
	Click Link	${APC_PDU_DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spmoutlets_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Out1NSC-DemoDontTurnOFF
	Page Should Contain	Out1NSC-DemoDontTurnOFF
	Page Should Contain	Outlet_2
	Page Should Contain	Outlet_3

Test case to do pdu operations on Access table
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="|${APC_PDU_DEVICE}"]/td[1]/div/a/span
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	css=.btn:nth-child(4)
	Click Button	css=.btn:nth-child(4)
	GUI::Basic::Spinner Should Be Invisible
	Page Should not contain	unknown
	Select Checkbox	//*[@id="${APC_PDU_DEVICE}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[1]/input
	Click Button	//*[@id="nonAccessControls"]/input[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	Wait Until Element Contains	//*[@id="${APC_PDU_DEVICE}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[6]	Off
	Select Checkbox	//*[@id="${APC_PDU_DEVICE}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[1]/input
	Click Button	//*[@id="nonAccessControls"]/input[1]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	Wait Until Element Contains	//*[@id="${APC_PDU_DEVICE}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[6]	On
	Select Checkbox	//*[@id="${APC_PDU_DEVICE}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[1]/input
	Click Button	//*[@id="nonAccessControls"]/input[3]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	Wait Until Element Contains	//*[@id="${APC_PDU_DEVICE}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[6]	Off
	Select Checkbox	//*[@id="${APC_PDU_DEVICE}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[1]/input
	Click Button	//*[@id="nonAccessControls"]/input[1]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	//*[@id="${APC_PDU_DEVICE}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[6]	On
	Close Browser

Test case to add managed device merge outlets
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=spm_name	${TEST_DEVICE}
	Select From List By Label	id=type	${TEST_DEVICE_TYPE}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	${TEST_DEVICE}
	Click Link	${TEST_DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spmcommands_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	id=command	${COMMAND}
	GUI::Basic::Spinner Should Be Invisible
	Sleep	10s
	GUI::Basic::Spinner Should Be Invisible
	Double Click Element	//div[@id="outlets"]//option[@value="${APC_PDU_DEVICE}"]
	Sleep	10s
	GUI::Basic::Spinner Should Be Invisible
	Double Click Element	//*[@id="outlets"]/div[2]/div[3]/select/option
	GUI::Basic::Spinner Should Be Invisible
	Double Click Element	//*[@id="outlets"]/div[2]/div[4]/select/option[1]
	Click Button	//*[@id="outlets"]/div[2]/div[5]/button[1]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save

Test to perform merge outlet operations
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep	15s
	Wait Until Page Contains Element	xpath=//tr[@id='|mergeoutlet_apc']/td/div/a/span
	Click Element	xpath=//tr[@id='|mergeoutlet_apc']/td/div/a/span
	Wait Until Page Contains Element	css=.btn:nth-child(4)
	Click Button	css=.btn:nth-child(4)
	GUI::Basic::Spinner Should Be Invisible
	Page Should not contain	unknown

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::ManagedDevices::Delete Device If Exists	${APC_PDU_DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${TEST_DEVICE}

SUITE:Add apc pdu Managed device
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=spm_name	${APC_PDU_DEVICE}
	Select From List By Label	id=type	${APC_DEVICE_TYPE}
	Input Text	id=phys_addr	${APC_IP}
	Select From List By Label	id=status	On-demand
	GUI::Basic::Save

SUITE:Add SNMPV2 community
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Link	${APC_PDU_DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spmmgmt_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="snmp_proto_enabled"]
	Input Text	//*[@id="community2"]	private
	GUI::Basic::Save

SUITE:Add SNMP protocol
	Click Element	//*[@id="spmcommands_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="Outlet"]
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	//*[@id="protocol"]	SNMP
	GUI::Basic::Save

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${APC_PDU_DEVICE}
	GUI::ManagedDevices::Delete Device If Exists	${TEST_DEVICE}
	GUI::Basic::Logout and Close Nodegrid

Auto discovery for device
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Auto Discovery::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="discovery_now"]
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Disabled	//*[@id="discovernow"]
	Page Should Contain Element	//*[@id="discovernowTable"]
	Select Checkbox	//*[@id="pdu|${APC_PDU_DEVICE}"]/td[1]/input
	Click Button	//*[@id="discovernow"]
	GUI::Basic::Spinner Should Be Invisible