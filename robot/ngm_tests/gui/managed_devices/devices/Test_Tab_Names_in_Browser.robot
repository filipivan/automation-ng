*** Settings ***
Documentation	Test To Check Tab Names in Browsers
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4

*** Variables ***
${Name}     test
${Type}     device_console
${Ip_address}       10.0.0.1
${cmd-kvm}      KVM
${cmd-ssh}      SSH
${cmd-telnet}       Telnet
${window_title}     nodegrid - Nodegrid

*** Test Cases ***
Test case to add a device
    GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${Name}
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text      //*[@id='spm_name']	${Name}
	Select From List By Label       //*[@id="type"]	${Type}
	Input Text	id=phys_addr        ${Ip_address}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Table Should Has Row With Id	SPMTable	${Name}
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain     ${Name}

Test case to add commands
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Setup
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox     //*[@id="${Name}"]/td[1]/input
	Click Element       xpath=//a[contains(text(),'${Name}')]
	Sleep       5s
	Click Element       css=#spmcommands_nav > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	id=command	${cmd-kvm}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep       3s
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	id=command	${cmd-ssh}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep       3s
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	id=command	${cmd-telnet}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain     ${Name}

Test Case to Check title of the window
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:To Check SSH Window
	SUITE:To Check Telent Window
	SUITE:To Check KVM Window

Test Case to delete the device
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	#click element		//*[@id="modal"]/div/div/div[1]/button
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${Name}

Test Case to check the main page window title
	Switch Window	MAIN
	Title Should be     ${window_title}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
    Close All Browsers

SUITE:To Check SSH Window
	SUITE:Setup
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	click element       xpath=//span[contains(.,'${Name}')]
	Sleep       5s
	click element       xpath=//button[contains(.,'SSH')]
	GUI::Basic::Spinner Should Be Invisible
	Sleep       10s
	Switch Window       ${Name} - ${cmd-ssh}
	Select Frame        xpath=//*[@id='termwindow']
	Title Should be     ${Name} - ${cmd-ssh}
	Sleep       10s
	Switch Window	MAIN
	wait until page contains element		//*[@id="modal"]/div/div/div[1]/button
	click element		//*[@id="modal"]/div/div/div[1]/button

SUITE:To Check Telent Window
	SUITE:Setup
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	click element       xpath=//span[contains(.,'${Name}')]
	Sleep       5s
	click element       xpath=//button[contains(.,'Telnet')]
	GUI::Basic::Spinner Should Be Invisible
	Sleep       10s
	Switch Window       ${Name} - ${cmd-telnet}
	Select Frame        xpath=//*[@id='termwindow']
	Title Should be     ${Name} - ${cmd-telnet}
	Sleep       10s
	Switch Window	MAIN
	wait until page contains element		//*[@id="modal"]/div/div/div[1]/button
	click element		//*[@id="modal"]/div/div/div[1]/button

SUITE:To Check KVM Window
	SUITE:Setup
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	click element       xpath=//span[contains(.,'${Name}')]
	Sleep       5s
	click element       xpath=//input[@id='kvmSession']
	GUI::Basic::Spinner Should Be Invisible
	Sleep       10s
	Switch Window       ${Name} - ${cmd-kvm}
	Sleep       10s
	Switch Window	MAIN
	wait until page contains element		//*[@id="modal"]/div/div/div[1]/button
	click element		//*[@id="modal"]/div/div/div[1]/button