*** Settings ***
Documentation	Testing changing status on  managed devices -> devices for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***
Test Enable, Disable, and On Demand
    GUI::ManagedDevices::Add Dummy Device Console       Dummy   device_console  ${EMPTY}  ${EMPTY}  ${EMPTY}  no  ${EMPTY}  ondemand
    Element Should Contain      //*[@id="Dummy"]/td[5]  	On-demand
    Select Checkbox       //*[@id="Dummy"]/td[1]/input
    Click Element       //*[@id="nonAccessControls"]/input[6]
    GUI::Basic::Spinner Should Be Invisible
    Element Should Contain      //*[@id="Dummy"]/td[5]  	Enabled
    Select Checkbox      //*[@id="Dummy"]/td[1]/input
    Click Element       //*[@id="nonAccessControls"]/input[7]
    Wait Until Element Contains  //*[@id="Dummy"]/td[5]  	Disabled
    Select Checkbox    //*[@id="Dummy"]/td[1]/input
    Click Element    //*[@id="nonAccessControls"]/input[8]
    Wait Until Element Contains   //*[@id="Dummy"]/td[5]  	On-demand

Delete Devices
    GUI::ManagedDevices::Delete Devices     Dummy
    GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
    GUI::ManagedDevices::Open
    GUI::ManagedDevices::Delete Device If Exists    Dummy
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid