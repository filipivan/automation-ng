*** Settings ***
Documentation	Test pdu operations for metered/non-switched CPI pdu
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	DEPENDENCE_PDU	CHROME	FIREFOX

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${CPI_METERED_PDU}	test_metered_cpi
${CPI_METERED_TYPE}	pdu_cpi
${CPI_METERED_IP}	192.168.3.70
${CPI_METERED_USER}	admin
${CPI_METERED_PASSWORD}	admin
${CPI_ID}	1
${CPI_OUTLET_ID}	2
${CPI_OUTLET1_NAME}	OutletName1Chain
${CPI_OUTLET2}	OutletName2Chain
${CPI_OUTLET3}	OutletName3Chain
${TEST_DEVICE}	mergeoutlet_meteredcpi
${TEST_DEVICE_TYPE}	device_console
${OUTLET_COMMAND}	Outlet

*** Test Cases ***
Test Case To Add Geist pdu/protocol/SNMPV2
	[Tags]	NON-CRITICAL	NEED-REVIEW	MISSING_DEVICE
	#we need to have the Geist pdu metered on Automation side
	SUITE:Add CPI metered pdu Managed device
	SUITE:Add SNMPV2 community

Test case to check outlets discovery
	[Tags]	NON-CRITICAL	NEED-REVIEW	MISSING_DEVICE
	Auto discovery for device
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	${CPI_METERED_PDU}
	Click Link	${CPI_METERED_PDU}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spmoutlets_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	${CPI_OUTLET1_NAME}
	Page Should Contain	${CPI_OUTLET2}
	Page Should Contain	${CPI_OUTLET3}

Test case to do pdu operations on Access table page
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If 	'${NGVERSION}' <= '5.6'	Click Element	//*[@id="|${CPI_METERED_PDU}"]/td[1]/div/a/span
	...	ELSE	click element		//*[@id="|${CPI_METERED_PDU}|"]/td[1]/div/a/span
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	css=.btn:nth-child(4)
	Click Button	css=.btn:nth-child(4)
	GUI::Basic::Spinner Should Be Invisible
	Page Should not contain	unknown

Test case to add managed for merging outlet
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text  id=spm_name	  ${TEST_DEVICE}
	Select From List By Label	id=type	${TEST_DEVICE_TYPE}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${TEST_DEVICE}

Test case to merge outlet
	[Tags]	NON-CRITICAL	NEED-REVIEW	MISSING_DEVICE
	Wait Until Page Contains	${TEST_DEVICE}
	Click Link	${TEST_DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spmcommands_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	id=command	${OUTLET_COMMAND}
	GUI::Basic::Spinner Should Be Invisible
	Sleep	10s
	GUI::Basic::Spinner Should Be Invisible
	Double Click Element	//div[@id="outlets"]//option[@value="${CPI_METERED_PDU}"]
	Sleep	10s
	GUI::Basic::Spinner Should Be Invisible
	Double Click Element	//*[@id="outlets"]/div[2]/div[3]/select/option
	GUI::Basic::Spinner Should Be Invisible
	Double Click Element	//*[@id="outlets"]/div[2]/div[4]/select/option[1]
	Click Button	//*[@id="outlets"]/div[2]/div[5]/button[1]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save

Test to perform merge outlet operations
	[Tags]	NON-CRITICAL	NEED-REVIEW	MISSING_DEVICE
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait until Page Contains	${TEST_DEVICE}
	Click Element	//*[@id="|${TEST_DEVICE}"]/td[1]/div/a/span
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	css=.btn:nth-child(5)
	Click Button	css=.btn:nth-child(5)
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	//*[@id="commandresponse"]	OFF

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::ManagedDevices::Delete Device If Exists	${CPI_METERED_PDU}
	GUI::ManagedDevices::Delete Device If Exists	${TEST_DEVICE}

SUITE:Add CPI metered pdu Managed device
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text  id=spm_name	 ${CPI_METERED_PDU}
	Select From List By Label	id=type	${CPI_METERED_TYPE}
	Input Text  id=phys_addr	${CPI_METERED_IP}
	Select From List By Label	id=status	On-demand
	GUI::Basic::Save

SUITE:Add SNMPV2 community
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Link	${CPI_METERED_PDU}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spmmgmt_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="snmp_proto_enabled"]
	Input Text	//*[@id="community2"]	private
	GUI::Basic::Save

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${CPI_METERED_PDU}
	sleep	  5s
	GUI::ManagedDevices::Delete Device If Exists	${TEST_DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	close all browsers

Auto discovery for device
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Auto Discovery::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="discovery_now"]
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Disabled	//*[@id="discovernow"]
	Page Should Contain Element	//*[@id="discovernowTable"]
	Select Checkbox	//*[@id="pdu|${CPI_METERED_PDU}"]/td[1]/input
	Click Button	//*[@id="discovernow"]
	GUI::Basic::Spinner Should Be Invisible