*** Settings ***
Documentation	Editing managed devices -> Preferences for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Variable ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}


*** Test Cases ***
Test Session Preferences Elements
    Click Element       //*[@id="spm_session_pref"]/span
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain Element     //*[@id="discSeq"]

Test Session Preferences Error
    ${RAND}=    Evaluate    random.randint(1,9)     modules=random
    gui::auditing::auto input tests  discSeq    asd     ^#$     **     &^t     :"?     ^p${RAND}
    Input Text      //*[@id="discSeq"]        ${EMPTY}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible

Test Power Menu Elements
    Click Element       //*[@id="spm_preferences"]/span
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain Element     //*[@id="exitOption"]
    Page Should Contain Element     //*[@id="exitLabel"]
    Page Should Contain Element     //*[@id="statusOption"]
    Page Should Contain Element     //*[@id="statusLabel"]
    Page Should Contain Element     //*[@id="onOption"]
    Page Should Contain Element     //*[@id="onLabel"]
    Page Should Contain Element     //*[@id="offOption"]
    Page Should Contain Element     //*[@id="offLabel"]
    Page Should Contain Element     //*[@id="cycleOption"]
    Page Should Contain Element     //*[@id="cycleLabel"]

Test Empty Label Error
    Input Text      //*[@id="exitLabel"]        ${EMPTY}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    Element Should Contain      //*[@id="errormsg"]     Validation error.
    Input Text      //*[@id="exitLabel"]        Exit

Test Error Throw Exit Option
    Dropdown Loop Testing  exitOption   1

Test Error Throw Menu Option
    Dropdown Loop Testing  statusOption     2

Test Error Throw Power On Option
    Dropdown Loop Testing  onOption     3

Test Error Throw Power Off Option
    Dropdown Loop Testing  offOption    4

Test Error Throw Power Cycle Option
    Dropdown Loop Testing   cycleOption     5


*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Preferences::Open Tab
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

Dropdown Loop Testing
    [Arguments]     ${LOC}  ${NUM}
    FOR    ${I}    IN RANGE    1   6
		${EQ}=      run keyword and return status   Should Not Be Equal     '${I}'    '${NUM}'
		${IS}=      Convert To String   ${I}
		run keyword if  ${EQ}   run keyword and continue on failure  Check Error Throw  ${LOC}  ${IS}
	END
    Select From List By Value   jquery=#${LOC}  ${NUM}
    Click element   jquery=#saveButton

Check Error Throw
    [Arguments]     ${LOC}  ${NUM}
    Select From List By Value   jquery=#${LOC}  ${NUM}
    Click element   jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    ${TST}=     Execute Javascript      var ro=document.getElementById('${LOC}');if(ro==null){return false;}var er=ro.parentNode.nextSibling;if(er==null){return false;}return true;
    Should Be True     ${TST}
