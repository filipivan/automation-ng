*** Settings ***
Documentation	Test for Tabular View Configuration
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE      
Default Tags	EXCLUDEIN3_2

*** Variables ***
${Name}     Testing
${DEVICE_TYPE}     device_console
${DEVICE}       Test
${CUSTOM_VALUE}     zpe_nsr
${Edit_Custom_value}        Automation

*** Test Cases ***
Test Case to Add Pre-defined Columns and Custom Coulmns
    SUITE:To Add Pre-defined Columns
    SUITE:To Add Managed Device and Custom fields

Test to Validate the Custom feild and Value on Access Page
	[Tags]	NON-CRITICAL	BUG_NG_11500
    SUITE:To Check Add Custom field and value to show on Access Page

Test Case to Edit Custom Field Value
	[Tags]	NON-CRITICAL	BUG_NG_11500
    SUITE:Edit Custom Fields

Test Case to Delete Pre-defined Columns and Custom Coulmns
    SUITE:To Delete Configuration and custom field value
    SUITE:To Delete the custom field value
    SUITE:Delete Managed device

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:To Add Pre-defined Columns
    GUI::Basic::Managed Devices::Preferences::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    click element       xpath=//span[contains(.,'Views')]
    GUI::Basic::Spinner Should Be Invisible
    Click Element       //*[@id="cols"]/div/div[1]/div[1]/select/option[1]      ###Type
    click Element       xpath=//button[contains(.,'Add ►')]
    Click Element       //*[@id="cols"]/div/div[1]/div[1]/select/option[1]      ###Mode
    click Element       xpath=//button[contains(.,'Add ►')]
    Click Element       //*[@id="cols"]/div/div[1]/div[1]/select/option[1]      ###IP Address
    click Element       xpath=//button[contains(.,'Add ►')]
    Click Element       //*[@id="cols"]/div/div[1]/div[1]/select/option[1]      ###Nodegrid Host
    click Element       xpath=//button[contains(.,'Add ►')]
    Click Element       //*[@id="cols"]/div/div[1]/div[1]/select/option[1]      ###Groups
    click Element       xpath=//button[contains(.,'Add ►')]
    click element       xpath=//input[@id='custom_cols']
    input text      xpath=//input[@id='custom_cols']        ${Name}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible

SUITE:To Add Managed Device and Custom fields
    GUI::ManagedDevices::Add Device    ${DEVICE}  ${DEVICE_TYPE}
    click element       xpath=//a[contains(text(),'Test')]
    GUI::Basic::Spinner Should Be Invisible
    click element       xpath=//span[contains(.,'Custom Fields')]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	input text      xpath=//input[@id='custom_name']        ${Name}
	input text      xpath=//input[@id='custom_value']       ${CUSTOM_VALUE}
	GUI::Basic::Save If Configuration Changed
	GUI::Basic::Spinner Should Be Invisible

SUITE:Edit Custom Fields
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    click element       xpath=//a[contains(text(),'Test')]
    GUI::Basic::Spinner Should Be Invisible
    click element       xpath=//span[contains(.,'Custom Fields')]
    GUI::Basic::Spinner Should Be Invisible
    select checkbox       xpath=//tr[@id='Testing']/td/input
    click element       xpath=//input[@id='editButton']
    GUI::Basic::Spinner Should Be Invisible
    input text      xpath=//input[@id='custom_value']       ${Edit_Custom_value}
    GUI::Basic::Save If Configuration Changed
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    wait until page contains element        xpath=//span[contains(.,'Automation')]

SUITE:To Check Add Custom field and value to show on Access Page
    GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    click element       xpath=//span[contains(.,'Columns')]
    wait until page contains element        name=Testing
    click element      css=#col_custom0 > #col_custom0
    wait until page contains element   xpath=//th[contains(.,'Testing')]
    wait until page contains element        xpath=//span[contains(.,'zpe_nsr')]
    wait until page contains element     xpath=//th[contains(.,'Type')]
    wait until page contains element    xpath=//th[contains(.,'IP Address')]
    wait until page contains element   xpath=//th[contains(.,'Groups')]
    wait until page contains element   xpath=//th[contains(.,'Nodegrid Host')]
    wait until page contains element   xpath=//th[contains(.,'Mode')]

SUITE:To Delete Configuration and custom field value
    GUI::Basic::Managed Devices::Preferences::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    click element       xpath=//span[contains(.,'Views')]
    GUI::Basic::Spinner Should Be Invisible
    click element       //*[@id="cols"]/div/div[1]/div[3]/select/option[1]
    click element       xpath=//button[contains(.,'◄ Remove')]
    click element       //*[@id="cols"]/div/div[1]/div[3]/select/option[1]
    click element       xpath=//button[contains(.,'◄ Remove')]
    click element       //*[@id="cols"]/div/div[1]/div[3]/select/option[1]
    click element       xpath=//button[contains(.,'◄ Remove')]
    click element       //*[@id="cols"]/div/div[1]/div[3]/select/option[1]
    click element       xpath=//button[contains(.,'◄ Remove')]
    click element       //*[@id="cols"]/div/div[1]/div[3]/select/option[1]
    click element       xpath=//button[contains(.,'◄ Remove')]
    clear element text      xpath=//input[@id='custom_cols']
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    sleep       5s
    click element       xpath=//span[contains(.,'Columns')]
    unselect checkbox       name=Groups
    unselect checkbox       name=IP Address
    unselect checkbox       name=Mode
    unselect checkbox       name=Nodegrid Host
    unselect checkbox       name=Type

SUITE:To Delete the custom field value
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    click element       xpath=//a[contains(text(),'Test')]
    GUI::Basic::Spinner Should Be Invisible
    click element       xpath=//span[contains(.,'Custom Fields')]
    GUI::Basic::Spinner Should Be Invisible
    click element       xpath=//tr[@id='Testing']/td/input
    Click Element       //*[@id="delButton"]
    GUI::Basic::Spinner Should Be Invisible

SUITE:Delete Managed device
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    select checkbox        xpath=//tr[@id='Test']/td/input
    Click Element       //*[@id="delButton"]
    handle alert
    GUI::Basic::Spinner Should Be Invisible