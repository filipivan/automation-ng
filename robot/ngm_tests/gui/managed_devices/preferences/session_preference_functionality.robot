*** Settings ***
Documentation	Testing--> Add Shortcut to Close Device Sessions
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	NON-CRITICAL	NEED-REVIEW
Default Tags	EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${USERNAME_ROOT}	root
${PASSWORD_ROOT}	${ROOT_PASSWORD}
${DEVICE_TYPE}	device_console
${DEVICE_NAME}	target_device
${DISCONNECTION_HOTKEY}	~.
${HOST_IP}	127.0.0.1
${exit_session}	exit

*** Test Cases ***
Test Case To Add Shortcut to Close Device Sessions By Enabling Disconnection Hotkey
	GUI::Basic::Managed Devices::Preferences::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_session_pref"]/span
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="discSeq"]	${DISCONNECTION_HOTKEY}
	select Checkbox	//*[@id="terminatesession"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="|${DEVICE_NAME}"]/td[2]/div/a[1]
	sleep	5s
	Run Keyword If	'${NGVERSION}'<='5.4'	Switch Window	title=${DEVICE_NAME}
	Run Keyword If	'${NGVERSION}'>'5.4'	Switch Window	title=${DEVICE_NAME} - Console
	Wait Until Page Contains Element	//iframe[@id='termwindow']
	Select Frame	//iframe[@id='termwindow']
	Sleep	5s
	Press Keys	None	RETURN
	Press Keys	None	RETURN

	Press Keys	None	ENTER
	sleep	5s
	Press Keys	None	ENTER
	${OUTPUT}=	GUI:Access::Generic Console Command Output	whoami
	Should Contain	${OUTPUT}	${USERNAME_ROOT}
	sleep	5s
	${OUTPUT}=	GUI:Access::Generic Console Command Output	${DISCONNECTION_HOTKEY}
	sleep	5s
	Switch Browser	1
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Preferences::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Teardown

Test Case To Check Device Functionality By Disabling Disconnection Hotkey
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Preferences::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_session_pref"]/span
	GUI::Basic::Spinner Should Be Invisible
	Unselect Checkbox	//*[@id="terminatesession"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="|${DEVICE_NAME}"]/td[2]/div/a[1]
	sleep	5s
	Run Keyword If	'${NGVERSION}'<='5.4'	Switch Window	title=${DEVICE_NAME}
	Run Keyword If	'${NGVERSION}'>'5.4'	Switch Window	title=${DEVICE_NAME} - Console
	Wait Until Page Contains Element	//iframe[@id='termwindow']
	Select Frame	//iframe[@id='termwindow']
	Sleep	5s
	Press Keys	None	RETURN
	Press Keys	None	RETURN

	Press Keys	None	ENTER
	sleep	5s
	Press Keys	None	ENTER
	${OUTPUT}=	GUI:Access::Generic Console Command Output	whoami
	Should Contain	${OUTPUT}	${USERNAME_ROOT}
	sleep	5s
	${OUTPUT}=	GUI:Access::Generic Console Command Output	${DISCONNECTION_HOTKEY}
	sleep	5s
	${OUTPUT}=	GUI:Access::Generic Console Command Output	${exit_session}
	Switch Browser	1
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Preferences::Open Tab
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE_NAME}
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=spm_name	${DEVICE_NAME}
	Select From List By Label	id=type	${DEVICE_TYPE}
	Input Text	id=phys_addr	${HOST_IP}
	Input Text	id=username	${USERNAME_ROOT}
	Input Text	id=passwordfirst	${PASSWORD_ROOT}
	Input Text	id=passwordconf	${PASSWORD_ROOT}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE_NAME}
	GUI::Basic::Spinner Should Be Invisible
	close all browsers

SUITE:Console Command Output
	[Arguments]	${COMMAND}
	[Documentation]	Input the given command into the ttyd terminal and retrieve the output after prompt lines
	...	== ARGUMENTS ==
	...	-	COMMAND = Command to be executed
	...	== EXPECTED RESULT ==
	...	Input the command into the ttyd terminal and returns the OUTPUT between the command and the last prompt

	${CHECK_TAB}=	Run Keyword And Return Status	Should Not Contain	${COMMAND}	TAB
	Run Keyword If	${CHECK_TAB}	Press Keys 	None	${COMMAND}	RETURN
	...	ELSE	Press Keys 	None	${COMMAND}
	Sleep	1

	${SELECTION}=	Execute JavaScript	term.selectAll(); return term.getSelection().trim();
	Should Not Contain	${SELECTION}	[error.connection.failure] Could not establish a connection to device

	${LINES}=	Split String	${SELECTION}	\n
	${INDEXES_OCCURRED}=	Create List
	${LENGTH}=	Get Length	${LINES}
	FOR	${INDEX}	IN RANGE	0	${LENGTH}
		${LINE}=	Get From List	${LINES}	${INDEX}
		${CHECK}=	Run Keyword And Return Status	Should Contain	${LINE}	@
		Run Keyword If	${CHECK}	Append to List	${INDEXES_OCCURRED}	${INDEX}
	END

	${STRING}=	Set Variable
	${END}=	Get From List	${INDEXES_OCCURRED}	-1
	${PREVIOUS}=	Get From List	${INDEXES_OCCURRED}	-2
	FOR	${INDEX}	IN RANGE	${PREVIOUS}	${END}
		${LINE}=	Get From List	${LINES}	${INDEX}
		${STRING}=	Set Variable	${STRING}\n${LINE}
	END
	${STRING}=	Get Substring	${STRING}	1
	[Return]	${STRING}