*** Settings ***
Documentation	Test webpage forwading behavior with the plugin installed
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}

Suite Setup     SUITE:Setup
Suite Teardown  SUITE:Teardown
Test Teardown   SUITE:Test Teardown

*** Variables ***
${DEVICE}            test_device_perle
${DEVICE_TYPE}       console_server_perle
${DEVICE_IP}         ${PERLE_SCR_SERVER_IP}
${DEVICE_USERNAME}   ${PERLE_SCR_SERVER_USERNAME}
${DEVICE_PASSWORD}   ${PERLE_SCR_SERVER_PASSWORD}
${PLUGIN_FILEPATH}   ${WEBSESSION_PLUGIN_FILEPATH}
#for locally running uncomment the line bellow and comment the line above
#${WEBSESSION_PLUGIN_FILEPATH}     ~/.config/google-chrome/Default/Extensions/cmcpkbfnablakhllgdmbhkedpoengpik/1_2_0.crx

*** Test Cases ***
Test Webpage Forwading With The Plugin Not Installed
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Set Browser Extension Forwarder Option For Device "${DEVICE}"
    GUI::Basic::Spinner Should Be Invisible
    GUI::Access::Table::Open Device "${DEVICE}" Websession
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Page Contains     Authorization Required

Test Webpage Forwading With The Plugin Installed
	[Tags]	NON-CRITICAL	NEED-REVIEW
    ${PLUGIN_CAPABILITY}=   SUITE:Get Plugin Capability
    SUITE:Open And Login Nodegrid       DESIRED_CAPABILITIES=${PLUGIN_CAPABILITY}
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Set Browser Extension Forwarder Option For Device "${DEVICE}"
    GUI::Basic::Spinner Should Be Invisible
    GUI::Access::Table::Open Device "${DEVICE}" Websession
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Page Contains Element   //body
    Page Should Not Contain     Authorization Required

Test Internal Browser With The Plugin Not Installed
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Set Iternal Browser Option For Device "${DEVICE}"
    GUI::Basic::Spinner Should Be Invisible
    GUI::Access::Table::Open Device "${DEVICE}" Websession
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Element Is Visible   //div[@id="bodyBlock"]//div[@id="object"]

Test Internal Browser With The Plugin Installed
	[Tags]	NON-CRITICAL	NEED-REVIEW
    ${PLUGIN_CAPABILITY}=   SUITE:Get Plugin Capability
    SUITE:Open And Login Nodegrid       DESIRED_CAPABILITIES=${PLUGIN_CAPABILITY}
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Set Iternal Browser Option For Device "${DEVICE}"
    GUI::Basic::Spinner Should Be Invisible
    GUI::Access::Table::Open Device "${DEVICE}" Websession
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Element Is Visible   //div[@id="bodyBlock"]//div[@id="object"]

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists    ${DEVICE}
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Add Device    ${DEVICE}  ${DEVICE_TYPE}  ${DEVICE_IP}
    ...   ${DEVICE_USERNAME}  no  ${DEVICE_PASSWORD}  ondemand
    Close All Browsers

SUITE:Teardown
    Close All Browsers
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists    ${DEVICE}
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Logout And Close Nodegrid

SUITE:Test Teardown
    Close All Browsers

SUITE:Open Nodegrid
    [Arguments]     ${PAGE}=${HOMEPAGE}     ${BROWSER}     ${NGVERSION}    ${DESIRED_CAPABILITIES}=None
    ${BROWSER}	Convert To Lower Case	${BROWSER}
    Log	${BROWSER}	console=yes
    IF	'${DESIRED_CAPABILITIES}' == '${EMPTY}'
    	Run Keyword If	'${BROWSER}' == 'chrome' or '${BROWSER}' == 'headlesschrome'	Wait Until Keyword Succeeds	3x	3s	Open Browser	${PAGE}	${BROWSER}	options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors")
    	Run Keyword If	'${BROWSER}' == 'firefox' or '${BROWSER}' == 'headlessfirefox'	Wait Until Keyword Succeeds	3x	3s	Open Browser	${PAGE}	${BROWSER}	options=set_preference("moz:dom.disable_beforeunload", "true")
    ELSE
    	Run Keyword If	'${BROWSER}' == 'chrome' or '${BROWSER}' == 'headlesschrome'	Wait Until Keyword Succeeds	3x	3s	Open Browser	${PAGE}	${BROWSER}	desired_capabilities=${DESIRED_CAPABILITIES}	options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors")
    	Run Keyword If	'${BROWSER}' == 'firefox' or '${BROWSER}' == 'headlessfirefox'	Wait Until Keyword Succeeds	3x	3s	Open Browser	${PAGE}	${BROWSER}	desired_capabilities=${DESIRED_CAPABILITIES}	options=set_preference("moz:dom.disable_beforeunload", "true")
    END
    Maximize Browser Window
    ${CERTIFICATION_ERROR}=    Run Keyword And Return Status
    ...    Wait Until Page Contains    Your connection is not private
    Run Keyword If  "${CERTIFICATION_ERROR}" == "True" and "${BROWSER}" == "Chrome"
    ...    Press Keys    None    thisisunsafe

    Run Keyword If  '${NGVERSION}' >= '4.0'     GUI4.x::Basic::Open NodeGrid
    Run Keyword If  '${NGVERSION}' == '3.2'        GUI3.x::Basic::Open NodeGrid

SUITE:Open And Login Nodegrid
    [Arguments]     ${USERNAME}=${DEFAULT_USERNAME}   ${PASSWORD}=${DEFAULT_PASSWORD}   ${DESIRED_CAPABILITIES}=None
    SUITE:Open NodeGrid    ${HOMEPAGE}    ${BROWSER}  ${NGVERSION}  ${DESIRED_CAPABILITIES}
    GUI::Basic::Login   ${NGVERSION}    ${USERNAME}   ${PASSWORD}

SUITE:Set Browser Extension Forwarder Option For Device "${DEVICE_NAME}"
    GUI::Basic::Managed Devices::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
    Click Element   //a[@id="${DEVICE_NAME}"]
    GUI::Basic::Spinner Should Be Invisible
    Select Checkbox  id=webhtml5
    Select Radio Button  webhtml5method  forwarder
    SUITE:Save
    GUI::Basic::Spinner Should Be Invisible

SUITE:Set Iternal Browser Option For Device "${DEVICE_NAME}"
    GUI::Basic::Managed Devices::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
    Click Element   //a[@id="${DEVICE_NAME}"]
    GUI::Basic::Spinner Should Be Invisible
    Select Checkbox  id=webhtml5
    Select Radio Button  webhtml5method  internal_browser
    SUITE:Save

SUITE:Save
    ${DISABLED}=    Get Element Attribute   saveButton  disabled
    Run Keyword If  not "${DISABLED}" == "true"     GUI::Basic::Save

SUITE:Get Plugin Capability
    ${CHROME_OPTIONS} =     Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
	Log	\n\n++++++++++++++++++++++++++++ CHROME_OPTIONS: ++++++++++++++++++++++++++++\n ${CHROME_OPTIONS}\n	INFO	console=yes
	Log	\n\n++++++++++++++++++++++++++++ PLUGIN_FILEPATH: ++++++++++++++++++++++++++++\n ${PLUGIN_FILEPATH}\n	INFO	console=yes
    Call Method    ${CHROME_OPTIONS}    add_extension    ${PLUGIN_FILEPATH}
    ${OPTIONS}=     Call Method     ${CHROME_OPTIONS}    to_capabilities
    [Return]   ${OPTIONS}
