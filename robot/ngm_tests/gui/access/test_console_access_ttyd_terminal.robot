*** Settings ***
Documentation	Test if Console buttons and TTYD funcionalities works without errors for admin & regular user
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../init.robot
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EDGE	IE	NON-CRITICAL	NEED-REVIEW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME_ADMIN}	${DEFAULT_USERNAME}
${PASSWORD_ADMIN}	${DEFAULT_PASSWORD}
${DEVICE}	template
${DUMMY_USER}	dummy
${DUMMY_PASSWORD}	dummy

*** Test Cases ***
Test Console Button Access HTML TEMPLATE | ADMIN user
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Access ${DEVICE} Device And Check Terminal ${FALSE}
	[Teardown]	SUITE:Teardown

Test Console Button Access TTYD TERMINAL and RESIZE WINDOW | ADMIN user
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Access ${DEVICE} Device And Check Terminal ${TRUE}
	[Teardown]	SUITE:Teardown

Test Console Button Access TTYD TERMINAL INPUT COMMANDS and VALIDATE OUTPUT | ADMIN user
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	${MAIN_WINDOW_TITLE}=	Get Title
	GUI::Access::Access ${DEVICE} Device and Console Window Open ${TRUE}
	Sleep	10
	Press Keys	//body	RETURN


	${OUTPUT}=	GUI:Access::Generic Console Command Output	ls
	Should Contain  ${OUTPUT}	access/	system/	settings/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	pwd
	Should Contain  ${OUTPUT}	/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	hostname
	Should Contain  ${OUTPUT}	nodegrid

	${OUTPUT}=	GUI:Access::Generic Console Command Output	whoami
	Should Contain  ${OUTPUT}	${DEFAULT_USERNAME}

	${OUTPUT}=	GUI:Access::Generic Console Command Output	TAB+TAB

	${COMMANDS}=	Create List	apply_settings	cd	change_password	commit
	...	event_system_audit	event_system_clear	exit	factory_settings
	...	hostname	ls	pwd	quit	reboot	revert	save_settings	set
	...	shell	show	shutdown	software_upgrade	system_certificate
	...	system_config_check	whoami
	FOR    ${COMMAND}    IN    @{COMMANDS}
		Should Contain	${OUTPUT}	${COMMAND}
	END
	Unselect Frame
	Press Keys	//body	cw
	Close Window
	[Teardown]	SUITE:Teardown

Test Console Button Access TTYD TERMINAL COPY AND PASTE | ADMIN user
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	${MAIN_WINDOW_TITLE}=	Get Title
	${USERLINE_LENGTH}=	GUI::Access::Get Userline Length

	GUI::Access::Access ${DEVICE} Device and Console Window Open ${TRUE}
	${CONSOLE_WINDOW_TITLE}=	Get Title
	Sleep	10
	Press Keys	//body	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output	ls
	Should Contain  ${OUTPUT}	access/	system/	settings/

#	Selection of the 'ls' command full line in index 4
	Execute JavaScript	term.selectLines(4,4);
#	Copy selection of the command to clipboard
	Press Keys	//body	${COPY_KEY}
#	Goes to the main window to paste the clipboard selection into the search field
#	and manipulates it to get only the correct command 'ls' and copy that to clipboard
	GUI:Basic::Wait Until Window Exists    ${MAIN_WINDOW_TITLE}
	Click Element	//input[@id='search_expr']
	Input Text	//input[@id='search_expr']	ls
	Click Element	//input[@id='search_expr']
	Press Keys	//input[@id='search_expr']	${SELECT_ALL_KEY}
	Press Keys	//input[@id='search_expr']	${COPY_KEY}
	Click Element	//input[@id='search_expr1']
	Press Keys	//input[@id='search_expr1']	CTRL+V
#	Goes to the console window and paste the correct command to the console
	Switch Window	title=${CONSOLE_WINDOW_TITLE}
	Close Window
	GUI:Basic::Wait Until Window Exists    ${MAIN_WINDOW_TITLE}
	${USERLINE_LENGTH}=	GUI::Access::Get Userline Length
	GUI::Access::Access ${DEVICE} Device and Console Window Open ${TRUE}
	${CONSOLE_WINDOW_TITLE}=	Get Title
	Sleep	10
	Press Keys	//body	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output	${PASTE_KEY}
	Should Contain  ${OUTPUT}	access/	system/	settings/

	Close Window
	[Teardown]	SUITE:Teardown

Test Console Button Access HTML TEMPLATE | DUMMY user
	SUITE:Setup
	Close Browser
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid	${DUMMY_USER}	${DUMMY_PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Access ${DEVICE} Device And Check Terminal ${FALSE}
	[Teardown]	SUITE:Teardown


Test Console Button Access TTYD TERMINAL and RESIZE WINDOW | DUMMY user
	SUITE:Setup
	Close Browser
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid	${DUMMY_USER}	${DUMMY_PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Access ${DEVICE} Device And Check Terminal ${TRUE}
	GUI::Basic::Spinner Should Be Invisible
	[Teardown]	SUITE:Teardown

Test Console Button Access TTYD TERMINAL INPUT COMMANDS and VALIDATE OUTPUT | DUMMY user
	SUITE:Setup
	Close Browser
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid	${DUMMY_USER}	${DUMMY_PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	${MAIN_WINDOW_TITLE}=	Get Title
	GUI::Access::Access ${DEVICE} Device and Console Window Open ${TRUE}
	Sleep	10
	Press Keys	//body	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output	ls
	Should Contain  ${OUTPUT}	access/	system/	settings/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	pwd
	Should Contain  ${OUTPUT}	/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	hostname
	Should Contain  ${OUTPUT}	nodegrid

	${OUTPUT}=	GUI:Access::Generic Console Command Output	whoami	#	template device user credential = admin
	Should Contain  ${OUTPUT}	${DEFAULT_USERNAME}

	${OUTPUT}=	GUI:Access::Generic Console Command Output	TAB+TAB
	${COMMANDS}=	Create List	apply_settings	cd	change_password	commit
	...	event_system_audit	event_system_clear	exit	factory_settings
	...	hostname	ls	pwd	quit	reboot	revert	save_settings	set
	...	shell	show	shutdown	software_upgrade	system_certificate
	...	system_config_check	whoami
	FOR    ${COMMAND}    IN    @{COMMANDS}
		Should Contain	${OUTPUT}	${COMMAND}
	END
	Unselect Frame
	Press Keys	//body	cw
	Close Browser
	[Teardown]	SUITE:Teardown

Test Console Button Access TTYD TERMINAL COPY AND PASTE | DUMMY user
	SUITE:Setup
	Close Browser
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid	${DUMMY_USER}	${DUMMY_PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	${MAIN_WINDOW_TITLE}=	Get Title
	${USERLINE_LENGTH}=	GUI::Access::Get Userline Length

	GUI::Access::Access ${DEVICE} Device and Console Window Open ${TRUE}
	${CONSOLE_WINDOW_TITLE}=	Get Title
	Sleep	10
	Press Keys	//body	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output	ls
	Should Contain  ${OUTPUT}	access/	system/	settings/

#	Goes to the main window to paste the clipboard selection into the search field
#	and manipulates it to get only the correct command 'ls' and copy that to clipboard
	GUI:Basic::Wait Until Window Exists    ${MAIN_WINDOW_TITLE}
	Click Element	//input[@id='search_expr']
	Input Text	//input[@id='search_expr']	ls
	Click Element	//input[@id='search_expr']
	Press Keys	//input[@id='search_expr']	${SELECT_ALL_KEY}
	Press Keys	//input[@id='search_expr']	${COPY_KEY}
	Click Element	//input[@id='search_expr1']
	Press Keys	//input[@id='search_expr1']	CTRL+V
#	Goes to the console window and paste the correct command to the console
	Switch Window	title=${CONSOLE_WINDOW_TITLE}
	Close Window
	GUI:Basic::Wait Until Window Exists    ${MAIN_WINDOW_TITLE}
	${USERLINE_LENGTH}=	GUI::Access::Get Userline Length
	GUI::Access::Access ${DEVICE} Device and Console Window Open ${TRUE}
	${CONSOLE_WINDOW_TITLE}=	Get Title
	Sleep	10
	Press Keys	//body	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output	${PASTE_KEY}
	Should Contain  ${OUTPUT}	access/	system/	settings/

	Close Window
	[Teardown]	SUITE:Teardown

Test TTYD Console Sessions Crashing when closing multiple sessions at once
	[Setup]	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	${MAIN_WINDOW_TITLE}=	Get Title

	# Opens 15 consoles connections
	${CONSOLES_WINDOWS}=	Create List
	FOR	${INDEX}	IN RANGE	0	25
		Sleep	1
		Click Element	xpath=//a[.='Console']
	Switch Window	title=${MAIN_WINDOW_TITLE}
	END
	SUITE:User Logout

	# Closes the consoles windows all at once
	Close all Browsers

	# Goes to NG window and validates that ttyd is opening with no issue
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Access ${DEVICE} Device And Check Terminal ${TRUE}
	[Teardown]	SUITE:User Logout

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add Console Device
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Local Accounts::Add	dummy
    GUI::Basic::Spinner Should Be Invisible
#	Sets the keyboard keys support from the current OS - support to macOS
#	${OS}=	CLIENT:Get Operating System No Version
#	${COPY_KEY}	Set Variable If	'${OS}' == 'macOS'	COMMAND+C	CTRL+C
#	${PASTE_KEY}	Set Variable If	'${OS}' == 'macOS'	COMMAND+V	CTRL+V
#	${SELECT_ALL_KEY}	Set Variable If	'${OS}' == 'macOS'	COMMAND+A	CTRL+A
#	${HOME_KEY}	Set Variable If	'${OS}' == 'macOS'	COMMAND+ARROW_LEFT	HOME
#	Set Suite Variable	${COPY_KEY}	${COPY_KEY}
#	Set Suite Variable	${PASTE_KEY}	${PASTE_KEY}
#	Set Suite Variable	${SELECT_ALL_KEY}	${SELECT_ALL_KEY}
#	Set Suite Variable	${HOME_KEY}	${HOME_KEY}
	Set Suite Variable	${COPY_KEY}	CTRL+C
	Set Suite Variable	${PASTE_KEY}	SHIFT+INSERT
	Set Suite Variable	${SELECT_ALL_KEY}	CTRL+A
	Set Suite Variable	${HOME_KEY}	HOME

SUITE:Teardown
	Close all Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Local Accounts::Delete	dummy
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout
	Close all Browsers

SUITE:User Logout
	Close all Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Logout
	Close Browser

SUITE:Add Console Device
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE}
	GUI::ManagedDevices::Add Device	${DEVICE}	device_console	127.0.0.1	${USERNAME_ADMIN}	no	${PASSWORD_ADMIN}	enabled