*** Settings ***
Documentation	Test visibility of the Event Log button and its content in pdf and also via console
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Force Tags   PART-1	GUI	${BROWSER}  ${SYSTEM}   ${MODEL}    ${VERSION}	EDGE    IE

Suite Setup     SUITE:Setup
Suite Teardown  SUITE:Teardown
Test Teardown   SUITE:Test Teardown
Test Setup      SUITE:Test Setup

*** Variables ***
${USER}             ${DEFAULT_USERNAME}
${PASSWORD}         ${DEFAULT_PASSWORD}
${USER}	    ${DEFAULT_USERNAME}
@{ALL_CATEGORIES}   System Events  AAA Events  Device Events   Logging Events
${PDF_DEVICE}       test_pdf_device
${CONSOLE_DEVICE}   test_console_device
${PDF_DEVICE}       test_pdf_device

${EVENT_LOG_HEADER_REGEX}          <\\d{4}-\\d{2}-\\d{2}[A-Z]{1}\\d{2}:\\d{2}:\\d{2}[A-Z]{1}> Event ID \\d{3}:
${PDF_DEVICE_CREATED_REGEX}        Event ID 302: Device created. User: ${USER}. Device: ${PDF_DEVICE}.
${CONSOLE_DEVICE_CREATED_REGEX}    Event ID 302: Device created. User: ${USER}. Device: ${CONSOLE_DEVICE}.
${CONFIG_EVENT_REGEX}              Event ID 108: The configuration has changed. Change made by user: ${USER}.
${PDF_DEVICE_UP_REGEX}             Event ID 306: Device Up. Device: ${PDF_DEVICE}
${CONSOLE_DEVICE_UP_REGEX}         Event ID 306: Device Up. Device: ${CONSOLE_DEVICE}
${USER_LOGOUT_REGEX}               Event ID 201: A user logged out of the system. User: ${USER}.
${USER_LOGIN_REGEX}                Event ID 200: A user logged into the system. User: ${USER}@\\d{1,3}.\\d{1,3}.\\d{1,3}.\\d{1,3}\\. Session type: (SSH|SS H)\\.
@{CONSOLE_REGEXES}                 ${CONSOLE_DEVICE_CREATED_REGEX}  ${CONFIG_EVENT_REGEX}  ${CONSOLE_DEVICE_UP_REGEX}  ${USER_LOGOUT_REGEX}  ${USER_LOGIN_REGEX}
@{PDF_REGEXES}                     ${PDF_DEVICE_CREATED_REGEX}  ${CONFIG_EVENT_REGEX}  ${PDF_DEVICE_UP_REGEX}  ${USER_LOGOUT_REGEX}  ${USER_LOGIN_REGEX}
${WARNING_MESSAGE}	SEPARATOR=
	...	Test to copy content inside PDF works only for FIREFOX due to Selenium Library
	...	${SPACE}doesn't find element inside shadow_dom of the PDF reader from CHROME

*** Test Cases ***
Test Event Log Button Visibility With All Event Types Checked
    SUITE:Select Event Categories   @{ALL_CATEGORIES}
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Event Log Button Should Be Visible
    GUI::Basic::Spinner Should Be Invisible

Test Event Log Button Visibility With No Event Type Checked
	[Tags]	NON-CRITICAL	NEED-REVIEW
    SUITE:Select Event Categories
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Event Log Button Should Not Be Visible
    GUI::Basic::Spinner Should Be Invisible

Test Event Log Button Visibility With System Events Type Checked
    SUITE:Select Event Categories   System Event
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Event Log Button Should Be Visible
    GUI::Basic::Spinner Should Be Invisible

Test Event Log Button Visibility With AAA Events Type Checked
    SUITE:Select Event Categories   AAA Event
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Event Log Button Should Be Visible
    GUI::Basic::Spinner Should Be Invisible

Test Event Log Button Visibility With Device Events Type Checked
    SUITE:Select Event Categories   Device Event
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Event Log Button Should Be Visible
    GUI::Basic::Spinner Should Be Invisible

Test Event Log Button Visibility With Logging Events Type Checked
    SUITE:Select Event Categories   Logging Event
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Event Log Button Should Be Visible
    GUI::Basic::Spinner Should Be Invisible

Test Check Event Log Content From Console
	[Tags]	NON-CRITICAL	NEED-REVIEW
    SUITE:Select Event Categories   @{ALL_CATEGORIES}
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Clear Event Log And Check From Console If It Is Empty
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Add Device Console "${CONSOLE_DEVICE}" And Relog
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Event Log Accessed Via Console Should Have Right Content

Test Check Event Log Content From PDF
	[Tags]	NON-CRITICAL	NEED-REVIEW
	Run Keyword If	'${BROWSER}' == 'CHROME'	Run Keywords	Set Tags	NON-CRITICAL	AND	Skip	${WARNING_MESSAGE}
    SUITE:Select Event Categories   @{ALL_CATEGORIES}
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Clear Event Log And Check From PDF If It Is Empty
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Add Device Console "${PDF_DEVICE}" And Relog
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Event Log Accessed Via PDF Should Have Right Content

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists  ${PDF_DEVICE}
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists  ${CONSOLE_DEVICE}
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Logout
    Close Browser

SUITE:Teardown
    Close All Browsers
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists  ${PDF_DEVICE}
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists  ${CONSOLE_DEVICE}
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Select Event Categories   @{ALL_CATEGORIES}
    GUI::Basic::Spinner Should Be Invisible
    Close All Browsers

SUITE:Test Setup
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible

SUITE:Test Teardown
    Close All Browsers

SUITE:Select Event Categories
    [Arguments]  @{CATEGORIES}
    GUI::Auditing::Events::Open "File" Row
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Uncheck All Event Categories
    FOR    ${CATEGORY}    IN    @{CATEGORIES}
        Select Checkbox     //label[text()[contains(.,"${CATEGORY}")]]//input
    END
    ${DISABLED}=    Get Element Attribute   saveButton  disabled
    Run Keyword If  not "${DISABLED}" == "true"     GUI::Basic::Save
	Capture Page Screenshot

SUITE:Uncheck All Event Categories
    FOR   ${CATEGORY}   IN  @{ALL_CATEGORIES}
        Unselect Checkbox   //label[text()[contains(.,"${CATEGORY}")]]//input
	END

SUITE:Event Log Button Should Be Visible
    SUITE:Open Info Window
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be Visible   //div[@class="modal-body"]//div[@class="modal_buttons"]//input[@id="eventlog"]

SUITE:Event Log Button Should Not Be Visible
    SUITE:Open Info Window
    GUI::Basic::Spinner Should Be Invisible
    Run Keyword And Continue On Failure     Element Should Not Be Visible   //div[@class="modal-body"]//div[@class="modal_buttons"]//input[@id="eventlog"]
    SUITE:Close Info Window

SUITE:Open Info Window
    GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
    Click Element   //div[@class="peer_header"]//a[text()="Info"]
    Wait Until Element Is Visible   //div[@class="modal-body"]//div[@class="modal_buttons"]

SUITE:Close Info Window
    Click Element   //*[@id="modal"]/div/div/div[1]/button

SUITE:Open Console
    GUI::Access::Table::Open Console
    Sleep   5

    Switch Window   title=nodegrid
    Wait Until Page Contains Element    xpath=//*[@id='termwindow']
    Select Frame    xpath=//*[@id='termwindow']
    Sleep   5
    Press Keys  None    RETURN
    Press Keys  None    RETURN

SUITE:Close Console
    Unselect Frame
    Switch Window   title=nodegrid
    Close Window

#SUITE:Console Command Output
#    [Arguments]	${COMMAND}
#    [Documentation]     Input the given 3 command into the ttyd terminal and retrieve the output after prompt lines
#
#    ${CHECK_TAB}=   Run Keyword And Return Status   Should Not Contain  ${COMMAND}  TAB
#    Run Keyword If  ${CHECK_TAB}    Press Keys 	None    ${COMMAND}  RETURN
#    ...  ELSE    Press Keys     None    ${COMMAND}
#    Sleep   1
#
#    ${CONSOLE_CONTENT}=	Execute JavaScript  term.selectAll(); return term.getSelection().trim();
#    Should Not Contain  ${CONSOLE_CONTENT}  [error.connection.failure] Could not establish a connection to device
#    ${OUTPUT}=  Output From Last Command    ${CONSOLE_CONTENT}
#    [Return]    ${OUTPUT}

SUITE:Clear Event Log And Check From Console If It Is Empty
    ${MAIN_WINDOW_TITLE}=   Get Title
    SUITE:Open Console

    ${OUTPUT}=  GUI:Access::Generic Console Command Output  TAB+TAB
    Should Contain  ${OUTPUT}   event_system_audit  event_system_clear

    ${OUTPUT}=  GUI:Access::Generic Console Command Output  event_system_clear

    # If the regex matches, it means the Event Log is not empty (there is at least one log line)
    ${CONSOLE_EVENT_LOG}=  GUI:Access::Generic Console Command Output  event_system_audit
    ${CONSOLE_EVENT_LOG}=     Remove String   ${CONSOLE_EVENT_LOG}  \r  \n
    Should Not Match Regexp  ${CONSOLE_EVENT_LOG}  ${EVENT_LOG_HEADER_REGEX}

    SUITE:Close Console
    Switch Window   ${MAIN_WINDOW_TITLE}

SUITE:Clear Event Log And Check From PDF If It Is Empty
    ${MAIN_WINDOW_TITLE}=   Get Title
    Set Suite Variable	${MAIN_WINDOW_TITLE}
    SUITE:Open Console

    ${OUTPUT}=  GUI:Access::Generic Console Command Output  TAB+TAB
    Should Contain  ${OUTPUT}   event_system_audit  event_system_clear

    ${OUTPUT}=  GUI:Access::Generic Console Command Output  event_system_clear
    Should Contain  ${OUTPUT}   event_system_clear

    Switch Window   ${MAIN_WINDOW_TITLE}

    SUITE:Open Event Log PDF
    ${PDF_EVENT_LOG}=   SUITE:Page Text Without New Lines
    Should Not Match Regexp  ${PDF_EVENT_LOG}  ${EVENT_LOG_HEADER_REGEX}

    Switch Window   ${MAIN_WINDOW_TITLE}

SUITE:Add Device Console "${DEVICE_NAME}" And Relog
    GUI::ManagedDevices::Add Device     ${DEVICE_NAME}   device_console  127.0.0.1   ${USER}   no  ${PASSWORD}   enabled
    GUI::Basic::Logout
    GUI::Basic::Login

SUITE:Event Log Accessed Via Console Should Have Right Content
    ${MAIN_WINDOW_TITLE}=   Get Title
    SUITE:Open Console

    ${OUTPUT}=  GUI:Access::Generic Console Command Output  TAB+TAB
    Should Contain  ${OUTPUT}   event_system_audit  event_system_clear

    ${CONSOLE_EVENT_LOG}=  GUI:Access::Generic Console Command Output  event_system_audit
    ${CONSOLE_EVENT_LOG}=     Remove String   ${CONSOLE_EVENT_LOG}  \r  \n

    FOR    ${PATTERN}  IN  @{CONSOLE_REGEXES}
       Should Match Regexp    ${CONSOLE_EVENT_LOG}    ${PATTERN}
	END
    SUITE:Close Console
    Switch Window   ${MAIN_WINDOW_TITLE}

SUITE:Event Log Accessed Via PDF Should Have Right Content
    Switch Window   title=nodegrid
    Close Window
    Switch Window   MAIN

    SUITE:Open Event Log PDF
    ${PDF_EVENT_LOG}=   SUITE:Page Text Without New Lines

    FOR    ${PATTERN}  IN  @{PDF_REGEXES}
       Should Match Regexp    ${PDF_EVENT_LOG}    ${PATTERN}
	END
    Close Window

SUITE:Page Text Without New Lines
    Capture Page Screenshot
	Press Keys	None	CTRL+A
	Press Keys	None	CTRL+C
    Switch Window   title=${MAIN_WINDOW_TITLE}
    Click Element	//div[@id='modal']//span[@aria-hidden='true'][normalize-space()='×']
    GUI::Basic::Spinner Should Be Invisible
    Click Element	//input[@id='search_expr']
    Press Keys   //input[@id='search_expr']    CTRL+V
    Capture Page Screenshot
    GUI::Basic::Spinner Should Be Invisible
    Click Element	//input[@id='search_expr']
    ${PDF_CONTENT}=	Get Value	//input[@id='search_expr']
    ${PDF_CONTENT}=     Remove String   ${PDF_CONTENT}  \r  \n
    [Return]    ${PDF_CONTENT}

SUITE:Open Event Log PDF
    SUITE:Open Info Window
    Click Element   //div[@class="modal-body"]//div[@class="modal_buttons"]//input[@id="eventlog"]
    Sleep   5
    Switch Window   NEW