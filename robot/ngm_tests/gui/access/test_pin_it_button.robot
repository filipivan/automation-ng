*** Settings ***
Documentation	Test if when accessing Access page the last pined subpage is presented as homepage
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../init.robot
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE	EXCLUDEIN4_2	EXCLUDEIN5_0

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

*** Test Cases ***
Access System::Preferences tab
	Wait Until Element Is Enabled       pwl
    Click Element                       pwl
    Wait Until Element Is Visible       abt
    Click Element                       abt
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Page Contains            Serial Number
    wait until element is visible       //*[@class='close']
	Click Element	                    //*[@class='close']
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible

Test Pin it in Image Tab
	#GUI::Basic::Access::Image::Open Tab
	#GUI::Basic::Spinner Should Be Invisible
	#Wait Until Element Is Visible	    jquery=#PushpinLabel
	#Click Element	                    jquery=#PushpinLabel
	click element       xpath=(//a[contains(text(),'Map')])[2]
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Page Title Should Be	Access :: Table

Test Pin it in Map Tab 
	click element       xpath=(//a[contains(text(),'Map')])[2]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	    jquery=#PushpinLabel
	Click Element	                    jquery=#PushpinLabel
	GUI::Basic::Access::Node::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Page Title Should Be	Access :: Map

Test Pin it in Node Tab 
	click element       xpath=(//a[contains(text(),'Node')])[2]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	    jquery=#PushpinLabel
	Click Element	                    jquery=#PushpinLabel
	GUI::Basic::Access::Tree::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Page Title Should Be	Access :: Node

Test Pin it in Tree Tab 
	click element       xpath=(//a[contains(text(),'Tree')])[2]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	    jquery=#PushpinLabel
	Click Element	                    jquery=#PushpinLabel
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Page Title Should Be	Access :: Tree

Test Pin it in Table Tab 
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	    jquery=#PushpinLabel
	Click Element	                    jquery=#PushpinLabel
	#GUI::Basic::Access::Image::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Page Title Should Be	Access :: Table

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	Close all Browsers
