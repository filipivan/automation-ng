*** Settings ***
Documentation	Test visibility of the system and managed device Data Log button
...             and its content in pdf and also via console
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Resource	../../init.robot
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EDGE	IE

Suite Setup     SUITE:Setup
Suite Teardown  SUITE:Teardown
Test Setup      SUITE:Test Setup
Test Teardown   SUITE:Test Teardown

*** Variables ***
${USERNAME}                     ${DEFAULT_USERNAME}
${PASSWORD}                     ${DEFAULT_PASSWORD}
${DEVICE}                       test_device_digi_cm
${DEVICE_TYPE_TO_CLONE}         console_server_digicp
${DEVICE_TYPE}                  test_console_server_digi
${DEVICE_IP}                    192.168.3.142	#was192.168.3.183	#was 192.168.2.56
${DEVICE_USERNAME}              root
${DEVICE_PASSWORD}              dbps
${HOSTNAME}                     ${HOSTNAME_NODEGRID}
${DEVICE_HOSTNAME}              Digi_CM_Device
${SHELL_PROMPT_REGEX}           [^@:>]+(@?)([^@:>]?)+(:?)([^@:>]?)+[#\$>]
${FILEPATH_REGEX}               [a-zA-Z0-9\\-_\\./\\\ ]+
${DATETIME_REGEX}               <\\d{4}-\\d{2}-\\d{2}[A-Z]{1}\\d{2}:\\d{2}:\\d{2}[A-Z]{1}>
${ATTACH_LOG_REGEX}             \\[\\-\\- ${USERNAME}@\\d+ attached \\-\\- ${DATETIME_REGEX}\\]
${DETACH_LOG_REGEX}             \\[\\-\\- ${USERNAME}@\\d+ detached \\-\\- ${DATETIME_REGEX}\\]
${CONSOLE_LOG_END_REGEX}        \\(h->Help, q->Quit\\) ${FILEPATH_REGEX} (?:\\(End\\))?(?:[ ]*\\(Begin\\))?
${PDF_LOG_PAGE_START_REGEX}     ${SPACE}Page \\d{1} \\- \\d{2}\\/\\d{2}\\/\\d{4} \\d{2}:\\d{2}:\\d{2}
${DISCARDED_LOG_CONTENT}	SEPARATOR=
...	\[${USERNAME}@${HOSTNAME} \/]# access_system_audit
...	The output of this command will not be logged in the system data logging.

${EXPECTED_SYS_LOG_CONTENT}	SEPARATOR=
...	[${USERNAME}@${HOSTNAME} /]# whoami
...	${SPACE}${USERNAME}
...	${SPACE}\[${USERNAME}@${HOSTNAME} /]# ls
...	${SPACE}access/
...	${SPACE}system/
...	${SPACE}settings/
...	${SPACE}\[${USERNAME}@${HOSTNAME} /]# cd
...	${SPACE}\[${USERNAME}@${HOSTNAME} /]# hostname
...	${SPACE}${HOSTNAME}

${EXPECTED_DEV_LOG_CONTENT}	SEPARATOR=
...	${DEVICE_USERNAME}@${DEVICE_HOSTNAME}:~# ls
...	${SPACE}${DEVICE_USERNAME}@${DEVICE_HOSTNAME}:~# cd
...	${SPACE}${DEVICE_USERNAME}@${DEVICE_HOSTNAME}:~# whoami
...	${SPACE}${DEVICE_USERNAME}
...	${SPACE}${DEVICE_USERNAME}@${DEVICE_HOSTNAME}:~# hostname
...	${SPACE}${DEVICE_HOSTNAME}
...	${SPACE}${DEVICE_USERNAME}@${DEVICE_HOSTNAME}:~# pwd
...	${SPACE}\/${DEVICE_USERNAME}

${EXPECTED_SYS_LOG_CONTENT_WITHOUT_SPACE}	SEPARATOR=
...	[${USERNAME}@${HOSTNAME} /]# whoami
...	${USERNAME}
...	[${USERNAME}@${HOSTNAME} /]# ls
...	access/
...	system/
...	settings/
...	[${USERNAME}@${HOSTNAME} /]# cd
...	[${USERNAME}@${HOSTNAME} /]# hostname
...	${HOSTNAME}

${EXPECTED_DEV_LOG_CONTENT_WITHOUT_SPACE}	SEPARATOR=
...	${DEVICE_USERNAME}@${DEVICE_HOSTNAME}:~# ls
...	${DEVICE_USERNAME}@${DEVICE_HOSTNAME}:~# cd
...	${DEVICE_USERNAME}@${DEVICE_HOSTNAME}:~# whoami
...	${DEVICE_USERNAME}
...	${DEVICE_USERNAME}@${DEVICE_HOSTNAME}:~# hostname
...	${DEVICE_HOSTNAME}
...	${DEVICE_USERNAME}@${DEVICE_HOSTNAME}:~# pwd
...	/${DEVICE_USERNAME}
${SHADOW_DOM_SELECTOR}	dom:document.querySelector("#viewer").shadowRoot.querySelector("#plugin")
${WARNING_MESSAGE}	SEPARATOR=
	...	Test to copy content inside PDF works only for FIREFOX due to Selenium Library
	...	${SPACE}doesn't find element inside shadow_dom of the PDF reader from CHROME

*** Test Cases ***
Test System Data Log Button Visibility With Logging Enabled
    SUITE:Enable System Logging
    GUI::Basic::Spinner Should Be Invisible
    SUITE:System Data Log Button Should Be Visible
    GUI::Basic::Spinner Should Be Invisible

Test System Data Log Button Visibility With Logging Disabled
    SUITE:Disable System Logging
    GUI::Basic::Spinner Should Be Invisible
    SUITE:System Data Log Button Should Not Be Visible
    GUI::Basic::Spinner Should Be Invisible

Test System Data Log Content Check From Console After Clearing It
	[Tags]	NON-CRITICAL	NEED-REVIEW
    SUITE:Enable System Logging
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Clear System Data Log
    GUI::Basic::Spinner Should Be Invisible
    SUITE:System Data Log Accessed From Console Should Be Empty
    GUI::Basic::Spinner Should Be Invisible

Test System Data Log Content Check From Console After Executing Some Commands
	[Tags]	NON-CRITICAL	NEED-REVIEW
    SUITE:Enable System Logging
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Clear System Data Log
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Execute Some Commands From System Console
    GUI::Basic::Spinner Should Be Invisible
    SUITE:System Data Log Accessed From Console Should Have Right Content
    GUI::Basic::Spinner Should Be Invisible

Test System Data Log Content Check From PDF After Clearing It
	[Tags]	NON-CRITICAL	NEED-REVIEW
    SUITE:Enable System Logging
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Clear System Data Log
    GUI::Basic::Spinner Should Be Invisible
    SUITE:System Data Log Accessed From PDF Should Be Empty
    GUI::Basic::Spinner Should Be Invisible

Test System Data Log Content Check From PDF After Executing Some Commands
	[Tags]	NON-CRITICAL	NEED-REVIEW
    SUITE:Enable System Logging
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Clear System Data Log
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Execute Some Commands From System Console
    GUI::Basic::Spinner Should Be Invisible
    SUITE:System Data Log Accessed From PDF Should Have Right Content
    GUI::Basic::Spinner Should Be Invisible

Test Device Data Log Button Visibility With Logging Enabled
	[Tags]	NON-CRITICAL	NEED-REVIEW
    SUITE:Enable Device ${DEVICE} Logging
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Device ${DEVICE} Data Log Button Should Be Visible
    GUI::Basic::Spinner Should Be Invisible

Test Device Data Log Button Visibility With Logging Disabled
	[Tags]	NON-CRITICAL	NEED-REVIEW
    SUITE:Disable Device ${DEVICE} Logging
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Device ${DEVICE} Data Log Button Should Not Be Visible
    GUI::Basic::Spinner Should Be Invisible

Test Device Data Log Content Check From Console After Clearing It
	[Tags]	NON-CRITICAL	NEED-REVIEW
    SUITE:Enable Device ${DEVICE} Logging
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Clear Device ${DEVICE} Data Log
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Device "${DEVICE}" Data Log Accessed From Console Should Be Empty

Test Device Data Log Content Check From Console After Executing Some Commands
	[Tags]	NON-CRITICAL	NEED-REVIEW
    SUITE:Enable Device ${DEVICE} Logging
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Clear Device ${DEVICE} Data Log
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Execute Some Commands From Device "${DEVICE}" Console
    Sleep	10s
    SUITE:Device "${DEVICE}" Data Log Accessed From Console Should Be Have Right Content

Test Device Data Log Content Check From PDF After Clearing It
	[Tags]	NON-CRITICAL	NEED-REVIEW
    SUITE:Enable Device ${DEVICE} Logging
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Clear Device ${DEVICE} Data Log
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Device "${DEVICE}" Data Log Accessed From PDF Should Be Empty

Test Device Data Log Content Check From PDF After Executing Some Commands
	[Tags]	NON-CRITICAL	NEED-REVIEW
    SUITE:Enable Device ${DEVICE} Logging
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Clear Device ${DEVICE} Data Log
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Execute Some Commands From Device "${DEVICE}" Console
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Device "${DEVICE}" Data Log Accessed From PDF Should Be Have Right Content

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Add Digi CM Type
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists    ${DEVICE}
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Add Device   ${DEVICE}   ${DEVICE_TYPE}  ${DEVICE_IP}
    ...   ${DEVICE_USERNAME}   no  ${DEVICE_PASSWORD}   enabled
    Close Browser

SUITE:Teardown
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists    ${DEVICE}
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Remove Digi CM Type If Exists
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Disable System Logging
    Close Browser

SUITE:Test Setup
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible

SUITE:Test Teardown
    GUI::Basic::Spinner Should Be Invisible
    Close All Browsers

SUITE:Add Digi CM Type
    SUITE:Remove Digi CM Type If Exists
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Open Types tab
    GUI::Basic::Spinner Should Be Invisible
    Select Checkbox     //tr[@id="${DEVICE_TYPE_TO_CLONE}"]//td[@class="selectable"]//input
    GUI::Basic::Clone
    GUI::Basic::Spinner Should Be Invisible
    Input Text   spmtargettype_name    ${DEVICE_TYPE}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Open Types tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element   //a[@id="${DEVICE_TYPE}"]
    GUI::Basic::Spinner Should Be Invisible
    Input Text   ssh_options    ${EMPTY}
    Input Text   shell_prompt   ${SHELL_PROMPT_REGEX}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible

SUITE:Remove Digi CM Type If Exists
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    click element       xpath=(//a[contains(text(),'Types')])[2]
    GUI::Basic::Spinner Should Be Invisible
    ${DEVICE_TYPES} =	Create List	${DEVICE_TYPE}
    GUI::Basic::Delete Rows In Table If Exists  jquery=table\#SPMTargettypeTable   ${DEVICE_TYPES}

SUITE:Enable System Logging
    GUI::Basic::System::Logging::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Select Checkbox   id=sessionlogenable
    GUI::Basic::Save If Configuration Changed

SUITE:Disable System Logging
    GUI::Basic::System::Logging::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Unselect Checkbox   id=sessionlogenable
    GUI::Basic::Save If Configuration Changed

SUITE:System Data Log Button Should Be Visible
    GUI::Access::Table::Open Info Window
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be Visible   //div[@class="modal-body"]//div[@class="modal_buttons"]//input[@id="datalog"]

SUITE:System Data Log Button Should Not Be Visible
    GUI::Access::Table::Open Info Window
    GUI::Basic::Spinner Should Be Invisible
    Element Should Not Be Visible   //div[@class="modal-body"]//div[@class="modal_buttons"]//input[@id="datalog"]

SUITE:Enable Device ${DEVICE_NAME} Logging
    GUI::Basic::Managed Devices::Devices::Open Tab
    GUI::ManagedDevices::Enter Device    ${DEVICE_NAME}
    GUI::ManagedDevices::Devices::Open Logging Menu
    Select Checkbox     name=databuf
    GUI::Basic::Save If Configuration Changed

SUITE:Disable Device ${DEVICE_NAME} Logging
    GUI::Basic::Managed Devices::Devices::Open Tab
    GUI::ManagedDevices::Enter Device    ${DEVICE_NAME}
    GUI::ManagedDevices::Devices::Open Logging Menu
    Unselect Checkbox     name=databuf
    GUI::Basic::Save If Configuration Changed

SUITE:Device ${DEVICE_NAME} Data Log Button Should Be Visible
    GUI::Access::Table::Open Device ${DEVICE} Info Window
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be Visible   //div[@class="modal-body"]//div[@class="modal_buttons"]//input[@id="datalog"]

SUITE:Device ${DEVICE_NAME} Data Log Button Should Not Be Visible
    GUI::Access::Table::Open Device ${DEVICE} Info Window
    GUI::Basic::Spinner Should Be Invisible
    Element Should Not Be Visible   //div[@class="modal-body"]//div[@class="modal_buttons"]//input[@id="eventlog"]

SUITE:Access System Console
    ${WINDOW_TITLE}=    GUI::Access::Table::Access Console
    Sleep   5
    Press Keys  None    RETURN

    [Return]  ${WINDOW_TITLE}

SUITE:Access Device "${DEVICE_NAME}" Console
    GUI:Access::Open Table tab
    ${CURRENT_WINDOW_TITLE}=   Get Title
    Set Suite Variable   ${MAIN_WINDOW_TITLE}   ${CURRENT_WINDOW_TITLE}
    GUI::Basic::Spinner Should Be Invisible

    GUI::Access::Access ${DEVICE_NAME} Device With Console
    GUI::Basic::Spinner Should Be Invisible

#    TTYD terminal elements
    Wait Until Page Contains Element    xpath=//*[@id='termwindow']
    Select Frame    xpath=//*[@id='termwindow']
    Current Frame Should Contain    terminal-container
    Current Frame Should Contain    xterm-viewport
    Current Frame Should Contain    xterm-screen
#    TTYD xterm.js elements
    Wait Until Page Contains Element    css=.xterm-helpers
    Wait Until Page Contains Element    css=.xterm-helper-textarea
    Wait Until Page Contains Element    css=.xterm-text-layer
    Wait Until Page Contains Element    css=.xterm-selection-layer
    Wait Until Page Contains Element    css=.xterm-link-layer
    Wait Until Page Contains Element    css=.xterm-cursor-layer
    Press Keys  None    RETURN
    Press Keys  None    RETURN

SUITE:Clear System Data Log
    ${CURRENT_WINDOW_TITLE}=   Get Title
    Set Suite Variable   ${MAIN_WINDOW_TITLE}   ${CURRENT_WINDOW_TITLE}
    ${TITLE}=  SUITE:Access System Console
    Set Suite Variable  ${CONSOLE_WINDOW_TITLE}  ${TITLE}

    ${OUTPUT}=  GUI:Access::Generic Console Command Output  TAB+TAB
    Should Contain  ${OUTPUT}   access_system_audit  access_system_clear

    ${OUTPUT}=  GUI:Access::Generic Console Command Output  access_system_clear

SUITE:Execute Some Commands From System Console
	Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists	${CONSOLE_WINDOW_TITLE} - Console
	...	ELSE	GUI:Basic::Wait Until Window Exists	${CONSOLE_WINDOW_TITLE}

    ${CONSOLE_DATA_LOG}=  GUI:Access::Generic Console Command Output  whoami
    ${CONSOLE_DATA_LOG}=  GUI:Access::Generic Console Command Output  ls
    ${CONSOLE_DATA_LOG}=  GUI:Access::Generic Console Command Output  cd
    ${CONSOLE_DATA_LOG}=  GUI:Access::Generic Console Command Output  hostname

SUITE:System Data Log Accessed From Console Should Be Empty
	Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists	${CONSOLE_WINDOW_TITLE} - Console
	...	ELSE	GUI:Basic::Wait Until Window Exists	${CONSOLE_WINDOW_TITLE}

    ${OUTPUT}=  GUI:Access::Generic Console Command Output  access_system_audit
    ${CONSOLE_DATA_LOG}=  SUITE:Get Data Log Console Content
    ${CONSOLE_DATA_LOG}=  Remove String                ${CONSOLE_DATA_LOG}    \r  \n
    ${CONSOLE_DATA_LOG}=  Remove String                ${CONSOLE_DATA_LOG}    ${DISCARDED_LOG_CONTENT}
    ${CONSOLE_DATA_LOG}=  Remove String Using Regexp   ${CONSOLE_DATA_LOG}    ${CONSOLE_LOG_END_REGEX}
    Should Be Empty                                    ${CONSOLE_DATA_LOG}

SUITE:System Data Log Accessed From Console Should Have Right Content
	Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists	${CONSOLE_WINDOW_TITLE} - Console
	...	ELSE	GUI:Basic::Wait Until Window Exists	${CONSOLE_WINDOW_TITLE}

    ${OUTPUT}=  GUI:Access::Generic Console Command Output  access_system_audit
    ${CONSOLE_DATA_LOG}=  SUITE:Get Data Log Console Content
    ${CONSOLE_DATA_LOG}=  Remove String                ${CONSOLE_DATA_LOG}    \r  \n
    Should Contain                                     ${CONSOLE_DATA_LOG}    ${EXPECTED_SYS_LOG_CONTENT_WITHOUT_SPACE}

SUITE:System Data Log Accessed From PDF Should Be Empty
    Switch Window   title=${MAIN_WINDOW_TITLE}
    GUI::Access::Table::Open Data Log PDF

    ${PDF_DATA_LOG}=    SUITE:Get Page Text Without New Lines
    ${PDF_DATA_LOG}=  Remove String                ${PDF_DATA_LOG}    \r  \n
    ${PDF_DATA_LOG}=  Remove String                ${PDF_DATA_LOG}    ${DISCARDED_LOG_CONTENT}
    ${PDF_DATA_LOG}=  Remove String Using Regexp   ${PDF_DATA_LOG}    ${PDF_LOG_PAGE_START_REGEX}
    Log	\n\n++++++++++++++++++++++++++++ Text from PDF: (Should Be Empty)++++++++++++++++++++++++++++\n${PDF_DATA_LOG}\n	INFO	console=yes
    Should Be Empty                                ${PDF_DATA_LOG}

SUITE:System Data Log Accessed From PDF Should Have Right Content
    Switch Window   title=${MAIN_WINDOW_TITLE}
    GUI::Access::Table::Open Data Log PDF

    ${PDF_DATA_LOG}=    SUITE:Get Page Text Without New Lines
    ${PDF_DATA_LOG}=  Remove String                ${PDF_DATA_LOG}    \r  \n
    Log	\n\n++++++++++++++++++++++++++++ Text from PDF: (Should Not Be Empty)++++++++++++++++++++++++++++\n${PDF_DATA_LOG}\n	INFO	console=yes
    Sleep		5s
    Should Contain                                 ${PDF_DATA_LOG}    ${EXPECTED_SYS_LOG_CONTENT}

SUITE:Clear Device ${DEVICE_NAME} Data Log
    ${CURRENT_WINDOW_TITLE}=   Get Title
    Set Suite Variable   ${MAIN_WINDOW_TITLE}   ${CURRENT_WINDOW_TITLE}
    ${TITLE}=  SUITE:Access System Console
    Set Suite Variable  ${CONSOLE_WINDOW_TITLE}  ${TITLE}

    ${OUTPUT}=  GUI:Access::Generic Console Command Output  ls
    Should Contain   ${OUTPUT}  access/

    ${OUTPUT}=  GUI:Access::Generic Console Command Output  cd access/
    Should Not Contain   ${OUTPUT}   Error: Invalid path: access/

    ${OUTPUT}=  GUI:Access::Generic Console Command Output  ls
    Should Contain   ${OUTPUT}  ${DEVICE}/

    ${OUTPUT}=  GUI:Access::Generic Console Command Output  cd ${DEVICE}/
    Should Not Contain   ${OUTPUT}   Error: Invalid path: ${DEVICE}/

    ${OUTPUT}=  GUI:Access::Generic Console Command Output  ls
    Should Contain   ${OUTPUT}  ${DEVICE}

    ${OUTPUT}=  GUI:Access::Generic Console Command Output  TAB+TAB
    Should Contain   ${OUTPUT}  access_log_clear    access_log_audit

    ${OUTPUT}=  GUI:Access::Generic Console Command Output  access_log_clear

SUITE:Device "${DEVICE_NAME}" Data Log Accessed From Console Should Be Empty
	Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists	${CONSOLE_WINDOW_TITLE} - Console
	...	ELSE	GUI:Basic::Wait Until Window Exists	${CONSOLE_WINDOW_TITLE}

    ${OUTPUT}=  GUI:Access::Generic Console Command Output  access_log_audit
    ${CONSOLE_DATA_LOG}=  SUITE:Get Data Log Console Content
    ${CONSOLE_DATA_LOG}=  Remove String                ${CONSOLE_DATA_LOG}    \r  \n
    ${CONSOLE_DATA_LOG}=  Remove String Using Regexp   ${CONSOLE_DATA_LOG}    ${CONSOLE_LOG_END_REGEX}
    Should Be Empty                                    ${CONSOLE_DATA_LOG}

SUITE:Execute Some Commands From Device "${DEVICE_NAME}" Console
    Switch Window   title=${MAIN_WINDOW_TITLE}
    SUITE:Access Device "${DEVICE}" Console

    ${CONSOLE_DATA_LOG}=  GUI:Access::Generic Console Command Output  ls
    ${CONSOLE_DATA_LOG}=  GUI:Access::Generic Console Command Output  cd
    ${CONSOLE_DATA_LOG}=  GUI:Access::Generic Console Command Output  whoami
    ${CONSOLE_DATA_LOG}=  GUI:Access::Generic Console Command Output  hostname
    ${CONSOLE_DATA_LOG}=  GUI:Access::Generic Console Command Output  pwd

    Close Window

SUITE:Device "${DEVICE_NAME}" Data Log Accessed From Console Should Be Have Right Content
	Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists	${CONSOLE_WINDOW_TITLE} - Console
	...	ELSE	GUI:Basic::Wait Until Window Exists	${CONSOLE_WINDOW_TITLE}

    ${OUTPUT}=  GUI:Access::Generic Console Command Output  access_log_audit
    ${CONSOLE_DATA_LOG}=  SUITE:Get Data Log Console Content
    ${CONSOLE_DATA_LOG}=  Remove String                ${CONSOLE_DATA_LOG}    \r  \n
    ${CONSOLE_DATA_LOG}=  Remove String Using Regexp   ${CONSOLE_DATA_LOG}    ${ATTACH_LOG_REGEX}
    ${CONSOLE_DATA_LOG}=  Remove String Using Regexp   ${CONSOLE_DATA_LOG}    ${DETACH_LOG_REGEX}
    Should Contain                                     ${CONSOLE_DATA_LOG}    ${EXPECTED_DEV_LOG_CONTENT_WITHOUT_SPACE}

SUITE:Device "${DEVICE}" Data Log Accessed From PDF Should Be Empty
    Switch Window   title=${MAIN_WINDOW_TITLE}
    GUI::Access::Table::Open Device "${DEVICE}" Data Log PDF

    ${PDF_DATA_LOG}=  SUITE:Get Page Text Without New Lines
    ${PDF_DATA_LOG}=  Remove String                ${PDF_DATA_LOG}    \r  \n
    ${PDF_DATA_LOG}=  Remove String Using Regexp   ${PDF_DATA_LOG}    ${PDF_LOG_PAGE_START_REGEX}
    Log	\n\n++++++++++++++++++++++++++++ Text from PDF: (Should Be Empty)++++++++++++++++++++++++++++\n${PDF_DATA_LOG}\n	INFO	console=yes
    Should Be Empty                                ${PDF_DATA_LOG}

SUITE:Device "${DEVICE}" Data Log Accessed From PDF Should Be Have Right Content
    Switch Window   title=${MAIN_WINDOW_TITLE}
    GUI::Access::Table::Open Device "${DEVICE}" Data Log PDF

    ${PDF_DATA_LOG}=  SUITE:Get Page Text Without New Lines
    ${PDF_DATA_LOG}=  Remove String                ${PDF_DATA_LOG}    \r  \n
    Log	\n\n++++++++++++++++++++++++++++ Text from PDF: (Should Not Be Empty)++++++++++++++++++++++++++++\n${PDF_DATA_LOG}\n	INFO	console=yes
    Should Contain                                 ${PDF_DATA_LOG}    ${EXPECTED_DEV_LOG_CONTENT}

SUITE:Get Data Log Console Content
	${STATUS}	Run Keyword And Return Status	Select Frame	xpath=//*[@id='termwindow']
	Run Keyword If	${STATUS}	Sleep	15s
	Run Keyword If	${STATUS}	Press Keys      //body      RETURN
    Wait Until Keyword Succeeds    1m    1s     Execute JavaScript  term.selectAll();
    ${CONSOLE_CONTENT}=    Wait Until Keyword Succeeds    1m    1s     Execute Javascript   return term.getSelection().trim();
    ${SEPARATOR}=   Get Regexp Matches   ${CONSOLE_CONTENT}   [\\n|\\r\\n|\\r]{4,}
    ${SEPARATOR}=   Get From List   ${SEPARATOR}   0
    ${CONSOLE_PARTS}=   Split String    ${CONSOLE_CONTENT}  ${SEPARATOR}
    ${DATA_LOG}=    Get From List   ${CONSOLE_PARTS}    1
    [Return]    ${DATA_LOG}

SUITE:Get Page Text Without New Lines
	Press Keys	None	CTRL+A
	Press Keys	None	CTRL+C
    Switch Window   title=${MAIN_WINDOW_TITLE}
    Click Element	//div[@id='modal']//span[@aria-hidden='true'][normalize-space()='×']
    GUI::Basic::Spinner Should Be Invisible
    Click Element	//input[@id='search_expr']
    Press Keys   //input[@id='search_expr']    CTRL+L
    Capture Page Screenshot
    GUI::Basic::Spinner Should Be Invisible
    Click Element	//input[@id='search_expr']
    ${PDF_CONTENT}=	Get Value	//input[@id='search_expr']
    ${PDF_CONTENT}=     Remove String   ${PDF_CONTENT}  \r  \n
    [Return]    ${PDF_CONTENT}