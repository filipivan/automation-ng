*** Settings ***
Resource	../../init.robot
Documentation	Tests for PDU Operations with regular and remote user
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EDGE	IE	DEPENDENCE_PDU	NON-CRITICAL
Default Tags	ACCESS	PDU

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEV_NAME} 	${PDU_NAME}
${DEV_TYPE} 	${PDU_TYPE}
${DEV_IP} 	 ${PDU_IP}
${DEV_USERNAME} 	${PDU_USERNAME}
${DEV_PASSWORD}	${PDU_PASSWORD}
${DEV_MODE}	on-demand
${SERVER115}	192.168.2.115
${SECRET}	${RADIUSSERVER_SECRET}
${COMMON_USER}	${RADIUSSERVER2_USER_TEST1}
${COMMON_PASS}	${RADIUSSERVER2_USER_TEST1_PASSWORD}
${METHOD}	RADIUS
${RADIUS_ACCOUNTING_PORT}	default
${STATUS}	Enabled
${RADIUS_PORT}	default
${OUTLET1_NAME}		TowerA_InfeedA_Outlet1
${PDU_ID}	A
${OUTLET_ID}	AA1

*** Test Cases ***
Test case to add pdu device
	SUITE:Add device
	SUITE:Add SNMPV2 community
	SUITE:Add SNMP protocol

Test case to check outlets are discoverd
	SUITE:perform auto discovery
	SUITE:check outlets discovered

Test case to Add users
	SUITE:Delete users if exists
	SUITE:Add regular user
	SUITE:Configure RADIUS server	${SERVER115}

Test case to perform outlet operations with admin user
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Table::Device Name
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	css=.btn:nth-child(4)
	Click Button	css=.btn:nth-child(4)
	GUI::Basic::Spinner Should Be Invisible
	Page Should not contain	unknown
	Select Checkbox	//*[@id="${DEV_NAME}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[1]/input
	Click Button	//*[@id="nonAccessControls"]/input[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	Wait Until Element Contains	//*[@id="${DEV_NAME}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[6]	Off
	Select Checkbox	//*[@id="${DEV_NAME}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[1]/input
	Click Button	//*[@id="nonAccessControls"]/input[1]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	Wait Until Element Contains	//*[@id="${DEV_NAME}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[6]	On
	Select Checkbox	//*[@id="${DEV_NAME}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[1]/input
	Click Button	//*[@id="nonAccessControls"]/input[2]
	GUI::Basic::Spinner Should Be Invisible
	Click Button 	//*[@id="nonAccessControls"]/input[4]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	//*[@id="${DEV_NAME}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[6]	Off
	Select Checkbox	//*[@id="${DEV_NAME}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[1]/input
	Click Button	//*[@id="nonAccessControls"]/input[3]
	GUI::Basic::Spinner Should Be Invisible
	Click Button 	//*[@id="nonAccessControls"]/input[4]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	//*[@id="${DEV_NAME}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[6]	On
	Close Browser

Test case to perform outlet operations with regular grumpy user
	GUI::Basic::Open And Login Nodegrid	grumpy	grumpy
	GUI::Basic::Spinner Should Be Invisible
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Table::Device Name
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	css=.btn:nth-child(4)
	Click Button	css=.btn:nth-child(4)
	GUI::Basic::Spinner Should Be Invisible
	Page Should not contain	unknown
	Select Checkbox	//*[@id="${DEV_NAME}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[1]/input
	Click Button	//*[@id="nonAccessControls"]/input[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	Wait Until Element Contains	//*[@id="${DEV_NAME}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[6]	Off
	Select Checkbox	//*[@id="${DEV_NAME}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[1]/input
	Click Button	//*[@id="nonAccessControls"]/input[1]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	Wait Until Element Contains	//*[@id="${DEV_NAME}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[6]	On
	Select Checkbox	//*[@id="${DEV_NAME}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[1]/input
	Click Button	//*[@id="nonAccessControls"]/input[2]
	GUI::Basic::Spinner Should Be Invisible
	Click Button 	//*[@id="nonAccessControls"]/input[4]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	//*[@id="${DEV_NAME}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[6]	Off
	Select Checkbox	//*[@id="${DEV_NAME}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[1]/input
	Click Button	//*[@id="nonAccessControls"]/input[3]
	GUI::Basic::Spinner Should Be Invisible
	Click Button 	//*[@id="nonAccessControls"]/input[4]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	//*[@id="${DEV_NAME}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[6]	On
	Close Browser

Test case to perform outlet operations with remote user
	GUI::Basic::Open And Login Nodegrid	${COMMON_USER}	${COMMON_PASS}
	GUI::Basic::Spinner Should Be Invisible
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Table::Device Name
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	css=.btn:nth-child(4)
	Click Button	css=.btn:nth-child(4)
	GUI::Basic::Spinner Should Be Invisible
	Page Should not contain	unknown
	Select Checkbox	//*[@id="${DEV_NAME}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[1]/input
	Click Button	//*[@id="nonAccessControls"]/input[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	Wait Until Element Contains	//*[@id="${DEV_NAME}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[6]	Off
	Select Checkbox	//*[@id="${DEV_NAME}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[1]/input
	Click Button	//*[@id="nonAccessControls"]/input[1]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	Wait Until Element Contains	//*[@id="${DEV_NAME}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[6]	On
	Select Checkbox	//*[@id="${DEV_NAME}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[1]/input
	Click Button	//*[@id="nonAccessControls"]/input[2]
	GUI::Basic::Spinner Should Be Invisible
	Click Button 	//*[@id="nonAccessControls"]/input[4]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	//*[@id="${DEV_NAME}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[6]	Off
	Select Checkbox	//*[@id="${DEV_NAME}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[1]/input
	Click Button	//*[@id="nonAccessControls"]/input[3]
	GUI::Basic::Spinner Should Be Invisible
	Click Button 	//*[@id="nonAccessControls"]/input[4]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	//*[@id="${DEV_NAME}:${PDU_ID}:${OUTLET_ID}:${OUTLET1_NAME}"]/td[6]	On
	Close Browser

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::ManagedDevices::Delete Device If Exists	${DEV_NAME}
	GUI::Basic::Spinner Should Be Invisible

SUITE:Add SNMPV2 community
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Link	${DEV_NAME}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spmmgmt_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="snmp_proto_enabled"]
	Input Text	//*[@id="community2"]	private
	GUI::Basic::Save

SUITE:Add SNMP protocol
	Click Element	//*[@id="spmcommands_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="Outlet"]
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	//*[@id="protocol"]	SNMP
	GUI::Basic::Save

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::ManagedDevices::Delete Device If Exists	${DEV_NAME}
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Delete users if exists
	SUITE:Delete RADIUS server
	GUI::Basic::Logout and Close Nodegrid

SUITE:perform auto discovery
	GUI::Basic::Managed Devices::Auto Discovery::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="discovery_now"]
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Disabled	//*[@id="discovernow"]
	Page Should Contain Element	//*[@id="discovernowTable"]
	Select Checkbox	//*[@id="pdu|${DEV_NAME}"]/td[1]/input
	Click Button	//*[@id="discovernow"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:check outlets discovered
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	${DEV_NAME}
	Click Link	${DEV_NAME}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spmoutlets_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	TowerA_InfeedA_Outlet1
	Page Should Contain	TowerA_InfeedA_Outlet2

SUITE:Delete users if exists
	GUI::Basic::Security::Local Accounts::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Wait Until Element Is Accessible	jquery=#user_namesTable
	${TEST_ACCOUNT_EXISTS}=	Run Keyword And Return Status	GUI::Element Exists	//*[@id="grumpy"]
	Run Keyword If	${TEST_ACCOUNT_EXISTS}	GUI::Basic::Delete Rows In Table	\#user_namesTable	grumpy

SUITE:Add regular user
	GUI::Basic::Security::Local Accounts::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Local Accounts::Add	grumpy

SUITE:Configure RADIUS server
	[Arguments]	${IP_ADDRESS}
	GUI::Security::Open Authentication Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	Wait Until Page Contains Element   xpath=//*[@id="mauthMethod"]
	Select From List By Label   xpath=//*[@id="mauthMethod"]	${METHOD}
	Select From List By Label	//*[@id="mauthStatus"]	${STATUS}
	Select Checkbox	//*[@id="mauthFallback"]
	Input Text	//*[@id="mauthServer"]	${IP_ADDRESS}
	Input Text	//*[@id="radAccServer"]	${IP_ADDRESS}
	Input Text	//*[@id="radauthport"]	${RADIUS_PORT}
	Input Text	//*[@id="radacctport"]	${RADIUS_ACCOUNTING_PORT}
	Input Text	//*[@id="radsecret"]	${RADIUSSERVER_SECRET}
	Input Text	//*[@id="radsecret2"]	${RADIUSSERVER_SECRET}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Add device
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=spm_name	${DEV_NAME}
	Select From List By Label	id=type	${DEV_TYPE}
	Input Text	id=phys_addr	${DEV_IP}
	Select From List By Label	id=status	On-demand
	GUI::Basic::Save

SUITE:Delete RADIUS server
	GUI::Security::Open Authentication Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="1"]/td[1]/input
	GUI::Basic::Delete