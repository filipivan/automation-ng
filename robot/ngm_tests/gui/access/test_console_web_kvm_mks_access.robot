*** Settings ***
Documentation	Test if Console, Web, KVM and MKS buttons are opening their window sessions without error
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../init.robot
Force Tags	PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE}  template

*** Test Cases ***
Test Console Button Access
	[Setup]	SUITE:Add Console Device
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' <= '5.6'	click element		//*[@id="|template"]/td[2]/div/a[1]
	...	ELSE	click element		//*[@id="|template|"]/td[2]/div/a[1]
	GUI::Basic::Spinner Should Be Invisible	SPINNER_TIMEOUT=10s
	[Teardown]	SUITE:Teardown to test case

Test Web Button Access
	[Setup]	SUITE:Add Console Device
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	wait until page contains element		xpath=//a[contains(text(),'Web')]
	click element		xpath=//a[contains(text(),'Web')]
	GUI::Basic::Spinner Should Be Invisible	SPINNER_TIMEOUT=10s
	[Teardown]	SUITE:Teardown to test case

Test Case Wget and Install VMRC Package
	SUITE:Wget VMRC file in /var/sw
	GUI::ManagedDevices::Open VM Manager
	GUI::Basic::Spinner Should Be Invisible
	Wait until Element Is Visible	//*[@id="vmrc_bundle"]	timeout=45s
	Click Element	//*[@id="vmrc_bundle"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//label[normalize-space()='Local System']//input[@id='vmrctypeselection']
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	//select[@id='localFilename']	${FTPSERVER2_VMRC_NAME}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=#saveButton
	Wait until Element Is Not Visible	xpath=//*[@id='loading']	timeout=300s
	Page Should Contain Button	//input[@id='del_vmrc_bundle']

Test MKS Button Access
	[Tags]		NON-CRITICAL		NEED-REVIEW		BUG_NG_8167		EXCLUDEIN4_2		EXCLUDEIN5_0		EXCLUDEIN5_2		EXCLUDEIN5_4		EXCLUDEIN5_6
	[Documentation]  Problems with device firmware on issue 8167. Device 'MKS_DEVICE' on QA Enviroment
	${ret}=	Run Keyword And Return Status	Variable Should Not Exist	${VMMANAGER}
	Run Keyword If	${ret}	Run Keywords	Log	Variable VMMANAGER not found. Skipping test...	WARN	${TRUE}	${TRUE}
	...	AND	Skip	Skipping...
	${ret}=	Run Keyword And Return Status	Variable Should Not Exist	${MKS_TEMPLATE}
	Run Keyword If	${ret}	Run Keywords	Log	Variable MKS_TEMPLATE not found. Skipping test...	WARN	${TRUE}	${TRUE}
	...	AND	Skip	Skipping...

	Wait until Keyword Succeeds	3x	5s	SUITE:Add MKS Device
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Access ${MKS_DEVICE} Device With MKS
	[Teardown]	Run Keywords	SUITE:Teardown	AND	SUITE:Teardown to test case

Test KVM Button Access
	${ret}=	Run Keyword And Return Status	Variable Should Not Exist	${KVM_DEVICE["name"]}
	Run Keyword If	${ret}	Run Keywords	Log	Variable KVM_DEVICE not found. Skipping test...	WARN	${TRUE}	${TRUE}
	...	AND	Skip	Skipping...

	SUITE:Add KVM Device
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	Wait until Page Contains Element        xpath=//a[contains(text(),'KVM')]
	click element		xpath=//a[contains(text(),'KVM')]
	GUI::Basic::Spinner Should Be Invisible	SPINNER_TIMEOUT=10s
	[Teardown]	SUITE:Teardown to test case

Test case to remove VMRC Package
	GUI::ManagedDevices::Open VM Manager
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//input[@id='del_vmrc_bundle']
	Wait until Element Is Not Visible	xpath=//*[@id='loading']	timeout=300s
	Page Should Contain Button	//input[@id='vmrc_bundle']
	[Teardown]	SUITE:Teardown to test case

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Ignore Error		GUI::ManagedDevices::Delete All Devices
	GUI::Basic::Spinner Should Be Invisible	SPINNER_TIMEOUT=200s
	GUI::ManagedDevices::Delete All VM Managers
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete All Discovery Rules

SUITE:Teardown
	Close all Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Ignore Error		GUI::ManagedDevices::Delete All Devices
	GUI::Basic::Spinner Should Be Invisible	SPINNER_TIMEOUT=10s
	GUI::ManagedDevices::Delete All VM Managers
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete All Discovery Rules
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout
	Close all Browsers

SUITE:Teardown to test case
	Close all Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Add Console Device
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE}
	GUI::ManagedDevices::Add Device	${DEVICE}	device_console	127.0.0.1

SUITE:Add KVM Device
	GUI::ManagedDevices::Delete Device If Exists	${KVM_DEVICE["name"]}
	GUI::ManagedDevices::Add Device	${KVM_DEVICE["name"]}	${KVM_DEVICE["type"]}	${KVM_DEVICE["ip"]}	${KVM_DEVICE["username"]}	no	${KVM_DEVICE["password"]}	enabled
	SUITE:Add KVM Device Command	${KVM_DEVICE["name"]}	kvm	${KVM_DEVICE["pymod"]}

SUITE:Add KVM Device Command
	[Arguments]	${DEVICE}	${COMMAND}	${PYMOD}=${EMPTY}
	GUI::Basic::Managed Devices::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Enter Device	${DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Devices::Open Commands Menu
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	id=command	${COMMAND}
	Select Checkbox	id=enabled
	Run Keyword Unless	'${PYMOD}' == '${EMPTY}'	Select From List By Value	id=pymod_file	${PYMOD}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Add MKS Device
	# add vm mamager
	GUI::ManagedDevices::Delete All VM Managers
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=vmmanager	${VMMANAGER["ip"]}
	Input Text	id=username	${VMMANAGER["username"]}
	Input Text	id=fpasswd	${VMMANAGER["password"]}
	Input Text	id=cpasswd	${VMMANAGER["password"]}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	#Sometimes vmmanager does not connect. Added non-critical tag.
	GUI::ManagedDevices::Enable VM Manager Discover By Index	1
	${discovery_ids}=	Create List	vm|${VMMANAGER["ip"]}

	# add template device
	GUI::ManagedDevices::Delete Device If Exists	${MKS_DEVICE}
	GUI::ManagedDevices::Add Device     ${MKS_DEVICE}	${MKS_TEMPLATE["type"]}	${MKS_TEMPLATE["ip"]}	${MKS_TEMPLATE["username"]}	no	${MKS_TEMPLATE["password"]}	enabled
	# add discovery rule
	GUI::ManagedDevices::Delete All Discovery Rules
	${FIELDS}=	Create Dictionary	discovery_name=vmdisc	source=vmmanager	operation=ondemand	seed=${MKS_DEVICE}
	GUI::ManagedDevices::Add VM Manager Discovery Rule	${FIELDS}

	Wait Until Keyword Succeeds	20 times	5 seconds	GUI::ManagedDevices::Launch Discovery By Id	${discovery_ids}

	Wait Until Keyword Succeeds	20 times	5 seconds	Run Keywords	GUI::ManagedDevices::Open Devices tab
	...	AND	GUI::Basic::Table Rows Count Should Be Greater	SPMTable	0

Skip If Variable ${VAR} Does Not Exist
	# this keyword does not work and I don't know why. I sent an email to robot user mail list asking for help
	# the Variable Should Not Exist checks if the ${VAR} exists, not the ${VAR} value (the right thing to do)
	# for now, I'll copy and paste the code to make it work in the suite
	${ret}=	Run Keyword And Return Status	Variable Should Not Exist	MKS_TEMPLATE
	Run Keyword If	${ret}	Run Keywords	Log	Variable ${VAR} not found. Skipping test...	WARN	${TRUE}	${TRUE}
	...	AND	Skip	Skipping...

SUITE:Wget VMRC file in /var/sw
	CLI:Open
	CLI:Connect As Root
	CLI:Write	cd /var/sw
	Set Default Configuration    timeout=360s
	${OUTPUT}	CLI:Write	wget --user=${FTPSERVER2_USER} --password=${FTPSERVER2_PASSWORD} ${FTPSERVER2_URL}${FTPSERVER2_VMRC_PATH}
	Set Default Configuration    timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Close Connection
	Should Contain	${OUTPUT}	100%