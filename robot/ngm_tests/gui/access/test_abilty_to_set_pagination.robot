*** Settings ***
Documentation	Testing --> Test for Ability to set pagination size in Access view and Manage Devices View
Metadata        Version	1.0
Metadata        Executed At	${HOST}
Metadata        Executed with	${BROWSER}
Resource        ../init.robot
Force Tags   PART-1      GUI  ${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}      NON-CRITICAL
Default Tags	EXCLUDEIN3_2        EXCLUDEIN4_2        EXCLUDEIN5_0        EXCLUDEIN5_2        EXCLUDEIN5_4

Suite Setup	    SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${ONE_THOUSAND_DEVICES_LICENSE}      ${ONE_THOUSAND_DEVICES_ACCESS_LICENSE}
${LINE_TABLE}   1
${ELEMENT_PAGINATION_STATUS}    1
${DEVICE_NAME}          dummy
${DEVICE_IP}            127.0.0.1
${DEVICE_TYPE}          device_console
${DEVICE_STATUS}        ondemand
${DUMMY_ON_TABLE}       //*[@id="SPMTable"]//tbody//tr[@id="dummy"]//input[@type="checkbox"]
${CLONE_NAME}           dummy_clone
${CLONE_NUMBER}         900
${ADD_BUTTON}           addButton
${SAVE_BUTTON}          saveButton
${CLONE_BUTTON}         //input[contains(@value,'Clone')]
${DELETE_BUTTON}        //input[contains(@id,'delButton')]
${REFRESH_ICON}         //span[contains(@id,'refresh-icon')]
${PAGE_TWO_BUTTON}      //a[@href='javascript:void(0);'][contains(.,'2')]
${CHECKBOX_ALL_ITENS}   (//input[contains(@type,'checkbox')])[1]
${TABLE_ON_MANAGED_DEVICES}                 xpath://table[@id='SPMTable']/tbody/tr
${SEARCH_FIELD_MANAGED_DEVICES}             //input[contains(@id,'filter_field')]
${DROPDOWN_NUMBER_DEVICES_ON_PAGINATION}    //select[contains(@class,'pagination_size_select')]

*** Test Cases ***
Test Access License Page And ADD a License
    [Documentation]     This test add a license for 1000 devices
    GUI::System::License::Add License   ${ONE_THOUSAND_DEVICES_LICENSE}

Test Access Devices Page And ADD Device
    [Documentation]     This test add a device "dummy"
    # FAIL: ElementNotInteractableException: Message: Element <input id="passwordfirst" class="form-control" name="passwordfirst" type="password"> could not be scrolled into view
    #GUI::ManagedDevices::Add Dummy Device Console   dummy	device_console	127.0.0.1	0	admin	yes	admin	on-demand
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text  locator=spm_name    text=${DEVICE_NAME}
	Input Text  locator=phys_addr    text=${DEVICE_IP}
    Select From List By Value    type  ${DEVICE_TYPE}
    Select From List By Value    status  ${DEVICE_STATUS}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible

Test Clone Devices
    [Documentation]     This test creates 900 clones of the "dummy" device
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	    ${DUMMY_ON_TABLE}
    Click Element	    ${CLONE_BUTTON}
    GUI::Basic::Spinner Should Be Invisible
    Input Text  locator=spm_name    text=${CLONE_NAME}
    Input Text  locator=num_clones    text=${CLONE_NUMBER}
    Select Checkbox     locator=inc_addr
    Select From List By Value    status  ${DEVICE_STATUS}
    Click Element	locator=${SAVE_BUTTON}
    GUI::Basic::Spinner Should Be Invisible     5m  # Increased time to wait for devices to be cloned

Test Pagination on Access::Table Page
    [Documentation]     This test checks the pagination options on the "Table" page in the "Access" category
    GUI:Access::Open Table tab
    GUI::Basic::Spinner Should Be Invisible
    # Select number of devices shown in the table, refresh the screen and check the alias of the devices
    SUITE:Access:Table:Pagination     100     dummy_clone90   dummy_clone190
    SUITE:Access:Table:Pagination     250     dummy_clone240   dummy_clone490
    SUITE:Access:Table:Pagination     500     dummy_clone490   dummy_clone900
    SUITE:Access:Table:Pagination     750     dummy_clone740   dummy_clone900
    SUITE:Access:Table:Pagination     1000     dummy_clone900   dummy_clone900
    GUI::Basic::Spinner Should Be Invisible

Test Search for device on Access::Table Page
    [Documentation]     This test checks the search function on the "Table" page in the "Access" category
    GUI:Access::Open Table tab
    GUI::Basic::Spinner Should Be Invisible
    Input Text  locator=search_expr1    text=dummy_clone900
    Press Keys  search_expr1    ENTER
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Element Is Visible       locator=(//span[contains(.,'dummy_clone900')])[1]

Test Pagination on Managed Devices::Devices Page
    [Documentation]     This test checks the pagination options on the "Table" page in the "Access" category
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Managed Devices:Devices:Pagination      0       100
    SUITE:Managed Devices:Devices:Pagination      100     250
    SUITE:Managed Devices:Devices:Pagination      250     500
    SUITE:Managed Devices:Devices:Pagination      500     750
    SUITE:Managed Devices:Devices:Pagination      750     1000

Test Delete all devices
    [Documentation]     This test deletes all devices created
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    Select From List By Value    ${DROPDOWN_NUMBER_DEVICES_ON_PAGINATION}  1000
    Click Element   locator=${REFRESH_ICON}
    GUI::Basic::Spinner Should Be Invisible
    ${NUM_ELEMENTS}     Get Element Count    ${TABLE_ON_MANAGED_DEVICES}
    FOR    ${ELEMENT}    IN RANGE    ${NUM_ELEMENTS}
        ${DEVICE_NAME}   Get Text     xpath://table[@id='SPMTable']/tbody/tr[${ELEMENT+1}]/td[2]
        ${CONTAINS}      Run Keyword And Return Status      Should Be True      """dummy""" in """${DEVICE_NAME}"""
        IF    ${CONTAINS}
            Select Checkbox     //*[@id="SPMTable"]//tbody//tr[@id="${DEVICE_NAME}"]//input[@type="checkbox"]
        END
    END
    Click Element   locator=${DELETE_BUTTON}
    Handle Alert    action=ACCEPT
    GUI::Basic::Spinner Should Be Invisible     10m  # Increased time to wait for devices to be deleted
    GUI::ManagedDevices::Open Devices tab
    Page Should Not Contain     text=dummy

Test Remove License
    [Documentation]     This test removes linked licenses
    SUITE:Setup
    GUI::Basic::System::License::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete All Rows In Table If Exists	\#license_table	False

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
    Close All Browsers

SUITE:Access:Table:Pagination
    [Arguments]     ${NUMBER_DEVICES}   ${DEVICE_NAME_PAGE_ONE}  ${DEVICE_NAME_PAGE_TWO}
    Select From List By Value    ${DROPDOWN_NUMBER_DEVICES_ON_PAGINATION}  ${NUMBER_DEVICES}
    Click Element   locator=${REFRESH_ICON}
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain    text=${DEVICE_NAME_PAGE_ONE}
    ${ELEMENT_PAGINATION_STATUS}    Get Element Count    locator=${PAGE_TWO_BUTTON}
    Log     ${ELEMENT_PAGINATION_STATUS}
    IF    ${ELEMENT_PAGINATION_STATUS} > 0
        Click Element	locator=${PAGE_TWO_BUTTON}
    END
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain    text=${DEVICE_NAME_PAGE_TWO}

SUITE:Managed Devices:Devices:Pagination
    [Arguments]     ${NUM_DEVICES_MINIMUM}  ${NUM_DEVICES_PAGE}
    Select From List By Value    ${DROPDOWN_NUMBER_DEVICES_ON_PAGINATION}  ${NUM_DEVICES_PAGE}
    Click Element   locator=${REFRESH_ICON}
    GUI::Basic::Spinner Should Be Invisible
    ${NUM_LINES}    Get Element Count    ${TABLE_ON_MANAGED_DEVICES}
    Log     ${NUM_LINES}
    IF    ${NUM_LINES} >= ${NUM_DEVICES_MINIMUM} and ${NUM_LINES} <= ${NUM_DEVICES_PAGE}
            Log     ${NUM_LINES}
            IF     ${NUM_DEVICES_PAGE} < 1000
                Click Element   locator=${PAGE_TWO_BUTTON}
                GUI::Basic::Spinner Should Be Invisible
                Page Should Contain    text=dummy
            END
    ELSE
            Fail    "Pagination Not Working"
    END