*** Settings ***
Documentation   Test Map lock view button. A lot of sleeps are necessary, don't remove them, since image comparation is very sensitive
...	If it starts failing a lot, a way to solve is replace all imag comparations only for coordinate comparations
Metadata    Version 1.0
Metadata    Executed At ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot
Force Tags   GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10
Library	ImageCompare
Suite Setup     SUITE:Setup
Suite Teardown  SUITE:Teardown

*** Variables ***


*** Test Cases ***
Check if Default Value is Unlocked
	[Documentation]	Check if the default status of the unlocked view is locked as it should be. It uses the button element toggle for this.
	GUI:Access::Open Map tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	id=LockLabel
	Sleep	2s
	${TOGGLE}	Get Element Attribute	xpath=//*[@id="main_doc"]/div[2]/div[2]/div/a	toggle
	Should Be True	'${TOGGLE}' == 'false'
	Click Element	xpath=/html/body/div[8]/div[2]/div[1]/span/a[6]	#click reload
	Sleep	15s

Test Lock Map View
	[Documentation]	Lock map and check if it keeps.
	Scroll Element Into View	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div
	Wait Until Element Is Visible	locator=//*[@id="node_map"]
	Sleep	2s
	Capture Page Screenshot
	Capture Element Screenshot	locator=//*[@id="node_map"]	filename=/tmp/defaultMap.png
	${DEFAULT_COORDS}	SUITE:Get Latitude and Long
	Set Suite Variable	${DEFAULT_COORDS}
	Wait Until Element Is Visible	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div
	Sleep	2s
	Scroll Element Into View	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div
	Click Element	xpath=//*[@id="geomap_div"]/div[2]/div[1]/div/a[1]
	Sleep	2s
	Wait Until Element Is Visible	xpath=//*[@id="geomap_div"]/div[2]/div[1]/div/a[1]
	Click Element	xpath=//*[@id="geomap_div"]/div[2]/div[1]/div/a[1]
	Sleep	2s
	Wait Until Element Is Visible	xpath=//*[@id="geomap_div"]/div[2]/div[1]/div/a[1]
	Click Element	xpath=//*[@id="geomap_div"]/div[2]/div[1]/div/a[1]
	Sleep	2s
	Wait Until Element Is Visible	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div
	Sleep	2s
	Scroll Element Into View	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div
	Wait Until Element Is Visible	id=LockLabel
	Click Element	id=LockLabel
	Wait Until Element Is Visible	xpath=/html/body/div[8]/div[2]/div[1]/span/a[6]
	Click Element	xpath=/html/body/div[8]/div[2]/div[1]/span/a[6]	#click reload
	Wait Until Element Is Visible	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div
	Sleep	15s
	Scroll Element Into View	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div
	Sleep	2s
	Capture Page Screenshot
	Capture Element Screenshot	locator=//*[@id="node_map"]	filename=/tmp/pinnedMap1.png
	${PINNED_COORDS_1}	SUITE:Get Latitude and Long
	Set Suite Variable	${PINNED_COORDS_1}
	Run Keyword And Expect Error	The compared images are different.	ImageCompare.Compare Images	/tmp/pinnedMap1.png	/tmp/defaultMap.png
	${FIRST_LOCK}	SUITE:Get Latitude and Long
	${DIFF}	Run Keyword And Return Status	Lists Should Be Equal	${FIRST_LOCK}	${DEFAULT_COORD}
	Should Be True	not ${DIFF}

Test If Button Value is True
	[Documentation]	Check if the button locked it active after clicking it. It uses the button element toggle for this.
	GUI:Access::Open Map tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	${TOGGLE}	Get Element Attribute	xpath=//*[@id="main_doc"]/div[2]/div[2]/div/a	toggle
	Should Be True	'${TOGGLE}' == 'true'

Test Click Reload And Keep Locked View
	[Documentation]	Check if lock keeps after reload
	Click Element	xpath=/html/body/div[8]/div[2]/div[1]/span/a[6]	#click reload
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div
	Sleep	2s
	Scroll Element Into View	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div	#goto botton of the map
	Wait Until Element Is Visible	locator=//*[@id="node_map"]
	Capture Page Screenshot
	${RELOAD_COORDS}	SUITE:Get Latitude and Long
	Lists Should Be Equal	${RELOAD_COORDS}	${PINNED_COORDS_1}

Test Logout and Login and Keep Locked View
	[Documentation]	Check if lock keeps after logout and login
	GUI::Basic::Logout
	GUI::Basic::Login
	GUI:Access::Open Map tab
	Click Element	xpath=/html/body/div[8]/div[2]/div[1]/span/a[6]	#click reload
	Wait Until Element Is Visible	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div
	Sleep	2s
	Scroll Element Into View	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div
	Sleep	15s
	Wait Until Element Is Visible	locator=//*[@id="node_map"]
	Capture Page Screenshot
	${LOGOUT_COORDS}	SUITE:Get Latitude and Long
	Lists Should Be Equal	${LOGOUT_COORDS}	${PINNED_COORDS_1}

Test Change Map but Don't Lock and Keep Locked View
	[Documentation]	Check if change map but don't lock keeps locked view
	GUI:Access::Open Map tab
	Wait Until Element Is Visible	//*[@id="geomap_div"]/div[2]/div[1]/div/a[2]
	Click Element	//*[@id="geomap_div"]/div[2]/div[1]/div/a[2]
	Sleep	2s
	Click Element	//*[@id="geomap_div"]/div[2]/div[1]/div/a[2]
	Sleep	2s
	Wait Until Element Is Visible	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div
	Sleep	2s
	Scroll Element Into View	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div
	Sleep	15s
	Wait Until Element Is Visible	locator=//*[@id="node_map"]
	Capture Page Screenshot
	Capture Element Screenshot	locator=//*[@id="node_map"]	filename=/tmp/unpinnedMap1.png
	Run Keyword And Expect Error	The compared images are different.	ImageCompare.Compare Images	/tmp/pinnedMap1.png	/tmp/unpinnedMap1.png
	${CHANGED_COORDS}	SUITE:Get Latitude and Long
	${DIFF}	Run Keyword And Return Status	Lists Should Be Equal	${CHANGED_COORDS}	${PINNED_COORDS_1}
	Should Be True	not ${DIFF}
	Click Element	xpath=/html/body/div[8]/div[2]/div[1]/span/a[6]	#click reload
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div
	Sleep	2s
	Scroll Element Into View	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div	#goto botton of the map
	Sleep	15s
	Wait Until Element Is Visible	locator=//*[@id="node_map"]
	Capture Page Screenshot
	${CHANGED_VIEW_COORDS}	SUITE:Get Latitude and Long
	Lists Should Be Equal	${CHANGED_VIEW_COORDS}	${PINNED_COORDS_1}

Test Create Device Out of Map Range and Check if Keep Locked View
	[Documentation]	Check if a managed device out of the view changes the locked map.
	GUI::ManagedDevices::Add Device	testLockView
	GUI::ManagedDevices::Enter Device	testLockView
	Input Text	xpath=//*[@id="coordinates"]	-26.9195567,-49.0658025
	GUI::Basic::Save
	Sleep	15s
	GUI:Access::Open Map tab
	Wait Until Element Is Visible	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div
	Sleep	2s
	Scroll Element Into View	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div
	Sleep	15s
	Wait Until Element Is Visible	locator=//*[@id="node_map"]
	Capture Page Screenshot
	${WITH_DEVICE_COORDS}	SUITE:Get Latitude and Long
	Lists Should Be Equal	${WITH_DEVICE_COORDS}	${PINNED_COORDS_1}
	[Teardown]	GUI::ManagedDevices::Delete Device If Exists	testLockView

Test Unlock View
	[Documentation]	Check if after unlocked, view is back to default
	GUI:Access::Open Map tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Click Element	id=LockLabel
	Click Element	xpath=/html/body/div[8]/div[2]/div[1]/span/a[6]	#click reload
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div
	Sleep	2s
	Scroll Element Into View	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div	#goto botton of the map
	Sleep	15s
	Wait Until Element Is Visible	locator=//*[@id="node_map"]
	Capture Page Screenshot
	${UNLOCK_COORDS}	SUITE:Get Latitude and Long
	Lists Should Be Equal	${DEFAULT_COORDS}	${UNLOCK_COORDS}

Test If Value is False Again
	[Documentation]	Checks if button status is false after deactiving lock view
	GUI:Access::Open Map tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	${TOGGLE}	Get Element Attribute	xpath=//*[@id="main_doc"]/div[2]/div[2]/div/a	toggle
	Should Be True	'${TOGGLE}' == 'false'

Test Lock Map View Second Time
	[Documentation]	Checks if locking a second time in a different view works
	GUI:Access::Open Map tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Scroll Element Into View	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div
	Wait Until Element Is Visible	locator=//*[@id="node_map"]
	Sleep	2s
	Capture Page Screenshot
	Capture Element Screenshot	locator=//*[@id="node_map"]	filename=/tmp/defaultMap.png
	Wait Until Element Is Visible	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div
	Sleep	2s
	Scroll Element Into View	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div
	Click Element	xpath=//*[@id="geomap_div"]/div[2]/div[1]/div/a[1]
	Sleep	2s
	Wait Until Element Is Visible	xpath=//*[@id="geomap_div"]/div[2]/div[1]/div/a[1]
	Click Element	xpath=//*[@id="geomap_div"]/div[2]/div[1]/div/a[1]
	Sleep	2s
	Wait Until Element Is Visible	xpath=//*[@id="geomap_div"]/div[2]/div[1]/div/a[1]
	Click Element	xpath=//*[@id="geomap_div"]/div[2]/div[1]/div/a[1]
	Sleep	2s
	Wait Until Element Is Visible	xpath=//*[@id="geomap_div"]/div[2]/div[1]/div/a[1]
	Click Element	xpath=//*[@id="geomap_div"]/div[2]/div[1]/div/a[1]
	Sleep	2s
	Wait Until Element Is Visible	xpath=//*[@id="geomap_div"]/div[2]/div[1]/div/a[1]
	Click Element	xpath=//*[@id="geomap_div"]/div[2]/div[1]/div/a[1]
	Sleep	2s
	Wait Until Element Is Visible	xpath=//*[@id="geomap_div"]/div[2]/div[1]/div/a[1]
	Click Element	xpath=//*[@id="geomap_div"]/div[2]/div[1]/div/a[1]
	Sleep	2s
	Scroll Element Into View	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div
	Wait Until Element Is Visible	id=LockLabel
	Click Element	id=LockLabel
	Wait Until Element Is Visible	xpath=/html/body/div[8]/div[2]/div[1]/span/a[6]
	Click Element	xpath=/html/body/div[8]/div[2]/div[1]/span/a[6]	#click reload
	Wait Until Element Is Visible	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div
	Sleep	15s
	Scroll Element Into View	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div
	Sleep	2s
	Capture Page Screenshot
	Capture Element Screenshot	locator=//*[@id="node_map"]	filename=/tmp/pinnedMap6.png
	Run Keyword And Expect Error	The compared images are different.	ImageCompare.Compare Images	/tmp/pinnedMap6.png	/tmp/defaultMap.png
	${SECOND_LOCK}	SUITE:Get Latitude and Long
	${DIFF}	Run Keyword And Return Status	Lists Should Be Equal	${SECOND_LOCK}	${DEFAULT_COORD}
	Should Be True	not ${DIFF}
	Wait Until Element Is Visible	xpath=/html/body/div[8]/div[2]/div[1]/span/a[6]
	Click Element	xpath=/html/body/div[8]/div[2]/div[1]/span/a[6]	#click reload
	Wait Until Element Is Visible	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div
	Sleep	15s
	Scroll Element Into View	xpath=//*[@id="geomap_div"]/div[2]/div[4]/div
	Sleep	2s
	Capture Page Screenshot
	Capture Element Screenshot	locator=//*[@id="node_map"]	filename=/tmp/pinnedMap7.png
	ImageCompare.Compare Images	/tmp/pinnedMap7.png	/tmp/pinnedMap6.png

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    ${LAT_$_LONG}	Create list	lat	lng
    Set Suite Variable	${LAT_$_LONG}

SUITE:Teardown
	GUI::ManagedDevices::Delete Device If Exists	testLockView
	SUITE:Disable Lock View
    Close Browser

SUITE:Disable Lock View
	GUI:Access::Open Map tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	${TOGGLE}	Get Element Attribute	xpath=//*[@id="main_doc"]/div[2]/div[2]/div/a	toggle
	IF	'${TOGGLE}' == 'true'
		Click Element	id=LockLabel
		Sleep	2s
	END

SUITE:Get Latitude and Long
	${COORDS}	Create List
	${BOUNDS}	Execute Javascript	return map.getBounds();
	${BOUNDS}	Convert to String	${BOUNDS}
	${BOUNDS}	Split String	${BOUNDS}	,
	FOR	${BOUND}	IN	@{BOUNDS}
		${CONTAINS}	Run keyword and return status	Should Contain Any	${BOUND}	@{LAT_$_LONG}
		IF	${CONTAINS}
			Append to List	${COORDS}	${BOUND}
		END
	END
	[Return]	${COORDS}