*** Settings ***
Documentation	Test  Access page Elements
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../init.robot
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test Access SubMenu Tabs
    FOR    ${I}    IN RANGE    1    4
		Page Should Contain Element             jquery=body > div.main_menu > div > ul > li:nth-child(${I})
	END

Test Access Table Elements
    GUI::Basic::Access::Table::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain Element     //*[@id="|nodegrid"]/div[1]/div[2]/a
    Page Should Contain Element     //*[@id="|nodegrid"]/div[1]/div[3]/a
    ${Table_enabled}=               Run Keyword And Return Status           Page Should Contain Element         //*[@id="|nodegrid"]/div[2]/div/table/thead/tr/th[1]
    Click Element                   //*[@id="|nodegrid"]/div[1]/div[1]
    Run Keyword If                  ${Table_enabled}                        Page Should Not Contain Element     //*[@id="|nodegrid"]/div[2]/div/table/thead/tr/th[1]
    ...     ELSE                    Page Should Contain Element             //*[@id="|nodegrid"]/div[2]/div/table/thead/tr/th[1]
    Click Element                   //*[@id="|nodegrid"]/div[1]/div[3]
    GUI::Basic::Spinner Should Be Invisible
    sleep       5s
    FOR    ${I}    IN RANGE    1    11
        Page Should Contain Element             //*[@id="tbody"]/tr[${I}]
    END
    Element Should Be Enabled       //*[@id="eventlog"]
    Element Should Be Enabled       //*[@id="hostsysSession"]
    Click Element                   //*[@id="modal"]/div/div/div[1]/button

Test Access Tree Elements
    GUI::Basic::Access::Tree::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain Element     //*[@id="tree_div"]/ol[1]
    Page Should Contain Element     //*[@id="tree_content"]

Test Access Node Elements
    GUI::Basic::Access::Node::Open Tab
	GUI::Basic::Spinner Should Be Invisible
    Page Should Contain Element     //*[local-name()='svg']
    Click Element                   //*[local-name()='svg']

Test Access Map Elements
    [Tags]    NON-CRITICAL    NEED-REVIEW
    GUI::Basic::Access::Map::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Keyword Succeeds    3x    5s    Mouse Over     //*[@id="geomap_div"]/div[1]/div[2]/div[3]/img
    FOR    ${INDEX}    IN RANGE    1    5
        Page Should Contain Element     //*[@id="geomap_div"]/div[1]/div[2]/div[4]/div/div[1]/div/ul/li[${INDEX}]
	END

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	Close all Browsers
