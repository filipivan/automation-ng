*** Settings ***
Resource	../../init.robot
Documentation	Tests for PDU Commands Using SNMPv3
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EDGE	IE	DEPENDENCE_DEVICE	DEPENDENCE_PDU		NON-CRITICAL	NEED-REVIEW
#Checked manually and run on run_test_1 the issue was not occurred
Default Tags	ACCESS	PDU

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE}	servertech
${CLONE_NAME}	pdu_s
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${DEV_NAME} 	${PDU_TYPE}
${DEV_TYPE} 	${PDU_TYPE}
${DEV_IP} 	 ${PDU_IP}
${DEV_USERNAME} 	${PDU_USERNAME}
${DEV_PASSWORD}	${PDU_PASSWORD}
${DEV_STATUS}	enabled
${DEVCON_ASKPASS}	no
${DEVCON_URL}	http://%IP
${SNMPv3USER}	zpeautomation
${SNMPv3PASSWORD}	zpeautomation

*** Test Cases ***
Test Outlets Count
	Wait Until Keyword Succeeds 	30	2	SUITE: Outlets
	Sleep	10
	${OUTLETS_NUM}=	Get Element Count	//tbody/tr
	Should Be True	${OUTLETS_NUM} > 0
	GUI:Access::Open Table tab
	Page Should Contain	${DEV_NAME}
	Click Link	${DEV_NAME}
	Wait Until Element Is Visible	nonAccessControls
	${OUTLETS_NUM2}=  	Get Element Count	//tbody/tr/td/input
	Should Be Equal	${OUTLETS_NUM}	${OUTLETS_NUM2}
	[Teardown]	Click Element	xpath=//div[@id='modal']/div/div/div/button/span

Test Outlet Status
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${DEV_NAME}
	Click Link	${DEV_NAME}
	Wait Until Element Is Visible	nonAccessControls
	Wait Until Element Is Visible	xpath=//input[@value='Outlet Status']
	Click Element	xpath=//input[@value='Outlet Status']
	Wait Until Keyword Succeeds	10	10	GUI::Basic::Spinner Should Be Invisible
	Sleep	10
	Page Should Not Contain	unknown
	[Teardown]	Click Element	xpath=//div[@id='modal']/div/div/div/button/span

############# SINGLE OUTLETS ############
Test Outlet Off With Single Outlet
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${DEV_NAME}
	Click Link	${DEV_NAME}
	Wait Until Element Is Visible	nonAccessControls
	Wait Until Element Is Visible	xpath=(//input[@type='checkbox'])[2]
	Select Checkbox	xpath=(//input[@type='checkbox'])[2]
	Sleep	10
	Wait Until Element Is Visible	xpath=//input[@value='Outlet Off']
	Click Element	xpath=//input[@value='Outlet Off']
	Wait Until Keyword Succeeds	10	10	GUI::Basic::Spinner Should Be Invisible
	Sleep	30
	Element Should Contain	xpath=//td[6]	Off
	[Teardown]	Click Element	xpath=//div[@id='modal']/div/div/div/button/span

Test Outlet On With Single Outlet
	Sleep	2
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${DEV_NAME}
	Click Link	${DEV_NAME}
	Wait Until Element Is Visible	nonAccessControls
	Wait Until Element Is Visible	xpath=(//input[@type='checkbox'])[2]
	Select Checkbox	xpath=(//input[@type='checkbox'])[2]
	Sleep	10
	Wait Until Element Is Visible	xpath=//input[@value='Outlet Off']
	Click Element	xpath=//input[@value='Outlet Off']
	Wait Until Keyword Succeeds	10	10	GUI::Basic::Spinner Should Be Invisible
	Sleep	30
	Element Should Contain	xpath=//td[6]	Off
	Wait Until Element Is Visible	xpath=(//input[@type='checkbox'])[2]
	Select Checkbox	xpath=(//input[@type='checkbox'])[2]
	Sleep	10
	Wait Until Element Is Visible	xpath=//input[@value='Outlet On']
	Click Element	xpath=//input[@value='Outlet On']
	Wait Until Keyword Succeeds	10	10	GUI::Basic::Spinner Should Be Invisible
	Sleep	30
	wait until page contains		On
	Wait Until Keyword Succeeds	30	2	page should contain element	xpath=//td[6]	On
	[Teardown]	Click Element	xpath=//div[@id='modal']/div/div/div/button/span

Test Outlet Cycle With Single Outlet
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${DEV_NAME}
	Click Link	${DEV_NAME}
	Wait Until Element Is Visible	nonAccessControls
	Wait Until Element Is Visible	xpath=(//input[@type='checkbox'])[2]
	Select Checkbox	xpath=(//input[@type='checkbox'])[2]
	Sleep	10
	Wait Until Element Is Visible	xpath=//input[@value='Outlet Cycle']
	Click Element	xpath=//input[@value='Outlet Cycle']
	Wait Until Keyword Succeeds	10	10	GUI::Basic::Spinner Should Be Invisible
	Sleep	30
	Wait Until Keyword Succeeds	30	2	Element Should Contain	xpath=//td[6]	On
	[Teardown]	Click Element	xpath=//div[@id='modal']/div/div/div/button/span

############ MULTIPLE OUTLETS ############
Test Outlet Off With Multiple Outlets
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${DEV_NAME}
	Click Link	${DEV_NAME}
	Wait Until Element Is Visible	nonAccessControls
	Wait Until Element Is Visible	xpath=(//input[@type='checkbox'])[2]
	Select Checkbox	xpath=(//input[@type='checkbox'])[2]
	Sleep	2
	Wait Until Element Is Visible	xpath=(//input[@type='checkbox'])[3]
	Select Checkbox	xpath=(//input[@type='checkbox'])[3]
	Sleep	2
	Wait Until Element Is Visible	xpath=(//input[@type='checkbox'])[4]
	Select Checkbox	xpath=(//input[@type='checkbox'])[4]
	Sleep	2
	Wait Until Element Is Visible	xpath=(//input[@type='checkbox'])[5]
	Select Checkbox	xpath=(//input[@type='checkbox'])[5]
	Sleep	10
	Wait Until Element Is Visible	xpath=//input[@value='Outlet Off']
	Click Element	xpath=//input[@value='Outlet Off']
	Wait Until Keyword Succeeds	10	10	GUI::Basic::Spinner Should Be Invisible
	Sleep	30
	Element Should Contain	xpath=//td[6]	Off
	[Teardown]	Click Element	xpath=//div[@id='modal']/div/div/div/button/span

Test Outlet On With Multiple Outlets
	Sleep	2
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${DEV_NAME}
	Click Link	${DEV_NAME}
	Wait Until Element Is Visible	nonAccessControls
	Wait Until Element Is Visible	xpath=(//input[@type='checkbox'])[2]
	Select Checkbox	xpath=(//input[@type='checkbox'])[2]
	Sleep	2
	Wait Until Element Is Visible	xpath=(//input[@type='checkbox'])[3]
	Select Checkbox	xpath=(//input[@type='checkbox'])[3]
	Sleep	2
	Wait Until Element Is Visible	xpath=(//input[@type='checkbox'])[4]
	Select Checkbox	xpath=(//input[@type='checkbox'])[4]
	Sleep	2
	Wait Until Element Is Visible	xpath=(//input[@type='checkbox'])[5]
	Select Checkbox	xpath=(//input[@type='checkbox'])[5]
	Sleep	10
	Wait Until Element Is Visible	xpath=//input[@value='Outlet Off']
	Click Element	xpath=//input[@value='Outlet Off']
	Wait Until Keyword Succeeds	10	10	GUI::Basic::Spinner Should Be Invisible
	Sleep	30
	Element Should Contain	xpath=//td[6]	Off
	Wait Until Element Is Visible	xpath=(//input[@type='checkbox'])[2]
	Select Checkbox	xpath=(//input[@type='checkbox'])[2]
	Sleep	2
	Wait Until Element Is Visible	xpath=(//input[@type='checkbox'])[3]
	Select Checkbox	xpath=(//input[@type='checkbox'])[3]
	Sleep	2
	Wait Until Element Is Visible	xpath=(//input[@type='checkbox'])[4]
	Select Checkbox	xpath=(//input[@type='checkbox'])[4]
	Sleep	2
	Wait Until Element Is Visible	xpath=(//input[@type='checkbox'])[5]
	Select Checkbox	xpath=(//input[@type='checkbox'])[5]
	Sleep	10
	Wait Until Element Is Visible	xpath=//input[@value='Outlet On']
	Click Element	xpath=//input[@value='Outlet On']
	Wait Until Keyword Succeeds	10	10	GUI::Basic::Spinner Should Be Invisible
	Sleep	30
	Wait Until Keyword Succeeds	30	2	Element Should Contain	xpath=//td[6]	On

	[Teardown]	Click Element	xpath=//div[@id='modal']/div/div/div/button/span

Test Outlet Cycle With Multiple Outlets
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${DEV_NAME}
	Click Link	${DEV_NAME}
	Wait Until Element Is Visible	nonAccessControls
	Wait Until Element Is Visible	xpath=(//input[@type='checkbox'])[2]
	Select Checkbox	xpath=(//input[@type='checkbox'])[2]
	Sleep	2
	Wait Until Element Is Visible	xpath=(//input[@type='checkbox'])[3]
	Select Checkbox	xpath=(//input[@type='checkbox'])[3]
	Sleep	2
	Wait Until Element Is Visible	xpath=(//input[@type='checkbox'])[4]
	Select Checkbox	xpath=(//input[@type='checkbox'])[4]
	Sleep	2
	Wait Until Element Is Visible	xpath=(//input[@type='checkbox'])[5]
	Select Checkbox	xpath=(//input[@type='checkbox'])[5]
	Sleep	10
	Wait Until Element Is Visible	xpath=//input[@value='Outlet Cycle']
	Click Element	xpath=//input[@value='Outlet Cycle']
	Wait Until Keyword Succeeds	10	10	GUI::Basic::Spinner Should Be Invisible
	Sleep	30
	Wait Until Keyword Succeeds	30	2	Element Should Contain	xpath=//td[6]	On
	[Teardown]	Click Element	xpath=//div[@id='modal']/div/div/div/button/span

############ ALL OUTLETS ############
Test Outlet Off With All Outlets
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${DEV_NAME}
	Click Link	${DEV_NAME}
	Wait Until Element Is Visible	nonAccessControls
	Wait Until Element Is Visible	//*[@id="thead"]/tr/th[1]/input
	Click Element	//*[@id="thead"]/tr/th[1]/input
	Sleep	10
	Wait Until Element Is Visible	xpath=//input[@value='Outlet Off']
	Click Element	xpath=//input[@value='Outlet Off']
	Wait Until Keyword Succeeds	10	10	GUI::Basic::Spinner Should Be Invisible
	Sleep	30
	Element Should Contain	xpath=//td[6]	Off
	[Teardown]	Click Element	xpath=//div[@id='modal']/div/div/div/button/span

Test Outlet On With All Outlets
	Sleep	2
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${DEV_NAME}
	Click Link	${DEV_NAME}
	Wait Until Element Is Visible	nonAccessControls
	Wait Until Element Is Visible	//*[@id="thead"]/tr/th[1]/input
	Select Checkbox	//*[@id="thead"]/tr/th[1]/input
	Sleep	10
	Wait Until Element Is Visible	xpath=//input[@value='Outlet Off']
	Click Element	xpath=//input[@value='Outlet Off']
	Wait Until Keyword Succeeds	10	10	GUI::Basic::Spinner Should Be Invisible
	Sleep	30
	Element Should Contain	xpath=//td[6]	Off
	Sleep	3
	Wait Until Element Is Visible	//*[@id="thead"]/tr/th[1]/input
	Select Checkbox	//*[@id="thead"]/tr/th[1]/input
	Sleep	10
	Wait Until Element Is Visible	xpath=//input[@value='Outlet On']
	Click Element	xpath=//input[@value='Outlet On']
	Wait Until Keyword Succeeds	10	10	GUI::Basic::Spinner Should Be Invisible
	Sleep	30
	Wait Until Keyword Succeeds	30	2	Element Should Contain	xpath=//td[6]	On

	[Teardown]	Click Element	xpath=//div[@id='modal']/div/div/div/button/span

Test Outlet Cycle With All Outlets
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${DEV_NAME}
	Click Link	${DEV_NAME}
	Wait Until Element Is Visible	nonAccessControls
	Wait Until Element Is Visible	//*[@id="thead"]/tr/th[1]/input
	Select Checkbox	//*[@id="thead"]/tr/th[1]/input
	Sleep	10
	Wait Until Element Is Visible	xpath=//input[@value='Outlet Cycle']
	Click Element	xpath=//input[@value='Outlet Cycle']
	Wait Until Keyword Succeeds	10	10	GUI::Basic::Spinner Should Be Invisible
	Sleep	30
	Wait Until Keyword Succeeds	30	2	Element Should Not Contain	xpath=//td[6]	Off
	[Teardown]	Click Element	xpath=//div[@id='modal']/div/div/div/button/span

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid	
	GUI::ManagedDevices::Delete Device If Exists	${DEV_NAME}
	GUI::ManagedDevices::Add Device	${DEV_NAME} 	${DEV_TYPE} 	${DEV_IP} 	${DEV_USERNAME}	${DEVCON_ASKPASS}	${DEV_PASSWORD}	${DEV_STATUS}	${DEVCON_URL}
	SUITE: Set SNMPv3
	SUITE: Outlet Commands
	SUITE: Discover Now

SUITE:Teardown
	GUI::ManagedDevices::Delete Device If Exists	${DEV_NAME}
	GUI::Basic::Logout
	Close all Browsers

SUITE: Set SNMPv3
	GUI::Basic::Managed Devices::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	${DEV_NAME}
	Click Link	${DEV_NAME}
	GUI::Basic::Wait Until Element Is Accessible	xpath=//a[@id='spmmgmt_nav']/span
	Click Element	xpath=//a[@id='spmmgmt_nav']/span
	Sleep	2
	Click Element	snmp_proto_enabled
	Select Radio Button	 snmpVersion	3
	Input Text	user	${SNMPv3USER}
	Select From List By Value	securityLevel	priv
	Select From List By Value	authType	MD5
	Input Text	authPassword	${SNMPv3PASSWORD}
	Select From List By Value	privType	DES
	Input Text	privPassPhrase	${SNMPv3PASSWORD}
	GUI::Basic::Save

SUITE: Outlet Commands
	GUI::Basic::Managed Devices::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	${DEV_NAME}
	Click Link	${DEV_NAME}
	Wait Until Element Is Visible	spmcommands_nav
	Click Element	spmcommands_nav
	Wait Until Element Is Visible	Outlet
	Click Link	Outlet
	Wait Until Element Is Visible	protocol
	Select From List By Value	protocol	snmp
	GUI::Basic::Save

SUITE: Discover Now
	GUI::Basic::Managed Devices::Auto Discovery::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep	10
	Click Element	discovery_now
	Wait Until Keyword Succeeds	30	2	Wait Until Page Contains Element	pdu|${DEV_NAME}
	Click Element	xpath=//*[@id="pdu|${DEV_NAME}"]/td[1]/input
	GUI::Basic::Discovery Now

SUITE: Outlets
	GUI::Basic::Managed Devices::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	${DEV_NAME}
	Click Link	${DEV_NAME}
	Wait Until Element Is Visible	spmoutlets_nav
	Click Element	spmoutlets_nav
	Sleep	5
	Click Element	spmoutlets_nav
	Page Should Contain Element	//*[@id="tbody"]/tr