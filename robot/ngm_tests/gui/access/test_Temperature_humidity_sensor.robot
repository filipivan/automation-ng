*** Settings ***
Documentation	Test  Show temperature and humidity on the Access::Sensor::Overlay
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../init.robot
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE	EXCLUDEIN4_2	NO-ENV

#Has USB-Sensor only to v5.0 nythly host device

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${WARNING_MESSAGE}	SEPARATOR=
	...	There is no USB Sensor connected to NG Host of this Jenkins build.

*** Test Cases ***
Go to Tracking :: HW Monitor :: USB Sensors and check the sensors.
	GUI::Basic::Tracking::HW Monitor::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click element  css=#usbsensors > .title_b
	GUI::Basic::Spinner Should Be Invisible
	${HAS_USB_SENSOR_TRACKING}	Run Keyword and Return Status	Wait Until Page Contains Element	//*[@id="0"]/td[1]
	Set Suite Variable	${HAS_USB_SENSOR_TRACKING}
	Run Keyword If	not ${HAS_USB_SENSOR_TRACKING}	Run Keywords	Set Tags	NON-CRITICAL	AND	Skip	${WARNING_MESSAGE}

Go to Access :: Table and check for USB sensor.
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${HAS_USB_SENSOR_ACCESS}	Run Keyword and Return Status	Page Should Contain Element	(//span[contains(text(),'usbS0-1')])[1]
	Run Keyword If	not ${HAS_USB_SENSOR_ACCESS}	Run Keywords	Set Tags	NON-CRITICAL	AND	Skip	${WARNING_MESSAGE}

Click on the USB sensor and check the Temperature and Humidity sensors data.
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${HAS_USB_SENSOR_ACCESS}	Run Keyword and Return Status	Click element	(//span[contains(text(),'usbS0-1')])[1]
	Run Keyword If	not ${HAS_USB_SENSOR_ACCESS}	Run Keywords	Set Tags	NON-CRITICAL	AND	Skip	${WARNING_MESSAGE}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="1"]/td[4]
	Wait Until Page Contains Element	//*[@id="0"]/td[4]
	Click element	(//input[@value='Sensors Status'])[1]
	@{LIST_TO_VALIDATE}	Create List	Name	Value	Unit	Description	Percent	Celsius	Humidity sensor	Temperature sensor
	...	Local Serial Port	Status	Type	Mode	Licensed	Nodegrid Host	Groups	Connected	usbS0-1
	FOR	${ITEM}	IN	@{LIST_TO_VALIDATE}
		Page Should Contain	${ITEM}
	END
	[Teardown]	Click Element	//*[@id="modal"]/div/div/div[1]/button/span

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Close all Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout
	Close all Browsers