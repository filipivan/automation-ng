*** Settings ***
Documentation	Test for Access Console directly
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EDGE	IE	NON-CRITICAL	NEED-REVIEW

*** Variables ***
${USERNAME}     admin
${PASSWORD}     admin
${URL}      https://${HOST}/direct/

*** Test Cases ***
Test case to access console Directly
    Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists    nodegrid.localdomain - Console
    ...	ELSE	GUI:Basic::Wait Until Window Exists    nodegrid.localdomain
    Select Frame	xpath=//*[@id='termwindow']
	Sleep	15s
	Press Keys      //body      RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output      ls
	Should Contain	${OUTPUT}	access/	system/	settings/

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open NodeGrid            ${URL}
	GUI::Basic::Spinner Should Be Invisible
	Input Text	username	${USERNAME}
    Input Text	password	${PASSWORD}
    Click Element	id=login-btn
    Sleep       10s


SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	Close All Browsers