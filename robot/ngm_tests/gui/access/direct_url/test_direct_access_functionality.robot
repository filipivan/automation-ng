*** Settings ***
Documentation	Test if DirectURL works with valid device and valid action
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EDGE	IE	NON-CRITICAL	NEED-REVIEW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME_ADMIN}	${DEFAULT_USERNAME}
${PASSWORD_ADMIN}	${DEFAULT_PASSWORD}
${DEVICE}	template
${INVALID_DEVICE}	invalid_template
${INVALID_ACTION}	KVM
${TELNET_PORT}	4321

*** Test Cases ***
Direct access COMMANDS with valid device and valid action | console
	[Setup]	GUI::Basic::Open And Login Nodegrid
	SUITE:Add Console Device
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open NodeGrid	${HOMEPAGE}/direct/${DEVICE}/console
	${MAIN_WINDOW_TITLE}=	Get Title
	GUI::Basic::Direct Login	${USERNAME_ADMIN}	${PASSWORD_ADMIN}	${TRUE}	${DEVICE}
	Sleep       5s

	Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists    ${DEVICE} - Console
    ...	ELSE	GUI:Basic::Wait Until Window Exists    ${DEVICE}
	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
	Select Frame	xpath=//*[@id='termwindow']
	Sleep       10s
	Press Keys	//body	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output      ls
	Should Contain	${OUTPUT}	access/	system/	settings/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	pwd
	Should Contain	${OUTPUT}	/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	hostname
	Should Contain	${OUTPUT}	nodegrid

	${OUTPUT}=	GUI:Access::Generic Console Command Output      whoami
	Should Contain	${OUTPUT}	${DEFAULT_USERNAME}

	${OUTPUT}=	GUI:Access::Generic Console Command Output	TAB+TAB

	${COMMANDS}=	Create List	apply_settings	cd	change_password	commit
	...	event_system_audit	event_system_clear	exit	factory_settings
	...	hostname	ls	pwd	quit	reboot	revert	save_settings	set
	...	shell	show	shutdown	software_upgrade	system_certificate
	...	system_config_check	whoami
    FOR	${COMMAND}	IN	@{COMMANDS}
		Should Contain	${OUTPUT}	${COMMAND}
	END
	Unselect Frame
	Press Keys	//body	cw
	[Teardown]	Close All Browsers

Direct access COPY AND PASTE with valid device and valid action | console
	SUITE:Setup
	#GUI::Basic::Open And Login Nodegrid
	#${USERLINE_LENGTH}=	GUI::Access::Get Userline Length

	GUI::Basic::Open NodeGrid	${HOMEPAGE}/direct/${DEVICE}/console
	${MAINDIRECT_WINDOW_TITLE}=	Get Title
	GUI::Basic::Direct Login	${USERNAME_ADMIN}	${PASSWORD_ADMIN}	${TRUE}	${DEVICE}
	Sleep       5s
#	Switch Window       title=${MAINDIRECT_WINDOW_TITLE}

	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	${USERLINE_LENGTH}=	GUI::Access::Get Userline Length
	${MAIN_WINDOW_TITLE}=	Get Title

	Switch Browser	1
	Sleep       5s
	Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists    ${DEVICE} - Console
    ...	ELSE	GUI:Basic::Wait Until Window Exists    ${DEVICE}
	Sleep       5s
	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
	Select Frame	xpath=//*[@id='termwindow']
	Sleep       5s
	Press Keys	//body	RETURN
	Sleep       5s

	${OUTPUT}=	GUI:Access::Generic Console Command Output      ls
	Sleep       5s
	Should Contain	${OUTPUT}	access/	system/	settings/
#	Selection of the 'ls' command full line in index 4
	Execute JavaScript	term.selectLines(4,4);
#	Copy selection of the command to clipboard
	Sleep       5s
	${ex}=	Press Keys	//body      ${COPY_KEY}
	Sleep       5s
#	Goes to the main window to paste the clipboard selection into the search field
#	and manipulates it to get only the correct command 'ls' and copy that to clipboard
	Switch Browser	2
	Switch Window	title=${MAIN_WINDOW_TITLE}
	Click Element	id:search_expr
	Sleep       5s
	Press Keys	//body      ${PASTE_KEY}	${HOME_KEY}
    FOR	${INDEX}	IN RANGE	0	${USERLINE_LENGTH}
		Press Keys	None	ARROW_RIGHT	BACKSPACE
	END
	Sleep       5s
	Press Keys	//body	${SELECT_ALL_KEY}	${COPY_KEY}
#	Goes to the console window and paste the correct command to the console
	Switch Browser	1
	Sleep       5s
	Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists    ${DEVICE} - Console
    ...	ELSE	GUI:Basic::Wait Until Window Exists    ${DEVICE}
    Sleep       5s
	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
	Select Frame	xpath=//*[@id='termwindow']
	Sleep       5s
	Press Keys	//body	RETURN
	Sleep       5s
	${OUTPUT}=	GUI:Access::Generic Console Command Output      ${PASTE_KEY}
	Sleep       5s
	Should Contain	${OUTPUT}	access/	system/	settings/

	Unselect Frame
	Press Keys	//body	cw
	[Teardown]	Close All Browsers

Direct access COMMANDS with invalid device and valid action | console
	GUI::Basic::Open NodeGrid	${HOMEPAGE}/direct/${INVALID_DEVICE}/console
	${MAIN_WINDOW_TITLE}=	Get Title
	GUI::Basic::Direct Login	${USERNAME_ADMIN}	${PASSWORD_ADMIN}	${TRUE}	${INVALID_DEVICE}
	Sleep       5s

	Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists    ${INVALID_DEVICE} - Console
    ...	ELSE	GUI:Basic::Wait Until Window Exists    ${INVALID_DEVICE}
	Click Button	xpath=//*[@id="targetParameters"]/button[1]
	Handle Alert
	Close Browser

	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add Console Device
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open NodeGrid	${HOMEPAGE}/direct/${DEVICE}/console
	${MAIN_WINDOW_TITLE}=	Get Title
	GUI::Basic::Direct Login	${USERNAME_ADMIN}	${PASSWORD_ADMIN}	${TRUE}	${DEVICE}
	Sleep       5s
	Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists    ${DEVICE} - Console
    ...	ELSE	GUI:Basic::Wait Until Window Exists    ${DEVICE}
	Sleep       5s
	Wait Until Page Contains Element	xpath=//*[@id='termwindow']

	${OUTPUT}=	GUI:Access::Generic Console Command Output      ls
	Should Contain	${OUTPUT}	access/	system/	settings/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	pwd
	Should Contain	${OUTPUT}	/

	${OUTPUT}=	GUI:Access::Generic Console Command Output	hostname
	Should Contain	${OUTPUT}	nodegrid

	${OUTPUT}=	GUI:Access::Generic Console Command Output	whoami
	Should Contain	${OUTPUT}	${DEFAULT_USERNAME}

	${OUTPUT}=	GUI:Access::Generic Console Command Output	TAB+TAB

	${COMMANDS}=	Create List	apply_settings	cd	change_password	commit
	...	event_system_audit	event_system_clear	exit	factory_settings
	...	hostname	ls	pwd	quit	reboot	revert	save_settings	set
	...	shell	show	shutdown	software_upgrade	system_certificate
	...	system_config_check	whoami

    FOR	${COMMAND}	IN	@{COMMANDS}
		Should Contain	${OUTPUT}	${COMMAND}
	END
	Sleep       5s

	Unselect Frame
	Press Keys	//body      cw
	[Teardown]	Close All Browsers

Direct access COMMANDS with valid device and invalid action | console
	Wait Until Keyword Succeeds   3 times  3 seconds  SUITE:Direct access COMMANDS with valid device and invalid action
	[Teardown]	Close All Browsers

Direct access NG
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open NodeGrid	${HOMEPAGE}/direct/
	${MAIN_WINDOW_TITLE}=	Get Title
	GUI::Basic::Direct Login	${USERNAME_ADMIN}	${PASSWORD_ADMIN}
	Sleep       2s

	Switch Window	NEW
	Wait Until Keyword Succeeds	30	2	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
	Run Keyword And Ignore Error	Select Frame	xpath=//*[@id='termwindow']
	Sleep       10s
	Press Keys	//body	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output      ls
	Should Contain	${OUTPUT}	access/	system/	settings/

	Unselect Frame
	Press Keys	//body	cw
	[Teardown]	Close All Browsers

#Test direct access on device with IPv6 as Alias
##don't have ipv6 alias in environment
#	[Tags]	NON-CRITICAL	NEED-REVIEW
#	[Setup]	GUI::Basic::Open And Login Nodegrid
#	SUITE:Add Console Device
#	GUI::ManagedDevices::Open Devices tab
#	Select Checkbox	xpath=//tr[@id='${DEVICE}']/td/input
#	GUI::Basic::Edit
#	Wait Until Page Contains Element	inbipEnable
#	Select Checkbox	inbipEnable
#	Wait Until Page Contains Element	inbipAddr
#	Input Text	inbipAddr	${IP_ALIAS_IPV6}
#	GUI::Basic::Save
#	Sleep       2s
#	GUI::Basic::Open NodeGrid       http://[${IP_ALIAS_IPV6}]/
#	${MAIN_WINDOW_TITLE}=	Get Title
#	GUI::Basic::Direct Login	${USERNAME_ADMIN}	${PASSWORD_ADMIN}	${TRUE}	${DEVICE}
#	Sleep       5s
#	Wait Until Keyword Succeeds	30	2	GUI:Basic::Wait Until Window Exists	${DEVICE}
#    Switch Window       ${DEVICE}
#	Wait Until Keyword Succeeds	30	2	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
#	Select Frame	xpath=//*[@id='termwindow']
#	Sleep       10s
#	Press Keys	//body	RETURN
#
#	${OUTPUT}=	GUI:Access::Generic Console Command Output      ls
#	Should Contain	${OUTPUT}	access/	system/	settings/
#
#	${OUTPUT}=	GUI:Access::Generic Console Command Output      pwd
#	Should Contain	${OUTPUT}	/
#
#	${OUTPUT}=	GUI:Access::Generic Console Command Output      hostname
#	Should Contain	${OUTPUT}	nodegrid
#
#	${OUTPUT}=	GUI:Access::Generic Console Command Output      whoami
#	Should Contain	${OUTPUT}	${DEFAULT_USERNAME}
#
#	${OUTPUT}=	GUI:Access::Generic Console Command Output      TAB+TAB
#
#	${COMMANDS}=	Create List	apply_settings	cd	change_password	commit
#	...	event_system_audit	event_system_clear	exit	factory_settings
#	...	hostname	ls	pwd	quit	reboot	revert	save_settings	set
#	...	shell	show	shutdown	software_upgrade	system_certificate
#	...	system_config_check	whoami
#	FOR	${COMMAND}	IN	@{COMMANDS}
#		Should Contain	${OUTPUT}	${COMMAND}
#	END
#
#	Unselect Frame
#	Press Keys	//body      cw
#	Switch Window	title=${MAIN_WINDOW_TITLE}
#	[Teardown]	Close All Browsers

Test Direct Access On Device With Tellnet Port
    [Setup]    GUI::Basic::Open And Login Nodegrid
    SUITE:Add Console Device
    SUITE:Set Telnet Port   ${TELNET_PORT}
    ${WINDOW_TITLE}=    Get Title
    Set Test Variable   ${MAIN_WINDOW_TITLE}    ${WINDOW_TITLE}
    GUI::Access::Direct URL   DEVICE_NAME=${TELNET_PORT}
    Sleep       10s
    SUITE:Executing Some Commands On Console Should Succeed
    Sleep       5s
    [Teardown]   Close All Browsers

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE}
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add Console Device
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

	Set Suite Variable	${COPY_KEY}	CTRL+ INSERT
	Set Suite Variable	${PASTE_KEY}        SHIFT+INSERT
	Set Suite Variable	${SELECT_ALL_KEY}	CTRL+A
	Set Suite Variable	${HOME_KEY}	HOME

SUITE:Teardown
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE}
	GUI::Basic::Logout And Close Nodegrid

SUITE:Add Console Device
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE}
	GUI::ManagedDevices::Add Device	${DEVICE}	device_console	127.0.0.1	${USERNAME_ADMIN}	no	${PASSWORD_ADMIN}	enabled

SUITE:Console Command Output
	[Arguments]	${COMMAND}
	[Documentation]	Input the given command into the ttyd terminal and retrieve the output after prompt lines
	...	== ARGUMENTS ==
	...	-	COMMAND = Command to be executed
	...	== EXPECTED RESULT ==
	...	Input the command into the ttyd terminal and returns the OUTPUT between the command and the last prompt

	${CHECK_TAB}=	Run Keyword And Return Status	Should Not Contain	${COMMAND}	TAB
	Run Keyword If	${CHECK_TAB}	Press Keys	None	${COMMAND}	RETURN
	...	ELSE	Press Keys	None	${COMMAND}
	Sleep	1
#	Since ttyd(xterm) draw everything in canvases, text cannot be easily extracted, and so far
#	the best approach I've found is by calling term.getSelection().trim() to get a copy of all text in the canvas.
#	Create selection, then get selection:
	${SELECTION}=	Execute JavaScript	term.selectAll(); return term.getSelection().trim();
#	Should Contain	${SELECTION}	[Enter '^Ec?' for help]
	Should Not Contain	${SELECTION}	[error.connection.failure] Could not establish a connection to device

	${LINES}=	Split String	${SELECTION}	\n
	${INDEXES_OCCURRED}=	Create List
	${LENGTH}=	Get Length	${LINES}
	FOR	${INDEX}	IN RANGE	0	${LENGTH}
		${LINE}=	Get From List	${LINES}	${INDEX}
		${CHECK}=	Run Keyword And Return Status	Should Contain	${LINE}	]#
		Run Keyword If	${CHECK}	Append to List	${INDEXES_OCCURRED}	${INDEX}
	END

	${STRING}=	Set Variable
	${END}=	Get From List	${INDEXES_OCCURRED}	-1
	${PREVIOUS}=	Get From List	${INDEXES_OCCURRED}	-2
	FOR	${INDEX}	IN RANGE	${PREVIOUS}	${END}
		${LINE}=	Get From List	${LINES}	${INDEX}
		${STRING}=	Set Variable	${STRING}\n${LINE}
	END
	${STRING}=	Get Substring	${STRING}	1
	[Return]	${STRING}

SUITE:Set Telnet Port
    [Arguments]   ${PORT}
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Select Rows In Table Containing Value  SPMTable  ${DEVICE}
    GUI::Basic::Edit
    Select Checkbox  inbteln
    Wait Until Element Is Visible   inbtelnTCP
    Input Text   inbtelnTCP    ${PORT}
    GUI::Basic::Save

SUITE:Executing Some Commands On Console Should Succeed
    ${OUTPUT}=   GUI:Access::Generic Console Command Output     whoami
    Should Contain    ${OUTPUT}    ${USERNAME_ADMIN}

    ${OUTPUT}=   GUI:Access::Generic Console Command Output     hostname
    Should Contain    ${OUTPUT}    ${HOSTNAME_NODEGRID}

    ${OUTPUT}=   GUI:Access::Generic Console Command Output     ls
    Should Contain  ${OUTPUT}   access/   system/   settings/

    ${OUTPUT}=   GUI:Access::Generic Console Command Output     pwd
    Should Contain  ${OUTPUT}   /

    ${OUTPUT}=   GUI:Access::Generic Console Command Output     TAB+TAB
    ${COMMANDS}=	Create List	apply_settings	cd	change_password	commit
	...	event_system_audit	event_system_clear	exit	factory_settings
	...	hostname	ls	pwd	quit	reboot	revert	save_settings	set
	...	shell	show	shutdown	software_upgrade	system_certificate
	...	system_config_check	whoami
    FOR	${COMMAND}	IN	@{COMMANDS}
		Should Contain	${OUTPUT}	${COMMAND}
	END
	Unselect Frame
	Press Keys	//body	cw
	Close All Browsers

SUITE:Direct access COMMANDS with valid device and invalid action
	Close All Browsers
	GUI::Basic::Open NodeGrid	${HOMEPAGE}/direct/${DEVICE}/${INVALID_ACTION}	ALIAS=first
	GUI::Basic::Direct Login	${USERNAME_ADMIN}	${PASSWORD_ADMIN}	${TRUE}	${DEVICE}
	Wait Until Keyword Succeeds   5 times  1 seconds  Switch Window   NEW
#	Handle Alert   timeout=10s
	Sleep       20s
	${STATUS}	Run Keyword And Return Status	Wait Until Page Contains Element	xpath=//*[@id='termwindow']	loglevel=NONE
	Should Not Be True	${STATUS}