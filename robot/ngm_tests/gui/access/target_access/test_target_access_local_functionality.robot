*** Settings ***
Documentation   Test target accessing local TTYD consoles from Table and Tree views
Metadata    Version 1.0
Metadata    Executed At ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	NON-CRITICAL	NEED-REVIEW

Suite Setup     SUITE:Setup
Suite Teardown  SUITE:Teardown
Test Setup      SUITE:Test Setup
Test Teardown   SUITE:Test Teardown

*** Variables ***
${USERNAME}               ${DEFAULT_USERNAME}
${PASSWORD}               ${DEFAULT_PASSWORD}
${HOSTNAME}               ${HOSTNAME_NODEGRID}
${DEVICE}                 test_device
${DEVICE_TYPE}            device_console
${DEVICE_IP}              127.0.0.1

*** Test Cases ***
Test Access Console From Table View
    SUITE:Accessing Console From Table View Should Succeed

Test Access Managed Device Console From Table View
    SUITE:Accessing Managed Device Console From Table View Should Succeed

Test Access Console From Tree View
    SUITE:Accessing Console From Tree View Should Succeed

Test Access Managed Device Console From Tree View
    SUITE:Accessing Managed Device Console From Tree View Should Succeed

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists    ${DEVICE}
    GUI::ManagedDevices::Add Device   ${DEVICE}   ${DEVICE_TYPE}  ${DEVICE_IP}
    ...   ${USERNAME}   no  ${PASSWORD}   enabled
    Close Browser

SUITE:Teardown
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists    ${DEVICE}
    Close Browser

SUITE:Test Setup
    GUI::Basic::Open And Login Nodegrid

SUITE:Test Teardown
    Close All Browsers

SUITE:Accessing Console From Table View Should Succeed
    GUI::Access::Table::Access Console
    Sleep  5

    SUITE:Executing Some Commands On Console Should Succeed   ${HOSTNAME}

SUITE:Accessing Console From Tree View Should Succeed
    GUI::Access::Tree::Access Console
    Sleep  5

    SUITE:Executing Some Commands On Console Should Succeed   ${HOSTNAME}

SUITE:Accessing Managed Device Console From Table View Should Succeed
    GUI:Access::Table::Access Device Console  ${DEVICE}
    Sleep  5

    SUITE:Executing Some Commands On Console Should Succeed   ${HOSTNAME}

SUITE:Accessing Managed Device Console From Tree View Should Succeed
    GUI:Access::Tree::Access Device Console  ${DEVICE}
    Sleep  5

    SUITE:Executing Some Commands On Console Should Succeed   ${HOSTNAME}

SUITE:Executing Some Commands On Console Should Succeed
    [Arguments]   ${HOSTNAME}

    ${OUTPUT}=   GUI:Access::Generic Console Command Output   whoami
    Sleep  5
    Should Contain    ${OUTPUT}    ${USERNAME}

    ${OUTPUT}=   GUI:Access::Generic Console Command Output   hostname
    Sleep  5
    Should Contain    ${OUTPUT}    ${HOSTNAME}

    ${OUTPUT}=   GUI:Access::Generic Console Command Output	ls
    Sleep  5
    Should Contain  ${OUTPUT}   access/   system/   settings/

    ${OUTPUT}=   GUI:Access::Generic Console Command Output	pwd
    Sleep  5
    Should Contain  ${OUTPUT}   /

GUI:Access::Generic Console Command Output
    [Arguments]    ${COMMAND}    ${CONTAINS_HELP}=no
    [Documentation]     Input the given command into the ttyd terminal and retrieve the output after prompt lines
    ...    == REQUIREMENTS ==
    ...    The console type needs to be one of those:
    ...    -    CLI -> ends with "]#"
    ...    -    Shell -> ends with "$"
    ...    -    Root -> ends with "#"
    ...    == ARGUMENTS ==
    ...    -    COMMAND = Command to be executed
    ...    -    CONTAINS_HELP = [yes/no] Console contains "[Enter '^Ec?' for help]" message
    ${CHECK_TAB}=   Run Keyword And Return Status   Should Not Contain  ${COMMAND}  TAB
    Run Keyword If  ${CHECK_TAB}    Press Keys  //body    ${COMMAND}  RETURN
    ...  ELSE    Press Keys     //body    ${COMMAND}
    Sleep   1

    Wait Until Keyword Succeeds    1m    1s     Execute JavaScript  term.selectAll();
    ${CONSOLE_CONTENT}=    Wait Until Keyword Succeeds    1m    1s     Execute Javascript   return term.getSelection().trim();
    Should Not Contain  ${CONSOLE_CONTENT}  [error.connection.failure] Could not establish a connection to device
    Run Keyword If   "${CONTAINS_HELP}" == "yes"   Should Contain   ${CONSOLE_CONTENT}   [Enter '^Ec?' for help]
    ${OUTPUT}=  Output From Last Command    ${CONSOLE_CONTENT}
    [Return]    ${OUTPUT}

GUI::Access::Table::Access Console
    [Arguments]    ${HOSTNAME}=${HOSTNAME_NODEGRID}
    [Documentation]     Open the console for the host from Access :: Table and wait until it is ready to be used
    ...    == ARGUMENTS ==
    ...    -    HOSTNAME = Hostname of the console to be accessed
    GUI:Access::Open Table tab
    Wait Until Element Is Visible   //div[@class="peer_header"]/div[text()="${HOSTNAME}"]/../div/a[text()="Console"]
    Click Element   //div[@class="peer_header"]/div[text()="${HOSTNAME}"]/../div/a[text()="Console"]
#    GUI::Basic::Wait Until Window Handle Exists    NEW
#    GUI::Basic::Handle Browser Certificate
    Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists    ${HOSTNAME} - Console
    ...	ELSE	GUI:Basic::Wait Until Window Exists    ${HOSTNAME}

    Wait Until Page Contains Element    xpath=//*[@id='termwindow']
    Select Frame    xpath=//*[@id='termwindow']
    Wait Until Keyword Succeeds    1m    1s     Execute JavaScript  term.selectAll();

    Wait Until Page Contains Element    terminal-container
    Current Frame Should Contain    xterm-viewport
    Current Frame Should Contain    xterm-screen

    Wait Until Page Contains Element    css=.xterm-helpers
    Wait Until Page Contains Element    css=.xterm-helper-textarea
    Wait Until Page Contains Element    css=.xterm-text-layer
    Wait Until Page Contains Element    css=.xterm-selection-layer
    Wait Until Page Contains Element    css=.xterm-link-layer
    Wait Until Page Contains Element    css=.xterm-cursor-layer
    Press Keys     //body    RETURN
    [Return]    ${HOSTNAME}

GUI:Access::Table::Access Device Console
    [Arguments]   ${DEVICE_NAME}   ${HOSTNAME}=${HOSTNAME_NODEGRID}
    [Documentation]   Goes to the Access::Table page and opens the given device console and keeps
    ...               the console window opened
    ...    == ARGUMENTS ==
    ...    -   DEVICE_NAME = Name of the device to access the console
    ...    -   HOSTNAME = Hostname (Works for clustering) that manages the device (default is 'nodegrid')
    GUI:Access::Open Table tab

    ${EXPANDADED}=   Get Element Attribute   //div[@id[contains(., "|${HOSTNAME}")]]  class
    Run Keyword If  "${EXPANDADED}" == "peer"    Run Keywords
    ...   Click Element    //div[@id[contains(., "|${HOSTNAME}")]]//img[@id="expandstate"]   AND
    ...   Wait Until Page Contains Element    //div[@id[contains(., "|${HOSTNAME}")]][@class="peer expanded"]

    Wait Until Element Is Visible   //div[@id[contains(.,"|${HOSTNAME}")]]//tr[@id[contains(.,"|${DEVICE_NAME}")]]//a[text()[contains(.,"Console")]]
    Click Element    //div[@id[contains(.,"|${HOSTNAME}")]]//tr[@id[contains(.,"|${DEVICE_NAME}")]]//a[text()[contains(.,"Console")]]
#    GUI::Basic::Wait Until Window Handle Exists    NEW
#    GUI::Basic::Handle Browser Certificate
    Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists    ${DEVICE_NAME} - Console
    ...	ELSE	GUI:Basic::Wait Until Window Exists    ${DEVICE_NAME}

    ${FOUND_ALERT}=    Run Keyword And Return Status    Alert Should Be Present
    Run Keyword If    ${FOUND_ALERT}  Switch Window    title=ZPE Systems®, Inc.
    Run Keyword If    ${FOUND_ALERT}    Fail    No alert should be visible after ${DEVICE_NAME} window launch
#    TTYD terminal elements
    Wait Until Page Contains Element    xpath=//*[@id='termwindow']
    Select Frame    xpath=//*[@id='termwindow']
    Current Frame Should Contain    terminal-container
    Current Frame Should Contain    xterm-viewport
    Current Frame Should Contain    xterm-screen
#    TTYD xterm.js elements
    Wait Until Page Contains Element    css=.xterm-helpers
    Wait Until Page Contains Element    css=.xterm-helper-textarea
    Wait Until Page Contains Element    css=.xterm-text-layer
    Wait Until Page Contains Element    css=.xterm-selection-layer
    Wait Until Page Contains Element    css=.xterm-link-layer
    Wait Until Page Contains Element    css=.xterm-cursor-layer
    Press Keys  //body    RETURN

GUI::Access::Tree::Access Console
    [Arguments]    ${HOSTNAME}=${HOSTNAME_NODEGRID}   ${DOMAIN_NAME}=${EMPTY}
    [Documentation]     Open the console for the host from Access :: Tree and wait until it is ready to be used
    ...    == ARGUMENTS ==
    ...    -    HOSTNAME = Hostname of the console to be accessed
    ...    -    DOMAIN_NAME = This variable is only set if the console is being accessed via cluster (i.e., it's a peer)
    GUI:Access::Open Tree Tab
    Wait Until Element Is Visible    //ol[@id="tree_content"]//a[text()="${HOSTNAME}"]/../../span/a[text()="Console"]
    Click Element   //ol[@id="tree_content"]//a[text()="${HOSTNAME}"]/../../span/a[text()="Console"]

    ${WINDOW_TITLE}=   Set Variable   ${HOSTNAME}
    ${WINDOW_TITLE}=   Set Variable If   "${DOMAIN_NAME}" == "${EMPTY}"
    ...   ${HOSTNAME}    ${HOSTNAME}.${DOMAIN_NAME}
#    GUI::Basic::Wait Until Window Handle Exists    NEW
#    GUI::Basic::Handle Browser Certificate
    Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists    ${WINDOW_TITLE} - Console
    ...	ELSE	GUI:Basic::Wait Until Window Exists    ${WINDOW_TITLE}

    Wait Until Page Contains Element    xpath=//*[@id='termwindow']
    Select Frame    xpath=//*[@id='termwindow']
    Wait Until Keyword Succeeds    1m    1s     Execute JavaScript  term.selectAll();

    Wait Until Page Contains Element    terminal-container
    Current Frame Should Contain    xterm-viewport
    Current Frame Should Contain    xterm-screen

    Wait Until Page Contains Element    css=.xterm-helpers
    Wait Until Page Contains Element    css=.xterm-helper-textarea
    Wait Until Page Contains Element    css=.xterm-text-layer
    Wait Until Page Contains Element    css=.xterm-selection-layer
    Wait Until Page Contains Element    css=.xterm-link-layer
    Wait Until Page Contains Element    css=.xterm-cursor-layer
    Press Keys     //body    RETURN
    [Return]    ${WINDOW_TITLE}

GUI:Access::Tree::Access Device Console
    [Arguments]   ${DEVICE_NAME}   ${HOSTNAME}=${HOSTNAME_NODEGRID}
    [Documentation]   Goes to the Access::Table page and opens the given device console and keeps
    ...               the console window opened
    ...    == ARGUMENTS ==
    ...    -   DEVICE_NAME = Name of the device to access the console
    ...    -   HOSTNAME = Hostname (Works for clustering) that manages the device (default is 'nodegrid')
    GUI:Access::Open Tree tab

    ${EXPANDADED}=   Get Element Attribute   //div[@rowid[contains(., "|${HOSTNAME}")]]/label/img[@id="view"]  state
    Run Keyword If  "${EXPANDADED}" == "off"    Run Keywords
    ...   Click Element    //div[@rowid[contains(., "|${HOSTNAME}")]]/label/img[@id="view"]   AND
    ...   Wait Until Page Contains Element    //div[@rowid[contains(., "|${HOSTNAME}")]]/label/img[@id="view"][@state="on"]

    Wait Until Element Is Visible   //div[@parent[contains(.,"|${HOSTNAME}")]]//span[@id[contains(.,"|${DEVICE_NAME}")]]//a[text()="Console"]
    Click Element    //div[@parent[contains(.,"|${HOSTNAME}")]]//span[@id[contains(.,"|${DEVICE_NAME}")]]//a[text()="Console"]
#    GUI::Basic::Wait Until Window Handle Exists    NEW
#    GUI::Basic::Handle Browser Certificate
    Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists    ${DEVICE_NAME} - Console
    ...	ELSE	GUI:Basic::Wait Until Window Exists    ${DEVICE_NAME}

    ${FOUND_ALERT}=    Run Keyword And Return Status    Alert Should Be Present
    Run Keyword If    ${FOUND_ALERT}  Switch Window    title=ZPE Systems®, Inc.
    Run Keyword If    ${FOUND_ALERT}    Fail    No alert should be visible after ${DEVICE_NAME} window launch
#    TTYD terminal elements
    Wait Until Page Contains Element    xpath=//*[@id='termwindow']
    Select Frame    xpath=//*[@id='termwindow']
    Current Frame Should Contain    terminal-container
    Current Frame Should Contain    xterm-viewport
    Current Frame Should Contain    xterm-screen

    Wait Until Page Contains Element    css=.xterm-helpers
    Wait Until Page Contains Element    css=.xterm-helper-textarea
    Wait Until Page Contains Element    css=.xterm-text-layer
    Wait Until Page Contains Element    css=.xterm-selection-layer
    Wait Until Page Contains Element    css=.xterm-link-layer
    Wait Until Page Contains Element    css=.xterm-cursor-layer
    Press Keys  //body    RETURN