*** Settings ***
Documentation	Testing --> "SSH" button to open user's ssh client software (forward)
Metadata        Version	1.0
Metadata        Executed At	${HOST}
Metadata        Executed with	${BROWSER}
Resource        ../init.robot
Force Tags   PART-1      GUI  ${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	                 ${DEFAULT_USERNAME}
${PASSWORD}	                 ${DEFAULT_PASSWORD}
${DEVICE_TYPE}                   device_console
${DEVICE_NAME}                   target_device
${HOST_IP}                       127.0.0.1

*** Test Cases ***
Test Case To Open User's SSH Client Software
	[Tags]	NON-CRITICAL	BUG-NG-12936
      GUI::Basic::Add
      GUI::Basic::Spinner Should Be Invisible
      Input Text    id=spm_name     ${DEVICE_NAME}
      Select From List By Label    id=type   ${DEVICE_TYPE}
      Input Text    id=phys_addr      ${HOST_IP}
      Input Text    id=username       ${USERNAME}
      Input Text    id=passwordfirst   ${PASSWORD}
      Input Text    id=passwordconf    ${PASSWORD}
      GUI::Basic::Save
      GUI::Basic::Spinner Should Be Invisible
      Click Element       //*[@id="${DEVICE_NAME}"]/td[2]/a
      GUI::Basic::Spinner Should Be Invisible
      Click Element       //*[@id="spmcommands_nav"]
      GUI::Basic::Add
      GUI::Basic::Spinner Should Be Invisible
      Select From List By Label     id=command    SSH
      Select Checkbox    //*[@id="launch_local_application"]
      GUI::Basic::Save
      GUI::Basic::Spinner Should Be Invisible
      GUI::Access::Open
      GUI::Basic::Spinner Should Be Invisible
      Run Keyword If 	'${NGVERSION}' <= '5.6'	Click Element	//*[@id="|${DEVICE_NAME}"]/td[1]/div/a/span
      ...	ELSE	Click Element	//*[@id="|${DEVICE_NAME}|"]/td[1]/div/a/span
      sleep  5s
      Click Element       css=.dropdown_button_second
      Click Element       //a[contains(text(),'Launch Local Application')]
      sleep  10s
      Switch Browser   1
      GUI::Basic::Open And Login Nodegrid
      GUI::Basic::Spinner Should Be Invisible
      GUI::ManagedDevices::Open Devices tab
      GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
      GUI::Basic::Open And Login Nodegrid
      GUI::Basic::Spinner Should Be Invisible
      GUI::ManagedDevices::Open Devices tab
      GUI::Basic::Spinner Should Be Invisible
      GUI::ManagedDevices::Delete Device If Exists  ${DEVICE_NAME}

SUITE:Teardown
      GUI::ManagedDevices::Delete Device If Exists  ${DEVICE_NAME}
      GUI::Basic::Spinner Should Be Invisible
      GUI::Basic::Logout And Close Nodegrid
