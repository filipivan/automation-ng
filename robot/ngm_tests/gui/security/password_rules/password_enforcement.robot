*** Settings ***
Documentation	Test common behaviour in security ->password rules page
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE	EXCLUDEIN3_2
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variable ***
${STRING}	str
${Min Size}	10
${Large Value}	100
${ONE}	1
${Zero}	0

*** Test Cases ***
Test Initial Page
	#ASSUMES THAT CHECK PASSOWRD COMPLEXITY IS INITIALY UNSELECTED
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Disabled	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Elements Should Be Visible	//*[@id='min_days']
	GUI::Basic::Elements Should Be Visible	//*[@id='max_days']
	GUI::Basic::Elements Should Be Visible	//*[@id='warn_days']

Test Set Min Values
    GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	jquery=#enabled
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Enabled	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Elements Should Be Visible	//*[@id='min_digits']
	GUI::Basic::Elements Should Be Visible	//*[@id='min_upper_char']
	GUI::Basic::Elements Should Be Visible	//*[@id='min_special_char']
	GUI::Basic::Elements Should Be Visible	//*[@id='min_size']
	GUI::Basic::Elements Should Be Visible	//*[@id='remember']

	SUITE:Set Min Values

	Unselect Checkbox	jquery=#enabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Elements Should Not Be Visible	//*[@id='min_digits']
	GUI::Basic::Elements Should Not Be Visible	//*[@id='min_upper_char']
	GUI::Basic::Elements Should Not Be Visible	//*[@id='min_special_char']
	GUI::Basic::Elements Should Not Be Visible	//*[@id='min_size']
	GUI::Basic::Elements Should Not Be Visible	//*[@id='remember']
	Element Should Be Disabled	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

Test Minimum Number of Digits Using different values
	GUI::Basic::Spinner Should Be Invisible
	${E}=	Set Variable	The value must be between 0 and ${Min Size}.
	SUITE:Auto Check Pass Errors	min_digits	${E}	${STRING}	${Large Value}	${ONE}

Test Minimum Number of Upper Case Characters Using different values
	GUI::Basic::Spinner Should Be Invisible
	${E}=	Set Variable	The value must be between 0 and ${Min Size}.
	SUITE:Auto Check Pass Errors	min_upper_char	${E}	${STRING}	${Large Value}	${ONE}

Test Minimum Number of Special Characters Using different values
	GUI::Basic::Spinner Should Be Invisible
	${E}=	Set Variable	The value must be between 0 and ${Min Size}.
	SUITE:Auto Check Pass Errors	min_special_char	${E}	${STRING}	${Large Value}	${ONE}

Test Minimum Number of Passwords to Store in History Using different values
	GUI::Basic::Spinner Should Be Invisible
	${E}=	Set Variable	The value must be between 1 and 20.
	SUITE:Auto Check Pass Errors	remember	${E}	${STRING}	${Large Value}	${Zero}	${ONE}

Test Minimum Size Using different values
	GUI::Basic::Spinner Should Be Invisible
	${E}=	Set Variable	The value must be between 6 and 30.
	SUITE:Auto Check Pass Errors	min_size	${E}	-1	${Large Value}	11

Test Unselect Check Password
	Unselect Checkbox	jquery=#enabled
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Password Rules::Open Tab
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

SUITE:Set Min Values
    GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	jquery=#enabled
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text	//*[@id='min_digits']	${Zero}
	GUI::Basic::Input Text	//*[@id='min_upper_char']	${Zero}
	GUI::Basic::Input Text	//*[@id='min_special_char']	${Zero}
	GUI::Basic::Input Text	//*[@id='min_size']	${Min Size}
	GUI::Basic::Input Text	//*[@id='remember']	2
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Check Pass Errors
	[Arguments]	${ID}	${INP}	${ERR}	${EXP}
	Input Text	jquery=#${ID}	${INP}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	${EXP}	Wait Until Element Contains	jquery=#errormsg	${ERR}
	...	ELSE	GUI::Basic::Page Should Not Contain Error

SUITE:Auto Check Pass Errors
	[Arguments]	${ID}	${ERR}	@{TESTS}
	Select Checkbox	jquery=#enabled
	${LEN}=	Get Length	${TESTS}
	${LEN}=	Evaluate	${LEN}-1
	FOR	${I}	IN RANGE	0	${LEN}
		SUITE:Check Pass Errors	${ID}	${TESTS}[${I}]	${ERR}	${TRUE}
	END
	SUITE:Check Pass Errors	${ID}	${TESTS}[${LEN}]	${ERR}	${FALSE}
