*** Settings ***
Documentation	Test password validation in security ->password rules page
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variable ***
${New User}	User123
${Pass}	password
${Pass Too Short}	pass
${Pass with digit}	password1
${Pass with special}	password!
${Pass with Upper}	Password
${Min Digits}	1
${Defualt Min}	0

*** Test Cases ***
#Test Create User With Min Digits
#
#	SUITE:Set Pass Rules	${Min Digits}	${Defualt Min}	${Defualt Min}
#	SUITE:Create New User	${New User}	${Pass}
#	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
#	Wait Until Element Contains	jquery=#errormsg	Password minimum digits requirement: ${Min Digits}
#	SUITE:Update Password	${Pass with digit}
#	GUI::Basic::Page Should Not Contain Error
#	GUI::Basic::Delete Rows In Table Containing Value	user_namesTable	${New User}

Test Only Length
	#000
	SUITE:Set Pass Rules	0	0	0
	SUITE:Set Password Test	passw	${TRUE}
	SUITE:Set Password Test	passwo	${FALSE}
	GUI::Basic::Delete Rows In Table Containing Value	user_namesTable	${New User}
Test Upper
	#010
	SUITE:Create Set and Del	0	1	0	p4sswo	Passwo
Test Special
	#001
	SUITE:Create Set and Del	0	0	1	Passwo	pa$swo
Test Upper and Special
	#011
	SUITE:Create Set and Del	0	1	1	p4sswo	Pa$swo
Test Digit
	#100
	SUITE:Create Set and Del	1	0	0	Passwo	p4sswo
Test Digit and Upper
	#110
	SUITE:Create Set and Del	1	1	0	pa$swo	P4sswo
Test Digit and Special
	#101
	SUITE:Create Set and Del	1	0	1	Passwo	p4$swo
Test Digit and Upper and Special
	#111
	SUITE:Create Set and Del	1	1	1	passwo	P4$swo


*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Password Rules::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#enabled
	${IS_SETTING_ENABLED}=	Run Keyword And Return Status	Checkbox Should Be Selected	//*[@id="enabled"]
	Run Keyword If	${IS_SETTING_ENABLED}==${FALSE}	Select Checkbox	jquery=#enabled
	Wait until element is visible	jquery=#min_size
	Input Text	jquery=#min_size	6
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Security::Password Rules::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	Unselect Checkbox	jquery=#enabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout
	Close all Browsers

SUITE:Set Pass Rules
	[Arguments]	${Digits}=0	${Upper}=0	${Special}=0	#${Min Size}=6
	GUI::Basic::Security::Password Rules::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	#Select Checkbox	jquery=#enabled
	Wait until element is visible	jquery=#min_size
	Input Text	//*[@id='min_digits']	${Digits}
	Input Text	//*[@id='min_upper_char']	${Upper}
	Input Text	//*[@id='min_special_char']	${Special}
	#GUI::Basic::Input Text	//*[@id='min_size']	${Min Size}
	${IS_SAVE_ENABLED}=	Run Keyword And Return Status	Element Should Be Enabled	//*[@id='saveButton']
	Run Keyword If	${IS_SAVE_ENABLED}	GUI::Basic::Save

#SUITE:Create New User
#	[Arguments]	${Name}	${User Pass}
#	GUI::Security::Open Local Accounts tab
#	GUI::BASIC::ADD
#	GUI::Basic::Input Text	//*[@id='uName']	${Name}
#	GUI::Basic::Input Text	//*[@id='uPasswd']	${User Pass}
#	GUI::Basic::Input Text	//*[@id='cPasswd']	${User Pass}
#	GUI::Basic::Save
#
#SUITE:Update Password
#	[Arguments]	${New Password}
#	GUI::Basic::Input Text	//*[@id='uPasswd']	${New Password}
#	GUI::Basic::Input Text	//*[@id='cPasswd']	${New Password}
#	GUI::Basic::Save

SUITE:Make New Account
	GUI::Basic::Security::Local Accounts::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=#addButton
	GUI::Basic::Spinner Should Be Invisible
	Input Text	jquery=#uName	${New User}

SUITE:Set Password Test
	[Arguments]	${PASS}	${EXPECTS}
	[Documentation]	pass is expecting the password to test
	...	expects wants a t/f on whether there should be an error
	${WHERE_AM_I}=	run keyword and return status	Element Should Not Be Visible	jquery=#cancelButton
	run keyword if	${WHERE_AM_I}	SUITE:Make New Account
	Input Text	jquery=#uPasswd	${PASS}
	Input Text	jquery=#cPasswd	${PASS}
	Click Element	jquery=#saveButton
	Wait Until Element Is Not Visible	xpath=//*[@id='loading']	timeout=20s
	Run Keyword if	${EXPECTS}	Page Should Contain Element	xpath=//p[@class="bg-danger text-danger"]
	Page Should Not Contain Element	Page Should Not Contain Element	xpath=//p[@class="bg-danger text-danger"]

SUITE:Create Set and Del
	[Arguments]	${DIGITS}	${UPPER}	${SPECIAL}	${P_INV}	${P_YES}
	[Documentation]	Digits is the min number of digits in pass
	...	Upper is the min number of upper case characters
	...	Special is the min number of special characters
	...	Pass_INV is the first test, MUST BE INVALID
	...	Pass_YES is the second test, MUST BE VALID
	SUITE:Set Pass Rules	${DIGITS}	${UPPER}	${SPECIAL}
	SUITE:Set Password Test	passw	${TRUE}
	SUITE:Set Password Test	${P_INV}	${TRUE}
	SUITE:Set Password Test	${P_YES}	${FALSE}
	GUI::Basic::Delete Rows In Table Containing Value	user_namesTable	${New User}
