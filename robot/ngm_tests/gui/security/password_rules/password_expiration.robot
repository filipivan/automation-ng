*** Settings ***
Documentation	Test password expiration field in security ->password rules page
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variable ***
${STRING}	str
${Zero}	0

*** Test Cases ***
Test Min Days Using String
	SUITE:Input Min, Max, Warning Days And Save	${STRING}	${EMPTY}	${EMPTY}
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	The value must be between 0 and 99999.

Test Max Days Using String
	SUITE:Input Min, Max, Warning Days And Save	${EMPTY}	${STRING}	${EMPTY}
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	The value must be between 0 and 99999.

Test Warn Days Using String
	SUITE:Input Min, Max, Warning Days And Save	${EMPTY}	${EMPTY}	${STRING}
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	The warning days must be greater than 0 and less than the difference between the maximum and the minimum days, or empty.

Test Min Days Greater Than Max Days
	SUITE:Input Min, Max, Warning Days And Save	10	5	${EMPTY}
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Page Should Contain	The minimum days must be smaller than the maximum days, or empty.
	Page Should Contain	The maximum days must be greater than the minimum days, or empty.

Test Warning Days Using String Value
	SUITE:Input Min, Max, Warning Days And Save	${EMPTY}	${EMPTY}	${STRING}
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Page Should Contain	The warning days must be greater than 0 and less than the difference between the maximum and the minimum days, or empty.

Test Warning Days Using Zero
	SUITE:Input Min, Max, Warning Days And Save	${EMPTY}	${EMPTY}	${Zero}
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Page Should Contain	The warning days must be greater than 0 and less than the difference between the maximum and the minimum days, or empty.

Test Warning Days Using Large Value
	SUITE:Input Min, Max, Warning Days And Save	5	10	15
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Page Should Contain	The warning days must be greater than 0 and less than the difference between the maximum and the minimum days, or empty.

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Password Rules::Open Tab
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

SUITE:Input Min, Max, Warning Days And Save
	[Arguments]	${Min}	${Max}	${Warn}
	GUI::Basic::Input Text	//*[@id='min_days']	${Min}
	GUI::Basic::Input Text	//*[@id='max_days']	${Max}
	GUI::Basic::Input Text	//*[@id='warn_days']	${Warn}
	GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible