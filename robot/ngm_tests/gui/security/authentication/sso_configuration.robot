*** Settings ***
Documentation	Testing Security:Authentication:SSO Configuration
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NAME}	duo
${STATUS}	enabled
${ENTITY_ID}	${SSO_ENTITY_ID}
${DUO_ICON}	css=.iconItem:nth-child(1) > img

${SSO_URL}	${SSOSERVER_URL}${SSOSERVER_SERVICE_PATH}
${SSO_USER}	${SSOSERVER_USER}
${SSO_PASSWORD}	${SSOSERVER_PASSWORD}
${SSO_ISSUER}	${SSOSERVER_URL}${SSOSERVER_ISSUER_PATH}
${SSO_LOGOUT_URL}	${SSOSERVER_LOGOUT_URL}

${FTP_URL}	${FTPSERVER2_URL}${SSOSERVER_CRT_PATH}
${FTP_USERNAME}	${FTPSERVER2_USER}
${FTP_PASSWORD}	${FTPSERVER2_PASSWORD}

*** Test Cases ***
Add valid Identity Provider configuration
	GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5
	Click Element	ssoTable
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Wait Until Element Is Accessible	ssoName
	Input Text	ssoName	${NAME}
	Select From List by Value	ssoStatus	${STATUS}
	Input Text	ssoEntityid	${ENTITY_ID}
	Input Text	ssoUrl	${SSO_URL}
	Input Text	ssoIdp	${SSO_ISSUER}
	Click Button	xpath=//input[@value='Select Icon']
	Click Element	${DUO_ICON}
	Select Radio Button	systemcertselection	systemcert-remote
	Input Text	url	${FTP_URL}
	Input Text	userName	${FTP_USERNAME}
	Input Text	password	${FTP_PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	sloEnabled
	Input Text	sloUrl	${SSO_LOGOUT_URL}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	ssoTable
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${NAME}

Edit Identity Provider configuration
	Page Should Contain Element	jquery=table\#multiSSOTable
	Click Element	xpath=//a[contains(text(),'duo')]
	GUI::Basic::Spinner Should Be Invisible
	set focus to element		jquery=#sloUrl
	Input Text	jquery=#sloUrl	${SSO_URL}
	Click Element	jquery=#saveButton
	gui::basic::spinner should be invisible
	Click Element		xpath=//a[contains(text(),'duo')]
	GUI::Basic::Spinner Should Be Invisible
	${SSO_LOGOUT_URL}=	Get Value	jquery=#sloUrl
	Should Be Equal	${SSO_LOGOUT_URL}	${SSO_URL}
	Click Element	jquery=#cancelButton
	GUI::Basic::Spinner Should Be Invisible

Delete Identity Provider
	select checkbox		xpath=//input[@type='checkbox']
	Click Element	jquery=#delButton
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Authentication::Open Sso Tab
	GUI::Basic::Spinner Should Be Invisible
	${NAMES}=	Create List	${NAME}
	GUI::Basic::Delete Rows In Table If Exists	jquery=table\#multiSSOTable	${NAMES}	HAS_ALERT=False

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
