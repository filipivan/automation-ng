*** Settings ***
Documentation	Test single logout functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NAME}	duo
${STATUS}	enabled
${ENTITY_ID}	${SSO_ENTITY_ID}
${DUO_ICON}	css=.iconItem:nth-child(1) > img
${SELECT_CERTIFICATE}	systemcert-localcomputer

${SSO_URL}	${SSOSERVER_URL}${SSOSERVER_SERVICE_PATH}
${SSO_USER}	${SSOSERVER_USER}
${SSO_PASSWORD}	${SSOSERVER_PASSWORD}
${SSO_ISSUER}	${SSOSERVER_URL}${SSOSERVER_ISSUER_PATH}
${SSO_LOGOUT_URL}	${SSOSERVER_LOGOUT_URL}
${SSO_FILEPATH}	${SSOSERVER_FILEPATH}

*** Test Cases ***
Test Single Logout Functionality
	GUI::Basic::Open NodeGrid
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	sso_button
	Click Button	sso_button
	Sleep	5
	Page Should Contain	Please enter your  credentials to access ${ENTITY_ID}
	Input Text	username	${SSO_USER}
	Input Text	password	${SSO_PASSWORD}
	Click Button	login-button
	Sleep	10
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	main_menu
	SUITE: Single Logout
	GUI::Basic::Open NodeGrid
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	sso_button
	Click Button	sso_button

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Open Authentication Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	ssoTable
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5
	GUI::Basic::Wait Until Element Is Accessible	ssoName
	Input Text	ssoName	${NAME}
	Select From List by Value	ssoStatus	${STATUS}
	Input Text	ssoEntityid	${ENTITY_ID}
	Input Text	ssoUrl	${SSO_URL}
	Input Text	ssoIdp	${SSO_ISSUER}
	Click Button	xpath=//input[@value='Select Icon']
	Click Element	${DUO_ICON}
	Radio Button Should Be Set To	systemcertselection	${SELECT_CERTIFICATE}
	Choose File	clientFilename	${SSO_FILEPATH}
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	sloEnabled
	Input Text	sloUrl	${SSO_LOGOUT_URL}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

SUITE: Single Logout
	Wait Until Element Is Visible	lo
	GUI::Basic::Click Element	//*[@id='lo']
	Page Should Contain Element	ui-id-1
	Page Should Contain	Would you like to logout of all your SSO sessions?
	Click Element	xpath=//span[contains(.,'Yes')]

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	ssoTable
	GUI::Basic::Delete All Rows In Table If Exists	multiSSOTable
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid