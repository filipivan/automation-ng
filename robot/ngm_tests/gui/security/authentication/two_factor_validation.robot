*** Settings ***
Documentation	Testing Security:Authentication:Two Factor
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE	EXCLUDEIN3_2
Default Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${2FACTOR_ENTRY}	name

*** Test Cases ***
Test Fields
	GUI::Basic::Click Element	//*[@id='addButton']
	GUI::Basic::Spinner Should Be Invisible
	Input Text	jquery=#secondFactorName	${2FACTOR_ENTRY}
	Run Keyword If  '${NGVERSION}'>='5.8' 		Wait Until Element Is Visible 	id=secondFactorMethod
	Run Keyword If  '${NGVERSION}'>='5.8'		Select From List By Label 		id=secondFactorMethod		RSA
	Input Text	jquery=#client-id	,.;'123notit[]
	GUI::Basic::Click Element	//*[@id='saveButton']
	GUI::Basic::Spinner Should Be Invisible
	${HAS_ERROR}=	Execute Javascript	var er=document.getElementById('client-id').parentNode.nextSibling;if(er==null){return false;}return true;
	run keyword and continue on failure  Should Be True	${HAS_ERROR}
	wait until keyword succeeds	10	2	gui::auditing::auto input tests	read-timeout	str	29	241	239
	wait until keyword succeeds	10	2	gui::auditing::auto input tests	connect-timeout	str	9	121	119
	wait until keyword succeeds	10	2	gui::auditing::auto input tests	max-retries	str	0	6	4

Add 2Factor and Test Certificate
	Input Text	jquery=#rest-url	a
	Input Text	jquery=#client-key	a
	Input Text	jquery=#client-id	a
	Input Text	jquery=#read-timeout	100
	Input Text	jquery=#connect-timeout	99
	Input Text	jquery=#max-retries	3
	GUI::Basic::Click Element	//*[@id='saveButton']
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Link	//*[@id='${2FACTOR_ENTRY}']
	GUI::Basic::Click Element	//*[@id='certificate']
	Select Radio Button	systemcertselection	systemcert-localcomputer
	GUI::Basic::Spinner Should Be Invisible
	Select Radio Button	systemcertselection	systemcert-remote
	gui::basic::spinner should be invisible

Test Errors
	SUITE:Check Error Banner	url	!@#$%
	SUITE:Check Error Banner	url	http://aa.aa
	SUITE:Check Error Banner	url	ftp://aa.aa
	SUITE:Check Error Banner	userName	naming

Delete Rule
	GUI::Basic::Click Element	//*[@id='cancelButton']
	GUI::Basic::Click Element	//*[@id='cancelButton']
	GUI::Basic::Delete Rows In Table If Exists	multiSecondFactorTable	${2FACTOR_LIST}	${FALSE}
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	${2FACTOR_LIST}=	Create List	${2FACTOR_ENTRY}
	Set Suite Variable	${2FACTOR_LIST}	${2FACTOR_LIST}
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Security::Authentication::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	Click Element	secondFactorTable
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table If Exists	multiSecondFactorTable	${2FACTOR_LIST}	${FALSE}

SUITE:Teardown
	Run Keyword If Any Tests Failed	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout
	Close all Browsers

SUITE:Check Error Banner
	[Arguments]	${LOCATOR}	${INPUT}
	Input Text	jquery=#${LOCATOR}	${INPUT}
	Click Element	//*[@id='saveButton']
	wait until keyword succeeds	10	2	Handle Alert
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	xpath=//*[@id='globalerr']
	Element Should Be Visible	xpath=//*[@id='globalerr']
