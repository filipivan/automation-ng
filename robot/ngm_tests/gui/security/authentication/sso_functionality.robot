*** Settings ***
Documentation	Testing Security:Authentication:SSO Functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NAME}	duo
${STATUS}	enabled
${ENTITY_ID}	${SSO_ENTITY_ID}
${DUO_ICON}	css=.iconItem:nth-child(1) > img
${SSO_DAG}	${SSOSERVER_URL}${SSOSERVER_DAG_PATH}
${SSO_URL}	${SSOSERVER_URL}${SSOSERVER_SERVICE_PATH}
${SSO_USER}	${SSOSERVER_USER}
${SSO_PASSWORD}	${SSOSERVER_PASSWORD}
${SSO_ISSUER}	${SSOSERVER_URL}${SSOSERVER_ISSUER_PATH}
${SSO_LOGOUT_URL}	${SSOSERVER_LOGOUT_URL}
${FTP_URL}	${FTPSERVER2_URL}${SSOSERVER_CRT_PATH}
${FTP_USERNAME}	${FTPSERVER2_USER}
${FTP_PASSWORD}	${FTPSERVER2_PASSWORD}


*** Test Cases ***
Add valid Identity Provider configuration
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5
	GUI::Basic::Security::Authentication::Open Tab
	Sleep	5
	Click Element	ssoTable
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Wait Until Element Is Accessible	ssoName
	Input Text	ssoName	${NAME}
	Select From List by Value	ssoStatus	${STATUS}
	Input Text	ssoEntityid	${ENTITY_ID}
	Input Text	ssoUrl	${SSO_URL}
	Input Text	ssoIdp	${SSO_ISSUER}
	Click Button	xpath=//input[@value='Select Icon']
	Click Element	${DUO_ICON}
	Select Radio Button	systemcertselection	systemcert-remote
	Input Text	url	${FTP_URL}
	Input Text	userName	${FTP_USERNAME}
	Input Text	password	${FTP_PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	sloEnabled
	Input Text	sloUrl	${SSO_LOGOUT_URL}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep	10
	GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	ssoTable
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${NAME}

Check that Identity Provider button shows on login page
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open NodeGrid
	Wait Until Element Is Visible	password
	${IDP}= 	Get Value 	jquery=#sso_button
	Should Be Equal	${IDP}	Login with ${NAME}

Check that Identity Provider asks for username/password
	[Tags]	NON-CRITICAL	NEED-REVIEW
	Click Element	jquery=#sso_button
	Sleep   15s
	#Wait Until Element Is Visible	username
#	Location Should Contain		${SSOSERVER_URL}

Enter credentials and login to Nodegrid
	[Tags]	NON-CRITICAL	NEED-REVIEW
	Page Should Contain Element	sso_button
	Click Button	sso_button
	Sleep	5
	Page Should Contain	Please enter your	credentials to access ${ENTITY_ID}
	Input Text	username	${SSO_USER}
	Input Text	password	${SSO_PASSWORD}
	Click Button	login-button
	Wait until Keyword Succeeds	1m	2s	Wait Until Element Is Visible	id=main_menu
#	Location Should Contain	saml

Refresh page, login by just clicking button
	[Tags]	NON-CRITICAL	NEED-REVIEW
	Go To	${HOMEPAGE}
	Wait Until Element Is Visible	password
	Click Element	jquery=#sso_button
	Wait until Keyword Succeeds	1m	2s	Wait Until Element Is Visible	id=main_menu

Logout using SLO
	[Tags]	NON-CRITICAL	NEED-REVIEW
	Wait Until Element Is Visible	id=lo
	GUI::Basic::Click Element	//*[@id='lo']
	Wait Until Element Is Visible	id=username

Check that SLO worked
	[Tags]	NON-CRITICAL	NEED-REVIEW
	Click Element	jquery=#sso_button
	Wait Until Element Is Visible	username
	Location Should Contain	${SSO_DAG}
	Close all Browsers

Delete Identity Provider
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Authentication::Open Sso Tab
	GUI::Basic::Spinner Should Be Invisible
	select checkbox	xpath=//input[@type='checkbox']
	Click Element	jquery=#delButton
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ssoTable"]/span
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
