*** Settings ***
Documentation	Testing Security:Authentication:SSO Validation
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NAME}	duo
${STATUS}	enabled
${ENTITY_ID}	${SSO_ENTITY_ID}
${x.509}	remote_server

${SSO_URL}	${SSOSERVER_URL}${SSOSERVER_SERVICE_PATH}
${SSO_USER}	${SSOSERVER_USER}
${SSO_PASSWORD}	${SSOSERVER_PASSWORD}
${SSO_ISSUER}	${SSOSERVER_URL}${SSOSERVER_ISSUER_PATH}

${FTP_URL}	${FTPSERVER2_URL}${SSOSERVER_CRT_PATH}
${FTP_USER}	${FTPSERVER_USER}
${FTP_PASSWD}	${FTPSERVER_PASSWORD}
#${FTP_URL_XML}	${FTPSERVER2_URL}${SSOSERVER_XML_PATH}

*** Test Cases ***
Test validation for field=Name
	[Tags]	NON-CRITICAL	NEED-REVIEW
	Click Element	jquery=#addButton
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Fill out fields	ssoName
	Wait Until Keyword Succeeds	30	2	GUI::Basic::Check Text Input Error	ssoName	${EMPTY}	Field must not be empty.
	Wait Until Keyword Succeeds	30	2	GUI::Basic::Check Text Input Error	ssoName	two words	This field must not have spaces.
	Wait Until Keyword Succeeds	30	2	GUI::Basic::Check Text Input Error	ssoName	${POINTS}	This field contains invalid characters.
	Click Element	jquery=#cancelButton
	GUI::Basic::Spinner Should Be Invisible

Test validation for field=Entity ID
	[Tags]	NON-CRITICAL	NEED-REVIEW	BUG_NG_1339
	[Setup]	SUITE:Setup
	Click Element	jquery=#addButton
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Fill out fields	ssoEntityid
	Wait Until Keyword Succeeds	30	2	GUI::Basic::Check Text Input Error	ssoEntityid	${EMPTY}	Field must not be empty.
	[Teardown]	Click Element	jquery=#cancelButton
	GUI::Basic::Spinner Should Be Invisible

Test validation for field=SSO URL
	[Tags]	NON-CRITICAL	NEED-REVIEW
	[Setup]	SUITE:Setup
	Click Element	jquery=#addButton
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Fill out fields	ssoUrl
	Wait Until Keyword Succeeds	30	2	GUI::Basic::Check Text Input Error	ssoUrl	${EMPTY}	Field must not be empty.
	Click Element	jquery=#cancelButton
	GUI::Basic::Spinner Should Be Invisible
	[Teardown]	SUITE:Teardown

Test validation for field=Issuer
	[Tags]	NON-CRITICAL	NEED-REVIEW
	[Setup]	SUITE:Setup
	Click Element	jquery=#addButton
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Fill out fields	ssoIdp
	Wait Until Keyword Succeeds	30	2	GUI::Basic::Check Text Input Error	ssoIdp	${EMPTY}	Field must not be empty.
	Click Element	jquery=#cancelButton
	GUI::Basic::Spinner Should Be Invisible
	[Teardown]	SUITE:Teardown

Test validation for field=systemcert-remote
	[Setup]	SUITE:Setup
	Click Element	jquery=#addButton
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Fill out fields	url
	Input Text	jquery=#userName 	${EMPTY}
	Input Text	jquery=#password	${EMPTY}
	Wait Until Keyword Succeeds	30	2	SUITE:Check Error Banner for URL	url	!@#$%
	Wait Until Keyword Succeeds	30	2	SUITE:Check Error Banner	url	http://aa.aa
	Wait Until Keyword Succeeds	30	2	SUITE:Check Error Banner for FTP URL	url	ftp://aa.aa
	Wait Until Keyword Succeeds	30	2	SUITE:Check Error Banner for Username	userName	name
	Click Element	jquery=#cancelButton
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Open Authentication Tab
	GUI::Security::Authentication::Open Sso Tab
	GUI::Basic::Spinner Should Be Invisible
	${NAMES}=	Create List	${NAME}
	GUI::Basic::Delete Rows In Table If Exists	jquery=table\#multiSSOTable	${NAMES}	HAS_ALERT=False

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Open Authentication Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Authentication::Open Sso Tab
	GUI::Basic::Spinner Should Be Invisible
	${NAMES}=	Create List	${NAME}
	GUI::Basic::Delete Rows In Table If Exists	jquery=table\#multiSSOTable	${NAMES}	HAS_ALERT=False
	GUI::Basic::Logout And Close Nodegrid

SUITE:Fill out fields
	[Arguments]	${FIELD}
	Run Keyword if	'${FIELD}' != 'ssoName'	Input Text	jquery=#ssoName	${NAME}
	Run Keyword if	'${FIELD}' != 'ssoStatus'	Select From List By Value	jquery=#ssoStatus	enabled
	Run Keyword if	'${FIELD}' != 'ssoEntityid'	Input Text	jquery=#ssoEntityid	${ENTITY_ID}
	Run Keyword if	'${FIELD}' != 'ssoUrl'	Input Text	jquery=#ssoUrl	${SSO_URL}
#	Run Keyword if	'${FIELD}' != 'sloUrl'	Input Text	jquery=#sloUrl	${SSO_URL}
	Run Keyword if	'${FIELD}' != 'ssoIdp'	Input Text	jquery=#ssoIdp	${SSO_ISSUER}
	Select Radio Button	systemcertselection	systemcert-remote
	Run Keyword if	'${FIELD}' != 'url'	Input Text	jquery=#url	${FTP_URL}
	Run Keyword if	'${FIELD}' != 'userName'	Input Text	jquery=#userName	${FTP_USER}
	Run Keyword if	'${FIELD}' != 'password'	Input Text	jquery=#password	${FTP_PASSWD}
	#Select Checkbox	//*[@id="absolutepath"]

SUITE:Check Error Banner for URL
	[Arguments]	${LOC}	${INP}
	Input Text	jquery=#${LOC}	${INP}
	Click Element	Jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element		xpath=//p[contains(.,'Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>')]

SUITE:Check Error Banner
	[Arguments]	${LOC}	${INP}
	Input Text	jquery=#${LOC}	${INP}
	Click Element	Jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element		xpath=//p[contains(.,'Error on page. Please check.')]

SUITE:Check Error Banner for FTP URL
	[Arguments]	${LOC}	${INP}
	Input Text	jquery=#${LOC}	${INP}
	Click Element	Jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element		xpath=//p[contains(.,'Protocol requires username/password.')]

SUITE:Check Error Banner for Username
	[Arguments]	${LOC}	${INP}
	Input Text	jquery=#${LOC}	${INP}
	Click Element	Jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element		xpath=//p[contains(.,'File Transfer Failed')]