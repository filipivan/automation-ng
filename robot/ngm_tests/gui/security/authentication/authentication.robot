*** Settings ***
Documentation	Test common behaviour in authentication page
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Test Setup	SUITE:Test Setup

*** Variable ***

*** Test Cases ***
Check if cancel button work in the add page
	SUITE:Add And Cancel

Check if cancel button work in the drilldown page
	SUITE:Drilldown And Cancel

Check if cancel button work in the console page
	SUITE:Console And Cancel

Check id cancel button works after cancel a previous page
	SUITE:Add And Cancel
	SUITE:Drilldown And Cancel
	SUITE:Console And Cancel
	SUITE:Console And Cancel
	SUITE:Drilldown And Cancel
	SUITE:Add And Cancel

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Logout And Close Nodegrid

SUITE:Test Setup
    GUI::Security::Open Authentication Tab
    GUI::Basic::Spinner Should Be Invisible

SUITE:Add And Cancel
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save Button Should Be Visible
	GUI::Basic::Cancel Button Should Be Visible
	Wait Until Element Is Visible	id=mauthMethod
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Wait For Authentication Table

SUITE:Drilldown And Cancel
	GUI::Basic::Drilldown Table Row By Index	\#multiAuthTable	1
	GUI::Basic::Cancel Button Should Be Visible
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Wait For Authentication Table

SUITE:Console And Cancel
	GUI::Basic::Console
	GUI::Basic::Save Button Should Be Visible
	GUI::Basic::Cancel Button Should Be Visible
	Wait Until Element Is Visible	id=consoleAuthTitle
	Wait Until Element Is Visible	id=consoleAuthNote
	GUI::Basic::Cancel
	GUI::Security::Wait For Authentication Table
