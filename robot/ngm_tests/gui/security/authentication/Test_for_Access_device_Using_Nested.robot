*** Settings ***
Documentation	Access device using radius
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE      
Default Tags	EXCLUDEIN3_2

*** Variables ***
${Remote_Server}        ${NESTED_SERVER}
${ldap_base}       ${NESTED_BASE_DOMAIN}
${Confirm_Secret}       secret
${time_out}     2
${Retries}      2
${Method}       LDAP or AD
${Status}       Enabled
${ldap_Port}      default
${ldap_data_username}       ${NESTED_DATA_USER_DN}
${ldap_database_password}       ${NESTED_BROWSE_PASSWORD}
${ldap_confirm_password}        ${NESTED_BROWSE_CONFIRM_PASSWORD}
${ldap_group_attribute}     ${NESTED_GROUP_ATRRIB}
${ldap_login_attributes}        ${LDAPSERVER2_USER}
${ldap_Gbase}       ${NESTED_GROUP_BASE}
${USERNG}       ${NESTED_USER1}
${PWDNG}        ${NESTED_PASS1}

*** Test Cases ***
Test case to add Radius
    GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
    Sleep       5s
    Click Element       //*[@id="mauthMethod"]
    Select From List By Label       //*[@id="mauthMethod"]      ${Method}
    Click Element       //*[@id="mauth2factor"]
    Select From List By Label       //*[@id="mauthStatus"]      ${Status}
    Input Text      //*[@id="mauthServer"]      ${Remote_Server}
    Input Text      //*[@id="ldapbase"]     ${ldap_base}
    Click Element       //*[@id="ldap_authz"]
    Select From List By Label       //*[@id="ldapsecure"]      Start_TLS
    Input Text      //*[@id="ldapPort"]      ${ldap_Port}
    Input Text      //*[@id="ldapbinddn"]      ${ldap_data_username}
    Input Text      //*[@id="ldapbindpw"]      ${ldap_database_password}
    Input Text      //*[@id="ldapbindpw2"]     ${ldap_confirm_password}
    Input Text      //*[@id="ldaploginattributes"]      ${ldap_login_attributes}
    Input Text      //*[@id="ldapgroupattributes"]      ${ldap_group_attribute}
    Click Element       //*[@id="ldapSearchNestGN"]
    Input Text      //*[@id="ldapGNbase"]       ${ldap_Gbase}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible

Test Case to log NG with Radius Crediantials
    GUI::Basic::Open And Login Nodegrid     ${USERNG}       ${PWDNG}
    GUI::Basic::Spinner Should Be Invisible
    Sleep       10s
    Wait Until Element Is Enabled   pwl
    GUI::Basic::Spinner Should Be Invisible

Test Case to Delete Nested Server
    SUITE:Setup
    GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep       5s
	Click Element       //*[@id="1"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid