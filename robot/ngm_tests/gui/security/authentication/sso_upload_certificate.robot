*** Settings ***
Documentation	Test Upload Certificate Functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NAME}	duo
${STATUS}	enabled
${ENTITY_ID}	${SSO_ENTITY_ID}
${DUO_ICON}	css=.iconItem:nth-child(1) > img
${SELECT_CERTIFICATE}	systemcert-localcomputer
${SSO_URL}	${SSOSERVER_URL}
${SSO_USER}	${SSOSERVER_USER}
${SSO_PASSWORD}	${SSOSERVER_PASSWORD}
${SSO_ISSUER}	${SSOSERVER_URL}${SSOSERVER_ISSUER_PATH}
${SSO_LOGOUT_URL}	${SSOSERVER_LOGOUT_URL}
${SSO_FILEPATH}	${SSOSERVER_FILEPATH}
${ENTITY_ID}	${SSO_ENTITY_ID}
${FTP_URL}	${FTPSERVER2_URL}${SSOSERVER_CRT_PATH}
${FTP_USERNAME}	${FTPSERVER2_USER}
${FTP_PASSWORD}	${FTPSERVER2_PASSWORD}
${SSO_ENTITY_ID}	sso_validation.robot 16
#${SSOSERVER_FILEPATH1}	C:\\Users\\QAusers\\dag.crt	#for local user

*** Test Cases ***
Test Upload Certificate Functionality - Remote Server
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Open Authentication Tab
	Click Element	//*[@id="ssoTable"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	${NAME}
	Click Link	${NAME}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	certificate
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	systemcertselection
	Select Radio Button	systemcertselection	systemcert-remote
	Input Text	url	${FTP_URL}
	Input Text	userName	${FTP_USERNAME}
	Input Text	password	${FTP_PASSWORD}
	Sleep	2s
	Click Button	saveButton
	Handle Alert
	GUI::Basic::Spinner Should Be Invisible
	Page Should Not Contain	FILE TRANSFER FAILED
	Sleep	2s
	Page Should Contain	Certificate successfully imported.

	Sleep	5s
	GUI::Basic::Open NodeGrid
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	sso_button
	Click Button	sso_button
	Sleep	5s
	Wait Until Page Contains Element	password	20s
	Input Text	username	${SSO_USER}
	Input Text	password	${SSO_PASSWORD}
	Click Element	//*[@id="login-btn"]
	Sleep	10s
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	main_menu
	[Teardown]	SUITE:Teardown

Test Upload Certificate Functionality - Local Computer
	[Setup]	SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Open Authentication Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ssoTable"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	${NAME}
	Click Link	${NAME}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	certificate
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	systemcertselection
	Radio Button Should Be Set To	systemcertselection	systemcert-localcomputer
	Choose File	clientFilename	${SSOSERVER_FILEPATH1}
	Click Element	saveButton
	Handle Alert
	GUI::Basic::Spinner Should Be Invisible
	Page Should Not Contain	FILE TRANSFER FAILED
	Page Should Contain	Certificate successfully imported.
	Sleep	5s
	GUI::Basic::Open NodeGrid
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	sso_button
	Click Button	sso_button
	Sleep	10s
	Wait Until Page Contains Element	password	20s
	Input Text	username	${SSO_USER}
	Input Text	password	${SSO_PASSWORD}
	Click Element	//*[@id="login-btn"]
	Sleep	10s
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	main_menu

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5
	GUI::Security::Open Authentication Tab
	Click Element	//*[@id="ssoTable"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Wait Until Element Is Accessible	ssoName
	Input Text	ssoName	${NAME}
	Select From List by Value	ssoStatus	${STATUS}
	Input Text	ssoEntityid	${ENTITY_ID}
	Input Text	ssoUrl	${SSO_URL}
	Input Text	ssoIdp	${SSO_ISSUER}
	Click Element	xpath=//input[@value='Select Icon']
	Click Element	${DUO_ICON}
	Radio Button Should Be Set To	systemcertselection	${SELECT_CERTIFICATE}
	Sleep	5s
	wait until keyword succeeds	30	2	Choose File	//*[@id="clientFilename"]	${SSOSERVER_FILEPATH1}
	Select Checkbox	sloEnabled
	Input Text	sloUrl	${SSO_LOGOUT_URL}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Open Authentication Tab
	Click Element	//*[@id="ssoTable"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${NAME}
	GUI::Basic::Logout And Close Nodegrid

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Security::Open Authentication Tab
	Click Element	//*[@id="ssoTable"]
	Wait Until Page Contains Element	//*[@id="duo"]/td[1]/input
	Select Checkbox	//*[@id="duo"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Open Authentication Tab
	Click Element	//*[@id="ssoTable"]
	Page Should Not Contain	${NAME}
	Close All Browsers
