*** Settings ***
Documentation	Test server behaviour in authentication page
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variable ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
@{SERVER_METHODS}	LDAP or AD	RADIUS	TACACS+	Kerberos

*** Test Cases ***
Test LDAP Textboxes
	[Tags]	NON-CRITICAL	NEED-REVIEW
	Click Element	jquery=#addButton
	GUI::Basic::Spinner Should Be Invisible
	${RAND}=	Evaluate	random.randint(1,1000)	modules=random
	gui::auditing::auto input tests	ldapPort	str	12.3	54/1	12,14	${RAND}	FAILBACK=Yes
	gui::auditing::auto input tests	ldaploginattributes	!@#$%^&;[],../	valid	FAILBACK=Yes
	gui::auditing::auto input tests	ldapgroupattributes	!@#$%^&;[],../	valid	FAILBACK=Yes
	Click element	jquery=#ldapSearchNestGN
	GUI::Basic::Elements Should Be Visible	jquery=#ldapGNbase
	gui::auditing::auto input tests	ldapGNbase	!@#$%^&;[],../	valid	FAILBACK=Yes

Test LDAP Passwords
	SUITE:Password Check	a	s	${TRUE}
	SUITE:Password Check	a	a	${FALSE}

Test LDAP or AD Elements
	FOR	${I}	IN RANGE	1	12
		Page Should Contain Element	//*[@id="mauthAddForm"]/div[2]/div/div/div[8]/div[2]/div/div/div[${I}]
	END

Test Radius Elements
	Select From List By Value	mauthMethod	radius
	FOR	${I}	IN RANGE	1	7
		Page Should Contain Element	//*[@id="mauthAddForm"]/div[2]/div/div/div[6]/div[2]/div/div/div[${I}]
	END

Test Radius TextBoxes
	[Tags]	NON-CRITICAL	NEED-REVIEW
	${RAND}=	evaluate	random.randint(1,1000)	modules=random
	gui::basic::auto input tests	radtimeout	str	12.3	54/1	12,14	${RAND}
	gui::basic::auto input tests	radretries	str	12.3	54/1	12,14	${RAND}
	wait until page contains element	radAccServer
	gui::basic::auto input tests	radAccServer	!@#$%^&;[],../	1.1.1.1
	Select Checkbox	//*[@id="radservicetype"]
	gui::basic::auto input tests	servicetype1	12.3	54/1	12,14	1lol2	**	@#$%	.	thisWorks
	gui::basic::auto input tests	servicetype2	12.3	54/1	12,14	1lol2	**	@#$%	.	thisWorks
	gui::basic::auto input tests	servicetype3	12.3	54/1	12,14	1lol2	**	@#$%	.	thisWorks
	gui::basic::auto input tests	servicetype4	12.3	54/1	12,14	1lol2	**	@#$%	.	thisWorks
	gui::basic::auto input tests	servicetype5	12.3	54/1	12,14	1lol2	**	@#$%	.	thisWorks
	gui::basic::auto input tests	servicetype6	12.3	54/1	12,14	1lol2	**	@#$%	.	thisWorks

Test Radius Secret
	Input text	jquery=#radsecret	123
	input text	jquery=#radsecret2	456
	Click element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	Element Should Contain	//*[@id="errormsg"]	Confirm secret mismatch.
	Input text	jquery=#radsecret	${EMPTY}
	input text	jquery=#radsecret2	${EMPTY}
	Click element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

Test TACACS+ Elements
	[Tags]	NON-CRITICAL	NEED-REVIEW
	Select From List By Value	mauthMethod	tacplus
	FOR	${I}	IN RANGE	1	10
		Page Should Contain Element	//*[@id="mauthAddForm"]/div[2]/div/div/div[7]/div[2]/div/div/div[${I}]
	END
	Select Checkbox	//*[@id="tacuserlevel"]
	FOR	${I}	IN RANGE	1	17
		Page Should Contain Element	//*[@id="mauthAddForm"]/div[2]/div/div/div[7]/div[2]/div/div/div[10]/div[2]/div[${I}]
	END
	Unselect Checkbox	//*[@id="tacuserlevel"]

Test TACACS+ Textboxes
	${RAND}=	evaluate	random.randint(1,1000)	modules=random
	gui::basic::auto input tests	tacacsPort	str	12.3	54/1	12,14	${RAND}
	gui::basic::auto input tests	tactimeout	str	12.3	54/1	12,14	${RAND}
	gui::basic::auto input tests	tacretries	str	12.3	54/1	12,14	${RAND}

Test TACACS+ Secret
	Input text	jquery=#tacsecret	123
	input text	jquery=#tacsecret2	456
	Click element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	Element Should Contain	//*[@id="errormsg"]	Confirm secret mismatch.
	Input text	jquery=#tacsecret	${EMPTY}
	input text	jquery=#tacsecret2	${EMPTY}
	Click element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

Test Kerberos
	Select From List By Value	mauthMethod	kerberos
	FOR	${I}	IN RANGE	1	3
		Page Should Contain Element	//*[@id="mauthAddForm"]/div[2]/div/div/div[9]/div[2]/div/div/div[${I}]
	END

Add Server and Delete
	gui::auditing::auto input tests	mauthServer	!@#$%^&;[],../	${EMPTY}	1.1.1.1	FAILBACK=Yes
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="1"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=#delButton
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Authentication::Open Tab

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Authentication::Open Tab
	Click Element	//*[@id="1"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=#delButton
	GUI::Basic::Spinner Should Be Invisible
	FOR	${SERVER_METHOD}	IN	@{SERVER_METHODS}
		Page Should Not Contain	${SERVER_METHOD}
	END
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

SUITE:Password Check
	[Arguments]	${P1}	${P2}	${EXP}
	Input text	jquery=#ldapbindpw	${P1}
	input text	jquery=#ldapbindpw2	${P2}
	Click element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	${TST}=	Execute Javascript	var er=document.getElementById('ldapbindpw2').parentNode.nextSibling;if(er==null){return false;}return true;
	Run Keyword If	${EXP}	Should Be True	${TST}
	...	ELSE	Should Not Be True	${TST}
