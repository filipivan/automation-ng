*** Settings ***
Documentation	Test realms validation
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_0

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test if realms button available on Authentication page
	#Page Should Contain	Realms
	Wait Until Page Contains Element	xpath=//*[@id="realms"]

Test if able to enable/disable Authentication Realms
	Click Element	//*[@id="realms"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//*[@id="local_realms"]
	Unselect Checkbox	//*[@id="local_realms"]
	GUI::Basic::Save
	GUI::Basic::Page Should Not Contain Error
	Select Checkbox	//*[@id="local_realms"]
	GUI::Basic::Save
	GUI::Basic::Page Should Not Contain Error

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Logout And Close Nodegrid

