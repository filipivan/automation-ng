*** Settings ***
Documentation	Access device using Extended_radius_vsa_authentication
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE      
Default Tags	EXCLUDEIN3_2

*** Variables ***
${Remote_Server}        ${RADIUSSERVER2}
${Accounting_Server}        ${RADIUSSERVER2}
${Secret}       ${RADIUSSERVER_SECRET}
${Confirm_Secret}       ${RADIUSSERVER_SECRET}
${time_out}     2
${Retries}      2
${Method}       RADIUS
${Status}       Enabled
${Radius_Port}      default
${Radius_accounting_port}     default
${Service_type_login}       ${RADIUSSERVER_SERVICE_TYPE}
${Service_type_framed}      ${RADIUSSERVER_SERVICE_FRAMED}
${Service_type_Call_back_login}      ${RADIUSSERVER_SERVICE_CALL_BACK_LOGIN}
${Service_type_Call_back_framed}      ${RADIUSSERVER_SERVICE_CALL_BACK_FRAMED}
${USERNG}       ${RADIUSSERVER_USER1}
${PWDNG}        ${RADIUSSERVER_PASS1}

*** Test Cases ***
Test case to add Radius
    GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
    Sleep       5s
    Click Element       //*[@id="mauthMethod"]
    Select From List By Label       //*[@id="mauthMethod"]      ${Method}
    Click Element       //*[@id="mauth2factor"]
    Select From List By Label       //*[@id="mauthStatus"]      ${Status}
    Input Text      //*[@id="mauthServer"]      ${Remote_Server}
    Input Text      //*[@id="radAccServer"]      ${Accounting_Server}
    Input Text      //*[@id="radauthport"]      ${Radius_Port}
    Input Text      //*[@id="radacctport"]      ${Radius_accounting_port}
    Input Text      //*[@id="radsecret"]      secret
    Input Text      //*[@id="radsecret2"]      secret
    Input Text      //*[@id="radtimeout"]      2
    Input Text      //*[@id="radretries"]      2
    Click Element   //*[@id="radservicetype"]
    Input Text      //*[@id="servicetype1"]     ${Service_type_login}
    Input Text      //*[@id="servicetype2"]     ${Service_type_framed}
    Input Text      //*[@id="servicetype3"]     ${Service_type_Call_back_login}
    Input Text      //*[@id="servicetype4"]     ${Service_type_Call_back_framed}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible

Test Case to log NG with Radius Crediantials
    GUI::Basic::Open And Login Nodegrid     ${USERNG}       ${PWDNG}
    GUI::Basic::Spinner Should Be Invisible		1m
    Wait Until Element Is Enabled   pwl
    GUI::Basic::Spinner Should Be Invisible

Test Case to Delete Radius Server
    SUITE:Setup
    GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep       3s
	Click Element       //*[@id="1"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid