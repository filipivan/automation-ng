*** Settings ***
Documentation	Access device using AD Server
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE      
Default Tags	EXCLUDEIN3_2

*** Variables ***
${Remote_Server}        ${AD_SERVER}
${AD_base}      ${AD_BASE_DOMAIN}
${Method}       LDAP or AD
${Status}       Enabled
${AD_Port}      default
${AD_data_username}       testuser@zpesystems.local
${AD_database_password}       ${AD_BROWSE_PASSWORD}
${AD_confirm_password}        ${AD_BROWSE_PASSWORD}
${AD_login_Attr}        ${AD_USER_ATRRIB}
${AD_group_attribute}     ${AD_GROUP_ATRRIB}
${USERNG}       testuser
${PWDNG}        Passw0rd
${Secure}       Start_TLS

*** Test Cases ***
Test case to add AD Server
    GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
    Sleep       5s
    Click Element       //*[@id="mauthMethod"]
    Select From List By Label       //*[@id="mauthMethod"]      LDAP or AD
    Click Element       //*[@id="mauth2factor"]
    Select From List By Label       //*[@id="mauthStatus"]      Enabled
    Input Text      //*[@id="mauthServer"]      ${Remote_Server}
    Input Text      //*[@id="ldapbase"]     ${AD_base}
    Select From List By Label       //*[@id="ldapsecure"]      ${Secure}
    Input Text      //*[@id="ldapPort"]      ${AD_Port}
    Input Text      //*[@id="ldapbinddn"]      ${AD_data_username}
    Input Text      //*[@id="ldapbindpw"]      ${AD_database_password}
    Input Text      //*[@id="ldapbindpw2"]     ${AD_confirm_password}
    Input Text      //*[@id="ldaploginattributes"]      ${AD_USER_ATRRIB}
    Input Text      //*[@id="ldapgroupattributes"]      ${AD_group_attribute}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible

Test Case to log NG with AD Server Crediantials
    GUI::Basic::Open And Login Nodegrid     ${USERNG}       ${PWDNG}
    GUI::Basic::Spinner Should Be Invisible
    Sleep       15s
    Wait Until Element Is Enabled   pwl
    GUI::Basic::Spinner Should Be Invisible

Test Case to Delete AD Server
    SUITE:Setup
    GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep       5s
	Click Element       //*[@id="1"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid