*** Settings ***
Documentation	Device Using Tacacs Plus
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

*** Variables ***
${Remote_Server}	192.168.2.88
${Accounting_Server}	192.168.2.88
${Secret}	secret
${Confirm_Secret}	secret
${time_out}	2
${Retries}	2
${Method}	TACACS+
${Status}	Enabled
${Tac_Service}	raccess
${Tac_Version}	V0_V1
${Tacacs_Port}	49
${USERNG}	tacacs1
${PWDNG}	tacacs1
${User_Level1}	tac-grp1
${User_Level2}	tac-grp2
${User_Level3}	tac-grp3

*** Test Cases ***
Test case to add Tacacs+
	GUI::Security::Authentication Delete TACACS+ Server If Exists	${Method}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	//*[@id="mauthMethod"]	${Method}
	Click Element	//*[@id="mauth2factor"]
	Select From List By Label	//*[@id="mauthStatus"]	${Status}
	Input Text	//*[@id="mauthServer"]	${Remote_Server}
	Input Text	//*[@id="tacAccServer"]	${Accounting_Server}
	Click Element	//*[@id="tac_authz"]
	Input Text	//*[@id="tacacsPort"]	${Tacacs_Port}
	Select From List By Label	//*[@id="tacservice"]	${Tac_Service}
	Input Text	//*[@id="tacsecret"]	${Secret}
	Input Text	//*[@id="tacsecret2"]	${Confirm_Secret}
	Input Text	//*[@id="tactimeout"]	${time_out}
	Input Text	//*[@id="tacretries"]	${Retries}
	Select From List By Label	//*[@id="tacversion"]	${Tac_Version}
	Click Element	//*[@id="tacuserlevel"]
	Input Text	//*[@id="userlevel1tac"]	tac-grp1
	Input Text	//*[@id="userlevel2tac"]	tac-grp2
	Input Text	//*[@id="userlevel3tac"]	tac-grp3
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Test Case to log NG with Tacacs+ Crediantials
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	Wait Until Element Is Enabled	pwl
	GUI::Basic::Spinner Should Be Invisible

Test Case to Delete Tacacs+ Server
	SUITE:Setup
	Click Element	//*[@id="1"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid