*** Settings ***
Documentation	Testing Security:Authentication:Two Factor Functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NAME}	${RSASERVER_NAME}
${REST_URL}	${RSASERVER_REST_URL}
${CLIENT_KEY}	${RSASERVER_CLIENT_KEY}
${CLIENT_ID}	${RSASERVER_CLIENT_ID}
${READ_TIMEOUT}	${RSASERVER_READ_TIMEOUT}
${CONNECT_TIMEOUT}	${RSASERVER_CONNECT_TIMEOUT}
${MAX_RETRIES}	${RSASERVER_MAX_RETRIES}
${REPLICAS}	${RSASERVER_REPLICAS}
${REPLICAS_PORTS}	${RSASERVER_REPLICAS_PORTS}
${POLICY_ID}	${RSASERVER_POLICY_ID}
${TENANT_ID}	${RSASERVER_TENANT_ID}

${FTP_ROOTCA}	${FTPSERVER2_URL}${RSASERVER_ROOTCA}
${FTP_USERNAME}	${FTPSERVER2_USER}
${FTP_PASSWORD}	${FTPSERVER2_PASSWORD}

${USER_JOE}	joe
${PASSWD_JOE}	Joe@12345

*** Test Cases ***
Add valid 2-factor configuration
	${NAMES}=	Create List	${NAME}
	GUI::Basic::Delete Rows In Table If Exists	jquery=table\#multiSecondFactorTable	${NAMES}	HAS_ALERT=False
	Click Element	jquery=#addButton
	GUI::Basic::Spinner Should Be Invisible
	Input Text	jquery=#secondFactorName	${NAME}
	Run Keyword If  '${NGVERSION}'>='5.8' 		Wait Until Element Is Visible 	id=secondFactorMethod
	Run Keyword If  '${NGVERSION}'>='5.8'		Select From List By Label 		id=secondFactorMethod		RSA
	Select From List By Value	jquery=#secondFactorStatus	enabled
	Input Text	jquery=#rest-url	${REST_URL}
	Input Text	jquery=#client-key	${CLIENT_KEY}
	Input Text	jquery=#client-id	${CLIENT_ID}
	Select Checkbox	//*[@id="replicas-enabled"]
	Input Text	jquery=#replicas	${REPLICAS_PORTS}
	Input Text	jquery=#read-timeout	${READ_TIMEOUT}
	Input Text	jquery=#connect-timeout	${CONNECT_TIMEOUT}
	Input Text	jquery=#max-retries	${MAX_RETRIES}
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

Add Valid Certificate
	Click Element	xpath=//a[contains(text(),'rsa_method')]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=#certificate
	GUI::Basic::Spinner Should Be Invisible
	Select Radio Button	systemcertselection	systemcert-remote
	GUI::Basic::Spinner Should Be Invisible
	Input Text	jquery=#url	${FTP_ROOTCA}
	Input Text	jquery=#userName	${FTP_USERNAME}
	Input Text	jquery=#password	${FTP_PASSWORD}
	Click Element	jquery=#saveButton
	Handle Alert	ACCEPT
	GUI::Basic::Spinner Should Be Invisible
	Wait until Element Is Not Visible	xpath=//*[@id='loading']	timeout=300s
	Click Element	//*[@id="finish"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=#cancelButton
	GUI::Basic::Spinner Should Be Invisible

Add user for 2-factor
	GUI::Basic::Security::Local Accounts::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Local Accounts::Add	${USER_JOE}

Add 2-factor to local authentication
	GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Drilldown Table Row By Index	\#multiAuthTable	1
	Select From List By Value	jquery=#mauth2factor	${NAME}
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Hosts::Open Tab
	GUI::Basic::Add Button Should Be Visible
	GUI::Basic::Add
	Input Text	id=hostip	${RSASERVER_ADDRESS}
	Input Text	id=name	${RSASERVER_ALIAS}
	Input Text	id=alias	${RSASERVER_ALIAS}
	GUI::Basic::Save
	GUI::Basic::Logout And Close Nodegrid

Login and check that login popup appears
	[Tags]	BUG_NG_3286
	GUI::Basic::Open NodeGrid
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	password
	Input Text	username	 ${USER_JOE}
	Input Text	password	 ${USER_JOE}
	Click Element	id=login-btn
	GUI::Basic::Spinner Should Be Invisible
	Sleep	10s
	Wait Until Element Is Visible	id=pam-popup	timeout=60s
	${USER_NAME}=	Get Text	jquery=#pam-username
	Should Be Equal	${USER_NAME}	${USER_JOE}:
	${RSA_NAME}=	Get Text	jquery=#pam-server-name
	Should Be Equal	${RSA_NAME}	${NAME}
	Close all Browsers

Reset local authentication
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Drilldown Table Row By Index	\#multiAuthTable	1
	Select From List By Value	jquery=#mauth2factor	none
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

Delete rule and user
	GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	secondFactorTable
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox		xpath=//thead[@id='thead']/tr/th/input
	Click Element	jquery=#delButton
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Local Accounts::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Local Accounts::Delete	${USER_JOE}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	secondFactorTable
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers