*** Settings ***
Documentation	Test realms functionality tests through the GUI as Realm and as NG auth step selector
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_0

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SERVER115}	192.168.2.115
${SECRET}	${RADIUSSERVER_SECRET}
${COMMON_USER}	${RADIUSSERVER2_USER_TEST1}
${COMMON_PASS}	${RADIUSSERVER2_USER_TEST1_PASSWORD}
${METHOD}	RADIUS
${STATUS}	Enabled
${RADIUS_PORT}	default
${RADIUS_ACCOUNTING_PORT}	default
${TEST_ZPE}	testzpe
${REALM_115}	newrealm

*** Test Cases ***
Test normal logins should work with NG realms enabled
	SUITE:Check Server Selection Based On Realms enabled
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid	${COMMON_USER}  ${COMMON_PASS}
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

Test legacy logins with user@server syntax and NG realms enabled works
	GUI::Basic::Open And Login Nodegrid	${COMMON_USER}@${SERVER115} 	${COMMON_PASS}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//*[@id="pwl"]
	Close All Browsers

Test legacy logins with server\user syntax and NG realms enabled works
	GUI::Basic::Open And Login Nodegrid	${SERVER115}\\${COMMON_USER} 	${COMMON_PASS}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//*[@id="pwl"]
	Close All Browsers

Test RADIUS realms logins with user@realm syntax and NG realms enabled do not works
	GUI::Basic::Open NodeGrid
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	password
	Input Text	username	${TEST_ZPE}@${REALM_115}
	Input Text	password	${TEST_ZPE}
	Click Element	id=login-btn
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Authentication failure
	Close All Browsers

Test RADIUS realms logins with realm\user syntax and NG realms enabled do not works
	GUI::Basic::Open NodeGrid
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	password
	Input Text	username	${REALM_115}\\${TEST_ZPE}
	Input Text	password	${TEST_ZPE}
	Click Element	id=login-btn
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Authentication failure
	Close All Browsers

Test normal logins should work with NG realms disabled
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Set Server Selection Based On Realms disabled
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//*[@id="pwl"]
	GUI::Basic::Logout And Close Nodegrid
	GUI::Basic::Open And Login Nodegrid	${COMMON_USER}  ${COMMON_PASS}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//*[@id="pwl"]
	Close All Browsers

Test legacy logins with user@server syntax and NG realms disabled do not works
	GUI::Basic::Open NodeGrid
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	password
	Input Text	username	${COMMON_USER}@${SERVER_115}
	Input Text	password	${COMMON_PASS}
	Click Element	id=login-btn
	GUI::Basic::Spinner Should Be Invisible
	Page Should Not Contain	xpath=//*[@id="pwl"]
	Close All Browsers

Test legacy logins with server\user syntax and NG realms disabled do not works
	GUI::Basic::Open NodeGrid
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	password
	Input Text	username	${SERVER_115}\\${COMMON_USER}
	Input Text	password	${COMMON_PASS}
	Click Element	id=login-btn
	GUI::Basic::Spinner Should Be Invisible
	Page Should Not Contain	xpath=//*[@id="pwl"]
	Close All Browsers

Test RADIUS realms logins with user@realm syntax and NG realms disabled works
	GUI::Basic::Open And Login Nodegrid	${TEST_ZPE}@${REALM_115}	${TEST_ZPE}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//*[@id="pwl"]
	Close All Browsers

Test RADIUS realms logins with realm\user syntax and NG realms disabled works
	GUI::Basic::Open And Login Nodegrid	${REALM_115}\\${TEST_ZPE}   ${TEST_ZPE}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//*[@id="pwl"]
	Close All Browsers

Test Delete RADIUS realms
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Set Server Selection Based On Realms enabled
	SUITE:Delete RADIUS server
	CLI:Close Connection
	GUI::Basic::Logout And Close Nodegrid

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Configure RADIUS server	${SERVER115}
	SUITE:Run Freeradius Process In Foreground

SUITE:Configure RADIUS server
	[Arguments]	${IP_ADDRESS}
	GUI::Security::Open Authentication Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	Wait Until Page Contains Element   xpath=//*[@id="mauthMethod"]
	Select From List By Label   xpath=//*[@id="mauthMethod"]	${METHOD}
	Select From List By Label	//*[@id="mauthStatus"]	${STATUS}
	Select Checkbox	//*[@id="mauthFallback"]
	Input Text	//*[@id="mauthServer"]	${IP_ADDRESS}
	Input Text	//*[@id="radAccServer"]	${IP_ADDRESS}
	Input Text	//*[@id="radauthport"]	${RADIUS_PORT}
	Input Text	//*[@id="radacctport"]	${RADIUS_ACCOUNTING_PORT}
	Input Text	//*[@id="radsecret"]	${RADIUSSERVER_SECRET}
	Input Text	//*[@id="radsecret2"]	${RADIUSSERVER_SECRET}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Delete RADIUS server
	GUI::Security::Open Authentication Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="1"]/td[1]/input
	GUI::Basic::Delete

SUITE:Run Freeradius Process In Foreground
	CLI:Open	root	${ROOT_PASSWORD}	server115_session	HOST_DIFF=${SERVER115}	TYPE=shell
	${PROCESSES}	CLI:Write	ps -ef | grep freeradius
	Should Contain	${PROCESSES}	freerad
	${PID}	CLI:Write	pidof freeradius	user=Yes
	Log	${PID}
	CLI:Write	kill ${PID}
	Write	/usr/sbin/freeradius -X
	Read Until	Ready to process requests.

SUITE:Check Server Selection Based On Realms enabled
	GUI::Security::Open Authentication Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element   xpath=//*[@id="realms"]
	Click Element   //*[@id="realms"]
	Wait Until Page Contains Element   xpath=//*[@id="local_realms"]
	checkbox should be selected	//*[@id="local_realms"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:Set Server Selection Based On Realms enabled
	GUI::Security::Open Authentication Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element   xpath=//*[@id="realms"]
	Click Element   //*[@id="realms"]
	Wait Until Page Contains Element   xpath=//*[@id="local_realms"]
	Select Checkbox	//*[@id="local_realms"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Set Server Selection Based On Realms disabled
	GUI::Security::Open Authentication Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element   xpath=//*[@id="realms"]
	Click Element   //*[@id="realms"]
	Wait Until Page Contains Element   xpath=//*[@id="local_realms"]
	Unselect Checkbox	//*[@id="local_realms"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	CLI:Close Connection
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid


