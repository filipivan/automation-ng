*** Settings ***
Documentation	Access device using radius
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

*** Variables ***
${Remote_Server}	${RADIUSSERVER2}
${Accounting_Server}	${RADIUSSERVER2}
${Secret}	${RADIUSSERVER_SECRET}
${Confirm_Secret}	${RADIUSSERVER_SECRET}
${time_out}	2
${Retries}	2
${Method}	RADIUS
${Status}	Enabled
${Radius_Port}	default
${Radius_accounting_port}	default
${USERNG}	${RADIUSSERVER2_USER_TEST1}
${PWDNG}	${RADIUSSERVER2_USER_TEST1_PASSWORD}

*** Test Cases ***
Test case to add Radius
	GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	Click Element	//*[@id="mauthMethod"]
	Select From List By Label	//*[@id="mauthMethod"]	${Method}
	Click Element	//*[@id="mauth2factor"]
	Select From List By Label	//*[@id="mauthStatus"]	${Status}
	Input Text	//*[@id="mauthServer"]	${Remote_Server}
	Input Text	//*[@id="radAccServer"]	${Accounting_Server}
	Input Text	//*[@id="radauthport"]	${Radius_Port}
	Input Text	//*[@id="radacctport"]	${Radius_accounting_port}
	Input Text	//*[@id="radsecret"]	${RADIUSSERVER_SECRET}
	Input Text	//*[@id="radsecret2"]	${RADIUSSERVER_SECRET}
	Input Text	//*[@id="radtimeout"]	${time_out}
	Input Text	//*[@id="radretries"]	${Retries}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Test Case to log NG with Radius Crediantials
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	Wait Until Element Is Enabled	pwl
	GUI::Basic::Spinner Should Be Invisible

Test Case to Delete Radius Server
	SUITE:Setup
	GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	Click Element	//*[@id="1"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid