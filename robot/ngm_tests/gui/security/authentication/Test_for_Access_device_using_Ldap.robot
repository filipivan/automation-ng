*** Settings ***
Documentation	Access device using radius
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE      
Default Tags	EXCLUDEIN3_2

*** Variables ***
${Remote_Server}        ${LDAPSERVER}
${ldap_base}       ${LDAPSERVER_BASE}
${Confirm_Secret}       ${LDAPSERVER_SECRET}
${time_out}     2
${Retries}      2
${Method}       LDAP or AD
${Status}       Enabled
${ldap_Port}      default
${ldap_data_username}       ${LDAPSERVER_USR}
${ldap_database_password}       ${LDAPSERVER_PASS}
${ldap_confirm_password}        ${LDAPSERVER_CONFRM_PASS}
${ldap_group_attribute}     ${LDAPSERVER_GRP_ATTR}
${USERNG}       ${LDAPSERVER_USER1}
${PWDNG}        ${LDAPSERVER_PASS1}

*** Test Cases ***
Test case to add Radius
    GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
    Sleep       5s
    Click Element       //*[@id="mauthMethod"]
    Select From List By Label       //*[@id="mauthMethod"]      ${Method}
    Click Element       //*[@id="mauth2factor"]
    Select From List By Label       //*[@id="mauthStatus"]      ${Status}
    Click Element       //*[@id="mauthFallback"]
    Input Text      //*[@id="mauthServer"]      ${Remote_Server}
    Input Text      //*[@id="ldapbase"]     ${ldap_base}
    Click Element       //*[@id="ldap_authz"]
    Input Text      //*[@id="ldapPort"]      ${ldap_Port}
    Input Text      //*[@id="ldapbinddn"]      ${ldap_data_username}
    Input Text      //*[@id="ldapbindpw"]      ${ldap_database_password}
    Input Text      //*[@id="ldapbindpw2"]     ${ldap_confirm_password}
    Input Text      //*[@id="ldapgroupattributes"]      ${ldap_group_attribute}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible

Test Case to log NG with Radius Crediantials
    GUI::Basic::Open And Login Nodegrid     ${USERNG}       ${PWDNG}
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Element Is Enabled   pwl
    GUI::Basic::Spinner Should Be Invisible

Test Case to Delete Radius Server
    SUITE:Setup
    GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep       5s
	Click Element       //*[@id="1"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid