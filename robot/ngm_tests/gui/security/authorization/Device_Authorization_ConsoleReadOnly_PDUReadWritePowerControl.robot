*** Settings ***
Documentation	Manage Devices Adding Permission
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS		CHROME		FIREFOX
Default Tags	GUI

Suite Setup	SUITE:Setup

*** Variables ***
${USER}		test_user
${PASS}		test_user
${SUDO_USER}	test_sudo_user
${GROUP}	test_group
${SUDO_GROUP}	test_sudo_group
${DEVICE}	test-dev.ice
${PDU_DEVICE}	test_raritan
${DEVICE_TYPE}		pdu_raritan
${DEVICE_IP}	192.168.3.7
${CONSOLE_IP}	127.0.0.1
${DEVICE_USER}	admin
${DEVICE_ASKPASS}	no
${DEVICE_PWD}	admin
${PDU_ID}	My-PDU
${OUTLET_ID1}	1
${OUTLET1_NAME}		Outlet_1
${CONSOLE_DEVICE}	test_device
${TEST_DEVICE_TYPE}		device_console
${COMMAND}		Outlet

*** Test Cases ***
Test Enter In Root Mode In Console With Sudo Permissions
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Manage All Devices Permissions

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid

SUITE:Manage All Devices Permissions
	GUI::Security::Local Accounts::Add	${USER}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Adding Devices Permissions
	SUITE:Add pdu device/protocol/SNMPV2
	SUITE:Auto discovery and check outlets discovered on outlets page
	SUITE:Accessing Other Devices
	SUITE:Accessing Device through Console
	SUITE:Accessing Device through User
	SUITE:Teardown

SUITE:Adding Devices Permissions
	[Documentation]		Adding Device Permissions through Security->Services->Clicking on Devices access Enforced via user group Authorization.
	GUI::Security::Open Services tab	${USER}
	Click Element	xpath=//*[@id="authorization"]
	GUI::Basic::Save


SUITE:Accessing Other Devices
	[Documentation]		Adding Device to a specific Group
	GUI::Security::Open Authorization tab
	GUI::Basic::Spinner Should Be Invisible
	click link		//*[@id="user"]
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on the group and adding device console and PDU device
	Click Element	//*[@id="spm"]/span
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on Add button
	click element	//*[@id="addButton"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	double click element	//*[@id="targetSelection"]/div/div[1]/div[1]/select/option[@value="${PDU_DEVICE}"]
	double click element	//*[@id="targetSelection"]/div/div[1]/div[1]/select/option[@value="${DEVICE}"]
	click element	//*[@id="saveButton"]
	handle alert
	GUI::Basic::Spinner Should Be Invisible
    #Clicking  on console device
	click link		//*[@id="${DEVICE}"]
	Sleep	5s
	# Handling Permissions for added device as No access for console device
	select radio button		rSessionMode	ReadOnly
	# Clicking on save button
	double click element	//*[@id='saveButton']
	sleep	5s
	handle alert

SUITE:Add pdu device/protocol/SNMPV2
	[Documentation]		Adding a console and a PDU device
	GUI::ManagedDevices::Add Device		${DEVICE}
	# Adding a PDU device
	GUI::ManagedDevices::Add Device		${PDU_DEVICE}	${DEVICE_TYPE}	${DEVICE_IP}	${DEVICE_USER}	${DEVICE_ASKPASS}	${DEVICE_PWD}
	GUI::Basic::Spinner Should Be Invisible
	# Click link with the device that just got added   $
	Click Link	${PDU_DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	# Click Management sub tab
	Click Element	//*[@id="spmmgmt_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	# Select the SNMP version 2 radio button
	Select Checkbox		//*[@id="snmp_proto_enabled"]
	# Enter text as private into Community
	Input Text		//*[@id="community2"] 	private
	# Click on save button
	GUI::Basic::Save
	# Click on Command sub menu
	Click Element	//*[@id="spmcommands_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	# Click on Outlet check box
	Click Element	//*[@id="Outlet"]
	GUI::Basic::Spinner Should Be Invisible
	# Select SNMP as the Protocol from the list
	Select From List By Label	//*[@id="protocol"]	SNMP
	# Click on Save button
	GUI::Basic::Save


SUITE:Auto discovery and check outlets discovered on outlets page
	[Documentation]		Click on Manage Devices -> Auto Discovery sub tab
	GUI::Basic::Managed Devices::Auto Discovery::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	# Click on Discover Now Sub tab
	Click Element	//*[@id="discovery_now"]
	GUI::Basic::Spinner Should Be Invisible
	# Verify that Discover now button is disabled
	Element Should Be Disabled	//*[@id="discovernow"]
	Page Should Contain Element		//*[@id="discovernowTable"]
	# Select the checkbox for the PDU device added
	Select Checkbox		//*[@id="pdu|${PDU_DEVICE}"]/td[1]/input
	# Once the discover now button is enabled click on Discover now
	Click Button	//*[@id="discovernow"]
	GUI::Basic::Spinner Should Be Invisible
	# Click on Manage Devices -> Devices tab
	GUI::Basic::Managed Devices::Devices::open tab
	Sleep    15s
	# Click on the PDU device that got added
	Click Link		${PDU_DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	# Click on Outlets sub tab
	Click Element	//*[@id="spmoutlets_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	# Verify that outlet name appears as expected
	Page Should Contain	${OUTLET1_NAME}


SUITE:Accessing Device through Console
	[Documentation]		Accessing the device console through Console
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	double click element	//*[@id='|${DEVICE}']/td[1]/div/a/span
	GUI::Basic::Spinner Should Be Invisible
	# Verifying the web button is enabled on the pop up
	Element Should Be Enabled	//*[@id="webSession"]
	GUI::Basic::Spinner Should Be Invisible
	# Cancelling the pop up for the device.
	click button	//html/body/div[5]/div/div/div[1]/button
	GUI::Basic::Spinner Should Be Invisible
	#get value  xpath=/html/head/title
	# Checking for read-write permissions clicking on console button as admin
	[Tags]	NON-CRITICAL	BUG_NG-9567
	GUI:Access::Table::Access Device Console	${DEVICE}
	# Press the enter keys
	Press Keys	//body	RETURN
	${OUTPUT}=	GUI:Access::Generic Console Command Output	connect
	Press Keys	//body	RETURN
	${OUTPUT}=	GUI:Access::Generic Console Command Output	connect
	Press Keys	//body	RETURN
	${OUTPUT}=	GUI:Access::Generic Console Command Output	connect
	Press Keys	//body	RETURN
	# Type ls command in the output terminal
	${OUTPUT}=	GUI:Access::Generic Console Command Output	ls
	# Output should contain the folders when we do ls
	Should Contain  ${OUTPUT}	access/	system/	settings/
	switch window
	Close Browser
	GUI::Basic::Open And Login Nodegrid
	# Accessing PDU device through console
	# Click on Access ->Table tab
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	# Click on the PDU device that appears on Table tab.
	Click Element	//*[@id="|${PDU_DEVICE}"]/td[1]/div/a/span
	GUI::Basic::Spinner Should Be Invisible
	# Wait until the outlets appear
	wait until page contains element	//*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]
	# Select the checkbox for the outlet that appears
	Select Checkbox		//tr[@id='${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}']/td[1]/input
	# Click on Outlet Off button
	Click Button	//*[@id="nonAccessControls"]/input[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	# Verify that page should not contain any errors as we have power status
	page should not contain		Error on page. Please check.
	Sleep	5s
	# Click on cancel button on the top right corner
	click button	//*[@id="modal"]/div/div/div[1]/button
	Sleep	5s
	# Click on console button for PDU device appearing under Access-> Table tab
	GUI:Access::Table::Access Device Console	${PDU_DEVICE}
	# Press the enter keys
	Press Keys	//body	RETURN
	${OUTPUT}=	GUI:Access::Generic Console Command Output	connect
	Press Keys	//body	RETURN
	${OUTPUT}=	GUI:Access::Generic Console Command Output	exit
	Sleep	5s
	close browser

SUITE:Accessing Device through User
	[Documentation]		Accessing the device console as user
	[Tags]	NON-CRITICAL	BUG_NG_9567
	# Accessing the device through User
	GUI::Basic::Open And Login Nodegrid 	${USER}	${PASS}
	# Test that the device is visible but read only for the User meaning it should not be clickable
	double click element	//*[@id='|${DEVICE}']/td[1]/div/a/span
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	# Verifying the web button is enabled on the pop up
	Element Should Be Enabled	//*[@id="webSession"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	# Cancelling the pop up for the device.
	click button	//*[@id="modal"]/div/div/div[1]/button
	GUI::Basic::Spinner Should Be Invisible
	#get value  xpath=/html/head/title
	# Checking for read-write permissions clicking on console button as admin
	GUI:Access::Table::Access Device Console	${DEVICE}
	# Press the enter keys
	Press Keys	//body	RETURN
	${OUTPUT}=	GUI:Access::Generic Console Command Output	connect
	Press Keys	//body	RETURN
	# Verify that page should contain read-only message as the user has read only permissions.
	Should Contain  ${OUTPUT}	[read-only -- use ^E c ? for help]
	# Switching the window
	switch window
	Close Browser
	GUI::Basic::Open And Login Nodegrid
	# Accessing PDU device through console
	# Click on Access ->Table tab
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	# Click on the PDU device that appears on Table tab.    /
	Click Element	//*[@id="|${PDU_DEVICE}"]/td[1]/div/a/span
	GUI::Basic::Spinner Should Be Invisible
	# Wait until the outlets appear
	wait until page contains element	//*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]
	# Select the checkbox for the outlet that appears
	Select Checkbox		//tr[@id='${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}']/td[1]/input
	# Click on Outlet Off button
	Click Button	//*[@id="nonAccessControls"]/input[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	# Verify that page should not contain any errors as we have power status
	page should not contain		Error on page. Please check.
	Sleep	5s
	# Click on cancel button on the top right corner
	click button	//*[@id="modal"]/div/div/div[1]/button
	Sleep	5s
	# Click on console button for PDU device appearing under Access-> Table tab
	GUI:Access::Table::Access Device Console	${PDU_DEVICE}
	Sleep  5s
	# Click on Enter keys
	Press Keys	//body	RETURN
	# Write a command for example "exit"
	${OUTPUT}=	GUI:Access::Generic Console Command Output	exit
	Sleep	5s
	close browser

SUITE:Teardown
	[Documentation]		Setting back the permissions
	GUI::Basic::Open And Login Nodegrid
	# Adding the device permissions
	SUITE:Adding Devices Permissions
	# Delete if the user group exists
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	# Delete if the user exists
	GUI::Security::Local Accounts::Delete	${USER}
	# Delete the PDU device which was added
	GUI::ManagedDevices::Delete Device If Exists	${PDU_DEVICE}
	# Delete the console device that was added earlier
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE}
	# Logout and close the browser
	GUI::Basic::Logout And Close Nodegrid