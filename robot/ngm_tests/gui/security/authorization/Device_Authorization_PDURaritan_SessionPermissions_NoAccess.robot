*** Settings ***
Documentation	Manage Devices Adding Permission
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS		CHROME		FIREFOX
Default Tags	GUI

Suite Setup	SUITE:Setup

*** Variables ***
${USER}		test_user
${PASS}		test_user
${SUDO_USER}	test_sudo_user
${GROUP}	test_group
${SUDO_GROUP}	test_sudo_group
${DEVICE}	test-pdu-raritan

*** Test Cases ***
Test Enter In Root Mode In Console With Sudo Permissions
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Manage All Devices Permissions

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid

SUITE:Manage All Devices Permissions
	GUI::Security::Local Accounts::Add	${USER}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Adding Devices Permissions
	SUITE:Accessing Other Devices
	SUITE:Accessing Device through Console
	SUITE:Accessing Device through User
	SUITE:Teardown

SUITE:Adding Devices Permissions
	[Documentation]		Adding Device Permissions through Security->Services->Clicking on Devices access Enforced via user group Authorization.
	GUI::Security::Open Services tab	${USER}
	Click Element	//*[@id="authorization"]
	GUI::Basic::Save

SUITE:Accessing Other Devices
	[Documentation]		Clicking on user group
	GUI::Basic::Security::Authorization::Open Tab
	click link		//*[@id="user"]
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on Devices in the sub menu
	Click Element	//*[@id="spm"]/span
	GUI::Basic::Spinner Should Be Invisible
	#Clicking on other devices
	click link		//*[@id="OTHER DEVICES"]
	Sleep	5s
	# Handling Permissions
	Radio Button Should Be Set To	rSessionMode	NoAccess
	Radio Button Should Be Set To	rPowerMode		NoAccess
	Radio Button Should Be Set To	rDoorMode	NoAccess
	Sleep	5s
	# Clicking on cancel button
	click element	//*[@id="cancelButton"]
	Sleep	5s


SUITE:Accessing Device through Console
	[Documentation]		Accessing the device through Console
	GUI::Basic::Access::Table::Open Tab
	# Clicking on expand for Nodegrid
	click element	//*[@id="expandstate"]
	Sleep	5s
	# Verifying console button is not showing up anywhere on the page.
	element_should_not_be_visible	//*[@id="modal"]/div/div/div[2]/div[1]/div[1]/div/button
	Sleep	5s

SUITE:Accessing Device through User
	[Documentation]		Accessing the device through User
	GUI::Basic::Open And Login Nodegrid 	${USER}	${PASS}
	GUI::Basic::Spinner Should Be Invisible
	# Verifying console button is not showing up anywhere on the page.
	element_should_not_be_visible	//*[@id="modal"]/div/div/div[2]/div[1]/div[1]/div/button
	Sleep	5s

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	SUITE:Adding Devices Permissions
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	GUI::Security::Local Accounts::Delete	${USER}
	GUI::Basic::Logout And Close Nodegrid
