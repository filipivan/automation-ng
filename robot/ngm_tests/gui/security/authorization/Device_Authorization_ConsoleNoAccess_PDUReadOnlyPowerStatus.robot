*** Settings ***
Documentation	Manage Devices Adding Permission
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}		CHROME		FIREFOX
Default Tags	GUI

Suite Setup	SUITE:Setup

*** Variables ***

${USER}		test_user
${PASS}		test_user
${SUDO_USER}	test_sudo_user
${GROUP}	test_group
${SUDO_GROUP}	test_sudo_group
${DEVICE}	test-dev.ice
${PDU_DEVICE}	test_raritan
${DEVICE_TYPE}		pdu_raritan
${DEVICE_IP}	192.168.3.7
${CONSOLE_IP}	127.0.0.1
${DEVICE_USER}	admin
${DEVICE_ASKPASS}	no
${DEVICE_PWD}	admin
${PDU_ID}	My-PDU
${OUTLET_ID1}	1
${OUTLET1_NAME}		Outlet_1
${CONSOLE_DEVICE}	test_device
${TEST_DEVICE_TYPE}		device_console
${COMMAND}		Outlet

*** Test Cases ***
Test Enter In Root Mode In Console With Sudo Permissions
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Manage All Devices Permissions

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid

SUITE:Manage All Devices Permissions
	GUI::Security::Local Accounts::Add	${USER}
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	SUITE:Adding Devices Permissions
	SUITE:Add pdu device/protocol/SNMPV2
	SUITE:Auto discovery and check outlets discovered on outlets page
	SUITE:Accessing Other Devices
	SUITE:Accessing Device through Console
	SUITE:Accessing Device through User
	SUITE:Teardown

SUITE:Adding Devices Permissions
	[Documentation]		Adding Device Permissions through Security->Services->Clicking on Devices access Enforced via user group Authorization.
	GUI::Security::Open Services tab	${USER}
	Run Keyword And Continue on Failure		select checkbox	//*[@id="authorization"]
	Run Keyword And Continue on Failure		Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:Unselect Devices Permissions
	[Documentation]		Adding Device Permissions through Security->Services->Clicking on Devices access Enforced via user group Authorization.
	GUI::Security::Open Services tab	${USER}
	unselect checkbox	//*[@id="authorization"]
	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible


SUITE:Accessing Other Devices
	[Documentation]		Adding Device to a specific Group
	GUI::Security::Open Authorization tab
	# Click on User link
	click link		//*[@id="user"]
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on the group and adding device
	Click Element	//*[@id="spm"]/span
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on Add button
	click element	//*[@id="addButton"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	double click element	//*[@id="targetSelection"]/div/div[1]/div[1]/select/option[@value="${PDU_DEVICE}"]
	double click element	//*[@id="targetSelection"]/div/div[1]/div[1]/select/option[@value="${DEVICE}"]
	click element	//*[@id="saveButton"]
	handle alert
	GUI::Basic::Spinner Should Be Invisible
	#Clicking on PDU device
	click link		//*[@id="${PDU_DEVICE}"]
	Sleep	5s
	# Handling Permissions for added device session as Read Only and power as Power status
	select radio button		rSessionMode	ReadOnly
	select radio button		rPowerMode	PowerStatus
	sleep	5s
	double click element	//*[@id='saveButton']
	sleep	5s
	handle alert
	Sleep	5s
	#Click on console device
	click link		//*[@id="${DEVICE}"]
	Sleep	5s
	# Handling Permissions for added device as No access for console device
	select radio button		rSessionMode	NoAccess
	Sleep	5s
	# Clicking on save button
	double click element	//*[@id='saveButton']
	sleep	5s
	handle alert
	GUI::Basic::Spinner Should Be Invisible

SUITE:Add pdu device/protocol/SNMPV2
	[Documentation]		Adding a console and PDU device
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE}
	GUI::ManagedDevices::Add Device		${DEVICE}
	Sleep	5s
	# Adding a PDU device
	GUI::ManagedDevices::Delete Device If Exists	${PDU_DEVICE}
	GUI::ManagedDevices::Add Device		${PDU_DEVICE}	${DEVICE_TYPE}	${DEVICE_IP}	${DEVICE_USER}	${DEVICE_ASKPASS}	${DEVICE_PWD}
	# Click link with the device that just got added
	Click Link		${PDU_DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	# Click Management sub tab
	Click Element	//*[@id="spmmgmt_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	# Select the SNMP version 2 radio button
	Select Checkbox		//*[@id="snmp_proto_enabled"]
	# Enter text as private into Community
	Input Text		//*[@id="community2"]	private
	# Click on save button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	# Click on Command sub menu
	Click Element	//*[@id="spmcommands_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	# Click on Outlet check box
	Click Element	//*[@id="Outlet"]
	GUI::Basic::Spinner Should Be Invisible
	# Select SNMP as the Protocol from the list
	Select From List By Label	//*[@id="protocol"]		SNMP
	# Click on Save button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible


SUITE:Auto discovery and check outlets discovered on outlets page
	[Documentation]		Click on Manage Devices -> Auto Discovery sub tab
	GUI::Basic::Managed Devices::Auto Discovery::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	# Click on Discover Now Sub tab
	Click Element	//*[@id="discovery_now"]
	GUI::Basic::Spinner Should Be Invisible
	# Verify that Discover now button is disabled
	Element Should Be Disabled		//*[@id="discovernow"]
	Page Should Contain Element		//*[@id="discovernowTable"]
	# Select the checkbox for the PDU device added
	Select Checkbox	//*[@id="pdu|${PDU_DEVICE}"]/td[1]/input
	# Once the discover now button is enabled click on Discover now
	Click Button	//*[@id="discovernow"]
	GUI::Basic::Spinner Should Be Invisible
	# Click on Manage Devices -> Devices tab
	GUI::Basic::Managed Devices::Devices::open tab
	Sleep	15s
	# Click on the PDU device that got added
	Click Link		${PDU_DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	# Click on Outlets sub tab
	Click Element	//*[@id="spmoutlets_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	# Verify that outlet name appears as expected
	Page Should Contain		${OUTLET1_NAME}



SUITE:Accessing Device through Console
	[Documentation]		Accessing the device console through Console
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	# Verifying the console button on the pop up
	Element Should Be Enabled	//*[@id="|${DEVICE}"]/td[2]/div/a[1]
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on the device through console as Admin
	double click element	//*[@id='|${DEVICE}']/td[1]/div/a/span
	Sleep	10s
	# Cancelling the pop up for the device.
	Click Button	//*[@id="modal"]/div/div/div[1]/button
	# Checking for read-write permissions clicking on console button as admin
	[Tags]	NON-CRITICAL	BUG_NG-9920
	GUI:Access::Table::Access Device Console	${DEVICE}
	# Press the enter keys
	Press Keys	//body	RETURN
	${OUTPUT}=	GUI:Access::Generic Console Command Output	connect
	Press Keys	//body	RETURN
	${OUTPUT}=	GUI:Access::Generic Console Command Output	connect
	Press Keys	//body	RETURN
	${OUTPUT}=	GUI:Access::Generic Console Command Output	connect
	Press Keys	//body	RETURN
	# Type ls command in the output terminal
	${OUTPUT}=	GUI:Access::Generic Console Command Output	ls
	# Output should contain the folders when we do ls
	Should Contain  ${OUTPUT}	access/	system/	settings/
	switch window
	Close Browser
	GUI::Basic::Open And Login Nodegrid
	# Accessing PDU device through console
	# Click on Access ->Table tab
	GUI:Access::Open Table tab
	# Click on the PDU device that appears on Table tab.
	Click Element	//*[@id="|${PDU_DEVICE}"]/td[1]/div/a/span
	GUI::Basic::Spinner Should Be Invisible
	# Wait until the outlets appear
	wait until page contains element	//*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]
	# Select the checkbox for the outlet that appears
	Select Checkbox		//tr[@id='${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}']/td[1]/input
	# Click on Outlet Off button
	Click Button	//*[@id="nonAccessControls"]/input[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	# Verify that page should not contain any errors as we have power status
	page should not contain  Error on page. Please check.
	Sleep	5s
	# Click on cancel button on the top right corner
	Click Button	//*[@id="modal"]/div/div/div[1]/button
	Sleep	5s
	# Click on console button for PDU device appearing under Access-> Table tab
	GUI:Access::Table::Access Device Console	${PDU_DEVICE}
	# Press the enter keys
	Press Keys	//body	RETURN
	${OUTPUT}=	GUI:Access::Generic Console Command Output	connect
	Press Keys	//body	RETURN
	${OUTPUT}=	GUI:Access::Generic Console Command Output	exit
	Sleep	5s
	GUI::Basic::Spinner Should Be Invisible
	close browser


SUITE:Accessing Device through User
	[Documentation]		Accessing the device through User
	GUI::Basic::Open And Login Nodegrid 	${USER}	${PASS}
	# Click on Access-> Open tab
	GUI:Access::Open Table tab
	# Click on the PDU device that is visible for the user
	Click Element	//*[@id="|${PDU_DEVICE}"]/td[1]/div/a/span
	GUI::Basic::Spinner Should Be Invisible
	# Wait until the page contains all the buttons
	Wait Until Page Contains Element	css=.btn:nth-child(4)
	# Wait until the page contains outlet names
	wait until page contains element	//*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]
	# Select the check box for any of the outlet  that appears in the table.
	Select Checkbox		//tr[@id='${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}']/td[1]/input
	# Click on Outlet Off button
	Click Button	//*[@id="nonAccessControls"]/input[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	# Verify that page should contain "Error on page.please check" because the user has read only permissions
	page should contain		Error on page. Please check.
	Sleep	5s
	# Click on Cancel button on the top right corner
	Click Button	//*[@id="modal"]/div/div/div[1]/button
	Sleep	5s
	# Click on Access -> Table tab -> PDU console button
	GUI:Access::Table::Access Device Console	${PDU_DEVICE}
	Sleep	5s
	# Press the enter keys
	Press Keys	//body	RETURN
	${OUTPUT}=	GUI:Access::Generic Console Command Output	connect
	Press Keys	//body	RETURN
	# Verify that page should contain read-only message as the user has read only permissions.
	Should Contain  ${OUTPUT}	[read-only -- use ^E c ? for help]
	GUI::Basic::Spinner Should Be Invisible
	close browser

SUITE:Teardown

	[Documentation]		Setting back all the permissions
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Unselect Devices Permissions
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	GUI::Security::Local Accounts::Delete	${USER}
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	GUI::Security::Local Accounts::Delete	${USER}
	SUITE:Delete User on Security :: Authorization :: user :: Devices
	GUI::ManagedDevices::Delete Device If Exists	${PDU_DEVICE}
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE}
	GUI::Basic::Logout And Close Nodegrid

SUITE:Delete User on Security :: Authorization :: user :: Devices
	GUI::Security::Open Authorization tab
	# Click on User link
	click link		//*[@id="user"]
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on the group and adding device
	Click Element	//*[@id="spm"]/span
	GUI::Basic::Spinner Should Be Invisible
	click element		//*[@id="thead"]/tr/th[1]/input
	click element		//*[@id="delButton"]
	GUI::Basic::Spinner Should Be Invisible
