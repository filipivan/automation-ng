*** Settings ***
Documentation	Testing for SSH Access using Public Key
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	NON-CRITICAL	NEED-REVIEW
Default Tags	EXCLUDEIN4_2

Suite Setup		SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${METHOD2}	tacplus
${SECRET}	secret
${BASE}	dc=zpe,dc=net
${DATABASE_USERNAME}	cn=admin,dc=zpe,dc=net
${DATABASE_PASSWORD}	administrator
${LDAP_PORT}	default
${root_login}	shell sudo su -
${USER}	jane
${PASSWD}	jane123
${SSH_COPYID}	ssh-copy-id ${USER}@${HOST}
${LOGIN_COPYID}	ssh '${USER}@${HOST}'
${USER_TACACS}	tacacs1
${PASSWD_TACACS}	tacacs
${SSH_COPYID_TACACS}	ssh-copy-id ${USER_TACACS}@${HOST}
${LOGIN_COPYID_TACACS}	ssh '${USER_TACACS}@${HOST}'
${exit_session}	exit
${DELETE_KNOWNHOST}	rm -rf /home/root/.ssh/

*** Test Cases ***
Test Generate Public Key And Login With It Into Nodegrid For LDAP
	SUITE:Setup
	GUI::Security::Open Authentication Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	id=mauthStatus	enabled
	Select Checkbox	id=mauthFallback
	Input Text	id=mauthServer	${LDAPSERVER}
	Input Text	id=ldapbase	${BASE}
	Select Checkbox	id=ldap_authz
	Select From List By Value	id=ldapsecure	off
	Input Text	id=ldapPort	${LDAP_PORT}
	Input Text	id=ldapbinddn	${DATABASE_USERNAME}
	Input Text	id=ldapbindpw	${DATABASE_PASSWORD}
	Input Text	id=ldapbindpw2	${DATABASE_PASSWORD}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Local Accounts::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=uName	${USER}
	Input Text	id=uPasswd	${PASSWD}
	Input Text	id=cPasswd	${PASSWD}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Authorization::Open User Group Profile Tab	user
	sleep	5
	Select Radio Button	start_appl	start_appl_shell
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${ret}=	Get value	id=hostname
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="peer_header"]//a[text()="Console"]
	sleep	5
	Switch Window	title=${ret}
	Wait Until Page Contains Element	//iframe[@id='termwindow']
	Select Frame	//iframe[@id='termwindow']
	Sleep	10
	Press Keys	None	RETURN
	Press Keys	None	RETURN

	sleep	5
	${OUTPUT}=	SUITE:Console Command Output		${root_login}
	sleep	5
	Press Keys	None	ENTER
	${OUTPUT}=	SUITE:Console Command Output	${DELETE_KNOWNHOST}
	${OUTPUT}=	SUITE:Console Command Output	ssh-keygen
	sleep	5
	Press Keys	None	ENTER
	sleep	5
	Press Keys	None	ENTER
	sleep	5
	Press Keys	None	ENTER
	sleep	5
	${OUTPUT}=	SUITE:Console Command Output	${SSH_COPYID}
	sleep	5
	${OUTPUT}=	SUITE:Console Command Output	yes
	sleep	10
	${OUTPUT}=	SUITE:Console Command Output	${PASSWD}
	sleep	5
	${OUTPUT}=	SUITE:Console Command Output	${LOGIN_COPYID}
	sleep	5
	Unselect Frame
	${OUTPUT}=	SUITE:Console Command Output	${exit_session}
	${OUTPUT}=	SUITE:Console Command Output	${exit_session}
	${OUTPUT}=	SUITE:Console Command Output	${exit_session}
	Switch Browser	1
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Open Security
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Teardown

Test Generate Public Key And Login With It Into Nodegrid For TACACS
	SUITE:Setup
	GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	id=mauthMethod	${METHOD2}
	Select From List By Value	id=mauthStatus	enabled
	Select Checkbox	id=mauthFallback
	Input Text	id=mauthServer	${TACACS_SERVER}
	Input Text	id=tacsecret	${SECRET}
	Input Text	id=tacsecret2	${SECRET}
	Select Checkbox	id=tac_authz
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Local Accounts::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=uName	${USER_TACACS}
	Input Text	id=uPasswd	${PASSWD_TACACS}
	Input Text	id=cPasswd	${PASSWD_TACACS}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Authorization::Open User Group Profile Tab	user
	sleep	5
	Select Radio Button	start_appl	start_appl_shell
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${ret}=	Get value	id=hostname
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="peer_header"]//a[text()="Console"]
	sleep	5
	Switch Window	title=${ret}
	Wait Until Page Contains Element	//iframe[@id='termwindow']
	Select Frame	//iframe[@id='termwindow']
	Sleep	10
	Press Keys	None	RETURN
	Press Keys	None	RETURN

	${OUTPUT}=	SUITE:Console Command Output		${root_login}
	sleep	5
	Press Keys	None	ENTER
	${OUTPUT}=	SUITE:Console Command Output	${DELETE_KNOWNHOST}
	${OUTPUT}=	SUITE:Console Command Output	ssh-keygen
	sleep	5
	Press Keys	None	ENTER
	sleep	5
	Press Keys	None	ENTER
	sleep	5
	Press Keys	None	ENTER
	sleep	5
	${OUTPUT}=	SUITE:Console Command Output	${SSH_COPYID_TACACS}
	sleep	5
	${OUTPUT}=	SUITE:Console Command Output	yes
	sleep	5
	${OUTPUT}=	SUITE:Console Command Output	${PASSWD_TACACS}
	sleep	5
	${OUTPUT}=	SUITE:Console Command Output	${LOGIN_COPYID_TACACS}
	sleep	5
	Unselect Frame
	${OUTPUT}=	SUITE:Console Command Output	${exit_session}
	${OUTPUT}=	SUITE:Console Command Output	${exit_session}
	${OUTPUT}=	SUITE:Console Command Output	${exit_session}
	Switch Browser	1
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Open Security
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Open Security
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Local Accounts::Delete	${USER}
	GUI::Security::Local Accounts::Delete	${USER_TACACS}

SUITE:Teardown
	GUI::Security::Authorization::Open User Group Profile Tab	user
	sleep	5
	Select Radio Button	start_appl	start_appl_cli
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Authentication::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="1"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Local Accounts::Delete	${USER}
	GUI::Security::Local Accounts::Delete	${USER_TACACS}
	GUI::Basic::Logout And Close Nodegrid

SUITE:Console Command Output
	[Arguments]	${COMMAND}
	[Documentation]	Input the given command into the ttyd terminal and retrieve the output after prompt lines
	...	== ARGUMENTS ==
	...	-	COMMAND = Command to be executed
	...	== EXPECTED RESULT ==
	...	Input the command into the ttyd terminal and returns the OUTPUT between the command and the last prompt

	${CHECK_TAB}=	Run Keyword And Return Status	Should Not Contain	${COMMAND}	TAB
	Run Keyword If	${CHECK_TAB}	Press Keys 	None	${COMMAND}	RETURN
	...	ELSE	Press Keys 	None	${COMMAND}
	Sleep	1
