*** Settings ***
Documentation	Manage Devices Adding Permission
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EXCLUDEIN3_2
Default Tags	GUI

Suite Setup	SUITE:Setup

*** Variables ***
${USER}	test_user
${PASS}  test_user
${SUDO_USER}	test_sudo_user
${GROUP}	test_group
${SUDO_GROUP}	test_sudo_group
${DEVICE}	test-dev.ice
*** Test Cases ***
Test Enter In Root Mode In Console With Sudo Permissions
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Manage All Devices Permissions

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid

SUITE:Manage All Devices Permissions
	GUI::Security::Local Accounts::Add	${USER}
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	SUITE:Adding Devices Permissions
	SUITE:Accessing Other Devices
	SUITE:Accessing Device through Console
	SUITE:Accessing Device through User
	SUITE:Teardown

SUITE:Adding Devices Permissions
	[Documentation]		Adding Device Permissions through Security->Services->Clicking on Devices access Enforced via user group Authorization.
	GUI::Security::Open Services tab	${USER}
	# Click on authorization subtab
	Click Element	xpath=//*[@id="authorization"]
	# Click on save button
	GUI::Basic::Save


SUITE:Accessing Other Devices
	[Documentation]		Accessing other devices
	# Click on Security -> Authorization sub tab
	GUI::Basic::Security::Authorization::Open Tab
	# Open devices section in Manage devices
	GUI::ManagedDevices::Open
	# Delete the device if already exists
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE}
	# Add a device
	GUI::ManagedDevices::Add Device		${DEVICE}
	# Click on Security -> Authorization tab
	GUI::Security::Open Authorization tab
	# Click on User link
	click link		//*[@id="user"]
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on the group and adding device
	Click Element	//*[@id="spm"]/span
	GUI::Basic::Spinner Should Be Invisible
	# Click on add button
	click element	//*[@id="addButton"]
	GUI::Basic::Spinner Should Be Invisible
	# Double click the device and select it
	double click element	//*[@id="targetSelection"]/div/div[1]/div[1]/select/option[@value='${DEVICE}']
	# Click on save button and handle the pop up
	click element	//*[@id="saveButton"]
	handle alert
	GUI::Basic::Spinner Should Be Invisible
	#Clicking on other devices
	click link		//*[@id="OTHER DEVICES"]
	Sleep	5s
	# Handling Permissions
	select radio button		rSessionMode  ReadWrite
	Sleep	5s
	# Clicking on save button
	double click element	//*[@id='saveButton']
	sleep	5s
	handle alert
	GUI::Basic::Spinner Should Be Invisible


SUITE:Accessing Device through Console
	[Tags]	NON-CRITICAL	BUG_NG_9920
	[Documentation]		Accessing device through console
	# Open and login Nodegrid
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	# Verifying the console button on the pop up
	Element Should Be Enabled	//*[@id="|${DEVICE}"]/td[2]/div/a[1]
	GUI::Basic::Spinner Should Be Invisible
	# Checking for read-write permissions clicking on console button as admin
	GUI:Access::Table::Access Device Console	${DEVICE}
	# Press the enter keys
	Press Keys	//body	RETURN
	Press Keys	//body	RETURN
	${OUTPUT}=  GUI:Access::Generic Console Command Output	connect
	Press Keys	//body	RETURN
	${OUTPUT}=  GUI:Access::Generic Console Command Output	connect
	Press Keys	//body	RETURN
	${OUTPUT}=  GUI:Access::Generic Console Command Output	connect
	# Type ls command in the output terminal
	${OUTPUT}=  GUI:Access::Generic Console Command Output	ls
	# Output should contain the folders when we do ls
	Should Contain  ${OUTPUT}	access/	system/	settings/
	switch window
	Close Browser
	# Open and login Nodegrid
	GUI::Basic::Open And Login Nodegrid
	# Click on the console button of the device from Tree tab
	GUI:Access::Tree::Access Device Console		${DEVICE}
	# Press the enter keys
	Press Keys	//body	RETURN
	Press Keys	//body	RETURN
	${OUTPUT}=  GUI:Access::Generic Console Command Output	connect
	Press Keys	//body	RETURN
	${OUTPUT}=  GUI:Access::Generic Console Command Output	connect
	Press Keys	//body	RETURN
	${OUTPUT}=  GUI:Access::Generic Console Command Output	connect
	# Type ls command in the output terminal
	${OUTPUT}=  GUI:Access::Generic Console Command Output	ls
	# Output should contain the folders when we do ls
	Should Contain  ${OUTPUT}	access/	system/	settings/
	switch window
	Close Browser


SUITE:Accessing Device through User
	[Tags]	NON-CRITICAL	BUG_NG_9920
		[Documentation]		Accessing device through user
	GUI::Basic::Open And Login Nodegrid		${USER}		${PASS}
	# Test that the device is visible but read only for the User meaning it should not be clickable
	wait until element is visible	//*[@id='|${DEVICE}']/td[1]/div/a/span
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on the device through console as User
	double click element	//*[@id='|${DEVICE}']/td[1]/div/a/span
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	# Verifying the console button on the pop up
	Element Should Be Enabled	//*[@id="|${DEVICE}"]/td[2]/div/a[1]
	GUI::Basic::Spinner Should Be Invisible
	# Cancelling the pop up for the device.
	click button	//*[@id="modal"]/div/div/div[1]/button
	GUI::Basic::Spinner Should Be Invisible
	# Checking for read-write permissions clicking on console button as user
	GUI:Access::Table::Access Device Console	${DEVICE}
	# Press the enter keys
	Press Keys	//body	RETURN
	Press Keys	//body	RETURN
	${OUTPUT}=		GUI:Access::Generic Console Command Output		connect
	Press Keys	//body	RETURN
	${OUTPUT}=		GUI:Access::Generic Console Command Output		connect
	Press Keys	//body	RETURN
	${OUTPUT}=		GUI:Access::Generic Console Command Output		connect
	# Type ls command in the terminal
	${OUTPUT}=  GUI:Access::Generic Console Command Output	ls
	# Output should contain folders when we hit ls
	Should Contain  ${OUTPUT}	access/	system/	settings/
	switch window
	Close Browser

SUITE:Set back the device permissions
	[Documentation]		Setting back the device permissions
	GUI::Basic::Security::Authorization::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	click link		//*[@id="user"]
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on Devices in the sub menu
	Click Element	//*[@id="spm"]/span
	GUI::Basic::Spinner Should Be Invisible
	#Clicking on other devices
	click link		//*[@id="OTHER DEVICES"]
	Sleep  5s
	# Handling Permissions
	select radio button		rSessionMode  NoAccess
	Sleep	5s
	# Clicking on save button
	double click element	//*[@id='saveButton']
	sleep	5s
	handle alert

SUITE:Teardown
	[Documentation]		Setting all permissions back
	# Open and login nodegrid application
	GUI::Basic::Open And Login Nodegrid
	# Adding the device permissions
	SUITE:Adding Devices Permissions
	# Delete if the user group exists
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	# Delete if the user exists
	GUI::Security::Local Accounts::Delete	${USER}
	# Delete the console device if exists
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE}
	# Setting back the device permissions
	SUITE:Set back the device permissions
	# Logout of nodegrid and close the browser
	GUI::Basic::Logout And Close Nodegrid