*** Settings ***
Documentation	Manage Devices Adding Permission
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup

*** Variables ***
${USER}		test_user
${PASS}		test_user
${GROUP}	test_group
${DEVICE}	test-dev.ice

*** Test Cases ***
Test Enter In Root Mode In Console With Sudo Permissions
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Manage All Devices Permissions

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid

SUITE:Manage All Devices Permissions
	GUI::Security::Local Accounts::Add	${USER}
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	SUITE:Adding Devices Permissions
	SUITE:Accessing Other Devices
	SUITE:Accessing Device through Console
	SUITE:Accessing Device through User
	SUITE:Teardown

SUITE:Adding Devices Permissions
	# Adding Device Permissions through Security->Services->Clicking on Devices access Enforced via user group Authorization.
	GUI::Security::Open Services tab	${USER}
	# Click on authorization subtab
	Click Element	//input[@id='authorization']
	# Click on save button
	GUI::Basic::Save


SUITE:Accessing Other Devices
	 # Adding Device to a specific Group
	# Clicking on Security -> Authorization tab
	GUI::Basic::Security::Authorization::Open Tab
	# Clicking on add device and adding a console device
	GUI::ManagedDevices::Add Device	${DEVICE}
	# Click on Authorization tab
	GUI::Security::Open Authorization tab
	# Clicking on user link
	Click Link	//a[@id='user']
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on the Devices from sub menu
	Click Element	//span[normalize-space()='Devices']
	GUI::Basic::Spinner Should Be Invisible
	# Click on Add button from Devices
	GUI::Basic::Add
	# Select the PDU device that got added by double clicking it
	double click element	//option[@value='${DEVICE}']
	sleep	5s
	# Click on Save button
	double click element	//*[@id='saveButton']
	#GUI::Basic::Save
	handle alert
	sleep	5s
	#Clicking on other devices
	Click Link	//a[@id='OTHER DEVICES']
	GUI::Basic::Spinner Should Be Invisible
	# Handling Permissions
	select radio button		rSessionMode	ReadOnly
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on save button
	# GUI::Basic::Click Element Accept Alert		Save
	double click element	//*[@id='saveButton']
	sleep	5s
	handle alert


SUITE:Accessing Device through Console
	# Clicking on Access and opening Table tab
	GUI::Basic::Access::Table::Open Tab
	# Clicking on the device through console as Admin
	Click element	(//span[contains(text(),'${DEVICE}')])[1]
	GUI::Basic::Spinner Should Be Invisible
	# Verifying the console button on the pop up
	Element Should Be Enabled	//a[normalize-space()='Console']
	GUI::Basic::Spinner Should Be Invisible
	# Cancelling the pop up for the device.
	close browser


SUITE:Accessing Device through User
	# Accessing the device through User
	GUI::Basic::Open And Login Nodegrid		${USER}		${PASS}
	# Test that the device is visible but read only for the User meaning it should not be clickable
	wait until element is visible	xpath=//*[@id='|${DEVICE}']/td[1]/div/a/span
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on the device through console as User
	Click element	(//span[contains(text(),'${DEVICE}')])[1]
	Sleep	5s
	# Verifying the console button on the pop up
	Element Should Be Enabled	//a[normalize-space()='Console']
	Sleep	5s
	# Cancelling the pop up for the device.
	close browser

SUITE:Set back the device permissions
	# Clicking on user group
	GUI::Basic::Security::Authorization::Open Tab
	# Clicking on user link
	Click Link	//a[@id='user']
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on the Devices from sub menu
	Click Element	//span[normalize-space()='Devices']
	GUI::Basic::Spinner Should Be Invisible
	#Clicking on other devices
	Click Link	//a[@id='OTHER DEVICES']
	GUI::Basic::Spinner Should Be Invisible
	# Handling Permissions
	select radio button		rSessionMode	NoAccess
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on save button
	double click element	//*[@id='saveButton']
	sleep	5s
	handle alert


SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	SUITE:Adding Devices Permissions
	# Delete the User group
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	# Delete the user from local accounts
	GUI::Security::Local Accounts::Delete	${USER}
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	GUI::Security::Local Accounts::Delete	${USER}
	# Delete the device from Manage devices tab
	GUI::ManagedDevices::Delete Device If Exists  ${DEVICE}
	# Setting back the device permissions to be unchecked
	SUITE:Set back the device permissions
	# Logout and close nodegrid
	GUI::Basic::Logout And Close Nodegrid