*** Settings ***
Resource	../../init.robot
Documentation	Test entering in root mode on console with/without sudo permissions
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USER}	test_user
${SUDO_USER}	test_sudo_user
${GROUP}	test_group
${SUDO_GROUP}	test_sudo_group

*** Test Cases ***
Test Enter In Root Mode In Console With No Sudo Permissions
	[Tags]	NON-CRITICAL	NEED-REVIEW	BUG_NG_9567
	SUITE:Add User Group With No Sudo Permissions And Assign User To It
	SUITE:Entering Sudo Su On Console With User Should Fail	${USER}	${USER}

Test Enter In Root Mode In Console With Sudo Permissions
	[Tags]	NON-CRITICAL	NEED-REVIEW	BUG_NG_9567
	SUITE:Add User Group With Sudo Permissions And Assign User To It
	SUITE:Entering Sudo Su On Console With Sudo User Should Succeed	${SUDO_USER}	${SUDO_USER}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open NodeGrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	GUI::Security::Local Accounts::Delete	${USER}
	GUI::Security::Authorization::Delete User Group If Exists	${SUDO_GROUP}
	GUI::Security::Local Accounts::Delete	${SUDO_USER}
	GUI::Basic::Logout And Close Nodegrid

SUITE:Add User Group With Sudo Permissions And Assign User To It
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Local Accounts::Add	${SUDO_USER}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Authorization::Delete User Group If Exists	${SUDO_GROUP}
	GUI::Security::Authorization::Add User Group	${SUDO_GROUP}
	SUITE:Add Shell and Sudo Permissions To User Group
	GUI::Security::Authorization::Add User To User Group	${SUDO_USER}	${SUDO_GROUP}
	GUI::Basic::Logout

SUITE:Add User Group With No Sudo Permissions And Assign User To It
	GUI::Basic::Open And Login Nodegrid
	GUI::Security::Local Accounts::Add	${USER}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	GUI::Security::Authorization::Add User Group	${GROUP}
	SUITE:Add Shell Permissions To User Group
	GUI::Security::Authorization::Add User To User Group	${USER}	${GROUP}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout

SUITE:Add Shell Permissions To User Group
	GUI::Security::Authorization::Open User Group Profile Tab	${GROUP}
	Double Click Element	//select[@class='selectbox groupFrom']//option[@value="shell"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Add Shell and Sudo Permissions To User Group
	GUI::Security::Authorization::Open User Group Profile Tab	${SUDO_GROUP}
	Double Click Element	//select[@class='selectbox groupFrom']//option[@value="shell"]
	Select Checkbox	//input[@id="sudo_permission"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Entering Sudo Su On Console With User Should Fail
	[Arguments]	${USERNAME}	${PASSWORD}
	${MAIN_WINDOW_TITLE}=	Get Title
	GUI::Basic::Login	${NGVERSION}	${USERNAME}	${PASSWORD}
	GUI::Access::Table::Open Console
	Sleep	5

	Switch Window	title=nodegrid
	Wait Until Page Contains Element	//iframe[@id='termwindow']
	Select Frame	//iframe[@id='termwindow']
	Sleep	5
	Press Keys	None	RETURN
	Press Keys	None	RETURN

	${OUTPUT}=	SUITE:Console Command Output	TAB+TAB
	Should Contain	${OUTPUT}	shell	whoami

	${OUTPUT}=	SUITE:Console Command Output	whoami
	Should Contain	${OUTPUT}	${USER}

	${OUTPUT}=	SUITE:Console Command Output	shell
	Should Not Contain	${OUTPUT}	Error

	${OUTPUT}=	SUITE:Console Command Output	whoami
	Should Contain	${OUTPUT}	${USER}

	${OUTPUT}=	SUITE:Console Command Output	sudo su -
	Should Contain	${OUTPUT}	Password:

	# Enters password, which is the same as the user
	${OUTPUT}=	SUITE:Console Command Output	${USER}
	Should Contain	${OUTPUT}	${USER} is not in the sudoers file.	This incident will be reported.

	Unselect Frame
	Switch Window	title=nodegrid
	Close Window
	Switch Window	${MAIN_WINDOW_TITLE}
	GUI::Basic::Logout

SUITE:Entering Sudo Su On Console With Sudo User Should Succeed
	[Arguments]	${USERNAME}	${PASSWORD}
	${MAIN_WINDOW_TITLE}=	Get Title
	GUI::Basic::Login	${NGVERSION}	${USERNAME}	${PASSWORD}
	GUI::Access::Table::Open Console
	Sleep	5

	Switch Window	title=nodegrid
	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
	Select Frame	xpath=//*[@id='termwindow']
	Sleep	5
	Press Keys	None	RETURN
	Press Keys	None	RETURN

	${OUTPUT}=	SUITE:Console Command Output	TAB+TAB
	Should Contain	${OUTPUT}	shell

	${OUTPUT}=	SUITE:Console Command Output	shell
	Should Not Contain	${OUTPUT}	Error

	${OUTPUT}=	SUITE:Console Command Output	whoami
	Should Contain	${OUTPUT}	${SUDO_USER}

	${OUTPUT}=	SUITE:Console Command Output	sudo su -
	Should Not Contain	${OUTPUT}	Error

	${OUTPUT}=	SUITE:Console Command Output	whoami
	Should Contain	${OUTPUT}	root

	Unselect Frame
	Switch Window	title=nodegrid
	Close Window
	Switch Window	${MAIN_WINDOW_TITLE}
	GUI::Basic::Logout

SUITE:Console Command Output
	[Arguments]	${COMMAND}
	[Documentation]	Input the given 3 command into the ttyd terminal and retrieve the output after prompt lines
	...	== ARGUMENTS ==
	...	-	COMMAND = Command to be executed
	...	== EXPECTED RESULT ==
	...	Input the command into the ttyd terminal and returns the OUTPUT between the command and the last prompt

	${CHECK_TAB}=	Run Keyword And Return Status	Should Not Contain	${COMMAND}	TAB
	Run Keyword If	${CHECK_TAB}	Press Keys	None	${COMMAND}	RETURN
	...	ELSE	Press Keys	None	${COMMAND}
	Sleep	1

	${CONSOLE_CONTENT}=	Execute JavaScript	term.selectAll(); return term.getSelection().trim();
	Should Not Contain	${CONSOLE_CONTENT}	[error.connection.failure] Could not establish a connection to device
	${OUTPUT}=	Output From Last Command	${CONSOLE_CONTENT}
	[Return]	${OUTPUT}