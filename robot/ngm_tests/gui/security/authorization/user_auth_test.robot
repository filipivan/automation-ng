*** Settings ***
Documentation	Test Security:Authorization - Admin Fields
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Drilldown User
	Element Should Be Visible	jquery=#addButton
	GUI::Security::Authorization::User::Open User tab
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	jquery=#agMember_nav

Check Users
	Select Checkbox		xpath=//thead[@id='thead']/tr/th/input
	${IS}=	run keyword and return status	Element Should Be Disabled	jquery=#delButton
	Run Keyword Unless	${IS}	Click Element	//*[@id='delButton']
	GUI::Basic::Spinner Should Be Invisible

Check Member Subtab
	Click element	jquery=#addButton
	GUI::Basic::Spinner Should Be Invisible
	Input Text	jquery=#memberRemote	temps
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element		xpath=//tr[@id='temps']
	Click Element	xpath=//tr[@id='temps']/td/input
	Click element	//*[@id='delButton']
	GUI::Basic::Spinner Should Be Invisible
	Page Should Not Contain Element		xpath=//tr[@id='temps']

Check Profile Subtab
	GUI::Security::Authorization::User::Open Profile tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	jquery=#permissionList
	Select Checkbox	jquery=#custom_session_timeout
	GUI::Basic::Spinner Should Be Invisible
	GUI::Auditing::Auto Input Tests	custom_timeout	${EMPTY}	str	98.7	12:30	999
	Input Text	jquery=#custom_timeout	300
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	Unselect Checkbox	jquery=#custom_session_timeout
	GUI::Basic::Spinner Should Be Invisible
	Click element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

Check Remote Groups
	GUI::Security::Authorization::User::Open Remote Groups tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	jquery=#remoteGroup

Check Devices
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Security::Authorization::User::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	jquery=#agasSpm_Table
	Click Element	jquery=#addButton
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Check Page Contain List	tMKS	tKVM	tReset	tSPConsole	tVirtualmedia	tAccessLogAudit	tAccessLogClear
	...	tEventLogAudit	tEventLogClear	tSensorInfo	tMonitoring	tCustomCommands	capNotice	tarAccessRights
	Click Element	xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[1]/div[1]/select/option
	Click Element	xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[1]/div[2]/div/div[1]/button
	Click Element	jquery=#saveButton
	Handle Alert
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	xpath=//*[@id="tbody"]/tr
	Click Element	xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[1]/input
	Click Element	jquery=#delButton
	GUI::Basic::Spinner Should Be Invisible

Check Outlets
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Security::Authorization::User::Open Outlets tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	jquery=#agasOutlet_Table
	Click Element	jquery=#addButton
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	jquery=#targetSelection

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add Missing Device
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Authorization::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::ManagedDevices::Delete Device If Exists	aaaa
	GUI::Basic::Logout
	Close all Browsers

SUITE:Add Missing Device
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	${count}=	GUI::Basic::Count Table Rows	SPMTable
	Run Keyword If	${count} > 0	Log	"Device already exists"
	Run Keyword If	${count} == 0	SUITE:AMD
	GUI::Basic::Spinner Should Be Invisible

SUITE:AMD
	Wait Until Element Is Visible	jquery=#addButton
	Click Element	jquery=#addButton
	Wait Until Element Is Visible	jquery=#cancelButton
	Input Text	xpath=//input[@id="spm_name"]	aaaa
	Wait Until Element Is Visible	jquery=#saveButton
	Select From List By Value	type	device_console
	Wait Until element Is Visible	jquery=#saveButton
	Click Element	jquery=#saveButton

SUITE:Check Page Contain List
	[Arguments]	@{LST}
	FOR	${I}	IN	@{LST}
		Page Should Contain Element	${I}
	END