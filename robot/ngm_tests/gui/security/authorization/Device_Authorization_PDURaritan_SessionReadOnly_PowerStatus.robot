*** Settings ***
Documentation	Manage Devices Adding Permission
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS		CHROME		FIREFOX
Default Tags	GUI

Suite Setup	SUITE:Setup

*** Variables ***
${USER}		test_user
${PASS}		test_user
${SUDO_USER}	test_sudo_user
${GROUP}	test_group
${SUDO_GROUP}	test_sudo_group
${PDU_DEVICE}	test_raritan
${DEVICE_TYPE}	pdu_raritan
${DEVICE_IP}	192.168.3.7
${DEVICE_USER}	admin
${DEVICE_ASKPASS}	no
${DEVICE_PWD}	admin
${PDU_ID}	My-PDU
${OUTLET_ID1}	1
${OUTLET1_NAME}		Outlet_1
${TEST_DEVICE}		test_device
${TEST_DEVICE_TYPE}		device_console
${COMMAND}		Outlet

*** Test Cases ***
Test Enter In Root Mode In Console With Sudo Permissions
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Manage All Devices Permissions

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid

SUITE:Manage All Devices Permissions
	GUI::Security::Local Accounts::Add	${USER}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Adding Devices Permissions
	SUITE:Add pdu device/protocol/SNMPV2
	SUITE:Auto discovery and check outlets discovered on outlets page
	SUITE:Accessing Other Devices
	SUITE:Accessing Device through Console
	SUITE:Accessing Device through User
	SUITE:Teardown

SUITE:Adding Devices Permissions
	[Documentation]		Adding Device Permissions through Security->Services->Clicking on Devices access Enforced via user group Authorization.
	GUI::Security::Open Services tab	${USER}
	Click Element	xpath=//*[@id="authorization"]
	GUI::Basic::Save


SUITE:Accessing Other Devices
	[Documentation]		Adding Device to a specific Group
	GUI::Security::Open Authorization tab
	GUI::Basic::Spinner Should Be Invisible
	click link		//*[@id="user"]
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on the group and adding device
	Click Element	//*[@id="spm"]/span
	GUI::Basic::Spinner Should Be Invisible
	click element	//*[@id="addButton"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	double click element	//*[@id="targetSelection"]/div/div[1]/div[1]/select/option[@value="${PDU_DEVICE}"]
	click element	//*[@id="saveButton"]
	handle alert
	GUI::Basic::Spinner Should Be Invisible
	#Clicking on PDU device
	click link		//*[@id="${PDU_DEVICE}"]
	Sleep	5s
	# Handling Permissions for added device Read Only and Power status
	select radio button		rSessionMode	ReadOnly
	select radio button		rPowerMode		PowerStatus
	Sleep	5s
	# Clicking on save button
	double click element	//*[@id='saveButton']
	sleep	5s
	handle alert

SUITE:Add pdu device/protocol/SNMPV2
	[Documentation]		Adding a PDU device
	GUI::ManagedDevices::Add Device		${PDU_DEVICE}	${DEVICE_TYPE}	${DEVICE_IP}	${DEVICE_USER}	${DEVICE_ASKPASS}	${DEVICE_PWD}
	GUI::Basic::Spinner Should Be Invisible
	# Click link with the device that just got added
	Click Link	${PDU_DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	# Click Management sub tab
	Click Element	//*[@id="spmmgmt_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	# Select the SNMP version 2 radio button
	Select Checkbox		//*[@id="snmp_proto_enabled"]
	# Enter text as private into Community
	Input Text		//*[@id="community2"]	private
	# Click on save button
	GUI::Basic::Save
	# Click on Command sub menu
	Click Element	//*[@id="spmcommands_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	# Click on Outlet check box
	Click Element	//*[@id="Outlet"]
	GUI::Basic::Spinner Should Be Invisible
	# Select SNMP as the Protocol from the list
	Select From List By Label	//*[@id="protocol"]	SNMP
	# Click on Save button
	GUI::Basic::Save


SUITE:Auto discovery and check outlets discovered on outlets page
	[Documentation]		Click on Manage Devices -> Auto Discovery sub tab
	GUI::Basic::Managed Devices::Auto Discovery::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	# Click on Discover Now Sub tab
	Click Element	//*[@id="discovery_now"]
	GUI::Basic::Spinner Should Be Invisible
	# Verify that Discover now button is disabled
	Element Should Be Disabled		//*[@id="discovernow"]
	Page Should Contain Element		//*[@id="discovernowTable"]
	# Select the checkbox for the PDU device added
	Select Checkbox		//*[@id="pdu|${PDU_DEVICE}"]/td[1]/input
	# Once the discover now button is enabled click on Discover now
	Click Button	//*[@id="discovernow"]
	GUI::Basic::Spinner Should Be Invisible
	# Click on Manage Devices -> Devices tab
	GUI::Basic::Managed Devices::Devices::open tab
	Sleep	15s
	# Click on the PDU device that got added
	Click Link		${PDU_DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	# Click on Outlets sub tab
	Click Element	//*[@id="spmoutlets_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	# Verify that outlet name appears as expected
	Page Should Contain		${OUTLET1_NAME}



SUITE:Accessing Device through Console
	[Documentation]		Click on Access ->Table tab -> Acccess device through console
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	# Click on the PDU device that appears on Table tab.
	Click Element	//*[@id="|${PDU_DEVICE}"]/td[1]/div/a/span
	GUI::Basic::Spinner Should Be Invisible
	# Wait until the outlets appear
	wait until page contains element	//*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]
	# Select the checkbox for the outlet that appears
	Select Checkbox		//tr[@id='${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}']/td[1]/input
	# Click on Outlet Off button
	Click Button	//*[@id="nonAccessControls"]/input[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	# Verify that page should not contain any errors as we have power status
	page should not contain		Error on page. Please check.
	Sleep	5s
	# Click on cancel button on the top right corner
	click button	//*[@id="modal"]/div/div/div[1]/button
	Sleep	5s
	# Click on console button for PDU device appearing under Access-> Table tab
	GUI:Access::Table::Access Device Console	${PDU_DEVICE}
	Sleep	5s
	# Click on Enter keys
	Press Keys		//body	RETURN
	${OUTPUT}=	GUI:Access::Generic Console Command Output	connect
	Press Keys		//body	RETURN
	# Write a command for example "exit"
	${OUTPUT}=	GUI:Access::Generic Console Command Output	exit
	Sleep	5s
	close browser

SUITE:Accessing Device through User
	[Documentation]		Accessing the device through User
	GUI::Basic::Open And Login Nodegrid		${USER}		${PASS}
	# Click on Access-> Open tab
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	# Click on the PDU device that is visible for the user
	Click Element	//*[@id="|${PDU_DEVICE}"]/td[1]/div/a/span
	GUI::Basic::Spinner Should Be Invisible
	# Wait until the page contains all the buttons
	Wait Until Page Contains Element	css=.btn:nth-child(4)
	# Wait until the page contains outlet names
	wait until page contains element	//*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]
	# Select the check box for any of the outlet  that appears in the table.
	Select Checkbox		//tr[@id='${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}']/td[1]/input
	 # Click on Outlet Off button
	Click Button	//*[@id="nonAccessControls"]/input[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	# Verify that page should contain "Error on page.please check" because the user has read only permissions
	page should contain		Error on page. Please check.
	Sleep	5s
	# Click on Cancel button on the top right corner
	click button	//*[@id="modal"]/div/div/div[1]/button
	Sleep	5s
	# Click on Access -> Table tab -> PDU console button
	GUI:Access::Table::Access Device Console	${PDU_DEVICE}
	${OUTPUT}=	GUI:Access::Generic Console Command Output	connect
	# Click on Enter Keys
	Press Keys	//body	RETURN
	#Should Contain  ${OUTPUT}	read-only -- use
	Should Contain  ${OUTPUT}	[read-only -- use ^E c ? for help]
	close browser


SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	SUITE:Adding Devices Permissions
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	GUI::Security::Local Accounts::Delete	${USER}
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	GUI::Security::Local Accounts::Delete	${USER}
	GUI::ManagedDevices::Delete Device If Exists	${PDU_DEVICE}
	GUI::Basic::Logout And Close Nodegrid