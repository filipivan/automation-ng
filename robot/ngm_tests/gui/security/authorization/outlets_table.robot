#*** Settings ***
#Documentation	Test the outlet table presentation
#Metadata	Version	1.0
#Metadata	Executed At	${HOST}
#Metadata	Executed With	${BROWSER}
#Resource	../../init.robot
#
#Suite Setup     Setup
#Suite Teardown  Teardown
#Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
#Default Tags	EXCLUDEIN3_2    EXCLUDEIN4_1
#
#*** Test Cases ***
#Check if "admin" user group has access to all outlets
#	GUI::Security::Drilldown User Group	admin
#	GUI::Security::Open User Group Outlet Tab
#	Table Cell Should Contain	jquery=#agasOutlet_Table	2	1	ALL
#	Table Cell Should Contain	jquery=#agasOutlet_Table	2	4	Control
#
#
#Check if "user" user group has access to some outlet
#	GUI::Security::Drilldown User Group	user
#	GUI::Security::Open User Group Outlet Tab
#	GUI::Basic::Delete All Rows In Table If Exists	\#agasOutlet_Table	False
#	#adds an outlet to the user
#	GUI::Basic::Add
#	${INDEXES}=	Create List	0	1
#	GUI::Basic::Select Listbuilder Values By Index	\#targetSelection	${INDEXES}
#	${OUTLETS}=	GUI::Basic::Get Selected Listbuilder Items	\#targetSelection
#	GUI::Basic::Save    True
#	FOR	$2_{outlet}	IN	@{OUTLETS}
#		Check If Outlet Is Formatted Right	$2_{outlet}
#	END
#
#*** Keywords ***
#Setup
#	${HAS_PDU_INFO}=	Run Keyword And Return Status	Variable Should Not Exist	${PDU}
#	Run Keyword If	${HAS_PDU_INFO}	Fail	Missing PDU data	WARN
#	GUI::Basic::Open And Login Nodegrid
#	Add PDU
#	GUI::Security::Open Authorization tab
#	Table Column Should Contain	jquery=#author_groupsTable	2	admin
#	Table Column Should Contain	jquery=#author_groupsTable	2	user
#
#Teardown
#	Delete PDU
#	GUI::Basic::Logout
#	Close all Browsers
#
#Add PDU
#	GUI::ManagedDevices::Add Device If Not Exists	 &{PDU}[name]	 &{PDU}[type]	 &{PDU}[ip]	 &{PDU}[username]	 no 	 &{PDU}[password]	 enabled
#	#Launch the outlet discovery
#	${DISCOVER}=	Create List	pdu|&{PDU}[name]
#	GUI::ManagedDevices::Launch Discovery By Id	${DISCOVER}
#	GUI::ManagedDevices::Wait For Outlet Discovery	&{PDU}[name]
#
#Delete PDU
#	${HAS_PDU_INFO}=	Run Keyword And Return Status	Variable Should Exist	${PDU}
#	Run Keyword If	${HAS_PDU_INFO}	GUI::ManagedDevices::Delete Devices	&{PDU}[name]
#
#Check If Outlet Is Formatted Right
#	[Arguments]	${OUTLET}	${SCREENSHOT}=FALSE
#	[Documentation]	Check if the given outlet is formatted correctly in the outlet table
#	GUI::Basic::Table Should Has Row With Id	agasOutlet_Table	${OUTLET}
#	@{infos}=	Split String	${OUTLET}	:
#	FOR	${info}	IN	@{infos}
#		Table Should Contain	agasOutlet_Table	${info}
#	END
#
