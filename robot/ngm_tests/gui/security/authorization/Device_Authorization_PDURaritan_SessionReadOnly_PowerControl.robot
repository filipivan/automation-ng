*** Settings ***
Documentation	Manage Devices Adding Permission
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS		CHROME		FIREFOX
Default Tags	GUI

Suite Setup	SUITE:Setup

*** Variables ***
${USER}		test_user
${PASS}		test_user
${SUDO_USER}	test_sudo_user
${GROUP}	test_group
${SUDO_GROUP}	test_sudo_group
${PDU_DEVICE}	test_raritan
${DEVICE_TYPE}	pdu_raritan
${DEVICE_IP}	192.168.3.7
${DEVICE_USER}	admin
${DEVICE_ASKPASS}	no
${DEVICE_PWD}	admin
${PDU_ID}	My-PDU
${OUTLET_ID1}	1
${OUTLET1_NAME}		Outlet_1
${TEST_DEVICE}		test_device
${TEST_DEVICE_TYPE}		device_console
${COMMAND}		Outlet


*** Test Cases ***
Test Enter In Root Mode In Console With Sudo Permissions
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Manage All Devices Permissions

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid

SUITE:Manage All Devices Permissions
	GUI::Security::Local Accounts::Add	${USER}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Adding Devices Permissions
	SUITE:Add pdu device/protocol/SNMPV2
	SUITE:Auto discovery and check outlets discovered on outlets page
	SUITE:Accessing Other Devices
	SUITE:Accessing Device through Console
	SUITE:Accessing Device through User
	SUITE:Teardown

SUITE:Adding Devices Permissions
	[Documentation]		Adding Device Permissions through Security->Services->Clicking on Devices access Enforced via user group Authorization.
	GUI::Security::Open Services tab	${USER}
	Click Element	xpath=//*[@id="authorization"]
	GUI::Basic::Save


SUITE:Accessing Other Devices
	[Documentation]		Adding Device to a specific Group
	GUI::Security::Open Authorization tab
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on User link from Local Accounts
	click link		//*[@id="user"]
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on the devices sub menu
	Click Element	//*[@id="spm"]/span
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on Add button from devices
	click element	//*[@id="addButton"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep  5s
	# Select the device by double clicking it
	double click element	//*[@id="targetSelection"]/div/div[1]/div[1]/select/option[@value="${PDU_DEVICE}"]
	click element	//*[@id="saveButton"]
	handle alert
	GUI::Basic::Spinner Should Be Invisible
	#Clicking on PDU device
	click link		//*[@id="${PDU_DEVICE}"]
	Sleep	5s
	# Handling Permissions Read only and Power control for added device
	select radio button		rSessionMode	ReadOnly
	select radio button		rPowerMode	PowerControl
	Sleep	5s
	# Clicking on save button
	double click element	//*[@id='saveButton']
	sleep	5s
	handle alert

SUITE:Add pdu device/protocol/SNMPV2
	[Documentation]		Adding a PDU device
	GUI::ManagedDevices::Add Device		${PDU_DEVICE}	${DEVICE_TYPE}	${DEVICE_IP}	${DEVICE_USER}	${DEVICE_ASKPASS}	${DEVICE_PWD}
	GUI::Basic::Spinner Should Be Invisible
	# Click link with the device that just got added
	Click Link		${PDU_DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	# Click Management sub tab
	Click Element	//*[@id="spmmgmt_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	# Select the SNMP version 2 radio button
	Select Checkbox		//*[@id="snmp_proto_enabled"]
	# Enter text as private into Community
	Input Text	//*[@id="community2"]    private
	# Click on save button
	GUI::Basic::Save
	# Click on Command sub menu
	Click Element	//*[@id="spmcommands_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	# Click on Outlet check box
	Click Element	//*[@id="Outlet"]
	GUI::Basic::Spinner Should Be Invisible
	# Select SNMP as the Protocol from the list
	Select From List By Label	//*[@id="protocol"]	SNMP
	# Click on Save button
	GUI::Basic::Save


SUITE:Auto discovery and check outlets discovered on outlets page
	[Tags]	NON-CRITICAL
	[Documentation]		Click on Manage Devices -> Auto Discovery sub tab -> Discovering the outlets
	GUI::Basic::Managed Devices::Auto Discovery::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	# Click on Discover Now Sub tab
	Click Element	//*[@id="discovery_now"]
	GUI::Basic::Spinner Should Be Invisible
	# Verify that Discover now button is disabled
	Element Should Be Disabled		//*[@id="discovernow"]
	Page Should Contain Element		//*[@id="discovernowTable"]
	# Select the checkbox for the PDU device added
	Select Checkbox		//*[@id="pdu|${PDU_DEVICE}"]/td[1]/input
	# Once the discover now button is enabled click on Discover now
	Click Button	//*[@id="discovernow"]
	GUI::Basic::Spinner Should Be Invisible
	# Click on Manage Devices -> Devices tab
	GUI::Basic::Managed Devices::Devices::open tab
	Sleep	15s
	# Click on the PDU device that got added
	Click Link	${PDU_DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	# Click on Outlets sub tab
	Click Element	//*[@id="spmoutlets_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	# Verify that outlet name appears as expected
	Page Should Contain		${OUTLET1_NAME}



SUITE:Accessing Device through Console
	[Documentation]		Clicking on Access->Table tab
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	# Click on the added PDU device from table tab
	Click Element	//*[@id="|${PDU_DEVICE}"]/td[1]/div/a/span
	GUI::Basic::Spinner Should Be Invisible
	# Wait until the pop up contains all the buttons
	Wait Until Page Contains Element	css=.btn:nth-child(4)
	# Click on the Outlet Status button
	Click Button	css=.btn:nth-child(4)
	GUI::Basic::Spinner Should Be Invisible
	# Verify that Page should not contain unknown
	Page Should not contain		unknown
	# Wait until the page contains PDU outlets
	wait until page contains element	//*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]
	# Select the checkbox for the first PDU outlet appearing
	Select Checkbox		//tr[@id='${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}']/td[1]/input
	# Click on Outlet Off button
	Click Button	//*[@id="nonAccessControls"]/input[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	# Wait until the outlet shows off state
	Wait Until Element Contains		//*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]		Off
    # Click on cancel button on the top right corner
	click button	//*[@id="modal"]/div/div/div[1]/button
	Sleep	5s
	# Clicn on Access->Table -> Console button of PDU
	GUI:Access::Table::Access Device Console	${PDU_DEVICE}
	# Click on Enter keys
	Press Keys		//body	RETURN
	${OUTPUT}=	GUI:Access::Generic Console Command Output	connect
	Press Keys		//body	RETURN
	# Enter the command "exit" on console
	${OUTPUT}=	GUI:Access::Generic Console Command Output	exit
	Sleep	5s
	close browser





SUITE:Accessing Device through User
	[Documentation]		Accessing the device through User
	GUI::Basic::Open And Login Nodegrid 	${USER}	${PASS}
	# Click on Access->Table
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on PDU device appearing on access tab.
	Click Element	//*[@id="|${PDU_DEVICE}"]/td[1]/div/a/span
	GUI::Basic::Spinner Should Be Invisible
	# Wait until the page contains all the buttons
	Wait Until Page Contains Element	css=.btn:nth-child(4)
	# Click on Outlet Status button
	Click Button	css=.btn:nth-child(4)
	GUI::Basic::Spinner Should Be Invisible
	# Verify that page should not contain unknown messages
	Page Should not contain	unknown
	# Wait until the page contains the outlets showing up
	wait until page contains element	//*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]
	# Select the checkbox for any of the outlet available
	Select Checkbox		//tr[@id='${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}']/td[1]/input
	# Click on Outlet off button
	Click Button	//*[@id="nonAccessControls"]/input[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	# Wait until the page shows up that the outlet is in Off state
	Wait Until Element Contains		//*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]	Off
	# Click on cancel button on the top right corner
	click button	//*[@id="modal"]/div/div/div[1]/button
	Sleep	5s
	# Click on Access->Table ->Console button of PDU
	GUI:Access::Table::Access Device Console	${PDU_DEVICE}
	${OUTPUT}=	GUI:Access::Generic Console Command Output	connect
	# Click on Enter Keys
	Press Keys	//body	RETURN
	#Should Contain  ${OUTPUT}	read-only -- use
	Should Contain  ${OUTPUT}	[read-only -- use ^E c ? for help]
	close browser


SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	# Removing the device permissions which was added already
	SUITE:Adding Devices Permissions
	# Removing the user group if it exists already
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	# Removing the user if it exists
	GUI::Security::Local Accounts::Delete	${USER}
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	GUI::Security::Local Accounts::Delete	${USER}
	# Removing the devices that exists
	GUI::ManagedDevices::Delete Device If Exists  ${PDU_DEVICE}
	# Logout anc close the browser
	GUI::Basic::Logout And Close Nodegrid