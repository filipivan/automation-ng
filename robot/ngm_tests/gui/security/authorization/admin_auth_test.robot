*** Settings ***
Documentation	Test Security:Authorization - Admin Fields
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

*** Test Cases ***
Drilldown Admin
	Element Should Be Visible   jquery=#addButton
	Click Element   xpath=(//a[contains(text(),'admin')])[2]
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible   jquery=#agMember_nav

Check Member Subtab
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text      jquery=#memberRemote    temps
	Click Element   jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element     xpath=//td[contains(.,'temps')]
	Click Element   xpath=//tr[@id='temps']/td/input
	Click element   jquery=#delButton
	GUI::Basic::Spinner Should Be Invisible
	Page Should Not Contain Element     xpath=//td[contains(.,'temps')]

Check Profile Subtab
	Click Element		xpath=//span[contains(.,'Profile')]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element     jquery=#restrictConfsys
	Page Should Contain Element     jquery=#enablelogprof
	Page Should Contain Element     jquery=#group_emailto
	Select Radio Button     start_appl  start_appl_cli
	Select Radio Button     start_appl  start_appl_shell
	Select Radio Button     start_appl  start_appl_cli
	Select Checkbox     jquery=#custom_session_timeout
	GUI::Basic::Spinner Should Be Invisible
	GUI::Auditing::Auto Input Tests     custom_timeout      ${EMPTY}    str     98.7    12:30   999
	Input Text      jquery=#custom_timeout  300
	Click Element   jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	Unselect Checkbox   jquery=#custom_session_timeout
	GUI::Basic::Spinner Should Be Invisible
	Click element   jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

Check Remote Groups
	Click Element		xpath=//span[contains(.,'Remote Groups')]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element     jquery=#remoteGroup

Check Devices
	GUI::Security::Authorization::admin::Open Device Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element     jquery=#agasSpm_Table

Check Outlets
	GUI::Security::Authorization::admin::Open Outlets tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element     jquery=#agasOutlet_Table

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Authorization::Open Tab
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
