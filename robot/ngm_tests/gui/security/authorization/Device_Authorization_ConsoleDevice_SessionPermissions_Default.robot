*** Settings ***
Documentation	Manage Devices Default Permissions
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup

*** Variables ***
${USER}		test_user
${PASS}		test_user
${GROUP}	test_group
${DEVICE}	test-dev.ice

*** Test Cases ***
Test Enter In Root Mode In Console With Sudo Permissions
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Manage All Devices Permissions

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid

SUITE:Manage All Devices Permissions
	GUI::Security::Local Accounts::Add	${USER}
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	SUITE:Adding Devices Permissions
	SUITE:Accessing Other Devices
	SUITE:Accessing Device through Console
	SUITE:Accessing Device through User
	SUITE:Teardown

SUITE:Adding Devices Permissions
	# Adding Device Permissions through Security->Services->Clicking on Devices access Enforced via user group Authorization.
	GUI::Security::Open Services tab	${USER}
	# Click on authorization subtab
	Click Element	//input[@id='authorization']
	# Click on save button
	GUI::Basic::Save


SUITE:Accessing Other Devices
	# Clicking on user group
	GUI::Basic::Security::Authorization::Open Tab
	# Clicking on user link
	Click Link	//a[@id='user']
	GUI::Basic::Spinner Should Be Invisible
	#Clicking on other devices
	Click Element	//span[normalize-space()='Devices']
	GUI::Basic::Spinner Should Be Invisible
	Click Link	//a[@id='OTHER DEVICES']
	GUI::Basic::Spinner Should Be Invisible
	#Verifying Session,PowerMode and DoorMode are set to No acccess by default
	radio_button_should_be_set_to	rSessionMode	NoAccess
	radio_button_should_be_set_to	rPowerMode	NoAccess
	radio button should be set to	rDoorMode	NoAccess
	# Clicking on cancel button
	GUI::Basic::Cancel


SUITE:Accessing Device through Console
	# Accessing the device through Console
	# Clicking on Access -> Table tab
	GUI::Basic::Access::Table::Open Tab
	# Verifying that there are no rows in the table meaning no devices are there
	Page Should Not Contain Element    ${DEVICE}

SUITE:Accessing Device through User
	# Accessing the device through User
	GUI::Basic::Open And Login Nodegrid	${USER}	${PASS}
	# Verifying that there are no rows in the table meaning no devices are there
	Page Should Not Contain Element    ${DEVICE}

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	SUITE:Adding Devices Permissions
	# Deleting the user group
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	# Deleting the user
	GUI::Security::Local Accounts::Delete	${USER}
	# Delete the console device if exists
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE}
	# Logout of nodegrid and close the browser
	GUI::Basic::Logout And Close Nodegrid