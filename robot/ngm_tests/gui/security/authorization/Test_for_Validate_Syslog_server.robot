*** Settings ***
Documentation	Validating Syslog Server
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

*** Variables ***
${Remote_Ip_Address}	1.1.1.1:1468

*** Test Cases ***
Test case to validate Syslog Server
	GUI::Basic::Auditing::Settings::open tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="datalogTypeS"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Auditing::Destinations::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#auditing_destinations_syslog > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="syslogConsole"]
	Select Checkbox	//*[@id="syslogRemote"]
	Input Text	//*[@id="syslogRemoteIP"]	${Remote_Ip_Address}
	GUI::Basic::Save

Test case to Verify Configuration of Syslog Server
	GUI::Basic::Auditing::Settings::open tab
	GUI::Basic::Spinner Should Be Invisible
	checkbox should be selected	//*[@id="datalogTypeS"]
	GUI::Basic::Auditing::Destinations::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#auditing_destinations_syslog > .title_b
	GUI::Basic::Spinner Should Be Invisible
	checkbox should be selected	//*[@id="syslogConsole"]
	checkbox should be selected	//*[@id="syslogRemote"]

Test Case to delete Configuration of Syslog Server
	GUI::Basic::Auditing::Settings::open tab
	GUI::Basic::Spinner Should Be Invisible
	unselect checkbox	//*[@id="datalogTypeS"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Auditing::Destinations::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#auditing_destinations_syslog > .title_b
	GUI::Basic::Spinner Should Be Invisible
	unselect checkbox	//*[@id="syslogConsole"]
	unselect checkbox	//*[@id="syslogRemote"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid