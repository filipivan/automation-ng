*** Settings ***
Documentation	Test FRR Quagga
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW
Default Tags	EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NAME}	Test_acc1
${auth_name}	Testquagga
${root_login}	shell sudo su -
${acc_login}	su Test_acc1 -c cli
${exec_frr}	exec frr
${bgp}	show bgp ?

*** Test Cases ***
Configure new account and authorisation and check quagga/frr support.
	GUI::Security::Local Accounts::Add	${NAME}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Authorization::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=uGroup	${auth_name}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[1]/td[2]/a
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@id="memberSelect"]//option[@value="${NAME}"]
	Click Element	//*[@id="memberSelect"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#profile > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@id="permissionList"]//option[@value="shell"]
	Click Element	//*[@id="permissionList"]/div/div[1]/div[2]/div/div[1]/button
	Click Element	id=sudo_permission
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${HOST_NAME}=	Get value	id=hostname
	GUI::Access::Open
	Click Element	//div[@class="peer_header"]//a[text()="Console"]
	sleep	5
	Run Keyword If	'${NGVERSION}' >= '5.6'	Switch Window	title=${HOST_NAME} - Console
	...	ELSE	Switch Window	title=${HOST_NAME}

	Wait Until Page Contains Element	//iframe[@id='termwindow']
	Select Frame	//iframe[@id='termwindow']
	Sleep	5
	Press Keys	None	RETURN
	Press Keys	None	RETURN

	${OUTPUT}=	SUITE:Console Command Output	${root_login}
	sleep	5
	${OUTPUT}=	SUITE:Console Command Output	${acc_login}
	sleep	5
	${OUTPUT}=	SUITE:Console Command Output	${exec_frr}}
	sleep	5
	${OUTPUT}=	SUITE:Console Command Output	${bgp}
	sleep	5
	Unselect Frame
	switch window	MAIN

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Local Accounts::open tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Console Command Output
	[Arguments]	${COMMAND}
	[Documentation]	Input the given command into the ttyd terminal and retrieve the output after prompt lines
	...	== ARGUMENTS ==
	...	-	COMMAND = Command to be executed
	...	== EXPECTED RESULT ==
	...	Input the command into the ttyd terminal and returns the OUTPUT between the command and the last prompt

	${CHECK_TAB}=	Run Keyword And Return Status	Should Not Contain	${COMMAND}	TAB
	Run Keyword If	${CHECK_TAB}	Press Keys 	None	${COMMAND}	RETURN
	...	ELSE	Press Keys 	None	${COMMAND}
	Sleep	1

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Authorization::open tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	xpath=//*[@id="Testquagga"]/td[1]/input
	GUI::Basic::Delete With Alert
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Local Accounts::open tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	xpath=//*[@id="Test_acc1"]/td[1]/input
	GUI::Basic::Delete With Alert
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid