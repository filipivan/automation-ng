*** Settings ***
Documentation	Manage Devices Adding Permission
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS		CHROME		FIREFOX
Default Tags	GUI

Suite Setup	SUITE:Setup

*** Variables ***
${USER}		test_user
${PASS}		test_user
${SUDO_USER}	test_sudo_user
${GROUP}	test_group
${SUDO_GROUP}	test_sudo_group
${PDU_DEVICE}	test_raritan
${DEVICE_TYPE}	pdu_raritan
${DEVICE_IP}	192.168.3.7
${DEVICE_USER}	admin
${DEVICE_ASKPASS}	no
${DEVICE_PWD}	admin
${PDU_ID}	My-PDU
${OUTLET_ID1}	1
${OUTLET1_NAME}		Outlet_1
${TEST_DEVICE}		test_device
${TEST_DEVICE_TYPE}		device_console
${COMMAND}		Outlet



*** Test Cases ***
Test Enter In Root Mode In Console With Sudo Permissions
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Manage All Devices Permissions

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid

SUITE:Manage All Devices Permissions
	GUI::Security::Local Accounts::Add	${USER}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Adding Devices Permissions
	SUITE:Add pdu device/protocol/SNMPV2
	SUITE:Auto discovery and check outlets discovered on outlets page
	SUITE:Accessing Other Devices
	SUITE:Accessing Device through Console
	SUITE:Accessing Device through User
	SUITE:Teardown

SUITE:Adding Devices Permissions
	[Documentation]		Adding Device Permissions through Security->Services->Clicking on Devices access Enforced via user group Authorization.
	GUI::Security::Open Services tab	${USER}
	Click Element	xpath=//*[@id="authorization"]
	GUI::Basic::Save

SUITE:Accessing Other Devices
	[Documentation]		Adding Device to a specific Group
	GUI::Security::Open Authorization tab
	GUI::Basic::Spinner Should Be Invisible
	click link		//*[@id="user"]
	GUI::Basic::Spinner Should Be Invisible
	# Clicking on the group and adding device
	Click Element	//*[@id="spm"]/span
	GUI::Basic::Spinner Should Be Invisible
	click element	//*[@id="addButton"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	double click element	//*[@id="targetSelection"]/div/div[1]/div[1]/select/option[@value="${PDU_DEVICE}"]
	click element	//*[@id="saveButton"]
	handle alert
	GUI::Basic::Spinner Should Be Invisible
	#Clicking on other devices
	click link		//*[@id="OTHER DEVICES"]
	Sleep	5s
	# Handling Permissions
	select radio button		rSessionMode	ReadWrite
	Sleep	5s
	# Clicking on save button
	double click element	//*[@id='saveButton']
	sleep	5s
	handle alert

SUITE:Add pdu device/protocol/SNMPV2
	[Documentation]		Adding a PDU device
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::ManagedDevices::Delete Device If Exists	${PDU_DEVICE}
	GUI::ManagedDevices::Delete Device If Exists	${TEST_DEVICE}
	GUI::Basic::Button::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text		id=spm_name		${PDU_DEVICE}
	Select From List By Label	id=type		${DEVICE_TYPE}
	Input Text		id=phys_addr	${DEVICE_IP}
	input text		id=username		${DEVICE_USER}
	input password		id=passwordfirst	${DEVICE_PWD}
	input password  id=passwordconf		${DEVICE_PWD}
	Select From List By Label	id=status	On-demand
	GUI::Basic::Save
	Click Link		${PDU_DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spmmgmt_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox		//*[@id="snmp_proto_enabled"]
	Input Text		//*[@id="community2"]	private
	GUI::Basic::Save
	Click Element	//*[@id="spmcommands_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="Outlet"]
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	//*[@id="protocol"]		SNMP
	GUI::Basic::Save


SUITE:Auto discovery and check outlets discovered on outlets page
	[Documentation]		Doing Auto Discovery and finding outlets
	[Tags]		NON-CRITICAL
	GUI::Basic::Managed Devices::Auto Discovery::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="discovery_now"]
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Disabled		//*[@id="discovernow"]
	Page Should Contain Element		//*[@id="discovernowTable"]
	Select Checkbox		//*[@id="pdu|${PDU_DEVICE}"]/td[1]/input
	Click Button	//*[@id="discovernow"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	Sleep	15s
	Click Link		${PDU_DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spmoutlets_nav"]/span
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain		${OUTLET1_NAME}


SUITE:Accessing Device through Console
	[Documentation]		Accessing device through console
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="|${PDU_DEVICE}"]/td[1]/div/a/span
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	css=.btn:nth-child(4)
	Click Button	css=.btn:nth-child(4)
	GUI::Basic::Spinner Should Be Invisible
	Page Should not contain	unknown
	wait until page contains element	//*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]
	Select Checkbox		//tr[@id='${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}']/td[1]/input
	Click Button	//*[@id="nonAccessControls"]/input[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	Wait Until Element Contains	//*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]		Off
	# Click on cancel button on the top right corner
	click button	//*[@id="modal"]/div/div/div[1]/button
	Sleep	5s
	GUI:Access::Table::Access Device Console	${PDU_DEVICE}
	Sleep	5s
	# Click on Enter keys
	Press Keys		//body	RETURN
	${OUTPUT}=	GUI:Access::Generic Console Command Output	connect
	Press Keys		//body	RETURN
	# Write a command for example "exit"
	${OUTPUT}=	GUI:Access::Generic Console Command Output	exit
	Sleep	5s
	close browser


SUITE:Accessing Device through User
	[Documentation]		Accessing the device through User
	GUI::Basic::Open And Login Nodegrid		${USER}		${PASS}
	# Test that the device is visible but read only for the User meaning it should not be clickable
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="|${PDU_DEVICE}"]/td[1]/div/a/span
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	css=.btn:nth-child(4)
	Click Button	css=.btn:nth-child(4)
	GUI::Basic::Spinner Should Be Invisible
	Page Should not contain	unknown
	wait until page contains element	//*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]
	Select Checkbox		//tr[@id='${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}']/td[1]/input
	Click Button	//*[@id="nonAccessControls"]/input[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	20s
	Wait Until Element Contains		//*[@id="${PDU_DEVICE}:${PDU_ID}:${OUTLET_ID1}:${OUTLET1_NAME}"]/td[6]	Off
	# Click on cancel button on the top right corner
	click button	//*[@id="modal"]/div/div/div[1]/button
	Sleep	5s
	GUI:Access::Table::Access Device Console	${PDU_DEVICE}
	Sleep	5s
	# Click on Enter keys
	Press Keys		//body	RETURN
	${OUTPUT}=	GUI:Access::Generic Console Command Output	connect
	Press Keys		//body	RETURN
	# Write a command for example "exit"
	${OUTPUT}=	GUI:Access::Generic Console Command Output	exit
	Sleep	5s
	close browser


SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	SUITE:Adding Devices Permissions
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	GUI::Security::Local Accounts::Delete	${USER}
	GUI::Security::Authorization::Delete User Group If Exists	${GROUP}
	GUI::Security::Local Accounts::Delete	${USER}
	GUI::ManagedDevices::Delete Device If Exists  ${PDU_DEVICE}
	GUI::Basic::Logout And Close Nodegrid