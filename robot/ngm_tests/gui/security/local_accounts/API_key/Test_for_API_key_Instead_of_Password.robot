*** Settings ***
Documentation	API
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_0

*** Variables ***
${NAME}	Test
${API_Key}

*** Test Cases ***
Test Case to add Local account user
	GUI::Security::Open Local Accounts tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input text	//*[@id="uName"]	${NAME}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	css=.radio:nth-child(6) #uType	60s
	Click Button	css=.radio:nth-child(6) #uType
	${API_Key}=	Get Value	//*[@id="apikey"]
	${API_Key}=  Set Suite Variable  ${API_Key}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	user_namesTable	API
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Setup2
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Authentication failure
	GUI::Basic::Spinner Should Be Invisible

Test case to edit Local User Account
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Open Local Accounts tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${NAME}"]/td[1]/input
	Click Element	//*[@id="editButton"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="reset_api_key"]
	wait until element is visible	//*[@id="apikey"]
	${API_Key}=	Get Value	//*[@id="apikey"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	user_namesTable	API
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Setup2
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Authentication failure
	GUI::Basic::Spinner Should Be Invisible

Test Case to Lock the added Account
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Open Local Accounts tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${NAME}"]/td[1]/input
	Click Element	//*[@id="lockUser"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	user_namesTable	LOCKED
	GUI::Basic::Spinner Should Be Invisible

Test case to Unlock the added Account
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Open Local Accounts tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${NAME}"]/td[1]/input
	Click Element	//*[@id="unlockUser"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	user_namesTable	UNLOCKED
	GUI::Basic::Spinner Should Be Invisible

Test Case to delete Local account user
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Open Local Accounts tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${NAME}"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="delButton"]
	Handle Alert
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Setup2
	GUI::Basic::Open Nodegrid	${HOMEPAGEPEER}	${BROWSER}	${NGVERSION}
	GUI::Basic::Spinner Should Be Invisible
	Input Text	username	${NAME}
	Input Text	password	${API_Key}
	Click Element	login-btn
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
