*** Settings ***
Documentation	Editing Local Accounts Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	LOCAL_ACCOUNTS

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variable ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${NAME}	test

*** Test Cases ***
Local Accounts, remove user test if exists
	GUI::Basic::Security::Local Accounts::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Wait Until Element Is Accessible	jquery=#user_namesTable
	${TEST_ACCOUNT_EXISTS}=	Run Keyword And Return Status	GUI::Element Exists	//*[@id="test"]
	Run Keyword If	${TEST_ACCOUNT_EXISTS}	GUI::Basic::Delete Rows In Table	\#user_namesTable	test

Enable Password Rules
	GUI::Basic::Security::Password Rules::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	jquery=#enabled
	GUI::Basic::Input Text	//*[@id='min_digits']	4
	${IS_SAVE_ENABLED}=	Run Keyword And Return Status	Element Should Be Enabled	//*[@id='saveButton']
	Run Keyword If	${IS_SAVE_ENABLED}	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Checkbox Should Be Selected	jquery=#enabled

Local Accounts, add user
	GUI::Basic::Security::Local Accounts::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Local Accounts::Add	test
	Wait Until Element Contains	jquery=#errormsg	The password is too short. Minimum length:

	Input Text	jquery=#uPasswd	aaaaaAAAAA!!!!!cccccc
	Input Text	jquery=#cPasswd	aaaaaAAAAA!!!!!ccc
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#errormsg	Password mismatch.

	Input Text	jquery=#cPasswd	aaaaaAAAAA!!!!!cccccc
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#errormsg	Password minimum digits requirement: 4

	Input Text	jquery=#uPasswd	aaaaaAAAAA!!!!!12345
	Input Text	jquery=#cPasswd	aaaaaAAAAA!!!!!12345
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Does Not Contain	jquery=#globalerr	assword

Local Accounts, Lock user
	GUI::Basic::Security::Local Accounts::Open Tab
	Select Checkbox	jquery=#user_namesTable tr#test input[type=checkbox]
	Click Element	jquery=#lockUser
	GUI::Security::Local Accounts::Wait For Local Accounts Table
	Table Should Contain	jquery=#user_namesTable	Locked
	GUI::Basic::Spinner Should Be Invisible

Local Accounts, login Locked user
	GUI::Basic::Logout
	GUI::Basic::Login Failure	test	aaaaaAAAAA!!!!!12345
	[Teardown]	GUI::Basic::Login

Local Accounts, Unlock user
	GUI::Basic::Security::Local Accounts::Open Tab
	Select Checkbox	jquery=#user_namesTable tr#test input[type=checkbox]
	Click Element	jquery=#unlockUser
	GUI::Security::Local Accounts::Wait For Local Accounts Table
	GUI::Basic::Table Should Not Contain	user_namesTable	Locked
	GUI::Basic::Spinner Should Be Invisible

Local Accounts, login Unlocked user
	GUI::Basic::Logout
	GUI::Basic::Login	${NGVERSION}	test	aaaaaAAAAA!!!!!12345
	[Teardown]	Run Keywords	GUI::Basic::Logout	AND	GUI::Basic::Login

Local Accounts, Require password change at login time
	[Tags]	EXCLUDEIN4_2		NON-CRITICAL	NEED-REVIEW
#	Checked manually/run test_1 the issue was not occurred
	GUI::Basic::Security::Local Accounts::Open Tab
	GUI::Basic::Wait Until Element Is Accessible	jquery=a#test
	GUI::Basic::Click Element	//*/a[@id='test']
	GUI::Basic::Wait Until Element Is Accessible	jquery=#pwForceChange
	GUI::Basic::Select Checkbox	//*[@id='pwForceChange']
	GUI::Basic::Click Element	//*[@id='saveButton']
	GUI::Basic::Logout
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#username	jquery=input[name='password']
	GUI::Basic::Input Text	//*[@id='username']	test
	GUI::Basic::Input Text	//*/input[@name='password']	aaaaaAAAAA!!!!!12345
	GUI::Basic::Click Element	//*[@id='login-btn']
	GUI::Basic::Wait Until Element Is Accessible	//input[@id='newPassword']
	GUI::Basic::Input Text	//input[@id='newPassword']	bbbbBBBB!!!!!56789
	GUI::Basic::Input Text	//input[@id='confirmation']	bbbbBBBB!!!!!56789
	GUI::Basic::Click Element	//input[@id='change-password-btn']
	GUI::Basic::Wait Until Element Is Accessible	//a[@id='lo']//span[contains(text(),'Logout')]
	GUI::Basic::Wait Until Element Is Accessible	//span[@id='hel']
	GUI::Basic::Wait Until Element Is Accessible	//a[@id='pwl']
	[Teardown]	Run Keywords	Close All Browsers	AND	GUI::Basic::Open And Login Nodegrid	AND	GUI::Security::Open Local Accounts Tab

Local Accounts, Delete user
	GUI::Basic::Logout
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#username	jquery=input[name='password']
	GUI::Basic::Login	${NGVERSION}	${USERNAME}	${PASSWORD}
	GUI::Basic::Security::Local Accounts::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table	\#user_namesTable	test

Disable Password Rules
	GUI::Basic::Security::Password Rules::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Unselect Checkbox	jquery=#enabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Checkbox Should Not Be Selected	jquery=#enabled

Local Accounts, add user again
	GUI::Basic::Security::Local Accounts::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Local Accounts::Add	test

Local Accounts, Delete user again
	GUI::Basic::Security::Local Accounts::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete Rows In Table	\#user_namesTable	test

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
