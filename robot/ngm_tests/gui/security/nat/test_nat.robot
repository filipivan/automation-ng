*** Settings ***
Documentation	Testing Security:NAT tab fields
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test Checkboxes
	@{CHKBOX}=	Create List	nat_inv_source	nat_inv_dest	nat_inv_inIface	nat_inv_proto	nat_inv_sport	nat_inv_dport
	SUITE:Check Checkboxes	@{CHKBOX}
	Select Checkbox	jquery=#nat_stateM_enabled
	GUI::Basic::Spinner Should Be Invisible
	@{ENABOX}=	Create List	nat_st_state_new	nat_st_state_est	nat_st_state_rel	nat_st_state_inv	nat_st_state_snat	nat_st_state_dnat	nat_stateM_state-inv
	SUITE:Check Checkboxes	@{ENABOX}

Rule Dropdown and Textbox
	Select From List By Value		nat_target	DNAT

	Select Radio Button	nat_protocol	tcp
	Input Text	xpath=//*[@id="nat_sportTCP"]	1000
	Input Text	xpath=//*[@id="nat_dportTCP"]	1000

	Wait Until Keyword Succeeds	30	2	GUI::Auditing::Auto Input Tests	nat_todest	stringinv	::2c	345.345.345.345	123.123.123.123
	Wait Until Keyword Succeeds	30	2	GUI::Auditing::Auto Input Tests	nat_source	345.345.345.345	stringinv	::2c	123.123.123.123
	Wait Until Keyword Succeeds	30	2	GUI::Auditing::Auto Input Tests	nat_dest	345.345.345.345	stringinv	::2c	123.123.123.123

Test Lists
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Compare Interfaces

Test LOG
	Select From List By Value	nat_target	LOG
	@{EXP_LLV}=	Create List	Debug	Info	Notice	Warning	Error	Critical	Alert	Emergency
	@{LLV}=	Get List Items	nat_log_level
	Should Be Equal	${EXP_LLV}	${LLV}
	@{LOGCHK}=	Create List	nat_log_tcp_seq	nat_log_tcp_opt	nat_log_ip_opt
	SUITE:Check Checkboxes	@{LOGCHK}

Test Numeric Protocol
	Select Radio Button	nat_protocol	numeric
	GUI::Auditing::Auto Input Tests	nat_protoNum	-1	str	65537	100

Test TCP Protocol
	Select From List By Value	nat_target	REDIRECT

	GUI::Basic::Select Radio Button    nat_protocol	tcp
	Wait Until Keyword Succeeds	30	2	GUI::Auditing::Auto Input Tests	nat_sportTCP		-1  str	65537	1000
	Wait Until Keyword Succeeds	30	2	GUI::Auditing::Auto Input Tests	nat_dportTCP		-1  str	65537	1000
	Wait Until Keyword Succeeds	30	2	GUI::Auditing::Auto Input Tests	nat_toportsTCP	-1	str	65537	1000

	@{CHKTCP}=	Create List	nat_SYN	nat_ACK	nat_FIN	nat_RST	nat_URG	nat_PSH
	SUITE:List Checker TCP	@{CHKTCP}
	SUITE:Check Checkboxes	nat_inv_tcp-flags

Test UDP Protocol
	GUI::Basic::Select Radio Button	nat_protocol	udp

	Wait Until Keyword Succeeds	30	2	GUI::Auditing::Auto Input Tests	nat_sportUDP	-1	str	65537	1000
	Wait Until Keyword Succeeds	30	2	GUI::Auditing::Auto Input Tests	nat_dportUDP	-1	str	65537	1000

Test ICMP Protocol
	GUI::Basic::Select Radio Button	nat_protocol	icmp
	
	@{CHKICMP}=	Create List	Any	Echo Reply	Destination Unreachable	Network Unreachable
	...	Host Unreachable	Protocol Unreachable	Port Unreachable	Fragmentation Needed	Source Route Failed
	...	Network Unknown	Host Unknown	Network Prohibited	Host Prohibited	TOS Network Unreachable
	...	TOS Host Unreachable	Communication Prohibited	Host Precedence Violation	Precedence Cutoff
	...	Source Quench	Redirect	Network Redirect	Host Redirect	TOS Network Redirect
	...	TOS Host Redirect	Echo Request	Router Advertisement	Router Solicitation	Time Exceeded
	...	TTL Zero During Transit	TTL Zero During Reassembly	Parameter Problem	Bad IP Header
	...	Required Option Missing	Timestamp Request	Timestamp Reply	Address Mask Request
	...	Address Mask Reply
	@{ICMP}=	Get List Items	nat_icmp-type
	Should Be Equal	${CHKICMP}	${ICMP}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::NAT::Open Tab
	GUI::Basic::Spinner Should Be Invisible

	Click Element  xpath=//*[@id="PREROUTING"]
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be visible	jquery=#nat_rulesTable
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Logout
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:Check Checkboxes
	[Arguments]	@{LST}
	FOR	${I}	IN	@{LST}
		Page Should Contain Element	jquery=#${I}
	END

SUITE:List Checker TCP
	[Arguments]	@{LST}
	@{CHK}=	Create List	Any	Set	Unset
	FOR	${I}	IN	@{LST}
		@{LEL}=	Get List Items	${I}
		${LENGTH}=	Get Length	${LEL}
		${LENGTH_CHOICES}=	Get Length	${CHK}
		Should Be Equal	${LENGTH_CHOICES}	${LENGTH}
	END

SUITE:Compare Interfaces
	@{EXP_INF}=	Create List	Any	lo	eth0
	${SYSTEM}=	GUI4.x::Basic::Get System
	Run Keyword If	'${SYSTEM}' == 'Nodegrid Services Router' or '${SYSTEM}' == 'Nodegrid Net SR'	Append To List	${EXP_INF}	eth1	backplane0	backplane1
	...	ELSE	Run Keyword If	'${SYSTEM}' == 'Nodegrid Bold SR'	Append To List	${EXP_INF}	backplane0
	...	ELSE	Run Keyword If	'${SYSTEM}' == 'Nodegrid Gate SR'	Append To List	${EXP_INF}	backplane0	backplane1	sfp0	sfp1
	...	ELSE	Run Keyword If	'${SYSTEM}' == 'Nodegrid Hive SR'	Append To List	${EXP_INF}	eth1	eth2	eth3
	...	ELSE	Run Keyword If	'${SYSTEM}' == 'Nodegrid Serial Console'	Append To List	${EXP_INF}	eth1
	Sort List	${EXP_INF}
	@{INF}=	Get List Items	nat_inIface
	Remove Values From List	${INF}	cdc-wdm0	cdc-wdm1	cdc-wdm2	cdc-wdm3	cdc-wdm4	cdc-wdm5	cdc-wdm6
	Sort List	${INF}
	Lists Should Be Equal	${EXP_INF}	${INF}
	@{EXP_FRA}=	Create List	All packets and fragments	Unfragmented packets and 1st packets	2nd and further packets
	@{FRA}=	Get List Items	nat_frags
	Should Be Equal	${EXP_FRA}	${FRA}