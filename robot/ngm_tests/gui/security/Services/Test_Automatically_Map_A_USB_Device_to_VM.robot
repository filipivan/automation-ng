*** Settings ***
Documentation	Functionality test for Automatically Map A Usb Device to VM
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	      NON-CRITICAL
Default Tags	EXCLUDEIN4_2        EXCLUDEIN5_0        EXCLUDEIN5_2		EXCLUDEIN5_4

*** Variables ***
${Vm_Name}		Automation
${Storage_Size}		1
${Memory_Size}		512
${ALPINE_FILE_NAME}	alpine-virt-3.14.2-x86_64.iso
${Img_Path}		/tmp/alpine-virt-3.14.2-x86_64.iso
${New_name}		Testing
${LICENSE}      ${LICENSE_VM_AND_DOCKER}
${WARNING_MESSAGE}	...	There is no USB Device Connected

*** Test Cases ***
Test Case to Add Docker License and Enable Qemu/kvm
	SUITE:Add Docker License
	SUITE:Enable Qemu/Kvm
	SUITE:To Select Checkbox Automatically map connected devices to Virtual Machine
	SUITE:To Check Status in Tracking Page
	page should not contain		Automation

Test Case to create VM
	SUITE:Test Get Image File
	SUITE:To Create Vm

Test Case to Check the Mapped Status
	SUITE:To Check the Device Should Mapped on Virtual Machines Page
	page should not contain		No host devices assigned to this VM
	SUITE:To Check Status in Tracking Page
	wait until page contains element		//*[@id="usbDetailForm"]/div[2]/div/div/div[11]/div
	SUITE:To Check to Login to the Console
	SUITE:To Check the Device on the Console

Test Case to Disable Qemu/Kvm and Check the Status on Tracking Page
	SUITE:To Disable Qemu/kvm
	SUITE:To Check Status in Tracking Page
	page should not contain		Automation
	SUITE:To Check the Device Should Mapped on Virtual Machines Page
	page should contain		No host devices assigned to this VM

Test Case to Enable Qemu/kvm and Check the Status in tracking page
	SUITE:Enable Qemu/Kvm
	SUITE:To Check Status in Tracking Page
	wait until page contains element		//*[@id="usbDetailForm"]/div[2]/div/div/div[11]/div

Test Case to Un-Select Automatically Map option on Managed device and Check the Status on Tracking Page
	SUITE:To Un-Select Automatically Map option on Managed device Page
	SUITE:To Check Status in Tracking Page
	page should not contain		Automation
	SUITE:To Check the Device Should Mapped on Virtual Machines Page
	page should contain		No host devices assigned to this VM

Test Case to Rename The Vm and Check the Status
	SUITE:To Select Checkbox Automatically map connected devices to Virtual Machine
	SUITE:To Rename the Vm
	SUITE:To Check Status in Tracking Page
	page should not contain		Testing
	SUITE:To Check the Device Should Mapped on Virtual Machines Page
	page should contain		No host devices assigned to this VM

Test Case to Renamed in Managed device Page and Check the Status
	SUITE:To Rename on Managed device Page
	SUITE:To Check Status in Tracking Page
	wait until page contains element		//*[@id="usbDetailForm"]/div[2]/div/div/div[11]/div
	SUITE:To Check the Device Should Mapped on Virtual Machines Page
	page should not contain		No host devices assigned to this VM

Test Case to Reboot Vm and Check the Status
	SUITE:To Reboot the Vm
	SUITE:To Check Status in Tracking Page
	wait until page contains element		//*[@id="usbDetailForm"]/div[2]/div/div/div[11]/div

Test Case to Delete the Config
	SUITE:TO Delete the Vm
	SUITE:To Un-Select Automatically Map option on Managed device Page
	SUITE:To Disable Qemu/kvm
	SUITE:To Delete Docker License
	SUITE:TO Delete Image File

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	SUITE:To Check USB Device

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:Add Docker License
    GUI::Basic::System::License::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="licensevalue"]     ${LICENSE}
	GUI::Basic::Save If Configuration Changed
	GUI::Basic::Spinner Should Be Invisible

SUITE:Enable Qemu/Kvm
	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible       xpath=//input[@id='virtualization_libvirt']
	Run Keyword And Continue on Failure     Select Checkbox     xpath=//input[@id='virtualization_libvirt']
	Run Keyword And Continue on Failure     Click Element       //*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Select Checkbox Automatically map connected devices to Virtual Machine
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	click element		xpath=//a[contains(text(),'usbS4-3')]
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible		xpath=//label[contains(.,'Automatically map connected devices to Virtual Machine')]
	click element		xpath=//label[contains(.,'Automatically map connected devices to Virtual Machine')]
	input text		xpath=//input[@id='mapUsbToVmName']		${Vm_Name}
	GUI::Basic::Save If Configuration Changed
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Create Vm
	GUI::Basic::Applications::Virtual Machines::open tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep		30s
	Select Frame        //iframe
	wait until page contains element		xpath=//button[contains(.,'Create VM')]
	Click Element		xpath=//button[contains(.,'Create VM')]
	input text		xpath=//input[@id='vm-name']		${Vm_Name}
	sleep		5s
	Select From List By value		//*[@id="source-type"]		file
	wait until page contains element		//*[@id="source-file-group"]/div[2]
	set focus to element		//*[@id="source-file-group"]/div[2]
	click element		//*[@id="source-file-group"]/div[2]
	sleep		5s
	wait until page contains element		//*[@id="source-file-group"]/div[2]
	Input Text		xpath=//div[@id='source-file-group']/div[2]/div/div/div/input		${Img_Path}
	sleep		10s
	Press Keys    xpath=//div[@id='source-file-group']/div[2]/div/div/div/input   ENTER
	sleep		3s
	click element		xpath=//button[contains(.,'/tmp/alpine-virt-3.14.2-x86_64.iso')]
	wait until page contains element		xpath=//span[contains(.,'Operating system')]
	set focus to element		xpath=//span[contains(.,'Operating system')]
	input text		//*[@id="storage-size"]		${Storage_Size}
	set focus to element		//*[@id="memory-size"]
	input text		//*[@id="memory-size"]		${Memory_Size}
	sleep		5s
	set focus to element		//*[@id="create-vm-dialog"]/footer/button[1]
	click element		//*[@id="create-vm-dialog"]/footer/button[1]
	sleep		15s
	page should contain		Running

SUITE:To Check Status in Tracking Page
	GUI::Basic::Tracking::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#usbDevices > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="usbTable_wrapper"]
	click element		xpath=//a[contains(text(),'S4-3')]
	GUI::Basic::Spinner Should Be Invisible
	execute javascript  document.getElementById("usbMappedVm").scrollIntoView()
	sleep		5s

SUITE:To Check the Device Should Mapped on Virtual Machines Page
	GUI::Basic::Applications::Virtual Machines::open tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep		30s
	Select Frame        //iframe
	wait until page contains element		xpath=//button[contains(.,'Create VM')]
	set focus to element		xpath=//a[contains(text(),'Automation')]
	click element		xpath=//a[contains(text(),'Automation')]
	wait until page contains element		xpath=//h2[contains(.,'Host devices')]
	execute javascript  document.getElementById("vm-Automation-hostdevs").scrollIntoView()
	sleep		5s

SUITE:To Check to Login to the Console
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="|nodegrid"]/div[1]/div[2]/a
	Sleep	10s
	Switch Window	nodegrid - Console
	Sleep	10s
	Select Frame	xpath=//*[@id='termwindow']
	Press Keys	None	RETURN
	${OUTPUT}=	GUI:Access::Generic Console Command Output	shell sudo su -
	Sleep	15s
	Should Contain	${OUTPUT}	root@nodegrid:~#

	${OUTPUT}=	GUI:Access::Generic Console Command Output	virsh console ${Vm_Name}
	Sleep	15s
	${OUTPUT}=	GUI:Access::Generic Console Command Output   ENTER
	${OUTPUT}=	GUI:Access::Generic Console Command Output   ENTER
	Should Match Regexp	${OUTPUT}	.*Alpine.*

	${OUTPUT}=	GUI:Access::Generic Console Command Output	root
	Press Keys	None   ENTER
	${OUTPUT}=	GUI:Access::Generic Console Command Output	root
	Press Keys	None   ENTER
	Sleep		10s

SUITE:To Check the Device on the Console
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="|nodegrid"]/div[1]/div[2]/a
	Sleep	10s
	Switch Window	nodegrid - Console
	Sleep	10s
	Select Frame	xpath=//*[@id='termwindow']
	Press Keys	None	RETURN
	${OUTPUT}=	GUI:Access::Generic Console Command Output	shell sudo su -
	Sleep	15s
	Should Contain	${OUTPUT}	root@nodegrid:~#

	${OUTPUT}=	GUI:Access::Generic Console Command Output	virsh console ${Vm_Name}
	Sleep	15s
	${OUTPUT}=	GUI:Access::Generic Console Command Output   ENTER
	${OUTPUT}=	GUI:Access::Generic Console Command Output   ENTER
	${OUTPUT}=	GUI:Access::Generic Console Command Output	lsusb
	Sleep		5s
	Should Match Regexp	${OUTPUT}	.*Bus.*

SUITE:To Disable Qemu/kvm
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible       xpath=//input[@id='virtualization_libvirt']
	unselect checkbox     xpath=//input[@id='virtualization_libvirt']
	Click Element       //*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep		5s

SUITE:To Un-Select Automatically Map option on Managed device Page
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	click element		xpath=//a[contains(text(),'usbS4-3')]
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible		xpath=//label[contains(.,'Automatically map connected devices to Virtual Machine')]
	execute javascript  document.getElementById("mapUsbToVm").scrollIntoView()
	unselect checkbox		//*[@id="mapUsbToVm"]
	GUI::Basic::Save If Configuration Changed
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Rename the Vm
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="|nodegrid"]/div[1]/div[2]/a
	Sleep	15s
	Switch Window	nodegrid - Console
	Sleep	10s
	Select Frame	xpath=//*[@id='termwindow']
	Sleep	10s
	Press Keys	None	RETURN
	${OUTPUT}=	GUI:Access::Generic Console Command Output	shell sudo su -
	Sleep	15s
	Should Contain	${OUTPUT}	root@nodegrid:~#

	${OUTPUT}=	GUI:Access::Generic Console Command Output	virsh console ${Vm_Name}
	Sleep	15s
	${OUTPUT}=	GUI:Access::Generic Console Command Output   ENTER
	${OUTPUT}=	GUI:Access::Generic Console Command Output   ENTER
	${OUTPUT}=	GUI:Access::Generic Console Command Output		virsh domerename ${Vm_Name} ${New_name}
	page should contain		successfully

SUITE:To Rename on Managed device Page
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	click element		xpath=//a[contains(text(),'usbS4-3')]
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible		xpath=//label[contains(.,'Automatically map connected devices to Virtual Machine')]
	execute javascript  document.getElementById("mapUsbToVm").scrollIntoView()
	Run Keyword And Continue on Failure 		select checkbox		//*[@id="mapUsbToVm"]
	input text		xpath=//input[@id='mapUsbToVmName']		${New_name}
	GUI::Basic::Save If Configuration Changed
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Reboot the Vm
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Applications::Virtual Machines::open tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep		30s
	Select Frame        //iframe
	wait until page contains element		xpath=//button[contains(.,'Create VM')]
	set focus to element		xpath=//a[contains(text(),'Automation')]
	click element		xpath=//a[contains(text(),'Automation')]
	Sleep		3s
	wait until page contains element		//*[@id="vm-Automation-autostart-checkbox"]
	execute javascript  document.getElementById("vm-Automation-autostart-checkbox").scrollIntoView()
	sleep		5s
	click element		//*[@id="vm-Automation-autostart-checkbox"]
	click element		//*[@id="vm-Automation-action-kebab"]
	execute javascript  document.getElementById("vm-Automation-reboot").scrollIntoView()

SUITE:TO Delete the Vm
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Applications::Virtual Machines::open tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep		30s
	Select Frame        //iframe
	wait until page contains element		xpath=//button[contains(.,'Create VM')]
	set focus to element		xpath=//a[contains(text(),'Testing')]
	click element		xpath=//a[contains(text(),'Testing')]
	Sleep		3s
	click element		//*[@id="vm-Automation-autostart-checkbox"]
	click element		//*[@id="vm-Automation-action-kebab"]
	execute javascript  document.getElementById("vm-Automation-delete").scrollIntoView()
	click element		//*[@id="vm-Automation-delete"]
	wait until page contains element		//*[@id="pf-modal-part-3"]
	click element		//*[@id="vm-Automation-delete-modal-dialog"]/footer/button[1]
	sleep		5s

SUITE:To Delete Docker License
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::System::License::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete All Rows In Table If Exists	\#license_table	False

SUITE:To Check USB Device
	GUI::Tracking::Open Devices Tab
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//span[contains(.,'USB devices')]
	GUI::Basic::Spinner Should Be Invisible
	${HAS_USB_Device}	Run Keyword and Return Status	Page Should Contain	 Mass Storage
	Run Keyword If	not ${HAS_USB_Device}	Skip	${WARNING_MESSAGE}

SUITE:To Get Image File
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="|nodegrid"]/div[1]/div[2]/a
	Sleep	15s
	Switch Window	nodegrid - Console
	Sleep	10s
	Select Frame	xpath=//*[@id='termwindow']
	Sleep	10s
	Press Keys	None	RETURN
	${OUTPUT}=	GUI:Access::Generic Console Command Output	shell sudo su -
	Sleep	15s
	Should Contain	${OUTPUT}	root@nodegrid:~#
	${OUTPUT}=	GUI:Access::Generic Console Command Output	cd /tmp
	Should Contain	${OUTPUT}		root@nodegrid:/tmp#
	${OUTPUT}=	GUI:Access::Generic Console Command Output		wget --user=${FTPSERVER2_USER} --password=${FTPSERVER2_PASSWORD} ftp://${FTPSERVER2_USER}@${FTPSERVER2_IP}/files/${ALPINE_FILE_NAME}
	Sleep		5s
	Page Should Contain		100%	saved

SUITE:TO Delete Image File
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="|nodegrid"]/div[1]/div[2]/a
	Sleep	15s
	Switch Window	nodegrid - Console
	Sleep	10s
	Select Frame	xpath=//*[@id='termwindow']
	Sleep	10s
	Press Keys	None	RETURN
	${OUTPUT}=	GUI:Access::Generic Console Command Output	shell sudo su -
	Sleep	15s
	Should Contain	${OUTPUT}	root@nodegrid:~#
	${OUTPUT}=	GUI:Access::Generic Console Command Output	cd /tmp
	Should Contain	${OUTPUT}		root@nodegrid:/tmp#
	${OUTPUT}=	GUI:Access::Generic Console Command Output	rm ${ALPINE_FILE_NAME}
	${OUTPUT}=	GUI:Access::Generic Console Command Output	ls
	page should not contain		${ALPINE_FILE_NAME}
