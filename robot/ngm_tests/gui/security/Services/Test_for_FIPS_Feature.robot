*** Settings ***
Documentation	Test for FIPS Functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	     NON-CRITICAL
Default Tags	EXCLUDEIN4_2        EXCLUDEIN5_0        EXCLUDEIN5_2		EXCLUDEIN5_4		EXCLUDEIN5_6

*** Variables ***
${Name}		test
${Type}		device_console
${Ip_address}		127.0.0.1
${Usr}		${DEFAULT_USERNAME}
${Passwd}		${DEFAULT_PASSWORD}
${Confm_passwd}		${DEFAULT_PASSWORD}
${Idrac9}		${IDRAC9_NAME}
${Idrac_type}     drac
${Idrac_Ip_address}       ${IDRAC9_IP}
${Idrac_Usr}     ${IDRAC9_USERNAME}
${Idrac_Passwd}     ${IDRAC9_PASSWORD}
${Idrac_Confm_passwd}     ${IDRAC9_PASSWORD}
${Idrac_Status}     On-demand
${cmd}      KVM
${type_extn}        idrac.py

*** Test Cases ***
Test Case to Add Dummy Managed device Before Enable FIPS
	SUITE:To Add Managed Device

Test Case to Enable FIPS
	SUITE:To Enable FIPS
	SUITE:To Check OpenSSl List Providers
	SUITE:To Check the Console and Web Access for Dummy managed Device after Enabling FIPS

Test Case to Add Idrac9 Device after Enable FIPS and Check the Kvm Window Is Accessible
	SUITE:To Add Idrac9 device

Test Case to Disable FIPS
	SUITE:To Disable FIPS
	SUITE:To Delete Managed devices

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:To Add Managed Device
	GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists	${Name}
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text	    //*[@id='spm_name']	    ${Name}
	Select From List By Label   //*[@id="type"]    ${Type}
	Input Text      id=phys_addr    ${Ip_address}
	Input Text      id=username          ${Usr}
    Input Text      id=passwordfirst     ${Passwd}
    Input Text      id=passwordconf      ${Confm_passwd}
    GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Table Should Has Row With Id        SPMTable    ${Name}

SUITE:To Enable FIPS
	SUITE:Setup
	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	select checkbox		xpath=//input[@id='fips']
	Click Element	jquery=#saveButton
	handle alert
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	3x	3s	SUITE:Check If Device is Up

SUITE:Check If Device is Up
	Sleep	3m
	Wait Until Keyword Succeeds	40x	3s	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible		id=search_expr1

SUITE:To Add Idrac9 device
	SUITE:Setup
	GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists	${Idrac9}
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text	    //*[@id='spm_name']	    ${Idrac9}
	Select From List By Label   //*[@id="type"]    ${Idrac_type}
	Input Text      id=phys_addr    ${Idrac_Ip_address}
	Input Text      id=username          ${Idrac_Usr}
    Input Text      id=passwordfirst     ${Idrac_Passwd}
    Input Text      id=passwordconf      ${Idrac_Confm_passwd}
    Select From List By Label    id=status    ${Idrac_Status}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Table Should Has Row With Id        SPMTable    ${Idrac9}
    GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain    ${Idrac9}
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    Select Checkbox    //*[@id="${Idrac9}"]/td[1]/input
    Click Element       xpath=//a[contains(text(),'${Idrac9}')]
    Sleep       5s
    Click Element       css=#spmcommands_nav > .title_b
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
    Select From List By Label       id=command      ${cmd}
    Select From List By Label       id=pymod_file      ${type_extn}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain    ${Idrac9}
    Wait Until Page Contains Element     xpath=//a[contains(text(),'KVM')]
	GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    Wait until Page Contains Element        xpath=//a[contains(text(),'KVM')]
    Click Element       xpath=//a[contains(text(),'KVM')]
    Sleep       20s
    Switch Window        ${Idrac9} - KVM

SUITE:To Check the Console and Web Access for Dummy managed Device after Enabling FIPS
	SUITE:Setup
	GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain    ${Name}
    Wait Until Page Contains Element        xpath=(//a[contains(text(),'Console')])[2]
    Click Element       xpath=(//a[contains(text(),'Console')])[2]
    Sleep       20s
    Switch Window        ${Name} - Console
    Wait Until Page Contains Element        xpath=//*[@id='termwindow']
    Select Frame	xpath=//*[@id='termwindow']
    Sleep       20s
	Press Keys      //body      RETURN
    ${OUTPUT}=	GUI:Access::Generic Console Command Output	info
    sleep		20s
	Should Contain	${OUTPUT}	test
	Switch Window	MAIN
	GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element       xpath=//a[contains(text(),'Web')]
    GUI::Basic::Spinner Should Be Invisible
    Sleep       30s
    Switch Window        ${Name} - Web

SUITE:To Disable FIPS
	Switch Window	MAIN
	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	unselect checkbox		xpath=//input[@id='fips']
	Click Element	jquery=#saveButton
	handle alert
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	3x	3s	SUITE:Check If Device is Up

SUITE:To Delete Managed devices
	SUITE:Setup
	GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists	${Name}
    sleep		5s
    GUI::ManagedDevices::Delete Device If Exists	${Idrac9}

SUITE:To Check OpenSSl List Providers
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	wait until page contains element	//*[@id="|nodegrid"]/div[1]/div[2]/a
	Click Element	//*[@id="|nodegrid"]/div[1]/div[2]/a
	Sleep	20s
	Switch Window	nodegrid - Console
	Sleep	15s
	Select Frame	xpath=//*[@id='termwindow']
	Press Keys	//body	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output	shell sudo su -
	Sleep	15s
	Should Contain	${OUTPUT}	root@nodegrid:~#
	${OUTPUT}=	GUI:Access::Generic Console Command Output	openssl list -providers
	Sleep	15s
	Should Contain	${OUTPUT}	base
	Sleep		5s
	Should Contain	${OUTPUT}	fips

