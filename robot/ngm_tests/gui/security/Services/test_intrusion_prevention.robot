*** Settings ***
Documentation	Test common behaviour in security -> services page
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variable ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${string_value}	str
${Zero}	0
${ONE}	10
${Large_val}	65536
${Empty}

*** Test Cases ***
Test Block Host Disabled
	Run Keyword If	'${NGVERSION}' == '4.2'	Wait Until Element Is Visible	jquery=#fail2ban
	Ensure Intrusion Prevention Is Disabled

Test Block Host Period Using String Value
	Wait Until Element Is Visible	jquery=#authfail
	Click Element	jquery=#authfail
	Wait Until Element Is Visible	jquery=#bantime
	Input Text	//*[@id="bantime"]	${string_value}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Validation error.

Test Block Host Period Using Zero Value
	Input Text	//*[@id="bantime"]	${Zero}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Validation error.

Test Block Host Period Using Large Value
	Input Text	//*[@id="bantime"]	${Large_val}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Validation error.

Test Block Host Period Using Empty Value
	Input Text	//*[@id="bantime"]	${Empty}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Validation error.

Test Block Host Period Using Valid Value
	Input Text	//*[@id="bantime"]	${ONE}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Page Should Not Contain Error

Test Timeframe To Monitor Authentication Fails Using String Value
	Ensure Intrusion Prevention Is Disabled
	Click Element	jquery=#authfail
	Wait Until Element Is Visible	jquery=#findtime
	Input Text	//*[@id="findtime"]	${string_value}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Validation error.

Test Timeframe To Monitor Authentication Fails Using Large Value
	Input Text	//*[@id="findtime"]	${Large_val}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Validation error.

Test Timeframe To Monitor Authentication Fails Empty Value
	Input Text	//*[@id="findtime"]	${Empty}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Validation error.

Test Timeframe To Monitor Authentication Fails Using Zero Value
	Input Text	//*[@id="findtime"]	${Zero}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Validation error.

Test Timeframe To Monitor Authentication Fails Using Valid Value
	Input Text	//*[@id="findtime"]	${ONE}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Page Should Not Contain Error

Test Number Of Authentication Fails To Block Host Using String Value
	Ensure Intrusion Prevention Is Disabled
	Click Element	jquery=#authfail
	Wait Until Element Is Visible	jquery=#maxretry
	Input Text	//*[@id="maxretry"]	${string_value}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Validation error.

Test Number Of Authentication Fails To Block Host Using Zero Value
	Input Text	//*[@id="maxretry"]	${Zero}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Validation error.

Test Number Of Authentication Fails To Block Host Using Large Value
	Input Text	//*[@id="maxretry"]	${Large_val}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Validation error.

Test Number Of Authentication Fails To Block Host Using Empty Value
	Input Text	//*[@id="maxretry"]	${Empty}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Validation error.

Test Number Of Authentication Fails To Block Host Using Valid Value
	Input Text	//*[@id="maxretry"]	${ONE}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Page Should Not Contain Error

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Services::Open Tab
	GUI::Basic::Spinner Should Be Invisible

Ensure Intrusion Prevention Is Disabled
	${Auth_Selected}=	Run Keyword And Return Status	Checkbox Should Be Selected	jquery=#authfail
	Run Keyword If	${Auth_Selected}	Unselect Checkbox	//*[@id="authfail"]
	Run Keyword If	${Auth_Selected}	GUI::Basic::Save

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	Unselect Checkbox	//*[@id="authfail"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' > '4.2'	GUI::Basic::Security::Services::Open tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' > '4.2'	Click Element  css=#intrusion > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' < '4.2'  Unselect Checkbox	//*[@id="bootPasswdProtected"]
	Run Keyword If	'${NGVERSION}' < '4.2'  GUI::Basic::Save
	Close All Browsers