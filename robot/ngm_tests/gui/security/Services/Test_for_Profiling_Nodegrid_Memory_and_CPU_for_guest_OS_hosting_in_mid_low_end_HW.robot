*** Settings ***
Documentation	Test for Cluster With NG Profiling Memory and CPU
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE      NON-CRITICAL
Default Tags	EXCLUDEIN3_2        EXCLUDEIN4_2        EXCLUDEIN5_0        EXCLUDEIN5_2

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${PEER_IP}	${HOSTPEER}
${PEER_HOMEPAGE}	${HOMEPAGEPEER}
${COORD_IP}	${HOST}
${COORD_HOMEPAGE}	${HOMEPAGE}
${DEVICE}	test_device
${DEVICE_TYPE}	device_console
${DEVICE_IP}	127.0.0.1
${CLUSTER_LICENSE}      ${FIVE_DEVICES_CLUSTERING_LICENSE_2}
${DEFAULT_HOSTNAME}	${HOSTNAME_NODEGRID}
${COORD_HOSTNAME}	test-nodegrid-coord
${PEER_HOSTNAME}	test-nodegrid-peer
${DEV_COORD_NAME}	test-dev-nodegrid-coord
${DEV_PEER_NAME}	test-dev-nodegrid-peer
${DEFAULT_DOMAIN_NAME}	localdomain
${COORD_DOMAIN_NAME}	test-localdomain-coord
${PEER_DOMAIN_NAME}	test-localdomain-peer
${PRE_SHARED_KEY}	test-qa-123
${MESH_MODE}	no


*** Test Cases ***
Test Case Enable the Elasticsearch/kibana/Vmware/Telegrf
    SUITE:Enable Elasticsearch/Kibana/Telegraf/VMware

Test case to Configure cluster
    SUITE:Coordinator Setup
    SUITE:Peer Setup

Test Case to Verify the Cluster Configuration
    SUITE:Test case to Verify the Peer status On Co-Ordinator Side
    SUITE:Test case to Verify the Co-Ordinator status On Peer Side
    SUITE:Access Table Page Should Succeed
    SUITE:Check that Tree/Node/Maps tabs are Visible

Test Case to Disable the Elasticsearch and kibana
    GUI::Basic::Open And Login Nodegrid     PAGE=${PEER_HOMEPAGE}
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Disable Elasticsearch and Kibana
    GUI::Basic::Open And Login Nodegrid     PAGE=${COORD_HOMEPAGE}
    GUI::Basic::Spinner Should Be Invisible
	SUITE:Check After Disable Elasticsearch on Peer Side

Test Case to Enable the Elasticsearch and kibana
    GUI::Basic::Open And Login Nodegrid     PAGE=${PEER_HOMEPAGE}
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Enable Elasticsearch and Kibana

Test Case to Delete Cluster Configuration
    SUITE:Peer Teardown
    SUITE:Coordinator Teardown

Test Case to Verify the tabs are hidden depending on Elasticsearch and Kibana Disabled
    GUI::Basic::Open And Login Nodegrid     PAGE=${COORD_HOMEPAGE}
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Disable Elasticsearch/Kibana/Telegraf/VMware
    SUITE:To Check hidden tabs for Elasticsearch/kibana/Vmware
    SUITE:log Should not Succeed for elastic search/kibana/telegraf/vmware

Test Case to Re-Check Enable the Elasticsearch/kibana/Vmware/
    GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Enable Elasticsearch/Kibana/Telegraf/VMware
    SUITE:To Verify hidden tabs are Visible after Elasticsearch/kibana/Vmware
    SUITE:Disable the telegraf

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:Coordinator Setup
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Change Hostname To "${COORD_HOSTNAME}" And Domain Name To "${COORD_DOMAIN_NAME}"
	GUI::ManagedDevices::Delete Device If Exists  ${DEV_COORD_NAME}
	GUI::Cluster::Add Coordinator	${DEV_COORD_NAME}	${COORD_IP}	${PRE_SHARED_KEY}	${MESH_MODE}
	GUI::Cluster::Delete Licenses
	GUI::Cluster::Add License	${CLUSTER_LICENSE}

SUITE:Peer Setup
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Change Hostname To "${PEER_HOSTNAME}" And Domain Name To "${PEER_DOMAIN_NAME}"
	GUI::ManagedDevices::Delete Device If Exists  ${DEV_PEER_NAME}
	GUI::Cluster::Add Peer	${DEV_PEER_NAME}	${PEER_IP}	${COORD_IP}	${PRE_SHARED_KEY}
	SUITE:Wait For License Lease From Coordinator To Complete

SUITE:Peer Teardown
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Cluster::Remove Peer From Coordinator
	GUI::Basic::Open And Login Nodegrid	PAGE=${PEER_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists  ${DEV_PEER_NAME}
	GUI::Cluster::Disable Cluster
	SUITE:Change Hostname To "${DEFAULT_HOSTNAME}" And Domain Name To "${DEFAULT_DOMAIN_NAME}"

SUITE:Coordinator Teardown
	GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists  ${DEV_COORD_NAME}
	GUI::Cluster::Disable Cluster
	GUI::Cluster::Delete Licenses
	SUITE:Change Hostname To "${DEFAULT_HOSTNAME}" And Domain Name To "${DEFAULT_DOMAIN_NAME}"

SUITE:Change Hostname To "${HOSTNAME}" And Domain Name To "${DOMAIN_NAME}"
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Input Text	hostname	${HOSTNAME}
	Input Text	domainname	${DOMAIN_NAME}
	GUI::Basic::Save If Configuration Changed

SUITE:Wait For License Lease From Coordinator To Complete
	Wait Until Keyword Succeeds  2m  3s	SUITE:Check License Lease

SUITE:Check License Lease
	GUI::Basic::System::License::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//table[@id="license_table"]/tbody/tr/td[text()="Leased From"]

SUITE:Access Table Page Should Succeed
    GUI::Basic::Open And Login Nodegrid     PAGE=${COORD_HOMEPAGE}
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Wait For DEV_PEER_NAME Appear

SUITE:Check that Tree/Node/Maps tabs are Visible
    GUI::Basic::Open And Login Nodegrid     PAGE=${COORD_HOMEPAGE}
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Access::Tree::open tab
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Page Contains Element        //*[@id="tree_div"]
    Input Text      id=search_expr1     ${PEER_HOSTNAME}
    Press Keys    id=search_expr1   ENTER
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain     ${PEER_HOSTNAME}
    GUI::Basic::Access::Node::open tab
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain Element     css=.node
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Access::Map::open tab
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Page Contains Element        //*[@id="node_map"]     10s

SUITE:Check After Disable Elasticsearch on Peer Side
    GUI::Basic::Access::Tree::open tab
    GUI::Basic::Spinner Should Be Invisible
    Sleep       5s
    Wait Until Page Contains Element        //*[@id="tree_div"]
    Input Text      id=search_expr1     ${PEER_HOSTNAME}
    Press Keys    id=search_expr1   ENTER
    GUI::Basic::Spinner Should Be Invisible
    page should not contain element     ${PEER_HOSTNAME}
    GUI::Basic::Access::Node::open tab
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain Element     css=.node
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Access::Map::open tab
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Page Contains Element        //*[@id="node_map"]     10s

SUITE:Wait For DEV_PEER_NAME Appear
    Wait Until Keyword Succeeds  2m  3s	SUITE:Check DEV_PEER_NAME

SUITE:Check DEV_PEER_NAME
	GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    Input Text      id=search_expr1     ${DEV_PEER_NAME}
    Press Keys    id=search_expr1   ENTER
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain     ${DEV_PEER_NAME}

SUITE:DEV_PEER_NAME Should Not Succed
	GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    Input Text      id=search_expr1     ${DEV_PEER_NAME}
    Press Keys    id=search_expr1   ENTER
    GUI::Basic::Spinner Should Be Invisible
    page should not contain element     ${DEV_PEER_NAME}

SUITE:Test case to Verify the Peer status On Co-Ordinator Side
    GUI::Basic::Open And Login Nodegrid     PAGE=${COORD_HOMEPAGE}
    GUI::Basic::Cluster::Peers::open tab
    GUI::Basic::Spinner Should Be Invisible
    Sleep       5s
    Page Should Contain Element     xpath=//td[contains(.,'Online')]
    Page Should Contain Element     xpath=//td[contains(.,'Peer')]

SUITE:Test case to Verify the Co-Ordinator status On Peer Side
    GUI::Basic::Open And Login Nodegrid     PAGE=${PEER_HOMEPAGE}
    GUI::Basic::Cluster::Peers::open tab
    GUI::Basic::Spinner Should Be Invisible
    Sleep       5s
    Page Should Contain Element     xpath=//td[contains(.,'Online')]
    Page Should Contain Element     xpath=//td[contains(.,'Peer')]

SUITE:To Check hidden tabs for Elasticsearch/kibana/Vmware
    Sleep       10s
    GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Element Is Not Visible      css=#search_expr1
    Wait Until Element Is Not Visible     css=.submenu li:nth-child(2) > a     ####tree
    Wait Until Element Is Not Visible      css=.submenu li:nth-child(3) > a     ##node
    Wait Until Element Is Not Visible      css=.submenu li:nth-child(4) > a      ###map
    Wait Until Element Is Not Visible      css=#menu_main_dashboard img
    GUI::Tracking::Open Event List Tab
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Element Is Not Visible      css=#listEvents > .title_b           ###events
    GUI::Basic::Managed Devices::Auto Discovery::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Element Is Not Visible      css=#vm_managers > .title_b      ##vm_manager

SUITE:log Should not Succeed for elastic search/kibana/telegraf/vmware
    SUITE:To Enter the Shell

    ${OUTPUT}=	GUI:Access::Generic Console Command Output  ps -aux | grep elasticsearch
    Sleep       10s
    Should Not Match Regexp      ${OUTPUT}       daemon.*Elasticsearch

    ${OUTPUT}=	GUI:Access::Generic Console Command Output  ps -aux | grep kibana
    Sleep       10s
    Should Not Match Regexp      ${OUTPUT}       usr.*kibana

    ${OUTPUT}=	GUI:Access::Generic Console Command Output  ps -aux | grep telegraf
    Sleep       10s
    Should Not Match Regexp     ${OUTPUT}      telegraf.*d

    ${OUTPUT}=	GUI:Access::Generic Console Command Output  ps -aux | grep vmware
    Sleep       10s
    Should Not Match Regexp     ${OUTPUT}      root.*vmware.*vCenterDaemon

SUITE:To Enter the Shell
    GUI::Basic::Open And Login Nodegrid	PAGE=${COORD_HOMEPAGE}
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    wait until page contains element       //*[@id="|nodegrid"]/div[1]/div[2]/a
    Click Element       //*[@id="|nodegrid"]/div[1]/div[2]/a
    Sleep       15s
    Switch Window       nodegrid
    Sleep       10s
    Select Frame	xpath=//*[@id='termwindow']
    Press Keys      //body      RETURN

    ${OUTPUT}=	GUI:Access::Generic Console Command Output  shell sudo su -
    Sleep       10s
	Should Contain	${OUTPUT}	root@nodegrid:~#

SUITE:Disable Elasticsearch/Kibana/Telegraf/VMware
    GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible       //*[@id="elasticsearch"]
	Click Element     //*[@id="elasticsearch"]
	Click Element     //*[@id="telegraf"]
	Click Element     //*[@id="vmware"]
	Click Element       //*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:Enable Elasticsearch/Kibana/Telegraf/VMware
    GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible       //*[@id="elasticsearch"]
	Run Keyword And Continue on Failure     Select Checkbox     //*[@id="elasticsearch"]
    Run Keyword And Continue on Failure     Select Checkbox     //*[@id="kibana"]
    Run Keyword And Continue on Failure     Select Checkbox     //*[@id="vmware"]
    Run Keyword And Continue on Failure     Select Checkbox     //*[@id="telegraf"]
    Run Keyword And Continue on Failure     Click Element       //*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Verify hidden tabs are Visible after Elasticsearch/kibana/Vmware
    Sleep       10s
    GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    wait until page contains element      css=#search_expr1
    wait until page contains element    css=.submenu li:nth-child(2) > a     ####tree
    wait until page contains element      css=.submenu li:nth-child(3) > a     ##node
    wait until page contains element      css=.submenu li:nth-child(4) > a      ###map
    wait until page contains element      css=#menu_main_dashboard img
    GUI::Tracking::Open Event List Tab
    GUI::Basic::Spinner Should Be Invisible
    wait until page contains element     css=#listEvents > .title_b           ###events
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Managed Devices::Auto Discovery::Open Tab
    wait until page contains element     css=#vm_managers > .title_b      ##vm_manager

SUITE:Disable the telegraf
    GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep       10s
	wait until element is visible       //*[@id="telegraf"]
	unselect checkbox     //*[@id="telegraf"]
    Click Element       //*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:Disable Elasticsearch and Kibana
    GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep       10s
	wait until element is visible       //*[@id="elasticsearch"]
	Click Element     //*[@id="elasticsearch"]
	Click Element       //*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:Enable Elasticsearch and Kibana
    GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep       10s
	wait until element is visible       //*[@id="elasticsearch"]
	Click Element     //*[@id="elasticsearch"]
	wait until element is visible       //*[@id="kibana"]
	Click Element     //*[@id="kibana"]
	Click Element       //*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible