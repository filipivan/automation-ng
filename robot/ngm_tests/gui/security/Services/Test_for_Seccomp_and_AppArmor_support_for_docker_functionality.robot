*** Settings ***
Documentation	Test for Seccomp and AppArmor support for docker functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE      NON-CRITICAL
Default Tags	EXCLUDEIN3_2        EXCLUDEIN4_2        EXCLUDEIN5_0        EXCLUDEIN5_2

*** Variables ***
${IMG_NAME}     memcached
${LICENSE}      ${LICENSE_VM_AND_DOCKER}

*** Test Cases ***
Test Case to Add Docker License
    SUITE:Add Docker License

Test Case to Enable Docker
    SUITE:Enable Docker

Test Case to Download image and Add Docker Container
    SUITE:Add Container

Test Case to check the Continer ir running
    SUITE:Check the Container is running

Test Case to Check the Container Process in List
    SUITE:To Enter the Shell
    SUITE:Should Match the process list
    SUITE:Delete the Container

Test Case to Run the Container with Priveiledge Mode
    SUITE:Add Container with Priviledge mode

Test Case to Check the Container Process is Not in the List
    SUITE:To Enter the Shell
    SUITE:Should Not Match the process list
    SUITE:Delete the Container

Test Case to delete the image
    SUITE:Delete the Image
    SUITE:Disable Docker
    SUITE:Delete Docker License

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:Add Docker License
    GUI::Basic::System::License::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="licensevalue"]     ${LICENSE}
	GUI::Basic::Save If Configuration Changed

SUITE:Enable Docker
    GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible       //*[@id="virtualization_docker"]
	Run Keyword And Continue on Failure     Select Checkbox     //*[@id="virtualization_docker"]
	Run Keyword And Continue on Failure     Click Element       //*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Enter the Shell
    SUITE:Setup
    GUI::Basic::Access::Table::open tab
    GUI::Basic::Spinner Should Be Invisible
    wait until page contains element       //*[@id="|nodegrid"]/div[1]/div[2]/a
    Click Element       //*[@id="|nodegrid"]/div[1]/div[2]/a
    Sleep       20s
    Run Keyword If  '${NGVERSION}'=='5.4'    Switch Window       nodegrid
    Run Keyword If  '${NGVERSION}'>'5.4'    Switch Window       nodegrid - Console
    Sleep       15s
    Select Frame	xpath=//*[@id='termwindow']
    Press Keys      //body      RETURN

    ${OUTPUT}=	GUI:Access::Generic Console Command Output  shell sudo su -
    Sleep       15s
	Should Contain	${OUTPUT}	root@nodegrid:~#

SUITE:Add Container
    GUI::Basic::Applications::Docker::open tab
    GUI::Basic::Spinner Should Be Invisible
    Sleep       20s
    Select Frame        //iframe
    GUI::Basic::Spinner Should Be Invisible
    wait until page contains element        //*[@id="containers-images"]/div/section/header/div/div/button
    execute javascript  document.getElementsByClassName("link-button")[0].scrollIntoView()
    Click Element       xpath=//button[contains(.,' Get new image')]
    wait until page contains element        //*[@id="containers-search-image-dialog"]/div/div
    input text      //*[@id="containers-search-image-search"]      ${IMG_NAME}
    Press Keys    css=#containers-search-image-search   ENTER
    page should contain element     //*[@id="containers-search-image-results"]/table/tbody
    Sleep       5s
    click element       xpath=//div[@id='containers-search-image-results']/table/tbody/tr/td[2]
    click element       css=#containers-search-download
    Sleep       5s
    wait until page contains element        //*[@id="containers-images"]/div/section/table/tbody/tr[1]/td[4]
    execute javascript  document.getElementsByClassName("pf-c-button pf-m-secondary play-button btn-sm")[0].scrollIntoView()
    click element       //*[@id="containers-images"]/div/section/table/tbody/tr[1]/td[4]
    wait until page contains element        //*[@id="containers_run_image_dialog"]/div/div/div[2]
    Sleep       5s
    execute javascript  document.getElementById("containers-run-image-run").scrollIntoView()
    wait until page contains element        //*[@id="image-details-run"]
    click element       //*[@id="image-details-run"]
    Sleep       5s
    execute javascript  document.getElementById("containers-run-image-run").scrollIntoView()
    wait until page contains element        //*[@id="containers-run-image-run"]
    click element       //*[@id="containers-run-image-run"]
    sleep       5s
    wait until page contains element        //*[@id="containers-containers"]/section/table/tbody

SUITE:Check the Container is running
    SUITE:Setup
    GUI::Basic::Applications::Docker::open tab
    GUI::Basic::Spinner Should Be Invisible
    Sleep       20s
    Select Frame        //iframe
    GUI::Basic::Spinner Should Be Invisible
    wait until page contains element        //*[@id="containers-images"]/div/section/header/div/div/button
    execute javascript  document.getElementsByClassName("link-button")[0].scrollIntoView()
    wait until page contains element        //*[@id="containers-containers"]/section/table/tbody

SUITE:Delete the Container
    SUITE:Setup
    GUI::Basic::Applications::Docker::open tab
    GUI::Basic::Spinner Should Be Invisible
    Sleep       20s
    Select Frame        //iframe
    GUI::Basic::Spinner Should Be Invisible
    execute javascript  document.getElementsByClassName("pf-c-button pf-m-secondary play-button btn-sm")[0].scrollIntoView()
    click element       //*[@id="containers-images"]/div/section/table/tbody/tr[1]/td[4]
    wait until page contains element        //*[@id="containers_run_image_dialog"]/div/div/div[2]
    Sleep       10s
    wait until page contains element        xpath=//div[3]/div[2]/table/tbody/tr/td[2]
    click element       xpath=//div[3]/div[2]/table/tbody/tr/td[2]
    wait until page contains element        //*[@id="container-details"]/div[2]/div/div[2]
    click element       xpath=//button[contains(.,'Delete')]
    wait until page contains element        //*[@id="confirmation-dialog"]/div/div
    click element       //*[@id="confirmation-dialog-confirm"]
    Sleep       3s
    wait until page contains element        //*[@id="confirmation-dialog"]/div
    click element       //*[@id="confirmation-dialog-confirm"]

SUITE:Should Match the process list
    ${OUTPUT}=	GUI:Access::Generic Console Command Output  aa-status
    Sleep       10s
    Should Match Regexp     ${OUTPUT}      .*memcached.*

    Sleep       5s

    ${OUTPUT}=	GUI:Access::Generic Console Command Output  grep Seccomp /proc/*/status | grep -v 0$
    Sleep       5s
    Should Match Regexp     ${OUTPUT}      .*Seccomp_filters.*

SUITE:Should Not Match the process list
    ${OUTPUT}=	GUI:Access::Generic Console Command Output  aa-status
    Sleep       10s
    Should Not Match Regexp     ${OUTPUT}      .*memcached.*

    Sleep       5s

    ${OUTPUT}=	GUI:Access::Generic Console Command Output  grep Seccomp /proc/*/status | grep -v 0$
    Sleep       10s
    Should Not Match Regexp     ${OUTPUT}      .*Seccomp_filters.*

SUITE:Add Container with Priviledge mode
    SUITE:Setup
    GUI::Basic::Applications::Docker::open tab
    GUI::Basic::Spinner Should Be Invisible
    Sleep       20s
    Select Frame        //iframe
    GUI::Basic::Spinner Should Be Invisible
    wait until page contains element        //*[@id="containers-images"]/div/section/header/div/div/button
    execute javascript  document.getElementsByClassName("link-button")[0].scrollIntoView()
    Sleep       5s
    click element       //*[@id="containers-images"]/div/section/table/tbody/tr[1]/td[4]
    execute javascript  document.getElementsByClassName("pf-c-button pf-m-secondary play-button btn-sm")[0].scrollIntoView()
    wait until page contains element        //*[@id="containers_run_image_dialog"]/div/div/div[2]
    click element       //*[@id="image-details-run"]
    Sleep       10s
    Click Element       xpath=//input[@id='containers-run-privileged']
    execute javascript  document.getElementById("containers-run-image-run").scrollIntoView()
    wait until page contains element        //*[@id="image-details-run"]
    click element       //*[@id="containers-run-image-run"]

SUITE:Delete the Image
    SUITE:Setup
    GUI::Basic::Applications::Docker::open tab
    GUI::Basic::Spinner Should Be Invisible
    Sleep       20s
    Select Frame        //iframe
    GUI::Basic::Spinner Should Be Invisible
    wait until page contains element        //*[@id="containers-images"]/div/section/header/div/div/button
    execute javascript  document.getElementsByClassName("link-button")[0].scrollIntoView()
    Sleep       5s
    click element       //*[@id="containers-images"]/div/section/table/tbody/tr[1]/td[4]
    click element       //*[@id="image-details-delete"]
    Sleep       3s
    wait until page contains element        //*[@id="delete-image-confirmation-dialog"]/div/div
    click element       //*[@id="delete-image-confirmation-dialog-confirm"]
    sleep       3s
    execute javascript  document.getElementsByClassName("link-button")[0].scrollIntoView()
    wait until page does not contain        //*[@id="containers-images"]/div/section/header/div/div/button

SUITE:Disable Docker
    SUITE:Setup
    GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible       //*[@id="virtualization_docker"]
	unselect checkbox     //*[@id="virtualization_docker"]
	Click Element       //*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:Delete Docker License
    GUI::Basic::System::License::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete All Rows In Table If Exists	\#license_table	False