*** Settings ***
Documentation	Test Boot Protection
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Variable ***
${pass}     password123

*** Test Cases ***
Test boot protection is present
    GUI::Basic::Spinner Should Be Invisible
    Run Keyword If	'${NGVERSION}' > '4.2'	Click Element  css=#intrusion > .title_b
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be Visible	bootPasswdProtected

Test rescue mode operation is present
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be Visible	rescpwd

Enable boot protection with password
    Select Checkbox         jquery=#rescpwd
    Select Checkbox         jquery=#bootPasswdProtected
    Input Text          jquery=#bootPassword        ${pass}
    Input Text          jquery=#bootPasswordConf    ${pass}
    Click Element           jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible

Disable boot protection with password
    Unselect Checkbox       jquery=#bootPasswdProtected
    Unselect Checkbox       jquery=#bootPasswdProtected
    Click Element           jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid