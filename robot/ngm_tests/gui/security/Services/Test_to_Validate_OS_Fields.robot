*** Settings ***
Documentation	Validate OS fields
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	      NON-CRITICAL
Default Tags	EXCLUDEIN4_2        EXCLUDEIN5_0		EXCLUDEIN5_2

*** Variables ***
${Ubuntu_os}		Ubuntu
${Windows_os}		Windows
${Alpine_os}		Alpine
${Fedora_os}		Fedora

*** Test Cases ***
Test Case to Enable Qemu/Kvm
	SUITE:Enable Qemu/Kvm

Test Case to Validate Ubuntu feilds
	SUITE:Validate OS Fields
	input text		xpath=//div[@id='os-select-group']/div[2]/div/div/div/input		${Ubuntu_os}
	wait until page contains element		//*[@id="os-select"]
	sleep		10s
	page should contain		Ubuntu 18.04 LTS (Bionic Beaver)
	page should contain		Ubuntu 20.04 LTS (Focal Fossa)
	page should contain		Ubuntu 21.04 (Hirsute Hippo)
	page should contain		Ubuntu 22.04 LTS (Jammy Jellyfish)
	page should contain		Ubuntu 21.10 (Impish Indri)

Test Case to Validate Windows feilds
	SUITE:Validate OS Fields
	input text		xpath=//div[@id='os-select-group']/div[2]/div/div/div/input		${Windows_os}
	wait until page contains element		//*[@id="os-select"]
	sleep		10s
	page should contain		Microsoft Windows Server 2022 (10.0)
	page should contain		Microsoft Windows Server 2019 (10.0)
	page should contain		Microsoft Windows 10 (10.0)
	page should contain		Microsoft Windows 8.1 (6.3)
	page should contain		Microsoft Windows 7 (6.1)
	page should contain		Microsoft Windows Server 2012 (6.3)
	page should contain		Microsoft Windows Server 2016 (10.0)
	page should contain		Microsoft Windows Server 2012 R2 (6.3)

Test Case to Validate Alpine feilds
	SUITE:Validate OS Fields
	input text		xpath=//div[@id='os-select-group']/div[2]/div/div/div/input		${Alpine_os}
	wait until page contains element		//*[@id="os-select"]
	sleep		10s
	page should contain		Alpine Linux 3.11
	page should contain		Alpine Linux 3.12
	page should contain		Alpine Linux 3.13
	page should contain		Alpine Linux 3.14
	page should contain		Alpine Linux 3.15

Test Case to Validate Fedora feilds
	SUITE:Validate OS Fields
	input text		xpath=//div[@id='os-select-group']/div[2]/div/div/div/input		${Fedora_os}
	wait until page contains element		//*[@id="os-select"]
	sleep		10s
	page should contain		Fedora Silverblue (unknown)
	page should contain		Fedora Silverblue Rawhide
	page should contain		Fedora CoreOS (Stable)
	page should contain		Fedora 33
	page should contain		Fedora Silverblue 33
	page should contain		Fedora Silverblue 34
	page should contain		Fedora Silverblue 35
	page should contain		Fedora Silverblue 36
	page should contain		Fedora 35
	page should contain		Fedora 34
	page should contain		Fedora Linux 36
	page should contain		Fedora CoreOS (Testing)
	page should contain		Fedora CoreOS (Next)
	page should contain		Fedora Rawhide
	page should contain		Fedora (unknown)

Test Case to Disable Qemu/kvm
	SUITE:Disable Qemu/Kvm

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:Enable Qemu/Kvm
	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible       xpath=//input[@id='virtualization_libvirt']
	Run Keyword And Continue on Failure     Select Checkbox     xpath=//input[@id='virtualization_libvirt']
	Run Keyword And Continue on Failure     Click Element       //*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:Validate OS Fields
	GUI::Basic::Applications::Virtual Machines::open tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep		30s
	Select Frame        //iframe
	wait until page contains element		xpath=//button[contains(.,'Create VM')]
	Click Element		xpath=//button[contains(.,'Create VM')]
	Select From List By value		//*[@id="source-type"]		file
	wait until page contains element		//*[@id="source-file-group"]/div[2]
	set focus to element		//*[@id="source-file-group"]/div[2]
	click element		//*[@id="source-file-group"]/div[2]
	sleep		5s
	wait until page contains element		//*[@id="source-file-group"]/div[2]
	wait until page contains element		xpath=//span[contains(.,'Operating system')]
	set focus to element		xpath=//span[contains(.,'Operating system')]
	sleep		10s
	execute javascript  document.getElementById("os-select-group").scrollIntoView()
	Sleep		10s
	click element		//*[@id="os-select-group"]/div[2]/div[1]

SUITE:Disable Qemu/Kvm
	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible       xpath=//input[@id='virtualization_libvirt']
	unselect checkbox     xpath=//input[@id='virtualization_libvirt']
	Click Element       //*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
