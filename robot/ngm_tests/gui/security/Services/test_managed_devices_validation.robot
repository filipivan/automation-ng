*** Settings ***
Documentation	Test to validate the checkboxes about managed devices in security -> services page
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test Device access enforced via user group authorization
	Wait Until Element Is Visible	jquery=#authorization
	Page Should Contain	Managed Devices
	Page Should Contain	Device access enforced via user group authorization
	Select Checkbox	jquery=#authorization
	Unselect Checkbox	jquery=#authorization
	${DEVICE_ACCESS_STATUS}=	Run Keyword And Return Status	Checkbox Should Be Selected	jquery=#authorization
	Run Keyword If	${DEVICE_ACCESS_STATUS}	Unselect Checkbox	//*[@id="discEnabled"]
	Run Keyword If	${DEVICE_ACCESS_STATUS}	GUI::Basic::Save

Test Enable Autodiscovery
	Checkbox Should Be Selected	jquery=#discEnabled
	Page Should Contain	Enable Autodiscovery
	${ENABLE_AUTO_STATUS}=	Run Keyword And Return Status	Checkbox Should Not Be Selected	jquery=#discEnabled
	Run Keyword If	${ENABLE_AUTO_STATUS}	Select Checkbox	//*[@id="discEnabled"]
	Run Keyword If	${ENABLE_AUTO_STATUS}	GUI::Basic::Save

Test DHCP lease controlled by autodiscovery rules
	Page Should Contain Checkbox	//*[@id="discRules"]
	Page Should Contain	DHCP lease controlled by autodiscovery rules
	${DHCP_CHECKBOX_STATUS}=	Run Keyword And Return Status	Checkbox Should Be Selected	//*[@id="discRules"]
	Run Keyword If	${DHCP_CHECKBOX_STATUS}	Unselect Checkbox	//*[@id="discEnabled"]
	Run Keyword If	${DHCP_CHECKBOX_STATUS}	GUI::Basic::Save

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Open Services tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	Close All browsers

GUI::Security::Open Services tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]
	...	Opens the NodeGrid Security::Authorization page
	...     == ARGUMENTS ==
	...     -	SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Services page is open and all elements are accessible
	[Tags]	GUI	SECURITY
	GUI::Security::Open Security
	Click Element	xpath=(//a[contains(text(),'Services')])[2]
	Wait Until Element Is Visible	jquery=#webService
	Wait Until Element Is Visible	jquery=#services
	Wait Until Element Is Visible	jquery=#sshServer
	Wait Until Element Is Visible	jquery=#crypto
	Wait Until Element Is Visible	jquery=#managedDevices