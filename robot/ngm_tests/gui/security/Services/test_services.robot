*** Settings ***
Documentation	Test common behaviour in security -> services page
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Variable ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${Empty}

*** Test Cases ***
Test Inital Servies Rescue Mode
	[Tags]	NON-CRITICAL	NEED-REVIEW
    GUI::Basic::Spinner Should Be Invisible
    Run Keyword If	'${NGVERSION}' > '4.2'	Click Element  css=#intrusion > .title_b
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Element Is Visible	jquery=#rescpwd
    Select Checkbox         jquery=#rescpwd
    Click Element           jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    Unselect Checkbox       jquery=#rescpwd
    Click Element           jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible

Test Inital Servies Password Protected Boot Setup
	[Tags]	NON-CRITICAL	NEED-REVIEW
    Run Keyword If	'${NGVERSION}' > '4.2'	GUI::Basic::Security::Services::Open tab
    GUI::Basic::Spinner Should Be Invisible
    Run Keyword If	'${NGVERSION}' > '4.2'	Click Element  css=#intrusion > .title_b
    Wait Until Element Is Visible	jquery=#bootPasswdProtected
    Select Checkbox         jquery=#bootPasswdProtected
    Click Element           jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
#    Page Should Contain     This password protects bios and bootloader
    Wait Until Element Is Visible	jquery=#bootPassword
    Wait Until Element Is Visible	jquery=#bootPasswordConf

Test Inital Servies Password Protected Boot Empty
	[Tags]	NON-CRITICAL	NEED-REVIEW
    Input Text          jquery=#bootPassword        ${Empty}
    Input Text          jquery=#bootPasswordConf    ${Empty}
    Click Element           jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg    Validation error.

Test Inital Servies Password Protected Boot Invalid
	[Tags]	NON-CRITICAL	NEED-REVIEW
    Input Text          jquery=#bootPassword            inv
    Input Text          jquery=#bootPasswordConf        inv
    Click Element           jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg    Validation error.

Test Inital Servies Password Protected Boot Mismatch Passwd
	[Tags]	NON-CRITICAL	NEED-REVIEW
	Input Text          jquery=#bootPassword            inv
    Input Text          jquery=#bootPasswordConf        inv1
    Click Element           jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg    Password mismatch.

Test Inital Servies Password Protected Boot Valid
	[Tags]	NON-CRITICAL	NEED-REVIEW
	Input Text          jquery=#bootPassword            Valid
    Input Text          jquery=#bootPasswordConf        Valid
    Click Element           jquery=#saveButton
    GUI::Basic::Page Should Not Contain Error
    GUI::Basic::Spinner Should Be Invisible
    Unselect Checkbox       jquery=#bootPasswdProtected
    Click Element           jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	Wait Until Keyword Succeeds	3x	10s	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	Close All Browsers