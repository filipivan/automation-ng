*** Settings ***
Documentation	Test common behaviour in security -> services page
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variable ***

*** Test Cases ***
Test Web Service
	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#crypto
	Page Should Contain Checkbox	//*[@id="httpsTLS1_2"]
	Page Should Contain Checkbox	//*[@id="httpsTLS1_1"]
	Page Should Contain Checkbox	//*[@id="httpsTLS1"]
	Page Should Contain	Changes affecting HTTP and HTTPS services will terminate all HTTP sessions
	Select Radio Button	httpsSSLcipher	custom
	Page Should Contain Element	//*[@id="httpsSSLcipherString"]

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid