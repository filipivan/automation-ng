*** Settings ***
Documentation	Test common behaviour in security -> services page
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test SSH
	Wait Until Element Is Visible	jquery=#sshServer
	Page Should Contain Checkbox	//*[@id="allow"]
	Page Should Contain Element	//*[@id="sshport"]
	Page Should Contain Element	//*[@id="sshciphers"]
	Page Should Contain Element	//*[@id="sshmacs"]
	Page Should Contain Element	//*[@id="sshkexalgorithms"]

Test SSH Fields
	gui::auditing::auto input tests	sshport	a	1-2	12.3	12/3	23
	input text	jquery=#sshport	22
	click element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Not Visible	xpath=//*[@id='loading']	timeout=20s
	Check Error Thrown	sshciphers	qwer
	Check Error Thrown	sshmacs	qwer
	Check Error Thrown	sshkexalgorithms	qwer
	Check Error Thrown	httpport	qwer
	Check Error Thrown	httpsport	qwer

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

Check Error Thrown
	[Arguments]	${LOC}	${TST}
	Input Text	jquery=#${LOC}	${TST}
	Click element	jquery=#saveButton
	Wait Until Element Is Not Visible	xpath=//*[@id='loading']	timeout=20s
	${TXT}=	Execute Javascript	var er=document.getElementById('${LOC}');if(er==null){return false;}var ro=er.parentNode.nextSibling;if(ro==null){return false;}return true;
	Should Be True	${TXT}