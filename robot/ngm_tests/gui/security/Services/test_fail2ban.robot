*** Settings ***
Documentation	Test Fail2Ban ignoreip
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX		NON-CRITICAL
Default Tags	EXCLUDEIN4_2

*** Variable ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${ban_time}	1
${find_time}	1
${max_retry}	5
${ignoreip}	${CLIENTIP}

*** Test Cases ***
Go to Security:Services:General Services and then enable block host with multiple auth fails.
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	jquery=#authfail
	Input Text	id=bantime	2
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

Attempt to login for the DUT with wrong passwords for more than 5 times.
	GUI::Basic::Logout
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#username	jquery=input[name='password']
	GUI::Basic::Login Failure	test	aaaaaAAAAA!!!!!12345
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#username	jquery=input[name='password']
	GUI::Basic::Login Failure	test	aaaaaAAAAA!!!!!12345
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#username	jquery=input[name='password']
	GUI::Basic::Login Failure	test	aaaaaAAAAA!!!!!12345
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#username	jquery=input[name='password']
	GUI::Basic::Login Failure	test	aaaaaAAAAA!!!!!12345
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#username	jquery=input[name='password']
	GUI::Basic::Login Failure	test	aaaaaAAAAA!!!!!12345
	Sleep	150

Change the Period Host will stay blocked timing to 5 mins.
	Close all Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Services::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Auto Input Tests	bantime	a	9999999	1.1	20

Change the Timeframe to monitor authentication fails to 5 mins.
	Close all Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Services::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Auto Input Tests	findtime	a	9999999	1.1	20

Change the Number of authentication fails to block host to 2.
	Close all Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Services::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Auto Input Tests	maxretry	a	9999999	1.1	20

Chnage the parameters to default and Whitelist the ip.
	Close all Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Services::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=bantime	${ban_time}
	Input Text	id=findtime	${find_time}
	Input Text	id=maxretry	${max_retry}
	Input Text	id=ignoreip	${ignoreip}
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

Attempt to login for the DUT with wrong passwords for more than 5 times with Whitelist the ip.
	GUI::Basic::Logout
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#username	jquery=input[name='password']
	GUI::Basic::Login Failure	test	aaaaaAAAAA!!!!!12345
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#username	jquery=input[name='password']
	GUI::Basic::Login Failure	test	aaaaaAAAAA!!!!!12345
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#username	jquery=input[name='password']
	GUI::Basic::Login Failure	test	aaaaaAAAAA!!!!!12345
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#username	jquery=input[name='password']
	GUI::Basic::Login Failure	test	aaaaaAAAAA!!!!!12345
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#username	jquery=input[name='password']
	GUI::Basic::Login Failure	test	aaaaaAAAAA!!!!!12345
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#username	jquery=input[name='password']
	GUI::Basic::Login Failure	test	aaaaaAAAAA!!!!!12345
	Sleep	150s

Disable block host with multiple auth fails.
	Close all Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Services::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Unselect Checkbox	jquery=#authfail
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

Attempt to login for the DUT with wrong passwords for more than 5 times with multiple auth fails disabled.
	GUI::Basic::Logout
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#username	jquery=input[name='password']
	GUI::Basic::Login Failure	test	aaaaaAAAAA!!!!!12345
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#username	jquery=input[name='password']
	GUI::Basic::Login Failure	test	aaaaaAAAAA!!!!!12345
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#username	jquery=input[name='password']
	GUI::Basic::Login Failure	test	aaaaaAAAAA!!!!!12345
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#username	jquery=input[name='password']
	GUI::Basic::Login Failure	test	aaaaaAAAAA!!!!!12345
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#username	jquery=input[name='password']
	GUI::Basic::Login Failure	test	aaaaaAAAAA!!!!!12345
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#username	jquery=input[name='password']
	GUI::Basic::Login Failure	test	aaaaaAAAAA!!!!!12345
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#username	jquery=input[name='password']
	GUI::Basic::Login Failure	test	aaaaaAAAAA!!!!!12345

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Services::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	Close all Browsers