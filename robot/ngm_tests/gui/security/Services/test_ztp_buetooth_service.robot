*** Settings ***
Documentation	Test ZTP bluetooth services
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	NO-ENV    EXCLUDEIN3_2


*** Variable ***

*** Test Cases ***
Test Bluetooth settings are enabled by default in Security Services
    GUI::Basic::Spinner Should Be Invisible
    Checkbox Should Be Selected	ztp
    Checkbox Should Be Selected	bluetooth
    Checkbox Should Be Selected	bluetoothAllowNew

Test Bluetooth settings are enabled by default in Network settings
    GUI::Basic::Network::Settings::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Checkbox Should Be Selected	bluetoothEnable

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Services::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
