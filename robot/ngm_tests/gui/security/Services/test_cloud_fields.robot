*** Settings ***
Documentation	Test common behaviour in security -> services page
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test Checkbox Exists
	Wait Until Element Is Visible	jquery=#services
	Page Should Contain Checkbox	//*[@id="cloud_enable"]

Test Fields Presence Cloud
	Select Checkbox	jquery=#cloud_enable
	Element Should Be Visible	jquery=#pass_prot
	Select Checkbox	jquery=#pass_prot
	GUI::Basic::Elements Should Be Visible	jquery=#file_pass	jquery=#file_pass_conf	jquery=#file_encrypt

Test Passwords
	SUITE:Password Check	qwer	asdf	${TRUE}
	SUITE:Password Check	12345678901234567890123456789012345678901	12345678901234567890123456789012345678901	${TRUE}
	SUITE:Password Check	${EMPTY}	${EMPTY}	${TRUE}
	SUITE:Password Check	zxcv	zxcv	${FALSE}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Unselect Checkbox	jquery=#pass_prot
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible	SPINNER_TIMEOUT=2m
	Unselect Checkbox	jquery=#cloud_enable
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible	SPINNER_TIMEOUT=2m
	GUI::Basic::Logout And Close Nodegrid

SUITE:Password Check
	[Arguments]	${P1}	${P2}	${EXP}
	Input text	jquery=#file_pass	${P1}
	input text	jquery=#file_pass_conf	${P2}
	Click element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	${TST}=	Execute Javascript	var er=document.getElementById('file_pass').parentNode.nextSibling;if(er==null){return false;}return true;
	Run Keyword If	${EXP}	Should Be True	${TST}
	...	ELSE	Should Not Be True	${TST}
