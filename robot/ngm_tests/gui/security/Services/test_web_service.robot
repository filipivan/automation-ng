*** Settings ***
Documentation	Test common behaviour in security -> services page
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test Web Service
	Wait Until Element Is Visible	jquery=#webService
	Page Should Contain Checkbox	//*[@id="http"]
	Page Should Contain Checkbox	//*[@id="https"]
	Page Should Contain	Changes affecting HTTP and HTTPS services will terminate all HTTP sessions

Test HTTP Port
	${Http_Port_Disabled}=	Run Keyword And Return Status	Checkbox Should Not Be Selected	jquery=#http
	Run Keyword If	${Http_Port_Disabled}	Select Checkbox	jquery=#http
	Run Keyword If	${Http_Port_Disabled}	GUI::Basic::Save
	Page Should Contain Element	//*[@id="httpport"]

Test HTTPS Port
	${Http_Port_Disabled}=	Run Keyword And Return Status	Checkbox Should Not Be Selected	jquery=#https
	Run Keyword If	${Http_Port_Disabled}	Select Checkbox	jquery=#https
	Run Keyword If	${Http_Port_Disabled}	GUI::Basic::Save
	Page Should Contain Element	//*[@id="httpsport"]
	Page Should Contain Checkbox	//*[@id="redirect"]

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers
