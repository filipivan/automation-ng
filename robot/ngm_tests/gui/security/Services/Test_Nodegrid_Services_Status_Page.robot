*** Settings ***
Documentation	Test to Nodegrid Services Status Page
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	      NON-CRITICAL
Default Tags	EXCLUDEIN3_2        EXCLUDEIN4_2        EXCLUDEIN5_0

*** Variables ***
${Url_Services Status Page}		https://${HOST}/services/status
${Username}	${DEFAULT_USERNAME}
${Password}	${DEFAULT_PASSWORD}

*** Test Cases ***
Test Case to Check the Nodegrid Services Status Page Is Visible
	SUITE:To Check the Services Status Page and Reboot on Service status Page are Enabled
	SUITE:To Login Service Status Page with admin user

Test Case to Disable the Nodegrid Services and Reboot Services Status Page
	SUITE:To Disable the Services Status Page and Reboot on Service status Page
	SUITE:To Check Service Status Page Should Show Error

Test Case to Set Default Config
	SUITE:To Set Enable the Services Status Page and Reboot on Service status Page

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:To Check the Services Status Page and Reboot on Service status Page are Enabled
	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible		xpath=//input[@id='status_page_enable']
	checkbox should be selected		xpath=//input[@id='status_page_enable']
	checkbox should be selected		xpath=//input[@id='status_page_reboot']

SUITE:To Login Service Status Page with admin user
	GUI::Basic::Open NodeGrid	${Url_Services Status Page}
	Input Text	username	${USERNAME}
	Input Text	password	${PASSWORD}
	Click Element	//*[@id="login"]/input
	Sleep		30s
	Run Keyword If  '${NGVERSION}'<'5.4'		Title Should be     Services Status
	Run Keyword If  '${NGVERSION}'>='5.4'		Title Should be     Services Status - Nodegrid
	wait until page contains		Services :: Status Table
	wait until element is visible		//*[@id="refresh-icon"]
	wait until element is visible		//*[@id="rebootButton"]

SUITE:To Disable the Services Status Page and Reboot on Service status Page
	SUITE:Setup
	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible		xpath=//input[@id='status_page_enable']
	unselect checkbox		xpath=//input[@id='status_page_enable']
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible	SPINNER_TIMEOUT=60s

SUITE:To Check Service Status Page Should Show Error
	Open Browser 	${Url_Services Status Page}
	page should contain		The requested URL was not found on this server.

SUITE:To Set Enable the Services Status Page and Reboot on Service status Page
	SUITE:Setup
	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible		xpath=//input[@id='status_page_enable']
	select checkbox		xpath=//input[@id='status_page_enable']
	select checkbox		xpath=//input[@id='status_page_reboot']
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible	SPINNER_TIMEOUT=60s
