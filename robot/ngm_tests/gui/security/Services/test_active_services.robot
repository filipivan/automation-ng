*** Settings ***
Documentation	Test common behaviour in security -> services page
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variable ***
${String}               str
${Zero}                 0
${Large_Num}            99999
${Empty}
${Valid_Value}          101
${Temp}

*** Test Cases ***
Test All Checkbox Exist
    Wait Until Element Is Visible	    jquery=#services
    Page Should Contain Checkbox        //*[@id="usbD"]
    Page Should Contain Checkbox        //*[@id="rpc"]
    Page Should Contain Checkbox        //*[@id="ftp"]
    Page Should Contain Checkbox        //*[@id="snmp"]
    Page Should Contain Checkbox        //*[@id="telnet"]
    Page Should Contain Checkbox        //*[@id="telnetDev"]
    Page Should Contain Checkbox        //*[@id="icmp"]
    Page Should Contain Checkbox        //*[@id="usbip"]
    #Page Should Contain Checkbox        //*[@id="virtualization"]
    Page Should Contain Checkbox        //*[@id="autoEnroll"]
    Run Keyword If  '${NGVERSION}'<='5.6'		Page Should Contain Checkbox        //*[@id="essecure"]
    Page Should Contain Checkbox        //*[@id="vmserial"]
    Page Should Contain Checkbox        //*[@id="ztp"]
    Page Should Contain Checkbox        //*[@id="pxeState"]
    Page Should Contain Element         //*[@id="clusterport"]
    Run Keyword If  '${NGVERSION}'<='5.6'        Page Should Contain Element         //*[@id="esport"]
    Run Keyword If  '${NGVERSION}'>'5.6'        Page Should Contain Element         //*[@id="osport"]

Test Get Initial Cluster Value
    ${T}=     Get Value    //*[@id="clusterport"]
    Set Global Variable     ${Temp}     ${T}

Test Cluster TCP Port Validation With Empty
    SUITE:Test Port Value  //*[@id="clusterport"]      ${Empty}     True

Test Cluster TCP Port Validation With Zero
    SUITE:Test Port Value  //*[@id="clusterport"]      ${Zero}     True

Test Cluster TCP Port Validation With Large Num
    SUITE:Test Port Value  //*[@id="clusterport"]      ${Large_Num}     True

Test Cluster TCP Port Validation With Valid Value
    SUITE:Test Port Value  //*[@id="clusterport"]      ${Valid_Value}     False

Test Reset Cluster Value
    SUITE:Test Port Value  //*[@id="clusterport"]      ${Temp}     False

Test Get Initial Search Engine Value
	[Tags]		EXCLUDEIN5_8	NON-CRITICAL
    ${T}=     Get Value    //*[@id="esport"]
    Set Global Variable     ${Temp}     ${T}

Test Get Initial Search Engine Value on 5.8 Version
	[Tags]	EXCLUDEIN5_6	EXCLUDEIN5_4	EXCLUDEIN5_2	EXCLUDEIN5_0	EXCLUDEIN4_2	NON-CRITICAL
    ${T}=     Get Value    //*[@id="osport"]
    Set Global Variable     ${Temp}     ${T}


Test Search Engine TCP Port Validation With Empty
    Run Keyword If  '${NGVERSION}'<='5.6'		SUITE:Test Port Value  //*[@id="esport"]      ${Empty}     True
    Run Keyword If  '${NGVERSION}'>'5.6'		SUITE:Test Port Value  //*[@id="osport"]      ${Empty}     True

Test Search Engine TCP Port Validation With Zero
    Run Keyword If  '${NGVERSION}'<='5.6'		SUITE:Test Port Value  //*[@id="esport"]      ${Zero}     True
    Run Keyword If  '${NGVERSION}'>'5.6'		SUITE:Test Port Value  //*[@id="osport"]      ${Zero}     True

Test Search Engine TCP Port Validation With Large Num
    Run Keyword If  '${NGVERSION}'<='5.6'		SUITE:Test Port Value  //*[@id="esport"]      ${Large_Num}     True
    Run Keyword If  '${NGVERSION}'>'5.6'		SUITE:Test Port Value  //*[@id="osport"]       ${Large_Num}     True

Test Search Engine TCP Port Validation With Valid Value
    Run Keyword If  '${NGVERSION}'<='5.6'		SUITE:Test Port Value  //*[@id="esport"]      ${Valid_Value}     False
    Run Keyword If  '${NGVERSION}'>'5.6'		SUITE:Test Port Value  //*[@id="osport"]      ${Valid_Value}     False

Test Reset Search Engine Value
    Run Keyword If  '${NGVERSION}'<='5.6'		SUITE:Test Port Value  //*[@id="esport"]      ${Temp}     False
    Run Keyword If  '${NGVERSION}'>'5.6'		SUITE:Test Port Value  //*[@id="osport"]       ${Temp}     False

Test Enable Telnet Service to Nodegrid
	${Telnet_Init_Disabled}=	Run Keyword And Return Status	Checkbox Should Not Be Selected	jquery=#telnet
	Run Keyword If	${Telnet_Init_Disabled}      Select Checkbox    jquery=#telnet
	Run Keyword If	${Telnet_Init_Disabled}     GUI::Basic::Save
	Page Should Contain Element     //*[@id="telnetPort"]
	${T}=     Get Value    //*[@id="telnetPort"]
    Set Global Variable     ${Temp}     ${T}

Test Enable Telnet Service to Nodegrid With Empty
    SUITE:Test Port Value  //*[@id="telnetPort"]      ${Empty}     True

Test Enable Telnet Service to Nodegrid With Zero
    SUITE:Test Port Value  //*[@id="telnetPort"]      ${Zero}     True

Test Enable Telnet Service to Nodegrid With Large Value
    SUITE:Test Port Value  //*[@id="telnetPort"]      ${Large_Num}     True

Test Enable Telnet Service to Nodegrid With Valid Value
    SUITE:Test Port Value  //*[@id="telnetPort"]      ${Valid_Value}    False

Test Reset Enable Telnet Service to Nodegrid Value
    SUITE:Test Port Value  //*[@id="telnetPort"]      ${Temp}     False

Test Enable GRPC Service to Nodegrid
	Select Checkbox         jquery=#grpc
	Page Should Contain Element     //*[@id="grpcPort"]
	${T}=     Get Value    //*[@id="grpcPort"]
    Set Global Variable     ${Temp}     ${T}

Test Enable GRPC Service to Nodegrid With Empty
    SUITE:Test Port Value  //*[@id="grpcPort"]      ${Empty}     True

Test Enable GRPC Service to Nodegrid With Zero
    SUITE:Test Port Value  //*[@id="grpcPort"]      ${Zero}     True

Test Enable GRPC Service to Nodegrid With Large Value
    SUITE:Test Port Value  //*[@id="grpcPort"]      ${Large_Num}     True

Test Enable GRPC Service to Nodegrid With Valid Value
    SUITE:Test Port Value  //*[@id="grpcPort"]      ${Valid_Value}    False

Test Reset GRPC Telnet Service to Nodegrid Value
    SUITE:Test Port Value  //*[@id="grpcPort"]      ${Temp}     False
    Unselect Checkbox       jquery=#grpc
    Click Element           jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible


*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Services::Open Tab
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	close all browsers

SUITE:Test Port Value
    [Arguments]	${LOCATOR}      ${Value}	    ${Expect_Error}=FALSE
    Wait Until Element Is Visible       ${LOCATOR}
    Input Text      ${LOCATOR}      ${Value}
    GUI::Basic::Save        SPINNER_TIMEOUT=60s
    GUI::Basic::Spinner Should Be Invisible
    Run Keyword If	${Expect_Error}     Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
    Run Keyword If	${Expect_Error}     Wait Until Element Contains	jquery=#errormsg    Validation error.
    ...     ELSE    GUI::Basic::Page Should Not Contain Error
