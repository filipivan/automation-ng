*** Settings ***
Documentation	syslog settings for firewall
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_0
#the feature is implemented for 5.2 and 5.4

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variable ***
${rule}	0
${desc}	description_test_new
${Target}	LOG
${root_login}	shell sudo su -
${logrotation}	logrotate /etc/logrotate.d/iptables --debug

*** Test Cases ***
Test Case to Add Firewall config.
	GUI::Basic::Table Should Not Contain	rulesTable	${desc}
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	//*[@id="target"]	${Target}
	Input Text	//*[@id="rule_num"]	${rule}
	Input Text	//*[@id="rule_desc"]	${desc}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${desc}

Test Case to validate the Log Level
	Select Checkbox  //*[@id="0"]/td[1]/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	//*[@id="log_level"]
	Select From List By Label	//*[@id="log_level"]	Info
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="0"]/td[1]/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	//*[@id="log_level"]	Notice
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="0"]/td[1]/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	//*[@id="log_level"]	Warning
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="0"]/td[1]/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	//*[@id="log_level"]	Emergency
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="0"]/td[1]/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	//*[@id="log_level"]	Debug
	Input Text	//*[@id="log_prefix"]	**DEBUG**
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Test Case to check iptables.log
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open Settings Tab
	GUI::Basic::Spinner Should Be Invisible
	${ret}=	Get value	id=hostname
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="peer_header"]//a[text()="Console"]
	sleep	15s
	Switch Window	title=${ret}
	Wait Until Page Contains Element	//iframe[@id='termwindow']
	Select Frame	//iframe[@id='termwindow']
	Sleep	50s
	Press Keys	//body	RETURN
	Press Keys	//body	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output	${root_login}
	sleep	15s
	${OUTPUT}=	GUI:Access::Generic Console Command Output	cd /var/log
	${OUTPUT}=	GUI:Access::Generic Console Command Output	ls -lrth
	sleep	15s
	Should Contain	${OUTPUT}	iptables.log
	${OUTPUT}=	GUI:Access::Generic Console Command Output	tail -f iptables.log
	Should Contain	${OUTPUT}	**DEBUG**
	${OUTPUT}=	GUI:Access::Generic Console Command Output	CTRL+C
	Unselect Frame
	Press Keys	//body	cw
	Close all browsers

Test Case to check log rotation
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open Settings Tab
	GUI::Basic::Spinner Should Be Invisible
	${ret}=	Get value	id=hostname
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="peer_header"]//a[text()="Console"]
	sleep	15s
	Switch Window	title=${ret}
	Wait Until Page Contains Element	//iframe[@id='termwindow']
	Select Frame	//iframe[@id='termwindow']
	Sleep	15s
	Press Keys	//body	RETURN
	Press Keys	//body	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output	${root_login}
	${OUTPUT}=	GUI:Access::Generic Console Command Output	cd /var/log
	${OUTPUT}=	GUI:Access::Generic Console Command Output	logrotate /etc/logrotate.d/iptables --debug
	sleep	15s
	Should Contain	${OUTPUT}	Last rotated at
	Unselect Frame
	Press Keys	//body	cw
	Close all browsers

Test Case to Delete Firewall Config
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Firewall::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="INPUT"]
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox  //*[@id="0"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Firewall::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="INPUT"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	close all browsers

GUI:Access::Generic Console Command Output
	[Arguments]	${COMMAND}	${CONTAINS_HELP}=no
	[Documentation]	Input the given command into the ttyd terminal and retrieve the output after prompt lines
	...	== REQUIREMENTS ==
	...	The console type needs to be one of those:
	...	-	CLI -> ends with "]#"
	...	-	Shell -> ends with "$"
	...	-	Root -> ends with "#"
	...	== ARGUMENTS ==
	...	-	COMMAND = Command to be executed
	...	-	CONTAINS_HELP = [yes/no] Console contains "[Enter '^Ec?' for help]" message
	${CHECK_TAB}=	Run Keyword And Return Status	Should Not Contain  ${COMMAND}  TAB
	Run Keyword If  ${CHECK_TAB}	Press Keys  //body	${COMMAND}  RETURN
	...  ELSE	Press Keys	//body	${COMMAND}
	Sleep	1

	Wait Until Keyword Succeeds	1m	1s	Execute JavaScript  term.selectAll();
	${CONSOLE_CONTENT}=	Wait Until Keyword Succeeds	1m	1s	Execute Javascript	return term.getSelection().trim();
	Should Not Contain  ${CONSOLE_CONTENT}  [error.connection.failure] Could not establish a connection to device
	Run Keyword If	"${CONTAINS_HELP}" == "yes"	Should Contain	${CONSOLE_CONTENT}	[Enter '^Ec?' for help]
	${OUTPUT}=  Output From Last Command	${CONSOLE_CONTENT}
	[Return]	${OUTPUT}