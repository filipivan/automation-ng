*** Settings ***
Documentation	Test Security:Firewall tab
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Test Template	SUITE:Drilldown set scope value

*** Test Cases ***
Test Add Drilldown Delete IPv4	test	IPv4
Test Add Drilldown Delete IPv6	test	IPv6

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Logout And Close Nodegrid

SUITE:Drilldown set scope value
	[Arguments]	${CHAIN_NAME}	${CHAIN_TYPE}
	GUI::Basic::Security::Firewall::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	@{IDS}=	Create List	//*[@id="${CHAIN_NAME}:${CHAIN_TYPE}"]/*/input

	GUI::Basic::Delete Rows In Table If Exists	jquery=#filterChains	${IDS}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Add Firewall Chain	${CHAIN_NAME}	${CHAIN_TYPE}
	GUI::Basic::Drilldown table row	\#filterChains	${CHAIN_NAME}\\\\:${CHAIN_TYPE}
	GUI::Basic::Title Should Contain	${CHAIN_NAME}:${CHAIN_TYPE}
	GUI::Basic::Scope Should Be	${CHAIN_NAME}:${CHAIN_TYPE}
	GUI::Basic::Return Button
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Scope Should Be	${EMPTY}
	GUI::Basic::Scope Should Be	${EMPTY}

	@{IDS}=	Create List	${CHAIN_NAME}\\\\:${CHAIN_TYPE}
	GUI::Basic::Delete Rows In Table Without Alert	\#filterChains	@{IDS}
