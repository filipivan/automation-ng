*** Settings ***
Documentation	Testing Firewall elements under the Security tab
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	GUI

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test nonAccess Controls and Table Exists
	Element Should Be Enabled	//*[@id="addButton"]
	Element Should Be Disabled	//*[@id="delButton"]
	Element Should Be Disabled	//*[@id="policyButton"]
	Page Should Contain Element	//*[@id="filterChains"]

Test Add Elements
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="addChain_Form"]/div[2]/div/div/div[1]/div/div[1]
	Page Should Contain Element	//*[@id="addChain_Form"]/div[2]/div/div/div[1]/div/div[2]
	Page Should Contain Element	//*[@id="chain"]
	Click Element	//*[@id="addChain_Form"]/div[2]/div/div/div[1]/div/div[1]/label/input
	Input Text	//*[@id="chain"]	DummyFire
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Test Change Policy Fail
	Click Element	//*[@id="DummyFire:IPv4"]/td[1]/input
	Element Should Be enabled	//*[@id="policyButton"]
	Click Element	//*[@id="policyButton"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	//*[@id="main_doc"]/div[1]/div/div	User-defined chains are not allowed to have policy

Test Drilldown Rule
	Click Element	//*[@id="DummyFire:IPv4"]/td[2]/a
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	@{Targets}=	Create List	ACCEPT	DROP	REJECT	LOG	RETURN
	@{TXT}=	Get List Items	//*[@id="target"]
	Should Be Equal	${Targets}	${TXT}
	FOR	${I}	IN RANGE	1	11
		Page Should Contain Element	//*[@id="chainRule"]/div[3]/div[1]/div/div/div[2]/div/div/div[${I}]
	END

	@{Log}=	Create List	Debug	Info  Notice  Warning	Error	Critical	Alert	Emergency
	@{TXT}=	Get List Items	//*[@id="log_level"]
	Should Be Equal	${Log}	${TXT}
	Select From List By Value	//*[@id="target"]	REJECT
	@{Reject}=	Create List	Network Unreacheable	Host Unreacheable	Port Unreacheable	Protocol Unreacheable	Network Prohibited	Host Prohibited	Administratively Prohibited	TCP Reset
	@{Re}=	Get List Items	//*[@id="reject_with"]
	Should Be Equal	${Reject}	${Re}
	FOR	${I}	IN RANGE	1	5
		Page Should Contain Element	//*[@id="chainRule"]/div[3]/div[2]/div/div/div[2]/div/div/div[${I}]
	END

Cancel and Delete
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Return
	Click Element	//*[@id="DummyFire:IPv4"]/td[1]/input
	Element Should Be enabled	//*[@id="delButton"]
	Click Element	//*[@id="delButton"]
	GUI::Basic::Spinner Should Be Invisible

Test Change Policy Tab
	Click Element	//*[@id="INPUT:IPv4"]/td[1]/input
	Element Should Be enabled	//*[@id="policyButton"]
	Click Element	//*[@id="policyButton"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="changePolicy_Form"]/div[2]/div/div/div[2]/label
	@{Policy}=	Create List	ACCEPT	DROP
	@{Re}=	Get List Items	//*[@id="policy"]
	Should Be Equal	${Policy}  ${Re}
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible

Test Delete Built-In Test
	Click Element	//*[@id="FORWARD:IPv4"]/td[1]/input
	Element Should Be enabled	//*[@id="delButton"]
	Click Element	//*[@id="delButton"]
	Wait Until Element Is Visible	//*[@id="main_doc"]/div[1]/div/div
	Element Should Contain	//*[@id="main_doc"]/div[1]/div/div	Built-in chains cannot be deleted.

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Security::Firewall::Open Tab
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

