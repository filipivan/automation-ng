*** Settings ***
Documentation	Test Security:Firewall rules description/comments
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-2	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_0

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variable ***
${rule_1}	1
${desc_1}	description_test_1
${desc_new}	description_test_new
${desc_del}	' '
${rule_2}	2
${desc_2}	description_test_2
${rule_3}	3

*** Test Cases ***
Go to Security :: Firewall :: INPUT:IPv4 Check for no description without any new rules.
	GUI::Basic::Table Should Not Contain	rulesTable	${desc_1}

Add a new rule and provide description.
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=rule_num	${rule_1}
	Input Text	id=rule_desc	${desc_1}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${desc_1}

Edit the rules and change the description.
	Select Checkbox	//*[@id="${rule_1}"]/td[1]/input
	GUI::Basic::Edit
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=rule_desc	${desc_new}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${desc_new}
	GUI::Basic::Table Should Not Contain	rulesTable	${desc_1}

Edit the rules and remove the description.
	Select Checkbox	//*[@id="${rule_1}"]/td[1]/input
	GUI::Basic::Edit
	Input Text	id=rule_desc	${desc_del}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Table Should Not Contain	rulesTable	${desc_new}

Add new rule and add new description.
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=rule_num	${rule_2}
	Input Text	id=rule_desc	${desc_2}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${desc_2}

Add another rule but without description.
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=rule_num	${rule_3}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${desc_2}
	GUI::Basic::Table Should Not Contain	rulesTable	${desc_new}

Delete one of the rule with description.
	Select Checkbox	//*[@id="${rule_2}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Table Should Not Contain	rulesTable	${desc_new}
	GUI::Basic::Table Should Not Contain	rulesTable	${desc_2}

Delete all the rules and check description.
	Select Checkbox	//*[@id="${rule_1}"]/td[1]/input
	Select Checkbox	//*[@id="${rule_2}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Table Should Not Contain	rulesTable	${desc_new}
	GUI::Basic::Table Should Not Contain	rulesTable	${desc_2}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Firewall::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="INPUT"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid