*** Settings ***
Documentation	Testing Tracking -> Scheduler for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variable ***

*** Test Cases ***
Test Scheduler Elements
	GUI::Tracking::Open Scheduler Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="ScheduleLogTable"]
	Page Should Contain Element	//*[@id="reset_task_log"]

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Tracking::Open
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
