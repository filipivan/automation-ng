*** Settings ***
Documentation	Functionality test cases about heart beat for cellular module
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DATA_MONITORING_MESSAGE}	SIM data usage monitoring should be enabled in Network :: Connections.

*** Test Cases ***
Test case to check enable data monitoring warming message
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Tracking::Devices::Open Tab
	GUI::Basic::Click Element	//span[normalize-space()='Wireless Modem']
	${TABLE_CELL}	Get Table Cell	id=wmodemTable	2	1
	Log	\nTABLE_CELL: \n${TABLE_CELL}	INFO	console=yes
	GUI::Basic::Click Element	//a[@id='${TABLE_CELL}']
	Page Should Contain	${DATA_MONITORING_MESSAGE}

*** Keywords ***
SUITE:Setup
#	CLI: Check if has wireless modem with SIM card
	CLI:Open
	SUITE:Check if has wireless modem support
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	SUITE:Check if has wireless modem with sim card
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	CLI:Close Connection
#	GUI: Basic login
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Run Keyword If	not ${HAS_WMODEM_SUPPORT} or not ${HAS_WMODEM}	CLI:Close Connection
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
#	GUI: Teardown
	Close All Browsers

SUITE:Check if has wireless modem support
	${HAS_WMODEM_SUPPORT}=	CLI:Has Wireless Modem Support
	Set Suite Variable	${HAS_WMODEM_SUPPORT}
	Log	Has Wireless Modem Support: ${HAS_WMODEM_SUPPORT}	INFO	console=yes

SUITE:Check if has wireless modem with sim card
	${HAS_WMODEM}	${MODEM_INFO}=	CLI:Get Wireless Modems SIM Card Info	0
	Set Suite Variable	${HAS_WMODEM}
	Log	Has Wireless Modem: ${HAS_WMODEM}	INFO	console=yes
	Set Suite Variable	${MODEM_INFO}
	Log	Modem Info: ${MODEM_INFO}	INFO	console=yes