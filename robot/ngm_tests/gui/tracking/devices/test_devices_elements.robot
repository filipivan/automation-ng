*** Settings ***
Documentation	Testing Tracking -> Devices for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${NGVERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW

*** Test Cases ***
Test Devices Elements
	${MODEL}=	GUI::Basic::GUI4.0::Get Model
	GUI::Tracking::Open Devices Tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' == '4.2'	Click Element	usbDevices
	Run Keyword If	'${MODEL}' == 'Nodegrid Serial Console' or '${MODEL}' == 'Nodegrid Services Router'	Page Should Contain Element	//*[@id="serialStatsTable"]
	...	ELSE	Page Should Contain Element	//*[@id="usbTable"]
	Run Keyword If	'${MODEL}' == 'Nodegrid Serial Console' or '${MODEL}' == 'Nodegrid Services Router'	Suite:Check Devices Tab

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Tracking::Open
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

Suite:Check Devices Tab
	Click Element	//*[@id="usbDevices"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="usbTable"]
