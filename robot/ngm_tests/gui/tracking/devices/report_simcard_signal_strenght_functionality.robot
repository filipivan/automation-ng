*** Settings ***
Documentation	Testing ----> Report for SIM Card Signal Strength results
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	DEPENDENCE_MODEM	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW
Default Tags	EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${root_login}	shell sudo su -
${SELECT_CHANNEL}	S1-A
${INTERFACE}	cdc-wdm0
${SQLITE_ACCESS}	sqlite3 /var/wmodem/mgmt.db
${CIRCUIT_CARD_ID}	89014103272567918202
${CONNECTION_TYPE}	Mobile Broadband GSM
${CELLULAR_CONNECTION}	cellular
${output1}	|registered|LTE: RSSI:
${output2}	SNR:

*** Test Cases ***
Test Case To Add Report for SIM Card Signal Strength results
	GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login	${NGVERSION}	${USERNAME}	${QA_PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Tracking::Open Devices Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="wmodem"]/span
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="${SELECT_CHANNEL}"]
	GUI::Basic::Spinner Should Be Invisible
	${CIRCUIT_CARD_ID}=	Get Value	//*[@id="wmodICCID1"]
	Page Should Contain Element	//*[@id="chart1strline"]
	Unselect Checkbox	//*[@id="signalStrengthcb1"]
	Page Should Not Contain	//*[@id="chart1strline"]
	Select Checkbox	//*[@id="signalStrengthcb1"]
	Click Element	//*[@id="wmod_Form"]/div[3]/div[1]/div/div/div[2]/div/div/div[3]/input
	sleep	5
	Element Should Contain	//*[@id="${SELECT_CHANNEL}|${INTERFACE}"]/td[6]	0 B / -- GB
	GUI::Network::Open Settings Tab
	GUI::Basic::Spinner Should Be Invisible
	${HOST_NAME}=	Get value	id=hostname
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="peer_header"]//a[text()="Console"]
	sleep	5
	Switch Window	title=${HOST_NAME}
	Wait Until Page Contains Element	//iframe[@id='termwindow']
	Select Frame	//iframe[@id='termwindow']
	Sleep	5
	Press Keys	None	RETURN
	Press Keys	None	RETURN

	${OUTPUT}=	GUI::Console Command Output	${root_login}
	sleep	5
	Press Keys	None	RETURN
	Press Keys	None	RETURN
	${OUTPUT}=	GUI::Console Command Output	${SQLITE_ACCESS}
	sleep	5
	${OUTPUT}=	GUI:Access::Generic Console Command Output	SELECT * FROM sim_stats WHERE sim_ccid='${CIRCUIT_CARD_ID}' AND timestamp > DATETIME('now','-1 month') ORDER BY timestamp DESC;
	sleep	10
	should contain	${OUTPUT}	${output1}
	should contain	${OUTPUT}	${output2}
	sleep	5
	Press Keys	None	RETURN

*** Keywords ***
Suite:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Change Password	${QA_PASSWORD}
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="connName"]	${CELLULAR_CONNECTION}
	Select From List By Label	//*[@id="connType"]	${CONNECTION_TYPE}
	Select From List By Label	//*[@id="connItfName"]	${INTERFACE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${CELLULAR_CONNECTION}"]/td[1]/input
	Click Element	//*[@id="nonAccessControls"]/input[3]

SUITE:Teardown
	GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login	${NGVERSION}	${USERNAME}	${QA_PASSWORD}
	GUI::Basic::Change Password	${DEFAULT_PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${CELLULAR_CONNECTION}"]/td[1]/input
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout and Close Nodegrid
