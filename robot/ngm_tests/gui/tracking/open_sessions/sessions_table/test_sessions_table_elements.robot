*** Settings ***
Documentation	Testing Tracking -> Open Sessions -> Sessions Table NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variable ***

*** Test Cases ***
Test Devices Elements
	GUI::Tracking::Open Open Sessions Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="sessionsTable"]
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Disabled	//*[@id="nonAccessControls"]/input
	Page Should Contain Element	//*[@id="activeSessionsTable"]
	Click Element	//*[@id="tbody"]/tr/td[1]
	Element Should Be Enabled	//*[@id="nonAccessControls"]/input

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Tracking::Open
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
