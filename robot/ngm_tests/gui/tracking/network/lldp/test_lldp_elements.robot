*** Settings ***
Documentation	Testing Tracking -> Network -> LLDP for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Variable ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}

*** Test Cases ***
Test LLDP Elements
    GUI::Tracking::Open Network Tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element                   //*[@id="lldpMon"]
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain Element     //*[@id="lldpMonTable"]

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Tracking::Open
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
