*** Settings ***
Documentation	Testing Tracking -> Network -> Wifi Hotspots
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	EDGE	IE	EXCLUDEIN4_2

*** Variable ***
${HOTSPOT}      hotspot

*** Test Cases ***
Connect to the Wifi Hotspot and bring up the connection
    Skip If    ${IS_VM}    System is a VM and has no hotspot connection in "Network :: Connections"
    GUI::Basic::Network::Connections::Open tab
    GUI::Basic::Spinner Should Be Invisible
    Select Checkbox    //*[@id="${HOTSPOT}"]/td[1]/input
    GUI::Basic::Click Element	//input[@value='Up Connection']
    GUI::Basic::Spinner Should Be Invisible

Go to Tracking :: Network :: Hotspot and check the connected hoptspot mac-address
# This page is available for v5.2+
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	BUG_NG_8201
	Skip If    ${IS_VM}    System is a VM and has no hotspot connection in "Network :: Connections"
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=menu_main_tracking
	GUI::Basic::Spinner Should Be Invisible
	#relative xpath to Tracking::"Network"
	Click Element	//li[@role='presentation']//a[contains(text(),'Network')]
	GUI::Basic::Spinner Should Be Invisible
	#relative xpath to Tracking::Network::"IPsec"
	Click Element	//span[normalize-space()='Hotspot']
	GUI::Basic::Spinner Should Be Invisible

Bring down the Hotspot connection and verify
    Skip If    ${IS_VM}    System is a VM and has no hotspot connection in "Network :: Connections"
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Network::Connections::Open tab
    GUI::Basic::Spinner Should Be Invisible
    Select Checkbox   //*[@id="${HOTSPOT}"]/td[1]/input
    GUI::Basic::Click Element	//input[@value='Down Connection']
    GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	CLI:Open
	${IS_VM}	CLI:Is VM System
	Set Suite Variable	${IS_VM}
	CLI:Close Connection

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid