*** Settings ***
Documentation	Testing Tracking -> Network -> Interface for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Variable ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}

*** Test Cases ***
Test LLDP Elements
    GUI::Tracking::Open Network Tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element           //*[@id="netItfStats"]
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain Element     //*[@id="netitfStatsTable"]

Test Drilldown Presence eth0
    Click Element           xpath=//a[contains(text(),'eth0')]
    GUI::Basic::Spinner Should Be Invisible
    Check Element Present List      itfName     itfSpeed    itfCollisions   itfRxPack   itfRxBytes  itfRxErr
    ...     itfRxCRC    itfRxDrop
    ...     itfRxFifo   itfRxCompressed     itfRxFrame      itfRxLength     itfRxMissed     itfRxOver
    ...     itfTxPack   itfTxBytes  itfTxErr    itfTxCRC    itfTxDrop
    ...     itfTxFifo   itfTxCompressed     itfTxAbort  itfTxHeartB     itfTxWind
    Click Element           jquery=#cancelButton
    GUI::Basic::Spinner Should Be Invisible

Test Drilldown Presence loopback
	[Tags]	NON-CRITICAL	NEED-REVIEW
#	The 5.8v AT Device not conrain looback interface
    Click Element           xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[2]/td[1]/a
    GUI::Basic::Spinner Should Be Invisible
    Check Element Present List      itfName     itfSpeed    itfCollisions   itfRxPack   itfRxBytes  itfRxErr
    ...     itfRxCRC    itfRxDrop
    ...     itfRxFifo   itfRxCompressed     itfRxFrame      itfRxLength     itfRxMissed     itfRxOver
    ...     itfTxPack   itfTxBytes  itfTxErr    itfTxCRC    itfTxDrop
    ...     itfTxFifo   itfTxCompressed     itfTxAbort  itfTxHeartB     itfTxWind
    Click Element                   jquery=#cancelButton
    GUI::Basic::Spinner Should Be Invisible

Test Drilldown Presence loopback0
	[Tags]	NON-CRITICAL	NEED-REVIEW
	#	The 5.8v AT Device not conrain looback0 interface
    Click Element           xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[3]/td[1]/a
    GUI::Basic::Spinner Should Be Invisible
    Check Element Present List      itfName     itfSpeed    itfCollisions   itfRxPack   itfRxBytes  itfRxErr
    ...     itfRxCRC    itfRxDrop
    ...     itfRxFifo   itfRxCompressed     itfRxFrame      itfRxLength     itfRxMissed     itfRxOver
    ...     itfTxPack   itfTxBytes  itfTxErr    itfTxCRC    itfTxDrop
    ...     itfTxFifo   itfTxCompressed     itfTxAbort  itfTxHeartB     itfTxWind
    Click Element           jquery=#cancelButton
    GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Tracking::Open
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

Check Element Present List
    [Arguments]  @{LST}
    FOR    ${I}    IN  @{LST}
       Element Should Be Visible     ${I}
    END