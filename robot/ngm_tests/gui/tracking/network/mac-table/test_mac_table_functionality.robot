*** Settings ***
Documentation	Testing Tracking -> Network -> Mac_Table for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8

*** Variable ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${Vlan_Name}	vlan_5
${Vlan_id}	5
${ip_address_1}	5.1.1.33
${ip_address_2}	5.1.1.31
${net_mask}	24
${mac_address}	e4:1a:2c:00:56:e2
${port_33}	sfp1
${port_31}	sfp0


*** Test Cases ***
Test Case for Configure Vlan 5 On both NSR
	GUI::Basic::Open And Login Nodegrid
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="connName"]	${Vlan_Name}
	Select From List By value	//*[@id="connType"]	vlan
	Select From List By value	//*[@id="connItfName"]	backplane0
	Click Element	css=.radio:nth-child(3) #method
	Click Element	id=address
	Input Text	id=address	${ip_address_1}
	Click Element	id=netmask
	Input Text	id=netmask	${net_mask}
	Input Text	//*[@id="vlanId"]	${Vlan_id}
	Click Element	//*[@id="method6"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=vlan
	Input Text	//*[@id="vlan"]	${Vlan_id}
	Click Element	//div[@id="memberTagged"]//option[@value="backplane0"]
	Click Element	//div[@id="memberTagged"]//option[@value="${port_33}"]
	Click Element	//*[@id="memberTagged"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout and Close Nodegrid
	SUITE:Setup2
	GUI::Basic::login
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="connName"]	${Vlan_Name}
	Select From List By value	//*[@id="connType"]	vlan
	Select From List By value	//*[@id="connItfName"]	backplane0
	Click Element	css=.radio:nth-child(3) #method
	Click Element	id=address
	Input Text	id=address	${ip_address_2}
	Click Element	id=netmask
	Input Text	id=netmask	${net_mask}
	Input Text	//*[@id="vlanId"]	${Vlan_id}
	Click Element	//*[@id="method6"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=vlan
	Input Text	//*[@id="vlan"]	${Vlan_id}
	Click Element	//div[@id="memberTagged"]//option[@value="backplane0"]
	Click Element	//div[@id="memberTagged"]//option[@value="${port_31}"]
	Click Element	//*[@id="memberTagged"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Test Case to Send Traffic
	GUI::Open System tab
	click element	css=.submenu li:nth-child(5) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="icon_NetworkTools"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=.fixed-top
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="ipaddress"]	${ip_address_1}
	Select From List By value	//*[@id="interface"]	backplane0.5
	Click Element	xpath=//input[@value="Ping"]
	GUI::Basic::Spinner Should Be Invisible
	sleep	15s
	Wait Until Page Contains	ping statistics
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible

Test Case for Disable the Interface
	GUI::Basic::Open And Login Nodegrid
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${Vlan_Name}"]/td[1]/input
	click element	//*[@id="nonAccessControls"]/input[4]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	select checkbox	//*[@id="${port_33}"]/td[1]/input
	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	select from list by value	//*[@id="status"]	disabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Tracking::Open Network Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="trackingMacTable"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="trackingMacTable-table"]
	Click Element	//*[@id="refresh"]

Test Case for Enable the Interface
	GUI::Basic::Open And Login Nodegrid
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${Vlan_Name}"]/td[1]/input
	click element	//*[@id="nonAccessControls"]/input[3]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchItfs > .title_b
	GUI::Basic::Spinner Should Be Invisible
	select checkbox	//*[@id="${port_33}"]/td[1]/input
	click element	xpath=//input[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible
	select from list by value	//*[@id="status"]	enabled
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Tracking::Open Network Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="trackingMacTable"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="trackingMacTable-table"]
	Click Element	//*[@id="refresh"]


Test Case for Mac_Table Reload Button and Search Field
	GUI::Basic::Open And Login Nodegrid
	GUI::Tracking::Open Network Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="trackingMacTable"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="trackingMacTable-table"]
	Click Element	//*[@id="refresh"]
	Page Should Contain Element	//*[@id="trackingMacTable-table"]
	Input Text	filter_field	${port_33}
	Press Keys	id=filter_field	ENTER
	GUI::Basic::Spinner Should Be Invisible
	Input Text	filter_field	${mac_address}
	Press Keys	id=filter_field	ENTER
	Page Should Contain Element	//*[@id="trackingMacTable-table"]
	GUI::Basic::Spinner Should Be Invisible
	wait until page contains element	//*[@id="refresh"]
	click element	//*[@id="refresh"]

Test Case for Deleting Configure Vlan 5 On both NSR
	GUI::Basic::Open And Login Nodegrid
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${Vlan_Name}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${Vlan_id}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout and Close Nodegrid
	SUITE:Teardown2
	GUI::Basic::login
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${Vlan_Name}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open
	Click element	css=.submenu li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="${Vlan_id}"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Network::Open
	GUI::Tracking::Open
	GUI::Open System tab

SUITE:Setup2
	${HOMEPAGE}=	Set Variable	${HOMEPAGEB}
	${USERNAME}=	Set Variable	${DEFAULT_USERNAME}
	${PASSWORD}=	Set Variable	${DEFAULT_PASSWORD}
	GUI::Basic::Open Nodegrid	${HOMEPAGEB}	${BROWSER}	${NGVERSION}


SUITE:Teardown
	Close All Browsers

SUITE:Teardown2
	${HOMEPAGE}=	Set Variable	${HOMEPAGEB}
	${USERNAME}=	Set Variable	${DEFAULT_USERNAME}
	${PASSWORD}=	Set Variable	${DEFAULT_PASSWORD}
	Close All Browsers
	GUI::Basic::Open Nodegrid	${HOMEPAGEB}	${BROWSER}	${NGVERSION}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

