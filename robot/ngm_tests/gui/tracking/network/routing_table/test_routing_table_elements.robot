*** Settings ***
Documentation	Testing Tracking -> Network -> Routing Table for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Variable ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}

*** Test Cases ***
Test LLDP Elements
    GUI::Tracking::Open Network Tab
    Click Element                   //*[@id="trackingRouteTable"]
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain Element     //*[@id="routingTable"]

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Tracking::Open

SUITE:Teardown
	GUI::Basic::Logout And Close Nodegrid
