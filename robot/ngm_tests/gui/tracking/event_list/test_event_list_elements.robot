*** Settings ***
Documentation	Testing Tracking -> Event List NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE	EVENT	TRACKING
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variable ***
${RESET_BUTTON}	//*[@id="nonAccessControls"]/input
${EVENT200_CHECKBOX}	//*[@id="200"]/td[1]
${EVENT200_COUNT}	//*[@id="200"]/td[4]
${EVENTLIST_CHART}	//*[@id="chart_form"]/div[1]/div[1]
${EVENTLIST_TABLE}	//*[@id="eventListTable"]

*** Test Cases ***
Test Devices Elements
	GUI::Tracking::Open Event List Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	${EVENTLIST_CHART}
	Page Should Contain Element	${EVENTLIST_TABLE}

Test Event List Table has generated event
	${COUNT}=	Get Text	${EVENT200_COUNT}
	Should Not Be Equal	0	${COUNT}

Test Event List Table disables after Reset Button
	Element Should Be Disabled	${RESET_BUTTON}
	Click Element	${EVENT200_CHECKBOX}
	Wait Until Element Is Enabled	${RESET_BUTTON}	timeout=${GUI_DEFAULT_TIMEOUT}
	Click Element	${RESET_BUTTON}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	30	2	Element Should Be Disabled	${RESET_BUTTON}

Test Event List Table reseted event
	${COUNT}=	Get Text	${EVENT200_COUNT}
	Should Be Equal	0	${COUNT}

Test Event List Table enables/disables Reset Button
	Element Should Be Disabled	${RESET_BUTTON}
	Click Element	${EVENT200_CHECKBOX}
	Wait Until Element Is Enabled	${RESET_BUTTON}	timeout=${GUI_DEFAULT_TIMEOUT}
	Click Element	${EVENT200_CHECKBOX}
	Wait Until Keyword Succeeds	30	2	Element Should Be Disabled	${RESET_BUTTON}


*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
