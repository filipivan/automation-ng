*** Settings ***
Documentation	Testing --> Display Filtered Nodegrid Events
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	EXCLUDEIN4_2	EXCLUDEIN5_0	#BUG_NG_8196

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test Case To Check Search and Export to PDF buttons availability
	GUI::Basic::Spinner Should Be Invisible
	GUI::Tracking::Open Event List Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//a[@id='listEvents']/span
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=filter_field	${EMPTY}
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//input[@id='field_from_date']	${EMPTY}
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//input[@id='field_to_date']	${EMPTY}
	GUI::Basic::Spinner Should Be Invisible
	Click Button	id=button_search_date
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'<='5.6'		Click Button	id=button_export_pdf
	Run Keyword If	'${NGVERSION}'>'5.6'		click element	xpath=//button[contains(.,'Export to PDF')]
	GUI::Basic::Spinner Should Be Invisible
	[Teardown]	SUITE:Test Teardown

Test Case to check If event (200 HTTPS) was generated after login Date
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open NodeGrid
	${CURRENT_DATE_BEFORE_LOGIN}	GUI::Basic::Get Current Date
	Sleep	5s
	GUI::Basic::Login
	${RESULT}	GUI::Tracking::Check Nodegrid Events	${CURRENT_DATE_BEFORE_LOGIN}	EVENT_ID=200
	Run Keyword If	not ${RESULT}	Fail	Event (200 HTTPS) NOT generated.
	[Teardown]	SUITE:Test Teardown

Test Case to check If event (201 SSH) was generated after login Date
	GUI::Basic::Open NodeGrid
	${CURRENT_DATE_BEFORE_LOGIN}	GUI::Basic::Get Current Date
	GUI::Basic::Login
	CLI:Open	CHECK_MEMORY_CPU=No
	Sleep	5s
	CLI:Close Connection
	${RESULT}	GUI::Tracking::Check Nodegrid Events	${CURRENT_DATE_BEFORE_LOGIN}	EVENT_ID=201
	Run Keyword If	not ${RESULT}	Fail	Event (201 SSH) NOT generated.
	[Teardown]	SUITE:Test Teardown

Test Case to check If event 136 was generated after login Date
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open NodeGrid
	${CURRENT_DATE_BEFORE_LOGIN}	GUI::Basic::Get Current Date
	GUI::Basic::Login
	CLI:Open	CHECK_MEMORY_CPU=No
	Set Client Configuration	prompt=~$
	CLI:Write	shell
	Set Client Configuration	prompt=]#
	Sleep	5s
	CLI:Write	exit
	${RESULT}	GUI::Tracking::Check Nodegrid Events	${CURRENT_DATE_BEFORE_LOGIN}	EVENT_ID=136
	Run Keyword If	not ${RESULT}	Fail	Event 136 NOT generated.
	[Teardown]	SUITE:Test Teardown

Test Case to check If event 135 was generated after login Date
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open NodeGrid
	${CURRENT_DATE_BEFORE_LOGIN}	GUI::Basic::Get Current Date
	GUI::Basic::Login
	CLI:Open	CHECK_MEMORY_CPU=No
	Set Client Configuration	prompt=~$
	Sleep	5s
	CLI:Write	shell
	${RESULT}	GUI::Tracking::Check Nodegrid Events	${CURRENT_DATE_BEFORE_LOGIN}	EVENT_ID=135
	Run Keyword If	not ${RESULT}	Fail	Event 135 NOT generated.
	[Teardown]	SUITE:Test Teardown

Test Case to check If event 137 was generated after login Date
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open NodeGrid
	${CURRENT_DATE_BEFORE_LOGIN}	GUI::Basic::Get Current Date
	GUI::Basic::Login
	CLI:Open	CHECK_MEMORY_CPU=No
	Set Client Configuration	prompt=~$
	CLI:Write	shell
	Set Client Configuration	prompt=~#
	Sleep	5s
	CLI:Write	sudo su -
	${RESULT}	GUI::Tracking::Check Nodegrid Events	${CURRENT_DATE_BEFORE_LOGIN}	EVENT_ID=137
	Run Keyword If	not ${RESULT}	Fail	Event 137 NOT generated.
	[Teardown]	SUITE:Test Teardown

Test Case to check If event (200 SSH) was generated after login Date
	GUI::Basic::Open NodeGrid
	${CURRENT_DATE_BEFORE_LOGIN}	GUI::Basic::Get Current Date
	GUI::Basic::Login
	Sleep	5s
	CLI:Open	CHECK_MEMORY_CPU=No
	${RESULT}	GUI::Tracking::Check Nodegrid Events	${CURRENT_DATE_BEFORE_LOGIN}	EVENT_ID=200
	Run Keyword If	not ${RESULT}	Fail	Event (200 SSH) NOT generated.
	[Teardown]	SUITE:Test Teardown

Test Case to check If event 108 was generated after login Date
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open NodeGrid
	${CURRENT_DATE_BEFORE_LOGIN}	GUI::Basic::Get Current Date
	GUI::Basic::Login
	GUI::ManagedDevices::Delete Device If Exists	${DUMMY_DEVICE_CONSOLE_NAME}
	Sleep	5s
	GUI::ManagedDevices::Add Device	${DUMMY_DEVICE_CONSOLE_NAME}	device_console	127.0.0.1
	${RESULT}	GUI::Tracking::Check Nodegrid Events	${CURRENT_DATE_BEFORE_LOGIN}	EVENT_ID=108
	Run Keyword If	not ${RESULT}	Fail	Event 108 NOT generated.
	[Teardown]	SUITE:Test Teardown

Test Case to check If event 306 was generated after login Date
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open NodeGrid
	${CURRENT_DATE_BEFORE_LOGIN}	GUI::Basic::Get Current Date
	GUI::Basic::Login
	GUI::ManagedDevices::Delete Device If Exists	${DUMMY_DEVICE_CONSOLE_NAME}
	Sleep	5s
	GUI::ManagedDevices::Add Device	${DUMMY_DEVICE_CONSOLE_NAME}	device_console	127.0.0.1
	${RESULT}	GUI::Tracking::Check Nodegrid Events	${CURRENT_DATE_BEFORE_LOGIN}	TABLE_ROWS_ROW=2	EVENT_ID=306
	Run Keyword If	not ${RESULT}	Fail	Event 306 NOT generated.
	[Teardown]	SUITE:Test Teardown

Test Case to check If event 302 was generated after login Date
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open NodeGrid
	${CURRENT_DATE_BEFORE_LOGIN}	GUI::Basic::Get Current Date
	GUI::Basic::Login
	GUI::ManagedDevices::Delete Device If Exists	${DUMMY_DEVICE_CONSOLE_NAME}
	Sleep	5s
	GUI::ManagedDevices::Add Device	${DUMMY_DEVICE_CONSOLE_NAME}	device_console	127.0.0.1
	${RESULT}	GUI::Tracking::Check Nodegrid Events	${CURRENT_DATE_BEFORE_LOGIN}	TABLE_ROWS_ROW=4	EVENT_ID=302
	Run Keyword If	not ${RESULT}	Fail	Event 302 NOT generated.
	[Teardown]	SUITE:Test Teardown

Test Case to check If event 303 was generated after login Date
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open NodeGrid
	${CURRENT_DATE_BEFORE_LOGIN}	GUI::Basic::Get Current Date
	GUI::Basic::Login
	GUI::ManagedDevices::Delete Device If Exists	${DUMMY_DEVICE_CONSOLE_NAME}
	GUI::ManagedDevices::Add Device	${DUMMY_DEVICE_CONSOLE_NAME}	device_console	127.0.0.1
	Sleep	5s
	GUI::ManagedDevices::Delete Device If Exists	${DUMMY_DEVICE_CONSOLE_NAME}
	${RESULT}	GUI::Tracking::Check Nodegrid Events	${CURRENT_DATE_BEFORE_LOGIN}	TABLE_ROWS_ROW=3	EVENT_ID=303
	Run Keyword If	not ${RESULT}	Fail	Event 303 NOT generated.
	[Teardown]	SUITE:Test Teardown

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Close All Browsers
	CLI:Close Connection
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${DUMMY_DEVICE_CONSOLE_NAME}
	GUI::Basic::Logout And Close Nodegrid

SUITE:Test Teardown
	Close All Browsers
	CLI:Close Connection