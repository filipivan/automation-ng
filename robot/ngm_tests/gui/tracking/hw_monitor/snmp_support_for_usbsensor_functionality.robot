*** Settings ***
Documentation	Testing ----> SNMP Support for USB Sensors
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	DEPENDENCE_USBSENSORS	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${root_login}	shell sudo su -
${snmpwalk1}	snmpwalk -v 2c -c ${COMMUNITY_NAME} ${HOST} .1.3.6.1.4.1.42518.4.2.1.1.5.1.0
${snmpwalk2}	snmpwalk -v 2c -c ${COMMUNITY_NAME} ${HOST} .1.3.6.1.4.1.42518.4.2.1.1.5.2.1.2
${snmpwalk3}	snmpwalk -v 2c -c ${COMMUNITY_NAME} ${HOST} .1.3.6.1.4.1.42518.4.2.1.1.5.2.1.3
${snmpwalk4}	snmpwalk -v 2c -c ${COMMUNITY_NAME} ${HOST} .1.3.6.1.4.1.42518.4.2.1.1.5.2.1.4
${snmpwalk5}	snmpwalk -v 2c -c ${COMMUNITY_NAME} ${HOST} .1.3.6.1.4.1.42518.4.2.1.1.5.2.1.5
${EXPORT_MIBS_COMMAND}	export MIBS=ALL
${HOST_NAME}	nodegrid
${COMMUNITY_NAME}	test_snmpwalk
${exit_session}	exit

*** Test Cases ***
Test Case To Perform Snmpwalk and Check USB Sensor's Connected To Nodegride Device
	GUI::Network::Open Settings Tab
	GUI::Basic::Spinner Should Be Invisible
	${HOST_NAME}=	Get value	id=hostname
	GUI::Network::Open SNMP Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=oid	.1
	GUI::BASIC::SPINNER SHOULD BE INVISIBLE
	Input Text	id=community	${COMMUNITY_NAME}
	GUI::BASIC::SPINNER SHOULD BE INVISIBLE
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="peer_header"]//a[text()="Console"]
	sleep	5
	Switch Window	title=${HOST_NAME}
	Wait Until Page Contains Element	//iframe[@id='termwindow']
	Select Frame	//iframe[@id='termwindow']
	Sleep	5
	Press Keys	None	RETURN
	Press Keys	None	RETURN

	${OUTPUT}=	SUITE:Console Command Output	${root_login}
	${OUTPUT}=	SUITE:Console Command Output	${EXPORT_MIBS_COMMAND}
	${OUTPUT}=	SUITE:Console Command Output	${snmpwalk1}
	Should Contain	${OUTPUT}	.3.6.1.4.1.42518.4.2.1.1.5.1.0 = INTEGER:
	sleep	5s
	Press Keys	None	ENTER
	${OUTPUT}=	SUITE:Console Command Output	 ${snmpwalk2}
	Should Contain	${OUTPUT}	.3.6.1.4.1.42518.4.2.1.1.5.2.1.2
	sleep	5s
	Press Keys	None	ENTER
	${OUTPUT}=	SUITE:Console Command Output	${snmpwalk3}
	Should Contain	${OUTPUT}	.3.6.1.4.1.42518.4.2.1.1.5.2.1.3
	sleep	5s
	Press Keys	None	ENTER
	sleep	5
	${OUTPUT}=	SUITE:Console Command Output	${snmpwalk4}
	Should Contain	${OUTPUT}	.3.6.1.4.1.42518.4.2.1.1.5.2.1.4
	sleep	5s
	Press Keys	None	ENTER
	${OUTPUT}=	SUITE:Console Command Output	${snmpwalk5}
	Should Contain	${OUTPUT}	.3.6.1.4.1.42518.4.2.1.1.5.2.1.5
	${OUTPUT}=	SUITE:Console Command Output	${exit_session}
	sleep	5s
	Unselect Frame
	Press Keys	None	cw
	Close All Browsers

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Network::Open SNMP Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="community|${COMMUNITY_NAME}~default"]/td[1]/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

SUITE:Console Command Output
	[Arguments]	${COMMAND}
	[Documentation]	 Input the given command into the ttyd terminal and retrieve the output after prompt lines
	...	== ARGUMENTS ==
	...	-	COMMAND = Command to be executed
	...	== EXPECTED RESULT ==
	...	Input the command into the ttyd terminal and returns the OUTPUT between the command and the last prompt

	${CHECK_TAB}=	Run Keyword And Return Status	Should Not Contain	${COMMAND}	TAB
	Run Keyword If	${CHECK_TAB}	Press Keys 	None	${COMMAND}	RETURN
	...	ELSE	Press Keys 	None	${COMMAND}
	Sleep	1

	${SELECTION}=	Execute JavaScript	term.selectAll(); return term.getSelection().trim();
	Should Not Contain	${SELECTION}	[error.connection.failure] Could not establish a connection to device

	${LINES}=	Split String	${SELECTION}	\n
	${INDEXES_OCCURRED}=	Create List
	${LENGTH}=	Get Length	${LINES}
	FOR	${INDEX}	IN RANGE	0	${LENGTH}
		${LINE}=	Get From List	${LINES}	${INDEX}
		${CHECK}=	Run Keyword And Return Status	Should Contain	${LINE}	@
		Run Keyword If	${CHECK}	Append to List	${INDEXES_OCCURRED}	${INDEX}
	END
	${STRING}=	Set Variable
	${END}=	Get From List	${INDEXES_OCCURRED}	-1
	${PREVIOUS}=	Get From List	${INDEXES_OCCURRED}	-2
	FOR	${INDEX}	IN RANGE	${PREVIOUS}	${END}
		${LINE}=	Get From List	${LINES}	${INDEX}
		${STRING}=	Set Variable	${STRING}\n${LINE}
	END
	${STRING}=	Get Substring	${STRING}	1
	[Return]	${STRING}