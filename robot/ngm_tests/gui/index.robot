*** Settings ***
Documentation	index.html behaviour tests
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	init.robot
Force Tags	PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}

*** Test Cases ***
Username field focus
	[Documentation]	Test to verify if the username fields has the focus
	Page Should Contain Element	jquery=#username[autofocus]

Enter should submit login form
	[Documentation]	Login form should allow user submit the form pressing ENTER
	GUI::Basic::LoginWithEnter	${USERNAME}	${PASSWORD}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open NodeGrid

SUITE:Teardown
	Close all Browsers
