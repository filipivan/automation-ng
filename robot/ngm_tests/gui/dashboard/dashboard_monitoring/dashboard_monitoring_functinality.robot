*** Settings ***
Documentation	dashboard_monitoring
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX
Default Tags	EXCLUDEIN4_2		EXCLUDEIN5_8

*** Variable ***
${NAME}	Testing
${Description}	Testing_dashboard_automation

*** Test Cases ***
Test case to add dashboard
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Dashboard::Open Dashboard Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Frame	//iframe
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	1m	2s	wait until page contains element	//*[@id="kibana-body"]/div/nav/div[2]/div[1]/app-switcher/div[3]/a/div[2]
	Click Element	xpath=//body[@id='kibana-body']/div/nav/div[2]/div/app-switcher/div/a/div[2]
	Click Element	css=.active .global-nav-link__title
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	css=.kuiButton--primary	60s
	Click Element	css=.kuiButton--primary
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=.kuiLocalMenu:nth-child(1) > .kuiLocalMenuItem:nth-child(3)
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="template_wrapper"]/div/div[2]/saved-object-finder/form/div/div[1]/div/input	${NAME}
	Click Element	css=.kuiLocalMenu:nth-child(1) > .kuiLocalMenuItem:nth-child(1)
	Input Text	//*[@id="dashboardDescription"]	${Description}
	Click Element	//*[@id="template_wrapper"]/form/button
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=.active .global-nav-link__title

Test case to check Nodegrid graph
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open And Login Nodegrid
	GUI::Dashboard::Open Dashboard Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Frame	//iframe
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	1m	2s	wait until page contains element	//*[@id="kibana-body"]/div/nav/div[2]/div[1]/app-switcher/div[3]/a/div[2]
	Click Element	xpath=//body[@id='kibana-body']/div/nav/div[2]/div/app-switcher/div/a/div[2]
	Click Element	css=.active .global-nav-link__title
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//a[contains(text(),'Nodegrid')]	60s
	Click Element	xpath=//a[contains(text(),'Nodegrid')]
	Wait Until Page Contains Element	//*[@id="kibana-body"]/div[1]/div/div/div[3]/dashboard-app/dashboard-grid/ul/li/dashboard-panel/div/visualize/div/div	60s
	Sleep	10s

Test case to check visualize feild
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open And Login Nodegrid
	GUI::Dashboard::Open Dashboard Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Frame	//iframe
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	1m	2s	wait until page contains element	//*[@id="kibana-body"]/div/nav/div[2]/div[1]/app-switcher/div[3]/a/div[2]
	click element	xpath=//body[@id='kibana-body']/div/nav/div[2]/div/app-switcher/div[2]/a/div[2]
	click element	css=.active .global-nav-link__title
	click element	xpath=//body[@id='kibana-body']/div/nav/div[2]/div/app-switcher/div[2]/a/div[2]
	Wait Until Page Contains Element	xpath=//a[contains(text(),'User Activity')]	60s
	Click Element	xpath=//a[contains(text(),'User Activity')]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="kibana-body"]/div[1]/div/div/div[3]	60s
	GUI::Basic::Spinner Should Be Invisible

Test case to check Discover feild
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open And Login Nodegrid
	GUI::Dashboard::Open Dashboard Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Frame	//iframe
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	1m	2s	wait until page contains element	//*[@id="kibana-body"]/div/nav/div[2]/div[1]/app-switcher/div[3]/a/div[2]
	Click Element	xpath=//body[@id='kibana-body']/div/nav/div[2]/div/app-switcher/div/a/div[2]
	click element	css=.active .global-nav-link__title
	click element	xpath=//body[@id='kibana-body']/div/nav/div[2]/div/app-switcher/div/a/div[2]
	Wait Until Page Contains Element	css=.ui-select-match-text > span	60s
	Sleep	10s
	GUI::Basic::Spinner Should Be Invisible

Test case to check Timelon feild
	[Tags]	NON-CRITICAL
	GUI::Basic::Open And Login Nodegrid
	GUI::Dashboard::Open Dashboard Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Frame	//iframe
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	1m	2s	wait until page contains element	//*[@id="kibana-body"]/div/nav/div[2]/div[1]/app-switcher/div[3]/a/div[2]
	Click Element	xpath=//body[@id='kibana-body']/div/nav/div[2]/div/app-switcher/div[4]/a/div[2]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//body[@id='kibana-body']/div/nav/div[2]/div/app-switcher/div[4]/a/div[2]	60s
	Click Element	xpath=//body[@id='kibana-body']/div/nav/div[2]/div/app-switcher/div[4]/a/div[2]
	Wait Until Page Contains Element	//*[@id="kibana-body"]/div/div/div/div[3]/div/div/div/div/timelion-cells/div/div	60s
	GUI::Basic::Spinner Should Be Invisible

Test case to check Management feild
	[Tags]	NON-CRITICAL
	GUI::Basic::Open And Login Nodegrid
	GUI::Dashboard::Open Dashboard Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Frame	//iframe
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	1m	2s	wait until page contains element	//*[@id="kibana-body"]/div/nav/div[2]/div[1]/app-switcher/div[3]/a/div[2]
	Click Element	css=.global-nav-link:nth-child(5) .global-nav-link__title
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	css=.global-nav-link:nth-child(5) .global-nav-link__title	60s
	Click Element	css=.global-nav-link:nth-child(5) .global-nav-link__title
	Wait Until Page Contains Element	css=.col-xs-4:nth-child(1) > .management-panel__link	60s
	GUI::Basic::Spinner Should Be Invisible

Test case to delete dashboard
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open And Login Nodegrid
	GUI::Dashboard::Open Dashboard Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Frame	//iframe
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	1m	2s	wait until page contains element	//*[@id="kibana-body"]/div/nav/div[2]/div[1]/app-switcher/div[3]/a/div[2]
	Click Element	xpath=//body[@id='kibana-body']/div/nav/div[2]/div/app-switcher/div/a/div[2]
	Click Element	css=.active .global-nav-link__title
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	css=.kuiTableRow:nth-child(1) .kuiCheckBox	60s
	select checkbox	css=.kuiTableRow:nth-child(1) .kuiCheckBox
	click element	//*[@id="kibana-body"]/div/div/div/div[3]/div/div/div[1]/div[2]/button/span
	click element	//*[@id="kibana-body"]/div[2]/confirm-modal/div/div[2]/button[2]

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Unselect Frame
	GUI::Basic::Logout and Close Nodegrid
