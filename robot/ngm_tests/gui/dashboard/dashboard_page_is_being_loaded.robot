*** Settings ***
Documentation	Checking Dashboard Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../init.robot
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variable ***
${NAME}	test

*** Test Cases ***
Dashboard is being loaded
	GUI::Basic::Dashboard::Dashboard::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//iframe[@name='dashFrame']	1

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Close all Browsers
