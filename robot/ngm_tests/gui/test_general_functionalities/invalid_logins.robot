*** Settings ***
Documentation	Invalid Login Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	init.robot
Force Tags	PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE	LOGIN
Default Tags	GUI	${BROWSER}

#Suite Setup	SUITE:Setup
#Suite Teardown	SUITE:Teardown
#
#Test Template	GUI::Basic::Login Failure
#
#*** Variables ***
#${TITLECASE_USERNAME}	Convert To Title Case	${DEFAULT_USERNAME}
#${TITLECASE_PASSWORD}	Convert To Title Case	${DEFAULT_PASSWORD}
#${TITLECASE_ROOT_PASSWORD}	Convert To Title Case	${ROOT_PASSWORD}
#
#*** Test Cases ***
#Case Sensitiv username	${TITLECASE_USERNAME}	${DEFAULT_PASSWORD}
#Case Sensitive password	${DEFAULT_USERNAME}	${TITLECASE_PASSWORD}
#Invalid username	invalid	${DEFAULT_PASSWORD}
#Invalid password	${DEFAULT_USERNAME}	invalid
#Invalid Username and password	invalid	invalid
#Empty username	${EMPTY}	${DEFAULT_PASSWORD}
#Empty password	${DEFAULT_USERNAME}	${EMPTY}
#Empty username and password	${EMPTY}	${EMPTY}
#root login	root	${ROOT_PASSWORD}
#Case sensitive root username	Root	${ROOT_PASSWORD}
#Case sensitive root password	root	${TITLECASE_ROOT_PASSWORD}
#Case sensitive root username and password	Root	${TITLECASE_ROOT_PASSWORD}
#
#*** Keywords ***
#SUITE:Setup
#	GUI::Basic::Open NodeGrid
#
#SUITE:Teardown
#	Close all Browsers

