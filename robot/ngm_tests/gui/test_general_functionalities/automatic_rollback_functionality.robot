*** Settings ***
Documentation	Automatic Roll back functionality test
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags	PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4

*** Variables ***
${Name}	testing
${timeout_value}	9000
${Default_timeout}	300
${Type}	device_console
${Ip_address}	127.0.0.1
${vlan_intf}  vlan_intf

*** Test Cases ***
Test Case to Check on System Preference Page
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:To Check Configuration Before Confirmation on System Preference Page
	SUITE:To Check After Configuration Confirmed on System Preference Page

Test Case to Check on Managed device Page
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Setup
	SUITE:To Check Configuration Before Confirmation on Managed Device Page
	SUITE:To Check After Configuration Confirmed on Managed Device Page

Test Case to Check on Network Connection Page
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:To Check Configuration Before Confirmation on Network Connection Page
	SUITE:To Check After Configuration Confirmed on Network Connection Page

Test Case to Check When the start timeout More than 30sec it Should revert Automatically
	SUITE:To Check Start timeout morethan 30sec

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	close all browsers

SUITE:To Check Configuration Before Confirmation on System Preference Page
	GUI::Basic::System::Preferences::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//a[@id='config_start']
	Input Text	//*[@id="idletimeout"]	${timeout_value}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//a[@id='config_revert']
	GUI::Basic::System::Preferences::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	execute javascript  document.getElementById("help_url").scrollIntoView()
	page should contain element	//*[@id="idletimeout"]	${Default_timeout}

SUITE:To Check After Configuration Confirmed on System Preference Page
	GUI::Basic::System::Preferences::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//a[@id='config_start']
	Input Text	//*[@id="idletimeout"]	${timeout_value}
	execute javascript  document.getElementById("saveButton").scrollIntoView()
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//a[@id='config_confirm']
	GUI::Basic::System::Preferences::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//a[@id='config_revert']
	execute javascript  document.getElementById("help_url").scrollIntoView()
	page should contain element	//*[@id="idletimeout"]	${timeout_value}
	SUITE:Set the idle timeout seconds to default value

SUITE:To Check Configuration Before Confirmation on Managed Device Page
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${Name}
	click element	xpath=//a[@id='config_start']
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text		//*[@id='spm_name']		${Name}
	Select From List By Label   //*[@id="type"]	${Type}
	Input Text	id=phys_addr	${Ip_address}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//a[@id='config_revert']
	GUI::Basic::Table Should Not Have Row With Id	SPMTable	//*[@id="testing"]

SUITE:To Check After Configuration Confirmed on Managed Device Page
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${Name}
	click element	xpath=//a[@id='config_start']
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text		//*[@id='spm_name']		${Name}
	Select From List By Label   //*[@id="type"]	${Type}
	Input Text	id=phys_addr	${Ip_address}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//a[@id='config_confirm']
	click element	xpath=//a[@id='config_revert']
	GUI::Basic::Table Should Has Row With Id	SPMTable	${Name}
	SUITE:To Delete Configuration on Managed device Page

SUITE:To Check Configuration Before Confirmation on Network Connection Page
	GUI::Basic::Network::Connections::Open tab
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//a[@id='config_start']
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="connName"]	${vlan_intf}
	Select From List By value	//*[@id="connType"]	vlan
	Select From List By value	//*[@id="connItfName"]	backplane0
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//a[@id='config_revert']
	GUI::Basic::Table Should Not Have Row With Id	SPMTable	${vlan_intf}

SUITE:To Check After Configuration Confirmed on Network Connection Page
	SUITE:Setup
	GUI::Basic::Network::Connections::Open tab
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//a[@id='config_start']
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="connName"]	${vlan_intf}
	Select From List By value	//*[@id="connType"]	vlan
	Select From List By value	//*[@id="connItfName"]	backplane0
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//a[@id='config_confirm']
	click element	xpath=//a[@id='config_revert']
	page should contain element	//*[@id="vlan_intf"]
	SUITE:To Delete Configuration on Network Connection Page

SUITE:To Delete Configuration on Network Connection Page
	SUITE:Setup
	GUI::Basic::Network::Connections::Open tab
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//tr[@id='vlan_intf']/td/input
	click element	//*[@id="delButton"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:To Delete Configuration on Managed device Page
	SUITE:Setup
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${Name}

SUITE:Set the idle timeout seconds to default value
	SUITE:Setup
	GUI::Basic::System::Preferences::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//a[@id='config_start']
	Input Text	//*[@id="idletimeout"]	${Default_timeout}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//a[@id='config_confirm']

SUITE:To Check Start timeout morethan 30sec
	SUITE:Setup
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${Name}
	click element	xpath=//a[@id='config_start']
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text		//*[@id='spm_name']		${Name}
	Select From List By Label   //*[@id="type"]	${Type}
	Input Text	id=phys_addr	${Ip_address}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep	40s
	GUI::Basic::Table Should Not Have Row With Id	SPMTable	//*[@id="testing"]
