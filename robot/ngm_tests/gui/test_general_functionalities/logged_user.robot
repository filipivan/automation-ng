*** Settings ***
Documentation	Validates hostname
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../init.robot
Force Tags	PART-1	GUI	${SYSTEM}	${BROWSER}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

*** Test Cases ***
Check If user@hostname is correct
	Page Should Not Contain	${DEFAULT_USERNAME}@undefined

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid

SUITE:Teardown
	Close All Browsers
