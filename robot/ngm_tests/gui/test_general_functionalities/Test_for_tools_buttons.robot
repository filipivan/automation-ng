*** Settings ***
Documentation	Editing System > Preferences Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../init.robot
Force Tags	PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

*** Test Cases ***
Access Access::Table
	Click Element	jquery=#main_menu li:first-child a
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=.submenu ul li:first-child a
	Click Element	jquery=.submenu ul li:first-child a
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=.topologytable
	Wait Until Element Is Visible	jquery=.control-group
	Wait Until Element Is Visible	jquery=.form-control
	Wait Until Element Is Visible	jquery=#search_expr1

Test reload button
	Input Text	jquery=#search_expr1	asd
	Click Element	jquery=#refresh-icon
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#search_expr1
	${SEARCH_FIELD_CONTENT}=	Get Text	jquery=#search_expr1
	Should Be Equal	${SEARCH_FIELD_CONTENT}	${EMPTY}

Test reload button on drilldown page
	Click Element	jquery=#main_menu li:nth-child(5) a
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=.submenu ul li:nth-child(2) a
	Click Element	jquery=.submenu ul li:nth-child(2) a
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="thead"]/tr/th[1]/input
	Checkbox Should Be Selected	//*[@id="thead"]/tr/th[1]/input
	GUI::Basic::Click Element	//*[@id='refresh-icon']
	GUI::Basic::Spinner Should Be Invisible
	Checkbox Should Not Be Selected	//*[@id="thead"]/tr/th[1]/input

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid

SUITE:Teardown
	GUI::Basic::Logout
	Close all Browsers
