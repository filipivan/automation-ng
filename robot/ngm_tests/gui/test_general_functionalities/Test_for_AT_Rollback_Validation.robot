*** Settings ***
Documentation	Test for Automatic Rollback Validation
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags	PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE      NON-CRITICAL
Default Tags	EXCLUDEIN3_2        EXCLUDEIN4_2        EXCLUDEIN5_0        EXCLUDEIN5_2        EXCLUDEIN5_4

*** Variables ***
${eror_msg_on_start_button}      Another configuration transaction is underway.
${error_msg_on_revert_button}       There is no active configuration transaction.
${error_msg_on_confirm_button}      There is no active configuration transaction.

#Need Review for 5.8

*** Test Cases ***
Test Case to Validate the Second Start Config

    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
	click element       xpath=//a[@id='config_start']
	Sleep   5s
	click element       xpath=//a[@id='config_start']
	sleep       3s
	${eror_msg_on_start_button}      handle alert
	sleep       30s

Test Case to validate the Revert Button before starting the Config transaction
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    click element       xpath=//a[@id='config_revert']
    Sleep       5s
    ${error_msg_on_revert_button}       handle alert

Test Case to validate the Confirm Button before starting the Config transaction
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    click element       xpath=//a[@id='config_confirm']
    Sleep       5s
    ${error_msg_on_confirm_button}       handle alert

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	close all browsers
