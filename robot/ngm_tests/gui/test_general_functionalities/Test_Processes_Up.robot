*** Settings ***
Resource	../init.robot
Documentation	Tests If Processes Are Still UP After All Tests Were Executed
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	PART-1	CLI
Default Tags	CLI	SSH	SHOW	PROCESSES

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

*** Test Cases ***
Check If M2MGMT Service is Up After Build
	[Tags]	EXCLUDEIN3_2
	Skip If	${IS_VM}
	CLI:Connect As Root
	SUITE:Check M2MGMT

*** Keywords ***
SUITE:Setup
	CLI:Open
	${IS_VM}	CLI:Is VM System
	Set Suite variable	${IS_VM}
	CLI:Connect As Root

SUITE:Teardown
	CLI:Close Connection

SUITE:Check M2MGMT
	${OUTPUT}	CLI:Write	ps -ef | grep m2m
	Should Contain	${OUTPUT}	/usr/sbin/m2mgmtd