*** Settings ***
Documentation	Get System Test System Details
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../init.robot
Force Tags	PART-1	GUI	${BROWSER}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Get Information
	${VERSION}=	Run Keyword If	'${NGVERSION}' >= '4.2'	SUITE::Get Version 4.2+
	${MODEL}=	Run Keyword If	'${NGVERSION}' >= '4.2'	SUITE::Get Model 4.2+
	${SYSTEM}=	Run Keyword If	'${NGVERSION}' >= '4.2'	SUITE::Get System 4.2+
#	${VERSION}=	Run Keyword If	'${NGVERSION}' == '3.2'	GUI3.x::Get Version
#	${MODEL}=	Run Keyword If	'${NGVERSION}' == '3.2'	GUI3.x::Get Model
#	${SYSTEM}=	Run Keyword If	'${NGVERSION}' == '3.2'	GUI3.x::Get System'
	Set Global Variable	${VERSION}
	Set Global Variable	${MODEL}
	Set Global Variable	${SYSTEM}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid

SUITE:Teardown
	Close all Browsers

SUITE::Get System 4.2+
	Wait Until Element Is Enabled	pwl
	Click Element	pwl
	Wait Until Element Is Visible	abt
	Click Element	abt
	Wait Until Page Contains	Serial Number	10s
	${SYSTEM}=	Get Text	jquery=div.modal \#system
	Click Button	jquery=div.modal button
	Wait Until Element Is Not Visible	jquery=div.modal
	[Return]	${SYSTEM}

SUITE::Get Model 4.2+
	Wait Until Element Is Enabled	pwl
	Click Element	pwl
	Wait Until Element Is Visible	abt
	Click Element	abt
	Wait Until Page Contains	Serial Number	10s
	${MODEL}=	Get Text	jquery=div.modal \#model
	Click Button	jquery=div.modal button
	Wait Until Element Is Not Visible	jquery=div.modal
	[Return]	${MODEL}

SUITE::Get Version 4.2+
	Wait Until Element Is Enabled	pwl
	Click Element	pwl
	Wait Until Element Is Visible	abt
	Click Element	abt
	Wait Until Page Contains	Serial Number	10s
	${VERSION}=	Get Text	jquery=div.modal \#version
	Click Button	jquery=div.modal button
	Wait Until Element Is Not Visible	jquery=div.modal
	[Return]	${VERSION}

GUI3.x::Get System
	Wait Until Element Is Visible	//*[@id="action"]/div/div/div[1]/a
	Wait Until Element Is Enabled	id=pwl
	Click Element	id=pwl
	Wait Until Page Contains Element	id=abt
	Click Element	id=abt
	Wait Until Page Contains	Serial Number	10s
	${SYSTEM}=	Get Text	//table/tbody/tr[2]/td/table/tbody/tr[1]/td[2]
	Click Element	//*[@id="modal_b"]/button
	[Return]	${SYSTEM}

GUI3.x::Get Model
	Wait Until Element Is Visible	//*[@id="action"]/div/div/div[1]/a
	Wait Until Element Is Enabled	id=pwl
	Click Element	id=pwl
	Wait Until Page Contains Element	id=abt
	Click Element	id=abt
	Wait Until Page Contains	Serial Number	10s
	${MODEL}=	Run Keyword And Continue On Failure	Get Text	//table/tbody/tr[2]/td/table/tbody/tr[8]/td[2]
	Click Element	//*[@id="modal_b"]/button
	[Return]	${MODEL}

GUI3.x::Get Version
	Wait Until Element Is Visible	//*[@id="action"]/div/div/div[1]/a
	Wait Until Element Is Enabled	id=pwl
	Click Element	id=pwl
	Wait Until Page Contains Element	id=abt
	Click Element	id=abt
	Wait Until Page Contains	Serial Number	10s
	${VERSION}=	Get Text	//table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]
	Click Element	//*[@id="modal_b"]/button
	[Return]	${VERSION}
