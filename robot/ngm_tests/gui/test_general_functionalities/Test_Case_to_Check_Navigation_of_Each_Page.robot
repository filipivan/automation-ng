*** Settings ***
Documentation	Navigation Pages
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags	PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

*** Variables ***
${Name}	 nodegrid

*** Test Cases ***
Test Case to Check Access Page
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=search_expr1	 ${Name}
	Press Keys	id=search_expr1	 ENTER
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	 nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Access::Tree::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="tree_div"]
	GUI::Basic::Access::Node::open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	 css=.node
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Access::Map::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="node_map"]	 10s
	Page Should Contain Element	 //*[@id="node_map"]

Test Case to Check Access page search
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Add Dummy Device Console	${DUMMY_DEVICE_CONSOLE_NAME}	device_console	127.0.0.1	22	root	no	${ROOT_PASSWORD}	ondemand
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Table Should Has Row With Id	SPMTable	${DUMMY_DEVICE_CONSOLE_NAME}
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=search_expr1	 ${DUMMY_DEVICE_CONSOLE_NAME}
	GUI::Basic::Spinner Should Be Invisible
	Press Keys	id=search_expr1	 ENTER
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	 ${DUMMY_DEVICE_CONSOLE_NAME}
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Devices	${DUMMY_DEVICE_CONSOLE_NAME}
	GUI::Basic::Spinner Should Be Invisible

Test Case to Check Tracking Page
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Setup
	GUI::Basic::Tracking::Open Sessions::open Tab
	Wait Until Page Contains Element	 //*[@id="activeSessionsTable_wrapper"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Tracking::Open Sessions::open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="deviceSessionsTable"]
	Wait Until Page Contains Element	//*[@id="deviceSessionsTable"]
	GUI::Basic::Tracking::Event List::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="chart-0"]
	Run Keyword If	'${NGVERSION}'>'4.2'	GUI::Basic::Tracking::Event List::open tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'>'4.2'	click element	//*[@id="listEvents"]/span
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'>'4.2'	Wait Until Page Contains Element	//*[@id="eventListTable_wrapper"]
	GUI::Basic::Tracking::System Usage::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="chart_form"]
	GUI::Basic::Tracking::System Usage::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="cpuInfo"]/span
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="chart_form"]/div[1]/div[1]
	GUI::Basic::Tracking::System Usage::open tab
	GUI::Basic::Spinner Should Be Invisible
	click element	//*[@id="diskInfo"]/span
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="chart_form"]
	GUI::Basic::Tracking::Discovery Logs::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="DiscoveryLogTable_wrapper"]
	GUI::Basic::Tracking::Tracking::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=.submenu li:nth-child(5) > a
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="netitfStatsTable_wrapper"]
	GUI::Basic::Tracking::Tracking::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=.submenu li:nth-child(5) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="switchItfMon"]/span
	Wait Until Page Contains Element	//*[@id="switchItfMonTable_wrapper"]
	GUI::Basic::Tracking::Tracking::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=.submenu li:nth-child(5) > a
	GUI::Basic::Spinner Should Be Invisible
	click element	//*[@id="lldpMon"]
	Wait Until Page Contains Element	//*[@id="lldpMonTable_wrapper"]
	Run Keyword If	'${NGVERSION}'>'4.2'	GUI::Basic::Tracking::Tracking::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=.submenu li:nth-child(5) > a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="trackingRouteTable"]
	Wait Until Page Contains Element	//*[@id="routingTable_wrapper"]
	Run Keyword If	'${NGVERSION}'>'4.2'	GUI::Basic::Tracking::Tracking::open tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'>'4.2'	Click Element	css=.submenu li:nth-child(5) > a
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'>'4.2'	Click Element	//*[@id="trackingIpsecTable"]
	Run Keyword If	'${NGVERSION}'>'4.2'	Wait Until Page Contains Element	//*[@id="trackingIpsecTable_wrapper"]
	GUI::Basic::Tracking::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#usbDevices > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="usbTable_wrapper"]
	Run Keyword If	'${NGVERSION}'>'4.2'	GUI::Basic::Tracking::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'>'4.2'	Click Element	//*[@id="bluetooth"]
	Run Keyword If	'${NGVERSION}'>'4.2'	Wait Until Page Contains Element	//*[@id="peerTable_wrapper"]
	GUI::Basic::Tracking::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="wmodem"]
	Wait Until Page Contains Element	//*[@id="wmodemTable_wrapper"]
	GUI::Basic::Tracking::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="gps"]
	Wait Until Page Contains Element	//*[@id="trackGPSTable_wrapper"]
	Run Keyword If	'${NGVERSION}'>'4.2'	GUI::Basic::Tracking::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'>'4.2'	Click Element	//*[@id="geoFence"]
	Run Keyword If	'${NGVERSION}'>'4.2'	Wait Until Page Contains Element	//*[@id="trackGPSTable_wrapper"]
	GUI::Basic::Tracking::Scheduler::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="ScheduleLogTable_wrapper"]
	GUI::Basic::Tracking::HW Monitor::open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="hwmonThemalTable_wrapper"]
	GUI::Basic::Tracking::HW Monitor::open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="power"]
	Wait Until Page Contains Element	//*[@id="hwmonPowerTable_wrapper"]
	GUI::Basic::Tracking::HW Monitor::open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="usbsensors"]
	Wait Until Page Contains Element	//*[@id="hwmonUSBSensorTable_wrapper"]
	GUI::Basic::Tracking::ZPE Cloud::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="cloudDeviceForm"]

Test Case to Check System Page
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::System::License::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="license_table_wrapper"]
	GUI::Basic::System::Preferences::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="securityProfile"]
	GUI::Basic::System::Slots::open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="nsr_img"]
	GUI::Basic::System::Date and Time::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="dateTimeLocalForm"]
	GUI::Basic::System::Date and Time::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#ntp_server > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="serverEnabled"]
	GUI::Basic::System::Date and Time::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#ntp > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="ntpTable_wrapper"]
	GUI::Basic::System::Toolkit::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="icon_upgrade"]
	 GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="localfilename"]
	GUI::Basic::System::Toolkit::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="icon_saveSettings"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="localFilename"]
	GUI::Basic::System::Toolkit::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="icon_applySettings"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="localFilename"]
	GUI::Basic::System::Toolkit::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="icon_factoryDefault"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="clearlog"]
	GUI::Basic::System::Toolkit::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="icon_systemCert"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="url"]
	GUI::Basic::System::Toolkit::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="icon_systemCert"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="systemCSRGenerate"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="countryCode"]
	GUI::Basic::System::Toolkit::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="icon_systemCfgChk"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="systemCfgChkselection"]
	GUI::Basic::System::Toolkit::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="icon_NetworkTools"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="ipaddress"]
	GUI::Basic::System::Toolkit::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="icon_diagnosticData"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="diagnosticData_web_result"]
	GUI::Basic::System::Toolkit::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="icon_cloudEnrollment"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="status"]
	GUI::Basic::System::Custom Fields::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="CustomFieldsForm_wrapper"]
	GUI::Basic::System::Dial Up::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="dialupSettings_form"]
	GUI::Basic::System::Dial Up::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	 jquery=#dialupCallback
	Wait Until Page Contains Element	//*[@id="dialupCallbackTable"]
	GUI::Basic::System::Scheduler::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="taskTable_wrapper"]
	GUI::Basic::System::SMS::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="smsSetStatus"]
	GUI::Basic::System::SMS::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#smsWhitelist > .title_b
	GUI::Basic::Spinner Should be Invisible
	Wait Until Page Contains Element	//*[@id="smsWhitelistTable_wrapper"]
	GUI::Basic::System::Remote File System::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="RFSTable_wrapper"]
	GUI::Basic::System::Toolkit::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	css=#icon_shutdown
	GUI::Basic::System::Toolkit::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	css=#icon_reboot

Test Case to Check Networking Page
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Network::Settings::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="hostname"]
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="peerTable_wrapper"]
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="switchItfTable"]
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="netswitchHost"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="chart_form"]
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchVlan > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="switchVlanTable_wrapper"]
	Run Keyword If	'${NGVERSION}'=='5.2.3'	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'=='5.2.3'	Click Element	css=#netswitchPoE > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'=='5.2.3'	Wait Until Page Contains Element	//*[@id="switchItfPoETable_wrapper"]
	Run Keyword If	'${NGVERSION}'>'4.2'	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'>'4.2'	Click Element	css=#netswitchAcl > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'>'4.2'	Wait Until Page Contains Element	//*[@id="addButton"]
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#netswitchGlobal > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="jumboSize_title"]
	GUI::Basic::Network::Static Routes::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="netmnt_StroutTable_wrapper"]
	GUI::Basic::Network::Hosts::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="hostTable"]
	GUI::Basic::Network::SNMP::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="snmpTable"]

	GUI::Basic::Network::Settings::open tab
	Run Keyword If	'${NGVERSION}' <= '5.4'	GUI::Basic::Network::DHCP Server::Open Tab
	Run Keyword If	'${NGVERSION}' >= '5.6'	Run Keywords	GUI::Basic::Network::DHCP::Open Tab	AND	Click Element	//span[normalize-space()='DHCP Server']
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="hostTable"]

	GUI::Basic::Network::Settings::open tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' <= '5.2'	GUI::Basic::Network::SSL VPN::open tab
	Run Keyword If	'${NGVERSION}' >= '5.4'	Run Keywords	GUI::Basic::Network::VPN::Open Tab	AND	Click Element	//span[normalize-space()='SSL VPN']
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//span[normalize-space()='Client']
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="sslvpnClientTable"]

	GUI::Basic::Network::Settings::open tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' <= '5.2'	GUI::Basic::Network::SSL VPN::open tab
	Run Keyword If	'${NGVERSION}' >= '5.4'	Run Keywords	GUI::Basic::Network::VPN::Open Tab	AND	Click Element	//span[normalize-space()='SSL VPN']
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//span[normalize-space()='Server']
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="status"]

	GUI::Basic::Network::Settings::open tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' <= '5.2'	GUI::Basic::Network::SSL VPN::open tab
	Run Keyword If	'${NGVERSION}' >= '5.4'	Run Keywords	GUI::Basic::Network::VPN::Open Tab	AND	Click Element	//span[normalize-space()='SSL VPN']
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//span[normalize-space()='Server Status']
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'<'5.4'	Wait Until Page Contains Element	//*[@id="sslvpnStatus_wrapper"]

	GUI::Basic::Network::Settings::open tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' <= '5.2'	GUI::Basic::Network::IPsec::open tab
	Run Keyword If	'${NGVERSION}' >= '5.4'	Run Keywords	GUI::Basic::Network::VPN::Open Tab	AND	Click Element	//span[normalize-space()='IPsec']
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="ipsecTunnelTable_wrapper"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//span[normalize-space()='IKE Profile']
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="ipsecIkeTable_wrapper"]
	Click Element	//span[normalize-space()='Global']
	Wait Until Page Contains Element	//*[@id="ipsecGlobalVtiEnabled"]
	GUI::Basic::Spinner Should Be Invisible

	GUI::Basic::Network::Settings::open tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' <= '5.2'	GUI::Basic::Network::Wireguard::open tab
	Run Keyword If	'${NGVERSION}' >= '5.4'	Run Keywords	GUI::Basic::Network::VPN::Open Tab	AND	Click Element	//span[normalize-space()='Wireguard']
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="addButton"]
	GUI::Basic::Spinner Should Be Invisible

	GUI::Basic::Network::Wireless Modem::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="reset"]
	Run Keyword If	'${NGVERSION}'>'4.2'	GUI::Basic::Network::Flow Exporter::open tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'>'4.2'	Wait Until Page Contains Element	//*[@id="peerTable_wrapper"]
	Run Keyword If	'${NGVERSION}'>'4.2'	GUI::Basic::Network::802.1x::open tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'>'4.2'	Wait Until Page Contains Element	//*[@id="addButton"]
	Run Keyword If	'${NGVERSION}'>'4.2'	GUI::Basic::Network::802.1x::open tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'>'4.2'	Click Element	//*[@id="netswitchDot1xUsers"]/span
	Run Keyword If	'${NGVERSION}'>'4.2'	Wait Until Page Contains Element	//*[@id="addButton"]
	Run Keyword If	'${NGVERSION}'>'4.2'	GUI::Basic::Network::QoS::open tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'>'4.2'	Wait Until Page Contains Element	//*[@id="interfaceTable_wrapper"]
	Run Keyword If	'${NGVERSION}'>'4.2'	GUI::Basic::Network::QoS::open tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'>'4.2'	Click Element	//*[@id="qosClassNav"]/span
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'>'4.2'	Wait Until Page Contains Element	//*[@id="qos_class_table"]
	Run Keyword If	'${NGVERSION}'>'4.2'	GUI::Basic::Network::QoS::open tab
	Run Keyword If	'${NGVERSION}'>'4.2'	Click Element	//*[@id="qosRuleNav"]
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'>'4.2'	Wait Until Page Contains Element	//*[@id="qos_rules_table_wrapper"]

Test Case to check Managed device pages
	SUITE:Setup
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="SPMTable"]
	GUI::Basic::Managed Devices::Views::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="addButton"]
	GUI::Basic::Managed Devices::Views::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="image"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="addButton"]
	GUI::Basic::Managed Devices::Types::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="SPMTargettypeTable_wrapper"]
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="addButton"]
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="vm_managers"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="addButton"]
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_discovery"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="addButton"]
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_discoveryname"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="addButton"]
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_discoverylogs"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="reset_logs"]
	GUI::Basic::Managed Devices::Auto Discovery::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="discovery_now"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="discovernow"]
	GUI::Basic::Managed Devices::Preferences::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="exitOption"]
	GUI::Basic::Managed Devices::Preferences::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_session_pref"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="discSeq"]
	GUI::Basic::Managed Devices::Preferences::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="spm_views_pref"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="SPMViewsForm"]

Test to case to Check Cluster Pages
	SUITE:Setup
	GUI::Basic::Cluster::Peers::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="refresh-icon"]
	GUI::Basic::Cluster::Settings::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="autoPreSharedKey"]
	GUI::Basic::Cluster::Management::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="refresh-icon"]

Test Case to Check Security Pages
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Setup
	GUI::Basic::Security::Local Accounts::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="user_namesTable_wrapper"]
	GUI::Basic::Security::Password Rules::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="saveButton"]
	GUI::Basic::Security::Authorization::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="author_groupsTable"]
	GUI::Basic::Security::Authentication::open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="addButton"]
	GUI::Basic::Security::Authentication::open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="secondFactorTable"]
	Wait Until Page Contains Element	//*[@id="addButton"]
	GUI::Basic::Security::Authentication::open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ssoTable"]
	Wait Until Page Contains Element	//*[@id="addButton"]
	GUI::Basic::Security::Firewall::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="addButton"]
	GUI::Basic::Security::NAT::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="addButton"]
	Run Keyword If	'${NGVERSION}'>'4.2'	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'>'4.2'	Wait Until Page Contains Element	//*[@id="zpe_cloud_url"]
	Run Keyword If	'${NGVERSION}'>'4.2'	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'>'4.2'	Click Element	css=.submenu li:nth-child(8) > a
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'>'4.2'	Wait Until Page Contains Element	//*[@id="geoFenceEnabled"]

Test case to Check Auditing Pages
	SUITE:Setup
	GUI::Basic::Auditing::Settings::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="evttimeType"]
	GUI::Basic::Auditing::Events::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="CustomFieldsForm"]
	Run Keyword If	'${NGVERSION}'>'5.1'	GUI::Basic::Auditing::Events::open tab
	Run Keyword If	'${NGVERSION}'>'5.1'	Wait Until Page Contains Element	//*[@id="auditing_event_list"]
	Run Keyword If	'${NGVERSION}'>'5.1'	Click Element	//*[@id="auditing_event_list"]
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'>'5.1'	Wait Until Page Contains Element	//*[@id="enable"]
	GUI::Basic::Auditing::Destinations::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="filetype"]
	GUI::Basic::Auditing::Destinations::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#auditing_destinations_syslog > .title_b
	Wait Until Page Contains Element	//*[@id="syslogConsole"]
	GUI::Basic::Auditing::Destinations::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#auditing_destinations_snmp > .title_b
	Wait Until Page Contains Element	//*[@id="trapEngine"]
	GUI::Basic::Auditing::Destinations::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#auditing_destinations_email > .title_b
	Wait Until Page Contains Element	//*[@id="emailServer"]

Test case to Check Dashboard Page
	[Tags]	NON-CRITICAL
	SUITE:Setup
	GUI::Basic::Dashboard::Dashboard::open Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Frame	//iframe
	GUI::Basic::Spinner Should Be Invisible
	Sleep		20s
	Run Keyword If	'${NGVERSION}'<='5.6'		Wait Until Keyword Succeeds	 1m	2s	wait until page contains element	//*[@id="kibana-body"]/div/nav/div[2]/div[1]/app-switcher/div[3]/a/div[2]
	Run Keyword If	'${NGVERSION}'>'5.6'		Wait Until Keyword Succeeds	 1m	2s	wait until page contains element		xpath=//div[@id='globalHeaderBars']/div/div/div

Test case to check Application pages
	SUITE:Setup
	Run Keyword If	'${NGVERSION}'>'5.1'	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'>'5.1'	wait until element is visible	//*[@id="virtualization_docker"]
	Run Keyword If	'${NGVERSION}'>'5.1'	Run Keyword And Continue on Failure	 Select Checkbox	 //*[@id="virtualization_docker"]
	Run Keyword If	'${NGVERSION}'>'5.1'	Run Keyword And Continue on Failure	 Select Checkbox	//*[@id="virtualization_libvirt"]
	Run Keyword If	'${NGVERSION}'>'5.1'	GUI::Basic::Save
	Run Keyword If	'${NGVERSION}'>'5.1'	GUI::Basic::Applications::Docker::open tab
	Run Keyword If	'${NGVERSION}'>'5.1'	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	Run Keyword If	'${NGVERSION}'>'5.1'	Wait Until Page Contains Element	//*[@id="refresh-icon"]
	Run Keyword If	'${NGVERSION}'>'5.1'	GUI::Basic::Applications::Virtual Machines::open tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}'>'5.1'	Sleep	5s
	Run Keyword If	'${NGVERSION}'>'5.1'	Wait Until Page Contains Element	//*[@id="refresh-icon"]
	SUITE:Disable Docker

Test case to check Filemanager
	SUITE:Setup
	GUI::Basic::System::Toolkit::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="icon_File_Manager"]
	Wait Until Keyword Succeeds	1m	5s	Switch Window	title=File Manager
	GUI::Basic::Spinner Should Be Invisible
	Select Frame	//iframe
	Wait Until Element Is Visible	xpath=//a[contains(text(),'admin_home')]

Test case to check API
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Setup
	GUI::Basic::System::Toolkit::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#icon_API
	Sleep	5s
	Switch Window	API Documentation

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Unselect Frame
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:Disable Docker
	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible	//*[@id="virtualization_docker"]
	unselect checkbox	 //*[@id="virtualization_docker"]
	unselect checkbox	//*[@id="virtualization_libvirt"]
	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible