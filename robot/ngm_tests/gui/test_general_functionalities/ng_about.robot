*** Settings ***
Documentation	Successfully Display the about information
Resource	../init.robot
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Force Tags	PART-1	GUI	${SYSTEM}	${BROWSER}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE
Default Tags	GUI

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

*** Test Cases ***
Display About information
	GUI::Basic::About	${NGVERSION}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid

SUITE:Teardown
	GUI::Basic::Logout
	Close all Browsers