*** Settings ***
Documentation	Successfull Login Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../init.robot
Force Tags	PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Test Template	SUITE:Login with valid credentails should succeed

*** Test Cases ***
Successfull admin login	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}	TRUE

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open NodeGrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	Close all Browsers

SUITE:Login with valid credentails should succeed
	[Arguments]	${USERNAME}	${PASSWORD}	${SCREENSHOT}=FALSE
	[Documentation]	Testest SUccessful logins with valid username and password combinations
	...	== REQUIREMENTS ==
	...	None
	...	== ARGUMENTS ==
	...	- USERNAME =   Username which should be used
	...	- PASSWORD  =   Password which should be used
	...	== EXPECTED RESULT ==
	...	User Sessions are expected to succeed
	[Tags]	GUI	LOGIN
	GUI::Basic::Login	${NGVERSION}	${USERNAME}	${PASSWORD}
	Run Keyword If	'${SCREENSHOT}' != 'FALSE'	Capture Page Screenshot	filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png
	GUI::Basic::Logout
