*** Settings ***
Documentation	Successfully Display the Help Files
Resource	../init.robot
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Force Tags	PART-1	GUI	${SYSTEM}	${BROWSER}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Display Help Details
	[Tags]	NON-CRITICAL
#	User guide was not updated on 5.8v
	${NGLASTVERSION}=  GUI::Basic::NG Last Version
	Skip If  '${NGLASTVERSION}'=='${True}'
	GUI::Basic::Help
	GUI::Basic::Spinner Should Be Invisible
    
Check Help link
	GUI::Basic::System::Preferences::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${HELP_LINK}=	Get Value	jquery=#help_url
	${SPLIT_VERSION}=	Split String	${NGVERSION}	.
	${PATH_VERSION}=	Set Variable	${SPLIT_VERSION}[0]_${SPLIT_VERSION}[1]
	
	IF	'${NGVERSION}' >= "5.2"  
		${STRING_URL}=  Convert To String  https://www.zpesystems.com/nodegrid/v${PATH_VERSION}/NodegridManual${NGVERSION}.html
	ELSE
		${STRING_URL}=  Convert To String  https://www.zpesystems.com/ng/v${PATH_VERSION}/NodegridManual${NGVERSION}.html
	END

	Should Be Equal  ${HELP_LINK}  ${STRING_URL}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
