*** Settings ***
Documentation	Testing ----> import/export functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../init.robot
Force Tags	PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${ROOT_LOGIN}	shell sudo su -
${EXPORT_SETTINGS1}	export_settings /settings --plain-password --file test.config
${EXPORT_SETTINGS2}	export_settings /settings --plain-password --file test2.config
${IMPORT_SETTINGS}	import_settings --file test.config
${FACTORY_SETTINGS}	factory_settings
${CONFIRMATION_MESSAGE}	yes
${RESTORE_CONFIGURATION}	restore
${DIFF_COMMAND}	diff test.config test2.config
${EXIT_SESSION}	exit
${TRANSFER_FILE1}	scp /home/admin/test.config admin@${HOSTPEER}:/home/admin/
${TRANSFER_FILE2}	scp /home/admin/test.config admin@${HOST}:/home/admin/
${DELETE_KNOWNHOST}	rm -rf /home/root/.ssh/
${HOSTNAME_TEST}	nodegrid-testing
${PATH}	cd /home/admin
${USB_NAME}	automation._test_usb_rename
${DEVICE}	dummy_automation_device

*** Test Cases ***
Test Case to edit NG hostname
	GUI::Network::Open Settings Tab
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=hostname	${HOSTNAME_TEST}
	GUI::Basic::Save

Test Edit Session Idle Timeout
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Preferences::Open Tab
	Wait Until Element Is Visible	//*[@id='idletimeout']
	${TIMEOUT_VALUE}	Get Value	//*[@id='idletimeout']
	${STATUS}	Run Keyword And Return Status	Should Be Equal	${TIMEOUT_VALUE}	300
	GUI::Basic::Click Element	//*[@id='idletimeout']
	Run Keyword If	not ${STATUS}	Run Keywords	GUI::Basic::Input Text	//*[@id='idletimeout']	300	AND
	...	GUI::Basic::Save	AND	GUI::Basic::Click Element	//*[@id='idletimeout']
	GUI::Basic::Input Text	//*[@id='idletimeout']	0
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save

Test Modifie an Usb Port
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::Open Tab
	${BDOT}	${ADOT}	GUI::Basic::Fetch NGversion
	${HAS_USB}	Run Keyword And Return Status	Page Should Contain	usbS
	Run Keyword If	not ${HAS_USB}	Run Keywords
	...	Log	"Test Modifie an Usb Port" was excluded from that build, because the unit using v${NGVERSION} Has no Usb Port	WARN	console=yes
	...	AND	Skip
	IF	${HAS_USB}
		SUITE:Change Usb port name
		SUITE:Edit Usb Port Configuration
		SUITE:Check Usb port on Access page
	END

Test Enable Docker And Qemu/KVM
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible	//*[@id="virtualization_docker"]
	Unselect Checkbox	//*[@id="virtualization_docker"]
	wait until element is visible	//input[@id='virtualization_libvirt']
	Unselect Checkbox	//input[@id='virtualization_libvirt']
	GUI::Basic::Save If Configuration Changed	HAS_ALERT=True
	Checkbox Should Not Be Selected	//*[@id="virtualization_docker"]
	Select Checkbox	//*[@id="virtualization_docker"]
	Checkbox Should Not Be Selected	//input[@id='virtualization_libvirt']
	Select Checkbox	//input[@id='virtualization_libvirt']
	GUI::Basic::Save If Configuration Changed	HAS_ALERT=True
	GUI::Basic::Spinner Should Be Invisible
	Sleep		20s
	GUI::Basic::Logout And Close Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid

Test case to add console device
	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	NEED-REVIEW
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE}
	GUI::ManagedDevices::Add Device	${DEVICE}	device_console	127.0.0.1

Test case to perform export_settings and Host factory settings
	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	NEED-REVIEW
	CLI:Open
	Set Client Configuration	timeout=60s
	CLI:Write	${EXPORT_SETTINGS1}
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Set Client Configuration	prompt=~#
	CLI:Write	${ROOT_LOGIN}
	CLI:Write	${DELETE_KNOWNHOST}
	Set Client Configuration	prompt=)?
	CLI:Write	${TRANSFER_FILE1}
	Set Client Configuration	prompt=assword:
	CLI:Write	yes
	Set Client Configuration	prompt=~#
	CLI:Write	${PASSWORD}
	Set Client Configuration	prompt=]#
	CLI:Write	${EXIT_SESSION}
	CLI:Write	${FACTORY_SETTINGS}
	Set Client Configuration	prompt=do you want to proceed?
	CLI:Write	${RESTORE_CONFIGURATION}
	Set Client Configuration	prompt=]#
	Run Keyword And Continue On Failure	CLI:Write	${CONFIRMATION_MESSAGE}
	Close All Connections
	SUITE:Check If factory_settings done
	GUI::Basic::Change Password	admin
	GUI::Security::Open Security
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(7) > a
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="allow"]
	Select Checkbox	//*[@id="rootconsole"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save If Configuration Changed
	GUI::Basic::Spinner Should Be Invisible
	[Teardown]	SUITE:Test Teardown

Test case to perform import_settings
	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	NEED-REVIEW
	CLI:Open	HOST_DIFF=${HOSTPEER}
	Set Client Configuration	prompt=~#
	CLI:Write	${ROOT_LOGIN}
	CLI:Write	${DELETE_KNOWNHOST}
	Set Client Configuration	prompt=)?
	CLI:Write	${TRANSFER_FILE2}
	Set Client Configuration	prompt=assword:
	CLI:Write	yes
	Set Client Configuration	prompt=~#
	CLI:Write	${PASSWORD}
	Close All Connections

	CLI:Open
	# Set idle_timeout=0 to avoid lost SSH session connection during import settings
	CLI:Enter Path	/settings/system_preferences
	CLI:Set	idle_timeout=0
	CLI:Commit
	Set Client Configuration	timeout=15m
	CLI:Write	${IMPORT_SETTINGS}
	Set Client Configuration	timeout=60s
	CLI:Write	${EXPORT_SETTINGS2}
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Set Client Configuration	prompt=~#
	CLI:Write	${ROOT_LOGIN}
	Set Client Configuration	prompt=#
	CLI:Write	${PATH}
	CLI:Write	${DIFF_COMMAND}
	[Teardown]	Close All Connections

Test case to check if NG hostname settings was imported
	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Network::Settings::open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${HOSTNAME_TEST}
	Input Text	id=hostname	nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Test case to Check Session Idle Timeout settings was imported
	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Preferences::Open Tab
	Wait Until Element Is Visible	//*[@id='idletimeout']
	${TIMEOUT_VALUE}	Get Value	//*[@id='idletimeout']
	${STATUS}	Run Keyword And Return Status	Should Be Equal	${TIMEOUT_VALUE}	0

Test case to Check Usb Port settings was imported
	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::Open Tab
	${BDOT}	${ADOT}	GUI::Basic::Fetch NGversion
	${HAS_USB}	Run Keyword And Return Status	Page Should Contain	usbS
	Run Keyword If	not ${HAS_USB}	Run Keywords
	...	Log	"Test Check Imported Settings about Usb Port" was excluded from that build, because the unit using v${NGVERSION} Has no Usb Port	WARN	console=yes
	...	AND	Skip
	IF	${HAS_USB}
		Page Should Contain	${USB_NAME}
		GUI::Basic::Spinner Should Be Invisible
		GUI::Basic::Click Element	(//a[contains(text(),'${USB_NAME}')])[1]
		GUI::Basic::Spinner Should Be Invisible
		Element Should Contain	//select[@id='baud']	115200
		Element Should Contain	//select[@id='status']	Enabled
		Page Should Contain Image	(//img[@src='/icon/usb.png'])[1]
		Checkbox Should Be Selected	//input[@id='dataflow']
		Checkbox Should Be Selected	//input[@id='discname']
		SUITE:Check Usb port on Access page
	END

Test case to Check Docker And Qemu/KVM settings was imported
	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	Checkbox Should Be Selected	//*[@id="virtualization_docker"]
	Checkbox Should Be Selected	//input[@id='virtualization_libvirt']

Test case to check console access availability after import settings
	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	GUI::Access::Access ${DEVICE} Device With Console

*** Keywords ***
Suite:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Change NG hostname to default
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Edit Session Idle Timeout to Default
	SUITE:Edit Usb device to Default
	SUITE:Disable Docker And Qemu/KVM
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Check If Host Has Factory Settings
	Close All Browsers
	${1ST_LOGIN}	Run Keyword And Return Status	GUI::Basic::Open And Login Nodegrid
	Close All Browsers
	IF	'${NGVERSION}' >= '5.0' and not ${1ST_LOGIN}
		${STATUS}	Run Keyword And Return Status	GUI::Basic::Open And Login Nodegrid	USERNAME=${DEFAULT_USERNAME}	PASSWORD=${QA_PASSWORD}
		Run Keyword If	${STATUS}	GUI::Basic::Enable Security For SSH Root Access By GUI and Change the Passwords By CLI
	END
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	SUITE:Change NG hostname to default
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Edit Session Idle Timeout to Default
	SUITE:Edit Usb device to Default
	SUITE:Disable Docker And Qemu/KVM
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${DEVICE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

SUITE:Test Teardown
	GUI::Basic::Check If Host Has Factory Settings
	Close All Browsers
	${1ST_LOGIN}	Run Keyword And Return Status	GUI::Basic::Open And Login Nodegrid
	Close All Browsers
	IF	'${NGVERSION}' >= '5.0' and not ${1ST_LOGIN}
	${STATUS}	Run Keyword And Return Status	GUI::Basic::Open And Login Nodegrid	USERNAME=${DEFAULT_USERNAME}	PASSWORD=${QA_PASSWORD}
	Run Keyword If	${STATUS}	GUI::Basic::Enable Security For SSH Root Access By GUI and Change the Passwords By CLI
	END
	Close All Browsers

SUITE:Console Command Output
	[Arguments]	${COMMAND}
	[Documentation]	Input the given command into the ttyd terminal and retrieve the output after prompt lines
	...	== ARGUMENTS ==
	...	-	COMMAND = Command to be executed
	...	== EXPECTED RESULT ==
	...	Input the command into the ttyd terminal and returns the OUTPUT between the command and the last prompt

	${CHECK_TAB}=	Run Keyword And Return Status	Should Not Contain	${COMMAND}	TAB
	Run Keyword If	${CHECK_TAB}	Press Keys	//body	${COMMAND}	RETURN
	...	ELSE	Press Keys	//body	${COMMAND}
	Sleep	1s

SUITE:Login Successful
	GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	GUI::Basic::Spinner Should Be Invisible

SUITE:Check If factory_settings done
	Sleep	30s
	Close All Browsers
	Wait Until Keyword Succeeds	6m	10s	SUITE:Login Successful
	Run Keyword If	'${NGVERSION}' == '3.2'	SUITE:Post Factory Settingsb
	Run Keyword If	'${NGVERSION}' >= '5.0'	GUI::Basic::Open And Login Nodegrid	POST_FACTORY_LOGIN=Yes
	Sleep	10s
	Wait Until Element Is Enabled	pwl
	GUI::Basic::Spinner Should Be Invisible

SUITE:Post Factory Settingsb
	Input Text	username	${DEFAULT_USERNAME}
	Input Text	password	${DEFAULT_PASSWORD}
	Click Element	id=login-btn
	GUI::Basic::Spinner Should Be Invisible

SUITE:Change NG hostname to default
	GUI::Basic::Network::Settings::open tab
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=hostname	nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save If Configuration Changed
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	nodegrid

SUITE:Rename Usb to Default
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::Open Tab
	${USB_ID_NAME}	SUITE:Get Usb ID Name
	GUI::Basic::Select Checkbox	//tr[@id='${USB_NAME}']//input[@type='checkbox']
	GUI::Basic::Click Element	//input[@value='Rename']
	GUI::Basic::Click Element	//input[@id='spm_name']
	GUI::Basic::Input Text	//input[@id='spm_name']	${USB_ID_NAME}
	GUI::Basic::Save If Configuration Changed

SUITE:Get Usb ID Name
	${USB_ID_NAME}	Set Variable	usbS2
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::Open Tab
	${USB_ID_NAME2}	Get Text	(//a[contains(text(),'usbS')])[2]
	${STATUS}	Run Keyword And Return Status	Should Match Regexp	${USB_ID_NAME2}	usbS[0-5]-.
	${USB_NAME_PART1}	Run Keyword If	${STATUS}	Fetch From Left	${USB_ID_NAME2}	-
	IF	${STATUS}
		${USB_ID_NAME}	Set Variable	${USB_NAME_PART1}-2
	END
	Log To Console	\n\nFirst Usb Default Name Port:\n\n${USB_ID_NAME}
	[Return]	${USB_ID_NAME}

SUITE:Set Usb Configuration to Default
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::Open Tab
	Wait Until Element Is Visible	(//a[contains(text(),'usbS')])[2]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	(//a[contains(text(),'${USB_NAME}')])[1]
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	//select[@id='baud']	9600
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	//select[@id='status']	Disabled
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//input[@value='Select Icon']
	GUI::Basic::Click Element	//img[@src='/icon/terminal.png']
	GUI::Basic::Click Element	//input[@value='Select Icon']
	GUI::Basic::Unselect Checkbox	//input[@id='dataflow']
	GUI::Basic::Unselect Checkbox	//input[@id='discname']
	GUI::Basic::Save If Configuration Changed
	[Teardown]	SUITE:Cancel or Return if needed

SUITE:Cancel or Return if needed
	[Arguments]	${RE-LOGIN}=No
	GUI::Basic::Spinner Should Be Invisible
	${HAS_CANCEL_BUTTON}	Run Keyword And Return Status	Page Should Contain Button	//input[@id='cancelButton']
	Run Keyword If	${HAS_CANCEL_BUTTON}	Run Keyword And Ignore Error	GUI::Basic::Button::Cancel
	GUI::Basic::Spinner Should Be Invisible
	${HAS_RETURN_BUTTON}	Run Keyword And Return Status	Page Should Contain Button	//input[@id='returnButton']
	Run Keyword If	${HAS_RETURN_BUTTON}	Run Keyword And Ignore Error	GUI::Basic::Click Element	//input[@id='returnButton']
	IF	'${RE-LOGIN}' == 'Yes'
		Close All Browsers
		Wait until Keyword Succeeds	3x	3s	GUI::Basic::Open And Login Nodegrid	ALIAS=HOST_BROWSER_LEFT
		GUI::Basic::Spinner Should Be Invisible
		Wait until Keyword Succeeds	3x	3s	GUI::Basic::Open And Login Nodegrid	PAGE=${HOMEPAGESHARED}	ALIAS=PEER_BROWSER_RIGHT
		GUI::Basic::Spinner Should Be Invisible
	END

SUITE:Edit Session Idle Timeout to Default
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Preferences::Open Tab
	Wait Until Element Is Visible	//*[@id='idletimeout']
	${TIMEOUT_VALUE}	Get Value	//*[@id='idletimeout']
	${STATUS}	Run Keyword And Return Status	Should Be Equal	${TIMEOUT_VALUE}	0
	GUI::Basic::Click Element	//*[@id='idletimeout']
	Run Keyword If	not ${STATUS}	Run Keywords	GUI::Basic::Input Text	//*[@id='idletimeout']	0	AND
	...	GUI::Basic::Save	AND	GUI::Basic::Click Element	//*[@id='idletimeout']
	GUI::Basic::Input Text	//*[@id='idletimeout']	300
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save

SUITE:Edit Usb device to Default
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::Open Tab
	${BDOT}	${ADOT}	GUI::Basic::Fetch NGversion
	${HAS_USB}	Run Keyword And Return Status	Page Should Contain	usbS
	IF	${HAS_USB}
		${HAS_USB_NAME}	Run Keyword And Return Status	Page Should Contain	${USB_NAME}
		Run Keyword If	${HAS_USB_NAME}	Run Keywords	SUITE:Set Usb Configuration to Default	AND	SUITE:Rename Usb to Default
	END

SUITE:Disable Docker And Qemu/KVM
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible	//*[@id="virtualization_docker"]
	Unselect Checkbox	//*[@id="virtualization_docker"]
	wait until element is visible	//input[@id='virtualization_libvirt']
	Unselect Checkbox	//input[@id='virtualization_libvirt']
	GUI::Basic::Save If Configuration Changed	HAS_ALERT=True
	GUI::Basic::Spinner Should Be Invisible
	Sleep		10s


SUITE:Change Usb port name
	${HAS_USB_NAME}	Run Keyword And Return Status	Page Should Contain	${USB_NAME}
	Run Keyword If	${HAS_USB_NAME}	Run Keywords	SUITE:Set Usb Configuration to Default	AND	SUITE:Rename Usb to Default
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::Open Tab
	Wait Until Element Is Visible	(//a[contains(text(),'usbS')])[2]
	${USB_ID_NAME}	Get Text	(//a[contains(text(),'usbS')])[2]
	GUI::Basic::Select Checkbox	//tr[@id='${USB_ID_NAME}']//input[@type='checkbox']
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//input[@value='Rename']
	GUI::Basic::Click Element	//input[@id='spm_name']
	GUI::Basic::Input Text	//input[@id='spm_name']	${USB_NAME}
	GUI::Basic::Save If Configuration Changed

SUITE:Edit Usb Port Configuration
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	(//a[contains(text(),'${USB_NAME}')])[1]
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	//select[@id='baud']	115200
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	//select[@id='status']	Enabled
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//input[@value='Select Icon']
	GUI::Basic::Click Element	//img[@src='/icon/usb.png']
	GUI::Basic::Click Element	//input[@value='Select Icon']
	GUI::Basic::Select Checkbox	//input[@id='dataflow']
	GUI::Basic::Select Checkbox	//input[@id='discname']
	GUI::Basic::Save

SUITE:Check Usb port on Access page
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Image	(//img[@src='/icon/usb.png'])[1]
	Page Should Contain	${USB_NAME}