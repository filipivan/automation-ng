*** Settings ***
Resource	../init.robot
Documentation	Test factory settings toolkit functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Force Tags   PART-1	CLI	GUI ${BROWSER}	FACTORY_SETTINGS	FACTORY_SETTINGS_FUNCTIONALITY	RESET

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
# Configuration for persistency
${CONFIG_CUSTOMFIELD_NAME}	customfield_testing_Factory
${CONFIG_CUSTOMFIELD_VALUE}	customvalue_testing_Factory
${CONFIG_DEVICE_NAME}	myself
${CONFIG_DEVICE_TYPE}	device_console
${CONFIG_DEVICE_IP}	127.0.0.1
${CONFIG_DEVICE_USER}	${DEFAULT_USERNAME}
${CONFIG_DEVICE_PASSWD}	${DEFAULT_PASSWORD}
${CONFIG_HOST_IP}	1.1.1.1
${CONFIG_HOST_NAME}	aliasTestingFactory
${CONFIG_HOST_ALIAS}	aliasTestingFactory
${CONFIG_LOCALACCOUNT_NAME}	user_testing_Factory
${CONFIG_LOCALACCOUNT_PASSWD}	user_testing_Factory

*** Test Cases ***
Test DD Command Right After Upgrade
	[Tags]	NON-CRITICAL
	Repeat Keyword	3 times	SUITE:Test DD
	[Teardown]	CLI:Switch Connection	default

Check M2MGMT Service
	[Tags]	EXCLUDEIN3_2
	Skip If	${IS_VM}
	CLI:Connect As Root
	SUITE:Check M2MGMT
	[Teardown]	CLI:Switch Connection	default

Test Copyright on Login Page Right After Upgrade
	GUI::Basic::Open NodeGrid
	GUI::Basic::Spinner Should Be Invisible
	${CURRENT_YEAR}	Evaluate	'{dt.year}'.format(dt=datetime.datetime.now())	modules=datetime
	LOG	${CURRENT_YEAR}
	${GOT_DATE}	Get Text	xpath=//*[@id="login-copyright"]
	LOG	${GOT_DATE}
	Should Be True	'© 2013-${CURRENT_YEAR} ZPE Systems®, Inc.' == '${GOT_DATE}'
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

Test Copyright on Dashboard Right After Upgrade
	[Tags]		NON-CRITICAL		BUG_NG-12178	EXCLUDEIN_OFFICIAL	#excluding from official while not all versions with fix are released.
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	${CURRENT_YEAR}	Evaluate	'{dt.year}'.format(dt=datetime.datetime.now())	modules=datetime
	LOG	${CURRENT_YEAR}
	IF	'${NGVERSION}' <= '5.2'
		${GOT_DATE}	Get Text	xpath=//div[8]/span
	ELSE
		${GOT_DATE}	Get Text	xpath=//div[9]/span
	END
	LOG	${GOT_DATE}
	Should Be True	'© 2013-${CURRENT_YEAR} ZPE Systems®, Inc.' == '${GOT_DATE}'
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

Test apply Factory Settings
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	CLI:Open
	CLI:Apply Factory Settings	Yes
	[Teardown]	Run Keyword If	${NGVERSION} <= 4.2	CLI:Wait Nodegrid Webserver To Be Up

Test admin password should be changed at login time
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Change Password At Login Time
	CLI:Wait Nodegrid Webserver To Be Up	${HOST}	${QA_PASSWORD}	2m
	[Teardown]	CLI:Close Connection

Test SSH and Serial Console access as root should be disabled
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	[Setup]	CLI:Open	${DEFAULT_USERNAME}	${QA_PASSWORD}
	SUITE:Access Options For Root Should Be Disabled
	SUITE:Connecting As Root Should Fail
	[Teardown]	CLI:Close Connection

Test root password should be changed at login time
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	[Setup]	CLI:Open	${DEFAULT_USERNAME}	${QA_PASSWORD}
	CLI:Enable Ssh And Console Access As Root
	CLI:Change Password At Login Time	root	root	prompt=~#
	[Teardown]	Run Keywords	CLI:Change Root And Admin Password To QA Default
	...	AND	CLI:Close Connection

Test configuration should not persist
	[Setup]	Run Keywords	CLI:Open	AND	CLI:Connect As Root
	SUITE:Configuration Should Not Persist
	Run Keyword If	'${NGVERSION}' >= '4.2'	SUITE:Users Home Folders Should Be Empty
	SUITE:Previous Logs Should Be Clean
	[Teardown]	CLI:Close Connection

Test save diff between current /etc and factory /etc
	[Setup]	CLI:Connect As Root
	CLI:Write	tar -C /tmp/ -xzf /media/hdCnf/backup/factory_default_files.tgz
	CLI:Write	diff -r /etc/ /tmp/etc/ > /backup/test-factory-etc.diff
	[Teardown]	CLI:Close Connection

*** Keywords ***
SUITE:Setup
	CLI:Open
	${IS_VM}	CLI:Is VM System
	Set Suite variable	${IS_VM}
	SUITE:Delete Persistency Check Configuration
	SUITE:Add Configuration To Check Persistency
	Run Keyword If	'${NGVERSION}' >= '4.2'	SUITE:Add Files To Users Folders
	${TIME_SECONDS_BEFORE_FACTORY}=	CLI:Get Current System Time In Seconds
	Set Suite Variable	${TIME_SECONDS_BEFORE_FACTORY}

SUITE:Teardown
	Run Keyword If Any Tests Failed
	...	Fatal Error	Configuration was kept after applying factory settings to the system

	CLI:Close Connection

SUITE:Test DD
	CLI:Connect As Root
	Write	time dd if=/dev/zero of=/var/sw/xyz bs=1024 count=10000000
	Sleep	60s
	${OUTPUT}	CLI:Read Until Prompt
	${OUTPUT}	Split To Lines	${OUTPUT}	4	-3
	${OUTPUT}	Convert To String	${OUTPUT[0]}
	Should Match Regexp	${OUTPUT}	real\\t0m[0-4]\\d.\\d{3}s
	${SYS_VERSION}	CLI:Write	cli show /system/about software	user=yes
	${TIME}	CLI:Write	date	user=yes
	Write	mkdir /var/DD
	Read Until Prompt
	Write	echo 'Current Date: ${TIME}${SPACE}${SPACE}${SPACE}${SYS_VERSION}${SPACE}${SPACE}${SPACE}time: ${OUTPUT}' >> /var/DD/DD_TIME.log
	CLI:Read Until Prompt
	CLI:Write	rm /var/sw/xyz
	CLI:Close Current Connection

SUITE:Check M2MGMT
	${OUTPUT}	CLI:Write	ps -ef | grep m2m
	Should Contain	${OUTPUT}	/usr/sbin/m2mgmtd

SUITE:Add Configuration To Check Persistency
	CLI:Connect as Root
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Write	> /etc/scripts/custom_commands/config_test.py
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Write	/bin/echo "def CustomTest(dev):" > /etc/scripts/custom_commands/config_test.py
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Write	/bin/echo " print('hello')" >> /etc/scripts/custom_commands/config_test.py

	Switch Connection	default
	CLI:Enter Path	/settings/custom_fields/
	CLI:Add
	CLI:Set	field_name=${CONFIG_CUSTOMFIELD_NAME} field_value=${CONFIG_CUSTOMFIELD_VALUE}
	CLI:Commit
	CLI:Add Device	${CONFIG_DEVICE_NAME}	${CONFIG_DEVICE_TYPE}	${CONFIG_DEVICE_IP}	${CONFIG_DEVICE_USER}	${CONFIG_DEVICE_PASSWD}

	CLI:Enter Path	/settings/hosts
	CLI:Add
	CLI:Set	ip_address=${CONFIG_HOST_IP} hostname=${CONFIG_HOST_NAME} alias=${CONFIG_HOST_ALIAS}
	CLI:Commit

	CLI:Enter Path	/settings/local_accounts
	CLI:Add
	CLI:Set	username=${CONFIG_LOCALACCOUNT_NAME} password=${CONFIG_LOCALACCOUNT_PASSWD}
	CLI:Commit

	${CHECKSUM_BEFORE}=	CLI:Generate System Checksum
	Set Suite Variable	${CHECKSUM_BEFORE}

SUITE:Configuration Should Not Persist
	CLI:Switch Connection	root_session
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Write	cd /etc/scripts/custom_commands/
	${OUTPUT}=	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Write	ls
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Not Contain	${OUTPUT}	config_test.py

	CLI:Switch Connection	default
	${OUTPUT}=	CLI:Show	/settings/custom_fields/
	Should Not Contain	${OUTPUT}	${CONFIG_CUSTOMFIELD_NAME}

	${OUTPUT}=	CLI:Show	/settings/devices/
	Should Not Contain	${OUTPUT}	${CONFIG_DEVICE_NAME}

	${OUTPUT}=	CLI:Show	/settings/hosts/
	Should Not Contain	${OUTPUT}	${CONFIG_HOST_IP}

	${OUTPUT}=	CLI:Show	/settings/local_accounts/
	Should Not Contain	${OUTPUT}	${CONFIG_LOCALACCOUNT_NAME}

	${CHECKSUM_AFTER}=	CLI:Generate System Checksum

	Should Not Be Equal	${CHECKSUM_BEFORE}	${CHECKSUM_AFTER}

SUITE:Add Files To Users Folders
	CLI:Write	shell sudo touch /home/admin/test-file-factory-admin.txt
	CLI:Write	shell sudo mkdir /home/admin/test-folder-factory-admin
	CLI:Write	shell sudo touch /home/root/test-file-factory-root.txt
	CLI:Write	shell sudo mkdir /home/root/test-folder-factory-root

SUITE:Access Options For Root Should Be Disabled
	CLI:Enter Path	/settings/services
	${SHOW_OUTPUT}=    CLI:Show
    Should Contain	${SHOW_OUTPUT}	ssh_allow_root_access = no
    Should Contain	${SHOW_OUTPUT}	allow_root_console_access = no

SUITE:Connecting As Root Should Fail
	${STATUS}=	Run Keyword And Return Status	CLI:Connect As Root
	Run Keyword If 	${STATUS} == ${TRUE}
	...	Fail	Should not be able to login as root on 5.0+ using default configuration
	CLI:Close Connection

SUITE:Users Home Folders Should Be Empty
	${OUTPUT}=	CLI:Write	shell sudo ls /home/root/	user=Yes
	Should Be Empty	${OUTPUT}
	${OUTPUT}=	CLI:Write	shell sudo ls /home/admin/	user=Yes
	Should Be Empty	${OUTPUT}

SUITE:Previous Logs Should Be Clean
	SUITE:Previous Data Logs Should Be Clean
	SUITE:Previous SNR Should Be Clean
	SUITE:Previous Event Logs Should Be Clean

SUITE:Previous Data Logs Should Be Clean
	${OUTPUT}=	CLI:Write	shell sudo ls /var/local/DB	user=Yes
	Should Be Empty	${OUTPUT}
	${OUTPUT}=	CLI:Write	shell sudo ls /var/unsent/DB	user=Yes
	Should Be Empty	${OUTPUT}

SUITE:Previous SNR Should Be Clean
	${OUTPUT}=	CLI:Write	shell sudo ls /var/local/SNR	user=Yes
	Should Be Empty	${OUTPUT}
	${OUTPUT}=	CLI:Write	shell sudo ls /var/unsent/SNR	user=Yes
	Should Be Empty	${OUTPUT}

SUITE:Previous Event Logs Should Be Clean
	${OUTPUT}=	CLI:Write	shell sudo cat /var/local/EVT/*
	${TIMESTAMPS}=	Get Regexp Matches	${OUTPUT}	<(.*)[A-Z]>	1
	FOR		${TIMESTAMP}	IN	@{TIMESTAMPS}
		${TIME_SECONDS}=	Convert Date	${TIMESTAMP}	date_format=%Y-%m-%dT%H:%M:%S	result_format=epoch
		Should Be True	'${TIME_SECONDS_BEFORE_FACTORY}' < '${TIME_SECONDS}'
	END

	${OUTPUT}=	CLI:Write	shell sudo cat /var/unsent/EVT/*
	${TIMESTAMPS}=	Get Regexp Matches	${OUTPUT}	<(.*)[A-Z]>	1
	FOR		${TIMESTAMP}	IN	@{TIMESTAMPS}
		${TIME_SECONDS}=	Convert Date	${TIMESTAMP}	date_format=%Y-%m-%dT%H:%M:%S	result_format=epoch
		Should Be True	'${TIME_SECONDS_BEFORE_FACTORY}' < '${TIME_SECONDS}'
	END

SUITE:Delete Persistency Check Configuration
	CLI:Delete Devices	${CONFIG_DEVICE_NAME}
	CLI:Enter Path	/settings/custom_fields/
	Run Keyword If	'${NGVERSION}' == '4.2'	CLI:Delete If Exists	1
	...	ELSE	CLI:Delete If Exists	${CONFIG_CUSTOMFIELD_NAME}
	CLI:Enter Path	/settings/hosts/
	CLI:Delete If Exists	${CONFIG_HOST_IP}
	CLI:Delete Users	${CONFIG_LOCALACCOUNT_NAME}