*** Settings ***
Resource	../init.robot
Documentation	Downgrade the system to latest released image, then upgrade the nodegrid image with the latest available image from ftp server
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Force Tags   PART-1	CLI	GUI	${BROWSER}	SWUPGRADE	SWDOWNGRADE	DEPENDENCE	DEPENDENCE_SERVER

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${FTP_PRESENT}	${FTPSERVER_PRESENT}
${FTP_URL}	${FTPSERVER_URL}
${FTP_IP}	${FTPSERVER_IP}
${FTP_PORT}	${FTPSERVER_PORT}
${FTP_USER}	${FTPSERVER_USER}
${FTP_PASSWD}	${FTPSERVER_PASSWORD}
${RELEASE_PATH}	/releases/

# Version regular expressions
${ISO_REGEX_3_2}	\\s(nodegrid-genericx86-64-\\d+_Branch-NG_3.2_\\d+_R\\d+-r\\d+.iso)\\s
${ISO_REGEX_4_2}	\\s(nodegrid-genericx86-64-\\d+_Branch-NG_4.2_\\d+_R\\d+-r\\d+.iso)\\s
${ISO_REGEX_5_0}	\\s(nodegrid-genericx86-64-\\d+_Branch-NG_5.0-\\d+.iso)\\s
${ISO_REGEX_5_2}	\\s(nodegrid-genericx86-64-\\d+_Branch-NG_5.2-\\d+.signed.iso)\\s
${ISO_REGEX_5_4}	\\s(nodegrid-genericx86-64-\\d+_Branch-NG_5.4-\\d+.signed.iso)\\s
${ISO_REGEX_5_6}	\\s(nodegrid-genericx86-64-\\d+_Branch-NG_5.6-\\d+.signed.iso)\\s
${ISO_REGEX_5_8}	\\s(nodegrid-genericx86-64-\\d+_Branch-NG_5.8-\\d+.signed.iso)\\s
${ISO_REGEX_VERSION}	\\s(nodegrid-genericx86-64-\\d+_Branch-NG_${NGVERSION}-\\d+.iso)\\s
${ISO_REGEX_VERSION_SVN}	\\s(nodegrid-genericx86-64-\\d+_Branch-NG_${NGVERSION}_\\d+_R\\d+-r\\d+.iso)\\s
${ISO_REGEX_MASTER}	\\s(nodegrid-genericx86-64-\\d+_Master-\\d+.signed.iso)\\s

# Configuration for persistency
${CONFIG_CUSTOMFIELD_NAME}	customfield_testing_swupgrade
${CONFIG_CUSTOMFIELD_VALUE}	customvalue_testing_swupgrade
${CONFIG_DEVICE_NAME}	myself
${CONFIG_DEVICE_TYPE}	device_console
${CONFIG_DEVICE_IP}	127.0.0.1
${CONFIG_DEVICE_USER}	${DEFAULT_USERNAME}
${CONFIG_DEVICE_PASSWD}	${DEFAULT_PASSWORD}
${CONFIG_HOST_IP}	1.1.1.1
${CONFIG_HOST_NAME}	aliasTestingSwUpgrade
${CONFIG_HOST_ALIAS}	aliasTestingSwUpgrade
${CONFIG_LOCALACCOUNT_NAME}	user_testing_swupgrade
${CONFIG_LOCALACCOUNT_PASSWD}	user_testing_swupgrade

# Helper variables to filter test cases execution
${OFFICIAL_UPGRADE}	${FALSE}
${WILL_OFFICIAL_UPGRADE}	${FALSE}

*** Test Cases ***
Find Images For 3.2 Official Build
	[Tags]	EXCLUDEIN_NIGHTLY	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

	${RELEASE_3_2}	${PATH_RELEASE_3_2}=	SUITE:Find The Latest Release Image	${ISO_REGEX_3_2}
	Set Suite Variable	${RELEASE_3_2}
	Set Suite Variable	${PATH_RELEASE_3_2}

	Log	\nReleased Nodegrid Image For 3.2: '${PATH_RELEASE_3_2}'	INFO	console=yes

Find Images For 3.2 Nightly Build
	[Tags]	EXCLUDEIN_OFFICIAL	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

	${RELEASE_3_2}	${PATH_RELEASE_3_2}=	SUITE:Find The Latest Release Image	${ISO_REGEX_3_2}
	${DAILY_3_2}	${PATH_DAILY_3_2}=		SUITE:Find The Latest Daily Image	${ISO_REGEX_3_2}
	Set Suite Variable	${RELEASE_3_2}
	Set Suite Variable	${PATH_RELEASE_3_2}
	Set Suite Variable	${DAILY_3_2}
	Set Suite Variable	${PATH_DAILY_3_2}

	Log	\nReleased Nodegrid Image For 3.2: '${PATH_RELEASE_3_2}'	INFO	console=yes
	Log	\nDaily Nodegrid Image For 3.2: '${PATH_DAILY_3_2}'	INFO	console=yes

Find Images For 4.2 Official Build
	[Tags]	EXCLUDEIN_NIGHTLY	EXCLUDEIN3_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade
	${RELEASE_4_2}	${PATH_RELEASE_4_2}=	SUITE:Find The Latest Release Image	${ISO_REGEX_4_2}
	Set Suite Variable	${RELEASE_4_2}
	Set Suite Variable	${PATH_RELEASE_4_2}

	Log	Released Nodegrid Image For 4.2: '${PATH_RELEASE_4_2}'	INFO	console=yes

Find Images For 4.2 Nightly Build
	[Tags]	EXCLUDEIN_OFFICIAL	EXCLUDEIN3_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

	${RELEASE_4_2}	${PATH_RELEASE_4_2}=	SUITE:Find The Latest Release Image	${ISO_REGEX_4_2}
	${DAILY_4_2}	${PATH_DAILY_4_2}=		SUITE:Find The Latest Daily Image	${ISO_REGEX_4_2}
	Set Suite Variable	${RELEASE_4_2}
	Set Suite Variable	${PATH_RELEASE_4_2}
	Set Suite Variable	${DAILY_4_2}
	Set Suite Variable	${PATH_DAILY_4_2}

	Log	\nReleased Nodegrid Image For 4.2: '${PATH_RELEASE_4_2}'	INFO	console=yes
	Log	\nDaily Nodegrid Image For 4.2: '${PATH_DAILY_4_2}'	INFO	console=yes

Find Images For 5.0 Official Build
	[Tags]	EXCLUDEIN_NIGHTLY	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

	${DAILY_4_2}	${PATH_DAILY_4_2}=		SUITE:Find The Latest Daily Image	${ISO_REGEX_4_2}
	${RELEASE_5_0}	${PATH_RELEASE_5_0}=	SUITE:Find The Latest Release Image	${ISO_REGEX_5_0}
	Set Suite Variable	${DAILY_4_2}
	Set Suite Variable	${PATH_DAILY_4_2}
	Set Suite Variable	${RELEASE_5_0}
	Set Suite Variable	${PATH_RELEASE_5_0}

	Log	\nDaily Nodegrid Image For 4.2: '${PATH_DAILY_4_2}'	INFO	console=yes
	Log	\nReleased Nodegrid Image For 5.0: '${PATH_RELEASE_5_0}'	INFO	console=yes

Find Images For 5.0 Nightly Build
	[Tags]	EXCLUDEIN_OFFICIAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

	${RELEASE_5_0}	${PATH_RELEASE_5_0}=	SUITE:Find The Latest Release Image	${ISO_REGEX_5_0}
	${DAILY_5_0}	${PATH_DAILY_5_0}=		SUITE:Find The Latest Daily Image	${ISO_REGEX_5_0}
	Set Suite Variable	${RELEASE_5_0}
	Set Suite Variable	${PATH_RELEASE_5_0}
	Set Suite Variable	${DAILY_5_0}
	Set Suite Variable	${PATH_DAILY_5_0}

	Log	\nReleased Nodegrid Image For 5.0: '${PATH_RELEASE_5_0}'	INFO	console=yes
	Log	\nDaily Nodegrid Image For 5.0: '${PATH_DAILY_5_0}'	INFO	console=yes

Find Images For 5.2 Official Build
	[Tags]	EXCLUDEIN_NIGHTLY	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

	${DAILY_5_0}	${PATH_DAILY_5_0}=	SUITE:Find The Latest Daily Image	${ISO_REGEX_5_0}
	${RELEASE_5_2}	${PATH_RELEASE_5_2}=		SUITE:Find The Latest Release Image	${ISO_REGEX_5_2}
	Set Suite Variable	${DAILY_5_0}
	Set Suite Variable	${PATH_DAILY_5_0}
	Set Suite Variable	${RELEASE_5_2}
	Set Suite Variable	${PATH_RELEASE_5_2}

	Log	\nDaily Nodegrid Image For 5.0: '${PATH_DAILY_5_0}'	INFO	console=yes
	Log	\nReleased Nodegrid Image For 5.2: '${PATH_RELEASE_5_2}'	INFO	console=yes

Find Images For 5.2 Nightly Build
	[Tags]	EXCLUDEIN_OFFICIAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

	${RELEASE_5_2}	${PATH_RELEASE_5_2}=	SUITE:Find The Latest Release Image	${ISO_REGEX_5_2}
	${DAILY_5_2}	${PATH_DAILY_5_2}=		SUITE:Find The Latest Daily Image	${ISO_REGEX_5_2}
	Set Suite Variable	${RELEASE_5_2}
	Set Suite Variable	${PATH_RELEASE_5_2}
	Set Suite Variable	${DAILY_5_2}
	Set Suite Variable	${PATH_DAILY_5_2}

	Log	\nReleased Nodegrid Image For 5.2: '${PATH_RELEASE_5_2}'	INFO	console=yes
	Log	\nDaily Nodegrid Image For 5.2: '${PATH_DAILY_5_2}'	INFO	console=yes

Find Images For 5.4 Official Build
	[Tags]	EXCLUDEIN_NIGHTLY	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

	${DAILY_5_2}	${PATH_DAILY_5_2}=	SUITE:Find The Latest Daily Image	${ISO_REGEX_5_2}
	${RELEASE_5_4}	${PATH_RELEASE_5_4}=	SUITE:Find The Latest Release Image	${ISO_REGEX_5_4}
	Set Suite Variable	${DAILY_5_2}
	Set Suite Variable	${PATH_DAILY_5_2}
	Set Suite Variable	${RELEASE_5_4}
	Set Suite Variable	${PATH_RELEASE_5_4}

	Log	\nDayly Nodegrid Image For 5.2: '${PATH_DAILY_5_2}'	INFO	console=yes
	Log	\nReleased Nodegrid Image For 5.4: '${PATH_RELEASE_5_4}'	INFO	console=yes

Find Images For 5.4 Nightly Build
	[Tags]	EXCLUDEIN_OFFICIAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

	${RELEASE_5_4}	${PATH_RELEASE_5_4}=	SUITE:Find The Latest Release Image	${ISO_REGEX_5_4}
	${DAILY_5_4}	${PATH_DAILY_5_4}=		SUITE:Find The Latest Daily Image	${ISO_REGEX_5_4}
	Set Suite Variable	${RELEASE_5_4}
	Set Suite Variable	${PATH_RELEASE_5_4}
	Set Suite Variable	${DAILY_5_4}
	Set Suite Variable	${PATH_DAILY_5_4}

	Log	\nReleased Nodegrid Image For 5.4: '${PATH_RELEASE_5_4}'	INFO	console=yes
	Log	\nDaily Nodegrid Image For 5.4: '${PATH_DAILY_5_4}'	INFO	console=yes

Find Images For 5.6 Official Build
	[Tags]	EXCLUDEIN_NIGHTLY	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_8	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

	${DAILY_5_4}	${PATH_DAILY_5_4}=	SUITE:Find The Latest Daily Image	${ISO_REGEX_5_4}
	${RELEASE_5_6}	${PATH_RELEASE_5_6}=	SUITE:Find The Latest Release Image	${ISO_REGEX_5_6}
	Set Suite Variable	${DAILY_5_4}
	Set Suite Variable	${PATH_DAILY_5_4}
	Set Suite Variable	${RELEASE_5_6}
	Set Suite Variable	${PATH_RELEASE_5_6}

	Log	\nDayly Nodegrid Image For 5.4: '${PATH_DAILY_5_4}'	INFO	console=yes
	Log	\nReleased Nodegrid Image For 5.6: '${PATH_RELEASE_5_6}'	INFO	console=yes

Find Images For 5.6 Nightly Build
	[Tags]	EXCLUDEIN_OFFICIAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_8	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

	${RELEASE_5_6}	${PATH_RELEASE_5_6}=		SUITE:Find The Latest Release Image	${ISO_REGEX_5_6}
	${DAILY_5_6}	${PATH_DAILY_5_6}=		SUITE:Find The Latest Daily Image	${ISO_REGEX_5_6}

	Set Suite Variable	${RELEASE_5_6}
	Set Suite Variable	${PATH_RELEASE_5_6}
	Set Suite Variable	${DAILY_5_6}
	Set Suite Variable	${PATH_DAILY_5_6}

	Log	\nReleased Nodegrid Image For 5.6: '${PATH_RELEASE_5_6}'	INFO	console=yes
	Log	\nDaily Nodegrid Image For 5.6: '${PATH_DAILY_5_6}'	INFO	console=yes

Find Images For 5.8 Official Build
	[Tags]	EXCLUDEIN_NIGHTLY	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

	${DAILY_5_6}	${PATH_DAILY_5_6}=	SUITE:Find The Latest Daily Image	${ISO_REGEX_5_6}
	${RELEASE_5_8}	${PATH_RELEASE_5_8}=	SUITE:Find The Latest Release Image	${ISO_REGEX_5_8}
	Set Suite Variable	${DAILY_5_6}
	Set Suite Variable	${PATH_DAILY_5_6}
	Set Suite Variable	${RELEASE_5_8}
	Set Suite Variable	${PATH_RELEASE_5_8}

	Log	\nDayly Nodegrid Image For 5.6: '${PATH_DAILY_5_6}'	INFO	console=yes
	Log	\nReleased Nodegrid Image For 5.8: '${PATH_RELEASE_5_8}'	INFO	console=yes

Find Images For 5.8 Nightly Build
	[Tags]	EXCLUDEIN_OFFICIAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

#	${DAILY_5_4}	${PATH_DAILY_5_4}=	SUITE:Find The Latest Daily Image	${ISO_REGEX_5_4}
##	${RELEASE_5_6}	${PATH_RELEASE_5_6}=		SUITE:Find The Latest Release Image	${ISO_REGEX_5_6}	Should be updated when v5.6 != v5.8
#	${DAILY_5_8}	${PATH_DAILY_5_8}=		SUITE:Find The Latest Daily Image	${ISO_REGEX_MASTER}
	${RELEASE_5_8}	${PATH_RELEASE_5_8}=	SUITE:Find The Latest Release Image	${ISO_REGEX_5_8}
	${DAILY_5_8}	${PATH_DAILY_5_8}=		SUITE:Find The Latest Daily Image	${ISO_REGEX_5_8}
#	Set Suite Variable	${RELEASE_5_6}	Should be updated when v5.6 != v5.8
#	Set Suite Variable	${PATH_RELEASE_5_6}	Should be updated when v5.6 != v5.8
	Set Suite Variable	${RELEASE_5_8}
	Set Suite Variable	${PATH_RELEASE_5_8}
	Set Suite Variable	${DAILY_5_8}
	Set Suite Variable	${PATH_DAILY_5_8}

	Log	\nReleased Nodegrid Image For 5.6: '${PATH_RELEASE_5_8}'	INFO	console=yes
	Log	\nDaily Nodegrid Image For 5.8: '${PATH_DAILY_5_8}'	INFO	console=yes

Find Images For 5.10 Nightly Build
	[Tags]	EXCLUDEIN_OFFICIAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

	${RELEASE_5_8}	${PATH_RELEASE_5_8}=		SUITE:Find The Latest Release Image	${ISO_REGEX_5_8}
#	${RELEASE_5_8}	${PATH_RELEASE_5_8}=		SUITE:Find The Latest Release Image	${ISO_REGEX_5_8} CHANGE WHEN 5.8!=5.10
	${DAILY_5_10}	${PATH_DAILY_5_10}=		SUITE:Find The Latest Daily Image	${ISO_REGEX_MASTER}

	Set Suite Variable	${RELEASE_5_8}
	Set Suite Variable	${PATH_RELEASE_5_8}
#	Set Suite Variable	${RELEASE_5_8}	CHANGE WHEN 5.8!=5.10
#	Set Suite Variable	${PATH_RELEASE_5_8}	CHANGE WHEN 5.8!=5.10
	Set Suite Variable	${DAILY_5_10}
	Set Suite Variable	${PATH_DAILY_5_10}

	Log	\nReleased Nodegrid Image For 5.8: '${PATH_RELEASE_5_8}'	INFO	console=yes
#	Log	\nReleased Nodegrid Image For 5.8: '${PATH_RELEASE_5_8}'	INFO	console=yes CHANGE WHEN 5.8!=5.10
	Log	\nDaily Nodegrid Image For 5.10: '${PATH_DAILY_5_10}'	INFO	console=yes

Software Upgrade/Downgrade in 3.2 Nightly Build
	[Tags]	EXCLUDEIN_OFFICIAL	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade
	${IS_CRITICAL_3_2_RELEASE}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_RELEASE_3_2}
	${IS_CRITICAL_3_2_DAILY}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_DAILY_3_2}
	SUITE:Check If Final Nodegrid Version Is The Same As NGVERSION
	Run Keyword If	not ${IS_CRITICAL_3_2_RELEASE} and not ${IS_CRITICAL_3_2_DAILY}
	...	Set Tags	NON-CRITICAL

Software Upgrade/Downgrade in 3.2 Official Build
	[Tags]	EXCLUDEIN_NIGHTLY	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for applying SW Upgrade

	${IS_CRITICAL_3_2_RELEASE}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_RELEASE_3_2}
	SUITE:Check If Final Nodegrid Version Is The Same As NGVERSION
	Run Keyword If	not ${IS_CRITICAL_3_2_RELEASE}	Set Tags	NON-CRITICAL

Software Upgrade/Downgrade in 4.2 Nightly Build
	[Tags]	EXCLUDEIN_OFFICIAL	EXCLUDEIN3_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

	${IS_CRITICAL_4_2_RELEASE}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_RELEASE_4_2}
	${IS_CRITICAL_4_2_DAILY}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_DAILY_4_2}
	SUITE:Check If Final Nodegrid Version Is The Same As NGVERSION
	Run Keyword If	not ${IS_CRITICAL_4_2_RELEASE} and not ${IS_CRITICAL_4_2_DAILY}
	...	Set Tags	NON-CRITICAL

Software Upgrade/Downgrade in 4.2 Official Build
	[Tags]	EXCLUDEIN_NIGHTLY	EXCLUDEIN3_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for applying SW Upgrade

	${IS_CRITICAL_4_2_RELEASE}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_RELEASE_4_2}
	SUITE:Check If Final Nodegrid Version Is The Same As NGVERSION
	Run Keyword If	not ${IS_CRITICAL_4_2_RELEASE}	Set Tags	NON-CRITICAL

Software Upgrade/Downgrade in 5.0 Nightly Build
	[Tags]	EXCLUDEIN_OFFICIAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

	${IS_CRITICAL_5_0_RELEASE}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_RELEASE_5_0}
	${IS_CRITICAL_5_0_DAILY}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_DAILY_5_0}
	SUITE:Check If Final Nodegrid Version Is The Same As NGVERSION
	Run Keyword If	not ${IS_CRITICAL_5_0_RELEASE} and not ${IS_CRITICAL_5_0_DAILY}
	...	Set Tags	NON-CRITICAL

Software Upgrade/Downgrade in 5.0 Release Build
	[Tags]	EXCLUDEIN_NIGHTLY	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

	${IS_CRITICAL_4_2_DAILY}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_DAILY_4_2}
	${IS_CRITICAL_5_0_RELEASE}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_RELEASE_5_0}
	SUITE:Check If Final Nodegrid Version Is The Same As NGVERSION
	Run Keyword If	not ${IS_CRITICAL_4_2_DAILY} and not ${IS_CRITICAL_5_0_RELEASE}
	...	Set Tags	NON-CRITICAL

Software Upgrade/Downgrade in 5.2 Nightly Build
	[Tags]	EXCLUDEIN_OFFICIAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

	${IS_CRITICAL_5_2_RELEASE}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_RELEASE_5_2}
	${IS_CRITICAL_5_2_DAILY}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_DAILY_5_2}
	SUITE:Check If Final Nodegrid Version Is The Same As NGVERSION
	Run Keyword If	not ${IS_CRITICAL_5_2_RELEASE} and not ${IS_CRITICAL_5_2_DAILY}
	...	Set Tags	NON-CRITICAL

Software Upgrade/Downgrade in 5.2 Official Build
	[Tags]	EXCLUDEIN_NIGHTLY	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

	${IS_CRITICAL_5_0_DAILY}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_DAILY_5_0}
	${IS_CRITICAL_5_2_RELEASE}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_RELEASE_5_2}
	SUITE:Check If Final Nodegrid Version Is The Same As NGVERSION
	Run Keyword If	not ${IS_CRITICAL_5_0_DAILY} and not ${IS_CRITICAL_5_2_RELEASE}
	...	Set Tags	NON-CRITICAL

Software Upgrade/Downgrade in 5.4 Nightly Build
	[Tags]	EXCLUDEIN_OFFICIAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

	${IS_CRITICAL_5_4_RELEASE}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_RELEASE_5_4}
	${IS_CRITICAL_5_4_DAILY}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_DAILY_5_4}
	SUITE:Check If Final Nodegrid Version Is The Same As NGVERSION
	Run Keyword If	not ${IS_CRITICAL_5_4_RELEASE} and not ${IS_CRITICAL_5_4_DAILY}
	...	Set Tags	NON-CRITICAL

Software Upgrade/Downgrade in 5.4 Official Build
	[Tags]	EXCLUDEIN_NIGHTLY	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

	${IS_CRITICAL_5_2_DAILY}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_DAILY_5_2}
	${IS_CRITICAL_5_4_RELEASE}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_RELEASE_5_4}
	SUITE:Check If Final Nodegrid Version Is The Same As NGVERSION
	Run Keyword If	not ${IS_CRITICAL_5_2_DAILY} and not ${IS_CRITICAL_5_4_RELEASE}
	...	Set Tags	NON-CRITICAL

Software Upgrade/Downgrade in 5.6 Nightly Build
	[Tags]	EXCLUDEIN_OFFICIAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_8	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

	${IS_CRITICAL_5_6_RELEASE}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_RELEASE_5_6}
	${IS_CRITICAL_5_6_DAILY}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_DAILY_5_6}
	SUITE:Check If Final Nodegrid Version Is The Same As NGVERSION
	Run Keyword If	not ${IS_CRITICAL_5_6_RELEASE} and not ${IS_CRITICAL_5_6_DAILY}
	...	Set Tags	NON-CRITICAL

Software Upgrade/Downgrade in 5.6 Official Build
	[Tags]	EXCLUDEIN_NIGHTLY	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_8	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

	${IS_CRITICAL_5_4_DAILY}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_DAILY_5_4}
	${IS_CRITICAL_5_6_RELEASE}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_RELEASE_5_6}
	SUITE:Check If Final Nodegrid Version Is The Same As NGVERSION
	Run Keyword If	not ${IS_CRITICAL_5_4_DAILY} and not ${IS_CRITICAL_5_6_RELEASE}
	...	Set Tags	NON-CRITICAL

Software Upgrade/Downgrade in 5.8 Nightly Build
	[Tags]	EXCLUDEIN_OFFICIAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

#	${IS_CRITICAL_5_6_RELEASE}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_RELEASE_5_6}	Should be updated when v5.6 != v5.8
	${IS_CRITICAL_5_8_RELEASE}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_RELEASE_5_8}
	${IS_CRITICAL_5_8_DAILY}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_DAILY_5_8}
	SUITE:Check If Final Nodegrid Version Is The Same As NGVERSION
#	Run Keyword If	not ${IS_CRITICAL_5_6_RELEASE} and not ${IS_CRITICAL_5_8_DAILY}	Should be updated when v5.6 != v5.8
	Run Keyword If	not ${IS_CRITICAL_5_8_RELEASE} and not ${IS_CRITICAL_5_8_DAILY}
	...	Set Tags	NON-CRITICAL

Software Upgrade/Downgrade in 5.8 Official Build
	[Tags]	EXCLUDEIN_NIGHTLY	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_10
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

	${IS_CRITICAL_5_6_DAILY}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_DAILY_5_6}
	${IS_CRITICAL_5_8_RELEASE}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_RELEASE_5_8}
	SUITE:Check If Final Nodegrid Version Is The Same As NGVERSION
	Run Keyword If	not ${IS_CRITICAL_5_6_DAILY} and not ${IS_CRITICAL_5_8_RELEASE}
	...	Set Tags	NON-CRITICAL

Software Upgrade/Downgrade in 5.10 Nightly Build
	[Tags]	EXCLUDEIN_OFFICIAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	Skip If	not ${FTP_PRESENT}	No FTP server available for testing Software Upgrade/Downgrade

	${IS_CRITICAL_5_8_RELEASE}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_RELEASE_5_8}

#	${IS_CRITICAL_5_8_RELEASE}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_RELEASE_5_8} CHANGE WHEN 5.8!=5.10
	${IS_CRITICAL_5_10_DAILY}=	SUITE:Execute Software Upgrade/Downgrade	${PATH_DAILY_5_10}
	SUITE:Check If Final Nodegrid Version Is The Same As NGVERSION	5.10
	Run Keyword If	not ${IS_CRITICAL_5_8_RELEASE} and not ${IS_CRITICAL_5_10_DAILY}

	...	Set Tags	NON-CRITICAL
#	Run Keyword If	not ${IS_CRITICAL_5_8_RELEASE} and not ${IS_CRITICAL_5_10_DAILY}
#	...	Set Tags	NON-CRITICAL CHANGE WHEN 5.8!=5.10


*** Keywords ***
SUITE:Setup
	Skip If	not ${FTP_PRESENT}	No FTP server available for applying SW Upgrade

	${MASTER_VAR_EXIST}=	Run Keyword And Return Status	Variable Should Exist	${ISMASTER}
	${MASTER_VERSION}=	Run Keyword If	${MASTER_VAR_EXIST}	Set Variable	${NGVERSION}
	Set Suite Variable	${MASTER_VERSION}

	CLI:Open
	SUITE:Delete Configuration From Testing Persistency
	CLI:Connect As Root

SUITE:Teardown
	Skip If	not ${FTP_PRESENT}	No FTP server available for applying SW Upgrade/Downgrade

	CLI:Open
	SUITE:Delete Configuration From Testing Persistency
	CLI:Close Connection
	Run Keyword If Any Tests Failed	Fatal Error	SW Upgrade Test Suite failed because of a Test Case

SUITE:Connect To FTP
	[Arguments]	${IP}=${FTP_IP}	${PORT}=${FTP_PORT}	${USER}=${FTP_USER}	${PASSWD}=${FTP_PASSWD}
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nftp ${IP} ${PORT}	INFO	console=yes
	Write	ftp ${IP} ${PORT}
	${OUTPUT1}=	Read Until	Name (${IP}:
	Write	${USER}
	${OUTPUT2}=	Read Until	Password:
	Write	${PASSWD}
	${OUTPUT3}=	Read Until	ftp>
	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${OUTPUT1}\n${OUTPUT2}\n${OUTPUT3}	INFO	console=yes

	${FAIL_AUTH}=	Run Keyword And Return Status	Should Not Contain	${OUTPUT3}	Login successful
	Run Keyword If	${FAIL_AUTH}	Fatal Error	Authentication failed to ftp ${IP} ${PORT}

	Set Client Configuration	prompt=ftp>

SUITE:Disconnect From FTP
	Set Client Configuration	prompt=#
	CLI:Write	quit

SUITE:Execute Software Upgrade/Downgrade
	[Arguments]	${FTP_FILEPATH}
	Set Test Variable	${IS_CRITICAL}	${TRUE}
	Run Keyword And Continue On Failure
	...	SUITE:Execute Software Upgrade/Downgrade Implementation	${FTP_FILEPATH}
	[Return]	${IS_CRITICAL}

SUITE:Execute Software Upgrade/Downgrade Implementation
	[Arguments]	${FTP_FILEPATH}
	CLI:Switch Connection	default
	${CURR_FULL_VERSION}=	CLI:Get System Version
	${FTP_PATH}	${IMAGE}=	Split String From Right	${FTP_FILEPATH}	/	max_split=1
	${IMAGE_FULL_VERSION}=	SUITE:Download ISO Image And Get Its Full Version	${FTP_URL}${FTP_FILEPATH}	${IMAGE}
	Run Keyword If	'${IMAGE_FULL_VERSION}' == '${CURR_FULL_VERSION}'	Run Keywords
	...	SUITE:Clear Content From /var/sw	AND
	...	Log To Console	\nCurrent Nodegrid version and ISO version are already the same\nISO: ${IMAGE_FULL_VERSION}\nCurrent: ${CURR_FULL_VERSION}\n	AND
	...	Return From Keyword

	Set Suite Variable	${IMAGE_FULL_VERSION}
	Set Suite Variable	${CURR_FULL_VERSION}
	Log To Console	\n------------------------------------------------------------------------------		console=yes
	Log To Console	\nSoftware upgrade from ${CURR_FULL_VERSION} to ${IMAGE_FULL_VERSION}
	Log To Console	\n------------------------------------------------------------------------------		console=yes

	SUITE:Delete Configuration From Testing Persistency
	${SPLIT_IMAGE}	Split String From Right	${IMAGE_FULL_VERSION}	.
	${SPLIT_CURR}	Split String From Right	${CURR_FULL_VERSION}	.
	IF	${SPLIT_IMAGE}[0]${SPLIT_IMAGE}[1] == ${SPLIT_CURR}[0]${SPLIT_CURR}[1]
		Log	\nSPLIT_IMAGE[0]SPLIT_CURR[1] == SPLIT_CURR[0]SPLIT_CURR[1]?: ${SPLIT_IMAGE}[0]${SPLIT_IMAGE}[1] == ${SPLIT_CURR}[0]${SPLIT_CURR}[1] \n	console=yes
		${IS_UPGRADE}=	Run Keyword And Return Status
		...	Should Be True	${SPLIT_IMAGE}[2] > ${SPLIT_CURR}[2]
		Log	SPLIT_IMAGE[2] > SPLIT_CURR[2]: ${SPLIT_IMAGE}[2] > ${SPLIT_CURR}[2] \n	console=yes
	ELSE
		${IS_UPGRADE}=	Run Keyword And Return Status
		...	Should Be True	${SPLIT_IMAGE}[0]${SPLIT_IMAGE}[1] > ${SPLIT_CURR}[0]${SPLIT_CURR}[1]
	END
	Log	\n------------------------------------------------------------------------------	console=yes
	Log	\nIS_UPGRADE?: ${IS_UPGRADE}	console=yes
	Log	\n------------------------------------------------------------------------------	console=yes

	Run Keyword If	${IS_UPGRADE}	SUITE:Generate Checksum Before Adding Configuration
	Run Keyword If	${IS_UPGRADE}	SUITE:Add Configuration To Check Persistency In Upgrade
	Run Keyword If	${IS_UPGRADE}	SUITE:Check If Checksum Changed After Adding Configuration

	${STATUS}=	SUITE:Execute Software Upgrade Command	${IMAGE}

	# Workaround: It will try a second time to run software upgrade
	${IS_ISO_MOUNT_ERROR}=	Run Keyword And Return Status
	...	Should Contain	${STATUS}	Error: Could not mount ISO image
	${NEW_STATUS}=	Run Keyword If	${IS_ISO_MOUNT_ERROR}	SUITE:Download ISO And Mount Again	${IMAGE}	${FTP_FILEPATH}
	${STATUS}=	Set Variable If	${IS_ISO_MOUNT_ERROR}	${NEW_STATUS}	${STATUS}

	${IS_WEBSERVER_ERROR}=	Run Keyword And Return Status
	...	Should Contain	${STATUS}	Error: Response from WEBSERVER has timed out
	Run Keyword If	not ${IS_WEBSERVER_ERROR}
	...	Should Contain	${STATUS}	The system is going down for reboot NOW!
	Run Keyword If	${IS_WEBSERVER_ERROR}	Run Keywords
	...	Set Client Configuration	timeout=120s	AND
	...	Read Until	The system is going down for reboot NOW!	AND
	...	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}

	CLI:Close Connection
	SUITE:Connect Again And Change Password If Needed

	CLI:Open
	CLI:Connect As Root
	Run Keyword If	${IS_UPGRADE}	SUITE:Check Persistency
	CLI:Switch Connection	default
	${CUR_FULL_VERSION}=	CLI:Get System Version
	Should Be Equal	${CUR_FULL_VERSION}	${IMAGE_FULL_VERSION}

	Set Test Variable	${IS_CRITICAL}	${FALSE}
	Run Keyword If	${IS_WEBSERVER_ERROR} or ${IS_ISO_MOUNT_ERROR}
	...	Fail	SW Upgrade Test failed as NON-CRITICAL due to Webserver timeout or ISO mount error

SUITE:Execute Software Upgrade Command
	[Arguments]	${LOCAL_IMAGE}
	CLI:Switch Connection	default
	CLI:Write	software_upgrade
	CLI:Set	image_location=local_system filename=${LOCAL_IMAGE} format_partitions_before_upgrade=no if_downgrading=restore_configuration_saved_on_version_upgrade
	Write	commit
	${STATUS}=	Read Until Regexp	(The system is going down for reboot NOW!)|(Error: Could not mount ISO image)
	Sleep	120s
	[Return]	${STATUS}

SUITE:Delete Configuration From Testing Persistency
	CLI:Switch Connection	default
	${CUR_VERSION}=	CLI:Get System Major And Minor Version
	CLI:Delete Devices	${CONFIG_DEVICE_NAME}
	CLI:Enter Path	/settings/custom_fields/
	CLI:Delete If Exists	${CONFIG_CUSTOMFIELD_NAME}	1	#1 was added as workaround once 4.2 official deletes using index. Can be removed after next 4.2 release
	CLI:Enter Path	/settings/hosts/
	CLI:Delete If Exists	${CONFIG_HOST_IP}
	CLI:Delete Users	${CONFIG_LOCALACCOUNT_NAME}

SUITE:Add Configuration To Check Persistency In Upgrade
	CLI:Switch Connection	root_session
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Write	> /etc/scripts/custom_commands/config_test.py
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Write	/bin/echo "def CustomTest(dev):" > /etc/scripts/custom_commands/config_test.py
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Write	/bin/echo " print('hello')" >> /etc/scripts/custom_commands/config_test.py

	CLI:Switch Connection	default
	CLI:Enter Path	/settings/custom_fields/
	CLI:Add
	CLI:Set	field_name=${CONFIG_CUSTOMFIELD_NAME} field_value=${CONFIG_CUSTOMFIELD_VALUE}
	CLI:Commit
	CLI:Add Device	${CONFIG_DEVICE_NAME}	${CONFIG_DEVICE_TYPE}	${CONFIG_DEVICE_IP}	${CONFIG_DEVICE_USER}	${CONFIG_DEVICE_PASSWD}

	CLI:Enter Path	/settings/hosts
	CLI:Add
	CLI:Set	ip_address=${CONFIG_HOST_IP} hostname=${CONFIG_HOST_NAME} alias=${CONFIG_HOST_ALIAS}
	CLI:Commit

	CLI:Enter Path	/settings/local_accounts
	CLI:Add
	CLI:Set	username=${CONFIG_LOCALACCOUNT_NAME} password=${CONFIG_LOCALACCOUNT_PASSWD}
	CLI:Commit

SUITE:Check Persistency
	CLI:Switch Connection	root_session
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Write	cd /etc/scripts/custom_commands/
	${OUTPUT}=	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Write	ls
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	config_test.py
	${CONTENT}=	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Write	/bin/cat config_test.py
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${CONTENT}	def CustomTest(dev):
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Write	rm config_test.py

	CLI:Switch Connection	default
	${CUR_VERSION}=	CLI:Get System Major And Minor Version
	CLI:Show	/settings/custom_fields/
	${PATH_BY_INDEX}	Run Keyword And Return Status	CLI:Show	/settings/custom_fields/1
	${OUTPUT}=	Run Keyword If	'${CUR_VERSION}' == '4.2' and ${PATH_BY_INDEX}
	...	CLI:Show	/settings/custom_fields/1
	...	ELSE	CLI:Show	/settings/custom_fields/${CONFIG_CUSTOMFIELD_NAME}
	Should Match Regexp	${OUTPUT}	field( |_)name( =|:) ${CONFIG_CUSTOMFIELD_NAME}
	Should Match Regexp	${OUTPUT}	field( |_)value( =|:) ${CONFIG_CUSTOMFIELD_VALUE}

	${OUTPUT}=	CLI:Show	/settings/devices/${CONFIG_DEVICE_NAME}/access
	Should Contain	${OUTPUT}	name: ${CONFIG_DEVICE_NAME}
	Should Contain	${OUTPUT}	type = ${CONFIG_DEVICE_TYPE}
	Should Contain	${OUTPUT}	ip_address = ${CONFIG_DEVICE_IP}
	Should Contain	${OUTPUT}	username = ${CONFIG_DEVICE_USER}
	Should Contain	${OUTPUT}	password =

	${OUTPUT}=	CLI:Show	/settings/hosts/${CONFIG_HOST_IP}
	Should Match Regexp	${OUTPUT}	ip( |_)address: ${CONFIG_HOST_IP}
	Should Contain	${OUTPUT}	hostname = ${CONFIG_HOST_NAME}
	Should Contain	${OUTPUT}	alias = ${CONFIG_HOST_ALIAS}

	${OUTPUT}=	CLI:Show	/settings/local_accounts/${CONFIG_LOCALACCOUNT_NAME}
	Should Contain	${OUTPUT}	username: ${CONFIG_LOCALACCOUNT_NAME}
	Should Contain	${OUTPUT}	password =

SUITE:Generate Checksum Before Adding Configuration
	${CHECKSUM}	CLI:Generate System Checksum
	Set Suite Variable	${CHECKSUM_BEFORECONFIG}	${CHECKSUM}

SUITE:Check If Checksum Changed After Adding Configuration
	CLI:Switch Connection	default
	${CHECKSUM}	CLI:Generate System Checksum
	Set Suite Variable	${CHECKSUM_AFTERCONFIG}	${CHECKSUM}
	Set Suite Variable	${CHECKSUM_BEFOREDOWNGRADE}	${CHECKSUM}

	${IS_SAME_CHECKSUM}=	Run Keyword And Return Status
	...	Should Be Equal	${CHECKSUM_BEFORECONFIG}	${CHECKSUM_AFTERCONFIG}
	Run Keyword If	${IS_SAME_CHECKSUM}
	...	Fatal Error	SW Upgrade Test Suite failed due MD5CheckSum did not change after adding configuration

SUITE:Find The Latest Release Image
	[Arguments]	${IMAGE_REGEX}
	CLI:Switch Connection	root_session
	SUITE:Connect To FTP
	CLI:Write	cd ${RELEASE_PATH}
	${OUTPUT}=	CLI:Write	ls
	${DISCONNECTION}=	Run Keyword And Return Status	Should Contain	${OUTPUT}	Failed to establish connection.
	Run Keyword If	${DISCONNECTION}	Fatal Error	Could not connect to ftp ${FTP_IP} ${FTP_PORT}
	${OUTPUT}=	Fetch From Left	${OUTPUT}	226 Directory send OK
	${OUTPUT}=	Fetch From Right	${OUTPUT}	Here comes the directory listing.
	${LINES}=	Split To Lines	${OUTPUT}

	# needs to get paths to navigate (i.e.: releases from 2019 and 2020 are different paths)
	${SUBPATHS}=	Create List
	FOR	${LINE}	IN	@{LINES}
		Continue For Loop If	'${LINE}' == '${EMPTY}'
		${YEAR}=	Fetch From Right	${LINE}	${SPACE}
		Append To List	${SUBPATHS}	${YEAR}
	END

	Reverse List	${SUBPATHS}
	${OUTPUT}=	Set Variable
	${FOUND_IMAGE}=	Set Variable	${FALSE}
	FOR	${SUBPATH}	IN	@{SUBPATHS}
		${OUTPUT}=	CLI:Write	ls ${RELEASE_PATH}${SUBPATH}
		${FOUND_IMAGE}=	Run Keyword And Return Status	Should Match Regexp	${OUTPUT}	${IMAGE_REGEX}
		Continue For Loop If	not ${FOUND_IMAGE}
		Set Suite Variable	${YEAR}	${SUBPATH}
		Exit For Loop If	${FOUND_IMAGE}
	END
	SUITE:Disconnect From FTP

	Run Keyword If	not ${FOUND_IMAGE}	Fatal Error	Could not find the release image

	${RELEASES}=	Get Regexp Matches	${OUTPUT}	${IMAGE_REGEX}	1
	${RELEASE_NAME}=	Set Variable	${RELEASES}[-1]
	${PATH_RELEASE}=	Set Variable	${RELEASE_PATH}${YEAR}/${RELEASE_NAME}
	[Return]	${RELEASE_NAME}	${PATH_RELEASE}

SUITE:Find The Latest Daily Image
	[Arguments]	${IMAGE_REGEX}
	CLI:Switch Connection	root_session
	SUITE:Connect To FTP
	${OUTPUT}=	CLI:Write	ls
	SUITE:Disconnect From FTP

	${DISCONNECTION}=	Run Keyword And Return Status	Should Contain	${OUTPUT}	Failed to establish connection.
	Run Keyword If	${DISCONNECTION}	Fatal Error	Could not connect to ftp ${FTP_IP} ${FTP_PORT}

	${DAILY_OUTPUT_IMAGES}=	Get Regexp Matches	${OUTPUT}	${IMAGE_REGEX}	1
	${FOUND_IMAGES}=	Run Keyword And Return Status	Should Not Be Empty	${DAILY_OUTPUT_IMAGES}
	Run Keyword If	not ${FOUND_IMAGES}	Fatal Error	Could not find the daily image

	Reverse List	${DAILY_OUTPUT_IMAGES}
	${FOUND_IMAGE}=	Set Variable	${FALSE}
	${DAILY}=	Set Variable	${EMPTY}
	FOR	${IMAGE}	IN	@{DAILY_OUTPUT_IMAGES}
		${IMAGE_HAS_MD5}=	Run Keyword And Return Status	Should Contain	${OUTPUT}	${IMAGE}.md5
		${FOUND_IMAGE}=	Set Variable If	${IMAGE_HAS_MD5}	${TRUE}	${FALSE}
		${DAILY}=	Set Variable	${IMAGE}
		Exit For Loop If	${FOUND_IMAGE}
	END
	Run Keyword If	not ${FOUND_IMAGE}	Fatal Error	Could not find the daily image with md5 file

	${PATH_DAILY}=	Set Variable	/${DAILY}
	[Return]	${DAILY}	${PATH_DAILY}

SUITE:Download ISO Image And Get Its Full Version
	[Arguments]	${FTP_IMAGE_PATH}	${IMAGE}
	CLI:Switch Connection	root_session
	CLI:Write	rm -rf /var/sw/*
	CLI:Write	cd /var/sw/
	Set Client Configuration	timeout=480s
	${OUTPUT_ISO}=	CLI:Write	wget --user=${FTPSERVER_USER} --password=${FTPSERVER_PASSWORD} ${FTP_IMAGE_PATH}
	Should Contain	${OUTPUT_ISO}	${IMAGE} saved

	${OUTPUT_ISO_MD5}=	CLI:Write	wget --user=${FTPSERVER_USER} --password=${FTPSERVER_PASSWORD} ${FTP_IMAGE_PATH}.md5
	Should Contain	${OUTPUT_ISO_MD5}	${IMAGE}.md5 saved
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}

	${MD5_CONTENT}=	CLI:Write	cat ${IMAGE}.md5
	${HASH_MD5_FILE}=	Get Substring	${MD5_CONTENT}	0	32
	${OUTPUT_MD5SUM_COMMAND}=	CLI:Write	md5sum ${IMAGE}
	${HASH_ISO_MD5SUM_OUTPUT}=	Get Substring	${OUTPUT_MD5SUM_COMMAND}	0	32

	${MD5SUM_IS_EQUAL}=	Run Keyword And Return Status	Should Be Equal	${HASH_MD5_FILE}	${HASH_ISO_MD5SUM_OUTPUT}
	Log To Console	\n\n\n-------\n-------\nNodegrid image dowloaded is safe if: ${HASH_MD5_FILE} == ${HASH_ISO_MD5SUM_OUTPUT}\n-------\n-------\n\n\n
	Run Keyword If	not ${MD5SUM_IS_EQUAL}	Fatal Error	The MD5 hash value of .iso file downloaded does not match against the original sum in .md5. The file was not downloaded properly and some of its elements might have been corrupted (Check FTP server, it might be full).

	CLI:Write	mkdir -p /tmp/${IMAGE}-test-ng-img/ /tmp/${IMAGE}-test-rootfs/
	CLI:Write	mount /var/sw/${IMAGE} /tmp/${IMAGE}-test-ng-img/
	CLI:Write	mount /tmp/${IMAGE}-test-ng-img/rootfs.img /tmp/${IMAGE}-test-rootfs/

	${VERSION}=	CLI:Write	cat /tmp/${IMAGE}-test-rootfs/software | grep ^VERSION= | cut -d '=' -f 2	user=Yes

	CLI:Write	umount /tmp/${IMAGE}-test-rootfs/
	CLI:Write	umount /tmp/${IMAGE}-test-ng-img/
	[Return]	${VERSION}

SUITE:Clear Content From /var/sw
	CLI:Switch Connection	root_session
	CLI:Write	rm -rf /var/sw/*

SUITE:Connect Again And Change Password If Needed
	Sleep	120s
	${NEEDS_TO_CHANGE_PASSWD}=	Wait Until Keyword Succeeds	15m	10s	SUITE:Try To Open Connection
	Log To Console	\nNeeds to Change password: ${NEEDS_TO_CHANGE_PASSWD}\n
	CLI:Close Connection

	${IMAGE_VERSION}	${REVISION}=	Split String From Right	${IMAGE_FULL_VERSION}	.	max_split=1
	Run Keyword If	${NEEDS_TO_CHANGE_PASSWD}
	...	CLI:QA Device First Settings	${HOST}	${IMAGE_VERSION}
	[Teardown]	CLI:Close Connection

SUITE:Try To Open Connection
	${OPEN_VALUE}	${MSG}=	Run Keyword And Ignore Error	CLI:Open
	Log	\nOpen connection value: ${OPEN_VALUE}\n	INFO	console=yes
	Log	\nOpen Connection message: ${MSG}\n	INFO	console=yes
	${NEEDS_TO_CHANGE_PASSWD}=	Run Keyword If	'${OPEN_VALUE}' == 'FAIL'	Run Keyword And Return Status
	...	Should Contain	${MSG}	Authentication failed for user '${DEFAULT_USERNAME}'
	...	ELSE	Set Variable	${FALSE}
	Run Keyword If	not ${NEEDS_TO_CHANGE_PASSWD} and '${OPEN_VALUE}' == 'FAIL'	Fail	Fail to connect to host
	[Return]	${NEEDS_TO_CHANGE_PASSWD}
	[Teardown]	CLI:Close Connection

SUITE:Check If Final Nodegrid Version Is The Same As NGVERSION
	[Arguments]	${NG_VERSION}=${NGVERSION}
	CLI:Switch Connection	default
	${FINAL_VERSION}=	CLI:Get System Major And Minor Version
	Log To Console	\nCheck if FINAL VERSION == NGVERSION
	Log To Console	\n${FINAL_VERSION} == ${NG_VERSION}\n
	Should Be True	'${FINAL_VERSION}' == '${NG_VERSION}'

SUITE:Download ISO And Mount Again
	[Tags]	BUG_NG_5181
	[Arguments]	${LOCAL_IMAGE}	${FTP_FILEPATH}
	CLI:Switch Connection	default
	CLI:Cancel
	SUITE:Download ISO Image And Get Its Full Version	${FTP_URL}${FTP_FILEPATH}	${LOCAL_IMAGE}
	CLI:Switch Connection	default
	${NEW_STATUS}=	SUITE:Execute Software Upgrade Command	${LOCAL_IMAGE}
	[Return]	${NEW_STATUS}