*** Settings ***
Resource	../init.robot
Documentation	Check Sanity and factory settings Host, Peer and Hostshared if the build environment has it.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Force Tags   PART-1	CLI	GUI	${BROWSER}	CHROME	FIREFOX	FACTORY_SETTINGS	FACTORY_SETTINGS_CLEAR_SYSTEM

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${JENKINS_VM_USERNAME}	${GUI_JENKINS_VM_USERNAME}	#From FRE-QA-ENVIRONMENT.PY
${JENKINS_VM_PASSWORD}	${GUI_JENKINS_VM_PASSWORD}	#From FRE-QA-ENVIRONMENT.PY

*** Test Cases ***
Test to check sanity of host's system
	SUITE:Check NG Sanity	${HOST}

Test to check sanity of peer's system
	SUITE:Check NG Sanity	${HOSTPEER}

Test to check sanity of hostshared's system
	Pass Execution If	not ${HAS_HOSTSHARED}	This build environment has no HOSTSHARED
	SUITE:Check NG Sanity	${HOSTSHARED}

Test DD Command Right Before Upgrade
	[Tags]	NON-CRITICAL
	CLI:Open	session_alias=host_session	HOST_DIFF=${HOST}
	Repeat Keyword	3 times	SUITE:Test DD
	[Teardown]	CLI:Switch Connection	host_session

Check M2MGMT Service
	[Tags]	EXCLUDEIN3_2
	${IS_VM}	CLI:Is VM System
	Skip If	${IS_VM}
	CLI:Connect As Root
	SUITE:Check M2MGMT
	[Teardown]	CLI:Switch Connection	host_session

Test to apply Factory Settings that is going to clear host's system
	CLI:Apply Factory Settings	Yes
	CLI:QA Device First Settings	${HOST}	${NGVERSION}
	[Teardown]	CLI:Close Connection

Test to apply Factory Settings that is going to clear peer's system
	CLI:Open	session_alias=peer_session	HOST_DIFF=${HOSTPEER}
	CLI:Apply Factory Settings	Yes
	CLI:QA Device First Settings	${HOSTPEER}	${NGVERSION}
	[Teardown]	CLI:Close Connection

Test to apply Factory Settings that is going to clear hostshared's system
	Pass Execution If	not ${HAS_HOSTSHARED}	This build environment has no HOSTSHARED
	CLI:Open	session_alias=hostshared_session	HOST_DIFF=${HOSTSHARED}
	CLI:Apply Factory Settings	Yes
	CLI:QA Device First Settings	${HOSTSHARED}	${NGVERSION}
	[Teardown]	CLI:Close Connection

*** Keywords ***
SUITE:Setup
	${NG_REACHABLE}	Set Variable	${FALSE}
	${LOGIN_SUCCESS}	Set Variable	${FALSE}
	${ASK_CURRENT_PASSWORD}	Set Variable	${FALSE}
	${HAS_STRONG_PASSWORD}	Set Variable	${FALSE}

SUITE:Teardown
	CLI:Close Connection
	Run Keyword If Any Tests Failed	Fatal Error	Clearing system failed

SUITE:Test DD
	CLI:Connect As Root
	Write	time dd if=/dev/zero of=/var/sw/xyz bs=1024 count=10000000
	Sleep	60s
	${OUTPUT}	CLI:Read Until Prompt
	${OUTPUT}	Split To Lines	${OUTPUT}	4	-3
	${OUTPUT}	Convert To String	${OUTPUT[0]}
	Should Match Regexp	${OUTPUT}	real\\t0m[0-4]\\d.\\d{3}s
	${SYS_VERSION}	CLI:Write	cli show /system/about software	user=yes
	${TIME}	CLI:Write	date	user=yes
	Write	mkdir /var/DD
	Read Until Prompt
	Write	echo 'Current Date: ${TIME}${SPACE}${SPACE}${SPACE}${SYS_VERSION}${SPACE}${SPACE}${SPACE}time: ${OUTPUT}' >> /var/DD/DD_TIME.log
	CLI:Read Until Prompt
	CLI:Write	rm /var/sw/xyz
	CLI:Close Current Connection

SUITE:Check M2MGMT
	${OUTPUT}	CLI:Write	ps -ef | grep m2m
	Should Contain	${OUTPUT}	/usr/sbin/m2mgmtd

SUITE:Check NG Sanity
	[Arguments]	${HOSTDIFF}
	${NG_REACHABLE}	SUITE:Check in case NG is not reachable	${HOSTDIFF}
	Run Keyword If	not ${NG_REACHABLE}	Fail	NG not reacheble.
	${LOGIN_SUCCESS}	Run Keyword And Return Status	CLI:Open	HOST_DIFF=${HOSTDIFF}
	Run Keyword If	${LOGIN_SUCCESS}	Pass Execution	NG (${HOSTDIFF}) sanity is OK.
	${ASK_CURRENT_PASSWORD}	SUITE:Check in case NG has factory settings asking current password	${HOSTDIFF}
	Run Keyword If	${ASK_CURRENT_PASSWORD}	Log	Jenkins build started with NG (${HOSTDIFF}) in factory settings state without come it back to default setup.	WARN
	Run Keyword If	${ASK_CURRENT_PASSWORD}	Pass Execution	NG (${HOSTDIFF}) sanity is OK after setup it from factory settings state to default.
	${HAS_STRONG_PASSWORD}	SUITE:Check in case NG has strong password	${HOSTDIFF}
	Run Keyword If	${HAS_STRONG_PASSWORD}	Log	Jenkins build started with NG (${HOSTDIFF}) using strong password without come it back to default setup.	WARN
	Run Keyword If	${HAS_STRONG_PASSWORD}	Pass Execution	NG (${HOSTDIFF}) sanity is OK after setup it from strong password to default.

SUITE:Check in case NG is not reachable
	[Arguments]	${HOSTDIFF}
	${NG_REACHABLE}	Run Keyword And Return Status	Run Keywords	CLIENT:Start Target Ping	${HOSTDIFF}
	...	AND	Sleep	3s	AND	CLIENT:End Target Ping
	[Return]	${NG_REACHABLE}

SUITE:Check in case NG has factory settings asking current password
	[Arguments]	${HOSTDIFF}
	${ASK_CURRENT_PASSWORD}	Run Keyword And Return Status
	...	SUITE:Check Current password doing ssh to NG by Jenkins VM and close All connections	${HOSTDIFF}
	...	${JENKINS_VM_USERNAME}	${JENKINS_VM_PASSWORD}
	Run Keyword If	${ASK_CURRENT_PASSWORD}
	...	SUITE:Change to default passwords and allow ssh by root doing ssh to NG by Jenkins VM	${HOSTDIFF}
	...	${JENKINS_VM_USERNAME}	${JENKINS_VM_PASSWORD}
	Run Keyword If	${ASK_CURRENT_PASSWORD}	CLI:Open	HOST_DIFF=${HOSTDIFF}
	[Return]	${ASK_CURRENT_PASSWORD}

SUITE:Check in case NG has strong password
	[Arguments]	${HOSTDIFF}
	${HAS_STRONG_PASSWORD}	Run Keyword And Return Status	CLI:Open	HOST_DIFF=${HOSTDIFF}	PASSWORD=${QA_PASSWORD}
	Run Keyword If	${HAS_STRONG_PASSWORD}	SUITE:Change Root And Admin Passwords from admin user and Allow ssh and console access as root	${HOSTDIFF}
	[Return]	${HAS_STRONG_PASSWORD}

SUITE:Check Current password doing ssh to NG by Jenkins VM and close All connections
	[Arguments]	${HOSTDIFF}	${SUITE_JENKINS_VM_USERNAME}	${SUITE_JENKINS_VM_PASSWORD}
	${HAS_CURRENT_PASSWORD}	SUITE:Should Contain Current password doing ssh to NG by Jenkins VM	${HOSTDIFF}	${SUITE_JENKINS_VM_USERNAME}	${SUITE_JENKINS_VM_PASSWORD}
	CLI:Close Connection
	[Return]	${HAS_CURRENT_PASSWORD}

SUITE:Should Contain Current password doing ssh to NG by Jenkins VM
	[Arguments]	${HOSTDIFF}	${SUITE_JENKINS_VM_USERNAME}	${SUITE_JENKINS_VM_PASSWORD}
	CLI:Open	HOST_DIFF=${CLIENTIP}	USERNAME=${SUITE_JENKINS_VM_USERNAME}	PASSWORD=${SUITE_JENKINS_VM_PASSWORD}	TYPE=shell
	CLI:Write	ssh-keygen -f "/home/${SUITE_JENKINS_VM_USERNAME}/.ssh/known_hosts" -R "${HOST_DIFF}"
	Write	ssh ${DEFAULT_USERNAME}@${HOST_DIFF}
	${OUTPUT}=	CLI:Read Until Regexp	(Password:|Are you sure you want to continue connecting \\(.*\\)\\?)
	Should Not Contain Any	${OUTPUT}	No route to host	Connection refused
	${HAS_TO_CONFIRM}=	Run Keyword And Return Status	Should Match Regexp	${OUTPUT}	Are you sure you want to continue connecting \\(.*\\)\\?
	IF	${HAS_TO_CONFIRM}
		Write	yes
		${OUTPUT}=	Read Until	Password:
		Log To Console	\n${OUTPUT}\n
	END
	Write	${DEFAULT_PASSWORD}
	${OUTPUT}=	Read Until	assword:
	Log To Console	\n${OUTPUT}\n
	Should Contain	${OUTPUT}	You are required to change your password immediately (administrator enforced)
	Should Contain	${OUTPUT}	Changing password for ${DEFAULT_USERNAME}.
	${HAS_CURRENT_PASSWORD}	Run Keyword And Return Status	Should Contain	${OUTPUT}	Current password:
	[Return]	${HAS_CURRENT_PASSWORD}

SUITE:Change to default passwords and allow ssh by root doing ssh to NG by Jenkins VM
	[Arguments]	${HOSTDIFF}	${SUITE_JENKINS_VM_USERNAME}	${SUITE_JENKINS_VM_PASSWORD}
	SUITE:Should Contain Current password doing ssh to NG by Jenkins VM	${HOSTDIFF}	${SUITE_JENKINS_VM_USERNAME}	${SUITE_JENKINS_VM_PASSWORD}
	Write	${DEFAULT_PASSWORD}
	${OUTPUT}=	Read Until	New password:
	Log To Console	\n${OUTPUT}\n
	Write	${QA_PASSWORD}
	${OUTPUT}=	Read Until	Retype new password:
	Log To Console	\n${OUTPUT}\n
	Write	${QA_PASSWORD}
	${OUTPUT}=	Read Until	]#
	${WEBSERVER_ERROR}=	Run Keyword And Return Status
	...	Should Not Contain	${OUTPUT}	Failed to connect to WEBSERVER
	Run Keyword If	${WEBSERVER_ERROR}	Write	exit
	SUITE:Change Root And Admin Passwords from admin user and Allow ssh and console access as root	${HOSTDIFF}

SUITE:Change Root And Admin Passwords from admin user and Allow ssh and console access as root
	[Arguments]	${HOSTDIFF}
	CLI:Change Root And Admin Passwords from admin user	HOST_DIFF=${HOSTDIFF}
	SUITE:Allow ssh and console access as root	${HOSTDIFF}

SUITE:Allow ssh and console access as root
	[Arguments]	${HOSTDIFF}
	CLI:Open	HOST_DIFF=${HOST_DIFF}
	CLI:Enable Ssh And Console Access As Root
	CLI:Close Connection