*** Settings ***
Documentation	Test the field of the cloud settings form
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../init.robot
Force Tags   PART-1	GUI	${BROWSER}	CLOUD	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE	EXCLUDEIN3_2	EXCLUDEIN4_2
Default Tags	EXCLUDEIN3_2

#Test Template	Test 2_enable and 3_disable field

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Access Cloud::Settings tab
	GUI::Cloud::Open Settings Tab

Test Enable Cloud
	Wait Until Element Is Visible	jquery=#enabled
    Select Checkbox	                    id=enabled
    Checkbox Should Be Selected	        id=enabled
    GUI::Basic::Save
    Wait Until Page Contains Element 	jquery=#globalerr   1
    Wait Until Page Contains Element    jquery=#errormsg       1
    Wait Until Element Contains	jquery=#globalerr   	Error on page. Please check.
    Wait Until Element Contains   jquery=#errormsg      Please set this node's type as coordinator or peer
    Page Should Contain Radio Button    nodeType
    Unselect Checkbox                   id=enabled
    Checkbox Should Not Be Selected     id=enabled
    Element Should Not Be Visible       jquery=#master

Test Invalid Poll Rate for Coordinator
	GUI::Cloud::Open Settings Tab
	Wait Until Element Is Visible	    jquery=#enabled
    Select Checkbox	                    id=enabled
    Checkbox Should Be Selected	        id=enabled
    GUI::Basic::Save
    Wait Until Page Contains Element 	jquery=#globalerr   1
    Wait Until Page Contains Element    jquery=#errormsg       1
    Wait Until Element Contains	jquery=#globalerr   	Error on page. Please check.
    Wait Until Element Contains   jquery=#errormsg      Please set this node's type as coordinator or peer
    Select Radio Button                 nodeType    master
    Input Text                          jquery=#preSharedKey    p.key
    Input Text                          jquery=#pollRate    $30
    GUI::Basic::Save
    Wait Until Page Contains Element 	jquery=#globalerr   1
    Wait Until Page Contains Element    jquery=#errormsg       1
    Wait Until Element Contains	jquery=#globalerr   	Error on page. Please check.
    Wait Until Element Contains   jquery=#errormsg      Must be a number between 10 and 600.

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid

SUITE:Teardown
	GUI::Basic::Logout
	Close all Browsers

Test enable and disable field
	[Arguments]	${FIELD}	${VERSION}='3.2'	${SCREENSHOT}=FALSE
	[Documentation]         Test if the given checkbox is being saved
	...     == REQUIREMENTS ==
	...     None
	...     == ARGUMENTS ==
	...     - FIELD =   Checkbox to test
	...     - VERSION  =   Nodegrid minimum version which has the given checkbox
	Run Keyword If	'${NGVERSION}'<${VERSION}	Skip	Field available in version ${VERSION} or later
	Element Should Be Visible	id=${FIELD}
	${selected}=	Run Keyword And Return Status	Checkbox Should Be Selected	id=${FIELD}
	Run Keyword If	${selected}	Unselect Checkbox	id=${FIELD}
	...	ELSE	Select Checkbox	id=${FIELD}
	GUI::Basic::Save
	GUI::Cloud::Open Peers Tab
	GUI::Cloud::Open Settings Tab
	Run Keyword If	${selected}	Checkbox Should Not Be Selected	id=${FIELD}
	...	ELSE	Checkbox Should Be Selected	id=${FIELD}
	${selected}=	Run Keyword And Return Status	Checkbox Should Be Selected	id=${FIELD}
	Run Keyword If	${selected}	Unselect Checkbox	id=${FIELD}
	...	ELSE	Select Checkbox	id=${FIELD}
	GUI::Basic::Save
	GUI::Cloud::Open Peers Tab
	GUI::Cloud::Open Settings Tab
	Run Keyword If	${selected}	Checkbox Should Not Be Selected	id=${FIELD}
	...	ELSE	Checkbox Should Be Selected	id=${FIELD}

#*** Test Cases ***                      FIELD                   VERSION
#Enable and 3_disable peer management	management_enabled
#Enable and 3_disable license pool	lps_enabled	'4.0'