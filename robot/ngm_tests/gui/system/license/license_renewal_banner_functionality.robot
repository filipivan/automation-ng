*** Settings ***
Documentation	Testing For License Renewal Banner(use licence which expires within 30 days for this tests).
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX		NON-CRITICAL
Default Tags	EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Test Setup	SUITE:Test setup and teardown
Test Teardown	SUITE:Test setup and teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${LICENSE}	${ONE_HUNDRED_DEVICES_ACCESS_LICENSE}
${USER}	jane
${PASSWD}	jane123
${LICENSE_WARNING}	Warning: A license key is expiring.

*** Test Cases ***
Test For Checking Banner By Re-Login As admin User
#	Checked mnually and run on local environment the issue was not occured
	SUITE:Add License
	Wait Until Keyword Succeeds	12x	1s	Run Keywords	GUI::Basic::Re-Login	AND	GUI::Basic::System::License::open tab	AND
	...	Wait Until Page Contains	${LICENSE_WARNING}

Test For Checking Banner By Re-Login Using Other User Except admin/root As User
	GUI::System::License::Add License	${LICENSE}
	GUI::Security::Local Accounts::Delete	${USER}
	GUI::Basic::Security::Local Accounts::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=uName	${USER}
	Input Text	id=uPasswd	${PASSWD}
	Input Text	id=cPasswd	${PASSWD}
	GUI::Basic::Save
	GUI::Basic::Logout And Close Nodegrid
	GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login	${NGVERSION}	${USER}	${PASSWD}
	Wait Until Keyword Succeeds	12x	1s	Run Keywords	GUI::Basic::Re-Login	AND	GUI::Basic::System::License::open tab	AND
	...	Wait Until Page Does Not Contain	${LICENSE_WARNING}

Test For Removing License And Re-Login To Check Banner
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add License
	Click Element	//*[@id="refresh-icon"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::License::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete All Rows In Table If Exists	\#license_table	False
	GUI::Basic::Check Rows In Table	table	0
	Click Element	//*[@id="refresh-icon"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="refresh-icon"]
	Wait Until Keyword Succeeds	12x	1s	Run Keywords	GUI::Basic::Re-Login	AND	GUI::Basic::System::License::open tab	AND
	...	Wait Until Page Does Not Contain	${LICENSE_WARNING}

*** Keywords ***
SUITE:Setup
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid

SUITE:Test setup and teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::License::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete All Rows In Table If Exists	\#license_table	False
	GUI::Basic::Check Rows In Table	table	0

SUITE:Teardown
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Local Accounts::Delete	${USER}
	GUI::Basic::System::License::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete All Rows In Table If Exists	\#license_table	False
	GUI::Basic::Check Rows In Table	table	0
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

SUITE:Add License
	GUI::System::License::Add License	${LICENSE}
	Click Element	//*[@id="refresh-icon"]
	GUI::Basic::Spinner Should Be Invisible