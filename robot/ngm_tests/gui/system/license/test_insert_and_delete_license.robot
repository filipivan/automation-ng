*** Settings ***
Documentation	Editing System > License Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

*** Variables ***
${LICENSE50}	${FIFTY_DEVICES_ACCESS_LICENSE}
#${LICENSE100}	${ONE_HUNDRED_DEVICES_ACCESS_LICENSE}	#Expired
${LICENSE1000}	${ONE_THOUSAND_DEVICES_ACCESS_LICENSE}
${EXPLICENSE}	${FIFTY_DEVICES_ACCESS_LICENSE_EXPIRED}
${INVLICENSE}	Invalid-License
${TEXT}
${NUMBER_OF_LICENSES}
${TOTAL_LICS}

*** Test Cases ***
Test Add Valid Licenses
	[Tags]	NON-CRITICAL	NEED-REVIEW
	${SYSTEM}=	GUI4.x::Basic::Get System
	Run Keyword If	'${SYSTEM}'=='Nodegrid Manager'	SUITE:vm
		...	ELSE	SUITE: Unit
	GUI::System::License::Add License	${LICENSE1000}
	GUI::Basic::Spinner Should Be Invisible
	GUI::System::License::Add License	${LICENSE50}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Check Rows In Table	license_table	2
	Run Keyword If  '${NGVERSION}'=='5.4'	Page Should Contain	Access: ( Licensed | Used | Leased | Available ): ${TOTAL_LICS}
	Run Keyword If  '${NGVERSION}'<'5.4'	Page Should Contain	Access: ( Licensed | Used | Available ): ${TOTAL_LICS}

Test Add Duplicate License Error
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::System::License::Add License	${LICENSE1000}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	License Already Installed
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible

Test Cancel Button and Add Invalid License Error
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::License::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Cancel Button Should Be Visible
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible
	GUI::System::License::Add License	${INVLICENSE}
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Not a Valid License Key
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible

Test Add Expired License
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::License::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::System::License::Add License	${EXPLICENSE}
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Invalid Expiration or Clock Turned Back Detected

Test Delete License One by One
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::License::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Check Rows In Table	license_table	3
	Suite delete License	${LICENSE1000}
	GUI::Basic::Check Rows In Table	license_table	2
	Suite delete License	${LICENSE50}
	GUI::Basic::Check Rows In Table	license_table	1
	Suite delete License	${EXPLICENSE}
	GUI::Basic::Check Rows In Table	license_table	0

Test Alert for Unsaved Configuration
	GUI::System::License::Add License	${INVLICENSE}
	Click Element	jquery=#main_menu > li:nth-child(3) > a
	Handle Alert	ACCEPT

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::License::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete All Rows In Table If Exists	\#license_table	False
	GUI::Basic::Check Rows In Table	table	0

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

Suite delete License
	[Arguments]	${LICENSE}
	${key}=	Fetch From Right	${LICENSE}	-
	${Licensekey}=	Catenate	SEPARATOR=	xxxxx-xxxxx-xxxxx-	${key}
	GUI::Basic::Select Rows In Table Containing Value	license_table	${Licensekey}
	GUI::Basic::Delete

SUITE:vm
	Sleep	5
	${NUMBER_OF_LICENSES}=	Set Variable	0
	${TOTAL_LICS}=	Evaluate	${NUMBER_OF_LICENSES} + 1050
	[Return]	${TOTAL_LICS}

SUITE: Unit
	Sleep	20
	${TEXT}=	Get Text	//*[@id="nonAccessControls"]/div/div[1]/span
	${TEXT}=	Remove String	${TEXT}	Access: ( Licensed | Used | Available ):
	${TEXT}=	Fetch From Left	${TEXT}	|
	${NUMBER_OF_LICENSES}=	Evaluate	${TEXT} - 5
	${TOTAL_LICS}=	Evaluate	${NUMBER_OF_LICENSES} + 1050
	[Return]	${TOTAL_LICS}
