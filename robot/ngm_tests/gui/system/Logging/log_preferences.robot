*** Settings ***
Documentation	Editing System > Logging preferences testing for NG4.1
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}

*** Test Cases ***
Checkbox Value Check
    Element Should Be Visible       jquery=#sessionlogenable
    ${SELECTED}=            Run Keyword And Return Status   Checkbox Should Be Selected     jquery=#sessionlogenable
    #Log     SELECTED value is ${SELECTED}
    run keyword if          ${SELECTED}==${FALSE}           Click Element                   jquery=#sessionlogenable
    Element Should Be Visible    jquery=#alertenable
    ${SELECTED_TWO}=        Run Keyword And Return Status   Checkbox Should Not Be Selected     jquery=#alertenable
    #Log     SELECTED_TWO value is ${SELECTED_TWO}
    run keyword if          ${SELECTED_TWO}==${FALSE}       Click Element                   jquery=#alertenable
    Checkbox Should Not Be Selected     jquery=#alertenable
    Click Button            jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible

Test Alert Box Button Enable
    GUI::Basic::Wait Until Element Is Accessible  jquery=#alertenable
    Select Checkbox                 jquery=#alertenable
    Element Should Be Visible      //*[@id="sessionform"]/div[2]/div/div/div/div[2]/div/div/div/div[2]/div/div[2]
    Click Button                    jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible

Test Alert Box Button Disable
    GUI::Basic::Wait Until Element Is Accessible  jquery=#alertenable
    Click Element                       jquery=#alertenable
    Element Should Not Be Visible       //*[@id="sessionform"]/div[2]/div/div/div/div[2]/div/div/div/div[2]/div/div[2]
    Click Button                        jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Logging::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
