*** Settings ***
Documentation	Editing System > Testing dialup services permanence
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Testing dropdown options
	@{EXP_TYPES}=	Create list	Disable	Enable	Callback
	Wait Until Element Is Visible	jquery=#dialupSetLogin
	sleep  2
	@{SES_TYPES}=	get list items	jquery=#dialupSetLogin
	@{PPP_TYPES}=	get list items	jquery=#dialupSetPPP
	lists should be equal	${SES_TYPES}	${PPP_TYPES}

Testing login settings
	#Test Disable
	${DISABLE}=	SUITE:Test Sections	dialupSetLogin	1
	should be equal	${DISABLE}	true
	#Test Enable
	${ENABLE}=	SUITE:Test Sections	dialupSetLogin	2
	should be equal	${DISABLE}	true
	#Test Callback
	${CALLBACK}=	SUITE:Test Sections	dialupSetLogin	3
	should be equal	${CALLBACK}	true

Testing PPP settings
	#Test Disable PPP
	${DISABLE}=	SUITE:Test Sections	dialupSetPPP	1
	should be equal	${DISABLE}	true
	#Test Enable PPP
	${ENABLE}=	SUITE:Test Sections	dialupSetPPP	2
	should be equal	${DISABLE}	true
	#Test Callback PP
	${CALLBACK}=	SUITE:Test Sections	dialupSetPPP	3
	should be equal	${CALLBACK}	true

Resetting PPP Settings
	#Set default login and check
	${DISABLEL}=	SUITE:Test Sections	dialupSetLogin	1
	should be equal	${DISABLEL}	true
	#Set default ppp and check
	${DISABLEP}=	SUITE:Test Sections	dialupSetPPP	1
	should be equal	${DISABLEP}	true
	#leave

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Dial Up::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click element	jquery=#dialupSettings

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout
	Close all Browsers

SUITE:Test Sections
	[Arguments]	${TABLE}	${ID}
	Execute Javascript	window.scrollTo(0,document.body.scrollHeight);
	Wait Until Element Is Visible	xpath=//select[@id="${TABLE}"]
	Wait Until Element Is Visible	xpath=//*[@id="${TABLE}"]/option[${ID}]
	Click element	xpath=//*[@id="${TABLE}"]/option[${ID}]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	xpath=//select[@id="${TABLE}"]
	Wait Until Element Is Visible	xpath=//*[@id="${TABLE}"]/option[${ID}]
	${SELECTED}=	wait until keyword succeeds	3x	200ms	Get Element Attribute	xpath=//*[@id="${TABLE}"]/option[${ID}]	selected
	[Return]	${SELECTED}