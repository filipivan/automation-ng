*** Settings ***
Documentation	Editing System > Testing dialup services permanence
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Add callback user
	GUI::System::Add Dummy Dialup Callback User	bob	9452839183
	${ret}=	GUI::Basic::Count Table Rows	dialupCallbackTable
	should be true	${ret}>=1

Edit callback user
	Click element	xpath=//a[@id='bob']
	Wait until element is visible	jquery=#saveButton
	Clear element text	xpath=//input[@id='dialupCbNumber']
	Input Text	xpath=//input[@id='dialupCbNumber']	1234567890
	Click element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	Wait until element is visible	jquery=#addButton
	${CHECK}=	Get Text	//*[@id="bob"]/td[3]
	should be equal	'1234567890'	'${CHECK}'

Test Invalid numbers
	Click Element	xpath=//a[@id='bob']
	Wait Until Element Is Visible	jquery=#saveButton
	Input Text	xpath=//input[@id='dialupCbNumber']	${EMPTY}
	Input Text	xpath=//input[@id='dialupCbNumber']	hellaillegal
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	xpath=//span[@id='errormsg']
	Input Text	xpath=//input[@id='dialupCbNumber']	${EMPTY}
	Input Text	xpath=//input[@id='dialupCbNumber']	1,2,3,4,5,
	Click element	jquery=#saveButton

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Dial Up::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click element	jquery=#dialupCallback

SUITE:Teardown
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Dial Up::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click element	jquery=#dialupCallback
	Execute Javascript	window.scrollTo(0,document.body.scrollHeight);
	Wait Until Element Is Visible	jquery=#addButton
	Click Element	xpath=//*[@id="thead"]/tr/th[1]/input
	Click Element	jquery=#delButton
	${ret}=	GUI::Basic::Count Table Rows	dialupCallbackTable
	should be equal	'${ret}'	'0'
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
