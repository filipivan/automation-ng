*** Settings ***
Documentation	Editing System > Scheduler Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${Valid TaskName}	TaskName
${Invalid TaskName1}	Task Name
${Invalid TaskName2}	0TaskName
${Valid Command}	Command to Execute
${Invalid Command}	Com
${Invalid User}	User

*** Test Cases ***
Test Task Name Using Two Names
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="taskName"]	${Invalid TaskName1}
	Input Text	//*[@id="command"]	${Valid Command}
	GUI::Basic::Save
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	This field contains invalid characters.

Test Task Name Using String Starting With Int
	Input Text	//*[@id="taskName"]	${Invalid TaskName2}
	GUI::Basic::Save
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	This field contains invalid characters.

Test Task Name Using Valid Name
	Input Text	//*[@id="taskName"]	${Valid TaskName}
	GUI::Basic::Save
	GUI::Basic::Page Should Not Contain Error
	GUI::Basic::Check Rows In Table	taskTable	1
	GUI::Basic::Delete All Rows In Table If Exists	\#taskTable	False

Test Command to Execute Using Short String
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="taskName"]	${Valid TaskName}
	Input Text	//*[@id="command"]	${Invalid Command}
	GUI::Basic::Save
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Command length must be between 4 and 1024

Test Command to Execute Using Valid String
	Input Text	//*[@id="command"]	${Valid Command}
	GUI::Basic::Save
	GUI::Basic::Page Should Not Contain Error
	GUI::Basic::Check Rows In Table	taskTable	1
	GUI::Basic::Delete All Rows In Table If Exists	\#taskTable	False

Test User Using Invaild User Name
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="taskName"]	${Valid TaskName}
	Input Text	//*[@id="command"]	${Valid Command}
	Input Text	//*[@id="user"]	${Invalid User}
	GUI::Basic::Save
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	User does not exist or is locked in local accounts
	GUI::Basic::Cancel
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Scheduler::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete All Rows In Table If Exists	\#taskTable	False

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
