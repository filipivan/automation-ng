*** Settings ***
Documentation	Test for Add Enable Disable and Clone Capabilities to the Scheduler In Order Facilities its Use
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE      
Default Tags	EXCLUDEIN3_2        EXCLUDEIN4_2

*** Variables ***
${Task_Name}        automation_task
${Status}       Enabled
${Test_Description}     testing
${Command}      echo >> tmp/test.log
${Clone_Name}       Testing

*** Test Cases ***
Test Case to Add Task
    SUITE:Add Task

Test Case to Check the task was Succeed
	Wait Until Keyword Succeeds  9x  20s   SUITE:Check the Task was added successfully

Test Case to Disable task
    GUI::Basic::System::Scheduler::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Select Checkbox     //*[@id="automation_task"]/td[1]/input
    Click Element       //*[@id="disable"]
    GUI::Basic::Spinner Should Be Invisible
    page should contain element     xpath=//td[contains(.,'Disabled')]
    Sleep       60s

    GUI::Tracking::Open Scheduler Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element       //*[@id="refresh-icon"]
	GUI::Basic::Spinner Should Be Invisible
    
	page should not contain element      xpath=//td[contains(.,'automation_task')]

Test Case to Clone the Task
    GUI::Basic::System::Scheduler::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element       //*[@id="automation_task"]/td[1]/input
    Click Element       //*[@id="clone_task"]
    GUI::Basic::Spinner Should Be Invisible

    input text      //*[@id="clone_name"]       ${Clone_Name}
    GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

	Click Element       xpath=//a[contains(text(),'Testing')]
	GUI::Basic::Spinner Should Be Invisible
	Sleep       3s

	page should contain textfield       //*[@id="taskName"]     ${Clone_Name}
	page should contain textfield       //*[@id="description"]     ${Test_Description}
	page should contain textfield       //*[@id="command"]     ${Command}

Test Case to Enable the Task
    GUI::Basic::System::Scheduler::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element       //*[@id="thead"]/tr/th[1]/input
    Click Element       //*[@id="enable"]
    GUI::Basic::Spinner Should Be Invisible
    page should contain element     xpath=//td[contains(.,'Enabled')]

Test Case to Check Clone the task was Succeed
	Wait Until Keyword Succeeds  9x  20s   SUITE:Check Added and Cloned Task was Suceeded

Test Case to delete task
    SUITE:Delete the task

Test Case to Verify Add & delete Task
    SUITE:Add Task
    SUITE:Delete the task

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Scheduler::Open Tab
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

SUITE:Add Task
    GUI::Basic::Add
    GUI::Basic::Spinner Should Be Invisible
    Input Text      //*[@id="taskName"]     ${Task_Name}
    select from list by label       //*[@id="status"]       ${Status}
    Input Text      //*[@id="description"]      ${Test_Description}
    Input Text      //*[@id="command"]      ${Command}
    GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Check the Task was added successfully
    GUI::Tracking::Open Scheduler Tab
	GUI::Basic::Spinner Should Be Invisible
    Page Should Contain Element      xpath=//td[contains(.,'automation_task')]
    Sleep       3s
    Click Element       //*[@id="reset_task_log"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:Check Added and Cloned Task was Suceeded
    GUI::Tracking::Open Scheduler Tab
	GUI::Basic::Spinner Should Be Invisible
    Page Should Contain Element      xpath=//td[contains(.,'automation_task')]
    Page Should Contain Element      xpath=//td[contains(.,'Testing')]
    Sleep       3s
    Click Element       //*[@id="reset_task_log"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:Delete the task
    GUI::Basic::System::Scheduler::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element       //*[@id="thead"]/tr/th[1]/input
    GUI::Basic::Delete
    GUI::Basic::Spinner Should Be Invisible

