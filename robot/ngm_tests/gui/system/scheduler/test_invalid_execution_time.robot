*** Settings ***
Documentation	Editing System > Scheduler Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${TaskName}	TaskName
${Command}	Command to Execute
${Invalid Number}	invalid#
${Invalid Min}	60
${Invalid Hour}	24
${Invalid DOM}	32
${Invalid Month}	13
${Invalid DOW}	8
${Zero}	0
${Valid Value}	5

*** Test Cases ***
Test Minute Using String Value
	#Test using non number value
	Suite Setup Valid Task
	Input Text	//*[@id="minute"]	${Invalid Number}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Invalid entry. This field may start with number or '*' and it may contain only numbers [0-59], ',', '-', '/'.

Test Minute Using Invalid Value
	Input Text	//*[@id="minute"]	${Invalid Min}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Invalid entry. This field may start with number or '*' and it may contain only numbers [0-59], ',', '-', '/'.

Test Minute Using Valid Value
	Input Text	//*[@id="minute"]	${Valid Value}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Page Should Not Contain Error
	GUI::Basic::Check Rows In Table	taskTable	1
	GUI::Basic::Delete All Rows In Table If Exists	\#taskTable	False

Test Hour Using Sting Value
	#Test using non number value
	Suite Setup Valid Task
	Input Text	//*[@id="hour"]	${Invalid Number}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Invalid entry. This field may start with number or '*' and it may contain only numbers [0-23], ',', '-', '/'.

Test Hour Using Invalid Value
	Input Text	//*[@id="hour"]	${Invalid Hour}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Invalid entry. This field may start with number or '*' and it may contain only numbers [0-23], ',', '-', '/'.

Test Hour Using Valid Value
	Input Text	//*[@id="hour"]	${Valid Value}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Page Should Not Contain Error
	GUI::Basic::Check Rows In Table	taskTable	1
	GUI::Basic::Delete All Rows In Table If Exists	\#taskTable	False

Test Day Of Month Using String Value
	#Test using non number value
	Suite Setup Valid Task
	Input Text	//*[@id="dom"]	${Invalid Number}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Invalid entry. This field may start with number or '*' and it may contain only numbers [1-31], ',', '-', '/'.

Test DOM Using Invalid value
	Input Text	//*[@id="dom"]	${Invalid DOM}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Invalid entry. This field may start with number or '*' and it may contain only numbers [1-31], ',', '-', '/'.

Test DOM Using Zero Value
	Input Text	//*[@id="dom"]	${Zero}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Invalid entry. This field may start with number or '*' and it may contain only numbers [1-31], ',', '-', '/'.

Test DOM Using Valid Value
	Input Text	//*[@id="dom"]	${Valid Value}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Page Should Not Contain Error
	GUI::Basic::Check Rows In Table	taskTable	1
	GUI::Basic::Delete All Rows In Table If Exists	\#taskTable	False

Test Month Using String Value
	#Test using non number value
	Suite Setup Valid Task
	Input Text	//*[@id="month"]	${Invalid Number}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Invalid entry. This field may start with number or '*'. It may contain only numbers, ',', '-', '/'.(Jan=1, Feb=2, ..., Dec=12)

Test Month Using Invalid Value
	Input Text	//*[@id="month"]	${Invalid Month}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Invalid entry. This field may start with number or '*'. It may contain only numbers, ',', '-', '/'.(Jan=1, Feb=2, ..., Dec=12)

Test Month Using Zero Value
	Input Text	//*[@id="month"]	${Zero}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Invalid entry. This field may start with number or '*'. It may contain only numbers, ',', '-', '/'.(Jan=1, Feb=2, ..., Dec=12)

Test Month Using Valid Value
	Input Text	//*[@id="month"]	${Valid Value}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Page Should Not Contain Error
	GUI::Basic::Check Rows In Table	taskTable	1
	GUI::Basic::Delete All Rows In Table If Exists	\#taskTable	False


Test Date Of Week Using String Value
	#Test using non number value
	Suite Setup Valid Task
	Input Text	//*[@id="dow"]	${Invalid Number}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Invalid entry. This field may start with number or '*'. It may contain only numbers, ',', '-', '/'.(Sun=0, Mon=1, ..., Sat=6)

Test DOW Using Invalid Value
	Input Text	//*[@id="dow"]	${Invalid DOW}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Invalid entry. This field may start with number or '*'. It may contain only numbers, ',', '-', '/'.(Sun=0, Mon=1, ..., Sat=6)

Test DOW Using Valid Value
	Input Text	//*[@id="dow"]	${Valid Value}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Page Should Not Contain Error
	GUI::Basic::Check Rows In Table	taskTable	1
	GUI::Basic::Delete All Rows In Table If Exists	\#taskTable	False

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	Sleep	10s
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Scheduler::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete All Rows In Table If Exists	\#taskTable	False

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

Suite Setup Valid Task
	#	GUI::Open System tab
	GUI::Basic::System::Scheduler::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="taskName"]	${TaskName}
	Input Text	//*[@id="command"]	${Command}
