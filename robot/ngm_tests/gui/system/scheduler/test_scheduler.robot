*** Settings ***
Documentation	Editing System > Scheduler Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test Scheduler tab
	Element Should Be Visible	jquery=#addButton
	Element Should Be Disabled	jquery=#delButton
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="taskName"]	taskName
	Input Text	//*[@id="command"]	command
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete All Rows In Table    \#taskTable     False

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Scheduler::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete All Rows In Table If Exists	\#taskTable	False

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
