*** Settings ***
Documentation	Editing System > Custom Fields Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Test insert custom field and check the insertion
    GUI::Open System tab
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Custom Fields::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::System::Custom Fields::Add Custom Field	test test	val val
	GUI::System::Custom Fields::Add::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::System::Custom Fields::Add Custom Field	test2 test2	val val
	GUI::System::Custom Fields::Add::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::System::Custom Fields::Add Custom Field	test3 test3	val val
	GUI::System::Custom Fields::Add::Save
	GUI::Basic::Spinner Should Be Invisible

	GUI::Basic::Check Rows In Table	CustomFieldsForm	3

Test editing one field
	GUI::Basic::Edit Rows In Table	\#CustomFieldsForm	test test
	Input Text	//*[@id="custom_value"]	custom_value
	GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Check Rows In Table	CustomFieldsForm	3

Test multi edit custom fields
 	# select all fields
 	Select Checkbox	            jquery=#CustomFieldsForm >thead input[type=checkbox]
	GUI::Basic::Click Element	//*[@id='editButton']
 	GUI::Basic::Spinner Should Be Invisible

 	Wait Until Element Is Visible	jquery=#custom_value
	GUI::Basic::Input Text	        //*[@id='custom_value']	    foo foo
 	GUI::System::Custom Fields::Add::Save
 	GUI::Basic::Spinner Should Be Invisible

	Wait Until Element Is Visible	jquery=#CustomFieldsForm
	GUI::Basic::Table Should Not Contain	table	val val

Test Delete Custom Fields
    GUI::Basic::Delete All Rows In Table If Exists	\#CustomFieldsForm	False
    GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Custom Fields::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete All Rows In Table If Exists	\#CustomFieldsForm	False
	GUI::Basic::Check Rows In Table	table	0

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

