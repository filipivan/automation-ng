*** Settings ***
Documentation	File system check
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3  	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS		CHROME
Default Tags	EXCLUDEIN4_2   	EXCLUDEIN5_0	EXCLUDEIN5_1	EXCLUDEIN5_2    EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	   ${DEFAULT_USERNAME}
${PASSWORD}    ${DEFAULT_PASSWORD}
${FSCK_O/P_MSG}    Checking filesystem /dev/sda3
${FSCK_O/P_after_file_check}    /dev/sda3 has been mounted 10 times without being checked, check forced.

*** Test Cases ***
Test case to perform file system check
    Click Element    //*[@id="icon_fileSystemCheck"]
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain    Output of Last File System Check
    Text Area Should Contain    xpath=//textarea[@id='fscklog']    ${FSCK_O/P_MSG}
    Click Button    //*[@id="checkFileSystem"]
    Sleep    10s
    Handle Alert
    Sleep    240s
    SUITE:Setup
    Click Element    //*[@id="icon_fileSystemCheck"]
    GUI::Basic::Spinner Should Be Invisible
    Text Area Should Contain    xpath=//textarea[@id='fscklog']    ${FSCK_O/P_after_file_check}

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::System::Toolkit::open tab
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Logout And Close Nodegrid