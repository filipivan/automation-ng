*** Settings ***
Documentation	System > Toolkit > Node File Manager Test Suite for admin and regular user (dummy)
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DUMMY_USER}	dummy
${DUMMY_PASSWD}	dummy

*** Test Cases ***
Test Node File Manager Button is present
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Toolkit::open tab
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	icon_File_Manager

Test Node File Manager Button is available
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Toolkit::open tab
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	icon_File_Manager

Test Node File Manager Button Admin User
	SUITE:User Login And Open System Toolkit Admin User
	sleep	2
	Element Should Be Visible	icon_File_Manager
	GUI::Basic::Logout
	[Teardown]	Close Browser

Test Node File Manager Button Dummy User
	[Setup]
	SUITE:Rollback Security:Authorization::user::Profile:System Permission
	SUITE:Setup Create User dummy
	SUITE:Set Security:Authorization::user::Profile:System Permission
	sleep	2
	SUITE:User Login And Open System Toolkit Regular User	${DUMMY_USER}	${DUMMY_PASSWD}
	Sleep	2
	Element Should Not Be Visible	icon_File_Manager
	[Teardown]	Close Browser

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Setup Create User dummy
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Local Accounts::Add	${DUMMY_USER}
	GUI::Basic::Spinner Should Be Invisible

SUITE:User Login And Open System Toolkit Admin User
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Toolkit::open tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:User Login And Open System Toolkit Regular User
	[Arguments]	${USERNAME}	${PASSWORD}
	GUI::Basic::Open And Login Nodegrid	${USERNAME}	${PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Open System tab Regular User
	GUI::Basic::Spinner Should Be Invisible
	SUITE:System::Open Toolkit tab Regular User

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Local Accounts::Delete	${DUMMY_USER}
	GUI::Basic::Spinner Should Be Invisible
	Close all Browsers

SUITE:System::Open Toolkit tab Regular User
	[Arguments]	${SCREENSHOT}=${FALSE}
	[Documentation]	Opens the NodeGrid Manager System::Toolkit Tab, it expects the System tab to be open
	...     == REQUIREMENTS ==
	...     System tab is open
	...     == ARGUMENTS ==
	...     -   SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Toolkit Tab is open and the File Manager Button must not be show it.
	[Tags]      GUI     SYSTEM      LICENSE
	Wait Until Element Is Visible	jquery=body > div.main_menu > div > ul > li:nth-child(1)
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(1)
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Not Visible	//*[@id="icon_File_Manager"]
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	${SCREENSHOT} != ${FALSE}	Capture Page Screenshot	filename=	${SCREENSHOTDIR}	${TEST NAME}	{index:06}.png

SUITE:Open System tab Regular User
	[Arguments]	${SCREENSHOT}=${FALSE}
	[Documentation]	Opens the NodeGrid Manager System tab
	...	== REQUIREMENTS ==
	...	None
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	System Tab is open and all elements are accessable
	[Tags]	GUI	SYSTEM	LICENSE
	Run Keyword If	'${NGVERSION}'=='3.2nth-child('
	...	Run Keywords
	...	Wait Until Element Is Visible	//*[@id="main.system"]/div/img
	...	AND	Click Element	//*[@id="menu_1"]/li[3]
	...	AND	Wait Until Element Is Visible	//*[@id="main.system.SystemPreference"]/span[2]
	...	ELSE IF	'${NGVERSION}'>='4.0'
	...	Run Keywords
	...	GUI::Basic::Spinner Should Be Invisible
	...	AND	Click Element	jquery=#main_menu > li:nth-child(2) > a
	...	AND	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	${SCREENSHOT} != ${FALSE}	Capture Page Screenshot	filename=	${SCREENSHOTDIR}	${TEST NAME}	{index:06}.png

SUITE:Set Security:Authorization::user::Profile:System Permission
	[Arguments]	${ROLLBACK}='False'	${PERMISSION}='upgrade'
	[Documentation]	This Keyword, open the user Profile and set the Permission to ${PERMISSION} Argument
	...	The user to run this Keyword, must be admin
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

	GUI::Security::Open Authorization tab
	
	Click Element	xpath=//a[contains(text(),'user')]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="profile"]/span
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	jquery=#permissionList
	GUI::Basic::Spinner Should Be Invisible
	Click Element  //div[@id="permissionList"]//option[@value="upgrade"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="permissionList"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Spinner Should Be Invisible
	Click element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

SUITE:Rollback Security:Authorization::user::Profile:System Permission
	[Arguments]	${ROLLBACK}='False'	${PERMISSION}='upgrade'
	[Documentation]	This Keyword, open the user Profile and set the Permission to ${PERMISSION} Argument
	...	The user to run this Keyword, must be admin
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

	GUI::Security::Open Authorization tab

	Click Element	xpath=//a[contains(text(),'user')]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="profile"]/span
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	jquery=#permissionList
	GUI::Basic::Spinner Should Be Invisible
	Click Element  //div[@id="permissionList"]//option[@value="upgrade"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="permissionList"]/div/div[1]/div[2]/div/div[1]/button
	GUI::Basic::Spinner Should Be Invisible
	Click element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
