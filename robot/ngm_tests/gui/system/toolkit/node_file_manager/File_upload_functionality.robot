*** Settings ***
Documentation	Automated test for File upload
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	admin
${PASSWORD}	admin
${Delay}	5s
${File_Path}	\\home\\qausers\\Documents\\test_file_manager\\Examplecode.py.tx
${FILE_NAME}	Examplecode.py.txt
#C:\\Users\\QAusers\\Documents\\Examplecode.py.txt	####Local path

*** Test Cases ***
Test case to upload the file
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//a[contains(text(),'File Manager')]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	1m	5s	Switch Window  title=File Manager
	select frame	//iframe
	Sleep	${Delay}
	Click Element	css=.sortable:nth-child(1) > td > .ng-binding
	Click Button	css=.btn:nth-child(7)
	Choose File	css=[type='file']	${File_Path}
	Click Element	css=#uploadFileModal .btn-primary
	Sleep	${Delay}
	Page Should Contain	${FILE_NAME}

Test case to delete file
	GUI::Basic::Spinner Should Be Invisible
	Unselect Frame
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=(//a[contains(text(),'Toolkit')])[2]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//a[contains(text(),'File Manager')]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	1m	5s	Switch Window  title=File Manager
	select frame	//iframe
	Sleep	${Delay}
	Click Element	css=.sortable:nth-child(1) > td > .ng-binding
	Sleep	${Delay}
	Select Checkbox	xpath=//input[@value='${FILE_NAME}']
	Click Element	css=.btn-default:nth-child(2)
	Sleep	${Delay}
	Page Should Not Contain	${FILE_NAME}
	Unselect Frame
	Switch Window	MAIN
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=(//a[contains(text(),'Toolkit')])[2]
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Close All Browsers
