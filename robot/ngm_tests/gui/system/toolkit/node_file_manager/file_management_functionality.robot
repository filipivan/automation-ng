*** Settings ***
Documentation	Testing ----> Support to file management functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW
Default Tags	EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${FILE_NAME}	Examplecode.py
${NEW_FOLDER_NAME}	NEW_FOLDER
${ACCESS_URL}	${HOMEPAGE}/datastore/${FILE_NAME}
${RENAME_FILE_NAME}	Rename.txt
${UPLOAD_FILE_PATH_FROM_LOCAL_SYSTEM}	\\home\\qausers\\Documents\\Examplecode.py
#C:\\Users\\QAusers\\Documents\\Examplecode.py.txt		####Local path

*** Test Cases ***
Test Case To Perform File Upload and File Delete
	${MAIN_WINDOW_TITLE}=	Get Title
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=(//a[contains(text(),'Toolkit')])[2]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	xpath=//a[contains(text(),'File Manager')]
	Click Element	xpath=//a[contains(text(),'File Manager')]
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//a[contains(text(),'File Manager')]
	Wait Until Keyword Succeeds	1m	5s	Switch Window	title=File Manager
	Select Frame	//iframe
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	xpath=//a[contains(text(),'admin_home')]
	Click Element	xpath=//a[contains(text(),'admin_home')]
	Sleep	2s
	Click Element	xpath=//button[contains(.,' Upload')]
	Sleep	5s
	Choose File	xpath=//input[@type='file']	${UPLOAD_FILE_PATH_FROM_LOCAL_SYSTEM}
	Click Element	css=#uploadFileModal .btn-primary
	Wait Until Page Contains Element	//*[@id="successAlert"]
	Sleep	5s
	Wait Until Page Contains	${FILE_NAME}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//input[@value='Examplecode.py.txt']
	Click Element	xpath=//button[contains(.,' Delete')]
	Wait Until Page Contains Element	//*[@id="successAlert"]
	Unselect Frame
	Switch Window	${MAIN_WINDOW_TITLE}
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Teardown

Test Case To Create New Folder
	Suite:Setup
	${MAIN_WINDOW_TITLE}=	Get Title
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=(//a[contains(text(),'Toolkit')])[2]
	Wait Until Element Is Visible	xpath=//a[contains(text(),'File Manager')]
	Click Element	xpath=//a[contains(text(),'File Manager')]
	Switch Window	${MAIN_WINDOW_TITLE}
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//a[contains(text(),'File Manager')]
	Wait Until Keyword Succeeds	1m	5s	Switch Window	title=File Manager
	Select Frame	//iframe
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	xpath=//a[contains(text(),'admin_home')]
	Click Element	xpath=//a[contains(text(),'admin_home')]
	Click Button	css=.btn:nth-child(6)
	Sleep	10s
	Wait Until Element Is Visible	xpath=//div[@id='createFolderModal']/div/div/div[2]/input
	Input Text	xpath=//div[@id='createFolderModal']/div/div/div[2]/input	${NEW_FOLDER_NAME}
	Click Element	//*[@id="createFolderModal"]/div/div/div[3]/button[2]
	Wait Until Page Contains Element	//*[@id="successAlert"]
	Wait Until Page Contains	${NEW_FOLDER_NAME}
	Wait Until Element Is Visible	xpath=//input[@value='${NEW_FOLDER_NAME}']
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Checkbox	xpath=//input[@value='${NEW_FOLDER_NAME}']
	Select Checkbox	xpath=//input[@value='${NEW_FOLDER_NAME}']
	Click Element	xpath=//button[contains(.,' Delete')]
	Wait Until Page Contains Element	//*[@id="successAlert"]
	Unselect Frame
	Switch Window	${MAIN_WINDOW_TITLE}
	SUITE:Teardown

Test Case For Moving File To New Folder
	Suite:Setup
	${MAIN_WINDOW_TITLE}=	Get Title
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=(//a[contains(text(),'Toolkit')])[2]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	xpath=//a[contains(text(),'File Manager')]
	Click Element	xpath=//a[contains(text(),'File Manager')]
	Switch Window	${MAIN_WINDOW_TITLE}
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//a[contains(text(),'File Manager')]
	Wait Until Keyword Succeeds	1m	5s	Switch Window	title=File Manager
	Select Frame	//iframe
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	xpath=//a[contains(text(),'admin_home')]
	Click Element	xpath=//a[contains(text(),'admin_home')]
	Sleep	5s
	Click Button	css=.btn:nth-child(6)
	Sleep	10s
	Wait Until Element Is Visible	xpath=//div[@id='createFolderModal']/div/div/div[2]/input
	Input Text	xpath=//div[@id='createFolderModal']/div/div/div[2]/input	${NEW_FOLDER_NAME}
	Click Element	//*[@id="createFolderModal"]/div/div/div[3]/button[2]
	Wait Until Page Contains Element	//*[@id="successAlert"]
	Wait Until Page Contains	${NEW_FOLDER_NAME}
	Wait Until Element Is Visible	xpath=//input[@value='${NEW_FOLDER_NAME}']
	GUI::Basic::Spinner Should Be Invisible
	Click Button	css=.btn:nth-child(7)
	Choose File	css=[type='file']	${UPLOAD_FILE_PATH_FROM_LOCAL_SYSTEM}
	Click Element	css=#uploadFileModal .btn-primary
	Wait Until Page Contains Element	//*[@id="successAlert"]
	Sleep	5s
	Wait Until Page Contains	${FILE_NAME}
	Click Element	xpath=//input[@value='Examplecode.py.txt']
	Click Element	css=.btn:nth-child(3)
	Wait Until Element Is Visible	xpath=//div[@id='moveModal']/div/div/div[2]/input
	Input Text	xpath=//div[@id='moveModal']/div/div/div[2]/input	${NEW_FOLDER_NAME}
	Click Element	css=#moveModal .btn-primary
	Wait Until Page Contains Element	//*[@id="successAlert"]
	Sleep	2s
	Wait Until Page Does Not Contain		xpath=//input[@value='${FILE_NAME}']
	Wait Until Page Contains	${NEW_FOLDER_NAME}
	Wait Until Element Is Visible	xpath=//input[@value='${NEW_FOLDER_NAME}']
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Checkbox	xpath=//input[@value='${NEW_FOLDER_NAME}']
	Select Checkbox	xpath=//input[@value='${NEW_FOLDER_NAME}']
	Click Element	xpath=//button[contains(.,' Delete')]
	Wait Until Page Contains Element	//*[@id="successAlert"]
	Unselect Frame
	Switch Window	${MAIN_WINDOW_TITLE}
	SUITE:Teardown

Test Case To Perform File Rename,File Download,File Archive
	Suite:Setup
	${MAIN_WINDOW_TITLE}=	Get Title
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=(//a[contains(text(),'Toolkit')])[2]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	xpath=//a[contains(text(),'File Manager')]
	Click Element	xpath=//a[contains(text(),'File Manager')]
	Switch Window	${MAIN_WINDOW_TITLE}
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//a[contains(text(),'File Manager')]
	Wait Until Keyword Succeeds	1m	5s	Switch Window	title=File Manager
	Select Frame	//iframe
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	xpath=//a[contains(text(),'admin_home')]
	Click Element	xpath=//a[contains(text(),'admin_home')]
	Sleep	5s
	Click Button	css=.btn:nth-child(7)
	Choose File	css=[type='file']	${UPLOAD_FILE_PATH_FROM_LOCAL_SYSTEM}
	Sleep	10s
	Click Element	css=#uploadFileModal .btn-primary
	Sleep	3s
	Wait Until Page Contains Element	//*[@id="successAlert"]
	Wait Until Page Contains	${FILE_NAME}
	Click Element	xpath=//input[@value='${FILE_NAME}']
	Click Element	xpath=//button[contains(.,' Rename')]
	Wait Until Element Is Visible	xpath=//div[@id='renameModal']/div/div/div[2]/input
	Input Text	xpath=//div[@id='renameModal']/div/div/div[2]/input	${RENAME_FILE_NAME}
	Click Element	css=#renameModal .btn-primary
	Wait Until Page Contains Element	//*[@id="successAlert"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	xpath=//input[@value='${RENAME_FILE_NAME}']
	Page Should Contain Checkbox	xpath=//input[@value='${RENAME_FILE_NAME}']
	Select Checkbox	xpath=//input[@value='${RENAME_FILE_NAME}']
	Wait Until Element Is Visible	xpath=//button[contains(.,' Download')]
	Click Element	xpath=//button[contains(.,' Download')]
	Sleep	5s
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Checkbox	xpath=//input[@value='${RENAME_FILE_NAME}']
	Select Checkbox	xpath=//input[@value='${RENAME_FILE_NAME}']
	Wait Until Element Is Visible	xpath=//button[contains(.,' Archive')]
	Click Element	xpath=//button[contains(.,' Archive')]
	Click Element	css=#archiveModal .btn-primary
	Sleep	5s
	wait until page contains	Create Archive Succeed!
	Sleep	5s
	Page Should Contain Checkbox	xpath=//input[@value='${RENAME_FILE_NAME}']
	Select Checkbox	xpath=//input[@value='${RENAME_FILE_NAME}']
	Click Element	xpath=//button[contains(.,' Delete')]
	Sleep	2s
	Wait Until Page Contains Element	//*[@id="successAlert"]
	Unselect Frame
	Switch Window	${MAIN_WINDOW_TITLE}

Test Case to upload file in datastore folder
	[Tags]	NON-CRITICAL	NEED-REVIEW	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	Suite:Setup
	${MAIN_WINDOW_TITLE}=	Get Title
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=(//a[contains(text(),'Toolkit')])[2]
	Wait Until Element Is Visible	xpath=//a[contains(text(),'File Manager')]
	Click Element	xpath=//a[contains(text(),'File Manager')]
	Switch Window	${MAIN_WINDOW_TITLE}
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//a[contains(text(),'File Manager')]
	Wait Until Keyword Succeeds	1m	5s	Switch Window	title=File Manager
	Select Frame	//iframe
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	xpath=//a[contains(text(),'datastore')]
	Click Element	xpath=//a[contains(text(),'datastore')]
	Wait Until Element Is Visible	xpath=//button[7]
	Click Button	css=.btn:nth-child(7)
	Sleep	10s
	Choose File	xpath=//input[@type='file']	${UPLOAD_FILE_PATH_FROM_LOCAL_SYSTEM}
	Click Element	css=#uploadFileModal .btn-primary
	Wait Until Page Contains Element	//*[@id="successAlert"]
	Unselect Frame
	Switch Window	${MAIN_WINDOW_TITLE}

Test Case to access the file via syntax <NG_IP>/datastore/<FILE_NAME>
	[Tags]	NON-CRITICAL	NEED-REVIEW	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	${BROWSER}	Convert To Lower Case	${BROWSER}
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nOpening Browser: ${BROWSER}	INFO	console=yes
	Run Keyword If	'${BROWSER}' == 'chrome' or '${BROWSER}' == 'headlesschrome'	Wait Until Keyword Succeeds	3x	3s	Open Browser	${ACCESS_URL}	${BROWSER}	options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors")
	Run Keyword If	'${BROWSER}' == 'firefox' or '${BROWSER}' == 'headlessfirefox'	Wait Until Keyword Succeeds	3x	3s	Open Browser	${ACCESS_URL}	${BROWSER}	options=set_preference("moz:dom.disable_beforeunload", "true")
	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\nBrowser ${BROWSER} opened	INFO	console=yes
	Maximize Browser Window
	Page Should Contain	${FILE_NAME}
	SUITE:Delete the file uploaded to datastore folder

*** Keywords ***
Suite:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Enable HTTP/S File Repository

SUITE:Teardown
	SUITE:Disable HTTP/S File Repository
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:Delete the file uploaded to datastore folder
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	${MAIN_WINDOW_TITLE}=	Get Title
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=(//a[contains(text(),'Toolkit')])[2]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	xpath=//a[contains(text(),'File Manager')]
	Click Element	xpath=//a[contains(text(),'File Manager')]
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//a[contains(text(),'File Manager')]
	Wait Until Keyword Succeeds	1m	5s	Switch Window	title=File Manager
	Select Frame	//iframe
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	xpath=//a[contains(text(),'datastore')]
	Click Element	xpath=//a[contains(text(),'datastore')]
	Wait Until Element Is Visible	xpath=//input[@value='${FILE_NAME}']
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Checkbox	xpath=//input[@value='${FILE_NAME}']
	Select Checkbox	xpath=//input[@value='${FILE_NAME}']
	Click Element	xpath=//button[contains(.,' Delete')]
	Wait Until Page Contains Element	//*[@id="successAlert"]
	Unselect Frame
	Switch Window	${MAIN_WINDOW_TITLE}

SUITE:Enable HTTP/S File Repository
	GUI::Basic::Security::Services::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//input[@id='http_repo_enable']
	Select Checkbox	xpath=//input[@id='http_repo_enable']
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Disable HTTP/S File Repository
	GUI::Basic::Security::Services::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//input[@id='http_repo_enable']
	Unselect Checkbox	xpath=//input[@id='http_repo_enable']
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

