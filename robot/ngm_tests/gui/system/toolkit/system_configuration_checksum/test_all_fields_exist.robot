*** Settings ***
Documentation	Editing System > Toolkit > System Configuration Checksum Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test Checksum nonAccess Buttons and MD5View
	Open Checksum
	Element Should Be Disabled	jquery=#saveButton
	Element Should Be Enabled	jquery=#cancelButton
	Select Radio Button	systemCfgChkselection	md5check
	Select Radio Button	systemCfgChkActionMD5	md5view
	GUI::Basic::Cancel

Test MD5SUM Baseline
	Test Checksum	md5	baseline

Test MD5SUM Verify
	Test Checksum	md5	verify

Test SHA256SUM View
	Test Checksum	sha256	view

Test SHA256SUM Baseline
	Test Checksum	sha256	baseline

Test SHA256SUM Verify
	Test Checksum	sha256	verify

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Toolkit::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

Open Checksum
	Wait Until Element Is Visible	jquery=#icon_systemCfgChk
	Click Element	jquery=#icon_systemCfgChk
	GUI::Basic::Wait Until Element Is Accessible	jquery=#systemCfgChkselection

Test Checksum
	[Arguments]	${Check_type}	${Action}
	Open Checksum
	Select Radio Button	systemCfgChkselection	${Check_type}check
	${MD5}=	Run Keyword And Return Status	Should Be Equal	${Check_type}	md5
	${SHA}=	Run Keyword And Return Status	Should Be Equal	${Check_type}	sha256
	${View}=	Run Keyword And Return Status	Should Be Equal	${Action}	view
	${Base}=	Run Keyword And Return Status	Should Be Equal	${Action}	baseline
	${Verify}=	Run Keyword And Return Status	Should Be Equal	${Action}	verify
	Run Keyword If	${MD5}	Select Radio Button	systemCfgChkActionMD5	md5${Action}
	Run Keyword If	${SHA}	Select Radio Button	systemCfgChkActionSHA	sha${Action}
	GUI::Basic::Save
	Page Should Contain	Checksum Type:
	Run Keyword If	${View}	Page Should Contain	Checksum Result:
	Run Keyword If	${View}	Click Element	jquery=#finish
	Run Keyword If	${Base}	Page Should Contain	Checksum Result:
	Run Keyword If	${Base}	Page Should Contain	Baseline Reference:
	Run Keyword If	${Base}	Click Element	jquery=#finish
	Run Keyword If	${Verify}	Page Should Contain	Baseline Reference:
	Run Keyword If	${Verify}	Page Should Contain	Current Result:
	Run Keyword If	${Verify}	Page Should Contain	Baseline Result:
	Run Keyword If	${Verify}	Page Should Contain	Checksum Status:
	Run Keyword If	${Verify}	Click Element	jquery=#finish
