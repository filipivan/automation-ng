*** Settings ***
Documentation	Validation test cases after NG come back to out of band profile
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	EXCLUDEIN4_2	EXCLUDEIN5_0
	...	EXCLUDEIN5_2	NON-CRITICAL

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${BRIDGE_NET}	192.168.10.0
${BRIDGE_BITMASK}	24
${BACKPLANE_ROUTE_METRIC}	100
${ETH0_ROUTE_METRIC}	90
${SFP_ETH1_SFP_ROUTE_METRIC}	100
${WIFI_IP_ADDRESS}	192.168.162.1
${WIFI_BITMASK}	24
${BRIDGE_INTERFACE}	lanbrd
${DHCP_SUBNET_NETMASK}	192.168.10.0/255.255.255.0

*** Test Cases ***
Test case to check in Security::Services if system profile is out of band
	GUI::Basic::Security::Services::Open Tab
	Page Should Contain	System Profile:
	Textfield Should Contain	//input[@id='sysprof']	Out Of Band

Test case to check network connections in out of band profile
	GUI::Basic::Network::Connections::Open Tab
	${CONNECTIONS_TABLE_CONTENTS}	Get Text	peerTable
	Log	\nTable contents in Network::Connections: \n${CONNECTIONS_TABLE_CONTENTS}	INFO	console=yes
	Should Not Contain	${CONNECTIONS_TABLE_CONTENTS}	BRIDGE
	Should Contain	${CONNECTIONS_TABLE_CONTENTS}	ETH0
	Should Contain	${CONNECTIONS_TABLE_CONTENTS}	hotspot
	Should Not Contain	${CONNECTIONS_TABLE_CONTENTS}	CELLULAR
	IF	${IS_NSR}
		Should Contain	${CONNECTIONS_TABLE_CONTENTS}	BACKPLANE1
		Should Contain	${CONNECTIONS_TABLE_CONTENTS}	BACKPLANE0
		Should Contain	${CONNECTIONS_TABLE_CONTENTS}	ETH1	#IT was removed from BRIDGE connection to avoit that the other devices in the same network get an IP from its DHCP server
	ELSE
		Should Contain	${CONNECTIONS_TABLE_CONTENTS}	SFP0
		Should Contain	${CONNECTIONS_TABLE_CONTENTS}	SFP1
	END

Test case to check the default configuration of each network connection in out of band profile
	GUI::Basic::Network::Connections::Open Tab
	${CONNECTIONS_TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections: \n${CONNECTIONS_TABLE_CONTENTS}	INFO	console=yes
	@{GET_LINES}	Split To Lines	${CONNECTIONS_TABLE_CONTENTS}	1
	FOR	${LINE}	IN	@{GET_LINES}
		${CONNECTION}	@{MATCHS}	Should Match Regexp	${LINE}	(\\w+\\-+\\w|\\w)*(?=\\s)
		Log	\nConnection Name: \n${CONNECTION}	INFO	console=yes
		GUI::Basic::Click Element	//a[@id='${CONNECTION}']
		IF	'${CONNECTION}' == 'BACKPLANE0' or '${CONNECTION}' == 'BACKPLANE1'
			Checkbox Should not Be Selected	//input[@id='primaryConn']	#Set as Primary Connection
			SUITE:Radio Button Should Be Set To DHCP and Auto IPv6
			Checkbox Should not Be Selected	//input[@id='inputFilter']	#Block Unsolicited Incoming Packets
			Checkbox Should not Be Selected	//input[@id='lldp']
			Textfield Should Contain	//input[@id='ipv4routeMetric']	${BACKPLANE_ROUTE_METRIC}
			Textfield Should Contain	//input[@id='ipv6routeMetric']	${BACKPLANE_ROUTE_METRIC}
			IF	'${CONNECTION}' == 'BACKPLANE1' and not ${IS_NSR}
				Checkbox Should Not Be Selected	//input[@id='connAuto']
			ELSE
				Checkbox Should Be Selected	//input[@id='connAuto']
			END
			SUITE:Checkboxes about ignore obtained IP or DNS Should not Be Selected
		ELSE IF	'${CONNECTION}' == 'ETH0' or '${CONNECTION}' == 'ETH1' or '${CONNECTION}' == 'SFP0' or '${CONNECTION}' == 'SFP1'
			SUITE:Radio Button Should Be Set To DHCP and Auto IPv6
			Checkbox Should not Be Selected	//input[@id='lldp']
			IF	'${CONNECTION}' == 'ETH0'
				Checkbox Should Be Selected	//input[@id='primaryConn']	#Set as Primary Connection
				Checkbox Should Not Be Selected	//input[@id='inputFilter']	#Block Unsolicited Incoming Packets
				Textfield Should Contain	//input[@id='ipv4routeMetric']	${ETH0_ROUTE_METRIC}
				Textfield Should Contain	//input[@id='ipv6routeMetric']	${ETH0_ROUTE_METRIC}
			ELSE
				IF	'${CONNECTION}' == 'ETH1'
					Checkbox Should Not Be Selected	//input[@id='inputFilter']	#Block Unsolicited Incoming Packets
				ELSE
					Checkbox Should Not Be Selected	//input[@id='inputFilter']	#Block Unsolicited Incoming Packets
				END
				Checkbox Should Not Be Selected	//input[@id='primaryConn']	#Set as Primary Connection
				Textfield Should Contain	//input[@id='ipv4routeMetric']	${SFP_ETH1_SFP_ROUTE_METRIC}
				Textfield Should Contain	//input[@id='ipv6routeMetric']	${SFP_ETH1_SFP_ROUTE_METRIC}
			END
			IF	'${CONNECTION}' == 'SFP1'
				Checkbox Should Not Be Selected	//input[@id='connAuto']
			ELSE
				Checkbox Should Be Selected	//input[@id='connAuto']
			END
			SUITE:Checkboxes about ignore obtained IP or DNS Should not Be Selected
		ELSE IF	'${CONNECTION}' == 'hotspot'
			Checkbox Should Not Be Selected	//input[@id='inputFilter']	#Block Unsolicited Incoming Packets
			Radio Button Should Be Set To	methodWifi	shared	#IPv4 Mode:Server (shared interface to others)
			Textfield Should Contain	//input[@id='addressWifi']	${WIFI_IP_ADDRESS}
			Textfield Should Contain	//input[@id='netmaskWifi']	${WIFI_BITMASK}
			Radio Button Should Be Set To	method6Wifi	disabled	#IPv6 Mode:No IPv6 Address
			Radio Button Should Be Set To	wifiSec	wpa-psk	#WiFi Security:WPA2 Personal
			Radio Button Should Be Set To	wifiBand	2_4ghz	#WiFi Band:2.4 GHz
		END
		SUITE:Test Teardown
	END
	[Teardown]	SUITE:Test Teardown

Test case to check network settings in out of band profile
	GUI::Basic::Network::Settings::Open Tab
	Checkbox Should Not Be Selected	//input[@id='ipv4-ipforward']
	Checkbox Should Not Be Selected	//input[@id='ipv6-forward']
	Checkbox Should Be Selected	//input[@id='multiroute']
	Textfield Should Contain	//input[@id='dnsProxy']	${EMPTY}
	${RP_FILTER_LABEL}	Get Selected List Label	//select[@id='rpfilter']
	Should Be Equal	${RP_FILTER_LABEL}	Strict Mode
	IF	not ${IS_NSR}
		Textfield Should Contain	//input[@id='dnsAddress']	8.8.8.8
		Checkbox Should Not Be Selected	//input[@id='failover']
	ELSE
		Textfield Should Contain	//input[@id='dnsAddress']	192.168.2.205 75.75.75.75 75.75.76.76
		Checkbox Should Not Be Selected	//input[@id='failover']
	END
	Execute Javascript	window.document.getElementById("bluetoothEnable").scrollIntoView(true);
	Checkbox Should Be Selected	//input[@id='bluetoothEnable']
	Checkbox Should Not Be Selected	//input[@id='failover_second']

Test case to check ipv4 firewall chains and rules are the default ones after come back to out of band profile
	GUI::Basic::Security::Firewall::Open Tab
	GUI::Basic::Click Element	//tr[@id='INPUT:IPv4']//a[@id='INPUT']
	Page Should Not Contain	//tr[@id='1']//input[@type='checkbox']
	GUI::Basic::Return
	GUI::Basic::Click Element	//tr[@id='FORWARD:IPv4']//a[@id='FORWARD']
	Page Should Not Contain	//td[@class='selectable']//input[@type='checkbox']
	GUI::Basic::Return
	Page Should Not Contain	//a[@id='NG-GW-IN-BRIDGE']
	Page Should Not Contain	//a[@id='NG-GW-FW-BRIDGE']
	[Teardown]	SUITE:Test Teardown

Test case to check ipv6 firewall rules are the default ones after come back to out of band profile
	GUI::Basic::Security::Firewall::Open Tab
	GUI::Basic::Click Element	//tr[@id='INPUT:IPv6']//a[@id='INPUT']
	Page Should Not Contain	//tr[@id='1']//input[@type='checkbox']
	GUI::Basic::Return
	GUI::Basic::Click Element	//tr[@id='FORWARD:IPv6']//a[@id='FORWARD']
	Page Should Not Contain	//tr[@id='0']//input[@type='checkbox']
	Page Should Not Contain	//tr[@id='1']//input[@type='checkbox']
	GUI::Basic::Return
	GUI::Basic::Click Element	//tr[@id='OUTPUT:IPv6']//a[@id='OUTPUT']
	Page Should Not Contain	//tr[@id='1']//input[@type='checkbox']
	GUI::Basic::Return
	[Teardown]	SUITE:Test Teardown

Test case to check ipv4 NAT rule is on default settings after come back to out of band profile
	GUI::Basic::Security::NAT::Open Tab
	GUI::Basic::Click Element	//tr[@id='POSTROUTING:IPv4']//a[@id='POSTROUTING']
	Page Should Not Contain	//td[@class='selectable']//input[@type='checkbox']
	GUI::Basic::Return
	[Teardown]	SUITE:Test Teardown

Test case to check DHCP server configuration in out of band profile
	GUI::Basic::Network::Settings::open tab
	Run Keyword If	'${NGVERSION}' <= '5.4'	GUI::Basic::Network::DHCP Server::Open Tab
	Run Keyword If	'${NGVERSION}' >= '5.6'	Run Keywords	GUI::Basic::Network::DHCP::Open Tab	AND	Click Element	//span[normalize-space()='DHCP Server']
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="hostTable"]
	Page Should Not Contain	//a[@id='${DHCP_SUBNET_NETMASK}']

Test case to check elements of overview on the out of band profile
	GUI::Access::Open
	GUI::Basic::Click Element	(//a[contains(.,'Overview')])[2]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Connections
	SUITE:Access:Overview: Check elements with system profile is Out Of Band

*** Keywords ***
SUITE:Setup
	Run Keyword If	not ${GLOBAL_HAS_CONSOLE_ACCESS}	Skip	The Nodegrid console access is not working on "1_out_of_band_to_gateway_functionality.robot" script.
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	${IS_VM}	Set Variable	${FALSE}
	${IS_SR}	Set Variable	${FALSE}
	${IS_NSR}	Set Variable	${FALSE}
	${IS_VM}	GUI::Basic::Is VM System
	${IS_SR}	GUI::Basic::Is SR System
	${IS_NSR}	GUI::Basic::Is Net SR
	Set Suite Variable	${IS_VM}
	Set Suite Variable	${IS_SR}
	Set Suite Variable	${IS_NSR}
	Skip If	not ${IS_SR} or ${IS_VM}	Host is not SR System, probably is a VM (${IS_VM}). It has no System Profile feature.

SUITE:Teardown
	Run Keyword If	not ${GLOBAL_HAS_CONSOLE_ACCESS}	Skip	The Nodegrid console access is not working on "1_out_of_band_to_gateway_functionality.robot" script.
	Run Keyword If	not ${IS_SR} or ${IS_VM}	Close All Browsers
	Skip If	not ${IS_SR} or ${IS_VM}	Host is not SR System, probably is a VM (${IS_VM}). It has no System Profile feature.
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	Close All Browsers

SUITE:Test Teardown
	GUI::Basic::Spinner Should Be Invisible
	${HAS_CANCEL_BUTTON}	Run Keyword And Return Status	Page Should Contain Button	//input[@id='cancelButton']
	Run Keyword If	${HAS_CANCEL_BUTTON}	Run Keyword And Ignore Error	GUI::Basic::Button::Cancel
	GUI::Basic::Spinner Should Be Invisible
	${HAS_RETURN_BUTTON}	Run Keyword And Return Status	Page Should Contain Button	//input[@id='returnButton']
	Run Keyword If	${HAS_RETURN_BUTTON}	Run Keyword And Ignore Error	GUI::Basic::Return

SUITE:Checkboxes about ignore obtained IP or DNS Should not Be Selected
	Checkbox Should not Be Selected	//input[@id='ipv4neverDefault']	#Ignore obtained IPv4 Default Gateway
	Checkbox Should not Be Selected	//input[@id='ipv4ignoreAutoDns']	#Ignore obtained DNS server
	Checkbox Should not Be Selected	//input[@id='ipv6neverDefault']	#Ignore obtained IPv6 Default Gateway
	Checkbox Should not Be Selected	//input[@id='ipv6ignoreAutoDns']	#Ignore obtained DNS server

SUITE:Radio Button Should Be Set To DHCP and Auto IPv6
	Radio Button Should Be Set To	method	auto	#IPv4 Mode:DHCP
	Radio Button Should Be Set To	method6	auto	#IPv6 Mode:Address Auto Configuration

SUITE:Access:Overview: Check elements with system profile is Out Of Band
	GUI::Basic::Click Element	//span[@class='columns_btn_text'][contains(.,'Widgets')]

	GUI::Basic::Select Checkbox	//input[@id='title_overallStatus_cb']
	Element Should Be Visible	//div[@id='title_overallStatus']
	GUI::Basic::Unselect Checkbox	//input[@id='title_overallStatus_cb']
	Element Should Not Be Visible	//div[@id='title_overallStatus']

	GUI::Basic::Select Checkbox	//input[@id='title_dhcpClients_cb']
	Element Should Be Visible	//div[@id='title_dhcpClients']
	GUI::Basic::Unselect Checkbox	//input[@id='title_dhcpClients_cb']
	Element Should Not Be Visible	//div[@id='title_dhcpClients']

	GUI::Basic::Select Checkbox	//input[@id='title_btConns_cb']
	Element Should Be Visible	//div[@id='title_btConns']
	GUI::Basic::Unselect Checkbox	//input[@id='title_btConns_cb']
	Element Should Not Be Visible	//div[@id='title_btConns']

	GUI::Basic::Select Checkbox	//input[@id='title_vms_cb']
	Element Should Be Visible	//div[@id='title_vms']
	GUI::Basic::Unselect Checkbox	//input[@id='title_vms_cb']
	Element Should Not Be Visible	//div[@id='title_vms']

	GUI::Basic::Select Checkbox	//input[@id='title_netConn_cb']
	Element Should Be Visible	//div[@id='title_netConn']
	GUI::Basic::Unselect Checkbox	//input[@id='title_netConn_cb']
	Element Should Not Be Visible	//div[@id='title_netConn']

	GUI::Basic::Select Checkbox	//input[@id='title_genSysInfo_cb']
	Element Should Be Visible	//div[contains(@id,'genSysInfo')]
	GUI::Basic::Unselect Checkbox	//input[@id='title_genSysInfo_cb']
	Element Should Not Be Visible	//div[contains(@id,'genSysInfo')]

	GUI::Basic::Select Checkbox	//input[@id='title_genNetInfo_cb']
	Element Should Be Visible	//div[contains(@id,'genNetInfo')]
	GUI::Basic::Unselect Checkbox	//input[@id='title_genNetInfo_cb']
	Element Should Not Be Visible	//div[contains(@id,'genNetInfo')]

	GUI::Basic::Select Checkbox	//input[@id='title_vpnConn_cb']
	Element Should Be Visible	//div[@id='title_vpnConn']
	GUI::Basic::Unselect Checkbox	//input[@id='title_vpnConn_cb']
	Element Should Not Be Visible	//div[@id='title_vpnConn']

	GUI::Basic::Select Checkbox	//input[@id='title_hsClients_cb']
	Element Should Be Visible	//div[@id='title_hsClients']
	GUI::Basic::Unselect Checkbox	//input[@id='title_hsClients_cb']
	Element Should Not Be Visible	//div[@id='title_hsClients']

	GUI::Basic::Select Checkbox	//input[@id='title_routingTable_cb']
	Element Should Be Visible	//div[@id='title_routingTable']
	GUI::Basic::Unselect Checkbox	//input[@id='title_routingTable_cb']
	Element Should Not Be Visible	//div[@id='title_routingTable']

	GUI::Basic::Select Checkbox	//input[@id='title_switchConn_cb']
	Element Should Be Visible	//div[@id='title_switchConn']
	GUI::Basic::Unselect Checkbox	//input[@id='title_switchConn_cb']
	Element Should Not Be Visible	//div[@id='title_switchConn']