*** Settings ***
Documentation	Functionality test cases about gateway profile
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	EXCLUDEIN4_2	EXCLUDEIN5_0
	...	EXCLUDEIN5_2	NON-CRITICAL

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${STRONG_PASSWORD}	${QA_PASSWORD}
${ETH0_INTERFACE}	eth0
${BRIDGE_INTERFACE}	bridge
${EXTERNAL_DNS_TARGET}	google.com
${TEST_CONNECTION_BACK0}	BACKPLANE0
${TEST_INTERFACE_BACK0}	backplane0
${PART_BRIDGE_IP}	192.168.10.

*** Test Cases ***
Test case to ping from a WAN interface an external DNS in gateway profile
	GUI::System::Toolkit::Network Tools::Ping target through interface	${ETH0_INTERFACE}	${EXTERNAL_DNS_TARGET}

Test case to ping from a LAN interface an external DNS in gateway profile
	${PING_STATUS}	Run Keyword And Return Status	GUI::System::Toolkit::Network Tools::Ping target through interface	${BRIDGE_INTERFACE}	${EXTERNAL_DNS_TARGET}
	Should Be Equal	${PING_STATUS}	${FALSE}

Test case to ping the HOSTSHAREd'S BACKPLANE0 from HOST'S BACKPLANE0 in gateway profile
	Skip If	not ${HAS_HOSTSHARED} or not ${VLAN_AVAILABLE}	This test environment has no hostshared device with switch physical connections on the host's switch.
	${HOST_BACKPLANE0_IP}	${HOSTSHARED_BACKPLANE0_IP}	SUITE:Setup VLAN on host and hostshared
	GUI::System::Toolkit::Network Tools::Ping target through interface	${TEST_INTERFACE_BACK0}	${HOSTSHARED_BACKPLANE0_IP}	${HOST_BACKPLANE0_IP}
	[Teardown]	SUITE:Setup Untagged VLAN to default

*** Keywords ***
SUITE:Setup
	Run Keyword If	not ${GLOBAL_HAS_CONSOLE_ACCESS}	Skip	The Nodegrid console access is not working on "1_out_of_band_to_gateway_functionality.robot" script.
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid	PASSWORD=${STRONG_PASSWORD}
	${IS_VM}	Set Variable	${FALSE}
	${IS_SR}	Set Variable	${FALSE}
	${IS_NSR}	Set Variable	${FALSE}
	${IS_VM}	GUI::Basic::Is VM System
	${IS_SR}	GUI::Basic::Is SR System
	${IS_NSR}	GUI::Basic::Is Net SR
	Set Suite Variable	${IS_VM}
	Set Suite Variable	${IS_SR}
	Set Suite Variable	${IS_NSR}
	Skip If	not ${IS_SR} or ${IS_VM}	Host is not SR System, probably is a VM (${IS_VM}). It has no System Profile feature.

SUITE:Teardown
	Run Keyword If	not ${GLOBAL_HAS_CONSOLE_ACCESS}	Skip	The Nodegrid console access is not working on "1_out_of_band_to_gateway_functionality.robot" script.
	Run Keyword If	not ${IS_SR} or ${IS_VM}	Close All Browsers
	Skip If	not ${IS_SR} or ${IS_VM}	Host is not SR System, probably is a VM (${IS_VM}). It has no System Profile feature.
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid	PASSWORD=${STRONG_PASSWORD}
	GUI::Basic::Logout and Close Nodegrid

SUITE:Test Teardown
	GUI::Basic::Spinner Should Be Invisible
	${HAS_CANCEL_BUTTON}	Run Keyword And Return Status	Page Should Contain Button	//input[@id='cancelButton']
	Run Keyword If	${HAS_CANCEL_BUTTON}	Run Keyword And Ignore Error	GUI::Basic::Button::Cancel
	GUI::Basic::Spinner Should Be Invisible
	${HAS_RETURN_BUTTON}	Run Keyword And Return Status	Page Should Contain Button	//input[@id='returnButton']
	Run Keyword If	${HAS_RETURN_BUTTON}	Run Keyword And Ignore Error	GUI::Basic::Return

SUITE:Setup VLAN on host and hostshared
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid	PAGE=${HOMEPAGE}	PASSWORD=${STRONG_PASSWORD}	ALIAS=HOST_HOMEPAGE
	${HOST_IS_NSR}	GUI::Basic::Is Net SR
	${HOST_IS_GSR}	GUI::Basic::Is Gate SR
	Set Suite Variable	${HOST_IS_NSR}
	Set Suite Variable	${HOST_IS_GSR}
	${HOST_HAS_BACK0}	GUI::Network::Check If Has Connection	BACKPLANE0
	Set Suite Variable	${HOST_HAS_BACK0}
	GUI::Basic::Open And Login Nodegrid	PAGE=${HOMEPAGESHARED}	ALIAS=SHARED_HOMEPAGE
	${HOSTSHARED_IS_NSR}	GUI::Basic::Is Net SR
	${HOSTSHARED_IS_GSR}	GUI::Basic::Is Gate SR
	Set Suite Variable	${HOSTSHARED_IS_NSR}
	Set Suite Variable	${HOSTSHARED_IS_GSR}
	${HOSTSHARED_HAS_BACK0}	GUI::Network::Check If Has Connection	BACKPLANE0
	Set Suite Variable	${HOSTSHARED_HAS_BACK0}
	Skip If	not ${HOST_HAS_BACK0}	HOST has no BACKPLANE0 connection
	Skip If	not ${HOSTSHARED_HAS_BACK0}	HOSTSHARED has no BACKPLANE0 connection
	Run Keyword If	not ${HOST_IS_NSR} and not ${HOST_IS_GSR}
	...	Fail	HOST system is not NSR and not GSR, the switch test cases must be updated for the new system
	Run Keyword If	not ${HOSTSHARED_IS_NSR} and not ${HOSTSHARED_IS_GSR}
	...	Fail	HOSTSHARED system is not NSR and not GSR, the switch test cases must be updated for the new system
	# Switch to host browser page
	GUI::Basic::Switch Browser	ALIAS=HOST_HOMEPAGE
	# Network backplane connection to default
	GUI::Network::Setup BACKPLANE Connection to default	BACKPLANE=${TEST_CONNECTION_BACK0}
	# VLAN to default
	Run Keyword If	${HOST_IS_NSR}	GUI::Network::Setup Untagged VLAN to default with sfp0 and sfp1 on switch
	...	ELSE	GUI::Network::Setup Untagged VLAN to default without sfp0 and sfp1 on switch
	# Switch interfaces to default
	Run Keyword If	${HOST_IS_NSR}	GUI::Network::Switch::Switch Interfaces::Setup Switch Interfaces to default with sfp0 and sfp1 on switch
	...	ELSE	GUI::Network::Switch::Switch Interfaces::Setup Switch Interfaces to default without sfp0 and sfp1 on switch
	# Edit VLAN
	Run Keyword If	${HOST_IS_NSR}	GUI::Network::Switch::Add Backplane1 and sfp1 to first Untagged VLAN
	...	ELSE	GUI::Network::Switch::Add Backplane1 and sfp1 to first Untagged VLAN	HAS_SFP1=No
	# Enable switch interface
	GUI::Network::Switch::Switch Interfaces::Enable switch interface	${SWITCH_NETPORT_HOST_INTERFACE2}
	# Get host Backplane0 DHCP IPv4 and check if it's Up
	GUI::Network::Connections::Setup Backplane with DHCP method and select connect automatically	${TEST_CONNECTION_BACK0}
	${HOST_BACKPLANE0_HAS_IP}	Run Keyword And Return Status	GUI::Network::Get Connection Ipv4	${TEST_CONNECTION_BACK0}
	Run Keyword If	not ${HOST_BACKPLANE0_HAS_IP}	Fail	BACKPLANE0 has no IPv4
	${HOST_BACKPLANE0_IP}	GUI::Network::Get Connection Ipv4	${TEST_CONNECTION_BACK0}
	Should Contain	${HOST_BACKPLANE0_IP}	${PART_BRIDGE_IP}
	Wait Until Keyword Succeeds	3x	3s	GUI::Network::Connections::Check If connection Has IPv4 and Is Up
	...	${TEST_CONNECTION_BACK0}	${TEST_INTERFACE_BACK0}	${HOST_BACKPLANE0_IP}

	# Switch to hostshared browser page
	GUI::Basic::Switch Browser	ALIAS=SHARED_HOMEPAGE
	# Network backplane connection to default
	GUI::Network::Setup BACKPLANE Connection to default	BACKPLANE=${TEST_CONNECTION_BACK0}
	# VLAN to default
	Run Keyword If	${HOSTSHARED_IS_NSR}	GUI::Network::Setup Untagged VLAN to default with sfp0 and sfp1 on switch
	...	ELSE	GUI::Network::Setup Untagged VLAN to default without sfp0 and sfp1 on switch
	# Switch interfaces to default
	Run Keyword If	${HOSTSHARED_IS_NSR}	GUI::Network::Switch::Switch Interfaces::Setup Switch Interfaces to default with sfp0 and sfp1 on switch
	...	ELSE	GUI::Network::Switch::Switch Interfaces::Setup Switch Interfaces to default without sfp0 and sfp1 on switch
	# Edit VLAN
	Run Keyword If	${HOSTSHARED_IS_NSR}	GUI::Network::Switch::Add Backplane1 and sfp1 to first Untagged VLAN
	...	ELSE	GUI::Network::Switch::Add Backplane1 and sfp1 to first Untagged VLAN	HAS_SFP1=No
	# Enable switch interface
	GUI::Network::Switch::Switch Interfaces::Enable switch interface	${SWITCH_NETPORT_SHARED_INTERFACE2}
	# Get hostshared Backplane0 DHCP IPv4 and check if it's Up
	GUI::Network::Connections::Setup Backplane with DHCP method and select connect automatically	${TEST_CONNECTION_BACK0}
	${HOSTSHARED_BACKPLANE0_HAS_IP}	Run Keyword And Return Status	GUI::Network::Get Connection Ipv4	${TEST_CONNECTION_BACK0}
	Run Keyword If	not ${HOSTSHARED_BACKPLANE0_HAS_IP}	Fail	BACKPLANE0 has no IPv4
	${HOSTSHARED_BACKPLANE0_IP}	GUI::Network::Get Connection Ipv4	${TEST_CONNECTION_BACK0}
	Should Contain	${HOSTSHARED_BACKPLANE0_IP}	${PART_BRIDGE_IP}
	Wait Until Keyword Succeeds	3x	3s	GUI::Network::Connections::Check If connection Has IPv4 and Is Up
	...	${TEST_CONNECTION_BACK0}	${TEST_INTERFACE_BACK0}	${HOSTSHARED_BACKPLANE0_IP}
	GUI::Basic::Switch Browser	ALIAS=HOST_HOMEPAGE
	[Return]	${HOST_BACKPLANE0_IP}	${HOSTSHARED_BACKPLANE0_IP}

GUI::Network::Connections::Setup Backplane with DHCP method and select connect automatically
	[Arguments]	${BACKPLANE}
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Click Element	//a[@id='${BACKPLANE}']
	GUI::Basic::Click Element	//label[normalize-space()='DHCP']//input[@id='method']
	Select Checkbox	//input[@id='connAuto']
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save If Configuration Changed
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//tr[@id='${BACKPLANE}']//input[@type='checkbox']
	GUI::Basic::Click Element	//input[@value='Up Connection']

SUITE:Setup Untagged VLAN to default
	Skip If	not ${VLAN_AVAILABLE}	This automation test environment has no VLAN available
	Run Keyword If	not ${HOST_IS_NSR} and not ${HOST_IS_GSR}
	...	Fail	HOST system is not NSR and not GSR, the switch test cases must be updated for the new system
	Run Keyword If	not ${HOSTSHARED_IS_NSR} and not ${HOSTSHARED_IS_GSR}
	...	Fail	HOSTSHARED system is not NSR and not GSR, the switch test cases must be updated for the new system
	GUI::Basic::Switch Browser	ALIAS=HOST_HOMEPAGE
	Run Keyword If	${HOST_IS_NSR}	GUI::Network::Setup Untagged VLAN to default with sfp0 and sfp1 on switch
	...	ELSE	GUI::Network::Setup Untagged VLAN to default without sfp0 and sfp1 on switch
	Run Keyword If	${HOST_IS_NSR}	GUI::Network::Switch::Switch Interfaces::Setup Switch Interfaces to default with sfp0 and sfp1 on switch
	...	ELSE	GUI::Network::Switch::Switch Interfaces::Setup Switch Interfaces to default without sfp0 and sfp1 on switch
	GUI::Network::Setup BACKPLANE Connection to default	BACKPLANE=${TEST_CONNECTION_BACK0}
	GUI::Basic::Switch Browser	ALIAS=SHARED_HOMEPAGE
	Run Keyword If	${HOSTSHARED_IS_NSR}	GUI::Network::Setup Untagged VLAN to default with sfp0 and sfp1 on switch
	...	ELSE	GUI::Network::Setup Untagged VLAN to default without sfp0 and sfp1 on switch
	Run Keyword If	${HOSTSHARED_IS_NSR}	GUI::Network::Switch::Switch Interfaces::Setup Switch Interfaces to default with sfp0 and sfp1 on switch
	...	ELSE	GUI::Network::Switch::Switch Interfaces::Setup Switch Interfaces to default without sfp0 and sfp1 on switch
	GUI::Network::Setup BACKPLANE Connection to default	BACKPLANE=${TEST_CONNECTION_BACK0}
	[Teardown]	SUITE:Test Teardown