*** Settings ***
Documentation	Functionality tests for switching from gateway to out of band profile
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	EXCLUDEIN4_2	EXCLUDEIN5_0
	...	EXCLUDEIN5_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${STRONG_PASSWORD}	${QA_PASSWORD}

*** Test Cases ***
Test case for switching from gateway to out of band profile
	SUITE:Security::Services::General Services::System Profile should be gateway
	SUITE:Apply factory default settings using out of band profile
	SUITE:Wait Until Factory Settings Done
	GUI::Basic::Post Factory Settings to v5.0+
	SUITE:Security::Services::General Services::System Profile should be out of band

*** Keywords ***
SUITE:Setup
	Close All Browsers
	${HAS_STRONG_PASSWORD}	Run Keyword And Return Status	GUI::Basic::Open And Login Nodegrid	PASSWORD=${STRONG_PASSWORD}
	Set Suite Variable	${HAS_STRONG_PASSWORD}
	Skip If	not ${HAS_STRONG_PASSWORD}	Host has no strong password. The system is not on Gateway profile. Check "1_out_of_band_to_gateway_functionality.robot".
	${IS_VM}	Set Variable	${FALSE}
	${IS_SR}	Set Variable	${FALSE}
	${IS_VM}	GUI::Basic::Is VM System
	${IS_SR}	GUI::Basic::Is SR System
	Set Suite Variable	${IS_VM}
	Set Suite Variable	${IS_SR}
	Skip If	not ${IS_SR} or ${IS_VM}	Host is not SR System, probably is a VM (${IS_VM}). It has no System Profile feature.
	Skip If	not ${GLOBAL_HAS_CONSOLE_ACCESS}	The Nodegrid console access is not working on "1_out_of_band_to_gateway_functionality.robot" script.

SUITE:Teardown
	Skip If	not ${HAS_STRONG_PASSWORD}	Host has no strong password. The system is not on Gateway profile. Check "1_out_of_band_to_gateway_functionality.robot".
	Run Keyword If	not ${IS_SR} or ${IS_VM}	Close All Browsers
	Skip If	not ${IS_SR} or ${IS_VM}	Host is not SR System, probably is a VM (${IS_VM}). It has no System Profile feature.
	Skip If	not ${GLOBAL_HAS_CONSOLE_ACCESS}	The Nodegrid console access is not working on "1_out_of_band_to_gateway_functionality.robot" script.
	GUI::Basic::Check If Host Has Factory Settings
	Close All Browsers
	${LOGIN_DEFAULT_PASSWORD}	Run Keyword And Return Status	GUI::Basic::Open And Login Nodegrid
	Close All Browsers
	IF	'${NGVERSION}' >= '5.0' and not ${LOGIN_DEFAULT_PASSWORD}
		${LOGIN_STRONG_PASSWORD}	Run Keyword And Return Status	GUI::Basic::Open And Login Nodegrid	USERNAME=${DEFAULT_USERNAME}	PASSWORD=${QA_PASSWORD}
		Run Keyword If	${LOGIN_STRONG_PASSWORD}	GUI::Basic::Enable Security For SSH Root Access By GUI and Change the Passwords By CLI
	END
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Logout And Close Nodegrid

SUITE:Security::Services::General Services::System Profile should be gateway
	GUI::Basic::Security::Services::Open Tab
	Page Should Contain	System Profile:
	Textfield Should Contain	//input[@id='sysprof']	Gateway

SUITE:Security::Services::General Services::System Profile should be out of band
	GUI::Basic::Security::Services::Open Tab
	Page Should Contain	System Profile:
	Textfield Should Contain	//input[@id='sysprof']	Out Of Band

SUITE:Apply factory default settings using out of band profile
	GUI::Basic::System::Toolkit::Open Tab
	Wait Until Element Is Visible	//a[@id='icon_factoryDefault']
	GUI::Basic::Click Element	//a[@id='icon_factoryDefault']
	Wait Until Element Is Visible	//label[normalize-space()='Out Of Band']//input[@id='applyProfileType']
	GUI::Basic::Click Element	//label[normalize-space()='Out Of Band']//input[@id='applyProfileType']
	GUI::Basic::Click Element Accept Alert	//input[@value='Restore']
	Close All Browsers

SUITE:Wait Until Factory Settings Done
	Wait Until Keyword Succeeds	3x	3s	SUITE:Check If Factory Settings Done

SUITE:Check If Factory Settings Done
	Sleep	3m
	Wait Until Keyword Succeeds	40x	3s	GUI::Basic::Open NodeGrid
