*** Settings ***
Documentation	Functionality tests for switching from out-of-band to gateway profile
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	EXCLUDEIN4_2	EXCLUDEIN5_0
	...	EXCLUDEIN5_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${TEST_CONSOLE_PORT_TTYS}	${CONSOLE_PORT_TTYS}
${TEST_CONSOLE_PORT_NAME}	${CONSOLE_PORT_NAME}
${TEST_CONSOLE_ACCESS_IP}	${CONSOLE_ACCESS_IP}
${STRONG_PASSWORD}	${QA_PASSWORD}

*** Test Cases ***
Test case to switch from out-of-band to gateway profile
	SUITE:Security::Services::General Services::System Profile should be out of band
	SUITE:Apply factory default settings using gateway profile
	SUITE:Wait Until open via console to first login and unblock unsolicited incoming packets
	GUI::Basic::Open And Login Nodegrid	PASSWORD=${STRONG_PASSWORD}
	GUI::Basic::Enable Security For SSH Root Access By GUI and Change the Passwords By CLI	NEW_PASSWORD=${STRONG_PASSWORD}
	SUITE:Security::Services::General Services::System Profile should be gateway

*** Keywords ***
SUITE:Setup
	${HAS_CONSOLE_ACCESS}	Set Variable	${FALSE}
	${IS_VM}	Set Variable	${FALSE}
	${IS_SR}	Set Variable	${FALSE}
	${IS_NSR}	Set Variable	${FALSE}
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	${IS_VM}	GUI::Basic::Is VM System
	${IS_SR}	GUI::Basic::Is SR System
	${IS_NSR}	GUI::Basic::Is Net SR
	Set Suite Variable	${IS_VM}
	Set Suite Variable	${IS_SR}
	Set Suite Variable	${IS_NSR}
	Skip If	not ${IS_SR} or ${IS_VM}	Host is not SR System, probably is a VM (${IS_VM}). It has no System Profile feature.
	${HAS_CONSOLE_ACCESS}	Run Keyword And Return Status	SUITE:Check if console access is working
	CLI:Close Connection
	${GLOBAL_HAS_CONSOLE_ACCESS}	Set Variable	${HAS_CONSOLE_ACCESS}
	Set Global Variable	${GLOBAL_HAS_CONSOLE_ACCESS}
	Skip If	not ${GLOBAL_HAS_CONSOLE_ACCESS}	The Nodegrid console access is not working.

SUITE:Teardown
	CLI:Close Connection
	Close All Browsers
	Skip If	not ${IS_SR} or ${IS_VM}	Host is not SR System, probably is a VM (${IS_VM}). It has no System Profile feature.
	Skip If	not ${GLOBAL_HAS_CONSOLE_ACCESS}	The Nodegrid console access is not working.
	GUI::Basic::Open And Login Nodegrid	PASSWORD=${STRONG_PASSWORD}
	GUI::Basic::Logout and Close Nodegrid

SUITE:Check if console access is working
	Run Keyword If	'${TEST_CONSOLE_PORT_TTYS}' == '${FALSE}' or '${TEST_CONSOLE_PORT_NAME}' == '${FALSE}' or '${TEST_CONSOLE_ACCESS_IP}' == '${FALSE}'
	...	Fail	NGM has no console access by ssh.
	${STATUS}	Run Keyword And Return Status	CLI:Open	USERNAME=${DEFAULT_USERNAME}:${TEST_CONSOLE_PORT_TTYS}
	...	TYPE=nodegrid_device	HOST_DIFF=${TEST_CONSOLE_ACCESS_IP}
	Run Keyword If	not ${STATUS}	CLI:Open	USERNAME=${DEFAULT_USERNAME}:${TEST_CONSOLE_PORT_NAME}
	...	TYPE=nodegrid_device	HOST_DIFF=${TEST_CONSOLE_ACCESS_IP}
	SUITE:Wait Until Success Press Enter And Read Until	login:

SUITE:Wait Until Success Press Enter And Read Until
	[Arguments]	${PROMPT}
	Wait Until Keyword Succeeds	12x	1s	SUITE:Read Until And Press Enter	${PROMPT}

SUITE:Read Until And Press Enter
	[Arguments]	${PROMPT}
	Set Client Configuration	timeout=5s
	Write Bare	\n
	${OUTPUT}=	Read Until	${PROMPT}	loglevel=INFO
	Write Bare	\n
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Log To Console	${OUTPUT}
	[Return]	${OUTPUT}

SUITE:Security::Services::General Services::System Profile should be out of band
	GUI::Basic::Security::Services::Open Tab
	Page Should Contain	System Profile:
	Textfield Should Contain	//input[@id='sysprof']	Out Of Band

SUITE:Security::Services::General Services::System Profile should be gateway
	GUI::Basic::Security::Services::Open Tab
	Page Should Contain	System Profile:
	Textfield Should Contain	//input[@id='sysprof']	Gateway

SUITE:Apply factory default settings using gateway profile
	GUI::Basic::System::Toolkit::Open Tab
	Wait Until Element Is Visible	//a[@id='icon_factoryDefault']
	GUI::Basic::Click Element	//a[@id='icon_factoryDefault']
	Wait Until Element Is Visible	//label[normalize-space()='Gateway']//input[@id='applyProfileType']
	GUI::Basic::Click Element	//label[normalize-space()='Gateway']//input[@id='applyProfileType']
	GUI::Basic::Click Element Accept Alert	//input[@value='Restore']
	Close All Browsers

SUITE:Wait Until open via console to first login and unblock unsolicited incoming packets
	Sleep	3m
	${STATUS}	Run Keyword And Return Status	Wait Until Keyword Succeeds	40x	3s
	...	SUITE:Open via console to first login and unblock unsolicited incoming packets
	Run Keyword If	not ${STATUS}	Fatal Error	First login Fail after factory settigs to switch to gateway profile

SUITE:Open via console to first login and unblock unsolicited incoming packets
	SUITE:Check if console access is working
	Write	${DEFAULT_USERNAME}
	SUITE:Read Until	Password:
	Write	${DEFAULT_PASSWORD}
	SUITE:Read Until	Current password:
	Write	${DEFAULT_PASSWORD}
	SUITE:Read Until	New password:
	Write	${STRONG_PASSWORD}
	SUITE:Read Until	Retype new password:
	Write	${STRONG_PASSWORD}
	SUITE:Wait Until Success Press Enter And Read Until	]#
	Write	cd /settings/network_connections/ETH0/
	SUITE:Wait Until Success Press Enter And Read Until	]#
	Write	set block_unsolicited_incoming_packets=no
	SUITE:Wait Until Success Press Enter And Read Until	]#
	Write	commit
	IF	${IS_NSR}
		SUITE:Wait Until Success Press Enter And Read Until	]#
		Write	cd /settings/network_connections/BRIDGE/
		SUITE:Wait Until Success Press Enter And Read Until	]#
		Write	set bridge_interfaces=backplane1
		SUITE:Wait Until Success Press Enter And Read Until	]#
		Write	commit
	END
	SUITE:Wait Until Success Press Enter And Read Until	]#
	Write	exit
	SUITE:Wait Until Success Press Enter And Read Until	login:
	Write	\x05c.

SUITE:Read Until
	[Arguments]	${PROMPT}
	Set Client Configuration	timeout=120s
	${OUTPUT}=	Read Until	${PROMPT}	loglevel=INFO
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Log To Console	${OUTPUT}
	[Return]	${OUTPUT}