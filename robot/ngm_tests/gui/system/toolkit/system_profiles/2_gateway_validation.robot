*** Settings ***
Documentation	Validation test cases about gateway profile
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	EXCLUDEIN4_2	EXCLUDEIN5_0
	...	EXCLUDEIN5_2	NON-CRITICAL

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${STRONG_PASSWORD}	${QA_PASSWORD}
${BRIDGE_INTERF_NSR}	backplane1
${BRIDGE_INTERF_SR}	backplane0	backplane1
${BRIDGE_ROUTE_METRIC}	425
${BRIDGE_NET}	192.168.10.0
${BRIDGE_IP}	192.168.10.1
${BRIDGE_BITMASK}	24
${BACKPLANE_ROUTE_METRIC}	100
${CELLULAR_ROUTE_METRIC}	700
${ETH0_ROUTE_METRIC}	90
${SFP_ETH1_SFP_ROUTE_METRIC}	100
${WIFI_IP_ADDRESS}	192.168.162.1
${WIFI_BITMASK}	24
${IPV4_REGEXP}	((\\d{1,3}\\.){3}\\d{1,3}\\/\\d{2})
${MAC_ADDRESS_REGEXP}	([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})
${RANGE_START}	192.168.10.10
${RANGE_END}	192.168.10.200
${BRIDGE_DESCRIPTION}	bridge for lan ports - gateway profile
${CELLULAR_DESCRIPTION}	autogenerated cellular connection
${BRIDGE_INTERFACE}	lanbrd
${BRIDGE_CONNECTION}	BRIDGE
${TRIGGER_IP}	api.zpecloud.com
${DHCP_SUBNET_NETMASK}	192.168.10.0/255.255.255.0

*** Test Cases ***
Test case to check in Security::Services if system profile is Gateway
	GUI::Basic::Security::Services::Open Tab
	Page Should Contain	System Profile:
	Textfield Should Contain	//input[@id='sysprof']	Gateway

Test case to check network connections in gateway profile
	GUI::Basic::Network::Connections::Open Tab
	${CONNECTIONS_TABLE_CONTENTS}	Get Text	peerTable
	Log	\nTable contents in Network::Connections: \n${CONNECTIONS_TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${CONNECTIONS_TABLE_CONTENTS}	BRIDGE\\s+Connected+\\s+Bridge+\\s+${BRIDGE_INTERFACE}+\\s+Up+\\s+${BRIDGE_IP}\\/${BRIDGE_BITMASK}+\\s+${MAC_ADDRESS_REGEXP}+\\s+${BRIDGE_DESCRIPTION}
	Should Contain	${CONNECTIONS_TABLE_CONTENTS}	ETH0
	Should Contain	${CONNECTIONS_TABLE_CONTENTS}	hotspot
	IF	${IS_NSR}
		Should Not Contain	${CONNECTIONS_TABLE_CONTENTS}	BACKPLANE1
		Should Contain	${CONNECTIONS_TABLE_CONTENTS}	BACKPLANE0
		Should Not Contain	${CONNECTIONS_TABLE_CONTENTS}	CELLULAR
		Should Contain	${CONNECTIONS_TABLE_CONTENTS}	ETH1	#IT was removed from BRIDGE connection to avoit that the other devices in the same network get an IP from its DHCP server
	ELSE
		Should Contain	${CONNECTIONS_TABLE_CONTENTS}	SFP0
		Should Contain	${CONNECTIONS_TABLE_CONTENTS}	SFP1
		GUI::Tracking::Open Devices Tab
		GUI::Basic::Spinner Should Be Invisible
		GUI::Basic::Click Element	//span[normalize-space()='Wireless Modem']
		${TRACKING_TABLE_CONTENTS}	Get Text	wmodemTable
		Log	\nTable contents in Tracking :: Devices :: Wireless Modem \n${TRACKING_TABLE_CONTENTS}	INFO	console=yes
		@{GET_LINES}	Split To Lines	${TRACKING_TABLE_CONTENTS}	1
		@{CELLULAR_INTERFACES}	Create List
		FOR	${LINE}	IN	@{GET_LINES}
			${CELLULAR_INTERFACE}	Should Match Regexp	${LINE}	(cdc-wdm\\d{1})
			Append To List	${CELLULAR_INTERFACES}	${CELLULAR_INTERFACE}
		END
		FOR	${CELLULAR_INTERFACE}	IN	@{CELLULAR_INTERFACES}
			Should Match Regexp	${CONNECTIONS_TABLE_CONTENTS}	CELLULAR.*${CELLULAR_INTERFACE}.*${CELLULAR_DESCRIPTION}
		END
	END

Test case to check the default configuration of each network connection in gateway profile
	GUI::Basic::Network::Connections::Open Tab
	${CONNECTIONS_TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections: \n${CONNECTIONS_TABLE_CONTENTS}	INFO	console=yes
	@{GET_LINES}	Split To Lines	${CONNECTIONS_TABLE_CONTENTS}	1
	FOR	${LINE}	IN	@{GET_LINES}
		${CONNECTION}	@{MATCHS}	Should Match Regexp	${LINE}	(\\w+\\-+\\w|\\w)*(?=\\s)
		Log	\nConnection Name: \n${CONNECTION}	INFO	console=yes
		GUI::Basic::Click Element	//a[@id='${CONNECTION}']
		IF	'${CONNECTION}' == 'BACKPLANE0' or '${CONNECTION}' == 'BACKPLANE1'
			Checkbox Should not Be Selected	//input[@id='primaryConn']	#Set as Primary Connection
			SUITE:Radio Button Should Be Set To DHCP and Auto IPv6
			Checkbox Should Be Selected	//input[@id='inputFilter']	#Block Unsolicited Incoming Packets
			Checkbox Should not Be Selected	//input[@id='lldp']
			Textfield Should Contain	//input[@id='ipv4routeMetric']	${BACKPLANE_ROUTE_METRIC}
			Textfield Should Contain	//input[@id='ipv6routeMetric']	${BACKPLANE_ROUTE_METRIC}
			IF	'${CONNECTION}' == 'BACKPLANE1' and not ${IS_NSR}
				Checkbox Should Not Be Selected	//input[@id='connAuto']
			ELSE
				Checkbox Should Be Selected	//input[@id='connAuto']
			END
			SUITE:Checkboxes about ignore obtained IP or DNS Should not Be Selected
		ELSE IF	'${CONNECTION}' == 'CELLULAR-A' or '${CONNECTION}' == 'CELLULAR-B'
			Checkbox Should Be Selected	//input[@id='connAuto']
			Checkbox Should not Be Selected	//input[@id='primaryConn']	#Set as Primary Connection
			Radio Button Should Be Set To	methodMobile	auto	#IPv4 Mode:DHCP
			Radio Button Should Be Set To	method6Mobile	ignore	#IPv6 Mode:No IPv6 Address
			Textfield Should Contain	//input[@id='ipv4routeMetric']	${CELLULAR_ROUTE_METRIC}
			Checkbox Should Be Selected	//input[@id='inputFilter']	#Block Unsolicited Incoming Packets
			Checkbox Should not Be Selected	//input[@id='lldp']
			Checkbox Should not Be Selected	//input[@id='connHealth']
			Run Keyword If	'${NGVERSION}' >= '5.6'	Radio Button Should Be Set To	mobileApnConf	automatic	#SIM-1 APN Configuration:Automatic
			Checkbox Should not Be Selected	//input[@id='dataUsageMonitor1']
			Checkbox Should not Be Selected	//input[@id='ipPass']
			Checkbox Should not Be Selected	//input[@id='gnssStatus']
			Checkbox Should not Be Selected	//input[@id='mobileDualSim']
			SUITE:Checkboxes about ignore obtained IP or DNS Should not Be Selected
		ELSE IF	'${CONNECTION}' == 'BRIDGE'
			Checkbox Should Be Selected	//input[@id='connAuto']
			Checkbox Should not Be Selected	//input[@id='primaryConn']	#Set as Primary Connection
			Checkbox Should not Be Selected	//input[@id='inputFilter']	#Block Unsolicited Incoming Packets
			Checkbox Should not Be Selected	//input[@id='bridgeSTP']	#Enable Spanning Tree Protocol
			Textfield Should Contain	//input[@id='address']	${BRIDGE_IP}
			Textfield Should Contain	//input[@id='netmask']	${BRIDGE_BITMASK}
			Textfield Should Contain	//input[@id='ipv4routeMetric']	${BRIDGE_ROUTE_METRIC}
			Radio Button Should Be Set To	method6	ignore	#IPv6 Mode: No IPv6 Address
			IF	not ${IS_NSR}
				Textfield Should Contain	//input[@id='bridgeSlave']	${BRIDGE_INTERF_SR}
			ELSE
			Textfield Should Contain	//input[@id='bridgeSlave']	${BRIDGE_INTERF_NSR}
			END
			SUITE:Checkboxes about ignore obtained IP or DNS Should not Be Selected
		ELSE IF	'${CONNECTION}' == 'ETH0' or '${CONNECTION}' == 'ETH1' or '${CONNECTION}' == 'SFP0' or '${CONNECTION}' == 'SFP1'
			SUITE:Radio Button Should Be Set To DHCP and Auto IPv6
			Checkbox Should not Be Selected	//input[@id='lldp']
			IF	'${CONNECTION}' == 'ETH0'
				Checkbox Should Be Selected	//input[@id='primaryConn']	#Set as Primary Connection
				Checkbox Should Not Be Selected	//input[@id='inputFilter']	#Block Unsolicited Incoming Packets (dibled during first access by console)
				Textfield Should Contain	//input[@id='ipv4routeMetric']	${ETH0_ROUTE_METRIC}
				Textfield Should Contain	//input[@id='ipv6routeMetric']	${ETH0_ROUTE_METRIC}
			ELSE
				IF	'${CONNECTION}' == 'ETH1'
					Checkbox Should Not Be Selected	//input[@id='inputFilter']	#Block Unsolicited Incoming Packets (dibled during first access by console)
				ELSE
					Checkbox Should Be Selected	//input[@id='inputFilter']	#Block Unsolicited Incoming Packets
				END
				Checkbox Should Not Be Selected	//input[@id='primaryConn']	#Set as Primary Connection
				Textfield Should Contain	//input[@id='ipv4routeMetric']	${SFP_ETH1_SFP_ROUTE_METRIC}
				Textfield Should Contain	//input[@id='ipv6routeMetric']	${SFP_ETH1_SFP_ROUTE_METRIC}
			END
			IF	'${CONNECTION}' == 'SFP1'
				Checkbox Should Not Be Selected	//input[@id='connAuto']
			ELSE
				Checkbox Should Be Selected	//input[@id='connAuto']
			END
			SUITE:Checkboxes about ignore obtained IP or DNS Should not Be Selected
		ELSE IF	'${CONNECTION}' == 'hotspot'
			Checkbox Should Not Be Selected	//input[@id='inputFilter']	#Block Unsolicited Incoming Packets
			Radio Button Should Be Set To	methodWifi	shared	#IPv4 Mode:Server (shared interface to others)
			Textfield Should Contain	//input[@id='addressWifi']	${WIFI_IP_ADDRESS}
			Textfield Should Contain	//input[@id='netmaskWifi']	${WIFI_BITMASK}
			Radio Button Should Be Set To	method6Wifi	disabled	#IPv6 Mode:No IPv6 Address
			Radio Button Should Be Set To	wifiSec	wpa-psk	#WiFi Security:WPA2 Personal
			Radio Button Should Be Set To	wifiBand	2_4ghz	#WiFi Band:2.4 GHz
		END
		SUITE:Test Teardown
	END
	[Teardown]	SUITE:Test Teardown

Test case to check network settings in gateway profile
	GUI::Basic::Network::Settings::Open Tab
	Checkbox Should Be Selected	//input[@id='ipv4-ipforward']
	Checkbox Should Be Selected	//input[@id='ipv6-forward']
	Checkbox Should Be Selected	//input[@id='multiroute']
	Textfield Should Contain	//input[@id='dnsProxy']	${BRIDGE_IP}
	${RP_FILTER_LABEL}	Get Selected List Label	//select[@id='rpfilter']
	Should Be Equal	${RP_FILTER_LABEL}	Loose Mode
	IF	not ${IS_NSR}
		Textfield Should Contain	//input[@id='dnsAddress']	8.8.8.8
		Checkbox Should Be Selected	//input[@id='failover']
		Checkbox Should Be Selected	//input[@id='primaryAddressTrigger']
		Radio Button Should Be Set To	trigger	ipaddress
		Textfield Should Contain	//input[@id='address']	${TRIGGER_IP}
		${PRIMARY_LABEL}	Get Selected List Label	//select[@id='primaryConn']
		${SECONDARY_LABEL}	Get Selected List Label	//select[@id='secondaryConn']
		Should Be Equal	${PRIMARY_LABEL}	ETH0
		Should Be Equal	${SECONDARY_LABEL}	CELLULAR-A
	ELSE
		Textfield Should Contain	//input[@id='dnsAddress']	192.168.2.205 75.75.75.75 75.75.76.76
		Checkbox Should Not Be Selected	//input[@id='failover']
	END
	Execute Javascript	window.document.getElementById("bluetoothEnable").scrollIntoView(true);
	Checkbox Should Be Selected	//input[@id='bluetoothEnable']
	Checkbox Should Not Be Selected	//input[@id='failover_second']

Test case to check ipv4 firewall chains and rules are created automatically
	GUI::Basic::Security::Firewall::Open Tab
	GUI::Basic::Click Element	//tr[@id='INPUT:IPv4']//a[@id='INPUT']
	GUI::Basic::Click Element	//tr[@id='1']//input[@type='checkbox']
	GUI::Basic::Edit
	Textfield Should Contain	//input[@id='rule_desc']	gateway bridge input chain
	${TARGET_LABEL}	Get Selected List Label	//select[@id='target']
	${IN_IFACE_LABEL}	Get Selected List Label	//select[@id='inIface']
	Should Be Equal	${TARGET_LABEL}	NG-GW-IN-BRIDGE
	Should Be Equal	${IN_IFACE_LABEL}	${BRIDGE_INTERFACE}
	GUI::Basic::Button::Cancel
	GUI::Basic::Return
	GUI::Basic::Click Element	//tr[@id='FORWARD:IPv4']//a[@id='FORWARD']
	GUI::Basic::Click Element	//td[@class='selectable']//input[@type='checkbox']
	GUI::Basic::Edit
	Textfield Should Contain	//input[@id='rule_desc']	gateway bridge forward chain
	${TARGET_LABEL}	Get Selected List Label	//select[@id='target']
	${IN_IFACE_LABEL}	Get Selected List Label	//select[@id='inIface']
	Should Be Equal	${TARGET_LABEL}	NG-GW-FW-BRIDGE
	Should Be Equal	${IN_IFACE_LABEL}	Any
	GUI::Basic::Button::Cancel
	GUI::Basic::Return
	GUI::Basic::Click Element	//a[@id='NG-GW-IN-BRIDGE']
	${FIREWALL_IN_TABLE_CONTENTS}=	Get Text	rulesTable
	Log	\nTable contents in Security::Firewall::NG-GW-IN-BRIDGE:IPv4: \n${FIREWALL_IN_TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${FIREWALL_IN_TABLE_CONTENTS}	0+\\s+ACCEPT+\\s+udp
	Should Match Regexp	${FIREWALL_IN_TABLE_CONTENTS}	1+\\s+ACCEPT+\\s+tcp
	Should Match Regexp	${FIREWALL_IN_TABLE_CONTENTS}	2+\\s+ACCEPT+\\s+tcp
	Should Match Regexp	${FIREWALL_IN_TABLE_CONTENTS}	3+\\s+ACCEPT+\\s+udp
	GUI::Basic::Return
	GUI::Basic::Click Element	//a[@id='NG-GW-FW-BRIDGE']
	${FIREWALL_FW_TABLE_CONTENTS}=	Get Text	rulesTable
	Log	\nTable contents in Security::Firewall::NG-GW-IN-BRIDGE:IPv4: \n${FIREWALL_FW_TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${FIREWALL_FW_TABLE_CONTENTS}	0+\\s+ACCEPT+\\s+${BRIDGE_NET}\\/${BRIDGE_BITMASK}+\\s+${BRIDGE_INTERFACE}
	Should Match Regexp	${FIREWALL_FW_TABLE_CONTENTS}	1+\\s+ACCEPT+\\s+${BRIDGE_NET}\\/${BRIDGE_BITMASK}+\\s+${BRIDGE_INTERFACE}
	Should Match Regexp	${FIREWALL_FW_TABLE_CONTENTS}	2+\\s+ACCEPT+\\s+${BRIDGE_INTERFACE}+\\s+${BRIDGE_INTERFACE}
	Should Match Regexp	${FIREWALL_FW_TABLE_CONTENTS}	3+\\s+DROP+\\s+${BRIDGE_INTERFACE}
	Should Match Regexp	${FIREWALL_FW_TABLE_CONTENTS}	4+\\s+DROP+\\s+${BRIDGE_INTERFACE}
	GUI::Basic::Return
	[Teardown]	SUITE:Test Teardown

Test case to check ipv6 firewall rules are created automatically
	GUI::Basic::Security::Firewall::Open Tab
	GUI::Basic::Click Element	//tr[@id='INPUT:IPv6']//a[@id='INPUT']
	GUI::Basic::Click Element	//tr[@id='1']//input[@type='checkbox']
	GUI::Basic::Edit
	${TARGET_LABEL}	Get Selected List Label	//select[@id='target']
	${IN_IFACE_LABEL}	Get Selected List Label	//select[@id='inIface']
	Should Be Equal	${TARGET_LABEL}	DROP
	Should Be Equal	${IN_IFACE_LABEL}	${BRIDGE_INTERFACE}
	GUI::Basic::Button::Cancel
	GUI::Basic::Return
	GUI::Basic::Click Element	//tr[@id='FORWARD:IPv6']//a[@id='FORWARD']
	GUI::Basic::Click Element	//tr[@id='0']//input[@type='checkbox']
	GUI::Basic::Edit
	${TARGET_LABEL}	Get Selected List Label	//select[@id='target']
	${IN_IFACE_LABEL}	Get Selected List Label	//select[@id='inIface']
	Should Be Equal	${TARGET_LABEL}	DROP
	Should Be Equal	${IN_IFACE_LABEL}	Any
	GUI::Basic::Button::Cancel
	GUI::Basic::Click Element	//tr[@id='1']//input[@type='checkbox']
	GUI::Basic::Edit
	${TARGET_LABEL}	Get Selected List Label	//select[@id='target']
	${IN_IFACE_LABEL}	Get Selected List Label	//select[@id='inIface']
	Should Be Equal	${TARGET_LABEL}	DROP
	Should Be Equal	${IN_IFACE_LABEL}	${BRIDGE_INTERFACE}
	GUI::Basic::Button::Cancel
	GUI::Basic::Return
	GUI::Basic::Click Element	//tr[@id='OUTPUT:IPv6']//a[@id='OUTPUT']
	GUI::Basic::Click Element	//tr[@id='1']//input[@type='checkbox']
	GUI::Basic::Edit
	${TARGET_LABEL}	Get Selected List Label	//select[@id='target']
	${IN_IFACE_LABEL}	Get Selected List Label	//select[@id='inIface']
	${OUT_IFACE_LABEL}	Get Selected List Label	//select[@id='outIface']
	Should Be Equal	${TARGET_LABEL}	DROP
	Should Be Equal	${IN_IFACE_LABEL}	Any
	Should Be Equal	${OUT_IFACE_LABEL}	${BRIDGE_INTERFACE}
	GUI::Basic::Button::Cancel
	GUI::Basic::Return
	[Teardown]	SUITE:Test Teardown

Test case to check ipv4 NAT rule is created automatically
	GUI::Basic::Security::NAT::Open Tab
	GUI::Basic::Click Element	//tr[@id='POSTROUTING:IPv4']//a[@id='POSTROUTING']
	GUI::Basic::Click Element	//td[@class='selectable']//input[@type='checkbox']
	GUI::Basic::Edit
	Textfield Should Contain	//input[@id='nat_rule_desc']	gateway bridge
	Textfield Should Contain	//input[@id='nat_source']	${BRIDGE_NET}/${BRIDGE_BITMASK}
	Textfield Should Contain	//input[@id='nat_dest']	${BRIDGE_NET}/${BRIDGE_BITMASK}
	${TARGET_LABEL}	Get Selected List Label	//select[@id='nat_target']
	${OUT_IFACE_LABEL}	Get Selected List Label	//select[@id='nat_outIface']
	Should Be Equal	${TARGET_LABEL}	MASQUERADE
	Should Be Equal	${OUT_IFACE_LABEL}	Any
	GUI::Basic::Button::Cancel
	GUI::Basic::Return
	[Teardown]	SUITE:Test Teardown

Test case to check DHCP server configuration in gateway profile
	GUI::Basic::Network::Settings::open tab
	Run Keyword If	'${NGVERSION}' <= '5.4'	GUI::Basic::Network::DHCP Server::Open Tab
	Run Keyword If	'${NGVERSION}' >= '5.6'	Run Keywords	GUI::Basic::Network::DHCP::Open Tab	AND	Click Element	//span[normalize-space()='DHCP Server']
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="hostTable"]
	GUI::Basic::Click Element	//a[@id='${DHCP_SUBNET_NETMASK}']
	Textfield Should Contain	//input[@id='dhcpserverprotocol']	DHCP4
	Textfield Should Contain	//input[@id='dhcpdip']	${DHCP_SUBNET_NETMASK}
	Textfield Should Contain	//input[@id='domainnameserver']	${BRIDGE_IP}
	Textfield Should Contain	//input[@id='routers']	${BRIDGE_IP}
	Textfield Should Contain	//input[@id='leasetime']	86400
	GUI::Basic::Click Element	//span[normalize-space()='Network Range']
	Page Should Contain Element	//td[normalize-space()='192.168.10.10/192.168.10.200']
	GUI::Basic::Click Element	//span[normalize-space()='Hosts']
	Page Should Not Contain Element	//td

*** Keywords ***
SUITE:Setup
	Close All Browsers
	${HAS_STRONG_PASSWORD}	Run Keyword And Return Status	GUI::Basic::Open And Login Nodegrid	PASSWORD=${STRONG_PASSWORD}
	Set Suite Variable	${HAS_STRONG_PASSWORD}
	Skip If	not ${HAS_STRONG_PASSWORD}	Host has no strong password. The system is not on Gateway profile. Check "1_out_of_band_to_gateway_functionality.robot".
	${IS_VM}	Set Variable	${FALSE}
	${IS_SR}	Set Variable	${FALSE}
	${IS_NSR}	Set Variable	${FALSE}
	${IS_VM}	GUI::Basic::Is VM System
	${IS_SR}	GUI::Basic::Is SR System
	${IS_NSR}	GUI::Basic::Is Net SR
	Set Suite Variable	${IS_VM}
	Set Suite Variable	${IS_SR}
	Set Suite Variable	${IS_NSR}
	Skip If	not ${IS_SR} or ${IS_VM}	Host is not SR System, probably is a VM (${IS_VM}). It has no System Profile feature.
	Skip If	not ${GLOBAL_HAS_CONSOLE_ACCESS}	The Nodegrid console access is not working on "1_out_of_band_to_gateway_functionality.robot" script.

SUITE:Teardown
	Close All Browsers
	Skip If	not ${HAS_STRONG_PASSWORD}	Host has no strong password. The system is not on Gateway profile. Check "1_out_of_band_to_gateway_functionality.robot".
	Skip If	not ${IS_SR} or ${IS_VM}	Host is not SR System, probably is a VM (${IS_VM}). It has no System Profile feature.
	Skip If	not ${GLOBAL_HAS_CONSOLE_ACCESS}	The Nodegrid console access is not working on "1_out_of_band_to_gateway_functionality.robot" script.
	GUI::Basic::Open And Login Nodegrid	PASSWORD=${STRONG_PASSWORD}
	GUI::Basic::Logout and Close Nodegrid

SUITE:Test Teardown
	GUI::Basic::Spinner Should Be Invisible
	${HAS_CANCEL_BUTTON}	Run Keyword And Return Status	Page Should Contain Button	//input[@id='cancelButton']
	Run Keyword If	${HAS_CANCEL_BUTTON}	Run Keyword And Ignore Error	GUI::Basic::Button::Cancel
	GUI::Basic::Spinner Should Be Invisible
	${HAS_RETURN_BUTTON}	Run Keyword And Return Status	Page Should Contain Button	//input[@id='returnButton']
	Run Keyword If	${HAS_RETURN_BUTTON}	Run Keyword And Ignore Error	GUI::Basic::Return

SUITE:Checkboxes about ignore obtained IP or DNS Should not Be Selected
	Checkbox Should not Be Selected	//input[@id='ipv4neverDefault']	#Ignore obtained IPv4 Default Gateway
	Checkbox Should not Be Selected	//input[@id='ipv4ignoreAutoDns']	#Ignore obtained DNS server
	Checkbox Should not Be Selected	//input[@id='ipv6neverDefault']	#Ignore obtained IPv6 Default Gateway
	Checkbox Should not Be Selected	//input[@id='ipv6ignoreAutoDns']	#Ignore obtained DNS server

SUITE:Radio Button Should Be Set To DHCP and Auto IPv6
	Radio Button Should Be Set To	method	auto	#IPv4 Mode:DHCP
	Radio Button Should Be Set To	method6	auto	#IPv6 Mode:Address Auto Configuration