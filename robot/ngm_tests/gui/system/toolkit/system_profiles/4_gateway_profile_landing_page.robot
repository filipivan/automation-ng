*** Settings ***
Documentation	Validation test cases about gateway profile
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${TEST_CONSOLE_PORT_TTYS}	${CONSOLE_PORT_TTYS}
${TEST_CONSOLE_PORT_NAME}	${CONSOLE_PORT_NAME}
${TEST_CONSOLE_ACCESS_IP}	${CONSOLE_ACCESS_IP}
${STRONG_PASSWORD}	${QA_PASSWORD}
${DEVICE_NAME}	${HOSTNAME_NODEGRID}


*** Test Cases ***
Test case to check in Security::Services if system profile is gateway
	GUI::Basic::Open And Login Nodegrid	PASSWORD=${STRONG_PASSWORD}
	GUI::Basic::Security::Services::Open Tab
	Page Should Contain	System Profile:
	Textfield Should Contain	//input[@id='sysprof']	Gateway

Test case to set Access::Overview as first page
	GUI::Access::Open
	GUI::Basic::Click Element	(//a[contains(.,'Overview')])[2]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Access :: Overview
	GUI::Basic::Click Element	//span[@id='PushpinLabel']
	GUI::Basic::Logout
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Login
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Access :: Overview

Test case to check elements of overview on the gateway profile
	GUI::Access::Open
	GUI::Basic::Click Element	(//a[contains(.,'Overview')])[2]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	WAN
	Page Should Contain	LAN
	SUITE:Access:Overview: Check elements with system profile is gateway

*** Keywords ***
SUITE:Setup
	Run Keyword If	not ${GLOBAL_HAS_CONSOLE_ACCESS}	Skip	The Nodegrid console access is not working on "1_out_of_band_to_gateway_functionality.robot" script.
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid	PASSWORD=${STRONG_PASSWORD}
	${IS_VM}	Set Variable	${FALSE}
	${IS_SR}	Set Variable	${FALSE}
	${IS_NSR}	Set Variable	${FALSE}
	${IS_VM}	GUI::Basic::Is VM System
	${IS_SR}	GUI::Basic::Is SR System
	${IS_NSR}	GUI::Basic::Is Net SR
	Set Suite Variable	${IS_VM}
	Set Suite Variable	${IS_SR}
	Set Suite Variable	${IS_NSR}
	Skip If	not ${IS_SR} or ${IS_VM}	Host is not SR System, probably is a VM (${IS_VM}). It has no System Profile feature.

SUITE:Teardown
	Run Keyword If	not ${GLOBAL_HAS_CONSOLE_ACCESS}	Skip	The Nodegrid console access is not working on "1_out_of_band_to_gateway_functionality.robot" script.
	Run Keyword If	not ${IS_SR} or ${IS_VM}	Close All Browsers
	Skip If	not ${IS_SR} or ${IS_VM}	Host is not SR System, probably is a VM (${IS_VM}). It has no System Profile feature.
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid	PASSWORD=${STRONG_PASSWORD}
	GUI::Basic::Logout and Close Nodegrid

### MY SUITES ###
SUITE:Access:Overview: Check elements with system profile is gateway
	GUI::Basic::Click Element	//span[@class='columns_btn_text'][contains(.,'Widgets')]

	GUI::Basic::Select Checkbox	//input[@id='title_overallStatus_cb']
	Element Should Be Visible	//div[@id='title_overallStatus']
	GUI::Basic::Unselect Checkbox	//input[@id='title_overallStatus_cb']
	Element Should Not Be Visible	//div[@id='title_overallStatus']

	GUI::Basic::Select Checkbox	//input[@id='title_netWAN_cb']
	Element Should Be Visible	//div[@id='title_netWAN']
	GUI::Basic::Unselect Checkbox	//input[@id='title_netWAN_cb']
	Element Should Not Be Visible	//div[@id='title_netWAN']

	GUI::Basic::Select Checkbox	//input[@id='title_netLAN_cb']
	Element Should Be Visible	//div[@id='title_netLAN']
	GUI::Basic::Unselect Checkbox	//input[@id='title_netLAN_cb']
	Element Should Not Be Visible	//div[@id='title_netLAN']

	GUI::Basic::Select Checkbox	//input[@id='title_dhcpClients_cb']
	Element Should Be Visible	//div[@id='title_dhcpClients']
	GUI::Basic::Unselect Checkbox	//input[@id='title_dhcpClients_cb']
	Element Should Not Be Visible	//div[@id='title_dhcpClients']

	GUI::Basic::Select Checkbox	//input[@id='title_btConns_cb']
	Element Should Be Visible	//div[@id='title_btConns']
	GUI::Basic::Unselect Checkbox	//input[@id='title_btConns_cb']
	Element Should Not Be Visible	//div[@id='title_btConns']

	GUI::Basic::Select Checkbox	//input[@id='title_vms_cb']
	Element Should Be Visible	//div[@id='title_vms']
	GUI::Basic::Unselect Checkbox	//input[@id='title_vms_cb']
	Element Should Not Be Visible	//div[@id='title_vms']

	GUI::Basic::Select Checkbox	//input[@id='title_genSysInfo_cb']
	Element Should Be Visible	//div[contains(@id,'genSysInfo')]
	GUI::Basic::Unselect Checkbox	//input[@id='title_genSysInfo_cb']
	Element Should Not Be Visible	//div[contains(@id,'genSysInfo')]

	GUI::Basic::Select Checkbox	//input[@id='title_genNetInfo_cb']
	Element Should Be Visible	//div[contains(@id,'genNetInfo')]
	GUI::Basic::Unselect Checkbox	//input[@id='title_genNetInfo_cb']
	Element Should Not Be Visible	//div[contains(@id,'genNetInfo')]

	GUI::Basic::Select Checkbox	//input[@id='title_vpnConn_cb']
	Element Should Be Visible	//div[@id='title_vpnConn']
	GUI::Basic::Unselect Checkbox	//input[@id='title_vpnConn_cb']
	Element Should Not Be Visible	//div[@id='title_vpnConn']

	GUI::Basic::Select Checkbox	//input[@id='title_hsClients_cb']
	Element Should Be Visible	//div[@id='title_hsClients']
	GUI::Basic::Unselect Checkbox	//input[@id='title_hsClients_cb']
	Element Should Not Be Visible	//div[@id='title_hsClients']

	GUI::Basic::Select Checkbox	//input[@id='title_routingTable_cb']
	Element Should Be Visible	//div[@id='title_routingTable']
	GUI::Basic::Unselect Checkbox	//input[@id='title_routingTable_cb']
	Element Should Not Be Visible	//div[@id='title_routingTable']

	GUI::Basic::Select Checkbox	//input[@id='title_switchConn_cb']
	Element Should Be Visible	//div[@id='title_switchConn']
	GUI::Basic::Unselect Checkbox	//input[@id='title_switchConn_cb']
	Element Should Not Be Visible	//div[@id='title_switchConn']

