*** Settings ***
Documentation	Editing System > Toolkit > Apply Settings Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE	EXCLUDEIN3_2	TOOLKIT	APPLY_SETTINGS
Default Tags	GUI

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

*** Test Cases ***
Test Open Apply Settings
	Element Should Be Visible	jquery=#icon_applySettings
	Click Element	jquery=#icon_applySettings
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Disabled	jquery=#saveButton
	Element Should Be Enabled	jquery=#cancelButton

Test Apply Settings Blank URL
	Select Radio Button	applytypeselection	applysettings-remote
	GUI::Basic::Save	${TRUE}
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Incorrect URL information.

Test Apply Settings Invalid URL Format
	GUI::Basic::Wait Until Element Is Accessible	jquery=#url
	GUI::Basic::Input Text	//*[@id='url']	)!*¨%)!$%[[´]
	GUI::Basic::Save	${TRUE}
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>

Test Apply Settings Invalid URL Path
	GUI::Basic::Input Text	//*[@id='url']	http://%IP/invalidpath
	GUI::Basic::Save	${TRUE}
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.

Test Apply Settings Username And Password Required
	GUI::Basic::Input Text	//*[@id='url']	ftp://asdf/asfd
	GUI::Basic::Save	${TRUE}
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Protocol requires username/password.

Test Apply Settings Invalid File URL
	GUI::Basic::Input Text	//*[@id='userName']	user
	GUI::Basic::Input Text	//*[@id='password']	password
	wait until keyword succeeds	30s	5x	GUI::Basic::Save	True
	Wait Until Element Contains	jquery=#globalerr	File Transfer Failed

Test Apply Settings Invalid Remote File Name URL
	GUI::Basic::Input Text	//*[@id='url']	ftp://%IP/invalidpath
	GUI::Basic::Save	${TRUE}
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.

Test Screen Blocks on File Not Selected
	[Documentation]	Disabled save and handle alert actions due Selenium is not prepared to handle 2 alerts in sequence
	Select Radio Button	applytypeselection	applysettings-localcomputer
	# Click Element	//*[@id='saveButton']
	# Wait Until Keyword Succeeds	15s	3x	Handle Alert	timeout=10s
	Wait Until Keyword Succeeds	30s	5x	GUI::Basic::Spinner Should Be Invisible

Test Apply Settings File Not Found
	Select Radio Button	applytypeselection	applysettings-local
	GUI::Basic::Input Text	//*[@id='localFilename']	why1would2this3file4exist5lol6.txt
	GUI::Basic::Save	${TRUE}
	Wait Until Element Contains	jquery=#globalerr	Apply configuration settings failed. File not found.
	GUI::Basic::Cancel

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Toolkit::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	${HAS_ALERT}=	Run Keyword And Return Status	Alert Should Be Present	timeout=10s
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
