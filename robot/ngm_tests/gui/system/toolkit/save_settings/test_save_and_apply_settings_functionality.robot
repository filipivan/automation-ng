*** Settings ***
Documentation	Functionality test cases throught GUI in System > Toolkit > Save Settings and Apply Settings
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USB_NAME}	automation._test_usb_rename
${SAVE_FILE_NAME}	automated_test_save_settings

*** Test Cases ***
Test Edit Session Idle Timeout
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Preferences::Open Tab
	Wait Until Element Is Visible	//*[@id='idletimeout']
	${TIMEOUT_VALUE}	Get Value	//*[@id='idletimeout']
	${STATUS}	Run Keyword And Return Status	Should Be Equal	${TIMEOUT_VALUE}	300
	GUI::Basic::Click Element	//*[@id='idletimeout']
	Run Keyword If	not ${STATUS}	Run Keywords	GUI::Basic::Input Text	//*[@id='idletimeout']	300	AND
	...	GUI::Basic::Save	AND	GUI::Basic::Click Element	//*[@id='idletimeout']
	GUI::Basic::Input Text	//*[@id='idletimeout']	0
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save

Test Modifie an Usb Port
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::Open Tab
	${BDOT}	${ADOT}	GUI::Basic::Fetch NGversion
	${HAS_USB}	Run Keyword And Return Status	Page Should Contain	usbS
	Run Keyword If	not ${HAS_USB}	Run Keywords
	...	Log	"Test Modifie an Usb Port" was excluded from that build, because the unit using v${NGVERSION} Has no Usb Port	WARN	console=yes
	...	AND	Skip
	IF	${HAS_USB}
		SUITE:Change Usb port name
		SUITE:Edit Usb Port Configuration
		SUITE:Check Usb port on Access page
	END

Test Enable Docker And Qemu/KVM
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	wait until element is visible	//*[@id="virtualization_docker"]
	Unselect Checkbox	//*[@id="virtualization_docker"]
	wait until element is visible	//input[@id='virtualization_libvirt']
	Unselect Checkbox	//input[@id='virtualization_libvirt']
	GUI::Basic::Save If Configuration Changed	HAS_ALERT=True
	Checkbox Should Not Be Selected	//*[@id="virtualization_docker"]
	Select Checkbox	//*[@id="virtualization_docker"]
	Checkbox Should Not Be Selected	//input[@id='virtualization_libvirt']
	Select Checkbox	//input[@id='virtualization_libvirt']
	GUI::Basic::Save If Configuration Changed	HAS_ALERT=True
	GUI::Basic::Spinner Should Be Invisible

Test Save Settings
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Toolkit::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	jquery=#icon_saveSettings
	Click Element	jquery=#icon_saveSettings
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Disabled	jquery=#saveButton
	Element Should Be Enabled	jquery=#cancelButton
	GUI::Basic::Click Element	//input[@id='localFilename']
	GUI::Basic::Input Text	//input[@id='localFilename']	${SAVE_FILE_NAME}
	GUI::Basic::Save
	Page Should Contain	Configuration saved. File:${SAVE_FILE_NAME}
	Click Element	//input[@id='finish']
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	jquery=#icon_saveSettings

Test Edit Session Idle Timeout to Default
	SUITE:Edit Session Idle Timeout to Default

Test Edit Usb device to Default
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::Open Tab
	${HAS_USB}	Run Keyword And Return Status	Page Should Contain	usbS
	SUITE:Edit Usb device to Default

Test Disable Docker And Qemu/KVM
	SUITE:Disable Docker And Qemu/KVM

Test Apply Settings
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Toolkit::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	wait until page contains element	jquery=#icon_applySettings
	Click Element	jquery=#icon_applySettings
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Disabled	jquery=#saveButton
	Element Should Be Enabled	jquery=#cancelButton
	GUI::Basic::Click Element	//input[@id='localFilename']
	GUI::Basic::Input Text	//input[@id='localFilename']	${SAVE_FILE_NAME}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save	HAS_ALERT=True
	GUI::Basic::Spinner Should Be Invisible	SPINNER_TIMEOUT=2m
	Page Should Contain	Configuration settings applied. This may disconnect your session.
	Click Element	//input[@id='finish']
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	//a[@id='icon_applySettings']
	GUI::Basic::Logout And Close Nodegrid
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

Test Check Saved Settings about Session Idle Timeout
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	3x	1s	Run Keywords	GUI::Basic::Re-Login	AND	GUI::Basic::System::Preferences::Open Tab
	Wait Until Element Is Visible	//*[@id='idletimeout']
	${TIMEOUT_VALUE}	Get Value	//*[@id='idletimeout']
	${STATUS}	Run Keyword And Return Status	Should Be Equal	${TIMEOUT_VALUE}	0

Test Check Saved Settings about Usb Port
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${BDOT}	${ADOT}	GUI::Basic::Fetch NGversion
	${HAS_USB}	Run Keyword And Return Status	Page Should Contain	usbS
	Run Keyword If	not ${HAS_USB}	Run Keywords
	...	Log	"Test Check Saved Settings about Usb Port" was excluded from that build, because the unit using v${NGVERSION} Has no Usb Port	WARN	console=yes
	...	AND	Skip
	IF	${HAS_USB}
		Page Should Contain	${USB_NAME}
		GUI::Basic::Spinner Should Be Invisible
		GUI::Basic::Click Element	(//a[contains(text(),'${USB_NAME}')])[1]
		GUI::Basic::Spinner Should Be Invisible
		Element Should Contain	//select[@id='baud']	115200
		Element Should Contain	//select[@id='status']	Enabled
		Page Should Contain Image	(//img[@src='/icon/usb.png'])[1]
		Checkbox Should Be Selected	//input[@id='dataflow']
		Checkbox Should Be Selected	//input[@id='discname']
		SUITE:Check Usb port on Access page
	END

Test Check Saved Settings about Docker And Qemu/KVM
	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	Checkbox Should Be Selected	//*[@id="virtualization_docker"]
	Checkbox Should Be Selected	//input[@id='virtualization_libvirt']

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Edit Session Idle Timeout to Default
	SUITE:Edit Usb device to Default
	SUITE:Disable Docker And Qemu/KVM
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

SUITE:Rename Usb to Default
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${USB_ID_NAME}	SUITE:Get Usb ID Name
	GUI::Basic::Select Checkbox	//tr[@id='${USB_NAME}']//input[@type='checkbox']
	GUI::Basic::Click Element	//input[@value='Rename']
	GUI::Basic::Click Element	//input[@id='spm_name']
	GUI::Basic::Input Text	//input[@id='spm_name']	${USB_ID_NAME}
	GUI::Basic::Save

SUITE:Get Usb ID Name
	${USB_ID_NAME}	Set Variable	usbS2
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${USB_ID_NAME2}	Get Text	(//a[contains(text(),'usbS')])[1]
	${STATUS}	Run Keyword And Return Status	Should Match Regexp	${USB_ID_NAME2}	usbS[0-5]-.
	${USB_NAME_PART1}	Run Keyword If	${STATUS}	Fetch From Left	${USB_ID_NAME2}	-
	IF	${STATUS}
		${USB_ID_NAME}	Set Variable	${USB_NAME_PART1}-2
	END
	Log To Console	\n\nFirst Usb Default Name Port:\n\n${USB_ID_NAME}
	[Return]	${USB_ID_NAME}

SUITE:Set Usb Configuration to Default
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::Open Tab
	Wait Until Element Is Visible	(//a[contains(text(),'usbS')])[1]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	(//a[contains(text(),'${USB_NAME}')])[1]
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	//select[@id='baud']	9600
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	//select[@id='status']	Disabled
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//input[@value='Select Icon']
	GUI::Basic::Click Element	//img[@src='/icon/terminal.png']
	GUI::Basic::Click Element	//input[@value='Select Icon']
	GUI::Basic::Unselect Checkbox	//input[@id='dataflow']
	GUI::Basic::Unselect Checkbox	//input[@id='discname']
	Set Focus To Element		//*[@id="saveButton"]
	execute javascript  document.getElementById("saveButton").scrollIntoView()
	wait until page contains element		//*[@id="saveButton"]
	click element		//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:Edit Session Idle Timeout to Default
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Preferences::Open Tab
	Wait Until Element Is Visible	//*[@id='idletimeout']
	${TIMEOUT_VALUE}	Get Value	//*[@id='idletimeout']
	${STATUS}	Run Keyword And Return Status	Should Be Equal	${TIMEOUT_VALUE}	0
	GUI::Basic::Click Element	//*[@id='idletimeout']
	Run Keyword If	not ${STATUS}	Run Keywords	GUI::Basic::Input Text	//*[@id='idletimeout']	0	AND
	...	GUI::Basic::Save	AND	GUI::Basic::Click Element	//*[@id='idletimeout']
	GUI::Basic::Input Text	//*[@id='idletimeout']	300
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save

SUITE:Edit Usb device to Default
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${BDOT}	${ADOT}	GUI::Basic::Fetch NGversion
	${HAS_USB}	Run Keyword And Return Status	Page Should Contain	usbS
	IF	${HAS_USB}
		${HAS_USB_NAME}	Run Keyword And Return Status	Page Should Contain	${USB_NAME}
		Run Keyword If	${HAS_USB_NAME}	Run Keywords	SUITE:Set Usb Configuration to Default	AND	SUITE:Rename Usb to Default
	END

SUITE:Disable Docker And Qemu/KVM
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	set focus to element	//*[@id="virtualization_docker"]
	wait until element is visible	//*[@id="virtualization_docker"]
	Unselect Checkbox	//*[@id="virtualization_docker"]
	wait until element is visible	//input[@id='virtualization_libvirt']
	Unselect Checkbox	//input[@id='virtualization_libvirt']
	GUI::Basic::Save If Configuration Changed	HAS_ALERT=True

SUITE:Change Usb port name
	${HAS_USB_NAME}	Run Keyword And Return Status	Page Should Contain	${USB_NAME}
	Run Keyword If	${HAS_USB_NAME}	Run Keywords	SUITE:Set Usb Configuration to Default	AND	SUITE:Rename Usb to Default
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::Open Tab
	Wait Until Element Is Visible	(//a[contains(text(),'usbS')])[2]
	${USB_ID_NAME}	Get Text	(//a[contains(text(),'usbS')])[2]
	GUI::Basic::Select Checkbox	//tr[@id='${USB_ID_NAME}']//input[@type='checkbox']
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//input[@value='Rename']
	GUI::Basic::Click Element	//input[@id='spm_name']
	GUI::Basic::Input Text	//input[@id='spm_name']	${USB_NAME}
	GUI::Basic::Save

SUITE:Edit Usb Port Configuration
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	(//a[contains(text(),'${USB_NAME}')])[1]
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	//select[@id='baud']	115200
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	//select[@id='status']	Enabled
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//input[@value='Select Icon']
	GUI::Basic::Click Element	//img[@src='/icon/usb.png']
	GUI::Basic::Click Element	//input[@value='Select Icon']
	GUI::Basic::Select Checkbox	//input[@id='dataflow']
	GUI::Basic::Select Checkbox	//input[@id='discname']
	GUI::Basic::Save

SUITE:Check Usb port on Access page
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Image	(//img[@src='/icon/usb.png'])[1]
	Page Should Contain	${USB_NAME}