*** Settings ***
Documentation	Editing System > Toolkit > Save Settings Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test Open Save Settings
	Element Should Be Visible	jquery=#icon_saveSettings
	Click Element	jquery=#icon_saveSettings
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Disabled	jquery=#saveButton
	Element Should Be Enabled	jquery=#cancelButton

Test Save Settings Blank URL
	Select Radio Button	savetypeselection	savesettings-remote
	GUI::Basic::Save
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Incorrect URL information.

Test Save Settings Invalid URL Format
	GUI::Basic::Wait Until Element Is Accessible	jquery=#url
	GUI::Basic::Input Text	//*[@id='url']	)!*¨%)!$%[[´]
	GUI::Basic::Save
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>

Test Save Settings Invalid URL Path
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Basic::Input Text	//*[@id='url']	http://%IP/invalidpath
	GUI::Basic::Save
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Run Keyword If	'${NGVERSION}' == '4.2'	Wait Until Element Contains	jquery=#errormsg	Invalid protocol. Supported:ftp,tftp,sftp and scp.
	Run Keyword If	'${NGVERSION}' >= '5.0'	Wait Until Element Contains	jquery=#errormsg	Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>

Test Save Settings Username And Password Required
	GUI::Basic::Input Text	//*[@id='url']	ftp://asdf/asfd
	GUI::Basic::Save
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Protocol requires username/password.

Test Save Settings Invalid File URL
	GUI::Basic::Input Text	//*[@id='userName']	user
	GUI::Basic::Input Text	//*[@id='password']	password
	GUI::Basic::Save
	Wait Until Element Contains	jquery=#globalerr	Save configuration settings failed. File transfer error.

Test Save Settings Invalid Remote File Name URL
	GUI::Basic::Input Text	//*[@id='url']	ftp://%IP/invalidpath
	GUI::Basic::Save
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Run Keyword If	'${NGVERSION}' == '4.2'	Wait Until Element Contains	jquery=#errormsg	Invalid remote file name
	Run Keyword If	'${NGVERSION}' >= '5.0'	Wait Until Element Contains	jquery=#errormsg	Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>
	GUI::Basic::Cancel

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Toolkit::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
