*** Settings ***
Documentation	Editing System > Toolkit > Network Tools Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${IP Addr}	8.8.8.8

*** Test Cases ***
Test Network Tools Page
	Element Should Be Visible	jquery=#icon_NetworkTools
	Click Element	jquery=#icon_NetworkTools
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Enabled	jquery=#cancelButton
	Element Should Be Enabled	jquery=#ipaddress
	Element Should Be Enabled	jquery=#wanname
	Element Should Be Enabled	jquery=#log

Test Ping
	Input Text	//*[@id="ipaddress"]	${IP Addr}
	Run Keyword If  '${NGVERSION}'>'5.0'    Run Keyword And Continue On Failure     Select From List By Label  id=interface   eth0
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//input[@value="Ping"]
	Wait Until Page Contains	ping statistics   15

Test Ping with the source interface
    Input Text	//*[@id="ipaddress"]	${IP Addr}
	Run Keyword If  '${NGVERSION}'>'5.0'    Run Keyword And Continue On Failure     Select From List By Label  id=interface   Source IP Address
	Run Keyword If  '${NGVERSION}'>'5.0'    Input Text  id=interfaceIp      ${HOST}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//input[@value="Ping"]
	Wait Until Page Contains	ping statistics   15

Test DNS Lookup
	Input Text	jquery=#wanname	bc.edu
	Click element	xpath=//input[@value='Lookup']
	Sleep	2
	Page Should Contain Element	xpath=//*[@id="log"]
	${OUTPT}=	Get Text	xpath=//*[@id="log"]
	Should Be Equal	${OUTPT}	136.167.2.220

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Toolkit::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
