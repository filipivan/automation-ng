*** Settings ***
Documentation	Detecting of MTU Size
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_2

*** Variables ***
${ip_address}	google.com
${Interface}	eth0

*** Test Cases ***
Test case to check mtu button
	GUI::Basic::System::Toolkit::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="icon_NetworkTools"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//input[@value='Detect MTU']

Test Case to check mtu size packet
	GUI::Basic::System::Toolkit::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="icon_NetworkTools"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="ipaddress"]
	input text	//*[@id="ipaddress"]	${ip_address}
	Select From List By Value	css=#interface	${Interface}
	click element	xpath=//input[@value='Detect MTU']
	GUI::Basic::Spinner Should Be Invisible
	Wait Until page contains	Detected MTU is 1500.	60s

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid