*** Settings ***
Documentation	Check System CSR create
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3		GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	MAC	CHROME
Default Tags	EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	Close all Browsers

*** Variables ***
${CSR}      -----BEGIN CERTIFICATE REQUEST-----
${COUNT_CODE_I}   IN
${STATE_K}   Karnataka
${LOC_B}   Bangalore
${ORG_Z}   ZPE
${ORG_UNI_A}   Automation
${COM_NAME_T}   www.test.com
${EMAIL_T}   test@zpesystems.com
${ALT_T}   test.zpesystems.com
${VALID_DUR}    500
${COUNT_CODE_B}   BR
${STATE_C}   Centro
${LOC_Bl}   Blumenau
${ORG_UNI_Q}   QA
${COM_NAME_A}   www.auto.com
${EMAIL_A}   auto@zpesystems.com
${ALT_A}   auto.zpesystems.com

*** Test Cases ***
Create a self signed certificate
    SUITE:Setup
    SUITE:Certificate View
    Click Element  id=countryCode
    Input Text  id=countryCode   ${COUNT_CODE_I}
    Click Element  id=state
    Input Text  id=state   ${STATE_K}
    Click Element  id=locality
    Input Text  id=locality   ${LOC_B}
    Click Element  id=organization
    Input Text  id=organization   ${ORG_Z}
    Click Element  id=orgUnit
    Input Text  id=orgUnit   ${ORG_UNI_A}
    Click Element  id=commonName
    Input Text  id=commonName   ${COM_NAME_T}
    Click Element  id=email
    Input Text  id=email   ${EMAIL_T}
    Click Element  id=altNames
    Input Text  id=altNames   ${ALT_T}
    Select Checkbox  //*[@id="enableAlternativeSave"]
    Click Element  id=validDuration
    Input Text  id=validDuration   ${VALID_DUR}
    GUI::Basic::Spinner Should Be Invisible
    Click Element  //*[@id='alternativeSaveButton']
    Sleep       10s

Create a self signed certificate and check Banner
	[Tags]	NON-CRITICAL	NEED-REVIEW
#	Checked manually/run test_1 the issue was not occurred
    Wait Until Keyword Succeeds 	30		5		SUITE:Setup
    SUITE:Certificate View
    Click Element  id=countryCode
    Input Text  id=countryCode   ${COUNT_CODE_B}
    Click Element  id=state
    Input Text  id=state   ${STATE_C}
    Click Element  id=locality
    Input Text  id=locality   ${LOC_Bl}
    Click Element  id=organization
    Input Text  id=organization   ${ORG_Z}
    Click Element  id=orgUnit
    Input Text  id=orgUnit   ${ORG_UNI_Q}
    Click Element  id=commonName
    Input Text  id=commonName   ${COM_NAME_A}
    Click Element  id=email
    Input Text  id=email   ${EMAIL_A}
    Click Element  id=altNames
    Input Text  id=altNames   ${ALT_A}
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    Sleep  20s
    Wait Until Page Contains Element        //textarea[@id='csrBannerMessage']        60s          ${CSR}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Toolkit::open tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Certificate View
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be Visible   jquery=#icon_systemCert
    Click Element   jquery=#icon_systemCert
    GUI::Basic::Spinner Should Be Invisible
    Click Element  css=#systemCSRGenerate > .title_b
    GUI::Basic::Spinner Should Be Invisible