*** Settings ***
Documentation	Editing System > Toolkit >System Certifacte Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test System Certificate Page
	Element Should Be Visible	jquery=#icon_systemCert
	Click Element	jquery=#icon_systemCert
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="notice"]
	Element Should Be Disabled	jquery=#saveButton
#	Element Should Be Enabled	jquery=#cancelButton

Test System Certificate Blank URL
	Select Radio Button	systemcertselection	systemcert-remote
	GUI::Basic::Input Text	//*[@id='userName']	user
	GUI::Basic::Save	True
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Incorrect URL information.

Test System Certificate Invalid URL Format
	GUI::Basic::Wait Until Element Is Accessible	jquery=#url
	GUI::Basic::Input Text	//*[@id='url']	)!*¨%)!$%[[´]
	GUI::Basic::Input Text	//*[@id='userName']	${EMPTY}
	GUI::Basic::Save	True
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>

Test System Certificate Invalid URL Path
	GUI::Basic::Input Text	//*[@id='url']	http://%IP/invalidpath
	GUI::Basic::Save	True
	sleep  1s
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Run Keyword If  '${NGVERSION}'=='4.2'	Wait Until Element Contains	jquery=#errormsg	Invalid protocol. Supported: ftp, tftp, sftp and scp.
	Run Keyword If  '${NGVERSION}'>'4.2'	  Wait Until Element Contains	jquery=#errormsg	Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>

Test System Certificate Username And Password Required
	GUI::Basic::Input Text	//*[@id='url']	ftp://asdf/asfd
	GUI::Basic::Save	True
	sleep  1s
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Protocol requires username/password.

Test System Certificate Invalid File URL
	GUI::Basic::Input Text	//*[@id='userName']	user
	GUI::Basic::Input Text	//*[@id='password']	password
	GUI::Basic::Save	True
	Wait Until Element Contains	jquery=#globalerr	File Transfer Failed

Test System Certificate Invalid Remote File Name URL
	GUI::Basic::Input Text	//*[@id='url']	ftp://%IP/invalidpath
	GUI::Basic::Save	True
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Run Keyword If  '${NGVERSION}'=='4.2'	Wait Until Element Contains	jquery=#errormsg	Invalid remote file name
	Run Keyword If  '${NGVERSION}'>'4.2'	Wait Until Element Contains	jquery=#errormsg	Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>

Test System Certificate Screen Blocks on File Not Selected
	Select Radio Button	systemcertselection	systemcert-localcomputer
	Click Element	//*[@id='saveButton']
	#Handle Alert
	#Handle Alert
	Run Keyword If	'${BROWSER}' == 'FIREFOX'	Handle Alert	action=DISMISS
	Run Keyword If	'${BROWSER}' == 'CHROME'	Handle Alert	action=ACCEPT
	Run Keyword If	'${BROWSER}' == 'CHROME'	Handle Alert	action=ACCEPT

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Toolkit::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Close All Browsers