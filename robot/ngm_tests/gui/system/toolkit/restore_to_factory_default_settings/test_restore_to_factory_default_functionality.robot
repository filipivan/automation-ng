*** Settings ***
Documentation	Functionality test cases about restore to factory default settings
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-3   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}

*** Variables ***
${VALUE}	10
${NAME}	${DUMMY_DEVICE_CONSOLE_NAME}
${NAME_HOSTS}	Test-Automation.qa
${TYPE}	device_console
${IP_ADDRESS}	20.0.0.1
${ALIAS_NAME}	automation-Test.of-validationqa.com
${TEST_PASSWORD}	admin
${TEST_CONFIRM_PASSWORD}	admin

*** Test Cases ***
Test Case to Add Custom Field
	GUI::Basic::System::Custom Fields::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="custom_name"]	${NAME}
	Input Text	//*[@id="custom_value"]	${VALUE}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="${NAME}"]

Test Case to Add Host Field
	SUITE:Test Setup
	GUI::Basic::Network::Hosts::open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="hostip"]	${IP_ADDRESS}
	Input Text	//*[@id="name"]	${NAME_HOSTS}
	Input Text	//*[@id="alias"]	${ALIAS_NAME}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="${IP_ADDRESS}"]

Test Case to Add Device
	SUITE:Test Setup
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text	//*[@id='spm_name']	${NAME}
	Select From List By Label	//*[@id="type"]	${TYPE}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="${NAME}"]

Test Case to Add Local Accounts
	SUITE:Test Setup
	GUI::Basic::Security::Local Accounts::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="uName"]	${NAME}
	Input Text	//*[@id="uPasswd"]	${TEST_PASSWORD}
	Input Text	//*[@id="cPasswd"]	${TEST_CONFIRM_PASSWORD}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="${NAME}"]

Test Case to Restore Factory default Settings
	SUITE:Test Setup
	GUI::Basic::System::Toolkit::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="icon_factoryDefault"]
	Click Element	//*[@id="icon_factoryDefault"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element Accept Alert	//input[@value='Restore']
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	3x	3s	SUITE:Check If Factory Settings Is Done
	GUI::Basic::Spinner Should Be Invisible

Test Case Configuration Should Not Persist
	SUITE:Test Setup
	GUI::Basic::System::Custom Fields::open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Not Contain Element	//*[@id="${NAME}"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Hosts::open Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Not Contain Element	//*[@id="${IP_ADDRESS}"]
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Not Contain Element	//*[@id="${NAME}"]
	GUI::Basic::Security::Local Accounts::open tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Not Contain Element	//*[@id="${NAME}"]

*** Keywords ***
SUITE:Setup
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	SUITE:Delete Devices, Custon Fields, Hosts And Local Accounts If Exist

SUITE:Teardown
	GUI::Basic::Check If Host Has Factory Settings
	Close All Browsers
	${1ST_LOGIN}	Run Keyword And Return Status	GUI::Basic::Open And Login Nodegrid
	Close All Browsers
	IF	'${NGVERSION}' >= '5.0' and not ${1ST_LOGIN}
		${STATUS}	Run Keyword And Return Status	GUI::Basic::Open And Login Nodegrid	USERNAME=${DEFAULT_USERNAME}	PASSWORD=${QA_PASSWORD}
		Run Keyword If	${STATUS}	GUI::Basic::Enable Security For SSH Root Access By GUI and Change the Passwords By CLI
	END
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	SUITE:Delete Devices, Custon Fields, Hosts And Local Accounts If Exist
	GUI::Basic::Logout And Close Nodegrid

SUITE:Delete Devices, Custon Fields, Hosts And Local Accounts If Exist
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete All Devices
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Delete Custom Field If Exist
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Delete Hosts If Exist
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Delete Local Account If Exist
	GUI::Basic::Spinner Should Be Invisible

SUITE:Test Setup
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Check If Factory Settings Is Done
	Sleep	3m
	Wait Until Keyword Succeeds	40x	3s	GUI::Basic::Open NodeGrid
	Run Keyword If	'${NGVERSION}' >= '5.0'	GUI::Basic::Post Factory Settings to v5.0+
	Run Keyword If	'${NGVERSION}' == '4.2'	SUITE:Post Factory Settings To v4.2
	GUI::Basic::Spinner Should Be Invisible

SUITE:Post Factory Settings To v4.2
	Wait Until Element Is Visible	password
	Input Text	username	${DEFAULT_USERNAME}
	Input Text	password	${DEFAULT_PASSWORD}
	Click Element	id=login-btn
	GUI::Basic::Spinner Should Be Invisible	SPINNER_TIMEOUT=2m
	Wait until Keyword Succeeds	1m	2s	Wait Until Element Is Visible	id=main_menu
	Wait Until Element Is Visible	id=pwl
	Wait Until Element Is Visible	jquery=div.widget-title > h4

SUITE:Delete Custom Field If Exist
	GUI::Basic::System::Custom Fields::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete All Rows In Table If Exists	\#CustomFieldsForm	HAS_ALERT=False

SUITE:Delete Hosts If Exist
	GUI::Basic::Network::Hosts::open tab
	GUI::Basic::Spinner Should Be Invisible
	${STATUS}	Run Keyword And Return Status	Page Should Contain	${IP_ADDRESS}
	Run Keyword If	${STATUS}	GUI::Basic::Delete Rows In Table Containing Value	TABLE_ID=hostTable	VALUE=${IP_ADDRESS}	HAS_ALERT=False
	GUI::Basic::Spinner Should Be Invisible
	Page Should Not Contain	${IP_ADDRESS}

SUITE:Delete Local Account If Exist
	GUI::Basic::Security::Local Accounts::open tab
	GUI::Basic::Spinner Should Be Invisible
	${STATUS}	Run Keyword And Return Status	Page Should Contain	${NAME}
	Run Keyword If	${STATUS}	GUI::Basic::Delete Rows In Table Containing Value	TABLE_ID=user_namesTable	VALUE=${NAME}
	GUI::Basic::Spinner Should Be Invisible
	Page Should Not Contain	${NAME}