*** Settings ***
Documentation	Editing System > Toolkit > Restore To Factory Default Settings Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

*** Test Cases ***
Test Restore
	Element Should Be Visible	jquery=#icon_factoryDefault
	Click Element	jquery=#icon_factoryDefault
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Configuration will be restored to the factory default settings and system will reboot.
	Wait Until Element Is Visible	jquery=#clearlog
	Element Should Be Enabled	//*[@id="nonAccessControls"]/input[1]
	Element Should Be Enabled	//*[@id="cancelButton"]
	Click Element	//*[@id="cancelButton"]

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Toolkit::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
