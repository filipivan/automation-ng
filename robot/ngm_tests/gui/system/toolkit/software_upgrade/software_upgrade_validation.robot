*** Settings ***
Documentation	Editing System > Toolkit > Software Upgrade Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_0

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test Open Upgrade
	Element Should Be Visible	jquery=#icon_upgrade
	Click Element	jquery=#icon_upgrade
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Disabled	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Enabled	jquery=#cancelButton
	Page Should Contain	The system will reboot automatically to complete upgrade process
	GUI::Basic::Select Checkbox	//*[@id='reformat']
	Click Element	jquery=#main_menu > li:nth-child(3) > a
	Handle Alert	DISMISS
	GUI::Basic::Unselect Checkbox	//*[@id='reformat']

Test Downgrading Options
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0
	GUI::Basic::System::Toolkit::Open Tab
	Click Element	jquery=#icon_upgrade
	GUI::Basic::Wait Until Element Is Accessible	jquery=#downgrading
	Select Radio Button	downgrading	factory
	Element Should Be Enabled	jquery=#saveButton
	Select Radio Button	downgrading	restore
	Element Should Be Disabled	jquery=#saveButton

Test Upgrage URL Validation
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0
	GUI::Basic::System::Toolkit::Open Tab
	Click Element	jquery=#icon_upgrade
	GUI::Basic::Wait Until Element Is Accessible	jquery=#upgradetypeselection
	Select Radio Button	upgradetypeselection	upgrade-remote
	GUI::Basic::Save	True
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Incorrect URL information.
	GUI::Basic::Wait Until Element Is Accessible	jquery=#url
	GUI::Basic::Input Text	//*[@id='url']	)!*¨%)!$%[[´]
	GUI::Basic::Save	True
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>
	GUI::Basic::Wait Until Element Is Accessible	jquery=#upgradetypeselection
	Select Radio Button	upgradetypeselection	upgrade-remote
	GUI::Basic::Input Text	//*[@id='url']	http://%IP/invalidpath
	GUI::Basic::Input Text	//*[@id='userName']	user
	GUI::Basic::Input Text	//*[@id='password']	password
	GUI::Basic::Save	True
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	GUI::Basic::Cancel

Test Screen Blocks on File Not Selected
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Toolkit::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=#icon_upgrade
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Wait Until Element Is Accessible	jquery=#upgradetypeselection
	Select Radio Button	upgradetypeselection	upgrade-localcomputer
	Click Element	//*[@id='saveButton']
	Handle Alert
	Handle Alert
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Toolkit::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
