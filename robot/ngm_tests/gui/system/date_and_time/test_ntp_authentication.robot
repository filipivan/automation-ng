*** Settings ***
Documentation	Editing System > Date and Time > NTP Authentication testing for NG4.1
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${KEY}	2

*** Test Cases ***
Test table headings
	Element Should Contain	id:thead	Key Number
	Element Should Contain	id:thead	Hash Algorithm

Test add drilldown fields
	Click Element	jquery=#addButton
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#saveButton
	Element Should Be Disabled   jquery=#saveButton
	Element Should Contain	id:main_doc	Key Number
	Element Should Contain	id:main_doc		Hash Algorithm
	Element Should Contain	id:main_doc	Password

Test combo values for field=Hash Algorithm
	@{ALGS}=	Get List Items	jquery=#ntpHash
	@{SHOULD_HAVE}=	Create List	MD5	RMD160	SHA1	SHA256	SHA384	SHA512	SHA3-224	SHA3-256	SHA3-384	SHA3-512
	Sort List	${ALGS}
	Sort List	${SHOULD_HAVE}
	Lists Should Be Equal	${ALGS}	${SHOULD_HAVE}

Test adding invalid values for field=Key Number
	GUI::Basic::Auto Input Tests	ntpKey	${WORD}	${POINTS}	${EMPTY}	${KEY}
	${ret}=	GUI::Basic::Count Table Rows	ntpTable
	Should Be True	${ret}>=1

Test editing valid NTP
	Click element	xpath=//a[@id='${KEY}']
	Wait until element is visible	jquery=#saveButton
	Select From List By Value	xpath=//select[@id='ntpHash']	SHA1
	Click element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	Wait until element is visible	jquery=#addButton
	${KEY_NUM}=	Get Text	//*[@id="${KEY}"]/td[2]
	should be Equal	'${KEY}'	'${KEY_NUM}'
	${HASH}=	Get Text	//*[@id="${KEY}"]/td[3]
	Should Be Equal	'SHA1'	'${HASH}'

Test deleting valid NTP
	Run Keywords	Click Element	xpath=//*[@id="thead"]/tr/th[1]/input	AND	Click Element	jquery=#delButton
	${ret}=	GUI::Basic::Count Table Rows	ntpTable
	should be equal	'${ret}'	'0'

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Date and Time::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=#ntp
	Wait Until Element Is Visible	jquery=#addButton

	#Delete if it already exists
	${ret}=	GUI::Basic::Count Table Rows	ntpTable
	Run Keyword If	${ret} != '0'	Run Keywords	Click Element	xpath=//*[@id="thead"]/tr/th[1]/input	AND	Click Element	jquery=#delButton
	${ret}=	Run Keyword If	${ret} != '0'	GUI::Basic::Count Table Rows	ntpTable
	should be equal	'${ret}'	'0'

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
