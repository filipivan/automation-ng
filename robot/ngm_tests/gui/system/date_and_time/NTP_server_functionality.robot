*** Settings ***
Documentation	Automated test for NTP server
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${DELAY}	5s
${root_login}	shell sudo su -

*** Test Cases ***
Test case to enable Auto via Network Time Protocol at client
	SUITE:AddServerInfo_server
	SUITE:EnableNTP
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${ret}=	Get value	id=hostname
	GUI:Access::Open Table tab
	Click Element	//div[@class="peer_header"]//a[text()="Console"]
	sleep	5
	Switch Window	title=${ret}
	Wait Until Page Contains Element	//iframe[@id='termwindow']
	Select Frame	//iframe[@id='termwindow']
	Sleep	10
	Press Keys	//body	RETURN
	Press Keys	//body	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output	${root_login}
	${OUTPUT}=	GUI:Access::Generic Console Command Output	chronyc sources
	Should Contain	${OUTPUT}	MS Name/IP address	Stratum Poll Reach LastRx Last sample
	${OUTPUT}=	SUITE:Console Command Output	chronyc sources -v
	Should Contain	${OUTPUT}	MS Name/IP address	Stratum Poll Reach LastRx Last sample
	${OUTPUT}=	SUITE:Console Command Output	chronyc tracking
	Should Contain	${OUTPUT}	Leap status	: Normal
	Unselect Frame
	Press Keys	//body	cw
	Close All Browsers
	SUITE:Teardown

Test case to Enable Auto via Network Time Protocol and perform commands at server
	SUITE:AddServerInfo_server
	GUI::Basic::Open Nodegrid	${HOMEPAGEPEER}	${BROWSER}	${NGVERSION}
	GUI::Basic::login
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${ret}=	Get value	id=hostname
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="peer_header"]//a[text()="Console"]
	sleep	5
	Switch Window	title=${ret}
	Wait Until Page Contains Element	//iframe[@id='termwindow']
	Select Frame	//iframe[@id='termwindow']
	Sleep	10
	Press Keys	//body	RETURN
	Press Keys	//body	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output	${root_login}
	${OUTPUT}=	GUI:Access::Generic Console Command Output	watch chronyc sources -v
	Should Contain	${OUTPUT}	^*
	Press Keys	//body	CTRL+C
	Press Keys	//body	RETURN
	Press Keys	//body	RETURN
	${OUTPUT}=	SUITE:Console Command Output	chronyc tracking
	Should Contain	${OUTPUT}	Leap status	: Normal
	Unselect Frame
	Press Keys	//body	cw
	Close All Browsers
	SUITE:Teardown

Test case to Add Manual Settings
	SUITE:AddServerInfo_server
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="dateTimeLocalForm"]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[4]/label
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open Nodegrid	${HOMEPAGEPEER}	${BROWSER}	${NGVERSION}
	GUI::Basic::login
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${ret}=	Get value	id=hostname
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="peer_header"]//a[text()="Console"]
	sleep	5
	Switch Window	title=${ret}
	Wait Until Page Contains Element	//iframe[@id='termwindow']
	Select Frame	//iframe[@id='termwindow']
	Sleep	10
	Press Keys	//body	RETURN
	Press Keys	//body	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output	${root_login}
	${OUTPUT}=	GUI:Access::Generic Console Command Output	watch chronyc sources -v
	Sleep	5
	Should Contain	${OUTPUT}	10
	Unselect Frame
	Press Keys	//body	cw
	Close All Browsers
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="dateTimeLocalForm"]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[1]/label
	GUI::Basic::Spinner Should Be Invisible
	sleep	${DELAY}
	wait until Element is visible	//*[@id="server"]
	Click Element	//*[@id="server"]
	Clear Element Text	//*[@id="server"]
	Input Text	//*[@id="server"]	pool.ntp.org
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Teardown

Test case to Disable NTP server
	SUITE:AddServerInfo_server
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#ntp_server > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Unselect Checkbox	//*[@id="serverEnabled"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open Nodegrid	${HOMEPAGEPEER}	${BROWSER}	${NGVERSION}
	GUI::Basic::login
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${ret}=	Get value	id=hostname
	GUI::Basic::Access::Table::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="peer_header"]//a[text()="Console"]
	sleep	5
	Switch Window	title=${ret}
	Wait Until Page Contains Element	//iframe[@id='termwindow']
	Select Frame	//iframe[@id='termwindow']
	Sleep	10
	Press Keys	//body	RETURN
	Press Keys	//body	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output	${root_login}
	${OUTPUT}=	GUI:Access::Generic Console Command Output	watch chronyc sources -v
	Should Contain	${OUTPUT}	MS Name/IP address	Stratum Poll Reach LastRx Last sample
	Unselect Frame
	Press Keys	//body	cw
	Close All Browsers

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Open System tab
	Open Date and Time tab

SUITE:AddServerInfo_Client
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="dateTimeLocalForm"]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[1]/label
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="server"]
	Clear Element Text	//*[@id="server"]
	Input Text	//*[@id="server"]	pool.ntp.org
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:AddServerInfo_server
	GUI::Basic::Open Nodegrid	${HOMEPAGEPEER}	${BROWSER}	${NGVERSION}
	GUI::Basic::login
	GUI::Open System tab
	Open Date and Time tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="dateTimeLocalForm"]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[1]/label
	Click Element	//*[@id="server"]
	Clear Element Text	//*[@id="server"]
	Input Text	//*[@id="server"]	${HOST}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:EnableNTP
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Click Element	css=#ntp_server > .title_b
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="serverEnabled"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Open Nodegrid	${HOMEPAGEPEER}	${BROWSER}	${NGVERSION}
	GUI::Basic::login
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	Open Date and Time tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="dateTimeLocalForm"]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[1]/label
	GUI::Basic::Spinner Should Be Invisible
	sleep	${DELAY}
	wait until Element is visible	//*[@id="server"]
	Click Element	//*[@id="server"]
	Clear Element Text	//*[@id="server"]
	Input Text	//*[@id="server"]	pool.ntp.org
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout and Close Nodegrid

GUI:Access::Generic Console Command Output
	[Arguments]	${COMMAND}	${CONTAINS_HELP}=no
	[Documentation]	Input the given command into the ttyd terminal and retrieve the output after prompt lines
	...	== REQUIREMENTS ==
	...	The console type needs to be one of those:
	...	-	CLI -> ends with "]#"
	...	-	Shell -> ends with "$"
	...	-	Root -> ends with "#"
	...	== ARGUMENTS ==
	...	-	COMMAND = Command to be executed
	...	-	CONTAINS_HELP = [yes/no] Console contains "[Enter '^Ec?' for help]" message
	${CHECK_TAB}=	Run Keyword And Return Status	Should Not Contain	${COMMAND}	TAB
	Run Keyword If	${CHECK_TAB}	Press Keys	//body	${COMMAND}	RETURN
	...	ELSE	Press Keys	//body	${COMMAND}
	Sleep	1

	Wait Until Keyword Succeeds	1m	1s	Execute JavaScript	term.selectAll();
	${CONSOLE_CONTENT}=	Wait Until Keyword Succeeds	1m	1s	Execute Javascript	return term.getSelection().trim();
	Should Not Contain	${CONSOLE_CONTENT}	[error.connection.failure] Could not establish a connection to device
	Run Keyword If	"${CONTAINS_HELP}" == "yes"	Should Contain	${CONSOLE_CONTENT}	[Enter '^Ec?' for help]
	${OUTPUT}=	Output From Last Command	${CONSOLE_CONTENT}
	[Return]	${OUTPUT}

Open Date and Time tab
	[Arguments]	${SCREENSHOT}=${FALSE}
	[Documentation]	Opens the NodeGrid Manager System::Date and Time Tab, it expects the System tab to be open
	...	== REQUIREMENTS ==
	...	System tab is open
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Date and Time Tab is open and all elements are accessable
	[Tags]	GUI	SYSTEM	LICENSE
	${ret}=	GUI::Basic::Get NodeGrid System
	Run Keyword If	'${ret}' == 'Nodegrid Services Router'	Set Suite Variable	${TAB_NUM}	4
	...	ELSE 	Set Suite Variable	${TAB_NUM}	3
	Wait Until Element Is Visible	jquery=body > div.main_menu > div > ul > li:nth-child(${TAB_NUM})
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(${TAB_NUM})
	Run Keyword If	${SCREENSHOT} != ${FALSE}	Capture Page Screenshot	filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png