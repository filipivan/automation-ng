*** Settings ***
Documentation	Editing System > Date settings testing for NG4.1
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test Radio Buttons and Current Time Are Present
	Page Should Contain Element	//input[@value='auto']
	Page Should Contain Element	//input[@value='manual']
	Page Should Contain Element	jquery=#currentTimeFormatted

Test Items Are Properly Displayed
	${TOP}=	SUITE:Is Top Button Checked
	run keyword if	${TOP}==${TRUE}	Element should not be visible	jquery=#timepicker
	...	ELSE	Element should be visible	jquery=#ntpUpdate
	Click Element	//input[@value='auto']
	Click Element	//input[@value='manual']
	${BOT}=	SUITE:Is Bottom Button Checked
	run keyword if	${BOT}==${TRUE}	Element should not be visible	jquery=#ntpUpdate
	...	ELSE	Element should be visible	jquery=#timepicker
	Click Element	//input[@value='auto']
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

Test invalid input
	#pool.ntp.org
	Wait Until Element Is Visible	jquery=#saveButton
	Input Text	xpath=//input[@id='server']	${EMPTY}
	Input Text	xpath=//input[@id='server']	...../////...//.././.
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	Execute Javascript	window.scrollTo(0,document.body.scrollHeight);
	Wait Until Element Is Visible	jquery=#saveButton
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Validation error.
	Input Text	xpath=//input[@id='server']	${EMPTY}
	Input Text	xpath=//input[@id='server']	pool.ntp.org
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

Test available value for field zone
	@{TYPES}=	Get List Items	jquery=#timezone_list
	@{MUST_HAVE_TYPES}=	Create List
	Append to List	${MUST_HAVE_TYPES}	GMT	UTC+12:00	UTC+11:00	UTC+10:00 (Sydney)	UTC+9:00 (Seoul, Tokyo)
	...	UTC+8:00 (Beijing, Singapore)	UTC+7:00 (Bangkok, Jakarta)	UTC+6:00	UTC+5:30 (India)	UTC+5:00 (Karachi, Tashkent)
	...	UTC+4:00 (Dubai, Moscow)	UTC+3:00 (Baghdad, Riyadh)	UTC+2:00 (Cairo, Jerusalem)	UTC+1:00 (Berlin, Paris)

	Run Keyword If	'${NGVERSION}'<='4.2'  Append To List    ${MUST_HAVE_TYPES}  UTC (Dublin, London)
	Run Keyword If	'${NGVERSION}'>='5.0'  Append To List    ${MUST_HAVE_TYPES}  GMT0

	Append To List  ${MUST_HAVE_TYPES}  UTC-1:00  UTC-2:00	UTC-3:00 (Buenos Aires, Sao Paulo)	UTC-4:00 (Santiago, San Juan)	UTC-5:00 (New York, Toronto)
	...	UTC-6:00 (Chicago, Mexico City)	UTC-7:00 (Calgary, Denver)	UTC-8:00 (Los Angeles, Vancouver)
	...	UTC-9:00 (Anchorage)	UTC-10:00 (Honolulu)	UTC-11:00  
	...	UTC-12:00 (International Date Line)	US/Eastern	US/Central	US/Mountain	US/Pacific
	...	US/Alaska	US/Hawaii	UTC	Africa/Nairobi	Africa/Lagos	America/Buenos Aires	America/Sao Paulo	America/Mexico City
	...	America/Vancouver	America/Edmonton	America/Regina	America/Toronto	America/Halifax	Asia/Tokyo 	Asia/Shanghai
	...	Asia/Hong Kong	Asia/Seoul	Asia/Singapore	Asia/Taipei	Asia/Dubai	Australia/Perth	Australia/Eucla
	...	Australia/Darwin 	Australia/Adelaide 	Australia/Brisbane	Australia/Sydney	Australia/Lord_Howe  Europe/Berlin	Europe/London
	...	Europe/Lisbon 	Europe/Paris 	Europe/Sofia 	Europe/Moscow

	Lists Should Be Equal	${TYPES}	${MUST_HAVE_TYPES}

Test Manual Time Setting
    Sleep    100s
	${BOT}=	SUITE:Is Bottom Button Checked
	run keyword if	${BOT}==${FALSE}	Click Element	//input[@value='manual']
	#Getting and Setting the Day
	Click Element	//a[@class='ui-datepicker-prev ui-corner-all']
	Click Element	//td[@class=' ui-datepicker-week-end ']
	#Resetting the cursor
	wait until element is visible	xpath=(//input[@id='dateTimeOption'])[2]
	Click Element	xpath=(//input[@id='dateTimeOption'])[2]
	#Setting the H/M/S
	Execute Javascript	window.scrollTo(0,document.body.scrollHeight);
	Wait Until Page Contains Element	xpath=//div[@id='timepicker']/div/div[2]/dl/dd[2]/div/span
	Click Element At Coordinates	xpath=//div[@id='timepicker']/div/div[2]/dl/dd[2]/div/span	-30	0
	Wait Until Page Contains Element	xpath=//div[@id='timepicker']/div/div[2]/dl/dd[3]/div/span
	Click Element At Coordinates	xpath=//div[@id='timepicker']/div/div[2]/dl/dd[3]/div/span	20	0
	Wait Until Page Contains Element	xpath=//div[@id='timepicker']/div/div[2]/dl/dd[4]/div/span
	Click Element At Coordinates	xpath=//div[@id='timepicker']/div/div[2]/dl/dd[4]/div/span	-45	0
	Click Element	jquery=#saveButton

Checking Time Change
	${DAY}=	Get Text	xpath=//div[@id='datepicker']/div/table/tbody/tr[2]/td[3]/a
	${MONTH_TXT}=	Get Text	//span[@class='ui-datepicker-month']
	${MONTH_LST}=	Create List	January	February	March	April	May	June	July	August	 September	October	November	December
	${MONTH_NUMBER}=	Get index from list	${MONTH_LST}	${MONTH_TXT}
	${MONTH_NUMBER}=	Evaluate	${MONTH_NUMBER} + 1
	${REAL_TIME}=	Get Time	year month day hour minute second
	should not be true	 '${MONTH_NUMBER}' == '${REAL_TIME}[1]'
	should not be true	'${DAY}' == '${REAL_TIME}[2]'

Test Case to Set Default Config
	Execute Javascript	window.scrollTo(0,document.body.scrollHeight);
	sleep	3s
	wait until element is visible	xpath=//input[@id='dateTimeOption']
	Run Keyword And Continue on Failure	Click Element	xpath=//input[@id='dateTimeOption']
	Run Keyword And Continue on Failure	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Date and Time::open tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	close all browsers

SUITE:Is Top Button Checked
	${TOP_BTN}=	run keyword and return status	Element Should Not Be Visible	jquery=#datepicker
	[return]	${TOP_BTN}

SUITE:Is Bottom Button Checked
	${BOT_BTN}=	run keyword and return status	Element Should Not Be Visible	jquery=#ntpUpdate
	[return]	${BOT_BTN}
