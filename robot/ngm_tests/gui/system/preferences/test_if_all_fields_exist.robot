*** Settings ***
Documentation	Editing System > Preferences Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

*** Test Cases ***
Access System::Preferences tab
	GUI::Basic::System::Preferences::Open Tab
	GUI::Basic::Spinner Should Be Invisible

Verify if all fields exist
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#LogoOptions	jquery=#loginBannerStatus
	Execute Javascript    window.document.evaluate("//*[@id='LogoOptions']", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
 	Select Checkbox	jquery=#LogoOptions
 	Select Checkbox	jquery=#loginBannerStatus
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#applytypeselection
	Select Radio Button	applytypeselection	applylogo-localcomputer
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#loginBannerMessage	jquery=#addr_location	jquery=#coordinates	jquery=#help_url	jquery=#idletimeout	jquery=#applytypeselection	jquery=#utilRateLicense	jquery=#utilRateTrigger	jquery=#netboot_ipaddr	jquery=#netboot_netmask	jquery=#netboot_interf	jquery=#netboot_url	jquery=#file_form	jquery=#getcoordinates_btn
	Select Radio Button	applytypeselection	applylogo-remote
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#url	jquery=#userName	jquery=#password

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
