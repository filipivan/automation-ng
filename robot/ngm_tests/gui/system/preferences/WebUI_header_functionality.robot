*** Settings ***
Documentation	Automated Test for WebUI-Header
Metadata		Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${DELAY}	5s
${header_colour}	e71818
${hostname}	NSR1

*** Test Cases ***
Test case to provide hostname
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="hostname"]
	Clear Element Text	//*[@id="hostname"]
	Input Text	//*[@id="hostname"]	${hostname}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Preferences::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="display_name"]
	Click Element	//*[@id="display_color"]
	Input Text	id=display_color	${header_colour}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${hostname}
	sleep	${DELAY}
	Close Browser
	SUITE:Setup
	Select Checkbox	 //*[@id="display_name"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Setup
	Page should not Contain Element	//*[@id="displayNameText"]
	sleep	${DELAY}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Preferences::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="hostname"]
	Clear Element Text	//*[@id="hostname"]
	Input Text	//*[@id="hostname"]	nodegrid
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Preferences::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${SELECTED}=	Run Keyword And return Status	Checkbox Should Be Selected	  //*[@id="display_name"]
	Run Keyword If	${SELECTED} == True	Unselect Checkbox	//*[@id="display_name"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

