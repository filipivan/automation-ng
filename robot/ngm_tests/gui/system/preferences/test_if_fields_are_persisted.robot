*** Settings ***
Documentation	Editing System > Preferences Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}

*** Test Cases ***
Access System::Preferences tab
    GUI::Basic::System::Preferences::Open Tab
	GUI::Basic::Spinner Should Be Invisible

Test if fields are persisted
	GUI::Basic::Input Text	//*[@id='addr_location']	an_address
	GUI::Basic::Input Text	//*[@id='coordinates']	50,50
	GUI::Basic::Input Text	//*[@id='help_url']	http://www.URL.com/ng/v3_0/NodeGrid-UserGuide-v3_0.pdf
	GUI::Basic::Input Text	//*[@id='idletimeout']	99999
	GUI::Basic::Select Checkbox	//*[@id='loginBannerStatus']
	GUI::Basic::Wait Until Element Is Accessible	jquery=#loginBannerMessage
	GUI::Basic::Input Text	//*[@id='loginBannerMessage']	A_message
	GUI::Basic::Unselect Checkbox	//*[@id='utilRateLicense']
	GUI::Basic::Input Text	//*[@id='utilRateTrigger']	99
	GUI::Basic::Input Text	//*[@id='netboot_ipaddr']	255.255.255.255
	GUI::Basic::Input Text	//*[@id='netboot_netmask']	255.255.255.255
	GUI::Basic::Input Text	//*[@id='netboot_url']	http://IPAddress/PATH/FILENAME.ISO
	GUI::Basic::Click Element	//*[@id='saveButton']
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Preferences::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Wait Until Element Is Accessible	jquery=input#addr_location
	${ADDR_LOCATION}=	Get Value	jquery=input#addr_location
	Should Be Equal	${ADDR_LOCATION}	an_address
	${COORDINATES}=	Get Value	jquery=#coordinates
	Should Be Equal	${COORDINATES}	50,50
	${HELP_URL}=	Get Value	jquery=#help_url
	Should Be Equal	${HELP_URL}	http://www.URL.com/ng/v3_0/NodeGrid-UserGuide-v3_0.pdf
	${IDLETIMEOUT}=	Get Value	jquery=#idletimeout
	Should Be Equal	${IDLETIMEOUT}	99999
	Checkbox Should Be Selected	jquery=#loginBannerStatus
	${LOGINBANNERMESSAGE}=	Get Value	jquery=#loginBannerMessage
	Checkbox Should Not Be Selected	jquery=#utilRateLicense
	${UTILRATETRIGGER}=	Get Value	jquery=#utilRateTrigger
	Should Be Equal	${UTILRATETRIGGER}	99
	${NETBOOT_IPADDR}=	Get Value	jquery=#netboot_ipaddr
	Should Be Equal	${NETBOOT_IPADDR}	255.255.255.255
	${NETBOOT_NETMASK}=	Get Value	jquery=#netboot_netmask
	Should Be Equal	${NETBOOT_NETMASK}	255.255.255.255
	${NETBOOT_URL}=	Get Value	jquery=#netboot_url
	Should Be Equal	${NETBOOT_URL}	http://IPAddress/PATH/FILENAME.ISO

Test Set default values
	GUI::Basic::System::Preferences::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text	//*[@id='addr_location']	${EMPTY}
	GUI::Basic::Input Text	//*[@id='coordinates']	${EMPTY}
	GUI::Basic::Input Text	//*[@id='help_url']	https://www.zpesystems.com/ng/v4_0/NodegridManual4.0.html
	GUI::Basic::Input Text	//*[@id='idletimeout']	300
	Select Radio Button	applytypeselection	applylogo-restore
	GUI::Basic::Unselect Checkbox	//*[@id='loginBannerStatus']
	GUI::Basic::Select Checkbox	//*[@id='utilRateLicense']
	GUI::Basic::Input Text	//*[@id='utilRateTrigger']	90
	GUI::Basic::Input Text	//*[@id='netboot_ipaddr']	192.168.160.1
	GUI::Basic::Input Text	//*[@id='netboot_netmask']	255.255.255.0
	GUI::Basic::Input Text	//*[@id='netboot_url']	http://ServerIPAddress/PATH/FILENAME.ISO
	GUI::Basic::Click Element	//*[@id='saveButton']
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
