*** Settings ***
Documentation	Editing System > Preferences Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	NON-CRITICAL	NEED-REVIEW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}

*** Test Cases ***

Test coordinates field validation
	GUI::Basic::Spinner Should Be Invisible
	${RAND}=	Evaluate	random.randint(0,180)	modules=random
	GUI::Basic::Auto Input Tests	coordinates	asd	-91,180	91,180	90,-181	90,181	90,${RAND}
	Input Text	query=#coordinates	${EMPTY}
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

Test Help Location Validation
	GUI::Basic::Auto Input Tests	help_url	${EMPTY}	url
	Input Text	jquery=#help_url	https://www.zpesystems.com/ng/v4_0/NodegridManual4.0.html
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Test logo server url validation
	GUI::Basic::Wait Until Element Is Accessible	jquery=#LogoOptions
	GUI::Basic::Select Checkbox	//*[@id='LogoOptions']

	GUI::Basic::Wait Until Element Is Accessible	jquery=#applytypeselection
	Select Radio Button	applytypeselection	applylogo-remote

	GUI::Basic::Wait Until Element Is Accessible	jquery=#url
	GUI::Basic::Input Text	//*[@id='url']	)!*¨%)!$%[[´]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Wait Until Element Contains	jquery=#errormsg	Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>
	GUI::Basic::Wait Until Element Is Accessible	jquery=#applytypeselection
	Select Radio Button	applytypeselection	applylogo-remote
	GUI::Basic::Input Text	//*[@id='url']	http://%IP/invalidpath
	GUI::Basic::Input Text	//*[@id='userName']	user
	GUI::Basic::Input Text	//*[@id='password']	password
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=#globalerr	Error on page. Please check.
	Run Keyword If	'${NGVERSION}'>'4.2'	Wait Until Element Contains	jquery=#errormsg	Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>
	Run Keyword If	'${NGVERSION}'=='4.2'	Wait Until Element Contains	jquery=#errormsg	Invalid remote file name
	GUI::Basic::Guarantees The Field Is Optically Visible	//*[@value='applylogo-restore']
	Select Radio Button	applytypeselection	applylogo-restore
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Does Not Contain	jquery=#globalerr	Error on page. Please check.

Test utilRateTrigger field validation
	GUI::Basic::Auto Input Tests	utilRateTrigger	asd	-1	100	77
	Input Text	query=#utilRateTrigger	90
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

Test idletimeout field validation
	GUI::Basic::Auto Input Tests	idletimeout	asd	-1	100000	301
	Input Text	query=#idletimeout	300
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

#unit ipv4	192.168.160.1
#unit netmask	255.255.255.0
#iso url	http://ServerIPAddress/PATH/FILENAME.ISO
Test netboot_ipaddr field validation
	GUI::Basic::Auto Input Tests	netboot_ipaddr	asd	255.255.255.256	::2c	255.255.255.123
	GUI::Basic::Auto Input Tests	netboot_netmask	asd	255.255.255.256	::2c	255.255.0.0
	Input Text	query=#netboot_ipaddr	192.168.160.1
	Input Text	query=#netboot_netmask	255.255.255.0
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Preferences::Open Tab
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
