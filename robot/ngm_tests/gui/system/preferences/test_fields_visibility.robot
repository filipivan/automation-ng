*** Settings ***
Documentation	Editing System > Preferences Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

*** Test Cases ***
Access System::Preferences tab
	GUI::Basic::System::Preferences::Open Tab
	GUI::Basic::Spinner Should Be Invisible

Verify if Logo Image radio buttons are visible when "Logo Image selection" checkbox is selected
	GUI::Basic::Unselect Checkbox	//*[@id='LogoOptions']
	Sleep	1
	GUI::Basic::Elements Should Not Be Visible	jquery=#applytypeselection
	Select Checkbox	//*[@id='LogoOptions']
	GUI::Basic::Wait Until Element Is Accessible	jquery=#applytypeselection

Verify if Banner Text Area is visible when "Enable Banner Message" checkbox is selected
	GUI::Basic::Wait Until Element Is Accessible	jquery=#loginBannerStatus
	Unselect Checkbox	jquery=#loginBannerStatus
	Sleep	1
	GUI::Basic::Elements Should Not Be Visible	jquery=#loginBannerMessage
	Select Checkbox	jquery=#loginBannerStatus
	GUI::Basic::Wait Until Element Is Accessible	jquery=#loginBannerMessage

Verify if Choose File button is visible when "Update logo image from local computer" selected
	Select Radio Button	applytypeselection	applylogo-localcomputer
	GUI::Basic::Wait Until Element Is Accessible	jquery=#file_form
	GUI::Basic::Elements Should Not Be Visible	jquery=#url	jquery=#userName	jquery=#password

Verify if "URL", "Username", "Password" fields are visible when "Remote Server" selected
	Select Radio Button	applytypeselection	applylogo-remote
	Sleep	1
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#url	jquery=#userName	jquery=#password
	GUI::Basic::Elements Should Not Be Visible	jquery=#file_form

Verify if "URL", "Username", "Password" and "Choose File" button are hidden when "Use default logo image" selected
	Select Radio Button	applytypeselection	applylogo-restore
	Sleep	1
	GUI::Basic::Elements Should Not Be Visible	jquery=#url	jquery=#userName	jquery=#password	jquery=#file_form

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Close All Browsers
