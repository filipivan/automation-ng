*** Settings ***
Documentation	Editing System > Preferences Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-3	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

*** Test Cases ***
Access System::Preferences tab
	GUI::Basic::System::Preferences::Open Tab
	GUI::Basic::Spinner Should Be Invisible

Test If screen blocks on logo image not selected
	GUI::Basic::Select Checkbox	//*[@id='LogoOptions']
	GUI::Basic::Wait Until Element Is Accessible	jquery=#applytypeselection
	Select Radio Button	applytypeselection	applylogo-localcomputer
	GUI::Basic::Save	True
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
