*** Settings ***
Documentation	Testing LTE uptime limit script
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX   NON-CRITICAL  BUG_NG_10089
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_0

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CELLULAR_CONNECTION}	${GSM_CELLULAR_CONNECTION}
${TYPE}	${GSM_TYPE_GUI}
${STRONG_PASSWORD}	${QA_PASSWORD}
${SCRIPT_NAME}	GSMUptimeLimit_Event143_sample.sh
${FTP}	${FTPSERVER2_URL}
${FOLDER}	/files/
${SCRIPT_NAME}	GSMUptimeLimit_Event143_sample.sh

*** Test Cases ***
Test case to check GSM Uptime limit
	SUITE:Add Script to event 143
	SUITE:Add GSM Connection
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox     xpath=//tr[@id='${CELLULAR_CONNECTION}']/td/input
	Click Element	//*[@id="nonAccessControls"]/input[3]
	Wait Until Keyword Succeeds		3x	15s		SUITE:Check if GSM Connection is Up
	Sleep	30s
	Wait Until Keyword Succeeds		3x	15s		SUITE:Check if GSM Connection is Down

Test case to check GSM connection goes down after 30s
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox     xpath=//tr[@id='${CELLULAR_CONNECTION}']/td/input
	Click Element	//*[@id="nonAccessControls"]/input[3]
	Wait Until Keyword Succeeds		3x	15s		SUITE:Check if GSM Connection is Up
	Sleep	30s
	Wait Until Keyword Succeeds	3x	15s	SUITE:Check if GSM Connection is Down

Test case to delete the GSM connection and remove the uptime limit script
	SUITE:Delete GSM Connection
	SUITE:Remove Action Script For Event 143

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	${HAS_GSM}	GUI::Tracking::Check If Has GSM Module and Sim Card with Signal
	Set Suite Variable	${HAS_GSM}
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	#	CLI: Change password
	CLI:Open
	SUITE:Change passwords of admin and root to strong passwords
	SUITE:GET GSM uptime control script
	CLI:Close Connection
	#SUITE:Remove lte uptime limit Script File
	SUITE:Get GSM operator name and interface information

SUITE:Check if has wireless modem support
	${HAS_WMODEM_SUPPORT}=	CLI:Has Wireless Modem Support
	Set Suite Variable	${HAS_WMODEM_SUPPORT}
	Log	Has Wireless Modem Support: ${HAS_WMODEM_SUPPORT}	INFO	console=yes

SUITE:Teardown
	Run Keyword If	not ${HAS_GSM}	Close All Browsers
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
#	GUI: Teardown
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid	PASSWORD=${STRONG_PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout and Close Nodegrid
#	CLI: Teardown
	CLI:Open	PASSWORD=${STRONG_PASSWORD}
	SUITE:Change passwords of admin and root to default passwords
	CLI:Close Connection

SUITE:Delete GSM Connection
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox     xpath=//tr[@id='${CELLULAR_CONNECTION}']/td/input
	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

SUITE:Remove Action Script For Event 143
	GUI::Auditing::Events::Open Event List tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element   xpath=//a[contains(text(),'143')]
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label	xpath=//select[@id='action_script']		${EMPTY}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	CLI:Connect As Root    PASSWORD=${STRONG_PASSWORD}
	CLI:Enter Path  /etc/scripts/auditing
	CLI:Write	rm ${SCRIPT_NAME}

SUITE:GET GSM uptime control script
	CLI:Connect As Root    PASSWORD=${STRONG_PASSWORD}
	CLI:Enter Path  /etc/scripts/auditing
	${OUTPUT}=  CLI:Write  wget --password=${FTPSERVER2_PASSWORD} ftp://${FTPSERVER2_USER}@${FTPSERVER2_IP}${FOLDER}${SCRIPT_NAME}
	Should Not Contain	${OUTPUT}	No such file
	CLI:Write	chmod +x ${SCRIPT_NAME}

SUITE:Add Script to event 143
	GUI::Auditing::Events::Open Event List tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element   xpath=//a[contains(text(),'143')]
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Label   id=action_script   ${SCRIPT_NAME}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Wget Script
	CLI:Enter Path	/etc/scripts/auditing
	${OUTPUT}	CLI:Write	wget ${FTP}${FOLDER}${SCRIPT_NAME} --password=${FTPSERVER2_PASSWORD} --user=${FTPSERVER2_USER}
	Should Not Contain	${OUTPUT}	No such file
	CLI:Write	chmod +x ${SCRIPT_NAME}
	CLI:Switch Connection	default

SUITE:Get GSM operator name and interface information
	Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
	${INDEX_NUMBER}	GUI::Tracking::Get Index Number With 5G Sim Card Up Otherwise 4G Up
	${ROW_NUMBER}	Evaluate	${INDEX_NUMBER} + 1
	${GSM_CDC_WDMX}	GUI::Tracking::Get Tracking Wireless Modem Table Cell	ROW=${ROW_NUMBER}	COLUMN=2
	Set Suite Variable	${GSM_CDC_WDMX}
	${GSM_OPERATOR_NAME}	GUI::Tracking::Get Tracking Wireless Modem Table Cell	ROW=${ROW_NUMBER}	COLUMN=7
	Set Suite Variable	${GSM_OPERATOR_NAME}
	${ROOT_GSM_INTERFACE}	GUI::Tracking::Rewrite to root GSM interface format	${GSM_CDC_WDMX}
	Set Suite Variable	${ROOT_GSM_INTERFACE}

SUITE:Add GSM Connection
	GUI::Basic::Network::Connections::Open Tab
	GUI::Basic::Button::Add
	Click Element	id=connName
	Input Text	//*[@id="connName"]	${CELLULAR_CONNECTION}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=connType
	Select From List By Label	//*[@id="connType"]	${TYPE}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=connItfName
	Select From List By Label	//*[@id="connItfName"]	${GSM_CDC_WDMX}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=mobileAPN
	Input Text	//*[@id="mobileAPN"]	${GSM_WMODEM_CARRIERS_APN["${GSM_OPERATOR_NAME}"]}
	Log	\nGSM_WMODEM_CARRIERS_APN.keys(${GSM_OPERATOR_NAME}): \n${GSM_WMODEM_CARRIERS_APN["${GSM_OPERATOR_NAME}"]}	INFO	console=yes
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save

SUITE:Check if GSM Connection is Up
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	${CELLULAR_CONNECTION}\\s+\\w+\\s+${TYPE}\\s+${GSM_CDC_WDMX}\\s+Up

SUITE:Check if GSM Connection is Down
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::Open Tab
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections: \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	${CELLULAR_CONNECTION}\\s+\\w+\\s+${TYPE}\\s+${GSM_CDC_WDMX}\\s+Down

SUITE:Change passwords of admin and root to strong passwords
	CLI:Change User Password From Shell	root	${STRONG_PASSWORD}	${ROOT_PASSWORD}
	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${STRONG_PASSWORD}	${STRONG_PASSWORD}
	Log	Changed the passwords of admin and root to: ${STRONG_PASSWORD}	INFO	console=yes

SUITE:Change passwords of admin and root to default passwords
	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}	${STRONG_PASSWORD}
	CLI:Change User Password From Shell	root	${ROOT_PASSWORD}	${STRONG_PASSWORD}
	Log	The passwords of admin and root users came back to default: ${DEFAULT_PASSWORD} and ${ROOT_PASSWORD}	INFO	console=yes