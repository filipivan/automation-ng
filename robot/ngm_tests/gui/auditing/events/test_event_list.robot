*** Settings ***
Documentation	Testing Auditing:Events:Events Lists enable/disable logs.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX
Default Tags	EXCLUDEIN4_2	EXCLUDEIN5_0

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE}	test-event-list-device

*** Test Cases ***
Test Disable Event 100
	GUI::Auditing::Events::Open Event List tab
	Select Checkbox	//*[@id="100"]/td[1]/input
	Click Element	jquery=#disable
	GUI::Basic::Spinner Should Be Invisible

Test Enable Event 100
	GUI::Auditing::Events::Open Event List tab
	Select Checkbox	//*[@id="100"]/td[1]/input
	Click Element	jquery=#enable
	GUI::Basic::Spinner Should Be Invisible

Test Disable Event 108 and 302 and check logging
	[Tags]	NON-CRITICAL	NEED-REVIEW
	GUI::Auditing::Events::Open Event List tab
	Select Checkbox	//*[@id="108"]/td[1]/input
	Select Checkbox	//*[@id="302"]/td[1]/input
	Click Element	jquery=#disable
	GUI::Basic::Spinner Should Be Invisible

	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	GUI::ManagedDevices::Add Device	${DEVICE}
	GUI::ManagedDevices::Delete All Devices

	CLI:Switch Connection	root_session
	CLI:Test Event ID Not Logged	${HOSTNAME_NODEGRID}	302

Test Enable Event 108 and 302 and check logging
	[Tags]	NON-CRITICAL	NEED-REVIEW
	CLI:Switch Connection	default
	GUI::Auditing::Events::Open Event List tab
	Select Checkbox	//*[@id="108"]/td[1]/input
	Select Checkbox	//*[@id="302"]/td[1]/input
	Click Element	jquery=#enable
	GUI::Basic::Spinner Should Be Invisible

	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	GUI::ManagedDevices::Add Device	${DEVICE}
	GUI::ManagedDevices::Delete All Devices

	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	1m	1s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	302
	...	Device created. User: admin. Device: ${DEVICE}.

Test trigger Event with action script configured
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Enable Action Script For Events
	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	GUI::ManagedDevices::Add Device	${DEVICE}
	GUI::ManagedDevices::Delete All Devices
	SUITE:Script Should Have Been Run

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	SUITE:Disable Action Script For Events

	CLI:Open
	CLI:Connect As Root
	SUITE:Remove Script Log File

SUITE:Teardown
	CLI:Close Connection
	SUITE:Disable Action Script For Events
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout
	Close all Browsers

SUITE:Remove Script Log File
	CLI:Switch Connection	default
	CLI:Write	shell sudo rm /tmp/action_script_sample.log

SUITE:Script Should Have Been Run
	CLI:Switch Connection	default
	${OUTPUT}=	CLI:Write	shell cat /tmp/action_script_sample.log
	Should Contain	${OUTPUT}	Event Number: 302
	Should Contain	${OUTPUT}	Event Number: 303

SUITE:Disable Action Script For Events
	GUI::Auditing::Events::Open Event List tab
	Select Checkbox	//*[@id="302"]/td[1]/input
	Select Checkbox	//*[@id="303"]/td[1]/input
	Click Element	//*[@id="nonAccessControls"]/input[4]	# Edit button
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	enable
	Select From List By Value	action_script	${EMPTY}
	GUI::Basic::Save If Configuration Changed

SUITE:Enable Action Script For Events
	GUI::Auditing::Events::Open Event List tab
	Select Checkbox	//*[@id="302"]/td[1]/input
	Select Checkbox	//*[@id="303"]/td[1]/input
	Click Element	//*[@id="nonAccessControls"]/input[4]	# Edit button
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	enable
	Select From List By Value	action_script	ActionScript_sample.sh
	GUI::Basic::Save