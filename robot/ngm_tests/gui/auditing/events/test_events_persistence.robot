*** Settings ***
Documentation	Testing Auditing:Events table item persistance
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test Email
    SUITE:Table Mod and Check     email

Test File
    SUITE:Table Mod and Check     file

Test SNMP Trap
    SUITE:Table Mod and Check     snmptrap

Test Syslog
    SUITE:Table Mod and Check     syslog

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Auditing::Events::Open Tab
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Logout
    Close all Browsers

SUITE:Table Mod and Check
    [Arguments]  ${ID}
    [Documentation]     ID just wants the id of the row
    ${START_POS}=   Get Text    //*[@id='${ID}']/td[2]
    @{BEGG}=    Run Keyword If  '${START_POS}' == '-'   Create List     -   -   -   -
    ...         ELSE    Create List     Yes     Yes     Yes     Yes
    SUITE:Table Mod  ${ID}
    @{MODIFIED}=    SUITE:Table Check     ${ID}
    Should Not be Equal     ${MODIFIED}     ${BEGG}
    SUITE:Table Mod  ${ID}
    @{MODIFIED}=    SUITE:Table Check     ${ID}
    Should be Equal         ${MODIFIED}     ${BEGG}

SUITE:Table Mod
    [Arguments]  ${ID}
    [Documentation]     ID just wants the id
    Wait until Element is Visible   jquery=#${ID}
    Wait until element is enabled   jquery=#${ID}
    Click Element   //*[@id='${ID}']/td[1]/a
    GUI::Basic::Spinner Should Be Invisible
    FOR    ${C}    IN RANGE    1   5
		Click Element       //input[@id='cat${C}']
    END
    Click Element   jquery=#saveButton

SUITE:Table Check
    [Arguments]  ${ID}
    [Documentation]     ID always wants the id
    @{OUT_VALS}=    Create List
    GUI::Basic::Spinner Should Be Invisible
    FOR    ${C}    IN RANGE    2   6
		${VAL}=     Get Text    //tr[@id='${ID}']/td[${C}]
		Append to List      ${OUT_VALS}     ${VAL}
    END
    [Return]  @{OUT_VALS}