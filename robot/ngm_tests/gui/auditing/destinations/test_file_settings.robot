*** Settings ***
Documentation	Testing Auditing:Destinations:File Subtab
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test Local Selection
    Select From List By Value   //*[@id="filetype"]     local
    Element Should Be Visible   //*[@id="local_size"]

Test File Size
    GUI::Basic::Auto Input Tests        local_size          2049    9999     -1    1000

Test Number of Archives
    GUI::Basic::Auto Input Tests        local_segments      0   a   3

Test Archive by Time
    GUI::Basic::Auto Input Tests        local_close_time    aaaaa    25:01     15:30

Reset Text Local
    Input Text  //*[@id='local_size']   256
    Input Text  //*[@id="local_segments"]   1
    Input Text  //*[@id="local_close_time"]     ${EMPTY}
    Click element   jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible

Test NFS Selection
    Select From List By Value   //*[@id="filetype"]     nfs
    Element Should Be Visible   //*[@id="nfs_server"]

Test Server IP
    GUI::Basic::Auto Input Tests        nfs_server          .//./##    127.0.0.1

Test NFS File Size
    GUI::Basic::Auto Input Tests        nfs_size            big     -1    1000

Test Number of NFS Archives
    GUI::Basic::Auto Input Tests        nfs_segments        a    -1    3

Test NFS Archive by Time
    GUI::Basic::Auto Input Tests        nfs_close_time      aaaaa    25:01     15:30

Reset Text NFS
    Input Text  //*[@id="nfs_server"]   ${EMPTY}
    Input Text  //*[@id='nfs_size']   1024
    Input Text      //*[@id="nfs_segments"]     1
    Input Text  //*[@id="nfs_close_time"]   ${EMPTY}
    Click element   jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI::Auditing::Open File Tab
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    Select From List By Value   //*[@id="filetype"]     local
    Element Should Be Visible   //*[@id="local_size"]
    GUI::Basic::Spinner Should Be Invisible
    Click Element  jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Logout
    Close all Browsers