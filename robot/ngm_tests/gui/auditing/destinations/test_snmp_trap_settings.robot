*** Settings ***
Documentation	Testing Auditing:Destinations:SNMP Trap Subtab
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test Trap Server
    ${RND}=     Evaluate    random.randint(1,256)   modules=random
    @{TESTS_1}=   Create List     1..245.2.1  .   12.53.12.53/2   12/25\\12#24     12.12.12.${RND}
    GUI::Basic::Auto Input Tests     trapServer  @{TESTS_1}

Test Trap Protocol
    @{PROT}=        Get List Items      jquery=#trapProtocol
    @{CHECK}=       Create List         UDP-IPv4    TCP-IPv4    UDP-IPv6    TCP-IPv6
    Should Be Equal     ${PROT}     ${CHECK}

Test Trap Port
    ${RND}=     Evaluate    random.randint(1,256)   modules=random
    @{TESTS_2}=   Create List     12.4  .   aaaa     2\\24     24/1    ${RND}
    GUI::Basic::Auto Input Tests     trapPort  @{TESTS_2}

Test Version 2c
    Click Element   //input[@value='2c']
    Execute Javascript  document.getElementById("trapCommunity").scrollIntoView(true);
    Element Should Be Visible       //input[@id='trapCommunity']

Test Version 3 Display
    Click Element   //input[@value='3']
    @{CHECKS}=  Create List     trapSecName     trapSecLevel    trapAuthType    trapAuthPass    trapPrivType    trapPrivPass
    FOR    ${I}    IN  @{CHECKS}
           Execute Javascript  document.getElementById("${I}").scrollIntoView(true);
           Element Should Be Visible       //*[@id='${I}']
	END

Test Username
    @{TESTS_3}=   Create List     not/valid  a'm'i'   sec/nonam][   test#works   a\\s\\     this.valid
    GUI::Basic::Auto Input Tests     trapSecName  @{TESTS_3}

Test Trap Security
    @{PROT}=        Get List Items      jquery=#trapSecLevel
    @{CHECK}=       Create List         noAuthNoPriv    authNoPriv    authPriv
    Should Be Equal     ${PROT}     ${CHECK}

Test Auth and Priv Types
    @{PROT_A}=        Get List Items      jquery=#trapAuthType
    @{CHECK}=       Create List         MD5     SHA
    Should Be Equal     ${PROT_A}     ${CHECK}
    @{PROT_P}=        Get List Items      jquery=#trapPrivType
    @{CHECK}=       Create List         DES     AES
    Should Be Equal     ${PROT_P}     ${CHECK}

Test Auth Password
    @{TESTS_4}=   Create List     short     \#thisaintit    passwordYES
    GUI::Basic::Auto Input Tests     trapAuthPass  @{TESTS_4}

Test Priv Password
    @{TESTS_5}=   Create List     short     \#thisaintit    passwordYES
    GUI::Basic::Auto Input Tests     trapPrivPass  @{TESTS_5}

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI::Auditing::Open SNMPTrap Tab
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    Click element       //*[@value='2c']
    Click Element       jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Logout
    Close all Browsers