import imaplib
import ssl

# -------------------------------------------------
#
# Utility to search an email from Gmail Using Python
#
# ------------------------------------------------

IMAP_SERVER = "outlook.office365.com"
IMAP_PORT = 993
IMAP_EMAIL = "auto.qa@zpesystems.com"
IMAP_PWD = ".QA@automation!"

SEARCH_BOX = "autobuild"
SEARCH_EMAIL = "autobuild.zpesystems@gmail.com"
SEARCH_SUBJECT = "Nodegrid Notification Test"
SEARCH_CONTENT = "Email test from Nodegrid at"


def test_mail():
    try:
        tls_context = ssl.create_default_context()
        mail = imaplib.IMAP4(IMAP_SERVER, IMAP_PORT)
        mail.debug = 4
        mail.starttls(ssl_context=tls_context)
        mail.login(IMAP_EMAIL, IMAP_PWD)
        mail.select(SEARCH_BOX)

        type, data = mail.search(None,
                                 '(FROM "' + SEARCH_EMAIL + '" SUBJECT "' + SEARCH_SUBJECT + '" BODY "' + SEARCH_CONTENT + '")')

        if data[0] == '':
            raise Exception("Email not found")

        mail.store(data[0].replace(' ', ','), '+FLAGS', '\SEEN')

        print('Email found')

    except Exception as e:
        print('Error: ' + str(e))
    return


if __name__ == "__main__":
    test_mail()
