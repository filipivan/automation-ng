*** Settings ***
Documentation	Testing Auditing:Destinations:Email Subtab
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test nonAccess Controls
    Element Should Be Enabled       //*[@id="testemail"]
    Element Should Be Disabled      //*[@id="saveButton"]

Test "Test Email" Button
    Input Text      //*[@id="emailServer"]          ${EMPTY}
    Input Text      //*[@id="emailPort"]            25
    Input Text      //*[@id="emailUser"]            ${EMPTY}
    Input Text      //*[@id="emailPass"]            aaaaaa
    Input Text      //*[@id="emailPassAgain"]       aaaaaa
    Input Text      //*[@id="destinationEmail"]     @
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    Click Element   //*[@id="testemail"]
    Wait Until Element Is Not Visible     xpath=//*[@id='loading']    timeout=300s
    Element Should Contain      //*[@id="globalerr"]    ssmtp: Cannot open :

Test Email Server Name
    @{TESTS}=   Create List     wrong/      doesn\\twork    not''real   th[]no    still..no   this.works
    GUI::Auditing::Auto Input Tests     emailServer  @{TESTS}

Test Email Port
    @{TESTS_2}=   Create List     12.4  .   aaaa     2\\24     24/1    161
    GUI::Auditing::Auto Input Tests     emailPort  @{TESTS_2}

Test Passwords
    SUITE:Check Passwords  one        two                 emailPass   emailPassAgain  ${TRUE}
    SUITE:Check Passwords  .//./##    ../..//.[]././      emailPass   emailPassAgain  ${TRUE}
    SUITE:Check Passwords  itsnot     thesame             emailPass   emailPassAgain  ${TRUE}
    SUITE:Check Passwords  two        two                 emailPass   emailPassAgain  ${FALSE}

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI::Auditing::Open Destinations tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element    //*[@id="auditing_destinations_email"]/span
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
    Input Text  jquery=#emailServer     ${EMPTY}
    Input Text  jquery=#emailPort       25
    Input Text  jquery=#emailPass       aaaaaa
    Input Text  jquery=#emailPassAgain  aaaaaa
    Input Text      //*[@id="destinationEmail"]     ${EMPTY}
    Click Element   jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Logout
    Close All Browsers

SUITE:Check Passwords
    [Arguments]  ${P_ONE}   ${P_TWO}    ${LOCATION_ONE}     ${LOCATION_TWO}     ${EXPECTS}
    Input Text  ${LOCATION_ONE}     ${P_ONE}
    Input Text  ${LOCATION_TWO}     ${P_TWO}
    Click Element                   jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    Run Keyword if      ${EXPECTS}==${TRUE}     Page Should Contain Element         xpath=//p[@class="bg-danger text-danger"]
    Run Keyword if      ${EXPECTS}==${FALSE}    Page Should Not Contain Element     xpath=//p[@class="bg-danger text-danger"]
