*** Settings ***
Documentation	change sender for email notification
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX
Default Tags	AUDITING	DESTINATIONS

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}    ${DEFAULT_USERNAME}
${PASSWORD}    ${DEFAULT_PASSWORD}
${SENDER1}     QA_AUTOMATION1
${SENDER2}     QA_AUTOMATION2
${SUBJECT}     SUBJECT:Nodegrid Event Notification
${SUBJECT1}    Nodegrid Notification Test
${ERROR_MSG}        ssmtp: Authorization failed

*** Test Cases ***
Enable the categories of event logs that you want to receive in your e-mail
    GUI::Basic::Auditing::Events::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element    //*[@id="Email"]
    GUI::Basic::Spinner Should Be Invisible
    Select Checkbox    //*[@id="cat1"]
    Select Checkbox    //*[@id="cat2"]
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible

Add configuration and sender for email log/validate for wrong user
    SUITE:Setup
    GUI::Basic::Spinner Should Be Invisible
    Input Text    //*[@id="emailServer"]    ${EMAIL_SERVER}
    Input Text    //*[@id="emailPort"]   ${EMAIL_PORT}
    Input Text    //*[@id="emailUser"]    ${EMAIL_ACCOUNT}
    Input Text    //*[@id="emailPass"]    jamie1
    Input Text    //*[@id="emailPassAgain"]    jamie1
    Input Text    //*[@id="destinationEmail"]    ${EMAIL_DESTINATION}
    Input Text    //*[@id="senderEmail"]    ${SENDER1}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    Click Button    //*[@id="testemail"]
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain    ${ERROR_MSG}

Test Email with enabling system events and AAA events
    SUITE:ADD Configuration for email with sender1
    GUI::Basic::Spinner Should Be Invisible
    Click Button    //*[@id="testemail"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    CLI:Open	USERNAME=${EMAIL_ACCOUNT}	PASSWORD=${EMAIL_PASSWD}	session_alias=mail_server	TYPE=shell	HOST_DIFF=${EMAIL_SERVER}
    Wait Until Keyword Succeeds	4x	10s	SUITE:Check Email From Sender 1
    Close All Browsers

Test enable Logging events and check email notification
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Auditing::Events::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element    //*[@id="Email"]
    GUI::Basic::Spinner Should Be Invisible
    Checkbox Should Be Selected    id=cat1
    Checkbox Should Be Selected    id=cat2
    SUITE:ADD Configuration for email with sender2
    Click Button    //*[@id="testemail"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Open And Login Nodegrid
    GUI::Basic::Spinner Should Be Invisible
    CLI:Open    USERNAME=${EMAIL_ACCOUNT}	PASSWORD=${EMAIL_PASSWD}	session_alias=mail_server	TYPE=shell	HOST_DIFF=${EMAIL_SERVER}
	Wait Until Keyword Succeeds	4x	10s	SUITE:Check Email From Sender 2

*** Keywords ***
SUITE:Setup
    GUI::Basic::Open And Login Nodegrid
    GUI::Auditing::Open
    GUI::Basic::Spinner Should Be Invisible
    GUI::Auditing::Open Destinations tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element    //*[@id="auditing_destinations_email"]/span
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	SUITE:Remove Old Emails From Mailbox
    GUI::Basic::Spinner Should Be Invisible
    SUITE:Setup
    SUITE:Remove configuration added for email sender and disable logs
    GUI::Basic::Logout And Close Nodegrid

SUITE:ADD Configuration for email with sender1
    SUITE:Setup
    Input Text    //*[@id="emailServer"]    ${EMAIL_SERVER}
    Input Text    //*[@id="emailPort"]   ${EMAIL_PORT}
    Input Text    //*[@id="emailUser"]    ${EMAIL_ACCOUNT}
    Input Text    //*[@id="emailPass"]    ${EMAIL_PASSWD}
    Input Text    //*[@id="emailPassAgain"]    ${EMAIL_PASSWD}
    Input Text    //*[@id="destinationEmail"]    ${EMAIL_DESTINATION}
    Input Text    //*[@id="senderEmail"]    ${SENDER1}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible

SUITE:ADD Configuration for email with sender2
    SUITE:Setup
    Input Text    //*[@id="emailServer"]    ${EMAIL_SERVER}
    Input Text    //*[@id="emailPort"]   ${EMAIL_PORT}
    Input Text    //*[@id="emailUser"]    ${EMAIL_ACCOUNT}
    Input Text    //*[@id="emailPass"]    ${EMAIL_PASSWD}
    Input Text    //*[@id="emailPassAgain"]    ${EMAIL_PASSWD}
    Input Text    //*[@id="destinationEmail"]    ${EMAIL_DESTINATION}
    Input Text    //*[@id="senderEmail"]    ${SENDER2}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible

SUITE:Check Email From Sender 1
    Write	echo 1 | mail
    ${OUTPUT}=	CLI:Read Until Prompt
    Should Not Contain	${OUTPUT}	No mail for ${EMAIL_ACCOUNT}
    Should Contain  ${OUTPUT}     ${SENDER1}

SUITE:Check Email From Sender 2
    Write	echo 2 | mail
    ${OUTPUT}=	CLI:Read Until Prompt
    Should Not Contain	${OUTPUT}	No mail for ${EMAIL_ACCOUNT}
    Should Contain  ${OUTPUT}     ${SENDER2}

SUITE:Remove Old Emails From Mailbox
    CLI:Switch Connection	mail_server
    CLI:Write	echo -n > /var/mail/${EMAIL_ACCOUNT}
    ${ERASED_MAILBOX}	CLI:Write	cat /var/mail/${EMAIL_ACCOUNT}
    Should Not Contain	${ERASED_MAILBOX}	${SENDER1}
    Should Not Contain	${ERASED_MAILBOX}	${SENDER2}

SUITE:Remove configuration added for email sender and disable logs
    Input Text    //*[@id="emailServer"]    ${EMPTY}
    Input Text    //*[@id="emailUser"]    ${EMPTY}
    Input Text    //*[@id="emailPass"]    ${EMPTY}
    Input Text    //*[@id="emailPassAgain"]    ${EMPTY}
    Input Text    //*[@id="destinationEmail"]    ${EMPTY}
    Input Text    //*[@id="senderEmail"]    ${EMPTY}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Auditing::Events::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element    //*[@id="Email"]
    GUI::Basic::Spinner Should Be Invisible
    Unselect Checkbox    //*[@id="cat1"]
    Unselect Checkbox    //*[@id="cat2"]
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible