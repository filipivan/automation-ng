*** Settings ***
Documentation	Testing Auditing:Destinations:SNMP Subtab
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX	CLI

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Check Lists
	@{EVENTS_L}=	Get List Items	jquery=#eventfacility
	@{DATA_L}=	Get List Items	jquery=#datafacility
	@{ACTUAL}=	Create List	Log Local 0	Log Local 1	Log Local 2	Log Local 3	Log Local 4	Log Local 5
	Should Be Equal	${EVENTS_L}	${ACTUAL}
	Should Be Equal	${DATA_L}	${ACTUAL}

Test IPv4 Settings
	Select Checkbox	jquery=#syslogRemote
	Element Should Be Visible	jquery=#syslogRemoteIP
	Element Should Be Visible	jquery=#syslogRemoteNotes
	@{TEST_F}=	Create List	.....	.//./##	123.245.234.321	127.0.0.2
	GUI::Auditing::Auto Input Tests	syslogRemoteIP	@{TEST_F}

Test IPv6 Settings
	Select Checkbox	jquery=#syslogRemote6
	Element Should Be Visible	jquery=#syslogRemoteIP6
	Element Should Be Visible	jquery=#syslogRemoteNotes6
	@{TEST_S}=	Create List	.....	.//./##	123.245.234.321	127.0.0.2	293d:29g2::000a	002d::2491
	GUI::Auditing::Auto Input Tests	syslogRemoteIP6	@{TEST_S}

Test Syslog Remote Server IPv4
	Select Checkbox	syslogRemote
	Input Text	syslogRemoteIP	1.1.1.1:1468
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	${TEXT}=	Get Value	syslogRemoteIP
	Should Be Equal	1.1.1.1:1468	${TEXT}
	Input Text	syslogRemoteIP	1.1.1.1:514
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	${TEXT}=	Get Value	syslogRemoteIP
	Should Be Equal	1.1.1.1	${TEXT}

	#CLI
	CLI:Connect As Root
	CLI:Enter Path  /etc/syslog-ng/
	${TEXT}=	CLI:Write	tail -n2 syslog-ng.conf
	Should Contain	${TEXT}	514

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Auditing::Open Syslog Tab
    GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Unselect Checkbox	jquery=#syslogRemote
	Unselect Checkbox	jquery=#syslogRemote6
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout
	Close all Browsers