*** Settings ***
Library    Process
Resource	../../init.robot
Documentation	Tests for SNMP Trap and Email destination working simultaneously
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX
Default Tags	AUDITING	DESTINATIONS	SNMPTRAP	EMAIL

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SNMPTRAP_SERVER}	snmpuser
${SNMPTRAP_SERVER_HOST}	${NFSSERVER}
${SERVERPRESENT}	${SNMPSERVERPRESENT}

${SERVER_PATH}	${SNMPPATH}
${SERVER_FILE}	${SNMPFILE}

${EMAIL_SERVER}	${EMAIL_SERVER}
${EMAIL_PORT}	${EMAIL_PORT}
${EMAIL_ACCOUNT}	${EMAIL_ACCOUNT}
${EMAIL_PASSWD}	${EMAIL_PASSWD}
${EMAIL_DESTINATION}	${EMAIL_DESTINATION}

*** Test Cases ***
Test SNMPTrap Server Version 2c udp-ipv4 with Email destination external server
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:SELECT SNMPTRAP EVENTS
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Setup SNMPTRAP v2 udp-ipv4
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

	CLI:Open	${SNMPTRAP_SERVER}	${SNMPTRAP_SERVER}	TYPE=root	HOST_DIFF=${SNMPTRAP_SERVER_HOST}
	SUITE:Test Log	${SERVER_PATH}	${SERVER_FILE}

	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	SUITE:SELECT EMAIL EVENTS
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Setup Email destination external server
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="testemail"]
	Sleep	5
	Page Should Not Contain	ssmtp


	${result} =	Run Process	python3	./ngm_tests/gui/auditing/destinations/test_email_gmail.py
	should be equal as integers  	${result.rc}	1
	[Teardown]	SUITE:Teardown

Test SNMPTrap Server Version 2c udp-ipv4 with Email destination internal mail server
	[Tags]	NON-CRITICAL	NEED-REVIEW
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:SELECT SNMPTRAP EVENTS
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Setup SNMPTRAP v2 udp-ipv4
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

	CLI:Open	${SNMPTRAP_SERVER}	${SNMPTRAP_SERVER}	TYPE=root	HOST_DIFF=${SNMPTRAP_SERVER_HOST}
	${USER}=	CLI:Get User Whoami
	${USER}=	Set Suite Variable	${USER}
	SUITE:Test Log	${SERVER_PATH}	${SERVER_FILE}

	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	SUITE:SELECT EMAIL EVENTS
	GUI::Basic::Auditing::Email::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	Input Text	emailServer	${EMAIL_SERVER}
	Input Text	emailUser	${EMAIL_ACCOUNT}
	Input Text	emailPass	${EMAIL_PASSWD}
	Input Text	emailPassAgain	${EMAIL_PASSWD}
	Input Text	destinationEmail	${EMAIL_DESTINATION}
	Input Text	emailPort	${EMAIL_PORT}
	Checkbox Should Be Selected	startTLS
	GUI::Basic::Save

	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

	SUITE:Check Email Server

	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid
	CLI:Open	${SNMPTRAP_SERVER}	${SNMPTRAP_SERVER}	TYPE=root	HOST_DIFF=${SNMPTRAP_SERVER_HOST}
	SUITE:Test Log	${SERVER_PATH}	${SERVER_FILE}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	CLI:Close Connection
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	SUITE:UNSELECT SNMPTRAP EVENTS
	GUI::Basic::Spinner Should Be Invisible
	SUITE:UNSELECT EMAIL EVENTS
	GUI::Basic::Auditing::SNMPTrap::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	Input Text	trapServer	0
	Input Text	trapPort	0
	Wait Until Keyword Succeeds	30	1	GUI::Basic::Save
	SUITE: Clear Email destination setup
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

SUITE:SELECT SNMPTRAP EVENTS
	GUI::Basic::Auditing::Events::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	Click Link	SNMP Trap
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2
	Select Checkbox	//*[@id="cat1"]
	Select Checkbox	//*[@id="cat2"]
	Select Checkbox	//*[@id="cat3"]
	Select Checkbox	//*[@id="cat4"]
	Wait Until Keyword Succeeds	30	1	GUI::Basic::Save

SUITE:UNSELECT SNMPTRAP EVENTS
	GUI::Basic::Auditing::Events::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	Click Link	SNMP Trap
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2
	Unselect Checkbox	//*[@id="cat1"]
	Unselect Checkbox	//*[@id="cat2"]
	Unselect Checkbox	//*[@id="cat3"]
	Unselect Checkbox	//*[@id="cat4"]
	Wait Until Keyword Succeeds	30	1	GUI::Basic::Save

SUITE:SELECT EMAIL EVENTS
	GUI::Basic::Auditing::Events::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	Click Link	Email
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2
	Select Checkbox	//*[@id="cat1"]
	Select Checkbox	//*[@id="cat2"]
	Select Checkbox	//*[@id="cat3"]
	Select Checkbox	//*[@id="cat4"]
	Wait Until Keyword Succeeds	30	1	GUI::Basic::Save

SUITE:UNSELECT EMAIL EVENTS
	GUI::Basic::Auditing::Events::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	Click Link	Email
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2
	Unselect Checkbox	//*[@id="cat1"]
	Unselect Checkbox	//*[@id="cat2"]
	Unselect Checkbox	//*[@id="cat3"]
	Unselect Checkbox	//*[@id="cat4"]
	Wait Until Keyword Succeeds	30	1	GUI::Basic::Save

SUITE:Setup SNMPTRAP v2 udp-ipv4
	GUI::Basic::Auditing::SNMPTrap::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	Clear Element Text	trapServer
	Input Text	trapServer	${SNMPTRAP_SERVER_HOST}
	Select From List By Value	trapProtocol	udp
	Clear Element Text	trapPort
	Input Text	trapPort	162
	Wait Until Keyword Succeeds	30	1	GUI::Basic::Save

SUITE:Setup Email destination external server
	GUI::Basic::Auditing::Email::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	Input Text	emailServer	outlook.office365.com
	Input Text	emailUser	autobuild.zpesystems
	Input Text	emailPass	N0d3Gr1d!
	Input Text	emailPassAgain	N0d3Gr1d!
	Input Text	emailPort	587
	Input Text	destinationEmail	autoqazpesystems@gmail.com
	Checkbox Should Be Selected	startTLS
	Wait Until Keyword Succeeds	30	1	GUI::Basic::Save

SUITE: Clear Email destination setup
	GUI::Basic::Auditing::Email::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	Clear Element Text	emailServer
	Clear Element Text	emailPort
	Clear Element Text	emailUser
	Clear Element Text	emailPass
	Clear Element Text	emailPassAgain
	Clear Element Text	destinationEmail
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Test Log
	[Arguments]	${PATH}	${FILE}
	${OUTPUT}=	CLI:Write	cat ${PATH}/${FILE}
	Run Keyword If	'${NGVERSION}' >= '4.0'	Should Contain	${OUTPUT}	42518.2.1.1.120.0.200
	Run Keyword If	'${NGVERSION}' < '4.0'	Should Contain	${OUTPUT}	42518.100.120.0.200

SUITE:Check Email Server
	CLI:Open	USERNAME=${EMAIL_ACCOUNT}	PASSWORD=${EMAIL_PASSWD}	session_alias=mail_server	TYPE=shell	HOST_DIFF=${EMAIL_SERVER}
	Write	echo 1 | mail
	${OUTPUT}=	CLI:Read Until Prompt
	Should Not Contain	${OUTPUT}	No mail for jamie
	@{LINES}=	Split To Lines	${OUTPUT}
	${LINE}=	Set Variable	${LINES}[1]
	@{NUMBERS}=	Split String	${LINE}	${SPACE}
	${NUM}=	Set Variable	${NUMBERS}[1]
	${NUM}=	Convert To Integer	${NUM}
	${NUMBER}=	Evaluate	${NUM}-1
	Write	echo ${NUMBER} | mail
	${OUTPUT}=	CLI:Read Until Prompt
	@{LIST}=	Create List	Event ID	From: admin@nodegrid
	Run Keyword If	${NGVERSION} >= 4.1	Append To List	${LIST}	SUBJECT:Nodegrid Event Notification
	FOR	${INDEX}	IN	@{LIST}
		Should Contain	${OUTPUT}	${INDEX}
	END