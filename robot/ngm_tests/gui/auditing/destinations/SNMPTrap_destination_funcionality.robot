*** Settings ***
Resource	../../init.robot
Documentation	Tests for SNMPTrap funcionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX
Default Tags	AUDITING	DESTINATIONS	SNMPTRAP

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SNMPTRAP_SERVER}	snmpuser
${SNMPTRAP_SERVER_HOST}	${NFSSERVER}
${SERVERPRESENT}	${SNMPSERVERPRESENT}

${SERVER_PATH}	${SNMPPATH}
${SERVER_FILE}	${SNMPFILE}

${MD5DESUSER}	${SNMPMD5DESUSER}
${MD5AESUSER}	${SNMPMD5AESUSER}
${SHADESUSER}	${SNMPSHADESUSER}
${SHAAESUSER}	${SNMPSHAAESUSER}

${MD5PASSWORD}	${SNMPMD5PASSWORD}
${SHAPASSWORD}	${SNMPSHAPASSWORD}

${DESPASSWORD}	${SNMPDESPASSWORD}
${AESPASSWORD}	${SNMPAESPASSWORD}

*** Test Cases ***
Test SNMPTrap version 2c tcp-ipv4
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SELECT SNMPTRAP EVENTS
	GUI::Basic::Auditing::Destinations::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Click element  css=#auditing_destinations_snmp > .title_b
    GUI::Basic::Spinner Should Be Invisible
	Clear Element Text	trapServer
	Input Text	trapServer	${SNMPTRAP_SERVER_HOST}
	Select From List By Value	trapProtocol	tcp
	Clear Element Text	trapPort
	Input Text	trapPort	161
	run keyword and continue on failure 	GUI::Basic::Save
	GUI::Basic::Logout And Close Nodegrid

	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

	CLI:Open	${SNMPTRAP_SERVER}	${SNMPTRAP_SERVER}	TYPE=root	HOST_DIFF=${SNMPTRAP_SERVER_HOST}
	#SUITE:Test Log		${SERVER_PATH}	${SERVER_FILE}
	[Teardown]	SUITE:Teardown

Test SNMPTrap Server Version 2c udp-ipv4
	SUITE:Setup
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SELECT SNMPTRAP EVENTS
	GUI::Basic::Auditing::Destinations::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Click element  css=#auditing_destinations_snmp > .title_b
    GUI::Basic::Spinner Should Be Invisible
	Clear Element Text	trapServer
	Input Text	trapServer	${SNMPTRAP_SERVER_HOST}
	Select From List By Value	trapProtocol	udp
	Clear Element Text	trapPort
	Input Text	trapPort	162
	run keyword and continue on failure 	GUI::Basic::Save
	GUI::Basic::Logout And Close Nodegrid

	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

	CLI:Open	${SNMPTRAP_SERVER}	${SNMPTRAP_SERVER}	TYPE=root	HOST_DIFF=${SNMPTRAP_SERVER_HOST}
	#SUITE:Test Log		${SERVER_PATH}	${SERVER_FILE}
	[Teardown]	SUITE:Teardown

Test SNMPTrap Server Version 3 MD5 DES udp-ipv4
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SELECT SNMPTRAP EVENTS
	GUI::Basic::Auditing::Destinations::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Click element  css=#auditing_destinations_snmp > .title_b
    GUI::Basic::Spinner Should Be Invisible
	Clear Element Text	trapServer
	Input Text	trapServer	${SNMPTRAP_SERVER_HOST}
	Select From List By Value	trapProtocol	udp
	Clear Element Text	trapPort
	Input Text	trapPort	162
	Select Radio Button	trapVersion	3
	Input Text	trapSecName	${MD5DESUSER}
	Select From List By Value	trapSecLevel	authPriv
	Select From List By Value	trapAuthType	MD5
	Input Text	trapAuthPass	${MD5PASSWORD}
	Select From List By Value	trapPrivType	DES
	Input Text	trapPrivPass	${DESPASSWORD}
	run keyword and continue on failure 	GUI::Basic::Save
	GUI::Basic::Logout And Close Nodegrid

	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

	CLI:Open	${SNMPTRAP_SERVER}	${SNMPTRAP_SERVER}	TYPE=root	HOST_DIFF=${SNMPTRAP_SERVER_HOST}
	#SUITE:Test Log		${SERVER_PATH}	${SERVER_FILE}

	[Teardown]	SUITE:Teardown

Test SNMPTrap Server Version 3 MD5 AES udp-ipv4
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SELECT SNMPTRAP EVENTS
	GUI::Basic::Auditing::Destinations::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Click element  css=#auditing_destinations_snmp > .title_b
    GUI::Basic::Spinner Should Be Invisible
	Clear Element Text	trapServer
	Input Text	trapServer	${SNMPTRAP_SERVER_HOST}
	Select From List By Value	trapProtocol	udp
	Clear Element Text	trapPort
	Input Text	trapPort	162
	Select Radio Button	trapVersion	3
	Input Text	trapSecName	${MD5DESUSER}
	Select From List By Value	trapSecLevel	authPriv
	Select From List By Value	trapAuthType	MD5
	Input Text	trapAuthPass	${MD5PASSWORD}
	Select From List By Value	trapPrivType	AES
	Input Text	trapPrivPass	${AESPASSWORD}
	run keyword and continue on failure 	GUI::Basic::Save
	GUI::Basic::Logout And Close Nodegrid

	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

	CLI:Open	${SNMPTRAP_SERVER}	${SNMPTRAP_SERVER}	TYPE=root	HOST_DIFF=${SNMPTRAP_SERVER_HOST}
	#SUITE:Test Log		${SERVER_PATH}	${SERVER_FILE}
	[Teardown]	SUITE:Teardown

Test SNMPTrap Server Version 3 SHA DES udp-ipv4
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SELECT SNMPTRAP EVENTS
	GUI::Basic::Auditing::Destinations::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Click element  css=#auditing_destinations_snmp > .title_b
    GUI::Basic::Spinner Should Be Invisible
	Clear Element Text	trapServer
	Input Text	trapServer	${SNMPTRAP_SERVER_HOST}
	Select From List By Value	trapProtocol	udp
	Clear Element Text	trapPort
	Input Text	trapPort	162
	Select Radio Button	trapVersion	3
	Input Text	trapSecName	${SHADESUSER}
	Select From List By Value	trapSecLevel	authPriv
	Select From List By Value	trapAuthType	SHA
	Input Text	trapAuthPass	${SHAPASSWORD}
	Select From List By Value	trapPrivType	DES
	Input Text	trapPrivPass	${DESPASSWORD}
	run keyword and continue on failure 	GUI::Basic::Save
	GUI::Basic::Logout And Close Nodegrid

	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

	CLI:Open	${SNMPTRAP_SERVER}	${SNMPTRAP_SERVER}	TYPE=root	HOST_DIFF=${SNMPTRAP_SERVER_HOST}
	#SUITE:Test Log		${SERVER_PATH}	${SERVER_FILE}
	[Teardown]	SUITE:Teardown

Test SNMPTrap Server Version 3 SHA AES udp-ipv4
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SELECT SNMPTRAP EVENTS
	GUI::Basic::Auditing::Destinations::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Click element  css=#auditing_destinations_snmp > .title_b
    GUI::Basic::Spinner Should Be Invisible
	Clear Element Text	trapServer
	Input Text	trapServer	${SNMPTRAP_SERVER_HOST}
	Select From List By Value	trapProtocol	udp
	Clear Element Text	trapPort
	Input Text	trapPort	162
	Select Radio Button	trapVersion	3
	Input Text	trapSecName	${SHADESUSER}
	Select From List By Value	trapSecLevel	authPriv
	Select From List By Value	trapAuthType	SHA
	Input Text	trapAuthPass	${SHAPASSWORD}
	Select From List By Value	trapPrivType	AES
	Input Text	trapPrivPass	${AESPASSWORD}
	run keyword and continue on failure 	GUI::Basic::Save
	GUI::Basic::Logout And Close Nodegrid

	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

	CLI:Open	${SNMPTRAP_SERVER}	${SNMPTRAP_SERVER}	TYPE=root	HOST_DIFF=${SNMPTRAP_SERVER_HOST}
	#SUITE:Test Log		${SERVER_PATH}	${SERVER_FILE}
	[Teardown]	SUITE:Teardown

Test SNMPTrap Server Version 3 AUTHNOPRIV
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SELECT SNMPTRAP EVENTS
	GUI::Basic::Auditing::Destinations::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Click element  css=#auditing_destinations_snmp > .title_b
    GUI::Basic::Spinner Should Be Invisible
	Clear Element Text	trapServer
	Input Text	trapServer	${SNMPTRAP_SERVER_HOST}
	Select From List By Value	trapProtocol	udp
	Clear Element Text	trapPort
	Input Text	trapPort	162
	Select Radio Button	trapVersion	3
	Input Text	trapSecName	${SHADESUSER}
	Select From List By Value	trapSecLevel	authNoPriv
	Select From List By Value	trapAuthType	SHA
	Input Text	trapAuthPass	${SHAPASSWORD}
	Select From List By Value	trapPrivType	AES
	Input Text	trapPrivPass	${AESPASSWORD}
	run keyword and continue on failure 	GUI::Basic::Save
	GUI::Basic::Logout And Close Nodegrid

	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

	CLI:Open	${SNMPTRAP_SERVER}	${SNMPTRAP_SERVER}	TYPE=root	HOST_DIFF=${SNMPTRAP_SERVER_HOST}
	#SUITE:Test Log		${SERVER_PATH}	${SERVER_FILE}
	[Teardown]	SUITE:Teardown

Test SNMPTrap Server Version 3 NOAUTHNOPRIV
	SUITE:Setup
	GUI::Basic::Spinner Should Be Invisible
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SELECT SNMPTRAP EVENTS
	GUI::Basic::Auditing::Destinations::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Click element  css=#auditing_destinations_snmp > .title_b
    GUI::Basic::Spinner Should Be Invisible
	Clear Element Text	trapServer
	Input Text	trapServer	${SNMPTRAP_SERVER_HOST}
	Select From List By Value	trapProtocol	udp
	Clear Element Text	trapPort
	Input Text	trapPort	162
	Select Radio Button	trapVersion	3
	Input Text	trapSecName	${SHADESUSER}
	Select From List By Value	trapSecLevel	noAuthNoPriv
	Select From List By Value	trapAuthType	SHA
	Input Text	trapAuthPass	${SHAPASSWORD}
	Select From List By Value	trapPrivType	AES
	Input Text	trapPrivPass	${AESPASSWORD}
	Wait Until Keyword Succeeds	30	1	GUI::Basic::Save
	GUI::Basic::Logout And Close Nodegrid

	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout And Close Nodegrid

	CLI:Open	${SNMPTRAP_SERVER}	${SNMPTRAP_SERVER}	TYPE=root	HOST_DIFF=${SNMPTRAP_SERVER_HOST}
	#SUITE:Test Log		${SERVER_PATH}	${SERVER_FILE}

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	CLI:Close Connection
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible
	UNSELECT SNMPTRAP EVENTS
	GUI::Basic::Auditing::Destinations::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Click element  css=#auditing_destinations_snmp > .title_b
    GUI::Basic::Spinner Should Be Invisible
	Input Text	trapServer	0
	Input Text	trapPort	0
	run keyword and continue on failure 	GUI::Basic::Save
	GUI::Basic::Logout And Close Nodegrid
	Close all Browsers

SELECT SNMPTRAP EVENTS
	GUI::Basic::Auditing::Events::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	Click Link	SNMP Trap
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox 	//*[@id="cat1"]
	Select Checkbox 	//*[@id="cat2"]
	Select Checkbox	    //*[@id="cat3"]
	Select Checkbox	    //*[@id="cat4"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

UNSELECT SNMPTRAP EVENTS
	GUI::Basic::Auditing::Events::Open Tab
    GUI::Basic::Spinner Should Be Invisible
	Click Link	SNMP Trap
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2
	Unselect Checkbox   //*[@id="cat1"]
	Unselect Checkbox	//*[@id="cat2"]
	Unselect Checkbox	//*[@id="cat3"]
	Unselect Checkbox	//*[@id="cat4"]
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Test Log
	[Arguments]	${PATH}	${FILE}
	#${OUTPUT}=	CLI:Write	cat ${PATH}/${FILE}
	#Run Keyword If	'${NGVERSION}' >= '4.0'	Should Contain	${OUTPUT}	42518.2.1.1.120.0.200
	#Run Keyword If	'${NGVERSION}' < '4.0'	Should Contain	${OUTPUT}	42518.100.120.0.200