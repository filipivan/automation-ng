import imaplib
import sys

# -------------------------------------------------
#
# Utility to search an email from Gmail Using Python
#
# ------------------------------------------------
from robot.result import ExecutionResult

IMAP_SERVER = "imap.gmail.com"
IMAP_PORT = 993
IMAP_EMAIL = "autoqazpesystems@gmail.com"
IMAP_PWD = ".QA@automation!"

SEARCH_BOX = "inbox"
SEARCH_EMAIL = "autobuild.zpesystems@gmail.com"
SEARCH_SUBJECT = "Email Notification"
SEARCH_CONTENT = "Email test from Nodegrid at"

def test_mail():
    try:
        mail = imaplib.IMAP4_SSL(IMAP_SERVER, IMAP_PORT)
        # mail.debug = 4
        mail.login(IMAP_EMAIL, IMAP_PWD)
        mail.select(SEARCH_BOX)

        type, data = mail.search(None, '(FROM "' + SEARCH_EMAIL + '" SUBJECT "' + SEARCH_SUBJECT + '" BODY "' + SEARCH_CONTENT + '" UNSEEN)')

        emails = data[0].split()

        if len(emails) == 0:
            raise Exception("Email not found")

        for email in emails:
            mail.store(email, '+FLAGS', '\SEEN')

        sys.exit(1)


    except Exception as e:
        sys.exit(0)


if __name__ == "__main__":
    test_mail()