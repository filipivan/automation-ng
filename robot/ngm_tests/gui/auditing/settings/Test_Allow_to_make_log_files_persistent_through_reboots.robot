*** Settings ***
Documentation	Test Allow to make log files persistent through reboot
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX
Default Tags	EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NFS_Server1}	192.168.2.88
${NFS_path}	/nfs
${NFS_File_Size}	1024
${NFS_Segments}	10
${Name}	test automation
${Event_ID}	.*Event ID 200.*

*** Test Cases ***
Test Case to Enable Logs Persistent Feild
	GUI::Basic::Auditing::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	click element	xpath=//label[contains(.,'Enable Persistent Logs')]
	GUI::Basic::Save If Configuration Changed
	GUI::Basic::Spinner Should Be Invisible

Test Case to check logs
	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	NEED-REVIEW
	SUITE:Setup
	SUITE:To Enter Shell Mode
	${OUTPUT}=	GUI:Access::Generic Console Command Output	cd /var/local/LOGS/
	Sleep	15s
	Should Contain	${OUTPUT}	root@nodegrid:/var/local/LOGS#
	${OUTPUT}=	GUI:Access::Generic Console Command Output	ls
	sleep	15s
	Should Contain	${OUTPUT}	dlog.log	messages	wtmp
	${OUTPUT}=	GUI:Access::Generic Console Command Output	echo "${Name}" > /var/local/LOGS/dlog.log
	sleep	15s
	Should Contain	${OUTPUT}	root@nodegrid:/var/local/LOGS#
	${OUTPUT}=	GUI:Access::Generic Console Command Output	cat dlog.log
	sleep	15s
	Should Contain	${OUTPUT}	${Name}

Test Case to Reboot the Device
	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	NEED-REVIEW
	SUITE:To Reboot the System
	SUITE:To Check Reboot is Done

Test Case to check the logs after Reboot
	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	NEED-REVIEW
	SUITE:To Enter Shell Mode
	${OUTPUT}=	GUI:Access::Generic Console Command Output	cat /var/local/LOGS/dlog.log
	Sleep	15s
	Should Contain	${OUTPUT}	${Name}

Test Case to Set Destination to NFS Sever
	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	NEED-REVIEW
	SUITE:Setup
	GUI::Auditing::Open File Tab
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	select from list by value	xpath=//select[@id='filetype']	nfs
	input text	xpath=//input[@id='nfs_server']	${NFS_Server1}
	input text	xpath=//input[@id='nfs_path']	/nfs
	input text	xpath=//input[@id='nfs_size']	${NFS_File_Size}
	input text	xpath=//input[@id='nfs_segments']	${NFS_Segments}
	sleep	10s
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

Test Case to check logs for NFS
	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	NEED-REVIEW
	SUITE:Setup
	SUITE:To Enter Shell Mode
	${OUTPUT}=	GUI:Access::Generic Console Command Output	cd /var/NFS/LOGS
	Sleep	10s
	Should Contain	${OUTPUT}	root@nodegrid:/var/NFS/LOGS#
	${OUTPUT}=	GUI:Access::Generic Console Command Output	tail -f messages
	Sleep	10s
	Should Match Regexp	${OUTPUT}	${Event_ID}

Test Case to Reboot the Device after Destination Set to NFS
	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	NEED-REVIEW
	SUITE:To Reboot the System
	SUITE:To Check Reboot is Done

Test Case to check the logs after Reboot the device for NFS
	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	NEED-REVIEW
	SUITE:To Enter Shell Mode
	${OUTPUT}=	GUI:Access::Generic Console Command Output	cd /var/NFS/LOGS
	Sleep	15s
	Should Contain	${OUTPUT}	root@nodegrid:/var/NFS/LOGS#
	${OUTPUT}=	GUI:Access::Generic Console Command Output	tail -f messages
	Sleep	15s
	Should Match Regexp	${OUTPUT}	${Event_ID}

Test Case to Default Config
	[Tags]	EXCLUDEIN4_2	NON-CRITICAL	NEED-REVIEW
	SUITE:Setup
	GUI::Auditing::Open File Tab
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	select from list by value	xpath=//select[@id='filetype']	local
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Auditing::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	sleep	3s
	wait until page contains element	xpath=//label[contains(.,'Enable Persistent Logs')]
	click element	xpath=//label[contains(.,'Enable Persistent Logs')]
	GUI::Basic::Save If Configuration Changed
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	Close all Browsers

SUITE:To Enter Shell Mode
	SUITE:Setup
	GUI::Basic::Access::Table::open tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="|nodegrid"]/div[1]/div[2]/a
	Sleep	10s
	Run Keyword If	'${NGVERSION}'<='5.4'	Switch Window	nodegrid
	Run Keyword If	'${NGVERSION}'>'5.4'	Switch Window	nodegrid - Console
	Sleep	10s
	Select Frame	xpath=//*[@id='termwindow']
	Press Keys	None	RETURN

	${OUTPUT}=	GUI:Access::Generic Console Command Output	shell sudo su -
	Sleep	10s
	Should Contain	${OUTPUT}	root@nodegrid:~#

SUITE:To Reboot the System
	SUITE:Setup
	GUI::Basic::System::Toolkit::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	xpath=//a[contains(text(),'Reboot')]
	click element	xpath=//a[contains(text(),'Reboot')]
	handle alert

SUITE:To Check Reboot is Done
	Sleep	3m
	Wait Until Keyword Succeeds	40x	3s	SUITE:Setup
