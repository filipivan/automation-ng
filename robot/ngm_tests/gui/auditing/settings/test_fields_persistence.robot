*** Settings ***
Documentation	Testing Auditing:Settings tab persistance
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../init.robot
Force Tags   PART-1	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	CHROME	FIREFOX

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

*** Test Cases ***
Access Auditing::Settings
	GUI::Basic::Auditing::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible

Validate Timestamp Format Persistence
	GUI::Auditing::Settings::Set And Validate EvttimeType
	GUI::Basic::Spinner Should Be Invisible

Validate Disable All Log Destinations
	[Tags]	NON-CRITICAL	NEED-REVIEW
	${ENABLE_FILE_DESTINATION}=	Run Keyword And Return Status	Checkbox Should Be Selected	jquery=#datalogTypeF
	Run Keyword If	${ENABLE_FILE_DESTINATION}	Click Element	jquery=#datalogTypeF

	${ENABLE_FILE_DESTINATION}=	Run Keyword And Return Status	Checkbox Should Be Selected	jquery=#datalogTypeS
	Run Keyword If	${ENABLE_FILE_DESTINATION}	Click Element	jquery=#datalogTypeS

	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Error Count Should Be  1

	${ENABLE_FILE_DESTINATION}=	Run Keyword And Return Status	Checkbox Should Not Be Selected	jquery=#datalogTypeF
	Run Keyword If	${ENABLE_FILE_DESTINATION}	Click Element	jquery=#datalogTypeF

	${ENABLE_FILE_DESTINATION}=	Run Keyword And Return Status	Checkbox Should Not Be Selected	jquery=#timestamp
	Run Keyword If	${ENABLE_FILE_DESTINATION}	Click Element	jquery=#timestamp

	#GUI::Basic::Wait Until Element Is Accessible	jquery=#saveButton
	Checkbox Should Be Selected	jquery=#datalogTypeF
	#GUI::Basic::Save

	${ENABLE_FILE_DESTINATION}=	Run Keyword And Return Status	GUI::Basic::Wait Until Element Is Accessible	jquery=#saveButton
	Run Keyword If	${ENABLE_FILE_DESTINATION}	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible
	GUI::Auditing::Settings::Go To Events And Back to Settings


Validate Enable All Log Destinations
	${ENABLE_FILE_DESTINATION}=	Run Keyword And Return Status	Checkbox Should Be Selected	jquery=#datalogTypeF
	Run Keyword Unless	${ENABLE_FILE_DESTINATION}	Click Element	jquery=#datalogTypeF

	${ENABLE_FILE_DESTINATION}=	Run Keyword And Return Status	Checkbox Should Be Selected	jquery=#datalogTypeS
	Run Keyword Unless	${ENABLE_FILE_DESTINATION}	Click Element	jquery=#datalogTypeS

	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Auditing::Settings::Go To Events And Back to Settings

	Checkbox Should Be Selected	jquery=#datalogTypeF
	Checkbox Should Be Selected	jquery=#datalogTypeS

Validate Enable Only Syslog Destinations
	${ENABLE_FILE_DESTINATION}=	Run Keyword And Return Status	Checkbox Should Be Selected	jquery=#datalogTypeF
	Run Keyword If	${ENABLE_FILE_DESTINATION}	Click Element	jquery=#datalogTypeF

	${ENABLE_FILE_DESTINATION}=	Run Keyword And Return Status	Checkbox Should Be Selected	jquery=#datalogTypeS
	Run Keyword Unless	${ENABLE_FILE_DESTINATION}	Click Element	jquery=#datalogTypeS

	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Auditing::Settings::Go To Events And Back to Settings

	Checkbox Should Not Be Selected	jquery=#datalogTypeF
	Checkbox Should Be Selected	jquery=#datalogTypeS

Validate Add Timestamp on Every Line Logged
	GUI::Auditing::Settings::Set And Validate DatatimeType
	GUI::Basic::Spinner Should Be Invisible
	GUI::Auditing::Settings::Set And Validate DatatimeType
	GUI::Basic::Spinner Should Be Invisible

Validate Timestamp Format
	GUI::Auditing::Settings::Set And Validate Timestamp Format
	GUI::Basic::Spinner Should Be Invisible
	GUI::Auditing::Settings::Set And Validate Timestamp Format
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout
	Close all Browsers
