# Author: Gabriel Dinse
# File: test_ssh_paramiko.py
# Date: 10/19/20
# Made with PyCharm

# Standard Library

# Third party modules
import paramiko

# Local application imports

def main():
	host = "192.168.15.38"
	port = 22
	username = "root"
	password = ".NGAutom!2#"

	command = "ls"


	ssh = paramiko.SSHClient()
	ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	ssh.connect(host, port, username, password)

	stdin, stdout, stderr = ssh.exec_command(command)
	lines = stdout.readlines()

	print(lines)

if __name__ == "__main__":
	main()