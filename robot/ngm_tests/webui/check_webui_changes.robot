*** Settings ***
Documentation	Test webui.properties changes; cannot change a line without create a .nano, can just create new lines
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../ngm_keywords/rootSettings.robot
Force Tags	API	CLI	WEBUI

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USER}	ricardo
${PASSWD}	SVN!R1c4rd0.
${FILE}	webui.properties

*** Test Cases ***
Check webui.properties changes to NG versions
	:FOR	${VERSION}	IN	@{VERSIONS}
	\	SUITE:Deleting files

	#	PREPARING AND DOWNLOADING FILES
	\	${DICT_FROM_PATH}=	Create Dictionary
	\	${path}=	SUITE:Check Path to Test	${VERSION}
	\	${subpath}=	Fetch From Right	${path}	/
	\	SUITE:Checking If Exists Files and Downloading to Compare	${path}

	#	GETTING DUPLICATED LINES + QUANTITY
	\	Log	List to: ${subpath}	INFO	console=yes
	\	${duplicated}=	CLI:Write	sort ${FILE} | uniq -cd	Raw
	\	${duplicated_report}=	SUITE:Generate List to Report	${duplicated}
	\	${length}=	Get Length	${duplicated_report}
	\	Log	List of Duplicated: ${duplicated_report}	INFO	console=yes
	\	Log	Length of Duplicated: ${length}	INFO	console=yes
	\	Set to Dictionary	${DICT_FROM_PATH}	duplicated=${duplicated_report}

	#	IF HAS NO DIFF, WON'T COMPARE REMOVED
	\	${diff}=	CLI:Write	diff ${FILE}.${subpath} ${FILE}	Raw
	\	${diff_lines}=	Split String	${diff}
	\	Remove From List	${diff_lines}	-1
	\	${has_no_diff}=	Run Keyword And Return Status	Should Be Empty	${diff_lines}
	\	Run Keyword If	${has_no_diff}	Set to Dictionary	${VERSIONS_DICT}	${subpath}=${DICT_FROM_PATH}
	\	Continue For Loop If	${has_no_diff}

	#	GETTING NEW LINES + QUANTITY
	\	${new_lines}=	CLI:Write	grep -Fxvf ${FILE}.${subpath} ${FILE}	Raw
	\	${new_lines_report}=	SUITE:Generate New Lines Report	${new_lines}
	\	${length}=	Get Length	${new_lines_report}
	\	Log	List of Added: ${new_lines_report}	INFO	console=yes
	\	Log	Length of Added: ${length}	INFO	console=yes
	\	Set to Dictionary	${DICT_FROM_PATH}	new_lines=${new_lines_report}

	#	GETTING CHANGED LINES WITH NO NANO OR CHANGED NANO + QUANTITY
	\	${changed_wrong_label_report}=	SUITE:Search If Label Is Changed	${FILE}.${subpath}	${FILE}	${new_lines}
	\	${length}=	Get Length	${changed_wrong_label_report}
	\	Log	List of Changed: ${changed_wrong_label_report}	INFO	console=yes
	\	Log	Length of Changed: ${length}	INFO	console=yes
	\	Set to Dictionary	${DICT_FROM_PATH}	changed_lines_wrong=${changed_wrong_label_report}

	#	GETTING REMOVED LINES + QUANTITY
	\	${storaged}=	CLI:Write	sort ${FILE}.${subpath} | uniq	Raw
	\	${removed_report}=	SUITE:Search ID	${FILE}	${storaged}
	\	${length}=	Get Length	${removed_report}
	\	Log	List of Removed: ${removed_report}	INFO	console=yes
	\	Log	Length of Removed: ${length}	INFO	console=yes
	\	Set to Dictionary	${DICT_FROM_PATH}	removed=${removed_report}

	#	GETTING REMOVED LINES WITHOUT .nano + QUANTITY
	\	${removed_label_no_nano_report}=	SUITE:Search ID	${FILE}	${removed_report}	${TRUE}
	\	${length}=	Get Length	${removed_label_no_nano_report}
	\	Log	List of Removed And No .nano: ${removed_label_no_nano_report}	INFO	console=yes
	\	Log	Length of Removed And No .nano: ${length}	INFO	console=yes
	\	Set to Dictionary	${DICT_FROM_PATH}	removed_label_no_nano=${removed_label_no_nano_report}

	#	ADDING ALL DICTIONARIES TO THE VERSIONS DICTIONARIES
	\	Set to Dictionary	${VERSIONS_DICT}	${subpath}=${DICT_FROM_PATH}
	\	SUITE:Deleting files
	Log	ALL: ${VERSIONS_DICT}	INFO	console=yes
	# [Teardown]	Run Keywords	svn commit -m "updating "

Check webui.properties changes between NG versions
	${index}=	Evaluate	0
	:FOR	${VERSION}	IN	@{VERSIONS}
	\	SUITE:Deleting files

	#	PREPARING TO TEST BETWEEN VERSION
	\	${check_index}=	Run Keyword And Return Status	Should Not Be Equal As Integers	${index}	2
	#	IF EQUAL 2, IS TRUNK. AS DIDN'T TEST 4.0 TO trunk, LETS TRY IT
	\	${index}=	Run Keyword If	${check_index}	Evaluate	${index}+1
	\	...	ELSE	Evaluate	0
	\	${other_version}=	Run Keyword And Return Status	Get From List	${VERSIONS}	${index}
	\	Continue For Loop If	${other_version} == ${FALSE}
	\	${other_version}=	Get From List	${VERSIONS}	${index}

	#	PREPARING AND DOWNLOADING FILES
	\	${DICT_FROM_PATH}=	Create Dictionary
	\	${old_path}=	SUITE:Check Path to Test	${VERSION}
	\	${old_other_path}=	SUITE:Check Path to Test	${other_version}
	\	${path}=	Set Variable If	${check_index}	${old_path}	${old_other_path}	# TO COMPARE 4.0 to trunk and not trunk to 4.0
	\	${other_path}=	Set Variable If	${check_index}	${old_other_path}	${old_path}	# TO COMPARE 4.0 to trunk and not trunk to 4.0
	\	${subpath}=	Fetch From Right	${path}	/
	\	${other_subpath}=	Fetch From Right	${other_path}	/
	\	SUITE:Checking If Exists Files and Downloading to Compare	${path}	${other_path}

	#	DIFF IF HAS NO DIFF, WON'T COMPARE REMOVED	(file is the next version -if version 4.0, then file is 4.1-)
	\	${diff}=	CLI:Write	diff ${FILE}.${subpath} ${FILE}	Raw	# NO DIFFERENCE ABOUT trunk TO 4.0, IF HAS DIFF, CONTINUE...
	\	${diff_lines}=	Split String	${diff}
	\	Remove From List	${diff_lines}	-1
	\	${has_no_diff}=	Run Keyword And Return Status	Should Be Empty	${diff_lines}
	\	Run Keyword If	${has_no_diff}	Set to Dictionary	${VERSIONS_DICT}	${subpath}=${DICT_FROM_PATH}
	\	Continue For Loop If	${has_no_diff}

	#	GETTING NEW LINES + QUANTITY
	\	${new_lines}=	CLI:Write	grep -Fxvf ${FILE}.${subpath} ${FILE}	Raw
	\	${new_lines_report}=	SUITE:Generate New Lines Report	${new_lines}
	\	${length}=	Get Length	${new_lines_report}
	\	Log	List of Added: ${new_lines_report}	INFO	console=yes
	\	Log	Length of Added: ${length}	INFO	console=yes
	\	Set to Dictionary	${DICT_FROM_PATH}	new_lines=${new_lines_report}

	#	GETTING CHANGED LINES WITH NO NANO OR CHANGED NANO + QUANTITY
	\	${changed_wrong_label_report}=	SUITE:Search If Label Is Changed	${FILE}.${subpath}	${FILE}	${new_lines}
	\	${length}=	Get Length	${changed_wrong_label_report}
	\	Log	List of Changed: ${changed_wrong_label_report}	INFO	console=yes
	\	Log	Length of Changed: ${length}	INFO	console=yes
	\	Set to Dictionary	${DICT_FROM_PATH}	changed_lines_wrong=${changed_wrong_label_report}

	#	GETTING REMOVED LINES AND QUANTITY
#	\	${storaged}=	CLI:Write	sort ${FILE}.${subpath} | uniq	Raw
	\	${storaged}=	CLI:Write	grep -Fxvf ${FILE} ${FILE}.${subpath}	Raw
	\	${removed_report}=	SUITE:Search ID	${FILE}	${storaged}	#${updated_report}
	\	${length}=	Get Length	${removed_report}
	\	Log	List of Removed: ${removed_report}	INFO	console=yes
	\	Log	Length of Removed: ${length}	INFO	console=yes
	\	Set to Dictionary	${DICT_FROM_PATH}	removed=${removed_report}

	#	GETTING REMOVED LINES WITHOUT .nano AND QUANTITY
#	\	${updated}=	CLI:Write	sort ${FILE} | uniq	Raw
	\	${removed_label_no_nano_report}=	SUITE:Search ID	${FILE}	${removed_report}	${TRUE}
	\	${length}=	Get Length	${removed_label_no_nano_report}
	\	Log	List of Removed And No .nano: ${removed_label_no_nano_report}	INFO	console=yes
	\	Log	Length of Removed And No .nano: ${length}	INFO	console=yes
	\	Set to Dictionary	${DICT_FROM_PATH}	removed_label_no_nano=${removed_label_no_nano_report}

	#	ADDING ALL DICTIONARIES TO THE VERSIONS DICTIONARIES
	\	Set to Dictionary	${VERSIONS_DICT}	${subpath}-to-${other_subpath}=${DICT_FROM_PATH}
	\	SUITE:Deleting files
	Log	ALL: ${VERSIONS_DICT}	INFO	console=yes
	# [Teardown]	Run Keywords	svn commit -m "updating "

*** Keywords ***
SUITE:Setup
	#	JUST BECAUSE FILE CONTAIN A LOT OF #, THEN USES /var/tmp/webui#
	Open Connection	${HOST}	alias=webui_session
	Set Client Configuration	prompt=~#
	Set Client Configuration	newline=\n
	Set Client Configuration	width=400
	Set Client Configuration	height=600
	Set Client Configuration	timeout=300
	Login	root	root
	SUITE:Deleting files

	#	OR CHANGE TO GET BY NGVERSION, I DON'T KNOW WHICH IS BETTER -TALK TO LIVIO-
	${VERSIONS}=	Create List	4.0	4.1	trunk
	Set Suite Variable	${VERSIONS}	${VERSIONS}
	#	CONTAIN REPORTS FROM ALL VERSIONS
	${VERSIONS_DICT}=	Create Dictionary
	Set Suite Variable	${VERSIONS_DICT}	${VERSIONS_DICT}

SUITE:Teardown
	SUITE:Deleting files
	CLI:Close Connection

SUITE:Deleting files
	CLI:Write	rm -rf ~/${FILE}*
	${ls}=	SUITE:Split Ls
	List Should Not Contain Value	${ls}	${FILE}	Wrong: Contain element

SUITE:Split Ls
	${ls}=	CLI:Write	ls
	${ls}=	Split String	${ls}
	${response}=	Create List
	:FOR	${element}	IN	@{ls}
	\	Run Keyword If	'${element}' != ''	Append to List	${response}	${element}
	Remove From List	${response}	-1
	[Return]	${response}

SUITE:Check Path to Test
	[Arguments]	${VERSION}
	Return From Keyword If	'${VERSION}' == '3.2'	branches/NG_3.2_20160225_R2938
	Return From Keyword If	'${VERSION}' == '4.0'	branches/NG_4.0_20180504_R9735
	Return From Keyword If	'${VERSION}' == '4.1'	branches/NG_4.1_20190619_R12150
	Return From Keyword If	'${VERSION}' == 'trunk'	trunk

SUITE:Checking If Exists Files and Downloading to Compare
	[Arguments]	${PATH}	${OTHER_PATH}=${EMPTY}
	SUITE:Deleting files
	${subpath}=	Fetch From Right	${PATH}	/
	Run Keyword If	'${OTHER_PATH}' == '${EMPTY}'	CLI:Write	wget --user ${USER} --password '${PASSWD}' --no-check-certificate "https://96.126.100.250/automation/robot/ngm_tests/webui/${FILE}.${subpath}"
#	${ls}=	SUITE:Split Ls
#	${first}=	Get From List	${ls}	0
#	Should Be Equal	${first}	${FILE}.${subpath}

	${wget}=	CLI:Write	wget --user ${USER} --password '${PASSWD}' --no-check-certificate "https://96.126.100.250/yocto/sources-zpe/${PATH}/webserver-ng/${FILE}"
	Run Keyword If	'${OTHER_PATH}' != '${EMPTY}'	CLI:Write	mv ${FILE} ${FILE}.${subpath}
	Run Keyword If	'${OTHER_PATH}' != '${EMPTY}'	CLI:Write	wget --user ${USER} --password '${PASSWD}' --no-check-certificate "https://96.126.100.250/yocto/sources-zpe/${OTHER_PATH}/webserver-ng/${FILE}"
#	${ls}=	SUITE:Split Ls
#	${second}=	Get From List	${ls}	0
#	Should Be Equal	${second}	${FILE}

SUITE:Generate List to Report
	[Arguments]	${STRING}	${REMOVE_FIRST}=${FALSE}
	${first_list}=	Split String	${STRING}	${\n}
	Remove From List	${first_list}	0	#REVOME FIRST CASE: IS TO BLANK LINES
	Remove From List	${first_list}	-1	#REMOVE LAST CASE: USER (root) LINE
	${response}=	Create List
	:FOR	${element}	IN	@{first_list}
	\	${label}=	Fetch From Right	${element.strip()}	=
	\	${number_and_id}=	Fetch From Left	${element.strip()}	=
	\	${number_and_id_list}=	Split String	${number_and_id}
	\	${number}=	Get From List	${number_and_id_list}	0
	\	${id}=	Get From List	${number_and_id_list}	1

	\	${is_not_comment}=	Run Keyword and Return Status	Should Not Start With	${id}	\#
	\	${dict}=	Create Dictionary
	\	Set To Dictionary	${dict}	number=${number}
	\	Set To Dictionary	${dict}	id=${id}
	\	Set To Dictionary	${dict}	label=${label}
	\	Run Keyword If	${is_not_comment}	Append to List	${response}	${dict}
	[Return]	${response}

SUITE:Search Added New Lines
	[Documentation]	Check new added IDs to webui.properties on last commit
	[Arguments]	${FILENAME}	${SECOND_FILENAME}	${STRING}
	${list}=	Create List
	${first_list}=	SUITE:Format to Get Formated Lines	${STRING}
	:FOR	${element}	IN	@{first_list}
	\	${is_empty}=	Run Keyword And Return Status	Should Be Equal	${element}	${EMPTY}
	\	Continue For Loop If	${is_empty}
	#	ID AND LABEL FROM 'NEW LINES'
	\	${label}=	Fetch From Right	${element.strip()}	=
	\	${label}=	Set Variable	${label.strip()}
	\	${id}=	Fetch From Left	${element.strip()}	=
	\	${id}=	Set Variable	${id.strip()}

	#	CREATING DICT NOW< TO APPEND ANY TIME
	\	${dict}=	Create Dictionary
	\	Set To Dictionary	${dict}	id=${id}
	\	Set To Dictionary	${dict}	label=${label}

	\	${id_to_sort}=	Replace String	${id}	(	\\(
	\	${id_to_sort}=	Replace String	${id_to_sort}	)	\\)
	\	${id_to_sort}=	Replace String	${id_to_sort}	+	\\+
	\	${id_to_sort}=	Replace String	${id_to_sort}	.	\\.

	\	${is_comment}=	Run Keyword and Return Status	Should Start With	${id}	\#
	\	Continue For Loop If	${is_comment}
	\	${end_id}=	Get Substring	${id}	-5

	\	${get_id}=	CLI:Write	sort ${FILENAME} | uniq | grep -E "(\\s+${id_to_sort}=|^${id_to_sort}=)"	Raw	Yes
	#	IF TRUE YOU CHANGED THE LABEL, IF NOT IS NEW LINE
	\	${existed_before}=	Run Keyword And Return Status	Should Not Be Empty	${get_id_lines}
	\	Continue For Loop If	${existed_before} == ${FALSE}
	[Return]	${list}

SUITE:Search If Label Is Changed
	[Documentation]	Check if label is changed from last version committed to the new commit
		...	Also If is changed, check if .nano existed before and if yes it's ok
		...	(the check about changed .nano occours as a normal ID, but dont check ID.nano -because is a .nano-).
		...	If didn't exist an .nano before check added now, if yes it checks if label is equal label from previously version of ID.
		...	If yes it is ok
	[Arguments]	${FILENAME}	${SECOND_FILENAME}	${STRING}
	${list}=	Create List
	${first_list}=	SUITE:Format to Get Formated Lines	${STRING}
	:FOR	${element}	IN	@{first_list}
	\	${is_empty}=	Run Keyword And Return Status	Should Be Equal	${element}	${EMPTY}
	\	Continue For Loop If	${is_empty}
	#	ID AND LABEL FROM 'NEW LINES'
	\	${label}=	Fetch From Right	${element.strip()}	=
	\	${label}=	Set Variable	${label.strip()}
	\	${id}=	Fetch From Left	${element.strip()}	=
	\	${id}=	Set Variable	${id.strip()}

	\	${is_comment}=	Run Keyword and Return Status	Should Start With	${id}	\#
	\	Continue For Loop If	${is_comment}

	#	CREATING DICT NOW< TO APPEND ANY TIME
	\	${dict}=	Create Dictionary
	\	Set To Dictionary	${dict}	id=${id}
	\	Set To Dictionary	${dict}	label=${label}

	\	${id_to_sort}=	Replace String	${id}	(	\\(
	\	${id_to_sort}=	Replace String	${id_to_sort}	)	\\)
	\	${id_to_sort}=	Replace String	${id_to_sort}	+	\\+
	\	${id_to_sort}=	Replace String	${id_to_sort}	.	\\.

	\	${get_id}=	CLI:Write	sort ${FILENAME} | uniq | grep -E "(\\s+${id_to_sort}=|^${id_to_sort}=)"	Raw	Yes
	#	IF TRUE YOU CHANGED THE LABEL, IF NOT IS NEW LINE
	\	${existed_before}=	Run Keyword And Return Status	Should Not Be Empty	${get_id_lines}
	\	Continue For Loop If	${existed_before} == ${FALSE}

	\	${end_id}=	Get Substring	${id}	-5
	\	${is_nano}=	Run Keyword and Return Status	Should Be Equal	${end_id}	.nano
	#	IF YOU ARE NANO AND EXIST BEFORE, YOU CHANGED THE NANO (YOU CAN'T)
	\	Run Keyword If	${is_nano}	Append To List	${list}	${dict}
	\	Continue For Loop If	${is_nano}

	#	IF EXIST ID BEFORE (NOT .nano) CHECK IF ADDED ACTUALLY .nano
	\	${actual_id_nano}=	CLI:Write	sort ${SECOND_FILENAME} | uniq | grep -E "(\\s+${id_to_sort}.nano=|^${id_to_sort}.nano=)"	Raw
	\	${actual_id_nano_lines}=	SUITE:Format to Get Formated Lines	${actual_id_nano}
	\	${no_nano_actual}=	Run Keyword And Return Status	Should Be Empty	${actual_id_nano_lines}
	#	IF HAS NO NANO IT IS BROKEN
	\	Run Keyword If	${no_nano_actual}	Append to List	${list}	${dict}
	\	Continue For Loop If	${no_nano_actual}
	\	${actual_label_nano}=	Fetch From Right	${actual_id_nano}	=

	#	GETTING LABEL FROM BEFORE UPDATE
	\	${before_id_nano}=	CLI:Write	sort ${FILENAME} | uniq | grep -E "(\\s+${id_to_sort}.nano=|^${id_to_sort}.nano=)"	Raw
	\	${before_id_nano_lines}=	SUITE:Format to Get Formated Lines	${before_id_nano}
	\	${had_nano_before}=	Run Keyword And Return Status	Should Not Be Empty	${before_id_nano_lines}
	\	${before_label_nano}=	Fetch From Right	${before_id_nano}	=

	#	IF HAD BEFORE A .nano AND IS ACTUALLY EQUAL, IT'S OK TO CHANGE TO LABEL FROM A NORMAL ID
	\	${same_nano_label_as_before}=	Run Keyword And Return Status	Should Be Equal As String	${actual_label_nano}	${before_label_nano}
	\	Continue For Loop If	${same_nano_label_as_before}

	\	Append to List	${list}	${dict}
	[Return]	${list}

SUITE:Search ID
	[Documentation]	Check if IDs from webui.properties on last commit still exist on new commit
		...	Also check if .nano exist to removed IDs
		...	Obs: Can use strings or dictionaries to compare,
		...		if using dictionary put ${IS_DICT}=${TRUE}
	[Arguments]	${FILENAME}	${STRING}	${IS_DICT}=${FALSE}
	${first_list}=	Run Keyword If	${IS_DICT} == ${FALSE}	Split String	${STRING}	${\n}
	...	ELSE	Set Variable	${STRING}
	Run Keyword If	${IS_DICT} == ${FALSE}	Remove From List	${first_list}	-1	#REMOVE LAST CASE: USER (root) LINE
	${list}=	Create List
	:FOR	${element}	IN	@{first_list}
	\	${is_empty}=	Run Keyword And Return Status	Should Be Equal	${element}	${EMPTY}
	\	Continue For Loop If	${is_empty}
	\	${label}=	Run Keyword If	${IS_DICT} == ${FALSE}	Fetch From Right	${element.strip()}	=
	\	...	ELSE	Get From Dictionary	${element}	label
	\	${label}=	Set Variable	${label.strip()}
	\	${id}=	Run Keyword If	${IS_DICT} == ${FALSE}	Fetch From Left	${element.strip()}	=
	\	...	ELSE	Get From Dictionary	${element}	id
	\	${id}=	Set Variable	${id.strip()}

	\	${id_to_sort}=	Replace String	${id}	(	\\(
	\	${id_to_sort}=	Replace String	${id_to_sort}	)	\\)
	\	${id_to_sort}=	Replace String	${id_to_sort}	+	\\+
	\	${id_to_sort}=	Replace String	${id_to_sort}	.	\\.

	\	${end_id}=	Get Substring	${id}	-5
	\	${is_comment}=	Run Keyword and Return Status	Should Start With	${id}	\#
	\	Continue For Loop If	${is_comment}

	\	${is_nano}=	Run Keyword and Return Status	Should Be Equal	${end_id}	.nano
	\	${get_id}=	Run Keyword If	${IS_DICT} == ${FALSE}	CLI:Write	sort ${FILENAME} | uniq | grep -E "(\\s+${id_to_sort}=|^${id_to_sort}=)"	Raw
	\	...	ELSE	CLI:Write	sort ${FILENAME} | uniq | grep -E "(\\s+${id}.nano=|^${id}.nano=)"	Raw
	\	${get_id_lines}=	SUITE:Format to Get Formated Lines	${get_id}
	\	${is_not_found}=	Run Keyword And Return Status	Should Be Empty	${get_id_lines}
	\	${is_not_found}=	Set Variable If	${IS_DICT} and ${is_nano}	${FALSE}	${is_not_found}
	\	${length}=	Get Length	${get_id_lines}

	\	${dict}=	Create Dictionary
	\	Set To Dictionary	${dict}	id=${id}
	\	Set To Dictionary	${dict}	label=${label}
	\	Run Keyword If	${is_not_found}	Append to List	${list}	${dict}
	[Return]	${list}

SUITE:Format to Get Formated Lines
	[Arguments]	${FILE}
	${file_lines}=	Split String	${FILE}	${\n}
	Remove From List	${file_lines}	-1
	${tmp}=	Create List
	:FOR	${line}	IN	@{file_lines}
	\	Run Keyword If	'${line.strip()}' != ''	Append to List	${tmp}	${line.strip()}
	Log	\n${tmp}\n	INFO	console=yes
	[Return]	${tmp}

SUITE:Generate New Lines Report
	[Arguments]	${STRING}
	${first_list}=	Split String	${STRING}	${\n}
	Remove From List	${first_list}	-1	#REMOVE LAST CASE: USER (root) LINE
	${response}=	Create List
	:FOR	${element}	IN	@{first_list}
	\	${id}=	Fetch From Left	${element.strip()}	=
	\	${label}=	Fetch From Right	${element.strip()}	=

	\	${is_not_comment}=	Run Keyword and Return Status	Should Not Start With	${id}	\#
	\	${dict}=	Create Dictionary
	\	Set To Dictionary	${dict}	id=${id}
	\	Set To Dictionary	${dict}	label=${label}
	\	Run Keyword If	${is_not_comment}	Append to List	${response}	${dict}
	[Return]	${response}



