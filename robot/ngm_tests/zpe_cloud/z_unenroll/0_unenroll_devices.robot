*** Settings ***
Documentation	Tests for enrollment device on cloud
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Default Tags	ENROLLMENT	UNENROLLMENT	NON-CRITICAL
Force Tags	GUI	ZPECLOUD	${BROWSER}	DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${CUSTOMERCODE}
${ENROLLMENTKEY}
${ENROLLMENTSUCCESS}	Enrollment process successful!
${ZPE_CLOUD_CA_CERTIFICATES_FILE_NAME}	qa-automation-zpe-cloud-ca-certificates.crt

*** Test Cases ***
Test case to check default configuration on host's Security::Services page
	Wait Until Keyword Succeeds	3x	3s	GUI::Basic::Open And Login Nodegrid	USERNAME=${USERNG}	PASSWORD=${PWDNG}
	GUI::Basic::Security::Services::Open Tab
	${HAS_CLOUD_ENABLED}	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='cloud_enable']
	Run Keyword If	not ${HAS_CLOUD_ENABLED}	GUI::Basic::Select Checkbox	//input[@id='cloud_enable']
	${HAS_CLOUD_REMOTE_ACCESS_ENABLED}	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='remote_access']
	Run Keyword If	not ${HAS_CLOUD_REMOTE_ACCESS_ENABLED}	GUI::Basic::Select Checkbox	//input[@id='remote_access']
	${HAS_CLOUD_FILE_PROTECTION_ENABLED}	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='pass_prot']
	Run Keyword If	${HAS_CLOUD_FILE_PROTECTION_ENABLED}	Unselect Checkbox	//input[@id='pass_prot']
	${HAS_SSH_ALLOW_ROOT_ENABLED}	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='allow']
	Run Keyword If	not ${HAS_SSH_ALLOW_ROOT_ENABLED}	SUITE:Select Checkbox to enable ssh to root and change the password
	${HAS_ALLOW_ROOT_CONSOLE_ENABLED}	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='rootconsole']
	Run Keyword If	not ${HAS_ALLOW_ROOT_CONSOLE_ENABLED}	GUI::Basic::Select Checkbox	//input[@id='rootconsole']
	Checkbox Should Be Selected	//input[@id='cloud_enable']
	Checkbox Should Be Selected	//input[@id='remote_access']
	Checkbox Should Not Be Selected	//input[@id='pass_prot']
	Checkbox Should Be Selected	//input[@id='allow']
	Checkbox Should Be Selected	//input[@id='rootconsole']
	[Teardown]	GUI::Basic::Save If Configuration Changed

Test case to check default configuration on host's Managed Devices::Devices page
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::ManagedDevices::Delete All Devices
	GUI::Basic::Spinner Should Be Invisible
	[Teardown]	Close All Browsers

Test case to remove the On-Premises Server's certificate, IP and hostnames from host NG Device
	[Tags]	GUI	ZPECLOUD	${BROWSER}	ON-PREMISES-PRODUCT	DEVICE
	CLI:Open	USERNAME=${USERNAMENG}	PASSWORD=${PASSWORDNG}
	Set Client Configuration	prompt=#
	CLI:Write	shell sudo su -
	Set Client Configuration	prompt=ca-certificates#
	CLI:Write	cd /usr/local/share/ca-certificates/
	CLI:Write	rm /usr/local/share/ca-certificates/${ZPE_CLOUD_CA_CERTIFICATES_FILE_NAME}
	${UPDATE_OUTPUT}	CLI:Write	update-ca-certificates -f
	Should Contain	${UPDATE_OUTPUT}	Updating certificates
	CLI:Write	sed -i '/${ON_PREMISES_SERVER_IP}/d' /etc/hosts
	${CAT_OUTPUT}	CLI:Write	cat /etc/hosts
	Should Not Contain	${CAT_OUTPUT}	${ON_PREMISES_ETC_HOSTS}
	${AGENT_RESTART_OUTPUT}	CLI:Write	/etc/init.d/zpe_cloud_agent restart
	Should Contain	${AGENT_RESTART_OUTPUT}	Starting zpe_cloud_agent
	Wait Until Keyword Succeeds	3x	3s	SUITE:Check if NG device is offline
	[Teardown]	Close Connection

Test case to Unenroll host by CLI
	CLI:Connect As Root	${HOST}	${PASSWORDNG}
	${UNENROLL}=	Set Variable	zpe_cloud_enroll -c ${CUSTOMERCODE} -k ${ENROLLMENTKEY} -u ${ZPECLOUD_HOMEPAGE} -s
	Set Default Configuration	timeout=2m
	Set Client Configuration	prompt=(yes, no):
	CLI:Write	${UNENROLL}
	Set Client Configuration	prompt=#
	${OUTPUT}=	CLI:Write	yes
	Set Default Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Should Contain Any	${OUTPUT}	Unenrollment process successful!	Device is not registered	Please check it and retry the enrollment

Test case to Verify if host was unenrolled
	Wait Until Keyword Succeeds	2m	3s	SUITE:Check if NG device was unenrolled	${SERIALNUMBER}
	[Teardown]	Close All Browsers

#HOSTPEER

Test case to check default configuration on hostpeer's Security::Services page
	[Tags]	EXCLUDEIN5_2	EXCLUDEIN_ONPREMISES
	GUI::ZPECloud::Check and update the zpecloud serial number variable if needed	${USERNAMENGPEER}	${PASSWORDNG}	IS_HOSTPEER=${TRUE}
	Wait Until Keyword Succeeds	3x	3s	GUI::Basic::Open And Login Nodegrid	USERNAME=${USERNAMENGPEER}	PASSWORD=${PASSWORDNG}
	GUI::Basic::Security::Services::Open Tab
	${HAS_CLOUD_ENABLED}	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='cloud_enable']
	Run Keyword If	not ${HAS_CLOUD_ENABLED}	GUI::Basic::Select Checkbox	//input[@id='cloud_enable']
	${HAS_CLOUD_REMOTE_ACCESS_ENABLED}	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='remote_access']
	Run Keyword If	not ${HAS_CLOUD_REMOTE_ACCESS_ENABLED}	GUI::Basic::Select Checkbox	//input[@id='remote_access']
	${HAS_CLOUD_FILE_PROTECTION_ENABLED}	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='pass_prot']
	Run Keyword If	${HAS_CLOUD_FILE_PROTECTION_ENABLED}	Unselect Checkbox	//input[@id='pass_prot']
	${HAS_SSH_ALLOW_ROOT_ENABLED}	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='allow']
	Run Keyword If	not ${HAS_SSH_ALLOW_ROOT_ENABLED}	SUITE:Select Checkbox to enable ssh to root and change the password
	${HAS_ALLOW_ROOT_CONSOLE_ENABLED}	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='rootconsole']
	Run Keyword If	not ${HAS_ALLOW_ROOT_CONSOLE_ENABLED}	GUI::Basic::Select Checkbox	//input[@id='rootconsole']
	Checkbox Should Be Selected	//input[@id='cloud_enable']
	Checkbox Should Be Selected	//input[@id='remote_access']
	Checkbox Should Not Be Selected	//input[@id='pass_prot']
	Checkbox Should Be Selected	//input[@id='allow']
	Checkbox Should Be Selected	//input[@id='rootconsole']
	[Teardown]	GUI::Basic::Save If Configuration Changed

Test case to check default configuration on hostpeer's Managed Devices::Devices page
	[Tags]	EXCLUDEIN5_2	EXCLUDEIN_ONPREMISES
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::ManagedDevices::Delete All Devices
	GUI::Basic::Spinner Should Be Invisible
	[Teardown]	Close All Browsers

Test case to Unenroll hostpeer by CLI
	[Tags]	EXCLUDEIN5_2	EXCLUDEIN_ONPREMISES
	CLI:Connect As Root	${HOSTPEER}	${PASSWORDNG}
	${UNENROLL}=	Set Variable	zpe_cloud_enroll -c ${CUSTOMERCODE} -k ${ENROLLMENTKEY} -u ${ZPECLOUD_HOMEPAGE} -s
	Set Default Configuration	timeout=2m
	Set Client Configuration	prompt=(yes, no):
	CLI:Write	${UNENROLL}
	Set Client Configuration	prompt=#
	${OUTPUT}=	CLI:Write	yes
	Set Default Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Should Contain Any	${OUTPUT}	Unenrollment process successful!	Device is not registered	Please check it and retry the enrollment

Test case to Verify if hostpeer was unenrolled
	[Tags]	EXCLUDEIN5_2	EXCLUDEIN_ONPREMISES
	Wait Until Keyword Succeeds	2m	3s	SUITE:Check if NG device was unenrolled	${SERIALNUMBERPEER}
	[Teardown]	Close All Browsers

*** Keywords ***
SUITE:Setup
	SUITE:Get Device Enrolment Info on QA Instance
	GUI::ZPECloud::Settings::Open Tab
	${Is_Checkbox_Selected}=	Run Keyword And Return Status	Checkbox Should Be Selected	//*[@id="ca-setgen-enable-device-enrollment-checkbox"]
	Run Keyword If	'${Is_Checkbox_Selected}'== 'False'	Run Keyword	SUITE:select cloud enable checkbox
	GUI::ZPECloud::Check and update the zpecloud serial number variable if needed	${USERNG}	${PWDNG}

SUITE:Teardown
	Close All Browsers
	CLI:Close Connection

SUITE:Get Device Enrolment Info on QA Instance
	Wait Until Keyword Succeeds	3x	3s	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Settings::Open Tab
	${CUSTOMERCODE}=	Get Value	ca-setgen-customer-code-input
	${ENROLLMENTKEY}=	Get Value	filled-adornment-password
	${CUSTOMERCODE}=	Set Suite Variable	${CUSTOMERCODE}
	${ENROLLMENTKEY}=	Set Suite Variable	${ENROLLMENTKEY}
	[return]	${CUSTOMERCODE}	${ENROLLMENTKEY}

SUITE:select cloud enable checkbox
	Select Checkbox	//*[@id="ca-setgen-enable-device-enrollment-checkbox"]
	Sleep	2s
	Click Element	//span[contains(.,'SAVE')]

SUITE:Wait until page contains device serial number
	Page Should Contain	${SERIALNUMBER}

SUITE:Select Checkbox to enable ssh to root and change the password
	GUI::Basic::Select Checkbox	//input[@id='allow']
	CLI:Change Root And Admin Passwords from admin user

SUITE:Check if NG device is offline
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//td[contains(.,'${SERIALNUMBER}')]	timeout=1m
	Wait Until Page Contains Element	//td[contains(.,'${SERIALNUMBER}')]/../td/div/div/span[contains(.,'Offline')]

SUITE:Check if NG device was unenrolled
	[Arguments]	${DIFF_SERIALNUMBER}
	GUI::ZPECloud::Basic::Open and Login	${EMAIL}	${PWD}	${PASSWORD}
	GUI::ZPECloud::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Not Contain	${DIFF_SERIALNUMBER}