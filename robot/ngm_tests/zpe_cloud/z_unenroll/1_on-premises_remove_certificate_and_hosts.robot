*** Settings ***
Documentation	Tests to remove the on-premises certificate and IPs also hostnames from Jenkins VM
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Metadata	Executed using NG devices	${HOST}
Resource	../../init.robot
Default Tags	ENROLLMENT	UNENROLLMENT
Force Tags	GUI	ZPECLOUD	${BROWSER}	ON-PREMISES-PRODUCT

Suite Teardown	SUITE:Teardown

*** Variables ***
${ZPE_CLOUD_CA_CERTIFICATES_FILE_NAME}	qa-automation-zpe-cloud-ca-certificates.crt

*** Test Cases ***
Test case to check the On-Premises Server's certificate, IP and hostnames on Jenkins VM
	${OS}=	CLIENT:Get Operating System
	Set Suite Variable	${OS}
	Log	Client OS:\n ${OS}	INFO	console=yes
	IF	'Microsoft' in '''${OS}'''
		CLI:Open	HOST_DIFF=${CLIENTIP}	USERNAME=${CLOUD_JENKINS_VM_USERNAME}	PASSWORD=${CLOUD_JENKINS_VM_PASSWORD}	TYPE=windows
		Write	ssh-keygen -f "%home%\\.ssh\\known_hosts" -R "${HOST_DIFF}"
		...
	ELSE IF	'Linux' in '''${OS}'''
		CLI:Open	HOST_DIFF=${CLIENTIP}	USERNAME=${CLOUD_JENKINS_VM_USERNAME}	PASSWORD=${CLOUD_JENKINS_VM_PASSWORD}	TYPE=shell
		CLI:Write	ssh-keygen -f "/home/${CLOUD_JENKINS_VM_USERNAME}/.ssh/known_hosts" -R "${ON_PREMISES_SERVER_IP}"
		Set Client Configuration	prompt=qausers:
		CLI:Write	sudo su -
		Set Client Configuration	prompt=#
		CLI:Write	${CLOUD_JENKINS_VM_PASSWORD}
		Set Client Configuration	prompt=ca-certificates#
		CLI:Write	cd /usr/local/share/ca-certificates/
		${LS_OUTPUT}	CLI:Write	ls
		Should Contain	${LS_OUTPUT}	${ZPE_CLOUD_CA_CERTIFICATES_FILE_NAME}
		${CAT_OUTPUT}	CLI:Write	cat /etc/hosts
		Set Client Configuration	prompt=#
		Should Contain	${CAT_OUTPUT}	${ON_PREMISES_ETC_HOSTS}
	END
	[Teardown]	Close Connection

*** Keywords ***
SUITE:Teardown
	CLI:Close Connection
	Close All Browsers