*** Settings ***
Documentation	Adding, editing and deleting a company as Super Admin
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Force Tags	GUI	ZPECLOUD	LOGIN	${BROWSER}	COMPANY_CONFIGURATION	NON-DEVICE
#Suite Setup	SUITE:Setup
#Suite Teardown	SUITE:Teardown

*** Variables ***
${SUPER_USERNAME}	${SUPER_EMAIL_ADDRESS}
${SUPER_PASSWORD}	${SUPER_PASSWORD}

${BUSINNESS_NAME}	zpe_systems_automation_company
${ADDRESS}	46757 Fremont Blvd, Fremont, CA 94538
${CONTACT_INFO}	(510) 298-3022
${EDITED}	edited_name

*** Test Cases ***
Add Company
	SUITE:Setup
	SUITE:Add Company
	SUITE:Teardown	${BUSINNESS_NAME}

Edit Company
	[Setup]	SUITE:Setup
	SUITE:Add Company
	SUITE:Edit Company
	SUITE:Teardown	${EDITED}
#	[Teardown]	GUI::ZPECloud::Basic::Table::Delete If Exists	general_0	${EDITED}	${TRUE}

Delete Company
	[Setup]	SUITE:Setup
	SUITE:Add Company
	SUITE:Delete Company	${BUSINNESS_NAME}
	SUITE:Teardown	${BUSINNESS_NAME}
#	[Teardown]	Run Keywords	Run Keyword If Test Failed	SUITE:Teardown

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open and Login	${SUPER_USERNAME}	${SUPER_PASSWORD}
	Sleep	3
	GUI::ZPECloud::Companies::General::Open Tab
#	GUI::ZPECloud::Basic::Table::Delete If Exists	general_0	${BUSINNESS_NAME}	${TRUE}
#	GUI::ZPECloud::Basic::Table::Delete If Exists	general_0	${EDITED}	${TRUE}
	Wait Until Page Contains Element	//*[@id="search-input"]	15s
	Click Element	//*[@id="search-input"]
	Input Text	//*[@id="search-input"]	${BUSINNESS_NAME}
#	Press Keys	//*[@id="search-input"]	ENTER
	Sleep	5
	${ELEMENT}=	Run Keyword And return Status	Wait Until Page Contains	${BUSINNESS_NAME}	15s
	Run Keyword If	${ELEMENT} == True	SUITE:Delete Company	${BUSINNESS_NAME}

SUITE:Teardown
	[Arguments]	${COMPANY_DELETE}
	SUITE:Setup
	GUI::ZPECloud::Companies::General::Open Tab
	Wait Until Page Contains Element	//*[@id="search-input"]	15s
	Click Element	//*[@id="search-input"]
	Input Text	//*[@id="search-input"]	${COMPANY_DELETE}
#	Press Keys	//*[@id="search-input"]	ENTER
	Sleep	5
	${ELEMENT}=	Run Keyword And return Status	Wait Until Page Contains	${COMPANY_DELETE}	15s
	Run Keyword If	${ELEMENT} == True	SUITE:Delete Company	${COMPANY_DELETE}
	Close All Browsers

SUITE:Add Company
	GUI::ZPECloud::Basic::Table::Delete If Exists	general_0	${BUSINNESS_NAME}
	GUI::ZPECloud::Basic::Click Button	add
	Wait Until Page Contains Element	name=business_name	20s
	Input Text	name=business_name	${BUSINNESS_NAME}
	Input Text	name=address	${ADDRESS}
	Input Text	name=contact_info	${CONTACT_INFO}
	Click Element	//*[@id="submit-btn"]/span[1]
	Sleep	3
	Wait Until Keyword Succeeds	30	2	GUI::ZPECloud::Basic::Select Table Row	general_0	${BUSINNESS_NAME}

SUITE:Edit Company
#	Wait Until Keyword Succeeds	30	2	GUI::ZPECloud::Basic::Select Table Row	general_0	${BUSINNESS_NAME}
#	Sleep	5
#	GUI::ZPECloud::Basic::Select Table Row	general_0	${BUSINNESS_NAME}	${TRUE}
#	Sleep	3
	GUI::ZPECloud::Companies::General::Open Tab
	Wait Until Page Contains Element	//*[@id="search-input"]
	Click Element	//*[@id="search-input"]
	Input Text	//*[@id="search-input"]	${BUSINNESS_NAME}
	Press Keys	//*[@id="search-input"]	ENTER
	Sleep	5
	${ROWNUMBER}=	Execute Javascript	return Array.from(document.getElementById('sa-companies-general-table-1').getElementsByTagName('TBODY')[0].getElementsByTagName('TR')).indexOf(Array.from(document.getElementById('sa-companies-general-table-1').getElementsByTagName('TBODY')[0].getElementsByTagName('TR')).filter(e => e.getElementsByTagName('TD')[1].textContent == '${BUSINNESS_NAME}')[0])
	Run Keyword If	${ROWNUMBER} == 0	Click Element	//*[@id="sa-companies-general-table-1"]/div/div/table/tbody/tr/td[1]/span/span[1]/input
	Run Keyword If	${ROWNUMBER} > 0	Click Element	//*[@id="sa-companies-general-table-1"]/div/div/table/tbody/tr[${ROWNUMBER}]/td[1]/span/span[1]/input
	Click Button	Edit
	Wait Until Page Contains Element	name=business_name	20s
	Press Keys	name=business_name	CTRL+a+BACKSPACE
	Clear Element Text	name=business_name
	Input Text	name=business_name	${EDITED}
	Click Element	//*[@id="submit-btn"]/span[1]
	Sleep	3
	Wait Until Keyword Succeeds	30	2	GUI::ZPECloud::Basic::Select Table Row	general_0	${EDITED}

SUITE:Delete Company
	[Arguments]	${COMPANY_DELETE}
	GUI::ZPECloud::Companies::General::Open Tab
	Wait Until Page Contains Element	//*[@id="search-input"]	15s
	Click Element	//*[@id="search-input"]
	Input Text	//*[@id="search-input"]	${COMPANY_DELETE}
#	Press Keys	//*[@id="search-input"]	ENTER
	Sleep	5
	Wait Until Page Contains Element	//*[@id="sa-companies-general-table-1"]/div/div/table/tbody/tr/td[1]/span/span[1]/input
	Click Element	//*[@id="sa-companies-general-table-1"]/div/div/table/tbody/tr/td[1]/span/span[1]/input
	Wait Until Page Contains Element	xpath=//span[contains(.,'Delete')]	15s
	Click Element	xpath=//span[contains(.,'Delete')]
	Wait Until Page Contains Element	xpath=//button[@id='confirm-btn']/span	15s
	Click Element	xpath=//button[@id='confirm-btn']/span