*** Settings ***
Documentation	Tests for on-premise functionality testing
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	ON-PREMISE_TRANSFER
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_1

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${SUPER_EMAIL_ADDRESS}
${PWD}	${SUPER_PASSWORD}
${VALIDINFO}	On-Premises instance is valid
${SAVED}	On-Premises setting updated successfully
${NEWENROLLMENTKEY}	TESTENROLLMENTKEY
${UPDATEDKEY}	Enrollment Key has been set
${ONPREMISESTATUS}	On-Premises
${ONPREMISEFAIL}	On-Premises Failed
${TESTENROLLMENTKEY}	TESTKEY
${ROWNUMBER}
${CUSTOMERCODE_TEST}
${ENROLLMENTKEY_TEST}
${CUSTOMERCODEQA}
${ENROLLMENTKEYQA}
${ENROLL}
${UNENROLL}
${COMPANY_NOT_EXIST_MESSAGE}	SEPARATOR=
	...	Company doesn't exist in the CLOUD instance. Please create a new company with the credentials in the file
	...	${SPACE}"ZPE_CLOUD_QA_ADDITIONAL_COMPANIES_ON_QA|Staging.py" for the instance that is missing it.
${PRODUCTION_CUSTOMERCODE}	AWNII7	#From zpe779927xpbi@gmail.com account
${PRODUCTION_ENROLLMENTKEY}	BZivMUVnAptzUXrT
${PRODUCTION_ZPECLOUD_HOMEPAGE}	https://zpecloud.com

*** Test Cases ***
Test case to check if the onpremise company email exist in the instance via super admin
	${COMPANY_EXIST}	Set Variable	${FALSE}
	Set Suite Variable	${COMPANY_EXIST}
	SUITE:Login into ZPE Cloud SuperAdmin
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//button[contains(text(), 'Users')]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//button[contains(text(), 'GENERAL')]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//input[contains(@placeholder, 'Search Firstname, Lastname or Email')]	${ON_PREMISE_EMAIL}
	Wait Until Element Contains	(//tr)[2]	${ON_PREMISE_EMAIL}	timeout=30s
	${COMPANY_EXIST}	Set Variable	${TRUE}
	Set Suite Variable	${COMPANY_EXIST}
	Close All Browsers
	[Teardown]	Run Keyword If	not ${COMPANY_EXIST}	Fail	${COMPANY_NOT_EXIST_MESSAGE}

Test case on Premise Successful Operation
	Run Keyword If	not ${COMPANY_EXIST}	Fail	${COMPANY_NOT_EXIST_MESSAGE}
	Close All Browsers
	SUITE:Configure On-premise
	SUITE:Enroll device on QA Instance
	SUITE:Check That Device Status Is On-Premise
	SUITE:Check Device Under On-Premise Company
	[Teardown]	SUITE:Teardown

Test case on Premise Unsuccessful Operation
	Run Keyword If	not ${COMPANY_EXIST}	Fail	${COMPANY_NOT_EXIST_MESSAGE}
	SUITE:Setup
	SUITE:Configure On-premise
	SUITE:Change On-premise Information on Test Instance
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Enroll device on QA Instance
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Check That On-premise Failed
	[Teardown]	SUITE:Teardown 02

Test case on Premise Using Button
	Run Keyword If	not ${COMPANY_EXIST}	Fail	${COMPANY_NOT_EXIST_MESSAGE}
	SUITE:Setup
	SUITE:Configure On-premise
	SUITE:Change On-premise Information on Test Instance
	SUITE:Enroll device on QA Instance
	SUITE:On Premise By Button
	SUITE:Check That Device Status Is On-Premise
	SUITE:Check Device Under On-Premise Company
	[Teardown]	SUITE:Teardown 03

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	SUITE:Unenroll device from QA Instance
	SUITE:Unenroll device from Test Instance

SUITE:Teardown
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	(//button[normalize-space()='On-Premises'])[1]	timeout=1m
	Click Element	(//button[normalize-space()='On-Premises'])[1]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	(//input[@id='ca-setop-enable-device-enrollment-checkbox'])[1]
	${IS_ON-PREMISES_CHECKBOX_SELECTED}=	Run Keyword And Return Status	Checkbox Should Be Selected	(//input[@id='ca-setop-enable-device-enrollment-checkbox'])[1]
	Run Keyword If	${IS_ON-PREMISES_CHECKBOX_SELECTED}	GUI::ZPECloud::Settings::Disable On-Premises Enrollment and save
	SUITE:Unenroll device from production via CLI
	SUITE:Enroll device on production via CLI
	SUITE:Unenroll device from QA Instance
	SUITE:Unenroll device from Test Instance
	SUITE:Enroll device on QA Instance
	SUITE:Move Device From AVAILABLE To ENROLLED Tab
	Close All Browsers

SUITE:Teardown 02
	SUITE:Enrolment Info on Test Instance - Save as Default
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	(//button[normalize-space()='On-Premises'])[1]	timeout=1m
	Click Element	(//button[normalize-space()='On-Premises'])[1]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	(//input[@id='ca-setop-enable-device-enrollment-checkbox'])[1]
	${IS_ON-PREMISES_CHECKBOX_SELECTED}=	Run Keyword And Return Status	Checkbox Should Be Selected	(//input[@id='ca-setop-enable-device-enrollment-checkbox'])[1]
	Run Keyword If	${IS_ON-PREMISES_CHECKBOX_SELECTED}	GUI::ZPECloud::Settings::Disable On-Premises Enrollment and save
	SUITE:Unenroll device from QA Instance
	SUITE:Enroll device on QA Instance
	SUITE:Move Device From AVAILABLE To ENROLLED Tab
	Close All Browsers

SUITE:Teardown 03
	SUITE:Enrolment Info on Test Instance - Save as Default
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	(//button[normalize-space()='On-Premises'])[1]	timeout=1m
	Click Element	(//button[normalize-space()='On-Premises'])[1]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	(//input[@id='ca-setop-enable-device-enrollment-checkbox'])[1]
	${IS_ON-PREMISES_CHECKBOX_SELECTED}=	Run Keyword And Return Status	Checkbox Should Be Selected	(//input[@id='ca-setop-enable-device-enrollment-checkbox'])[1]
	Run Keyword If	${IS_ON-PREMISES_CHECKBOX_SELECTED}	GUI::ZPECloud::Settings::Disable On-Premises Enrollment and save
	SUITE:Unenroll device from QA Instance
	SUITE:Unenroll device from Test Instance
	SUITE:Enroll device on QA Instance
	SUITE:Move Device From AVAILABLE To ENROLLED Tab
	Close All Browsers

SUITE:Login into ZPE Cloud SuperAdmin
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Enrolment Info on Test Instance - Save as Default
	GUI::ZPECloud::Basic::Open ZPE Cloud	${ON_PREMISE_URL}	${BROWSER}
	GUI::ZPECloud::Basic::Login	${ON_PREMISE_EMAIL}	${ON_PREMISE_PASSWORD}
	GUI::ZPECloud::Settings::Open Tab
	Clear Element Text	filled-adornment-password
	Press Keys	filled-adornment-password	CTRL+a+BACKSPACE
	Input Text	filled-adornment-password	${TESTENROLLMENTKEY}
	GUI::ZPECloud::Basic::Click Button	save
	Wait Until Page Contains	${UPDATEDKEY}	15s
	Close All Browsers

SUITE:Get Device Enrolment Info on Test Instance
	GUI::ZPECloud::Basic::Open ZPE Cloud	${ON_PREMISE_URL}	${BROWSER}
	GUI::ZPECloud::Basic::Login	${ON_PREMISE_EMAIL}	${ON_PREMISE_PASSWORD}
	GUI::ZPECloud::Settings::Open Tab
	${CUSTOMERCODE_TEST}=	Get Value	ca-setgen-customer-code-input
	${ENROLLMENTKEY_TEST}=	Get Value	filled-adornment-password
	${CUSTOMERCODE_TEST}=	Set Suite Variable	${CUSTOMERCODE_TEST}
	${ENROLLMENTKEY_TEST}=	Set Suite Variable	${ENROLLMENTKEY_TEST}
	Close All Browsers

SUITE:Validate Device Enrolment Info Valid Information
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Settings::Open Tab
	${menu}=	GUI::ZPECloud::Basic::Get Main Menu
	${return}=	GUI::ZPECloud::Basic::Click Button	on-premises
	Wait Until Element Is Enabled	ca-setop-enable-device-enrollment-checkbox	15s
	${IS_NOTSELECTED}=	Run Keyword And return Status	Checkbox Should Not Be Selected	ca-setop-enable-device-enrollment-checkbox
	Run Keyword If	${IS_NOTSELECTED} == True	Select Checkbox	ca-setop-enable-device-enrollment-checkbox
	Wait Until Element Is Enabled	ca-setop-premise-url-input	15s
	Press Keys	//*[@id="ca-setop-premise-url-input"]	CTRL+a+BACKSPACE
	Clear Element Text	ca-setop-premise-url-input
	Input Text	ca-setop-premise-url-input	${ON_PREMISE_URL}
	Press Keys	ca-setop-customer-code-input	CTRL+a+BACKSPACE
	Clear Element Text	ca-setop-customer-code-input
	Input Text	ca-setop-customer-code-input	${CUSTOMERCODE_TEST}
	Press Keys	ca-setop-enroll-key-input	CTRL+a+BACKSPACE
	Clear Element Text	ca-setop-enroll-key-input
	Input Text	ca-setop-enroll-key-input	${ENROLLMENTKEY_TEST}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	1m	5s	SUITE:Validate Info

SUITE:Validate Info
	Click Element	xpath=//span[contains(.,'Validate')]
	Wait Until Page Contains	${VALIDINFO}	15s

SUITE:Get Device Enrolment Info on QA Instance
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Settings::Open Tab
	${CUSTOMERCODEQA}=	Get Value	ca-setgen-customer-code-input
	${ENROLLMENTKEYQA}=	Get Value	filled-adornment-password
	${CUSTOMERCODEQA}=	Set Suite Variable	${CUSTOMERCODEQA}
	${ENROLLMENTKEYQA}=	Set Suite Variable	${ENROLLMENTKEYQA}
	Close All Browsers

SUITE:Configure On-premise
	SUITE:Get Device Enrolment Info on Test Instance
	Wait Until Keyword Succeeds	3x	3s	SUITE:Validate Device Enrolment Info Valid Information
	Wait Until Element Is Enabled	xpath=//button[@id='ca-setop-premise-submit-btn']/span	10s
	GUI::ZPECloud::Basic::Click Button	save
	Wait Until Page Contains	${SAVED}	15s
	Close All Browsers

SUITE:Unenroll device from production via CLI
	Wait Until Keyword Succeeds	3x	1s	CLI:Connect As Root	${HOST}	${PASSWORDNG}
	${UNENROLL_PRODUCTION}=	Set Variable	zpe_cloud_enroll -c ${PRODUCTION_CUSTOMERCODE} -k ${PRODUCTION_ENROLLMENTKEY} -u ${PRODUCTION_ZPECLOUD_HOMEPAGE} -s
	Set Default Configuration	timeout=2m
	Set Client Configuration	prompt=(yes, no):
	CLI:Write	${UNENROLL_PRODUCTION}
	Set Client Configuration	prompt=#
	${OUTPUT}=	CLI:Write	yes
	Set Default Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Should Contain Any	${OUTPUT}	Unenrollment process successful!	Unenroll device failed. Device is not registered. Please check your device registration.

SUITE:Unenroll device from Test Instance
	${HAS_ENROLMENT_INFO}	Run Keyword And Return Status	SUITE:Get Device Enrolment Info on Test Instance
	Run Keyword If	not ${HAS_ENROLMENT_INFO}	Fail	Failed to Get Device Enrolment Info on Test Instance
	IF	${HAS_ENROLMENT_INFO}
		CLI:Connect As Root	${HOST}	${PASSWORDNG}
		${UNENROLL}=	Set Variable	zpe_cloud_enroll -c ${CUSTOMERCODE_TEST} -k ${ENROLLMENTKEY_TEST} -u ${ON_PREMISE_URL} -s
		Write	${UNENROLL}
		Read Until	(yes, no):
		Write	yes
		${OUTPUT}=	Read Until Prompt
		Should Contain Any	${OUTPUT}	Unenrollment process successful!	Device is not registered.	Wrong Customer Code or Enrollment Key.
	END

SUITE:Unenroll device from QA Instance
	${HAS_ENROLMENT_INFO}	Run Keyword And Return Status	SUITE:Get Device Enrolment Info on QA Instance
	Run Keyword If	not ${HAS_ENROLMENT_INFO}	Fail	Failed to Get Device Enrolment Info on QA Instance
	IF	${HAS_ENROLMENT_INFO}
		CLI:Connect As Root	${HOST}	${PASSWORDNG}
		${UNENROLL}=	Set Variable	zpe_cloud_enroll -c ${CUSTOMERCODEQA} -k ${ENROLLMENTKEYQA} -u ${ZPECLOUD_HOMEPAGE} -s
		Write	${UNENROLL}
		Read Until	(yes, no):
		Write	yes
		${OUTPUT}=	Read Until Prompt
		Should Contain Any	${OUTPUT}	Unenrollment process successful!	Device is not registered.
	END

SUITE:Enroll device on QA Instance
	SUITE:Get Device Enrolment Info on QA Instance
	CLI:Connect As Root	${HOST}	${PASSWORDNG}
	${ENROLL}=	Set Variable	zpe_cloud_enroll -c ${CUSTOMERCODEQA} -k ${ENROLLMENTKEYQA} -u ${ZPECLOUD_HOMEPAGE}
	${OUTPUT}=	CLI:Write	${ENROLL}
	${HAS_ENCRYPT_MESSAGE}=	Run Keyword And Return Status	Should Contain	${OUTPUT}	Failed to encrypt secret.
	Run Keyword If	${HAS_ENCRYPT_MESSAGE}	Log	\n The "Failed to enroll device: Failed to encrypt secret. Please contact ZPE Cloud support." happened, need to check if enroll worked.\n	WARN	console=yes
	Should Contain Any	${OUTPUT}	Enrollment process successful!	Device already under Company account.	Failed to encrypt secret.	#enhancement issue on v5.0 "Failed to encrypt secret."

SUITE:Enroll device on production via CLI
	${ENROLL}=	Set Variable	zpe_cloud_enroll -c ${PRODUCTION_CUSTOMERCODE} -k ${PRODUCTION_ENROLLMENTKEY} -u ${PRODUCTION_ZPECLOUD_HOMEPAGE}
	Set Default Configuration	timeout=2m
	${OUTPUT}=	CLI:Write	${ENROLL}
	Set Default Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Should Contain Any	${OUTPUT}	Enrollment process successful!

SUITE:Change On-premise Information on Test Instance
	GUI::ZPECloud::Basic::Open ZPE Cloud	${ON_PREMISE_URL}	${BROWSER}
	GUI::ZPECloud::Basic::Login	${ON_PREMISE_EMAIL}	${ON_PREMISE_PASSWORD}
	GUI::ZPECloud::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Press Keys	filled-adornment-password	CTRL+a+BACKSPACE
	Clear Element Text	filled-adornment-password
	Input Text	filled-adornment-password	${NEWENROLLMENTKEY}
	GUI::ZPECloud::Basic::Click Button	save
	Wait Until Page Contains	${UPDATEDKEY}	15s
	Close All Browsers

SUITE:Check That Device Status Is On-Premise
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${menu}=	GUI::ZPECloud::Basic::Get Sub Menu
	${return}=	GUI::ZPECloud::Basic::Click Button	on-premises
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[3]	60s
	Element Should Contain	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[3]	${SERIALNUMBER}
	Element Should Contain	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[5]	${ONPREMISESTATUS}
	Close All Browsers

SUITE:Check Device Under On-Premise Company
	GUI::ZPECloud::Basic::Open ZPE Cloud	${ON_PREMISE_URL}	${BROWSER}
	GUI::ZPECloud::Basic::Login	${ON_PREMISE_EMAIL}	${ON_PREMISE_PASSWORD}
	GUI::ZPECloud::Devices::Available::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SERIALNUMBER}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	${SERIALNUMBER}	15s
	${DEVICESTATUS}=	Get Text	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr[1]/td[5]/div/div
	Should Not Be Equal	${DEVICESTATUS}	${ONPREMISESTATUS}
	Close All Browsers

SUITE:Check That On-premise Failed
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Devices::Available::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SERIALNUMBER}
	Wait Until Page Contains	${SERIALNUMBER}	30s
	${DEVICESTATUS}=	Get Text	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr[1]/td[5]/div/div
	Should Not Be Equal	${DEVICESTATUS}	${ONPREMISESTATUS}
	Element Should Contain	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr[1]/td[5]/div/div	${ONPREMISEFAIL}
	Close All Browsers

SUITE:On Premise By Button
	SUITE:Check That On-premise Failed
	SUITE:Enrolment Info on Test Instance - Save as Default
	SUITE:Configure On-premise
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Devices::Available::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SERIALNUMBER}
	Wait until Page Contains	${SERIALNUMBER}	30s
	Wait Until Page Contains Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Click Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Wait Until Page Contains Element	xpath=//button[contains(.,'Enroll On-Premises')]	15s
	Click Element	xpath=//button[contains(.,'Enroll On-Premises')]
	Wait Until Keyword Succeeds	5m	1	SUITE:Wait Until Page Contains Device Enrolled Successfully
	Close All Browsers

SUITE:Wait Until Page Contains Device Enrolled Successfully
	GUI::ZPECloud::Devices::Available::Open Tab
	Wait Until Page Does Not Contain	${ONPREMISEFAIL}

SUITE:Move Device From AVAILABLE To ENROLLED Tab
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::ZPECloud::Devices::Available::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SERIALNUMBER}
	Wait until Page Contains	${SERIALNUMBER}	30s
	Wait Until Page Contains Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Click Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Wait Until Page Contains Element	//*[@id="enroll"]	15s
	Click Element	//*[@id="enroll"]
	Wait Until Keyword Succeeds	5m	1	Wait Until Page Contains	Device Enrolled Successfully