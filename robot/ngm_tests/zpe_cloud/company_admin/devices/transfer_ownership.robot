*** Settings ***
Documentation	Tests for transfer device ownership
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	TRANSFER_OWNERSHIP
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_1

*** Variables ***
${EMAIL}	${SUPER_EMAIL_ADDRESS}
${PWD}		${SUPER_PASSWORD}
#${TRANSFERTOTHESAMECOMPANY}	Transfer device failed. Failed to transfer ownership. Device already under Company account.
${TRANSFERTOTHESAMECOMPANYSUCCESS}	Successfully transferred device ownership.
${TRANSFER_OWNERSHIP_USER}
${TRANSFER_OWNERSHIP_PASSWORD}
${SERIALNUMBER}
${COMPANY}
${LOCATOR}	//*[@id="sa-devices-general-table-1"]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
${ROWNUMBER}
${DEVICE_ASSOCIATE_SUCCESS}	Device associated
${APPROVE_DENIED_SUCCESS}	Transfer request for devices are denied
${APPROVEED_SUCCESS}	Transfer request for devices are approved

*** Test Cases ***
Transfer Device Ownership - through ZPE Cloud
	[Tags]	TRANSFER_OWNERSHIP_VIA_ZPECLOUD
	SUITE:Associate to Transfer Ownership Company
	SUITE:Check Device Under Transfer Ownership Company
	[Teardown]	SUITE:Teardown

Transfer Device Ownership - through ZPE Cloud - Deny
	SUITE:Associate to Transfer Ownership Company - Deny
	[Teardown]	Close All Browsers

Transfer Device Ownership - through ZPE Cloud - to the same company
	SUITE:Associate to Transfer Ownership Company - To The Same Company
	[Teardown]	Close All Browsers

Transfer Device Ownership - through Device - to the same company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Settings::Open Tab
	${CUSTOMERCODE}=	Get Value	ca-setgen-customer-code-input
	Click Element	//*[@id="ca-setgen-password-toggle-btn"]/span[1]
	${ENROLLMENTKEY}=	Get Value	filled-adornment-password
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid	${USERNAMENG}	${PASSWORDNG}
	GUI::Open System tab
	Sleep	5
	Click Element	xpath=(//a[contains(text(),'Toolkit')])[2]
	Wait Until Page Contains Element	icon_cloudEnrollment	30s
	Click Element	icon_cloudEnrollment
	Wait Until Page Contains	Cloud Enrollment	30s
	Wait Until Page Contains Element	url	30s
	Sleep	5
	Clear Element Text	url
	Press Keys	url	CTRL+a+BACKSPACE
	Input Text	url	${ZPECLOUD_HOMEPAGE}
	Input Text	customer_code	${CUSTOMERCODE}
	Input Text	enrollment_key	${ENROLLMENTKEY}
	Click Element	saveButton
	Wait Until Keyword Succeeds	30	30	Wait Until Page Contains	${TRANSFERTOTHESAMECOMPANYSUCCESS}
	[Teardown]	Close All Browsers

Transfer Device Ownership - through Device - Transfer Ownserhip company
	GUI::ZPECloud::Basic::Open And Login	${TRANSFER_OWNERSHIP_USER}	${TRANSFER_OWNERSHIP_PASSWORD}
	GUI::ZPECloud::Settings::Open Tab
	${CUSTOMERCODE}=	Get Value	ca-setgen-customer-code-input
	Click Element	//*[@id="ca-setgen-password-toggle-btn"]/span[1]
	${ENROLLMENTKEY}=	Get Value	filled-adornment-password
	Close All Browsers
	GUI::Basic::Open And Login Nodegrid	${USERNAMENG}	${PASSWORDNG}
	GUI::Open System tab
	Sleep	5
	Click Element	xpath=(//a[contains(text(),'Toolkit')])[2]
	Wait Until Page Contains Element	icon_cloudEnrollment	30s
	Click Element	icon_cloudEnrollment
	Wait Until Page Contains	Cloud Enrollment	30s
	Sleep	5
	Clear Element Text	url
	Press Keys	url	CTRL+a+BACKSPACE
	Input Text	url	${ZPECLOUD_HOMEPAGE}
	Input Text	customer_code	${CUSTOMERCODE}
	Input Text	enrollment_key	${ENROLLMENTKEY}
	Click Element	saveButton
	Wait Until Keyword Succeeds	30	30		Wait Until Page Contains	${TRANSFERTOTHESAMECOMPANYSUCCESS}
	Close All Browsers
	GUI::ZPECloud::Basic::Open And Login	${TRANSFER_OWNERSHIP_USER}	${TRANSFER_OWNERSHIP_PASSWORD}
	GUI::ZPECloud::Devices::Available::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SERIALNUMBER}
	Wait Until Page Contains	${SERIALNUMBER}	30s
	Element Should Contain	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr[1]/td[3]	${SERIALNUMBER}
	[Teardown]	SUITE:Teardown

Transfer Device Ownership - through Console - Transfer Ownserhip company
	GUI::ZPECloud::Basic::Open And Login	${TRANSFER_OWNERSHIP_USER}	${TRANSFER_OWNERSHIP_PASSWORD}
	GUI::ZPECloud::Settings::Open Tab
	${CUSTOMERCODE}=	Get Value	ca-setgen-customer-code-input
	Click Element	//*[@id="ca-setgen-password-toggle-btn"]/span[1]
	${ENROLLMENTKEY}=	Get Value	filled-adornment-password
	Close All Browsers
	CLI:Connect As Root	${HOST}	${PASSWORDNG}
	${ENROLL}=	Set Variable	zpe_cloud_enroll -c ${CUSTOMERCODE} -k ${ENROLLMENTKEY} -u ${ZPECLOUD_HOMEPAGE}
	Write	${ENROLL}
	${OUTPUT}=	Read Until	.
	Should Contain Any	${OUTPUT}	${TRANSFERTOTHESAMECOMPANYSUCCESS}
	[Teardown]	SUITE:Teardown

*** Keywords ***
SUITE:Setup
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	GUI::ZPECloud::Dashboard::Access::Open Tab
	Input Text	search-input	${SERIALNUMBER}
	Wait Until Keyword Succeeds	30	15	Page Should Contain Element	xpath=//td[contains(.,'${SERIALNUMBER}')]
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SERIALNUMBER}
	Close All Browsers

SUITE:Teardown
	GUI::ZPECloud::Basic::Open And Login	${TRANSFER_OWNERSHIP_USER}	${TRANSFER_OWNERSHIP_PASSWORD}
	GUI::ZPECloud::Devices::Available::Open Tab
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains	${SERIALNUMBER}	30s
	Wait Until Page Contains Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr[1]/td[3]	30s
	Element Should Contain	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr[1]/td[3]		${SERIALNUMBER}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SERIALNUMBER}
	Wait Until Page Contains Element	${LOCATOR}	30s
	Element Should Contain	//*[@id="sa-devices-general-table-1"]/div[1]/div/table/tbody/tr[1]/td[3]	${TRANSFER_OWNERSHIP_COMPANY}
	SUITE:Associate to Official Company
	Close All Browsers
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Devices::Available::Open Tab
	Sleep	3
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait until Page Contains	${SERIALNUMBER}	30s
	Wait Until Page Contains Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr[1]/td[1]/span/input	30s
	Click Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
	Sleep	5
	Click Element	//*[@id="enroll"]
	Wait Until Keyword Succeeds	30	1	Wait Until Page Contains	Device Enrolled Successfully
	Close All Browsers

SUITE:Associate to Transfer Ownership Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains	${SERIALNUMBER}	30s
	Click Element	${LOCATOR}
	Sleep	5
	Element Should be Enabled	//*[@id="function-button-0"]
	Click Element	//*[@id="function-button-0"]
	Wait Until Page Contains Element	//*[@id="associate-company-dialog"]/div[3]/div
	Click Element	//*[@id="associate-company-dialog"]/div[3]/div/div/form/div[1]/div/div/div/div[1]/input
	Sleep	5
	Input Text	//*[@id="associate-company-dialog"]/div[3]/div/div/form/div[1]/div/div/div/div[1]/input	${TRANSFER_OWNERSHIP_COMPANY}
	Sleep	5
	Wait Until Page Contains Element	react-autowhatever-1--item-0	15s
	${COMPANYLOCATION}=	Execute Javascript	return Array.from(document.getElementById('react-autowhatever-1').children[0].getElementsByTagName('LI')).filter(li => li.textContent == '${TRANSFER_OWNERSHIP_COMPANY}')[0].id
	Click Element	${COMPANYLOCATION}
	Click Element	//*[@id="submit-btn"]
	Wait Until Page Contains	${DEVICE_ASSOCIATE_SUCCESS}	15s
	Close All Browsers

	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	SUITE::Pending approval::Open Tab
	Wait Until Page Contains Element	search-input	30s
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains	${SERIALNUMBER}	30s
	Wait Until Page Contains Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[3]/div	30s
	Element Should Contain	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[3]/div	${SERIALNUMBER}
	Click Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Wait Until Page Contains Element	//*[@id="function-button-0"]	15s
	Wait Until Keyword Succeeds	30	6	SUITE:APPROVING_DEVICE_TRANSFER
	Close All Browsers

SUITE:Check Device Under Transfer Ownership Company
	GUI::ZPECloud::Basic::Open And Login	${TRANSFER_OWNERSHIP_USER}	${TRANSFER_OWNERSHIP_PASSWORD}
	GUI::ZPECloud::Devices::Available::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SERIALNUMBER}
	Wait Until Page Contains	${SERIALNUMBER}	30s
	Wait Until Page Contains Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr[1]/td[3]		15s
	Element Should Contain	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr[1]/td[3]	${SERIALNUMBER}

SUITE:Associate to Transfer Ownership Company - To The Same Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains	${SERIALNUMBER}	30s
	Click Element	${LOCATOR}
	Element Should be Enabled	//*[@id="function-button-0"]
	Click Element	//*[@id="function-button-0"]
	Wait Until Page Contains Element	//*[@id="associate-company-dialog"]/div[3]/div
	Click Element	//*[@id="associate-company-dialog"]/div[3]/div/div/form/div[1]/div/div/div/div[1]/input
	Sleep	5
	Input Text	//*[@id="associate-company-dialog"]/div[3]/div/div/form/div[1]/div/div/div/div[1]/input	${COMPANY}
	sleep	5s
	Wait Until Page Contains Element	react-autowhatever-1--item-0	15s
	${COMPANYLOCATION}=	Execute Javascript	return Array.from(document.getElementById('react-autowhatever-1').children[0].getElementsByTagName('LI')).filter(li => li.textContent == '${COMPANY}')[0].id
	Click Element	${COMPANYLOCATION}
	Click Element	//*[@id="submit-btn"]
	Wait Until Page Contains	The device is already associated
	Close All Browsers

SUITE::Pending approval::Open Tab
	Wait Until Page Contains Element	//*[@id="ca-header-tab-2-2"]	15s
	Click Element	//*[@id="ca-header-tab-2-2"]

SUITE:Associate to Transfer Ownership Company - Deny
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains	${SERIALNUMBER}	30s
	Click Element	${LOCATOR}
	Element Should be Enabled	//*[@id="function-button-0"]
	Click Element	//*[@id="function-button-0"]
	Wait Until Page Contains Element	//*[@id="associate-company-dialog"]/div[3]/div
	Click Element	//*[@id="associate-company-dialog"]/div[3]/div/div/form/div[1]/div/div/div/div[1]/input
	Sleep	5
	Input Text	//*[@id="associate-company-dialog"]/div[3]/div/div/form/div[1]/div/div/div/div[1]/input	${TRANSFER_OWNERSHIP_COMPANY}
	Sleep	5
	${COMPANYLOCATION}=	Execute Javascript	return Array.from(document.getElementById('react-autowhatever-1').children[0].getElementsByTagName('LI')).filter(li => li.textContent == '${TRANSFER_OWNERSHIP_COMPANY}')[0].id
	Sleep	5
	Click Element	${COMPANYLOCATION}
	Click Element	//*[@id="submit-btn"]
	Wait Until Page Contains	${DEVICE_ASSOCIATE_SUCCESS}	15s
	Close All Browsers
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	SUITE::Pending approval::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains	${SERIALNUMBER}	30s
	Wait Until Page Contains Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[3]/div
	Element Should Contain	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[3]/div	${SERIALNUMBER}
	Click Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Wait Until Keyword Succeeds	30	6	SUITE:APPROVE_DENIED
	Close All Browsers
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Dashboard::Access::Open Tab
	Sleep	3
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Element Should Contain	xpath=//td[contains(.,'${SERIALNUMBER}')]	${SERIALNUMBER}
	Close All Browsers

SUITE:APPROVE_DENIED
	Click Element	//*[@id="function-button-1"]
	Wait Until Page Contains	${APPROVE_DENIED_SUCCESS}	15s

SUITE:Associate to Official Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SERIALNUMBER}
	Wait Until Page Contains	${SERIALNUMBER}	30s
	Click Element	${LOCATOR}
	Element Should be Enabled	//*[@id="function-button-0"]
	Click Element	//*[@id="function-button-0"]
	Wait Until Page Contains Element	//*[@id="associate-company-dialog"]/div[3]/div
	Click Element	//*[@id="associate-company-dialog"]/div[3]/div/div/form/div[1]/div/div/div/div[1]/input
	Sleep	5
	Input Text	//*[@id="associate-company-dialog"]/div[3]/div/div/form/div[1]/div/div/div/div[1]/input	${COMPANY}
	sleep	5s
	${COMPANYLOCATION}=	Execute Javascript	return Array.from(document.getElementById('react-autowhatever-1').children[0].getElementsByTagName('LI')).filter(li => li.textContent == '${COMPANY}')[0].id
	Click Element	${COMPANYLOCATION}
	Click Element	//*[@id="submit-btn"]
	Wait Until Page Contains	${DEVICE_ASSOCIATE_SUCCESS}	15s
	Close All Browsers
	GUI::ZPECloud::Basic::Open And Login	${TRANSFER_OWNERSHIP_USER}	${TRANSFER_OWNERSHIP_PASSWORD}
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	SUITE::Pending approval::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains	${SERIALNUMBER}	30s
	Wait Until Page Contains Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr[1]/td[3]	15s
	Element Should Contain	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr[1]/td[3]	${SERIALNUMBER}
	Click Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Wait Until Page Contains Element	//*[@id="function-button-0"]
	Wait Until Keyword Succeeds	30	6	SUITE:APPROVING_DEVICE_TRANSFER

SUITE:APPROVING_DEVICE_TRANSFER
	Click Element	//*[@id="function-button-0"]
	Wait Until Page Contains	${APPROVEED_SUCCESS}	15s