*** Settings ***
Documentation	Tests for enrollment device on cloud
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	ENROLLMENT	UNENROLLMENT	NON-CRITICAL
Force Tags	GUI	ZPECLOUD	${BROWSER}	DEVICE	PART_1

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${CUSTOMERCODE}
${ENROLLMENTKEY}
${ENROLLMENTSUCCESS}	Enrollment process successful!

*** Test Cases ***
Enroll the Device by Web
	SUITE:Unenroll device
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	${Is_Checkbox_Selected}=	Run Keyword And Return Status	Checkbox Should Be Selected	//*[@id="cloud_enable"]
	Run Keyword If	'${Is_Checkbox_Selected}'== 'False'	Run Keywords	GUI::Cloud::Enable cloud on NG
	GUI::Basic::Spinner Should Be Invisible
	GUI::Open System tab
	Sleep	2
	Click Element	xpath=(//a[contains(text(),'Toolkit')])[2]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	icon_cloudEnrollment	30s
	Click Element	icon_cloudEnrollment
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	url	30s
	Clear Element Text	url
	Press Keys	url	CTRL+a+BACKSPACE
	Input Text	url	${ZPECLOUD_HOMEPAGE}
	Input Text	customer_code	${CUSTOMERCODE}
	Input Text	enrollment_key	${ENROLLMENTKEY}
	Click Element	saveButton
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	${ENROLLMENTSUCCESS}	30s

Enroll device by CLI
	SUITE:Unenroll device
	CLI:Connect As Root	${HOST}	${PASSWORDNG}
	${ENROLL}=	Set Variable	zpe_cloud_enroll -c ${CUSTOMERCODE} -k ${ENROLLMENTKEY} -u ${ZPECLOUD_HOMEPAGE}
	Write	${ENROLL}
	sleep	5s
	${OUTPUT}=	Read Until	!
	Should Contain Any	${OUTPUT}	Enrollment process successful!

Move from available to enrolled
	GUI::ZPECloud::Basic::Open and Login	${EMAIL}	${PWD}	${PASSWORD}
	GUI::ZPECloud::Devices::Open Tab
	Wait Until Page Contains Element	//button[contains(.,'AVAILABLE')]
	Click Element	//button[contains(.,'AVAILABLE')]
	Wait Until Keyword Succeeds	20x	1s	Page Should Contain	${SERIALNUMBER}
	Click Element	//td[contains(.,'${SERIALNUMBER}')]/../td[1]
	Click Element	//button[contains(.,'Enroll')]
	Wait Until Page Contains	Device Enrolled Successfully

*** Keywords ***
SUITE:Setup
	SUITE:Get Device Enrolment Info on QA Instance
	GUI::ZPECloud::Settings::Open Tab
	${Is_Checkbox_Selected}=	Run Keyword And Return Status	Checkbox Should Be Selected	//*[@id="ca-setgen-enable-device-enrollment-checkbox"]
	Run Keyword If	'${Is_Checkbox_Selected}'== 'False'	Run Keyword	SUITE:select cloud enable checkbox
	GUI::ZPECloud::Check and update the zpecloud serial number variable if needed	${USERNAMENG}	${PASSWORDNG}

SUITE:Teardown
	Close All Browsers

SUITE:Unenroll device
	Wait Until Keyword Succeeds	3x	1s	CLI:Connect As Root	${HOST}	${PASSWORDNG}
	${UNENROLL}=	Set Variable	zpe_cloud_enroll -c ${CUSTOMERCODE} -k ${ENROLLMENTKEY} -u ${ZPECLOUD_HOMEPAGE} -s
	Set Default Configuration	timeout=2m
	Set Client Configuration	timeout=2m
	Set Client Configuration	prompt=(yes, no):
	CLI:Write	${UNENROLL}
	Set Client Configuration	prompt=#
	${OUTPUT}=	CLI:Write	yes
	Set Client Configuration	prompt=(yes, no):
	#TPM Clean
	${STATUS}	Evaluate NG Versions: ${NGVERSION} <= 5.2
	Run Keyword If	${STATUS}	CLI:Write	zpe_cloud_enroll -c c -k k -u ${ZPECLOUD_HOMEPAGE} -s
	...	ELSE	CLI:Write	zpe_cloud_enroll --enroll-clean -u ${ZPECLOUD_HOMEPAGE}
	Set Client Configuration	prompt=#
	CLI:Write	yes
	${HAS_RETRY_OR_IN_PROGRESS}	Run Keyword And Return Status	Should Contain Any	${OUTPUT}
	...	Please check it and retry the enrollment	Enrollment process already in progress
	IF	${HAS_RETRY_OR_IN_PROGRESS}
		Sleep	2m
		Set Client Configuration	prompt=(yes, no):
		CLI:Write	${UNENROLL}
		Set Client Configuration	prompt=#
		${OUTPUT}=	CLI:Write	yes
	END
	Should Contain Any	${OUTPUT}	Unenrollment process successful!	Device is not registered

SUITE:Get Device Enrolment Info on QA Instance
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Settings::Open Tab
	${CUSTOMERCODE}=	Get Value	ca-setgen-customer-code-input
	${ENROLLMENTKEY}=	Get Value	filled-adornment-password
	${CUSTOMERCODE}=	Set Suite Variable	${CUSTOMERCODE}
	${ENROLLMENTKEY}=	Set Suite Variable	${ENROLLMENTKEY}
	[return]	${CUSTOMERCODE}	${ENROLLMENTKEY}

SUITE:select cloud enable checkbox
	Select Checkbox	//*[@id="ca-setgen-enable-device-enrollment-checkbox"]
	Sleep	2s
	Click Element		//*[@id="ca-setgen-set-company-key-btn"]/span[1]
