*** Settings ***
Documentation	Tests for on-premise validation testing
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	ON-PREMISE
Force Tags	GUI	ZPECLOUD	${BROWSER}	DEVICE	PART_1

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${SUPER_EMAIL_ADDRESS}
${PWD}	${SUPER_PASSWORD}
${VALIDINFO}	On-Premises instance is valid
${INVALIDINFO}	Invalid credentials provided
${SAVED}		On-Premises setting updated successfully
${SERIALNUMBER}
${COMPANY}
${CUSTOMERCODE}
${ENROLLMENTKEY}
${ONPREMISEEMAIL}		zpeonpremises@gmail.com
${ONPREMISEPASSWORD}	.ZPE5ystems!2020
${ONPREMISEURL}		https://test-zpecloud.com/
${INVALIDINFORMATION}	INVALIDINFORMATION

*** Test Cases ***
On Premise Configuration - validate - valid information
	SUITE:Get Device Enrolment Info
	SUITE:Validate Device Enrolment Info Valid Information

On Premise Configuration - validate - invalid information
	SUITE:Validate Device Enrolment Info Invalid Information

On Premise Configuration - save Configuration
	SUITE:Get Device Enrolment Info
	SUITE:Validate Device Enrolment Info Valid Information
	GUI::ZPECloud::Basic::Click Button	save
	Wait Until Page Contains	${SAVED}	15s

*** Keywords ***
SUITE:Setup
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud

SUITE:Teardown
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	(//button[normalize-space()='On-Premises'])[1]	timeout=1m
	Click Element	(//button[normalize-space()='On-Premises'])[1]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	(//input[@id='ca-setop-enable-device-enrollment-checkbox'])[1]
	${IS_ON-PREMISES_CHECKBOX_SELECTED}=	Run Keyword And Return Status	Checkbox Should Be Selected	(//input[@id='ca-setop-enable-device-enrollment-checkbox'])[1]
	Run Keyword If	${IS_ON-PREMISES_CHECKBOX_SELECTED}	GUI::ZPECloud::Settings::Disable On-Premises Enrollment and save
	Close All Browsers

SUITE:Get Device Enrolment Info
	GUI::ZPECloud::Basic::Open ZPE Cloud	${ONPREMISEURL}	${BROWSER}
	GUI::ZPECloud::Basic::Login	${ONPREMISEEMAIL}	${ONPREMISEPASSWORD}
	GUI::ZPECloud::Settings::Open Tab
	${CUSTOMERCODE}=	Get Value	ca-setgen-customer-code-input
#	Click Element	//*[@id="ca-setop-2_enable-device-enrollment-checkbox"]
	${ENROLLMENTKEY}=	Get Value	filled-adornment-password
	${CUSTOMERCODE}=	Set Suite Variable	${CUSTOMERCODE}
	${ENROLLMENTKEY}=	Set Suite Variable	${ENROLLMENTKEY}
	Close All Browsers

SUITE:Validate Device Enrolment Info Valid Information
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Settings::Open Tab
	${menu}=	GUI::ZPECloud::Basic::Get Main Menu
	${return}=	GUI::ZPECloud::Basic::Click Button	on-premises
#	Wait Until Keyword Succeeds	30	5	Click Element	on-premises
	Wait Until Element Is Enabled	ca-setop-enable-device-enrollment-checkbox	15s
	${IS_NOTSELECTED}=	Run Keyword And return Status	Checkbox Should Not Be Selected	ca-setop-enable-device-enrollment-checkbox
	Run Keyword If	${IS_NOTSELECTED} == True	Select Checkbox	ca-setop-enable-device-enrollment-checkbox
	Wait Until Element Is Enabled	ca-setop-premise-url-input	15s
	Press Keys	//*[@id="ca-setop-premise-url-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="ca-setop-premise-url-input"]
	Input Text	ca-setop-premise-url-input	${ONPREMISEURL}
	Press Keys	//*[@id="ca-setop-customer-code-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="ca-setop-customer-code-input"]
	Input Text	ca-setop-customer-code-input	${CUSTOMERCODE}
	Press Keys	//*[@id="ca-setop-enroll-key-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="ca-setop-enroll-key-input"]
	Input Text	ca-setop-enroll-key-input	${ENROLLMENTKEY}
#	Clear Element Text	//*[@id="ca-setop-premise-url-input"]
#	Clear Element Text	//*[@id="ca-setop-customer-code-input"]
#	Clear Element Text	//*[@id="ca-setop-enroll-key-input"]
	Sleep	2
	Wait Until Keyword Succeeds	30	5	SUITE:Validate Info

SUITE:Validate Info
	Click Element	xpath=//span[contains(.,'Validate')]
#	GUI::ZPECloud::Basic::Click Button		Validate
	Wait Until Page Contains	${VALIDINFO}	15s

SUITE:Validate Device Enrolment Info Invalid Information
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Settings::Open Tab
	${menu}=	GUI::ZPECloud::Basic::Get Main Menu
	${return}=	GUI::ZPECloud::Basic::Click Button	on-premises
#	Wait Until Keyword Succeeds	30	5	Click Element	on-premises
	Wait Until Element Is Enabled	ca-setop-enable-device-enrollment-checkbox
	${IS_NOTSELECTED}=	Run Keyword And return Status	Checkbox Should Not Be Selected	ca-setop-enable-device-enrollment-checkbox
	Run Keyword If	${IS_NOTSELECTED} == True	Select Checkbox	ca-setop-enable-device-enrollment-checkbox
	Wait Until Element Is Enabled	ca-setop-premise-url-input	15s
	Press Keys	//*[@id="ca-setop-premise-url-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="ca-setop-premise-url-input"]
	Input Text	ca-setop-premise-url-input	${ONPREMISEURL}
#	Input Text	ca-setop-customer-code-input	${CUSTOMERCODE}
	Press Keys	//*[@id="ca-setop-enroll-key-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="ca-setop-enroll-key-input"]
	Input Text	ca-setop-enroll-key-input	${INVALIDINFORMATION}
	Sleep	2
	Click Element	xpath=//span[contains(.,'Validate')]
#	GUI::ZPECloud::Basic::Click Button		Validate
	Wait Until Page Contains	${INVALIDINFO}	15s