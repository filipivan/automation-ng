# <A NAME="main_menu" >ZPE Cloud - Test Plan</A>
---
- <A HREF="#menu_sa" >Super Admin</A>
- <A HREF="#menu_ca" >Customer Admin</A>
- <A HREF="#menu_ga" >Group Admin</A>
---

### <A NAME="menu_sa" >ZPE Cloud - Super Admin</A>
<A HREF="#main_menu" >Back</A>
 
- <A HREF="#dashboard_sa" >Dashboard</A>
- <A HREF="#devices_sa" >Devices</A>
- <A HREF="#companies_sa" >Companies</A>
- <A HREF="#users_sa" >Users</A>
- <A HREF="#versions_sa" >Versions</A>
---

#### <A NAME="dashboard_sa" >Dashboard - Super Admin</A>
<A HREF="#menu_sa" >Back</A>  

| **Test ID** | **Test summary** | **ETA** | **Priority** | **Status** | 
| :---: | :--- | :---: | :---: | :---: |
| DB-001 | MAP - Compare number of sites on the map with the graph, sites page and mouse over | 4 hours | HIGH | Scheduled | 

#### <A NAME="devices_sa" >Devices - Super Admin</A>
<A HREF="#menu_sa" >Back</A>  

| **Test ID** | **Test summary** | **ETA** | **Priority** | **Status** | 
| :---: | :--- | :---: | :---: | :---: | 
| DEV-001 | Compare number of devices with number in range | 4 hours | MEDIUM | Scheduled | 
| DEV-002 | Enable buttons if select device | 4 hours | MEDIUM | Scheduled |
| DEV-003 | Associate device clicking associate_company button | 4 hours | HIGH | Scheduled |
| DEV-004 | Associate device pressing Enter should submit form| 4 hours | HIGH | Scheduled |
| DEV-005 | Revoke device clicking revoke button | 4 hours | HIGH | Scheduled |
| DEV-006 | Revoke device pressing Enter should submit form | 4 hours | HIGH | Scheduled |
| DEV-007 | Blacklist device clicking blacklist button | 4 hours | HIGH | Scheduled |
| DEV-008 | Blacklist device pressing Enter should submit form | 4 hours | HIGH | Scheduled |
| DEV-009 | Transfer Key clicking PO_transfer_key button | 4 hours | HIGH | Scheduled |
| DEV-010 | Transfer Key pressing Enter should submit form | 4 hours | HIGH | Scheduled |
| DEV-011 | Search for serial/model/company/part number | 4 hours | MEDIUM | Scheduled |
| DEV-012 | Navigator buttons | 4 hours | LOW | Scheduled |
| DEV-013 | Row per page | 4 hours | LOW | Scheduled |
| DEV-014 | Devices details - Show details information when selecting device | 4 hours | MEDIUM | Scheduled |
| DEV-014 | Devices details - Check details information | 4 hours | HIGH | Scheduled |
| DEV-015 | Devices details - Compare quantity selected with quantity show | 4 hours | MEDIUM | Scheduled |
| DEV-016 | Devices details - Navigator buttons | 4 hours | LOW | Scheduled |
| DEV-017 | Devices details - Row per page | 4 hours | LOW | Scheduled |


#### <A NAME="companies_sa" >Companies - Super Admin</A>
<A HREF="#menu_sa" >Back</A>  

| **Test ID** | **Test summary** | **ETA** | **Priority** | **Status** | 
| :---: | :--- | :---: | :---: | :---: | 
| COMP-001 | Compare number of companies with number in range | 4 hours | MEDIUM | Scheduled | 
| COMP-002 | Enable buttons if select company | 4 hours | MEDIUM | Scheduled |
| COMP-003 | Add company clicking add button | 4 hours | HIGH | Scheduled |
| COMP-004 | Add company pressing Enter should submit form| 4 hours | HIGH | Scheduled |
| COMP-005 | Edit company clicking edit button | 4 hours | HIGH | Scheduled |
| COMP-006 | Edit company pressing Enter should submit form | 4 hours | HIGH | Scheduled |
| COMP-007 | Delete company clicking delete button | 4 hours | HIGH | Scheduled |
| COMP-008 | Delete company pressing Enter should submit form | 4 hours | HIGH | Scheduled |
| COMP-009 | Search for company | 4 hours | MEDIUM | Scheduled |
| COMP-010 | Navigator buttons | 4 hours | LOW | Scheduled |
| COMP-011 | Row per page | 4 hours | LOW | Scheduled |

#### <A NAME="users_sa">Users - Super Admin</A>
<A HREF="#menu_sa" >Back</A>  

| **Test ID** | **Test summary** | **ETA** | **Priority** | **Status** | 
| :---: | :--- | :---: | :---: | :---: | 
| USER-001 | General - Compare number of users with number in range | 4 hours | MEDIUM | Scheduled | 
| USER-002 | General - Enable buttons if select user | 4 hours | HIGH | Scheduled |
| USER-003 | General - Add user clicking add button | 4 hours | HIGH | Scheduled |
| USER-004 | General - Add user pressing Enter should submit form| 4 hours | HIGH | Scheduled |
| USER-005 | General - Edit user clicking edit button | 4 hours | HIGH | Scheduled |
| USER-006 | General - Edit user pressing Enter should submit form | 4 hours | HIGH | Scheduled |
| USER-007 | General - Delete user clicking delete button | 4 hours | HIGH | Scheduled |
| USER-008 | General - Delete user pressing Enter should submit form | 4 hours | HIGH | Scheduled |
| USER-009 | General - Search for user | 4 hours | MEDIUM | Scheduled |
| USER-010 | General - Navigator buttons | 4 hours | LOW | Scheduled |
| USER-011 | General - Row per page | 4 hours | LOW | Scheduled |
| USER-012 | Pending Approval - Compare number of users with number in range | 4 hours | MEDIUM | Scheduled | 
| USER-013 | Pending Approval - Enable buttons if select user | 4 hours | HIGH | Scheduled |
| USER-014 | Pending Approval - Search for pending user | 4 hours | MEDIUM | Scheduled |
| USER-015 | Pending Approval - Navigator buttons | 4 hours | LOW | Scheduled |
| USER-016 | Pending Approval - Row per page | 4 hours | LOW | Scheduled |
| USER-017 | Pending Approval - Approve user clicking Approve button | 4 hours | HIGH | Scheduled |
| USER-018 | Pending Approval - Approve user pressing Enter should submit form| 4 hours | HIGH | Scheduled |
| USER-019 | Pending Approval - Deny user clicking Deny button | 4 hours | HIGH | Scheduled |
| USER-020 | Pending Approval - Deny user pressing Enter should submit form | 4 hours | HIGH | Scheduled |

#### <A NAME="versions_sa" >Versions - Super Admin</A>
<A HREF="#menu_sa" >Back</A>  

| **Test ID** | **Test summary** | **ETA** | **Priority** | **Status** | 
| :---: | :--- | :---: | :---: | :---: | 
| VER-001 | General - Compare number of versions with number in range | 4 hours | MEDIUM | Scheduled | 
| VER-002 | General - Enable buttons if select version | 4 hours | HIGH | Scheduled |
| VER-003 | General - Add version clicking add button | 4 hours | HIGH | Scheduled |
| VER-004 | General - Add version pressing Enter should submit form| 4 hours | HIGH | Scheduled |
| VER-005 | General - Delete version clicking delete button | 4 hours | HIGH | Scheduled |
| VER-006 | General - Delete version pressing Enter should submit form | 4 hours | HIGH | Scheduled |
| VER-007 | General - Search for release | 4 hours | MEDIUM | Scheduled |
| VER-008 | General - Navigator buttons | 4 hours | LOW | Scheduled |
| VER-009 | General - Row per page | 4 hours | LOW | Scheduled |

---

### <A NAME="menu_ca" >ZPE Cloud - Customer Admin</A>
<A HREF="#main_menu" >Back</A>
 
- <A HREF="#login_ca" >Login</A>
- <A HREF="#dashboard_ca" >Dashboard</A>
- <A HREF="#sites_ca" >Sites</A>
- <A HREF="#groups_ca" >Groups</A>
- <A HREF="#devices_ca" >Devices</A>
- <A HREF="#users_ca" >Users</A>
- <A HREF="#profiles_ca" >Profiles</A>
- <A HREF="#logs_ca" >Logs</A>
- <A HREF="#settings_ca" >Settings</A>
---

#### <A NAME="login_ca" >Login - Customer Admin</A>
<A HREF="#menu_ca" >Back</A>                                       

| **Test ID** | **Test summary** | **ETA** | **Priority** | **Status** | 
| :---: | :--- | :---: | :---: | :---: | 
| LOGIN-001 | Login clicking sign_in button using username | 3 hours | HIGH | Done | 
| LOGIN-002 | Login pressing Enter should submit form using username| 3 hours | HIGH | Done |
| LOGIN-003 | Login clicking sign_in button using e-mail | 2 hours | HIGH | Scheduled |
| LOGIN-004 | Login pressing Enter should submit form using e-mail | 2 hours | HIGH | Scheduled |
| LOGIN-005 | Login clicking sign_in button using wrong username | 2 hours | MEDIUM | Scheduled | 
| LOGIN-006 | Login pressing Enter should submit form using wrong username| 2 hours | MEDIUM | Scheduled |
| LOGIN-007 | Login clicking sign_in button using wrong e-mail | 2 hours | MEDIUM | Scheduled |
| LOGIN-008 | Login pressing Enter should submit form using wrong e-mail | 2 hours | MEDIUM | Scheduled |
| LOGIN-009 | Login clicking sign_in button using wrong password | 2 hours | MEDIUM | Scheduled |
| LOGIN-010 | Login pressing Enter should submit form using wrong password | 2 hours | MEDIUM | Scheduled | 
| LOGIN-011 | Login clicking sign_in button using wrong username and password | 2 hours | LOW | Scheduled |
| LOGIN-012 | Login pressing Enter should submit form using wrong e-mail and password | 2 hours | LOW | Scheduled |
| LOGIN-013 | Login clicking sign_in button using username field blank | 2 hours | HIGH | Scheduled |
| LOGIN-014 | Login pressing Enter should submit form using username field blank | 2 hours | HIGH | Scheduled |
| LOGIN-015 | Login clicking sign_in button using password field blank | 2 hours | HIGH | Scheduled |
| LOGIN-016 | Login pressing Enter should submit form using password field blank | 2 hours | HIGH | Scheduled |
| LOGIN-017 | Display password clicking view button | 2 hours | MEDIUM | Scheduled |
| LOGIN-018 | Display password pressing Enter should submit form | 2 hours | MEDIUM | Scheduled |
| LOGIN-019 | Click forgot password and display Forgot Password window clicking forgot_password button | 3 hours | HIGH | Scheduled |
| LOGIN-020 | Click forgot password and display Forgot Password window pressing Enter should submit form | 3 hours | HIGH | Scheduled |
| LOGIN-021 | Test if it's a valid email clicking send_reset_request button | 2 hours | MEDIUM | Scheduled |
| LOGIN-022 | Test if it's a valid email pressing Enter should submit form | 2 hours | MEDIUM | Scheduled |
| LOGIN-023 | Test if request is sent clicking send_reset_request button | 3 hours | MEDIUM | Scheduled |
| LOGIN-024 | Test if request is sent pressing Enter should submit form | 3 hours | MEDIUM | Scheduled |
| LOGIN-025 | Test if window is closed clicking close button | 2 hours | LOW | Scheduled |
| LOGIN-026 | Test if window is closed pressing Enter should submit form | 2 hours | LOW | Scheduled |

#### <A NAME="dashboard_ca" >Dashboard - Customer Admin</A>
<A HREF="#menu_ca" >Back</A>  

| **Test ID** | **Test summary** | **ETA** | **Priority** | **Status** | 
| :---: | :--- | :---: | :---: | :---: | 
| DB-001 | MAP - Compare number of sites on the map with the graph, sites page and mouse over | 4 hours | HIGH | Scheduled | 
| DB-002 | MAP - Compare number of online sites on the map with the graph, sites page and mouse over | 12 hours | MEDIUM | Scheduled |
| DB-003 | MAP - Compare number of offline sites on the map with the graph, sites page and mouse over | 12 hours | MEDIUM | Scheduled |
| DB-004 | MAP - Compare number of partial sites on the map with the graph, sites page and mouse over | 12 hours | MEDIUM | Scheduled | 
| DB-005 | MAP - Add new site and compare number of sites on the map with the graph, sites page and mouse over | 3 hours | HIGH | Scheduled |
| DB-006 | MAP - Delete a site and compare number of sites on the map with the graph, site page and mouse over | 3 hours | HIGH | Scheduled |
| DB-007 | MAP - Compare number of devices on the graph with devices page and mouse over | 12 hours | HIGH | Scheduled |
| DB-008 | MAP - Compare number of online devices on the graph with devices page and mouse over | 12 hours | MEDIUM | Scheduled |
| DB-009 | MAP - Compare number of offline devices on the graph with devices page | 12 hours | MEDIUM | Scheduled |
| DB-010 | MAP - Compare number of never connected devices on the graph with devices page | 12 hours | MEDIUM | Scheduled |
| DB-011 | MAP - Add new device and compare number of devices on the graph with devices page and mouse over | 12 hours | HIGH | Scheduled |
| DB-012 | MAP - Disassociate a device and compare number of devices on the graph with devices page and mouse over | 12 hours | MEDIUM | Scheduled |
| DB-013 | MAP - Compare site name and address of the mouse over with sites page | 12 hours | LOW | Scheduled |
| DB-014 | MAP - Test zoom buttons | 12 hours | LOW | Scheduled |
| DB-015 | CELLULAR DATA - If the confirm button only enables to enter the period | 3 hours | HIGH | Scheduled |
| DB-016 | CELLULAR DATA - Select device, period pressing Enter should submit form | 3 hours | HIGH | Scheduled |
| DB-017 | CELLULAR DATA - Select device, period clicking Confirm button | 3 hours | HIGH | Scheduled |
| DB-018 | CELLULAR DATA - Compare model, imei and sim card information with devices page | 6 hours | HIGH | Scheduled |
| DB-019 | CELLULAR DATA - Confirm graphics are loaded | 6 hours | HIGH | Scheduled |
| DB-020 | CELLULAR DATA - If the button confirm only enables to enter the periods | 3 hours | MEDIUM | Scheduled |
| DB-021 | CELLULAR DATA - Select the periods pressing Enter should submit form | 3 hours | MEDIUM | Scheduled |
| DB-022 | CELLULAR DATA - Select the periods clicking Confirm button | 3 hours | MEDIUM | Scheduled |
| DB-023 | CELLULAR DATA - Confirm sim card graphics are loaded | 3 hours | MEDIUM | Scheduled |

#### <A NAME="sites_ca" >Sites - Customer Admin</A>
<A HREF="#menu_ca" >Back</A>  

| **Test ID** | **Test summary** | **ETA** | **Priority** | **Status** | 
| :---: | :--- | :---: | :---: | :---: | 
| ST-001 | Add site clicking add button | 3 hours | HIGH | Scheduled | 
| ST-002 | Add site pressing Enter should submit form | 3 hours | HIGH | Scheduled |
| ST-003 | Edit site clicking edit button | 3 hours | HIGH | Scheduled |
| ST-004 | Edit site pressing Enter should submit form | 3 hours | HIGH | Scheduled |
| ST-005 | Delete site clicking delete button | 3 hours | HIGH | Scheduled | 
| ST-006 | Delete site pressing Enter should submit form | 3 hours | HIGH | Scheduled |
| ST-007 | Search for site/address | 3 hours | MEDIUM | Scheduled |
| ST-008 | Navigation buttons | 4 hours | LOW | Scheduled |
| ST-009 | Row per page | 3 hours | LOW | Scheduled |
| ST-010 | Compare number of sites with number in range | 3 hours | HIGH | Scheduled |
| ST-011 | Compare number of devices with devices list | 4 hours | MEDIUM | Scheduled |
| ST-012 | Remove_from_site button only enables if select a device | 3 hours | HIGH | Scheduled |
| ST-013 | Remove device clicking Remove_from_site button | 3 hours | HIGH | Scheduled | 
| ST-014 | Remove device pressing Enter should submit form | 3 hours | HIGH | Scheduled |
| ST-015 | Compare device information with details device page | 6 hours | MEDIUM | Scheduled |
| ST-016 | Navigation buttons of device assigned | 6 hours | LOW | Scheduled |
| ST-017 | Row per page of device assigned | 3 hours | LOW | Scheduled |
| ST-018 | Compare number of devices assigned with number in range | 4 hours | LOW | Scheduled |
| ST-019 | Search for serial device assigned | 3 hours | LOW | Scheduled |
| ST-020 | Compare number of devices with devices list | 12 hours | HIGH | Scheduled |
| ST-021 | Assign_to_site button only enables if select a device | 3 hours | HIGH | Scheduled |
| ST-022 | Assign device clicking Assign_to_site button | 3 hours | HIGH | Scheduled | 
| ST-023 | Assign device pressing Enter should submit form | 2 hours | HIGH | Scheduled |
| ST-024 | Compare device information with details device page for unassigned | 6 hours | LOW | Scheduled |
| ST-025 | Navigation buttons of device unassigned | 12 hours | LOW | Scheduled |
| ST-026 | Row per page of device unassigned | 3 hours | LOW | Scheduled |
| ST-027 | Compare number of devices unassigned with number in range | 4 hours | LOW | Scheduled |
| ST-028 | Search for serial device unassigned | 3 hours | LOW | Scheduled |

#### <A NAME="groups_ca" >Groups - Customer Admin</A>
<A HREF="#menu_ca" >Back</A>  

| **Test ID** | **Test summary** | **ETA** | **Priority** | **Status** | 
| :---: | :--- | :---: | :---: | :---: | 
| GP-001 | Add group clicking add button | 3 hours | HIGH | Scheduled | 
| GP-002 | Add group pressing Enter should submit form | 3 hours | HIGH | Scheduled |
| GP-003 | Edit group clicking edit button | 3 hours | HIGH | Scheduled |
| GP-004 | Edit group pressing Enter should submit form | 3 hours | HIGH | Scheduled |
| GP-005 | Delete group clicking delete button | 3 hours | HIGH | Scheduled | 
| GP-006 | Delete group pressing Enter should submit form | 3 hours | HIGH | Scheduled |
| GP-007 | Search for group | 3 hours | MEDIUM | Scheduled |
| GP-008 | Navigation buttons | 6 hours | LOW | Scheduled |
| GP-009 | Row per page | 3 hours | LOW | Scheduled |
| GP-010 | Compare number of groups with number in range | 4 hours | HIGH | Scheduled |
| GP-011 | Add users on group clicking add_users_on_group button | 4 hours | HIGH | Scheduled | 
| GP-012 | Add users on group pressing Enter should submit form | 4 hours | HIGH | Scheduled |
| GP-013 | Apply profile clicking apply_profile button | 4 hours | MEDIUM | Scheduled | 
| GP-014 | Apply profile pressing Enter should submit form | 4 hours | MEDIUM | Scheduled |
| GP-015 | Apply script clicking apply_script button | 4 hours | MEDIUM | Scheduled | 
| GP-016 | Apply script pressing Enter should submit form | 4 hours | MEDIUM | Scheduled |
| GP-017 | Update clicking update button | 3 hours | MEDIUM | Scheduled | 
| GP-018 | Update pressing Enter should submit form | 3 hours | MEDIUM | Scheduled |
| GP-019 | Compare number of groups with groups list | 4 hours | HIGH | Scheduled |
| GP-020 | Remove_from_group button only enables if select a device | 3 hours | HIGH | Scheduled |
| GP-021 | Remove device clicking Remove_from_group button | 3 hours | HIGH | Scheduled | 
| GP-022 | Remove device pressing Enter should submit form | 3 hours | HIGH | Scheduled |
| GP-023 | Compare device information with details device page | 6 hours | LOW | Scheduled |
| GP-024 | Navigation buttons of device assigned | 6 hours | LOW | Scheduled |
| GP-025 | Row per page of device assigned | 3 hours | LOW | Scheduled |
| GP-026 | Compare number of devices assigned with number in range | 6 hours | MEDIUM | Scheduled |
| GP-027 | Search for serial device assigned | 4 hours | LOW | Scheduled |
| GP-028 | Compare number of groups with groups list | 6 hours | LOW | Scheduled |
| GP-029 | Assign_to_group button only enables if select a device | 4 hours | HIGH | Scheduled |
| GP-030 | Assign device clicking Assign_to_group button | 4 hours | HIGH | Scheduled | 
| GP-031 | Assign device pressing Enter should submit form | 4 hours | HIGH | Scheduled |
| GP-032 | Compare device information with details device page | 6 hours | LOW | Scheduled |
| GP-033 | Navigation buttons of device unassigned | 6 hours | LOW | Scheduled |
| GP-034 | Row per page of device unassigned | 4 hours | LOW | Scheduled |
| GP-035 | Compare number of devices unassigned with number in range | 4 hours | MEDIUM | Scheduled |
| GP-036 | Search for serial device unassigned | 4 hours | LOW | Scheduled |


#### <A NAME="devices_ca" >Devices - Customer Admin</A>
<A HREF="#menu_ca" >Back</A>  

| **Test ID** | **Test summary** | **ETA** | **Priority** | **Status** | 
| :---: | :--- | :---: | :---: | :---: | 
| DEV-001 | Enrolled - Apply profile clicking apply_profile button | 4 hours | HIGH | Scheduled | 
| DEV-002 | Enrolled - Apply profile pressing Enter should submit form | 4 hours | HIGH | Scheduled |
| DEV-003 | Enrolled - Apply script clicking apply_script button | 4 hours | HIGH | Scheduled |
| DEV-004 | Enrolled - Apply script pressing Enter should submit form | 4 hours | HIGH | Scheduled |
| DEV-005 | Enrolled - Upgrade clicking upgrade button | 4 hours | HIGH | Scheduled | 
| DEV-006 | Enrolled - Upgrade pressing Enter should submit form | 4 hours | HIGH | Scheduled |
| DEV-007 | Enrolled - Search for serial/model/site/part number | 4 hours | HIGH | Scheduled |
| DEV-008 | Enrolled - Navigation buttons | 6 hours | LOW | Scheduled |
| DEV-009 | Enrolled - Row per page | 4 hours | LOW | Scheduled |
| DEV-010 | Enrolled - Compare number of devices with number in range | 4 hours | HIGH | Scheduled |
| DEV-011 | Enrolled - Revoke profile clicking revoke button | 4 hours | LOW | Scheduled | 
| DEV-012 | Enrolled - Revoke profile pressing Enter should submit form | 4 hours | LOW | Scheduled |
| DEV-013 | Enrolled - Black list profile clicking black_list button | 4 hours | LOW | Scheduled | 
| DEV-014 | Enrolled - Black list profile pressing Enter should submit form | 4 hours | LOW | Scheduled |
| DEV-015 | Enrolled - Enable buttons when select device | 4 hours | HIGH | Scheduled |
| DEV-016 | Available - Enable enroll button when select device | 3 hours | HIGH | Scheduled |
| DEV-017 | Available - Enroll device clicking enroll button | 4 hours | HIGH | Scheduled | 
| DEV-018 | Available - Enroll device pressing Enter should submit form | 4 hours | HIGH | Scheduled |
| DEV-019 | Available - Add by key device clicking add_by_key button | 12 hours | MEDIUM | Scheduled | 
| DEV-020 | Available - Add by key device pressing Enter should submit form | 12 hours | MEDIUM | Scheduled |
| DEV-021 | Available - Search for serial/model/site/part number | 4 hours | HIGH | Scheduled |
| DEV-022 | Available - Navigation buttons | 6 hours | LOW | Scheduled |
| DEV-023 | Available - Row per page | 4 hours | LOW | Scheduled |
| DEV-024 | Available - Compare number of devices with number in range | 4 hours | MEDIUM | Scheduled |
| DEV-025 | Device Details - Show details information when selecting device | 4 hours | HIGH | Scheduled |
| DEV-026 | Device Details - Check details information | 6 hours | HIGH | Scheduled |
| DEV-027 | Device Details - Compare quantity selected with quantity shown | 4 hours | HIGH | Scheduled |

#### <A NAME="users_ca" >Users - Customer Admin</A>
<A HREF="#menu_ca" >Back</A>  

| **Test ID** | **Test summary** | **ETA** | **Priority** | **Status** | 
| :---: | :--- | :---: | :---: | :---: | 
| USER-001 | Add user clicking add button | 4 hours | HIGH | Scheduled | 
| USER-002 | Add user pressing Enter should submit form | 4 hours | HIGH | Scheduled |
| USER-003 | Edit user clicking edit button | 4 hours | HIGH | Scheduled |
| USER-004 | Edit user pressing Enter should submit form | 4 hours | HIGH | Scheduled |
| USER-005 | Delete user clicking delete button | 4 hours | HIGH | Scheduled | 
| USER-006 | Delete user pressing Enter should submit form | 4 hours | HIGH | Scheduled |
| USER-007 | Search for user | 4 hours | HIGH | Scheduled |
| USER-008 | Navigation buttons | 4 hours | LOW | Scheduled |
| USER-009 | Row per page | 4 hours | LOW | Scheduled |
| USER-010 | Compare number of users with number in range | 4 hours | MEDIUM | Scheduled |
| USER-011 | Enable buttons when select user | 4 hours | MEDIUM | Scheduled |

#### <A NAME="profiles_ca" >Profiles - Customer Admin</A>
<A HREF="#menu_ca" >Back</A>  

| **Test ID** | **Test summary** | **ETA** | **Priority** | **Status** | 
| :---: | :--- | :---: | :---: | :---: | 
| PF-001 | Configurations - Add profile clicking add button | 4 hours | HIGH | Scheduled | 
| PF-002 | Configurations - Add profile pressing Enter should submit form | 4 hours | HIGH | Scheduled |
| PF-003 | Configurations - Delete profile clicking delete button | 4 hours | HIGH | Scheduled |
| PF-004 | Configurations - Delete profile pressing Enter should submit form | 4 hours | HIGH | Scheduled |
| PF-005 | Configurations - Enable delete button when select profile | 3 hours | MEDIUM | Scheduled | 
| PF-006 | Configurations - Search for profile | 4 hours | MEDIUM | Scheduled |
| PF-007 | Configurations - Navigation buttons | 4 hours | LOW | Scheduled |
| PF-008 | Configurations - Row per page | 4 hours | LOW | Scheduled |
| PF-009 | Configurations - Compare number of profiles with number in range | 4 hours | MEDIUM | Scheduled |
| PF-010 | Scripts - Add script clicking add button | 6 hours | HIGH | Scheduled | 
| PF-011 | Scripts - Add script pressing Enter should submit form | 6 hours | HIGH | Scheduled |
| PF-012 | Scripts - Delete script clicking delete button | 6 hours | HIGH | Scheduled |
| PF-013 | Scripts - Delete script pressing Enter should submit form | 6 hours | HIGH | Scheduled |
| PF-014 | Scripts - Enable delete button when select profile | 4 hours | MEDIUM | Scheduled | 
| PF-015 | Scripts - Search for script | 4 hours | MEDIUM | Scheduled |
| PF-016 | Scripts - Navigation buttons | 4 hours | LOW | Scheduled |
| PF-017 | Scripts - Row per page | 4 hours | LOW | Scheduled |
| PF-018 | Scripts - Compare number of scripts with number in range | 4 hours | MEDIUM | Scheduled |
| PF-019 | Software - Search for software version | 4 hours | MEDIUM | Scheduled |
| PF-020 | Software - Navigation buttons | 4 hours | LOW | Scheduled |
| PF-021 | Software - Row per page | 4 hours | LOW | Scheduled |
| PF-022 | Software - Compare number of scripts with number in range | 4 hours | MEDIUM | Scheduled |
| PF-023 | Software - Create a new release as super admin, upload release and test if it appears in the list | 12 hours | HIGH | Scheduled |
| PF-024 | Operations - Clear selecting by status | 12 hours | HIGH | Scheduled |
| PF-025 | Operations - Search for profile name/status/serial number | 4 hours | HIGH | Scheduled |
| PF-026 | Operations - Navigation buttons | 4 hours | MEDIUM | Scheduled |
| PF-027 | Operations - Row per page | 4 hours | MEDIUM | Scheduled |
| PF-028 | Operations - Compare number of records with number in range | 4 hours | MEDIUM | Scheduled |
| PF-029 | Operations - Check box enable with status Pending or Scheduled | 4 hours | MEDIUM | Scheduled |
| PF-030 | Operations - Rescheduled and cancel button enable if select a check box | 4 hours | MEDIUM | Scheduled |
| PF-031 | Operations - Cancel operation clicking cancel button  | 4 hours | MEDIUM | Scheduled |
| PF-032 | Operations - Cancel operation pressing Enter should submit form | 4 hours | MEDIUM | Scheduled |
| PF-033 | Operations - Rescheduled profile clicking cancel button  | 6 hours | HIGH | Scheduled |
| PF-034 | Operations - Rescheduled profile pressing Enter should submit form | 6 hours | HIGH | Scheduled |

#### <A NAME="logs_ca" >Logs - Customer Admin</A>
<A HREF="#menu_ca" >Back</A>  

| **Test ID** | **Test summary** | **ETA** | **Priority** | **Status** | 
| :---: | :--- | :---: | :---: | :---: | 
| LOG-001 | General - Search for message | 4 hours | HIGH | Scheduled |
| LOG-002 | General - Navigation buttons | 4 hours | LOW | Scheduled |
| LOG-003 | General - Row per page | 4 hours | LOW | Scheduled |
| LOG-004 | General - Compare number of messages with number in range | 4 hours | MEDIUM | Scheduled |
| LOG-005 | Device logs - Search for job id | 4 hours | HIGH | Scheduled |
| LOG-006 | Device logs - Navigation buttons | 4 hours | LOW | Scheduled |
| LOG-007 | Device logs - Row per page | 4 hours | LOW | Scheduled |
| LOG-008 | Device logs - Compare number of job id with number in range | 4 hours | MEDIUM | Scheduled |
| LOG-009 | Device logs - Search for device and period clicking confirm button | 4 hours | HIGH | Scheduled |
| LOG-010 | Device logs - Search for device and period pressing Enter should submit form | 4 hours | HIGH | Scheduled |

#### <A NAME="settings_ca" >Settings - Customer Admin</A>
<A HREF="#menu_ca" >Back</A>  

| **Test ID** | **Test summary** | **ETA** | **Priority** | **Status** | 
| :---: | :--- | :---: | :---: | :---: | 
| SET-001 | Enrollment - Check customer code | 12 hours | HIGH | Scheduled |
| SET-002 | Enrollment - Display password clicking view button | 4 hours | MEDIUM | Scheduled |
| SET-003 | Enrollment - Display password pressing Enter should submit form  | 4 hours | MEDIUM | Scheduled |
| SET-004 | Enrollment - Enable and disable device enrollment | 6 hours | HIGH | Scheduled |

---


### <A NAME="menu_ga" >ZPE Cloud - Group Admin</A>
<A HREF="#main_menu" >Back</A>
 
- <A HREF="#dashboard_ga" >Dashboard</A>
- <A HREF="#groups_ga" >Groups</A>
- <A HREF="#devices_ga" >Devices</A>
- <A HREF="#profiles_ga" >Profiles</A>
- <A HREF="#logs_ga" >Logs</A>

#### <A NAME="dashboard_ga" >Dashboard - Group Admin</A>
<A HREF="#menu_ga" >Back</A>  

| **Test ID** | **Test summary** | **ETA** | **Priority** | **Status** | 
| :---: | :--- | :---: | :---: | :---: |
| DB-001 | MAP - Compare number of sites on the map with the graph, sites page and mouse over | 4 hours | HIGH | Scheduled | 
| DB-002 | MAP - Compare number of online sites on the map with the graph, sites page and mouse over | 12 hours | MEDIUM | Scheduled |
| DB-003 | MAP - Compare number of offline sites on the map with the graph, sites page and mouse over | 12 hours | MEDIUM | Scheduled |
| DB-004 | MAP - Compare number of partial sites on the map with the graph, sites page and mouse over | 12 hours | MEDIUM | Scheduled | 
| DB-005 | MAP - Add new site and compare number of sites on the map with the graph, sites page and mouse over | 3 hours | HIGH | Scheduled |
| DB-006 | MAP - Delete a site and compare number of sites on the map with the graph, site page and mouse over | 3 hours | HIGH | Scheduled |
| DB-007 | MAP - Compare number of devices on the graph with devices page and mouse over | 12 hours | HIGH | Scheduled |
| DB-008 | MAP - Compare number of online devices on the graph with devices page and mouse over | 12 hours | MEDIUM | Scheduled |
| DB-009 | MAP - Compare number of offline devices on the graph with devices page | 12 hours | MEDIUM | Scheduled |
| DB-010 | MAP - Compare number of never connected devices on the graph with devices page | 12 hours | MEDIUM | Scheduled |
| DB-011 | MAP - Add new device and compare number of devices on the graph with devices page and mouse over | 12 hours | HIGH | Scheduled |
| DB-012 | MAP - Disassociate a device and compare number of devices on the graph with devices page and mouse over | 12 hours | MEDIUM | Scheduled |
| DB-013 | MAP - Compare site name and address of the mouse over with sites page | 12 hours | LOW | Scheduled |
| DB-014 | MAP - Test zoom buttons | 12 hours | LOW | Scheduled |
| DB-015 | Sim Stats - If the confirm button only enables to enter the period | 3 hours | HIGH | Scheduled |
| DB-016 | Sim Stats - Select device, period pressing Enter should submit form | 3 hours | HIGH | Scheduled |
| DB-017 | Sim Stats - Select device, period clicking Confirm button | 3 hours | HIGH | Scheduled |
| DB-018 | Sim Stats - Compare model, imei and sim card information with devices page | 6 hours | HIGH | Scheduled |
| DB-019 | Sim Stats - Confirm graphics are loaded | 6 hours | HIGH | Scheduled |
| DB-020 | Sim Stats - If the button confirm only enables to enter the periods | 3 hours | MEDIUM | Scheduled |
| DB-021 | Sim Stats - Select the periods pressing Enter should submit form | 3 hours | MEDIUM | Scheduled |
| DB-022 | Sim Stats - Select the periods clicking Confirm button | 3 hours | MEDIUM | Scheduled |
| DB-023 | Sim Stats - Confirm sim card graphics are loaded | 3 hours | MEDIUM | Scheduled |

#### <A NAME="groups_ga" >Groups - Group Admin</A>
<A HREF="#menu_ga" >Back</A>  

| **Test ID** | **Test summary** | **ETA** | **Priority** | **Status** | 
| :---: | :--- | :---: | :---: | :---: | 
| GP-001 | Search for group | 3 hours | MEDIUM | Scheduled |
| GP-002 | Navigation buttons | 6 hours | LOW | Scheduled |
| GP-003 | Row per page | 3 hours | LOW | Scheduled |
| GP-004 | Compare number of groups with number in range | 4 hours | HIGH | Scheduled |
| GP-005 | Compare number of groups with groups list | 4 hours | HIGH | Scheduled |
| GP-006 | Remove_from_group button only enables if select a device | 3 hours | HIGH | Scheduled |
| GP-007 | Remove device clicking Remove_from_group button | 3 hours | HIGH | Scheduled | 
| GP-008 | Remove device pressing Enter should submit form | 3 hours | HIGH | Scheduled |
| GP-009 | Compare device information with details device page | 6 hours | LOW | Scheduled |
| GP-011 | Navigation buttons of device assigned | 6 hours | LOW | Scheduled |
| GP-012 | Row per page of device assigned | 3 hours | LOW | Scheduled |
| GP-013 | Compare number of devices assigned with number in range | 6 hours | MEDIUM | Scheduled |
| GP-014 | Search for serial device assigned | 4 hours | LOW | Scheduled |
| GP-015 | Compare number of groups with groups list | 6 hours | LOW | Scheduled |
| GP-016 | Assign_to_group button only enables if select a device | 4 hours | HIGH | Scheduled |
| GP-017 | Assign device clicking Assign_to_group button | 4 hours | HIGH | Scheduled | 
| GP-018 | Remove device pressing Enter should submit form | 3 hours | HIGH | Scheduled |
| GP-019 | Compare device information with details device page | 6 hours | LOW | Scheduled |
| GP-020 | Navigation buttons of device unassigned | 6 hours | LOW | Scheduled |
| GP-021 | Row per page of device unassigned | 4 hours | LOW | Scheduled |
| GP-022 | Compare number of devices unassigned with number in range | 4 hours | MEDIUM | Scheduled |
| GP-023 | Search for serial device unassigned | 4 hours | LOW | Scheduled |

#### <A NAME="devices_ga" >Devices - Group Admin</A>
<A HREF="#menu_ga" >Back</A>  

| **Test ID** | **Test summary** | **ETA** | **Priority** | **Status** | 
| :---: | :--- | :---: | :---: | :---: | 
| DEV-001 | Enrolled - Apply profile clicking apply_profile button | 4 hours | HIGH | Scheduled | 
| DEV-002 | Enrolled - Apply profile pressing Enter should submit form | 4 hours | HIGH | Scheduled |
| DEV-003 | Enrolled - Apply script clicking apply_script button | 4 hours | HIGH | Scheduled |
| DEV-004 | Enrolled - Apply script pressing Enter should submit form | 4 hours | HIGH | Scheduled |
| DEV-005 | Enrolled - Upgrade clicking upgrade button | 4 hours | HIGH | Scheduled |
| DEV-006 | Enrolled - Upgrade pressing Enter should submit form | 4 hours | HIGH | Scheduled |
| DEV-007 | Enrolled - Search for serial/model/site/part number | 4 hours | HIGH | Scheduled |
| DEV-008 | Enrolled - Navigation buttons | 6 hours | LOW | Scheduled |
| DEV-009 | Enrolled - Row per page | 4 hours | LOW | Scheduled |
| DEV-011 | Enrolled - Compare number of devices with number in range | 4 hours | HIGH | Scheduled |
| DEV-012 | Enrolled - Revoke profile clicking revoke button | 4 hours | LOW | Scheduled |
| DEV-013 | Enrolled - Revoke profile pressing Enter should submit form | 4 hours | LOW | Scheduled |
| DEV-014 | Enrolled - Black list profile clicking black_list button | 4 hours | LOW | Scheduled |
| DEV-015 | Enrolled - Black list profile pressing Enter should submit form | 4 hours | LOW | Scheduled |
| DEV-016 | Enrolled - Enable buttons when select device | 4 hours | HIGH | Scheduled |
| DEV-017 | Device Details - Show details information when selecting device | 4 hours | HIGH | Scheduled |
| DEV-018 | Device Details - Check details information | 6 hours | HIGH | Scheduled |
| DEV-019 | Device Details - Compare quantity selected with quantity shown | 4 hours | HIGH | Scheduled | 

#### <A NAME="profiles_ga" >Profiles - Group Admin</A>
<A HREF="#menu_ga" >Back</A>  

| **Test ID** | **Test summary** | **ETA** | **Priority** | **Status** | 
| :---: | :--- | :---: | :---: | :---: | 
| PF-001 | Configurations - Add profile clicking add button | 4 hours | HIGH | Scheduled | 
| PF-002 | Configurations - Add profile pressing Enter should submit form | 4 hours | HIGH | Scheduled |
| PF-003 | Configurations - Delete profile clicking delete button | 4 hours | HIGH | Scheduled |
| PF-004 | Configurations - Delete profile pressing Enter should submit form | 4 hours | HIGH | Scheduled |
| PF-005 | Configurations - Enable delete button when select profile | 3 hours | MEDIUM | Scheduled | 
| PF-006 | Configurations - Search for profile | 4 hours | MEDIUM | Scheduled |
| PF-007 | Configurations - Navigation buttons | 4 hours | LOW | Scheduled |
| PF-008 | Configurations - Row per page | 4 hours | LOW | Scheduled |
| PF-009 | Configurations - Compare number of profiles with number in range | 4 hours | MEDIUM | Scheduled |
| PF-010 | Scripts - Add script clicking add button | 6 hours | HIGH | Scheduled | 
| PF-011 | Scripts - Add script pressing Enter should submit form | 6 hours | HIGH | Scheduled |
| PF-012 | Scripts - Delete script clicking delete button | 6 hours | HIGH | Scheduled |
| PF-013 | Scripts - Delete script pressing Enter should submit form | 6 hours | HIGH | Scheduled |
| PF-014 | Scripts - Enable delete button when select profile | 4 hours | MEDIUM | Scheduled | 
| PF-015 | Scripts - Search for script | 4 hours | MEDIUM | Scheduled |
| PF-016 | Scripts - Navigation buttons | 4 hours | LOW | Scheduled |
| PF-017 | Scripts - Row per page | 4 hours | LOW | Scheduled |
| PF-018 | Scripts - Compare number of scripts with number in range | 4 hours | MEDIUM | Scheduled |
| PF-019 | Software - Search for software version | 4 hours | MEDIUM | Scheduled |
| PF-020 | Software - Navigation buttons | 4 hours | LOW | Scheduled |
| PF-021 | Software - Row per page | 4 hours | LOW | Scheduled |
| PF-022 | Software - Compare number of scripts with number in range | 4 hours | MEDIUM | Scheduled |

#### <A NAME="logs_ga" >Logs - Group Admin</A>
<A HREF="#menu_ga" >Back</A>  

| **Test ID** | **Test summary** | **ETA** | **Priority** | **Status** | 
| :---: | :--- | :---: | :---: | :---: | 
| LOG-001 | General - Search for message | 4 hours | HIGH | Scheduled |
| LOG-002 | General - Navigation buttons | 4 hours | LOW | Scheduled |
| LOG-003 | General - Row per page | 4 hours | LOW | Scheduled |
| LOG-004 | General - Compare number of messages with number in range | 4 hours | MEDIUM | Scheduled |
| LOG-005 | Device logs - Search for job id | 4 hours | HIGH | Scheduled |
| LOG-006 | Device logs - Navigation buttons | 4 hours | LOW | Scheduled |
| LOG-007 | Device logs - Row per page | 4 hours | LOW | Scheduled |
| LOG-008 | Device logs - Compare number of job id with number in range | 4 hours | MEDIUM | Scheduled |
| LOG-009 | Device logs - Search for device and period clicking confirm button | 4 hours | HIGH | Scheduled |
| LOG-010 | Device logs - Search for device and period pressing Enter should submit form | 4 hours | HIGH | Scheduled |

---

### Device Related Test

- Enrollment;
- Profiles;
- Cellular Data; 


## Tests Details

#### 10001 - Login Tests 

