import smtplib
import time
import imaplib
import email
import json

# -------------------------------------------------
#
# Utility to read email from Gmail Using Python
#
# ------------------------------------------------

ORG_EMAIL = "@gmail.com"
FROM_EMAIL = "autobuild.zpesystems" + ORG_EMAIL
FROM_PWD = "N0d3Gr1d!"
SMTP_SERVER = "imap.gmail.com"
SMTP_PORT = 993


def read_email_from_gmail():
    try:
        mail = imaplib.IMAP4_SSL(SMTP_SERVER)
        mail.login(FROM_EMAIL, FROM_PWD)
        mail.select('inbox')

        type, data = mail.search(None, 'FROM', '"ZPE CLOUD"')
        mail_ids = data[0]

        id_list = mail_ids.split()

        first_email_id = 0
        latest_email_id = 0

        if len(id_list) == 0:
            return ""
        if len(id_list) > 0:
            first_email_id = int(id_list[len(id_list) - 1])
            latest_email_id = int(id_list[len(id_list) - 1])
        if len(id_list) > 1:
            first_email_id = int(id_list[len(id_list) - 2])

        # print latest_email_id, first_email_id
        # for i in range(first_email_id, latest_email_id + 1):
        for i in range(latest_email_id, first_email_id - 1, -1):
            typ, data = mail.fetch(i, '(RFC822)')
            # print "I: ", i
            for response_part in data:
                if isinstance(response_part, tuple):
                    msg = email.message_from_string(response_part[1])
                    email_subject = msg['subject']
                    email_from = msg['from']
                    # print 'From : ' + email_from + '\n'
                    # print 'Subject : ' + email_subject + '\n'
                    # print 'MAILGUN : ' + msg['X-Mailgun-Variables'] + '\n'
                    loaded_json = json.loads(msg['X-Mailgun-Variables'])
                    url = str(loaded_json["front_base_url"]) + str(loaded_json["reset_password_url"]) + str(
                        loaded_json["reset_password_token"])
                    # print 'URL: ' + url + '\n'
                    # print 'DATE: ' + msg['Date'] + '\n'
                    print(url)
                    return url
                    # print msg
                    break
            break


    except Exception as e:
        print(str(e))


if __name__ == "__main__":
    read_email_from_gmail()


