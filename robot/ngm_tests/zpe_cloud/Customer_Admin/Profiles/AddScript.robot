*** Settings ***
Documentation	Tests for Verify Script Details
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	CUSTOMER_ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${Invalid_Script_Name}	Invalid
${Script_Name}	Template_Oro
${Description}	Oro_Automation_Template

*** Test Cases ***
Test Case Add Script
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-header-tab-1-5"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Input text	//*[@id="input-name"]	${Script_Name}
	Input Text	//*[@id="input-description"]	${Description}
	sleep	2s
	Click Element	//*[@id="mui-component-select-type"]
	Click Element	//*[@id="opt-SCRIPT"]
	Input Text	//*[@id="ca-profiles-config-wrapper"]/div[2]/div[2]/div[2]/div/div/textarea	echo "test file" > /tmp/test.txt
	Click Button	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Input Text	//*[@id="search-input"]	${Script_Name}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	${Script_Name}
	Page should Contain	${Script_Name}

Test Case Add Script with same script name
	Click Element	//*[@id="ca-header-tab-1-5"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Input text	//*[@id="input-name"]	${Script_Name}
	Input Text	//*[@id="input-description"]	${Description}
	sleep	2s
	Click Element	//*[@id="mui-component-select-type"]
	Click Element	//*[@id="opt-SCRIPT"]
	Input Text	//*[@id="ca-profiles-config-wrapper"]/div[2]/div[2]/div[2]/div/div/textarea	echo "test file" > /tmp/test.txt
	Click Button	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Configuration name already exists

Test case Check valid Script and download script in Profiles-Configuration
	Click Element	//*[@id="close-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Press Keys	//*[@id="search-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="search-input"]
	Input Text	//*[@id="search-input"]	${Script_Name}
	Wait Until Page Contains	${Script_Name}
	Click Element	//*[@id="ca-profiles-config-table-1"]/div/div/table/tbody/tr[1]/td[8]/button
	GUI::Basic::Spinner Should Be Invisible

Test case Check Invalid Script in Profiles-Configuration
	Press Keys	//*[@id="search-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="search-input"]
	Input Text	//*[@id="search-input"]	${Invalid_Script_Name}
	Wait Until Page Contains	No result found

Test case Check Add Script Log in logs
	Click ELement	//*[@id="ca-header-tab-1-6"]
	GUI::Basic::Spinner Should Be Invisible
	Click ELement	//*[@id="ca-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Script Created. Script: Template_Oro.
	Page should Contain	Script Created. Script: Template_Oro.

Test Case Delete Script
	Click Element	//*[@id="ca-header-tab-1-5"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	${Script_Name}
	Click Element	//*[@id="ca-profiles-config-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Click Element	//*[@id="function-button-5"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Page should Contain	Profile deleted successfully

Test case Check Delete Script Log in logs
	Click ELement	//*[@id="ca-header-tab-1-6"]
	GUI::Basic::Spinner Should Be Invisible
	Click ELement	//*[@id="ca-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Script Deleted. Script: Template_Oro.
	Page should Contain	Script Deleted. Script: Template_Oro.

Test case Check Profiles-operations loads successfully
	Click Element	//*[@id="ca-header-tab-1-5"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-header-tab-2-4"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Hostname

Test case Check Invalid Script in Profiles-operations
	Input Text	//*[@id="search-input"]	${Invalid_Script_Name}
	Wait Until Page Contains	No records to display

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Teardown
	Close All Browsers
