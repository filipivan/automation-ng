*** Settings ***
Documentation	Tests for Verify Template tab in Profiles
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	COMPANYADMIN	PROFILES
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${Invalid_Script_Name}	Invalid
${Script_Name}	Template_Oro
${Description}	Oro_Automation_Template

*** Test Cases ***
Test Case Add Script-Template
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-header-tab-1-5"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-header-tab-2-5"]
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Add Script Template

Test Case Download Script Template
	Sleep	3s
	SUITE:Search Valid Script-Template
	Click Element	//*[@id="customer-admin-layout"]/main/div[1]/div[2]/div[3]/div[1]/div/table/tbody/tr/td[6]/span
	GUI::Basic::Spinner Should Be Invisible

Test case Check Invalid Script-template
	Press Keys	//*[@id="search-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="search-input"]
	Input Text	//*[@id="search-input"]	${Invalid_Script_Name}
	Wait Until Page Contains	No result found

Test case Delete valid Script-Template from Cloud
	Press Keys	//*[@id="search-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="search-input"]
	SUITE:Search Valid Script-Template
	Click Element	//*[@id="customer-admin-layout"]/main/div[1]/div[2]/div[3]/div[1]/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="function-button-2"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Template deleted successfully

Test case Delete Downloaded File
	Remove File	../../../Downloads/${Script_Name}.txt

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Add Script Template
	Click Element	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Input text	//*[@id="template-name-input"]	${Script_Name}
	Input Text	//*[@id="template-description-select"]	${Description}
	sleep	2s
	Click Element	//*[@id="template-type-select"]
	Click Element	//*[@id="template-type-option-1"]
	Input Text	//*[@id="template-code-input"]	echo "test file" > /tmp/test.txt
	Click Button	//*[@id="function-button-1"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Template added successfully

SUITE:Search Valid Script-Template
	Input Text	//*[@id="search-input"]	${Script_Name}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	${Script_Name}
	Page should Contain	${Script_Name}

SUITE:Teardown
	Close All Browsers
