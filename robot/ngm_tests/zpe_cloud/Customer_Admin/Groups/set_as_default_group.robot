*** Settings ***
Documentation	Tests for Verification of Set as Default feature in groups tab.
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	CUSTOMER_ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}		NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${Group_Name}	Test_Group

*** Test Cases ***
Test Case Add group in the Company
	GUI::ZPECloud::Groups::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	SUITE: Add Group

Test Case verify set group as default button in groups tab
	SUITE:Set Group as default group

Test Case verify Unset group as default in groups tab
	SUITE:Set Administrator Group as default group

Test Case Delete group in the Company
	SUITE:Delete Group

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE: Add Group
	Click Button	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@name="name"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Input Text	//*[@name="name"]	${Group_Name}
	Sleep	3s
	Select Radio Button	groupManagement	3
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Click Button	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Wait Until Page Contains	Group added successfully	15s
	Page Should Contain	${Group_Name}
	Sleep	5s

SUITE:Set Group as default group
	Input Text	//*[@id="search-input"]	${Group_Name}
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Select Checkbox	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Click Element	//*[@id="function-button-3"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Group was set as default	15s

SUITE:Delete Group
	Press Keys	//*[@id="search-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="search-input"]
	Input Text	//*[@id="search-input"]	${Group_Name}
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Select Checkbox	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Click Button	//*[@id="function-button-2"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Page should not contain	${Group_Name}
	Sleep	5s

SUITE:Set Administrator Group as default group
	Press Keys	//*[@id="search-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="search-input"]
	Input Text	//*[@id="search-input"]	Administrator
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Select Checkbox	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="function-button-3"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Group was set as default	15s

SUITE:Teardown
	Close All Browsers
