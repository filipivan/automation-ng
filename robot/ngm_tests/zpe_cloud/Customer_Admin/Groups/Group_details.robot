*** Settings ***
Documentation	Tests for Verification of Group Page.
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	CUSTOMER_ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_1

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${GROUP_NAME}	Test_Group
${Status}	All
${CUSTOMERCODE}
${ENROLLMENTKEY}
${ENROLLMENTSUCCESS}	Enrollment process successful!
${Invalid_GroupName}	Invalid_Group

*** Test Cases ***
Test case to Enroll device on cloud.
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	[Teardown]	Close All Browsers

Test Case Add group in the Company
	SUITE:Setup
	GUI::ZPECloud::Groups::Create Group	${GROUP_NAME}
	[Teardown]	Close All Browsers

Test case Add Device to Group in the Company
	SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Groups::Devices::Add device to group	${GROUP_NAME}	${SERIALNUMBER}
	[Teardown]	Close All Browsers

Test case Unassign Device to Group in the Company
	SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Groups::Devices::Remove device from group	${GROUP_NAME}	${SERIALNUMBER}
	[Teardown]	Close All Browsers

Test Case Delete group in the Company
	SUITE:Setup
	GUI::ZPECloud::Groups::Delete Group	${GROUP_NAME}
	[Teardown]	Close All Browsers

Test Case Verify Invalid Group Search
	SUITE:Setup
	GUI::ZPECloud::Groups::Open Tab
	Sleep	3s
	Press Keys	//*[@id="search-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="search-input"]
	Input Text	//*[@id="search-input"]	${Invalid_GroupName}
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Page Should Contain	No result found
	[Teardown]	Close All Browsers

Test case Verify Group logs
    SUITE:Setup
	GUI::ZPECloud::Tracking::Logs::Open Tab
	Click Button	//*[@id="Confirm"]
	Wait Until Page Contains Element	//*[@id="ca-logs-customer-table-1"]/div[1]/div/table/tbody/tr[1]	30s
	Page Should Contain	Group Created.
	Page should Contain	Group Deleted. Group: Test_Group.
	Page Should Contain	Device Added to a Group.
	[Teardown]	Close All Browsers

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	Wait Until Keyword Succeeds	3x	3s	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Login into Nodegrid
	Wait Until Keyword Succeeds	3x	3s	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Add Group
	Click Button	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@name="name"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Input Text	//*[@name="name"]	${GROUP_NAME}
	Sleep	3s
	Select Radio Button	groupManagement	3
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Click Button	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Page Should Contain	${GROUP_NAME}
	Sleep	5s

SUITE:Teardown
	SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Groups::Open Tab
	${HAS_GROUP}=	Run Keyword And return Status	GUI::ZPECloud::Groups::Find Group and Select it	${GROUP_NAME}
	Run Keyword If	${HAS_GROUP}	GUI::ZPECloud::Groups::Delete Group	${GROUP_NAME}
	Close All Browsers
