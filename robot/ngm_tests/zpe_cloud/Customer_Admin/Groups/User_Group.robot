*** Settings ***
Documentation	Tests for Add and delete Groups, Assign and unassign user to group.
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	CUSTOMER_ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${Name of Group}	Test_Group
${Status}	Online
${Invalid_GroupName}	Invalid_Group
${New_User_Email}	QA_TestUser@gmail.com
${New_User_firstname}	QA
${New_User_lastname}	TestUser
${New_User_phone}	123456789

*** Test Cases ***
Test Case Add group in the Company
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="ca-header-tab-1-2"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@name="name"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Input Text	//*[@name="name"]	${Name of Group}
	Sleep	3s
	Select Radio Button	groupManagement	3
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Click Button	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Wait Until Page Contains	Group added successfully	5s

Test case Add User as Company User
	Click Button	//*[@id="ca-header-tab-1-4"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Input Text	//*[@id="ca-usergrp-form-field-email"]	${New_User_Email}
	Input Text	//*[@id="ca-usergrp-form-field-first_name"]	${New_User_firstname}
	Input Text	//*[@id="ca-usergrp-form-field-last_name"]	${New_User_lastname}
	Input Text	//*[@id="ca-usergrp-add-dialog"]/div[2]/div/div/div/div[1]/div/div[7]/div/div/input	${New_User_phone}
	Click Button	//*[@id="submit-btn"]
	Sleep	2s
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	User added successfully	5s

Test case Add User to Group in the Company
	Click Button	//*[@id="ca-header-tab-1-2"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Click Element	//*[@id="ca-header-tab-2-2"]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="search-input"]	${New_User_Email}
	Sleep	3s
	Select Checkbox	//*[@id="ca-groups-user-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="search-input"]	${Name of Group}
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Select Checkbox	//*[@id="ca-group-selection-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="function-button-1"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Users added to groups successfully	5s

Test case Unassign User to Group in the Company
	Sleep	2s
	Click Element	//*[@id="mui-component-select-selectedGroup"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="menu-selectedGroup"]/div[3]/ul/li[contains(.,"${Name of Group}")]
	Sleep	2s
	Select Checkbox	//*[@id="ca-groups-user-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="function-button-1"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Users removed from group successfully
	GUI::Basic::Spinner Should Be Invisible

Test Case Delete group in the Company
	Click Button	//*[@id="ca-header-tab-2-0"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Input Text	//*[@id="search-input"]	${Name of Group}
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Select Checkbox	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Click Button	//*[@id="function-button-2"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Group deleted successfully	5s

Test case Delete User with "KeepUserData"
	Click Element	//*[@id="ca-header-tab-1-4"]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="search-input"]	${New_User_Email}
	Sleep	2s
	Click element	//*[@id="ca-ugn-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="function-button-2"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="ca-ugn-delete-user-dialog"]/div[3]/div/div[1]/fieldset/div/label[1]/span[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	User logs deleted successfully. Your referenced logs are being masked. This operation will take a while.

Test case Verify logs
	Click Button	//*[@id="ca-header-tab-1-6"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="ca-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Page Should Contain	User Deleted.
	Page Should Contain	Group Deleted.
	Page Should Contain	User Removed from a Group.
	Page Should Contain	User Added to a Group.
	Page Should Contain	User Created.
	Page Should Contain	Group Created.

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	SUITE:Delete user if exist

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Teardown
	Close All Browsers

SUITE:Delete user if exist
	Click Element	//*[@id="ca-header-tab-1-4"]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="search-input"]	${New_User_Email}
	Sleep	2s
	${HAS_USER}	Run Keyword And Return Status	Click element	//*[@id="ca-ugn-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	Run Keyword If	not ${HAS_USER}	Return From Keyword
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="function-button-2"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="ca-ugn-delete-user-dialog"]/div[3]/div/div[1]/fieldset/div/label[1]/span[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	User deleted successfully
