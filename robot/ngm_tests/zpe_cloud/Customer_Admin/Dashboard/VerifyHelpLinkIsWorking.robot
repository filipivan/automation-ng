*** Settings ***
Documentation	Tests for Verification of Help Link
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	COMPANY_ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}

*** Test Cases ***
Test case Verify Help Link
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	3x	1s	SUITE:Click and verify Help Link

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Teardown
	Close All Browsers

SUITE:Click and verify Help Link
	Click Element	//*[@id="ca-help-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	10s
	Switch Window	Introduction to ZPE Cloud User Guide
	Sleep	5s
	Page Should Contain	Overview