*** Settings ***
Documentation	Tests for Access Groups,Sites and devices in Company
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	ACCESS	CONSOLE
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-CRITICAL	DEVICE	PART_1

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${Name of Site}	Test_Site
${Address}	India
${Latitude}	22.3511148
${Longitude}	78.6677428
${Name of Group}	Test_Group
${Status}	All
${NIC}	ETH0
${IP}	${HOST}
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}

*** Test Cases ***
Test case to Enroll device on cloud.
	SUITE:Login into ZPE Cloud Company
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud

Test case Add Site in the Company
	GUI::ZPECloud::Sites::General::Open Tab
	Click Button	//button[contains(text(),'Add')]
	Input Text	//*[@name="name"]	${Name of Site}
	Input Text	//*[@name="address"]	${Address}
	Input Text	//*[@name="latitude"]	${Latitude}
	Input Text	//*[@name="longitude"]	${Longitude}
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${Name of Site}

Test case Verify Site Chart for added site
	GUI::ZPECloud::Dashboard::Map::Open Tab
	Click Button	//*[@id="ca-header-tab-1-0"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-dashboard-map-wrapper"]/div[2]/div[1]/div/div/div
	GUI::Basic::Spinner Should Be Invisible
	Page should contain	${Name of Site}

Test case Add Device to Site in the Company
	GUI::ZPECloud::Sites::Devices::Open Tab
	Input Text	search-input	${SERIALNUMBER}
	Wait Until Page Does Not Contain Element	//td[contains(.,'${SERIALNUMBER}')]/../td[1]/span	30s
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//td[contains(.,'${SERIALNUMBER}')]/../td[1]/span	30s
	Click Element	//td[contains(.,'${SERIALNUMBER}')]/../td[1]/span
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Enabled	//*[@id="function-button-0"]	30s
	Click Element	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="search-input"]	${Name of Site}
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="ca-sd-table-2"]/div/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="function-button-1"]
	GUI::Basic::Spinner Should Be Invisible

Test case Verify Device Chart for added Device
	GUI::ZPECloud::Dashboard::Map::Open Tab
	Wait Until Keyword Succeeds	5x	1s	Click Element	//div[@class='echarts-for-react '][contains(.,'OnlineOfflineNever ConnectedFailover')]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	search-input	${SERIALNUMBER}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	//td[contains(.,'${SERIALNUMBER}')]/../td[1]/span
	GUI::Basic::Spinner Should Be Invisible

Test Case Add group in the Company
	GUI::ZPECloud::Groups::General::Open Tab
	Wait Until Element Is Enabled	//*[@id="function-button-0"]
	Click Element	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@name="name"]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@name="name"]	${Name of Group}
	GUI::Basic::Spinner Should Be Invisible
	Select Radio Button	groupManagement	3
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${Name of Group}

Test case Add Device to Group in the Company
	GUI::ZPECloud::Groups::Devices::Open Tab
	Input Text	//*[@id="search-input"]	${SERIALNUMBER}
	Wait Until Page Does Not Contain Element	//*[@id="ca-group-devices-wrapper"]/div[2]/div/table/tbody/tr/td[1]/span/input	15s
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="ca-group-devices-wrapper"]/div[2]/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="search-input"]	${Name of Group}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//div[contains(.,'${Name of Group}')]/../../td[1]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="function-button-1"]
	GUI::Basic::Spinner Should Be Invisible

Test Case Verify Dashboard Access Status Filter
	GUI::ZPECloud::Dashboard::Access::Open Tab
	Click Element	//*[@id="customer-admin-layout"]/main/div/div[4]/div/div/table/thead/tr/th[4]/div/div/div
	GUI::Basic::Spinner Should Be Invisible
	Set Focus To Element	//*[@id="menu-"]/div[3]/ul/li[contains(.,"${Status}")]
	Click Element	//*[@id="menu-"]/div[3]/ul/li[contains(.,"${Status}")]
	GUI::Basic::Spinner Should Be Invisible
	Double Click Element	//*[@id="customer-admin-layout"]/main/div/div[4]/div/div/table/thead/tr/th[6]/div/div/div
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${SERIALNUMBER}
	[Teardown]	Run Keywords	Reload Page	AND	GUI::Basic::Spinner Should Be Invisible

Test Case Verify Dashboard Access Site Filter
	GUI::ZPECloud::Dashboard::Access::Open Tab
	Click Element	//*[@id="customer-admin-layout"]/main/div/div[4]/div/div/table/thead/tr/th[6]/div/div/div
	GUI::Basic::Spinner Should Be Invisible
	Set Focus To Element	//*[@id="menu-sites"]/div[3]/ul/li[contains(.,"${Name of Site}")]
	Click Element	//*[@id="menu-sites"]/div[3]/ul/li[contains(.,"${Name of Site}")]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${SERIALNUMBER}
	[Teardown]	Run Keywords	Reload Page	AND	GUI::Basic::Spinner Should Be Invisible

Test Case Verify Dashboard Access Group Filter
	GUI::ZPECloud::Dashboard::Access::Open Tab
	Click Element	//*[@id="customer-admin-layout"]/main/div/div[4]/div/div/table/thead/tr/th[7]/div/div/div
	GUI::Basic::Spinner Should Be Invisible
	Set Focus To Element	//*[@id="menu-sites"]/div[3]/ul/li[contains(.,"${Name of Group}")]
	Click Element	//*[@id="menu-sites"]/div[3]/ul/li[contains(.,"${Name of Group}")]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${SERIALNUMBER}
	[Teardown]	Run Keywords	Reload Page	AND	GUI::Basic::Spinner Should Be Invisible

Test Case Delete group in the Company
	GUI::ZPECloud::Groups::Open Tab
	Input Text	//*[@id="search-input"]	${Name of Group}
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="function-button-2"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Page should not contain	${Name of Group}
	[Teardown]	Run Keywords	Reload Page	AND	GUI::Basic::Spinner Should Be Invisible

Test Case Delete Site in the Company
	GUI::ZPECloud::Sites::General::Open Tab
	Input Text	//*[@id="search-input"]	${Name of Site}
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="ca-sg-table-1"]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="function-button-2"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Page should not contain	${Name of Site}
	[Teardown]	Run Keywords	Reload Page	AND	GUI::Basic::Spinner Should Be Invisible

Test case Check Device Network Interface
	GUI::ZPECloud::Devices::Enrolled::Open Tab
	Input Text	//*[@id="search-input"]	${SERIALNUMBER}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	${SERIALNUMBER}
	Page Should Contain	${NIC}
	[Teardown]	Run Keywords	Reload Page	AND	GUI::Basic::Spinner Should Be Invisible

Test case Check Device IP Address Field
	GUI::ZPECloud::Devices::Enrolled::Open Tab
	Page Should Contain	${IP}

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::System::Preferences::Add cordinates and save if needed
	Close All Browsers

SUITE:Teardown
	Close All Browsers

