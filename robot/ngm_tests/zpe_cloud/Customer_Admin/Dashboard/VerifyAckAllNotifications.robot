*** Settings ***
Documentation	Tests for Verification of Notifications Button in the Company
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	COMPANY ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-DEVICE	NON-CRITICAL	BUG_CLOUD_7840

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}

*** Test Cases ***
Test case Verify Notification Button Appears
	Sleep	2s
	Page should Contain Element	//*[@id="ca-notification-btn"]

Test case Verify Notification Button Operation
	Sleep	2s
	Click Element	//*[@id="ca-notification-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s

Test case Verify View All Notifications in Open Tab
	Click Element	//*[@id="ca-view-all-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="filter-btn"]
	SUITE:Selection of Priority and Date in Open Tab
	Click Element	//*[@id="ca-open-logs-wrapper"]/div[2]/div[1]/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="filter-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Alerts Acknowledged Successfully

Test case Verify View All Notifications in Closed Tab
	Click Element	//*[@id="logs-event-ack-tab"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	SUITE:Selection of Priority and Date in Closed Tab
	Click Element	//*[@id="ca-ack-logs-wrapper"]/div[2]/div[1]/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="filter-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Alerts ReOpen Successfully

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Selection of Priority and Date in Open Tab
	Click Element	//*[@id="priority-select"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="priority-select-opt-0"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Click Element	//*[@id="start-date"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="logs-filter-components"]/div/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[2]/div[1]/div[1]
	GUI::Basic::Spinner Should Be Invisible
	Click ELement	//*[@id="logs-filter-components"]/div/div[1]/div[2]/div[2]/div[2]/div/div/div[3]/div[2]/div/ul/li[1]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	1s
	Click Element	//*[@id="end-date"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="logs-filter-components"]/div/div[1]/div[3]/div[2]/div[2]/div/div/div[2]/div[2]/div[5]/div[7]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="logs-filter-components"]/div/div[1]/div[3]/div[2]/div[2]/div/div/div[3]/div[2]/div/ul/li[1]
	GUI::Basic::Spinner Should Be Invisible

SUITE:Selection of Priority and Date in Closed Tab
	Click Element	//*[@id="priority-select"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="priority-select-opt-0"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="start-date"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="logs-filter-components"]/div/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[2]/div[1]/div[1]
	GUI::Basic::Spinner Should Be Invisible
	Click ELement	//*[@id="logs-filter-components"]/div/div[1]/div[2]/div[2]/div[2]/div/div/div[3]/div[2]/div/ul/li[1]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="end-date"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="logs-filter-components"]/div/div[1]/div[3]/div[2]/div[2]/div/div/div[2]/div[2]/div[5]/div[7]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="logs-filter-components"]/div/div[1]/div[3]/div[2]/div[2]/div/div/div[3]/div[2]/div/ul/li[1]
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	Close All Browsers