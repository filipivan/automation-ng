*** Settings ***
Documentation	Tests for Updation of Session Timeout
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	ACCESS	CONSOLE
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${Timeout}	60

*** Test Cases ***
Test case Update Session Timeout
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="ca-header-tab-1-7"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="ca-header-tab-2-2"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Press Keys	//*[@name="session_timeout"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@name="session_timeout"]
	Input Text	//*[@name="session_timeout"]	${Timeout}
	Click Button	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait until Page Contains	Sign in.	100s

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Teardown
	Close All Browsers