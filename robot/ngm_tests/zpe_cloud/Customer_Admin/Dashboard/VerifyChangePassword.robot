*** Settings ***
Documentation	Tests for Verification of Valid and Invalid Change Password
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	ACCESS	CONSOLE
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${New_PWD}	.Admin5ystems!2022
${Old_PWD}
${Invalid_PWD}	Admin

*** Test Cases ***
Test case Change Password in the Company
	
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-profile-btn"]/div[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Set Suite Variable	${Old_PWD}	${PWD}
	Click Element	//*[@id="ca-change-password-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="old_password"]	${PWD}
	Click Element	//*[@id="new_password"]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="new_password"]	${New_PWD}
	Click Element	//*[@id="confirm_new_password"]
	Input Text	//*[@id="confirm_new_password"]	${New_PWD}
	Click Button	//*[@id="change-password-submit"]
	Sleep	3s
	Set Suite Variable	${PWD}	${New_PWD}
	Sleep	3s
	SUITE:Login into ZPE Cloud Company

Test case Change Invalid Password in the Company
	
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-profile-btn"]/div[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Click Element	//*[@id="ca-change-password-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="old_password"]	${PWD}
	Click Element	//*[@id="new_password"]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="new_password"]	${Invalid_PWD}
	Click Element	//*[@id="confirm_new_password"]
	Input Text	//*[@id="confirm_new_password"]	${Invalid_PWD}
	Click Button	//*[@id="change-password-submit"]
	Sleep	3s
	Page Should Contain	Invalid password format
	Click Element	//*[@id="dialog-close-btn"]

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Teardown
	Sleep	2s
	Click Element	//*[@id="ca-profile-btn"]/div[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Click Element	//*[@id="ca-change-password-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="old_password"]	${PWD}
	Click Element	//*[@id="new_password"]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="new_password"]	${Old_PWD}
	Click Element	//*[@id="confirm_new_password"]
	Input Text	//*[@id="confirm_new_password"]	${Old_PWD}
	Click Button	//*[@id="change-password-submit"]
	Sleep	3s
	Set Suite Variable	${PWD}	${Old_PWD}
	Sleep	3s
	Close All Browsers