*** Settings ***
Documentation	Tests for Verification and Edit Company Details
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	ACCESS	CONSOLE
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${Old_Companyname}	${COMPANY}
${Address}	India
${New_Companyname}	Test_Company
${New_Address}	USA

*** Test Cases ***
Test case Verify Company Details
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-profile-btn"]/div[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Click Element	//*[@id="ca-company-details-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	Page should contain	${COMPANY}
	Textfield Value Should Be	//*[@name="contact_info"]	${EMAIL}
	Sleep	5s
	Click Button	xpath=//div[3]/div/div/div[2]/button
	GUI::Basic::Spinner Should Be Invisible

Test case Edit Company Details
	Click Element	//*[@id="ca-profile-btn"]/div[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Click Element	//*[@id="ca-company-details-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	xpath=//div[2]/button[2]
	GUI::Basic::Spinner Should Be Invisible
	Press Keys	//*[@name="business_name"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@name="business_name"]
	Input Text	//*[@name="business_name"]	${New_Companyname}
	Press Keys	//*[@name="address"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@name="address"]
	Input Text	//*[@name="address"]	${New_Address}
	Click Button	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Teardown
	Press Keys	//*[@name="business_name"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@name="business_name"]
	Input Text	//*[@name="business_name"]	${Old_Companyname}
	Press Keys	//*[@name="address"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@name="address"]
	Input Text	//*[@name="address"]	${Address}
	Click Button	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers


