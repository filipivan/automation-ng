*** Settings ***
Documentation	Tests for Verification of all tabs in the Company
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	COMPANY ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}

*** Test Cases ***
Test case Verify Sites Tab
	Click Element	//*[@id="ca-header-tab-1-1"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Hostname
	Click Element	//*[@id="ca-header-tab-2-0"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Site Name

Test case Verify Groups Tab
	Click Element	//*[@id="ca-header-tab-1-2"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Hostname
	Click Element	//*[@id="ca-header-tab-2-2"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Email
	Click Element	//*[@id="ca-header-tab-2-0"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Group Name

Test case Verify Devices Tab
	Click Element	//*[@id="ca-header-tab-1-3"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Hostname
	Click Element	//*[@id="ca-header-tab-2-0"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Hostname

Test case Verify Users Tab
	Click Element	//*[@id="ca-header-tab-1-4"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Email

Test case Verify Profiles Tab
	Click Element	//*[@id="ca-header-tab-1-5"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Release Date
	Click Element	//*[@id="ca-header-tab-2-2"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Checksum
	Click Element	//*[@id="ca-header-tab-2-3"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Model
	Click Element	//*[@id="ca-header-tab-2-4"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="simple-tab-1"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Name
	Click Element	//*[@id="simple-tab-0"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	ID
	Click Element	//*[@id="ca-header-tab-2-5"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="function-button-0"]
	Click Element	//*[@id="ca-header-tab-2-6"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="function-button-0"]

Test case Verify Tracking Tab
	Click Element	//*[@id="ca-header-tab-1-6"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="logs-page-ack-tab"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="Confirm"]
	Click Element	//*[@id="logs-page-tab-open-tab"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="Clear"]
	Click Element	//*[@id="ca-header-tab-2-2"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="logs-event-ack-tab"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="filter-btn"]
	Click Element	//*[@id="logs-event-tab-open-tab"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="filter-btn"]
	Click Element	//*[@id="ca-header-tab-2-0"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="function-button-0"]

Test case Verify Settings Tab
	Click Element	//*[@id="ca-header-tab-1-7"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="submit-btn"]
	Click Element	//*[@id="ca-header-tab-2-2"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="close-btn"]
	Click Element	//*[@id="ca-header-tab-2-3"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-settings-sso-wrapper"]/div[1]/div/div/div/button[2]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="ca-settings-cert-generate-btn"]
	Click Element	//*[@id="ca-settings-sso-wrapper"]/div[1]/div/div/div/button[1]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="ca-settings-sso-wrapper"]/div[2]/div[1]/button[1]
	Click Element	//*[@id="ca-header-tab-2-4"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="customer-admin-layout"]/main/div[1]/div/div/div/button[2]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="customer-admin-layout"]/main/div[2]/div[1]/button
	Click Element	//*[@id="customer-admin-layout"]/main/div[1]/div/div/div/button[3]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="customer-admin-layout"]/main/div[2]/div[1]/button[2]
	Click Element	//*[@id="customer-admin-layout"]/main/div[1]/div/div/div/button[1]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="customer-admin-layout"]/main/div[2]/div[1]/button
	Click Element	//*[@id="ca-header-tab-2-5"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="function-button-2"]
	Click Element	//*[@id="ca-header-tab-2-6"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="device_list_checkbox"]
	Click Element	//*[@id="ca-header-tab-2-0"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="simple-tab-1"]
	Click Element	//*[@id="simple-tab-1"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="ca-setop-premise-url-input"]
	Click Element	//*[@id="simple-tab-0"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="ca-setgen-customer-code-input"]

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Teardown
	Close All Browsers

