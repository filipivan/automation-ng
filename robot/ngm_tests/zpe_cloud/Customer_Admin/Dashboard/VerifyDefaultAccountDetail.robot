*** Settings ***
Documentation	Tests for Verification of Account Details
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	ACCESS	CONSOLE
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${Firstname}
${Lastname}
${Phone_number}
${New_Firstname}	Test
${New_Lastname}	Company
${New_Phone_number}	12345678910
${Time}	300
${Old_Firstname}
${Old_Lastname}
${Old_Phone_number}
${Old_Time}

*** Test Cases ***
Test case Get User Details
	
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="ca-header-tab-1-4"]
	GUI::Basic::Spinner Should Be Invisible
	Input text	//*[@id="search-input"]	${EMAIL}
	Sleep	5s
	${Firstname}	Get Text	//*[@id="ca-ugn-table-1"]/div/div/table/tbody/tr/td[3]
	Set Suite Variable	${Old_Firstname}	${Firstname}
	${Lastname}	Get Text	xpath=//div[@id='ca-ugn-table-1']/div/div/table/tbody/tr/td[4]/div
	Set Suite Variable	${Old_Lastname}	${Lastname}
	${Phone_number}	Get Text	xpath=//div[@id='ca-ugn-table-1']/div/div/table/tbody/tr/td[6]/div
	Set Suite Variable	${Old_Phone_number}	${Phone_number}
	Sleep	3s

Test case Verify User Details
	Click Element	//*[@id="ca-profile-btn"]/div[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Click Element	//*[@id="ca-account-details-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	Page should contain	${Firstname}
	Page should contain	${Lastname}
	Page should contain	${EMAIL}
	Page should contain	${Phone_number}

Test case Edit Account Details
	Click Button	xpath=//div[2]/button[2]
	GUI::Basic::Spinner Should Be Invisible
	Press Keys	name=first_name	CTRL+a+BACKSPACE
	Clear Element Text	name=first_name
	Input Text	name=first_name	${New_Firstname}
	Press Keys	name=last_name	CTRL+a+BACKSPACE
	Clear Element Text	name=last_name
	Input Text	name=last_name	${New_Lastname}
	Press Keys	xpath=//div[2]/div[2]/div/div/input	CTRL+a+BACKSPACE
	Clear Element Text	xpath=//div[2]/div[2]/div/div/input
	Input Text	xpath=//div[2]/div[2]/div/div/input	${New_Phone_number}
	Press Keys	name=session_timeout	CTRL+a+BACKSPACE
	Clear Element Text	name=session_timeout
	Input Text	name=session_timeout	${Time}
	Sleep	2s
	Click Button	id=submit-btn
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Teardown
	Press Keys	name=first_name	CTRL+a+BACKSPACE
	Clear Element Text	name=first_name
	Input Text	name=first_name	${Old_Firstname}
	Press Keys	name=last_name	CTRL+a+BACKSPACE
	Clear Element Text	name=last_name
	Input Text	name=last_name	${Old_Lastname}
	Press Keys	xpath=//div[2]/div[2]/div/div/input	CTRL+a+BACKSPACE
	Clear Element Text	xpath=//div[2]/div[2]/div/div/input
	Input Text	xpath=//div[2]/div[2]/div/div/input	${Old_Phone_number}
	Sleep	2s
	Click Button	id=submit-btn
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Close All Browsers


