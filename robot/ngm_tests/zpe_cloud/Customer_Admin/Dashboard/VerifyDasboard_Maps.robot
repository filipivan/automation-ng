*** Settings ***
Documentation	Tests for Verification of HideTopMenu and MapLock
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	ACCESS	CONSOLE
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}

*** Test Cases ***
Test case Hide Top Menu

	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="expand"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Not Contain	Notifications
	Sleep	3s
	Click Element	//*[@id="expand"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Notifications
	Sleep	3s

Test case Verify Map Lock
	CLick Button	//*[@id="ca-dashboard-map-wrapper"]/div[1]/div[2]/button
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Page Should Contain	Map locked successfully
	CLick Button	//*[@id="ca-dashboard-map-wrapper"]/div[1]/div[2]/button
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Page Should Contain	Map unlocked successfully

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Teardown
	Close All Browsers
