*** Settings ***
Documentation	Tests for Verify Release Details
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	COMPANY ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}

*** Test Cases ***
Test case Verify Release Details
	
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-profile-btn"]/div[2]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-about-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//p[contains(.,'Release Notes')]
	GUI::Basic::Spinner Should Be Invisible
	Switch Window	Release Notes
	Sleep	5s
	Page Should Contain	Release Notes

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Teardown
	Close All Browsers


