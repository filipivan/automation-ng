*** Settings ***
Documentation	Tests for Verification of Cloud Version
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	ACCESS	CONSOLE
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-CRITICAL	DEVICE	PART_1

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${Cloud_Version_NG}
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}

*** Test Cases ***
Test case Get Cloud version from NG
	SUITE:Setup
	Click Element	//*[@id="menu_main_tracking"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//div[6]/div/ul/li[9]/a
	GUI::Basic::Spinner Should Be Invisible
	${Version}=	Get Value	//*[@id="version"]
	Set Suite Variable		${Cloud_Version_NG}	${Version}
	Sleep	5s

Test case Verify Cloud version in the Company
	SUITE:Login into ZPE Cloud Company

	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-profile-btn"]/div[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Set Focus to Element	//*[@id="ca-about-btn"]
	Click Element	//*[@id="ca-about-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Page Should Contain	${Cloud_Version_NG}
	Sleep	2s

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Login into Nodegrid
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}

SUITE:Setup
	SUITE:Login into Nodegrid

SUITE:Teardown
	Close All Browsers