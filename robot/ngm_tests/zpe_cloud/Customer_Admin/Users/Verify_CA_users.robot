*** Settings ***
Documentation	Tests for Verify User details
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	COMPANYADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-DEVICE	NON-CRITICAL

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${Company_User_Email}	Company_QAuser@gmail.com
${Company_User_firstname}	CompanyQA
${Company_User_lastname}	User
${Company_User_phone}	1111111111
${User1_Email}	CompanyUser1@gmail.com
${User1_firstname}	CompanyUser
${User1_lastname}	One
${User1_phone}	1191234689
${User2_Email}	Companyuser2@gmail.com
${User2_firstname}	CompanyOROUser
${User2_lastname}	Two
${User2_phone}	1451671891
${Updated_User_Email}	ORO_QAuser@gmail.com
${Updated_firstname}	ORO
${Updated_lastname}	QAuser
${Updated_phone}	1234567890

*** Test Cases ***
Test case Add User as Company User
	Sleep	5s
	Click Button	//*[@id="ca-header-tab-1-4"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Input Text	//*[@id="ca-usergrp-form-field-email"]	${Company_User_Email}
	Input Text	//*[@id="ca-usergrp-form-field-first_name"]	${Company_User_firstname}
	Input Text	//*[@id="ca-usergrp-form-field-last_name"]	${Company_User_lastname}
	Input Text	//*[@id="ca-usergrp-add-dialog"]/div[2]/div/div/div/div[1]/div/div[7]/div/div/input	${Company_User_phone}
	Click Button	//*[@id="submit-btn"]
	Sleep	2s
	GUI::Basic::Spinner Should Be Invisible
	Page Should contain	User added successfully

Test case Add User1
	Click Button	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Input Text	//*[@id="ca-usergrp-form-field-email"]	${User1_Email}
	Input Text	//*[@id="ca-usergrp-form-field-first_name"]	${User1_firstname}
	Input Text	//*[@id="ca-usergrp-form-field-last_name"]	${User1_lastname}
	Input Text	//*[@id="ca-usergrp-add-dialog"]/div[2]/div/div/div/div[1]/div/div[7]/div/div/input	${User1_phone}
	Click Button	//*[@id="submit-btn"]
	Sleep	2s
	GUI::Basic::Spinner Should Be Invisible
	Page Should contain	User added successfully

Test case Add User2
	Click Button	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Input Text	//*[@id="ca-usergrp-form-field-email"]	${User2_Email}
	Input Text	//*[@id="ca-usergrp-form-field-first_name"]	${User2_firstname}
	Input Text	//*[@id="ca-usergrp-form-field-last_name"]	${User2_lastname}
	Input Text	//*[@id="ca-usergrp-add-dialog"]/div[2]/div/div/div/div[1]/div/div[7]/div/div/input	${User2_phone}
	Click Button	//*[@id="submit-btn"]
	Sleep	2s
	GUI::Basic::Spinner Should Be Invisible
	Page Should contain	User added successfully

Test case Edit Valid User in search
	Input Text	//*[@id="search-input"]	${User2_Email}
	Sleep	2s
	Click element	//*[@id="ca-ugn-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="function-button-1"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Press Keys	//*[@id="ca-usergrp-form-field-email"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="ca-usergrp-form-field-email"]
	Input Text	//*[@id="ca-usergrp-form-field-email"]	${Updated_User_Email}
	Press Keys	//*[@id="ca-usergrp-form-field-first_name"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="ca-usergrp-form-field-first_name"]
	Input Text	//*[@id="ca-usergrp-form-field-first_name"]	${Updated_firstname}
	Press Keys	//*[@id="ca-usergrp-form-field-last_name"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="ca-usergrp-form-field-last_name"]
	Input Text	//*[@id="ca-usergrp-form-field-last_name"]	${Updated_lastname}
	Click Button	//*[@id="submit-btn"]
	Sleep	2s
	Wait Until Page Contains	User details edited successfully

Test case Delete User with "KeepUserData"
	Press Keys	//*[@id="search-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="search-input"]
	Input Text	//*[@id="search-input"]	${Company_User_Email}
	Sleep	3s
	Click element	//*[@id="ca-ugn-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="function-button-2"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="ca-ugn-delete-user-dialog"]/div[3]/div/div[1]/fieldset/div/label[1]/span[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page contains	User logs deleted successfully. Your referenced logs are being masked. This operation will take a while.

Test case Delete User with "Completely delete user data"
	Press Keys	//*[@id="search-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="search-input"]
	Input Text	//*[@id="search-input"]	${User1_Email}
	Sleep	2s
	Click element	//*[@id="ca-ugn-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="function-button-2"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="ca-ugn-delete-user-dialog"]/div[3]/div/div[1]/fieldset/div/label[2]/span[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page contains	User logs deleted successfully. Your referenced logs are being masked. This operation will take a while.

Test case Delete User with "Mask user log"
	Press Keys	//*[@id="search-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="search-input"]
	Input Text	//*[@id="search-input"]	${Updated_User_Email}
	Wait Until Page contains	${Updated_User_Email}
	Sleep	2s
	Click element	//*[@id="ca-ugn-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="function-button-2"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="ca-ugn-delete-user-dialog"]/div[3]/div/div[1]/fieldset/div/label[3]/span[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page contains	User logs deleted successfully. Your referenced logs are being masked. This operation will take a while.

Test Case Download User Details in csv
	Sleep	4s
	Click Element	//*[@id="ca-users-general-wrapper"]/div[2]/div[1]/div[2]/button
	GUI::Basic::Spinner Should Be Invisible
	Sleep	4s
	Click Element	//*[@id="menu-list-grow"]/ul/li[2]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	4s

Test Case Download User Details in Excel
	[Tags]	GUI	ZPECLOUD	${BROWSER}	NON-DEVICE	NON-CRITICAL
	Sleep	4s
	Click Element	//*[@id="ca-users-general-wrapper"]/div[2]/div[1]/div[2]/button
	GUI::Basic::Spinner Should Be Invisible
	Sleep	4s
	Mouse Over	//*[@id="menu-list-grow"]/ul/li[2]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="menu-list-grow"]/ul/li[1]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	4s

Test Case Verify RemoveExEmployees Logs
	Click Element	//*[@id="ca-header-tab-1-6"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Page should contain	User Deleted. User: ${Updated_User_Email}.

Test Case Delete Excel Logs
	Remove File	../../../Downloads/User Details.xls

Test Case Delete csv Logs
	Remove File	../../../Downloads/User Details.csv

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Teardown
	Close All Browsers
