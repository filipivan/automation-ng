*** Settings ***
Documentation	Tests for Verification of Cloud Pre-Login Page
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	ACCESS	CONSOLE
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-DEVICE	NON-CRITICAL

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${User}	${EMAIL_ADDRESS}
${Pwd}	${PASSWORD}
${Invalid_User}	invalid@gmail.com
${Invalid_Pwd}	admin

*** Test Cases ***
Test Case Verify fields present in Sign up page.
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	xpath=//form//input[1]
	Page Should Contain Element	css=input[id=filled-adornment-password]
	Page Should Contain Element	//*[@id="oauth-signin-form-login-btn"]
	Page Should Contain Element	//*[@id="okta_okta"]
	Page Should Contain Element	//*[@id="duo_duo"]
	Page Should Contain Element	//*[@id="pingidentity_pingidentity"]
	Page Should Contain Element	//*[@id="adfs_adfs"]
	Page Should Contain Element	//*[@id="azure ad_azure ad"]
	Page Should Contain Element	//*[@id="saml2_saml2"]

Test Case Verify Invalid User Name
	Input Text	xpath=//form//input[1]	${Invalid_User}
	Input Text	css=input[id=filled-adornment-password]	${Pwd}
	Wait Until Page Contains Element	//*[@id="oauth-signin-form-login-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="oauth-signin-form-login-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	Wait Until Page Contains	Unable to log in with provided credentials.

Test Case Verify Invalid Password
	Press Keys	xpath=//form//input[1]	CTRL+a+BACKSPACE
	Clear Element Text	xpath=//form//input[1]
	Input Text	xpath=//form//input[1]	${User}
	Press Keys	css=input[id=filled-adornment-password]	CTRL+a+BACKSPACE
	Clear Element Text	css=input[id=filled-adornment-password]
	Input Text	css=input[id=filled-adornment-password]	${Invalid_Pwd}
	Wait Until Page Contains Element	//*[@id="oauth-signin-form-login-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="oauth-signin-form-login-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	Wait Until Page Contains	Unable to log in with provided credentials.
	Sleep	5s

Test Case Verify Forgot Password for Invalid User
	Press Keys	xpath=//form//input[1]	CTRL+a+BACKSPACE
	Clear Element Text	xpath=//form//input[1]
	Input Text	xpath=//form//input[1]	${Invalid_User}
	Click Element	//*[@id="oauth-signin-form-forgot-password-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="submit-btn"]
	Input Text	//*[@id="email"]	${Invalid_User}
	Click Button	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	5x	1s	Wait Until Page Contains	No such email address was found
	Sleep	5s

Test Case Verify Forgot Password for Valid User
	Press Keys	xpath=//form//input[1]	CTRL+a+BACKSPACE
	Clear Element Text	xpath=//form//input[1]
	Input Text	xpath=//form//input[1]	${USER}
	Click Element	//*[@id="oauth-signin-form-forgot-password-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="submit-btn"]
	Input Text	//*[@id="email"]	${USER}
	Click Button	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	5x	1s	Wait Until Page Contains	An email has been sent to your registered email address
	Sleep	5s

Test Case Verify Copyright text
	[Tags]	GUI	ZPECLOUD	NON-CRITICAL	${BROWSER}	ACCESS	NON-DEVICE
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//a[contains(.,'Privacy Policy')]
	Click Element	xpath=//a[contains(.,'Privacy Policy')]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	15x	1s	Switch Window	Privacy Policy - ZPE Systems - Rethink the Way Networks are Built and Managed
	Wait Until Keyword Succeeds	10x	1s	Page Should Contain	Privacy Policy

*** Keywords ***
SUITE:Open ZPE Cloud Homepage
	GUI::ZPECloud::Basic::Open ZPE Cloud	${ZPECLOUD_HOMEPAGE}	${BROWSER}

SUITE:Setup
	SUITE:Open ZPE Cloud Homepage

SUITE:Teardown
	Close All Browsers
