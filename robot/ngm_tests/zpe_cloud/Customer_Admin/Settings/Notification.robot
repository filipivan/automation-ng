*** Settings ***
Documentation	Tests for Verification of Settings Tab
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	CUSTOMER_ADMIN	ACCESS		CONSOLE
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}

*** Test Cases ***
Test case Verify Alerts Are Enabled in Notification
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-header-tab-1-7"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-header-tab-2-4"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Select Checkbox	//*[@id="customer-admin-layout"]/main/div[2]/div[2]/fieldset/div[1]/label/span[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="customer-admin-layout"]/main/div[2]/div[2]/fieldset/div[2]/label[1]/span[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="customer-admin-layout"]/main/div[2]/div[2]/fieldset/div[2]/label[2]/span[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="customer-admin-layout"]/main/div[2]/div[2]/fieldset/div[3]/label[1]/span[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="customer-admin-layout"]/main/div[2]/div[2]/fieldset/div[3]/label[2]/span[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="customer-admin-layout"]/main/div[2]/div[2]/fieldset/div[4]/label[1]/span[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="customer-admin-layout"]/main/div[2]/div[2]/fieldset/div[4]/label[2]/span[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="customer-admin-layout"]/main/div[2]/div[1]/button
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Page Should Contain	Settings saved successfully

Test Case Verify SMS Settings-Default
	Click Element	//*[@id="customer-admin-layout"]/main/div[1]/div/div/div/button[3]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Select Checkbox	//*[@id="customer-admin-layout"]/main/div[2]/div[2]/div/div[1]/fieldset/div/label/span[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="mui-component-select-service-provider"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="menu-service-provider"]/div[3]/ul/li
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="mui-component-select-configuration-settings"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="menu-configuration-settings"]/div[3]/ul/li[1]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="customer-admin-layout"]/main/div[2]/div[1]/button[1]
	GUI::Basic::Spinner Should Be Invisible

Test Case Verify SMS Settings-Custom
	Click Element	//*[@id="customer-admin-layout"]/main/div[1]/div/div/div/button[3]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Select Checkbox	//*[@id="customer-admin-layout"]/main/div[2]/div[2]/div/div[1]/fieldset/div/label/span[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="mui-component-select-service-provider"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="menu-service-provider"]/div[3]/ul/li
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="mui-component-select-configuration-settings"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="menu-configuration-settings"]/div[3]/ul/li[2]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="mui-component-select-region-name"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="menu-region-name"]/div[3]/ul/li[1]
	Click Element	xpath=//li[contains(.,'us-east-1')]
	Capture Page Screenshot
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Enabled	//*[@id="customer-admin-layout"]/main/div[2]/div[1]/button[1]
	Click Button	//*[@id="customer-admin-layout"]/main/div[2]/div[1]/button[1]
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Login into ZPE Cloud SuperAdmin
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud SuperAdmin

SUITE:Teardown
	Close All Browsers
