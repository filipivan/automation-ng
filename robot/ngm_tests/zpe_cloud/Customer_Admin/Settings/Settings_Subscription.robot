*** Settings ***
Documentation	Tests for Verification of Settings-Subscription Tab
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	CUSTOMER_ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-DEVICE	BUG_CLOUD_8372

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${SUPER_EMAIL_ADDRESS}
${PWD}	${SUPER_PASSWORD}
${COMPANY_EMAIL}	${EMAIL_ADDRESS}
${COMPANY_PWD}	${PASSWORD}
${Description}	ZPE Cloud License - 3 YEAR - Subscription - Reports App
${Type}	Subscription
${Invalid_Subscription}	Invalid Subscription

*** Test Cases ***
Test case Add apps to the Company in super-admin
	Sleep	3s
	Click Element	//*[@id="sa-header-tab-1-7"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	Click Element	xpath=//div[@id='mui-component-select-company-id']
	GUI::Basic::Spinner Should Be Invisible
	Set Focus To Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	Click Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	Select Checkbox	//*[@id="2"]
	Set Focus To Element	xpath=//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s

Test case Add Subscription in the Super admin
	Click Element	//*[@id="sa-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Click Button	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Input text	//*[@id="sa-apps-add-licence-dialog"]/div[2]/div/div/div[1]/div/div/div/div[1]/input	${COMPANY}
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Click Element	//li[contains(.,"${COMPANY}")]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="select-app"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Wait Until Page Contains Element	//*[@id="app-2"]
	Click Element	//*[@id="app-2"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Click Element	//*[@id="select-licence"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="licence-3"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	//*[@id="submit-btn"]	timeout=15s
	Wait Until Element Is Enabled	//*[@id="submit-btn"]	timeout=15s
	Click Button	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Clear Element Text	//*[@name="cc"]
	Press Keys	//*[@name="cc"]	CTRL+a+BACKSPACE
	Sleep	2s
	Click Button	//*[@id="function-button-1"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Subscription Added Successfully

Test case Verify Subscription Search Button in Company
	SUITE:Login into ZPE Cloud Company
	Click Element	//*[@id="ca-header-tab-1-7"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-header-tab-2-5"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="search-input"]

Test case Verify Subscription Filter-Cloud
	Sleep	3s
	Click Element	//*[@id="mui-component-select-subscription-type"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="menu-subscription-type"]/div[3]/ul/li[2]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	ZPE Cloud

Test case Verify Subscription Filter-App
	Click Element	//*[@id="mui-component-select-subscription-type"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="menu-subscription-type"]/div[3]/ul/li[3]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	App

Test case Verify Subscription Filter-All
	Click Element	//*[@id="mui-component-select-subscription-type"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="menu-subscription-type"]/div[3]/ul/li[1]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	ZPE Cloud
	Page Should Contain	App

Test case Verify Subscription Description in Search in Company
	Input Text	//*[@id="search-input"]	${Description}
	Sleep	2s
	Wait Until Page Contains	${Company}
	Page Should Contain	${Type}

Test Case Verify Renew Button Enabled in Company
	Click Element	//*[@id="ca-license-settings-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="function-button-1"]

Test Case Verify Activation Button Enabled in Company
	Wait Until Page Contains Element	//*[@id="function-button-0"]

Test case Verify Invalid Subscription Search in Company
	Press Keys	//*[@id="search-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="search-input"]
	Input Text	//*[@id="search-input"]	${Invalid_Subscription}
	Sleep	2s
	Wait Until Page Contains	No result found

*** Keywords ***
SUITE:Login into ZPE Cloud SuperAdmin
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${COMPANY_EMAIL}	${COMPANY_PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud SuperAdmin

SUITE:Teardown
	SUITE:Login into ZPE Cloud SuperAdmin
	Click Element	//*[@id="sa-header-tab-1-7"]
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	Click Element	xpath=//div[@id='mui-component-select-company-id']
	Set Focus To Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	Click Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	GUI::Basic::Spinner Should Be Invisible
	sleep	3s
	Unselect Checkbox	//*[@id="2"]
	Set Focus To Element	xpath=//*[@id="sa-apps-save-btn"]
	Sleep	5s
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	SUITE:Login into ZPE Cloud Company
	Page Should Not Contain	//*[contains(text(), 'Apps')]
	Sleep	10s
	Close All Browsers





