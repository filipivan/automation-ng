*** Settings ***
Documentation	Tests for Invalid lattitude and longitude
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	ACCESS	CONSOLE
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-DEVICE	#BUG_CLOUD_6484 fixed

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${Name of Site}	Test_Site
${Address}	India
${Latitude}	22.3511148
${Longitude}	78.6677428
${Invalid_SiteName}	Test_Site
${Invalid_Address}	Test
${Invalid_Latitude}	98
${Invalid_Longitude}	200
${Invalid_Site}	Invalid_Site
${Updated_SiteName}	Test_Site-Updated
${Updated_Address}	Brazil
${Updated_Latitude}	-10.3333333
${Updated_Longitude}	-53.2

*** Test Cases ***
Test case Verify "No site"
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="ca-header-tab-1-1"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Page should contain	No result found

Test case Invalid Latitude
	Wait Until Page Contains Element	//*[@id="function-button-0"]
	Click Button	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Input Text	//*[@name="name"]	${Invalid_SiteName}
	Input Text	//*[@name="address"]	${Invalid_Address}
	Sleep	2s
	Press Keys	//*[@name="latitude"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@name="latitude"]
	Input Text	//*[@name="latitude"]	${Invalid_Latitude}
	Press Keys	//*[@name="longitude"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@name="longitude"]
	Input Text	//*[@name="longitude"]	${Longitude}
	Sleep	3s
	Click Button	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Latitude is not valid!

Test case Invalid Longitude
	Press Keys	//*[@name="latitude"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@name="latitude"]
	Input Text	//*[@name="latitude"]	${Latitude}
	Press Keys	//*[@name="longitude"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@name="longitude"]
	Input Text	//*[@name="longitude"]	${Invalid_Longitude}
	Sleep	3s
	Click Button	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Longitude is not valid!

Test case Add Site in the Company
	Press Keys	//*[@name="name"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@name="name"]
	Input Text	//*[@name="name"]	${Name of Site}
	Press Keys	//*[@name="address"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@name="address"]
	Input Text	//*[@name="address"]	${Address}
	Sleep	2s
	Press Keys	//*[@name="latitude"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@name="latitude"]
	Input Text	//*[@name="latitude"]	${Latitude}
	Press Keys	//*[@name="longitude"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@name="longitude"]
	Input Text	//*[@name="longitude"]	${Longitude}
	Sleep	3s
	Click Button	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	Page Should Contain	${Name of Site}

Test Case Edit Site Details
	Click Element	//*[@id="ca-sg-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="function-button-1"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Press Keys	//*[@name="name"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@name="name"]
	Input Text	//*[@name="name"]	${Updated_SiteName}
	Press Keys	//*[@name="address"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@name="address"]
	Input Text	//*[@name="address"]	${Updated_Address}
	Sleep	2s
	Press Keys	//*[@name="latitude"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@name="latitude"]
	Input Text	//*[@name="latitude"]	${Updated_Latitude}
	Press Keys	//*[@name="longitude"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@name="longitude"]
	Input Text	//*[@name="longitude"]	${Updated_Longitude}
	Sleep	3s
	Click Button	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	Page Should Contain	${Updated_SiteName}

Test case Search Invalid Site_Name
	Input Text	//*[@id="search-input"]	${Invalid_Site}
	Sleep	2s
	Page Should Contain	No result found

Test case Search valid Site_Name
	Press Keys	//*[@id="search-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="search-input"]
	Input Text	//*[@id="search-input"]	${Updated_SiteName}
	Wait Until Page Contains	${Updated_SiteName}

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Teardown
	Sleep	2s
	Click Element	//*[@id="ca-sg-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="function-button-2"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Page should not contain	${Updated_SiteName}
	Sleep	5s
	Close All Browsers
