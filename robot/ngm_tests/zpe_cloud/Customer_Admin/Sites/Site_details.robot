*** Settings ***
Documentation	Tests for Site Details and Assign devices to site in Company
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	CUSTOMER_ADMIN		ACCESS		CONSOLE
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-CRITICAL	DEVICE	PART_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${Name of Site}	Test_Site
${Address}	India
${Latitude}	22.3511148
${Longitude}	78.6677428
${CUSTOMERCODE}
${ENROLLMENTKEY}
${ENROLLMENTSUCCESS}	Enrollment process successful!

*** Test Cases ***
Test case Add Site in the Company
	Click Button	//*[@id="ca-header-tab-1-1"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	Click Button	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@name="name"]	${Name of Site}
	Input Text	//*[@name="address"]	${Address}
	Input Text	//*[@name="latitude"]	${Latitude}
	Input Text	//*[@name="longitude"]	${Longitude}
	Sleep	3s
	Click Button	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	Page Should Contain	${Name of Site}
	Sleep	5s

Test case Add Device to Site in the Company
	Click Button	//*[@id="ca-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	Select Checkbox	//*[@id="ca-sd-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="function-button-0"]
	Input Text	//*[@id="search-input"]	${Name of Site}
	Select Checkbox	//*[@id="ca-sd-table-2"]/div/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="function-button-1"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s

Test case Unassign Device to Site in the Company
	Click Button	//*[@id="ca-header-tab-1-1"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="ca-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="search-input"]	${SERIALNUMBER}
	Wait Until Page Contains	${SERIALNUMBER}
	GUI::Basic::Spinner Should Be Invisible
	Click element	//*[@id="ca-sd-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="function-button-1"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Removed from Site

Test Case Delete Site in the Company
	Click Button	//*[@id="ca-header-tab-1-1"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Input Text	//*[@id="search-input"]	${Name of Site}
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Select Checkbox	//*[@id="ca-sg-table-1"]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="function-button-2"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Page should not contain	${Name of Site}
	Sleep	5s

Test case Verify Site logs
	Click Button	//*[@id="ca-header-tab-1-6"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="ca-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="Confirm"]
	Wait Until Page Contains Element	//*[@id="ca-logs-customer-table-1"]/div[1]/div/table/tbody/tr[1]
	Page Should Contain	Site Deleted.
	Page Should Contain	Device Added to a Site.
	Page Should Contain	Site Created.

*** Keywords ***
SUITE:Setup
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud

SUITE:Login into Nodegrid
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}

SUITE:Teardown
	Close All Browsers
