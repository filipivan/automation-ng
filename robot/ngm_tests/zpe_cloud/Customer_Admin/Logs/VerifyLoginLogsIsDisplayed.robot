*** Settings ***
Documentation	Tests for Display login details
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	ACCESS	CONSOLE
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${Invalid_user}	ABC@gmail.com
${Configuration_Name}	Test

*** Test Cases ***
Test case Verify login page on logs
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="ca-header-tab-1-6"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="ca-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="Confirm"]
	Wait Until Page Contains Element	//*[@id="ca-logs-customer-table-1"]/div[1]/div/table/tbody/tr[1]
	Page Should Contain	User Authenticated Successfully.

Test Case Verify confirm button
	Click Button	//*[@id="Confirm"]
	Wait Until Page Contains	${EMAIL}

Test case Verify Total Active Sessions
	Click Button	//*[@id="ca-header-tab-2-0"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Page Should Contain	Total Sessions:

Test case Terminate Button
	Click Element	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Teardown
	Close All Browsers
