*** Settings ***
Documentation	Tests for Verify Device logs in Tracking-Company Admin
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	COMPANY_ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${Date}
${Start_Date}

*** Test Cases ***
Test case Verify Confirm Button
	Sleep	2s
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="ca-header-tab-1-6"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="logs-page-ack-tab"]
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Selection of Device and Date
	Click Element	//*[@id="Confirm"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="ca-logs-customer-table-1"]/div[1]/div/table/tbody/tr/td[1]
	Sleep	2s

Test Case Verify Clear Search Button
	Click Element	//*[@id="Clear"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Page Should Not Contain	${Date}

Test case Download logs for Cloud in Excel
	SUITE:Selection of Device and Date
	Wait Until Page Contains Element	//*[@id="Download"]
	Sleep	2s
	Click Element	//*[@id="Download"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="mui-component-select-selectLicense"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="menu-selectLicense"]/div[3]/ul/li[1]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s

Test case Download logs for Cloud in csv
	Wait Until Page Contains Element	//*[@id="Download"]	10s
	Sleep	15s
	Click Element	//*[@id="Download"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="mui-component-select-selectLicense"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="menu-selectLicense"]/div[3]/ul/li[2]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s

Test Case Delete Excel Logs
	Remove File	../../../../Downloads/1. Device logs.xlsx

Test Case Delete csv Logs
	Remove File	../../../Downloads/1. Device logs.csv

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Selection of Device and Date
	Click Element	//*[@id="mui-component-select-device-id"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Click Element	//*[@id="menu-device-id"]/div[3]/ul/li[1]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-logs-site-startdate"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-logs-device-wrapper"]/div[2]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[2]/div[1]/div[1]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-logs-device-wrapper"]/div[2]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div/div/div[3]/div[2]/div/ul/li[1]
	GUI::Basic::Spinner Should Be Invisible
	${Start_Date}=	Get Value	//*[@id="ca-logs-site-startdate"]
	Set Suite Variable		${Date}	${Start_Date}
	Click Element	//*[@id="ca-logs-site-enddate"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-logs-device-wrapper"]/div[2]/div[1]/div/div[1]/div[3]/div[2]/div[2]/div/div/div[2]/div[2]/div[5]/div[7]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-logs-device-wrapper"]/div[2]/div[1]/div/div[1]/div[3]/div[2]/div[2]/div/div/div[3]/div[2]/div/ul/li[96]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	1s

SUITE:Teardown
	Close All Browsers
