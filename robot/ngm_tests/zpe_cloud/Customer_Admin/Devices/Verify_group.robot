*** Settings ***
Documentation	Tests for Verification of Group Name in device tab.
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	CUSTOMER_ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_1

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${Name of Group}	Test_Group
${CUSTOMERCODE}
${ENROLLMENTKEY}
${ENROLLMENTSUCCESS}	Enrollment process successful!

*** Test Cases ***
Test case to Enroll device on cloud.
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	SUITE:Teardown

Test Case Add group in the Company
    SUITE:Setup
	GUI::ZPECloud::Groups::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	SUITE:ADD Group
	Sleep	3s
    SUITE:Teardown

Test case Add Device to Group in the Company
    SUITE:Setup
	GUI::ZPECloud::Groups::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Select Device
	SUITE:Assign device to group
	Sleep	3s
	SUITE:Teardown

Test case Verify group name in device tab
    SUITE:Setup
	GUI::ZPECloud::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Verify group details for the device
	Sleep	3s
	SUITE:Teardown

Test case Unassign Device to Group in the Company
    SUITE:Setup
	GUI::ZPECloud::Groups::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Unassign Device from group
	Sleep	3s
	SUITE:Teardown

Test Case Delete group in the Company
    SUITE:Setup
	GUI::ZPECloud::Groups::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	SUITE:Delete Group
	Page should not contain	${Name of Group}
	Sleep	5s
	SUITE:Teardown

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Login into Nodegrid
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}

SUITE:ADD Group
	Click Button	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@name="name"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Input Text	//*[@name="name"]	${Name of Group}
	Sleep	3s
	Select Radio Button	groupManagement	3
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Click Button	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Group added successfully        30s

SUITE:Select Device
	Input Text	//*[@id="search-input"]	${SERIALNUMBER}
	Sleep	3s
	Select Checkbox	//*[@id="ca-group-devices-wrapper"]/div[2]/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible

SUITE:Assign device to group
	Click Button	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="search-input"]	${Name of Group}
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Click Element	xpath=//div[contains(.,'${Name of Group}')]/../../td[1]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="function-button-1"]
	GUI::Basic::Spinner Should Be Invisible

SUITE:Unassign Device from group
	Wait Until Page Contains	${SERIALNUMBER}
	Click Element	//*[@id="mui-component-select-group-assigned"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="menu-group-assigned"]/div[3]/ul/li[contains(.,"${Name of Group}")]
	Input Text	//*[@id="search-input"]	${SERIALNUMBER}
	Wait Until Page Contains	${SERIALNUMBER}
	Sleep	2s
	Select Checkbox	//*[@id="ca-group-devices-wrapper"]/div[2]/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="function-button-1"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Devices Removed from Group      30s
	GUI::Basic::Spinner Should Be Invisible

SUITE:Delete Group
	Input Text	//*[@id="search-input"]	${Name of Group}
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Select Checkbox	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Click Button	//*[@id="function-button-2"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Group deleted successfully      30s

SUITE:Verify group details for the device
	Input Text	//*[@id="search-input"]	${SERIALNUMBER}
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Page Should Contain	${SERIALNUMBER}
	Page Should Contain	${Name of Group}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Teardown
	Close All Browsers
