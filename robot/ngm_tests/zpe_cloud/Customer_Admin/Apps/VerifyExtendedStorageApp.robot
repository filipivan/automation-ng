*** Settings ***
Documentation	Tests for Activation of reports app and verify Extendedstorage app
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	CUSTOMER_ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	BUG_CLOUD_7289	BUG_CLOUD_7151	BUG_CLOUD_7373	DEVICE	PART_1	#BUG_CLOUD_7288

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${SUPER_EMAIL_ADDRESS}
${PWD}	${SUPER_PASSWORD}
${COMPANY_EMAIL}	${EMAIL_ADDRESS}
${COMPANY_PWD}	${PASSWORD}
${FILE_TXT}	${CURDIR}/Test_Files_For_Reports/first_version/test.txt
${FILE_TXT1}	${CURDIR}/Test_Files_For_Reports/second_version/test1.txt
${FILE_TXT2}	${CURDIR}/Test_Files_For_Reports/first_version/test2.txt
${COPYSUCCESS}	1 items copied.
${DELETESUCCESS}	Files deleted successfully
${FILEREVERSED}	File reverted successfully
${DOWNLOADSUCCESS}	Your download will start soon
${CLONESUCCESS}	File cloned successfully

*** Test Cases ***
Test case Add apps to the Company in super-admin
	Click Element	//*[@id="sa-header-tab-1-7"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//div[@id='mui-component-select-company-id']
	GUI::Basic::Spinner Should Be Invisible
	Set Focus To Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	Click Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	GUI::Basic::Spinner Should Be Invisible
	${MENU_EXISTS}=	Run Keyword And Return Status	GUI::Element Exists	//*[@id='menu-company-id']
	Run Keyword If	${MENU_EXISTS}	Wait Until Element Is Not Visible	//*[@id='menu-company-id']	timeout=1m
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="4"]
	GUI::Basic::Spinner Should Be Invisible
	Checkbox Should Be Selected	//*[@id="4"]
	Set Focus To Element	xpath=//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	[Teardown]	Close All Browsers

Test case to activate Extended storage in the Company.
	[Tags]	GUI	ZPECLOUD	${BROWSER}	BUG_CLOUD_7289	BUG_CLOUD_7151	BUG_CLOUD_7373	NON-CRITICAL	DEVICE	#BUG_CLOUD_7288
	${EXTENDES_ACTIVATED}=	Set Variable	${FALSE}
	Set Suite Variable	${EXTENDES_ACTIVATED}
	SUITE:Login into ZPE Cloud Company
	Click Element	//*[contains(text(), 'Apps')]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	xpath=//h6[contains(.,'Extended Storage')]	timeout=1m
	Click Element	xpath=//h6[contains(.,'Extended Storage')]
	GUI::Basic::Spinner Should Be Invisible
	${EXISTS}=	Run Keyword And Return Status	GUI::Element Exists	//*[@role='progressbar']//*[name()='svg']	#BUG_CLOUD_7373
	Run Keyword If	${EXISTS}	Wait Until Element Is Not Visible	//*[@role='progressbar']//*[name()='svg']	timeout=1m
	Click Element	//*[@id="ca-available-apps-wrapper"]/div[4]/form/div[2]/div/div[1]/div[1]/div[4]/button[2]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//button[contains(.,'YES')]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Not Visible	//*[contains(@class,'MuiLinearProgress-bar')]	timeout=1m
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//button[@id='ca-header-tab-2-0']
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	xpath=//h6[contains(.,'Extended Storage')]	timeout=1m
	Click Element	xpath=//h6[contains(.,'Extended Storage')]
	GUI::Basic::Spinner Should Be Invisible
	${EXISTS}=	Run Keyword And Return Status	GUI::Element Exists	//*[@role='progressbar']//*[name()='svg']	#BUG_CLOUD_7373
	Run Keyword If	${EXISTS}	Wait Until Element Is Not Visible	//*[@role='progressbar']//*[name()='svg']	timeout=1m
	Wait Until Element Is Visible	xpath=//button[contains(.,'STORAGE')]	timeout=5m
	GUI::Basic::Spinner Should Be Invisible
	${EXTENDES_ACTIVATED}=	Set Variable	${TRUE}
	Set Suite Variable	${EXTENDES_ACTIVATED}
	[Teardown]	Run Keywords	Close All Browsers	AND	Run Keyword If	${EXTENDES_ACTIVATED} == ${FALSE}	Fail	Could not activate Extended storage due BUG_CLOUD_7151

Test case to Create Folder
	[Tags]	GUI	ZPECLOUD	${BROWSER}	BUG_CLOUD_7289	BUG_CLOUD_7151	BUG_CLOUD_7373	NON-CRITICAL	DEVICE	#BUG_CLOUD_7288
	Run Keyword If	${EXTENDES_ACTIVATED} == ${FALSE}	Fail	Could not activate Extended storage	due BUG_CLOUD_7151
	SUITE:Login into ZPE Cloud Company
	SUITE:Login on Extended Storage
	Wait Until Keyword Succeeds	5 min	2	Wait Until Page Contains Element	//div[@class="chonky-toolbar-button-group"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="chonky-toolbar-button-group"]
	Click Element	//div[@class="chonky-toolbar-dropdown-content"]/button[1]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="extended-storage-new-folder-name"]	test
	Click Element	xpath=//div[2]/button[2]
	Wait Until Page Contains Element	xpath=//div[@title="test"]
	GUI::Basic::Spinner Should Be Invisible
	[Teardown]	Close All Browsers

Test case to Upload Files
	[Tags]	GUI	ZPECLOUD	${BROWSER}	BUG_CLOUD_7289	BUG_CLOUD_7151	BUG_CLOUD_7373	NON-CRITICAL	DEVICE	#BUG_CLOUD_7288
	Run Keyword If	${EXTENDES_ACTIVATED} == ${FALSE}	Fail	Could not activate Extended storage due BUG_CLOUD_7151
	SUITE:Login into ZPE Cloud Company
	SUITE:Login on Extended Storage
	SUITE:Upload New Files	${FILE_TXT}
	Wait Until Page Contains Element	xpath=//div[@title="test.txt"]
	[Teardown]	Close All Browsers

Test case to Download Files
	[Tags]	GUI	ZPECLOUD	${BROWSER}	BUG_CLOUD_7289	BUG_CLOUD_7151	BUG_CLOUD_7373	NON-CRITICAL	DEVICE	#BUG_CLOUD_7288
	Run Keyword If	${EXTENDES_ACTIVATED} == ${FALSE}	Fail	Could not activate Extended storage due BUG_CLOUD_7151
	SUITE:Login into ZPE Cloud Company
	SUITE:Login on Extended Storage
	Wait Until Keyword Succeeds	5 min	2	Wait Until Page Contains Element	xpath=//div[@title="test.txt"]
	${FILE}	Get WebElement	//*[@title='test.txt']
	Execute Javascript	arguments[0].click();	ARGUMENTS	${FILE}
	Wait Until Page Contains Element	//div[@class="chonky-toolbar-button-group"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="chonky-toolbar-button-group"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="chonky-toolbar-dropdown-content"]/button[5]
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	${DOWNLOADSUCCESS}
	[Teardown]	Close All Browsers

Test case to Copy and Paste Files
	[Tags]	GUI	ZPECLOUD	${BROWSER}	BUG_CLOUD_7289	BUG_CLOUD_7151	BUG_CLOUD_7373	NON-CRITICAL	DEVICE	#BUG_CLOUD_7288
	Run Keyword If	${EXTENDES_ACTIVATED} == ${FALSE}	Fail	Could not activate Extended storage due BUG_CLOUD_7151
	SUITE:Login into ZPE Cloud Company
	SUITE:Login on Extended Storage
	Wait Until Keyword Succeeds	5 min	2	Wait Until Page Contains Element	xpath=//div[@title="test.txt"]
	${FILE}	Get WebElement	//*[@title='test.txt']
	Execute Javascript	arguments[0].click();	ARGUMENTS	${FILE}
	Wait Until Page Contains Element	//div[@class="chonky-toolbar-button-group"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="chonky-toolbar-button-group"]
	Wait Until Page Contains Element	//div[@class="chonky-toolbar-dropdown-content"]/button[2]
	Click Element	//div[@class="chonky-toolbar-dropdown-content"]/button[2]
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	${COPYSUCCESS}
	GUI::Basic::Spinner Should Be Invisible
	${FILE1}	Get WebElement	xpath=//div[@title="test"]
	Execute Javascript	arguments[0].click();	ARGUMENTS	${FILE1}
	Execute Javascript	arguments[0].click();	ARGUMENTS	${FILE1}
	Wait Until Page Contains Element	//div[@class="chonky-toolbar-button-group"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="chonky-toolbar-button-group"]
	Wait Until Page Contains Element	//div[@class="chonky-toolbar-button-group"]/div/div/button[6]
	Click Element	//div[@class="chonky-toolbar-button-group"]/div/div/button[6]
	[Teardown]	Close All Browsers

Test case to Revert Version of File
	[Tags]	GUI	ZPECLOUD	${BROWSER}	BUG_CLOUD_7289	BUG_CLOUD_7151	BUG_CLOUD_7373	NON-CRITICAL	DEVICE	#BUG_CLOUD_7288
	Run Keyword If	${EXTENDES_ACTIVATED} == ${FALSE}	Fail	Could not activate Extended storage due BUG_CLOUD_7151
	SUITE:Login into ZPE Cloud Company
	SUITE:Login on Extended Storage
	Wait Until Keyword Succeeds	5 min	2	Wait Until Page Contains Element	xpath=//div[@title="test"]
	${FILE1}	Get WebElement	xpath=//div[@title="test.txt"]
	Execute Javascript	arguments[0].click();	ARGUMENTS	${FILE1}
	Execute Javascript	arguments[0].click();	ARGUMENTS	${FILE1}
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Upload New Files	${FILE_TXT1}
	Wait Until Page Contains Element	xpath=//div[@title="test1.txt"]
	${FILE2}	Get WebElement	xpath=//div[@title="test1.txt"]
	Execute Javascript	arguments[0].click();	ARGUMENTS	${FILE2}
	Wait Until Page Contains Element	//div[@class="chonky-toolbar-button-group"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="chonky-toolbar-button-group"]
	Click Element	//div[@class="chonky-toolbar-dropdown-content"]/button[7]
	GUI::Basic::Spinner Should Be Invisible
	${FILE3}	Get WebElement	//div[@class='chonky-file-entry-description'][contains(.,'test1')]
	Execute Javascript	arguments[0].click();	ARGUMENTS	${FILE3}
	Wait Until Page Contains Element	//div[@class="chonky-toolbar-button-group"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="chonky-toolbar-button-group"]
	Click Element	//div[@class="chonky-toolbar-dropdown-content"]/button[4]
	Click Element	xpath=//div[2]/button[2]
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	${FILEREVERSED}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="chonky-folder-chain"]/button[2]
	${FILE4}	Get WebElement	//div[@class='chonky-file-entry-description'][contains(.,'test1')]
	[Teardown]	Close All Browsers

Test case to Download Version
	[Tags]	GUI	ZPECLOUD	${BROWSER}	BUG_CLOUD_7289	BUG_CLOUD_7151	BUG_CLOUD_7373	NON-CRITICAL	DEVICE	#BUG_CLOUD_7288
	Run Keyword If	${EXTENDES_ACTIVATED} == ${FALSE}	Fail	Could not activate Extended storage due BUG_CLOUD_7151
	SUITE:Login into ZPE Cloud Company
	SUITE:Login on Extended Storage
	Wait Until Keyword Succeeds	5 min	2	Wait Until Page Contains Element	xpath=//div[@title="test.txt"]
	${FILE1}	Get WebElement	xpath=//div[@title="test.txt"]
	Execute Javascript	arguments[0].click();	ARGUMENTS	${FILE1}
	Wait Until Page Contains Element	//div[@class="chonky-toolbar-button-group"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="chonky-toolbar-button-group"]
	Click Element	//div[@class="chonky-toolbar-dropdown-content"]/button[7]
	GUI::Basic::Spinner Should Be Invisible
	${FILE2}	Get WebElement	//div[@class='chonky-file-entry-description'][contains(.,'test')]
	Execute Javascript	arguments[0].click();	ARGUMENTS	${FILE2}
	Wait Until Page Contains Element	//div[@class="chonky-toolbar-button-group"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="chonky-toolbar-button-group"]
	Click Element	//div[@class="chonky-toolbar-dropdown-content"]/button[3]
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	${DOWNLOADSUCCESS}
	GUI::Basic::Spinner Should Be Invisible
	[Teardown]	Close All Browsers

Test case to Clone Version
	[Tags]	GUI	ZPECLOUD	${BROWSER}	BUG_CLOUD_7289	BUG_CLOUD_7151	BUG_CLOUD_7373	NON-CRITICAL	DEVICE	#BUG_CLOUD_7288
	Run Keyword If	${EXTENDES_ACTIVATED} == ${FALSE}	Fail	Could not activate Extended storage due BUG_CLOUD_7151
	SUITE:Login into ZPE Cloud Company
	SUITE:Login on Extended Storage
	${FILE3}	Get WebElement	xpath=//div[@title="test.txt"]
	Execute Javascript	arguments[0].click();	ARGUMENTS	${FILE3}
	Execute Javascript	arguments[0].click();	ARGUMENTS	${FILE3}
	Wait Until Page Contains Element	//div[@class="chonky-toolbar-button-group"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="chonky-toolbar-button-group"]
	Wait Until Page Contains Element	//div[@class="chonky-toolbar-dropdown-content"]/button[7]
	Click Element	//div[@class="chonky-toolbar-dropdown-content"]/button[7]
	GUI::Basic::Spinner Should Be Invisible
	${FILE4}	Get WebElement	//*[text()='test']
	Execute Javascript	arguments[0].click();	ARGUMENTS	${FILE4}
	Wait Until Page Contains Element	//div[@class="chonky-toolbar-button-group"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="chonky-toolbar-button-group"]
	Click Element	//div[@class="chonky-toolbar-dropdown-content"]/button[1]
	Input Text	//*[@id="extended-storage-clone-file-name"]	test1.txt
	Click Element	xpath=//div[3]/div/div[3]/button[2]
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	${CLONESUCCESS}
	Wait Until Page Contains Element	//div[@class="chonky-toolbar-button-group"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="chonky-toolbar-button-group"]
	Click Element	//div[@class="chonky-toolbar-dropdown-content"]/button[5]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//div[@title="test1.txt"]
	[Teardown]	Close All Browsers

Test case to Delete Version
	[Tags]	GUI	ZPECLOUD	${BROWSER}	BUG_CLOUD_7289	BUG_CLOUD_7151	BUG_CLOUD_7373	NON-CRITICAL	DEVICE	#BUG_CLOUD_7288
	Run Keyword If	${EXTENDES_ACTIVATED} == ${FALSE}	Fail	Could not activate Extended storage due BUG_CLOUD_7151
	SUITE:Login into ZPE Cloud Company
	SUITE:Login on Extended Storage
	Wait Until Keyword Succeeds	5 min	2	Wait Until Page Contains Element	xpath=//div[@title="test.txt"]
	${FILE2}	Get WebElement	xpath=//div[@title="test.txt"]
	Execute Javascript	arguments[0].click();	ARGUMENTS	${FILE2}
	Execute Javascript	arguments[0].click();	ARGUMENTS	${FILE2}
	Wait Until Page Contains Element	//div[@class="chonky-toolbar-button-group"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="chonky-toolbar-button-group"]
	Wait Until Page Contains Element	//div[@class="chonky-toolbar-dropdown-content"]/button[7]
	Click Element	//div[@class="chonky-toolbar-dropdown-content"]/button[7]
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Delete Folder or File	text()='test'	2
	[Teardown]	Close All Browsers

Test case to Put File in Nodegrid with Copy and Paste
	[Tags]	GUI	ZPECLOUD	${BROWSER}	BUG_CLOUD_7289	BUG_CLOUD_7151	BUG_CLOUD_7373	NON-CRITICAL	DEVICE	#BUG_CLOUD_7288
	Run Keyword If	${EXTENDES_ACTIVATED} == ${FALSE}	Fail	Could not activate Extended storage due BUG_CLOUD_7151
	SUITE:Login into ZPE Cloud Company
	SUITE:Login on Extended Storage
	Wait Until Keyword Succeeds	5 min	2	Wait Until Page Contains Element	xpath=//div[@title="test1.txt"]
	${FILE}	Get WebElement	xpath=//div[@title="test1.txt"]
	Execute Javascript	arguments[0].click();	ARGUMENTS	${FILE}
	Wait Until Page Contains Element	//div[@class="chonky-toolbar-button-group"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="chonky-toolbar-button-group"]
	Click Element	//div[@class="chonky-toolbar-dropdown-content"]/button[2]
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	${COPYSUCCESS}
	Wait Until Page Contains Element	//div[@class="chonky-toolbar-button-group"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="chonky-toolbar-button-group"]
	Input Text	//input[contains(@class,'MuiInputBase-input MuiOutlinedInput-input css-1x5jdmq')]	${HOSTNAME_NODEGRID}
	${FILE1}	Get WebElement	//*[text()='${HOSTNAME_NODEGRID}']
	Execute Javascript	arguments[0].click();	ARGUMENTS	${FILE1}
	Wait Until Page Contains Element	//div[@class="chonky-toolbar-button-group"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="chonky-toolbar-button-group"]
	Click Element	//div[@class="chonky-toolbar-dropdown-content"]/button[6]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//div[@title="test1.txt"]
	[Teardown]	Close All Browsers

Test case to Put File in Nodegrid with Upload File
	[Tags]	GUI	ZPECLOUD	${BROWSER}	BUG_CLOUD_7289	BUG_CLOUD_7151	BUG_CLOUD_7373	NON-CRITICAL	DEVICE	#BUG_CLOUD_7288
	Run Keyword If	${EXTENDES_ACTIVATED} == ${FALSE}	Fail	Could not activate Extended storage due BUG_CLOUD_7151
	SUITE:Login into ZPE Cloud Company
	SUITE:Login on Extended Storage
	Input Text	//input[contains(@class,'MuiInputBase-input MuiOutlinedInput-input css-1x5jdmq')]	${HOSTNAME_NODEGRID}
	${FILE1}	Get WebElement	//*[text()='${HOSTNAME_NODEGRID}']
	Execute Javascript	arguments[0].click();	ARGUMENTS	${FILE1}
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Upload New Files	${FILE_TXT2}
	Wait Until Page Contains Element	xpath=//div[@title="test2.txt"]
	[Teardown]	Close All Browsers

Test case to Verify if Shared File is in Nodegrid
	[Tags]	GUI	ZPECLOUD	${BROWSER}	BUG_CLOUD_7289	BUG_CLOUD_7151	BUG_CLOUD_7373	NON-CRITICAL	DEVICE	#BUG_CLOUD_7288
	Run Keyword If	${EXTENDES_ACTIVATED} == ${FALSE}	Fail	Could not activate Extended storage due BUG_CLOUD_7151
	SUITE:Login on File Manager
	Wait Until Keyword Succeeds	1 min	2	Wait Until Element Is Visible	xpath=//a[contains(text(),'Shared')]
	Click Element	xpath=//a[contains(text(),'Shared')]
	GUI::Basic::Spinner Should Be Invisible
#	Wait Until Element Is Visible	xpath=//td[contains(text(),'6.36 KB')]
	Wait Until Page Contains Element	xpath=//a[contains(text(),'test1.txt')]
	[Teardown]	Close All Browsers

Test case to Verify if Uploaded File is in Nodegrid
	[Tags]	GUI	ZPECLOUD	${BROWSER}	BUG_CLOUD_7289	BUG_CLOUD_7151	BUG_CLOUD_7373	NON-CRITICAL	DEVICE	#BUG_CLOUD_7288
	Run Keyword If	${EXTENDES_ACTIVATED} == ${FALSE}	Fail	Could not activate Extended storage due BUG_CLOUD_7151
	SUITE:Login on File Manager
	Wait Until Keyword Succeeds	1 min	2	Wait Until Element Is Visible	xpath=//a[contains(text(),'nodegrid')]
	Click Element	xpath=//a[contains(text(),'nodegrid')]
	GUI::Basic::Spinner Should Be Invisible
#	Wait Until Element Is Visible	xpath=//td[contains(text(),'2.61 KB')]
	Wait Until Page Contains Element	xpath=//a[contains(text(),'test2.txt')]
	Click Element	xpath=//a[contains(text(),'test2.txt')]
	GUI::Basic::Spinner Should Be Invisible
	[Teardown]	Close All Browsers

Test case to Delete All Files
	[Tags]	GUI	ZPECLOUD	${BROWSER}	BUG_CLOUD_7289	BUG_CLOUD_7151	BUG_CLOUD_7373	NON-CRITICAL	DEVICE	#BUG_CLOUD_7288
	Run Keyword If	${EXTENDES_ACTIVATED} == ${FALSE}	Fail	Could not activate Extended storage due BUG_CLOUD_7151
	SUITE:Login into ZPE Cloud Company
	SUITE:Login on Extended Storage
	SUITE:Delete Folder or File	@title='test1.txt'	3
	${FILE1}=	Run Keyword and Return Status	Wait Until Page Contains Element	xpath=//div[@title="test1.txt"]
	Run Keyword If	${FILE1}==${FALSE}	SUITE:Delete Folder or File	@title='test'	3
	${FILE2}=	Run Keyword and Return Status	Wait Until Page Contains Element	xpath=//div[@title="test1.txt"]
	Should Be Equal	${FILE2}	${FALSE}
	Input Text	//input[contains(@class,'MuiInputBase-input MuiOutlinedInput-input css-1x5jdmq')]	${HOSTNAME_NODEGRID}
	${FILE3}	Get WebElement	//*[text()='${HOSTNAME_NODEGRID}']
	Execute Javascript	arguments[0].click();	ARGUMENTS	${FILE3}
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Delete Folder or File	@title='test2.txt'	3
	Page Should Not Contain Element	xpath=//div[@title="test2.txt"]
	[Teardown]	Close All Browsers

*** Keywords ***
SUITE:Setup
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	SUITE:Login into ZPE Cloud SuperAdmin

SUITE:Login into ZPE Cloud SuperAdmin
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${COMPANY_EMAIL}	${COMPANY_PWD}

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Login into ZPE Cloud SuperAdmin
	Click Element	//*[@id="sa-header-tab-1-7"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//div[@id='mui-component-select-company-id']
	GUI::Basic::Spinner Should Be Invisible
	Set Focus To Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	Click Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	GUI::Basic::Spinner Should Be Invisible
	${MENU_EXISTS}=	Run Keyword And Return Status	GUI::Element Exists	//*[@id='menu-company-id']
	Run Keyword If	${MENU_EXISTS}	Wait Until Element Is Not Visible	//*[@id='menu-company-id']	timeout=1m
	GUI::Basic::Spinner Should Be Invisible
	Unselect Checkbox	//*[@id="4"]
	GUI::Basic::Spinner Should Be Invisible
	Checkbox Should Not Be Selected	//*[@id="4"]
	Set Focus To Element	xpath=//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	//*[normalize-space()='Are you sure you want to deactivate this app?']	timeout=15s
	Click Element	//*[@id="confirm-btn"]	#BUG_CLOUD_7288
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Login into ZPE Cloud Company
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Ignore Error	Page Should Not Contain Element	//*[contains(text(), 'Apps')]	#BUG_CLOUD_7386
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:Activate App
	Click Element	//*[@id="ca-header-tab-2-1"]
	Wait Until Element Is Visible	xpath=//h6[contains(.,'Extended Storage')]	timeout=1m
	Click Element	xpath=//h6[contains(.,'Extended Storage')]
	GUI::Basic::Spinner Should Be Invisible
	${EXISTS}=	Run Keyword And Return Status	GUI::Element Exists	//*[@role='progressbar']//*[name()='svg']	#BUG_CLOUD_7373
	Run Keyword If	${EXISTS}	Wait Until Element Is Not Visible	//*[@role='progressbar']//*[name()='svg']	timeout=1m
	Wait Until Page Contains Element	//*[@id="ca-available-apps-wrapper"]/div[5]/form/div[2]/div/div[1]/div[1]/div[4]/button[2]/span[1]
	Click Element	//*[@id="ca-available-apps-wrapper"]/div[5]/form/div[2]/div/div[1]/div[1]/div[4]/button[2]/span[1]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@role="presentation"]/div[3]/div/div/div/button[2]/span
	Wait Until Page Contains	App activated successfully
	GUI::ZPECloud::Apps::Open Tab
	Wait Until Element Is Visible	xpath=//h6[contains(.,'Extended Storage')]	timeout=1m

SUITE:Upload New Files
	[Arguments]	${FILEUPLOAD}
	Wait Until Keyword Succeeds	5 min	2	Wait Until Page Contains Element	//div[@class="chonky-toolbar-button-group"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="chonky-toolbar-button-group"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="chonky-toolbar-dropdown-content"]/button[4]
	GUI::Basic::Spinner Should Be Invisible
	Choose File	xpath=//input[@type='file']	${FILEUPLOAD}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//div[3]/div/div[2]/div/button[2]
	GUI::Basic::Spinner Should Be Invisible

SUITE:Login on Extended Storage
	GUI::ZPECloud::Apps::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	xpath=//h6[contains(.,'Extended Storage')]	timeout=1m
	Click Element	xpath=//h6[contains(.,'Extended Storage')]
	GUI::Basic::Spinner Should Be Invisible
	${EXISTS}=	Run Keyword And Return Status	GUI::Element Exists	//*[@role='progressbar']//*[name()='svg']	#BUG_CLOUD_7373
	Run Keyword If	${EXISTS}	Wait Until Element Is Not Visible	//*[@role='progressbar']//*[name()='svg']	timeout=1m

SUITE:Delete Folder or File
	[Arguments]	${NAMEFILE}	${NUMBERBUTTON}
	Wait Until Page Contains Element	xpath=//div[${NAMEFILE}]
	${FILE}	Get WebElement	//*[${NAMEFILE}]
	Execute Javascript	arguments[0].click();	ARGUMENTS	${FILE}
	Wait Until Page Contains Element	//div[@class="chonky-toolbar-button-group"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//div[@class="chonky-toolbar-button-group"]
	Click Element	//div[@class="chonky-toolbar-dropdown-content"]/button[${NUMBERBUTTON}]
	Click Element	xpath=//div[3]/div/div/div/button[2]
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	${DELETESUCCESS}
	GUI::Basic::Spinner Should Be Invisible

SUITE:Login on File Manager
	GUI::Basic::Open And Login Nodegrid	${USERNAMENG}	${PASSWORDNG}
	Click Element	//*[@id="|nodegrid"]/div[1]/div[4]
	Wait Until Keyword Succeeds	1m	5s	Switch Window	title=File Manager
	Select Frame	//iframe
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	xpath=//a[contains(text(),'remote_file_system')]
	Click Element	xpath=//a[contains(text(),'remote_file_system')]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	xpath=//a[contains(text(),'extended_storage')]
	Click Element	xpath=//a[contains(text(),'extended_storage')]
	GUI::Basic::Spinner Should Be Invisible