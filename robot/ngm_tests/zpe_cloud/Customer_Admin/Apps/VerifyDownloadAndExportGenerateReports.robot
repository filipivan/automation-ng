*** Settings ***
Documentation	Tests for Activation of reports app and Generation of Reports
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	CUSTOMER_ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	BUG_CLOUD_7289	BUG_CLOUD_7373	BUG_CLOUD_7386	NON-DEVICE	#BUG_CLOUD_7288

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${SUPER_EMAIL_ADDRESS}
${PWD}	${SUPER_PASSWORD}
${COMPANY_EMAIL}	${EMAIL_ADDRESS}
${COMPANY_PWD}	${PASSWORD}

*** Test Cases ***
Test case Add apps to the Company in super-admin
	Click Element	//*[@id="sa-header-tab-1-7"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//div[@id='mui-component-select-company-id']
	GUI::Basic::Spinner Should Be Invisible
	Set Focus To Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	Click Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	GUI::Basic::Spinner Should Be Invisible
	${MENU_EXISTS}=	Run Keyword And Return Status	GUI::Element Exists	//*[@id='menu-company-id']
	Run Keyword If	${MENU_EXISTS}	Wait Until Element Is Not Visible	//*[@id='menu-company-id']	timeout=1m
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="2"]
	GUI::Basic::Spinner Should Be Invisible
	Checkbox Should Be Selected	//*[@id="2"]
	Set Focus To Element	xpath=//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	[Teardown]	Close All Browsers

Test case to Activate Reports app and Open the reports page in the Company.
	SUITE:Login into ZPE Cloud Company
	Click Element	//*[contains(text(), 'Apps')]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	10x	1s	Wait Until Page Contains Element	xpath=//h6[contains(.,'Reports')]
	Click Element	xpath=//h6[contains(.,'Reports')]
	GUI::Basic::Spinner Should Be Invisible
	${EXISTS}=	Run Keyword And Return Status	GUI::Element Exists	//*[@role='progressbar']//*[name()='svg']	#BUG_CLOUD_7373
	Run Keyword If	${EXISTS}	Wait Until Element Is Not Visible	//*[@role='progressbar']//*[name()='svg']	timeout=1m
	Click Element	//*[@id="ca-available-apps-wrapper"]/div[1]/form/div[2]/div/div[1]/div[1]/div[4]/button[2]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	xpath=//h6[contains(.,'Reports')]
	Click Element	xpath=//h6[contains(.,'Reports')]
	GUI::Basic::Spinner Should Be Invisible
	${EXISTS}=	Run Keyword And Return Status	GUI::Element Exists	//*[@role='progressbar']//*[name()='svg']	#BUG_CLOUD_7373
	Run Keyword If	${EXISTS}	Wait Until Element Is Not Visible	//*[@role='progressbar']//*[name()='svg']	timeout=1m

Test case to Verify report of Enrolled devices in the Company.
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="simple-tabpanel-0"]/div/div/div/div[1]/div/div[2]/div
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//button[normalize-space()='Run Report']
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	List of enrolled devices
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Enabled	//button[normalize-space()='Back']
	Click Button	//button[normalize-space()='Back']
	GUI::Basic::Spinner Should Be Invisible

Test case to Verify report of Available Devices in the Company.
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="simple-tabpanel-0"]/div/div/div/div[3]/div/div[2]/div
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="simple-tabpanel-0"]/div/div[1]/div/button[1]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	List of available devices
	Click Button	//*[@id="simple-tabpanel-0"]/div/div[1]/div/button[3]
	GUI::Basic::Spinner Should Be Invisible

Test case to GenerateReports for DeviceAvailabilityDay in the Company.
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="simple-tabpanel-0"]/div/div/div/div[2]/div/div[2]/div
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="simple-tabpanel-0"]/div/div[1]/div/button[1]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Device Availability by day
	Click Button	//*[@id="simple-tabpanel-0"]/div/div[1]/div/button[2]
	GUI::Basic::Spinner Should Be Invisible

Test case to GenerateReports for DeviceAvailabilityHour in the Company.
	Click Element	//*[@id="simple-tabpanel-0"]/div/div/div/div[6]/div/div[2]/div
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="simple-tabpanel-0"]/div/div[1]/div/button[1]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Device Availability by hour
	Click Button	//*[@id="simple-tabpanel-0"]/div/div[1]/div/button[2]
	GUI::Basic::Spinner Should Be Invisible

Test case to GenerateReports for DeviceFailoverbyday in the Company.
	Click Element	//*[@id="simple-tabpanel-0"]/div/div/div/div[4]/div/div[2]/div
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="simple-tabpanel-0"]/div/div[1]/div/button[1]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Device Failover by day
	Click Button	//*[@id="simple-tabpanel-0"]/div/div[1]/div/button[2]
	GUI::Basic::Spinner Should Be Invisible

Test case to GenerateReports for DeviceFailOverHour in the Company.
	Click Element	//*[@id="simple-tabpanel-0"]/div/div/div/div[8]/div/div[2]/div
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="simple-tabpanel-0"]/div/div[1]/div/button[1]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Device Failover by hour
	Click Button	//*[@id="simple-tabpanel-0"]/div/div[1]/div/button[2]
	GUI::Basic::Spinner Should Be Invisible

Test case to GenerateReports for DeviceOperations in the Company.
	Click Element	//*[@id="simple-tabpanel-0"]/div/div/div/div[9]/div/div[2]/div
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="simple-tabpanel-0"]/div/div[1]/div/button[1]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	List of device operations
	Click Button	//*[@id="simple-tabpanel-0"]/div/div[1]/div/button[3]
	GUI::Basic::Spinner Should Be Invisible

Test case to GenerateReports for Site in the Company.
	Click Element	//*[@id="simple-tabpanel-0"]/div/div/div/div[5]/div/div[2]/div
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="simple-tabpanel-0"]/div/div[1]/div/button[1]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	List of sites
	Click Button	//*[@id="simple-tabpanel-0"]/div/div[1]/div/button[3]
	GUI::Basic::Spinner Should Be Invisible

Test case to GenerateReports for Group in the Company.
	Click Element	//*[@id="simple-tabpanel-0"]/div/div/div/div[7]/div/div[2]/div
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="simple-tabpanel-0"]/div/div[1]/div/button[1]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	List of groups
	Click Button	//*[@id="simple-tabpanel-0"]/div/div[1]/div/button[3]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="customer-admin-layout"]/main/div/div/div/div/div[1]/div[2]/div[1]/button[2]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Login into ZPE Cloud SuperAdmin
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${COMPANY_EMAIL}	${COMPANY_PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud SuperAdmin

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Login into ZPE Cloud SuperAdmin
	Click Element	//*[@id="sa-header-tab-1-7"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//div[@id='mui-component-select-company-id']
	GUI::Basic::Spinner Should Be Invisible
	Set Focus To Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	Click Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	GUI::Basic::Spinner Should Be Invisible
	${MENU_EXISTS}=	Run Keyword And Return Status	GUI::Element Exists	//*[@id='menu-company-id']
	Run Keyword If	${MENU_EXISTS}	Wait Until Element Is Not Visible	//*[@id='menu-company-id']	timeout=1m
	Unselect Checkbox	//*[@id="2"]
	GUI::Basic::Spinner Should Be Invisible
	Checkbox Should Not Be Selected	//*[@id="2"]
	Set Focus To Element	xpath=//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Ignore Error	Wait Until Element Is Visible	//*[normalize-space()='Are you sure you want to deactivate this app?']	timeout=15s	#BUG_CLOUD_7386
	Run Keyword And Ignore Error	Click Element	//*[@id="confirm-btn"]	#BUG_CLOUD_7386
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Login into ZPE Cloud Company
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Ignore Error	Page Should Not Contain Element	//*[contains(text(), 'Apps')]	#BUG_CLOUD_7386
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers