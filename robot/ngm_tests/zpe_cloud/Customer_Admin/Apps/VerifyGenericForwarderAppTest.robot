*** Settings ***
Documentation	Tests for Activation of Generic Forwarder App
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	CUSTOMER_ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	BUG_CLOUD_7289	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${SUPER_EMAIL_ADDRESS}
${PWD}	${SUPER_PASSWORD}
${COMPANY_EMAIL}	${EMAIL_ADDRESS}
${COMPANY_PWD}	${PASSWORD}
${App_Name}	Google_Test
${Description}	Sample App
${Link}	https://www.google.com
${File_Path}	${CURDIR}/GenericForwarder_Logo/ZPE_logo.png

*** Test Cases ***
Test case Add apps to the Company in super-admin
	Click Element	//*[@id="sa-header-tab-1-7"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//div[@id='mui-component-select-company-id']
	GUI::Basic::Spinner Should Be Invisible
	Set Focus To Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	Click Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	GUI::Basic::Spinner Should Be Invisible
	${MENU_EXISTS}=	Run Keyword And Return Status	GUI::Element Exists	//*[@id='menu-company-id']
	Run Keyword If	${MENU_EXISTS}	Wait Until Element Is Not Visible	//*[@id='menu-company-id']	timeout=1m
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="5"]
	GUI::Basic::Spinner Should Be Invisible
	Checkbox Should Be Selected	//*[@id="5"]
	Set Focus To Element	xpath=//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	[Teardown]	Close All Browsers

Test case to VerifyGeneric ForwarderApp in the Company.
	[Tags]	GUI	ZPECLOUD	NON-CRITICAL	${BROWSER}	BUG_CLOUD_7289	NON-DEVICE
	SUITE:Login into ZPE Cloud Company
	Click Element	//*[contains(text(), 'Apps')]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//h6[contains(.,'Generic Forwarder')]
	Click Element	xpath=//h6[contains(.,'Generic Forwarder')]
	GUI::Basic::Spinner Should Be Invisible
	${EXISTS}=	Run Keyword And Return Status	GUI::Element Exists	//*[@role='progressbar']//*[name()='svg']	#BUG_CLOUD_7373
	Run Keyword If	${EXISTS}	Wait Until Element Is Not Visible	//*[@role='progressbar']//*[name()='svg']	timeout=1m
	Input Text	//*[@id="app-name-input"]	${App_Name}
	GUI::Basic::Spinner Should Be Invisible
	Choose File	id=file	${File_Path}
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="forwarder-description-input"]	${Description}
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="forwarder-url-input"]	${Link}
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//*[@id="ca-available-apps-wrapper"]/div[2]/form/div[2]/div/div[1]/div[1]/div[3]/div[4]/div[1]/div/div[1]/label/span[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="ca-available-apps-wrapper"]/div[2]/form/div[2]/div/div[1]/div[1]/div[3]/div[4]/div[2]/button
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	//*[@id="ca-available-apps-wrapper"]/div[2]/form/div[2]/div/div[1]/div[1]/div[4]/button[2]	timeout=30s
	Scroll Element Into View	//*[@id="ca-available-apps-wrapper"]/div[2]/form/div[2]/div/div[1]/div[1]/div[4]/button[2]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="ca-available-apps-wrapper"]/div[2]/form/div[2]/div/div[1]/div[1]/div[4]/button[2]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Launched Google_Test in new tab...	5s
	GUI::Basic::Spinner Should Be Invisible
	Switch Window	Google
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Google
	GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
SUITE:Login into ZPE Cloud SuperAdmin
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${COMPANY_EMAIL}	${COMPANY_PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud SuperAdmin

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Login into ZPE Cloud SuperAdmin
	Click Element	//*[@id="sa-header-tab-1-7"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//div[@id='mui-component-select-company-id']
	GUI::Basic::Spinner Should Be Invisible
	Set Focus To Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	Click Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	GUI::Basic::Spinner Should Be Invisible
	${MENU_EXISTS}=	Run Keyword And Return Status	GUI::Element Exists	//*[@id='menu-company-id']
	Run Keyword If	${MENU_EXISTS}	Wait Until Element Is Not Visible	//*[@id='menu-company-id']	timeout=1m
	GUI::Basic::Spinner Should Be Invisible
	Unselect Checkbox	//*[@id="5"]
	GUI::Basic::Spinner Should Be Invisible
	Checkbox Should Not Be Selected	//*[@id="5"]
	Set Focus To Element	xpath=//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Ignore Error	Wait Until Element Is Visible	//*[normalize-space()='Are you sure you want to deactivate this app?']	timeout=15s	#BUG_CLOUD_7386
	Run Keyword And Ignore Error	Click Element	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Login into ZPE Cloud Company
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Ignore Error	Page Should Not Contain Element	//*[contains(text(), 'Apps')]
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers