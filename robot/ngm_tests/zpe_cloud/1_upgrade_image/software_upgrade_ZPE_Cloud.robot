*** Settings ***
Resource	../../init.robot
Documentation	Upgrade Device to latest image based on the device's version using ZPE Cloud feature
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	SWUPGRADE	SWDOWNGRADE	VERSIONS	EXCLUDEIN_ZPECLOUD	DEVICE
Default Tags	VERSIONS

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${USERNAMENG}
${PASSWORDNG}	${PASSWORDNG}
${ROW}	0
${ISMASTER}	Master
${NG_PATTERN}	nodegrid-genericx86-64-\\
${FIND_FOR_VERSION}	_Branch-NG_${NGVERSION}
${FIND_FOR_OFFICIAL}	official - v${NGVERSION}
${SUCCESSFULMESSAGE}	Software upgrade on the selected devices was requested

*** Test Cases ***
Software Downgrade through ZPE Cloud
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Profiles::Software::Open Tab
	Sleep	5
	Wait Until Page Contains Element	ca-profiles-software-table-1
	${OFFICIAL_VERSION}=	Execute Javascript	return Array.from(document.getElementById('ca-profiles-software-table-1').getElementsByTagName('TBODY')[0].rows).find(r => r.cells[1].innerText.substr(0, '${FIND_FOR_OFFICIAL}'.length) == 'official - v4.2').cells[0].innerText
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Wait Until Page Contains Element	ca-devices-enrolled-table-1
	${ROWNUMBER}=	Execute Javascript	return Array.from(document.getElementById('ca-devices-enrolled-table-1').getElementsByTagName('TBODY')[0].getElementsByTagName('TR')).indexOf(Array.from(document.getElementById('ca-devices-enrolled-table-1').getElementsByTagName('TBODY')[0].getElementsByTagName('TR')).filter(e => e.getElementsByTagName('TD')[2].textContent == '${SERIALNUMBER}')[0])
	Convert To Number	${ROWNUMBER}
	${ROWNUMBER}=	Evaluate	${ROWNUMBER} + 1
	Set Suite Variable	${ROWNUMBER}	${ROWNUMBER}
	Click Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr[${ROWNUMBER}]/td[1]/span/span[1]
	GUI::Basic::Wait Until Element Is Accessible	//*[@id="function-button-2"]/span[1]
	Click Element	//*[@id="function-button-2"]/span[1]
	Wait Until Page Contains	Upgrade device
	Wait Until Page Contains Element	//*[@id="mui-component-select-version"]
	Click Element	//*[@id="mui-component-select-version"]
	Click Element	xpath=//li[contains(.,'${OFFICIAL_VERSION}')]
	GUI::ZPECloud::Basic::Click Button	save
	Wait Until Page Contains	${SUCCESSFULMESSAGE}
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Page Contains	${NGVERSION}
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[6]/div	Software Upgrade
	Sleep	10 minutes
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Page Contains	${NGVERSION}
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[6]/div	Software Upgrade
	Element Should Not Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[8]/div	Failed
	Wait Until Keyword Succeeds	30 minutes	5 min	SUITE:Status Is Successful

Software Upgrade through ZPE Cloud
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Wait Until Page Contains Element	ca-devices-enrolled-table-1
	${ROWNUMBER}=	Execute Javascript	return Array.from(document.getElementById('ca-devices-enrolled-table-1').getElementsByTagName('TBODY')[0].getElementsByTagName('TR')).indexOf(Array.from(document.getElementById('ca-devices-enrolled-table-1').getElementsByTagName('TBODY')[0].getElementsByTagName('TR')).filter(e => e.getElementsByTagName('TD')[2].textContent == '${SERIALNUMBER}')[0])
	Convert To Number	${ROWNUMBER}
	${ROWNUMBER}=	Evaluate	${ROWNUMBER} + 1
	Set Suite Variable	${ROWNUMBER}	${ROWNUMBER}
	Click Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr[${ROWNUMBER}]/td[1]/span/span[1]
	GUI::Basic::Wait Until Element Is Accessible	//*[@id="function-button-2"]/span[1]
	Click Element	//*[@id="function-button-2"]/span[1]
	Wait Until Page Contains	Upgrade device
	Wait Until Page Contains Element	//*[@id="mui-component-select-version"]
	Click Element	//*[@id="mui-component-select-version"]
	${ISOOPTIONS}	Execute Javascript	return document.getElementById('menu-version').innerText
	${lines}=	Get Lines Containing String	${ISOOPTIONS}	${FIND_FOR_VERSION}	case_insensitive=true
	@{ISOOPTIONS}	Split To Lines	${lines}
	Click Element	xpath=//li[contains(.,'${ISOOPTIONS[0]}')]
	GUI::ZPECloud::Basic::Click Button	save
	Wait Until Page Contains	${SUCCESSFULMESSAGE}
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Page Contains	${NGVERSION}
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[6]/div	Software Upgrade
	Sleep	10 minutes
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Page Contains	${NGVERSION}
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[6]/div	Software Upgrade
	Element Should Not Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[8]/div	Failed
	Wait Until Keyword Succeeds	30 minutes	5 min	SUITE:Status Is Successful

*** Keywords ***
SUITE:Setup
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud

SUITE:Teardown
	Close All Browsers

SUITE:Status Is Successful
	Reload Page
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Page Contains	${NGVERSION}
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[6]/div	Software Upgrade
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[8]/div	Successful

