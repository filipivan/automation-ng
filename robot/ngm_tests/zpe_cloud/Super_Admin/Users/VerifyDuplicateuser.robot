*** Settings ***
Documentation	Tests for Duplicate User from Superadmin
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	SUPER_ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${SUPER_EMAIL_ADDRESS}
${PWD}	${SUPER_PASSWORD}
${User_Email}	${EMAIL_ADDRESS}
${User_Name}	ORO
${User_last_name}	TEST
${User_PhoneNumber}	123456789

*** Test Cases ***
Test case Add Duplicate User from Superadmin
	Click Button	//*[@id="sa-header-tab-1-3"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@name="email"]
	Input Text	//*[@name="email"]	${User_Email}
	Input Text	//*[@name="first_name"]	${User_Name}
	Input Text	//*[@name="last_name"]	${User_last_name}
	Input Text	//*[@name="Phone Number"]	${User_PhoneNumber}
	Click Element	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	user with this email already exists.

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Teardown
	Close All Browsers
