*** Settings ***
Documentation	Tests for Send Email to Users from Superadmin
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	SUPER_ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${SUPER_EMAIL_ADDRESS}
${PWD}	${SUPER_PASSWORD}
${DATE}
${Email_Subject}	Oro Test
${GMAIL_SEARCH_URL}	https://accounts.google.com/v3/signin/identifier?dsh=S252604833%3A1665998746598243&continue=https%3A%2F%2Fmail.google.com%2Fmail%2Fu%2F0%2F%3Ftab%3Drm%26ogbl&emr=1&followup=https%3A%2F%2Fmail.google.com%2Fmail%2Fu%2F0%2F%3Ftab%3Drm%26ogbl&osid=1&passive=1209600&service=mail&flowName=GlifWebSignIn&flowEntry=ServiceLogin&ifkv=AQDHYWpKn62wwxtc-LEV6FczsuTyO1rL7_DjJoidzaGohi0ky_q3nIyivHMxqA5dGy5P75cd-0MepQ
${email_username}	${EMAIL_ADDRESS}
${email_password}	${PASSWORD}
*** Test Cases ***
Test case Send Email to Users from Superadmin
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="sa-header-tab-1-3"]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="search-input"]	${email_username}
	Sleep	2s
	Click Element	//*[@id="sa-users-general-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="function-button-5"]
	GUI::Basic::Spinner Should Be Invisible
	${DATE}=	Get Current Date	result_format=%Y-%m-%d %H:%M:%S
	${DATE}=	Add Time To Date	${DATE}	1 minute
	${DATE}=	Convert Date	${DATE}	result_format=%Y-%m-%d %H:%M:%S
	Press Keys	//*[@id="sa-users-general-edit-dialog"]/div[2]/div/div/div/div[1]/div[1]/div/div[1]/div/input	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="sa-users-general-edit-dialog"]/div[2]/div/div/div/div[1]/div[1]/div/div[1]/div/input
	Input Text	//*[@id="sa-users-general-edit-dialog"]/div[2]/div/div/div/div[1]/div[1]/div/div[1]/div/input	${DATE}
	Input Text	//*[@id="subject-input"]	${Email_Subject}
	Sleep	2s
	Click Button	//*[@id="submit-btn"]
	Wait Until Page Contains	Email sent successfully
	Sleep	5s

#Test Case Verify Email
#	[Tags]	GUI	ZPECLOUD	NON-CRITICAL	${BROWSER}	NON-DEVICE
#	Open Browser	${GMAIL_SEARCH_URL}	${BROWSER}
#	Wait Until Element Is Visible	id=identifierId
#	Input Text	id=identifierId	${email_username}
#	Wait Until Element Is Visible	xpath=//span[contains(.,'Next')]
#	Press Keys	identifierNext	Enter
##	Click Element	xpath=//div[@id='identifierNext']/div/button/div[2]
##	sleep	2
#	Wait Until Element Is Visible	css=#password .whsOnd	30s
#	Input Text	css=#password .whsOnd	${email_password}
#	Wait Until Element Is Visible	xpath=//span[contains(.,'Next')]
##	Click Element	xpath=//div[@id='passwordNext']/div/button/div[2]
#	Press Keys	passwordNext	Enter
#	sleep	10s
#	Wait Until Page Contains Element	//*[@id=":ms"]/div
#	Wait Until Page Contains Element	${Email_Subject}

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Teardown
	Close All Browsers
