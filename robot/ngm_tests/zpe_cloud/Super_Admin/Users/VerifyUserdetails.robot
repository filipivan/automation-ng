*** Settings ***
Documentation	Tests for Verify User details from Superadmin
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	SUPER_ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-DEVICE	NON-CRITICAL

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${SUPER_EMAIL_ADDRESS}
${PWD}	${SUPER_PASSWORD}
${New_User}	testuserORO@gmail.com
${First_name}	testuser
${Last_name}	ORO
${Invalid_User}	Invalid@gmail.com
${PhoneNumber}	1234567890

*** Test Cases ***
Test case Lock User from Superadmin
	Click Element	//*[@id="sa-header-tab-1-3"]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="search-input"]	${New_User}
	Sleep	2s
	Click Element	//*[@id="sa-users-general-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="function-button-3"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	User locked successfully

Test case Unlock User from Superadmin
	Sleep	2s
	Click Element	//*[@id="sa-users-general-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="function-button-4"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	User unlocked successfully

Test Case Verify Invalid User in superadmin
	Sleep	2s
	Press Keys	//*[@id="search-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="search-input"]
	Input text	//*[@id="search-input"]	${Invalid_User}
	Sleep	2s
	Wait Until Page Contains	No result found

Test Case Verify Download User List in superadmin as excel
	Press Keys	//*[@id="search-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="search-input"]
	Wait Until Page Contains Element	//*[@id="sa-users-general-table-1"]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
	Click Button	//*[@id="sa-users-general-wrapper"]/div[2]/div[2]/button
	Click Element	//*[@id="menu-list-grow"]/ul/li[1]
	Sleep	10s

Test Case Verify Download User List in superadmin as csv
	Click Button	//*[@id="sa-users-general-wrapper"]/div[2]/div[2]/button
	Click Element	//*[@id="menu-list-grow"]/ul/li[2]
	Sleep	10s

Test Case Delete Excel Logs
	Remove File	../../../Downloads/User Details.xls

Test Case Delete csv Logs
	Remove File	../../../Downloads/User Details.csv

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Teardown
	Close All Browsers
