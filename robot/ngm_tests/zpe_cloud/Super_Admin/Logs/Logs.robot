*** Settings ***
Documentation	Tests for Verify Logs Tab in Super Admin
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	SUPER_ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-DEVICE	NON-CRITICAL

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${SUPER_EMAIL_ADDRESS}
${PWD}	${SUPER_PASSWORD}
${Message}	Invalid

*** Test Cases ***
Test case Verify All Tabs in Logs
	Click Element	//*[@id="sa-header-tab-1-5"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="sa-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="Confirm"]
	Sleep	2s
	Click Element	//*[@id="sa-header-tab-2-0"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="simple-tab-1"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="sa-logs-fc-startdate"]
	Click Element	//*[@id="simple-tab-0"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="sa-logs-fc-submit"]

Test Case Verify Search Button in Notification Tab
	Page Should Contain Element	//*[@id="search-input"]
	Input Text	//*[@id="search-input"]	${Message}
	Sleep	2s
	Page Should Contain	No result found

Test Case Verify End and Start Date in Notification Tab
	Press Keys	//*[@id="search-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="search-input"]
	Sleep	2s
	SUITE:Selection of Priority and Date

Test Case Verify Open All Notification
	Click Element	//*[@id="sa-logs-alert-table-wrapper"]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="sa-logs-fc-submit"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Wait Until Page Contains	Alerts Acknowledged Successfully

Test Case Verify Notification Priority in Open-Notification Tab
	Click Element	//*[@id="sa-logs-fc-priprity-select"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="sa-logs-fc-priprity-opt-N"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="sa-logs-alert-table-wrapper"]/div[1]/div/table/tbody/tr[1]/td[1]/span/input

Test Case Verify Reopen All Notification
	Click ELement	//*[@id="simple-tab-1"]
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Selection of Priority and Date
	Sleep	2s
	Click Element	//*[@id="sa-logs-alert-table-wrapper"]/div[1]/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="sa-logs-fc-submit"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Wait Until Page Contains	Alerts ReOpen Successfully

Test Case Verify Confirm Button in Security:Notification:Logs
	Click Element	//*[@id="sa-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-logs-site-startdate"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-logs-customer-wrapper"]/div[2]/div/div[1]/div/div[1]/div[1]/div[2]/div[2]/div/div/div[2]/div[2]/div[1]/div[1]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-logs-customer-wrapper"]/div[2]/div/div[1]/div/div[1]/div[1]/div[2]/div[2]/div/div/div[3]/div[2]/div/ul/li[1]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-logs-site-enddate"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-logs-customer-wrapper"]/div[2]/div/div[1]/div/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[2]/div[5]/div[7]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-logs-customer-wrapper"]/div[2]/div/div[1]/div/div[1]/div[2]/div[2]/div[2]/div/div/div[3]/div[2]/div/ul/li[1440]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="Confirm"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="ca-logs-customer-table-1"]/div[1]/div/table/tbody/tr[1]/td[1]

Test Case To Download Security:Notification:Logs in Excel
	Click Element	//*[@id="Download"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="mui-component-select-selectLicense"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="menu-selectLicense"]/div[3]/ul/li[1]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s

Test Case To Download Security:Notification:Logs in csv
	Click Element	//*[@id="Download"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="mui-component-select-selectLicense"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="menu-selectLicense"]/div[3]/ul/li[2]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s

Test Case Delete Excel Logs
	Remove File	../../../Downloads/1. Security logs.xlsx

Test Case Delete csv Logs
	Remove File	../../../Downloads/1. Security logs.csv

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Selection of Priority and Date
	Click Element	//*[@id="sa-logs-fc-priprity-select"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="sa-logs-fc-priprity-opt-A"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Click Element	//*[@id="sa-logs-fc-startdate"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="sa-logs-filter-components-wrapper"]/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[2]/div[1]/div[1]
	GUI::Basic::Spinner Should Be Invisible
	Click ELement	//*[@id="sa-logs-filter-components-wrapper"]/div[1]/div[2]/div[2]/div[2]/div/div/div[3]/div[2]/div/ul/li[1]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	1s
	Click Element	//*[@id="sa-logs-fc-enddate"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="sa-logs-filter-components-wrapper"]/div[1]/div[3]/div[2]/div[2]/div/div/div[2]/div[2]/div[5]/div[7]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="sa-logs-filter-components-wrapper"]/div[1]/div[3]/div[2]/div[2]/div/div/div[3]/div[2]/div/ul/li[96]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s

SUITE:Teardown
	Close All Browsers
