*** Settings ***
Documentation	Tests for Verify Overview Metrics details in Super Admin
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	SUPER ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${SUPER_EMAIL_ADDRESS}
${PWD}	${SUPER_PASSWORD}

*** Test Cases ***
Test case Verify Overview Metrics by Custom Time
	Click Button	//*[@id="sa-header-tab-1-8"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="opt-custom"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="end-date"]
	Sleep	2s
	SUITE:Selection of Date
	Page Should Contain	Company Acquisition

Test case Verify Overview Metrics by 1 Month
	Click Element	//*[@id="opt-1-month"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Page Should Contain	Company Acquisition

Test case Verify Overview Metrics by 1 Week
	Click Element	//*[@id="opt-1-week"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Page Should Contain	Company Acquisition

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Selection of Date
	Click Element	//*[@id="start-date"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ui-datepicker-selector"]/div[2]/div[2]/div/div/div[2]/div[2]/div[1]/div[1]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="end-date"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ui-datepicker-selector"]/div[2]/div[2]/div/div/div[2]/div[2]/div[5]/div[7]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	1s

SUITE:Teardown
	Close All Browsers
