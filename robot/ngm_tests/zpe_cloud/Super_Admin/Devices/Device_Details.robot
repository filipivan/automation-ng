*** Settings ***
Documentation	Tests for Verify Device details in Super Admin
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	SUPER ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-CRITICAL	DEVICE	PART_4

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${SUPER_EMAIL_ADDRESS}
${PWD}	${SUPER_PASSWORD}
${S_Number}	${SERIALNUMBER}
${Invalid_Device_Number}	123456

*** Test Cases ***
Test case Verify View Device Details
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="sa-header-tab-1-1"]
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Serial Number
	Page Should Contain	Company Name
	Page Should Contain	Model
	Page Should Contain	Part Number
	Page Should Contain	Status
	Page Should Contain	Registration Date
	Page Should Contain	Version
	Sleep	2s

Test case Verify Device
	Input Text	//*[@id="search-input"]	${S_Number}
	Wait Until Page Contains Element	//*[@id="sa-devices-general-table-1"]/div[1]/div/table/tbody/tr/td[2]
	Click Element	//*[@id="sa-devices-general-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Table Should Contain	//*[@id="sa-devices-general-table-2"]	${S_Number}

Test case Verify Search Device by Company Name
	Press Keys	//*[@id="search-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="search-input"]
	Input Text	//*[@id="search-input"]	${COMPANY}
	Wait Until Page Contains Element	//*[@id="sa-devices-general-table-1"]/div[1]/div/table/tbody/tr/td[2]
	Page Should Contain ELement	//*[@id="sa-devices-general-table-1"]/div[1]/div/table/tbody/tr/td[2]

Test case Verify Search Invalid Device
	Press Keys	//*[@id="search-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="search-input"]
	Input Text	//*[@id="search-input"]	${Invalid_Device_Number}
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Page Should Contain	No result found

*** Keywords ***
SUITE:Setup
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud

SUITE:Teardown
	Close All Browsers
