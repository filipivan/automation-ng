*** Settings ***
Documentation	Tests for Activation of reports app and change Subscription
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	SUPER_ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${SUPER_EMAIL_ADDRESS}
${PWD}	${SUPER_PASSWORD}
${COMPANY_EMAIL}	${EMAIL_ADDRESS}
${COMPANY_PWD}	${PASSWORD}

*** Test Cases ***
Test case Add apps to the Company in super-admin
	SUITE: Selection of Company
	Sleep	5s
	Select Checkbox	//*[@id="7"]
	Select Checkbox	//*[@id="6"]
	Select Checkbox	//*[@id="5"]
	Select Checkbox	//*[@id="4"]
	Select Checkbox	//*[@id="3"]
	Select Checkbox	//*[@id="2"]
	Select Checkbox	//*[@id="1"]
	Set Focus To Element	xpath=//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s

Test case Activating the added app in the Company.
	SUITE:Login into ZPE Cloud Company
	Click Element	//*[contains(text(), 'Apps')]
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	Click Element	//*[@id="ca-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	10s
	Page Should Contain Element	xpath=//h6[contains(.,'Reports')]
	Page Should Contain Element	xpath=//h6[contains(.,'Generic Forwarder')]
	Page Should Contain Element	xpath=//h6[contains(.,'Palo Alto Prisma Access')]
	Page Should Contain Element	xpath=//h6[contains(.,'Extended Storage')]
	Page Should Contain Element	xpath=//h6[contains(.,'SD-WAN')]
	Page Should Contain Element	xpath=//h6[contains(.,'ZPE Mobile')]
	Page Should Contain Element	xpath=//h6[contains(.,'Nodegrid Data Lake')]

*** Keywords ***
SUITE:Login into ZPE Cloud SuperAdmin
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${COMPANY_EMAIL}	${COMPANY_PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud SuperAdmin

SUITE: Selection of Company
	Click Element	//*[@id="sa-header-tab-1-7"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	Click Element	xpath=//div[@id='mui-component-select-company-id']
	GUI::Basic::Spinner Should Be Invisible
	Set Focus To Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	Click Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	GUI::Basic::Spinner Should Be Invisible

SUITE:Teardown
	SUITE:Login into ZPE Cloud SuperAdmin
	SUITE: Selection of Company
	Sleep	5s
	Unselect Checkbox	//*[@id="7"]
	Unselect Checkbox	//*[@id="6"]
	Unselect Checkbox	//*[@id="5"]
	Unselect Checkbox	//*[@id="4"]
	Unselect Checkbox	//*[@id="3"]
	Unselect Checkbox	//*[@id="2"]
	Unselect Checkbox	//*[@id="1"]
	Set Focus To Element	xpath=//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	Close All Browsers