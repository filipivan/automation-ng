*** Settings ***
Documentation	Tests for Activation of reports app and change Subscription
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	SUPER_ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-DEVICE	BUG_CLOUD_8372

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${SUPER_EMAIL_ADDRESS}
${PWD}	${SUPER_PASSWORD}
${COMPANY_EMAIL}	${EMAIL_ADDRESS}
${COMPANY_PWD}	${PASSWORD}

*** Test Cases ***
Test case Add apps to the Company in super-admin
	Click Element	//*[@id="sa-header-tab-1-7"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	Click Element	xpath=//div[@id='mui-component-select-company-id']
	GUI::Basic::Spinner Should Be Invisible
	Set Focus To Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	Click Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	Select Checkbox	//*[@id="2"]
	Set Focus To Element	xpath=//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s

Test case Add Subscription in the Super admin
	Click Element	//*[@id="sa-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Click Button	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Input text	//*[@id="sa-apps-add-licence-dialog"]/div[2]/div/div/div[1]/div/div/div/div[1]/input	${COMPANY}
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Click Element	//li[contains(.,"${COMPANY}")]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="select-app"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Wait Until Page Contains Element	//*[@id="app-2"]
	Click Element	//*[@id="app-2"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Click Element	//*[@id="select-licence"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="licence-3"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Clear Element Text	//*[@name="cc"]
	Press Keys	//*[@name="cc"]	CTRL+a+BACKSPACE
	Sleep	2s
	Click Button	//*[@id="function-button-1"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Subscription Added Successfully

*** Keywords ***
SUITE:Login into ZPE Cloud SuperAdmin
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${COMPANY_EMAIL}	${COMPANY_PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud SuperAdmin

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="sa-header-tab-2-0"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	Click Element	xpath=//div[@id='mui-component-select-company-id']
	Set Focus To Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	Click Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Unselect Checkbox	//*[@id="2"]
	Set Focus To Element	xpath=//*[@id="sa-apps-save-btn"]
	Sleep	5s
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	sleep	3s
	Close All Browsers