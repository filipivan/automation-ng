*** Settings ***
Documentation	Tests for Verification of Add Subscription and Apply Filter in Company Tab
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	SUPER_ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${SUPER_EMAIL_ADDRESS}
${PWD}	${SUPER_PASSWORD}
${COMPANY_EMAIL}	${EMAIL_ADDRESS}
${COMPANY_PWD}	${PASSWORD}
${Invalid_Company}	Invalid Company

*** Test Cases ***
Test case Verify subscriptions table Visible
	Click Element	//*[@id="sa-header-tab-1-2"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="sa-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="sa-companies-licence-table-1"]/div[1]/div/table
	Sleep	2s

Test case Verify Download All Subscription
	Click Element	//*[@id="sa-companies-licence-wrapper"]/div[2]/div[2]/button
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="menu-list-grow"]/ul/li
	Sleep	5s

Test Case Verify Apply Filter On Search Subscription-CLOUD
	Sleep	2s
	Click Element	//*[@id="mui-component-select-subscription-type"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="menu-subscription-type"]/div[3]/ul/li[2]
	Sleep	2s
	Page Should Contain	ZPE Cloud

Test Case Verify Apply Filter On Search Subscription-APP
	Sleep	2s
	Click Element	//*[@id="mui-component-select-subscription-type"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="menu-subscription-type"]/div[3]/ul/li[3]
	Sleep	2s
	Page Should Contain	App

Test Case Verify Apply Filter On Search Subscription-ALL
	Sleep	2s
	Click Element	//*[@id="mui-component-select-subscription-type"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="menu-subscription-type"]/div[3]/ul/li[1]
	Sleep	2s
	Page Should Contain	App
	Page Should Contain	ZPE Cloud

Test Case Verify Invalid Company Details
	Input text	//*[@id="search-input"]	${Invalid_Company}
	Sleep	2s
	Page Should Contain	No result found

Test Case Verify Search Subscription By Company
	Press Keys	//*[@id="search-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="search-input"]
	Input text	//*[@id="search-input"]	${Company}
	Sleep	2s
	Page Should Contain Element	//*[@id="sa-companies-licence-table-1"]/div[1]/div/table/tbody/tr[1]

Test Case Delete Subscription Files
	Remove File	../../../Downloads/subscription-report.xlsx

*** Keywords ***
SUITE:Login into ZPE Cloud SuperAdmin
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud SuperAdmin

SUITE:Teardown
	Close All Browsers