*** Settings ***
Documentation	Tests for Verification of On-Premise Permission Tab
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	SUPER_ADMIN	CUSTOMER_ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${SUPER_EMAIL_ADDRESS}
${PWD}	${SUPER_PASSWORD}
${COMPANY_EMAIL}	${EMAIL_ADDRESS}
${COMPANY_PWD}	${PASSWORD}

*** Test Cases ***
Test case Disable On-Premise Permission in Super admin
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="sa-header-tab-1-2"]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="search-input"]	${COMPANY}
	Sleep	4s
	Click Element	//*[@id="sa-companies-general-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Set Focus To Element	//*[@id="sa-companies-general-edit-dialog"]/div[2]/div/div/div[2]/div[2]/div/label[5]/span[1]/input
	Sleep	2s
	Unselect Checkbox	//*[@id="sa-companies-general-edit-dialog"]/div[2]/div/div/div[2]/div[2]/div/label[2]/span[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Company details edited successfully
	Sleep	5s

Test Case Verify On-Premise Tab is not present in Company
	SUITE:Login into ZPE Cloud Company
	Click Element	//*[@id="ca-header-tab-1-7"]
	Sleep	2s
	GUI::Basic::Spinner Should Be Invisible
	Page Should Not Contain Element	//*[@id="simple-tab-1"]

Test Case Enable On-Premise Permission in Super admin
	SUITE:Login into ZPE Cloud SuperAdmin
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="sa-header-tab-1-2"]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="search-input"]	${COMPANY}
	Sleep	4s
	Click Element	//*[@id="sa-companies-general-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Set Focus To Element	//*[@id="sa-companies-general-edit-dialog"]/div[2]/div/div/div[2]/div[2]/div/label[5]/span[1]/input
	Sleep	2s
	Select Checkbox	//*[@id="sa-companies-general-edit-dialog"]/div[2]/div/div/div[2]/div[2]/div/label[2]/span[1]/input
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Company details edited successfully

Test Case Verify On-Premise Tab is present in the Company
	SUITE:Login into ZPE Cloud Company
	Click Element	//*[@id="ca-header-tab-1-7"]
	Sleep	2s
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//*[@id="simple-tab-1"]

*** Keywords ***
SUITE:Login into ZPE Cloud SuperAdmin
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${COMPANY_EMAIL}	${COMPANY_PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud SuperAdmin

SUITE:Teardown
	Close All Browsers