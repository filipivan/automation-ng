*** Settings ***
Documentation	Tests for Verification of Company Details in Superadmin
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	SUPER_ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${SUPER_EMAIL_ADDRESS}
${PWD}	${SUPER_PASSWORD}
${COMPANY_EMAIL}	${EMAIL_ADDRESS}
${COMPANY_PWD}	${PASSWORD}
${Invalid_Company}	ABC
${CREDENTIALSERROR}	User has been locked, please unlock the user and then login
${User_status}

*** Test Cases ***
Test case Verify Company Details in super-admin
	SUITE:Search Valid Company
	Page Should Contain	${COMPANY_EMAIL}
	Page Should Contain	${COMPANY}

Test Case Lock Company in Super-Admin
	SUITE:Lock Company
	SUITE:Verify If User Is Locked
	SUITE:Login into Locked Company
	SUITE:Teardown

Test Case Unlock Company in Super-Admin
	SUITE:Setup
	SUITE:Search Valid Company
	SUITE:Unlock Company
	SUITE:Verify If User Is Unlocked
	SUITE:Login into ZPE Cloud Company
	Page Should Contain Element	//*[@id="ca-header-tab-1-0"]
	SUITE:Teardown

Test Case Verify Invalid Company Details
	SUITE:Setup
	SUITE:Search Invalid Company

*** Keywords ***
SUITE:Login into ZPE Cloud SuperAdmin
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${COMPANY_EMAIL}	${COMPANY_PWD}

SUITE:Search Valid Company
	Click Element	//*[@id="sa-header-tab-1-2"]
	GUI::Basic::Spinner Should Be Invisible
	Input text	//*[@id="search-input"]	${COMPANY}
	Sleep	2s

SUITE:Search Invalid Company
	Click Element	//*[@id="sa-header-tab-1-2"]
	GUI::Basic::Spinner Should Be Invisible
	Press Keys	//*[@id="search-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="search-input"]
	Input text	//*[@id="search-input"]	${Invalid_Company}
	Sleep	2s
	Page Should Contain	No result found

SUITE:Lock Company
	Click Element	//*[@id="sa-companies-general-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="function-button-2"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Company locked successfully

SUITE:Unlock Company
	Click Element	//*[@id="sa-companies-general-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="function-button-3"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Company unlocked successfully

SUITE:Login into Locked Company
	GUI::ZPECloud::Basic::Open ZPE Cloud	${ZPECLOUD_HOMEPAGE}	${BROWSER}
	Sleep	2
	Set Window Size	1600	1000
	GUI::ZPECloud::Basic::Wait Elements Login
	Input Text	xpath=//form//input[1]	${COMPANY_EMAIL}
	Input Text	css=input[id=filled-adornment-password]	${COMPANY_PWD}
	Click Element	xpath=//form/button[1]
	Wait Until Page Contains	${CREDENTIALSERROR}	15s

SUITE:Verify If User Is Locked
	Click Element	//*[@id="sa-header-tab-1-3"]
	GUI::Basic::Spinner Should Be Invisible
	Input text	//*[@id="search-input"]	${COMPANY_EMAIL}
	Sleep	2s
	${User_status}	Get Text	//*[@id="sa-users-general-table-1"]/div[1]/div/table/tbody/tr/td[5]
	Should be equal as strings	${User_status}	Locked

SUITE:Verify If User Is Unlocked
	Click Element	//*[@id="sa-header-tab-1-3"]
	GUI::Basic::Spinner Should Be Invisible
	Input text	//*[@id="search-input"]	${COMPANY_EMAIL}
	Sleep	2s
	${User_status}	Get Text	//*[@id="sa-users-general-table-1"]/div[1]/div/table/tbody/tr/td[5]
	Should be equal as strings	${User_status}	Active

SUITE:Setup
	SUITE:Login into ZPE Cloud SuperAdmin

SUITE:Teardown
	Close All Browsers