*** Settings ***
Documentation	Tests for Verify Maintenance page in Super Admin
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	SUPER ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL    NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${SUPER_EMAIL_ADDRESS}
${PWD}	${SUPER_PASSWORD}
${Message}      Automation Testing

*** Test Cases ***
Test case Add Maintainance Banner in Superadmin
    Click Button       //*[@id="sa-header-tab-1-6"]
    GUI::Basic::Spinner Should Be Invisible
    Click Element       //*[@id="sa-header-tab-2-1"]
    GUI::Basic::Spinner Should Be Invisible
    Sleep       5s
    Select Checkbox      //*[@id="maintenance-checkbox"]
    GUI::Basic::Spinner Should Be Invisible
    Checkbox Should Be Selected     //*[@id="maintenance-checkbox"]
    SUITE:Selection of Start Date and end date for Banner
    SUITE: Custom Message
    SUITE:Selection of Start Date and end date for maintenance
    Click Element       //*[@id="function-button-0"]
    GUI::Basic::Spinner Should Be Invisible
    Wait Until Page Contains     Banner successfully updated.
    Sleep       70s

Test case Verify Banner and Maintenance Message
    SUITE:Open ZPE Cloud Homepage
    Page Should Contain     ${Message}      5s

*** Keywords ***
SUITE:Login into ZPE Cloud Company
    GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Open ZPE Cloud Homepage
    GUI::ZPECloud::Basic::Open ZPE Cloud	${ZPECLOUD_HOMEPAGE}	${BROWSER}

SUITE:Setup
    SUITE:Login into ZPE Cloud Company

SUITE:Selection of Start Date and end date for Banner
    ${DATE}=	Get Current Date        result_format=%Y-%m-%d %H:%M:%S
	${DATE}=	Add Time To Date	${DATE}	  1 minute
	${DATE}=	Convert Date	${DATE}  	result_format=%Y-%m-%d %H:%M:%S
	Press Keys     //*[@id="personalize-email-wrapper"]/div[3]/div[2]/span/div[1]/div/input     CTRL+a+BACKSPACE
    Clear Element Text      //*[@id="personalize-email-wrapper"]/div[3]/div[2]/span/div[1]/div/input
    Input Text      //*[@id="personalize-email-wrapper"]/div[3]/div[2]/span/div[1]/div/input        ${DATE}
    ${Start_Date}=	Get Value	//*[@id="personalize-email-wrapper"]/div[3]/div[2]/span/div[1]/div/input
	Set Suite Variable		${DATE}	    ${Start_Date}
    ${DATE1}=	Get Current Date        result_format=%Y-%m-%d %H:%M:%S
	${DATE1}=	Add Time To Date	${DATE1}	  3 minute
	${DATE1}=	Convert Date	${DATE1}  	result_format=%Y-%m-%d %H:%M:%S
	Press Keys     //*[@id="personalize-email-wrapper"]/div[3]/div[3]/span/div[1]/div/input     CTRL+a+BACKSPACE
    Clear Element Text      //*[@id="personalize-email-wrapper"]/div[3]/div[3]/span/div[1]/div/input
    Input Text      //*[@id="personalize-email-wrapper"]/div[3]/div[3]/span/div[1]/div/input       ${DATE1}
    ${End_Date}=	Get Value	//*[@id="personalize-email-wrapper"]/div[3]/div[3]/span/div[1]/div/input
	Set Suite Variable		${DATE1}	    ${End_Date}

SUITE:Selection of Start Date and end date for maintenance
	Press Keys     //*[@id="personalize-email-wrapper"]/div[5]/div[2]/span/div[1]/div/input     CTRL+a+BACKSPACE
    Clear Element Text      //*[@id="personalize-email-wrapper"]/div[5]/div[2]/span/div[1]/div/input
    Input Text      //*[@id="personalize-email-wrapper"]/div[5]/div[2]/span/div[1]/div/input        ${DATE}
	Press Keys     //*[@id="personalize-email-wrapper"]/div[5]/div[3]/span/div[1]/div/input     CTRL+a+BACKSPACE
    Clear Element Text      //*[@id="personalize-email-wrapper"]/div[5]/div[3]/span/div[1]/div/input
    Input Text      //*[@id="personalize-email-wrapper"]/div[5]/div[3]/span/div[1]/div/input        ${DATE1}
    Sleep       2s

SUITE: Custom Message
    Click Element       //*[@id="personalize-email-wrapper"]/div[4]/div[2]/label/span[1]/input
    GUI::Basic::Spinner Should Be Invisible
    Press Keys     //*[@id="email-text-input"]/div[2]/div[1]    CTRL+a+BACKSPACE
    Clear Element Text      //*[@id="email-text-input"]/div[2]/div[1]
    Input Text      //*[@id="email-text-input"]/div[2]/div[1]      ${Message}

SUITE:Teardown
    Close All Browsers
