*** Settings ***
Documentation	Tests for Verify Template details
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	SUPER ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${SUPER_EMAIL_ADDRESS}
${PWD}	${SUPER_PASSWORD}
${Invalid_Template}	Invalid
${Script_Name}	Template_Oro
${Description}	Oro_Automation_Template

*** Test Cases ***
Test case Verify Invalid Template
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="sa-header-tab-1-4"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="sa-header-tab-2-2"]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	//*[@id="search-input"]	${Invalid_Template}
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Page Should Contain	No result found

Test Case Add Script Template
	Press Keys	//*[@id="search-input"]	CTRL+a+BACKSPACE
	Clear Element Text	//*[@id="search-input"]
	Click Element	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Input text	//*[@id="template-name-input"]	${Script_Name}
	Input Text	//*[@id="template-description-select"]	${Description}
	sleep	2s
	Click Element	//*[@id="template-type-select"]
	Click Element	//*[@id="template-type-option-1"]
	Input Text	//*[@id="template-code-input"]	echo "test file" > /tmp/test.txt
	Click Button	//*[@id="function-button-1"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Input Text	//*[@id="search-input"]	${Script_Name}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	${Script_Name}

Test case Download Script Template
	Click Element	//*[@id="super-admin-layout"]/main/div[1]/div[2]/div[3]/div[1]/div/table/tbody/tr/td[6]/span
	GUI::Basic::Spinner Should Be Invisible

Test Case Delete Script Template
	Click Element	//*[@id="function-button-2"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="confirm-btn"]
	Sleep	3s
	Page should Contain	No result found

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Teardown
	Close All Browsers
