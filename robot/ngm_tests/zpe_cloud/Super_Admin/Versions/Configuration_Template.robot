*** Settings ***
Documentation	Tests for Verify Configuration Template details
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	SUPER ADMIN
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${SUPER_EMAIL_ADDRESS}
${PWD}	${SUPER_PASSWORD}
${Invalid_Template}	Invalid
${Configuration_Name}	Configuration_Template_Oro
${Description}	Oro_Automation_Template
${CODE}	${LINE1}	${\n}	${LINE2}	${\n}	${LINE3}
${LINE1}	cd /settings/network_settings/
${LINE2}	set hostname=test
${LINE3}	commit

*** Test Cases ***
Test Case Add Configuration Template
	Click Button	//*[@id="sa-header-tab-1-4"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="sa-header-tab-2-2"]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="function-button-0"]
	GUI::Basic::Spinner Should Be Invisible
	Input text	//*[@id="template-name-input"]	${Configuration_Name}
	Input Text	//*[@id="template-description-select"]	${Description}
	sleep	2s
	Click Element	//*[@id="template-type-select"]
	Click Element	//*[@id="template-type-option-0"]
	Input Text	//*[@id="template-code-input"]	${CODE}
	Click Button	//*[@id="function-button-1"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Input Text	//*[@id="search-input"]	${Configuration_Name}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	${Configuration_Name}

Test case Download Configuration Template
	Click Element	//*[@id="super-admin-layout"]/main/div[1]/div[2]/div[3]/div[1]/div/table/tbody/tr/td[6]/span
	GUI::Basic::Spinner Should Be Invisible

Test Case Delete Configuration Template
	Click Element	//*[@id="function-button-2"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="confirm-btn"]
	Sleep	3s
	Page should Contain	No result found

Test Case Remove Downloded Files
	Remove File	../../../Downloads/${Configuration_Name}.txt

*** Keywords ***
SUITE:Login into ZPE Cloud Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Company

SUITE:Teardown
	Close All Browsers
