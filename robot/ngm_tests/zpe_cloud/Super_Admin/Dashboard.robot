*** Settings ***
Documentation	Tests for Verification of Super Admin Dashboard
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Default Tags	ACCESS	CONSOLE
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${SUPER_EMAIL_ADDRESS}
${PWD}	${SUPER_PASSWORD}

*** Test Cases ***
Test case Verify Map Lock
	Sleep	3s
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="ca-dashboard-map-wrapper"]/div[1]/div[2]/button
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Map locked successfully

Test Case Verify Help Link
	Wait Until Keyword Succeeds	3x	5s	SUITE:Click and verify Help Link

*** Keywords ***
SUITE:Login into ZPE Cloud Super Admin
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup
	SUITE:Login into ZPE Cloud Super Admin

SUITE:Teardown
	Close All Browsers

SUITE:Click and verify Help Link
	Click Element	//*[@id="sa-help-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	15s
	Switch Window	NEW	#Introduction to ZPE Cloud User Guide
	Sleep	2s
	Page Should Contain	Overview
	Sleep	2s