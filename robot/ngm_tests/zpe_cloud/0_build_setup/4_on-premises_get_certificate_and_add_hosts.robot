*** Settings ***
Documentation	Tests to add the on-premises certificate and IPs also hostnames into Jenkins VM and NG devices
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Metadata	Executed using NG devices	${HOST} and ${HOSTPEER}
Resource	../../init.robot
Default Tags	ENROLLMENT	UNENROLLMENT
Force Tags	GUI	ZPECLOUD	${BROWSER}	ON-PREMISES-PRODUCT

Suite Teardown	SUITE:Teardown

*** Variables ***
${ZPE_CLOUD_CA_CERTIFICATES_FILE_NAME}	qa-automation-zpe-cloud-ca-certificates.crt

*** Test Cases ***
Test case to Get the on-premises certificate
	CLI:Open	HOST_DIFF=${ON_PREMISES_SERVER_IP}	USERNAME=${ON_PREMISES_SERVER_USERNAME}	PASSWORD=${ON_PREMISES_SERVER_PASSWORD}
	#line bellow is to use on single-node environment only
#	CLI:Write	kubectl -n certificate get secret zpecloud-poc-tls -o jsonpath='{.data.ca\\.crt}' | base64 -d > /tmp/${ZPE_CLOUD_CA_CERTIFICATES_FILE_NAME}
	#line bellow is to use on multi-node environment only
	CLI:Write	cat /etc/ssl/certs/self-signed-tls-secret.pem > /tmp/${ZPE_CLOUD_CA_CERTIFICATES_FILE_NAME}
	${LS_OUTPUT}	CLI:Write	ls /tmp/
	Should Contain	${LS_OUTPUT}	${ZPE_CLOUD_CA_CERTIFICATES_FILE_NAME}
	${CAT_OUTPUT}	CLI:Write	cat /tmp/${ZPE_CLOUD_CA_CERTIFICATES_FILE_NAME}
	Should Contain	${CAT_OUTPUT}	END CERTIFICATE
	[Teardown]	Close Connection

Test case to add the On-Premises Server's certificate, IP and hostnames into Jenkins VM
	${OS}=	CLIENT:Get Operating System
	Set Suite Variable	${OS}
	Log	Client OS:\n ${OS}	INFO	console=yes
	IF	'Microsoft' in '''${OS}'''
		CLI:Open	HOST_DIFF=${CLIENTIP}	USERNAME=${CLOUD_JENKINS_VM_USERNAME}	PASSWORD=${CLOUD_JENKINS_VM_PASSWORD}	TYPE=windows
		Write	ssh-keygen -f "%home%\\.ssh\\known_hosts" -R "${HOST_DIFF}"
		...
	ELSE IF	'Linux' in '''${OS}'''
		CLI:Open	HOST_DIFF=${CLIENTIP}	USERNAME=${CLOUD_JENKINS_VM_USERNAME}	PASSWORD=${CLOUD_JENKINS_VM_PASSWORD}	TYPE=shell
		CLI:Write	ssh-keygen -f "/home/${CLOUD_JENKINS_VM_USERNAME}/.ssh/known_hosts" -R "${ON_PREMISES_SERVER_IP}"
		Set Client Configuration	prompt=qausers:
		CLI:Write	sudo su -
		Set Client Configuration	prompt=#
		CLI:Write	${CLOUD_JENKINS_VM_PASSWORD}
		Set Client Configuration	prompt=ca-certificates#
		CLI:Write	cd /usr/local/share/ca-certificates/
		Log	\nscp -p ${ON_PREMISES_SERVER_USERNAME}@${ON_PREMISES_SERVER_IP}:/tmp/${ZPE_CLOUD_CA_CERTIFICATES_FILE_NAME} .\n	console=yes
		Set Client Configuration	timeout=120s
		Write	scp -p ${ON_PREMISES_SERVER_USERNAME}@${ON_PREMISES_SERVER_IP}:/tmp/${ZPE_CLOUD_CA_CERTIFICATES_FILE_NAME} .
		Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
		${WRITE_OUTPUT}=	CLI:Read Until Regexp	(~#|Password:|Are you sure you want to continue connecting \\(.*\\)\\?)
		${HAS_ROOT_PROMPT}=	Run Keyword And Return Status	Should Match Regexp	${WRITE_OUTPUT}	ca-certificates#
		Run Keyword If	${HAS_ROOT_PROMPT}	Fail	Failed to scp the "${ZPE_CLOUD_CA_CERTIFICATES_FILE_NAME}" file.
		${HAS_TO_CONFIRM}=	Run Keyword And Return Status	Should Match Regexp	${WRITE_OUTPUT}	Are you sure you want to continue connecting \\(.*\\)\\?
		IF	${HAS_TO_CONFIRM}
			Write	yes
			${WRITE_OUTPUT}=	CLI:Read Until Regexp	(~#|Password:)
			Log To Console	\n${WRITE_OUTPUT}\n
		END
		Set Client Configuration	prompt=ca-certificates#
		CLI:Write	${ON_PREMISES_SERVER_PASSWORD}
		${LS_OUTPUT}	CLI:Write	ls
		Should Contain	${LS_OUTPUT}	${ZPE_CLOUD_CA_CERTIFICATES_FILE_NAME}
		${UPDATE_OUTPUT}	CLI:Write	update-ca-certificates -f
		Should Contain	${UPDATE_OUTPUT}	Updating certificates
		${CAT_OUTPUT}	CLI:Write	cat /etc/hosts
		${HAS_ON_PREMISES_SERVER_IP}	Run Keyword And Return Status	Should Contain	${CAT_OUTPUT}	${ON_PREMISES_ETC_HOSTS}
		IF	${HAS_ON_PREMISES_SERVER_IP}
			CLI:Write	sed -i '/${ON_PREMISES_SERVER_IP}/d' /etc/hosts
			${CAT_OUTPUT}	CLI:Write	cat /etc/hosts
			Should Not Contain	${CAT_OUTPUT}	${ON_PREMISES_ETC_HOSTS}
		END
		CLI:Write	echo "${ON_PREMISES_ETC_HOSTS}" >> /etc/hosts
		${CAT_OUTPUT}	CLI:Write	cat /etc/hosts
		Set Client Configuration	prompt=#
		Should Contain	${CAT_OUTPUT}	${ON_PREMISES_ETC_HOSTS}
	END
	[Teardown]	Close Connection

Test case to add the On-Premises Server's certificate, IP and hostnames into host NG Device
	[Tags]	GUI	ZPECLOUD	${BROWSER}	ON-PREMISES-PRODUCT	DEVICE
	CLI:Open	USERNAME=${USERNAMENG}	PASSWORD=${PASSWORDNG}
	Set Client Configuration	prompt=#
	CLI:Write	shell sudo su -
	CLI:Write	mount -o remount, ro /
	CLI:Write	mkdir -p /usr/local/share/ca-certificates/
	Set Client Configuration	prompt=ca-certificates#
	CLI:Write	cd /usr/local/share/ca-certificates/
	CLI:Write	ssh-keygen -f "/home/${ROOT_DEFAULT_USERNAME}/.ssh/known_hosts" -R "${ON_PREMISES_SERVER_IP}"
	Log	\nscp -p ${ON_PREMISES_SERVER_USERNAME}@${ON_PREMISES_SERVER_IP}:/tmp/${ZPE_CLOUD_CA_CERTIFICATES_FILE_NAME} .\n	console=yes
	Set Client Configuration	timeout=120s
	Write	scp -p ${ON_PREMISES_SERVER_USERNAME}@${ON_PREMISES_SERVER_IP}:/tmp/${ZPE_CLOUD_CA_CERTIFICATES_FILE_NAME} .
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	${WRITE_OUTPUT}=	CLI:Read Until Regexp	(ca-certificates#|Password:|Are you sure you want to continue connecting \\(.*\\)\\?)
	${HAS_ROOT_PROMPT}=	Run Keyword And Return Status	Should Match Regexp	${WRITE_OUTPUT}	ca-certificates#
	Run Keyword If	${HAS_ROOT_PROMPT}	Fail	Failed to scp the "${ZPE_CLOUD_CA_CERTIFICATES_FILE_NAME}" file.
	${HAS_TO_CONFIRM}=	Run Keyword And Return Status	Should Match Regexp	${WRITE_OUTPUT}	Are you sure you want to continue connecting \\(.*\\)\\?
	IF	${HAS_TO_CONFIRM}
		Write	yes
		${WRITE_OUTPUT}=	CLI:Read Until Regexp	(~#|Password:)
		Log To Console	\n${WRITE_OUTPUT}\n
	END
		Set Client Configuration	prompt=ca-certificates#
	CLI:Write	${ON_PREMISES_SERVER_PASSWORD}
	${LS_OUTPUT}	CLI:Write	ls
	Should Contain	${LS_OUTPUT}	${ZPE_CLOUD_CA_CERTIFICATES_FILE_NAME}
	${UPDATE_OUTPUT}	CLI:Write	update-ca-certificates -f
	Should Contain	${UPDATE_OUTPUT}	Updating certificates
	${CAT_OUTPUT}	CLI:Write	cat /etc/hosts
	${HAS_ON_PREMISES_SERVER_IP}	Run Keyword And Return Status	Should Contain	${CAT_OUTPUT}	${ON_PREMISES_ETC_HOSTS}
	IF	${HAS_ON_PREMISES_SERVER_IP}
		CLI:Write	sed -i '/${ON_PREMISES_SERVER_IP}/d' /etc/hosts
		${CAT_OUTPUT}	CLI:Write	cat /etc/hosts
		Should Not Contain	${CAT_OUTPUT}	${ON_PREMISES_ETC_HOSTS}
	END
	CLI:Write	echo "${ON_PREMISES_ETC_HOSTS}" >> /etc/hosts
	${CAT_OUTPUT}	CLI:Write	cat /etc/hosts
	Set Client Configuration	prompt=#
	Should Contain	${CAT_OUTPUT}	${ON_PREMISES_ETC_HOSTS}
	[Teardown]	Close Connection

Test case to validate On-Premises API endpoint
	${BROWSER}	Convert To Lower Case	${BROWSER}
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nOpening Browser: ${BROWSER}	INFO	console=yes
	Run Keyword If	'${BROWSER}' == 'chrome' or '${BROWSER}' == 'headlesschrome'	Wait Until Keyword Succeeds	3x	3s	Open Browser	${ZPECLOUD_APIPAGE}	${BROWSER}	options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors")
	Run Keyword If	'${BROWSER}' == 'firefox' or '${BROWSER}' == 'headlessfirefox'	Wait Until Keyword Succeeds	3x	3s	Open Browser	${ZPECLOUD_APIPAGE}	${BROWSER}	options=set_preference("moz:dom.disable_beforeunload", "true")
	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\nBrowser ${BROWSER} opened	INFO	console=yes
	Wait Until Page Contains	Authentication credentials were not provided.	timeout=2m

Test case to validate On-Premises WebUI home page
	${BROWSER}	Convert To Lower Case	${BROWSER}
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nOpening Browser: ${BROWSER}	INFO	console=yes
	Run Keyword If	'${BROWSER}' == 'chrome' or '${BROWSER}' == 'headlesschrome'	Wait Until Keyword Succeeds	3x	3s	Open Browser	${ZPECLOUD_HOMEPAGE}	${BROWSER}	options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors")
	Run Keyword If	'${BROWSER}' == 'firefox' or '${BROWSER}' == 'headlessfirefox'	Wait Until Keyword Succeeds	3x	3s	Open Browser	${ZPECLOUD_HOMEPAGE}	${BROWSER}	options=set_preference("moz:dom.disable_beforeunload", "true")
	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\nBrowser ${BROWSER} opened	INFO	console=yes
	GUI::ZPECloud::Basic::Wait Elements Login

*** Keywords ***
SUITE:Teardown
	CLI:Close Connection
	Close All Browsers
	Run Keyword If Any Tests Failed	Fatal Error	The tests cases in this script are critical, need to check what's the root cause of the failure(s).