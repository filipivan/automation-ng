*** Settings ***
Documentation	Tests for enrollment device on cloud
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Metadata	Executed using NG devices	${HOST} and ${HOSTPEER}
Resource	../../init.robot
Default Tags	ENROLLMENT	UNENROLLMENT
Force Tags	GUI	ZPECLOUD	${BROWSER}	DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${CUSTOMERCODE}
${ENROLLMENTKEY}
${PRODUCTION_CUSTOMERCODE}	AWNII7	#From zpe779927xpbi@gmail.com account
${PRODUCTION_ENROLLMENTKEY}	BZivMUVnAptzUXrT
${PRODUCTION_ZPECLOUD_HOMEPAGE}	https://zpecloud.com

*** Test Cases ***
Test case to unenroll host device from production by CLI
	[Setup]	Wait Until Keyword Succeeds	3x	1s	CLI:Connect As Root	${HOST}	${PASSWORDNG}
	${UNENROLL_PRODUCTION}=	Set Variable	zpe_cloud_enroll -c ${PRODUCTION_CUSTOMERCODE} -k ${PRODUCTION_ENROLLMENTKEY} -u ${PRODUCTION_ZPECLOUD_HOMEPAGE} -s
	Set Default Configuration	timeout=2m
	Set Client Configuration	timeout=2m
	CLI:Write	rm -rf /var/zpe-cloud/astarte/persistency/control/urls/*
	Set Client Configuration	prompt=(yes, no):
	CLI:Write	${UNENROLL_PRODUCTION}
	Set Client Configuration	prompt=#
	${OUTPUT}=	CLI:Write	yes
	Set Client Configuration	prompt=(yes, no):
	#TPM Clean
	${STATUS}	Evaluate NG Versions: ${NGVERSION} <= 5.2
	Run Keyword If	${STATUS}	CLI:Write	zpe_cloud_enroll -c c -k k -u ${PRODUCTION_ZPECLOUD_HOMEPAGE} -s
	...	ELSE	CLI:Write	zpe_cloud_enroll --enroll-clean -u ${PRODUCTION_ZPECLOUD_HOMEPAGE}
	Set Client Configuration	prompt=#
	CLI:Write	yes
	${HAS_RETRY_OR_IN_PROGRESS}	Run Keyword And Return Status	Should Contain Any	${OUTPUT}
	...	Please check it and retry the enrollment	Enrollment process already in progress
	IF	${HAS_RETRY_OR_IN_PROGRESS}
		Sleep	2m
		Set Client Configuration	prompt=(yes, no):
		CLI:Write	${UNENROLL_PRODUCTION}
		Set Client Configuration	prompt=#
		${OUTPUT}=	CLI:Write	yes
	END
	Should Contain Any	${OUTPUT}	Unenrollment process successful!	Device is not registered
	[Teardown]	CLI:Close Connection

Test case to enroll host device on production by CLI
	[Setup]	Wait Until Keyword Succeeds	3x	1s	CLI:Connect As Root	${HOST}	${PASSWORDNG}
	${ENROLL}=	Set Variable	zpe_cloud_enroll -c ${PRODUCTION_CUSTOMERCODE} -k ${PRODUCTION_ENROLLMENTKEY} -u ${PRODUCTION_ZPECLOUD_HOMEPAGE}
	Set Default Configuration	timeout=2m
	Set Client Configuration	timeout=2m
	Set Client Configuration	prompt=#
	${OUTPUT}=	CLI:Write	${ENROLL}
	${HAS_RETRY_OR_IN_PROGRESS}	Run Keyword And Return Status	Should Contain Any	${OUTPUT}
	...	Please check it and retry the enrollment	Please contact ZPE Cloud support	Enrollment process already in progress
	IF	${HAS_RETRY_OR_IN_PROGRESS}
		Sleep	2m
		${OUTPUT}=	CLI:Write	${ENROLL}
	END
	Should Contain	${OUTPUT}	Enrollment process successful!
	[Teardown]	CLI:Close Connection

Test case to unenroll host device from instance by CLI
	[Setup]	Wait Until Keyword Succeeds	3x	1s	CLI:Connect As Root	${HOST}	${PASSWORDNG}
	${UNENROLL}=	Set Variable	zpe_cloud_enroll -c ${CUSTOMERCODE} -k ${ENROLLMENTKEY} -u ${ZPECLOUD_HOMEPAGE} -s
	Set Default Configuration	timeout=2m
	Set Client Configuration	timeout=2m
	Set Client Configuration	prompt=(yes, no):
	CLI:Write	${UNENROLL}
	Set Client Configuration	prompt=#
	${OUTPUT}=	CLI:Write	yes
	Set Client Configuration	prompt=(yes, no):
	#TPM Clean
	${STATUS}	Evaluate NG Versions: ${NGVERSION} <= 5.2
	Run Keyword If	${STATUS}	CLI:Write	zpe_cloud_enroll -c c -k k -u ${ZPECLOUD_HOMEPAGE} -s
	...	ELSE	CLI:Write	zpe_cloud_enroll --enroll-clean -u ${ZPECLOUD_HOMEPAGE}
	Set Client Configuration	prompt=#
	CLI:Write	yes
	${HAS_RETRY_OR_IN_PROGRESS}	Run Keyword And Return Status	Should Contain Any	${OUTPUT}
	...	Please check it and retry the enrollment	Enrollment process already in progress
	IF	${HAS_RETRY_OR_IN_PROGRESS}
		Sleep	2m
		Set Client Configuration	prompt=(yes, no):
		CLI:Write	${UNENROLL}
		Set Client Configuration	prompt=#
		${OUTPUT}=	CLI:Write	yes
	END
	Should Contain Any	${OUTPUT}	Unenrollment process successful!	Device is not registered
	[Teardown]	CLI:Close Connection

Test case to enroll host on instance by CLI
	[Setup]	Wait Until Keyword Succeeds	3x	1s	CLI:Connect As Root	${HOST}	${PASSWORDNG}
	${ENROLL}=	Set Variable	zpe_cloud_enroll -c ${CUSTOMERCODE} -k ${ENROLLMENTKEY} -u ${ZPECLOUD_HOMEPAGE}
	Set Default Configuration	timeout=2m
	Set Client Configuration	timeout=2m
	Set Client Configuration	prompt=#
	${OUTPUT}=	CLI:Write	${ENROLL}
	${HAS_RETRY_OR_IN_PROGRESS}	Run Keyword And Return Status	Should Contain Any	${OUTPUT}
	...	Please check it and retry the enrollment	Please contact ZPE Cloud support	Enrollment process already in progress
	IF	${HAS_RETRY_OR_IN_PROGRESS}
		Sleep	2m
		${OUTPUT}=	CLI:Write	${ENROLL}
	END
	Should Contain	${OUTPUT}	Enrollment process successful!
	[Teardown]	CLI:Close Connection

Move the host from Devices::Available to Devices::Enrolled page
	Wait Until Keyword Succeeds	3x	3s	SUITE:Move the NG from Devices::Available to Devices::Enrolled page	${SERIALNUMBER}
	Wait Until Keyword Succeeds	15m	5s	SUITE:Device Enrolled Successfully	${SERIALNUMBER}
	[Teardown]	Close All Browsers

#HOSTPEER

Test case to unenroll hostpeer device from production by CLI
	[Tags]	EXCLUDEIN5_2	EXCLUDEIN_ONPREMISES
	GUI::ZPECloud::Check and update the zpecloud serial number variable if needed	${USERNAMENGPEER}	${PASSWORDNG}	IS_HOSTPEER=${TRUE}
	Wait Until Keyword Succeeds	3x	1s	CLI:Connect As Root	${HOSTPEER}	${PASSWORDNG}
	${UNENROLL_PRODUCTION}=	Set Variable	zpe_cloud_enroll -c ${PRODUCTION_CUSTOMERCODE} -k ${PRODUCTION_ENROLLMENTKEY} -u ${PRODUCTION_ZPECLOUD_HOMEPAGE} -s
	Set Default Configuration	timeout=2m
	Set Client Configuration	timeout=2m
	Set Client Configuration	prompt=#
	CLI:Write	rm -rf /var/zpe-cloud/astarte/persistency/control/urls/*
	Set Client Configuration	prompt=(yes, no):
	CLI:Write	${UNENROLL_PRODUCTION}
	Set Client Configuration	prompt=#
	${OUTPUT}=	CLI:Write	yes
	Set Client Configuration	prompt=(yes, no):
	#TPM Clean
	${STATUS}	Evaluate NG Versions: ${NGVERSION} <= 5.2
	Run Keyword If	${STATUS}	CLI:Write	zpe_cloud_enroll -c c -k k -u ${PRODUCTION_ZPECLOUD_HOMEPAGE} -s
	...	ELSE	CLI:Write	zpe_cloud_enroll --enroll-clean -u ${PRODUCTION_ZPECLOUD_HOMEPAGE}
	Set Client Configuration	prompt=#
	CLI:Write	yes
	${HAS_RETRY_OR_IN_PROGRESS}	Run Keyword And Return Status	Should Contain Any	${OUTPUT}
	...	Please check it and retry the enrollment	Enrollment process already in progress
	IF	${HAS_RETRY_OR_IN_PROGRESS}
		Sleep	2m
		Set Client Configuration	prompt=(yes, no):
		CLI:Write	${UNENROLL_PRODUCTION}
		Set Client Configuration	prompt=#
		${OUTPUT}=	CLI:Write	yes
	END
	Should Contain Any	${OUTPUT}	Unenrollment process successful!	Device is not registered
	[Teardown]	CLI:Close Connection

Test case to enroll hostpeer device on production by CLI
	[Tags]	EXCLUDEIN5_2	EXCLUDEIN_ONPREMISES
	[Setup]	Wait Until Keyword Succeeds	3x	1s	CLI:Connect As Root	${HOSTPEER}	${PASSWORDNG}
	${ENROLL}=	Set Variable	zpe_cloud_enroll -c ${PRODUCTION_CUSTOMERCODE} -k ${PRODUCTION_ENROLLMENTKEY} -u ${PRODUCTION_ZPECLOUD_HOMEPAGE}
	Set Default Configuration	timeout=2m
	Set Client Configuration	timeout=2m
	Set Client Configuration	prompt=#
	${OUTPUT}=	CLI:Write	${ENROLL}
	${HAS_RETRY_OR_IN_PROGRESS}	Run Keyword And Return Status	Should Contain Any	${OUTPUT}
	...	Please check it and retry the enrollment	Please contact ZPE Cloud support	Enrollment process already in progress
	IF	${HAS_RETRY_OR_IN_PROGRESS}
		Sleep	2m
		${OUTPUT}=	CLI:Write	${ENROLL}
	END
	Should Contain	${OUTPUT}	Enrollment process successful!
	[Teardown]	CLI:Close Connection

Test case to unenroll hostpeer device from instance by CLI
	[Tags]	EXCLUDEIN5_2	EXCLUDEIN_ONPREMISES
	[Setup]	Wait Until Keyword Succeeds	3x	1s	CLI:Connect As Root	${HOSTPEER}	${PASSWORDNG}
	${UNENROLL}=	Set Variable	zpe_cloud_enroll -c ${CUSTOMERCODE} -k ${ENROLLMENTKEY} -u ${ZPECLOUD_HOMEPAGE} -s
	Set Default Configuration	timeout=2m
	Set Client Configuration	timeout=2m
	Set Client Configuration	prompt=(yes, no):
	CLI:Write	${UNENROLL}
	Set Client Configuration	prompt=#
	${OUTPUT}=	CLI:Write	yes
	Set Client Configuration	prompt=(yes, no):
	#TPM Clean
	${STATUS}	Evaluate NG Versions: ${NGVERSION} <= 5.2
	Run Keyword If	${STATUS}	CLI:Write	zpe_cloud_enroll -c c -k k -u ${ZPECLOUD_HOMEPAGE} -s
	...	ELSE	CLI:Write	zpe_cloud_enroll --enroll-clean -u ${ZPECLOUD_HOMEPAGE}
	Set Client Configuration	prompt=#
	CLI:Write	yes
	${HAS_RETRY_OR_IN_PROGRESS}	Run Keyword And Return Status	Should Contain Any	${OUTPUT}
	...	Please check it and retry the enrollment	Enrollment process already in progress
	IF	${HAS_RETRY_OR_IN_PROGRESS}
		Sleep	2m
		Set Client Configuration	prompt=(yes, no):
		CLI:Write	${UNENROLL}
		Set Client Configuration	prompt=#
		${OUTPUT}=	CLI:Write	yes
	END
	Should Contain Any	${OUTPUT}	Unenrollment process successful!	Device is not registered
	[Teardown]	CLI:Close Connection

Test case to enroll hostpeer on instance by CLI
	[Tags]	EXCLUDEIN5_2	EXCLUDEIN_ONPREMISES
	[Setup]	Wait Until Keyword Succeeds	3x	1s	CLI:Connect As Root	${HOSTPEER}	${PASSWORDNG}
	${ENROLL}=	Set Variable	zpe_cloud_enroll -c ${CUSTOMERCODE} -k ${ENROLLMENTKEY} -u ${ZPECLOUD_HOMEPAGE}
	Set Default Configuration	timeout=2m
	Set Client Configuration	timeout=2m
	Set Client Configuration	prompt=#
	${OUTPUT}=	CLI:Write	${ENROLL}
	${HAS_RETRY_OR_IN_PROGRESS}	Run Keyword And Return Status	Should Contain Any	${OUTPUT}
	...	Please check it and retry the enrollment	Please contact ZPE Cloud support	Enrollment process already in progress
	IF	${HAS_RETRY_OR_IN_PROGRESS}
		Sleep	2m
		${OUTPUT}=	CLI:Write	${ENROLL}
	END
	Should Contain	${OUTPUT}	Enrollment process successful!
	[Teardown]	CLI:Close Connection

Move the hostpeer from Devices::Available to Devices::Enrolled page
	[Tags]	EXCLUDEIN5_2	EXCLUDEIN_ONPREMISES
	Wait Until Keyword Succeeds	3x	3s	SUITE:Move the NG from Devices::Available to Devices::Enrolled page	${SERIALNUMBERPEER}
	Wait Until Keyword Succeeds	15m	5s	SUITE:Device Enrolled Successfully	${SERIALNUMBERPEER}
	[Teardown]	Close All Browsers

*** Keywords ***
SUITE:Setup
	${LOGIN_ON_NGPEER}	Set Variable	${FALSE}
	Set Suite Variable	${LOGIN_ON_NGPEER}
	${NG_REACHABLE}	Set Variable	${FALSE}
	${ADMIN_LOGIN_SUCCESS}	Set Variable	${FALSE}
	${ASK_CURRENT_PASSWORD}	Set Variable	${FALSE}
	${HAS_STRONG_PASSWORD}	Set Variable	${FALSE}
	${DEFAULT_USERNAME}	Set Variable	${USERNAMENG}
	${DEFAULT_PASSWORD}	Set Variable	${PASSWORDNG}
	${ROOT_PASSWORD}	Set Variable	${PASSWORDNG}
	${QA_PASSWORD}	Set Variable	${PASSWORDNG}
	Set Global Variable	${DEFAULT_USERNAME}
	Set Global Variable	${DEFAULT_PASSWORD}
	Set Global Variable	${ROOT_PASSWORD}
	Set Global Variable	${QA_PASSWORD}
	SUITE:Get Device Enrolment Info on QA Instance
	SUITE:Enable Device Enrolment and Disable On-Premises Enrollment If Needed
	GUI::ZPECloud::Check and update the zpecloud serial number variable if needed	${USERNG}	${PWDNG}

SUITE:Teardown
	CLI:Close Connection
	Close All Browsers
#	Run Keyword If Any Tests Failed	Fatal Error	The tests cases in this script are critical, need to check what's the root cause of the failure(s).

SUITE:Get Device Enrolment Info on QA Instance
	Wait Until Keyword Succeeds	3x	3s	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Settings::Open Tab
	${CUSTOMERCODE}=	Get Value	ca-setgen-customer-code-input
	${ENROLLMENTKEY}=	Get Value	filled-adornment-password
	${CUSTOMERCODE}=	Set Suite Variable	${CUSTOMERCODE}
	${ENROLLMENTKEY}=	Set Suite Variable	${ENROLLMENTKEY}

SUITE:Enable Device Enrolment and Disable On-Premises Enrollment If Needed
	GUI::ZPECloud::Settings::Open Tab
	Wait Until Page Contains Element	//input[@id='ca-setgen-enable-device-enrollment-checkbox']	timeout=1m
	${IS_CHECKBOX_SELECTED}=	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='ca-setgen-enable-device-enrollment-checkbox']
	Run Keyword If	not ${IS_CHECKBOX_SELECTED}	SUITE:Select cloud enable checkbox and save
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	(//button[normalize-space()='On-Premises'])[1]	timeout=1m
	Click Element	(//button[normalize-space()='On-Premises'])[1]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	(//button[normalize-space()='On-Premises'])[1]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//input[@id='ca-setop-enable-device-enrollment-checkbox']
	${IS_ON-PREMISES_CHECKBOX_SELECTED}=	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='ca-setop-enable-device-enrollment-checkbox']
	Run Keyword If	${IS_ON-PREMISES_CHECKBOX_SELECTED}	SUITE:Disable On-Premises Enrollment and save

SUITE:Select cloud enable checkbox and save
	Select Checkbox	//input[@id='ca-setgen-enable-device-enrollment-checkbox']
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Enabled	//span[contains(.,'SAVE')]
	Click Element	//span[contains(.,'SAVE')]

SUITE:Disable On-Premises Enrollment and save
	Click Element	//input[@id='ca-setop-enable-device-enrollment-checkbox']
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Enabled	//span[contains(.,'SAVE')]
	Click Element	//span[contains(.,'SAVE')]

SUITE:Device Enrolled Successfully
	[Arguments]	${SERIALNUMBER_DIFF}
	GUI::ZPECloud::Basic::Open and Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Enrolled::Open Tab
	Wait Until Page Contains Element	//button[contains(.,'ENROLLED')]	30s
	Click Element	//button[contains(.,'ENROLLED')]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//td[contains(.,'${SERIALNUMBER_DIFF}')]	timeout=1m
	Wait Until Page Contains Element	//td[contains(.,'${SERIALNUMBER_DIFF}')]/../td/div/div/span[contains(.,'Online')]
	[Teardown]	Close All Browsers

SUITE:Page should contain device's serial number
	[Arguments]	${NG_SERIALNUMBER}
	GUI::ZPECloud::Devices::Open Tab
	Wait Until Page Contains Element	//button[contains(.,'AVAILABLE')]	timeout=30s
	Click Element	//button[contains(.,'AVAILABLE')]
	Wait Until Page Contains Element	//td/div[contains(.,'${NG_SERIALNUMBER}')]	timeout=30s

SUITE:Move the NG from Devices::Available to Devices::Enrolled page
	[Arguments]	${NG_SERIALNUMBER}
	Wait Until Keyword Succeeds	3x	3s	GUI::ZPECloud::Basic::Open and Login	${EMAIL}	${PWD}	${PASSWORD}
	GUI::ZPECloud::Devices::Open Tab
	Wait Until Page Contains Element	//button[contains(.,'AVAILABLE')]
	Click Element	//button[contains(.,'AVAILABLE')]
	Wait Until Keyword Succeeds	2m	3s	SUITE:Page should contain device's serial number	${NG_SERIALNUMBER}
	Wait Until Element Is Visible	//*/tr/td	30s
	Input Text	search-input	${NG_SERIALNUMBER}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	//*/tr/td	30s
	Click Element	//td[contains(.,'${NG_SERIALNUMBER}')]/../td[1]/span
	Wait Until Keyword Succeeds	30s	3s	Checkbox Should Be Selected	(//input[@type='checkbox'])[2]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Enabled	//button[contains(.,'Enroll')]
	Click Element	//button[contains(.,'Enroll')]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Does Not Contain Element	//td[contains(.,'${NG_SERIALNUMBER}')]	timeout=1m