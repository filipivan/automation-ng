*** Settings ***
Resource	../../init.robot
Documentation	Check Sanity and factory settings Host, Peer and Hostshared if the build environment has it.
Metadata	Version	1.0
Metadata	Executed At	${HOST} and ${HOSTPEER}
Metadata	Executed With	${BROWSER}
Force Tags	CLI	GUI	CLOUD	${BROWSER}	CHROME	FIREFOX	FACTORY_SETTINGS	FACTORY_SETTINGS_CLEAR_SYSTEM	DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${JENKINS_VM_USERNAME}	${CLOUD_JENKINS_VM_USERNAME}	#From FRE-QA-ENVIRONMENT.PY
${JENKINS_VM_PASSWORD}	${CLOUD_JENKINS_VM_PASSWORD}	#From FRE-QA-ENVIRONMENT.PY
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}

*** Test Cases ***
Test to check sanity of host's system and factory settings if necessary
	SUITE:Check NG Sanity	${HOST}
	[Teardown]	CLI:Close Connection

Test to check sanity of peer's system
	[Tags]	EXCLUDEIN5_2	EXCLUDEIN_ONPREMISES
	SUITE:Check NG Sanity	${HOSTPEER}
	[Teardown]	CLI:Close Connection

Test to apply Factory Settings that is going to clear host's system
	Wait Until Keyword Succeeds	3x	3s	CLI:Open	session_alias=host_session	HOST_DIFF=${HOST}
	CLI:Apply Factory Settings	Yes
	SUITE:CLI:QA Device First Settings 5.0+ using jenkins	${HOST}	${NGVERSION}	${JENKINS_VM_USERNAME}	${JENKINS_VM_PASSWORD}
	[Teardown]	CLI:Close Connection

Test to apply Factory Settings that is going to clear peer's system
	[Tags]	EXCLUDEIN5_2	EXCLUDEIN_ONPREMISES
	Wait Until Keyword Succeeds	3x	3s	CLI:Open	session_alias=peer_session	HOST_DIFF=${HOSTPEER}
	CLI:Apply Factory Settings	Yes
	SUITE:CLI:QA Device First Settings 5.0+ using jenkins	${HOSTPEER}	${NGPEERVERSION}	${JENKINS_VM_USERNAME}	${JENKINS_VM_PASSWORD}
	[Teardown]	CLI:Close Connection

Test DD Command Right Before Upgrade
	[Tags]	CLI	GUI	CLOUD	${BROWSER}	CHROME	FIREFOX	FACTORY_SETTINGS	FACTORY_SETTINGS_CLEAR_SYSTEM	BUG_NG_10898
	Wait Until Keyword Succeeds	3x	3s	CLI:Open	session_alias=host_session	HOST_DIFF=${HOST}
	${IS_NSC}	CLI:Is Serial Console
	Skip If	${IS_NSC}
	${STATUS}	Evaluate NG Versions: ${NGVERSION} >= 5.6	#Skip to v5.6+ due BUG_NG_10256
	Skip If	${STATUS}
	Wait Until Keyword Succeeds	6x	2s	SUITE:Test DD
	[Teardown]	CLI:Close Connection

Check M2MGMT Service
	Wait Until Keyword Succeeds	3x	3s	CLI:Open	session_alias=host_session	HOST_DIFF=${HOST}
	${IS_NSC}	CLI:Is Serial Console
	${IS_VM}	CLI:Is VM System
	Skip If	${IS_VM} or ${IS_NSC}
	CLI:Connect As Root
	SUITE:Check M2MGMT
	[Teardown]	CLI:Close Connection

Test case to check default configuration on host's Security::Services page
	Wait Until Keyword Succeeds	3x	3s	GUI::Basic::Open And Login Nodegrid	USERNAME=${USERNG}	PASSWORD=${PWDNG}
	GUI::Basic::Security::Services::Open Tab
	${HAS_CLOUD_ENABLED}	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='cloud_enable']
	Run Keyword If	not ${HAS_CLOUD_ENABLED}	GUI::Basic::Select Checkbox	//input[@id='cloud_enable']
	${HAS_CLOUD_REMOTE_ACCESS_ENABLED}	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='remote_access']
	Run Keyword If	not ${HAS_CLOUD_REMOTE_ACCESS_ENABLED}	GUI::Basic::Select Checkbox	//input[@id='remote_access']
	${HAS_CLOUD_FILE_PROTECTION_ENABLED}	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='pass_prot']
	Run Keyword If	${HAS_CLOUD_FILE_PROTECTION_ENABLED}	Unselect Checkbox	//input[@id='pass_prot']
	${HAS_SSH_ALLOW_ROOT_ENABLED}	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='allow']
	Run Keyword If	not ${HAS_SSH_ALLOW_ROOT_ENABLED}	SUITE:Select Checkbox to enable ssh to root and change the password
	${HAS_ALLOW_ROOT_CONSOLE_ENABLED}	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='rootconsole']
	Run Keyword If	not ${HAS_ALLOW_ROOT_CONSOLE_ENABLED}	GUI::Basic::Select Checkbox	//input[@id='rootconsole']
	Checkbox Should Be Selected	//input[@id='cloud_enable']
	Checkbox Should Be Selected	//input[@id='remote_access']
	Checkbox Should Not Be Selected	//input[@id='pass_prot']
	Checkbox Should Be Selected	//input[@id='allow']
	Checkbox Should Be Selected	//input[@id='rootconsole']
	[Teardown]	GUI::Basic::Save If Configuration Changed

Test case to check default configuration on host's Managed Devices::Devices page
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::ManagedDevices::Delete All Devices
	GUI::Basic::Spinner Should Be Invisible
	[Teardown]	Close All Browsers

Test case to check default configuration on peer's Security::Services page
	[Tags]	EXCLUDEIN5_2	EXCLUDEIN_ONPREMISES
	Wait Until Keyword Succeeds	3x	3s	GUI::Basic::Open And Login Nodegrid	USERNAME=${USERNAMENGPEER}	PASSWORD=${PASSWORDNG}	PAGE=${HOMEPAGEPEER}
	GUI::Basic::Security::Services::Open Tab
	${HAS_CLOUD_ENABLED}	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='cloud_enable']
	Run Keyword If	not ${HAS_CLOUD_ENABLED}	GUI::Basic::Select Checkbox	//input[@id='cloud_enable']
	${HAS_CLOUD_REMOTE_ACCESS_ENABLED}	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='remote_access']
	Run Keyword If	not ${HAS_CLOUD_REMOTE_ACCESS_ENABLED}	GUI::Basic::Select Checkbox	//input[@id='remote_access']
	${HAS_CLOUD_FILE_PROTECTION_ENABLED}	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='pass_prot']
	Run Keyword If	${HAS_CLOUD_FILE_PROTECTION_ENABLED}	Unselect Checkbox	//input[@id='pass_prot']
	${HAS_SSH_ALLOW_ROOT_ENABLED}	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='allow']
	Run Keyword If	not ${HAS_SSH_ALLOW_ROOT_ENABLED}	SUITE:Select Checkbox to enable ssh to root and change the password
	${HAS_ALLOW_ROOT_CONSOLE_ENABLED}	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='rootconsole']
	Run Keyword If	not ${HAS_ALLOW_ROOT_CONSOLE_ENABLED}	GUI::Basic::Select Checkbox	//input[@id='rootconsole']
	Checkbox Should Be Selected	//input[@id='cloud_enable']
	Checkbox Should Be Selected	//input[@id='remote_access']
	Checkbox Should Not Be Selected	//input[@id='pass_prot']
	Checkbox Should Be Selected	//input[@id='allow']
	Checkbox Should Be Selected	//input[@id='rootconsole']
	[Teardown]	GUI::Basic::Save If Configuration Changed

Test case to check default configuration on peer's Managed Devices::Devices page
	[Tags]	EXCLUDEIN5_2	EXCLUDEIN_ONPREMISES
	GUI::Basic::Managed Devices::Devices::open tab
	GUI::ManagedDevices::Delete All Devices
	GUI::Basic::Spinner Should Be Invisible
	[Teardown]	Close All Browsers

*** Keywords ***
SUITE:Setup
	${LOGIN_ON_NGPEER}	Set Variable	${FALSE}
	Set Suite Variable	${LOGIN_ON_NGPEER}
	${NG_REACHABLE}	Set Variable	${FALSE}
	${ADMIN_LOGIN_SUCCESS}	Set Variable	${FALSE}
	${ASK_CURRENT_PASSWORD}	Set Variable	${FALSE}
	${HAS_STRONG_PASSWORD}	Set Variable	${FALSE}
	${DEFAULT_USERNAME}	Set Variable	${USERNAMENG}
	${DEFAULT_PASSWORD}	Set Variable	${PASSWORDNG}
	${ROOT_PASSWORD}	Set Variable	${PASSWORDNG}
	${QA_PASSWORD}	Set Variable	${PASSWORDNG}
	Set Global Variable	${DEFAULT_USERNAME}
	Set Global Variable	${DEFAULT_PASSWORD}
	Set Global Variable	${ROOT_PASSWORD}
	Set Global Variable	${QA_PASSWORD}

SUITE:Teardown
	CLI:Close Connection
	Run Keyword If Any Tests Failed	Fatal Error	Clearing system failed

SUITE:Test DD
	Wait Until Keyword Succeeds	3x	3s	CLI:Connect As Root
	Set Client Configuration	timeout=2m
	CLI:Write	rm -rf /var/sw/*	#BUG_NG_12763
#	CLI:Write	rm -rf /var/upgradeBackup/*	#BUG_NG_12763
	${OUTPUT}	CLI:Write	time dd if=/dev/zero of=/var/sw/xyz bs=1024 count=1000000
#	${OUTPUT}	Split To Lines	${OUTPUT}	4	-3
#	${OUTPUT_CONVERTED}	Convert To String	${OUTPUT[0]}
#	${HAS_REAL_TIME}	Run Keyword And Return Status	Should Contain	${OUTPUT_CONVERTED}	real
#	IF	not ${HAS_REAL_TIME}
#		${OUTPUT_CONVERTED}	Convert To String	${OUTPUT[1]}
#		Should Contain	${OUTPUT_CONVERTED}	real
#	END
	Should Match Regexp	${OUTPUT}	.*real\\s+0m[0-9].\\d{3}s
	Should Not Contain	${OUTPUT}	No space left on device
	${SYS_VERSION}	CLI:Write	cli show /system/about software	user=yes
	${TIME}	CLI:Write	date	user=yes
	Write	mkdir /var/DD
	Read Until Prompt
	Write	echo 'Current Date: ${TIME}${SPACE}${SPACE}${SPACE}${SYS_VERSION}${SPACE}${SPACE}${SPACE}time: ${OUTPUT}' >> /var/DD/DD_TIME.log
	CLI:Read Until Prompt
	[Teardown]	Run Keywords	CLI:Write	rm /var/sw/xyz	AND	CLI:Close Current Connection

SUITE:Check M2MGMT
	${OUTPUT}	CLI:Write	ps -ef | grep m2m
	Should Contain	${OUTPUT}	/usr/sbin/m2mgmtd

SUITE:Check NG Sanity
	[Arguments]	${HOSTDIFF}
	${NG_REACHABLE}	SUITE:Check in case NG is not reachable	${HOSTDIFF}
	Run Keyword If	not ${NG_REACHABLE}	Fail	NG not reacheble.
	${ROOT_LOGIN_SUCCESS}	Run Keyword And Return Status	CLI:Open	USERNAME=${ROOT_DEFAULT_USERNAME}	PASSWORD=${DEFAULT_PASSWORD}	HOST_DIFF=${HOSTDIFF}
	Run Keyword If	${ROOT_LOGIN_SUCCESS}	CLI:Write	rm -rf /var/sw/*	#BUG_NG_12763
#	Run Keyword If	${ROOT_LOGIN_SUCCESS}	CLI:Write	rm -rf /var/upgradeBackup/*	#BUG_NG_12763
	${ADMIN_LOGIN_SUCCESS}	Run Keyword And Return Status	CLI:Open	HOST_DIFF=${HOSTDIFF}
	Run Keyword If	${ADMIN_LOGIN_SUCCESS} and ${ROOT_LOGIN_SUCCESS}	Pass Execution	NG (${HOSTDIFF}) sanity is OK.
	${ASK_CURRENT_PASSWORD}	SUITE:Check in case NG has factory settings asking current password	${HOSTDIFF}
	Run Keyword If	${ASK_CURRENT_PASSWORD}	Log	Jenkins build started with NG (${HOSTDIFF}) in factory settings state without come it back to default setup.	WARN
	Run Keyword If	${ASK_CURRENT_PASSWORD}	Pass Execution	NG (${HOSTDIFF}) sanity is OK after setup it from factory settings state to default.
	Run Keyword If	'${ADMIN_LOGIN_SUCCESS}' == '${FALSE}' and '${ASK_CURRENT_PASSWORD}' == '${FALSE}'	Fail	Not able to login on NG and it's not asking to change the password. CHeck if was a network problem or If NG has no default credentials...

SUITE:Check in case NG is not reachable
	[Arguments]	${HOSTDIFF}
	${NG_REACHABLE}	Run Keyword And Return Status	Run Keywords	CLIENT:Start Target Ping	${HOSTDIFF}
	...	AND	Sleep	3s	AND	CLIENT:End Target Ping
	[Return]	${NG_REACHABLE}

SUITE:Check in case NG has factory settings asking current password
	[Arguments]	${HOSTDIFF}
	${ASK_CURRENT_PASSWORD}	Run Keyword And Return Status
	...	SUITE:Check Current password doing ssh to NG by Jenkins VM and close All connections	${HOSTDIFF}
	...	${JENKINS_VM_USERNAME}	${JENKINS_VM_PASSWORD}
	Run Keyword If	${ASK_CURRENT_PASSWORD}
	...	SUITE:Change to default passwords and allow ssh by root doing ssh to NG by Jenkins VM	${HOSTDIFF}
	...	${JENKINS_VM_USERNAME}	${JENKINS_VM_PASSWORD}
	Run Keyword If	${ASK_CURRENT_PASSWORD}	CLI:Open	HOST_DIFF=${HOSTDIFF}
	[Return]	${ASK_CURRENT_PASSWORD}

SUITE:Check in case NG has strong password
	[Arguments]	${HOSTDIFF}
	${HAS_STRONG_PASSWORD}	Run Keyword And Return Status	CLI:Open	HOST_DIFF=${HOSTDIFF}	PASSWORD=${QA_PASSWORD}
	Run Keyword If	${HAS_STRONG_PASSWORD}	SUITE:Change Root And Admin Passwords from admin user and Allow ssh and console access as root	${HOSTDIFF}
	[Return]	${HAS_STRONG_PASSWORD}

SUITE:Check Current password doing ssh to NG by Jenkins VM and close All connections
	[Arguments]	${HOSTDIFF}	${SUITE_JENKINS_VM_USERNAME}	${SUITE_JENKINS_VM_PASSWORD}
	${HAS_CURRENT_PASSWORD}	SUITE:Should Contain Current password doing ssh to NG by Jenkins VM	${HOSTDIFF}	${SUITE_JENKINS_VM_USERNAME}	${SUITE_JENKINS_VM_PASSWORD}
	CLI:Close Connection
	[Return]	${HAS_CURRENT_PASSWORD}

SUITE:Should Contain Current password doing ssh to NG by Jenkins VM
	[Arguments]	${HOSTDIFF}	${SUITE_JENKINS_VM_USERNAME}	${SUITE_JENKINS_VM_PASSWORD}
	${OS}=	CLIENT:Get Operating System
	Set Suite Variable	${OS}
	Log	Client OS:\n ${OS}	INFO	console=yes
	IF	'Microsoft' in '''${OS}'''
		CLI:Open	HOST_DIFF=${CLIENTIP}	USERNAME=${SUITE_JENKINS_VM_USERNAME}	PASSWORD=${SUITE_JENKINS_VM_PASSWORD}	TYPE=windows
		Write	ssh-keygen -f "%home%\\.ssh\\known_hosts" -R "${HOST_DIFF}"
	ELSE IF	'Linux' in '''${OS}'''
		CLI:Open	HOST_DIFF=${CLIENTIP}	USERNAME=${SUITE_JENKINS_VM_USERNAME}	PASSWORD=${SUITE_JENKINS_VM_PASSWORD}	TYPE=shell
		CLI:Write	ssh-keygen -f "/home/${SUITE_JENKINS_VM_USERNAME}/.ssh/known_hosts" -R "${HOST_DIFF}"
	END
	Write	ssh ${DEFAULT_USERNAME}@${HOST_DIFF}
	${OUTPUT}=	CLI:Read Until Regexp	(Password:|Are you sure you want to continue connecting \\(.*\\)\\?)
	Should Not Contain Any	${OUTPUT}	No route to host	Connection refused
	${HAS_TO_CONFIRM}=	Run Keyword And Return Status	Should Match Regexp	${OUTPUT}	Are you sure you want to continue connecting \\(.*\\)\\?
	IF	${HAS_TO_CONFIRM}
		Write	yes
		${OUTPUT}=	Read Until	Password:
		Log To Console	\n${OUTPUT}\n
	END
	Write	${FACTORY_PASSWORD}
	${OUTPUT}=	Read Until	assword:
	Log To Console	\n${OUTPUT}\n
	Should Contain	${OUTPUT}	You are required to change your password immediately (administrator enforced)
	Should Contain	${OUTPUT}	Changing password for ${DEFAULT_USERNAME}.
	${HAS_CURRENT_PASSWORD}	Run Keyword And Return Status	Should Contain	${OUTPUT}	Current password:
	[Return]	${HAS_CURRENT_PASSWORD}

SUITE:Change to default passwords and allow ssh by root doing ssh to NG by Jenkins VM
	[Arguments]	${HOSTDIFF}	${SUITE_JENKINS_VM_USERNAME}	${SUITE_JENKINS_VM_PASSWORD}
	SUITE:Should Contain Current password doing ssh to NG by Jenkins VM	${HOSTDIFF}	${SUITE_JENKINS_VM_USERNAME}	${SUITE_JENKINS_VM_PASSWORD}
	Write	${FACTORY_PASSWORD}
	${OUTPUT}=	Read Until	New password:
	Log To Console	\n${OUTPUT}\n
	Write	${QA_PASSWORD}
	${OUTPUT}=	Read Until	Retype new password:
	Log To Console	\n${OUTPUT}\n
	Write	${QA_PASSWORD}
	${OUTPUT}=	Read Until	]#
	${WEBSERVER_ERROR}=	Run Keyword And Return Status
	...	Should Not Contain	${OUTPUT}	Failed to connect to WEBSERVER
	Run Keyword If	${WEBSERVER_ERROR}	Write	exit
	SUITE:Change Root And Admin Passwords from admin user and Allow ssh and console access as root	${HOSTDIFF}

SUITE:Change Root And Admin Passwords from admin user and Allow ssh and console access as root
	[Arguments]	${HOSTDIFF}
	CLI:Change Root And Admin Passwords from admin user	HOST_DIFF=${HOSTDIFF}
	SUITE:Allow ssh and console access as root	${HOSTDIFF}

SUITE:Allow ssh and console access as root
	[Arguments]	${HOSTDIFF}
	CLI:Open	HOST_DIFF=${HOST_DIFF}
	CLI:Enable Ssh And Console Access As Root
	CLI:Close Connection

SUITE:CLI:QA Device First Settings 5.0+ using jenkins
	[Arguments]	${HOST_DIFF}	${VERSION}	${SUITE_JENKINS_VM_USERNAME}	${SUITE_JENKINS_VM_PASSWORD}
	SUITE:Change Password At Login Time using jenkins	HOST_DIFF=${HOST_DIFF}
	...	SUITE_JENKINS_VM_USERNAME=${SUITE_JENKINS_VM_USERNAME}	SUITE_JENKINS_VM_PASSWORD=${SUITE_JENKINS_VM_PASSWORD}
	CLI:Wait Nodegrid Webserver To Be Up	${HOST_DIFF}	${QA_PASSWORD}	4m
	CLI:Open	${DEFAULT_USERNAME}	${QA_PASSWORD}	HOST_DIFF=${HOST_DIFF}	session_alias=factory_session_admin
	CLI:Enable Ssh And Console Access As Root
	CLI:Close Connection	Yes
	SUITE:Change Password At Login Time using jenkins	root	root	prompt=~#	HOST_DIFF=${HOST_DIFF}
	...	SUITE_JENKINS_VM_USERNAME=${SUITE_JENKINS_VM_USERNAME}	SUITE_JENKINS_VM_PASSWORD=${SUITE_JENKINS_VM_PASSWORD}
	CLI:Change Root And Admin Password To QA Default	HOST_DIFF=${HOST_DIFF}

SUITE:Change Password At Login Time using jenkins
	[Arguments]	${USERNAME}=${DEFAULT_USERNAME}	${CURR_PASSWORD}=${FACTORY_PASSWORD}
	...	${NEW_PASSWORD}=${QA_PASSWORD}	${prompt}=]#	${HOST_DIFF}=${HOST}
	...	${CONNECT_TIMEOUT}=2m	${SUITE_JENKINS_VM_USERNAME}=${JENKINS_VM_USERNAME}	${SUITE_JENKINS_VM_PASSWORD}=${JENKINS_VM_PASSWORD}

	${OS}=	CLIENT:Get Operating System
	Set Suite Variable	${OS}
	Log	Client OS:\n ${OS}	INFO	console=yes
	IF	'Microsoft' in '''${OS}'''
		CLI:Open	HOST_DIFF=${CLIENTIP}	USERNAME=${SUITE_JENKINS_VM_USERNAME}	PASSWORD=${SUITE_JENKINS_VM_PASSWORD}	TYPE=windows
		Write	ssh-keygen -f "%home%\\.ssh\\known_hosts" -R "${HOST_DIFF}"
	ELSE IF	'Linux' in '''${OS}'''
		CLI:Open	HOST_DIFF=${CLIENTIP}	USERNAME=${SUITE_JENKINS_VM_USERNAME}	PASSWORD=${SUITE_JENKINS_VM_PASSWORD}	TYPE=shell
		CLI:Write	ssh-keygen -f "/home/${SUITE_JENKINS_VM_USERNAME}/.ssh/known_hosts" -R "${HOST_DIFF}"
	END

	${OUTPUT}=	Wait Until Keyword Succeeds	${CONNECT_TIMEOUT}	10s
	...	CLI:Open SSH Connection From Peer	${USERNAME}	${HOST_DIFF}

	${HAS_TO_CONFIRM}=	Run Keyword And Return Status
	...	Should Match Regexp	${OUTPUT}	Are you sure you want to continue connecting \\(.*\\)\\?
	Run Keyword If	${HAS_TO_CONFIRM}	Write	yes
	${OUTPUT}=	Run Keyword If	${HAS_TO_CONFIRM}
	...	Read Until	Password:
	Run Keyword If	${HAS_TO_CONFIRM}	Log To Console	\n${OUTPUT}\n

	Write	${CURR_PASSWORD}
	${OUTPUT}=	Read Until	Current password:
	Log To Console	\n${OUTPUT}\n
	Should Contain	${OUTPUT}	You are required to change your password immediately (administrator enforced)
	Should Contain	${OUTPUT}	Changing password for ${USERNAME}.

	Write	${CURR_PASSWORD}
	${OUTPUT}=	Read Until	New password:
	Log To Console	\n${OUTPUT}\n

	Write	${NEW_PASSWORD}
	${OUTPUT}=	Read Until	Retype new password:
	Log To Console	\n${OUTPUT}\n

	Write	${NEW_PASSWORD}
	${OUTPUT}=	CLI:Read Until Regexp	(~#|${prompt}|>|~$)
	${WEBSERVER_CONNECTED}=	Run Keyword And Return Status
	...	Should Not Contain	${OUTPUT}	Error: Failed to connect to WEBSERVER
	Run Keyword If	${WEBSERVER_CONNECTED}	CLI:Write	exit
	CLI:Close Connection	Yes

SUITE:Select Checkbox to enable ssh to root and change the password
	GUI::Basic::Select Checkbox	//input[@id='allow']
	CLI:Change Root And Admin Passwords from admin user