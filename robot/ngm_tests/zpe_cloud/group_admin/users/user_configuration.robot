*** Settings ***
Documentation	Testing Users Configuration
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	USER_CONFIGURATION	USERS
Force Tags	GUI	ZPECLOUD	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Library	XML

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}		${PASSWORD}

*** Test Cases ***
Change user name
	[Tags]	Name
	GUI::ZPECloud::Users::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Changing user name	ZPECloud
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Changing user name	${PREVIOUS_NAME}
	SUITE:Teardown

Change phone number
	[Tags]	Phone
	SUITE:Setup
	GUI::ZPECloud::Users::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Changing phone number	11111111119
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Changing phone number	${PREVIOUS_PHONE}
	SUITE:Teardown

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Teardown
	Close All Browsers

SUITE:Changing user name
	[Arguments]	${DESIRED_NAME}
	Click Element	xpath=//div[contains(.,'${EMAIL}')]/../../td[1]
	Click Button	locator=function-button-1
	Sleep	5
	${PREVIOUS_NAME}	Get Value	ca-usergrp-form-field-first_name
	Set Suite Variable	${PREVIOUS_NAME}
	Clear Element Text	ca-usergrp-form-field-first_name
	Press Keys	ca-usergrp-form-field-first_name	CTRL+a+BACKSPACE
	Input Text	locator=ca-usergrp-form-field-first_name	text=${DESIRED_NAME}
	Click Button	locator=submit-btn
	Sleep	3
	Element Should Be Visible	//td[@class='MuiTableCell-root MuiTableCell-body MuiTableCell-alignLeft MuiTableCell-sizeMedium css-1ntgwyd'][contains(.,'${DESIRED_NAME}')]

SUITE:Changing phone number
	[Arguments]	${DESIRED_PHONE}
	${PREVIOUS_PHONE}	Get Text	xpath=//div[contains(.,'${EMAIL}')]/../../td[6]
	Set Suite Variable	${PREVIOUS_PHONE}
	Click Element	xpath=//div[contains(.,'${EMAIL}')]/../../td[1]
	Click Button	locator=function-button-1
	Sleep	5
	Clear Element Text	//input[contains(@placeholder,'1 (702) 123-4567')]
	Press Keys	//input[contains(@placeholder,'1 (702) 123-4567')]	CTRL+a+BACKSPACE
	Input Text	locator=//input[contains(@placeholder,'1 (702) 123-4567')]	text=${DESIRED_PHONE}
	Click Button	locator=submit-btn
	Sleep	3
	Element Should Be Visible	(//td[@class='MuiTableCell-root MuiTableCell-body MuiTableCell-alignLeft MuiTableCell-sizeMedium css-1ntgwyd'][contains(.,'${DESIRED_PHONE}')])

