*** Settings ***
Documentation	Tests to check----> Allow company to configure custom email service
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	SETTINGS	NOTIFICATION
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${SMTP_HOST}	${SMTP_SERVER}
${SMTP_SENDER}	"testing" <alert@zpecloud.com>
${SMTP_TIMEOUT}	30
${TEST_SUCCESS}	Test email sent successfully, check your email inbox
${TEST_MAIL}	zpemanualtest@gmail.com
${GMAIL_SEARCH_URL}	https://accounts.google.com/AddSession/identifier?service=accountsettings&continue=https%3A%2F%2Fmyaccount.google.com%2F%3Futm_source%3Dsign_in_no_continue&ec=GAlAwAE&flowName=GlifWebSignIn&flowEntry=AddSession
${email_username}	zpemanualtest@gmail.com
${email_password}	.ZPE5ystems!2020

*** Test Cases ***
Test to check Email server configuration--->SMTP
	[Tags]	GUI	ZPECLOUD	NON-CRITICAL	${BROWSER}	NON-DEVICE
#	SUITE:Setup
	GUI::ZPECloud::Settings::Notification::Open Tab
	Click Element	//*[@id="customer-admin-layout"]/main/div[1]/div/div/div/button[2]
	Click Element	xpath=//body
	Wait Until Element Is Visible	//*[@id="mail-option"]
	Click Element	//*[@id="mail-option"]
	Wait Until Element Is Visible	xpath=//li[@id='mail-smtp']
	Click Element	xpath=//li[@id='mail-smtp']
	Press Keys	id=smtp-host	CTRL+a+BACKSPACE
	Clear Element Text	id=smtp-host
	Input Text	id=smtp-host	${SMTP_HOST}
	Press Keys	id=smtp-port	CTRL+a+BACKSPACE
	Clear Element Text	id=smtp-port
	Input Text	id=smtp-port	${SMTP_PORT}
	Press Keys	id=smtp-user	CTRL+a+BACKSPACE
	Clear Element Text	id=smtp-user
	Input Text	id=smtp-user	${SMTP_USER}
	Press Keys	id=smtp-password	CTRL+a+BACKSPACE
	Clear Element Text	id=smtp-password
	Input Text	id=smtp-password	${SMTP_USER_PASSWORD}
	Press Keys	id=smtp-sender	CTRL+a+BACKSPACE
	Clear Element Text	id=smtp-sender
	Input Text	id=smtp-sender	${SMTP_SENDER}
	Press Keys	id=smtp-timeout	CTRL+a+BACKSPACE
	Clear Element Text	id=smtp-timeout
	Input Text	id=smtp-timeout	${SMTP_TIMEOUT}
	sleep	5s
	Execute Javascript	window.document.getElementById("smtp-test-email").scrollIntoView(true);
	${CHECK}=	run keyword and return status	Checkbox Should Be Selected	//*[@id="smtp-tls"]
	Run Keyword If	${CHECK} == False	Click Element	//*[@id="smtp-tls"]
	Wait Until Element Is Visible	xpath=//button[@id='smtp-test-email']	30s
	Click Element	xpath=//button[@id='smtp-test-email']
	Input Text	id=test-mail	${TEST_MAIL}
	Click Element	xpath=//button[contains(.,'SEND')]
	Wait Until Page Contains	${TEST_SUCCESS}	timeout=1m
	Open Browser	${GMAIL_SEARCH_URL}	${BROWSER}
	Wait Until Element Is Visible	id=identifierId
	Input Text	id=identifierId	${email_username}
	Wait Until Element Is Visible	xpath=//span[contains(.,'Next')]
	Press Keys	identifierNext	Enter
#	Click Element	xpath=//div[@id='identifierNext']/div/button/div[2]
#	sleep	2
	Wait Until Element Is Visible	css=#password .whsOnd	30s
	Input Text	css=#password .whsOnd	${email_password}
	Wait Until Element Is Visible	xpath=//span[contains(.,'Next')]
#	Click Element	xpath=//div[@id='passwordNext']/div/button/div[2]
	Press Keys	passwordNext	Enter
	sleep	10s
	${status}=	Run Keyword and Return Status	Select Frame	//iframe
	${status}=	Run Keyword and Return Status	Wait Until Element Is Visible	//*[@id="yDmH0d"]/c-wiz/div/div/c-wiz/div/div/ul[1]/li[7]
	Run Keyword If	${status} == False	Click Element	//*[@id="gbwa"]
	Run Keyword If	${status} == False	Select Frame	//iframe
	Click Element	//*[@id="yDmH0d"]/c-wiz/div/div/c-wiz/div/div/ul[1]/li[7]
	sleep	2s
	Switch Window	NEW
	sleep	5s
	Wait Until Element Is Visible	xpath=(//span[@name='testing'])[2]
	Wait Until Page Contains Element	xpath=(//span[@name='testing'])[2]
	SUITE:Teardown

Test to check Email server configuration--->ZPE Default Server
	SUITE:Setup
	GUI::ZPECloud::Settings::Notification::Open Tab
	Click Element	//*[@id="customer-admin-layout"]/main/div[1]/div/div/div/button[2]
	Click Element	xpath=//body
	Wait Until Element Is Visible	//*[@id="mail-option"]
	Click Element	//*[@id="mail-option"]
	Wait Until Element Is Visible	xpath=//li[@id='mail-default']
	Click Element	xpath=//li[@id='mail-default']
	Wait Until Element Is Visible	xpath=//button[contains(.,'SAVE')]
	Click Element	xpath=//button[contains(.,'SAVE')]
	Wait Until Element Is Visible	xpath=//span[contains(.,'Settings saved successfully')]
	SUITE:Teardown

Test to check Email server configuration with wrong information--->SMTP
	SUITE:Setup
	GUI::ZPECloud::Settings::Notification::Open Tab
	Click Element	//*[@id="customer-admin-layout"]/main/div[1]/div/div/div/button[2]
	Click Element	xpath=//body
	Wait Until Element Is Visible	//*[@id="mail-option"]
	Click Element	//*[@id="mail-option"]
	Wait Until Element Is Visible	xpath=//li[@id='mail-smtp']
	Click Element	xpath=//li[@id='mail-smtp']
	Press Keys	id=smtp-host	CTRL+a+BACKSPACE
	Clear Element Text	id=smtp-host
	Input Text	id=smtp-host	${SMTP_HOST}
	Press Keys	id=smtp-port	CTRL+a+BACKSPACE
	Clear Element Text	id=smtp-port
	Input Text	id=smtp-port	${SMTP_PORT}
	Press Keys	id=smtp-user	CTRL+a+BACKSPACE
	Clear Element Text	id=smtp-user
	Input Text	id=smtp-user	${wrong_SMTP_USER}
	Press Keys	id=smtp-password	CTRL+a+BACKSPACE
	Clear Element Text	id=smtp-password
	Input Text	id=smtp-password	${wrong_SMTP_USER_PASSWORD}
	Press Keys	id=smtp-sender	CTRL+a+BACKSPACE
	Clear Element Text	id=smtp-sender
	Input Text	id=smtp-sender	${SMTP_SENDER}
	Press Keys	id=smtp-timeout	CTRL+a+BACKSPACE
	Clear Element Text	id=smtp-timeout
	Input Text	id=smtp-timeout	${SMTP_TIMEOUT}
	sleep	2s
	Execute Javascript	window.document.getElementById("smtp-test-email").scrollIntoView(true);
	${CHECK}=	run keyword and return status	Checkbox Should Be Selected	//*[@id="smtp-tls"]
	Run Keyword If	${CHECK} == False	Click Element	//*[@id="smtp-tls"]
	Wait Until Element Is Visible	xpath=//button[@id='smtp-test-email']	30s
	Click Element	xpath=//button[@id='smtp-test-email']
	Input Text	id=test-mail	${TEST_MAIL}
	Click Element	xpath=//button[contains(.,'SEND')]
	Wait Until Element Is Visible	xpath=//span[contains(.,'Error sending email, please check the settings provided')]	timeout=30s

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
#	Maximize Browser Window

SUITE:Teardown
	Close All Browsers