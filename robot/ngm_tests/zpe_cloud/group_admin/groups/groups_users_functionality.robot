*** Settings ***
Documentation	Testing Users Configuration in Groups
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	GROUPS
Force Tags	GUI	ZPECLOUD	LOGIN	${BROWSER}	NON-CRITICAL	DEVICE	PART_3

Suite Setup	SUITE:Setup
Suite Teardown	Close All Browsers

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${GROUP_NAME}	ZPE_GROUP
${CREATE_GROUP}	Group added successfully
${DELETE_GROUP}	Group deleted successfully
${ADD_USER}	Users added to groups successfully
${DELETE_USER}	Users removed from group successfully

*** Test Cases ***
Add user to group 
	${HAVE_GROUP}=	Run Keyword And return Status	SUITE:Find Group
	Run Keyword If	${HAVE_GROUP} == True	SUITE:Delete Group
	SUITE:Create Group
	SUITE:Create the User
	SUITE:Teardown

Remove user from Group
	SUITE:Setup
	${HAVE_GROUP}=	Run Keyword And return Status	SUITE:Find Group
	Run Keyword If	${HAVE_GROUP} == False	SUITE:Create Group
	SUITE:Verify if User Exists
	GUI::ZPECloud::Groups::Users::Open Tab
	Sleep	5
	Click Element	//*[@id="mui-component-select-selectedGroup"]
	Wait Until Page Contains Element	xpath=//li[contains(.,'${GROUP_NAME}')]	15s
	Click Element	xpath=//li[contains(.,'${GROUP_NAME}')]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="ca-groups-user-table-1"]/div/div/table/tbody/tr[1]/td[1]/span	timeout=30s
	Click Element	//*[@id="ca-groups-user-table-1"]/div/div/table/tbody/tr[1]/td[1]/span
	Click Element	//*[@id="function-button-1"]
	Wait Until Page Contains	${DELETE_USER}
	SUITE:Setup
#	GUI::ZPECloud::Groups::Open Tab
	SUITE:Delete Group

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Teardown
	Close All Browsers

SUITE:Create Group
	Reload Page
	sleep	5s
#	Wait Until Page Contains Element	//*[@id="function-button-0"]
	Click Element	//*[@id="function-button-0"]
	Sleep	5
	Input Text	xpath=//input[@name='name']	${GROUP_NAME}
	Click Element	//*[@id="ca-group-permission-management-radio"]/label[1]/span[1]/input
	Click Element	submit-btn
	Wait Until Page Contains	${CREATE_GROUP}	30s
	Reload Page
	sleep	5s
	GUI::ZPECloud::Groups::Open Tab
	Wait Until Page Contains Element	search-input	30s
	Input Text	search-input	${GROUP_NAME}
	Wait Until Page Contains Element	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr/td[2]/div
	Element Should Contain	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr/td[2]/div	${GROUP_NAME}

SUITE:Delete Group
	SUITE:Find Group
	Click Element	//*[@id="function-button-2"]
	Wait Until Page Contains Element	ca-groups-general-alert-dialog
	Click Element	confirm-btn
	Wait Until Page Contains	${DELETE_GROUP}	30s
	Reload Page
	Wait Until Page Contains Element	search-input	30s
	Input Text	search-input	${GROUP_NAME}
	Page Should Not Contain Element	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr/td[2]/div

SUITE:Find Group
	GUI::ZPECloud::Groups::Open Tab
	Reload Page
	sleep	5s
	Wait Until Page Contains Element	search-input	30s
	Input Text	search-input	${GROUP_NAME}
	Wait Until Page Contains Element	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input

SUITE:Create the User
	GUI::ZPECloud::Groups::Users::Open Tab
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${EMAIL}
	Wait Until Page Contains Element	//*[@id="ca-groups-user-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="ca-groups-user-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="function-button-0"]
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${GROUP_NAME}
	Wait Until Page Contains Element	//*[@id="ca-group-selection-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="ca-group-selection-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="function-button-1"]
	Wait Until Page Contains	${ADD_USER}

SUITE:Verify if User Exists
	GUI::ZPECloud::Groups::Users::Open Tab
	Sleep	5
	Click Element	//*[@id="mui-component-select-selectedGroup"]
	Wait Until Page Contains Element	xpath=//li[contains(.,'${GROUP_NAME}')]	15s
	Click Element	xpath=//li[contains(.,'${GROUP_NAME}')]
	${HAVE_USER}=	Run Keyword And return Status	Wait Until Page Contains Element	//*[@id="ca-groups-user-table-1"]/div/div/table/tbody/tr[1]/td[1]/span
	Run Keyword If	${HAVE_USER} == False	SUITE:Create the User