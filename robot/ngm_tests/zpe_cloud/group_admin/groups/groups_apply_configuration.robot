*** Settings ***
Documentation	Testing Configuration Apply in Groups
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	APPLY_CONFIGURATION	GROUPS
Force Tags	GUI	ZPECLOUD	LOGIN	${BROWSER}	NON-CRITICAL	DEVICE	PART_3

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}		${PASSWORD}
${GROUP_NAME}	ZPE_GROUP
${CREATE_GROUP}	Group added successfully
${CONFIG_NAME}	test configuration
${CONFIG_DESCRIPTION}	this code changes the hostname to be test
${CONFIG_PATH}	${CURDIR}/Groups_Apply_configuration_files/hostname.txt
${CONFIRM_CONFIG}	Apply configuration to selected groups was requested
${NEWHOSTNAME}	test
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${HOSTNAME}	nodegrid
${ADD_DEVICE}	Device Added to Group
${DELETE_GROUP}	Group deleted successfully
${DELETE_SCHEDULE}	Delete Schedule
${DELETED_SCHEDULE_MESSAGE}	Schedules deleted successfully

*** Test Cases ***
Apply configuration in group
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${CONFIG_NAME}
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${CONFIG_NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete Configuration
	GUI::ZPECloud::Profiles::Open Tab
	GUI::ZPECloud::Profiles::Configuration::Add	${CONFIG_NAME}	${CONFIG_DESCRIPTION}	${CONFIG_PATH}
	GUI::ZPECloud::Groups::Open Tab
	SUITE:Find Group
	Click Element	function-button-4
	Wait Until Page Contains Element	ca-profiles-config-search
	Input Text	//*[@id="ca-profiles-config-search"]/div/div/input	${CONFIG_NAME}
	Wait Until Page Does Not Contain Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	timeout=30s
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	timeout=30s
	Click Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Click Element	//button[contains(text(), 'Save')]
	Wait Until Page Contains	${CONFIRM_CONFIG} 
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Jobs::Check if job has been started	${EMAIL}	${PWD}	CONF
	GUI::ZPECloud::Profiles::Operation::Check if job is Successful	${EMAIL}	${PWD}	CONF
	# SUITE:Verify Operation Status
	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]
	Wait Until Keyword Succeeds	160	20s	SUITE:Hostname is 'Test'
	[Teardown]	SUITE:Teardown1

Apply configuration in group - Schedule
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Groups::Find Group and Select it	${GROUP_NAME}
	Click Element	function-button-4
	Wait Until Page Contains Element	ca-profiles-config-search
	Input Text	//*[@id="ca-profiles-config-search"]/div/div/input	${CONFIG_NAME}
	Wait Until Page Does Not Contain Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	timeout=30s
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	timeout=30s
	Click Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	SUITE:Schedule Script
	Click Element	//button[contains(text(), 'Save')]
	Wait Until Page Contains	${CONFIRM_CONFIG} 
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Jobs::Check if job has been started	${EMAIL}	${PWD}	CONF
	GUI::ZPECloud::Profiles::Operation::Check if job is Successful	${EMAIL}	${PWD}	CONF
	# SUITE:Verify Operation Status
	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]
	Wait Until Keyword Succeeds	160	20s	SUITE:Hostname is 'Test'
	[Teardown]	SUITE:Teardown1

Apply configuration in group - Recurrent
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Groups::Find Group and Select it	${GROUP_NAME}
	Click Element	function-button-4
	Wait Until Page Contains Element	ca-profiles-config-search
	Input Text	//*[@id="ca-profiles-config-search"]/div/div/input	${CONFIG_NAME}
	Wait Until Page Does Not Contain Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	timeout=30s
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	timeout=30s
	Click Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	SUITE:Make Profile Recurrent
	Click Element	//button[contains(text(), 'Save')]
	Wait Until Page Contains	${CONFIRM_CONFIG}
	sleep	120s
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Jobs::Check if job has been started	${EMAIL}	${PWD}	CONF
	GUI::ZPECloud::Profiles::Operation::Check if job is Successful	${EMAIL}	${PWD}	CONF
	# SUITE:Verify Operation Status
	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]
	Wait Until Keyword Succeeds	160	20s	SUITE:Hostname is 'Test'
	SUITE:Delete Recurrent

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Security::Services::Disable file protection on NG device if needed	${USERNG}	${PWDNG}
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	GUI::ZPECloud::Groups::Open Tab
	${HAS_GROUP}=	Run Keyword And return Status	GUI::ZPECloud::Groups::Find Group and Select it	${GROUP_NAME}
	Run Keyword If	${HAS_GROUP}	GUI::ZPECloud::Groups::Delete Group	${GROUP_NAME}
	GUI::ZPECloud::Groups::Create Group	${GROUP_NAME}
	GUI::ZPECloud::Groups::Devices::Add device to group	${GROUP_NAME}	${SERIALNUMBER}
	GUI::ZPECloud::Groups::Go to backup page on groups	${GROUP_NAME}

SUITE:Teardown1
	Close All Browsers
	
SUITE:Teardown
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	Sleep	5
	${ELEMENT}=	Run Keyword And return Status	Page Should Contain Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Run Keyword If	${ELEMENT} == True	SUITE:Delete Configuration
	Wait Until Keyword Succeeds	30	2	SUITE:Set hostname as 'nodegrid'
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]
	Wait Until Keyword Succeeds	160	2	SUITE:Hostname is 'Nodegrid'
	GUI::ZPECloud::Groups::Delete group If needed	${GROUP_NAME}
	Close All Browsers

SUITE:Find Group
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Groups::Open Tab
	Wait Until Page Contains Element	search-input	30s
	Input Text	search-input	${GROUP_NAME}
	Wait Until Page Contains Element	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input

SUITE:Status is 'Successful'
	Sleep	10
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[9]
	${CONTAINTEST}=	Run Keyword And return Status	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[9]	Successful
	Run Keyword If	${CONTAINTEST} == False	Reload Page
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[9]
	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[9]	Successful

SUITE:Verify Operation Status
	sleep	10s
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Element Contains	(//tr)[2]	${CONFIG_NAME}
	Wait Until Keyword Succeeds	5 minutes	2	SUITE:Status is 'Successful'

SUITE:Hostname is 'Nodegrid'
	Reload Page
	Sleep	5
	${CONTAINTEST}=	Run Keyword And return Status	Page Should Contain	${HOSTNAME} 
	Run Keyword If	${CONTAINTEST} == False	Reload Page
	Sleep	10
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	${HOSTNAME} 

SUITE:Hostname is 'Test'
	Reload Page
	Sleep	10
	${CONTAINTEST}=	Run Keyword And return Status	Page Should Contain	${NEWHOSTNAME}
	Run Keyword If	${CONTAINTEST} == False	Reload Page
	Sleep	5
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	${NEWHOSTNAME}

SUITE:Set hostname as 'nodegrid'
	CLI:Open	${USERNAMENG}	${PASSWORDNG}
	CLI:Enter Path	/settings/network_settings/
	CLI:Set Field	hostname	${HOSTNAME} 
	CLI:Commit
	CLI:Close Connection

SUITE:Delete Configuration
	GUI::ZPECloud::Profiles::Open Tab
	Reload Page
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${CONFIG_NAME}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	20s
	Select Checkbox	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Wait Until Element Is Enabled	//*[@id="function-button-5"]
	Click Element	//*[@id="function-button-5"]
	Sleep	2
	Page Should Contain Element	//*[@id="ca-profiles-config-delete-dialog"]/div[3]/div
	Click Element	//*[@id="confirm-btn"]
	Sleep	2
	Page Should Not Contain Element	//*[@id="ca-profiles-config-delete-dialog"]

SUITE:Create Group
	Reload Page
	sleep	5s
	Wait Until Page Contains Element	function-button-0
	Click Element	function-button-0
	Sleep	5
	Input Text	xpath=//input[@name='name']	${GROUP_NAME}
	Click Element	//*[@id="ca-group-permission-management-radio"]/label[1]/span[1]/input
	Click Element	//button[contains(text(), 'Save')]
	Wait Until Page Contains	${CREATE_GROUP}	30s
	Reload Page
	sleep	5s
	Wait Until Page Contains Element	search-input	30s
	Input Text	search-input	${GROUP_NAME}
	Wait Until Page Contains Element	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr/td[2]/div
	Element Should Contain	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr/td[2]/div	${GROUP_NAME}

SUITE:Add device to group
	GUI::ZPECloud::Groups::Devices::Open Tab
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${SERIALNUMBER}
	sleep	5s
	Wait Until Page Contains Element	//*[@id="ca-group-devices-wrapper"]/div[2]/div/table/tbody/tr/td[1]/span/input
	Click Element	//*[@id="ca-group-devices-wrapper"]/div[2]/div/table/tbody/tr/td[1]/span/input
	Click Element	function-button-0
	sleep	2s
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${GROUP_NAME}
	Wait Until Page Contains Element	//*[@id="ca-group-devices-addtogrp-wrapper"]/div[3]/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="ca-group-devices-addtogrp-wrapper"]/div[3]/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	function-button-1
	Wait Until Page Contains	${ADD_DEVICE}

SUITE:Delete Group
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Groups::Open Tab
	SUITE:Find Group
	Click Element	function-button-2
	Wait Until Page Contains Element	ca-groups-general-alert-dialog
	Click Element	confirm-btn
	Wait Until Page Contains	${DELETE_GROUP}
	Reload Page
	sleep	5s
	Wait Until Page Contains Element	search-input	30s
	Input Text	search-input	${GROUP_NAME}
	Page Should Not Contain Element	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr/td[2]/div

SUITE:Schedule Script
	Click Element	//*[@id="opt-schedule"]
	Execute Javascript	window.document.getElementById("recurrent-checkbox").scrollIntoView(true);
	${DATE}=	Get Current Date
	${DATE}=	Convert Date	${DATE}	result_format=timestamp
	${DATE}=	Add Time To Date	${DATE}	00:01:30.000
	${DATE}=	Fetch From Right	${DATE}	${SPACE}
	${DATE}=	Fetch From Left	${DATE}	.
	${firstDATE}	${restDATE}	${lastDATE}=	Split String From Right	${DATE}	:
	${DATE}=	Catenate	${firstDATE}:${restDATE}
	Click Element	schedule-datepicker
	${CLOUDDATE}=	Get Value	xpath=//div[@id='ui-datepicker-selector']/div/div/input
	${CLOUDDATE}=	Fetch From Left	${CLOUDDATE}	:
	${firstCLOUDDATE}	${restCLOUDDATE}	${lastCLOUDDATE}	${HOUR}=	Split String From Right	${CLOUDDATE}	${SPACE}
	${CLOUDDATE}=	Catenate	${firstCLOUDDATE} ${restCLOUDDATE} ${lastCLOUDDATE}
	${NEWDATE}=	Catenate	${CLOUDDATE} ${DATE}
	Press Keys	css=#schedule-datepicker	CTRL+a+BACKSPACE
	Clear Element Text	css=#schedule-datepicker
	sleep	2s
	Input Text	css=#schedule-datepicker	${NEWDATE}

SUITE:Make Profile Recurrent
	Click Element	//*[@id="opt-schedule"]
	Execute Javascript	window.document.getElementById("recurrent-checkbox").scrollIntoView(true);
	Wait Until Page Contains Element	//*[@id="recurrent-checkbox"]	15s
	Click Element	recurrent-checkbox
	Execute Javascript	window.document.getElementById("mui-component-select-recurrent-type").scrollIntoView(true);
	Click Element	//*[@id="mui-component-select-recurrent-type"]
	Click Element	xpath=//li[contains(.,'Hourly')]
	Sleep	15
	${DATE}=	Get Current Date
	${DATE}=	Add Time To Date	${DATE}	1 day
	${DATE}=	Convert Date	${DATE}	result_format=%d
	Click Element	expiry-date
	Input Text	expiry-date	${DATE}

SUITE:Delete Recurrent
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Open Tab
	GUI::ZPECloud::Schedules::Open Tab
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr/td[2]/div	60s
	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr/td[2]/div	${CONFIG_NAME}
	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr/td[4]/div	${SERIALNUMBER}
	Click Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Click Element	//*[@id="function-button-1"]
	Wait Until Page Contains	${DELETE_SCHEDULE}	60s
	Click Element	//*[@id="cancel-btn"]
	Click Element	//*[@id="function-button-1"]
	Wait Until Page Contains	${DELETE_SCHEDULE}	20s
	Click Element	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	${DELETED_SCHEDULE_MESSAGE}	20s