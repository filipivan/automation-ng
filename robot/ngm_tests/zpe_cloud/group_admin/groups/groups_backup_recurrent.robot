*** Settings ***
Documentation	Tests for recurrent backup feature via gropus
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	RECURRENT_BACKUP
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_3

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${GROUP_NAME}	ZPE_GROUP
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${HAS_GROUP}	${FALSE}
${HAS_NG_ON_GROUP}	${FALSE}

*** Test Cases ***
Test case to create group and add NG device to it
	GUI::ZPECloud::Security::Services::Disable file protection on NG device if needed	${USERNG}	${PWDNG}
	SUITE:Setup
	GUI::ZPECloud::Groups::Open Tab
	${HAS_GROUP}=	Run Keyword And return Status	GUI::ZPECloud::Groups::Find Group and Select it	${GROUP_NAME}
	Run Keyword If	${HAS_GROUP}	GUI::ZPECloud::Groups::Delete Group	${GROUP_NAME}
	${HAS_GROUP}	Set Variable	${FALSE}
	Set Suite Variable	${HAS_GROUP}
	GUI::ZPECloud::Groups::Create Group	${GROUP_NAME}
	${HAS_GROUP}	Set Variable	${TRUE}
	Set Suite Variable	${HAS_GROUP}
	GUI::ZPECloud::Groups::Devices::Add device to group	${GROUP_NAME}	${SERIALNUMBER}
	GUI::ZPECloud::Groups::Go to backup page on groups	${GROUP_NAME}
	${HAS_NG_ON_GROUP}	Set Variable	${TRUE}
	Set Suite Variable	${HAS_NG_ON_GROUP}

Test Recurrent Backup - File Protection:None - File Storage:Temporary
	Run Keyword If	not ${HAS_NG_ON_GROUP}	Fail	There is no device on group to execute the backup tests
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Groups::Go to backup page on groups	${GROUP_NAME}
	Wait Until Page Contains Element	//*[@id="opt-none"]	15s
	Click Element	//*[@id="opt-none"]
	Click Element	//*[@id="opt-temporary"]
	GUI::ZPECloud::Devices::Backup::Schedule Backup
	GUI::ZPECloud::Devices::Backup::Apply Configuration	GROUPS=Yes
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Backup::Check if backup has been started	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Check if backup is Successful	Temporary
	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Schedule::Delete Schedule	${EMAIL}	${PWD}
	[Teardown]	SUITE:Test Teardown

Test Recurrent Backup - File Protection:None - File Storage:Persistent
	Run Keyword If	not ${HAS_NG_ON_GROUP}	Fail	There is no device on group to execute the backup tests
	SUITE:Setup
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Groups::Go to backup page on groups	${GROUP_NAME}
	Click Element	//*[@id="opt-none"]
	Click Element	//*[@id="opt-persistant"]
	GUI::ZPECloud::Devices::Backup::Schedule Backup
	GUI::ZPECloud::Devices::Backup::Apply Configuration	GROUPS=Yes
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Backup::Check if backup has been started	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Check if backup is Successful	Persistent
	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Schedule::Delete Schedule	${EMAIL}	${PWD}
	[Teardown]	SUITE:Test Teardown

Test Recurrent Backup - File Protection:TPM Encrypted - File Storage:Temporary
	Run Keyword If	not ${HAS_NG_ON_GROUP}	Fail	There is no device on group to execute the backup tests
	SUITE:Setup
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Groups::Go to backup page on groups	${GROUP_NAME}
	Click Element	//*[@id="opt-tpm"]
	Click Element	//*[@id="opt-temporary"]
	GUI::ZPECloud::Devices::Backup::Schedule Backup
	GUI::ZPECloud::Devices::Backup::Apply Configuration	GROUPS=Yes
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Backup::Check if backup has been started	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Check if backup is Successful	Temporary
	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Schedule::Delete Schedule	${EMAIL}	${PWD}
	[Teardown]	SUITE:Test Teardown

Test Recurrent Backup - File Protection:TPM Encrypted - File Storage:Persistent
	Run Keyword If	not ${HAS_NG_ON_GROUP}	Fail	There is no device on group to execute the backup tests
	SUITE:Setup
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Groups::Go to backup page on groups	${GROUP_NAME}
	Click Element	//*[@id="opt-tpm"]
	Click Element	//*[@id="opt-persistant"]
	GUI::ZPECloud::Devices::Backup::Schedule Backup
	GUI::ZPECloud::Devices::Backup::Apply Configuration	GROUPS=Yes
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Backup::Check if backup has been started	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Check if backup is Successful	Persistent
	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Schedule::Delete Schedule	${EMAIL}	${PWD}
	[Teardown]	SUITE:Test Teardown

Test Recurrent Backup - File Protection:Password Encrypted - File Storage:Temporary
	Run Keyword If	not ${HAS_NG_ON_GROUP}	Fail	There is no device on group to execute the backup tests
	SUITE:Setup
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Security::Services::Enable File Protection on NG Device	${USERNG}	${PWDNG}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Groups::Go to backup page on groups	${GROUP_NAME}
	Click Element	//*[@id="opt-password-enc"]
	Click Element	//*[@id="opt-temporary"]
	GUI::ZPECloud::Devices::Backup::Schedule Backup
	GUI::ZPECloud::Devices::Backup::Apply Configuration	GROUPS=Yes
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Backup::Check if backup has been started	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Check if backup is Successful	Temporary
	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Schedule::Delete Schedule	${EMAIL}	${PWD}
	[Teardown]	SUITE:Test Teardown to password protected test cases

Test Recurrent Backup - File Protection:Password Encrypted - File Storage:Persistent
	Run Keyword If	not ${HAS_NG_ON_GROUP}	Fail	There is no device on group to execute the backup tests
	SUITE:Setup
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Security::Services::Enable File Protection on NG Device	${USERNG}	${PWDNG}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Groups::Go to backup page on groups	${GROUP_NAME}
	Click Element	//*[@id="opt-password-enc"]
	Click Element	//*[@id="opt-persistant"]
	GUI::ZPECloud::Devices::Backup::Schedule Backup
	GUI::ZPECloud::Devices::Backup::Apply Configuration	GROUPS=Yes
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Backup::Check if backup has been started	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Check if backup is Successful	Persistent
	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Schedule::Delete Schedule	${EMAIL}	${PWD}
	[Teardown]	SUITE:Test Teardown to password protected test cases

Test case to remove NG device from group
	Run Keyword If	not ${HAS_NG_ON_GROUP}	Fail	There is no device on group to execute the backup tests
	SUITE:Setup
	GUI::ZPECloud::Groups::Devices::Remove device from group	${GROUP_NAME}	${SERIALNUMBER}
	[Teardown]	Close All Browsers

Test case to delete the group
	Run Keyword If	not ${HAS_GROUP}	Fail	There is no group to be deleted
	SUITE:Setup
	GUI::ZPECloud::Groups::Delete Group	${GROUP_NAME}
	[Teardown]	Close All Browsers

*** Keywords ***
SUITE:Setup
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud

SUITE:Setup test
	Wait Until Keyword Succeeds	3x	3s	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Teardown
	SUITE:Setup
	GUI::ZPECloud::Groups::Delete group If needed	${GROUP_NAME}
	GUI::ZPECloud::Profiles::Operation::Open Tab
	GUI::ZPECloud::Schedules::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	(//*/tr/td)[1]	60s
	${HAS_SCHEDULE}	Run Keyword And Return Status	Page Should Contain Element	(//*/tr/td/div)[1]
	Run Keyword If	${HAS_SCHEDULE}	GUI::ZPECloud::Profiles::Operation::Schedule::Delete Schedule	${EMAIL}	${PWD}
	SUITE:Setup
	GUI::ZPECloud::Profiles::Backup::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	(//*/tr/td)[1]	60s
	${HAS_BACKUP}	Run Keyword And Return Status	Page Should Contain Element	(//*/tr/td/span/input)[1]
	Run Keyword If	${HAS_BACKUP}	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	Close All Browsers
	Close All Connections

SUITE:Test Teardown
	SUITE:Setup
	GUI::ZPECloud::Profiles::Operation::Open Tab
	GUI::ZPECloud::Schedules::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	(//*/tr/td)[1]	60s
	${HAS_SCHEDULE}	Run Keyword And Return Status	Page Should Contain Element	(//*/tr/td/div)[1]
	Run Keyword If	${HAS_SCHEDULE}	GUI::ZPECloud::Profiles::Operation::Schedule::Delete Schedule	${EMAIL}	${PWD}
	SUITE:Setup
	GUI::ZPECloud::Profiles::Backup::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	(//*/tr/td)[1]	60s
	${HAS_BACKUP}	Run Keyword And Return Status	Page Should Contain Element	(//*/tr/td/span/input)[1]
	Run Keyword If	${HAS_BACKUP}	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	Close All Browsers
	Close All Connections

SUITE:Test Teardown to password protected test cases
	SUITE:Test Teardown
	GUI::ZPECloud::Security::Services::Disable file protection on NG device if needed	${USERNG}	${PWDNG}