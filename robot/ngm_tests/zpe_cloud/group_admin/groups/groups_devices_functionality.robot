*** Settings ***
Documentation	Testing Devices Configuration in Groups
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	GROUPS
Force Tags	GUI	ZPECLOUD	LOGIN	${BROWSER}	NON-CRITICAL	DEVICE	PART_3

Suite Setup	SUITE:Setup
Suite Teardown	Close All Browsers

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${GROUP_NAME}	ZPE_GROUP
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${HAS_GROUP}	${FALSE}
${HAS_NG_ON_GROUP}	${FALSE}

*** Test Cases ***
Test case to create group and add NG device to it
	GUI::ZPECloud::Security::Services::Disable file protection on NG device if needed	${USERNG}	${PWDNG}
	SUITE:Setup
	GUI::ZPECloud::Groups::Open Tab
	${HAS_GROUP}=	Run Keyword And return Status	GUI::ZPECloud::Groups::Find Group and Select it	${GROUP_NAME}
	Run Keyword If	${HAS_GROUP}	GUI::ZPECloud::Groups::Delete Group	${GROUP_NAME}
	${HAS_GROUP}	Set Variable	${FALSE}
	Set Suite Variable	${HAS_GROUP}
	GUI::ZPECloud::Groups::Create Group	${GROUP_NAME}
	${HAS_GROUP}	Set Variable	${TRUE}
	Set Suite Variable	${HAS_GROUP}
	GUI::ZPECloud::Groups::Devices::Add device to group	${GROUP_NAME}	${SERIALNUMBER}
	GUI::ZPECloud::Groups::Go to backup page on groups	${GROUP_NAME}
	${HAS_NG_ON_GROUP}	Set Variable	${TRUE}
	Set Suite Variable	${HAS_NG_ON_GROUP}

Test case to remove NG device from group
	Run Keyword If	not ${HAS_NG_ON_GROUP}	Fail	There is no device on group to execute the backup tests
	SUITE:Setup
	GUI::ZPECloud::Groups::Devices::Remove device from group	${GROUP_NAME}	${SERIALNUMBER}
	[Teardown]	Close All Browsers

Test case to delete the group
	Run Keyword If	not ${HAS_GROUP}	Fail	There is no group to be deleted
	SUITE:Setup
	GUI::ZPECloud::Groups::Delete Group	${GROUP_NAME}
	[Teardown]	Close All Browsers

*** Keywords ***
SUITE:Setup
	Wait Until Keyword Succeeds	3x	3s	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Teardown
	SUITE:Setup
	GUI::ZPECloud::Groups::Delete group If needed	${GROUP_NAME}
	Close All Browsers
	Close All Connections