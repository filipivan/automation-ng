*** Settings ***
Documentation	Testing Groups Configuration
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	GROUPS
Force Tags	GUI	ZPECLOUD	LOGIN	${BROWSER}	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	Close All Browsers

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${GROUP_NAME}	ZPE_GROUP
${CREATE_GROUP}	Group added successfully
${DELETE_GROUP}	Group deleted successfully
${DEFAULT_GROUP}	Group was set as default
${GROUP_NAME_EDIT}	ZPE_GROUP1
${EDIT_NAME}	Group details edited successfully
${BACKUPSUCCESS}	Backup in the selected groups was requested

*** Test Cases ***
Create Group
	${HAVE_GROUP}=	Run Keyword And return Status	SUITE:Find Group
	Run Keyword If	${HAVE_GROUP} == True	SUITE:Delete the Group
	SUITE:Create the Group
	SUITE:Teardown

Edit Group name
	SUITE:Setup
	SUITE:Verify if Groups Exists
	Click Element	function-button-1
	Wait Until Page Contains Element	ca-groups-general-table-1
	Press Keys	xpath=//input[@name='name']	CTRL+a+BACKSPACE
	Clear Element Text	xpath=//input[@name='name']
	Input Text	xpath=//input[@name='name']	${GROUP_NAME_EDIT} 
	Click Element	submit-btn
	Wait Until Page Contains	${EDIT_NAME} 
	SUITE:Teardown

Set as Default
	SUITE:Setup
	SUITE:Verify if Groups Exists
	Click Element	function-button-3
	Wait Until Page Contains Element	ca-groups-general-default-dialog
	Click Element	confirm-btn
	Wait Until Page Contains	${DEFAULT_GROUP}
	SUITE:Teardown

Delete Deafult group
	SUITE:Setup
	SUITE:Verify if Groups Exists	
	Click Element	function-button-2
	Wait Until Page Contains Element	dialog-no-delete
	Click Element	cancel-btn
	SUITE:Teardown

Delete Group
	SUITE:Setup
	SUITE:Verify if Groups Exists	
	SUITE:Give Default to another Group
	SUITE:Delete the Group

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Groups::Open Tab
	
SUITE:Teardown
	Close All Browsers

SUITE:Find Group
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Groups::Open Tab
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${GROUP_NAME}
	Wait Until Page Contains Element	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr[1]/td[1]/span

SUITE:Give Default to another Group
	SUITE:Setup
	Wait Until Page Contains Element	search-input
	Input Text	search-input	User
	Wait Until Page Contains Element	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr[1]/td[1]/span
	Click Element	function-button-3
	Wait Until Page Contains Element	ca-groups-general-default-dialog
	Click Element	confirm-btn

SUITE:Delete the Group
	SUITE:Setup
	SUITE:Find Group
	Click Element	function-button-2
	Wait Until Page Contains Element	ca-groups-general-alert-dialog
	Click Element	confirm-btn
	Wait Until Page Contains	${DELETE_GROUP}
	Reload Page
	sleep	5s
	Wait Until Page Contains Element	search-input	30s
	Input Text	search-input	${GROUP_NAME}
	Page Should Not Contain Element	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr/td[2]/div

SUITE:Create the Group
	Reload Page
	sleep	5s
	Wait Until Page Contains Element	function-button-0
	Click Element	function-button-0
	Sleep	5
	Input Text	xpath=//input[@name='name']	${GROUP_NAME}
	Click Element	//*[@id="ca-group-permission-management-radio"]/label[1]/span[1]/input
	Click Element	submit-btn
	Wait Until Page Contains	${CREATE_GROUP}	30s
	Reload Page
	sleep	5s
	Wait Until Page Contains Element	search-input	30s
	Input Text	search-input	${GROUP_NAME}
	Wait Until Page Contains Element	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr/td[2]/div
	Element Should Contain	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr/td[2]/div	${GROUP_NAME}

SUITE:Verify if Groups Exists
	${HAVE_GROUP}=	Run Keyword And return Status	SUITE:Find Group
	Run Keyword If	${HAVE_GROUP} == False	SUITE:Create the Group
	Run Keyword If	${HAVE_GROUP} == False	GUI::ZPECloud::Groups::Open Tab
	SUITE:Find Group