*** Settings ***
Documentation	Check dashboard into ZPE Cloud: Behavior tests
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags	GUI	ZPECLOUD	LOGIN	${BROWSER}	DEVICE	EXCLUDEIN_ZPECLOUD	PART_4
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CUSTOMER_USERNAME}	${EMAIL_ADDRESS}
${CUSTOMER_PASSWORD}	${PASSWORD}
${NAME}	ZPE Brazil
${ADDRESS}	Rua 7 de Setembro, 1678, Blumenau-SC
${LATITUDE}	40.777800400000004
${LONGITUDE}	8.921996955790174

*** Test Cases ***
Test validate number of sites between map and sites tab
	GUI::ZPECloud::Sites::General::Add	${NAME}	${ADDRESS}	${LATITUDE}	${LONGITUDE}
	GUI::ZPECloud::Sites::Open Tab
	Sleep	5
	${SITESTEXT}=	Get Element Count	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr
	GUI::ZPECloud::Dashboard::Open Tab
	Sleep	10
	Wait Until Page Contains Element	//*[@id="ca-dashboard-map-wrapper"]/div[2]/div[1]/div/div/div/div[1]
	${MAPTEXT}=	Get Text	//*[@id="ca-dashboard-map-wrapper"]
	${MAPTEXT}=	Fetch From Right	${MAPTEXT}	Total Sites
	${MAPTEXT}=	Fetch From Left	${MAPTEXT}	O
	Should Be Equal As Integers	${MAPTEXT}	${SITESTEXT}
	[Teardown]	SUITE:Teardown

Test to compare the number of sites betweem dashboard and graffic
	[Setup]	SUITE:Setup
	GUI::ZPECloud::Dashboard::Open Tab
	${count_dashboard}=	Get Element Count	xpath=//main/*//img[contains(@class, 'leaflet-marker-icon')]
	${elements}=	Get Webelements	xpath=//main//main//div/*//p
	${qtt_sites_element}=	Get From List	${elements}	1
	${qtt_sites_count}=	Get Text	${qtt_sites_element}
	Should Be Equal As Integers	${count_dashboard}	${qtt_sites_count}
	[Teardown]	SUITE:Teardown

Test to compare the number of sites betweem graffic and sites page
	[Setup]	SUITE:Setup
	GUI::ZPECloud::Dashboard::Open Tab
	${count_dashboard}=	Get Element Count	xpath=//main/*//img[contains(@class, 'leaflet-marker-icon')]
	${elements}=	Get Webelements	xpath=//main//main//div/*//p
	${qtt_sites_element}=	Get From List	${elements}	1
	${qtt_sites_count}=	Get Text	${qtt_sites_element}
	GUI::ZPEC#Test validate number of sites between map and sites tab
	GUI::ZPECloud::Sites::General::Add	${NAME}	${ADDRESS}	${LATITUDE}	${LONGITUDE}
	GUI::ZPECloud::Sites::Open Tab
	Sleep	5
	${SITESTEXT}=	Get Element Count	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr
	GUI::ZPECloud::Dashboard::Open Tab
	Sleep	10
	Wait Until Page Contains Element	//*[@id="ca-dashboard-map-wrapper"]/div[2]/div[1]/div/div/div/div[1]
	${MAPTEXT}=	Get Text	//*[@id="ca-dashboard-map-wrapper"]
	${MAPTEXT}=	Fetch From Right	${MAPTEXT}	Total Sites
	${MAPTEXT}=	Fetch From Left	${MAPTEXT}	O
	Should Be Equal As Integers	${MAPTEXT}	${SITESTEXT}
	[Teardown]	SUITE:Teardown

Test to compare the number of sites betweem dashboard and graffic
	[Setup]	Sloud::Sites::Open Tab
	Sleep	5
	${SITESTEXT}=	Get Element Count	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr
	Should Be Equal As Integers	${qtt_sites_count}	${SITESTEXT}
	[Teardown]	SUITE:Teardown

Test to compare the number of sites betweem dashboard, graffic and sites page
	[Setup]	SUITE:Setup
	GUI::ZPECloud::Dashboard::Open Tab
	${count_dashboard}=	Get Element Count	xpath=//main/*//img[contains(@class, 'leaflet-marker-icon')]
	${elements}=	Get Webelements	xpath=//main//main//div/*//p
	${qtt_sites_element}=	Get From List	${elements}	1
	${qtt_sites_count}=	Get Text	${qtt_sites_element}
	Should Be Equal As Integers	${count_dashboard}	${qtt_sites_count}
	GUI::ZPECloud::Sites::Open Tab
	Sleep	5
	${SITESTEXT}=	Get Element Count	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr
	GUI::ZPECloud::Dashboard::Open Tab
	Sleep	10
	Wait Until Page Contains Element	//*[@id="ca-dashboard-map-wrapper"]/div[2]/div[1]/div/div/div/div[1]
	${MAPTEXT}=	Get Text	//*[@id="ca-dashboard-map-wrapper"]
	${MAPTEXT}=	Fetch From Right	${MAPTEXT}	Total Sites
	${MAPTEXT}=	Fetch From Left	${MAPTEXT}	O
	Should Be Equal As Integers	${MAPTEXT}	${SITESTEXT}
	Should Be Equal As Integers	${qtt_sites_count}	${SITESTEXT}
	Should Be Equal As Integers	${qtt_sites_count}	${MAPTEXT}
	[Teardown]	SUITE:Teardown

Test to compare the number of sites in the Dashboard and sites page adding site
	GUI::ZPECloud::Sites::General::Add	${NAME}	${ADDRESS}	${LATITUDE}	${LONGITUDE}
	Sleep	2
	GUI::ZPECloud::Dashboard::Map::Open Tab
	${elements}=	Get Webelements	xpath=//main//main//div/*//p
	${qtt_sites_element}=	Get From List	${elements}	1
	${qtt_sites_element}=	Get Text	${qtt_sites_element}
	${count_dashboard}=	Get Element Count	xpath=//main/*//img[contains(@class, 'leaflet-marker-icon')]
	GUI::ZPECloud::Sites::Open Tab
	Sleep	5
	${SITESTEXT}=	Get Element Count	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr
	Should Be Equal As Integers	${qtt_sites_element}	${SITESTEXT}
	Should Be Equal As Integers	${qtt_sites_element}	${count_dashboard}
	Should Be Equal As Integers	${count_dashboard}	${SITESTEXT}
	[Teardown]	GUI::ZPECloud::Basic::Table::Delete If Exists	general_0	${NAME}

Test to compare number the sites
	[Setup]	GUI::ZPECloud::Dashboard::Map::Open Tab

	Click Element	xpath=//main/*//img[contains(@class, 'leaflet-marker-icon')][2]
	Sleep	3

	${sites}=	Get Webelements	xpath=//main/*//img[contains(@class, 'leaflet-marker-icon')]
	:FOR	${site}	IN	${sites}
	\	Click Element	${site}

	${element}=	Get Webelements	xpath=//main/*//h5
	${site_name}=	Get From List	${element}	1
	${site_name}=	Get Text	${site_name}
	${site_name}=	Fetch From Right	${site_name}	:${SPACE}
	Log to Console	----------${site_name}----------------

	${site_address}=	Get From List	${element}	2
	${site_address}=	Get Text	${site_address}
	${site_address}=	Fetch From Right	${site_address}	:${SPACE}

	${assigned_devices}=	Get From List	${element}	3
	${assigned_devices}=	Get Text	${assigned_devices}
	${assigned_devices}=	Fetch From Right	${assigned_devices}	:${SPACE}

	${devices_online}=	Get From List	${element}	4
	${devices_online}=	Get Text	${devices_online}
	${devices_online}=	Fetch From Right	${devices_online}	:${SPACE}

	${total_devices}=	Get From List	${element}	5
	${total_devices}=	Get Text	${total_devices}
	${total_devices}=	Fetch From Right	${total_devices}	:${SPACE}

	GUI::ZPECloud::Sites::General::Open Tab

	Click Element	xpath=//div[@id='mui-component-select-site-assigned']
	Click Element	xpath=//div[@id='menu-site-assigned']//div//ul//li[text()='${site_name}']
	Sleep	3
	${count_devices}=	GUI::ZPECloud::Basic::Table::Get Count Rows	general_1

	${count_online}=	Evaluate	0
	${count_offline}=	Evaluate	0
	${never_connected}=	Evaluate	0
	${elements}=	Get Webelements	xpath=//table[@id='general_1']//tbody/tr/td[5]
	:FOR	${element}	IN	@{elements}
	\	${text}=	Get Text	${element}
	\	${text}=	Convert To Lowercase	${text}
	\	${count_offline}=	Set Variable	${count_offline + 1}
	\	${count_online}=	Set Variable If	'${text}' == 'online'	${count_online + 1}	${count_online}
	\	${count_offline}=	Set Variable If	'${text}' == 'offline'	${count_offline + 1}	${count_offline}
	\	${never_connected}=	Set Variable If	'${text}' == 'never connected'	${never_connected + 1}	${never_connected}

Test to compare the number of devices online and offline betweem graffic and devices page
	[Setup]	GUI::ZPECloud::Devices::Enrolled::Open Tab
	${count_online}=	Evaluate	0
	${elements}=	Get Webelements	xpath=//main//main//div/*//span[contains(@style, 'min-width: 100px')]
	:FOR	${element}	IN	@{elements}
	\	${text}=	Get Text	${element}
	\	Run Keyword If	'${text}' == 'Online'	Evaluate	${count_online} + 1

	GUI::ZPECloud::Dashboard::Map::Open Tab

	${svgs}=	Get Webelements	xpath=//*[local-name()='path']
	:FOR	${svg}	IN	@{svgs}
	\	Mouse Over	${svg}
	#\	${var}=	Get Webelements	xpath=//main//main//div/*//div[contains(@style, 'position: absolute; display: none;')]
	\	${var}=	Get Webelements	xpath=//main/*//div[contains(@style, 'position: absolute; display: none;')]
	\	${var2}=	Get From List	${var}	1
	\	${var3}=	Get Text	${var2}

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open And Login	${CUSTOMER_USERNAME}	${CUSTOMER_PASSWORD}
	GUI::ZPECloud::Sites::Open Tab
#	GUI::ZPECloud::Basic::Table::Delete If Exists	general_0	${NAME}

SUITE:Teardown
#	Run Keyword If Any Tests Failed	GUI::ZPECloud::Basic::Table::Delete If Exists	general_0	${NAME}
	GUI::ZPECloud::Basic::Open And Login	${CUSTOMER_USERNAME}	${CUSTOMER_PASSWORD}
	GUI::ZPECloud::Basic::Logout and Close All
