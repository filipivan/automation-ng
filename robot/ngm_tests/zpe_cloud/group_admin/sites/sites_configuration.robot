*** Settings ***
Documentation	Testing Site Configuration
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	SITE
Force Tags	GUI	ZPECLOUD	LOGIN	${BROWSER}	NON-CRITICAL	DEVICE	PART_4

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CUSTOMER_USERNAME}	${EMAIL_ADDRESS}
${CUSTOMER_PASSWORD}	${PASSWORD}
${SITE_NAME}	ZPE_SITE
${EDITED_SITE_NAME}	ZPE_SITE_EDITED
${SITE_ADDRESS}	Rua 7 de Setembro, 1678, Blumenau-SC
${SITE_LATITUDE}	40.777800400000004
${SITE_LONGITUDE}	8.921996955790174
${SITE_ADDED_MESSAGE}	Site added successfully
${SITE_EDITED_MESSAGE}	Site details edited successfully
${SITE_DELETED_MESSAGE}	Site deleted successfully
${DEVICE_ADDED_TO_SITE_MESSAGE}	Assigned to Site
${DEVICE_REMOVED_FROM_SITE_MESSAGE}	Removed from Site

*** Test Cases ***
Test to Add Site
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	GUI::ZPECloud::Sites::Open Tab
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${SITE_NAME}
	${HASSITE}=	Run Keyword and Return Status	Wait Until Page Contains	${SITE_NAME}	15s
	Run Keyword if	${HASSITE} == True	SUITE:Delete Site
	SUITE:Add Site
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${SITE_NAME}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	[Teardown]	SUITE:Teardown

Test to Edit Site
	[Setup]	SUITE:Setup
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${SITE_NAME}
	${HASSITE}=	Run Keyword and Return Status	Wait Until Page Contains	${SITE_NAME}	15s
	Run Keyword if	${HASSITE} == False	SUITE:Add Site
	Clear Element Text	search-input
	Press Keys	search-input	CTRL+a+BACKSPACE
	sleep	5s
	SUITE:Edit Site
	GUI::ZPECloud::Sites::Open Tab
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${EDITED_SITE_NAME}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	[Teardown]	SUITE:Teardown

test to delete site
	[Setup]	SUITE:Setup
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${SITE_NAME}
	${HASSITE}=	Run Keyword and Return Status	Wait Until Page Contains	${SITE_NAME}	15s
	Run Keyword if	${HASSITE} == False	SUITE:Add Site
	Clear Element Text	search-input
	Press Keys	search-input	CTRL+a+BACKSPACE
	sleep	5s
	SUITE:Delete Site

Test to add device on site
	[Setup]	SUITE:Setup
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${SITE_NAME}
	${HASSITE}=	Run Keyword and Return Status	Wait Until Page Contains	${SITE_NAME}	15s
	Run Keyword if	${HASSITE} == False	SUITE:Add Site
	Clear Element Text	search-input
	Press Keys	search-input	CTRL+a+BACKSPACE
	sleep	5s
	SUITE:Add Device On Site
	GUI::ZPECloud::Devices::Open Tab
	Wait Until Page Contains Element	search-input	30s
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Element Contains	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[8]	${SITE_NAME}

test to remove Device from site
	[Setup]	SUITE:Setup
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${SITE_NAME}
	${HASSITE}=	Run Keyword and Return Status	Wait Until Page Contains	${SITE_NAME}	15s
	Run Keyword if	${HASSITE} == False	SUITE:Add Site
	Clear Element Text	search-input
	Press Keys	search-input	CTRL+a+BACKSPACE
	sleep	5s
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${SITE_NAME}
	sleep	5s
	Wait Until Page Contains Element	xpath=//a/div	30s
	Click Element	xpath=//a/div
	sleep	5s
	Wait Until Page Contains Element	search-input	30s
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	${HAS_DEVICE_ON_SITE}=	Run Keyword and Return Status	Wait Until Page Contains Element	//*[@id="ca-sd-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Run Keyword if	${HAS_DEVICE_ON_SITE} == False	SUITE:Add Device On Site
	SUITE:Remove Device From Site
	GUI::ZPECloud::Devices::Open Tab
	Wait Until Page Contains Element	search-input	30s
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Element Does Not Contain	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[8]	${SITE_NAME}

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open And Login	${CUSTOMER_USERNAME}	${CUSTOMER_PASSWORD}
	GUI::ZPECloud::Sites::Open Tab

SUITE:Teardown
	GUI::ZPECloud::Basic::Open And Login	${CUSTOMER_USERNAME}	${CUSTOMER_PASSWORD}
	GUI::ZPECloud::Sites::Open Tab
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${SITE_NAME}
	${HASSITE}=	Run Keyword and Return Status	Wait Until Page Contains	${SITE_NAME}	15s
	Run Keyword if	${HASSITE} == True	SUITE:Delete Site
	Close All Browsers

SUITE:Click On Add Site Tab
	Clear Element Text	search-input
	Press Keys	search-input	CTRL+a+BACKSPACE
	sleep	5s
	Wait Until Page Contains Element	//*[@id="function-button-0"]	30s
	Click Element	//*[@id="function-button-0"]
	sleep	5s

SUITE:Add Site
	wait until keyword succeeds	30	5	SUITE:Click On Add Site Tab
	Wait Until Page Contains Element	xpath=//input[@name='name']	15s
	Input Text	xpath=//input[@name='name']	${SITE_NAME}
	Input Text	xpath=//input[@name='address']	${SITE_ADDRESS}
	Input Text	xpath=//input[@name='latitude']	${SITE_LATITUDE}
	Input Text	xpath=//input[@name='longitude']	${SITE_LONGITUDE}
	Wait Until Keyword Succeeds	30	6	Click Element	xpath=//button[@id='submit-btn']
	Wait Until Page Contains	${SITE_ADDED_MESSAGE}	15s

SUITE:Edit Site
	GUI::ZPECloud::Sites::Open Tab
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${SITE_NAME}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	Click Element	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	Wait Until Page Contains Element	xpath=//button[contains(.,'Edit')]	15s
	Click Element	xpath=//button[contains(.,'Edit')]
	sleep	5s
	Wait Until Page Contains Element	xpath=//input[@name='name']	15s
	Input Text	xpath=//input[@name='name']	${EDITED_SITE_NAME}
	Wait Until Keyword Succeeds	30	6	Click Element	xpath=//button[@id='submit-btn']
	Wait Until Page Contains	${SITE_EDITED_MESSAGE}	15s

SUITE:Delete Site
	GUI::ZPECloud::Sites::Open Tab
	Clear Element Text	search-input
	Press Keys	search-input	CTRL+a+BACKSPACE
	sleep	5s
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${SITE_NAME}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	Click Element	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	Wait Until Page Contains Element	xpath=//button[contains(.,'Delete')]	30s
	Click Element	xpath=//button[contains(.,'Delete')]
	Wait Until Page Contains Element	xpath=//button[@id='confirm-btn']	15s
	Click Element	xpath=//button[@id='confirm-btn']
	Wait Until Page Contains	${SITE_DELETED_MESSAGE}	15s

SUITE:Click On "Add On Site" Tab
	GUI::ZPECloud::Sites::Devices::Open Tab
	Wait Until Page Contains Element	search-input	30s
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-sd-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Click Element	//*[@id="ca-sd-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Wait Until Page Contains Element	//*[@id="function-button-0"]	30s
	Click Element	//*[@id="function-button-0"]
	Wait Until Page Contains Element	search-input	30s
	Input Text	search-input	${SITE_NAME}
	Sleep	5

SUITE:Add Device On Site
	wait until keyword succeeds	30	5	SUITE:Click On "Add On Site" Tab
	Wait Until Page Contains Element	//*[@id="ca-sd-table-2"]/div/div/table/tbody/tr[1]/td[1]/span/input	30s
	Click Element	//*[@id="ca-sd-table-2"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Wait Until Page Contains Element	//*[@id="function-button-1"]	30s
	Click Element	//*[@id="function-button-1"]
	Wait Until Page Contains	${DEVICE_ADDED_TO_SITE_MESSAGE}	15s

SUITE:Remove Device From Site
	GUI::ZPECloud::Sites::Open Tab
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${SITE_NAME}
	sleep	5s
	Wait Until Page Contains Element	xpath=//a/div
	Click Element	xpath=//a/div
	sleep	5s
	Wait Until Page Contains Element	search-input	30s
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-sd-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Click Element	//*[@id="ca-sd-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Wait Until Page Contains Element	//*[@id="function-button-1"]	30s
	Click Element	//*[@id="function-button-1"]
	Wait Until Page Contains	${DEVICE_REMOVED_FROM_SITE_MESSAGE}	15s