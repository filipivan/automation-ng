*** Settings ***
Documentation	Testing Nodegrid Watchdog for Daemons functionality
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Default Tags	NODEGRID_WATCHDOG_DAEMONS
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE}	nodegrid
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${PING_QA_CLOUD}	qa-zpecloud.com
${COMMAND_TO_BLOCK_AGENT}	iptables -A INPUT -s qa-zpecloud.com -j DROP
${COMMAND_TO_ALLOW_BACK_AGENT}	iptables -D INPUT -s qa-zpecloud.com -j DROP
${COMMAND_TO_GET_PID}	ps -ef f | grep [z]pe_cloud_agent |	awk '{print $2}'

*** Test Cases ***
Kill Agent and Forwarder process and check if they are restarted
	CLI:Connect As Root	${HOST}	${PASSWORDNG}
	${PID_OF_PROCESS}=	CLI:Write	${COMMAND_TO_GET_PID}	user=Yes
	${status}=	Run Keyword and Return Status	Should Not Be Empty	${PID_OF_PROCESS}
	Run Keyword If	${status} == False	CLI::ZPECloud::Restart agent process via root
	${PID_OF_PROCESS}=	CLI:Write	${COMMAND_TO_GET_PID}	user=Yes
	Should Not Be Empty	${PID_OF_PROCESS}
	CLI:Write	kill ${PID_OF_PROCESS}
	Wait Until Keyword Succeeds	10 min	1 min	SUITE:Check Agent Is Running
	${PID_OF_PROCESS1}=	CLI:Write	${COMMAND_TO_GET_PID}	user=Yes
	Should Not Be Equal	${PID_OF_PROCESS}	${PID_OF_PROCESS1}

Block Agent connection and check if the process is restarted
	SUITE:Setup
	CLI:Connect As Root	${HOST}	${PASSWORDNG}
	${PID_OF_PROCESS}=	CLI:Write	${COMMAND_TO_GET_PID}	user=Yes
#	Should Not Be Empty	${PID_OF_PROCESS}
	${status}=	Run Keyword and Return Status	Should Not Be Empty	${PID_OF_PROCESS}
	Run Keyword If	${status} == False	CLI::ZPECloud::Restart agent process via root
	${PID_OF_PROCESS}=	CLI:Write	${COMMAND_TO_GET_PID}	user=Yes
	Should Not Be Empty	${PID_OF_PROCESS}
	CLI:Write	${COMMAND_TO_BLOCK_AGENT}
	SUITE:Test_Ping_Loss	${PING_QA_CLOUD}
	${PID_OF_PROCESS1}=	CLI:Write	${COMMAND_TO_GET_PID}	user=Yes
	Should Be Equal	${PID_OF_PROCESS}	${PID_OF_PROCESS1}
	CLI:Connect As Root	${HOST}	${PASSWORDNG}
	CLI:Write	${COMMAND_TO_ALLOW_BACK_AGENT}
	SUITE:Test_Ping_No_Loss	${PING_QA_CLOUD}

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}


SUITE:Teardown
	CLI:Connect As Root	${HOST}	${PASSWORDNG}
	CLI:Write	${COMMAND_TO_ALLOW_BACK_AGENT}
	SUITE:Test_Ping_No_Loss	${PING_QA_CLOUD}
	Close All Browsers

SUITE:Check Agent Is Running
	CLI:Connect As Root	${HOST}	${PASSWORDNG}
	${OUTPUT}=	CLI:Write	${COMMAND_TO_GET_PID}	user=Yes
	Should Not Be Empty	${OUTPUT}

SUITE:Test_Ping_Loss
	[Arguments]	${IP_ADDRESS}	${NUM_PACKETS}=10	${TIMEOUT}=60
	...	${SOURCE_INTERFACE}=${EMPTY}
	${PING_COMMAND}=	Set Variable	ping ${IP_ADDRESS} -W ${TIMEOUT} -c ${NUM_PACKETS} -B
	${PING_COMMAND}=	Run Keyword If	'${SOURCE_INTERFACE}' != '${EMPTY}'
	...	Catenate	${PING_COMMAND}	${SPACE}-I ${SOURCE_INTERFACE}
	...	ELSE	Set Variable	${PING_COMMAND}

	Set Client Configuration	timeout=120s
	${OUTPUT}=	CLI:Write	${PING_COMMAND}
	Should Contain	${OUTPUT}	0 received
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}

SUITE:Test_Ping_No_Loss
	[Arguments]	${IP_ADDRESS}	${NUM_PACKETS}=10	${TIMEOUT}=60
	...	${SOURCE_INTERFACE}=${EMPTY}

	${PING_COMMAND}=	Set Variable	ping ${IP_ADDRESS} -W ${TIMEOUT} -c ${NUM_PACKETS} -B
	${PING_COMMAND}=	Run Keyword If	'${SOURCE_INTERFACE}' != '${EMPTY}'
	...	Catenate	${PING_COMMAND}	${SPACE}-I ${SOURCE_INTERFACE}
	...	ELSE	Set Variable	${PING_COMMAND}

	Set Client Configuration	timeout=120s
	${OUTPUT}=	CLI:Write	${PING_COMMAND}
	Should Contain	${OUTPUT}	${NUM_PACKETS} received
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}