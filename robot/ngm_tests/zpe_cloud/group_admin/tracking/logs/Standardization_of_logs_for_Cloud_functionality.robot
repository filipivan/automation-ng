*** Settings ***
Documentation	Testing Standardization of logs for Cloud
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	WEB	LOGS
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_4

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE}	nodegrid
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${MSGSUC}	Successful
${MSGBKP}	Backup configuration
${BACKUPDELETED}	Backups are deleted successfully
${Currentdate}
${TommorrowDate}

*** Test Cases ***
Test For Disabling ZPE Cloud destination And Check for Log
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	SUITE:Setup
	${CHECK}=	run keyword and return status	Checkbox Should Be Selected	xpath=//input[@id='cat5']
	Run Keyword If	${CHECK} == True	Unselect Checkbox	xpath=//input[@id='cat5']
	Run Keyword If	${CHECK} == True	Click Element	xpath=//input[@id='saveButton']
	GUI::Basic::Spinner Should Be Invisible
	${Currentdate}=	Get Current Date	result_format=%Y-%m-%d %H:%M:%S
	${datetime}=	Add Time To Date	${CurrentDate}	1 days
	${TommorrowDate} =	Convert Date	${datetime}	result_format=%Y-%m-%d %H:%M:%S
	SUITE:Backup Nodegrid Configuration
	SUITE:Backup Device Configuration - Operation was Successful
	GUI::ZPECloud::TRACKING::LOGS::Open Tab
	click element	xpath=//div/div/div/div/button[2]
	Wait Until Page Contains Element	//*[@id="mui-component-select-device-id"]
	Click Element	//*[@id="mui-component-select-device-id"]
	Wait Until Page Contains Element	//*[@id="menu-device-id"]/div[3]/ul/li
	Click Element	//*[@id="menu-device-id"]/div[3]/ul/li
	Wait Until Page Contains Element	xpath=//input[@id='ca-logs-site-startdate']
	Press Keys	xpath=//input[@id='ca-logs-site-startdate']	CTRL+a+BACKSPACE
	Click Element	xpath=//input[@id='ca-logs-site-startdate']
	sleep	1s
	Input Text	xpath=//input[@id='ca-logs-site-startdate']	${Currentdate}
	Wait Until Page Contains Element	xpath=//input[@id='ca-logs-site-enddate']
	Press Keys	xpath=//input[@id='ca-logs-site-enddate']	CTRL+a+BACKSPACE
	Click Element	xpath=//input[@id='ca-logs-site-enddate']
	Input Text	xpath=//input[@id='ca-logs-site-enddate']	${TommorrowDate}
	sleep	2s
	Wait Until Element Is Visible	xpath=//button[@id='ca-logs-site-confirm-btn']
	Click Element	xpath=//button[@id='ca-logs-site-confirm-btn']
	Wait Until Keyword Succeeds	5 min	2	Wait Until Page Contains	No result found
	SUITE:Teardown

Test For Enabling ZPE Cloud destination And Check for Log
	SUITE:Setup
	${CHECK}=	run keyword and return status	Checkbox Should Not Be Selected	xpath=//input[@id='cat5']
	Run Keyword If	${CHECK} == True	Select Checkbox	xpath=//input[@id='cat5']
	Run Keyword If	${CHECK} == True	Click Element	xpath=//input[@id='saveButton']
	GUI::Basic::Spinner Should Be Invisible
	sleep	2s
	${Currentdate}=	Get Current Date	result_format=%Y-%m-%d %H:%M:%S
	${Currentdate}=	Get Current Date	result_format=%Y-%m-%d %H:%M:%S
	${Currentdate}=	Subtract Time From Date	${Currentdate}	2 minute
	${Currentdate} =	Convert Date	${Currentdate}	result_format=%Y-%m-%d %H:%M:%S
	${datetime}=	Add Time To Date	${CurrentDate}	1 days
	${TommorrowDate} =	Convert Date	${datetime}	result_format=%Y-%m-%d %H:%M:%S
	SUITE:Backup Nodegrid Configuration
	SUITE:Backup Device Configuration - Operation was Successful
	GUI::ZPECloud::TRACKING::LOGS::Open Tab
	click element	xpath=//div/div/div/div/button[2]
	Wait Until Page Contains Element	//*[@id="mui-component-select-device-id"]
	Click Element	//*[@id="mui-component-select-device-id"]
	Wait Until Page Contains Element	//*[@id="menu-device-id"]/div[3]/ul/li	15s
	Click Element	//*[@id="menu-device-id"]/div[3]/ul/li
	Wait Until Page Contains Element	xpath=//input[@id='ca-logs-site-startdate']
	Press Keys	xpath=//input[@id='ca-logs-site-startdate']	CTRL+a+BACKSPACE
	Click Element	xpath=//input[@id='ca-logs-site-startdate']
	sleep	1s
	Input Text	xpath=//input[@id='ca-logs-site-startdate']	${Currentdate}
	Wait Until Page Contains Element	xpath=//input[@id='ca-logs-site-enddate']
	Press Keys	xpath=//input[@id='ca-logs-site-enddate']	CTRL+a+BACKSPACE
	Click Element	xpath=//input[@id='ca-logs-site-enddate']
	Input Text	xpath=//input[@id='ca-logs-site-enddate']	${TommorrowDate}
	Wait Until Keyword Succeeds	5 min	2	SUITE:Get LOG information from table
#	Wait Until Page Contains Element	//*[@id="ca-logs-customer-table-1"]/div[1]/div/table/tbody/tr[1]/td[2]/div	15s
#	${GET_MESSAGE}=	Get Text	//*[@id="ca-logs-customer-table-1"]/div[1]/div/table/tbody/tr[1]/td[2]/div
#	Should Contain Any	${GET_MESSAGE}	503	502	500
	Wait Until Page Contains	A backup was requested. Protection: None.	15s
	Wait Until Page Contains	502
	Wait Until Page Contains	503

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	Click Element	xpath=//span[contains(.,'Auditing')]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=(//a[contains(text(),'Events')])[2]
	Click Element	xpath=(//a[contains(text(),'Events')])[2]
	Wait Until Page Contains Element	xpath=(//a[contains(text(),'ZPE Cloud')])[2]
	Click Element	xpath=(//a[contains(text(),'ZPE Cloud')])[2]
	sleep	1s

SUITE:Setup1
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Teardown
	SUITE:Delete Backup
	Close All Browsers

SUITE:Backup Nodegrid Configuration
	GUI::Basic::System::Toolkit::Open Tab
	Click Element	icon_saveSettings
	Wait Until Element Is Visible	savetypeselection
	Select Radio Button	savetypeselection	savesettings-cloud
	Select Radio Button	backupSecurity	security-none
	Wait Until Keyword Succeeds	30	5	GUI::Basic::Save

SUITE:Backup Device Configuration - Operation was Successful
	SUITE:Setup1
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[8]
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[6]
	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[8]	${MSGBKP}
	Wait Until Keyword Succeeds	5 min	2 sec	SUITE:Status is 'Successful'
	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[9]	${MSGSUC}

SUITE:Delete Backup
	GUI::ZPECloud::Profiles::Backup::Open Tab
	Wait Until Page Contains Element	//*[@id="ca-profiles-bkp-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="ca-profiles-bkp-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	GUI::ZPECloud::Basic::Click Button	delete
	Wait Until Page Contains Element	//*[@id="confirm-btn"]	15s
	Click Element	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	30	2	Wait Until Page Contains	${BACKUPDELETED}

SUITE:Get LOG information from table
	Wait Until Element Is Visible	xpath=//button[@id='ca-logs-site-confirm-btn']
	Click Element	xpath=//button[@id='ca-logs-site-confirm-btn']
	sleep	5s
#	Wait Until Page Contains Element	//*[@id="ca-logs-site-table-1"]/div/div/div/table/tbody/tr[1]/td[2]

SUITE:Status is 'Successful'
	Sleep	10
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div
	${CONTAINTEST}=	Run Keyword And return Status	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div	${MSGSUC}
	Run Keyword If	${CONTAINTEST} == False	Reload Page
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[9]
	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[9]	${MSGSUC}

SUITE:Check That Devices::Enrolled Page Contain Online Devices
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Wait Until Page Contains	Online	15s