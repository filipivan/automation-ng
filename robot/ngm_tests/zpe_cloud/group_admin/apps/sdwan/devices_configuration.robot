*** Settings ***
Documentation	Tests for SD-WAN app of the ZPE Cloud
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
# Resource	../../../../gui/import_export_functionality.robot
Default Tags	ACCESS	WEB	APPS	SD_WAN	EXCLUDEIN5_0	EXCLUDEIN5_2	BUG_CLOUD_7386	BUG_CLOUD_7386	#BUG_CLOUD_7288
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${COMPANY_EMAIL}	${EMAIL_ADDRESS}
${COMPANY_PWD}	${PASSWORD}
${EMAIL}	${SUPER_EMAIL_ADDRESS}
${PWD}	${SUPER_PASSWORD}
${CONFIGURATION_PASSWORD_PROTECTION}	zpe#123
${HOSTNAME}	nodegrid
${CHECK}

*** Test Cases ***
Test case Add SD-WAN app to the Company in super-admin
	SUITE:Login into ZPE Cloud SuperAdmin
	Click Element	//*[@id="sa-header-tab-1-7"]
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	Click Element	xpath=//div[@id='mui-component-select-company-id']
	GUI::Basic::Spinner Should Be Invisible
	Set Focus To Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	Click Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	GUI::Basic::Spinner Should Be Invisible
	sleep	3s
	Select Checkbox	//*[@id="7"]
	Set Focus To Element	xpath=//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s

Testcase to activate the SD-WAN app in the company

	Click Element	//*[contains(text(), 'Apps')]
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	Click Element	//*[@id="ca-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	10s
	Wait Until Page Contains Element	xpath=//h6[contains(.,'SD-WAN')]
	Click Element	xpath=//h6[contains(.,'SD-WAN')]
	sleep	5s
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-available-apps-wrapper"]/div[7]/form/div[2]/div/div[1]/div[1]/div[4]/button[2]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	xpath=//h6[contains(.,'SD-WAN')]
	sleep	5s

Enable SD-WAN in device

	SUITE: Login SD-WAN
	Wait Until Page Contains Element	xpath=//button[contains(.,'DEVICES')]
	Click Element	xpath=//button[contains(.,'DEVICES')]
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	${IS_ENABLED}=	Run Keyword and Return Status	Wait Until Element Contains	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr/td[5]	ENABLED
	Run Keyword If	${IS_ENABLED} == False	SUITE:Apply actions in devices	function-button-0	False
	Wait Until Keyword Succeeds	2 min	10	SUITE:Status is 'Successful'	ENABLE SD-WAN 
	SUITE:Verifiy SD-WAN in device
	Checkbox Should Be Selected	sdwanSettingsStatus

Disable SD-WAN

	SUITE: Login SD-WAN
	Wait Until Page Contains Element	xpath=//button[contains(.,'DEVICES')]
	Click Element	xpath=//button[contains(.,'DEVICES')]
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	${IS_ENABLED}=	Run Keyword and Return Status	Wait Until Element Contains	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr/td[5]	ENABLED
	Run Keyword If	${IS_ENABLED} == True	SUITE:Apply actions in devices	function-button-1	False
	Wait Until Keyword Succeeds	2 min	10	SUITE:Status is 'Successful'	DISABLE SD-WAN
	SUITE:Verifiy SD-WAN in device
	Checkbox Should Not Be Selected	sdwanSettingsStatus

Enable SD-WAN in device with password
	SUITE:Enable Password Protection in device

	SUITE: Login SD-WAN
	Wait Until Page Contains Element	xpath=//button[contains(.,'DEVICES')]
	Click Element	xpath=//button[contains(.,'DEVICES')]
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	${IS_ENABLED}=	Run Keyword and Return Status	Wait Until Element Contains	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr/td[5]	ENABLED
	Run Keyword If	${IS_ENABLED} == False	SUITE:Apply actions in devices	function-button-0	True
	Wait Until Keyword Succeeds	2 min	10	SUITE:Status is 'Successful'	ENABLE SD-WAN
	SUITE:Verifiy SD-WAN in device
	Checkbox Should Be Selected	sdwanSettingsStatus

Disable SD-WAN in device with password

	SUITE: Login SD-WAN
	Wait Until Page Contains Element	xpath=//button[contains(.,'DEVICES')]
	Click Element	xpath=//button[contains(.,'DEVICES')]
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	${IS_ENABLED}=	Run Keyword and Return Status	Wait Until Element Contains	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr/td[5]	ENABLED
	Run Keyword If	${IS_ENABLED} == True	SUITE:Apply actions in devices	function-button-1	True
	Wait Until Keyword Succeeds	2 min	10	SUITE:Status is 'Successful'	DISABLE SD-WAN
	SUITE:Verifiy SD-WAN in device
	Checkbox Should Not Be Selected	sdwanSettingsStatus
	SUITE:Disable Password Protection in device

*** Keywords ***
SUITE:Setup
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud

Suite:Setup test
	GUI::ZPECloud::Basic::Open And Login	${COMPANY_EMAIL}	${COMPANY_PWD}

SUITE:Teardown
	SUITE:Unselect SD-WAN in the super admin
	Close All Browsers

SUITE: Login SD-WAN
	GUI::ZPECloud::Apps::Open Tab
	Wait Until Page Contains Element	xpath=//h6[contains(.,'SD-WAN')]
	Click Element	xpath=//h6[contains(.,'SD-WAN')]

SUITE:Apply actions in devices
	[Arguments]	${BUTTON}	${PASS}
	Click Element	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	Click Button	${BUTTON}
	Run Keyword If	${PASS} == True	SUITE:Insert Password
	Wait Until Page Contains Element	confirm-btn
	Sleep	2
	Click Button	confirm-btn

SUITE:Insert Password
	Click Element	enable-password-checkbox
	Wait Until Page Contains Element	password-protected-input
	Input Text	password-protected-input	${CONFIGURATION_PASSWORD_PROTECTION} 

SUITE:Status is 'Successful'
	[Arguments]	${ACTION}
#	Reload Page
#	sleep	2s
	Wait Until Page Contains Element	xpath=//button[contains(.,'JOBS')]
	Click Element	xpath=//button[contains(.,'JOBS')]
	Wait Until Page Contains Element	xpath=//div[@id='ca-sdwan-jobs-queue-table']/div/div/table/tbody/tr/td[3]/div
	Element Should Contain	xpath=//div[@id='ca-sdwan-jobs-queue-table']/div/div/table/tbody/tr/td[3]/div	${ACTION}
	${CONTAINTEST}=	Run Keyword And return Status	Element Should Contain	xpath=//div[@id='ca-sdwan-jobs-queue-table']/div/div/table/tbody/tr/td[4]/div	SUCCESSFUL
	Run Keyword If	${CONTAINTEST} == False	Reload Page
	Element Should Contain	xpath=//div[@id='ca-sdwan-jobs-queue-table']/div/div/table/tbody/tr/td[4]/div	SUCCESSFUL
	Element Should Contain	xpath=//div[@id='ca-sdwan-jobs-queue-table']/div/div/table/tbody/tr/td[5]/a/div	${HOSTNAME}
	Element Should Contain	xpath=//div[@id='ca-sdwan-jobs-queue-table']/div/div/table/tbody/tr/td[6]/div	${SERIALNUMBER}

SUITE:Password Protection in device
	GUI::Basic::Open And Login Nodegrid	${USERNAMENG}	${PASSWORDNG}
	GUI::Basic::Security::Services::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	${CHECK}=	Run Keyword and Return Status	Checkbox Should Be Selected	pass_prot
	Set Global Variable	${CHECK}

SUITE:Enable Password Protection in device
	SUITE:Password Protection in device
	Run Keyword If	${CHECK} == False	Click Element	pass_prot
	Run Keyword If	${CHECK} == False	Wait Until Page Contains Element	file_pass
	Run Keyword If	${CHECK} == False	Input Text	file_pass	${CONFIGURATION_PASSWORD_PROTECTION}
	Run Keyword If	${CHECK} == False	Input Text	file_pass_conf	${CONFIGURATION_PASSWORD_PROTECTION}
	Run Keyword If	${CHECK} == False	GUI::Basic::Save

SUITE:Disable Password Protection in device
	SUITE:Password Protection in device
	Run Keyword If	${CHECK} == True	Unselect Checkbox	pass_prot
	GUI::Basic::Save

SUITE:Login into ZPE Cloud SuperAdmin
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Unselect SD-WAN in the super admin
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Login into ZPE Cloud SuperAdmin
	Click Element	//*[@id="sa-header-tab-1-7"]
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	Click Element	xpath=//div[@id='mui-component-select-company-id']
	Set Focus To Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	Click Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	GUI::Basic::Spinner Should Be Invisible
	Unselect Checkbox	//*[@id="7"]
	Set Focus To Element	xpath=//*[@id="sa-apps-save-btn"]
	Sleep	5s
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Ignore Error	Wait Until Element Is Visible	//*[normalize-space()='Are you sure you want to deactivate this app?']	timeout=15s	#BUG_CLOUD_7386
	Run Keyword And Ignore Error	Click Element	//*[@id="confirm-btn"]	#BUG_CLOUD_7386
	GUI::Basic::Spinner Should Be Invisible
	GUI::ZPECloud::Basic::Open And Login	${COMPANY_EMAIL}	${COMPANY_PWD}
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Ignore Error	Page Should Not Contain Element	//*[contains(text(), 'Apps')]	#BUG_CLOUD_7386
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:Verifiy SD-WAN in device
	GUI::Basic::Open And Login Nodegrid	${USERNAMENG}	${PASSWORDNG}
	GUI::Basic::Network::SD-WAN::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	netSdwanSettings
	Click Element	netSdwanSettings
	GUI::Basic::Spinner Should Be Invisible