*** Settings ***
Documentation	Tests for SD-WAN profiles tab ZPE Cloud
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
# Resource	../../../../gui/import_export_functionality.robot
Default Tags	ACCESS	WEB	APPS	SD_WAN	EXCLUDEIN5_0	EXCLUDEIN5_2	BUG_CLOUD_7386	BUG_CLOUD_7386	#BUG_CLOUD_7288
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${COMPANY_EMAIL}	${EMAIL_ADDRESS}
${COMPANY_PWD}	${PASSWORD}
${EMAIL}	${SUPER_EMAIL_ADDRESS}
${PWD}	${SUPER_PASSWORD}
${CONFIGURATION_PASSWORD_PROTECTION}	zpe#123
${HOSTNAME}	nodegrid
${CHECK}
${DESIRED_NAME_LINK}	ZPE_test_LINK 
${CLONE_NAME_LINK}	${DESIRED_NAME_LINK}_Clone

*** Test Cases ***
Test case Add SD-WAN app to the Company in super-admin
	SUITE:Login into ZPE Cloud SuperAdmin
	Click Element	//*[@id="sa-header-tab-1-7"]
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	Click Element	xpath=//div[@id='mui-component-select-company-id']
	GUI::Basic::Spinner Should Be Invisible
	Set Focus To Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	Click Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	GUI::Basic::Spinner Should Be Invisible
	sleep	3s
	Select Checkbox	//*[@id="7"]
	Set Focus To Element	xpath=//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s

Testcase to activate the SD-WAN app in the company
	SUITE:Setup
	Click Element	//*[contains(text(), 'Apps')]
	GUI::Basic::Spinner Should Be Invisible
	
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	${AUX_APP}=	Run Keyword And Return Status	Page Should Contain Element	xpath=//h6[contains(.,'SD-WAN')]
	IF	'${AUX_APP}' == 'False'
	Click Element	//*[@id="ca-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	10s
	Wait Until Page Contains Element	xpath=//h6[contains(.,'SD-WAN')]
	Click Element	xpath=//h6[contains(.,'SD-WAN')]
	sleep	5s
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="ca-available-apps-wrapper"]/div[7]/form/div[2]/div/div[1]/div[1]/div[4]/button[2]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	xpath=//h6[contains(.,'SD-WAN')]
	sleep	5s 
	END

Create link
	[Tags]	Create_link	SD_WAN
	Suite:Setup
	SUITE: Login SD-WAN
	SUITE:Go to SDWAN link
	Wait Until Page Contains Element	xpath=//button[contains(.,'+ NEW')]
	Click Element	//button[contains(.,'+ NEW')]
	Wait Until Page Contains Element	xpath=//button[contains(.,'CANCEL')]
	SUITE:Clear and input text	//input[contains(@name,'name')]	${DESIRED_NAME_LINK} 
	Click Element	interface-type
	Click Element	//li[contains(.,'GSM')]
	SUITE:Clear and input text	probes_per_second	5
	SUITE:Clear and input text	probe_idle_time	35
	Wait Until Page Contains Element	xpath=//button[contains(.,'SAVE')]
	Click Element	//button[contains(.,'SAVE')]
	Wait Until Page Contains	Link profile created successfully

Edit link
	[Tags]	Edit_link	SD_WAN
	Suite:Setup
	SUITE: Login SD-WAN
	SUITE:Go to SDWAN link
	Sleep	2
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${DESIRED_NAME_LINK}
	Sleep	2
	Click Element	//input[contains(@type,'checkbox')]
	Click Element	//button[contains(.,'EDIT')]
	Wait Until Page Contains Element	xpath=//button[contains(.,'CANCEL')]
	Click Element	interface-type
	Click Element	//li[contains(.,'ADSL')]
	SUITE:Clear and input text	probes_per_second	4
	SUITE:Clear and input text	probe_idle_time	37
	Wait Until Page Contains Element	xpath=//button[contains(.,'SAVE')]
	Click Element	//button[contains(.,'SAVE')]
	Wait Until Page Contains	Link profile updated successfully

Clone link
	[Tags]	Clone_link	SD_WAN
	Suite:Setup
	SUITE: Login SD-WAN
	SUITE:Go to SDWAN link
	Sleep	2
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${DESIRED_NAME_LINK}
	Sleep	2
	Click Element	//input[contains(@type,'checkbox')]
	Click Element	//button[contains(.,'Clone')]
	Wait Until Page Contains Element	xpath=//button[contains(.,'CANCEL')]
	SUITE:Clear and input text	//input[contains(@name,'name')]	${CLONE_NAME_LINK} 
	Click Element	//button[contains(.,'SAVE')]
	Wait Until Page Contains	Link profile created successfully
	SUITE:Teardown

Delete link
	[Tags]	Delete_link	SD_WAN
	Suite:Setup
	SUITE: Login SD-WAN
	SUITE:Go to SDWAN link
	SUITE:Deleting link	${CLONE_NAME_LINK} 
	SUITE:Deleting link	${DESIRED_NAME_LINK} 
	SUITE:Teardown

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open And Login	${COMPANY_EMAIL}	${COMPANY_PWD}

SUITE:Teardown
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Login into ZPE Cloud SuperAdmin
	Click Element	//*[@id="sa-header-tab-1-7"]
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	Click Element	xpath=//div[@id='mui-component-select-company-id']
	Set Focus To Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	Click Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	GUI::Basic::Spinner Should Be Invisible
	Unselect Checkbox	//*[@id="7"]
	Set Focus To Element	xpath=//*[@id="sa-apps-save-btn"]
	Sleep	5s
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Ignore Error	Wait Until Element Is Visible	//*[normalize-space()='Are you sure you want to deactivate this app?']	timeout=15s	#BUG_CLOUD_7386
	Run Keyword And Ignore Error	Click Element	//*[@id="confirm-btn"]	#BUG_CLOUD_7386
	GUI::Basic::Spinner Should Be Invisible
	GUI::ZPECloud::Basic::Open And Login	${COMPANY_EMAIL}	${COMPANY_PWD}
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Ignore Error	Page Should Not Contain Element	//*[contains(text(), 'Apps')]	#BUG_CLOUD_7386
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:Login into ZPE Cloud SuperAdmin
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE: Login SD-WAN
	GUI::ZPECloud::Apps::Open Tab
	Wait Until Page Contains Element	xpath=//h6[contains(.,'SD-WAN')]
	Click Element	xpath=//h6[contains(.,'SD-WAN')]

SUITE:Go to SDWAN link
	Wait Until Page Contains Element	xpath=//button[contains(.,'PROFILES')]
	Click Element	xpath=//button[contains(.,'PROFILES')]
	Wait Until Page Contains Element	xpath=//button[contains(.,'Link')]
	Click Element	xpath=//button[contains(.,'Link')]

SUITE:Clear and input text
	[Arguments]	${AUX_LOCATOR}	${AUX_VALUE}
	Clear Element Text	${AUX_LOCATOR} 
	Press Keys	${AUX_LOCATOR}	CTRL+a+BACKSPACE
	Input Text	${AUX_LOCATOR}	text=${AUX_VALUE}

SUITE:Deleting link
	[Arguments]	${AUX_NAME}
	Sleep	2
	Wait Until Page Contains Element	search-input
	SUITE:Clear and input text	search-input	${AUX_NAME}
	Sleep	2
	Click Element	//input[contains(@type,'checkbox')]
	Click Element	//button[contains(.,'DELETE')]
	Wait Until Page Contains Element	confirm-btn	
	Click Element	confirm-btn
	Wait Until Page Contains	Link profile deleted successfully