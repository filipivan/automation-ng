*** Settings ***
Documentation	Tests for Nodegrid Data Lake app of the ZPE Cloud
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	ACCESS	WEB	APPS	NODEGRID_DATA_LAKE	BUG_CLOUD_7386	BUG_CLOUD_7489	BUG_CLOUD_7386	#BUG_CLOUD_7288
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${SUP_EMAIL}	${SUPER_EMAIL_ADDRESS}
${SUP_PWD}	${SUPER_PASSWORD}
${PROFILENAME}	test_profile_automation
${FILEPASSWORD}	123456789
${PLUGINNAME}	test_plugin_automation
${CLONENAME}	test_plugin_clone
${PROFILE_SETTED_SUCCESS}	Profile was set successfully
${FIRSTPLUGINPATH}	${CURDIR}/Nodegrid_datalake_files/ping_plugin.txt
${SECONDPLUGINPATH}	${CURDIR}/Nodegrid_datalake_files/cpu_plugin.txt

*** Test Cases ***
Test case to log-in to super-admin and select nodegrid data lake app.
	GUI::ZPECloud::Basic::Open And Login	${SUP_EMAIL}	${SUP_PWD}
	Click Element	//*[@id="sa-header-tab-1-7"]
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s

	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	Click Element	xpath=//div[@id='mui-component-select-company-id']
	GUI::Basic::Spinner Should Be Invisible
	Set Focus To Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	Click Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	GUI::Basic::Spinner Should Be Invisible
	sleep	3s
	Select Checkbox	//*[@id="6"]
	Set Focus To Element	xpath=//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s

Test case to activate app in the company
	${DATA_LAKE_ACTIVATED}=	Set Variable	${FALSE}
	Set Suite Variable	${DATA_LAKE_ACTIVATED}
	SUITE:Setup
	Click Element	//*[contains(text(), 'Apps')]
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	SUITE:Activate App
	GUI::Basic::Spinner Should Be Invisible
	${DATA_LAKE_ACTIVATED}=	Set Variable	${TRUE}
	Set Suite Variable	${DATA_LAKE_ACTIVATED}
	[Teardown]	Run Keywords	Close All Browsers	AND	Run Keyword If	'${DATA_LAKE_ACTIVATED}' == '${FALSE}'	Fail	Could not activate Data Lake

Test case to create Plugin
	Run Keyword If	'${DATA_LAKE_ACTIVATED}' == '${FALSE}'	Fail	Could not activate Data Lake
	SUITE:Setup
	SUITE:Login on Service Monitoring
	Click Element	xpath=//button[contains(.,'PLUGINS')]
	Wait Until Page Contains Element	//input[contains(@placeholder,'Search Plugin, Category, Description')]
	Input Text	//input[contains(@placeholder,'Search Plugin, Category, Description')]	${PLUGINNAME}
	Sleep	2
	${HASPLUGIN}=	Run Keyword and Return Status	Wait Until Page Contains	${PLUGINNAME}	15s
	Run Keyword if	${HASPLUGIN} == True	SUITE:Delete the Plugin	${PLUGINNAME}
	SUITE:Create Plugin
	#SUITE: Teardown

Test case to create Profile
	Run Keyword If	'${DATA_LAKE_ACTIVATED}' == '${FALSE}'	Fail	Could not activate Data Lake
	SUITE:Setup
	SUITE:Login on Service Monitoring
	Click Element	xpath=//button[contains(.,'PLUGINS')]
	Wait Until Page Contains Element	//input[contains(@placeholder,'Search Plugin, Category, Description')]
	Input Text	//input[contains(@placeholder,'Search Plugin, Category, Description')]	${PLUGINNAME}
	Sleep	2
	${HASPLUGIN}=	Run Keyword and Return Status	Wait Until Page Contains	${PLUGINNAME}	15s
	Run Keyword if	${HASPLUGIN} == False	SUITE:Create Plugin
	Click Element	xpath=//button[contains(.,'PROFILES')]
	Wait Until Page Contains Element	//input[contains(@placeholder,'Search Profile, Description')]
	Input Text	//input[contains(@placeholder,'Search Profile, Description')]	${PROFILENAME}
	Sleep	2
	${HASPROFILE}=	Run Keyword and Return Status	Wait Until Page Contains	${PROFILENAME}	15s
	Run Keyword if	${HASPROFILE} == True	SUITE:Delete Profile
	SUITE:Create Profile
	#SUITE: Teardown

Test case to apply Profile from Profiles
	Run Keyword If	'${DATA_LAKE_ACTIVATED}' == '${FALSE}'	Fail	Could not activate Data Lake
	SUITE:Check device before applying profile
	SUITE:Setup
	SUITE:Login on Service Monitoring
	Click Element	xpath=//button[contains(.,'PROFILES')]
	Wait Until Page Contains Element	//input[contains(@placeholder,'Search Profile, Description')]
	Input Text	//input[contains(@placeholder,'Search Profile, Description')]	${PROFILENAME}
	Sleep	2
	${HASPROFILE}=	Run Keyword and Return Status	Wait Until Page Contains	${PROFILENAME}	15s
	Run Keyword if	${HASPROFILE} == False	SUITE:Create Profile
	Click Element	xpath=//div[contains(.,'${PROFILENAME}')]/../../td[1]
	Click Button	xpath=//button[contains(.,'APPLY TO DEVICES')]
	Wait Until Page Contains Element	//input[contains(@placeholder,'Search Hostname, Serial Number, Profile in Use')]	30s
	Input Text	//input[contains(@placeholder,'Search Hostname, Serial Number, Profile in Use')]	${SERIALNUMBER}
	Sleep	2
	Wait Until Page Contains Element	xpath=//div[contains(.,'${SERIALNUMBER}')]/../../td[1]	30s
	Click Element	xpath=//div[contains(.,'${SERIALNUMBER}')]/../../td[1]
	Click Button	apply-btn
	Wait Until Page Contains	${PROFILE_SETTED_SUCCESS}	30s
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/div/div[2]/table/tbody/tr[1]/td[9]/span/div	${PROFILENAME}
	Wait Until Keyword Succeeds	5 minutes	2	SUITE:Status is 'Successful'
	#SUITE: Teardown

Test case to apply Profile from Devices
	Run Keyword If	'${DATA_LAKE_ACTIVATED}' == '${FALSE}'	Fail	Could not activate Data Lake
	SUITE:Check device before applying profile
	SUITE:Setup
	SUITE:Login on Service Monitoring
	Click Element	xpath=//button[contains(.,'PROFILES')]
	Wait Until Page Contains Element	//input[contains(@placeholder,'Search Profile, Description')]
	Input Text	//input[contains(@placeholder,'Search Profile, Description')]	${PROFILENAME}
	Sleep	2
	${HASPROFILE}=	Run Keyword and Return Status	Wait Until Page Contains	${PROFILENAME}	15s
	Run Keyword if	${HASPROFILE} == False	SUITE:Create Profile
#	SUITE:Setup
#	SUITE:Login on Service Monitoring
	SUITE:Apply the Profile from Devices
	#SUITE: Teardown

Test case to clone Plugin
	Run Keyword If	'${DATA_LAKE_ACTIVATED}' == '${FALSE}'	Fail	Could not activate Data Lake
	SUITE:Clone Plugin
	#SUITE: Teardown

Test case to edit Plugin
	Run Keyword If	'${DATA_LAKE_ACTIVATED}' == '${FALSE}'	Fail	Could not activate Data Lake
	SUITE:Setup
	SUITE:Login on Service Monitoring
	Click Element	xpath=//button[contains(.,'PLUGINS')]
	Wait Until Page Contains Element	//input[contains(@placeholder,'Search Plugin, Category, Description')]
	Input Text	//input[contains(@placeholder,'Search Plugin, Category, Description')]	${PLUGINNAME}
	Sleep	2
	${HASPLUGIN}=	Run Keyword and Return Status	Wait Until Page Contains	${PLUGINNAME}	15s
	Run Keyword if	${HASPLUGIN} == False	SUITE:Create Plugin
	Click Element	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	Click Button	function-button-3
	Wait Until Page Contains Element	//*[@id="edit-plugin-code"]
	${PLUGIN}=	OperatingSystem.Get File	${SECONDPLUGINPATH}
	Press Keys	//*[@id="edit-plugin-code"]	CTRL+a+BACKSPACE
	Sleep	2
	Input Text	//*[@id="edit-plugin-code"]	${PLUGIN}
	Sleep	2
	Click Element	//*[@id="edit-dialog-save-btn"]
	Wait Until Page Contains	Plugin updated successfully
	#SUITE: Teardown

Test case to edit Profile
	Run Keyword If	'${DATA_LAKE_ACTIVATED}' == '${FALSE}'	Fail	Could not activate Data Lake
	SUITE:Setup
	SUITE:Login on Service Monitoring
	Click Element	xpath=//button[contains(.,'PLUGINS')]
	Wait Until Page Contains Element	//input[contains(@placeholder,'Search Plugin, Category, Description')]
	Input Text	//input[contains(@placeholder,'Search Plugin, Category, Description')]	${CLONENAME}
	Sleep	2
	${HASPLUGIN}=	Run Keyword and Return Status	Wait Until Page Contains	${CLONENAME}	15s
	Run Keyword if	${HASPLUGIN} == False	SUITE:Clone Plugin
	Click Element	xpath=//button[contains(.,'PROFILES')]
	Wait Until Page Contains Element	//input[contains(@placeholder,'Search Profile, Description')]
	Input Text	//input[contains(@placeholder,'Search Profile, Description')]	${PROFILENAME}
	Sleep	2
	${HASPROFILE}=	Run Keyword and Return Status	Wait Until Page Contains	${PROFILENAME}	15s
	Run Keyword if	${HASPROFILE} == False	SUITE:Create Profile
	Click Element	xpath=//div[contains(.,'${PROFILENAME}')]/../../td[1]
	Click Element	function-button-3
	Sleep	5
	Input Text	ca-service-monitoring-new-template-search-available-plugins	${CLONENAME}
	Sleep	2
	Click Element	//*[@id="ca-service-monitoring-profile-dialog"]/div/div/div[2]/div/div[2]/div/div[1]/div/div[2]/div/div/div/div[1]/div/div[1]/span
	Click Button	add-profile-btn
	Sleep	2
	Click Element	//*[@id="apply-btn"]
	Wait Until Page Contains	Profile saved successfully
	#SUITE: Teardown

Test case to apply Profile - Password
	Run Keyword If	'${DATA_LAKE_ACTIVATED}' == '${FALSE}'	Fail	Could not activate Data Lake
	SUITE:Check device before applying profile
	SUITE:Insert Password
	SUITE:Setup
	SUITE:Login on Service Monitoring
	SUITE:Apply the Profile from Devices
	SUITE:Unselect File Protection
	#SUITE: Teardown

Test case to delete Plugin
	Run Keyword If	'${DATA_LAKE_ACTIVATED}' == '${FALSE}'	Fail	Could not activate Data Lake
	SUITE:Delete the Plugin	${PLUGINNAME}
	SUITE:Delete the Plugin	${CLONENAME}
	#SUITE: Teardown

Test case to delete Profile
	Run Keyword If	'${DATA_LAKE_ACTIVATED}' == '${FALSE}'	Fail	Could not activate Data Lake
	SUITE:Delete Profile

*** Keywords ***
SUITE:Setup
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud

SUITE:Teardown
	GUI::ZPECloud::Basic::Open And Login	${SUP_EMAIL}	${SUP_PWD}
	Click Element	//*[@id="sa-header-tab-1-7"]
	GUI::Basic::Spinner Should Be Invisible
	sleep	5s

	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	Click Element	xpath=//div[@id='mui-component-select-company-id']
	GUI::Basic::Spinner Should Be Invisible
	Set Focus To Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	Click Element	xpath=//*[@id="menu-company-id"]/div[3]/ul/li[contains(.,"${COMPANY}")]
	GUI::Basic::Spinner Should Be Invisible
	sleep	3s
	Unselect Checkbox	//*[@id="6"]
	Click Button	//*[@id="sa-apps-save-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Ignore Error	Wait Until Element Is Visible	//*[normalize-space()='Are you sure you want to deactivate this app?']	timeout=15s	#BUG_CLOUD_7386
	Run Keyword And Ignore Error	Click Element	//*[@id="confirm-btn"]	#BUG_CLOUD_7386
	GUI::Basic::Spinner Should Be Invisible
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword And Ignore Error	Page Should Not Contain Element	//*[contains(text(), 'Apps')]	#BUG_CLOUD_7386
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:Activate App
	Click Element	//*[@id="ca-header-tab-2-1"]
	Wait Until Page Contains Element	xpath=//h6[contains(.,'Nodegrid Data Lake')]
	Click Element	xpath=//h6[contains(.,'Nodegrid Data Lake')]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[contains(@type,'button')][normalize-space()='Activate']
	Click Element	//*[contains(text(),'Nodegrid Data Lake')]/../../../../..//*[contains(text(),'Activate')]
	GUI::Basic::Spinner Should Be Invisible
	#Click Element	//div[@role="presentation"]/div[3]/div/div/div/button[2]/span
	${HASACTIVATED}=	Run Keyword and Return Status	Wait Until Page Contains	Nodegrid Datalake is being deployed
	IF	${HASACTIVATED} == False
		GUI::ZPECloud::Apps::Open Tab
		Wait Until Page Contains Element	xpath=//h6[contains(.,'Nodegrid Data Lake')]
	END

SUITE:Login on Service Monitoring
	GUI::ZPECloud::Apps::Open Tab
	Sleep	5
	Wait Until Page Contains Element	xpath=//h6[contains(.,'Nodegrid Data Lake')]
	Click Element	xpath=//h6[contains(.,'Nodegrid Data Lake')]
	Sleep	5

SUITE:Insert Password
	SUITE:Setup
	SUITE:Login on Service Monitoring
	Click Element	xpath=//button[contains(.,'PROFILES')]
	Wait Until Page Contains Element	//input[contains(@placeholder,'Search Profile, Description')]
	Input Text	//input[contains(@placeholder,'Search Profile, Description')]	${PROFILENAME}
	Sleep	2
	${HASPROFILE}=	Run Keyword and Return Status	Wait Until Page Contains	${PROFILENAME}	15s
	Run Keyword if	${HASPROFILE} == False	SUITE:Create Profile
	Click Element	xpath=//div[contains(.,'${PROFILENAME}')]/../../td[1]
	Click Element	function-button-3
	Sleep	5
	Wait Until Page Contains Element	xpath=//input[@name='showPassword']
	Click Element	xpath=//input[@name='showPassword']
	Wait Until Page Contains Element	outlined-adornment-password
	Input Text	outlined-adornment-password	${FILEPASSWORD}
	Click Button	apply-btn
	Wait Until Page Contains	Profile saved successfully
	GUI::Basic::Open And Login Nodegrid	${USERNAMENG}	${PASSWORDNG}
	GUI::Basic::Security::Services::Open Tab
	Wait Until Page Contains Element	pass_prot
	Click Element	pass_prot
	Wait Until Page Contains Element	file_pass
	Input Text	file_pass	${FILEPASSWORD}
	Input Text	file_pass_conf	${FILEPASSWORD}
	GUI::Basic::Save

SUITE:Status is 'Successful'
	Sleep	10
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/div/div[2]/table/tbody/tr[1]/td[5]/span/div
	${CONTAINTEST}=	Run Keyword And return Status	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/div/div[2]/table/tbody/tr[1]/td[5]/span/div	Successful
	Run Keyword If	${CONTAINTEST} == False 	Reload Page
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/div/div[2]/table/tbody/tr[1]/td[5]/span/div
	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/div/div[2]/table/tbody/tr[1]/td[5]/span/div	Successful

SUITE:Apply the Profile from Devices
	Click Element	xpath=//button[contains(.,'DEVICES')]
	Wait Until Page Contains Element	//input[contains(@placeholder,'Search Hostname, Serial Number, Profile in Use')]
	Input Text	//input[contains(@placeholder,'Search Hostname, Serial Number, Profile in Use')]	${SERIALNUMBER}
	Sleep	2
	Click Element	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	Click Button	function-button-0
	Sleep	5
	Wait Until Page Contains Element	xpath=(//input[@id='search-input'])[2]
	Input Text	xpath=(//input[@id='search-input'])[2]	test_profile_automation
	Sleep	2
	Click Element	//*[@id="ca-services-monitoring-device-wrapper"]/div[2]/div[2]/div/div/table/tbody/tr[1]/td[1]/span/input
	Click Button	apply-btn
	Wait Until Page Contains	${PROFILE_SETTED_SUCCESS}	30s
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/div/div[2]/table/tbody/tr[1]/td[9]/span/div	${PROFILENAME}
	Wait Until Keyword Succeeds	5 minutes	20 seconds	SUITE:Status is 'Successful'

SUITE:Unselect File Protection
	GUI::Basic::Open And Login Nodegrid	${USERNAMENG}	${PASSWORDNG}
	GUI::Basic::Security::Services::Open Tab
	${CHECK}=	Run Keyword and Return Status	Checkbox Should Be Selected	pass_prot
	Run Keyword If	${CHECK} == True 	Unselect Checkbox	pass_prot
	GUI::Basic::Save

SUITE:Delete the Plugin
	[Arguments]	${NAMEPLUGIN}
	SUITE:Setup
	SUITE:Login on Service Monitoring
	Click Element	xpath=//button[contains(.,'PLUGINS')]
	sleep	2s
	Wait Until Page Contains Element	//input[contains(@placeholder,'Search Plugin, Category, Description')]
	Input Text	//input[contains(@placeholder,'Search Plugin, Category, Description')]	${NAMEPLUGIN}
	Sleep	2
	Click Element	xpath=//div[contains(.,'${NAMEPLUGIN}')]/../../td[1]
	Click Button	function-button-1
	Click Button	confirm-btn
	Wait Until Page Contains	Plugin deleted successfully

SUITE:Create Profile
	SUITE:Setup
	SUITE:Login on Service Monitoring
	Click Element	xpath=//button[contains(.,'PROFILES')]
	Wait Until Page Contains Element	//*[@id="function-button-0"]
	Click Element	//*[@id="function-button-0"]
	Sleep	5
	Wait Until Page Contains Element	profile-name
	Input Text	profile-name	${PROFILENAME}
	Input Text	profile-description	Testing profile
	Input Text	ca-service-monitoring-new-template-search-available-plugins	${PLUGINNAME}
	Sleep	5s
	Wait Until Page Contains Element	//*[@id="ca-service-monitoring-profile-dialog"]/div/div/div[2]/div/div[2]/div/div[1]/div/div[2]/div/div/div/div[1]/div/div[1]/span	30s
	Click Element	//*[@id="ca-service-monitoring-profile-dialog"]/div/div/div[2]/div/div[2]/div/div[1]/div/div[2]/div/div/div/div[1]/div/div[1]/span
	Wait Until Page Contains Element	add-profile-btn	30s
	Click Button	add-profile-btn
	Sleep	2
	Click Element	//*[@id="apply-btn"]
	Wait Until Page Contains	Profile saved successfully
	Click Element	xpath=//button[contains(.,'PROFILES')]
	Wait Until Page Contains Element	//input[contains(@placeholder,'Search Profile, Description')]
	Input Text	//input[contains(@placeholder,'Search Profile, Description')]	${PROFILENAME}
	Sleep	2
	Wait Until Page Contains	${PROFILENAME}	30s

SUITE:Create Plugin
	SUITE:Setup
	SUITE:Login on Service Monitoring
	Click Element	xpath=//button[contains(.,'PLUGINS')]
	Wait Until Page Contains Element	//*[@id="function-button-0"]
	Click Element	//*[@id="function-button-0"]
	Wait Until Page Contains Element	plugin-name
	Input Text	plugin-name	${PLUGINNAME}
	Input Text	plugin-description	Testing plugin
	${PLUGIN}=	OperatingSystem.Get File	${FIRSTPLUGINPATH}
	Input Text	plugin-code	${PLUGIN}
	Sleep	2
	Click Button	submit-btn
	Wait Until Page Contains	Plugin created successfully
	Click Element	xpath=//button[contains(.,'PLUGINS')]
	Wait Until Page Contains Element	//input[contains(@placeholder,'Search Plugin, Category, Description')]
	Input Text	//input[contains(@placeholder,'Search Plugin, Category, Description')]	${PLUGINNAME}
	Sleep	2
	Wait Until Page Contains	${PLUGINNAME}	30s

SUITE:Delete Profile
	SUITE:Setup
	SUITE:Login on Service Monitoring
	Click Element	xpath=//button[contains(.,'PROFILES')]
	sleep	2s
	Wait Until Page Contains Element	//input[contains(@placeholder,'Search Profile, Description')]
	Input Text	//input[contains(@placeholder,'Search Profile, Description')]	${PROFILENAME}
	Wait Until Page Contains	${PROFILENAME}	15s
	Click Element	xpath=//div[contains(.,'${PROFILENAME}')]/../../td[1]
	Click Button	function-button-1
	Click Button	confirm-btn
	Wait Until Page Contains	Profile deleted successfully

SUITE:Clone Plugin
	SUITE:Setup
	SUITE:Login on Service Monitoring
	Click Element	xpath=//button[contains(.,'PLUGINS')]
	Wait Until Page Contains Element	//input[contains(@placeholder,'Search Plugin, Category, Description')]
	Input Text	//input[contains(@placeholder,'Search Plugin, Category, Description')]	${PLUGINNAME}
	Wait Until Page Contains	${PLUGINNAME}	15s
	Click Element	xpath=//div[contains(.,'${PLUGINNAME}')]/../../td[1]
	Click Button	function-button-2
	Wait Until Page Contains Element	//*[@id="edit-plugin-name"]/div/div/input
	Press Keys	//*[@id="edit-plugin-name"]/div/div/input	CTRL+a+BACKSPACE
	Input Text	//*[@id="edit-plugin-name"]/div/div/input	${CLONENAME}
	Press Keys	//*[@id="edit-plugin-description"]/div/div/textarea[1]	CTRL+a+BACKSPACE
	Input Text	//*[@id="edit-plugin-description"]/div/div/textarea[1]	Test Plugin clone
	Sleep	2
	Click Element	edit-dialog-save-btn
	Wait Until Page Contains	Plugin created successfully

SUITE:Check device before applying profile
	GUI::Basic::Open And Login Nodegrid	${USERNAMENG}	${PASSWORDNG}
	GUI::Security::Open Services tab
	${CHECK}=	run keyword and return status	Checkbox Should Be Selected	pass_prot
	Run Keyword If	${CHECK} == True 	Click Element 	pass_prot
	Run Keyword If	${CHECK} == True 	Click Element	saveButton
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	SUITE:Setup
	GUI::ZPECloud::Devices::Open Tab
	sleep	5s
	Wait Until Keyword Succeeds	120 	4	GUI::ZPECloud::Devices::Enrolled::Page Should Contain Device With Online Status

SUITE:Verify if Device is Enrolled
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Wait until Page Contains	${SERIALNUMBER}

SUITE:Check That Devices::Enrolled Page Contain Online Devices
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Wait Until Page Contains	Online	15s
