*** Settings ***
Documentation	Tests to apply, restore and make backup persistent
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	WEB	BACKUP
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_4

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}

*** Test Cases ***
Backup Device Configuration - File Protection:None - File Storage:Temporary
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Backup Device Configuration
	Click Element	//*[@id="opt-none"]
	Click Element	//*[@id="opt-temporary"]
	GUI::ZPECloud::Devices::Backup::Apply Configuration
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Backup::Check if backup has been started	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Check if backup is Successful	Temporary
	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	[Teardown]	SUITE:Teardown

Backup Device Configuration - Make it Persistent
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Devices::Backup Device Configuration
	Click Element	//*[@id="opt-none"]
	Click Element	//*[@id="opt-temporary"]
	GUI::ZPECloud::Devices::Backup::Apply Configuration
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Backup::Check if backup has been started	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Check if backup is Successful	Temporary
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Backup::Open Tab
	Wait Until Page Contains Element	//*[@id="ca-profiles-bkp-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input	timeout=30s
	Element Should Contain	//*[@id="ca-profiles-bkp-table-1"]/div/div/table/tbody/tr[1]/td[7]	Temporary
	Click Element	//*[@id="ca-profiles-bkp-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	GUI::ZPECloud::Basic::Click Button	persistent
	Wait Until Page Contains	Backups are updated successfully
	Wait Until Keyword Succeeds	1m	3s	SUITE:Check if backup changed to Persistent
	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	[Teardown]	SUITE:Teardown

Backup Device Configuration - Restore
	[Tags]	NON-CRITICAL	BACKUP_RESTORE
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Devices::Backup Device Configuration
	Click Element	//*[@id="opt-none"]
	Click Element	//*[@id="opt-temporary"]
	GUI::ZPECloud::Devices::Backup::Apply Configuration
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Backup::Check if backup has been started	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Check if backup is Successful	Temporary
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Profiles::Backup::Restore
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Backup::Check if backup has been started	${EMAIL}	${PWD}	Yes
	GUI::ZPECloud::Profiles::Operation::Check if backup is Successful	Temporary
	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	[Teardown]	SUITE:Teardown

Backup Device Configuration - File Protection:None - File Storage:Persistent
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Devices::Backup Device Configuration
	Click Element	//*[@id="opt-none"]
	Click Element	//*[@id="opt-persistant"]
	GUI::ZPECloud::Devices::Backup::Apply Configuration
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Backup::Check if backup has been started	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Check if backup is Successful	Persistent
	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	[Teardown]	SUITE:Teardown

Backup Device Configuration - File Protection:TPM Encrypted - File Storage:Temporary
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Devices::Backup Device Configuration
	Click Element	//*[@id="opt-tpm"]
	Click Element	//*[@id="opt-temporary"]
	GUI::ZPECloud::Devices::Backup::Apply Configuration
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Backup::Check if backup has been started	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Check if backup is Successful	Temporary
	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	[Teardown]	SUITE:Teardown

Backup Device Configuration - File Protection:TPM Encrypted - File Storage:Persistent
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Devices::Backup Device Configuration
	Click Element	//*[@id="opt-tpm"]
	Click Element	//*[@id="opt-persistant"]
	GUI::ZPECloud::Devices::Backup::Apply Configuration
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Backup::Check if backup has been started	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Check if backup is Successful	Persistent
	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	[Teardown]	SUITE:Teardown

Backup Device Configuration - File Protection:Password Encrypted - File Storage:Temporary
	[Tags]	NON-CRITICAL	BUG_CLOUD-2287
	GUI::ZPECloud::Security::Services::Disable file protection on NG device if needed	${USERNG}	${PWDNG}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Security::Services::Enable File Protection on NG Device	${USERNG}	${PWDNG}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Backup Device Configuration
	Click Element	//*[@id="opt-password-enc"]
	Click Element	//*[@id="opt-temporary"]
	GUI::ZPECloud::Devices::Backup::Apply Configuration
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Backup::Check if backup has been started	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Check if backup is Successful	Temporary
	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	[Teardown]	SUITE:Test Teardown

Backup Device Configuration - File Protection:Password Encrypted - File Storage:Persistent
	[Tags]	NON-CRITICAL	BUG_CLOUD-2287
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Security::Services::Enable File Protection on NG Device	${USERNG}	${PWDNG}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Backup Device Configuration
	Click Element	//*[@id="opt-password-enc"]
	Click Element	//*[@id="opt-persistant"]
	GUI::ZPECloud::Devices::Backup::Apply Configuration
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Backup::Check if backup has been started	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Check if backup is Successful	Persistent
	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	[Teardown]	SUITE:Test Teardown

*** Keywords ***
SUITE:Setup
	Wait Until Keyword Succeeds	3x	3s	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Teardown
	SUITE:Setup
	GUI::ZPECloud::Profiles::Operation::Open Tab
	GUI::ZPECloud::Schedules::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	(//*/tr/td)[1]	60s
	${HAS_SCHEDULE}	Run Keyword And Return Status	Page Should Contain Element	(//*/tr/td/div)[1]
	Run Keyword If	${HAS_SCHEDULE}	GUI::ZPECloud::Profiles::Operation::Schedule::Delete Schedule	${EMAIL}	${PWD}
	SUITE:Setup
	GUI::ZPECloud::Profiles::Backup::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	(//*/tr/td)[1]	60s
	${HAS_BACKUP}	Run Keyword And Return Status	Page Should Contain Element	(//*/tr/td/span/input)[1]
	Run Keyword If	${HAS_BACKUP}	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	Close All Browsers
	Close All Connections

SUITE:Test Teardown
	SUITE:Teardown
	GUI::ZPECloud::Security::Services::Disable file protection on NG device if needed	${USERNG}	${PWDNG}

SUITE:Check if backup changed to Persistent
	GUI::ZPECloud::Profiles::Backup::Open Tab
	Element Should Contain	//*[@id="ca-profiles-bkp-table-1"]/div/div/table/tbody/tr[1]/td[7]	Persistent