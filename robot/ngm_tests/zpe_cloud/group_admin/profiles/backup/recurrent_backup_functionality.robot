*** Settings ***
Documentation	Tests for recurrent backup feature
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	RECURRENT_BACKUP
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_4

*** Variables ***
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${CONNECTION_NAME}	test_connection
${CONNECTION_LOCATOR}	xpath=//tr[@id='${CONNECTION_NAME}']/td/input

*** Settings ***
Suite Setup	SUITE:Teardown
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test Recurrent Backup - File Protection:None - File Storage:Temporary
	SUITE:Test Setup
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Devices::Backup Device Configuration
	Wait Until Page Contains Element	//*[@id="opt-none"]	15s
	Click Element	//*[@id="opt-none"]
	Click Element	//*[@id="opt-temporary"]
	GUI::ZPECloud::Devices::Backup::Schedule Backup
	GUI::ZPECloud::Devices::Backup::Apply Configuration
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Backup::Check if backup has been started	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Check if backup is Successful	Temporary
	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Schedule::Delete Schedule	${EMAIL}	${PWD}
	[Teardown]	SUITE:Teardown

Test Recurrent Backup - File Protection:None - File Storage:Persistent
	SUITE:Test Setup
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Devices::Backup Device Configuration
	Click Element	//*[@id="opt-none"]
	Click Element	//*[@id="opt-persistant"]
	GUI::ZPECloud::Devices::Backup::Schedule Backup
	GUI::ZPECloud::Devices::Backup::Apply Configuration
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Backup::Check if backup has been started	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Check if backup is Successful	Persistent
	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Schedule::Delete Schedule	${EMAIL}	${PWD}
	[Teardown]	SUITE:Teardown

Test Recurrent Backup - File Protection:TPM Encrypted - File Storage:Temporary
	SUITE:Test Setup
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Devices::Backup Device Configuration
	Click Element	//*[@id="opt-tpm"]
	Click Element	//*[@id="opt-temporary"]
	GUI::ZPECloud::Devices::Backup::Schedule Backup
	GUI::ZPECloud::Devices::Backup::Apply Configuration
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Backup::Check if backup has been started	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Check if backup is Successful	Temporary
	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Schedule::Delete Schedule	${EMAIL}	${PWD}
	[Teardown]	SUITE:Teardown

Test Recurrent Backup - File Protection:TPM Encrypted - File Storage:Persistent
	SUITE:Test Setup
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Devices::Backup Device Configuration
	Click Element	//*[@id="opt-tpm"]
	Click Element	//*[@id="opt-persistant"]
	GUI::ZPECloud::Devices::Backup::Schedule Backup
	GUI::ZPECloud::Devices::Backup::Apply Configuration
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Backup::Check if backup has been started	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Check if backup is Successful	Persistent
	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Schedule::Delete Schedule	${EMAIL}	${PWD}
	[Teardown]	SUITE:Teardown

Test Recurrent Backup - File Protection:Password Encrypted - File Storage:Temporary
	SUITE:Test Setup
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Security::Services::Enable File Protection on NG Device	${USERNG}	${PWDNG}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Backup Device Configuration
	Click Element	//*[@id="opt-password-enc"]
	Click Element	//*[@id="opt-temporary"]
	GUI::ZPECloud::Devices::Backup::Schedule Backup
	GUI::ZPECloud::Devices::Backup::Apply Configuration
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Backup::Check if backup has been started	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Check if backup is Successful	Temporary
	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Schedule::Delete Schedule	${EMAIL}	${PWD}
	[Teardown]	SUITE:Test Teardown

Test Recurrent Backup - File Protection:Password Encrypted - File Storage:Persistent
	SUITE:Test Setup
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Security::Services::Enable File Protection on NG Device	${USERNG}	${PWDNG}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Backup Device Configuration
	Click Element	//*[@id="opt-password-enc"]
	Click Element	//*[@id="opt-persistant"]
	GUI::ZPECloud::Devices::Backup::Schedule Backup
	GUI::ZPECloud::Devices::Backup::Apply Configuration
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Backup::Check if backup has been started	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Check if backup is Successful	Persistent
	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Schedule::Delete Schedule	${EMAIL}	${PWD}
	[Teardown]	SUITE:Test Teardown

*** Keywords ***
SUITE:Test Setup
	Wait Until Keyword Succeeds	3x	3s	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Teardown
	Wait Until Keyword Succeeds	3x	5s	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::ZPECloud::Security::Services::Disable file protection on NG device if needed on current browser window
	GUI::ZPECloud::Security::Firewall::Delete firewall rule if needed
	GUI::ZPECloud::Network::Settings::Disable failover if needed
	GUI::ZPECloud::Network::Connection::Delete connection if needed	${CONNECTION_NAME}	${CONNECTION_LOCATOR}
	GUI::Cloud::Verify if device is enrolled and Online
	SUITE:Test Setup
	GUI::ZPECloud::Profiles::Operation::Open Tab
	GUI::ZPECloud::Schedules::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	(//*/tr/td)[1]	60s
	${HAS_SCHEDULE}	Run Keyword And Return Status	Page Should Contain Element	(//*/tr/td/div)[1]
	Run Keyword If	${HAS_SCHEDULE}	GUI::ZPECloud::Profiles::Operation::Schedule::Delete Schedule	${EMAIL}	${PWD}
	SUITE:Test Setup
	GUI::ZPECloud::Profiles::Backup::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	(//*/tr/td)[1]	60s
	${HAS_BACKUP}	Run Keyword And Return Status	Page Should Contain Element	(//*/tr/td/span/input)[1]
	Run Keyword If	${HAS_BACKUP}	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	Close All Browsers

SUITE:Test Teardown
	SUITE:Teardown
	GUI::ZPECloud::Security::Services::Disable file protection on NG device if needed	${USERNG}	${PWDNG}