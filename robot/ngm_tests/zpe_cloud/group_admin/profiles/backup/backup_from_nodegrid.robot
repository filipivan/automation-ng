*** Settings ***
Documentation	Tests to apply Backup From Nodegrid
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	WEB	BACKUP_FROM_NODEGRID
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_4

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}

*** Test Cases ***
Get Previous Backups
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	[Teardown]	Close All Browsers

Nodegrid Configuration With security:None and Cloud Verification
	SUITE:Backup Nodegrid Configuration with security:None
	SUITE:Setup
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Backup::Check if backup has been started	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Check if backup is Successful	Temporary
	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	[Teardown]	SUITE:Teardown

Nodegrid Configuration With security:TPM and Cloud Verification
	SUITE:Setup
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	SUITE:Backup Nodegrid Configuration with security:TPM
	SUITE:Setup
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Backup::Check if backup has been started	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Check if backup is Successful	Temporary
	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	[Teardown]	SUITE:Teardown

Nodegrid Configuration With security:Password and Cloud Verification
	GUI::ZPECloud::Security::Services::Enable File Protection on NG Device	${USERNG}	${PWDNG}
	SUITE:Setup
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	SUITE:Backup Nodegrid Configuration with security:Password
	SUITE:Setup
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Backup::Check if backup has been started	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Check if backup is Successful	Temporary
	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	GUI::ZPECloud::Security::Services::Disable file protection on NG device if needed	${USERNG}	${PWDNG}
	[Teardown]	SUITE:Test Teardown

*** Keywords ***
SUITE:Setup
	Wait Until Keyword Succeeds	3x	3s	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Teardown
	SUITE:Setup
	GUI::ZPECloud::Profiles::Operation::Open Tab
	GUI::ZPECloud::Schedules::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	(//*/tr/td)[1]	60s
	${HAS_SCHEDULE}	Run Keyword And Return Status	Page Should Contain Element	(//*/tr/td/div)[1]
	Run Keyword If	${HAS_SCHEDULE}	GUI::ZPECloud::Profiles::Operation::Schedule::Delete Schedule	${EMAIL}	${PWD}
	SUITE:Setup
	GUI::ZPECloud::Profiles::Backup::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	(//*/tr/td)[1]	60s
	${HAS_BACKUP}	Run Keyword And Return Status	Page Should Contain Element	(//*/tr/td/span/input)[1]
	Run Keyword If	${HAS_BACKUP}	GUI::ZPECloud::Profiles::Backup::Delete Backup	${EMAIL}	${PWD}
	Close All Browsers

SUITE:Test Teardown
	SUITE:Teardown
	GUI::ZPECloud::Security::Services::Disable file protection on NG device if needed	${USERNG}	${PWDNG}

SUITE:Backup Nodegrid Configuration with security:None
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Basic::System::Toolkit::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	icon_saveSettings	30s
	Click Element	icon_saveSettings
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	savetypeselection	30s
	Wait Until Element Is Visible	savetypeselection
	Select Radio Button	savetypeselection	savesettings-cloud
	Select Radio Button	backupSecurity	security-none
	Wait Until Keyword Succeeds	30	5	GUI::Basic::Save
	[Teardown]	Close All Browsers

SUITE:Backup Nodegrid Configuration with security:TPM
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Basic::System::Toolkit::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	icon_saveSettings	30s
	Click Element	icon_saveSettings
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	savetypeselection	30s
	Wait Until Element Is Visible	savetypeselection
	Select Radio Button	savetypeselection	savesettings-cloud
	Select Radio Button	backupSecurity	security-tpm
	Wait Until Keyword Succeeds	30	5	GUI::Basic::Save
	[Teardown]	Close All Browsers

SUITE:Backup Nodegrid Configuration with security:Password
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Basic::System::Toolkit::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	icon_saveSettings	30s
	Click Element	icon_saveSettings
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	savetypeselection	30s
	Wait Until Element Is Visible	savetypeselection
	Select Radio Button	savetypeselection	savesettings-cloud
	Select Radio Button	backupSecurity	security-password
	Wait Until Keyword Succeeds	30	5	GUI::Basic::Save
	[Teardown]	Close All Browsers