*** Settings ***
Documentation	Tests for testing dynamic template functionality testing
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	APPLY_DYNAMIC_TEMPLATE_FUNCTIONALITY	WEB APPLY_CONFIGURATION
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_4

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${NAME}	test_dynamic_configuration
${TEMPLATE_NAME}	test_dynamic_configuration_template
${DESCRIPTION}	this code to get device version using dynamic template
${CONFIGURATIONADDED}	Configuration added successfully
${MYPATH}	${CURDIR}/dynamic_template_files/test_dynamic_template_script.txt
${MYPATH_configuration}	${CURDIR}/dynamic_template_files/test_dynamic_template_configuration.txt
${TYPESELECTION}	//*[@id="template-type-option-0"]
${TYPESELECTION_FOR_SCRIPT_TEMPLATE}	//*[@id="template-type-option-1"]

${CONFIGURATION_PASSWORD_PROTECTION}	zpe#123
${TYPE}	CONFIGURATION
${DEVICE}	nodegrid
${DELETE_CONFIGURATION}	Profile deleted successfully
${DELETE_TEMPLATE}	Template deleted successfully

${CODE}	${LINE1}	${\n}	${LINE2}
${LINE1}	echo "What's the device version?"
${LINE2}	echo "The device version is {{ device.version }}!"

${DYNAMIC_TEMPLATE_VARIABLE}	NEWHOSTNAME
${DYNAMIC_TEMPLATE_VALUE}	test1
${NEWHOSTNAME}	${DYNAMIC_TEMPLATE_VALUE}

${DYNAMIC_DEFAULT_VARIABLE}	device.version

${CODE1}	${CONFIG_LINE1}	${\n}	${CONFIG_LINE2}	${\n}	${CONFIG_LINE3}
${CONFIG_LINE1}	cd /settings/network_settings/
${CONFIG_LINE2}	set hostname={{${SPACE}${DYNAMIC_TEMPLATE_VARIABLE}${SPACE}}}
${CONFIG_LINE3}	commit

${CUSTOM_VARIABLE_CREATION_SUCCESS}	Custom field added successfully
${CUSTOM_VARIABLE_DISABLE}	Custom fields disabled successfully
${CUSTOM_VARIABLE_ENABLE}	Custom fields enabled successfully

*** Test Cases ***
Apply the dynamic template script on NG
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Custom_Fields::Open Tab
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${DYNAMIC_DEFAULT_VARIABLE}
	sleep	2s
	${HASVARIBLE}=	Run Keyword and Return Status	Wait Until Page Contains Element	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr[1]/td[1]/span/input	15s
	Run Keyword if	${HASVARIBLE} == False	SUITE:Enable default variable
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${NAME}
	Sleep	5
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete
	SUITE:Add script	${NAME}	${DESCRIPTION}	${MYPATH}
	SUITE:Apply configuration	${NAME}
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Jobs::Check if job has been started	${EMAIL}	${PWD}	SCRIPT
	GUI::ZPECloud::Profiles::Operation::Check if job is Successful	${EMAIL}	${PWD}	SCRIPT
	[Teardown]	SUITE:Teardown1

Apply the dynamic template configuration on NG
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Profiles::Custom_Fields::Open Tab
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${DYNAMIC_TEMPLATE_VARIABLE}
	sleep	2s
	${HASVARIBLE}=	Run Keyword and Return Status	Wait Until Page Contains Element	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr[1]/td[1]/span/input	15s
	Run Keyword if	${HASVARIBLE} == False	SUITE:Add variable with global scope
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${NAME}
	Sleep	5
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete
	SUITE:Add configuration	${NAME}	${DESCRIPTION}	${MYPATH_configuration}
	SUITE:Apply configuration	${NAME}
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Jobs::Check if job has been started	${EMAIL}	${PWD}	SCRIPT
	GUI::ZPECloud::Profiles::Operation::Check if job is Successful	${EMAIL}	${PWD}	SCRIPT
	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]
	Wait Until Keyword Succeeds	160	20s	SUITE:Hostname is 'Test'
	[Teardown]	SUITE:Teardown2

Apply the dynamic template configuration on NG - Add From Template
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Profiles::Custom_Fields::Open Tab
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${DYNAMIC_TEMPLATE_VARIABLE}
	sleep	2s
	${HASVARIBLE}=	Run Keyword and Return Status	Wait Until Page Contains Element	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr[1]/td[1]/span/input	15s
	Run Keyword if	${HASVARIBLE} == False	SUITE:Add variable with global scope
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${NAME}
	Sleep	5
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete
	GUI::ZPECloud::Profiles::Template::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${TEMPLATE_NAME}
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${TEMPLATE_NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete Template
	GUI::ZPECloud::Profiles::Template::Add	${CODE1}	${TEMPLATE_NAME}	${DESCRIPTION}	${TYPESELECTION}
	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	SUITE:Add Configuration From Template	${CONFIG_LINE1}
	SUITE:Apply configuration	${NAME}
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Jobs::Check if job has been started	${EMAIL}	${PWD}	CONF
	GUI::ZPECloud::Profiles::Operation::Check if job is Successful	${EMAIL}	${PWD}	CONF
	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]
	Wait Until Keyword Succeeds	160	20s	SUITE:Hostname is 'Test'
	[Teardown]	SUITE:Teardown2

Apply The Dynamic Template Script On NG - Add From Template
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Profiles::Custom_Fields::Open Tab
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${DYNAMIC_DEFAULT_VARIABLE}
	sleep	2s
	${HASVARIBLE}=	Run Keyword and Return Status	Wait Until Page Contains Element	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr[1]/td[1]/span/input	15s
	Run Keyword if	${HASVARIBLE} == False	SUITE:Enable default variable
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${NAME}
	Sleep	5
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete
	GUI::ZPECloud::Profiles::Template::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${TEMPLATE_NAME}
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${TEMPLATE_NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete Template
	GUI::ZPECloud::Profiles::Template::Add	${CODE}	${TEMPLATE_NAME}	${DESCRIPTION}	${TYPESELECTION_FOR_SCRIPT_TEMPLATE}
	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	SUITE:Add Script From Template
	SUITE:Apply configuration	${NAME}
	Wait Until Keyword Succeeds	5 min	10 sec	GUI::ZPECloud::Devices::Jobs::Check if job has been started	${EMAIL}	${PWD}	CONF
	GUI::ZPECloud::Profiles::Operation::Check if job is Successful	${EMAIL}	${PWD}	CONF
	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	[Teardown]	SUITE:Teardown1

*** Keywords ***
SUITE:Setup
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud

SUITE:Teardown
	SUITE:Setup
	Close All Browsers

SUITE:Teardown1
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Custom_Fields::Open Tab
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${DYNAMIC_DEFAULT_VARIABLE}
	sleep	2s
	${ELEMENT}=	Run Keyword And return Status	Wait Until Page Contains Element	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Run Keyword If	${ELEMENT} == True	SUITE:Disable default variable
	GUI::ZPECloud::Profiles::Template::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${TEMPLATE_NAME}
	sleep	5
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${TEMPLATE_NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete Template
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${NAME}
	Sleep	5
	${ELEMENT}=	Run Keyword And return Status	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Run Keyword If	${ELEMENT} == True	SUITE:Delete
	Close All Browsers

SUITE:Teardown2
	Wait Until Keyword Succeeds	30	2	Set hostname as 'nodegrid'
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]	30s
	Wait Until Keyword Succeeds	160	2	SUITE:Hostname is 'Nodegrid'
	GUI::ZPECloud::Profiles::Custom_Fields::Open Tab
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${DYNAMIC_TEMPLATE_VARIABLE}
	sleep	2s
	${ELEMENT}=	Run Keyword And return Status	Wait Until Page Contains Element	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Run Keyword If	${ELEMENT} == True	SUITE:Delete_Variable
	GUI::ZPECloud::Profiles::Template::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${TEMPLATE_NAME}
	sleep	5
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${TEMPLATE_NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete Template
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${NAME}
	Sleep	5
	${ELEMENT}=	Run Keyword And return Status	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Run Keyword If	${ELEMENT} == True	SUITE:Delete
	Close All Browsers

SUITE:Hostname is 'Nodegrid'
	Reload Page
	Sleep	5
	${CONTAINTEST}=	Run Keyword And return Status	Page Should Contain	${DEVICE}
	Run Keyword If	${CONTAINTEST} == False	Reload Page
	Sleep	10
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	${DEVICE}

Set hostname as 'nodegrid'
	CLI:Open	${USERNAMENG}	${PASSWORDNG}
	CLI:Enter Path	/settings/network_settings/
	CLI:Set Field	hostname	${DEVICE}
	CLI:Commit
	CLI:Close Connection

SUITE:Hostname is 'Test'
	Reload Page
	Sleep	10
	${CONTAINTEST}=	Run Keyword And return Status	Page Should Contain	${NEWHOSTNAME}
	Run Keyword If	${CONTAINTEST} == False	Reload Page
	Sleep	5
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	${NEWHOSTNAME}

SUITE:Add variable with global scope
	GUI::ZPECloud::Profiles::Custom_Fields::Open Tab
	Wait Until Page Contains Element	xpath=//button[contains(.,'NEW')]	30s
	Click Element	xpath=//button[contains(.,'NEW')]
	Input Text	id=custom-field-name	${DYNAMIC_TEMPLATE_VARIABLE}
	Input Text	id=custom-field-value	${DYNAMIC_TEMPLATE_VALUE}
	sleep	2s
	Click Element	xpath=//button[contains(.,'SAVE')]
	sleep	2s
	GUI::ZPECloud::Profiles::Custom_Fields::Open Tab
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${DYNAMIC_TEMPLATE_VARIABLE}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr/td[1]/span/input	15s

SUITE:Delete_Variable
	Click Element	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr/td[1]/span/input
	Wait Until Page Contains Element	xpath=//button[contains(.,'DELETE')]	15s
	Click Element	xpath=//button[contains(.,'DELETE')]
	Wait Until Page Contains Element	xpath=//button[contains(.,'YES')]	15s
	Click Element	xpath=//button[contains(.,'YES')]

SUITE:Enable default variable
	GUI::ZPECloud::Profiles::Custom_Fields::Open Tab
	Wait Until Page Contains Element	xpath=//button[contains(.,'DYNAMIC VARIABLES')]
	Click Element	xpath=//button[contains(.,'DYNAMIC VARIABLES')]
	sleep	5s
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${DYNAMIC_DEFAULT_VARIABLE}
	sleep	5s
	Wait Until Page Contains Element	//*[@id="ca-table-"]/div/div/table/tbody/tr[1]/td[1]/span/input	30s
	Click Element	//*[@id="ca-table-"]/div/div/table/thead/tr/th[1]/span/input
	sleep	2s
	Wait Until Page Contains Element	xpath=//button[contains(.,'ENABLE')]
	Click Element	xpath=//button[contains(.,'ENABLE')]
	Wait Until Page Contains Element	xpath=//button[contains(.,'YES')]
	Click Element	xpath=//button[contains(.,'YES')]
	Wait Until Page Contains	${CUSTOM_VARIABLE_ENABLE}	30s
	GUI::ZPECloud::Profiles::Custom_Fields::Open Tab
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${DYNAMIC_DEFAULT_VARIABLE}
	sleep	2s
	Wait Until Page Contains Element	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr/td[1]/span/input	15s

SUITE:Disable default variable
	Wait Until Page Contains Element	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr[1]/td[1]/span/input	15s
	Click Element	//*[@id="ca-table-custom-fields"]/div/div/table/thead/tr/th[1]/span/input
	sleep	2s
	Wait Until Page Contains Element	xpath=//button[contains(.,'DISABLE')]
	Click Element	xpath=//button[contains(.,'DISABLE')]
	Wait Until Page Contains Element	xpath=//button[contains(.,'YES')]
	Click Element	xpath=//button[contains(.,'YES')]
	Wait Until Page Contains	${CUSTOM_VARIABLE_DISABLE}	30s

SUITE:Apply configuration
	[Arguments]	${NAME}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Click Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Sleep	2
	Click Element	function-button-0
	Sleep	5
	Wait Until Page Contains Element	ca-profiles-config-table-1
	Input Text	//*[@id="ca-profiles-config-search"]/div/div/input	${NAME}
	Sleep	5
	Select Checkbox	xpath=//div[@id='ca-profiles-config-table-1']/div/div/table/tbody/tr/td[1]/span/input
	Click Element	xpath=(//button[@id='function-button-1'])[2]
	Wait Until Page Contains	Apply configuration on the selected devices was requested	timeout=30s

SUITE:Delete
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	20s
	Select Checkbox	//*[@id="ca-profiles-config-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	sleep	2s
	Wait Until Element Is Enabled	//*[@id="function-button-5"]
	Click Element	//*[@id="function-button-5"]
	Sleep	2
	Page Should Contain Element	//*[@id="ca-profiles-config-delete-dialog"]/div[3]/div
	Click Element	//*[@id="confirm-btn"]
	Wait Until Page Contains	${DELETE_CONFIGURATION}	30s

SUITE:Delete Template
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]/main/div[1]/div[2]/div[3]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
	Select Checkbox	//*[@id="customer-admin-layout"]/main/div[1]/div[2]/div[3]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
	sleep	2s
	Click Element	xpath=//div[@id='common-functions-toolbar-wrapper']/div/button[3]
	Page Should Contain Element	alert-dialog-title
	Click Element	//*[@id="confirm-btn"]
	Wait Until Page Contains	${DELETE_TEMPLATE}	30s

SUITE:Add Configuration From Template
	[Arguments]	${LINE}
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	//*[@id="function-button-2"]
	Click Element	//*[@id="function-button-2"]
	Wait Until Page Contains Element	input-name	15s
	Input Text	input-name	${NAME}
	Input Text	input-description	${DESCRIPTION}
	sleep	5s
	Wait Until Page Contains Element	css=#mui-component-select-template
	Click Element	css=#mui-component-select-template
	Wait Until Page Contains Element	xpath=//li[contains(.,'${TEMPLATE_NAME}')]	15s
	Click Element	xpath=//li[contains(.,'${TEMPLATE_NAME}')]
	Sleep	5
	Element Should Contain	xpath=//textarea	${LINE}
	Wait Until Page Contains Element	xpath=//input[@name='dynamic']
	Select Checkbox	xpath=//input[@name='dynamic']
	SUITE:Save Configuration From Template

SUITE:Add Script From Template
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	//*[@id="function-button-2"]	15s
	Click Element	//*[@id="function-button-2"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	input-name	30s
	GUI::Basic::Spinner Should Be Invisible
	Input Text	input-name	${NAME}
	Input Text	input-description	${DESCRIPTION}
	sleep	5s
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	css=#mui-component-select-template
	Click Element	css=#mui-component-select-template
	Wait Until Page Contains Element	xpath=//li[contains(.,'${TEMPLATE_NAME}')]	15s
	Click Element	xpath=//li[contains(.,'${TEMPLATE_NAME}')]
	Sleep	5
	Element Should Contain	xpath=//textarea	${LINE1}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	xpath=//input[@name='dynamic']
	Select Checkbox	xpath=//input[@name='dynamic']
	SUITE:Save Configuration From Template

SUITE:Save Configuration From Template
	GUI::ZPECloud::Basic::Click Button	save
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	${CONFIGURATIONADDED}

SUITE:Status is 'Successful'
	Sleep	10
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div
	${CONTAINTEST}=	Run Keyword And return Status	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div	Successful
	Run Keyword If	${CONTAINTEST} == False	Reload Page
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div
	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div	Successful

SUITE:Add script
	[Arguments]	${NAME}	${DESCRIPTION}	${MYPATH}	${CONFIGURATIONADDED}=Configuration added successfully
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	sleep	2s
	Click Element	xpath=//div[2]/div/button[2]
	Wait Until Page Contains Element	input-name	15s
	Wait Until Element Is Visible	input-name	15s
	GUI::Basic::Spinner Should Be Invisible
	Input Text	input-name	${NAME}
	Input Text	input-description	${DESCRIPTION}
	sleep	2s
	Click Element	mui-component-select-type
	Click Element	opt-SCRIPT
	Choose File	fileinput-file	${MYPATH}
	sleep	2s
	Wait Until Page Contains Element	xpath=//input[@name='dynamic']
	Select Checkbox	xpath=//input[@name='dynamic']
	GUI::ZPECloud::Profiles::Configuration::Save	${NAME}	timeout=30s

SUITE:Add configuration
	[Arguments]	${NAME}	${DESCRIPTION}	${MYPATH}	${CONFIGURATIONADDED}=Configuration added successfully
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	sleep	2s
	Click Element	xpath=//div[2]/div/button[2]
	Wait Until Page Contains Element	input-name	15s
	Click Element	input-name
	Input Text	input-name	${NAME}
	Click Element	input-description
	Input Text	input-description	${DESCRIPTION}
	sleep	2s
	Click Element	mui-component-select-type
	Click Element	opt-CONFIGURATION
	Choose File	fileinput-file	${MYPATH}
	sleep	2s
	Wait Until Page Contains Element	xpath=//input[@name='dynamic']
	Select Checkbox	xpath=//input[@name='dynamic']
	GUI::ZPECloud::Profiles::Configuration::Save	${NAME}	timeout=30s

GUI::ZPECloud::Profiles::Configuration::Save
	[Arguments]	${NAME}	${ADDEDLIST}=//*[@id="ca-profiles-config-table-1"]	${CONFIGURATIONADDED}=Configuration added successfully
	Click Element	//*[@id="submit-btn"]
	Wait Until Page Contains	${CONFIGURATIONADDED}	timeout=30s
	Sleep	5
	Wait Until Page Contains Element	${ADDEDLIST}	timeout=30s
	Table Should Contain	${ADDEDLIST}	${NAME}