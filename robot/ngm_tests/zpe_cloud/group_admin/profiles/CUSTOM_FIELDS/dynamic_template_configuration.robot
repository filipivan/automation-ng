*** Settings ***
Documentation	Tests for testing dynamic template configuration
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	APPLY_DYNAMIC_TEMPLATE_CONFIGURATION
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_4

Suite Setup	SUITE:Setup
Suite Teardown	Close All Browsers

*** Variables ***
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${DEVICE}	nodegrid
${DYNAMIC_TEMPLATE_VARIABLE}	test1
${DYNAMIC_TEMPLATE_SCOPE}	test1
${DYNAMIC_TEMPLATE_DEVICE}	${DEVICE}${SPACE}-${SPACE}${SERIALNUMBER}

${SITE_NAME}	ZPE_SITE
${SITE_ADDRESS}	Rua 7 de Setembro, 1678, Blumenau-SC
${SITE_LATITUDE}	40.777800400000004
${SITE_LONGITUDE}	8.921996955790174
${SITE_ADDED_MESSAGE}	Site added successfully
${SITE_DELETED_MESSAGE}	Site deleted successfully

${GROUP_NAME}	ZPE_GROUP
${CREATE_GROUP}	Group added successfully
${DELETE_GROUP}	Group deleted successfully
${DYNAMIC_DEFAULT_VARIABLE}	device.name

${CUSTOM_VARIABLE_CREATION_SUCCESS}	Custom field added successfully
${CUSTOM_VARIABLE_DISABLE}	Custom fields disabled successfully
${CUSTOM_VARIABLE_ENABLE}	Custom fields enabled successfully

*** Test Cases ***
Dynamic Template-Add New Variable with scope Device
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Custom_Fields::Open Tab
	Wait Until Page Contains Element	xpath=//button[contains(.,'NEW')]	30s
	Click Element	xpath=//button[contains(.,'NEW')]
	Input Text	id=custom-field-name	${DYNAMIC_TEMPLATE_VARIABLE}
	Input Text	id=custom-field-value	${DYNAMIC_TEMPLATE_SCOPE}
	Click Element	//*[@id="ca-apps-sdwan-third-party-tunnel-type"]
	sleep	2s
	Click Element	xpath=//li[contains(.,'Device')]
	Wait Until Element Is Not Visible	//li[contains(.,'Group')]	30s
	Wait Until Page Contains Element	//*[@id="generic-autocomplete"]	30s
	Wait Until Page Contains Element	//button[@title='Clear']//*[name()='svg']	30s
	${HAS_UNDEFINED_TEXT}	Run Keyword And Return Status	Page Should Contain Element	//*[contains(@value,'undefined')]
	Run Keyword If	${HAS_UNDEFINED_TEXT}	SUITE:Remove undefined text from field
	Input Text	//*[@id="generic-autocomplete"]	${DEVICE}
	Wait Until Page Contains Element	xpath=//li[@id='generic-autocomplete-option-0']	30s
	Click Element	xpath=//li[@id='generic-autocomplete-option-0']
	Click Element	xpath=//button[contains(.,'SAVE')]
	sleep	2s
	GUI::ZPECloud::Profiles::Custom_Fields::Open Tab
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${DYNAMIC_TEMPLATE_VARIABLE}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr/td[1]/span/input	15s
	[Teardown]	SUITE:Teardown1

Dynamic Template-Add New Variable with scope Site
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Sites::Open Tab
	SUITE:Add Site
	GUI::ZPECloud::Profiles::Custom_Fields::Open Tab
	Wait Until Page Contains Element	xpath=//button[contains(.,'NEW')]	30s
	Click Element	xpath=//button[contains(.,'NEW')]
	Input Text	id=custom-field-name	${DYNAMIC_TEMPLATE_VARIABLE}
	Input Text	id=custom-field-value	${DYNAMIC_TEMPLATE_SCOPE}
	Click Element	//*[@id="ca-apps-sdwan-third-party-tunnel-type"]
	sleep	2s
	Click Element	xpath=//li[contains(.,'Site')]
	Wait Until Element Is Not Visible	//li[contains(.,'Group')]	30s
	Wait Until Page Contains Element	//*[@id="generic-autocomplete"]	30s
	Wait Until Page Contains Element	//button[@title='Clear']//*[name()='svg']	30s
	${HAS_UNDEFINED_TEXT}	Run Keyword And Return Status	Page Should Contain Element	//*[contains(@value,'undefined')]
	Run Keyword If	${HAS_UNDEFINED_TEXT}	SUITE:Remove undefined text from field
	Input Text	//*[@id="generic-autocomplete"]	${SITE_NAME}
	Wait Until Page Contains Element	xpath=//li[@id='generic-autocomplete-option-0']	30s
	Click Element	xpath=//li[@id='generic-autocomplete-option-0']
	Click Element	xpath=//button[contains(.,'SAVE')]
	sleep	2s
	GUI::ZPECloud::Profiles::Custom_Fields::Open Tab
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${DYNAMIC_TEMPLATE_VARIABLE}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr/td[1]/span/input	15s
	[Teardown]	Run Keywords	SUITE:Teardown1	AND	SUITE:Delete Site

Dynamic Template-Add New Variable with scope Group
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Groups::Open Tab
	${HAVE_GROUP}=	Run Keyword And return Status	SUITE:Find Group
	Run Keyword If	${HAVE_GROUP} == True	SUITE:Delete Group
	${HAVE_GROUP}=	Run Keyword And return Status	SUITE:Find Group
	Run Keyword If	${HAVE_GROUP} == False	SUITE:Create Group
	GUI::ZPECloud::Profiles::Custom_Fields::Open Tab
	Wait Until Page Contains Element	xpath=//button[contains(.,'NEW')]	30s
	Click Element	xpath=//button[contains(.,'NEW')]
	Input Text	id=custom-field-name	${DYNAMIC_TEMPLATE_VARIABLE}
	Input Text	id=custom-field-value	${DYNAMIC_TEMPLATE_SCOPE}
	Click Element	//*[@id="ca-apps-sdwan-third-party-tunnel-type"]
	sleep	2s
	Click Element	xpath=//li[contains(.,'Group')]
	Wait Until Element Is Not Visible	//li[contains(.,'Site')]	30s
	Wait Until Page Contains Element	//*[@id="generic-autocomplete"]	30s
	Wait Until Page Contains Element	//button[@title='Clear']//*[name()='svg']	30s
	${HAS_UNDEFINED_TEXT}	Run Keyword And Return Status	Page Should Contain Element	//*[contains(@value,'undefined')]
	Run Keyword If	${HAS_UNDEFINED_TEXT}	SUITE:Remove undefined text from field
	Input Text	//*[@id="generic-autocomplete"]	${GROUP_NAME}
	Wait Until Page Contains Element	xpath=//li[@id='generic-autocomplete-option-0']	30s
	Click Element	xpath=//li[@id='generic-autocomplete-option-0']
	Click Element	xpath=//button[contains(.,'SAVE')]
	sleep	2s
	GUI::ZPECloud::Profiles::Custom_Fields::Open Tab
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${DYNAMIC_TEMPLATE_VARIABLE}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr/td[1]/span/input	15s
	[Teardown]	Run Keywords	SUITE:Teardown1	AND	SUITE:Delete Group

Dynamic Template-Add New Variable with scope Global (As By Default Scope)
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	SUITE:Add variable with global scope
	[Teardown]	SUITE:Teardown1

Dynamic Template-Add New Variable and edit it
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	SUITE:Add variable with global scope
	Click Element	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr/td[1]/span/input
	Click Element	xpath=//button[contains(.,'EDIT')]
	Wait Until Page Contains Element	id=custom-field-name	30s
	Input Text	id=custom-field-name	${DYNAMIC_TEMPLATE_VARIABLE}
	Input Text	id=custom-field-value	${DYNAMIC_TEMPLATE_SCOPE}
	Click Element	//*[@id="ca-apps-sdwan-third-party-tunnel-type"]
	sleep	2s
	Click Element	xpath=//li[contains(.,'Device')]
	Wait Until Page Contains Element	//*[@id="generic-autocomplete"]
	Input Text	//*[@id="generic-autocomplete"]	${DEVICE}
	Wait Until Page Contains Element	xpath=//li[@id='generic-autocomplete-option-0']	30s
	Click Element	xpath=//li[@id='generic-autocomplete-option-0']
	Click Element	xpath=//button[contains(.,'SAVE')]
	[Teardown]	SUITE:Teardown1

Dynamic Template-Add New Variable and disable it
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	SUITE:Add variable with global scope
	Element Should Contain	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr/td[4]	True
	Click Element	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr/td[1]/span/input
	Wait Until Page Contains Element	xpath=//button[contains(.,'DISABLE')]
	Click Element	xpath=//button[contains(.,'DISABLE')]
	Wait Until Page Contains Element	xpath=//button[contains(.,'YES')]
	Click Element	xpath=//button[contains(.,'YES')]
	Wait Until Page Contains	${CUSTOM_VARIABLE_DISABLE}	30s
	sleep	2s
	GUI::ZPECloud::Profiles::Custom_Fields::Open Tab
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${DYNAMIC_TEMPLATE_VARIABLE}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr/td[1]/span/input	15s
	Element Should Contain	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr/td[4]	False
	[Teardown]	SUITE:Teardown1

Dynamic Template-Add New Variable and enable it
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Custom_Fields::Open Tab
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${DYNAMIC_TEMPLATE_VARIABLE}
	Sleep	5
	Element Should Contain	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr/td[4]	False
	Click Element	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr/td[1]/span/input
	Wait Until Page Contains Element	xpath=//button[contains(.,'ENABLE')]
	Click Element	xpath=//button[contains(.,'ENABLE')]
	Wait Until Page Contains Element	xpath=//button[contains(.,'YES')]
	Click Element	xpath=//button[contains(.,'YES')]
	Wait Until Page Contains	${CUSTOM_VARIABLE_ENABLE}	30s
	sleep	2s
	GUI::ZPECloud::Profiles::Custom_Fields::Open Tab
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${DYNAMIC_TEMPLATE_VARIABLE}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr/td[1]/span/input	15s
	Element Should Contain	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr/td[4]	True
	[Teardown]	SUITE:Teardown1

Add and perform different operation on the dynamic default variable
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Custom_Fields::Open Tab
	Wait Until Page Contains Element	xpath=//button[contains(.,'DYNAMIC VARIABLES')]
	Click Element	xpath=//button[contains(.,'DYNAMIC VARIABLES')]
	sleep	5s
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${DYNAMIC_DEFAULT_VARIABLE}
	sleep	5s
	Wait Until Page Contains Element	//*[@id="ca-table-"]/div/div/table/tbody/tr[1]/td[1]/span/input	30s
	Click Element	//*[@id="ca-table-"]/div/div/table/thead/tr/th[1]/span/input
	sleep	2s
	Wait Until Page Contains Element	xpath=//button[contains(.,'ENABLE')]
	Click Element	xpath=//button[contains(.,'ENABLE')]
	Wait Until Page Contains Element	xpath=//button[contains(.,'YES')]
	Click Element	xpath=//button[contains(.,'YES')]
	Wait Until Page Contains	${CUSTOM_VARIABLE_ENABLE}	30s
	GUI::ZPECloud::Profiles::Custom_Fields::Open Tab
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${DYNAMIC_DEFAULT_VARIABLE}
	sleep	2s
	Wait Until Page Contains Element	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr/td[1]/span/input	15s
	Click Element	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr/td[1]/span/input
	sleep	2s
	GUI::ZPECloud::Basic::Element Status Should Be Disabled	function-button-1
	GUI::ZPECloud::Basic::Element Status Should Be Disabled	function-button-2
	GUI::ZPECloud::Basic::Element Status Should Be Disabled	function-button-3
	Wait Until Page Contains Element	xpath=//button[contains(.,'DISABLE')]
	Click Element	xpath=//button[contains(.,'DISABLE')]
	Wait Until Page Contains Element	xpath=//button[contains(.,'YES')]
	Click Element	xpath=//button[contains(.,'YES')]
	Wait Until Page Contains	${CUSTOM_VARIABLE_DISABLE}	30s
	[Teardown]	SUITE:Teardown1

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Security::Services::Disable file protection on NG device if needed	${USERNG}	${PWDNG}

SUITE:Teardown1
	${ELEMENT}=	Run Keyword And return Status	Page Should Contain Element	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr/td[1]/span/input
	Run Keyword If	${ELEMENT} == True	SUITE:Delete_Variable
	Close All Browsers

SUITE:Add variable with global scope
	GUI::ZPECloud::Profiles::Custom_Fields::Open Tab
	Wait Until Page Contains Element	xpath=//button[contains(.,'NEW')]	30s
	Click Element	xpath=//button[contains(.,'NEW')]
	Input Text	id=custom-field-name	${DYNAMIC_TEMPLATE_VARIABLE}
	Input Text	id=custom-field-value	${DYNAMIC_TEMPLATE_SCOPE}
	sleep	2s
	Click Element	xpath=//button[contains(.,'SAVE')]
	sleep	2s
	GUI::ZPECloud::Profiles::Custom_Fields::Open Tab
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${DYNAMIC_TEMPLATE_VARIABLE}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr/td[1]/span/input	15s

SUITE:Click On Add Site Tab
	Clear Element Text	search-input
	Press Keys	search-input	CTRL+a+BACKSPACE
	sleep	5s
	Wait Until Page Contains Element	//*[@id="function-button-0"]	30s
	Click Element	//*[@id="function-button-0"]
	sleep	5s

SUITE:Find Group
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Groups::Open Tab
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${GROUP_NAME}
	Wait Until Page Contains Element	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr[1]/td[1]/span

SUITE:Create Group
	Reload Page
	sleep	5s
	Wait Until Page Contains Element	function-button-0
	Click Element	function-button-0
	Sleep	5
	Input Text	xpath=//input[@name='name']	${GROUP_NAME}
	Click Element	//*[@id="ca-group-permission-management-radio"]/label[1]/span[1]/input
	Click Element	submit-btn
	Wait Until Page Contains	${CREATE_GROUP}	30s
	Reload Page
	sleep	5s
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${GROUP_NAME}
	Wait Until Page Contains Element	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr/td[2]/div
	Element Should Contain	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr/td[2]/div	${GROUP_NAME}

SUITE:Delete Group
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Groups::Open Tab
	SUITE:Find Group
	Click Element	function-button-2
	Wait Until Page Contains Element	ca-groups-general-alert-dialog
	Click Element	confirm-btn
	Wait Until Page Contains	${DELETE_GROUP}	30s
	Reload Page
	sleep	5s
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${GROUP_NAME}
	Page Should Not Contain Element	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr/td[2]/div
	Close All Browsers

SUITE:Add Site
	wait until keyword succeeds	30	5	SUITE:Click On Add Site Tab
	Wait Until Page Contains Element	xpath=//input[@name='name']	15s
	Input Text	xpath=//input[@name='name']	${SITE_NAME}
	Input Text	xpath=//input[@name='address']	${SITE_ADDRESS}
	Input Text	xpath=//input[@name='latitude']	${SITE_LATITUDE}
	Input Text	xpath=//input[@name='longitude']	${SITE_LONGITUDE}
	Wait Until Keyword Succeeds	30	6	Click Element	xpath=//button[@id='submit-btn']
	Wait Until Page Contains	${SITE_ADDED_MESSAGE}	15s

SUITE:Delete_Variable
	Click Element	//*[@id="ca-table-custom-fields"]/div/div/table/tbody/tr/td[1]/span/input
	Wait Until Page Contains Element	xpath=//button[contains(.,'DELETE')]	15s
	Click Element	xpath=//button[contains(.,'DELETE')]
	Wait Until Page Contains Element	xpath=//button[contains(.,'YES')]	15s
	Click Element	xpath=//button[contains(.,'YES')]

SUITE:Delete Site
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Sites::Open Tab
	Clear Element Text	search-input
	Press Keys	search-input	CTRL+a+BACKSPACE
	sleep	5s
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${SITE_NAME}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	Click Element	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	Wait Until Page Contains Element	xpath=//button[contains(.,'Delete')]	30s
	Click Element	xpath=//button[contains(.,'Delete')]
	Wait Until Page Contains Element	xpath=//button[@id='confirm-btn']	15s
	Click Element	xpath=//button[@id='confirm-btn']
	Wait Until Page Contains	${SITE_DELETED_MESSAGE}	15s
	Close All Browsers

SUITE:Remove undefined text from field
	Page Should Contain Element	//*[contains(@value,'undefined')]
	Mouse Over	//*[@id="generic-autocomplete"]
	Click Element	//button[@title='Clear']
	Wait Until Page Does Not Contain Element	//*[contains(@value,'undefined')]	30s
	Page Should Not Contain Element	//*[contains(@value,'undefined')]