*** Settings ***
Documentation	Testing Profile configuration/Script
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	ACCESS	WEB	APPLY_CONFIGURATION	APPLY_SCRIPT
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_4

Suite Setup	SUITE:Setup
Suite Teardown	Close All Browsers

*** Variables ***
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}

${SCRIPT_CODE}	echo "test file" > /tmp/test.txt
${CODE}	${LINE1}	${\n}	${LINE2}	${\n}	${LINE3}
${LINE1}	cd /settings/network_settings/
${LINE2}	set hostname=test
${LINE3}	commit

${NAME}	testing_configuration
${SCRIPT_NAME}	testing_script
${EDITED_CONFIGURATION_NAME}	testing_configuration_edited
${EDITED_SCRIPT_NAME}	testing_script_edited
${CLONED_CONFIGURATION}	testing_configuration_cloned
${CLONED_SCRIPT}	testing_script_cloned
${TEMPLATE_NAME}	configuration_template
${EDITED_TEMPLATE_NAME}	testing_configuration_template_edited
${DESCRIPTION}	this code changes the hostname to be test
${SCRIPT_DESCRIPTION}	this script create a new file /tmp/test.txt with 'test file' written
${CONFIGURATIONADDED}	Configuration added successfully
${CONFIGURATION_EDITED}	Configuration edited successfully
${TEMPLATE_EDITED}	Template edited successfully
${MYPATH}	${CURDIR}/profile_testing_files/hostname.txt
${SCRIPT_MYPATH}	${CURDIR}/profile_testing_files/test_script.txt
${TYPESELECTION}	//*[@id="template-type-option-0"]
${SCRIPT_TYPESELECTION}	//*[@id="template-type-option-1"]
	
${TYPE}	CONFIGURATION
${DEVICE}	nodegrid
${NEWHOSTNAME}	test

*** Test Cases ***
Testcase to Add configuration Using Import from file tab
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete	${NAME}
	GUI::ZPECloud::Profiles::Open Tab
	GUI::ZPECloud::Profiles::Configuration::Add	${NAME}	${DESCRIPTION}	${MYPATH}
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	GUI::ZPECloud::Profiles::Open Tab
	Reload Page
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${NAME}
	Sleep	2
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	20s
	[Teardown]	SUITE:Teardown1	${NAME}

Testcase to Add configuration From Template
	[Setup]	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME} 15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete	${NAME}
	GUI::ZPECloud::Profiles::Template::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${TEMPLATE_NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete Template	${TEMPLATE_NAME}
	GUI::ZPECloud::Profiles::Template::Open Tab
	GUI::ZPECloud::Profiles::Template::Add	${CODE}	${TEMPLATE_NAME}	${DESCRIPTION}	${TYPESELECTION}
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	SUITE:Add Configuration From Template
	[Teardown]	Run Keywords	SUITE:Teardown2	${TEMPLATE_NAME}	AND	SUITE:Teardown1	${NAME}

Testcase to Clone An Configuration
	[Setup]	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete	${NAME}
	GUI::ZPECloud::Profiles::Open Tab
	GUI::ZPECloud::Profiles::Configuration::Add	${NAME}	${DESCRIPTION}	${MYPATH}
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	GUI::ZPECloud::Profiles::Open Tab
	Reload Page
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${NAME}
	Sleep	2s
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	20s
	Select Checkbox	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	sleep	2s
	Wait Until Page Contains Element	//*[@id="function-button-3"]	15s
	Click Element	//*[@id="function-button-3"]
	sleep	5s
	Wait Until Page Contains Element	input-name	20s
	Press Keys	input-name	CTRL+a+BACKSPACE
	Clear Element Text	input-name
	sleep	2s
	Input Text	input-name	${CLONED_CONFIGURATION}
	sleep	5s
	Wait Until Keyword Succeeds	30	6	Click Element	//*[@id="submit-btn"]
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${CLONED_CONFIGURATION}
	Sleep	2
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	20s
	[Teardown]	Run Keywords	SUITE:Teardown1	${CLONED_CONFIGURATION}	AND	SUITE:Teardown1	${NAME}

Testcase to Clone Configuration Added from Template
	[Setup]	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete	${NAME}
	GUI::ZPECloud::Profiles::Template::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${TEMPLATE_NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete Template	${TEMPLATE_NAME}
	GUI::ZPECloud::Profiles::Template::Open Tab
	GUI::ZPECloud::Profiles::Template::Add	${CODE}	${TEMPLATE_NAME}	${DESCRIPTION}	${TYPESELECTION}
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	SUITE:Add Configuration From Template
	GUI::ZPECloud::Profiles::Open Tab
	Reload Page
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${NAME}
	Sleep	2s
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	20s
	Select Checkbox	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	sleep	2s
	Wait Until Page Contains Element	//*[@id="function-button-3"]	15s
	Click Element	//*[@id="function-button-3"]
	sleep	5s
	Wait Until Page Contains Element	input-name	20s
	Press Keys	input-name	CTRL+a+BACKSPACE
	Clear Element Text	input-name
	sleep	2s
	Input Text	input-name	${CLONED_CONFIGURATION}
	sleep	5s
	Wait Until Keyword Succeeds	30	6	Click Element	//*[@id="submit-btn"]
#	Wait Until Page Contains	${CONFIGURATIONADDED}	20s
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${CLONED_CONFIGURATION}
	Sleep	2
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	20s
	[Teardown]	Run Keywords	SUITE:Teardown1	${CLONED_CONFIGURATION}	AND	SUITE:Teardown1	${NAME}	AND	SUITE:Teardown2	${TEMPLATE_NAME}

Testcase to Edit Configuration
	[Setup]	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete	${NAME}
	GUI::ZPECloud::Profiles::Open Tab
	GUI::ZPECloud::Profiles::Configuration::Add	${NAME}	${DESCRIPTION}	${MYPATH}
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	GUI::ZPECloud::Profiles::Open Tab
	Reload Page
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${NAME}
	Sleep	2
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	20s
	Select Checkbox	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Wait Until Page Contains Element	//*[@id="function-button-4"]	15s
	Click Element	//*[@id="function-button-4"]
	Press Keys	input-name	CTRL+a+BACKSPACE
	Clear Element Text	input-name
	sleep	2s
	Input Text	input-name	${EDITED_CONFIGURATION_NAME}
	sleep	5s
	Wait Until Keyword Succeeds	30	6	Click Element	//*[@id="submit-btn"]
	Wait Until Page Contains	${CONFIGURATION_EDITED}	20s
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${EDITED_CONFIGURATION_NAME}
	Sleep	2
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	20s
	[Teardown]	SUITE:Teardown1	${EDITED_CONFIGURATION_NAME}

Testcase to edit template
	[Setup]	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Template::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${TEMPLATE_NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete Template	${TEMPLATE_NAME}
	GUI::ZPECloud::Profiles::Template::Open Tab
	GUI::ZPECloud::Profiles::Template::Add	${CODE}	${TEMPLATE_NAME}	${DESCRIPTION}	${TYPESELECTION}
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	GUI::ZPECloud::Profiles::Template::Open Tab
	sleep	2s
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${TEMPLATE_NAME}
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]/main/div[1]/div[2]/div[3]/div[1]/div/table/tbody/tr[1]/td[1]/span/input	15s
	Select Checkbox	//*[@id="customer-admin-layout"]/main/div[1]/div[2]/div[3]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
	Sleep	2
	Wait Until Page Contains Element	//*[@id="function-button-1"]	15s
	Click Element	//*[@id="function-button-1"]
	Press Keys	template-name-input	CTRL+a+BACKSPACE
	Clear Element Text	template-name-input
	sleep	2s
	Input Text	template-name-input	${EDITED_TEMPLATE_NAME}
	sleep	5s
	Wait Until Keyword Succeeds	30	6	Click Element	//*[@id="function-button-1"]
	Wait Until Page Contains	${TEMPLATE_EDITED}	20s
	GUI::ZPECloud::Profiles::Template::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${EDITED_TEMPLATE_NAME}
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]/main/div[1]/div[2]/div[3]/div[1]/div/table/tbody/tr[1]/td[1]/span/input	15s
	Sleep	2
	[Teardown]	SUITE:Teardown2	${EDITED_TEMPLATE_NAME}

Testcase to Add script
	[Setup]	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${SCRIPT_NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete	${SCRIPT_NAME}
	GUI::ZPECloud::Profiles::Open Tab
	GUI::ZPECloud::Profiles::Script::Add	${SCRIPT_NAME}	${SCRIPT_DESCRIPTION}	${SCRIPT_MYPATH}
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SCRIPT_NAME}
	Sleep	2
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	20s
	[Teardown]	SUITE:Teardown1	${SCRIPT_NAME}

Testcase to Add script From Template
	[Setup]	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${SCRIPT_NAME} 15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete	${SCRIPT_NAME}
	GUI::ZPECloud::Profiles::Template::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${TEMPLATE_NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete Template	${TEMPLATE_NAME}
	GUI::ZPECloud::Profiles::Template::Open Tab
	GUI::ZPECloud::Profiles::Template::Add	${SCRIPT_CODE}	${TEMPLATE_NAME}	${SCRIPT_DESCRIPTION}	${SCRIPT_TYPESELECTION}
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	SUITE:Add Script From Template
	[Teardown]	Run Keywords	SUITE:Teardown2	${TEMPLATE_NAME}	AND	SUITE:Teardown1	${SCRIPT_NAME}

Testcase to Clone An Script
	[Setup]	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${SCRIPT_NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete	${SCRIPT_NAME}
	GUI::ZPECloud::Profiles::Open Tab
	GUI::ZPECloud::Profiles::Script::Add	${SCRIPT_NAME}	${SCRIPT_DESCRIPTION}	${SCRIPT_MYPATH}
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SCRIPT_NAME}
	Sleep	2s
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	20s
	Select Checkbox	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	sleep	2s
	Wait Until Page Contains Element	//*[@id="function-button-3"]	15s
	Click Element	//*[@id="function-button-3"]
	sleep	5s
	Wait Until Page Contains Element	input-name	20s
	Press Keys	input-name	CTRL+a+BACKSPACE
	Clear Element Text	input-name
	sleep	2s
	Input Text	input-name	${CLONED_SCRIPT}
	sleep	5s
	Wait Until Keyword Succeeds	30	6	Click Element	//*[@id="submit-btn"]
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${CLONED_SCRIPT}
	Sleep	2
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	20s
	[Teardown]	Run Keywords	SUITE:Teardown1	${CLONED_SCRIPT}	AND	SUITE:Teardown1	${SCRIPT_NAME}

Testcase to Clone Script Added from Template
	[Setup]	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${SCRIPT_NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete	${SCRIPT_NAME}
	GUI::ZPECloud::Profiles::Template::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${TEMPLATE_NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete Template	${TEMPLATE_NAME}
	GUI::ZPECloud::Profiles::Template::Open Tab
	GUI::ZPECloud::Profiles::Template::Add	${SCRIPT_CODE}	${TEMPLATE_NAME}	${SCRIPT_DESCRIPTION}	${SCRIPT_TYPESELECTION}
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	SUITE:Add Script From Template
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SCRIPT_NAME}
	Sleep	2s
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	20s
	Select Checkbox	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	sleep	2s
	Wait Until Page Contains Element	//*[@id="function-button-3"]	15s
	Click Element	//*[@id="function-button-3"]
	sleep	5s
	Wait Until Page Contains Element	input-name	20s
	Press Keys	input-name	CTRL+a+BACKSPACE
	Clear Element Text	input-name
	sleep	2s
	Input Text	input-name	${CLONED_SCRIPT}
	sleep	5s
	Wait Until Keyword Succeeds	30	6	Click Element	//*[@id="submit-btn"]
#	Wait Until Page Contains	${CONFIGURATIONADDED}	20s
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${CLONED_SCRIPT}
	Sleep	2
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	20s
	[Teardown]	Run Keywords	SUITE:Teardown1	${CLONED_SCRIPT}	AND	SUITE:Teardown1	${SCRIPT_NAME}	AND	SUITE:Teardown2	${TEMPLATE_NAME}

Testcase to Edit Script
	[Setup]	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${SCRIPT_NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete	${SCRIPT_NAME}
	GUI::ZPECloud::Profiles::Open Tab
	GUI::ZPECloud::Profiles::Script::Add	${SCRIPT_NAME}	${SCRIPT_DESCRIPTION}	${SCRIPT_MYPATH}
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SCRIPT_NAME}
	Sleep	2
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	20s
	Select Checkbox	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Wait Until Page Contains Element	//*[@id="function-button-4"]	15s
	Click Element	//*[@id="function-button-4"]
	Wait Until Page Contains Element	input-name	20s
	Press Keys	input-name	CTRL+a+BACKSPACE
	Clear Element Text	input-name
	sleep	2s
	Input Text	input-name	${EDITED_SCRIPT_NAME}
	sleep	5s
	Wait Until Keyword Succeeds	30	6	Click Element	//*[@id="submit-btn"]
	Wait Until Page Contains	${CONFIGURATION_EDITED}	20s
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${EDITED_SCRIPT_NAME}
	Sleep	2
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	20s
	[Teardown]	SUITE:Teardown1	${EDITED_SCRIPT_NAME}

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud

SUITE:Teardown1
	[Arguments]	${Delete_CONFIGURATION}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	Sleep	5
	Input Text	search-input	${Delete_CONFIGURATION}
	Sleep	2
	${ELEMENT}=	Run Keyword And return Status	Page Should Contain Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Run Keyword If	${ELEMENT} == True	SUITE:Delete	${Delete_CONFIGURATION}
	Close All Browsers

SUITE:Teardown2
	[Arguments]	${Delete_CONFIGURATION}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Template::Open Tab
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${Delete_CONFIGURATION}
	Sleep	5
	${ELEMENT}=	Run Keyword And return Status	Page Should Contain Element	//*[@id="customer-admin-layout"]/main/div[1]/div[2]/div[3]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
	Run Keyword If	${ELEMENT} == True	SUITE:Delete Template	${Delete_CONFIGURATION}
	Close All Browsers

SUITE:Delete
	[Arguments]	${NAME}
	GUI::ZPECloud::Profiles::Open Tab
	Reload Page
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${NAME}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	15s
	Click Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Wait Until Element Is Enabled	//*[@id="function-button-5"]	10s
	Click Element	//*[@id="function-button-5"]
	Sleep	2
	Page Should Contain Element	//*[@id="ca-profiles-config-delete-dialog"]/div[3]/div
	Click Element	//*[@id="confirm-btn"]
	Wait Until Page Does Not Contain Element	//*[@id="ca-profiles-config-delete-dialog"]	15s

SUITE:Delete Template
	[Arguments]	${TEMPLATE_NAME}
	GUI::ZPECloud::Profiles::Template::Open Tab
	Reload Page
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${TEMPLATE_NAME}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]/main/div[1]/div[2]/div[3]/div[1]/div/table/tbody/tr[1]/td[1]/span/input	15s
	Select Checkbox	//*[@id="customer-admin-layout"]/main/div[1]/div[2]/div[3]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
	Sleep	2
	Wait Until Element Is Enabled	//*[@id="function-button-2"]
	Click Element	//*[@id="function-button-2"]
	Page Should Contain Element	alert-dialog-title
	Click Element	//*[@id="confirm-btn"]
	Sleep	2
	Wait Until Page Does Not Contain Element	alert-dialog-title

SUITE:Add Script From Template
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	//*[@id="function-button-2"]	15s
	Click Element	//*[@id="function-button-2"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	input-name	30s
	GUI::Basic::Spinner Should Be Invisible
	Input Text	input-name	${SCRIPT_NAME}
	Input Text	input-description	${SCRIPT_DESCRIPTION}
	sleep	5s
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	css=#mui-component-select-template
	Click Element	css=#mui-component-select-template
	Wait Until Page Contains Element	xpath=//li[contains(.,'${TEMPLATE_NAME}')]	15s
	Click Element	xpath=//li[contains(.,'${TEMPLATE_NAME}')]
	Sleep	5
	Element Should Contain	xpath=//textarea	${SCRIPT_CODE}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	10	1	SUITE:Save Script From Template

SUITE:Save Script From Template
	GUI::ZPECloud::Basic::Click Button	save
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	Configuration added successfully

SUITE:Add Configuration From Template
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	//*[@id="function-button-2"]
	Click Element	//*[@id="function-button-2"]
	Wait Until Page Contains Element	input-name	15s
	Input Text	input-name	${NAME}
	Input Text	input-description	${DESCRIPTION}
	sleep	5s
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	css=#mui-component-select-template
	Click Element	css=#mui-component-select-template
	Wait Until Page Contains Element	xpath=//li[contains(.,'${TEMPLATE_NAME}')]	15s
	Click Element	xpath=//li[contains(.,'${TEMPLATE_NAME}')]
	Sleep	5
	Element Should Contain	xpath=//textarea	${LINE1}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	10	1	SUITE:Save Configuration From Template

SUITE:Save Configuration From Template
	GUI::ZPECloud::Basic::Click Button	save
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	${CONFIGURATIONADDED}