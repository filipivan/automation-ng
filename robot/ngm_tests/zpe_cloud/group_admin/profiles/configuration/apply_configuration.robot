*** Settings ***
Documentation	Tests for apply configuration - changing device's hostname
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	ACCESS	WEB	APPLY_CONFIGURATION
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_4

Suite Setup	SUITE:Setup
Suite Teardown	Close All Browsers

*** Variables ***
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${CONSOLETTYD}	xpath=//*[@id='termwindow']
${CONSOLE}	//*[@id="|test"]/div[1]/div[2]/a

${CODE}	${LINE1}	${\n}	${LINE2}	${\n}	${LINE3}
${LINE1}	cd /settings/network_settings/
${LINE2}	set hostname=test
${LINE3}	commit
${NAME}	test configuration
${TEMPLATE_NAME}	test_configuration_template
${DESCRIPTION}	this code changes the hostname to be test
${CONFIGURATIONADDED}	Configuration added successfully
${MYPATH}	${CURDIR}/Apply_configuration_files/hostname.txt
${TYPESELECTION}	//*[@id="template-type-option-0"]
${CONFIGURATION_PASSWORD_PROTECTION}	zpe#123
${TYPE}	CONFIGURATION
${DEVICE}	nodegrid
${NEWHOSTNAME}	test

*** Test Cases ***
Apply configuration - Add
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME} 15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete
	GUI::ZPECloud::Profiles::Open Tab
	GUI::ZPECloud::Profiles::Configuration::Add	${NAME}	${DESCRIPTION}	${MYPATH}
	SUITE:Apply configuration
	Sleep	15
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]
	Wait Until Keyword Succeeds	160	20s	SUITE:Hostname is 'Test'
	[Teardown]	SUITE:Teardown1

Apply configuration - Add From Template
	[Setup]	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME} 15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete
	GUI::ZPECloud::Profiles::Template::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${TEMPLATE_NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete Template
	GUI::ZPECloud::Profiles::Template::Add	${CODE}	${TEMPLATE_NAME}	${DESCRIPTION}	${TYPESELECTION}
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	SUITE:Add Configuration From Template
	SUITE:Apply configuration
	Sleep	15
	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]
	Wait Until Keyword Succeeds	160	20s	SUITE:Hostname is 'Test'
	[Teardown]	SUITE:Teardown2

Apply Configuration - Add - Scheduled
	[Setup]	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME} 15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete
	GUI::ZPECloud::Profiles::Open Tab
	GUI::ZPECloud::Profiles::Configuration::Add	${NAME}	${DESCRIPTION}	${MYPATH}
	SUITE:Apply Configuration - Scheduled
	Sleep	15
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[8]/div	${NAME}
	Wait Until Keyword Succeeds	5 minutes	2	SUITE:Status is 'Successful'
	Sleep	15
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]
	Wait Until Keyword Succeeds	160	20s	SUITE:Hostname is 'Test'
	[Teardown]	SUITE:Teardown1

Apply configuration - Add From Template -Scheduled
	[Setup]	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME} 15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete
	GUI::ZPECloud::Profiles::Template::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${TEMPLATE_NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete Template
	GUI::ZPECloud::Profiles::Template::Add	${CODE}	${TEMPLATE_NAME}	${DESCRIPTION}	${TYPESELECTION}
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	SUITE:Add Configuration From Template
	SUITE:Apply Configuration - Scheduled
	Sleep	15
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]
	Wait Until Keyword Succeeds	160	20s	SUITE:Hostname is 'Test'
	[Teardown]	SUITE:Teardown2

Apply configuration - Add-Password_Protected
	[Setup]	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME} 15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete
	GUI::ZPECloud::Profiles::Open Tab
	ADD-configuration-password protected	${NAME}	${DESCRIPTION}	${MYPATH}
	GUI::ZPECloud::Security::Services::Enable File Protection on NG Device	${USERNG}	${PWDNG}
	SUITE:Apply configuration
	Sleep	15
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]
	Wait Until Keyword Succeeds	160	20s	SUITE:Hostname is 'Test'
	GUI::ZPECloud::Security::Services::Disable file protection on NG device if needed	${USERNG}	${PWDNG}
	[Teardown]	SUITE:Teardown1

Apply Configuration - Add - Scheduled- Password_Protected
	[Setup]	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME} 15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete
	GUI::ZPECloud::Profiles::Open Tab
	ADD-configuration-password protected	${NAME}	${DESCRIPTION}	${MYPATH}
	GUI::ZPECloud::Security::Services::Enable File Protection on NG Device	${USERNG}	${PWDNG}
	SUITE:Apply Configuration - Scheduled
	Sleep	15
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[8]/div	${NAME}
	Wait Until Keyword Succeeds	5 minutes	2	SUITE:Status is 'Successful'
	Sleep	15
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]
	Wait Until Keyword Succeeds	160	20s	SUITE:Hostname is 'Test'
	GUI::ZPECloud::Security::Services::Disable file protection on NG device if needed	${USERNG}	${PWDNG}
	[Teardown]	SUITE:Teardown1

Apply configuration - Add From Template - Password-Protected
	[Setup]	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME} 15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete
	GUI::ZPECloud::Profiles::Template::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${TEMPLATE_NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete Template
	GUI::ZPECloud::Profiles::Template::Add	${CODE}	${TEMPLATE_NAME}	${DESCRIPTION}	${TYPESELECTION}
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	SUITE:Add Configuration From Template- Password_Protected
	GUI::ZPECloud::Security::Services::Enable File Protection on NG Device	${USERNG}	${PWDNG}
	SUITE:Apply configuration
	Sleep	15
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]
	Wait Until Keyword Succeeds	160	20s	SUITE:Hostname is 'Test'
	GUI::ZPECloud::Security::Services::Disable file protection on NG device if needed	${USERNG}	${PWDNG}
	[Teardown]	SUITE:Teardown2

Apply configuration - Add From Template -Scheduled- Passwprd_protected
	[Setup]	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME} 15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete
	GUI::ZPECloud::Profiles::Template::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${TEMPLATE_NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete Template
	GUI::ZPECloud::Profiles::Template::Add	${CODE}	${TEMPLATE_NAME}	${DESCRIPTION}	${TYPESELECTION}
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	SUITE:Add Configuration From Template- Password_Protected
	GUI::ZPECloud::Security::Services::Enable File Protection on NG Device	${USERNG}	${PWDNG}
	SUITE:Apply Configuration - Scheduled
	Sleep	15
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]
	Wait Until Keyword Succeeds	160	20s	SUITE:Hostname is 'Test'
	GUI::ZPECloud::Security::Services::Disable file protection on NG device if needed	${USERNG}	${PWDNG}
	[Teardown]	SUITE:Teardown2

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Security::Open Services tab
	${CHECK}=	run keyword and return status	Checkbox Should Be Selected	pass_prot
	Run Keyword If	${CHECK} == True	Click Element	pass_prot
	Run Keyword If	${CHECK} == True	Click Element	saveButton
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud

SUITE:Teardown1
	Wait Until Keyword Succeeds	30	2	Set hostname as 'nodegrid'
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]
	Wait Until Keyword Succeeds	160	2	SUITE:Hostname is 'Nodegrid'
	GUI::ZPECloud::Profiles::Open Tab
	Sleep	5
	${ELEMENT}=	Run Keyword And return Status	Page Should Contain Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Run Keyword If	${ELEMENT} == True	SUITE:Delete
	Close All Browsers

SUITE:Teardown2
	Wait Until Keyword Succeeds	30	2	Set hostname as 'nodegrid'
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]	30s
	Wait Until Keyword Succeeds	160	2	SUITE:Hostname is 'Nodegrid'
	GUI::ZPECloud::Profiles::Template::Open Tab
	Sleep	5
	${ELEMENT}=	Run Keyword And return Status	Page Should Contain Element	//*[@id="customer-admin-layout"]/main/div[1]/div[2]/div[3]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
	Run Keyword If	${ELEMENT} == True	SUITE:Delete Template
	GUI::ZPECloud::Profiles::Open Tab
	Sleep	5
	${ELEMENT}=	Run Keyword And return Status	Page Should Contain Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Run Keyword If	${ELEMENT} == True	SUITE:Delete
	Close All Browsers

SUITE:Apply configuration
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Click Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
#	Select Checkbox	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr[1]/td[1]/span/span[1]/input
	Sleep	2
	Click Element	function-button-0
	Sleep	5
	Wait Until Page Contains Element	ca-profiles-config-table-1
	Input Text	//*[@id="ca-profiles-config-search"]/div/div/input	${NAME}
	Sleep	5
	Select Checkbox	xpath=//div[@id='ca-profiles-config-table-1']/div/div/table/tbody/tr/td[1]/span/input
	Click Element	xpath=(//button[@id='function-button-1'])[2]
	Wait Until Page Contains	Apply configuration on the selected devices was requested
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[8]/div	${NAME}
	Wait Until Keyword Succeeds	5 minutes	2	SUITE:Status is 'Successful'

SUITE:Delete
	GUI::ZPECloud::Profiles::Open Tab
	Reload Page
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${NAME}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	20s
	Select Checkbox	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Wait Until Element Is Enabled	//*[@id="function-button-5"]
	Click Element	//*[@id="function-button-5"]
	Sleep	2
	Page Should Contain Element	//*[@id="ca-profiles-config-delete-dialog"]/div[3]/div
	Click Element	//*[@id="confirm-btn"]
	Sleep	2
	Page Should Not Contain Element	//*[@id="ca-profiles-config-delete-dialog"]

SUITE:Delete Template
	GUI::ZPECloud::Profiles::Template::Open Tab
	Reload Page
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${TEMPLATE_NAME}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]/main/div[1]/div[2]/div[3]/div[1]/div/table/tbody/tr[1]/td[1]/span/input	15s
	Select Checkbox	//*[@id="customer-admin-layout"]/main/div[1]/div[2]/div[3]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
	Sleep	2
	Wait Until Element Is Enabled	//*[@id="function-button-2"]
	Click Element	//*[@id="function-button-2"]
	Page Should Contain Element	alert-dialog-title
	Click Element	//*[@id="confirm-btn"]
	Sleep	2
	Page Should Not Contain Element	alert-dialog-title

SUITE:Add Configuration From Template
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	//*[@id="function-button-2"]
	Click Element	//*[@id="function-button-2"]
	Wait Until Page Contains Element	input-name	15s
	Input Text	input-name	${NAME}
	Input Text	input-description	${DESCRIPTION}
	sleep	5s
	Wait Until Page Contains Element	css=#mui-component-select-template
	Click Element	css=#mui-component-select-template
	Wait Until Page Contains Element	xpath=//li[contains(.,'${TEMPLATE_NAME}')]	15s
	Click Element	xpath=//li[contains(.,'${TEMPLATE_NAME}')]
	Sleep	5
	Element Should Contain	xpath=//textarea	${LINE1}
	Wait Until Keyword Succeeds	10	1	SUITE:Save Configuration From Template

SUITE:Save Configuration From Template
	GUI::ZPECloud::Basic::Click Button	save
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	${CONFIGURATIONADDED}

SUITE:Hostname is 'Nodegrid'
	Reload Page
	Sleep	5
	${CONTAINTEST}=	Run Keyword And return Status	Page Should Contain	${DEVICE}
	Run Keyword If	${CONTAINTEST} == False	Reload Page
	Sleep	10
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	${DEVICE}

SUITE:Hostname is 'Test'
	Reload Page
	Sleep	10
	${CONTAINTEST}=	Run Keyword And return Status	Page Should Contain	${NEWHOSTNAME}
	Run Keyword If	${CONTAINTEST} == False	Reload Page
	Sleep	5
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	${NEWHOSTNAME}

Set hostname as 'nodegrid'
	CLI:Open	${USERNAMENG}	${PASSWORDNG}
	CLI:Enter Path	/settings/network_settings/
	CLI:Set Field	hostname	${DEVICE}
	CLI:Commit
	CLI:Close Connection

SUITE:Status is 'Successful'
	Sleep	10
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div
	${CONTAINTEST}=	Run Keyword And return Status	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div	Successful
	Run Keyword If	${CONTAINTEST} == False	Reload Page
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div
	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div	Successful

SUITE:Apply Configuration - Scheduled
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Click Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
#	Select Checkbox	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Sleep	2
	Click Element	function-button-0
	Sleep	5
	Wait Until Page Contains Element	ca-profiles-config-table-1
	Input Text	//*[@id="ca-profiles-config-search"]/div/div/input	${NAME}
	Sleep	5
	Wait Until Page Contains Element	xpath=//div[@id='ca-profiles-config-table-1']/div/div/table/tbody/tr/td[1]/span/input	15s
	Select Checkbox	xpath=//div[@id='ca-profiles-config-table-1']/div/div/table/tbody/tr/td[1]/span/input
	Sleep	5
	Click Element	//*[@id="opt-schedule"]
	Execute Javascript	window.document.getElementById("recurrent-checkbox").scrollIntoView(true);
	${DATE}=	Get Current Date
	${DATE}=	Convert Date	${DATE}	result_format=timestamp
	${DATE}=	Add Time To Date	${DATE}	00:01:30.000
	${DATE}=	Fetch From Right	${DATE}	${SPACE}
	${DATE}=	Fetch From Left	${DATE}	.
	${firstDATE}	${restDATE}	${lastDATE}=	Split String From Right	${DATE}	:
	${DATE}=	Catenate	${firstDATE}:${restDATE}
	Click Element	schedule-datepicker
	${CLOUDDATE}=	Get Value	xpath=//div[@id='ui-datepicker-selector']/div/div/input
	${CLOUDDATE}=	Fetch From Left	${CLOUDDATE}	:
	${firstCLOUDDATE}	${restCLOUDDATE}	${lastCLOUDDATE}	${HOUR}=	Split String From Right	${CLOUDDATE}	${SPACE}
	${CLOUDDATE}=	Catenate	${firstCLOUDDATE} ${restCLOUDDATE} ${lastCLOUDDATE}
	${NEWDATE}=	Catenate	${CLOUDDATE} ${DATE}
	Press Keys	css=#schedule-datepicker	CTRL+a+BACKSPACE
#	Press Keys	css=#schedule-datepicker	DELETE
	Clear Element Text	css=#schedule-datepicker
	sleep	2s
	Input Text	css=#schedule-datepicker	${NEWDATE}
	Wait Until Page Contains Element	xpath=(//button[@id='function-button-1'])[2]
	Wait Until Keyword Succeeds	10	1	Click Element	xpath=(//button[@id='function-button-1'])[2]
	Wait Until Page Contains	Apply configuration on the selected devices was requested	10s

ADD-configuration-password protected
	[Arguments]	${NAME}	${DESCRIPTION}	${MYPATH}	${CONFIGURATIONADDED}=Configuration added successfully
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	//*[@id="function-button-1"]
	Click Element	//*[@id="function-button-1"]
	Wait Until Page Contains Element	input-name	15s
	Input Text	name=name	${NAME}
	Input Text	name=description	${DESCRIPTION}
	Input Text	input-name	${NAME}
	Input Text	input-description	${DESCRIPTION}
	sleep	2s
	Click Element	mui-component-select-type
	Click Element	opt-CONFIGURATION
	Choose File	fileinput-file	${MYPATH}
#	Execute Javascript	window.document.getElementById("data-indeterminate").scrollIntoView(true);
	Wait Until Page Contains Element	xpath=//div[@id='ca-profiles-config-wrapper']/div[2]/div/form/div[2]/div/div/div[7]/label/span/input
	Select Checkbox	xpath=//div[@id='ca-profiles-config-wrapper']/div[2]/div/form/div[2]/div/div/div[7]/label/span/input
	Wait Until Page Contains Element	id=filled-adornment-password
	Input Text	id=filled-adornment-password	${CONFIGURATION_PASSWORD_PROTECTION}
	Wait Until Keyword Succeeds	30	6	GUI::ZPECloud::Profiles::Configuration::Save	${NAME}

GUI::ZPECloud::Profiles::Configuration::Save
	[Arguments]	${NAME}	${ADDEDLIST}=//*[@id="ca-profiles-config-table-1"]	${CONFIGURATIONADDED}=Configuration added successfully
	Click Element	//*[@id="submit-btn"]
	Wait Until Page Contains	${CONFIGURATIONADDED}
	Sleep	5
	Wait Until Page Contains Element	${ADDEDLIST}
	Table Should Contain	${ADDEDLIST}	${NAME}

SUITE:Add Configuration From Template- Password_Protected
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	//*[@id="function-button-2"]
	Click Element	//*[@id="function-button-2"]
	Wait Until Page Contains Element	input-name	15s
	Input Text	input-name	${NAME}
	Input Text	input-description	${DESCRIPTION}
	sleep	5s
	Wait Until Page Contains Element	css=#mui-component-select-template	20s
	Click Element	css=#mui-component-select-template
	Wait Until Page Contains Element	xpath=//li[contains(.,'${TEMPLATE_NAME}')]	15s
	Click Element	xpath=//li[contains(.,'${TEMPLATE_NAME}')]
	Sleep	5
#	Element Should Contain	xpath=//textarea	${LINE1}
#	Execute Javascript	window.document.getElementById("data-indeterminate").scrollIntoView(true);
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-wrapper"]/div[2]/div[1]/form/div[2]/div/div/div[7]/label/span[1]/input
	Select Checkbox	//*[@id="ca-profiles-config-wrapper"]/div[2]/div[1]/form/div[2]/div/div/div[7]/label/span[1]/input
	Wait Until Page Contains Element	id=filled-adornment-password
	Input Text	id=filled-adornment-password	${CONFIGURATION_PASSWORD_PROTECTION}
	Wait Until Keyword Succeeds	10	1	SUITE:Save Configuration From Template