*** Settings ***
Documentation	Allow Adjustable Columns on Profile::Operations table
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	ACCESS	WEB	APPLY_CONFIGURATION
Force Tags	NON-DEVICE	ZPECLOUD	${BROWSER}

Suite Setup	SUITE:Setup
Suite Teardown	Close All Browsers

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}		${PASSWORD}

*** Test Cases ***
Test Case to check if the operation table exist
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Element Is Visible	//*[text()="OPERATION"]
	Click Element	//*[text()="OPERATION"]
	Wait Until Element Is Visible	//*[text()="Jobs"]
	Click Element	//*[text()="Jobs"]
	Wait Until Element Is Visible	(//*[@data-testid="DragHandleIcon"])[1]	#drag icon

Test Case to check if list of manage columns exist
	Wait Until Element Is Visible	//*[@data-testid="SettingsIcon"]
	Click Element	//*[@data-testid="SettingsIcon"]
	Wait Until Element Is Visible	//*[text()="Hide all"]
	Wait Until Element Is Visible	//*[text()="ID"]
	Wait Until Element Is Visible	//*[text()="Hostname"]
	Wait Until Element Is Visible	//*[text()="Serial number"]
	Wait Until Element Is Visible	//*[text()="Model name"]
	Wait Until Element Is Visible	//*[text()="Type"]
	Wait Until Element Is Visible	//*[text()="Source"]
	Wait Until Element Is Visible	//*[text()="Name"]
	Wait Until Element Is Visible	//*[text()="Status"]
	Wait Until Element Is Visible	//*[text()="Registered"]
	Wait Until Element Is Visible	//*[text()="Finished"]

 Test Case to check if element 'ID' will disappears when selected
	Click Element	//*[text()="ID"]/../span/span/input
	Wait Until Element Is Visible	//*[@title="Hostname"]
	Page Should Not Contain Element	//*[@title="ID"]
	Element Should Contain	//*[@title="Hostname"]	Hostname

 Test Case to check if Hide all hides all columns
	Wait Until Element Is Visible	//*[text()="Hide all"]
	Click Button	//*[text()="Hide all"]
	Page Should Not Contain Element	//*[@title="ID"]
	Page Should Not Contain Element	//*[@title="Hostname"]
	Page Should Not Contain Element	//*[@title="Serial number"]
	Page Should Not Contain Element	//*[@title="Model name"]
	Page Should Not Contain Element	//*[@title="Type"]
	Page Should Not Contain Element	//*[@title="Source"]
	Page Should Not Contain Element	//*[@title="Name"]
	Page Should Not Contain Element	//*[@title="Status"]
	Page Should Not Contain Element	//*[@title="Registered"]
	Page Should Not Contain Element	//*[@title="Finished"]

Test Case to check if Show all shows all columns
	Wait Until Element Is Visible	//*[text()="Show all"]
	Click Button	//*[text()="Show all"]
	Page Should Contain Element	//*[@title="ID"]
	Page Should Contain Element	//*[@title="Hostname"]
	Page Should Contain Element	//*[@title="Serial number"]
	Page Should Contain Element	//*[@title="Model name"]
	Page Should Contain Element	//*[@title="Type"]
	Page Should Contain Element	//*[@title="Source"]
	Page Should Contain Element	//*[@title="Name"]
	Page Should Contain Element	//*[@title="Status"]
	Page Should Contain Element	//*[@title="Registered"]
	Page Should Contain Element	//*[@title="Finished"]

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
