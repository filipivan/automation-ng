*** Settings ***
Documentation	Tests for apply SCRIPT - creating a new directory tmp/qa_test_dir_zpe_N0d3Gr1d
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	ACCESS	WEB	APPLY_SCRIPT
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_4

Suite Setup	SUITE:Setup
Suite Teardown	Close All Browsers

*** Variables ***
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${CONSOLETTYD}	xpath=//*[@id='termwindow']
${CONSOLE}	//*[@id="|test"]/div[1]/div[2]/a

${CODE}	echo "test file" > /tmp/test.txt
${NAME}	test_script
${DESCRIPTION}	this script create a new file /tmp/test.txt with 'test file' written
${MYPATH}	${CURDIR}/Apply_script_files/test_script.txt
${TEMPLATE_NAME}	test_script_template

${TYPE}	SCRIPT
${DEVICE}	nodegrid
${NEWHOSTNAME}	test
${TYPESELECTION}	//*[@id="template-type-option-1"]

*** Test Cases ***
Apply script - Add
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete
	GUI::ZPECloud::Profiles::Open Tab
	GUI::ZPECloud::Profiles::Script::Add	${NAME}	${DESCRIPTION}	${MYPATH}
	SUITE:Apply Script
	Sleep	15
	GUI::Basic::Spinner Should Be Invisible
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[8]/div	${NAME}
	Wait Until Keyword Succeeds	5 minutes	2	SUITE:Status is 'Successful'
	CLI:Connect As Root	${HOST}	${PWDNG}
	${OUTPUT}=	CLI:Write	cat /tmp/test.txt
	Should Not Contain Any	${OUTPUT}	No such file or directory
	CLI:Close Connection
	[Teardown]	SUITE:Teardown1

Apply script - Add From Template
	[Setup]	SUITE:Setup
	GUI::ZPECloud::Profiles::Template::Open Tab
		${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${TEMPLATE_NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete Template
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete
	GUI::ZPECloud::Profiles::Template::Add	${CODE}		${TEMPLATE_NAME}		${DESCRIPTION}	${TYPESELECTION}
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	SUITE:Add Script From Template
	SUITE:Apply Script
	Sleep	15
	GUI::Basic::Spinner Should Be Invisible
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[8]	${NAME}
	Wait Until Keyword Succeeds	5 minutes	2	SUITE:Status is 'Successful'
	CLI:Connect As Root	${HOST}	${PWDNG}
	${OUTPUT}=	CLI:Write	cat /tmp/test.txt
	Should Not Contain Any	${OUTPUT}	No such file or directory
	CLI:Close Connection
	[Teardown]	SUITE:Teardown2

Apply script - Add - Scheduled
	[Setup]	SUITE:Setup
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete
	GUI::ZPECloud::Profiles::Open Tab
	GUI::ZPECloud::Profiles::Script::Add	${NAME}	${DESCRIPTION}	${MYPATH}
	SUITE:Apply Script - Scheduled
	Sleep	15
	GUI::Basic::Spinner Should Be Invisible
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[8]/div	${NAME}
	Wait Until Keyword Succeeds	5 minutes	2	SUITE:Status is 'Successful'
	CLI:Connect As Root	${HOST}	${PWDNG}
	${OUTPUT}=	CLI:Write	cat /tmp/test.txt
	Should Not Contain Any	${OUTPUT}	No such file or directory
	CLI:Close Connection
	[Teardown]	SUITE:Teardown1
#	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
#	GUI::ZPECloud::Profiles::Operation::Open Tab
#	SUITE:GUI::ZPECloud::Schedules::Open Tab
#	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr/td[2]/div	${NAME}	30s
#	Click Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[1]/span/span[1]/input
#	Click Element	//*[@id="function-button-1"]/span[1]
#	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-schedules-delete-dialog"]/div[3]/div
#	Click Element	//*[@id="confirm-btn"]/span[1]
#	Wait Until Page Contains	Schedules deleted successfully
#	[Teardown]	SUITE:Teardown1

Apply script - Add From Template - Scheduled
	[Setup]	SUITE:Setup
	GUI::ZPECloud::Profiles::Template::Open Tab
		${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${TEMPLATE_NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete Template
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete
	GUI::ZPECloud::Profiles::Template::Add	${CODE}		${TEMPLATE_NAME}		${DESCRIPTION}	${TYPESELECTION}
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	SUITE:Add Script From Template
	SUITE:Apply Script - Scheduled
	Sleep	15
	GUI::Basic::Spinner Should Be Invisible
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[8]/div	${NAME}
	Wait Until Keyword Succeeds	5 minutes	2	SUITE:Status is 'Successful'
	CLI:Connect As Root	${HOST}	${PWDNG}
	${OUTPUT}=	CLI:Write	cat /tmp/test.txt
	Should Not Contain Any	${OUTPUT}	No such file or directory
	CLI:Close Connection
#	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
#	GUI::ZPECloud::Profiles::Operation::Open Tab
#	SUITE:GUI::ZPECloud::Schedules::Open Tab
#	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr/td[2]/div	${NAME}	30s
#	Click Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[1]/span/span[1]/input
#	Click Element	//*[@id="function-button-1"]/span[1]
#	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-schedules-delete-dialog"]/div[3]/div
#	Click Element	//*[@id="confirm-btn"]/span[1]
#	Wait Until Page Contains	Schedules deleted successfully
	[Teardown]	SUITE:Teardown2

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Security::Open Services tab
	${CHECK1}=	run keyword and return status	Checkbox Should Be Selected	allow
	Run Keyword If	${CHECK1} == False	Click Element	allow
	Run Keyword If	${CHECK1} == False	Click Element	saveButton

	${CHECK2}=	run keyword and return status	Checkbox Should Be Selected	pass_prot
	Run Keyword If	${CHECK2} == True	Click Element	pass_prot
	Run Keyword If	${CHECK2} == True	Click Element	saveButton

	CLI:Connect As Root	${HOST}	${PWDNG}
	${OUTPUT}=	CLI:Write	cat /tmp/test.txt
	${STATUS}=	Run Keyword and Return Status	Should Contain Any	${OUTPUT}	No such file or directory
	Run Keyword If	${STATUS} == False	SUITE:Delete File

	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud

SUITE:Teardown1
#	SUITE:Delete File
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	Sleep	5
	Input Text	search-input	${NAME}
	Sleep	2
	${ELEMENT}=	Run Keyword And return Status	Page Should Contain Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Run Keyword If	${ELEMENT} == True	SUITE:Delete
	Close All Browsers

SUITE:Teardown2
#	SUITE:Delete File
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Template::Open Tab
	Sleep	5
	Input Text	search-input	${TEMPLATE_NAME}
	Sleep	5
	${ELEMENT}=	Run Keyword And return Status	Page Should Contain Element	//*[@id="customer-admin-layout"]/main/div[1]/div[2]/div[3]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
	Run Keyword If	${ELEMENT} == True	SUITE:Delete Template
	GUI::ZPECloud::Profiles::Open Tab
	Sleep	5
	Input Text	search-input	${NAME}
	Sleep	2
	${ELEMENT}=	Run Keyword And return Status	Page Should Contain Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Run Keyword If	${ELEMENT} == True	SUITE:Delete
#	Wait Until Keyword Succeeds	30	2	SUITE:Access web and add hostname as nodegrid
#	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
#	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]
#	Wait Until Keyword Succeeds	160	2	SUITE:Hostname is 'Nodegrid'
	Close All Browsers

SUITE:Apply Script
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Click Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
#	Select Checkbox	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Sleep	2
	Click Element	function-button-0
	Sleep	5
	Wait Until Page Contains Element	ca-profiles-config-table-1
	Input Text	//*[@id="ca-profiles-config-search"]/div/div/input	${NAME}
	Sleep	5
	Select Checkbox	xpath=//div[@id='ca-profiles-config-table-1']/div/div/table/tbody/tr/td[1]/span/input
	Wait Until Keyword Succeeds	10	1	Click Element	xpath=(//button[@id='function-button-1'])[2]
	Wait Until Page Contains	Apply configuration on the selected devices was requested

SUITE:Delete
	GUI::ZPECloud::Profiles::Open Tab
	Reload Page
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${NAME}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	15s
	Click Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Wait Until Element Is Enabled	//*[@id="function-button-5"]	10s
	Click Element	//*[@id="function-button-5"]
	Sleep	5
	Wait Until Page Contains Element		//*[@id="ca-profiles-config-delete-dialog"]/div[3]/div
	Click Element	//*[@id="confirm-btn"]
	Sleep	2
	Page Should Not Contain Element	//*[@id="ca-profiles-config-delete-dialog"]

SUITE:Delete Template
	GUI::ZPECloud::Profiles::Template::Open Tab
	Reload Page
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${TEMPLATE_NAME}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]/main/div[1]/div[2]/div[3]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
	Select Checkbox	//*[@id="customer-admin-layout"]/main/div[1]/div[2]/div[3]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
	Sleep	2
	Wait Until Element Is Enabled	//*[@id="function-button-2"]
	Click Element	//*[@id="function-button-2"]
	Wait Until Page Contains Element		alert-dialog-title
	Click Element	//*[@id="confirm-btn"]
	Sleep	2
	Page Should Not Contain Element	alert-dialog-title

SUITE:Add Script From Template
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
#	Sleep	3
	Wait Until Page Contains Element	//*[@id="function-button-2"]	15s
	Click Element	//*[@id="function-button-2"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	input-name	30s
	GUI::Basic::Spinner Should Be Invisible
	Input Text	input-name	${NAME}
	Input Text	input-description	${DESCRIPTION}
	sleep	5s
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	css=#mui-component-select-template
	Click Element	css=#mui-component-select-template
	Wait Until Page Contains Element	xpath=//li[contains(.,'${TEMPLATE_NAME}')]	15s
	Click Element	xpath=//li[contains(.,'${TEMPLATE_NAME}')]
	Sleep	5
	Element Should Contain	xpath=//textarea	${CODE}
	GUI::Basic::Spinner Should Be Invisible
#	GUI::ZPECloud::Basic::Click Button	save
#	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	Script added successfully
	Wait Until Keyword Succeeds	10	1	SUITE:Save Script From Template

SUITE:Save Script From Template
	GUI::ZPECloud::Basic::Click Button	save
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	Configuration added successfully

#SUITE:Access console and add hostname as nodegrid
#	Open Connection	${HOST}
#	CLI:Open	${USERNG}	${PWDNG}
#	CLI:Write	cd /settings/network_settings/
#	CLI:Set	hostname=${DEVICE}
#	CLI:Commit
#	CLI:Close Connection
#	Sleep	15
#	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
#	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
#	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]/main/div/div[3]/div/div/table/tbody/tr/td[2]
#	wait until keyword succeeds	60	2	SUITE:Hostname is 'Nodegrid'

#SUITE:Hostname is 'Nodegrid'
#	Reload Page
#	Sleep	5
#	${CONTAINTEST}=	Run Keyword And return Status	Element Should Contain	//*[@id="customer-admin-layout"]/main/div/div[3]/div/div/table/tbody/tr/td[2]	${DEVICE}
#	Run Keyword If	${CONTAINTEST} == False	Reload Page
#	Sleep	10
#	Wait Until Keyword Succeeds	10	1	Element Should Contain	//*[@id="customer-admin-layout"]/main/div/div[3]/div/div/table/tbody/tr/td[2]	${DEVICE}
#
#SUITE:Hostname is 'Test'
#	Reload Page
#	Sleep	5
#	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]/main/div/div[3]/div/div/table/tbody/tr/td[2]
#	${CONTAINTEST}=	Run Keyword And return Status	Element Should Contain	//*[@id="customer-admin-layout"]/main/div/div[3]/div/div/table/tbody/tr/td[2]	${NEWHOSTNAME}
#	Run Keyword If	${CONTAINTEST} == False	Reload Page
#	Sleep	10
#	Element Should Contain	//*[@id="customer-admin-layout"]/main/div/div[3]/div/div/table/tbody/tr/td[2]	${NEWHOSTNAME}

SUITE:Status is 'Successful'
	Sleep	10
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div
	${CONTAINTEST}=	Run Keyword And return Status	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div	Successful
	Run Keyword If	${CONTAINTEST} == False	Reload Page
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div
	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div	Successful

SUITE:Delete File
	CLI:Connect As Root	${HOST}	${PWDNG}
	${OUTPUT}=	CLI:Write	rm /tmp/test.txt
	Sleep	3
	${OUTPUT}=	CLI:Write	cat /tmp/test.txt
	Should Contain Any	${OUTPUT}	No such file or directory

SUITE:GUI::ZPECloud::Schedules::Open Tab
	[Documentation]	Click in Schedules Menu Tab
	${menu}=	GUI::ZPECloud::Basic::Get Main Menu
	${return}=	GUI::ZPECloud::Basic::Click Button	schedules
	[Return]	${return}

SUITE:Apply Script - Scheduled
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Click Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
#	Select Checkbox	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Sleep	2
	Click Element	function-button-0
	Sleep	5
	Wait Until Page Contains Element	ca-profiles-config-table-1
	Input Text	//*[@id="ca-profiles-config-search"]/div/div/input	${NAME}
	Sleep	5
#	GUI::ZPECloud::Devices::Open Tab
#	Sleep	5
#	Select Checkbox	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
#	Click Element	//*[@id="function-button-1"]/span[1]
#	Sleep	5
#	Click Element	css=#mui-component-select-scripts
#	Click Element	xpath=//li[contains(.,'${NAME}')]
	Wait Until Page Contains Element	xpath=//div[@id='ca-profiles-config-table-1']/div/div/table/tbody/tr/td[1]/span/input	15s
	Select Checkbox	xpath=//div[@id='ca-profiles-config-table-1']/div/div/table/tbody/tr/td[1]/span/input
	Sleep	5
	Click Element	//*[@id="opt-schedule"]
	Execute Javascript	window.document.getElementById("recurrent-checkbox").scrollIntoView(true);
	${DATE}=	Get Current Date
	${DATE}=	Convert Date	${DATE}	result_format=timestamp
	${DATE}=	Add Time To Date	${DATE}	00:01:30.000
	${DATE}=	Fetch From Right	${DATE}	${SPACE}
	${DATE}=	Fetch From Left	${DATE}	.
	${firstDATE}	${restDATE}	${lastDATE}=	Split String From Right	${DATE}	:
	${DATE}=	Catenate	${firstDATE}:${restDATE}
	Click Element	schedule-datepicker
	${CLOUDDATE}=	Get Value	xpath=//div[@id='ui-datepicker-selector']/div/div/input
	${CLOUDDATE}=	Fetch From Left	${CLOUDDATE}	:
	${firstCLOUDDATE}	${restCLOUDDATE}	${lastCLOUDDATE}	${HOUR}=	Split String From Right	${CLOUDDATE}	${SPACE}
	${CLOUDDATE}=	Catenate	${firstCLOUDDATE} ${restCLOUDDATE} ${lastCLOUDDATE}
	${NEWDATE}=	Catenate	${CLOUDDATE} ${DATE}
	Press Keys	css=#schedule-datepicker	CTRL+a+BACKSPACE
#	Press Keys	css=#schedule-datepicker	DELETE
	Clear Element Text	css=#schedule-datepicker
	sleep	2s
	Input Text	css=#schedule-datepicker	${NEWDATE}
	Wait Until Page Contains Element	xpath=(//button[@id='function-button-1'])[2]
	Wait Until Keyword Succeeds	10	1	Click Element	xpath=(//button[@id='function-button-1'])[2]
	Wait Until Page Contains	Apply configuration on the selected devices was requested	10s