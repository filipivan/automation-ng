#!/usr/bin/python3

import sys
import pexpect
import re

# IP or FQDN of the Nodegrid where QoS will be applied
HOST = "127.0.0.1"

# User and password used for CLI login
USER = "admin"
PASS = ".ZPE5ystems!2020"

# Interface settings
INTERFACE = "wwan0" # Name of the interface that will be limited
MAXINSPEED = "10000" # Input/download speed of the interface in KB/s (kilo bytes per second)
MAXOUTSPEED = "10000" # Output/upload speed of the interface in KB/s (kilo bytes per second)

PREFIX = "qos_settings_"

# List of classes
CLASSES = [ \
        # class name,  priority (0-7),  limit,    limit unity (% or rate)
        ["tunnels",         "0",        "100",        "%"], \
        ["access",          "1",        "10",         "%"], \
        ["cluster",         "2",        "10",         "%"], \
        ["logs",            "3",        "5",          "%"], \
        ["terminal",        "4",        "1",          "%"], \
        ["web",             "5",        "1",          "%"], \
        ["rdp",             "6",        "1",          "%"], \
        ["files",           "7",        "1",          "%"], \
        ["default",         "7",        "1",          "%"] \
        ]

# List of rules used to match traffic
# if "both" is given for the port source/dest, then two rules will be created
RULES = [ \
        # rule name,     protocols,          port,                src/dst/both,  class,    match priority
        ["openvpn",      "tcp,udp",          "1194",                  "both",    "tunnels",     10], \
        ["ipsec",        "udp",              "500,4500,1701",         "both",    "tunnels",     10], \
        ["ipsec2",       "",                 "50,51",                 "both",    "tunnels",     10], \
        ["wireguard",    "udp",              "51820",                 "both",    "tunnels",     10], \
        ["cluster",      "tcp",              "9966",                  "both",    "cluster",     20], \
        ["snmp",         "tcp,udp",          "161,162",               "both",    "access",      30], \
        ["kerberos",     "tcp,udp",          "88",                    "both",    "access",      30], \
        ["ldap",         "tcp,udp",          "389,636",               "both",    "access",      30], \
        ["radius",       "tcp,udp",          "1812,183,1645,1646",    "both",    "access",      30], \
        ["tacacs",       "tcp,udp",          "49",                    "both",    "access",      30], \
        ["syslog",       "udp",              "514",                   "both",    "logs",        40], \
        ["ssh",          "tcp",              "22",                    "both",    "terminal",    50], \
        ["telnet",       "tcp",              "23",                    "both",    "terminal",    50], \
        ["ping",         "icmp,ipv6-icmp",   "",                      "both",    "terminal",    50], \
        ["http",         "tcp",              "80,443",                "both",    "web",         60], \
        ["rdp",          "tcp",              "3389",                  "both",    "rdp",         70], \
        ["vnc",          "tcp",              "5800,5900:5920",        "both",    "rdp",         70], \
        ["tviewer",      "tcp,udp",          "5938",                  "both",    "rdp",         70], \
        ["ftp",          "tcp",              "21",                    "both",    "files",       80], \
        ["rsync",        "tcp",              "873",                   "both",    "files",       80], \
        ["nfs",          "tcp,udp",          "111,2049",              "both",    "files",       80], \
        ["samba",        "tcp,udp",          "137,138,139,445",       "both",    "files",       80], \
        ["all",          "all",              "",                      "both",    "default",     100] \
        ]

# The parameters below don't need to be changed
MINRATE = "1" # Minumum configurable rate KB/s

def compress_rules(rules):
    rules.sort(key= lambda x: (x[4], x[1]))
    compressed_rules = []
    prev = []
    for n in rules:
        if prev != []:
            # If class, protocols and traffic match/are equal
            if n[4] == prev[4] and n[1] == prev[1] and \
                    n[3] == prev[3]:
                # Concatenate name
                prev[0] = str(prev[0]) + "_" + str(n[0])
                # Concatenate ports
                prev[2] = str(prev[2]) + "," + str(n[2])
                continue
            else:
                compressed_rules.append(prev)
        prev = n

    # Append last item
    compressed_rules.append(prev)

    return compressed_rules

def cli_login(host, user, password):
    ret = 0
    login_str = "ssh -o StrictHostKeyChecking=no " + str(user) + "@" + str(host)

    try:
        ssh_pexpect = pexpect.spawn(login_str, dimensions=(1000,1000))
        i = 0
        while i < 2:
            i = ssh_pexpect.expect(['[Pp]assword: ', '# ', pexpect.EOF, pexpect.TIMEOUT], timeout=20)
            if i == 0:
                print("Authenticating...")
                ssh_pexpect.sendline(password)
            elif i == 1:
                print("Logged in")
                ret = ssh_pexpect
                break
            else:
                print("Error during authentication")

    except Exception as e:
        print(str(e))

    return ret

def check_result(cli_spawned):
    result = cli_spawned.expect(['# ', '[\r\n]Error', pexpect.EOF, pexpect.TIMEOUT], timeout=60)
    ret = False
    if result == 0:
        ret = True
    return ret


def change_path(cli_spawned, path):
    ret = False
    try:
        i = 0
        cli_spawned.sendline("cd " + str(path))
        ret = check_result(cli_spawned)

    except Exception as e:
        print(str(e))

    return ret


def set_parameters(cli_spawned, params, edit):
    ret = False
    try:
        if not edit:
            cli_spawned.sendline("add")
            if (not check_result(cli_spawned)):
                print("Failed when adding")
                return ret
        else:
            cli_spawned.sendline("edit " + str(edit))
            if (not check_result(cli_spawned)):
                print("Failed when editing")
                return ret
        cli_spawned.sendline("set " + str(str(" ").join(params)))
        if (not check_result(cli_spawned)):
            print("Failed when setting parameters")
            return ret
        cli_spawned.sendline("save")
        ret = check_result(cli_spawned)

    except Exception as e:
        print(str(e))

    return ret

def list_items(cli_spawned, match="(?<=[\n\r])[^\r\n/]+(?=/)"):
    items = []
    try:
        cli_spawned.sendline("ls")
        if (check_result(cli_spawned)):
            ret = cli_spawned.before.decode('ASCII')
            items = re.findall(match, ret)
    except Exception as e:
        print(str(e))
    return items

def delete_items(cli_spawned, items):
    ret = False
    try:
        cli_spawned.sendline("delete " + str(str(",").join(items)))
        if (check_result(cli_spawned)):
            cli_spawned.sendline("commit")
            ret = check_result(cli_spawned)
    except Exception as e:
        print(str(e))
    return ret

def add_qos(cli_spawned):
    try:
        C_RULES = compress_rules(RULES)
    except Exception as e:
        print("Failed to compress rules: ", str(e))

    # Will add interface
    if (not change_path(cli_spawned, "/settings/qos/interfaces")):
        return 1
    print("Adding interface '{0}'".format(str(INTERFACE)))
    itf_params = ["interface="+str(INTERFACE), "qos_direction=bidirectional", \
            "input_bandwidth="+str(MAXINSPEED), "output_bandwidth="+str(MAXOUTSPEED), \
            "input_bandwidth_unit=kbps", "output_bandwidth_unit=kbps", \
            "custom_parameters='minrate "+str(MINRATE)+"Kbps'", "enabled=no"]
    if (not set_parameters(cli_spawned, itf_params, False)):
        print("Failed to configure interface's QoS settings")
        return 1

    # Will add classes
    if (not change_path(cli_spawned, "/settings/qos/classes")):
        return 1
    for n in CLASSES:
        print("Adding class '{0}'".format(str(PREFIX)+str(n[0])))
        class_params = ["name="+str(PREFIX)+str(n[0]), "enabled=yes", "priority="+str(n[1]), \
                "interfaces="+str(INTERFACE), "input_max_bandwidth="+str(n[2]), \
                "input_max_unit="+str(n[3]), "output_max_bandwidth="+str(n[2]), \
                "output_max_unit="+str(n[3])]
        if (not set_parameters(cli_spawned, class_params, False)):
            print("Failed to add class '{0}'".format(str(PREFIX)+str(n[0])))
            return 1

    # Will add rules
    if (not change_path(cli_spawned, "/settings/qos/rules")):
        return 1
    for n in C_RULES:
        print("Adding rule '{0}'".format(str(PREFIX)+str(n[0])))
        port_conf = []
        if n[3] == "src" or n[3] == "both":
            port_conf.append("source_port="+str(n[2]))
        if n[3] == "dst" or n[3] == "both":
            port_conf.append("destination_port="+str(n[2]))

        rule_params = ["enabled=yes", "ipv4=yes", "ipv6=yes", \
                "protocol="+str(n[1]), "classes="+str(PREFIX)+str(n[4]), \
                "custom_parameters='prio "+str(n[5])+"'"]
        for m in port_conf:
            if "destination" in m:
                rule_name = "name="+str(PREFIX)+str(n[0])+"_dst"
            else:
                rule_name = "name="+str(PREFIX)+str(n[0])+"_src"

            if (not set_parameters(cli_spawned, rule_params+[rule_name,m], False)):
                print("Failed to add rule '{0}'".format(str(PREFIX)+str(n[0])))
                return 1

    # Will enable QoS for interface
    print("Enabling QoS for interface '{0}'".format(str(INTERFACE)))
    if (not change_path(cli_spawned, "/settings/qos/interfaces")):
        return 1
    if (not set_parameters(cli_spawned, ["enabled=yes"], str(INTERFACE))):
        print("Failed to enable interface's QoS")
        return 1

    return 0

def remove_qos(cli_spawned):
    # Will remove interface
    if (not change_path(cli_spawned, "/settings/qos/interfaces")):
        return 1
    print("Removing interface '{0}'".format(str(INTERFACE)))
    if (not delete_items(cli_spawned, [INTERFACE])):
        print("Failed to remove interface")
        return 1

    # Will remove classes
    if (not change_path(cli_spawned, "/settings/qos/classes")):
        return 1
    lsclasses = list_items(cli_spawned, "(?<=[\n\r])"+PREFIX+"[^\r\n/]+(?=/)")
    print("Removing classes prefixed with '{0}'".format(str(PREFIX)))
    if lsclasses:
        if (not delete_items(cli_spawned, lsclasses)):
            print("Failed to remove classes")
            return 1

    # Will remove rules
    if (not change_path(cli_spawned, "/settings/qos/rules")):
        return 1
    lsrules = list_items(cli_spawned, "(?<=[\n\r])"+PREFIX+"[^\r\n/]+(?=/)")
    print("Removing rules prefixed with '{0}'".format(str(PREFIX)))
    if lsrules:
        if (not delete_items(cli_spawned, lsrules)):
            print("Failed to remove rules")
            return 1

    return 0

def main():
    valid_cmds = ("add", "rm", "help")
    action = "add"
    ret = 0
    nargs = len(sys.argv)
    if (nargs > 2):
        ret = 1
    elif (nargs == 2):
        action = sys.argv[1]

    if (action not in valid_cmds):
        ret = 1

    if (ret != 0 or action == "help"):
        # Show help
        print("Usage:", sys.argv[0], str("|").join(valid_cmds))
        print("add:  add QoS rules to Nodegrid")
        print("rm:   remove QoS rules from Nodegrid")
        print("help: this help message")
    else:
        print("Starting...")
        # Spawn cli access
        cli_spawned = cli_login(HOST, USER, PASS)
        if (not cli_spawned):
            print("Failed to spawn session")
            ret = 1
        else:
            # Do action
            if (action == "add"):
                ret = add_qos(cli_spawned)
            elif (action == "rm"):
                ret = remove_qos(cli_spawned)

        if (ret == 0):
            cli_spawned.sendline("exit")
            print("Finished successfully")

    sys.exit(ret)

if __name__ == "__main__":
    main()