*** Settings ***
Documentation	Test script that apply QOS rules on SIM card connection
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	QOS	SIMCARD
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_4

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${ADDQOSCODE}	${CURDIR}/qos_4g_files/add_qos_settings.py
${RMQOSCODE}	${CURDIR}/qos_4g_files/rm_qos_settings.py
${NAMETEMPLATE}	qos_template
${DESCRIPTIONTEMPLATE}	testing qos rule 4g
${NAMESCRIPT}	script_qos
${DESCRIPTIONSCRIPT}	testing qos script
${CONNECTIONNAME}	test_cellular
${CONNECTION_TYPE}	Mobile Broadband GSM
${SIM_APN}	vzwinternet
${SPEED_TEST_SCRIPT}	speedtest.py
${SPEED_TEST_SCRIPT_URL}	https://raw.githubusercontent.com/sivel/speedtest-cli/master/${SPEED_TEST_SCRIPT}

*** Test Cases ***
Verify Cellular Interface
	CLI:Connect As Root	${HOST}	${PWDNG}
	${OUTPUT}=	CLI:Write	cat ${SPEED_TEST_SCRIPT}
	${STATUS}=	Run Keyword and Return Status	Should Contain Any	${OUTPUT}	No such file or directory
	Run Keyword If	${STATUS} == False	SUITE:Delete SCRIPT File
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	${HASCELLULAR}=	Run Keyword And return Status	Page Should Contain	Mobile Broadband GSM
	Run Keyword If	${HASCELLULAR} == False	SUITE:Create Cellular Connection
	${PARENT}=	Get Element Attribute	xpath=(//*[text()='Mobile Broadband GSM']/..)	id
	${UPDOWN}=	Get Text	//*[@id="${PARENT}"]/td[6]
	${CON}=	Run Keyword And return Status	Should be Equal	${UPDOWN}	Up
	Run Keyword If	${CON} == False	SUITE:Up Connection	${PARENT}
	${CONNECTED_STATUS}=	Get Text	//*[@id="${PARENT}"]/td[3]
	${STATUS}=	Run Keyword And return Status	Should be Equal	${CONNECTED_STATUS}	Connected
	Run Keyword If	${STATUS} == False	SUITE:Up Connection	${PARENT}
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	${IPNG}=	Get Text	//*[@id="${PARENT}"]/td[7]
	${IP}	${MASK}=	Split String	${IPNG}	/
	${IPCELLULAR}=	Set Variable	${IP}
	Set Global Variable	${IPCELLULAR}
	[Teardown]	SUITE:Teardown

Previous Scenario
	CLI:Connect As Root	${HOST}	${PWDNG}
	${OUTPUT}=	CLI:Write	wget ${SPEED_TEST_SCRIPT_URL}
	Should Contain	${OUTPUT}	${SPEED_TEST_SCRIPT} saved
	${OUTPUT}=	CLI:Write	python3 /home/root/${SPEED_TEST_SCRIPT} --source ${IPCELLULAR}
	${FULL_MATCH}	${BEFORESPEED}=	Should Match Regexp	${OUTPUT}	Download: (.*) Mbit\\/s
	Set Global Variable	${BEFORESPEED}
	[Teardown]	SUITE:Teardown

Apply script
	SUITE: Setup
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAMESCRIPT}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete	${NAMESCRIPT}
	SUITE:ADD SCRIPT
	SUITE:Apply Script
	Sleep	15
	GUI::Basic::Spinner Should Be Invisible
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[8]	${NAMESCRIPT}
	Wait Until Keyword Succeeds	5 minutes	2	SUITE:Status is 'Successful'
	[Teardown]	SUITE:Teardown

Verify Traffic
	CLI:Connect As Root	${HOST}	${PWDNG}
	${OUTPUT}=	CLI:Write	python3 /home/root/${SPEED_TEST_SCRIPT} --source ${IPCELLULAR}
	${FULL_MATCH}	${AFTERSPEED}=	Should Match Regexp	${OUTPUT}	Download: (.*) Mbit\\/s
	SUITE:GSM Speed Should Be Reduced	${AFTERSPEED}	${BEFORESPEED}
	Set Global Variable	${AFTERSPEED}
	[Teardown]	SUITE:Teardown

Edit Script
	SUITE:Setup
	GUI::ZPECloud::Profiles::Open Tab
	Input Text	search-input	${NAMESCRIPT}
	Sleep	2
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAMESCRIPT}	15s
	Run Keyword if	${HASCONFIGURATION} == False	SUITE:ADD SCRIPT
	GUI::ZPECloud::Profiles::Open Tab
	Input Text	search-input	${NAMESCRIPT}
	Sleep	2
	Click Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Wait Until Page Contains Element	//*[@id="function-button-4"]	15s
	Click Element	//*[@id="function-button-4"]
	Sleep	5
	Press Keys	xpath=//textarea	CTRL+a+BACKSPACE
	Clear Element Text	xpath=//textarea
	Choose File	fileinput-file	${RMQOSCODE}
	Wait Until Keyword Succeeds	30	6	Click Element	//*[@id="submit-btn"]
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	Configuration edited successfully
	[Teardown]	SUITE:Teardown

Remove QOS Rule
	SUITE:Setup
	SUITE:Apply Script
	Sleep	15
	GUI::Basic::Spinner Should Be Invisible
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[8]	${NAMESCRIPT}
	Wait Until Keyword Succeeds	5 minutes	2	SUITE:Status is 'Successful'
	${OUTPUT}=	CLI:Write	python3 /home/root/${SPEED_TEST_SCRIPT} --source ${IPCELLULAR}
	${FULL_MATCH}	${NOWSPEED}=	Should Match Regexp	${OUTPUT}	Download: (.*) Mbit//s
	CLI:Write	rm -f ${SPEED_TEST_SCRIPT}
	${NOFILE}=	CLI:Write	ls
	Should Not Contain	${NOFILE}	${SPEED_TEST_SCRIPT}
	Set Global Variable	${NOWSPEED}
	SUITE:GSM Speed Should Be Increased	${NOWSPEED}	${AFTERSPEED}
	[Teardown]	SUITE:Teardown

Delete Script
	SUITE:Setup
	GUI::ZPECloud::Profiles::Open Tab
	Input Text	search-input	${NAMESCRIPT}
	Sleep	2
	Click Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Wait Until Element Is Enabled	function-button-5	10s
	Click Element	function-button-5
	Sleep	2
	Page Should Contain Element	//*[@id="ca-profiles-config-delete-dialog"]/div[3]/div
	Click Element	//*[@id="confirm-btn"]
	Sleep	2
	Page Should Not Contain Element	//*[@id="ca-profiles-config-delete-dialog"]
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	Profile deleted successfully
	[Teardown]	SUITE:Teardown

Delete Template
	SUITE:Setup
	GUI::ZPECloud::Profiles::Template::Open Tab
	Input Text	search-input	${NAMETEMPLATE}
	Sleep	2
	Click Element	//*[@id="customer-admin-layout"]/main/div[1]/div[2]/div[3]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
	Sleep	2
	Wait Until Element Is Enabled	//*[@id="function-button-2"]
	Click Element	//*[@id="function-button-2"]
	Page Should Contain Element	alert-dialog-title
	Click Element	//*[@id="confirm-btn"]
	Sleep	2
	Page Should Not Contain Element	alert-dialog-title
	[Teardown]	SUITE:Teardown

Delete Connection
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Network::Open Connections Tab
	${PARENT}=	Get Element Attribute	xpath=(//*[text()='Mobile Broadband GSM']/..)	id
	Click Element	//*[@id="${PARENT}"]/td[1]/input
	Click Element	delButton
	GUI::Basic::Spinner Should Be Invisible
	Page Should Not Contain	Mobile Broadband GSM

*** Keywords ***
SUITE:Setup
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud

SUITE:Teardown
	Close All Browsers

SUITE:ADD SCRIPT
	GUI::ZPECloud::Profiles::Template::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAMETEMPLATE}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete Template	${NAMETEMPLATE}
	${CODE}=	OperatingSystem.Get File	${ADDQOSCODE}
	Set Global Variable	${CODE}
	GUI::ZPECloud::Profiles::Template::Add	${CODE}	${NAMETEMPLATE}	${DESCRIPTIONTEMPLATE}	//*[@id="template-type-option-1"]
	SUITE:Add Script From Template

SUITE:Add Script From Template
	SUITE:Setup
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	//*[@id="function-button-2"]	15s
	Click Element	//*[@id="function-button-2"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	input-name	30s
	GUI::Basic::Spinner Should Be Invisible
	Input Text	input-name	${NAMESCRIPT}
	Input Text	input-description	${DESCRIPTIONSCRIPT}
	sleep	5s
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	css=#mui-component-select-template
	Click Element	css=#mui-component-select-template
	Wait Until Page Contains Element	xpath=//li[contains(.,'${NAMETEMPLATE}')]	15s
	Click Element	xpath=//li[contains(.,'${NAMETEMPLATE}')]
	Sleep	5
	Element Should Contain	xpath=//textarea	${CODE}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	10	1	SUITE:Save Script From Template

SUITE:Save Script From Template
	GUI::ZPECloud::Basic::Click Button	save
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	Configuration added successfully

SUITE:Apply Script
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	15s
	Click Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Sleep	2
	Click Element	function-button-0
	Sleep	5
	Wait Until Page Contains Element	ca-profiles-config-table-1
	Input Text	//*[@id="ca-profiles-config-search"]/div/div/input	${NAMESCRIPT}
	Sleep	5
	Select Checkbox	xpath=//div[@id='ca-profiles-config-table-1']/div/div/table/tbody/tr/td[1]/span/input
	Click Element	xpath=(//button[@id='function-button-1'])[2]
	Wait Until Page Contains	Apply configuration on the selected devices was requested

SUITE:Status is 'Successful'
	Sleep	10
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div
	${CONTAINTEST}=	Run Keyword And return Status	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div	Successful
	Run Keyword If	${CONTAINTEST} == False	Reload Page
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div
	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div	Successful

SUITE:Create Cellular Connection
	Click Element	addButton
	GUI::Basic::Wait Until Element Is Accessible	connType
	Select From List By Label	//*[@id="connType"]	${CONNECTION_TYPE}
	${STATUS}	Evaluate NG Versions: ${NGVERSION} > 4.2
	Run Keyword If	${STATUS}	Select From List By Value	connItfName	cdc-wdm0
	${STATUS}	Evaluate NG Versions: ${NGVERSION} < 5.0
	Run Keyword If	${STATUS}	Select From List By Value	connIeth	cdc-wdm0
	Input Text	connName	${CONNECTIONNAME}
	Input Text	mobileAPN	${SIM_APN}
	${CHECK}=	Run Keyword and Return Status	Checkbox Should Be Selected	connAuto
	Run Keyword If	${CHECK} == False	Click Element	connAuto
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Should not be Empty	//*[@id="${CONNECTIONNAME}r"]/td[7]
	${IPCELLULAR}=	Get Text	//*[@id="${CONNECTIONNAME}"]/td[7]

SUITE:Up Connection
	[Arguments]	${IDCONNECTION}
	Click Element	//*[@id="${IDCONNECTION}"]/td[1]
	Click Element	//*[@id="nonAccessControls"]/input[3]
	GUI::Basic::Spinner Should Be Invisible

SUITE:GSM Speed Should Be Reduced
	[Arguments]	${SPEED_AFTER}	${SPEED_BEFORE}
	${SPEED_AFTER_FLOAT}=	Convert To Number	${SPEED_AFTER}
	${SPEED_BEFORE_FLOAT}=	Convert To Number	${SPEED_BEFORE}
	Should Be True	${SPEED_AFTER_FLOAT} < ${SPEED_BEFORE_FLOAT} * 0.4

SUITE:GSM Speed Should Be Increased
	[Arguments]	${SPEED_AFTER}	${SPEED_BEFORE}
	${SPEED_AFTER_FLOAT}=	Convert To Number	${SPEED_AFTER}
	${SPEED_BEFORE_FLOAT}=	Convert To Number	${SPEED_BEFORE}
	Should Be True	${SPEED_AFTER_FLOAT} > ${SPEED_BEFORE_FLOAT} * 2

SUITE:Delete
	[Arguments]	${NAME}
	Input Text	search-input	${NAME}
	Sleep	2
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	15s
	Click Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Wait Until Element Is Enabled	//*[@id="function-button-5"]	10s
	Click Element	//*[@id="function-button-5"]
	Sleep	2
	Page Should Contain Element	//*[@id="ca-profiles-config-delete-dialog"]/div[3]/div
	Click Element	//*[@id="confirm-btn"]
	Wait Until Page Does Not Contain Element	//*[@id="ca-profiles-config-delete-dialog"]	15s

SUITE:Delete Template
	[Arguments]	${TEMPLATE_NAME}
	Input Text	search-input	${TEMPLATE_NAME}
	Sleep	2
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]/main/div[1]/div[2]/div[3]/div[1]/div/table/tbody/tr[1]/td[1]/span/input	15s
	Select Checkbox	//*[@id="customer-admin-layout"]/main/div[1]/div[2]/div[3]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
	Sleep	2
	Wait Until Element Is Enabled	//*[@id="function-button-2"]
	Click Element	//*[@id="function-button-2"]
	Page Should Contain Element	alert-dialog-title
	Click Element	//*[@id="confirm-btn"]
	Sleep	2
	Wait Until Page Does Not Contain Element	alert-dialog-title

SUITE:Delete SCRIPT File
	CLI:Connect As Root	${HOST}	${PWDNG}
	${OUTPUT}=	CLI:Write	rm ${SPEED_TEST_SCRIPT}
	Sleep	3
	${OUTPUT}=	CLI:Write	cat ${SPEED_TEST_SCRIPT}
	Should Contain Any	${OUTPUT}	No such file or directory