*** Settings ***
Documentation	Tests for apply SCRIPT - to Collect Diagnostic Data Under Extended Storage
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	ACCESS	WEB	APPLY_SCRIPT
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_4

Suite Setup	SUITE:Setup
#Suite Teardown	Close All Browsers

*** Variables ***
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${DESCRIPTION}		this script intended collect diagnostic data
${MYPATH}	${CURDIR}/Collect_diagnostic_data_files/Collect Diagnostic Data.txt
${TEMPLATE_NAME}	Collect Diagnostic Data
${NAME}		test_Diagnostic_data_script
${TYPE}	SCRIPT
${DEVICE}	nodegrid
${CONFIGURATIONADDED}	Configuration added successfully
${COLLECTED_DIAGNOSTIC_FILE_TYPE}	.tar.gz
${COLLECTED_DIAGNOSTIC_FILE_NAME}	collection
${CONFIGURATION_PASSWORD_PROTECTION}	zpe#123
${TYPESELECTION}	//*[@id="template-type-option-1"]
${DELETESUCCESS}	Files deleted successfully

*** Test Cases ***
Apply Script-To Colllect Diagnostic Data
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete
	GUI::ZPECloud::Profiles::Open Tab
	GUI::ZPECloud::Profiles::Script::Add	${NAME}	${DESCRIPTION}	${MYPATH}
	SUITE:Apply Script
	Sleep	15
	GUI::Basic::Spinner Should Be Invisible
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[8]/div	${NAME}
	Wait Until Keyword Succeeds	5 minutes	2	SUITE:Status is 'Successful'
	SUITE:Check Diagnostic Data
	[Teardown]	SUITE:Teardown

Apply script (Add From Template)-To Colllect Diagnostic Data
	[Setup]	SUITE:Setup
	GUI::ZPECloud::Profiles::Template::Open Tab
		${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${TEMPLATE_NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == False	SUITE:Add template
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete
	SUITE:Add Script From Template
	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	SUITE:Apply Script
	Sleep	15
	GUI::Basic::Spinner Should Be Invisible
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[8]	${NAME}
	Wait Until Keyword Succeeds	5 minutes	2	SUITE:Status is 'Successful'
	SUITE:Check Diagnostic Data
	[Teardown]	SUITE:Teardown

Apply script (Password Protected)-To Colllect Diagnostic Data
	[Setup]	SUITE:Setup
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete
	ADD-configuration-password protected	${NAME}	${DESCRIPTION}	${MYPATH}
	GUI::ZPECloud::Security::Services::Enable File Protection on NG Device	${USERNG}	${PWDNG}
	SUITE:SETUP1
	SUITE:Apply Script
	Sleep	15
	GUI::Basic::Spinner Should Be Invisible
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[8]/div	${NAME}
	Wait Until Keyword Succeeds	5 minutes	2	SUITE:Status is 'Successful'
	SUITE:Check Diagnostic Data
	[Teardown]	SUITE:Teardown

Apply script (Add From Template_Password Protected)-To Colllect Diagnostic Data
	[Setup]	SUITE:Setup
	GUI::ZPECloud::Profiles::Template::Open Tab
		${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${TEMPLATE_NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == False	SUITE:Add template
	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	GUI::ZPECloud::Profiles::Open Tab
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${NAME}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete
	SUITE:Add Configuration From Template- Password_Protected
	GUI::ZPECloud::Security::Services::Enable File Protection on NG Device	${USERNG}	${PWDNG}
	SUITE:SETUP1
	SUITE:Apply Script
	Sleep	15
	GUI::Basic::Spinner Should Be Invisible
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[8]/div	${NAME}
	Wait Until Keyword Succeeds	5 minutes	2	SUITE:Status is 'Successful'
	SUITE:Check Diagnostic Data
	[Teardown]	SUITE:Teardown

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Security::Open Services tab
	${CHECK1}=	run keyword and return status	Checkbox Should Be Selected	allow
	Run Keyword If	${CHECK1} == False	Click Element	allow
	Run Keyword If	${CHECK1} == False	Click Element	saveButton

	${CHECK2}=	run keyword and return status	Checkbox Should Be Selected	pass_prot
	Run Keyword If	${CHECK2} == True	Click Element	pass_prot
	Run Keyword If	${CHECK2} == True	Click Element	saveButton

	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud

SUITE:SETUP1
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Teardown
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	Sleep	5
	Input Text	search-input	${NAME}
	Sleep	2
	${ELEMENT}=	Run Keyword And return Status	Page Should Contain Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Run Keyword If	${ELEMENT} == True	SUITE:Delete
	${Currentdate}=	Get Current Date	result_format=%Y-%m-%d
	SUITE:Delete Diagnostic Data From Extended Storage	${COLLECTED_DIAGNOSTIC_FILE_NAME}_${DEVICE}_${Currentdate}	3
	Close All Browsers

SUITE:Apply Script
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Click Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Sleep	2
	Click Element	function-button-0
	Sleep	5
	Wait Until Page Contains Element	ca-profiles-config-table-1
	Input Text	//*[@id="ca-profiles-config-search"]/div/div/input	${NAME}
	Sleep	5
	Select Checkbox	xpath=//div[@id='ca-profiles-config-table-1']/div/div/table/tbody/tr/td[1]/span/input
	Wait Until Keyword Succeeds	10	1	Click Element	xpath=(//button[@id='function-button-1'])[2]
	Wait Until Page Contains	Apply configuration on the selected devices was requested

SUITE:Delete
	GUI::ZPECloud::Profiles::Open Tab
	Reload Page
	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${NAME}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	15s
	Click Element		//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Wait Until Element Is Enabled	//*[@id="function-button-5"]	10s
	Click Element	//*[@id="function-button-5"]
	Sleep	5
	Wait Until Page Contains Element		//*[@id="ca-profiles-config-delete-dialog"]/div[3]/div
	Click Element	//*[@id="confirm-btn"]
	Sleep	2
	Page Should Not Contain Element	//*[@id="ca-profiles-config-delete-dialog"]

SUITE:Add Script From Template
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	//*[@id="function-button-2"]	15s
	Click Element	//*[@id="function-button-2"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	input-name	30s
	GUI::Basic::Spinner Should Be Invisible
	Input Text	input-name		${NAME}
	Input Text	input-description	${DESCRIPTION}
	sleep	5s
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	css=#mui-component-select-template
	Click Element	css=#mui-component-select-template
	Wait Until Page Contains Element	xpath=//li[contains(.,'${TEMPLATE_NAME}')]	15s
	Click Element	xpath=//li[contains(.,'${TEMPLATE_NAME}')]
	Sleep	5
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	10	1	SUITE:Save Script From Template

SUITE:Save Script From Template
	GUI::ZPECloud::Basic::Click Button	save
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	Configuration added successfully

SUITE:Status is 'Successful'
	Sleep	10
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div
	${CONTAINTEST}=	Run Keyword And return Status	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div	Successful
	Run Keyword If	${CONTAINTEST} == False	Reload Page
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div
	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div	Successful

SUITE:GUI::ZPECloud::Schedules::Open Tab
	[Documentation]	Click in Schedules Menu Tab
	${menu}=	GUI::ZPECloud::Basic::Get Main Menu
	${return}=	GUI::ZPECloud::Basic::Click Button	schedules
	[Return]	${return}

SUITE:Apply Script - Scheduled
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Click Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Sleep	2
	Click Element	function-button-0
	Sleep	5
	Wait Until Page Contains Element	ca-profiles-config-table-1
	Input Text	//*[@id="ca-profiles-config-search"]/div/div/input	${NAME}
	Sleep	5
	Wait Until Page Contains Element	xpath=//div[@id='ca-profiles-config-table-1']/div/div/table/tbody/tr/td[1]/span/input	15s
	Select Checkbox	xpath=//div[@id='ca-profiles-config-table-1']/div/div/table/tbody/tr/td[1]/span/input
	Sleep	5
	Click Element	//*[@id="opt-schedule"]
	Execute Javascript	window.document.getElementById("recurrent-checkbox").scrollIntoView(true);
	${DATE}=	Get Current Date
	${DATE}=	Convert Date	${DATE}	result_format=timestamp
	${DATE}=	Add Time To Date	${DATE}	00:01:30.000
	${DATE}=	Fetch From Right	${DATE}	${SPACE}
	${DATE}=	Fetch From Left	${DATE}	.
	${firstDATE}	${restDATE}	${lastDATE}=	Split String From Right	${DATE}	:
	${DATE}=	Catenate	${firstDATE}:${restDATE}
	Click Element	schedule-datepicker
	${CLOUDDATE}=	Get Value	xpath=//div[@id='ui-datepicker-selector']/div/div/input
	${CLOUDDATE}=	Fetch From Left	${CLOUDDATE}	:
	${firstCLOUDDATE}	${restCLOUDDATE}	${lastCLOUDDATE}	${HOUR}=	Split String From Right	${CLOUDDATE}	${SPACE}
	${CLOUDDATE}=	Catenate	${firstCLOUDDATE} ${restCLOUDDATE} ${lastCLOUDDATE}
	${NEWDATE}=	Catenate	${CLOUDDATE} ${DATE}
	Press Keys	css=#schedule-datepicker	CTRL+a+BACKSPACE
	Clear Element Text	css=#schedule-datepicker
	sleep	2s
	Input Text	css=#schedule-datepicker	${NEWDATE}
	Wait Until Page Contains Element	xpath=(//button[@id='function-button-1'])[2]
	Wait Until Keyword Succeeds	10	1	Click Element	xpath=(//button[@id='function-button-1'])[2]
	Wait Until Page Contains	Apply configuration on the selected devices was requested

SUITE:Login on Extended Storage
	GUI::ZPECloud::Apps::Open Tab
	Sleep	5
	Wait Until Page Contains Element	xpath=//h6[contains(.,'Extended Storage')]	30s
	Click Element	xpath=//h6[contains(.,'Extended Storage')]
	Sleep	5

SUITE:Delete Diagnostic Data From Extended Storage
	[Arguments]	${NAMEFILE}	${NUMBERBUTTON}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	SUITE:Login on Extended Storage
#	Wait Until Keyword Succeeds	30	1	Click Element	//*[@id="simple-tabpanel-0"]/div/div/div/div[2]/div/div[2]/div[3]/div[1]/div/div/div[1]/div/div/div
	Wait Until Page Contains Element	//div[@class="chonky-toolbar-button-group"]
	sleep	2s
	Click Element	//div[@class="chonky-toolbar-button-group"]
	Click Element	//div[@class="chonky-toolbar-dropdown-content"]/button[${NUMBERBUTTON}]
	Click Element	xpath=//div[3]/div/div/div/button[2]
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	${DELETESUCCESS}
	Sleep	2

SUITE:Check Diagnostic Data
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	${Currentdate}=	Get Current Date	result_format=%Y-%m-%d
	SUITE:Login on Extended Storage
	Wait Until Page Contains	${COLLECTED_DIAGNOSTIC_FILE_NAME}_${DEVICE}_${Currentdate}	30s

SUITE:Add template
	${CODE}=	OperatingSystem.Get File	${MYPATH}
	Set Global Variable	${CODE}
	GUI::ZPECloud::Profiles::Template::Add	${CODE}	${TEMPLATE_NAME}		${DESCRIPTION}	${TYPESELECTION}

ADD-configuration-password protected
	[Arguments]	${NAME}	${DESCRIPTION}	${MYPATH}	${CONFIGURATIONADDED}=Configuration added successfully
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	//*[@id="function-button-2"]
	Click Element	//*[@id="function-button-2"]
	Wait Until Page Contains Element		input-name	15s
	Input Text	name=name	${NAME}
	Input Text	name=description	${DESCRIPTION}
	Input Text	input-name	${NAME}
	Input Text	input-description	${DESCRIPTION}
	sleep	2s
	Click Element	mui-component-select-type
	Click Element	opt-SCRIPT
	Choose File	fileinput-file	${MYPATH}
	Wait Until Page Contains Element	xpath=//div[@id='ca-profiles-config-wrapper']/div[2]/div/form/div[2]/div/div/div[7]/label/span/input
	Select Checkbox	xpath=//div[@id='ca-profiles-config-wrapper']/div[2]/div/form/div[2]/div/div/div[7]/label/span/input
	Wait Until Page Contains Element	id=filled-adornment-password
	Input Text	id=filled-adornment-password	${CONFIGURATION_PASSWORD_PROTECTION}
	Wait Until Keyword Succeeds	30	6	GUI::ZPECloud::Profiles::Configuration::Save	${NAME}

SUITE:Add Configuration From Template- Password_Protected
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	Wait Until Page Contains Element	//*[@id="function-button-2"]
	Click Element	//*[@id="function-button-2"]
	Wait Until Page Contains Element	input-name	15s
	Input Text	input-name	${NAME}
	Input Text	input-description	${DESCRIPTION}
	sleep	5s
	Wait Until Page Contains Element	css=#mui-component-select-template	20s
	Click Element	css=#mui-component-select-template
	Wait Until Page Contains Element	xpath=//li[contains(.,'${TEMPLATE_NAME}')]	15s
	Click Element	xpath=//li[contains(.,'${TEMPLATE_NAME}')]
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-wrapper"]/div[2]/div[1]/form/div[2]/div/div/div[7]/label/span[1]/input
	Select Checkbox	//*[@id="ca-profiles-config-wrapper"]/div[2]/div[1]/form/div[2]/div/div/div[7]/label/span[1]/input
	Wait Until Page Contains Element	id=filled-adornment-password
	Input Text	id=filled-adornment-password	${CONFIGURATION_PASSWORD_PROTECTION}
	Wait Until Keyword Succeeds	10	1	SUITE:Save Configuration From Template

SUITE:Save Configuration From Template
	GUI::ZPECloud::Basic::Click Button	save
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	${CONFIGURATIONADDED}