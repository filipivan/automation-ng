*** Settings ***
Documentation	Login into ZPE Cloud: Behavior tests
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Force Tags	GUI	ZPECLOUD	SOFTWARE UPGRADE	SOFTWARE DOWNGRADE	NON-CRITICAL	BUG_CLOUD-1470	DEVICE	PART_4

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${VERSIONAFTER}	1
${VERSIONBEFORE}	2

*** Test Cases ***
#Device Software Downgrade
#	Click Element	//*[@id="menu-version"]/div[3]/ul/li[2]
#	Click Element	//*[@id="submit-btn"]/span[1]
#	Wait Until Keyword Succeeds	10	1	Page Should Contain	Software upgrade on the selected devices was requested
#	GUI::ZPECloud::Profiles::Operation::Open Tab
#	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[7]/div
#	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[6]	Nodegrid_Platform_
#	SUITE:Status is 'Successful'
#	SUITE:Get Version After
#	[Teardown]	SUITE:Teardown

#Device Software Upgrade
#	[Setup]	SUITE:Setup
#	Click Element	//*[@id="menu-version"]/div[3]/ul/li[1]
#	Click Element	//*[@id="submit-btn"]/span[1]
#	Wait Until Keyword Succeeds	10	1	Page Should Contain	Software upgrade on the selected devices was requested
#	GUI::ZPECloud::Profiles::Operation::Open Tab
#	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[7]/div
#	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[6]	Nodegrid_Platform_
#	SUITE:Status is 'Successful'
#	SUITE:Get Version After

*** Keywords ***
SUITE:Setup
	SUITE:Get Version Before
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr[2]/td[1]/span/span[1]/input
	Click Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr[2]/td[1]/span/span[1]/input
	Wait Until Element Is Enabled	//*[@id="function-button-2"]/span[1]
	Click Element	//*[@id="function-button-2"]/span[1]
	Page Should Contain	Upgrade device
	Wait Until Page Contains Element	mui-component-select-version
	Click Element	mui-component-select-version
	Sleep	5

SUITE:Teardown
	Close All Browsers

SUITE:Status is 'Successful'
	Sleep	10
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[7]/div
	${CONTAINTEST}=	Run Keyword And return Status	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[7]/div	Successful
	Run Keyword If	${CONTAINTEST} == False	Reload Page
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[7]/div
	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[7]/div	Successful

SUITE:Get Version Before
	GUI::Basic::Open And Login Nodegrid	${USERNAMENG}	${PASSWORDNG}
	Click Element	//*[@id="pwl"]/span
	Wait Until Page Contains Element	//*[@id="abt"]
	Click Element	//*[@id="abt"]
	${VERSIONBEFORE}=	Get Text	version
	Close Browser
	Sleep	5

SUITE:Get Version After and Compare with 'Version Before'
	GUI::Basic::Open And Login Nodegrid	${USERNAMENG}	${PASSWORDNG}
	Click Element	//*[@id="pwl"]/span
	Wait Until Page Contains Element	//*[@id="abt"]
	Click Element	//*[@id="abt"]
	${VERSIONAFTER}=	Get Text	version
	Should Not Be Equal	${VERSIONAFTER}	${VERSIONBEFORE}
	Close Browser

