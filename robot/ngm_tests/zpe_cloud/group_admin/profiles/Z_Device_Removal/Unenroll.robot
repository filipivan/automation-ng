*** Settings ***
Documentation	Tests to Remove device to the suite
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	WEB	BACKUP
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_4

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}

*** Test Cases ***
Unenroll Device for Profile suite
	${status}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	${status} == True	GUI::Cloud::UNEnroll Device on Cloud

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Teardown
	Close All Browsers
