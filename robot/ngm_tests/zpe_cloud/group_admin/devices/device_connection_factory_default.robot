*** Settings ***
Documentation	Tests device connection after the restore factory default
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	DEVICE_CONNECTION	FACTORY_RESTORE	WEB
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${FACTORYPASSWD}	admin
${root_login}	shell sudo su -
${SERIALNUMBER}	${SERIALNUMBER}
${ONPREMISECOMPANY}	Enrolled at https://zpecloud.com
${CUSTOMERCODEQA}
${ENROLLMENTKEYQA}

*** Test Cases ***
Restore Factory Default
	Click Element	//*[@id="nonAccessControls"]/input[1]
	Handle Alert
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	9x	20s	SUITE:Login Successful
	Close All Browsers
	${STATUS}	Evaluate NG Versions: ${NGVERSION} > 4.2
	Run Keyword If	${STATUS}	SUITE:Login after restore and Configure new password
	${STATUS}	Evaluate NG Versions: ${NGVERSION} < 5.0
	Run Keyword If	${STATUS}	SUITE:Login after restore and Configuring new password
	SUITE:Verify ZPE Cloud Agent
	SUITE:Verify Permission SSH
	${STATUS}	Evaluate NG Versions: ${NGVERSION} > 4.2
	Run Keyword If	${STATUS}	SUITE: Verify Permission Root Access
	Wait Until Keyword Succeeds	5 min	30	SUITE:setting an Root password to default afte factory settings
	SUITE:Login ZPE Cloud and find the device
	Wait Until Page Contains	nodegrid	20s
	${status}=	Run Keyword and Return Status	SUITE:Turns Online
	Run Keyword If	${status} == False	CLI::ZPECloud::Restart agent process via root
	Wait Until Keyword Succeeds	5 min	30	SUITE:Turns Online
	SUITE:Teardown

Restore Factory Default On Premise
	SUITE:Setup
	Click Element	clearcloud
	Click Element	//*[@id="nonAccessControls"]/input[1]
	Handle Alert
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	9x	20s	SUITE:Login Successful
	Close All Browsers
	${STATUS}	Evaluate NG Versions: ${NGVERSION} > 4.2
	Run Keyword If	${STATUS}	SUITE:Login after restore and Configure new password
	${STATUS}	Evaluate NG Versions: ${NGVERSION} < 5.0
	Run Keyword If	${STATUS}	SUITE:Login after restore and Configuring new password
	SUITE:Login ZPE Cloud and find the device
	Wait Until Page Contains	nodegrid	20s
	Wait Until Page Contains	Offline
	SUITE:Verify On Premise Company
	SUITE:Verify ZPE Cloud Agent
	SUITE:Verify Permission SSH
	${STATUS}	Evaluate NG Versions: ${NGVERSION} > 4.2
	Run Keyword If	${STATUS}	SUITE: Verify Permission Root Access
	Wait Until Keyword Succeeds	5 min	30	SUITE:setting an Root password to default afte factory settings
	SUITE:Unenroll device from QA Instance
	SUITE:Return Device to the Previous Company
	Move Device From AVAILABLE To ENROLLED Tab
	SUITE:Login ZPE Cloud and find the device
	Wait Until Page Contains	nodegrid	20s
	${status}=	Run Keyword and Return Status	SUITE:Turns Online
	Run Keyword If	${status} == False	CLI::ZPECloud::Restart agent process via root
	Wait Until Keyword Succeeds	5 min	30	SUITE:Turns Online

*** Keywords ***
SUITE:Setup
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Basic::System::Toolkit::Open Tab
	Wait Until Page Contains Element	icon_factoryDefault
	Click Element	icon_factoryDefault
	Wait Until Page Contains Element	nonAccessControls

SUITE:Teardown
	Close All Browsers

SUITE:Login ZPE Cloud and find the device
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Dashboard::Access::Open Tab
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5

SUITE:Login Successful
	GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	password
	Input Text	username	${DEFAULT_USERNAME}
	Input Text	password	${FACTORY_PASSWORD}
	Click Element	id=login-btn
	GUI::Basic::Spinner Should Be Invisible

SUITE:Login after restore and Configure new password
	GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	password
	Input Text	username	${DEFAULT_USERNAME}
	Input Text	password	${FACTORY_PASSWORD}
	Click Element	id=login-btn
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Wait Until Element Is Accessible	newPassword
	Input Text	newPassword	${QA_PASSWORD}
	Input Text	confirmation	${QA_PASSWORD}
	Click Element	change-password-btn
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:Login after restore and Configuring new password
	GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	password
	Input Text	username	${DEFAULT_USERNAME}
	Input Text	password	${FACTORY_PASSWORD}
	Click Element	id=login-btn
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Change Password	${PASSWORDNG}
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:Unenroll device from QA Instance
	SUITE:Get Device Enrolment Info on QA Instance
	CLI:Connect As Root	${HOST}	${PASSWORDNG}
	${UNENROLL}=	Set Variable	zpe_cloud_enroll -c ${CUSTOMERCODEQA} -k ${ENROLLMENTKEYQA} -u ${ZPECLOUD_HOMEPAGE} -s
	Write	${UNENROLL}
	Read Until	(yes, no):
	Write	yes
	${OUTPUT}=	Read Until Prompt
	Should Contain Any	${OUTPUT}	Unenrollment process successful!

SUITE:Get Device Enrolment Info on QA Instance
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Settings::Open Tab
	${CUSTOMERCODEQA}=	Get Value	ca-setgen-customer-code-input
	Click Element	//*[@id="ca-setgen-password-toggle-btn"]/span[1]
	${ENROLLMENTKEYQA}=	Get Value	filled-adornment-password
	${CUSTOMERCODEQA}=	Set Suite Variable	${CUSTOMERCODEQA}
	${ENROLLMENTKEYQA}=	Set Suite Variable	${ENROLLMENTKEYQA}
	Close All Browsers
	[return]	${CUSTOMERCODEQA}	${ENROLLMENTKEYQA}

SUITE:Return Device to the Previous Company
	SUITE:Get Device Enrolment Info on QA Instance
	CLI:Connect As Root	${HOST}	${PASSWORDNG}
	${UNENROLL}=	Set Variable	zpe_cloud_enroll -c ${CUSTOMERCODEQA} -k ${ENROLLMENTKEYQA} -u ${ZPECLOUD_HOMEPAGE}
	Write	${UNENROLL}
	${OUTPUT}=	Read Until	!
	Should Contain Any	${OUTPUT}	Enrollment process successful!

SUITE:Verify On Premise Company
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Basic::System::Toolkit::Open Tab
	Wait Until Page Contains Element	icon_cloudEnrollment	30s
	Click Element	icon_cloudEnrollment
	Wait Until Page Contains Element	status	15s
	${COMPANY}=	Get Value	status
	Should Be Equal	${COMPANY}	${ONPREMISECOMPANY}

SUITE:Turns Online
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
#	Sleep	5
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SERIALNUMBER}
#	Sleep	5
	Wait Until Page Contains	Online	15s

SUITE:Verify ZPE Cloud Agent
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Security::Open Services tab
	GUI::Basic::Spinner Should Be Invisible
	${CHECK1}=	run keyword and return status	Checkbox Should Be Selected	cloud_enable
	Run Keyword If	${CHECK1} == False	Click Element	cloud_enable
	Run Keyword If	${CHECK1} == False	Click Element	saveButton
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:Verify Permission SSH
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Security::Open Services tab
	GUI::Basic::Spinner Should Be Invisible
	${CHECK2}=	run keyword and return status	Checkbox Should Be Selected	allow
	Run Keyword If	${CHECK2} == False	Click Element	allow
	Run Keyword If	${CHECK2} == False	Click Element	saveButton
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE: Verify Permission Root Access
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Security::Open Services tab
	${CHECK2}=	run keyword and return status	Checkbox Should Be Selected	rootconsole
	Run Keyword If	${CHECK2} == False	Click Element	rootconsole
	Run Keyword If	${CHECK2} == False	Click Element	saveButton
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

Move Device From AVAILABLE To ENROLLED Tab
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Devices::Available::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SERIALNUMBER}
	Wait until Page Contains	${SERIALNUMBER}	30s
	Wait Until Page Contains Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Click Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Wait Until Page Contains Element	//*[@id="enroll"]	15s
	Click Element	//*[@id="enroll"]
	Wait Until Keyword Succeeds	5m	1	Wait Until Page Contains	Device Enrolled Successfully

SUITE:setting an Root password to default afte factory settings
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Network::Open Settings Tab
	${HOST_NAME}=	Get value	id=hostname
	GUI::Access::Open
	Click Element	//div[@class="peer_header"]//a[text()="Console"]
	sleep	5
	${STATUS}	Evaluate NG Versions: ${NGVERSION} >= 5.6
	Run Keyword If	${STATUS}	GUI:Basic::Wait Until Window Exists	nodegrid - Console
	...	ELSE	GUI:Basic::Wait Until Window Exists	${HOST_NAME}
	Wait Until Page Contains Element	//iframe[@id='termwindow']
	Select Frame	//iframe[@id='termwindow']
	Sleep	5
	Press Keys	//body	RETURN
	Press Keys	//body	RETURN
	${OUTPUT}=	SUITE:Console Command Output	${root_login}
	Press Keys	//body	ENTER
	${OUTPUT}=	SUITE:Console Command Output	passwd
	Sleep	5
	SUITE:Console Command Output	${PWDNG}
	Sleep	5
	SUITE:Console Command Output	${PWDNG}


SUITE:Console Command Output
	[Arguments]	${COMMAND}
	[Documentation]	Input the given command into the ttyd terminal and retrieve the output after prompt lines
	...	== ARGUMENTS ==
	...	-	COMMAND = Command to be executed
	...	== EXPECTED RESULT ==
	...	Input the command into the ttyd terminal and returns the OUTPUT between the command and the last prompt

	${CHECK_TAB}=	Run Keyword And Return Status	Should Not Contain	${COMMAND}	TAB
	Run Keyword If	${CHECK_TAB}	Press Keys	//body	${COMMAND}	RETURN
	...	ELSE	Press Keys	//body	${COMMAND}
	Sleep	1
