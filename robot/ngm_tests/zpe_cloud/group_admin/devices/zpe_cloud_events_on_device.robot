*** Settings ***
Documentation	Triggering events on ZPE Cloud and checking the information under the device
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	EVENTS
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${DEVICE}	nodegrid
${CONSOLE}	xpath=//*[@id='termwindow']
${BACKUPSUCCESS}	Backup in the selected devices was requested
${BACKUPDELETED}	Backups are deleted successfully
${SELECTDEVICE}	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
${BACKUPBUTTON}	//*[@id="function-button-2"]
${CONNECTIONERROR}	[error.connection.failure] Could not establish a connection
${STATUS}	Online

*** Test Cases ***
Test triggering Backup event on ZPE Cloud and checking the information under the device
	${status}=	Run Keyword and Return Status	SUITE:Check That Devices::Enrolled Page Contain Online Devices
	Run Keyword If	${status} == False	CLI::ZPECloud::Restart agent process via root
	SUITE:Setup
	GUI::ZPECloud::Devices::Open Tab
	Wait Until Keyword Succeeds	60	4	SUITE:Online_Status
	SUITE:Trigger event and check the information on the device - Backup
	[Teardown]	SUITE:Delete Backup

Test triggering Remote Access event on ZPE Cloud and checking the information under the device
	SUITE:Trigger event and check the information on the device - Remote Access

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud

SUITE:Teardown
	Close All Browsers

SUITE:Trigger event and check the information on the device - Backup
	SUITE:Check Backup Event Information Under the Device Before Event
	SUITE:Setup
	SUITE:Backup Device Configuration - Operation was Successful
	Sleep	10
	SUITE:Check Backup Event Information Under the Device After Event

SUITE:Backup Device Configuration
	GUI::ZPECloud::Devices::Open Tab
	Wait Until Page Contains Element	${SELECTDEVICE}	30s
	Click Element	${SELECTDEVICE}
	Wait Until Element Is Enabled	${BACKUPBUTTON}	5s
	Click Element	${BACKUPBUTTON}
	Wait Until Page Contains Element	//*[@id="file-protection-radio"]	10s
	Click Element	//*[@id="opt-none"]
	Click Element	//*[@id="opt-temporary"]
	Wait Until Element is Enabled	submit-btn	10s
	Click Element	submit-btn
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	${BACKUPSUCCESS}

SUITE:Status is 'Successful'
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Sleep	10
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div
	${CONTAINTEST}=	Run Keyword And return Status	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div	Successful
	Run Keyword If	${CONTAINTEST} == False	Reload Page
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div
	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/table/tbody/tr[1]/td[9]/div	Successful

SUITE:Delete Backup
	SUITE:Setup
	GUI::ZPECloud::Profiles::Backup::Open Tab
	Wait Until Page Contains Element	//*[@id="ca-profiles-bkp-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="ca-profiles-bkp-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	GUI::ZPECloud::Basic::Click Button	delete
	Wait Until Page Contains Element	//*[@id="confirm-btn"]	15s
	Click Element	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	30	2	Wait Until Page Contains	${BACKUPDELETED}

SUITE:Backup Device Configuration - Operation was Successful
	SUITE:Backup Device Configuration
	Sleep	4 minutes
	SUITE:Setup
	GUI::ZPECloud::Profiles::Backup::Open Tab
	Wait Until Page Contains Element	//*[@id="ca-profiles-bkp-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input	10s
	Click Element	//*[@id="ca-profiles-bkp-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Element Should Contain	//*[@id="ca-profiles-bkp-table-1"]/div/div/table/tbody/tr[1]/td[7]	Temporary
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[8]	10s
	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div/div/table/tbody/tr[1]/td[8]	Backup configuration
	Wait Until Keyword Succeeds	5 min	2	SUITE:Status is 'Successful'
	Close All Browsers

SUITE:Check Backup Event Information Under the Device Before Event
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Tracking::Open Event List Tab
#check the Cloud Event 'Nodegrid Backup Started'
#	Sleep	5
	Wait Until Page Contains Element	//*[@id="503"]/td[4]	10s
	${BACKUPEVENTCOUNTBEFORE1}=	Get Text	//*[@id="502"]/td[4]
	${BACKUPEVENTCOUNTBEFORE1}=	Convert To Integer	${BACKUPEVENTCOUNTBEFORE1}
	Should Be True	${BACKUPEVENTCOUNTBEFORE1} >= 0
#check the Cloud Event 'Nodegrid Backup Successful Finished'
	Wait Until Page Contains Element	//*[@id="503"]/td[4]	10s
	${BACKUPEVENTCOUNTBEFORE2}=	Get Text	//*[@id="503"]/td[4]
	${BACKUPEVENTCOUNTBEFORE1}=	Convert To Integer	${BACKUPEVENTCOUNTBEFORE2}
	Should Be True	${BACKUPEVENTCOUNTBEFORE2} >= 0
	${BACKUPEVENTCOUNTBEFORE1}=	Set Suite Variable	${BACKUPEVENTCOUNTBEFORE1}
	${BACKUPEVENTCOUNTBEFORE2}=	Set Suite Variable	${BACKUPEVENTCOUNTBEFORE2}
	Close All Browsers

SUITE:Check Backup Event Information Under the Device After Event
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Tracking::Open Event List Tab
	Sleep	5
#check the Cloud Event 'Nodegrid Backup Started'
	Wait Until Page Contains Element	//*[@id="502"]/td[4]	10s
	${EVENTCOUNT}=	Get Text	//*[@id="502"]/td[4]
	Convert To Integer	${EVENTCOUNT}
	Should Be True	${EVENTCOUNT} > ${BACKUPEVENTCOUNTBEFORE1}
#check the Cloud Event 'Nodegrid Backup Successful Finished'
	Wait Until Page Contains Element	//*[@id="503"]/td[4]	10s
	${EVENTCOUNT}=	Get Text	//*[@id="503"]/td[4]
	Convert To Integer	${EVENTCOUNT}
	Should Be True	${EVENTCOUNT} > ${BACKUPEVENTCOUNTBEFORE2}
	Close All Browsers

SUITE:Trigger event and check the information on the device - Remote Access
	SUITE:Check Remote Access Event Information Under the Device Before Event
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Dashboard::Access::Open Tab
	Sleep	15
	Page Should Contain	Online
	Page Should Contain	${DEVICE}
	GUI::ZPECloud::Basic::Click Button	console
	wait until keyword succeeds	60	5	GUI::Basic::Spinner Should Be Invisible

	Wait Until Keyword Succeeds	60	5	Switch Window	NEW
	Wait Until Keyword Succeeds	60	5	GUI::Basic::Direct Login	${USERNG}	${PWDNG}
	Sleep	5
	${STATUS}	Evaluate NG Versions: ${NGVERSION} >= 5.6
	Run Keyword If	${STATUS}	GUI:Basic::Wait Until Window Exists	nodegrid.localdomain - Console
	...	ELSE	GUI:Basic::Wait Until Window Exists	nodegrid.localdomain
	Wait Until Page Contains Element	${CONSOLE}	30s
	Wait Until Keyword Succeeds	60	5	Select Frame	${CONSOLE}
	Sleep	10
	Press Keys	None	RETURN
	Press Keys	None	RETURN
	${OUTPUT}=	SUITE:Console Command Output	ls
	Should Contain	${OUTPUT}	access/	system/	settings/
	Close All Browsers
	SUITE:Check Remote Access Event Information Under the Device After Event	${REMOTEACCESSEVENTCOUNTBEFORE1}	${REMOTEACCESSEVENTCOUNTBEFORE2}

SUITE:Check Remote Access Event Information Under the Device Before Event
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Tracking::Open Event List Tab
	Sleep	5
#check the Cloud Event 'Nodegrid Remote Access Started'
	Wait Until Page Contains Element	//*[@id="505"]/td[4]	10s
	${REMOTEACCESSEVENTCOUNTBEFORE1}=	Get Text	//*[@id="505"]/td[4]
	Convert To Integer	${REMOTEACCESSEVENTCOUNTBEFORE1}
	Should Be True	${REMOTEACCESSEVENTCOUNTBEFORE1} >= 0
#check the Cloud Event 'Nodegrid Remote Access Failed'
	Wait Until Page Contains Element	//*[@id="507"]/td[4]	10s
	${REMOTEACCESSEVENTCOUNTBEFORE2}=	Get Text	//*[@id="507"]/td[4]
	Should Be True	${REMOTEACCESSEVENTCOUNTBEFORE2} >= 0
	${REMOTEACCESSEVENTCOUNTBEFORE1}=	Set Suite Variable	${REMOTEACCESSEVENTCOUNTBEFORE1}
	${REMOTEACCESSEVENTCOUNTBEFORE2}=	Set Suite Variable	${REMOTEACCESSEVENTCOUNTBEFORE2}
	Close All Browsers

SUITE:Check Remote Access Event Information Under the Device After Event
	[Arguments]	${REMOTEACCESSEVENTCOUNTBEFORE1}	${REMOTEACCESSEVENTCOUNTBEFORE2}
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Tracking::Open Event List Tab
	Sleep	5
#check the Cloud Event 'Nodegrid Remote Access Started'
	Wait Until Page Contains Element	//*[@id="505"]/td[4]	10s
	${EVENTCOUNT}=	Get Text	//*[@id="505"]/td[4]
	Convert To Integer	${REMOTEACCESSEVENTCOUNTBEFORE1}
	Convert To Integer	${EVENTCOUNT}
	Should Be True	${EVENTCOUNT} > ${REMOTEACCESSEVENTCOUNTBEFORE1}
#check the Cloud Event 'Nodegrid Remote Access Failed'
	Wait Until Page Contains Element	//*[@id="507"]/td[4]	10s
	${EVENTCOUNT2}=	Get Text	//*[@id="507"]/td[4]
	Convert To Integer	${REMOTEACCESSEVENTCOUNTBEFORE2}
	Convert To Integer	${EVENTCOUNT2}
	Should Be Equal	${EVENTCOUNT2}	${REMOTEACCESSEVENTCOUNTBEFORE2}

SUITE:Console Command Output
	[Arguments]	${COMMAND}
	[Documentation]	Input the given command into the ttyd terminal and retrieve the output after prompt lines
	...	== ARGUMENTS ==
	...	-	COMMAND = Command to be executed
	...	== EXPECTED RESULT ==
	...	Input the command into the ttyd terminal and returns the OUTPUT between the command and the last prompt
	${CHECK_TAB}=	Run Keyword And Return Status	Should Not Contain	${COMMAND}	TAB
	Run Keyword If	${CHECK_TAB}	Press Keys	None	${COMMAND}	RETURN
	...	ELSE	Press Keys	None	${COMMAND}
	Sleep	1
	${SELECTION}=	Execute JavaScript	term.selectAll(); return term.getSelection().trim();
	Should Not Contain	${SELECTION}	${CONNECTIONERROR}
	${LINES}=	Split String	${SELECTION}	\n
	${INDEXES_OCCURRED}=	Create List
	${LENGTH}=	Get Length	${LINES}
	FOR	${INDEX}	IN RANGE	0	${LENGTH}
		${LINE}=	Get From List	${LINES}	${INDEX}
		${CHECK}=	Run Keyword And Return Status	Should Contain	${LINE}	]#
		Run Keyword If	${CHECK}	Append to List	${INDEXES_OCCURRED}	${INDEX}
	END
	${STRING}=	Set Variable
	${END}=	Get From List	${INDEXES_OCCURRED}	-1
	${PREVIOUS}=	Get From List	${INDEXES_OCCURRED}	-2
	FOR	${INDEX}	IN RANGE	${PREVIOUS}	${END}
		${LINE}=	Get From List	${LINES}	${INDEX}
		${STRING}=	Set Variable	${STRING}\n${LINE}
	END
	${STRING}=	Get Substring	${STRING}	1
	[Return]	${STRING}

SUITE:Online_Status
	Reload Page
	Sleep	5
	Wait Until Page Contains	${STATUS}	15s

SUITE:Check That Devices::Enrolled Page Contain Online Devices
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Wait Until Page Contains	Online	15s