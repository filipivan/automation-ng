*** Settings ***
Documentation	Tests to check Cloud status on device
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	CLOUD_STATUS
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_3

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${STATUS}	Online
${CONNECT}	Connected - Licensed
${URL_ENROLL}	${ZPECLOUD_HOMEPAGE}
${COMP}	${COMPANY}

*** Test Cases ***
Test device status on Cloud
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	SUITE:Check Online Devices

Test Cloud status on device
	[Tags]	DEVICE_COMPANY_INFO
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Basic::Tracking::ZPE Cloud::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	id=connected	30s
	${CON}	Get Value	id=connected
	${URL}	Get Value	id=url
	${COMP}	Get Value	id=company
	Should Be Equal	${CON}	${CONNECT}
	Should Be Equal	${URL}	${URL_ENROLL}
	Should Be Equal	${COMP}	${COMPANY}

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::System::Preferences::Add cordinates and save if needed
	SUITE:Teardown

SUITE:Check Online Devices
	GUI::ZPECloud::Dashboard::Access::Open Tab
	Sleep	5
	Wait Until Page Contains	${STATUS}	10s

SUITE:Teardown
	Close All Browsers
