*** Settings ***
Documentation	Tests to check if the status of a device under ZPE Cloud is 'Failover'
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	DEVICE_STATUS	WEB
Force Tags	GUI	ZPECLOUD	${BROWSER}	BUG_CLOUD-1003	NON-CRITICAL	DEVICE	PART_3

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE}	nodegrid
${CONNECTIONNAME}	test_connection
${CELLULARCONNECTION}	test_cellular
${SIM_APN}	vzwinternet
${FAILOVERIP}	1.1.1.1
${CONNECTION_TYPE}	Mobile Broadband GSM
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${DEVICESDISPLAY}	(//*[name()='text'][@dominant-baseline='central'][normalize-space()='Online'])[2]

*** Test Cases ***
Test 'Failover' Device under MAP
	GUI::System::Preferences::Add cordinates and save if needed
	Close All Browsers
	GUI::ZPECloud::Add connection	${USERNG}	${PWDNG}	${CONNECTIONNAME}
	GUI::ZPECloud::Configure Failover	${USERNG}	${PWDNG}	${CONNECTIONNAME}	${FAILOVERIP}
	wait until keyword succeeds	160	5	SUITE:Check That MAP Page Contain Failover Devices
	[Teardown]	SUITE:Teardown

Test 'Failover' Device under Dashboard::Access
	GUI::ZPECloud::Add connection	${USERNG}	${PWDNG}	${CONNECTIONNAME}
	GUI::ZPECloud::Configure Failover	${USERNG}	${PWDNG}	${CONNECTIONNAME}	${FAILOVERIP}
	wait until keyword succeeds	160	5	SUITE:Check That Dashboard::Access Page Contain Failover Devices
	[Teardown]	SUITE:Teardown

Test 'Failover' Device under MAP with Cellular Connection
	[Tags]	SIMCARD	CELLULAR_FAILOVER
	[Setup]	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Network::Open Connections Tab
	${HASCELLULAR}=	Run Keyword And return Status	Page Should Contain	Mobile Broadband GSM
	Run Keyword If	${HASCELLULAR} == False	SUITE:Create Cellular Connection
	Run Keyword If	${HASCELLULAR} == True	SUITE:Get Cellular Connection Name
	${PARENT}=	Get Element Attribute	xpath=(//*[text()='Mobile Broadband GSM']/..)	id
	${UPDOWN}=	Get Text	//*[@id="${PARENT}"]/td[6]
	${CON}=	Run Keyword And return Status	Should be Equal	${UPDOWN}	Up
	Run Keyword If	${CON} == False	SUITE:Up Connection	${PARENT}
	${CONNECTED_STATUS}=	Get Text	//*[@id="${PARENT}"]/td[3]
	${STATUS}=	Run Keyword And return Status	Should be Equal	${CONNECTED_STATUS}	Connected
	Run Keyword If	${STATUS} == False	SUITE:Up Connection	${PARENT}
	GUI::ZPECloud::Configure Failover	${USERNG}	${PWDNG}	${CELLULARCONNECTION}	${FAILOVERIP}
	wait until keyword succeeds	160	5s	SUITE:Check That MAP Page Contain Failover Devices
	[Teardown]	SUITE:Teardown

Test 'Failover' Device under Dashboard::Access with Cellular Connection
	[Tags]	SIMCARD	CELLULAR_FAILOVER
	[Setup]	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Network::Open Connections Tab
	${HASCELLULAR}=	Run Keyword And return Status	Page Should Contain	Mobile Broadband GSM
	Run Keyword If	${HASCELLULAR} == False	SUITE:Create Cellular Connection
	Run Keyword If	${HASCELLULAR} == True	SUITE:Get Cellular Connection Name
	${PARENT}=	Get Element Attribute	xpath=(//*[text()='Mobile Broadband GSM']/..)	id
	${UPDOWN}=	Get Text	//*[@id="${PARENT}"]/td[6]
	${CON}=	Run Keyword And return Status	Should be Equal	${UPDOWN}	Up
	Run Keyword If	${CON} == False	SUITE:Up Connection	${PARENT}
	${CONNECTED_STATUS}=	Get Text	//*[@id="${PARENT}"]/td[3]
	${STATUS}=	Run Keyword And return Status	Should be Equal	${CONNECTED_STATUS}	Connected
	Run Keyword If	${STATUS} == False	SUITE:Up Connection	${PARENT}
	GUI::ZPECloud::Configure Failover	${USERNG}	${PWDNG}	${CELLULARCONNECTION}	${FAILOVERIP}
	wait until keyword succeeds	160	5s	SUITE:Check That Dashboard::Access Page Contain Failover Devices
	[Teardown]	SUITE:Teardown

Test 'Failover' Device under Devices::Enrolled with Cellular Connection
	[Tags]	SIMCARD	CELLULAR_FAILOVER
	[Setup]	SUITE:Setup
	GUI::Network::Open Connections Tab
	${HASCELLULAR}=	Run Keyword And return Status	Page Should Contain	Mobile Broadband GSM
	Run Keyword If	${HASCELLULAR} == False	SUITE:Create Cellular Connection
	Run Keyword If	${HASCELLULAR} == True	SUITE:Get Cellular Connection Name
	${PARENT}=	Get Element Attribute	xpath=(//*[text()='Mobile Broadband GSM']/..)	id
	${UPDOWN}=	Get Text	//*[@id="${PARENT}"]/td[6]
	${CON}=	Run Keyword And return Status	Should be Equal	${UPDOWN}	Up
	Run Keyword If	${CON} == False	SUITE:Up Connection	${PARENT}
	${CONNECTED_STATUS}=	Get Text	//*[@id="${PARENT}"]/td[3]
	${STATUS}=	Run Keyword And return Status	Should be Equal	${CONNECTED_STATUS}	Connected
	Run Keyword If	${STATUS} == False	SUITE:Up Connection	${PARENT}
	GUI::ZPECloud::Configure Failover	${USERNG}	${PWDNG}	${CELLULARCONNECTION}	${FAILOVERIP}
	wait until keyword succeeds	160	5s	SUITE:Check That Devices::Enrolled Page Contain Failover Devices
	[Teardown]	SUITE:Teardown

Test 'Failover' Device under Devices::Enrolled
	GUI::ZPECloud::Add connection	${USERNG}	${PWDNG}	${CONNECTIONNAME}
	GUI::ZPECloud::Configure Failover	${USERNG}	${PWDNG}	${CONNECTIONNAME}	${FAILOVERIP}
	wait until keyword succeeds	160	5s	SUITE:Check That Devices::Enrolled Page Contain Failover Devices
#	[Teardown]	SUITE:Teardown

*** Keywords ***
SUITE:Setup
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Network::Open Settings Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	failover	15s
	${IS_SELECTED}=	Run Keyword And return Status	Checkbox Should Not Be Selected	failover
	Run Keyword If	${IS_SELECTED} == False	Click Element	failover
	Run Keyword If	${IS_SELECTED} == False	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	${HASCELLULAR}=	Run Keyword and Return Status	Page Should Contain	${CELLULARCONNECTION}
	Run Keyword if	${HASCELLULAR} == True	SUITE:Delete Connection	${CELLULARCONNECTION}
	${NEWCONN}=	Run Keyword and Return Status	Page Should Contain	${CONNECTIONNAME}
	Run Keyword if	${NEWCONN} == True	SUITE:Delete Connection	${CONNECTIONNAME}
	GUI::Security::Open Firewall tab
	Click Element	//*[@id='OUTPUT:IPv4']/td[2]/a
	Sleep	2
	Click Element	xpath=//tr[2]/td/input
	Click Element	delButton
	Wait Until Keyword Succeeds	60	2	GUI::Basic::Spinner Should Be Invisible
	Sleep	5
	Page Should Not Contain Element	xpath=//tr[2]/td/input
	Sleep	10
	SUITE:Setup
	Close All Browsers

SUITE:Check That MAP Page Contain Failover Devices
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	${DEVICESDISPLAY}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Status	30s
	Wait Until Keyword Succeeds	60	2	SUITE:Status is Failover

SUITE:Check That Dashboard::Access Page Contain Failover Devices
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Dashboard::Access::Open Tab
	Sleep	5
	Wait Until Keyword Succeeds	60	2	SUITE:Status is Failover

SUITE:Check That Devices::Enrolled Page Contain Failover Devices
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Wait Until Keyword Succeeds	60	2	SUITE:Status is Failover

SUITE:Create Cellular Connection
	Click Element	addButton
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	connType
	GUI::Basic::Wait Until Element Is Accessible	connType
	Select From List By Label	//*[@id="connType"]	${CONNECTION_TYPE}
	${STATUS}	Evaluate NG Versions: ${NGVERSION} > 4.2
	Run Keyword If	${STATUS}	Select From List By Value	connItfName	cdc-wdm0
	${STATUS}	Evaluate NG Versions: ${NGVERSION} < 5.0
	Run Keyword If	${STATUS}	Select From List By Value	connIeth	cdc-wdm0
	Input Text	connName	${CELLULARCONNECTION}
	Input Text	mobileAPN	${SIM_APN}
	${CHECK}=	Run Keyword and Return Status	Checkbox Should Be Selected	connAuto
	Run Keyword If	${CHECK} == False	Click Element	connAuto
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Should not be Empty	//*[@id="${CELLULARCONNECTION}"]/td[7]

SUITE:Get Cellular Connection Name
	${PARENT}=	Get Element Attribute	xpath=(//*[text()='Mobile Broadband GSM']/..)	id
	${CELLULARCONNECTION}=	Get Text	//*[@id='${PARENT}']/td[2]

SUITE:Up Connection
	[Arguments]	${IDCONNECTION}
	Click Element	//*[@id="${IDCONNECTION}"]/td[1]
	Click Element	//*[@id="nonAccessControls"]/input[3]
	GUI::Basic::Spinner Should Be Invisible

SUITE:Delete Connection
	[Arguments]	${CONN}
	GUI::Network::Open Connections Tab
	${CONNECTIONEXISTS}=	Run Keyword And return Status	Page Should Contain	${CONN}
	Run Keyword If	${CONNECTIONEXISTS} == True	Click Element	xpath=//tr[@id='${CONN}']/td/input
	Run Keyword If	${CONNECTIONEXISTS} == True	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

SUITE:Status is Failover
	Reload Page
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${SERIALNUMBER}
	Sleep	2
	Wait Until Page Contains	Failover	30s

SUITE:Status is Online
	Reload Page
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${SERIALNUMBER}
	Sleep	2
	Wait Until Page Contains	Online	15s

SUITE:Access Web
	[Arguments]	${STATUS}
	Sleep	15
	Run Keyword If	'${STATUS}' == 'Failover'	SUITE:Status is Failover
	Run Keyword If	'${STATUS}' == 'Online'	SUITE:Status is Online
	Wait Until Page Contains	${DEVICE}
	GUI::ZPECloud::Basic::Click Button	web
	wait until keyword succeeds	60	5	GUI::Basic::Spinner Should Be Invisible
	Sleep	5
	Wait Until Keyword Succeeds	60	2	Switch Window	NEW
	Sleep	5
	Wait Until Keyword Succeeds	60	2	SUITE:Web Access
	Input Text	username	${USERNG}
	Input Text	password	${PWDNG}
	Click Element	login-btn
	wait until keyword succeeds	60	5	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	main_menu

SUITE:Web Access
	${LOGIN}=	Run Keyword And return Status	Page Should Contain Element	username
	Run Keyword If	${LOGIN} == False	Reload Page
	Sleep	5
	Page Should Contain Element	username
