*** Settings ***
Documentation	Tests to check if the status of a device under ZPE Cloud is 'Online'
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	DEVICE_STATUS	WEB
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_3

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${DEVICESDISPLAY}	(//*[name()='text'][@dominant-baseline='central'][normalize-space()='Online'])[2]
${STATUS}	Online

*** Test Cases ***
Test 'Online' Device under MAP
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	SUITE:Check That MAP Page Contain Online Devices
	[Teardown]	SUITE:Teardown

Test 'Online' Device under Dashboard::Access
	[Setup]		GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	SUITE:Check That Dashboard::Access Page Contain Online Devices
	[Teardown]	SUITE:Teardown

Test 'Online' Device under Devices::Enrolled
	[Setup]		GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	SUITE:Check That Devices::Enrolled Page Contain Online Devices

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::System::Preferences::Add cordinates and save if needed
	SUITE:Teardown

SUITE:Teardown
	Close All Browsers

SUITE:Check That MAP Page Contain Online Devices
	GUI::ZPECloud::Dashboard::Map::Open Tab
	Sleep	5
	Click Element	${DEVICESDISPLAY}
	Sleep	5
	Wait Until Page Contains	Status	15s
	Wait Until Page Contains	${STATUS}	20s

SUITE:Check That Dashboard::Access Page Contain Online Devices
	GUI::ZPECloud::Dashboard::Access::Open Tab
	Sleep	5
	Wait Until Page Contains	${STATUS}	20s

SUITE:Check That Devices::Enrolled Page Contain Online Devices
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Wait Until Page Contains	${STATUS}	20s