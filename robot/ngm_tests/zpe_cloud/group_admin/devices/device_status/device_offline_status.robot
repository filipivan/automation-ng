*** Settings ***
Documentation	Tests to check if the status of a device under ZPE Cloud is 'Offline'
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	DEVICE_STATUS	WEB	CONSOLE_ACCESS
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_3

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${DEVICESDISPLAY}	(//*[name()='text'][@dominant-baseline='central'][normalize-space()='Online'])[2]
${STATUS}	Offline
${ROOTPROMPT}	root@nodegrid:~#
${DEVICE}	nodegrid
${CONSOLE}	xpath=//*[@id='termwindow']
${SHELLCOMMAND}	shell sudo su -
${REBOOTCOMMAND}	reboot
${REBOOTOUTPUT}	The system is going down for reboot NOW!
${CONSOLEBUTTON}	//*[@id="|nodegrid"]/div[1]/div[2]/a

*** Test Cases ***
Test 'Offline' Device under MAP
	wait until keyword succeeds	60	5	SUITE:Check That MAP Page Contain Offline Devices
#	[Teardown]	SUITE:Teardown

Test 'Offline' Device under Dashboard::Access
#	[Setup]	SUITE:Setup
	wait until keyword succeeds	60	5	SUITE:Check That Dashboard::Access Page Contain Offline Devices
#	[Teardown]	SUITE:Teardown

Test 'Offline' Device under Devices::Enrolled
#	[Setup]	SUITE:Setup
	wait until keyword succeeds	60	5	SUITE:Check That Devices::Enrolled Page Contain Offline Devices
	Wait Until Keyword Succeeds	5 min	5 sec	SUITE:Online_Status
#	[Teardown]	SUITE:Teardown

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	Wait Until Page Contains Element	${CONSOLEBUTTON}
	Click Element	${CONSOLEBUTTON}
	wait until keyword succeeds	30	5	GUI::Basic::Spinner Should Be Invisible
	Sleep	5
	Switch Window	NEW
	Wait Until Page Contains Element	${CONSOLE}
	Select Frame	${CONSOLE}
	Sleep	5
	SUITE:Reboot

SUITE:Teardown
	Close All Browsers

SUITE:Online_Status
	Reload Page
	Sleep	5
	Wait Until Page Contains	Online	15s

SUITE:Check That MAP Page Contain Offline Devices
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
#	Sleep	10
	Wait Until Page Contains Element	${DEVICESDISPLAY}	15s
	Click Element	${DEVICESDISPLAY}
	Sleep	5
	Wait Until Keyword Succeeds	60	4	SUITE:Status

SUITE:Check That Dashboard::Access Page Contain Offline Devices
#	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
#	Sleep	10
	GUI::ZPECloud::Dashboard::Access::Open Tab
	Wait Until Keyword Succeeds	60	4	SUITE:Status

SUITE:Check That Devices::Enrolled Page Contain Offline Devices
#	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
#	Sleep	10
	GUI::ZPECloud::Devices::Open Tab
	Wait Until Keyword Succeeds	60	4	SUITE:Status

SUITE:Access Console
	GUI::ZPECloud::Dashboard::Access::Open Tab
	Sleep	5
	Page Should Contain	Online
	Page Should Contain	${DEVICE}
	GUI::ZPECloud::Basic::Click Button	console
	wait until keyword succeeds	60	5	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	60	5	Switch Window	NEW
	Wait Until Keyword Succeeds	60	5	GUI::Basic::Direct Login	${USERNG}	${PWDNG}
	Sleep	5

SUITE:Reboot
	Sleep	5
#	Wait Until Keyword Succeeds	60	5	Wait Until Page Contains Element	${CONSOLE}
#	Select Frame	${CONSOLE}
	Sleep	20
	Press Keys	None	RETURN
	Press Keys	None	RETURN
	Press Keys	None	RETURN
	Press Keys	None	RETURN
	${OUTPUT}=	SUITE:Console Command Output	${SHELLCOMMAND}
	Should Contain	${OUTPUT}	${ROOTPROMPT}
	${OUTPUT}=	SUITE:Console Command Output	${REBOOTCOMMAND}
	Should Contain	${OUTPUT}	${REBOOTOUTPUT}
	Close All Browsers
	Sleep	20

SUITE:Console Command Output
	[Arguments]	${COMMAND}
	${CHECK_TAB}=	Run Keyword And Return Status	Should Not Contain	${COMMAND}	TAB
	Run Keyword If	${CHECK_TAB}	Press Keys	None	${COMMAND}	RETURN
	...	ELSE	Press Keys	None	${COMMAND}
	Sleep	1
	${CONSOLE_CONTENT}=	Execute JavaScript	term.selectAll(); return term.getSelection().trim();
	Should Not Contain	${CONSOLE_CONTENT}	[error.connection.failure] Could not establish a connection to device
	${OUTPUT}=	Output From Last Command	${CONSOLE_CONTENT}
	[Return]	${OUTPUT}

SUITE:Status
	Reload Page
#	Sleep	5
	Wait until Page Contains	${STATUS}	15s