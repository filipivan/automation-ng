*** Settings ***
Documentation	Tests that 'First connection' happened before 'Last connection'
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	DEVICE_INFORMATION	CONNECTION
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_3

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${FIRSTCONNECTION}
${LASTCONNECTION}
${SEPARATOR}	/
*** Test Cases ***
Test device First Connection
	${status}=	Run Keyword and Return Status	SUITE:Verify if Device is on Cloud
	Run Keyword If	${status} == False	SUITE:Enroll Device on Cloud
	${status}=	Run Keyword and Return Status	SUITE:Check That Devices::Enrolled Page Contain Online Devices
	Run Keyword If	${status} == False	CLI::ZPECloud::Restart agent process via root
	SUITE:Setup
	GUI::ZPECloud::Devices::Open Tab
	Wait Until Keyword Succeeds	120	4		SUITE:Online_Status
	SUITE:Check First Connection
	[Teardown]	SUITE:Teardown

Test device Last Connection
	[Setup]	SUITE:Setup
	SUITE:Check Last Connection
	[Teardown]	SUITE:Teardown

Test check connection dates
	[Tags]	NON-CRITICAL
	[Setup]	SUITE:Setup
	SUITE:Compare Connections Date

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud

SUITE:Teardown
	Close All Browsers

SUITE:Online_Status
	Reload Page
	Sleep	5
	Wait Until Page Contains	Online	15s

SUITE:Check First Connection
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Wait Until Keyword Succeeds	60	1	Click Element	xpath=//a/div
	Sleep	5
	Page Should Contain	Device Details
	Sleep	5
	${FIRSTCONNECTION}=	Get Text	xpath=//div[3]/p[7]/p/span
	Set Suite Variable	${FIRSTCONNECTION}	${FIRSTCONNECTION}

SUITE:Check Last Connection
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Wait Until Keyword Succeeds	60	1	Click Element	xpath=//a/div
	Sleep	5
	Page Should Contain	Device Details
	Sleep	5
	${LASTCONNECTION}=	Get Text	xpath=//p[8]/p/span
	Set Suite Variable	${LASTCONNECTION}	${LASTCONNECTION}

SUITE:Compare Connections Date
	${FIRSTCONNECTIONDSPLIT}	${FIRSTCONNECTIONTIME} =	Split String	${FIRSTCONNECTION}
	${LASTCONNECTIONSPLIT}	${LASTCONNECTIONTIME} =		Split String	${LASTCONNECTION}
	@{FIRSTCONNECTION}=	Split String	${FIRSTCONNECTIONDSPLIT}	${SEPARATOR}
	@{LASTCONNECTION}=	Split String	${LASTCONNECTIONSPLIT}	${SEPARATOR}
	${FIRSTCONNECTION}=	Catenate	@{FIRSTCONNECTION}	${FIRSTCONNECTIONTIME}
	${LASTCONNECTION}=	Catenate	@{LASTCONNECTION}	${LASTCONNECTIONTIME}
	${FIRSTCONNECTION}=	Convert Date	${FIRSTCONNECTION}	timestamp	yes	%m %d %Y %H:%M:%S
	${LASTCONNECTION}=	Convert Date	${LASTCONNECTION}	timestamp	yes	%m %d %Y %H:%M:%S
	${DATE}=	Subtract Date From Date		${LASTCONNECTION}	${FIRSTCONNECTION}
	Convert To Integer	${DATE}
#	Should Not Be Equal As Integers	${DATE}	0
	Should Not Be True	${DATE} < 0

SUITE:Verify if Device is on Cloud
	${status}=	Run Keyword and Return Status	SUITE:Verify if Device is Enrolled
	Run Keyword If	${status} == False	SUITE:Enroll Device on Cloud

SUITE:Enroll Device on Cloud
	SUITE:Verify if Device is Available
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Click Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Click Element	//*[@id="enroll"]
	Wait Until Keyword Succeeds	30	1	Wait Until Page Contains	Device Enrolled Successfully

SUITE:Verify if Device is Enrolled
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
#	Press Keys	search-input	ENTER
#	Sleep	5
	Wait until Page Contains	${SERIALNUMBER}	30s

SUITE:Verify if Device is Available
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Devices::Available::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
#	Press Keys	search-input	ENTER
#	Sleep	5
	Wait until Page Contains	${SERIALNUMBER}	30s

SUITE:Check That Devices::Enrolled Page Contain Online Devices
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Page Should Contain	Online