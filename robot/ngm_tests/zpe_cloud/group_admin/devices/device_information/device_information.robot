*** Settings ***
Documentation	Tests to check device information
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	DEVICE_INFORMATION
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_3

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${STATUS}	Online
${DEVICE}	nodegrid
${REMOTEACCESS}	remote_access

*** Test Cases ***
Test device details
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Security::Open Services tab
	GUI::Basic::Spinner Should Be Invisible
	${IS_NOTSELECTED}=	Run Keyword And return Status	Checkbox Should Not Be Selected	${REMOTEACCESS}
	Run Keyword If	${IS_NOTSELECTED} == True	Click Element	${REMOTEACCESS}
	Run Keyword If	${IS_NOTSELECTED} == True	GUI::Basic::Save
	${status}=	Run Keyword and Return Status	SUITE:Verify if Device is Enrolled
	Run Keyword If	${status} == False	SUITE:Enroll Device on Cloud
	${status}=	Run Keyword and Return Status	SUITE:Check That Devices::Enrolled Page Contain Online Devices
	Run Keyword If	${status} == False	CLI::ZPECloud::Restart agent process via root
	SUITE:Setup
	GUI::ZPECloud::Devices::Open Tab
	Wait Until Keyword Succeeds	60	4	SUITE:Online_Status
	SUITE:Check device details
	[Teardown]	SUITE:Teardown

Test device company
	[Tags]	NON-CRITICAL
	[Setup]	SUITE:Setup
	SUITE:Check device company

*** Keywords ***
SUITE:Setup
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud

SUITE:Teardown
	Close All Browsers
	CLI:Close Connection

SUITE:Online_Status
	Reload Page
	Sleep	5
	Wait Until Page Contains	Online	15s

SUITE:Check device details
	Wait Until Keyword Succeeds	3x	3s	CLI:Open	session_alias=host_session	HOST_DIFF=${HOST}
	${IS_VM}	CLI:Is VM System
	CLI:Close Connection
	SUITE:Access Web	${STATUS}
	Click Element	pwl
	Click Element	abt
	Wait Until Page Contains Element	model	15s
	${SERIAL_NUMBER}=	Get Text	serial_number
	${VERSION}=	Get Text	version
	${MODEL}=	Get Text	model
	IF	not ${IS_VM}
	${PART_NUMBER}=	Get Text	sku
	${BIOS_VERSION}=	Get Text	bios
	END
	${UPTIME}=	Get Text	uptime
#	${UPTIME}=	Fetch From Right	${UPTIME}	days,
#	${UPTIME}=	Fetch From Left	${UPTIME}	,
#	${UPTIME}=	Fetch From Left	${UPTIME}	hours
	${CPU}=	Get Text	cpu
	${REVISION_TAG}=	Get Text	revision_tag
	Click Button	jquery=div.modal button
	SUITE:Setup
	GUI::ZPECloud::Devices::Open Tab
	Wait Until Page Contains Element	xpath=//a/div	30s
	Click Element	xpath=//a/div
#	Sleep	5
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Device Details	30s
#	Sleep	5
	Wait Until Page Contains	${SERIAL_NUMBER}	30s
	${CLOUDUPTIME}=	Get Text	xpath=//div[3]/p[5]/p/span
	${CLOUDUPTIME_MIN}=	Fetch From Left	${CLOUDUPTIME}	minutes
	${CLOUDUPTIME_HOUR}=	Fetch From Left	${CLOUDUPTIME}	hours
	Wait Until Page Contains	${VERSION}	5s
	Wait Until Page Contains	${MODEL}
	Wait Until Page Contains	${PART_NUMBER}
	Wait Until Page Contains	${BIOS_VERSION}
#	${CLOUDUPTIME}=	Get Text	xpath=//div[3]/div/h5[5]/b
#	${CLOUDUPTIME_MIN}=	Fetch From Left	${CLOUDUPTIME}	minutes
#	${CLOUDUPTIME_HOUR}=	Fetch From Left	${CLOUDUPTIME}	hours
	Should Contain Any	${UPTIME}	${CLOUDUPTIME_HOUR}	${CLOUDUPTIME_MIN}
	${CLOUDCPU}=	Get Text	xpath=//p[4]/p/span
	Should Be Equal As Strings	${CPU}	${CLOUDCPU}
	Wait Until Page Contains	${REVISION_TAG}	30s

SUITE:Access Web
	[Arguments]	${STATUS}
	SUITE:Setup
	GUI::ZPECloud::Dashboard::Access::Open Tab
#	Sleep	15
	Wait Until Page Contains	${STATUS}	30s
	Wait Until Page Contains	${DEVICE}	30s
	GUI::ZPECloud::Basic::Click Button	web
	wait until keyword succeeds	30	5	GUI::Basic::Spinner Should Be Invisible
	Sleep	5
	Wait Until Keyword Succeeds	30	2	Switch Window	NEW
	Sleep	5
	Wait Until Keyword Succeeds	60	2	SUITE:Web Access
	Input Text	username	${USERNG}
	Input Text	password	${PWDNG}
	Click Element	login-btn
	wait until keyword succeeds	30	5	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	main_menu

SUITE:Web Access
	${LOGIN}=	Run Keyword And return Status	Page Should Contain Element	username
	Run Keyword If	${LOGIN} == False	Reload Page
	Sleep	5
	Wait Until Page Contains Element	username

SUITE:Check device company
	Click Element	xpath=//p[2]/div[2]
	Click Element	xpath=//li[contains(.,'Company Details')]
	${COMPANY}=	Get Text	xpath=//input[@name='business_name']
	Sleep	3
	Click Button	Close
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Wait Until Page Contains Element	xpath=//a/div	30s
	Click Element	xpath=//a/div
#	Sleep	5
	Wait Until Page Contains	Device Details	20s
	Sleep	5
	Element Should Contain	xpath=//div[3]/div/h5[4]/b	${COMPANY}

SUITE:Enroll Device on Cloud
	SUITE:Verify if Device is Available
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Click Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Click Element	//*[@id="enroll"]
	Wait Until Keyword Succeeds	30	1	Wait Until Page Contains	Device Enrolled Successfully

SUITE:Verify if Device is Enrolled
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
#	Press Keys	search-input	ENTER
#	Sleep	5
	Wait until Page Contains	${SERIALNUMBER}	30s

SUITE:Verify if Device is Available
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Devices::Available::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
#	Press Keys	search-input	ENTER
#	Sleep	5
	Wait until Page Contains	${SERIALNUMBER}	30s

SUITE:Check That Devices::Enrolled Page Contain Online Devices
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Wait Until Page Contains	Online	15s