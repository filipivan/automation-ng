*** Settings ***
Documentation	Tests for Checking Last Job status on device::enrolled page
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	ACCESS	WEB	LAST_JOB
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_2

*** Variables ***
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${JOB_ID}
${CHECK_PROFILE_OUTPUT}	ls /var/zpe-cloud/ztp/profiles_output

${Test_Script}	test_script
${Script_DESCRIPTION}	this script create a new file /tmp/test.txt with 'test file' written
${Script_MYPATH}	${CURDIR}/Last_profile_on_devices_file/test_script.txt
${DEVICE}	nodegrid
${NEWHOSTNAME}	test

${Failed_Script}	test_Failed_script
${Failed_Script_DESCRIPTION}	this script for testing Failed Script
${Failed_Script_MYPATH}	${CURDIR}/Last_profile_on_devices_file/test_Failed_script.txt

${Test_configuration}	test_configuration
${Test_Configuration_DESCRIPTION}	this code changes the hostname to be test
${Configuration_MYPATH}	${CURDIR}/Last_profile_on_devices_file/hostname.txt

${Failed_Configuration}	test_Failed_configuration
${Failed_Configuration_DESCRIPTION}	this code for testing an Failed Configuration
${Failed_Configuration_MYPATH}	${CURDIR}/Last_profile_on_devices_file/test_Failed_configuration.txt

${BACKUPSUCCESS}	Backup in the selected devices was requested
${BACKUPDELETED}	Backups are deleted successfully
${BACKUP}	Backup configuration
${FP_PWD}	zpesystems123

*** Test Cases ***
Apply script on device and check status under DEVICE::ENROLLED page
	[Tags]	NON-CRITICAL
	SUITE:Setup1
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	GUI::ZPECloud::Profiles::Open Tab
	Input Text	search-input    ${Test_Script}
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${Test_Script}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete	${Test_Script}
	GUI::ZPECloud::Profiles::Open Tab
	GUI::ZPECloud::Profiles::Script::Add	${Test_Script}	${Script_DESCRIPTION}	${Script_MYPATH}
	SUITE:Apply Script	${Test_Script}
	Sleep	15
	GUI::Basic::Spinner Should Be Invisible
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/div/div[2]/table/tbody/tr[1]/td[8]/span/div	${Test_Script}
	Wait Until Keyword Succeeds	5 minutes	2	SUITE:Status is 'Successful'
	CLI:Connect As Root	${HOST}	${PWDNG}
	${OUTPUT}=	CLI:Write	cat /tmp/test.txt
	Should Not Contain Any	${OUTPUT}	No such file or directory
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Select Checkbox	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Sleep	2s
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-2"]/div/div/div/table/tbody/tr/td[14]	30s
	Wait Until Element Contains	//*[@id="ca-devices-enrolled-table-2"]/div/div/div/table/tbody/tr/td[14]	${Test_Script}
	Wait Until Element Contains	//*[@id="ca-devices-enrolled-table-2"]/div/div/div/table/tbody/tr/td[15]	Successful
	SUITE:Teardown1	${Test_Script}

Apply script on device for getting Failed status and check status under DEVICE::ENROLLED page
	SUITE:Setup1
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	Input Text	search-input    ${Failed_Script}
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${Failed_Script}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete	${Failed_Script}
	GUI::ZPECloud::Profiles::Open Tab
	GUI::ZPECloud::Profiles::Script::Add	${Failed_Script}	${Failed_Script_DESCRIPTION}	${Failed_Script_MYPATH}
	SUITE:Apply Script	${Failed_Script}
	Sleep	15
	GUI::Basic::Spinner Should Be Invisible
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/div/div[2]/table/tbody/tr[1]/td[8]/span/div	${Failed_Script}
	Wait Until Keyword Succeeds	5 minutes	2	SUITE:Status is 'Failed'
	CLI:Connect As Root	${HOST}	${PWDNG}
	${OUTPUT}=	CLI:Write	cat /tmp/test.txt
	Should Contain Any	${OUTPUT}	No such file or directory
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Select Checkbox	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Sleep	2
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-2"]/div/div/div/table/tbody/tr/td[14]	30s
	Wait Until Element Contains	//*[@id="ca-devices-enrolled-table-2"]/div/div/div/table/tbody/tr/td[14]	${Failed_Script}
	Wait Until Element Contains	//*[@id="ca-devices-enrolled-table-2"]/div/div/div/table/tbody/tr/td[15]	Failed
	SUITE:Checking Profile Output
	SUITE:Teardown1	${Failed_Script}

Apply configuration On device and check status under DEVICE::ENROLLED page
	[Tags]	NON-CRITICAL
	SUITE:Setup2
	GUI::ZPECloud::Profiles::Open Tab
	Input Text	search-input    ${Test_configuration}
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${Test_configuration}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete	${Test_configuration}
	GUI::ZPECloud::Profiles::Open Tab
	GUI::ZPECloud::Profiles::Configuration::Add	${Test_configuration}	${Test_Configuration_DESCRIPTION}	${Configuration_MYPATH}
	SUITE:Apply configuration	${Test_configuration}
	Sleep	15
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/div/div[2]/table/tbody/tr[1]/td[8]/span/div	${Test_configuration}
	Wait Until Keyword Succeeds	5 minutes	2	SUITE:Status is 'Successful'
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Select Checkbox	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Sleep	2s
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-2"]/div/div/div/table/tbody/tr/td[14]	30s
	Wait Until Element Contains	//*[@id="ca-devices-enrolled-table-2"]/div/div/div/table/tbody/tr/td[14]	${Test_configuration}
	Wait Until Element Contains	//*[@id="ca-devices-enrolled-table-2"]/div/div/div/table/tbody/tr/td[15]	Successful
	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]
	Wait Until Keyword Succeeds	160	20s	SUITE:Hostname is 'Test'
	SUITE:Teardown2	${Test_configuration}

Apply configuration On device for getting Failed status and check status under DEVICE::ENROLLED page
	SUITE:Setup2
	GUI::ZPECloud::Profiles::Open Tab
	Input Text	search-input    ${Failed_configuration}
	${HASCONFIGURATION}=	Run Keyword and Return Status	Wait Until Page Contains	${Failed_Configuration}	15s
	Run Keyword if	${HASCONFIGURATION} == True	SUITE:Delete	${Failed_Configuration}
	GUI::ZPECloud::Profiles::Open Tab
	GUI::ZPECloud::Profiles::Configuration::Add	${Failed_Configuration}	${Failed_Configuration_DESCRIPTION}	${Failed_Configuration_MYPATH}
	SUITE:Apply configuration	${Failed_Configuration}
	Sleep	15
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Element Contains	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/div/div[2]/table/tbody/tr[1]/td[8]/span/div	${Failed_Configuration}
	Wait Until Keyword Succeeds	5 minutes	2	SUITE:Status is 'Failed'
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Select Checkbox	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Sleep	2
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-2"]/div/div/div/table/tbody/tr/td[14]	30s
	Wait Until Element Contains	//*[@id="ca-devices-enrolled-table-2"]/div/div/div/table/tbody/tr/td[14]	${Failed_Configuration}
	Wait Until Element Contains	//*[@id="ca-devices-enrolled-table-2"]/div/div/div/table/tbody/tr/td[15]	Failed
	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]
	Wait Until Keyword Succeeds	160	2	SUITE:Hostname is 'Nodegrid'
	SUITE:Checking Profile Output
	SUITE:Teardown2	${Failed_Configuration}

Take Successful Backup Of Device And Check Status Under DEVICE::ENROLLED page
	[Tags]	NON-CRITICAL
	SUITE:Setup3
	SUITE:Backup Device Configuration
	Click Element	//*[@id="opt-none"]
	Click Element	//*[@id="opt-temporary"]
	SUITE:Backup Device Configuration - Apply Configuration
	SUITE:Backup Device Configuration - Operation was Successful	Temporary
	SUITE:Delete Backup
	SUITE:Setup3
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Select Checkbox	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Sleep	2s
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-2"]/div/div/div/table/tbody/tr/td[14]	30s
	Wait Until Element Contains	//*[@id="ca-devices-enrolled-table-2"]/div/div/div/table/tbody/tr/td[14]	${BACKUP}
	Wait Until Element Contains	//*[@id="ca-devices-enrolled-table-2"]/div/div/div/table/tbody/tr/td[15]	Successful
	Close All Browsers

Take Failed Backup Of Device And Check Status Under DEVICE::ENROLLED page
	[Tags]	NON-CRITICAL
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Security::Open Services tab
	${CHECK}=	run keyword and return status	Checkbox Should Be Selected	pass_prot
	Run Keyword If	${CHECK} == True	Click Element	pass_prot
	Run Keyword If	${CHECK} == True	Click Element	saveButton
	SUITE:Setup3
	SUITE:Backup Device Configuration
	Click Element	//*[@id="opt-password-enc"]
	Click Element	//*[@id="opt-temporary"]
	SUITE:Backup Device Configuration - Apply Configuration
	SUITE:Backup Device Configuration - Operation was Failed
	SUITE:Setup3
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Select Checkbox	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Sleep	2s
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-2"]/div/div/div/table/tbody/tr/td[14]	30s
	Wait Until Element Contains	//*[@id="ca-devices-enrolled-table-2"]/div/div/div/table/tbody/tr/td[14]	${BACKUP}
	Wait Until Element Contains	//*[@id="ca-devices-enrolled-table-2"]/div/div/div/table/tbody/tr/td[15]	Failed
	Close All Browsers

*** Keywords ***
SUITE:Setup1
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Security::Open Services tab
	${CHECK1}=	run keyword and return status	Checkbox Should Be Selected	allow
	Run Keyword If	${CHECK1} == False	Select Checkbox	allow
	Run Keyword If	${CHECK1} == False	Click Element	saveButton

	${CHECK2}=	run keyword and return status	Checkbox Should Be Selected	pass_prot
	Run Keyword If	${CHECK2} == True	Unselect Checkbox	pass_prot
	Run Keyword If	${CHECK2} == True	Click Element	saveButton

	CLI:Connect As Root	${HOST}	${PWDNG}
	${OUTPUT}=	CLI:Write	cat /tmp/test.txt
	${STATUS}=	Run Keyword and Return Status	Should Contain Any	${OUTPUT}	No such file or directory
	Run Keyword If	${STATUS} == False	SUITE:Delete File

SUITE:Setup2
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Security::Open Services tab
	${CHECK}=	run keyword and return status	Checkbox Should Be Selected	pass_prot
	Run Keyword If	${CHECK} == True	Unselect Checkbox	pass_prot
	Run Keyword If	${CHECK} == True	Click Element	saveButton

	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Setup3
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Teardown1
	[Arguments]	${Delete_Script}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	Sleep	5
	Input Text	search-input	${Delete_Script}
	Sleep	2
	${ELEMENT}=	Run Keyword And return Status	Page Should Contain Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Run Keyword If	${ELEMENT} == True	SUITE:Delete	${Delete_Script}
	Close All Browsers

SUITE:Teardown2
	[Arguments]	${Delete_Configuation}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Open Tab
	Sleep	5
	Input Text	search-input	${Delete_Configuation}
	sleep	2s
	${ELEMENT}=	Run Keyword And return Status	Page Should Contain Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Run Keyword If	${ELEMENT} == True	SUITE:Delete	${Delete_Configuation}
	Wait Until Keyword Succeeds	30	2	Set hostname as 'nodegrid'
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::DASHBOARD::ACCESS::Open Tab
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]
	Wait Until Keyword Succeeds	160	2	SUITE:Hostname is 'Nodegrid'
	Close All Browsers

SUITE:Apply configuration
	[Arguments]	${Test_Configuration}
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr[1]/td[1]/span/input	20s
	Select Checkbox	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
	Sleep	2
	Click Element	function-button-0
	Sleep	5
	Wait Until Page Contains Element	ca-profiles-config-table-1
	Input Text	//*[@id="ca-profiles-config-search"]/div/div/input	${Test_Configuration}
	Sleep	5
	Select Checkbox	xpath=//div[@id='ca-profiles-config-table-1']/div/div/table/tbody/tr/td[1]/span/input
	Click Element	xpath=(//button[@id='function-button-1'])[2]
	Wait Until Page Contains	Apply configuration on the selected devices was requested

SUITE:Apply Script
	[Arguments]	${Test_Script}
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	20s
	Select Checkbox	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Sleep	2
	Click Element	function-button-0
	Sleep	5
	Wait Until Page Contains Element	ca-profiles-config-table-1
	Input Text	//*[@id="ca-profiles-config-search"]/div/div/input	${Test_Script}
	Sleep	5
	Select Checkbox	xpath=//div[@id='ca-profiles-config-table-1']/div/div/table/tbody/tr/td[1]/span/input
	Click Element	xpath=(//button[@id='function-button-1'])[2]
	Wait Until Page Contains	Apply configuration on the selected devices was requested

SUITE:Delete
	[Arguments]	${NAME}
	Wait Until Page Contains Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	15s
	Click Element	//*[@id="ca-profiles-config-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Wait Until Element Is Enabled	function-button-5	10s
	Click Element	function-button-5
	Sleep	2
	Page Should Contain Element	//*[@id="ca-profiles-config-delete-dialog"]/div[3]/div
	Click Element	//*[@id="confirm-btn"]
	Wait Until Page Does Not Contain Element	//*[@id="ca-profiles-config-delete-dialog"]	15s

SUITE:Hostname is 'Nodegrid'
	Reload Page
	Sleep	5
	${CONTAINTEST}=	Run Keyword And return Status	Page Should Contain	${DEVICE}
	Run Keyword If	${CONTAINTEST} == False	Reload Page
	Sleep	10
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	${DEVICE}

SUITE:Hostname is 'Test'
	Reload Page
	Sleep	10
	${CONTAINTEST}=	Run Keyword And return Status	Page Should Contain	${NEWHOSTNAME}
	Run Keyword If	${CONTAINTEST} == False	Reload Page
	Sleep	5
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	${NEWHOSTNAME}

SUITE:Status is 'Failed'
	Sleep	10
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/div/div[2]/table/tbody/tr[1]/td[9]/span/div
	${CONTAINTEST}=	Run Keyword And return Status	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/div/div[2]/table/tbody/tr[1]/td[9]/span/div	Failed
	Run Keyword If	${CONTAINTEST} == False	Reload Page
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/div/div[2]/table/tbody/tr[1]/td[9]/span/div
	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/div/div[2]/table/tbody/tr[1]/td[9]/span/div	Failed

SUITE:Delete File
	CLI:Connect As Root	${HOST}	${PWDNG}
	${OUTPUT}=	CLI:Write	rm /tmp/test.txt
	Sleep	3
	${OUTPUT}=	CLI:Write	cat /tmp/test.txt
	Should Contain Any	${OUTPUT}	No such file or directory
	CLI:Close Connection	Yes

Set hostname as 'nodegrid'
	CLI:Open	${USERNAMENG}	${PASSWORDNG}
	CLI:Enter Path	/settings/network_settings/
	CLI:Set Field	hostname	${DEVICE}
	CLI:Commit
	CLI:Close Connection

SUITE:Backup Device Configuration
	SUITE:Setup3
	GUI::ZPECloud::Devices::Open Tab
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	15s
	Click Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Wait Until Element Is Enabled	//*[@id="function-button-2"]
	Click Element	//*[@id="function-button-2"]
	Wait Until Page Contains Element	//*[@id="file-protection-radio"]

SUITE:Backup Device Configuration - Apply Configuration
	Wait Until Element is Enabled	submit-btn
	Click Element	submit-btn
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	${BACKUPSUCCESS}

SUITE:Enable File Protection
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Basic::Security::services::Open Tab
	Wait Until Page Contains Element	//*[@id="pass_prot"]
	Click Element	//*[@id="pass_prot"]
	Wait Until Page Contains Element	//*[@id="file_pass_conf"]
	Input Text	//*[@id="file_pass"]	${FP_PWD}
	Input Text	//*[@id="file_pass_conf"]	${FP_PWD}
	Click Element	//*[@id="saveButton"]

SUITE:Status is 'Successful'
	Sleep	10
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/div/div[2]/table/tbody/tr[1]/td[9]/span/div
	${CONTAINTEST}=	Run Keyword And return Status	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/div/div[2]/table/tbody/tr[1]/td[9]/span/div	Successful
	Run Keyword If	${CONTAINTEST} == False 	Reload Page
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/div/div[2]/table/tbody/tr[1]/td[9]/span/div
	Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/div/div[2]/table/tbody/tr[1]/td[9]/span/div	Successful

SUITE:Delete Backup
	SUITE:Setup3
	GUI::ZPECloud::Profiles::Backup::Open Tab
	Wait Until Page Contains Element	//*[@id="ca-profiles-bkp-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="ca-profiles-bkp-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	GUI::ZPECloud::Basic::Click Button	delete
	Wait Until Page Contains Element	//*[@id="confirm-btn"]	15s
	Click Element	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	30	2	Wait Until Page Contains	${BACKUPDELETED}
	Close All Browsers

SUITE:Backup Device Configuration - Operation was Successful
	[Arguments]	${FileStorage}
	Sleep    2 minutes
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/div/div[2]/table/tbody/tr[1]/td[6]/span/div
	Wait Until Keyword Succeeds	2 minutes	2    Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/div/div[2]/table/tbody/tr[1]/td[6]/span/div	BACKUP
	Wait Until Keyword Succeeds	2 min	2	SUITE:Status is 'Successful'

SUITE:Backup Device Configuration - Operation was Failed
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Page Contains Element    //*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/div/div[2]/table/tbody/tr[1]/td[6]/span/div
	Wait Until Keyword Succeeds	7 minutes	2    Element Should Contain	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/div/div[2]/table/tbody/tr[1]/td[6]/span/div	BACKUP
	Wait Until Keyword Succeeds	7 min	2	SUITE:Status is 'Failed'

SUITE:Checking Profile Output
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Page Contains Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/div/div[2]/table/tbody/tr[1]/td[2]/span/a
	${JOB_ID}=	Get Text	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/div/div[2]/table/tbody/tr[1]/td[2]/span/a
	Click Element	//*[@id="ca-profiles-operations-tab-jobs-table-1"]/div[1]/div/div/div[2]/table/tbody/tr[1]/td[2]
	sleep	5s
	Wait Until Page Contains Element	//*[@id="profile-operations-details-wrapper"]/div/div[2]/div/p[1]/p/span	30s
	Wait Until Page Contains	${JOB_ID}
	CLI:Connect As Root	${HOST}	${PWDNG}
	${OUTPUT}=	CLI:Write	${CHECK_PROFILE_OUTPUT}
	Sleep	3
	Should Contain Any	${OUTPUT}	${JOB_ID}