*** Settings ***
Documentation	Tests add license to a company
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	COMPANY_LICENSE
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_3

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
${License_Subscription}	ZPE-CLOUD-SUB1Y-025 - ZPE Cloud - 1 YEAR - Subscription - 25 Nodes
${ADD_LICENSE_SUCCESS}	Subscription Added Successfully

*** Test Cases ***
Activate License - Immediately
	SUITE:Add license
	SUITE:Setup
	GUI::ZPECloud::Settings::Subscriptions::Open Tab
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-license-settings-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	15s
	Click Element	//*[@id="ca-license-settings-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Sleep	5
	Wait Until Page Contains Element	//*[@id="activate"]	15s
	Click Element	//*[@id="activate"]
	Wait Until Page Contains Element	xpath=//span[contains(.,'Immediately')]	15s
	Click Element	xpath=//span[contains(.,'Immediately')]
	Wait Until Page Contains Element	xpath=//form/div/div/div/button[2]	15s
	Click Element	xpath=//form/div/div/div/button[2]
#	Sleep	2
	Wait Until Page Contains	Subscription activated successfully	15s
	sleep	5s
	GUI::ZPECloud::Settings::Subscriptions::Open Tab
	Wait Until Page Contains		Active	15s
	[Teardown]	SUITE:Teardown

Activate License - Schedule
	SUITE:Add license
	SUITE:Setup
	GUI::ZPECloud::Settings::Subscriptions::Open Tab
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-license-settings-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	15s
	Click Element	//*[@id="ca-license-settings-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Sleep	5
	Wait Until Page Contains Element	//*[@id="activate"]	15s
	Click Element	//*[@id="activate"]
	Wait Until Page Contains Element	xpath=//span[contains(.,'Schedule')]	15s
	Click Element	xpath=//span[contains(.,'Schedule')]
	Sleep	2
	${Currentdate}=	Get Current Date	result_format=%Y-%m-%d %H:%M:%S
	${datetime}=	Add Time To Date	${CurrentDate}	1 days
	${TommorrowDate} =	Convert Date	${datetime}	result_format=%Y-%m-%d %H:%M:%S
	Click Element	schedule-datepicker
	Input Text	schedule-datepicker	${TommorrowDate}
	Sleep	5
	Wait Until Page Contains Element	xpath=//form/div/div/div/button[2]	15s
	Click Element	xpath=//form/div/div/div/button[2]
#	Sleep	2
	Wait Until Page Contains	Subscription scheduled successfully	15s
	[Teardown]	SUITE:Teardown

Activate License - Auto
	SUITE:Add license
	SUITE:Setup
	GUI::ZPECloud::Settings::Subscriptions::Open Tab
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-license-settings-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	15s
	Click Element	//*[@id="ca-license-settings-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Sleep	5
	Wait Until Page Contains Element	//*[@id="activate"]	15s
	Click Element	//*[@id="activate"]
	Wait Until Page Contains Element	xpath=//span[contains(.,'Auto')]	15s
	Click Element	xpath=//span[contains(.,'Auto')]
	Wait Until Page Contains Element	xpath=//form/div/div/div/button[2]	15s
	Click Element	xpath=//form/div/div/div/button[2]
#	Sleep	2
	Wait Until Page Contains	Subscription will auto activate	15s
	[Teardown]	SUITE:Teardown

Activate License - Inactive
	SUITE:Add license
	SUITE:Setup
	GUI::ZPECloud::Settings::Subscriptions::Open Tab
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-license-settings-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	15s
	Click Element	//*[@id="ca-license-settings-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Sleep	5
	Wait Until Page Contains Element	//*[@id="activate"]	15s
	Click Element		//*[@id="activate"]
	Wait Until Page Contains Element	xpath=//span[contains(.,'Inactive')]	15s
	Click Element	xpath=//span[contains(.,'Inactive')]
	Wait Until Page Contains Element	xpath=//form/div/div/div/button[2]	15s
	Click Element	xpath=//form/div/div/div/button[2]
#	Sleep	2
	Wait Until Page Contains	Subscription deactivated	15s

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open And Login	${LICENSE_ENFORCEMENT_USER_II}	${LICENSE_ENFORCEMENT_PASSWORD_II}

SUITE:Teardown
	Delete License
	Close All Browsers

SUITE:Add license
	GUI::ZPECloud::Basic::Open And Login	${SUPER_EMAIL_ADDRESS}	${SUPER_PASSWORD}
	GUI::ZPECloud::Companies::Subscriptions::Open Tab
	Sleep	12
	Wait Until Page Contains Element	xpath=//div[2]/div/button/span	20s
	Click Element	xpath=//div[2]/div/button/span
	Sleep	12
	Wait Until Page Contains Element	//*[@id="mui-component-select-license"]	20s
#	Input Text	//*[@id="sa-companies-add-licence-dialog"]/div[2]/div/div/div[1]/div/div/div/div[1]/div/input	${LICENSE_ENFORCEMENT_COMPANY_II}
#	Click Element	xpath=//*[contains(., "${LICENSE_ENFORCEMENT_COMPANY_II}")]
	Click Element	//*[@id="mui-component-select-license"]
	Execute Javascript	Array.from(document.getElementById('menu-license').children[2].getElementsByTagName('LI')).filter(li => li.textContent == '${License_Subscription}')[0].click()
	Click Element	xpath=//*[contains(., "${License_Subscription}")]
#	Click Element	menu-license
#	sleep	5s
	Wait Until Page Contains Element	xpath=//form[@id='sa-companies-add-licence-dialog']/div[2]/div/div/div/div/div/div/div/input	15s
	Input Text	xpath=//form[@id='sa-companies-add-licence-dialog']/div[2]/div/div/div/div/div/div/div/input	${LICENSE_ENFORCEMENT_COMPANY_II}
	sleep	5s
#	${COMPANYLOCATION}=	Execute Javascript	return Array.from(document.getElementById('react-autowhatever-0').children[0].getElementsByTagName('LI')).filter(li => li.textContent == '${LICENSE_ENFORCEMENT_COMPANY_II}')[0].id
#	sleep	5s
#	Click Element	${COMPANYLOCATION}
	Click Element	xpath=//li[contains(.,'${LICENSE_ENFORCEMENT_COMPANY_II}')]
#	Sleep	5s
#	Click Element	css=strong:nth-child(4)
#	Click Element	xpath=//*[contains(., "${LICENSE_ENFORCEMENT_COMPANY_II}")]
	Wait Until Page Contains Element	submit-btn	20s
	Click Element	submit-btn
	Sleep	15
	Wait Until Page Contains	Email Text
#	Wait Until Page Contains Element	//*[@id="cc-input"]
#	Clear Element Text	//*[@id="cc-input"]
#	Press Keys	//*[@id="cc-input"]	CTRL+a+BACKSPACE
	sleep	5s
	Wait Until Page Contains Element	//*[@id="function-button-1"]	15s
	Wait Until Element Is Visible	//*[@id="function-button-1"]	15s
	Wait Until Keyword Succeeds	30	6	SUITE:SAVING LICENSE
	Close All Browsers

SUITE:SAVING LICENSE
	Click Element	//*[@id="function-button-1"]
	Wait Until Page Contains	${ADD_LICENSE_SUCCESS}	15s


Delete License
	GUI::ZPECloud::Basic::Open And Login	${SUPER_EMAIL_ADDRESS}	${SUPER_PASSWORD}
	GUI::ZPECloud::Companies::Subscriptions::Open Tab
	Wait Until Page Contains Element	//*[@id="search-input"]	15s
	Input Text	//*[@id="search-input"]	${LICENSE_ENFORCEMENT_COMPANY_II}
#	Sleep	10
	Wait Until Page Contains Element	//*[@id="sa-companies-licence-table-1"]/div[1]/div/table/tbody/tr[1]/td[1]/span/input	30s
	Click Element	//*[@id="sa-companies-licence-table-1"]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
#	Sleep	10
	Wait Until Page Contains Element	xpath=//button[contains(.,'Delete')]	15s
	Click Element	xpath=//button[contains(.,'Delete')]
	Wait Until Page Contains Element	xpath=//button[@id='confirm-btn']	15s
#	Sleep	10
	Click Element	xpath=//button[@id='confirm-btn']
	Wait Until Page Contains	Subscription Deleted Successfully	15s