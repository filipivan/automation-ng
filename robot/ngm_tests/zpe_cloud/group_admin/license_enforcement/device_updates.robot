*** Settings ***
Documentation	Tests for device update in 'Available' tab
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	DEVICE_CONFIGURATION	ENROLL
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_3

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${DEVICE}	nodegrid
${STATUS}	Online
${NEWHOSTNAME}	test
${ENROLLED}	Device Enrolled Successfully

*** Test Cases ***
Change hostname of device - Check that old hostname remain on Available tab
	GUI::ZPECloud::Move Device to Available
	SUITE:Set hostname as 'test'
#	SUITE:Check devices hostname on Available tab
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Available::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SERIALNUMBER}
#	Press Keys	search-input	ENTER
#	Sleep	5
	Wait Until Page Contains	${SERIALNUMBER}	30s
	Wait Until Keyword Succeeds	160	2	SUITE:Hostname is 'Nodegrid'
	SUITE:Check devices hostname on Available tab
	[Teardown]	Close All Browsers

Enroll device and check new hostname
	SUITE:Enroll device from Available tab
#	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Dashboard::Access::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SERIALNUMBER}
#	Press Keys	search-input	ENTER
#	Sleep	5
	Wait Until Page Contains	${SERIALNUMBER}	30s
	Wait Until Keyword Succeeds	160	2	SUITE:Hostname is 'Test'
	[Teardown]	Close All Browsers

*** Keywords ***
SUITE:Setup
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud

SUITE:Teardown
	SUITE:Set hostname as 'nodegrid'
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Dashboard::Access::Open Tab
	Wait Until Keyword Succeeds	2m	2s	SUITE:Hostname is 'Nodegrid'
	SUITE:Setup
	Close All Browsers

SUITE:Online_Status
	Reload Page
	Sleep	5
	Wait Until Page Contains	Online	10s

SUITE:Verify if Device is on Cloud
	${status}=	Run Keyword and Return Status	SUITE:Verify if Device is Enrolled
	Run Keyword If	${status} == False	SUITE:Verify if Device is Available

SUITE:Verify if Device is Available
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Devices::Available::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait until Page Contains	${SERIALNUMBER}	30s

SUITE:Get Device Enrolment Info on QA Instance
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Settings::Open Tab
	${CUSTOMERCODE}=	Get Value	ca-setgen-customer-code-input
	Click Element	//*[@id="ca-setgen-password-toggle-btn"]/span[1]
	${ENROLLMENTKEY}=	Get Value	filled-adornment-password
	${CUSTOMERCODE}=	Set Suite Variable	${CUSTOMERCODE}
	${ENROLLMENTKEY}=	Set Suite Variable	${ENROLLMENTKEY}
	Close All Browsers
	[return]	${CUSTOMERCODE}	${ENROLLMENTKEY}

SUITE:Hostname is 'Nodegrid'
	Reload Page
	Sleep	5
	${CONTAINTEST}=	Run Keyword And return Status	Page Should Contain	${DEVICE}
	Run Keyword If	${CONTAINTEST} == False	Reload Page
	Sleep	10
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	${DEVICE}

SUITE:Hostname is 'Test'
#	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
#	GUI::ZPECloud::Dashboard::Access::Open Tab
	Reload Page
	Sleep	10
	${CONTAINTEST}=	Run Keyword And return Status	Page Should Contain	${NEWHOSTNAME}
	Run Keyword If	${CONTAINTEST} == False	Reload Page
	Sleep	5
	Wait Until Keyword Succeeds	10	1	Wait Until Page Contains	${NEWHOSTNAME}

SUITE:Set hostname as 'test'
	CLI:Open	${USERNAMENG}	${PASSWORDNG}
	CLI:Enter Path	/settings/network_settings/
	CLI:Set Field	hostname	${NEWHOSTNAME}
	CLI:Commit
	CLI:Close Connection

SUITE:Set hostname as 'nodegrid'
	CLI:Open	${USERNAMENG}	${PASSWORDNG}
	CLI:Enter Path	/settings/network_settings/
	CLI:Set Field	hostname	${DEVICE}
	CLI:Commit
	CLI:Close Connection

SUITE:Check devices hostname on Available tab
#	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Available::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SERIALNUMBER}
#	Press Keys	search-input	ENTER
#	Sleep	5
	Wait Until Page Contains	${SERIALNUMBER}	30s
	${HOSTNAME}=	Get Text	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr[1]/td[2]/a/div
	Should Be Equal As Strings	${HOSTNAME}	${DEVICE}

SUITE:Enroll device from Available tab
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Available::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SERIALNUMBER}
#	Press Keys	search-input	ENTER
#	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Wait Until Page Contains	${SERIALNUMBER}	30s
#	${DEVICELOCATION}=	Execute Javascript	return Array.from(document.getElementById('ca-devices-available-table-1').getElementsByTagName('TBODY')[0].getElementsByTagName('TR')).indexOf(Array.from(document.getElementById('ca-devices-available-table-1').getElementsByTagName('TBODY')[0].getElementsByTagName('TR')).filter(e => e.getElementsByTagName('TD')[2].textContent == '${SERIALNUMBER}')[0])
#	Convert To Integer	${DEVICELOCATION}
#	Run Keyword If	${DEVICELOCATION} == 0	Click Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[1]
#	Run Keyword If	${DEVICELOCATION} > 0	Click Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr[${DEVICELOCATION}]/td[1]
	Click Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Wait Until Page Contains Element	//*[@id="enroll"]	15s
	Click Element	//*[@id="enroll"]
	Wait Until Keyword Succeeds	30	1	Wait Until Page Contains	Device Enrolled Successfully