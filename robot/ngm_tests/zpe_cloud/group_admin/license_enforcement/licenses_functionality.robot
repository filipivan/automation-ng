*** Settings ***
Documentation	Tests license functionality
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	COMPANY LICENSE	LICENSE_FUNCTIONALITY
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_3

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${TRANSFERTOTHESAMECOMPANYSUCCESS}	Successfully transferred device ownership.
${License_Subscription}	ZPE-CLOUD-SUB1Y-025 - ZPE Cloud - 1 YEAR - Subscription - 25 Nodes
#${TRIAL}	ZPE-CLOUD-TRIAL-10D - ZPE Cloud - 10 days - Trial${SPACE}${SPACE}- Unlimited Nodes
${ENROLLED}	Device Enrolled Successfully
${ADD_LICENSE_SUCCESS}	Subscription Added Successfully

*** Test Cases ***
Company without any licenses tests - device enrolled going from enrolled to available
	SUITE: Add and activate license
	SUITE:Associate to License Enforcement Company
	SUITE:Check Device Under License Enforcement Company
	SUITE:Delete License
	SUITE:Device should go to 'Available'
	[Teardown]	SUITE:Teardown

Automate - activate company's auto licenses tests when enrolling device
	SUITE:Add Auto License
	SUITE:Associate to License Enforcement Company
	SUITE:Check Device Under License Enforcement Company
	Enroll device from Available tab
	SUITE:Delete License
	SUITE:Device back to company

*** Keywords ***
SUITE:Setup
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	GUI::ZPECloud::Dashboard::Access::Open Tab

SUITE:Teardown
	SUITE:Setup
	Close All Browsers

SUITE:Associate to License Enforcement Company
	GUI::Basic::Open And Login Nodegrid	${USERNAMENG}	${PASSWORDNG}
	GUI::Open System tab
	Sleep	5
	Click Element	xpath=(//a[contains(text(),'Toolkit')])[2]
	Wait Until Page Contains Element	icon_cloudEnrollment	15s
	Click Element	icon_cloudEnrollment
	Wait Until Page Contains	Cloud Enrollment	30s
	Sleep	5
	Clear Element Text	url
	Press Keys	url	CTRL+a+BACKSPACE
	Input Text	url	${ZPECLOUD_HOMEPAGE}
	Input Text	customer_code	${LICENSE_ENFORCEMENT_CUSTOMERCODE_II}
	Input Text	enrollment_key	${LICENSE_ENFORCEMENT_ENROLLMENTKEY_II}
	Click Element	saveButton
	Wait Until Keyword Succeeds	30	30		Wait Until Page Contains	${TRANSFERTOTHESAMECOMPANYSUCCESS}
	Close All Browsers

SUITE:Check Device Under License Enforcement Company
	GUI::ZPECloud::Basic::Open And Login	${LICENSE_ENFORCEMENT_USER_II}	${LICENSE_ENFORCEMENT_PASSWORD_II}
	GUI::ZPECloud::Devices::Available::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	//*[@id="search-input"]		${SERIALNUMBER}
	Wait Until Page Contains	${SERIALNUMBER}	30s
	Element Should Contain		//*[@id="sa-devices-general-table-1"]/div[1]/div/table/tbody/tr/td[2]	${SERIALNUMBER}

SUITE:Delete License
	GUI::ZPECloud::Basic::Open And Login	${SUPER_EMAIL_ADDRESS}	${SUPER_PASSWORD}
	GUI::ZPECloud::Companies::Subscriptions::Open Tab
	Input Text	//*[@id="search-input"]	${LICENSE_ENFORCEMENT_COMPANY_II}
	Sleep	10
	Click Element	//*[@id="sa-companies-licence-table-1"]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
#	Sleep	10
	Wait Until Page Contains Element	xpath=//button[contains(.,'Delete')]	15s
	Click Element	xpath=//button[contains(.,'Delete')]
	Wait Until Page Contains Element	xpath=//button[@id='confirm-btn']	15s
#	Sleep	10
	Click Element	xpath=//button[@id='confirm-btn']
	Wait Until Page Contains	Subscription Deleted Successfully
	Close All Browsers

SUITE:Device should go to 'Available'
	GUI::ZPECloud::Basic::Open And Login	${LICENSE_ENFORCEMENT_USER_II}	${LICENSE_ENFORCEMENT_PASSWORD_II}
	GUI::ZPECloud::Devices::Available::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	//*[@id="search-input"]	${SERIALNUMBER}
	Wait Until Page Contains	${SERIALNUMBER}	30s
	Close All Browsers

SUITE: Add and activate license
	SUITE:Add license
	GUI::ZPECloud::Basic::Open And Login	${LICENSE_ENFORCEMENT_USER_II}	${LICENSE_ENFORCEMENT_PASSWORD_II}
	GUI::ZPECloud::Settings::Subscriptions::Open Tab
	Sleep	5
	Click Element	//*[@id="ca-license-settings-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Sleep	5
	Click Element	//*[@id="activate"]
	Wait Until Page Contains Element	xpath=//span[contains(.,'Immediately')]	15s
	Click Element	xpath=//span[contains(.,'Immediately')]
	Wait Until Page Contains Element	xpath=//form/div/div[2]/div/button[2]	15s
	Click Element	xpath=//form/div/div[2]/div/button[2]
	Sleep	5
	GUI::ZPECloud::Settings::Subscriptions::Open Tab
	Sleep	5
	Page Should Contain	Active
	Close All Browsers

SUITE:Add license
	GUI::ZPECloud::Basic::Open And Login	${SUPER_EMAIL_ADDRESS}	${SUPER_PASSWORD}
	GUI::ZPECloud::Companies::Subscriptions::Open Tab
	Sleep	12
	Wait Until Page Contains Element	xpath=//div[2]/div/button/span	20s
	Click Element	xpath=//div[2]/div/button/span
	Sleep	12
	Wait Until Page Contains Element	//*[@id="mui-component-select-license"]	20s
#	Input Text	//*[@id="sa-companies-add-licence-dialog"]/div[2]/div/div/div[1]/div/div/div/div[1]/div/input	${LICENSE_ENFORCEMENT_COMPANY_II}
#	Click Element	xpath=//*[contains(., "${LICENSE_ENFORCEMENT_COMPANY_II}")]
	Click Element	//*[@id="mui-component-select-license"]
	Execute Javascript	Array.from(document.getElementById('menu-license').children[2].getElementsByTagName('LI')).filter(li => li.textContent == '${License_Subscription}')[0].click()
	Click Element	xpath=//*[contains(., "${License_Subscription}")]
#	Click Element	menu-license
#	sleep	5s
	Wait Until Page Contains Element	//*[@id="sa-companies-add-licence-dialog"]/div[2]/div/div/div[1]/div/div/div/div[1]	15s
	Input Text	//*[@id="sa-companies-add-licence-dialog"]/div[2]/div/div/div[1]/div/div/div/div[1]	${LICENSE_ENFORCEMENT_COMPANY_II}
	sleep	5s
	Click Element	xpath=//li[contains(.,'${LICENSE_ENFORCEMENT_COMPANY_II}')]
#	${COMPANYLOCATION}=	Execute Javascript	return Array.from(document.getElementById('react-autowhatever-1').children[0].getElementsByTagName('LI')).filter(li => li.textContent == '${LICENSE_ENFORCEMENT_COMPANY_II}')[0].id
#	Click Element	${COMPANYLOCATION}
#	Sleep	5s
#	Click Element	css=strong:nth-child(4)
#	Click Element	xpath=//*[contains(., "${LICENSE_ENFORCEMENT_COMPANY_II}")]
	Wait Until Page Contains Element	submit-btn	20s
	Click Element	submit-btn
	Sleep	15
	Wait Until Page Contains	Email Text
#	Wait Until Page Contains Element	//*[@id="cc-input"]
#	Clear Element Text	//*[@id="cc-input"]
#	Press Keys	//*[@id="cc-input"]	CTRL+a+BACKSPACE
	sleep	5s
	Wait Until Page Contains Element	//*[@id="function-button-1"]	15s
	Wait Until Element Is Visible	//*[@id="function-button-1"]	15s
	Wait Until Keyword Succeeds	30	6	SUITE:SAVING LICENSE
	Close All Browsers

SUITE:SAVING LICENSE
	Click Element	//*[@id="function-button-1"]
	Wait Until Page Contains	${ADD_LICENSE_SUCCESS}	15s

SUITE:Add Auto License
	SUITE:Add license
	GUI::ZPECloud::Basic::Open And Login	${LICENSE_ENFORCEMENT_USER_II}	${LICENSE_ENFORCEMENT_PASSWORD_II}
	GUI::ZPECloud::Settings::Subscriptions::Open Tab
	Sleep	10
	Click Element	//*[@id="ca-license-settings-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Click Element		//*[@id="activate"]
	Wait Until Page Contains Element	xpath=//span[contains(.,'Auto')]	15s
	Click Element	xpath=//span[contains(.,'Auto')]
	Wait Until Page Contains Element	xpath=//form/div/div/div/button[2]	15s
	Click Element	xpath=//form/div/div/div/button[2]
#	Sleep	2
	Wait Until Page Contains	Subscription will auto activate	15s

Enroll device from Available tab
	GUI::ZPECloud::Basic::Open And Login	${LICENSE_ENFORCEMENT_USER_II}	${LICENSE_ENFORCEMENT_PASSWORD_II}
	GUI::ZPECloud::Devices::Available::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	//*[@id="search-input"]	${SERIALNUMBER}
	Wait Until Page Contains	${SERIALNUMBER}	30s
	Click Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Sleep	10
	GUI::ZPECloud::Basic::Click Button	enroll
	Wait Until Page Contains	${ENROLLED}	15s