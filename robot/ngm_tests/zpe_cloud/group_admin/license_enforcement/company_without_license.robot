*** Settings ***
Documentation	Tests for company without license
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	COMPANY	LICENSE
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_3

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${BANNER}	No subscription active. Devices won't be able to enroll
${TRANSFERTOTHESAMECOMPANYSUCCESS}	Successfully transferred device ownership.
${FAILENROLLED}		No available subscriptions for devices, please move some to available or activate a new subscription.
*** Test Cases ***
Check 'No subscription' banner
	Wait Until Page Contains	${BANNER}
	[Teardown]	Close All Browsers

Company without any licenses - device is not able to enroll
	SUITE:Associate to License Enforcement Company
	SUITE:Check Device Under License Enforcement Company
	SUITE:Enroll device from Available tab - Fails
	[Teardown]	SUITE:Device back to company

*** Keywords ***
SUITE:Setup
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	GUI::ZPECloud::Basic::Open And Login	${LICENSE_ENFORCEMENT_USER}	${LICENSE_ENFORCEMENT_PASSWORD}

SUITE:Teardown
	Close All Browsers

SUITE:Associate to License Enforcement Company
	GUI::Basic::Open And Login Nodegrid	${USERNAMENG}	${PASSWORDNG}
	GUI::Open System tab
	Sleep	5
	Click Element	xpath=(//a[contains(text(),'Toolkit')])[2]
	Wait Until Page Contains Element	icon_cloudEnrollment
	Click Element	icon_cloudEnrollment
	Wait Until Page Contains	Cloud Enrollment	20s
	Sleep	5
	Clear Element Text	url
	Press Keys	url	CTRL+a+BACKSPACE
	Input Text	url	${ZPECLOUD_HOMEPAGE}
	Input Text	customer_code	${LICENSE_ENFORCEMENT_CUSTOMERCODE}
	Input Text	enrollment_key	${LICENSE_ENFORCEMENT_ENROLLMENTKEY}
	Click Element	saveButton
	Wait Until Keyword Succeeds	30	30		Wait Until Page Contains	${TRANSFERTOTHESAMECOMPANYSUCCESS}
	Close All Browsers

SUITE:Check Device Under License Enforcement Company
	SUITE:Setup
	GUI::ZPECloud::Devices::Available::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${SERIALNUMBER}
#	Press Keys	search-input	ENTER
	Wait Until Page Contains	${SERIALNUMBER}	30s
	Element Should Contain	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr[1]/td[3]	${SERIALNUMBER}

SUITE:Enroll device from Available tab - Fails
#	GUI::ZPECloud::Devices::Available::Open Tab
#	Wait Until Page Contains Element	search-input	15s
#	Input Text	search-input	${SERIALNUMBER}
#	Press Keys	search-input	ENTER
	Wait Until Page Contains	${SERIALNUMBER}	30s
	Click Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Wait Until Page Contains Element	//*[@id="enroll"]	15s
	Click Element	//*[@id="enroll"]
	Wait Until Keyword Succeeds	30	1	Wait Until Page Contains	${FAILENROLLED}
	Close All Browsers

SUITE:Device back to company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Settings::Open Tab
	${CUSTOMERCODE}=	Get Value	ca-setgen-customer-code-input
	Click Element	//*[@id="ca-setgen-password-toggle-btn"]/span[1]
	${ENROLLMENTKEY}=	Get Value	filled-adornment-password
	GUI::Basic::Open And Login Nodegrid	${USERNAMENG}	${PASSWORDNG}
	GUI::Open System tab
	Sleep	5
	Click Element	xpath=(//a[contains(text(),'Toolkit')])[2]
	Wait Until Page Contains Element	icon_cloudEnrollment
	Click Element	icon_cloudEnrollment
	Wait Until Page Contains	Cloud Enrollment	20s
	Sleep	5
	Clear Element Text	url
	Press Keys	url	CTRL+a+BACKSPACE
	Input Text	url	${ZPECLOUD_HOMEPAGE}
	Input Text	customer_code	${CUSTOMERCODE}
	Input Text	enrollment_key	${ENROLLMENTKEY}
	Click Element	saveButton
	Wait Until Keyword Succeeds	30	30		Wait Until Page Contains	${TRANSFERTOTHESAMECOMPANYSUCCESS}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Available::Open Tab
	Sleep	10
	Input Text	//*[@id="search-input"]	${SERIALNUMBER}
	Wait Until Page Contains	${SERIALNUMBER}	30s
	Click Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Sleep	10
	GUI::ZPECloud::Basic::Click Button	enroll
	Wait Until Page Contains	Device Enrolled Successfully	15s