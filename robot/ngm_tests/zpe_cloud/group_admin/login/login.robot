*** Settings ***
Documentation	Tests for login into ZPE Cloud
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	ACCESS	CONSOLE
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-DEVICE

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${WRONGEMAIL}	wrongemail@email.com
${WRONGPWD}	wrong_p@ssw0rd
${CREDENTIALSERROR}	Unable to log in with provided credentials.
*** Test Cases ***
Login into ZPE Cloud with wrong credentials
	SUITE:Login with wrong credentials

Login into ZPE Cloud
	SUITE:Login into ZPE Cloud

Logout from ZPE Cloud
	SUITE: Logout from ZPE Cloud

Login into ZPE CLOUD AS superadmin
	SUITE:Login into ZPE Cloud AS Super-Admin

*** Keywords ***
SUITE:Login with wrong credentials
	GUI::ZPECloud::Basic::Open ZPE Cloud	${ZPECLOUD_HOMEPAGE}	${BROWSER}
	Sleep	2
	Set Window Size	1600	1000
	GUI::ZPECloud::Basic::Wait Elements Login
	Input Text	xpath=//form//input[1]	${WRONGEMAIL}
	Input Text	css=input[id=filled-adornment-password]	${WRONGPWD}
	Click Element	xpath=//form/button[1]
#	Sleep	5
	Wait Until Page Contains	${CREDENTIALSERROR}	15s
	Close Browser

SUITE:Login into ZPE Cloud
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

SUITE:Login into ZPE Cloud AS Super-Admin
	GUI::ZPECloud::Basic::Open and Login	${SUPER_EMAIL_ADDRESS}	${SUPER_PASSWORD}

SUITE: Logout from ZPE Cloud
	GUI::ZPECloud::Basic::Logout and Close All