*** Settings ***
Documentation	Tests for Clean password field on failed login attempt on cloud
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../init.robot
Default Tags	ACCESS	CONSOLE
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-DEVICE

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${WRONGEMAIL}	wrongemail@email.com
${WRONGPWD}	wrong_p@ssw0rd
${CREDENTIALSERROR}	Unable to log in with provided credentials.
${VALIDATE_URL}	${ZPECLOUD_HOMEPAGE}/login
${GET_URL}

*** Test Cases ***
Login into ZPE Cloud with wrong credentials By Cliking On SIGN IN
	SUITE:Login with wrong credentials By Cliking On SIGN IN	${WRONGEMAIL}	${WRONGPWD}
	Textfield Value Should Be	xpath=//form//input[1]	${WRONGEMAIL}
	Textfield Value Should Be	css=input[id=filled-adornment-password]	${EMPTY}
	${GET_URL}=	Get Location
	Should Be Equal	${GET_URL}	${VALIDATE_URL}
	Close All Browsers

Login into ZPE Cloud with wrong credentials By Cliking Enter
	SUITE:Login with wrong credentials By Clicking ENTER	${WRONGEMAIL}	${WRONGPWD}
	Element Should Be Focused	css=input[id=filled-adornment-password]
	Textfield Value Should Be	xpath=//form//input[1]	${WRONGEMAIL}
	Textfield Value Should Be	css=input[id=filled-adornment-password]	${EMPTY}
	${GET_URL}=	Get Location
	Should Be Equal	${GET_URL}	${VALIDATE_URL}
	Close All Browsers

*** Keywords ***
SUITE:Login with wrong credentials By Cliking On SIGN IN
	[Arguments]	${EMAIL_ID}	${MAIL_PASSWORD}
	GUI::ZPECloud::Basic::Open ZPE Cloud	${ZPECLOUD_HOMEPAGE}	${BROWSER}
	Sleep	2
	Set Window Size	1600	1000
	GUI::ZPECloud::Basic::Wait Elements Login
	Input Text	xpath=//form//input[1]	${EMAIL_ID}
	Input Text	css=input[id=filled-adornment-password]	${MAIL_PASSWORD}
	Click Element	xpath=//form/button[1]
#	Sleep	5
	Wait Until Page Contains	${CREDENTIALSERROR}	15s

SUITE:Login with wrong credentials By Clicking ENTER
	[Arguments]	${EMAIL_ID}	${MAIL_PASSWORD}
	GUI::ZPECloud::Basic::Open ZPE Cloud	${ZPECLOUD_HOMEPAGE}	${BROWSER}
	Sleep	2
	Set Window Size	1600	1000
	GUI::ZPECloud::Basic::Wait Elements Login
	Input Text	xpath=//form//input[1]	${EMAIL_ID}
	Input Text	css=input[id=filled-adornment-password]	${MAIL_PASSWORD}
	Press Keys	None	ENTER
#	Sleep	5
	Wait Until Page Contains	${CREDENTIALSERROR}	15s

