*** Settings ***
Documentation	Test Single Sign On functionality by configuring and login in ZPE Cloud with SSO
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	SSO
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${NAME}	duo
${DESCRIPTION}	SSO Test
${ENTITY}	ZPECloudQA
${SSO_URL}	https://sso-dag-8820128.test.zpecloud.com/dag/saml2/idp/SSOService.php
${ISSUER}	https://sso-dag-8820128.test.zpecloud.com/dag/saml2/idp/metadata.php
${SSOFIRSTNAME}	SSO
${SSOLASTNAME}	TEST
${NOTREALNUMBER}	2345678910
${USERGROUP}	Administrator
${SSO_CERTIFICATE}	/home/${JENKINS_VM_USERNAME}/Documents/dag 1.crt
${SSO_METADATA}	/home/${JENKINS_VM_USERNAME}/Documents/dag1.xml

*** Test Cases ***
Configure SSO manually and Login
	SUITE:Configure SSO manually
	SUITE:Login using SSO
	[Teardown]	SUITE:Teardown

Configure SSO load metadata and Login
	[Setup]	SUITE:Setup
	SUITE:SSO configure with metadata
	SUITE:Login using SSO
	[Teardown]	SUITE:Teardown

Configure Domain and Login
	[Setup]	SUITE:Setup
	SUITE:SSO configure with metadata
	SUITE:Configure the domain
	SUITE:Login using SSO by domain
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::SETTINGS::COMPANY::Open Tab
	Wait Until Page Contains Element	xpath=//input[@name='domain']	30s
	Press Keys	xpath=//input[@name='domain']	CTRL+a+BACKSPACE
	Clear Element Text	xpath=//input[@name='domain']
	Click Element	xpath=//button[@id='save']
	Close All Browsers

*** Keywords ***
SUITE:Setup
	SUITE:Check SSO User
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}

#	Maximize Browser Window

SUITE:Teardown
	Delete User from Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::SETTINGS::SSO::Open Tab
	Sleep	5
	Click Element	//*[@id="ca-sso-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="ca-settings-sso-wrapper"]/div[2]/div[1]/button[3]
	Click Element	xpath=//button[@id='confirm-btn']
	Wait Until Page Contains	SSO Deleted successfully
	Sleep	5
	Wait Until Page Does Not Contain	${NAME}
	GUI::ZPECloud::Basic::Logout and Close All

SUITE:Save Duo
	Wait Until Page Contains Element	xpath=//button[@id='submit-btn']
	Click Element	xpath=//button[@id='submit-btn']
	Wait Until Page Contains	SSO Added successfully
	Wait Until Page Contains Element	//*[@id="ca-sso-table-1"]/div/div/table
	Sleep	5
	Wait Until Page Contains	${NAME}

SUITE:Configure SSO manually
	GUI::ZPECloud::SETTINGS::SSO::Open Tab
	Sleep	5
	Click Element	xpath=//span[contains(.,'add')]
	Sleep	5
	Click Element	xpath=//div[3]/div/div/div
	Sleep	2
	Click Element	xpath=//div[@id='menu-sso_type']/div[3]/ul/li[3]
	Input Text	id=ca-sso-form-name	${NAME}
	wait until keyword succeeds	30	2	Choose File	add-certificate-btn	${SSO_CERTIFICATE}
	Input Text	id=ca-sso-form-description	${DESCRIPTION}
	Input Text	id=ca-sso-form-entity_id	${ENTITY}
	Input Text	id=ca-sso-form-sso_url	${SSO_URL}
	Input Text	id=ca-sso-form-issuer	${ISSUER}
	Sleep	5
	wait until keyword succeeds	30	2	SUITE:Save Duo
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-sso-table-1"]/div/div/table
	Wait Until Page Contains Element	//*[@id="ca-sso-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="ca-sso-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Wait Until Page Contains Element	//*[@id="ca-settings-sso-wrapper"]/div[2]/div[1]/button[4]
	Click Element	//*[@id="ca-settings-sso-wrapper"]/div[2]/div[1]/button[4]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	SSO Activated successfully
	Sleep	5
	Wait Until Page Contains	ACTIVE

SUITE:SSO configure with metadata
	GUI::ZPECloud::SETTINGS::SSO::Open Tab
	Sleep	5
	Click Element	xpath=//span[contains(.,'add')]
	Sleep	5
	wait until keyword succeeds	30	2	Choose File	file-metadata	${SSO_METADATA}
	Sleep	5
	Input Text	id=ca-sso-form-name	${NAME}
	Input Text	id=ca-sso-form-description	${DESCRIPTION}
	Input Text	id=ca-sso-form-entity_id	${ENTITY}
	Sleep	5
	wait until keyword succeeds	30	2	SUITE:Save Duo
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-sso-table-1"]/div/div/table
	Wait Until Page Contains Element	//*[@id="ca-sso-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="ca-sso-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Wait Until Page Contains Element	//*[@id="ca-settings-sso-wrapper"]/div[2]/div[1]/button[4]
	Click Element	//*[@id="ca-settings-sso-wrapper"]/div[2]/div[1]/button[4]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	SSO Activated successfully
#	Sleep	5
	Wait Until Page Contains	ACTIVE

SUITE:Configure the domain
	${COMPLETE_DOMAIN}=	Create List
	GUI::ZPECloud::SETTINGS::COMPANY::Open Tab
	Sleep	5
	${NAME_DOMAIN}=	Get Value	xpath=//input[contains(@name, 'domain')]
	${FIXED_DOMAIN}	Get Text	//*[@id="customer-admin-layout"]/main/div/div/form/div[2]/div/div/div[1]/div/div[4]/div/div/p
	Run Keyword If	'${NAME_DOMAIN}'==''	Input Text	xpath=//input[contains(@name, 'domain')]	jenkins
	Sleep	5
	Click Element	xpath=//button[@id='save']
	Sleep	5
	${NAME_DOMAIN}=	Get Value	xpath=//input[contains(@name, 'domain')]
	${COMPANY_DOMAIN}=	Catenate	SEPARATOR=	${NAME_DOMAIN}	${FIXED_DOMAIN}
	${COMPANY_DOMAIN}=	Set Suite Variable	${COMPANY_DOMAIN}

SUITE:Login using SSO
	GUI::ZPECloud::Basic::Logout and Close All
#	${FIREFOX}=	SUITE:Firefox
#	Switch Browser	${FIREFOX}
#	Execute Javascript	window.open('${ZPECLOUD_HOMEPAGE}');
#	Switch Window	NEW
	GUI::ZPECloud::Basic::Open ZPE Cloud	${ZPECLOUD_HOMEPAGE}	${BROWSER}
	Sleep	10
	GUI::ZPECloud::Basic::Wait Elements Login
	Input Text	xpath=//form//input[1]	${SSO_CLOUD_USER}
#	Sleep	5
	Wait Until Page Contains Element	//*[@id="duo_duo"]	20s
	Sleep	5
	Click Element	//*[@id="duo_duo"]
#	Sleep	10
	Wait Until Page Contains Element	username	30s
	Input Text	username	${SSO_CLOUD_USER}
	Input Text	password	${SSO_CLOUD_PASSWORD}
	Click Element	login-button
	Sleep	5
	Wait Until Keyword Succeeds	30	2	GUI::ZPECloud::Basic::Wait Until Menu Appears
	Close All Browsers

SUITE:Login using SSO by domain
	GUI::ZPECloud::Basic::Logout and Close All
	GUI::ZPECloud::Basic::Open ZPE Cloud	${ZPECLOUD_HOMEPAGE}	${BROWSER}
	Sleep	10
	GUI::ZPECloud::Basic::Wait Elements Login
	Wait Until Page Contains Element	//*[@id="duo_duo"]
	Sleep	5
	Click Element	//*[@id="duo_duo"]
	Sleep	5
	Wait Until Element Is Visible	oauth-signin-form-email-input
	Sleep	5
	Input Password	oauth-signin-form-email-input	${COMPANY_DOMAIN}
	Sleep	5
	Click Element	//*[@id="duo_duo"]
	Sleep	10
	Wait Until Page Contains Element	username	30s
	Input Text	username	${SSO_CLOUD_USER}
	Input Text	password	${SSO_CLOUD_PASSWORD}
	Click Element	login-button
	Sleep	5
	Wait Until Keyword Succeeds	30	2	GUI::ZPECloud::Basic::Wait Until Menu Appears
	Close All Browsers

#SUITE:Firefox
	${ff_capabilities}=	Create Dictionary	moz:webdriverClick	${False}
	...	acceptInsecureCerts	${True}
	...	acceptUntrustedCerts	${True}
	...	browserName	firefox
	...	marionette	${True}
	${FIREFOX}=	Create Webdriver	Firefox	desired_capabilities=${ff_capabilities}

SUITE:Check SSO User
	GUI::ZPECloud::Basic::Open And Login	${SUPER_EMAIL_ADDRESS}	${SUPER_PASSWORD}
	GUI::ZPECloud::Users::Open Tab
	Wait Until Page Contains Element	//*[@id="search-input"]	30s
	Click Element	//*[@id="search-input"]
	Input Text	//*[@id="search-input"]	${SSO_CLOUD_USER}
	Click Element	//*[@id="search-input"]
	Press Keys	//*[@id="search-input"]	ENTER
	Sleep	5
	${STATUS}=	Run Keyword and Return Status	Wait Until Page Contains	No result found
	Run Keyword If	${STATUS} == True	Add User on Company
#	Sleep	10
#	${STATUS}=	Run Keyword and Return Status	Element Should Contain	//*[@id="ca-ugn-table-1"]/div/div/table/tbody/tr/td[2]/div	${SSO_CLOUD_USER}
	Run Keyword If	${STATUS} == False	Move User to Company
	Close All Browsers

Move User to Company
	Delete User from Company
	Add User on Company

Delete User from Company
	GUI::ZPECloud::Basic::Open And Login	${SUPER_EMAIL_ADDRESS}	${SUPER_PASSWORD}
	GUI::ZPECloud::Users::Open Tab
	Wait Until Page Contains Element	//*[@id="search-input"]	30s
	Click Element	//*[@id="search-input"]
	Input Text	//*[@id="search-input"]	${SSO_CLOUD_USER}
	sleep	5s
	${STATUS}=	Run Keyword and Return Status	Wait Until Page Contains	${SSO_CLOUD_USER}
	Run Keyword If	${STATUS} == True	SUITE:DELETE SSO USER
#	Press Keys	//*[@id="search-input"]	ENTER
#	Sleep	5
#	${ROWNUMBER}=	Execute Javascript	return Array.from(document.getElementById('sa-users-general-table-1').getElementsByTagName('TBODY')[0].getElementsByTagName('TR')).indexOf(Array.from(document.getElementById('sa-users-general-table-1').getElementsByTagName('TBODY')[0].getElementsByTagName('TR')).filter(e => e.getElementsByTagName('TD')[1].textContent == '${SSO_CLOUD_USER}')[0])
#	Run Keyword If	${ROWNUMBER} == 0	Click Element	//*[@id="sa-users-general-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/span[1]/input
#	Run Keyword If	${ROWNUMBER} > 0	Click Element	//*[@id="sa-users-general-table-1"]/div[1]/div/table/tbody/tr[${ROWNUMBER}]/td[1]/span/span[1]/input

SUITE:DELETE SSO USER
	Wait Until Page Contains Element	//*[@id="sa-users-general-table-1"]/div[1]/div/table/tbody/tr[1]/td[1]/span/input	30s
	Click Element	//*[@id="sa-users-general-table-1"]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
	Wait Until Page Contains Element	//*[@id="function-button-2"]	30s
	Click Element	//*[@id="function-button-2"]
	Wait Until Page Contains	Delete User	30s
	Click Element	//*[@id="confirm-btn"]
#	Wait Until Page Contains	User deleted successfully

Add User on Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Users::Open Tab
	Click Element	//*[@id="function-button-0"]
	Wait Until Page Contains	Add New User
	Wait Until Page Contains Element	//*[@id="ca-usergrp-form-field-email"]
	Input Text	//*[@id="ca-usergrp-form-field-email"]	${SSO_CLOUD_USER}
	Input Text	//*[@id="ca-usergrp-form-field-first_name"]	${SSOFIRSTNAME}
	Input Text	//*[@id="ca-usergrp-form-field-last_name"]	${SSOLASTNAME}
	Input Text	//*[@id="ca-usergrp-add-dialog"]/div[2]/div/div/div/div[1]/div/div[7]/div/div/input	${NOTREALNUMBER}
	Click Element	//*[@id="ca-user-groups"]
	Click Element	xpath=//li[2]
	Press Keys	xpath=//li[contains(.,'User')]	ESC
	Sleep	5
	Click Element	//*[@id="submit-btn"]
	wait until keyword succeeds	30	5	Wait Until Page Contains	User added successfully
	Wait Until Keyword Succeeds	30	5	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Users::Open Tab
	Wait Until Page Contains Element	//*[@id="search-input"]
	Click Element	//*[@id="search-input"]
	Input Text	//*[@id="search-input"]	${SSO_CLOUD_USER}
#	Press Keys	//*[@id="search-input"]	ENTER
	Wait Until Page Contains	${SSO_CLOUD_USER}