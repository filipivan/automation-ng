*** Settings ***
Documentation	Test Single Sign On configuration
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	SSO
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	NON-DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${NAME}	duo
${DESCRIPTION}	SSO Test
${ENTITY}	ZPECloudQA
${SSO_URL}	https://sso-dag-8820128.test.zpecloud.com/dag/saml2/idp/SSOService.php
${ISSUER}	https://sso-dag-8820128.test.zpecloud.com/dag/saml2/idp/metadata.php
${SSO_CERTIFICATE}	/home/${JENKINS_VM_USERNAME}/Documents/dag 1.crt
${SSO_METADATA}	/home/${JENKINS_VM_USERNAME}/Documents/dag1.xml

*** Test Cases ***
Configure SSO manually
	SUITE:Configure SSO manually
	[Teardown]	SUITE:Teardown

Configure SSO load metadata
	[Setup]	SUITE:Setup
	SUITE:SSO configure with metadata

Configure Domain
	[Setup]	SUITE:Setup
	SUITE:Configure the domain
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::SETTINGS::COMPANY::Open Tab
	Wait Until Page Contains Element	xpath=//input[@name='domain']	30s
	Press Keys	xpath=//input[@name='domain']	CTRL+a+BACKSPACE
	Clear Element Text	xpath=//input[@name='domain']
	Click Element	xpath=//button[@id='save']

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
#	Maximize Browser Window

SUITE:Teardown
	SUITE:Setup
	GUI::ZPECloud::SETTINGS::SSO::Open Tab
	Sleep	5
	Click Element	//*[@id="ca-sso-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="ca-settings-sso-wrapper"]/div[2]/div[1]/button[3]
	Click Element	xpath=//button[@id='confirm-btn']
	Wait Until Page Contains	SSO Deleted successfully
	Sleep	5
	Page Should not Contain	${NAME}
	GUI::ZPECloud::Basic::Logout and Close All

SUITE:Save Duo
	Wait Until Page Contains Element	xpath=//button[@id='submit-btn']
	Click Element	xpath=//button[@id='submit-btn']
	Wait Until Page Contains	SSO Added successfully
	Wait Until Page Contains Element	//*[@id="ca-sso-table-1"]/div/div/table
	Sleep	5
	Wait Until Page Contains	${NAME}

SUITE:Configure SSO manually
	GUI::ZPECloud::SETTINGS::SSO::Open Tab
	Sleep	5
	Click Element	xpath=//span[contains(.,'add')]
	Sleep	5
	Click Element	xpath=//div[3]/div/div/div
	Sleep	2
	Click Element	xpath=//div[@id='menu-sso_type']/div[3]/ul/li[3]
	Input Text	id=ca-sso-form-name	${NAME}
	wait until keyword succeeds	30	2	Choose File	add-certificate-btn	${SSO_CERTIFICATE}
	Input Text	id=ca-sso-form-description	${DESCRIPTION}
	Input Text	id=ca-sso-form-entity_id	${ENTITY}
	Input Text	id=ca-sso-form-sso_url	${SSO_URL}
	Input Text	id=ca-sso-form-issuer	${ISSUER}
	Sleep	5
	wait until keyword succeeds	30	2	SUITE:Save Duo
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-sso-table-1"]/div/div/table
	Wait Until Page Contains Element	//*[@id="ca-sso-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="ca-sso-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Wait Until Page Contains Element	//*[@id="ca-settings-sso-wrapper"]/div[2]/div[1]/button[4]
	Click Element	//*[@id="ca-settings-sso-wrapper"]/div[2]/div[1]/button[4]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	SSO Activated successfully
	Sleep	5
	Wait Until Page Contains	ACTIVE

SUITE:SSO configure with metadata
	GUI::ZPECloud::SETTINGS::SSO::Open Tab
	Sleep	5
	Click Element	xpath=//span[contains(.,'add')]
	Sleep	5
	wait until keyword succeeds	30	2	Choose File	file-metadata	${SSO_METADATA}
	Sleep	5
	Input Text	id=ca-sso-form-name	${NAME}
	Input Text	id=ca-sso-form-description	${DESCRIPTION}
	Input Text	id=ca-sso-form-entity_id	${ENTITY}
	Sleep	5
	wait until keyword succeeds	30	2	SUITE:Save Duo
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-sso-table-1"]/div/div/table
	Wait Until Page Contains Element	//*[@id="ca-sso-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="ca-sso-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Wait Until Page Contains Element	//*[@id="ca-settings-sso-wrapper"]/div[2]/div[1]/button[4]
	Click Element	//*[@id="ca-settings-sso-wrapper"]/div[2]/div[1]/button[4]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	SSO Activated successfully
	Sleep	5
	Wait Until Page Contains	ACTIVE

SUITE:Configure the domain
	GUI::ZPECloud::SETTINGS::COMPANY::Open Tab
	Sleep	5
	${NAME_DOMAIN}=	Get Value	xpath=//input[contains(@name, 'domain')]
	${FIXED_DOMAIN}=	Get Text	//*[@id="customer-admin-layout"]/main/div/div/form/div[2]/div/div/div[1]/div/div[4]/div/div/p
	Log to Console	\nta na mão ${FIXED_DOMAIN}\n
	#${NAME_DOMAIN}=	Get Value	*//[@id="customer-admin-layout"]/main/div/form/div[2]/div/div/div[1]/div/div[4]/div/input
	Run Keyword If	'${NAME_DOMAIN}'==''	Input Text	xpath=//input[contains(@name, 'domain')]	jenkins
	Sleep	5
	Click Element	xpath=//button[@id='save']