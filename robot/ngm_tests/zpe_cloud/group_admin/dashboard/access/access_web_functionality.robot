*** Settings ***
Documentation	Tests for accessing device through web with ZPE Cloud
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	ACCESS_WEB
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_2

Suite Setup	SUITE:Setup
Suite Teardown	Close All Browsers

*** Variables ***
${DEVICE}	nodegrid
${CONNECTIONNAME}	test_connection
${REMOTEACCESS}	remote_access
${ERROR}	client-snackbar
${CONNECTION}	xpath=//tr[@id='${CONNECTIONNAME}']/td/input
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${FAILOVERIP}	1.1.1.1

*** Test Cases ***
Test access device through WEB when enable_remote_access=yes
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	Wait Until Keyword Succeeds	120	5	SUITE:Check That Devices::Enrolled Page Contain Online Devices
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Security::Open Services tab
	GUI::Basic::Spinner Should Be Invisible
	${IS_NOTSELECTED}=	Run Keyword And return Status	Checkbox Should Not Be Selected	${REMOTEACCESS}
	Run Keyword If	${IS_NOTSELECTED} == True	Click Element	${REMOTEACCESS}
	Run Keyword If	${IS_NOTSELECTED} == True	GUI::Basic::Save
	Wait Until Keyword Succeeds	60	5	SUITE:Access Web
	[Teardown]	Close All Browsers

Test access device through WEB when enable_remote_access=no
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Security::Open Services tab
	GUI::Basic::Spinner Should Be Invisible
	${IS_NOTSELECTED}=	Run Keyword And return Status	Checkbox Should Not Be Selected	${REMOTEACCESS}
	Run Keyword If	${IS_NOTSELECTED} == False	Click Element	${REMOTEACCESS}
	Run Keyword If	${IS_NOTSELECTED} == False	GUI::Basic::Save
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Dashboard::Access::Open Tab
	Wait Until Page Contains	Online	30s
	Wait Until Page Contains	${DEVICE}
	GUI::ZPECloud::Basic::Element Status Should Be Disabled	web
	[Teardown]	Close All Browsers

Test access device through WEB when enable_remote_access=yes and keep connection with failover=configured/triggered
	GUI::ZPECloud::Add connection	${USERNG}	${PWDNG}	${CONNECTIONNAME}
	GUI::ZPECloud::Configure Failover	${USERNG}	${PWDNG}	${CONNECTIONNAME}	${FAILOVERIP}
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Security::Open Services tab
	GUI::Basic::Spinner Should Be Invisible
	${IS_NOTSELECTED}=	Run Keyword And return Status	Checkbox Should Not Be Selected	remote_access
	Run Keyword If	${IS_NOTSELECTED} == True	Click Element	${REMOTEACCESS}
	Run Keyword If	${IS_NOTSELECTED} == True	GUI::Basic::Save
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Dashboard::Access::Open Tab
	wait until keyword succeeds	5m	5s	SUITE:Status is Failover
	Wait Until Page Contains	${DEVICE}
	Input Text	search-input	${SERIALNUMBER}
	Sleep	2
	Wait Until Keyword Succeeds	2m	5s	SUITE:Wait until web clickable
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	30s	1s	Switch Window	NEW
	Wait Until Keyword Succeeds	1m	1s	SUITE:Web Access
	Input Text	username	${USERNG}
	Input Text	password	${PWDNG}
	Click Element	login-btn
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	main_menu
	[Teardown]	SUITE:Teardown

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Dashboard::Access::Open Tab

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	Wait Until Keyword Succeeds	30	5	GUI::Network::Open Settings Tab
	Wait Until Page Contains Element	failover
	${IS_SELECTED}=	Run Keyword And return Status	Checkbox Should Not Be Selected	failover
	Run Keyword If	${IS_SELECTED} == False	Click Element	failover
	Run Keyword If	${IS_SELECTED} == False	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Delete Connection
	GUI::Security::Open Firewall tab
	Click Element	//*[@id='OUTPUT:IPv4']/td[2]/a
	Sleep	2
	Click Element	xpath=//tr[2]/td/input
	Click Element	delButton
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2
	Page Should Not Contain Element	xpath=//tr[2]/td/input
	Close All Browsers

SUITE:Wait until web clickable
	Reload Page
	Sleep	5
	GUI::ZPECloud::Basic::Click Button	web

SUITE:Check That Devices::Enrolled Page Contain Online Devices
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Wait Until Page Contains	Online

SUITE:Enable Network Failover
	Wait Until Keyword Succeeds	30	5	GUI::Network::Open Settings Tab
	Wait Until Page Contains Element	failover
	${IS_NOTSELECTED}=	Run Keyword And return Status	Checkbox Should Not Be Selected	failover
	Run Keyword If	${IS_NOTSELECTED} == True	Click Element	failover
	${CONTAINDNS}=	Run Keyword And return Status	Element Should Contain	address	1.1.1.1
	Run Keyword If	${CONTAINDNS} == False	Input Text	address	1.1.1.1
	Run Keyword If	${IS_NOTSELECTED} == True	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Access Web
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Dashboard::Access::Open Tab
#	Sleep	15
	Wait Until Page Contains	Online	30s
	Wait Until Page Contains	${DEVICE}
	Input Text	search-input	${SERIALNUMBER}
	Sleep	2
	GUI::ZPECloud::Basic::Click Button	web
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5
	Wait Until Keyword Succeeds	30	2	Switch Window	NEW
	Sleep	5
	Wait Until Keyword Succeeds	60	2	SUITE:Web Access
	Input Text	username	${USERNG}
	Input Text	password	${PWDNG}
	Click Element	login-btn
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	main_menu

SUITE:Delete Connection
	GUI::Network::Open Connections Tab
	${CONNECTIONEXISTS}=	Run Keyword And return Status	Page Should Contain	${CONNECTIONNAME}
	Run Keyword If	${CONNECTIONEXISTS} == True	Click Element	${CONNECTION}
	Run Keyword If	${CONNECTIONEXISTS} == True	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

SUITE:Web Access
	${LOGIN}=	Run Keyword And return Status	Page Should Contain Element	username
	Run Keyword If	${LOGIN} == False	Reload Page
	Sleep	5
	Wait Until Page Contains Element	username	15s

SUITE:Status is Failover
	Reload Page
	Wait Until Page Contains Element	search-input	1m
	Input Text	search-input	${SERIALNUMBER}
	Sleep	2
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Failover	30s
