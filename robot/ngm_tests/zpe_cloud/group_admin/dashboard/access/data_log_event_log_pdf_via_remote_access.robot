*** Settings ***
Documentation	Tests to check Data Log / Event Log (pdf) via Remote Access
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	ACCESS	WEB
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${REMOTEACCESS}	remote_access
${STATUS}	Online
${DEVICE}	nodegrid
${EVENT_ID}	Event ID
${PAGE_NUMBER}	Page 1

*** Test Cases ***
Test to check Data Log via Remote Access
	SUITE:Access Web
	GUI::Open System tab
#	GUI::System::Open Logging tab
	Wait Until Page Contains Element	xpath=(//a[contains(text(),'Logging')])[2]	15s
	Click Element	xpath=(//a[contains(text(),'Logging')])[2]
	Wait Until Page Contains Element	sessionlogenable
	GUI::Basic::Spinner Should Be Invisible
	${IS_NOTSELECTED}=	Run Keyword And return Status	Checkbox Should Not Be Selected	sessionlogenable
	Run Keyword If	${IS_NOTSELECTED} == True	Click Element	sessionlogenable
	Run Keyword If	${IS_NOTSELECTED} == True	GUI::Basic::Save
	GUI::Access::Open
	Wait Until Page Contains Element	xpath=//a[contains(text(),'Info')]
	${CURRENT_WINDOW_URL}	Get Location
	Click Element	xpath=//a[contains(text(),'Info')]
	Wait Until Element Is Visible	xpath=//input[@id='datalog']
	Click Element	xpath=//input[@id='datalog']
#	Sleep	5
	wait until keyword succeeds	30	5	GUI::Basic::Spinner Should Be Invisible
	Sleep	5
	Wait Until Keyword Succeeds	30	2	Switch Window	NEW
	Wait Until Keyword Succeeds	30	2	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	${PAGE_TEXT}=	GUI::Basic::Return PDF text without new lines	${CURRENT_WINDOW_URL}
	Should Contain	${PAGE_TEXT}	${PAGE_NUMBER}
	[Teardown]	SUITE:Teardown

Test to check Event Log (pdf) via Remote Access
#	SUITE:Setup
	SUITE:Access Web
	GUI::Open System tab
#	GUI::System::Open Logging tab
	Wait Until Page Contains Element	xpath=(//a[contains(text(),'Logging')])[2]	15s
	Click Element	xpath=(//a[contains(text(),'Logging')])[2]
	Wait Until Page Contains Element	sessionlogenable
	GUI::Basic::Spinner Should Be Invisible
	${IS_SELECTED}=	Run Keyword And return Status	Checkbox Should Not Be Selected	sessionlogenable
	Run Keyword If	${IS_SELECTED} == False	Click Element	sessionlogenable
	Run Keyword If	${IS_SELECTED} == False	GUI::Basic::Save
	GUI::Access::Open
	Wait Until Page Contains Element	xpath=//a[contains(text(),'Info')]
	${CURRENT_WINDOW_URL}	Get Location
	Click Element	xpath=//a[contains(text(),'Info')]
	Wait Until Element Is Visible	xpath=//input[@id='eventlog']
	Click Element	xpath=//input[@id='eventlog']
#	Sleep	5
	wait until keyword succeeds	30	5	GUI::Basic::Spinner Should Be Invisible
	Sleep	5
	Wait Until Keyword Succeeds	30	2	Switch Window	NEW
	Wait Until Keyword Succeeds	30	2	GUI::Basic::Spinner Should Be Invisible
	sleep	5s
	${PAGE_TEXT}=	GUI::Basic::Return PDF text without new lines	${CURRENT_WINDOW_URL}
	Should Contain	${PAGE_TEXT}	${EVENT_ID}

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	${status}=	Run Keyword and Return Status	SUITE:Check That Devices::Enrolled Page Contain Online Devices
	Run Keyword If	${status} == False	CLI::ZPECloud::Restart agent process via root
	Wait Until Keyword Succeeds	120	5	SUITE:Check That Devices::Enrolled Page Contain Online Devices
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Security::Open Services tab
	${IS_NOTSELECTED}=	Run Keyword And return Status	Checkbox Should Not Be Selected	remote_access
	Run Keyword If	${IS_NOTSELECTED} == True	Click Element	${REMOTEACCESS}
	Run Keyword If	${IS_NOTSELECTED} == True	GUI::Basic::Save

SUITE:Teardown
	Close All Browsers

SUITE:Check That Devices::Enrolled Page Contain Online Devices
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Wait Until Page Contains	Online	15s

SUITE:Access Web
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Dashboard::Access::Open Tab
#	Sleep	15
	Wait Until Page Contains	${STATUS}	30s
	Page Should Contain	${STATUS}
	Page Should Contain	${DEVICE}
	GUI::ZPECloud::Basic::Click Button	web
	wait until keyword succeeds	30	5	GUI::Basic::Spinner Should Be Invisible
	Sleep	5
	Wait Until Keyword Succeeds	30	2	Switch Window	NEW
	Sleep	5
	Wait Until Keyword Succeeds	60	2	SUITE:Web Access
	Input Text	username	${USERNG}
	Input Text	password	${PWDNG}
	Click Element	login-btn
	wait until keyword succeeds	30	5	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	main_menu

SUITE:Web Access
	${LOGIN}=	Run Keyword And return Status	Page Should Contain Element	username
	Run Keyword If	${LOGIN} == False	Reload Page
#	Sleep	5
	Wait Until Page Contains Element	username	20s