*** Settings ***
Documentation	Tests for accessing the console of managed device that was first accessed through the web from ZPE Cloud
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	MANAGED_DEVICES_ACCESS	ACCESS	CONSOLE	WEB
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-CRITICAL	DEVICE	PART_2

Suite Setup	SUITE:Setup

*** Variables ***
${DEVICE}	nodegrid
${CONNECTIONNAME}	test_connection
${FAILOVERIP}	1.1.1.1
${CONNECTION}	xpath=//tr[@id='${CONNECTIONNAME}']/td/input
${CONSOLETTYD}	xpath=//*[@id='termwindow']
${REMOTEACCESS}	remote_access
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${REMOTEACCESS}	remote_access
${CONNECTIONERROR}	[error.connection.failure] Could not establish a connection
${CONSOLE}	//*[@id="customer-admin-layout"]/main/div/div[4]/div/div/table/tbody/tr[3]/td/div/div[1]/div/div/div[2]/span[1]/button
${CONNECTEDDEVICE1}	test_fallback1
${CONNECTEDDEVICE2}	test_fallback2
${CONNECTEDDEVICE3}	test_fallback3
${CONNECTEDDEVICE4}	test_fallback4
${CONNECTEDDEVICE5}	test_fallback5
${managed_device_name1}
${Managed_device_type_name1}
${Managed_device_Mode1}
${Managed_device_ip_address1}

*** Test Cases ***
Test access device CONSOLE of managed device
	[Tags]	MANANGED_DEVICE_CONSOLE
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	SUITE:Check device status and add managed device
	Wait Until Keyword Succeeds	120	5	SUITE:Access console
	[Teardown]	SUITE:Teardown

Test access device WEB of managed device
	[Tags]	MANANGED_DEVICE_WEB
	SUITE:Check device status and add managed device
	Wait Until Keyword Succeeds	120	5	SUITE:Access Web
	[Teardown]	SUITE:Teardown

Test Case To Validate The Managed Device Information When NG Device Have More Than 1 Device On It
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	SUITE:Check device status and add 5 managed devices
	SUITE:Setup
	Wait Until Page Contains	${DEVICE}
	Click Element	//*[@id="customer-admin-layout"]/main/div/div[4]/div/div/table/tbody/tr/td[1]/button/span
	sleep	2s
	Wait Until Page Contains	${CONNECTEDDEVICE1}
	Wait Until Page Contains	${CONNECTEDDEVICE2}
	Wait Until Page Contains	${CONNECTEDDEVICE3}
	Wait Until Page Contains	${CONNECTEDDEVICE4}
	Wait Until Page Contains	${CONNECTEDDEVICE5}
	${managed_device_name1}=	Get Text	xpath=//div[@id='customer-admin-layout']/main/div/div[4]/div/div/table/tbody/tr[3]/td/div/div/div/div/div/div[2]
	Click Element	xpath=//div[@id='customer-admin-layout']/main/div/div[4]/div/div/table/tbody/tr[3]/td/div/div/div/div/div/div[2]
	sleep	5s
	${Managed_device_type_name1}=	Get Text	xpath=//div[@id='ca-connected-device-dialog']/div[3]/div/div/table/tbody/tr[3]/td[2]
	${Managed_device_Mode1}=	Get Text	xpath=//div[@id='ca-connected-device-dialog']/div[3]/div/div/table/tbody/tr[4]/td[2]
	${Managed_device_ip_address1}=	Get Text	xpath=//div[@id='ca-connected-device-dialog']/div[3]/div/div/table/tbody/tr[2]/td[2]
	${Managed_device_Mode1}=	Convert to Lowercase	${Managed_device_Mode1}
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Access::Open
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//span[contains(.,'${managed_device_name1}')]
	GUI::Basic::Spinner Should Be Invisible
	sleep	2s
	Wait Until Element Contains	//*[@id="tbody"]/tr[1]/td[2]	${managed_device_name1}
	Wait Until Element Contains	//*[@id="tbody"]/tr[4]/td[2]	${Managed_device_type_name1}
	${Managed_device_Mode1_INFO_FROM_NG}=	Get Text	//*[@id="tbody"]/tr[5]/td[2]
	${Managed_device_Mode1_INFO_FROM_NG}=	Convert to Lowercase	${Managed_device_Mode1_INFO_FROM_NG}
	Should Be Equal	${Managed_device_Mode1_INFO_FROM_NG}	${Managed_device_Mode1}
#	Wait Until Element Contains	//*[@id="tbody"]/tr[5]/td[2]	${Managed_device_Mode1}
	Wait Until Element Contains	//*[@id="tbody"]/tr[7]/td[2]	${Managed_device_ip_address1}
	[Teardown]	SUITE:Teardown2

Test access device CONSOLE of managed device When device in Failover
	[Tags]	MANANGED_DEVICE_FAILOVER_CONSOLE	MANANGED_DEVICE_CONSOLE
	SUITE:Check device status and add managed device
	GUI::ZPECloud::Add connection	${USERNG}	${PWDNG}	${CONNECTIONNAME}
	GUI::ZPECloud::Configure Failover	${USERNG}	${PWDNG}	${CONNECTIONNAME}	${FAILOVERIP}
	SUITE:Setup
	wait until keyword succeeds	160	5	SUITE:Status is Failover
	Wait Until Keyword Succeeds	120	5	SUITE:Access console
	[Teardown]	SUITE:Teardown1

Test access device WEB of managed device When device in Failover
	[Tags]	MANANGED_DEVICE_FAILOVER_WEB	MANANGED_DEVICE_WEB
	SUITE:Check device status and add managed device
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::ZPECloud::Add connection	${USERNG}	${PWDNG}	${CONNECTIONNAME}
	GUI::ZPECloud::Configure Failover	${USERNG}	${PWDNG}	${CONNECTIONNAME}	${FAILOVERIP}
	SUITE:Setup
	wait until keyword succeeds	160	5	SUITE:Status is Failover
	Wait Until Keyword Succeeds	120	5	SUITE:Access Web
	[Teardown]	SUITE:Teardown1

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Dashboard::Access::Open Tab
	Sleep	5

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::ManagedDevices::Delete Device If Exists	${CONNECTEDDEVICE1}
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:Teardown1
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	wait until keyword succeeds	120	5	GUI::Network::Open Settings Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	failover
	${IS_SELECTED}=	Run Keyword And return Status	Checkbox Should Not Be Selected	failover
	Run Keyword If	${IS_SELECTED} == False	Click Element	failover
	Run Keyword If	${IS_SELECTED} == False	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Delete Connection
	GUI::Security::Open Firewall tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id='OUTPUT:IPv4']/td[2]/a
	wait until keyword succeeds	60	5	GUI::Basic::Spinner Should Be Invisible
	Sleep	2
	Click Element	xpath=//tr[2]/td/input
	Click Element	delButton
	wait until keyword succeeds	60	5	GUI::Basic::Spinner Should Be Invisible
	Sleep	2
	Page Should Not Contain Element	xpath=//tr[2]/td/input
	SUITE:Setup
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	Close All Browsers

SUITE:Teardown2
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::ManagedDevices::Delete Device If Exists	${CONNECTEDDEVICE1}
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${CONNECTEDDEVICE2}
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${CONNECTEDDEVICE3}
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${CONNECTEDDEVICE4}
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${CONNECTEDDEVICE5}
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

SUITE:Access Web
	SUITE:Setup
#	Wait Until Page Contains	Online	30s
	Wait Until Page Contains	${DEVICE}
	Click Element	//*[@id="customer-admin-layout"]/main/div/div[4]/div/div/table/tbody/tr/td[1]/button/span
	Wait Until Page Contains	${CONNECTEDDEVICE1}
	Click Element	//*[@id="customer-admin-layout"]/main/div/div[4]/div/div/table/tbody/tr[3]/td/div/div[1]/div/div/div[2]/span[2]/button
	wait until keyword succeeds	60	5	GUI::Basic::Spinner Should Be Invisible
	Sleep	5
	Wait Until Keyword Succeeds	30	2	Switch Window	NEW
	Sleep	5
	Wait Until Keyword Succeeds	60	2	SUITE:Web Access
	Input Text	username	${USERNG}
	Input Text	password	${PWDNG}
	Click Element	login-btn
	wait until keyword succeeds	30	5	GUI::Basic::Spinner Should Be Invisible
	Sleep	10
	${STATUS}	Evaluate NG Versions: ${NGVERSION} >= 5.6
	Run Keyword If	${STATUS}	GUI:Basic::Wait Until Window Exists	${CONNECTEDDEVICE1} - Web
	...	ELSE	GUI:Basic::Wait Until Window Exists	${CONNECTEDDEVICE1}
#	Switch Window	title=${CONNECTEDDEVICE}
#	Sleep	10
	Wait Until Page Contains Element	targetParameters	30s
#	Sleep	15
#	Wait Until Page Contains Element	login_a

SUITE:Web Access
	${LOGIN}=	Run Keyword And return Status	Page Should Contain Element	username
	Run Keyword If	${LOGIN} == False	Reload Page
	Sleep	5
	Wait Until Page Contains Element	username	15s

SUITE:Access console
	SUITE:Setup
	Wait Until Page Contains	${DEVICE}
	Click Element	//*[@id="customer-admin-layout"]/main/div/div[4]/div/div/table/tbody/tr/td[1]/button/span[1]
	Wait Until Page Contains	${CONNECTEDDEVICE1}
	GUI::Basic::Wait Until Element Is Accessible	${CONSOLE}
	Wait Until Keyword Succeeds	3x	1s	SUITE:Click on Console Button
	Sleep	5
	Wait Until Keyword Succeeds	30s	1s	Switch Window	NEW
	Wait Until Page Contains Element	username	30s
	Input Text	username	${USERNG}
	Input Text	password	${PWDNG}
	Click Element	login-btn
	Sleep	10
	${STATUS}	Evaluate NG Versions: ${NGVERSION} >= 5.6
	Run Keyword If	${STATUS}	GUI:Basic::Wait Until Window Exists	${CONNECTEDDEVICE1} - Console
	...	ELSE	GUI:Basic::Wait Until Window Exists	${CONNECTEDDEVICE1}
	Sleep	10
	Wait Until Page Contains Element	targetParameters
	Sleep	20

SUITE:Click on Console Button
	Reload Page
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	${CONSOLE}	30s
	Click Element	${CONSOLE}
	GUI::Basic::Spinner Should Be Invisible

SUITE:Create Managed Device
	[Arguments]	${Managed_Device_Name}
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Security::Open Services tab
	${IS_NOTSELECTED}=	Run Keyword And return Status	Checkbox Should Not Be Selected	${REMOTEACCESS}
	Run Keyword If	${IS_NOTSELECTED} == True	Click Element	${REMOTEACCESS}
	Run Keyword If	${IS_NOTSELECTED} == True	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Delete Device If Exists	${Managed_Device_Name}
	GUI::ManagedDevices::Add Device	${Managed_Device_Name}	device_console	127.0.0.1	${USERNG}	no	${PWDNG}	enabled
	GUI::Basic::Spinner Should Be Invisible

SUITE:Delete Connection
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	${CONNECTIONEXISTS}=	Run Keyword And return Status	Page Should Contain	${CONNECTIONNAME}
	Run Keyword If	${CONNECTIONEXISTS} == True	Click Element	${CONNECTION}
	Run Keyword If	${CONNECTIONEXISTS} == True	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

SUITE:Status is Failover
	Reload Page
	Wait Until Page Contains	Failover	30s

SUITE:Check device status and add managed device
	SUITE:Setup
	${status}=	Run Keyword and Return Status	GUI::ZPECloud::Devices::Enrolled::Page Should Contain Device With Online Status
	Run Keyword If	${status} == False	CLI::ZPECloud::Restart agent process via root
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Security::Open Services tab
	GUI::Basic::Spinner Should Be Invisible
	${IS_NOTSELECTED}=	Run Keyword And return Status	Checkbox Should Not Be Selected	${REMOTEACCESS}
	Run Keyword If	${IS_NOTSELECTED} == True	Click Element	${REMOTEACCESS}
	Run Keyword If	${IS_NOTSELECTED} == True	Click Element	//*[@id="saveButton"]
	SUITE:Create Managed Device	${CONNECTEDDEVICE1}

SUITE:Check device status and add 5 managed devices
	SUITE:Setup
	${status}=	Run Keyword and Return Status	GUI::ZPECloud::Devices::Enrolled::Page Should Contain Device With Online Status
	Run Keyword If	${status} == False	CLI::ZPECloud::Restart agent process via root
	SUITE:Create Managed Device	${CONNECTEDDEVICE1}
	SUITE:Create Managed Device	${CONNECTEDDEVICE2}
	SUITE:Create Managed Device	${CONNECTEDDEVICE3}
	SUITE:Create Managed Device	${CONNECTEDDEVICE4}
	SUITE:Create Managed Device	${CONNECTEDDEVICE5}