*** Settings ***
Documentation	Tests for accessing the console of a device through ZPE Cloud
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Default Tags	ACCESS_CONSOLE
Force Tags	GUI	ZPECLOUD	${BROWSER}	ACCESS	NON-CRITICAL	DEVICE	PART_2

Suite Setup	SUITE:Setup
Suite Teardown	Close All Browsers

*** Variables ***
${DEVICE}	nodegrid
${CONNECTIONNAME}	test_connection
${CONSOLE}	xpath=//*[@id='termwindow']
${CONNECTION}	xpath=//tr[@id='${CONNECTIONNAME}']/td/input
${REMOTEACCESS}	remote_access
${ERROR}	client-snackbar
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${FAILOVERIP}	1.1.1.1

*** Test Cases ***
Test access device through CONSOLE when enable_remote_access=yes
	[Tags]	NON-CRITICAL
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	${status}=	Run Keyword and Return Status	SUITE:Check That Devices::Enrolled Page Contain Online Devices
	Run Keyword If	${status} == False	CLI::ZPECloud::Restart agent process via root
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Security::Open Services tab
	${IS_NOTSELECTED}=	Run Keyword And return Status	Checkbox Should Not Be Selected	${REMOTEACCESS}
	Run Keyword If	${IS_NOTSELECTED} == True	Click Element	${REMOTEACCESS}
	Run Keyword If	${IS_NOTSELECTED} == True	GUI::Basic::Save
	Wait Until Keyword Succeeds	120	5	SUITE:Access console
	Wait Until Keyword Succeeds	60	5	SUITE:Console Commands
	Close All Browsers

Test access device through CONSOLE when enable_remote_access=no
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Security::Open Services tab
	${IS_NOTSELECTED}=	Run Keyword And return Status	Checkbox Should Not Be Selected	${REMOTEACCESS}
	Run Keyword If	${IS_NOTSELECTED} == False	Click Element	${REMOTEACCESS}
	Run Keyword If	${IS_NOTSELECTED} == False	GUI::Basic::Save
	SUITE:Setup
	Page Should Contain	Online
	Page Should Contain	${DEVICE}
	GUI::ZPECloud::Basic::Element Status Should Be Disabled	console
	Close All Browsers

Test access device through CONSOLE when enable_remote_access=yes and keep connection with failover=configured/triggered
	[Tags]	NON-CRITICAL
	GUI::ZPECloud::Add connection	${USERNG}	${PWDNG}	${CONNECTIONNAME}
	GUI::ZPECloud::Configure Failover	${USERNG}	${PWDNG}	${CONNECTIONNAME}	${FAILOVERIP}
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Security::Open Services tab
	${IS_NOTSELECTED}=	Run Keyword And return Status	Checkbox Should Not Be Selected	${REMOTEACCESS}
	Run Keyword If	${IS_NOTSELECTED} == True	Click Element	${REMOTEACCESS}
	Run Keyword If	${IS_NOTSELECTED} == True	GUI::Basic::Save
	SUITE:Setup
	wait until keyword succeeds	160	5	SUITE:Status is Failover
	Page Should Contain	${DEVICE}
	Input Text	search-input	${SERIALNUMBER}
	Sleep	2
	Wait Until Keyword Succeeds	160	20s	SUITE:Wait until console clickable
#	wait until keyword succeeds	30	5	GUI::ZPECloud::Basic::Click Button	console
	wait until keyword succeeds	60	5	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	60	5	Switch Window	NEW
	Wait Until Keyword Succeeds	60	5	GUI::Basic::Direct Login	${USERNG}	${PWDNG}
	Sleep	5
	${STATUS}	Evaluate NG Versions: ${NGVERSION} >= 5.6
	Run Keyword If	${STATUS}	GUI:Basic::Wait Until Window Exists	nodegrid.localdomain - Console
	...	ELSE	GUI:Basic::Wait Until Window Exists	nodegrid.localdomain
	Wait Until Page Contains Element	${CONSOLE}	30
	Wait Until Keyword Succeeds	60	5	Select Frame	${CONSOLE}
	Sleep	10
	Press Keys	None	RETURN
	Press Keys	None	RETURN
	Wait Until Keyword Succeeds	60	5	SUITE:Console Commands
	[Teardown]	SUITE:Teardown

*** Keywords ***
SUITE:Setup
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Dashboard::Access::Open Tab

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Network::Open Settings Tab
	Page Should Contain Element	failover
	${IS_SELECTED}=	Run Keyword And return Status	Checkbox Should Not Be Selected	failover
	Run Keyword If	${IS_SELECTED} == False	Click Element	failover
	Run Keyword If	${IS_SELECTED} == False	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	SUITE:Delete Connection
	GUI::Security::Open Firewall tab
	Click Element	//*[@id='OUTPUT:IPv4']/td[2]/a
	wait until keyword succeeds	60	5	GUI::Basic::Spinner Should Be Invisible
	Sleep	2
	Click Element	xpath=//tr[2]/td/input
	Click Element	delButton
	wait until keyword succeeds	60	5	GUI::Basic::Spinner Should Be Invisible
	Sleep	2
	Page Should Not Contain Element	xpath=//tr[2]/td/input
	Close All Browsers

SUITE:Wait until console clickable
	Reload Page
	Sleep	5
	GUI::ZPECloud::Basic::Click Button	console

SUITE:Check That Devices::Enrolled Page Contain Online Devices
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
#	Sleep	5
	Wait Until Page Contains	Online	20s

SUITE:Console Command Output
	[Arguments]	${COMMAND}
	[Documentation]	Input the given command into the ttyd terminal and retrieve the output after prompt lines
	...	== ARGUMENTS ==
	...	-	COMMAND = Command to be executed
	...	== EXPECTED RESULT ==
	...	Input the command into the ttyd terminal and returns the OUTPUT between the command and the last prompt
	${CHECK_TAB}=	Run Keyword And Return Status	Should Not Contain	${COMMAND}	TAB
	Run Keyword If	${CHECK_TAB}	Press Keys	None	${COMMAND}	RETURN
	...	ELSE	Press Keys	None	${COMMAND}
	Sleep	1
	${SELECTION}=	Execute JavaScript	term.selectAll(); return term.getSelection().trim();
	Should Not Contain	${SELECTION}	${CONNECTIONNAME}
	${LINES}=	Split String	${SELECTION}	\n
	${INDEXES_OCCURRED}=	Create List
	${LENGTH}=	Get Length	${LINES}
	FOR	${INDEX}	IN RANGE	0	${LENGTH}
		${LINE}=	Get From List	${LINES}	${INDEX}
		${CHECK}=	Run Keyword And Return Status	Should Contain	${LINE}	]#
		Run Keyword If	${CHECK}	Append to List	${INDEXES_OCCURRED}	${INDEX}
	END
	${STRING}=	Set Variable
	${END}=	Get From List	${INDEXES_OCCURRED}	-1
	${PREVIOUS}=	Get From List	${INDEXES_OCCURRED}	-2
	FOR	${INDEX}	IN RANGE	${PREVIOUS}	${END}
		${LINE}=	Get From List	${LINES}	${INDEX}
		${STRING}=	Set Variable	${STRING}\n${LINE}
	END
	${STRING}=	Get Substring	${STRING}	1
	[Return]	${STRING}

SUITE:Access console
	SUITE:Setup
	Sleep	15
	Wait Until Page Contains	Online	30s
	Wait Until Page Contains	${DEVICE}
	Input Text	search-input	${SERIALNUMBER}
	Sleep	2
	GUI::ZPECloud::Basic::Click Button	console
	wait until keyword succeeds	60	5	GUI::Basic::Spinner Should Be Invisible

	Wait Until Keyword Succeeds	60	5	Switch Window	NEW
	Wait Until Keyword Succeeds	60	5	GUI::Basic::Direct Login	${USERNG}	${PWDNG}
	Sleep	5
	${STATUS}	Evaluate NG Versions: ${NGVERSION} >= 5.6
	Run Keyword If	${STATUS}	GUI:Basic::Wait Until Window Exists	nodegrid.localdomain - Console
	...	ELSE	GUI:Basic::Wait Until Window Exists	nodegrid.localdomain
	Wait Until Page Contains Element	${CONSOLE}	30s
	Wait Until Keyword Succeeds	60	5	Select Frame	${CONSOLE}
	Sleep	10
	Press Keys	None	RETURN

SUITE:Console Commands
	${OUTPUT}=	SUITE:Console Command Output	ls
	Should Contain	${OUTPUT}	access/	system/	settings/
	${OUTPUT}=	SUITE:Console Command Output	pwd
	Should Contain	${OUTPUT}	/
	${OUTPUT}=	SUITE:Console Command Output	hostname
	Should Contain	${OUTPUT}	nodegrid
	${OUTPUT}=	SUITE:Console Command Output	whoami
	Should Contain	${OUTPUT}	admin
	${OUTPUT}=	SUITE:Console Command Output	TAB+TAB
	${COMMANDS}=	Create List	apply_settings	cd	change_password	commit
	...	event_system_audit	event_system_clear	exit	factory_settings
	...	hostname	ls	pwd	quit	reboot	revert	save_settings	set
	...	shell	show	shutdown	software_upgrade	system_certificate
	...	system_config_check	whoami
	FOR	${COMMAND}	IN	@{COMMANDS}
		Should Contain	${OUTPUT}	${COMMAND}
	END
	Unselect Frame

SUITE:Delete Connection
	GUI::Network::Open Connections Tab
	${CONNECTIONEXISTS}=	Run Keyword And return Status	Page Should Contain	${CONNECTIONNAME}
	Run Keyword If	${CONNECTIONEXISTS} == True	Click Element	${CONNECTION}
	Run Keyword If	${CONNECTIONEXISTS} == True	GUI::Basic::Delete
	GUI::Basic::Spinner Should Be Invisible

SUITE:Status is Failover
	Reload Page
	Sleep	5
	Wait Until Page Contains	Failover	30s
