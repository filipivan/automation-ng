*** Settings ***
Documentation	Test Single Sign On on remote access for web
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../../init.robot
Default Tags	SSO	CONSOLE_ACCESS
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	DEVICE	PART_2
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${REMOTEACCESS}	remote_access
${CONSOLE}	xpath=//*[@id='termwindow']
${NAME}	duo
${DESCRIPTION}	SSO Test
${ENTITY}	ZPECloudQA
${SSO_URL}	https://sso-dag-8820128.test.zpecloud.com/dag/saml2/idp/SSOService.php
${ISSUER}	https://sso-dag-8820128.test.zpecloud.com/dag/saml2/idp/metadata.php
${SSOFIRSTNAME}	SSO
${SSOLASTNAME}	TEST
${NOTREALNUMBER}	2345678910
${USERGROUP}	Administrator
${SSO_CERTIFICATE}	/home/${JENKINS_VM_USERNAME}/Documents/dag 1.crt
${SSO_METADATA}	/home/${JENKINS_VM_USERNAME}/Documents/dag1.xml
${DEVICE_STATUS}	Online
${DEVICE}	nodegrid
${STATUS}	enable
${ENTITY_ID}	ZPERemoteAccessQA
${DUO_ICON}	css=.iconItem:nth-child(1) > img
${USER}

*** Test Cases ***
Configure SSO manually and Remote Access - CONSOLE
	SUITE:Configure SSO manually - Cloud
	SUITE:SSO configuration on the device
	SUITE:Login on Cloud using SSO
	SUITE:Remote Access Console with SSO
	[Teardown]	SUITE:Teardown

Configure SSO load metadata and Remote Access - CONSOLE
	[Setup]	SUITE:Setup
	SUITE:SSO configure with metadata - Cloud
	SUITE:SSO configuration on the device
	SUITE:Login on Cloud using SSO
	SUITE:Remote Access Console with SSO

*** Keywords ***
SUITE:Setup
	SUITE:Check SSO User
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Security::Open Services tab
	GUI::Basic::Spinner Should Be Invisible
	${IS_NOTSELECTED}=	Run Keyword And return Status	Checkbox Should Not Be Selected	${REMOTEACCESS}
	Run Keyword If	${IS_NOTSELECTED} == True	Click Element	${REMOTEACCESS}
	Run Keyword If	${IS_NOTSELECTED} == True	GUI::Basic::Save
	${status}=	Run Keyword and Return Status	SUITE:Check That Devices::Enrolled Page Contain Online Devices
	Run Keyword If	${status} == False	CLI::ZPECloud::Restart agent process via root
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
#	Maximize Browser Window
	${USER}=	Fetch From Left	${SSO_CLOUD_USER}	@
#	Set Suite Variable	${USER}	${USER}
	${USER}=	Set Suite Variable	${USER}

SUITE:Teardown
	Delete User from Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::SETTINGS::SSO::Open Tab
	Sleep	5
	Select Checkbox	//*[@id="ca-sso-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="ca-settings-sso-wrapper"]/div[2]/div[1]/button[3]
	Click Element	xpath=//button[@id='confirm-btn']
	Wait Until Page Contains	SSO Deleted successfully
	Sleep	5
	Wait Until Page Does Not Contain	${NAME}
	SUITE:Delete SSO on device
	SUITE:Delete User
#	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	Close All Browsers

SUITE:Save Duo
	Wait Until Page Contains Element	xpath=//button[@id='submit-btn']
	Click Element	xpath=//button[@id='submit-btn']
	Wait Until Page Contains	SSO Added successfully
	Wait Until Page Contains Element	//*[@id="ca-sso-table-1"]/div/div/table
	Sleep	5
	Wait Until Page Contains	${NAME}

SUITE:Configure SSO manually - Cloud
	GUI::ZPECloud::SETTINGS::SSO::Open Tab
	Sleep	5
	Click Element	xpath=//span[contains(.,'add')]
	Sleep	5
	Click Element	xpath=//div[3]/div/div/div
	Sleep	2
	Click Element	xpath=//div[@id='menu-sso_type']/div[3]/ul/li[3]
	Input Text	id=ca-sso-form-name	${NAME}
	wait until keyword succeeds	30	2	Choose File	add-certificate-btn	${SSO_CERTIFICATE}
	Input Text	id=ca-sso-form-description	${DESCRIPTION}
	Input Text	id=ca-sso-form-entity_id	${ENTITY}
	Input Text	id=ca-sso-form-sso_url	${SSO_URL}
	Input Text	id=ca-sso-form-issuer	${ISSUER}
	Sleep	5
	wait until keyword succeeds	30	2	SUITE:Save Duo
	wait until keyword succeeds	30	2	GUI::Basic::Spinner Should Be Invisible
#	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-sso-table-1"]/div/div/table	15s
	Wait Until Page Contains Element	//*[@id="ca-sso-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="ca-sso-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Wait Until Page Contains Element	//*[@id="ca-settings-sso-wrapper"]/div[2]/div[1]/button[4]
	Click Element	//*[@id="ca-settings-sso-wrapper"]/div[2]/div[1]/button[4]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	SSO Activated successfully
#	Sleep	5
	Wait Until Page Contains	ACTIVE	15s
	Close All Browsers

SUITE:SSO configure with metadata - Cloud
	GUI::ZPECloud::SETTINGS::SSO::Open Tab
	Sleep	5
	Click Element	xpath=//span[contains(.,'add')]
	Sleep	5
	wait until keyword succeeds	30	2	Choose File	file-metadata	${SSO_METADATA}
	Sleep	5
	Input Text	id=ca-sso-form-name	${NAME}
	Input Text	id=ca-sso-form-description	${DESCRIPTION}
	Input Text	id=ca-sso-form-entity_id	${ENTITY}
	Sleep	5
	wait until keyword succeeds	30	2	SUITE:Save Duo
#	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-sso-table-1"]/div/div/table	15s
	Wait Until Page Contains Element	//*[@id="ca-sso-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="ca-sso-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Wait Until Page Contains Element	//*[@id="ca-settings-sso-wrapper"]/div[2]/div[1]/button[4]
	Click Element	//*[@id="ca-settings-sso-wrapper"]/div[2]/div[1]/button[4]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	SSO Activated successfully
#	Sleep	5
	Wait Until Page Contains	ACTIVE	15s
	Close All Browsers

SUITE:Login on Cloud using SSO
#	Close All Browsers
	GUI::ZPECloud::Basic::Open ZPE Cloud	${ZPECLOUD_HOMEPAGE}	${BROWSER}
	GUI::ZPECloud::Basic::Wait Elements Login
	Input Text	xpath=//form//input[1]	${SSO_CLOUD_USER}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="duo_duo"]	30s
	Click Element	//*[@id="duo_duo"]
	Sleep	10
	Wait Until Page Contains Element	username	30s
	Input Text	username	${SSO_CLOUD_USER}
	Input Text	password	${SSO_CLOUD_PASSWORD}
	Click Element	login-button
	Wait Until Keyword Succeeds	30	2	GUI::ZPECloud::Basic::Wait Until Menu Appears

SUITE:Remote Access Console with SSO
	GUI::ZPECloud::Dashboard::Access::Open Tab
#	Sleep	15
	Wait Until Page Contains	${DEVICE_STATUS}	30s
	Wait Until Page Contains	${DEVICE}	30s
	GUI::ZPECloud::Basic::Click Button	console
	wait until keyword succeeds	30	5	GUI::Basic::Spinner Should Be Invisible
	Sleep	5
	Wait Until Keyword Succeeds	30	2	Switch Window	NEW
	Sleep	5
	Wait Until Page Contains Element	sso_button	30s
	Click Element	sso_button
	Sleep	10
	Switch Window	NEW
	Wait Until Page Contains Element	${CONSOLE}
	Select Frame	${CONSOLE}
	Sleep	5
	Press Keys	None	RETURN
	Press Keys	None	RETURN
	Close All Browsers

SUITE:Configure SSO - Device
	SUITE:Access Web
	SUITE:SSO configuration on the device
	SUITE:Add SSO user into device
	Close All Browsers

SUITE:SSO configuration on the device
	SUITE:Access Web	${STATUS}
	GUI::Security::Open Authentication Tab
	Sleep	5
	Click Element	ssoTable
	GUI::Basic::Spinner Should Be Invisible
	Click Element	upgrade
	GUI::Basic::Wait Until Element Is Accessible	ssoName
	Input Text	ssoName	${NAME}
	Select From List by Value	ssoStatus	enabled
	Input Text	ssoEntityid	${ENTITY_ID}
#	Select Radio Button	systemcertselection	systemcert-localcomputer
	Choose File	clientFilename	${SSO_METADATA}
	Click Button	xpath=//input[@value='Select Icon']
	Click Element	${DUO_ICON}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Open Authentication Tab
	Click Element	ssoTable
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	${NAME}
	SUITE:Add SSO user into device

SUITE:Add SSO user into device
	GUI::Security::Open Local Accounts Tab
	GUI::Security::Local Accounts::Add on Admin Group	${USER}	${SSO_CLOUD_PASSWORD}

SUITE:Delete SSO on device
	SUITE:Access Web	${STATUS}
	GUI::Security::Open Authentication Tab
	Click Element	xpath=/html/body/div[7]/div[1]/a[3]/span
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[1]/input
	Click Element	jquery=#delButton
	GUI::Basic::Spinner Should Be Invisible

SUITE:Delete User
	GUI::Security::Open Local Accounts Tab
	GUI::Basic::Wait Until Element Is Accessible	jquery=#user_namesTable
	GUI::Basic::Delete Rows In Table Containing Value	user_namesTable	${USER}
	GUI::Basic::Spinner Should Be Invisible

SUITE:Access Web
	[Arguments]	${STATUS}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Dashboard::Access::Open Tab
#	Sleep	15
	Wait Until Page Contains	${DEVICE_STATUS}	30s
	Wait Until Page Contains	${DEVICE}	30s
	GUI::ZPECloud::Basic::Click Button	web
	wait until keyword succeeds	30	5	GUI::Basic::Spinner Should Be Invisible
	Sleep	5
	Wait Until Keyword Succeeds	30	2	Switch Window	NEW
	Sleep	5
	Wait Until Keyword Succeeds	60	2	SUITE:Web Access
	Input Text	username	${USERNG}
	Input Text	password	${PWDNG}
	Click Element	login-btn
	wait until keyword succeeds	30	5	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	main_menu

SUITE:Web Access
	Sleep	5
	${LOGIN}=	Run Keyword And return Status	Page Should Contain Element	username
	Run Keyword If	${LOGIN} == False	Reload Page
	Sleep	10
	Wait Until Page Contains Element	username

SUITE:Check SSO User
	GUI::ZPECloud::Basic::Open And Login	${SUPER_EMAIL_ADDRESS}	${SUPER_PASSWORD}
	GUI::ZPECloud::Users::Open Tab
	Wait Until Page Contains Element	//*[@id="search-input"]
	Click Element	//*[@id="search-input"]
	Input Text	//*[@id="search-input"]	${SSO_CLOUD_USER}
	Click Element	//*[@id="search-input"]
	Press Keys	//*[@id="search-input"]	ENTER
	Sleep	5
	${STATUS}=	Run Keyword and Return Status	Wait Until Page Contains	No result found
	Run Keyword If	${STATUS} == True	Add User on Company
#	Sleep	10
#	${STATUS}=	Run Keyword and Return Status	Element Should Contain	//*[@id="ca-ugn-table-1"]/div/div/table/tbody/tr/td[2]/div	${SSO_CLOUD_USER}
	Run Keyword If	${STATUS} == False	Move User to Company
	Close All Browsers
#
Move User to Company
	Delete User from Company
	Add User on Company

Delete User from Company
	GUI::ZPECloud::Basic::Open And Login	${SUPER_EMAIL_ADDRESS}	${SUPER_PASSWORD}
	GUI::ZPECloud::Users::Open Tab
	Wait Until Page Contains Element	//*[@id="search-input"]
	Click Element	//*[@id="search-input"]
	Input Text	//*[@id="search-input"]	${SSO_CLOUD_USER}
	sleep	5s
	${STATUS}=	Run Keyword and Return Status	Wait Until Page Contains	${SSO_CLOUD_USER}
	Run Keyword If	${STATUS} == True	SUITE:DELETE SSO USER
#	Press Keys	//*[@id="search-input"]	ENTER
#	Sleep	5
#	${ROWNUMBER}=	Execute Javascript	return Array.from(document.getElementById('sa-users-general-table-1').getElementsByTagName('TBODY')[0].getElementsByTagName('TR')).indexOf(Array.from(document.getElementById('sa-users-general-table-1').getElementsByTagName('TBODY')[0].getElementsByTagName('TR')).filter(e => e.getElementsByTagName('TD')[1].textContent == '${SSO_CLOUD_USER}')[0])
#	Run Keyword If	${ROWNUMBER} == 0	Click Element	//*[@id="sa-users-general-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/span[1]/input
#	Click Element	//*[@id="sa-users-general-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/span[1]/input
#	Run Keyword If	${ROWNUMBER} > 0	Click Element	//*[@id="sa-users-general-table-1"]/div[1]/div/table/tbody/tr[${ROWNUMBER}]/td[1]/span/span[1]/input
#	Click Element	//*[@id="function-button-2"]

SUITE:DELETE SSO USER
	Wait Until Page Contains Element	//*[@id="sa-users-general-table-1"]/div[1]/div/table/tbody/tr[1]/td[1]/span/input	30s
	Click Element	//*[@id="sa-users-general-table-1"]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
	Wait Until Page Contains Element	//*[@id="function-button-2"]	30s
	Click Element	//*[@id="function-button-2"]
	Wait Until Page Contains	Delete User	30s
	Click Element	//*[@id="confirm-btn"]
#	Wait Until Page Contains	User deleted successfully

Add User on Company
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Users::Open Tab
	Click Element	//*[@id="function-button-0"]
	Wait Until Page Contains	Add New User
	Wait Until Page Contains Element	//*[@id="ca-usergrp-form-field-email"]
	Input Text	//*[@id="ca-usergrp-form-field-email"]	${SSO_CLOUD_USER}
	Input Text	//*[@id="ca-usergrp-form-field-first_name"]	${SSOFIRSTNAME}
	Input Text	//*[@id="ca-usergrp-form-field-last_name"]	${SSOLASTNAME}
	Input Text	//*[@id="ca-usergrp-add-dialog"]/div[2]/div/div/div/div[1]/div/div[7]/div/div/input	${NOTREALNUMBER}
	Click Element	//*[@id="ca-user-groups"]
	Click Element	xpath=//li[2]
	Press Keys	xpath=//li[contains(.,'User')]	ESC
	Sleep	5
	Click Element	//*[@id="submit-btn"]
	wait until keyword succeeds	30	5	Wait Until Page Contains	User added successfully
	Wait Until Keyword Succeeds	30	5	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Users::Open Tab
	Wait Until Page Contains Element	//*[@id="search-input"]
	Click Element	//*[@id="search-input"]
	Input Text	//*[@id="search-input"]	${SSO_CLOUD_USER}
#	Press Keys	//*[@id="search-input"]	ENTER
	Wait Until Page Contains	${SSO_CLOUD_USER}	15s

SUITE:Check That Devices::Enrolled Page Contain Online Devices
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Wait Until Page Contains	Online	15s