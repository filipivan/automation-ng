*** Settings ***
Documentation	Tests for date collection of a device under ZPE Cloud
Metadata	Version	1.0
Metadata	Executed At	${HOMEPAGE}
Metadata	Executed with	${BROWSER}
Resource	../../../../init.robot
Force Tags	GUI	ZPECLOUD	${BROWSER}	NON-CRITICAL	CELLULAR	DEVICE	PART_2
Default Tags	SIMCARD

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${REMOTEACCESS}	remote_access
${CONSOLE}	//*[@id="|nodegrid"]/div[1]/div[2]/a
${CONSOLESCREEN}	xpath=//*[@id='termwindow']
${TTYDCONSOLE}	xpath=//*[@id='termwindow']
${SHELLCOMMAND}	shell sudo su -
${ROOTPROMPT}	root@nodegrid:~#
${DATACONSUMER}	curl --interface wwan0 https://upload.wikimedia.org/wikipedia/commons/d/d4/Wikidata_Map_April_2016_Huge.png --output ./testing.png
${DATACONSUPTION}	Received
${CURRENTPERIOD}	Current Period
${LIST}	xpath=//div[@id='mui-component-select-period-id']
${CONFIRM}	//*[@id="confirm-btn"]/span[1]
${DATAUSAGE}	Data Usage
${ERROR}	Couldn't connect to server
${CELLULARCONNECTION}	cellular
${CONNECTION_TYPE}	Mobile Broadband GSM
${SIM_APN}	vzwinternet
${CONNECTIONUP}	Up
${CELLULARTABLE}	peerTable_wrapper
${CELLULARCHECKBOX}	//*[@id="cellular"]/td[1]
${UPCONNECTION}	//*[@id="nonAccessControls"]/input[3]
${DOWNCONNECTION}	//*[@id="nonAccessControls"]/input[4]
${WIRELESSMODEM}	wmodem
${INTERFACE}	cdc-wdm0
${DEVICETYPE}
${NGDATACONSUPTION}	//*[@id="S1-A|${INTERFACE}"]/td[6]
${DEVICE}	nodegrid/${SERIALNUMBER} (${DEVICETYPE})
${SELECTDEVICE}	//*[@id="mui-component-select-device-id"]
${DEVICEPATH}	xpath=//div[@id='menu-device-id']/div[3]/ul/li
${SELECTPERIOD}	xpath=//*[@id="mui-component-select-period-id"]
${CURRENTPERIODPATH}	xpath=//*[@id='menu-period-id']//li[text()='Current Period']

*** Test Cases ***
Test check Data collection with Device
	#1_down cellular connection
	SUITE:Down Connection
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Dashboard::CELLULAR_DATA::Open Tab
	Sleep	5
	Click Element	${SELECTDEVICE}
	Wait Until Element Is Visible	${DEVICEPATH}	timeout=30s
	Wait Until Element Is Enabled	${DEVICEPATH}	timeout=30s
	Click Element	${DEVICEPATH}
	Wait Until Element Is Visible	${SELECTPERIOD}	timeout=30s
	Wait Until Element Is Enabled	${SELECTPERIOD}	timeout=30s
	Click Element	${SELECTPERIOD}
	Wait Until Element Is Visible	${CURRENTPERIODPATH}	timeout=30s
	Wait Until Element Is Enabled	${CURRENTPERIODPATH}	timeout=30s
	Click Element	${CURRENTPERIODPATH}
	wait until keyword succeeds	30s	2s	click element	//*[@id="confirm-btn"]
	Sleep	5
	Wait Until Keyword Succeeds	30	5	Page Should Contain	${DATAUSAGE}
	${DATAUSEDCLOUD01}=	Get Data
	Close All Browsers
	SUITE:Access console and Spend Data
#	Sleep	20 minutes
	Wait Until Keyword Succeeds	10x	30s	SUITE:Validate Data	${DATAUSEDCLOUD01}
	[Teardown]	Close All Browsers

Test Connection by Cloud
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Network::Open Connections Tab
	SUITE:Verify if is Primary Connection
	SUITE:Up Connection
	${HASCELLULAR1}=	Run Keyword And return Status	SUITE:Verify if Device is Connect with Cellular
	Run Keyword If	${HASCELLULAR1} == True	SUITE:Access Web
	${HASCELLULAR2}=	Run Keyword And return Status	SUITE:Verify if Device is Connect with Cellular
	Run Keyword If	${HASCELLULAR2} == True	SUITE:Access Console
	[Teardown]	Close All Browsers

*** Keywords ***
SUITE:Setup
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Network::Open Connections Tab
	Sleep	5
	${status}=	Run Keyword and Return Status	Page Should Contain	Mobile Broadband GSM
	Run Keyword If	${status} == False	SUITE:Add Cellular Connection
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Security::Open Services tab
	${IS_NOTSELECTED}=	Run Keyword And return Status	Checkbox Should Not Be Selected	${REMOTEACCESS}
	Run Keyword If	${IS_NOTSELECTED} == True	Select Checkbox	${REMOTEACCESS}
	Run Keyword If	${IS_NOTSELECTED} == True	GUI::Basic::Save
	Close All Browsers
	CLI:Close Connection

SUITE:Teardown
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Security::Open Services tab
	GUI::Network::Open Connections Tab
	Click Element	${CELLULARCHECKBOX}
	GUI::Basic::Delete Rows In Table Containing Value	${CELLULARTABLE}	${CELLULARCONNECTION}	False
	Close All Browsers

SUITE:Verify if is Primary Connection
	${status}=	Run Keyword and Return Status	Page Should Contain	Mobile Broadband GSM
	Click Element	xpath=//a[@id="cellular"]
	${IS_NOTSELECTED}=	Run Keyword and Return Status	Checkbox Should Not Be Selected	primaryConn
	Run Keyword If	${IS_NOTSELECTED} == True	Select Checkbox	primaryConn
	Run Keyword If	${IS_NOTSELECTED} == True	GUI::Basic::Save
	GUI::Network::Open Connections Tab
	Sleep	10
	Should Not Be Empty	//tr[@id="cellular"]/td[7]

SUITE:Access Web
	Page Should Contain	Online
	GUI::ZPECloud::Basic::Click Button	web
	wait until keyword succeeds	30	5	GUI::Basic::Spinner Should Be Invisible
	Sleep	5
	Wait Until Keyword Succeeds	30	2	Switch Window	NEW
	Input Text	username	${USERNG}
	Input Text	password	${PWDNG}
	Click Element	login-btn
	Sleep	5
	#Switch Window	NEW
	Page Should Contain Element	main_menu

SUITE:Access Console
	Page Should Contain	Online
	GUI::ZPECloud::Basic::Click Button	console
	wait until keyword succeeds	30	5	GUI::Basic::Spinner Should Be Invisible
	Sleep	5
	Wait Until Keyword Succeeds	30	5	Switch Window	NEW
	Input Text	username	${USERNG}
	Input Text	password	${PWDNG}
	Click Element	login-btn
	Sleep	5
	Switch Window	NEW
	Wait Until Page Contains Element	${CONSOLESCREEN}
	Select Frame	${CONSOLESCREEN}

SUITE:Verify if Device is Connect with Cellular
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Dashboard::Access::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
	Press Keys	search-input	ENTER
	Sleep	5
	Wait until Page Contains	${SERIALNUMBER}
	Page Should Contain	cellular

SUITE:Access console and Spend Data
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Network::Open Connections Tab
	SUITE:Up Connection
	GUI:Access::Open Table tab
	Click Element	${CONSOLE}
	Sleep	5
	Switch Window	NEW
	Select Frame	${TTYDCONSOLE}
	Sleep	5
	Press Keys	None	RETURN
	sleep	2
	${OUTPUT}=	SUITE:Console Command Output	${SHELLCOMMAND}
	Should Contain	${OUTPUT}	${ROOTPROMPT}
	Press Keys	None	RETURN
	${OUTPUT}=	SUITE:Console Command Output	${DATACONSUMER}
	Should Contain	${OUTPUT}	${DATACONSUPTION}
	Should Not Contain	${OUTPUT}	${ERROR}
	${OUTPUT}=	SUITE:Console Command Output	${DATACONSUMER}
	Should Contain	${OUTPUT}	${DATACONSUPTION}
	Should Not Contain	${OUTPUT}	${ERROR}
	${OUTPUT}=	SUITE:Console Command Output	${DATACONSUMER}
	Should Contain	${OUTPUT}	${DATACONSUPTION}
	Should Not Contain	${OUTPUT}	${ERROR}
	Sleep	5 minutes
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Network::Open Connections Tab
#1_down cellular connection
	SUITE:Down Connection

SUITE:Console Command Output
	[Arguments]	${COMMAND}
	${CHECK_TAB}=	Run Keyword And Return Status	Should Not Contain	${COMMAND}	TAB
	Run Keyword If	${CHECK_TAB}	Press Keys	None	${COMMAND}	RETURN
	...	ELSE	Press Keys	None	${COMMAND}
	Sleep	1
	${CONSOLE_CONTENT}=	Execute JavaScript	term.selectAll(); return term.getSelection().trim();
	Should Not Contain	${CONSOLE_CONTENT}	[error.connection.failure] Could not establish a connection to device
	${OUTPUT}=	Output From Last Command	${CONSOLE_CONTENT}
	[Return]	${OUTPUT}

SUITE:Validate Data
	[Arguments]	${DATAUSEDCLOUD01}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Dashboard::CELLULAR_DATA::Open Tab
	Sleep	5
	Click Element	${SELECTDEVICE}
	Wait Until Element Is Visible	${DEVICEPATH}	timeout=30s
	Wait Until Element Is Enabled	${DEVICEPATH}	timeout=30s
	Click Element	${DEVICEPATH}
	Wait Until Element Is Visible	${SELECTPERIOD}	timeout=30s
	Wait Until Element Is Enabled	${SELECTPERIOD}	timeout=30s
	Click Element	${SELECTPERIOD}
	Wait Until Element Is Visible	${CURRENTPERIODPATH}	timeout=30s
	Wait Until Element Is Enabled	${CURRENTPERIODPATH}	timeout=30s
	Click Element	${CURRENTPERIODPATH}
	wait until keyword succeeds	30s	2s	click element	xpath=//button[@id='confirm-btn']
	Sleep	5
	Wait Until Keyword Succeeds	30	5	Page Should Contain	${DATAUSAGE}
	${DATAUSEDCLOUD02}=	Get Data
	Should Be True	'${DATAUSEDCLOUD02}' > '${DATAUSEDCLOUD01}'
	GUI::ZPECloud::Basic::Logout and Close All	False

SUITE:Add Cellular Connection
	Click Element	addButton
	GUI::Basic::Wait Until Element Is Accessible	connType
	Select From List By Label	//*[@id="connType"]	${CONNECTION_TYPE}
	${STATUS}	Evaluate NG Versions: ${NGVERSION} > 4.2
	Run Keyword If	${STATUS}	Select From List By Value	connItfName	cdc-wdm0
	${STATUS}	Evaluate NG Versions: ${NGVERSION} < 5.0
	Run Keyword If	${STATUS}	Select From List By Value	connIeth	cdc-wdm0
	Input Text	connName	${CELLULARCONNECTION}
	Input Text	mobileAPN	${SIM_APN}
	${CHECK}=	Run Keyword and Return Status	Checkbox Should Be Selected	connAuto
	Run Keyword If	${CHECK} == False	Click Element	connAuto
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Down Connection
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Network::Open Connections Tab
	Click Element	${CELLULARCHECKBOX}
	Sleep	5
	Click Button	${DOWNCONNECTION}
	Sleep	5
	GUI::Basic::Spinner Should Be Invisible

SUITE:Up Connection
	${CONTAIN}=	Run Keyword And return Status	Table Row Should Contain	${CELLULARTABLE}	${CELLULARCONNECTION}	${CONNECTIONUP}
	Run Keyword If	${CONTAIN} == False	Click Element	${CELLULARCHECKBOX}
	Run Keyword If	${CONTAIN} == False	Click Element	${UPCONNECTION}
	GUI::Basic::Spinner Should Be Invisible