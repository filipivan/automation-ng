*** Settings ***
Documentation	TEST FOR CHECKING DEVICE HOSTNAME ON MAP
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed With	${BROWSER}
Resource	../../../../init.robot
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	NON-CRITICAL	DEVICE	PART_2
Default Tags	EXCLUDEIN3_2	DEVICE_HOSTNAME_ON_MAP

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL}	${EMAIL_ADDRESS}
${PWD}	${PASSWORD}
${USERNG}	${USERNAMENG}
${PWDNG}	${PASSWORDNG}
${DEVICE_HOSTNAME}	nodegrid
${DEVICE_COORDINATE}	37.5485,21.9886
${NEWHOSTNAME}	Test
${SITE_NAME}	ZPE_SITE
${EDITED_SITE_NAME}	ZPE_SITE_EDITED
${SITE_ADDRESS}	Rua 7 de Setembro, 1678, Blumenau-SC
${SITE_LATITUDE}	40.777800400000004
${SITE_LONGITUDE}	8.921996955790174
${SITE_ADDED_MESSAGE}	Site added successfully
${SITE_EDITED_MESSAGE}	Site details edited successfully
${SITE_DELETED_MESSAGE}	Site deleted successfully
${DEVICE_ADDED_TO_SITE_MESSAGE}	Assigned to Site

*** Test Cases ***
Test device located on Map When it's Corordinate are not configured on device
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Basic::System::Preferences::Open Tab
	Wait Until Page Contains Element	jquery=#coordinates	15s
	Press Keys	jquery=#coordinates	CTRL+a+BACKSPACE
	Clear Element Text	jquery=#coordinates
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save If Configuration Changed
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	id=hostname
	${DEVICE_HOSTNAME}=	Get value	id=hostname
	Set Suite Variable	${DEVICE_HOSTNAME}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	Wait Until Page Contains Element	//*[@id="ca-site-checkbox"]	15s
	${IS_SELECTED_SITE}=	Run Keyword And return Status	Checkbox Should Be Selected	//*[@id="ca-site-checkbox"]
	Run Keyword If	${IS_SELECTED_SITE}	Click Element	//*[@id="ca-site-checkbox"]
	${IS_SELECTED_DEVICE}=	Run Keyword And return Status	Checkbox Should Be Selected	//*[@id="ca-device-checkbox"]
	Run Keyword If	not ${IS_SELECTED_DEVICE}	Click Element	//*[@id="ca-device-checkbox"]
	Wait Until Keyword Succeeds	2m	3s	SUITE:Locate_Device
	Wait Until Page Contains	Hostname: ${DEVICE_HOSTNAME}	30s
	[Teardown]	Close All Browsers

Test device not located when configured an Coordinate On Device And Disable Device Checkebox On Map
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Basic::System::Preferences::Open Tab
	SUITE:Configure Coordinate On Device
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	id=hostname
	${DEVICE_HOSTNAME}=	Get value	id=hostname
	Set Suite Variable	${DEVICE_HOSTNAME}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	Wait Until Page Contains Element	//*[@id="ca-site-checkbox"]	15s
	${IS_SELECTED_SITE}=	Run Keyword And return Status	Checkbox Should Be Selected	//*[@id="ca-site-checkbox"]
	Run Keyword If	${IS_SELECTED_SITE}	Click Element	//*[@id="ca-site-checkbox"]
	${IS_SELECTED_DEVICE}=	Run Keyword And return Status	Checkbox Should Be Selected	//*[@id="ca-device-checkbox"]
	Run Keyword If	not ${IS_SELECTED_DEVICE}	Click Element	//*[@id="ca-device-checkbox"]
	Wait Until Keyword Succeeds	1m	3s	Wait Until Page Does Not Contain Element	//*[@id="ca-dashboard-map-wrapper"]/div[1]/div[1]/div[1]/div[4]/img
	[Teardown]	Close All Browsers

Test device hostname on map
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Basic::System::Preferences::Open Tab
	SUITE:Configure Coordinate On Device
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	id=hostname
	${DEVICE_HOSTNAME}=	Get value	id=hostname
	Set Suite Variable	${DEVICE_HOSTNAME}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	Wait Until Page Contains Element	//*[@id="ca-site-checkbox"]	15s
	${IS_SELECTED_SITE}=	Run Keyword And return Status	Checkbox Should Be Selected	//*[@id="ca-site-checkbox"]
	Run Keyword If	${IS_SELECTED_SITE}	Click Element	//*[@id="ca-site-checkbox"]
	${IS_SELECTED_DEVICE}=	Run Keyword And return Status	Checkbox Should Be Selected	//*[@id="ca-device-checkbox"]
	Run Keyword If	not ${IS_SELECTED_DEVICE}	Click Element	//*[@id="ca-device-checkbox"]
	Wait Until Keyword Succeeds	2m	3s	SUITE:Locate_Device
	Wait Until Page Contains	Hostname: ${DEVICE_HOSTNAME}	30s
	[Teardown]	Close All Browsers

Change Hostname of device and Check it under Map
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Basic::System::Preferences::Open Tab
	SUITE:Configure Coordinate On Device
	SUITE:Set hostname as 'test'
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Basic::System::Preferences::Open Tab
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	id=hostname
	${DEVICE_HOSTNAME}=	Get value	id=hostname
	Set Suite Variable	${DEVICE_HOSTNAME}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	Wait Until Page Contains Element	//*[@id="ca-site-checkbox"]	15s
	${IS_SELECTED_SITE}=	Run Keyword And return Status	Checkbox Should Be Selected	//*[@id="ca-site-checkbox"]
	Run Keyword If	${IS_SELECTED_SITE}	Click Element	//*[@id="ca-site-checkbox"]
	${IS_SELECTED_DEVICE}=	Run Keyword And return Status	Checkbox Should Be Selected	//*[@id="ca-device-checkbox"]
	Run Keyword If	not ${IS_SELECTED_DEVICE}	Click Element	//*[@id="ca-device-checkbox"]
	Wait Until Keyword Succeeds	2m	3s	SUITE:Locate_Device
	Wait Until Page Contains	Hostname: ${DEVICE_HOSTNAME}	30s
	[Teardown]	SUITE:Teardown

Move Device to Available Tab and Change hostname and check on Map
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Basic::System::Preferences::Open Tab
	SUITE:Configure Coordinate On Device
	SUITE:Set hostname as 'test'
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	id=hostname
	${DEVICE_HOSTNAME}=	Get value	id=hostname
	Set Suite Variable	${DEVICE_HOSTNAME}
	GUI::ZPECloud::Move Device to Available
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	Wait Until Page Contains Element	//*[@id="ca-site-checkbox"]	15s
	${IS_SELECTED_SITE}=	Run Keyword And return Status	Checkbox Should Be Selected	//*[@id="ca-site-checkbox"]
	Run Keyword If	${IS_SELECTED_SITE}	Click Element	//*[@id="ca-site-checkbox"]
	${IS_SELECTED_DEVICE}=	Run Keyword And return Status	Checkbox Should Be Selected	//*[@id="ca-device-checkbox"]
	Run Keyword If	not ${IS_SELECTED_DEVICE}	Click Element	//*[@id="ca-device-checkbox"]
	Wait Until Keyword Succeeds	2m	3s	SUITE:Device_Not_Located
	Wait Until Page Does Not Contain Element	Hostname: ${NEWHOSTNAME}	30s
	GUI::Cloud::Move from available to enrolled
	[Teardown]	SUITE:Teardown

Add Device On Site And check Information On Dashboard::Map
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	${STATUS}=	Run Keyword and Return Status	GUI::ZPECloud::Devices::Enrolled::Page Should Contain Device With Online Status
	Run Keyword If	${STATUS} == False	CLI::ZPECloud::Restart agent process via root
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Basic::System::Preferences::Open Tab
	SUITE:Configure Coordinate On Device
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	id=hostname
	${DEVICE_HOSTNAME}=	Get value	id=hostname
	Set Suite Variable	${DEVICE_HOSTNAME}
	SUITE:Add device on site and check site Info on device::enrolled page
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	Wait Until Page Contains Element	//*[@id="ca-site-checkbox"]	15s
	${IS_SELECTED_SITE}=	Run Keyword And return Status	Checkbox Should Be Selected	//*[@id="ca-site-checkbox"]
	Run Keyword If	${IS_SELECTED_SITE}	Click Element	//*[@id="ca-site-checkbox"]
	${IS_SELECTED_DEVICE}=	Run Keyword And return Status	Checkbox Should Be Selected	//*[@id="ca-device-checkbox"]
	Run Keyword If	not ${IS_SELECTED_DEVICE}	Click Element	//*[@id="ca-device-checkbox"]
	Wait Until Keyword Succeeds	2m	3s	SUITE:Locate_Device
	Wait Until Page Contains	Site Name: ${SITE_NAME}	10s
	GUI::ZPECloud::Sites::Open Tab
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${SITE_NAME}
	${HASSITE}=	Run Keyword and Return Status	Wait Until Page Contains	${SITE_NAME}	15s
	Run Keyword if	${HASSITE} == True	SUITE:Delete Site

*** Keywords ***
SUITE:Setup
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	#>= 5.4 Has hostpeer
	${STATUS}	Evaluate NG Versions: ${NGVERSION} >= 5.4
	Run Keyword If	${STATUS}	GUI::ZPECloud::Move Device To Available	${SERIALNUMBERPEER}

SUITE:Teardown
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud
	SUITE:Set hostname as 'nodegrid'
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Preferences::Open Tab
	Wait Until Page Contains Element	jquery=#coordinates	15s
	Press Keys	jquery=#coordinates	CTRL+a+BACKSPACE
	Clear Element Text	jquery=#coordinates
	GUI::Basic::Spinner Should Be Invisible
	${SAVE_BUTTON}=	Run Keyword And return Status	Wait Until Element Is Enabled	//*[@id="saveButton"]	15s
	Run Keyword If	${SAVE_BUTTON} == True	GUI::Basic::Save
	${STATUS}	Evaluate NG Versions: ${NGVERSION} >= 5.4
	Run Keyword If	${STATUS}	GUI::Cloud::Move from available to enrolled	EMAIL=${EMAIL_ADDRESS}	PWD=${PASSWORD}	DIFF_SERIALNUMBER=${SERIALNUMBERPEER}
	Close All Browsers

SUITE:Configure Coordinate On Device
	GUI::Basic::Spinner Should Be Invisible
	Input Text	jquery=#coordinates	${DEVICE_COORDINATE}
	GUI::Basic::Spinner Should Be Invisible
	${SAVE_BUTTON}=	Run Keyword And return Status	Wait Until Element Is Enabled	//*[@id="saveButton"]	15s
	Run Keyword If	${SAVE_BUTTON} == True	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

SUITE:Enroll Device on Cloud
	${STATUS}=	Run Keyword and Return Status	GUI::Cloud::Verify if device is enrolled and Online
	Run Keyword If	'${STATUS}'== 'False'	Run Keywords
	...	GUI::Cloud::Get Customercode and Enrollmentkey
	...	GUI::Cloud::Enroll Device on Cloud

SUITE:Verify if Device is Enrolled
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Devices::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Wait until Page Contains	${SERIALNUMBER}

SUITE:Verify if Device is Available
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Devices::Available::Open Tab
	Sleep	5
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr[1]/td[1]/span/input	30s
	Wait until Page Contains	${SERIALNUMBER}

SUITE:Locate_Device
	GUI::ZPECloud::Dashboard::Map::Open Tab
	Reload Page
	Wait Until Page Contains Element	//*[@id="ca-dashboard-map-wrapper"]/div[1]/div[1]/div[1]/div[4]/img	1m
	Mouse Over	//*[@id="ca-dashboard-map-wrapper"]/div[1]/div[1]/div[1]/div[4]/img

SUITE:Device_Not_Located
	GUI::ZPECloud::Dashboard::Map::Open Tab
	Reload Page
	Wait Until Page Does Not Contain Element	//*[@id="ca-dashboard-map-wrapper"]/div[1]/div[1]/div[1]/div[4]/img	30s

SUITE:Set hostname as 'test'
	CLI:Open	${USERNAMENG}	${PASSWORDNG}
	CLI:Enter Path	/settings/network_settings/
	CLI:Set Field	hostname	${NEWHOSTNAME}
	CLI:Commit
	CLI:Close Connection

SUITE:Set hostname as 'nodegrid'
	CLI:Open	${USERNAMENG}	${PASSWORDNG}
	CLI:Enter Path	/settings/network_settings/
	CLI:Set Field	hostname	${DEVICE_HOSTNAME}
	CLI:Commit
	CLI:Close Connection

SUITE:Add Site
	wait until keyword succeeds	30	5	SUITE:Click On Add Site Tab
	Wait Until Page Contains Element	xpath=//input[@name='name']	15s
	Input Text	xpath=//input[@name='name']	${SITE_NAME}
	Input Text	xpath=//input[@name='address']	${SITE_ADDRESS}
	Input Text	xpath=//input[@name='latitude']	${SITE_LATITUDE}
	Input Text	xpath=//input[@name='longitude']	${SITE_LONGITUDE}
	Wait Until Keyword Succeeds	30	6	Click Element	xpath=//button[@id='submit-btn']
	Wait Until Page Contains	${SITE_ADDED_MESSAGE}	1m

SUITE:Delete Site
	Wait Until Page Contains Element	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	Click Element	//*[@id="ca-sg-table-1"]/div/div/table/tbody/tr/td[1]/span/input
	Wait Until Page Contains Element	xpath=//button[contains(.,'Delete')]	30s
	Click Element	xpath=//button[contains(.,'Delete')]
	Wait Until Page Contains Element	xpath=//button[@id='confirm-btn']	15s
	Click Element	xpath=//button[@id='confirm-btn']
#	Wait Until Page Contains	${SITE_DELETED_MESSAGE}	15s

SUITE:Click On "Add On Site" Tab
	GUI::ZPECloud::Sites::Devices::Open Tab
	Wait Until Page Contains Element	search-input	30s
	Clear Element Text	search-input
	Press Keys	search-input	CTRL+a+BACKSPACE
	sleep	5s
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-sd-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Click Element	//*[@id="ca-sd-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Wait Until Page Contains Element	//*[@id="function-button-0"]	30s
	Click Element	//*[@id="function-button-0"]
	Wait Until Page Contains Element	search-input	30s
	Input Text	search-input	${SITE_NAME}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="ca-sd-table-2"]/div/div/table/tbody/tr[1]/td[1]/span/input	30s
	Click Element	//*[@id="ca-sd-table-2"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Wait Until Page Contains Element	//*[@id="function-button-1"]	30s
	Click Element	//*[@id="function-button-1"]

SUITE:Add Device On Site
	wait until keyword succeeds	30	5	SUITE:Click On "Add On Site" Tab
	Wait Until Page Contains	${DEVICE_ADDED_TO_SITE_MESSAGE}	1m

SUITE:Click On Add Site Tab
	Clear Element Text	search-input
	Press Keys	search-input	CTRL+a+BACKSPACE
	sleep	5s
	Wait Until Page Contains Element	//*[@id="function-button-0"]	30s
	Click Element	//*[@id="function-button-0"]
	sleep	5s

SUITE:Add device on site and check site Info on device::enrolled page
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Sites::Open Tab
	Wait Until Page Contains Element	search-input
	Input Text	search-input	${SITE_NAME}
	${HASSITE}=	Run Keyword and Return Status	Wait Until Page Contains	${SITE_NAME}	15s
	Run Keyword if	${HASSITE} == True	SUITE:Delete Site
	SUITE:Add Site
	SUITE:Add Device On Site
	GUI::ZPECloud::Devices::Open Tab
	Wait Until Page Contains Element	search-input	30s
	Input Text	search-input	${SERIALNUMBER}
	Sleep	5
	Wait Until Element Contains	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[8]	${SITE_NAME}