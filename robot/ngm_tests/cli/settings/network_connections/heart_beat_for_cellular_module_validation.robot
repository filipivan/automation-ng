*** Settings ***
Resource	../../init.robot
Documentation	Tests for heartbeat for cellular module validation
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	 EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	NON-CRITICAL
Default Tags	CLI	SSH

Suite Setup 	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CELLULAR_CONNECTION}	GSM_CELLULAR_CONNECTION
${TYPE} 	mobile_broadband_gsm
${STRONG_PASSWORD}	${QA_PASSWORD}
@{VALID_IPS}    192.168.15.60  0.0.0.0  8.8.8.8
@{INVALID_IPS}  -1.-1.-1.-1  0,5.0,5.0,5.0,5  0,00,00,00  ${EMPTY}
@{VALID_INTERVALS}  1   3   10  24
@{INVALID_INTERVALS}    25  0
${INTERVAL_ERROR_MSG}   Error: Interval hour must be between 1 and 24

*** Test Cases ***
Test available fields to set for healthcheck
    Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
    Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
    CLI:Enter Path	/settings/network_connections/
    CLI:Add
    CLI:Set	name=${CELLULAR_CONNECTION} type=${TYPE}
    CLI:Set	ethernet_interface=${MODEM_INFO["interface"]} ipv4_mode=dhcp
    Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Set	sim-1_apn_configuration=manual
    CLI:Set	ipv6_mode=no_ipv6_address sim-1_access_point_name=${MODEM_INFO["apn"]}
    CLI:Test Set Available Fields    enable_connection_health_monitoring
    CLI:Set	 enable_connection_health_monitoring=yes
    CLI:Test Set Available Fields  ensure_connection_is_up  IP Address  Interval (hours)
    CLI:Commit

Test valid values for intervals
    Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
    Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
    CLI:Enter Path	/settings/network_connections/${CELLULAR_CONNECTION}
    CLI:Set    ip_address=8.8.8.8
    FOR		    ${VALID_INTERVAL}  IN  @{VALID_INTERVALS}
        CLI:Set  interval=${VALID_INTERVAL}
        ${OUTPUT}=   CLI:Commit
        Should Not Contain    ${OUTPUT}   ${INTERVAL_ERROR_MSG}
    END

Test valid values for ip Addresses
    Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
    Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
    CLI:Switch Connection    default
    CLI:Enter Path  /settings/network_connections/${CELLULAR_CONNECTION}
    CLI:Set  interval=24
    FOR		    ${VALID_IP}  IN  @{VALID_IPS}
        CLI:Set    ip_address=${VALID_IP}
        ${OUTPUT}=   CLI:Commit
        Should Not Contain    ${OUTPUT}    Invalid IP address.
    END

Test Invalid values for ip Addresses
    Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
    Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
    CLI:Enter Path  /settings/network_connections/${CELLULAR_CONNECTION}
    CLI:Set  interval=24
    FOR		    ${INVALID_IP}  IN  @{INVALID_IPS}
        Write    set ip_address=${INVALID_IP}
        ${OUTPUT}=     Read Until Prompt
        Should Contain    ${OUTPUT}  enable_connection_health_monitoring: Invalid IP address.
    END
    CLI:Cancel  Raw

Test Invalid values for intervals
    Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
    Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
    CLI:Enter Path	/settings/network_connections/${CELLULAR_CONNECTION}
    CLI:Set    ip_address=8.8.8.8
    FOR		    ${INVALID_INTERVAL}  IN  @{INVALID_INTERVALS}
       Write    set interval=${INVALID_INTERVAL}
       ${OUTPUT}=     Read Until Prompt
       Should Contain    ${OUTPUT}    ${INTERVAL_ERROR_MSG}
    END
    CLI:Cancel  Raw

*** Keywords ***
SUITE:Setup
    CLI:Open
    CLI:Switch Connection	default
    ${HAS_WMODEM_SUPPORT}=	CLI:Has Wireless Modem Support
    Set Suite Variable	${HAS_WMODEM_SUPPORT}
    Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
    ${HAS_WMODEM}=	CLI:Has Wireless Modem With SIM Card
    Set Suite Variable	${HAS_WMODEM}	${HAS_WMODEM}
    Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
    ${HAS_WMODEM}	${MODEM_INFO}=	CLI:Get Wireless Modems SIM Card Info	0
    Set Suite Variable	${HAS_WMODEM}
    Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
    Set Suite Variable	${MODEM_INFO}
    CLI:Delete Network Connections	${CELLULAR_CONNECTION}
    CLI:Change User Password From Shell	root	${STRONG_PASSWORD}	${ROOT_PASSWORD}
    CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${STRONG_PASSWORD}	${STRONG_PASSWORD}
	CLI:Open	${DEFAULT_USERNAME}	${STRONG_PASSWORD}

SUITE:Teardown
	Skip if	not ${HAS_WMODEM_SUPPORT}
	Skip if	not ${HAS_WMODEM}
	Run Keywords	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}	${STRONG_PASSWORD}	AND
    ...	CLI:Change User Password From Shell	root	${ROOT_PASSWORD}	${STRONG_PASSWORD}
    CLI:Open
    CLI:Delete Network Connections	${CELLULAR_CONNECTION}
    CLI:Close Connection