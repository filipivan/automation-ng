*** Settings ***
Resource	../../init.robot
Documentation	Tests for heartbeat for cellular module functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	 EXCLUDEIN3_2	EXCLUDEIN4_2
Default Tags	CLI	SSH

Suite Setup 	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${GSM_CONNECTION}	GSM_CELLULAR_CONNECTION
${TYPE} 	mobile_broadband_gsm

*** Test Cases ***
Test case to create new connection with GPS
    Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
    CLI:Enter Path	/settings/network_connections/
    CLI:Add
    CLI:Set	name=${GSM_CONNECTION} type=${TYPE}
    CLI:Test Set Available Fields  enable_global_positioning_system
    CLI:Set  enable_global_positioning_system=yes
    CLI:Test Set Available Fields  polling_time  gps_antenna
    CLI:Set   polling_time=1
    CLI:Commit
    ${WIRELESS_MODEM}	SUITE:Check Wifi Modem
    ${WIRELESS_MODEM}	Run Keyword If	${WIRELESS_MODEM}	SUITE:Check Modem and Set Interface
    Run Keyword If	${WIRELESS_MODEM} and '${NGVERSION}' >= '5.8'	SUITE:Set Allowed And Prefered Modes
    Run Keyword If	${WIRELESS_MODEM}	SUITE:Set Antenna Type	dedicated_gps
    Run Keyword If	${WIRELESS_MODEM}	SUITE:Set Antenna Type	passive_gps
    SUITE:Set Antenna Type	shared_diversity

*** Keywords ***
SUITE:Setup
    CLI:Open
    CLI:Switch Connection	default
    ${HAS_WMODEM_SUPPORT}=	CLI:Has Wireless Modem Support
    Set Suite Variable	${HAS_WMODEM_SUPPORT}
    Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system

SUITE:Teardown
    CLI:Open
    CLI:Delete Network Connections	${GSM_CONNECTION}
    CLI:Close Connection

SUITE:Set Antenna Type
	[arguments]	${GPS_ANTENNA_TYPE}
	CLI:Enter Path	/settings/network_connections/${GSM_CONNECTION}
	CLI:Set  gps_antenna=${GPS_ANTENNA_TYPE}
	${OUTPUT}=   CLI:Commit
	Should Not Contain    ${OUTPUT}   ERROR

SUITE:Check Wifi Modem
	CLI:Enter Path	/system/wireless_modem
	${OUTPUT}	CLI:Ls	/system/wireless_modem
	${OUTPUT}	Remove String	${OUTPUT}	[admin@nodegrid wireless_modem]#
	${OUTPUT}	Remove String Using Regexp	${OUTPUT}	\r|\n
	${WIRELESS_MODEM}	Set Variable	"${OUTPUT}"!="${EMPTY}"
	[return]	${WIRELESS_MODEM}

SUITE:Check Modem and Set Interface
	CLI:Connect As Root
	@{GSM_INTERFACE}	SUITE:Get GSM Interfaces
	${ETHERNET_INTERFACE}	Set Variable	${EMPTY}
	FOR	${INTERFACE}	IN	@{GSM_INTERFACE}
		${MODEM}=	CLI:Write	/usr/bin/qmicli -d /dev/${INTERFACE} -p --dms-get-model | grep Model | cut -d "'" -f 2	user=Yes
		${MODEM}	Run Keyword And Return Status	Should Not Contain	${MODEM}	EM9191
		${ETHERNET_INTERFACE}	Run Keyword If	${MODEM}	Set Variable	${INTERFACE}
		Exit For Loop IF	${MODEM}
	END
	Switch Connection	default
	IF	${MODEM}
		CLI:Enter Path	/settings/network_connections/${GSM_CONNECTION}
		CLI:Set	ethernet_interface=${ETHERNET_INTERFACE}
		CLI:Commit
	END
	[Return]	${MODEM}

SUITE:Get GSM Interfaces
	${OUTPUT}	CLI:Write	nmcli d | grep [[:space:]]gsm[[:space:]] | cut -d " " -f 1	no
	@{OUTPUT}	Split to Lines	${OUTPUT}
	[return]	@{OUTPUT}

SUITE:Set Allowed And Prefered Modes
	CLI:Enter Path  /settings/network_connections/${GSM_CONNECTION}
	CLI:Set	sim-1_allowed_modes=3g sim-1_preferred_mode=3g
	CLI:Commit