*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Cellular failover feature through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	NEED-REVIEW	NON-CRITICAL
...	DEPENDENCE_MODEM
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CONNECTION}	test-connection
${STRONG_PASSWORD}	${QA_PASSWORD}

*** Test Cases ***
Test invalid values for field=sim-1_phone_number
	Skip If	'${NGVERSION}' == '3.2'	Field not implemented in 3.2
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION}
	CLI:Set	type=mobile_broadband_gsm
	CLI:Test Set Field Invalid Options	sim-1_phone_number	${WORD}	Error: sim-1_phone_number: This field contains invalid characters.	yes
	CLI:Test Set Field Invalid Options	sim-1_phone_number	${POINTS}	Error: sim-1_phone_number: This field contains invalid characters.	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=sim-1_personal_identification_number
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION}
	CLI:Set	type=mobile_broadband_gsm
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	sim-1_personal_identification_number	${WORD}	Error: sim-1_personal_identification_number: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	personal_identification_number	${WORD}	Error: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	sim-1_personal_identification_number	${POINTS}	Error: sim-1_personal_identification_number: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	personal_identification_number	${POINTS}	Error: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	sim-1_personal_identification_number	${EXCEEDED}	Error: sim-1_personal_identification_number: Exceeded the maximum size for this field.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	personal_identification_number	${EXCEEDED}	Error: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	${HAS_WMODEM_SUPPORT}=	CLI:Has Wireless Modem Support
	Set Suite Variable	${HAS_WMODEM_SUPPORT}
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	${HAS_WMODEM}=	CLI:Has Wireless Modem With SIM Card
	Set Suite Variable	${HAS_WMODEM}	${HAS_WMODEM}
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	CLI:Enter Path	/settings/network_connections
	CLI:Delete If Exists	${CONNECTION}
	CLI:Change User Password From Shell	root	${STRONG_PASSWORD}	${ROOT_PASSWORD}
	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${STRONG_PASSWORD}	${STRONG_PASSWORD}
	Wait Until Keyword Succeeds	15s	5s	SUITE:Check Modem Up
	CLI:Switch Connection	default

SUITE:Teardown
	Skip if	not ${HAS_WMODEM_SUPPORT}
	Skip if	not ${HAS_WMODEM}
	Run Keywords	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}	${STRONG_PASSWORD}	AND
	...	CLI:Change User Password From Shell	root	${ROOT_PASSWORD}	${STRONG_PASSWORD}
	CLI:Open
	CLI:Enter Path	/settings/network_connections
	CLI:Delete If Exists	${CONNECTION}
	CLI:Close Connection

SUITE:Check Modem Up
	CLI:Open	root	${STRONG_PASSWORD}	root_connection
	${GSM_INTERFACE}	SUITE:Get GSM Interfaces
	FOR	${INTERFACE}	IN	@{GSM_INTERFACE}
		${OUTPUT}=	CLI:Write	/usr/bin/qmicli -d /dev/${INTERFACE} -p --dms-get-model | grep Model | cut -d "'" -f 2	user=Yes
		Should Contain	${OUTPUT}	EM7565
	END
	CLI:Close Connection	Yes

SUITE:Get GSM Interfaces
	${OUTPUT}	CLI:Write	nmcli d | grep [[:space:]]gsm[[:space:]] | cut -d " " -f 1	no	yes
	${OUTPUT}	Split to Lines	${OUTPUT}
	[return]	${OUTPUT}

