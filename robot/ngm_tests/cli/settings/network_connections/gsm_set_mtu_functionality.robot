*** Settings ***
Resource	../../init.robot
Documentation	Test set MTU value on GSM connection through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	DEPENDENCE_MODEM	NON-CRITICAL
Default Tags	CLI	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${GSM_NAME}	${GSM_CELLULAR_CONNECTION}
${GSM_CONN_TYPE}	${GSM_TYPE}
${STRONG_PASSWORD}	${QA_PASSWORD}

${CUSTOM_MTU}	1350
${ICMP_HEADER_SIZE}	8
${DEFAULT_IP_HEADER_SIZE}	20
${FULL_HEADER_SIZE}	${${ICMP_HEADER_SIZE}+${DEFAULT_IP_HEADER_SIZE}}

${EQUALS_DATA_SIZE}	${${CUSTOM_MTU}-${FULL_HEADER_SIZE}}
${LONGER_DATA_SIZE}	${${EQUALS_DATA_SIZE}+1}
${SMALLER_DATA_SIZE}	${${EQUALS_DATA_SIZE}-1}

${DESTINATION_ADDRESS}	outlook.com

*** Test Cases ***
Test Ping With Longer Packet Than MTU Value
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	Switch Connection	default
	SUITE:Test Ping With Custom Data Size For MTU Value	${LONGER_DATA_SIZE}

Test Ping With Smaller Packet Than MTU Value
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	Switch Connection	default
	SUITE:Test Ping With Custom Data Size For MTU Value	${SMALLER_DATA_SIZE}

Test Ping With Packet Of Same Size As MTU Value
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	Switch Connection	default
	SUITE:Test Ping With Custom Data Size For MTU Value	${EQUALS_DATA_SIZE}

*** Keywords ***
SUITE:Setup
	CLI:Open
	${HAS_WMODEM_SUPPORT}=	CLI:Has Wireless Modem Support
	Set Suite Variable	${HAS_WMODEM_SUPPORT}
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	${HAS_WMODEM}=	CLI:Has Wireless Modem With SIM Card
	Set Suite Variable	${HAS_WMODEM}
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available

	${HAS_WMODEM}	${MODEM_INFO}	CLI:Get Wireless Modems SIM Card Info	0
	Set Suite Variable	${MODEM_INFO}
	CLI:Change User Password From Shell	root	${STRONG_PASSWORD}	${ROOT_PASSWORD}
	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${STRONG_PASSWORD}	${STRONG_PASSWORD}

	Switch Connection	default
	SUITE:Add GSM
	SUITE:Set GSM Connection MTU	${CUSTOM_MTU}

SUITE:Teardown
	CLI:Close Connection
	Skip If	not ${HAS_WMODEM_SUPPORT}
	Skip If	not ${HAS_WMODEM}
	CLI:Open	PASSWORD=${STRONG_PASSWORD}
	CLI:Delete Network Connections	${GSM_NAME}
	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}	${STRONG_PASSWORD}
	CLI:Change User Password From Shell	root	${ROOT_PASSWORD}	${STRONG_PASSWORD}
	CLI:Close Connection

SUITE:Add GSM
	CLI:Enter Path	/settings/network_connections
	CLI:Add
	CLI:Set	name=${GSM_NAME} type=${GSM_CONN_TYPE}
	CLI:Set	ethernet_interface=${MODEM_INFO["interface"]} ipv4_mode=dhcp ipv6_mode=no_ipv6_address
	Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Set	sim-1_apn_configuration=manual
	CLI:Set	sim-1_access_point_name=${MODEM_INFO["apn"]}
	CLI:Commit

SUITE:Set GSM Connection MTU
	[Arguments]	${MTU_VALUE}
	CLI:Enter Path	/settings/network_connections/${GSM_NAME}
	CLI:Set	sim-1_mtu=${MTU_VALUE}
	CLI:Commit

SUITE:Test Ping With Custom Data Size For MTU Value
	[Documentation]  Test ping other ip address from current nodegrid (should be
	...	connected as admin)
	[Arguments]	${PACKET_SIZE}	${MTU_VALUE}=${CUSTOM_MTU}	${IP_ADDRESS}=${DESTINATION_ADDRESS}	${NUM_PACKETS}=1	${TIMEOUT}=15
	Set Client Configuration	prompt=~$
	CLI:Write	shell
	Write	sudo ping ${IP_ADDRESS} -I ${MODEM_INFO["shell interface"]} -c ${NUM_PACKETS} -M do -s ${PACKET_SIZE} -W ${TIMEOUT}
	${OUTPUT}=	Read Until Prompt
	Log To Console	\n${OUTPUT}\n
	Set Client Configuration	prompt=]#
	CLI:Write	exit
	${RECEIVED_STATUS}=	Run Keyword And Return Status
	...	Should Contain	${OUTPUT}	${NUM_PACKETS} received
	Run Keyword If	not ${RECEIVED_STATUS}
	...	Should Contain	${OUTPUT}	local error: message too long, mtu=${MTU_VALUE}