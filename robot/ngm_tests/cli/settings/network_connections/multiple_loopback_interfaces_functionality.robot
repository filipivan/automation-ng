*** Settings ***
Resource	../../init.robot
Documentation	Tests multiple loopback interfaces functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
Default Tags	CLI	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${LOOPBACK1}	test-loopback1
${LOOPBACK1_IPV4_IP}	10.20.30.1
${LOOPBACK1_IPV6_IP}	fe80::1
${LOOPBACK2}	test-loopback2
${LOOPBACK2_IPV4_IP}	10.20.30.2
${LOOPBACK2_IPV6_IP}	fe80::2
${LOOPBACK3}	test-loopback3
${LOOPBACK3_IPV4_IP}	10.20.30.3
${LOOPBACK3_IPV6_IP}	fe80::3
${LOOPBACK_HOSTNAME}	test-multi-loopback-hostname

*** Test Cases ***
Test Loopback interface Ipv4
	SUITE:Check If Interface Is In Operating System
	...	${SYS_LOOPBACK1}	IPV4_ADDR=${LOOPBACK1_IPV4_IP}
	CLI:Switch Connection	default
	CLI:Test Ping	${LOOPBACK1_IPV4_IP}
	SUITE:Test Loopback SSH	${LOOPBACK1_IPV4_IP}

Test Loopback interface Ipv6
	SUITE:Check If Interface Is In Operating System
	...	${SYS_LOOPBACK2}	IPV6_ADDR=${LOOPBACK2_IPV6_IP}
	CLI:Switch Connection	default
	CLI:Test Ping	${LOOPBACK2_IPV6_IP}%${SYS_LOOPBACK2}
	SUITE:Test Loopback SSH	${LOOPBACK2_IPV6_IP}%${SYS_LOOPBACK2}

Test Loopback interface Ipv4 and Ipv6
	SUITE:Check If Interface Is In Operating System
	...	${SYS_LOOPBACK3}	IPV4_ADDR=${LOOPBACK3_IPV4_IP}	IPV6_ADDR=${LOOPBACK3_IPV6_IP}
	CLI:Switch Connection	default
	CLI:Test Ping	${LOOPBACK3_IPV4_IP}
	CLI:Test Ping	${LOOPBACK3_IPV6_IP}%${SYS_LOOPBACK3}
	SUITE:Test Loopback SSH	${LOOPBACK3_IPV4_IP}
	SUITE:Test Loopback SSH	${LOOPBACK3_IPV6_IP}%${SYS_LOOPBACK3}

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Delete Loopback interfaces
	SUITE:Add Loopback Interfaces
	SUITE:Set System Loopback Names
	CLI:Change Hostname	${LOOPBACK_HOSTNAME}
	CLI:Connect As Root

SUITE:Teardown
	CLI:Switch Connection	default
	SUITE:Delete Loopback interfaces
	CLI:Change Hostname	${HOSTNAME_NODEGRID}
	CLI:Close Connection

SUITE:Delete Loopback Interfaces
	CLI:Enter Path	/settings/network_connections
	CLI:Delete If Exists	${LOOPBACK1}	${LOOPBACK2}	${LOOPBACK3}
	CLI:Test Not Show Command	${LOOPBACK1}	${LOOPBACK2}	${LOOPBACK3}

SUITE:Get System Loopback name
	[Arguments]	${NG_LOOPBACK_NAME}
	CLI:Enter Path	/settings/network_connections
	${OUTPUT}=	CLI:Show
	${MATCH}	${SYS_LOOPBACK_NAME}=	Should Match Regexp	${OUTPUT}	${NG_LOOPBACK_NAME}\\s{2,}\\w+\\s{2,}loopback\\s{2,}(loopback\\d+)
	[Return]	${SYS_LOOPBACK_NAME}

SUITE:Set System Loopback Names
	${SYS_LOOPBACK1}=	SUITE:Get System Loopback name	${LOOPBACK1}
	Set Suite Variable	${SYS_LOOPBACK1}
	${SYS_LOOPBACK2}=	SUITE:Get System Loopback name	${LOOPBACK2}
	Set Suite Variable	${SYS_LOOPBACK2}
	${SYS_LOOPBACK3}=	SUITE:Get System Loopback name	${LOOPBACK3}
	Set Suite Variable	${SYS_LOOPBACK3}

SUITE:Add Loopback Interfaces
	CLI:Enter Path	/settings/network_connections
	CLI:Add
	CLI:Set	name=${LOOPBACK1} type=loopback ipv6_mode=no_ipv6_address
	CLI:Set	ipv4_mode=static ipv4_address=${LOOPBACK1_IPV4_IP} ipv4_bitmask=32
	CLI:Commit

	CLI:Enter Path	/settings/network_connections
	CLI:Add
	CLI:Set	name=${LOOPBACK2} type=loopback ipv4_mode=no_ipv4_address
	CLI:Set	ipv6_mode=static ipv6_address=${LOOPBACK2_IPV6_IP} ipv6_prefix_length=128
	CLI:Commit

	CLI:Enter Path	/settings/network_connections
	CLI:Add
	CLI:Set	name=${LOOPBACK3} type=loopback ipv6_mode=static ipv6_address=${LOOPBACK3_IPV6_IP} ipv6_prefix_length=128
	CLI:Set	ipv4_mode=static ipv4_address=${LOOPBACK3_IPV4_IP} ipv4_bitmask=32
	CLI:Commit

SUITE:Check If Interface Is In Operating System
	[Arguments]	${INTERFACE}	${IPV4_ADDR}=${EMPTY}	${IPV6_ADDR}=${EMPTY}
	CLI:Switch Connection	root_session
	${OUTPUT}=	CLI:Write	ip addr show dev ${INTERFACE}
	Should Not Contain	${OUTPUT}	Device "${INTERFACE}" does not exist.
	Run Keyword If	'${IPV4_ADDR}' != '${EMPTY}'
	...	Should Contain	${OUTPUT}	inet ${IPV4_ADDR}/32
	Run Keyword If	'${IPV6_ADDR}' != '${EMPTY}'
	...	Should Contain	${OUTPUT}	inet6 ${IPV6_ADDR}/128

SUITE:Test Loopback SSH
	[Arguments]	${IP_ADDRESS}
	CLI:Switch Connection	root_session

	Write	ssh ${DEFAULT_USERNAME}@${IP_ADDRESS}
	${OUTPUT}=	Read Until Regexp	(~#|Password:|Are you sure you want to continue connecting \\(.*\\)\\?)
	Log To Console	\n${OUTPUT}\n
	Should Not Contain	${OUTPUT}	ssh: connect to host ${IP_ADDRESS} port 22: No route to host
	${AUTHENTICITY_DIALOG}=	Run Keyword And Return Status
	...	Should Match Regexp	${OUTPUT}	Are you sure you want to continue connecting \\(.*\\)\\?
	Run Keyword If	${AUTHENTICITY_DIALOG}	Write	yes
	${OUTPUT}=	Run Keyword If	${AUTHENTICITY_DIALOG}
	...	Read Until	Password:	loglevel=INFO
	...	ELSE	Set Variable	${OUTPUT}
	Run Keyword If	${AUTHENTICITY_DIALOG}	Log To Console	\n${OUTPUT}\n

	Set Client Configuration	prompt=]#
	${OUTPUT}=	CLI:Write	${DEFAULT_PASSWORD}
	Should Contain	${OUTPUT}	${LOOPBACK_HOSTNAME}
	Set Client Configuration	prompt=~#
	CLI:Write	exit
