*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Network Connections of type bridge... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN5_0
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${BRIDGE_NAME}	bridge_test
@{INTERFACES}	eth0	eth1	backplane0	backplane1	hotspot
@{INVALID_MODES}	${ELEVEN_WORD}	${ELEVEN_NUMBER}	${ELEVEN_POINTS}	${EXCEEDED}
@{VALID_MAC_ADDRESS}	00:11:22:33:44:55	00:ff:ee:dd:cc:bb:aa
@{INVALID_MAC_ADDRESS}	123:456:789:0aa	aa:bb:cc:dd:ee:ff:gg
@{VALID_NUMERIC}	${THREE_NUMBER}	${EIGHT_NUMBER}
@{INVALID_NUMERIC}	${THREE_WORD}	${THREE_POINTS}

*** Test Cases ***
Test show Command
	CLI:Enter Path	/settings/network_connections/${BRIDGE_NAME}
	CLI:Test Show Command	name:	type:	connect_automatically\ =	set_as_primary_connection\ =	bridge_interfaces\ =
	...	enable_spanning_tree_protocol\ =	hello_time\ =	forward_delay\ =	max_age\ =	ipv4_mode\ =	ipv4_dns_server\ =
	...	ipv4_dns_search\ =	ipv6_mode\ =	ipv6_dns_server\ =	ipv6_dns_search\ =
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Show Command	availables:
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Show Command	description\ =	ipv4_default_route_metric\ =
	...	ipv4_ignore_obtained_default_gateway\ =	ipv6_default_route_metric\ =	ipv6_ignore_obtained_default_gateway\ =
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Show Command	interface:
	Run Keyword If	'${NGVERSION}' >= '5.2'	CLI:Test Show Command	ipv4_ignore_obtained_dns_server\ =	ipv6_ignore_obtained_dns_server\ =
	Run Keyword If	'${NGVERSION}' >= '5.4'	CLI:Test Show Command	block_unsolicited_incoming_packets\ =

Test Available Fields After Set Command
	CLI:Test Set Available Fields	connect_automatically	set_as_primary_connection	bridge_interfaces	enable_spanning_tree_protocol
	...	hello_time	forward_delay	max_age	ipv4_mode	ipv4_dns_server	ipv4_dns_search	ipv6_mode	ipv6_dns_server	ipv6_dns_search
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Available Fields	description	ipv4_default_route_metric
	...	ipv4_ignore_obtained_default_gateway	ipv6_default_route_metric	ipv6_ignore_obtained_default_gateway
	Run Keyword If	'${NGVERSION}' >= '5.2'	CLI:Test Set Available Fields	ipv6_ignore_obtained_dns_server	ipv4_ignore_obtained_dns_server
	Run Keyword If	'${NGVERSION}' >= '5.4'	CLI:Test Set Available Fields	block_unsolicited_incoming_packets
	[Teardown]	CLI:Cancel	Raw

Test valid values for bridge_interfaces
	[Documentation]	Test checks interfaces listed in NG and then compares all possible values within a list of avaliable
	...	interfaces. If this interface is avaliable it is tested as a valid value for bridge_interfaces field
	${DEVICE_INTERFACES}	CLI:Ls	/system/network_statistics/
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bridge
	FOR	${INTERFACE}	IN	@{INTERFACES}
		${INTERFACE_AVALIABLE}	Run Keyword And Return Status	Should Contain	${DEVICE_INTERFACES}	${INTERFACE}
		IF	${INTERFACE_AVALIABLE}
			CLI:Test Set Field Valid Options	bridge_interfaces	${INTERFACE}	revert=no
		END
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for bridge_interfaces
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	#it's only validated on v5.2+
	[Documentation]	Test lefts bridge_interfaces field empty and expects error message
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bridge bridge_interfaces=${EMPTY}
	${ERROR}	CLI:Commit	Raw
	Should Contain	${ERROR}
	...	Error: bridge_interfaces: This field must be a list of interfaces separated by space enclosed in quotation marks (e.g. eth0 eth1).
	[Teardown]	CLI:Cancel	Raw

Test valid values for bridge_mac_configuration
	[Documentation]	Test field bridge_mac_configuration can be configured with all possible values
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	@{MAC_ADDRESS_MODES}	Create List	bridge_custom_mac	use_mac_from_first_interface
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bridge
	CLI:Test Set Validate Normal Fields	bridge_mac_configuration	@{MAC_ADDRESS_MODES}
	[Teardown]	CLI:Cancel	Raw

Test invalid values for bridge_mac_configuration
	[Documentation]	Test inserts invalid values (like random string, numbers and points) for bridge_mac_configuration
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN	5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bridge
	FOR	${MODE}	IN	@{INVALID_MODES}
		CLI:Test Set Validate Invalid Options	bridge_mac_configuration	${MODE}
		...	Error: Invalid value: ${MODE} for parameter: bridge_mac_configuration
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for bridge_mac_address
	[Documentation]	Test tries some valid MAC addresses (6-pair hexa divided by ":") for bridge_mac_address field. This
	...	field is only configurable if bond_mac_configuration=bridge_custom_mac.
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bridge bridge_mac_configuration=bridge_custom_mac
	FOR	${MAC}	IN	@{VALID_MAC_ADDRESS}
		CLI:Test Set Field Valid Options	bridge_mac_address	${MAC}	revert=no
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for bridge_mac_address
	[Documentation]	Test tries invalid values (like 7-pair hexa and other variants) for bridge_mac_address field and
	...	expects  correct error message after commit
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bridge bridge_mac_configuration=bridge_custom_mac
	FOR	${MAC}	IN	@{INVALID_MAC_ADDRESS}
		CLI:Set	bridge_mac_address=${MAC}
		${ERROR_EXPECTED}	CLI:Commit	Raw
		Should Contain	${ERROR_EXPECTED}	Error: bridge_mac_address: Validation error.
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for ageing_time
	[Documentation]	Test tries some valid numeric values for ageing_time field
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bridge
	FOR	${VALUE}	IN	@{VALID_NUMERIC}
		CLI:Test Set Field Valid Options	ageing_time	${VALUE}	revert=no
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for ageing_time
	[Documentation]	Test tries invalid values (strings and special characteters) for bridge_mac_address field and
	...	expects  correct error message after commit
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bridge
		FOR	${VALUE}	IN	@{INVALID_NUMERIC}
		CLI:Set	ageing_time=${VALUE}
		${ERROR_EXPECTED}	CLI:Commit	Raw
		Should Contain	${ERROR_EXPECTED}	Error: ageing_time: Valid value is a number greater than 0.
	END
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/network_connections/
	CLI:Delete If Exists	${BRIDGE_NAME}
	SUITE:Add Bridge Interface	${BRIDGE_NAME}

SUITE:Teardown
	CLI:Enter Path	/settings/network_connections/
	Write	delete ${BRIDGE_NAME}
	Read Until	]#
	CLI:Close Connection

SUITE:Add Bridge Interface
	[Arguments]	${BRIDGE_NAME}
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bridge name=${BRIDGE_NAME} bridge_interfaces=eth0
	CLI:Commit