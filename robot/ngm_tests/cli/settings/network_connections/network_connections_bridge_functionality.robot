*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Network Connections Functionalities for type Bridge through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	NON-CRITICAL	NEED-REVIEW	EXCLUDEIN5_0
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${BR_NAME}	test
${BR_INTERFACE}	backplane0
${LOW_AGEING_TIME}	300
${UNAGED_TIME}	0

*** Test Cases ***
Test Block Unsolicited Incoming Packets
	[Documentation]	Test verifies if iperf3 works on the bridge interface and then it enables Block Unsolicited Incoming Packets
	...	on the bridge interface and tests if iperf3 get blocked
	Skip If	'${NGVERSION}' < '5.4'
	SUITE:Add Bridge Interface	eth0	${HOST}
	SUITE:Iperf3 On Source
	SUITE:Iperf3 On Destination	${HOST}	${HOSTPEER}
	SUITE:Set Block Unsolicited Incoming Packets	yes
	SUITE:Iperf3 On Source
	SUITE:Iperf3 On Destination	${HOST}	${HOSTPEER}	True

Test Unblock Unsolicited Incoming Packets
	[Documentation]	Test verifies if iperf3 works again after unsetting Block Unsolicited Incoming Packets
	Skip If	'${NGVERSION}' < '5.4'
	SUITE:Set Block Unsolicited Incoming Packets	no
	SUITE:Iperf3 On Source
	SUITE:Iperf3 On Destination	${HOST}	${HOSTPEER}
	CLI:Write	shell sudo killall iperf3
	SUITE:Delete Interface	bridge_eth0

Test Set as Primary Connection
	[Documentation]	Test verifies if bridge interface becomes the primary one after setting it as primary interface
	Skip If	not ${HAS_ETH1}	Device doesn't have ETH1

	SUITE:Add Bridge Interface	eth1	${ETH1_IP}
	Wait Until Keyword Succeeds	2x	3s	SUITE:Check Primary Interface	False
	SUITE:Set Primary Interface	bridge_eth1	yes
	Wait Until Keyword Succeeds	2x	3s	SUITE:Check Primary Interface	True

Test Unset as Primary Connection
	[Documentation]	Test verifies if bridge interface is removed from primary one after unsetting it as primary interface
	Skip If	not ${HAS_ETH1}	Device doesn't have ETH1

	SUITE:Set Primary Interface	bridge_eth1	no
	Wait Until Keyword Succeeds	2x	3s	SUITE:Check Primary Interface	False

Test adding bridge with ageing time configuration
	[Documentation]	Test add a bridge configuration with ageing time and checks configuration was really applied
	...	in STP context using brtcl command
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	NON-CRITICAL
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${BR_NAME} type=bridge bridge_interfaces=${BR_INTERFACE}
	CLI:Set	enable_spanning_tree_protocol=yes hello_time=10 forward_delay=4 max_age=6 ageing_time=${LOW_AGEING_TIME}
	CLI:Commit
	CLI:Connect As Root
	${BRCTL_OUTPUT}	CLI:Write	 brctl showstp br0 | grep ageing
	Should Contain	${BRCTL_OUTPUT}	${LOW_AGEING_TIME}
	CLI:Close Current Connection

Test bridge interface aged out
	[Documentation]	Test uses the command brctl show to list all acitve bridge interfaces in Spanning Tree Protocol
	...	and checks that, after a while, the bridge created with ageing time property, ages out and is not shown on the table
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	NON-CRITICAL
	CLI:Connect As Root
	Wait Until Keyword Succeeds	150s	3s	SUITE:Check Bridge Aged Out
	CLI:Close Current Connection

Test bidge interface does not age out with ageing_time=0
	[Documentation]	Test changes ageing time to 0, which means the bridge interface will never age out,
	...	and then uses the command brctl show to list all acitve bridge interfaces in Spanning Tree Protocol and checks
	...	that, after a while, the bridge created without ageing time property, never ages out and is shown on the table
	...	The test fails if the bridge ages out (is not present on the table).
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	NON-CRITICAL
	CLI:Open
	CLI:Enter Path	/settings/network_connections/${BR_NAME}
	CLI:Set	ageing_time=${UNAGED_TIME}
	CLI:Commit
	CLI:Connect As Root
	${AGED_OUT}	Run Keyword And Return Status	Wait Until Keyword Succeeds	100s	5s	SUITE:Check Bridge Aged Out
	Run Keyword If	${AGED_OUT}	Fail	The bridge aged_out with ageing_time as value ${UNAGED_TIME}!
	CLI:Close Current Connection

*** Keywords ***
SUITE:Setup
	CLI:Open	session_alias=host_session
	${HAS_ETH1}	${ETH1_IP}	SUITE:Has Eth1
	Set Suite Variable	${HAS_ETH1}
	Run Keyword If	${HAS_ETH1}	Set Suite Variable	${ETH1_IP}

SUITE:Teardown
	CLI:Close Connection
	CLI:Open
	CLI:Enter Path	/settings/network_connections/
	write	delete bridge_eth0
	Sleep	10s
	Read Until Prompt
	SUITE:Delete Interface	bridge_eth0	bridge_eth1	${BR_NAME}
	Write	shell sudo killall iperf3
	Read Until Prompt
	CLI:Close Connection

SUITE:Add Bridge Interface
	[Arguments]	${INTERFACE}	${IP}
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bridge name=bridge_${INTERFACE} bridge_interfaces=${INTERFACE}
	CLI:Commit
	Wait Until Keyword Succeeds	5x	5s	SUITE:Check Bridge Creation	${INTERFACE}	${IP}

SUITE:Check Bridge Creation
	[Arguments]	${INTERFACE}	${IP}
	${OUTPUT}	CLI:Show
	@{LINES}	Split To Lines	${OUTPUT}	2
	FOR	${LINE}	IN	@{LINES}
		${STATUS}	Run Keyword and Return Status	Should Contain	${LINE}	bridge_${INTERFACE}
		${INTERFACE_LINE}	Run Keyword If	${STATUS}	Set Variable	${LINE}
		Exit For Loop If	${STATUS}
	END
	Should Match Regexp	${INTERFACE_LINE}	bridge_${INTERFACE}(\\s)+connected(\\s)+bridge(\\s)+br[0-9]+(\\s)+up(\\s)+${IP}(.)*

SUITE:Iperf3 On Source
	Set Client Configuration	prompt=~#
	CLI:Write	shell sudo su -
	Write	iperf3 -s -p 5201
	Read Until	Server listening on 5201

SUITE:Iperf3 On Destination
	[Arguments]	${IP_SOURCE}	${IP_DESTINATION}	${FAIL}=False
	CLI:Open	root	root	HOST_DIFF=${IP_DESTINATION}
	Set Client Configuration	timeout=300
	Write	iperf3 -c ${IP_SOURCE} -4 -p 5201 -t 3 -i 1 --bind ${IP_DESTINATION}
	Run Keyword If	${FAIL}	Sleep	45s
	Run Keyword If	${FAIL}	CLI:Write	\x03
	${OUTPUT}	CLI:Read Until Prompt
	Run Keyword If	${FAIL}	Should Not Contain	${OUTPUT}	/sec	Mbits	KBytes	0.00-
	...	ELSE	Should Contain	${OUTPUT}	iperf Done.
	LOG	${OUTPUT}
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Switch Connection	host_session
	CLI:Write	\x03
	CLI:Write	killall iperf3
	Set Client Configuration	prompt=]#
	CLI:Write	exit

SUITE:Set Block Unsolicited Incoming Packets
	[Arguments]	${STATUS}
	CLI:Enter Path	/settings/network_connections/bridge_eth0
	CLI:Set	block_unsolicited_incoming_packets=${STATUS}
	CLI:Commit

SUITE:Delete Interface
	[Arguments]	@{DELETE_INTERFACE}
	CLI:Enter Path	/settings/network_connections/
	FOR	${INTERFACE}	IN	@{DELETE_INTERFACE}
		Write	delete ${INTERFACE}
		Read Until	]#
	END

SUITE:Has Eth1
	${HAS_ETH1}	CLI:Has ETH1
	Return From Keyword If	not ${HAS_ETH1}	False	0.0.0.0
	${HAS_IP}	${IP}	SUITE:Get Interface Ip	eth1
	Return From Keyword If	not ${HAS_IP}	False	0.0.0.0
	[Return]	${HAS_IP}	${IP}

SUITE:Get Interface Ip
	[Arguments]	${INTERFACE}
	${IP}	CLI:Get Interface IP Address	${INTERFACE}
	${HAS_IP}	Run Keyword And Return Status	Should Match Regexp	${IP}	192.168.[0-9][0-9]?[0-9]?.[0-9][0-9]?[0-9]?
	[Return]	${HAS_IP}	${IP}

SUITE:Check Primary Interface
	[Arguments]	${STATUS}
	${OUTPUT}	CLI:Write	shell sudo ip r
	${LINES}	Split To Lines	${OUTPUT}
	FOR	${LINE}	IN	@{LINES}
		${BRIDGE}	Run Keyword And Return Status	Should Contain	${LINE}	br0
		${INTERFACE}	Run Keyword If	${BRIDGE}	Set Variable	${LINE}
		Exit For Loop If	${BRIDGE}
	END
	${TRASH}	${INTERFACE}	Split String From Right	${INTERFACE}	metric${SPACE}	1
	${INTERFACE}	Remove String	${INTERFACE}	${SPACE}
	Run keyword If	${STATUS}	Should Be Equal	${INTERFACE}	90
	Run keyword If	not ${STATUS}	Should Not Be Equal	${INTERFACE}	90


SUITE:Set Primary Interface
	[Arguments]	${BRIDGE}	${STATUS}
	CLI:Enter Path	/settings/network_connections/${BRIDGE}
	CLI:Set	set_as_primary_connection=${STATUS}
	CLI:Commit

SUITE:Check Bridge Aged Out
	${ARL_TABLE}	CLI:Write	brctl show
	Should Not Contain	${ARL_TABLE}	br0