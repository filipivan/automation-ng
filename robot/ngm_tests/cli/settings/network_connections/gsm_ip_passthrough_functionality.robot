*** Settings ***
Resource	../../init.robot
Documentation	Test IP Passthrough functionality for GSM
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	DEPENDENCE_MODEM	NON-CRITICAL
Default Tags	CLI	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CELLULAR_CONNECTION}	${GSM_CELLULAR_CONNECTION}
${TYPE}	${GSM_TYPE}
${STRONG_PASSWORD}	${QA_PASSWORD}
${VLAN100}	BACKPLANE0-100
${VLAN100_SHELL}	backplane0.100
${CUSTOM_HOSTNAME}	test-ip-passthrough-hostname

*** Test Cases ***
Test GSM Ipv4 should Passthrough to Peer
	Skip If	not ${IP_PASSTHROUGH_AVAILABLE}	IP Passthrough tests not available/configured for this testing build
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${IS_SR}	VLAN is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	SUITE:Wait For Peer To Have GSM Ipv4 Forwarded

Test connecting to GSM Ipv4
	Skip If	not ${IP_PASSTHROUGH_AVAILABLE}	IP Passthrough tests not available/configured for this testing build
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${IS_SR}	VLAN is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available

	Switch Connection	PEER
	CLI:Change Hostname	${CUSTOM_HOSTNAME}
	CLI:Connect To Host On Peer Shell	${HOSTSHARED}	${HOST}	${DEFAULT_USERNAME}
	...	${STRONG_PASSWORD}	]#	${STRONG_PASSWORD}
	${OUTPUT}=	CLI:Write	whoami
	CLI:Disconnect From Host On Peer Shell
	Should Contain	${OUTPUT}	${CUSTOM_HOSTNAME}

*** Keywords ***
SUITE:Setup
	Skip If	not ${IP_PASSTHROUGH_AVAILABLE}	IP Passthrough tests not available/configured for this testing build
	CLI:Open	session_alias=PEER	HOST_DIFF=${HOSTSHARED}
	CLI:Open	session_alias=HOST

	${HAS_WMODEM_SUPPORT}=	CLI:Has Wireless Modem Support
	Set Suite Variable	${HAS_WMODEM_SUPPORT}
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system

	${IS_SR}=	CLI:Is SR System
	Set Suite Variable	${IS_SR}
	Skip If	not ${IS_SR}	VLAN is not supported by this system

	${HAS_WMODEM}	${MODEM_INFO}=	CLI:Get Wireless Modems SIM Card Info	0
	Set Suite Variable	${HAS_WMODEM}
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	Set Suite Variable	${MODEM_INFO}

	CLI:Change User Password From Shell	root	${STRONG_PASSWORD}	${ROOT_PASSWORD}
	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${STRONG_PASSWORD}	${STRONG_PASSWORD}
	CLI:Change User Password From Shell	root	${STRONG_PASSWORD}	${ROOT_PASSWORD}	${HOSTSHARED}
	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${STRONG_PASSWORD}	${STRONG_PASSWORD}	${HOSTSHARED}

	SUITE:Configure IP Passthrough On Host
	SUITE:Configure Peer

SUITE:Teardown
	Skip If	not ${IP_PASSTHROUGH_AVAILABLE}	IP Passthrough tests not available/configured for this testing build
	Switch Connection	PEER
	CLI:Change Hostname	${HOSTNAME_NODEGRID}
	${STATUS}	SUITE:Teardown IP Passthrough On Host
	IF	${STATUS}
		CLI:Close Connection
	ELSE
		Fail	Couldn't login to NG with strong neither factory password
	END

SUITE:Configure IP Passthrough On Host
	CLI:Switch Connection	HOST
	CLI:Delete VLAN	100
	SUITE:Configure VLAN On Host
	SUITE:Set Network Connection To No Ipv4 Address	BACKPLANE0
	SUITE:Enable Ipv4 Forwarding
	SUITE:Add GSM Connection
	SUITE:Wait For GSM To Be Up
	SUITE:Get GSM Ipv4 On Host
	SUITE:Enable IP Passthrough

SUITE:Configure Peer
	CLI:Switch Connection	PEER
	Run Keyword If	${IP_PASSTHROUGH_SHARED_USES_SWITCH}	SUITE:Configure VLAN On Peer
	...	ELSE	SUITE:Configure Ethernet Connection On Peer

SUITE:Configure VLAN On Peer
	CLI:Delete VLAN	100
	CLI:Delete Network Connections	${VLAN100}
	CLI:Add VLAN	100	UNTAGGED_PORTS=${IP_PASSTHROUGH_SHARED_INTERFACE},backplane0
	CLI:Edit VLAN	100	TAGGED_PORTS=${IP_PASSTHROUGH_SHARED_INTERFACE},backplane0
	CLI:Enable Switch Interface	${IP_PASSTHROUGH_SHARED_INTERFACE}

	Set Client Configuration	timeout=60s
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${VLAN100} type=vlan ethernet_interface=backplane0 ipv4_mode=dhcp vlan_id=100
	CLI:Commit
	CLI:Write	up_connection ${VLAN100}
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}

SUITE:Configure VLAN On Host
	CLI:Add VLAN	100	UNTAGGED_PORTS=${IP_PASSTHROUGH_HOST_INTERFACE},backplane0
	CLI:Edit VLAN	100	TAGGED_PORTS=${IP_PASSTHROUGH_HOST_INTERFACE}	UNTAGGED_PORTS=backplane0
	CLI:Enable Switch Interface	${IP_PASSTHROUGH_HOST_INTERFACE}

SUITE:Configure Ethernet Connection On Peer
	${CONNECTION}=	${IP_PASSTHROUGH_SHARED_INTERFACE}
	SUITE:Set Network Connection To DHCP Ipv4	${CONNECTION}

SUITE:Teardown IP Passthrough On Host
	${OPEN}	Run Keyword And Return Status	CLI:Open	PASSWORD=${STRONG_PASSWORD}
	IF	not ${OPEN}
		${OPEN}	Run Keyword And Return Status	CLI:Open	PASSWORD=${FACTORY_PASSWORD}
	ELSE
		CLI:Delete Network Connections	${CELLULAR_CONNECTION}
		SUITE:Set Network Connection To DHCP Ipv4	BACKPLANE0
		SUITE:Disable Ipv4 Forwarding
		CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}	${STRONG_PASSWORD}
		CLI:Change User Password From Shell	root	${ROOT_PASSWORD}	${STRONG_PASSWORD}
		CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}	${STRONG_PASSWORD}	${HOSTSHARED}
		CLI:Change User Password From Shell	root	${ROOT_PASSWORD}	${STRONG_PASSWORD}	${HOSTSHARED}
	END
	[return]	${OPEN}

SUITE:Teardown Peer Connection
	CLI:Switch Connection	PEER
	Run Keyword If	${IP_PASSTHROUGH_SHARED_USES_SWITCH}	SUITE:Teardown VLAN On Peer

SUITE:Teardown VLAN On Peer
	CLI:Delete VLAN	100
	CLI:Delete Network Connections	${VLAN100}

SUITE:Add GSM Connection
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CELLULAR_CONNECTION} type=${TYPE}
	CLI:Set	ethernet_interface=${MODEM_INFO["interface"]} ipv4_mode=dhcp ipv6_mode=no_ipv6_address
	Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Set	sim-1_apn_configuration=manual
	CLI:Set	sim-1_access_point_name=${MODEM_INFO["apn"]}
	CLI:Commit

SUITE:Enable IP Passthrough
	CLI:Enter Path	/settings/network_connections/${CELLULAR_CONNECTION}
	CLI:Set	enable_ip_passthrough=yes ethernet_connection=backplane0
	CLI:Commit
	CLI:Enter Path	/settings/network_connections/
	CLI:Test Show Command Regexp	${CELLULAR_CONNECTION}.*IP Passthrough

SUITE:Enable Ipv4 Forwarding
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	enable_ipv4_ip_forward=yes
	CLI:Commit

SUITE:Disable Ipv4 Forwarding
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	enable_ipv4_ip_forward=no
	CLI:Commit

SUITE:Set Network Connection To DHCP Ipv4
	[Arguments]	${CONNECTION}
	CLI:Enter Path	/settings/network_connections/${CONNECTION}
	CLI:Set	ipv4_mode=dhcp
	CLI:Commit

SUITE:Set Network Connection To No Ipv4 Address
	[Arguments]	${CONNECTION}
	CLI:Enter Path	/settings/network_connections/${CONNECTION}
	CLI:Set	ipv4_mode=no_ipv4_address
	CLI:Commit

SUITE:Wait For GSM To Be Up
	CLI:Enter Path	/settings/network_connections/
	Wait Until Keyword Succeeds	30s	3s	CLI:Test Show Command Regexp	${CELLULAR_CONNECTION}.*(\\d+\\.*){4}

SUITE:Get GSM Ipv4 On Host
	${IP_ADDRESS}=	CLI:Get Interface IP Address	${MODEM_INFO["shell interface"]}
	Set Suite Variable	${GSM_IP_ADDRESS}	${IP_ADDRESS}

SUITE:Wait For Peer To Have GSM Ipv4 Forwarded
	Switch Connection	PEER
	Wait Until Keyword Succeeds	30s	3s	SUITE:Peer Should Have GSM Ipv4 Forwarded

SUITE:Peer Should Have GSM Ipv4 Forwarded
	${IP_ADDRESS}=	CLI:Get Interface IP Address	${VLAN100_SHELL}
	Should Be Equal	${IP_ADDRESS}	${GSM_IP_ADDRESS}
