*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Network Connections page, ls, show, add, etc... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CONNECTION}	test-connection-validation
@{ALL_VALUES}	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}	${EXCEEDED}
${SECRET}	secretzpe1234

*** Test Cases ***
Test ls and show commands
	CLI:Enter Path	/settings/network_connections/
	${OUTPUT}=	CLI:Write	ls	user=Yes
	@{NAMES}=	Split String	${OUTPUT}	/
	CLI:Test Show Command	name	status	type	interface	carrier state	ipv4 address	ipv6 address	mac address
	FOR			${NAME}	IN	@{NAMES}
	CLI:Enter Path	/settings/network_connections/${NAME}
		${OUTPUT}=	CLI:Show
		${TYPE}=	CLI:Get Type	${OUTPUT}
		Run Keyword If	'${TYPE}'=='analog modem'	CLI:Test Show Command	connect_automatically\ =	set_as_primary_connection\ =
		...	status\ =	device_name\ =	speed\ =	phone_number\ =	init_chat\ =	ppp_idle_timeout\ =	ppp_ipv4_address\ =	ppp_ipv6_address\ =	ppp_authentication\ =
		Run Keyword If	'${TYPE}'=='bonding'	CLI:Test Show Command	connect_automatically\ =	set_as_primary_connection\ =
		...	enable_lldp\ =	primary_interface\ =	secondary_interfaces\ =	bonding_mode\ =	link_monitoring\ =	monitoring_frequency\ =
		...	link_up_delay\ =	link_down_delay\ =	arp_target\ =	arp_validate\ =	ipv4_mode\ =	ipv4_dns_server\ =	ipv4_dns_search\ =
		...	ipv6_mode\ =	ipv6_dns_server\ =	ipv6_dns_search\ =
		Run Keyword If	'${TYPE}'=='bridge'	CLI:Test Show Command	connect_automatically\ =	set_as_primary_connection\ =
		...	bridge_interfaces\ =	enable_spanning_tree_protocol =	hello_time_(sec) =	forward_delay_(sec) =	max_age_(sec) =
		...	ipv4_mode =	ipv4_dns_server =	ipv4_dns_search =	ipv6_mode =	ipv6_dns_server =	ipv6_dns_search =
		Run Keyword If	'${TYPE}'=='ethernet'	CLI:Test Show Command	ethernet_interface\ =	connect_automatically\ =
		...	set_as_primary_connection\ =	enable_lldp\ =	ipv4_mode\ =	ipv4_dns_server\ =	ipv4_dns_search\ =	ipv6_mode\ =
		...	ipv6_dns_server\ =	ipv6_dns_search\ =
		Run Keyword If	'${TYPE}'=='mobile broadband gsm'	CLI:Test Show Command	connect_automatically\ =
		...	set_as_primary_connection\ =	enable_lldp\ =	user_name\ =	password\ =	access_point_name\ =
		...	personal_identification_number\ =	ipv4_mode\ =	ipv4_dns_server\ =	ipv4_dns_search\ =	ipv6_mode\ =
		...	ipv6_dns_server\ =	ipv6_dns_search\ =
		Run Keyword If	'${TYPE}'=='vlan'	CLI:Test Show Command	ethernet_interface\ =	connect_automatically\ =
		...	set_as_primary_connection\ =	vlan_id\ =	ipv4_mode\ =	ipv4_dns_server\ =	ipv4_dns_search\ =	ipv6_mode\ =
		...	ipv6_dns_server\ =	ipv6_dns_search\ =
		Run Keyword If	'${TYPE}'=='wifi'	CLI:Test Show Command	connect_automatically\ =	set_as_primary_connection\ =
		...	enable_lldp\ =	wifi_ssid\ =	wifi_security\ =	psk\ =	hotspot_ipv4_mode\ =	shared_ipv4_address\ =
		...	shared_ipv4_bitmask\ =	ipv4_dns_server\ =	ipv4_dns_search\ =	ipv6_mode\ =	ipv6_dns_server\ =	ipv6_dns_search\ =
	END

Test available commands after send tab-tab
	CLI:Test Available Commands	cd	delete	event_system_clear	ls	reboot	shell	shutdown	change_password	down_connection	exit
	...	pwd	revert	show	up_connection	add	commit	event_system_audit	hostname	quit	set	show_settings	whoami

Test available commands after add command
	CLI:Add
	CLI:Test Available Commands	cancel	commit	ls	save	set	show
	CLI:Cancel

Test available set fields
	${fields} =	Create List	arp_target	arp_validate	bonding_mode	bridge_interfaces	connect_automatically	device_name	enable_lldp	enable_spanning_tree_protocol
	...	init_chat	ipv4_dns_search	ipv4_dns_server	ipv6_dns_search	ipv6_dns_server	ipv6_mode	link_down_delay	link_monitoring	link_up_delay
	...	monitoring_frequency	name	phone_number	ppp_authentication	ppp_idle_timeout	ppp_ipv4_address	ppp_ipv6_address	primary_interface	secondary_interface
	...	set_as_primary_connection	speed	status	type	vlan_id	wifi_bssid	wifi_security
	Run Keyword If	'${NGVERSION}' == '3.2'	Append To List	${fields}	access_point_name	bond_mac_policy	password	personal_identification_number	user_name
	Run Keyword If	'${NGVERSION}' <= '4.2'	Append To List	${fields}	ethernet_interface
	Run Keyword If	'${NGVERSION}' >= '4.2'	Append To List	${fields}	actor_mac_address	aggregation_selection_logic	bond_fail-over-mac_policy	description
	...	enable_data_usage_monitoring	enable_global_positioning_system	enable_ip_passthrough	enable_second_sim_card	ipv4_default_route_metric
	...	ipv4_ignore_obtained_default_gateway	ipv6_default_route_metric	ipv6_ignore_obtained_default_gateway	lacp_rate	pppoe_parent_interface
	...	pppoe_password	pppoe_service	pppoe_username	sim-1_access_point_name	sim-1_mtu	sim-1_password	sim-1_personal_identification_number
	...	sim-1_phone_number	sim-1_user_name	slave(s)	system_priority	transmit_hash_policy	user_port_key
	Run Keyword If	'${NGVERSION}' >= '5.0'	Append To List	${fields}	ipv4_ignore_obtained_dns_server	ipv6_ignore_obtained_dns_server	mobile_interface
	Run Keyword If	'${NGVERSION}' >= '5.2'	Append To List	${fields}	enable_connection_health_monitoring
	Run Keyword If	'${NGVERSION}' >= '5.4'	Append To List	${fields}	block_unsolicited_incoming_packets
	Run Keyword If	'${NGVERSION}' >= '5.6'	Append To List	${fields}	sim-1_apn_configuration
	Run Keyword If	'${NGVERSION}' >= '5.8' and not ${IS_NSR}	Append To List	${fields}	ethernet_link_mode
	CLI:Add
	CLI:Test Set Available Fields	@{fields}
	[Teardown]	CLI:Cancel	Raw

Test show_settings command
	[Tags]	NEED-REVIEW
	Run Keyword If	'${HAS_ETH1}' == '${TRUE}'	CLI:Enter Path	/settings/network_connections/ETH1
	${DNS}=	Run Keyword And Return Status	CLI:Test Show Command Regexp	ipv4_dns_server\\s=\\s\\d+
	CLI:Enter Path	/settings/network_connections/
	${OUTPUT}=	CLI:Show Settings
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Match Regexp	${OUTPUT}	/settings/network_connections/ETH0 name=ETH0
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Match Regexp	${OUTPUT}	/settings/network_connections/ETH0 type=\\w*
	Run Keyword If	'${NGVERSION}' <= '5.8'	Should Match Regexp	${OUTPUT}	/settings/network_connections/ETH0 ethernet_interface=[a-z]
	Should Match Regexp	${OUTPUT}	/settings/network_connections/ETH0 connect_automatically=yes|no
	Should Match Regexp	${OUTPUT}	/settings/network_connections/ETH0 set_as_primary_connection=yes|no
	Should Match Regexp	${OUTPUT}	/settings/network_connections/ETH0 enable_lldp=yes|no
	Should Match Regexp	${OUTPUT}	/settings/network_connections/ETH0 ipv4_mode=dhcp|static|no_ipv4_address
	Should Match Regexp	${OUTPUT}	/settings/network_connections/ETH0 ipv6_mode=address_auto_configuration|stateful_dhcpv6|no_ipv6_address|static
	Run Keyword If	'${NGVERSION}' >= '4.2' and '${HAS_ETH1}' == '${TRUE}'	Should Match Regexp	${OUTPUT}	/settings/network_connections/ETH1 name=ETH1
	Run Keyword If	'${NGVERSION}' >= '4.2' and '${HAS_ETH1}' == '${TRUE}'	Should Match Regexp	${OUTPUT}	/settings/network_connections/ETH1 type=\\w*
	Run Keyword If	'${HAS_ETH1}' == '${TRUE}' and '${NGVERSION}' <= '5.8'	Should Match Regexp	${OUTPUT}	/settings/network_connections/ETH1 ethernet_interface=[a-z]
	Run Keyword If	'${HAS_ETH1}' == '${TRUE}'	Should Match Regexp	${OUTPUT}	/settings/network_connections/ETH1 connect_automatically=yes|no
	Run Keyword If	'${HAS_ETH1}' == '${TRUE}'	Should Match Regexp	${OUTPUT}	/settings/network_connections/ETH1 set_as_primary_connection=yes|no
	Run Keyword If	'${HAS_ETH1}' == '${TRUE}'	Should Match Regexp	${OUTPUT}	/settings/network_connections/ETH1 enable_lldp=yes|no
	Run Keyword If	'${HAS_ETH1}' == '${TRUE}'	Should Match Regexp	${OUTPUT}	/settings/network_connections/ETH1 ipv4_mode=dhcp|static|no_ipv4_address
	Run Keyword If	'${HAS_ETH1}' == '${TRUE}'	Run Keyword if	${DNS}	Should Match Regexp	${OUTPUT}	/settings/network_connections/ETH1 ipv4_dns_server=.*
	Run Keyword If	'${HAS_ETH1}' == '${TRUE}'	Run Keyword if	${DNS}	Should Match Regexp	${OUTPUT}	/settings/network_connections/ETH1 ipv4_dns_search=.*
    ...	ELSE	Run Keyword if	${DNS}	Should Not Match Regexp	${OUTPUT}   /settings/network_connections/ETH1 ipv4_dns_search=.*

	Run Keyword If	'${HAS_ETH1}' == '${TRUE}'	Should Match Regexp	${OUTPUT}	/settings/network_connections/ETH1 ipv6_mode=address_auto_configuration|stateful_dhcpv6|no_ipv6_address|static
	Run Keyword If	'${HAS_ETH1}' == '${TRUE}'	Run Keyword if	${DNS}	Should Match Regexp	${OUTPUT}	/settings/network_connections/ETH1 ipv6_dns_server=.*
	Run Keyword If	'${HAS_ETH1}' == '${TRUE}'	Run Keyword if	${DNS}	Should Match Regexp	${OUTPUT}	/settings/network_connections/ETH1 ipv6_dns_search=.*
	Run Keyword If	'${NGVERSION}' >= '4.2' and not ${IS_VM}	Should Match Regexp	${OUTPUT}	/settings/network_connections/hotspot name=hotspot
	Run Keyword If	'${NGVERSION}' >= '4.2' and not ${IS_VM}	Should Match Regexp	${OUTPUT}	/settings/network_connections/hotspot type=\\w*
	Run Keyword If	not ${IS_VM}	Should Match Regexp	${OUTPUT}	/settings/network_connections/hotspot connect_automatically=yes|no
	Run Keyword If	not ${IS_VM}	Should Match Regexp	${OUTPUT}	/settings/network_connections/hotspot set_as_primary_connection=no|yes
	Run Keyword If	not ${IS_VM}	Should Match Regexp	${OUTPUT}	/settings/network_connections/hotspot enable_lldp=no|yes
	Run Keyword If	not ${IS_VM}	Should Match Regexp	${OUTPUT}	/settings/network_connections/hotspot wifi_ssid=\\*+
	${WIFI_SECURITY_DISABLED}	Run Keyword And Return Status	Should Match Regexp	${OUTPUT}	/settings/network_connections/hotspot wifi_security=disabled
	${WIFI_SECURITY_PERSONAL}	Run Keyword And Return Status	Should Match Regexp	${OUTPUT}	/settings/network_connections/hotspot wifi_security=wpa2_personal
	Run Keyword If	not ${IS_VM}	Should Be True	${WIFI_SECURITY_DISABLED} or ${WIFI_SECURITY_PERSONAL}
	Run Keyword If	not ${IS_VM} and ${WIFI_SECURITY_PERSONAL} and '${NGVERSION}' >= '5.0'	Should Match Regexp	${OUTPUT}	/settings/network_connections/hotspot psk=\\*+
	Run Keyword If	not ${IS_VM} and '${NGVERSION}' <= '4.2'	Should Match Regexp	${OUTPUT}	/settings/network_connections/hotspot psk=\\*+
	Run Keyword If	not ${IS_VM}	Should Match Regexp	${OUTPUT}	/settings/network_connections/hotspot hotspot_ipv4_mode=server|no_ipv4_address
	Run Keyword If	not ${IS_VM}	Should Match Regexp	${OUTPUT}	/settings/network_connections/hotspot shared_ipv4_address=\\d*
	Run Keyword If	not ${IS_VM}	Should Match Regexp	${OUTPUT}	/settings/network_connections/hotspot shared_ipv4_bitmask=\\d*
	Run Keyword If	not ${IS_VM}	Should Match Regexp	${OUTPUT}	/settings/network_connections/hotspot ipv6_mode=address_auto_configuration|stateful_dhcpv6|no_ipv6_address|static

Test valid values for field=arp_validate
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Field Options	arp_validate	none	active	none
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=bonding_mode
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Field Options	bonding_mode	round-robin	active_backup
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Field Options
	...	bonding_mode	802.3ad(lacp)	adaptive_load_balancing
	...	xor_load_balancing	adaptive_transmit_load_balancing	broadcast
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=connect_automatically
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Field Options	connect_automatically	no	no	yes
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=enable_spanning_tree_protocol
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Field Options	enable_spanning_tree_protocol	no	no	yes
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=ipv4_mode
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Field Options	ipv4_mode	dhcp	dhcp	no_ipv4_address	static
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=ipv6_mode
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Field Options	ipv6_mode	no_ipv6_address	address_auto_configuration	no_ipv6_address	stateful_dhcpv6	static
	[Teardown]	CLI:Cancel	Raw]

Test valid values for field=link_monitoring
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Field Options	link_monitoring	mii	arp	mii
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=ppp_authentication
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Field Options	ppp_authentication	none	by_local_system	by_remote_peer	none
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=ppp_ipv4_address
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Field Options	ppp_ipv4_address	no_ipv4_address	local_ipv4	no_ipv4_address	remote_ipv4
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=ppp_ipv6_address
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Field Options	ppp_ipv6_address	no_ipv6_address	local_ipv6	no_ipv6_address	remote_ipv6
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=set_as_primary_connection
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Field Options	set_as_primary_connection	no	no	yes
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=speed
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Field Options	speed	38400	115200	19200	38400	57600	9600
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=status
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Field Options	status	disabled	disabled	enabled
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=type
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Field Options	type	ethernet	analog_modem	bonding	bridge	ethernet	mobile_broadband_gsm	vlan	wifi
	Run Keyword If	${NGVERSION} >= 5.2	CLI:Test Set Field Options	type	loopback
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=wifi_security
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Field Options	wifi_security	disabled	disabled	wpa2_personal
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=bond_fail-over-mac_policy
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Field Options	bond_mac_policy	current_active_interf_	follow	primary_interf
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Field Options	bond_fail-over-mac_policy	current_active_interface	follow_active_interface	primary_interface
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=hidden_network
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Field Options	hidden_network	no	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=name
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	name	~!@	Error: name: This field contains invalid characters.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	name	~!@	Error: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	name	${EXCEEDED}	Error: name: This field contains invalid characters.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	name	${EXCEEDED}	Error: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=ipv4_dns_server
	[Tags]  EXCLUDEIN3_2
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Validate Normal Fields  ipv4_dns_server	${EMPTY}
	CLI:Test Set Validate Normal Fields  ipv4_dns_server		8.8.8.8
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields  ipv4_dns_server	8.8.8.8,8.8.4.4
	[Teardown]  CLI:Cancel  	Raw

Test invalid values for field=ipv4_dns_server
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION}
	CLI:Test Set Field Invalid Options	ipv4_dns_server	~!@	Error: ipv4_dns_server: Invalid IP address.	yes
	CLI:Test Set Field Invalid Options	ipv4_dns_server	${EXCEEDED}	Error: ipv4_dns_server: Invalid IP address.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	 CLI:Test Set Field Invalid Options  ipv4_dns_server  8.8.8.8,8.256.8.8				Error: ipv4_dns_server: Invalid IP address.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	 CLI:Test Set Field Invalid Options  ipv4_dns_server  8.8.8.8,10.20.30.0,8.8.256.4	Error: ipv4_dns_server: Invalid IP address.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	 CLI:Test Set Field Invalid Options  ipv4_dns_server  8.8.8.8,10.20.30.0,8.8::4		Error: ipv4_dns_server: Invalid IP address.	yes
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=ipv6_dns_server
	[Tags]  EXCLUDEIN3_2
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Validate Normal Fields  ipv6_dns_server	${EMPTY}
	CLI:Test Set Validate Normal Fields  ipv6_dns_server	123::123
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields  ipv6_dns_server	123::123,123.76::123
	[Teardown]  CLI:Cancel  	Raw

Test invalid values for field=ipv6_dns_server
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION} ipv6_mode=stateful_dhcpv6
	CLI:Test Set Field Invalid Options	ipv6_dns_server	~!@	Error: ipv6_dns_server: Invalid IP address.	yes
	CLI:Test Set Field Invalid Options	ipv6_dns_server	${EXCEEDED}	Error: ipv6_dns_server: Invalid IP address.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	 CLI:Test Set Field Invalid Options  ipv6_dns_server  8::8,8::8::8				Error: ipv6_dns_server: Invalid IP address.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	 CLI:Test Set Field Invalid Options  ipv6_dns_server  8::8,123::123,8::8::8		Error: ipv6_dns_server: Invalid IP address.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	 CLI:Test Set Field Invalid Options  ipv6_dns_server  8::8,123::123,8.8.8.8		Error: ipv6_dns_server: Invalid IP address.	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=ipv4_mode
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION} ipv4_mode=static
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ipv4_address	~!@	Error: ipv4_address: Invalid IP address.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ipv4_address	~!@	Error: Invalid IP address.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ipv4_address	${EXCEEDED}	Error: ipv4_address: Exceeded the maximum size for this field.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ipv4_address	${EXCEEDED}	Error: Exceeded the maximum size for this field.	yes
	CLI:Set Field	ipv4_address	127.0.0.1
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ipv4_bitmask	~!@	Error: ipv4_bitmask: Invalid mask.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ipv4_bitmask	~!@	Error: Invalid mask.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ipv4_bitmask	${EXCEEDED}	Error: ipv4_bitmask: Exceeded the maximum size for this field.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ipv4_bitmask	${EXCEEDED}	Error: Exceeded the maximum size for this field.	yes
	CLI:Set Field	ipv4_bitmask	255.255.255.255
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ipv4_gateway	~!@	Error: ipv4_gateway: Invalid IP address.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ipv4_gateway	~!@	Error: Invalid IP address.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ipv4_gateway	${EXCEEDED}	Error: ipv4_gateway: Exceeded the maximum size for this field.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ipv4_gateway	${EXCEEDED}	Error: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=ipv6_mode
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION} ipv6_mode=static
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ipv6_address	~!@	Error: ipv6_address: Invalid IP address.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ipv6_address	~!@	Error: Invalid IP address.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ipv6_address	${EXCEEDED}	Error: ipv6_address: Exceeded the maximum size for this field.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ipv6_address	${EXCEEDED}	Error: Exceeded the maximum size for this field.	yes
	CLI:Set Field	ipv6_address	f111::c111:1111:a111:c111
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ipv6_prefix_length	~!@	Error: ipv6_prefix_length: Invalid mask.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ipv6_prefix_length	~!@	Error: Invalid mask.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ipv6_prefix_length	${EXCEEDED}	Error: ipv6_prefix_length: Exceeded the maximum size for this field.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ipv6_prefix_length	${EXCEEDED}	Error: Exceeded the maximum size for this field.	yes
	CLI:Set Field	ipv6_prefix_length	1
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ipv6_gateway	~!@	Error: ipv6_gateway: Invalid IP address.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ipv6_gateway	~!@	Error: Invalid IP address.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ipv6_gateway	${EXCEEDED}	Error: ipv6_gateway: Exceeded the maximum size for this field.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ipv6_gateway	${EXCEEDED}	Error: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

####Type: vlan
Test invalid values for field=vlan_id
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION}
	CLI:Set Field	type	vlan
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	vlan_id	~!@	Error: vlan_id: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	vlan_id	~!@	Error: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

####Type: analog_modem
Test invalid values for field=device_name
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION}
	CLI:Set Field	type	analog_modem
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	device_name	${EMPTY}	Error: device_name: This field is empty or it contains invalid characters.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	device_name	${EMPTY}	Error: This field is empty or it contains invalid characters.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	device_name	~!@	Error: device_name: This field is empty or it contains invalid characters.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	device_name	~!@	Error: This field is empty or it contains invalid characters.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	device_name	${EXCEEDED}	Error: device_name: This field is empty or it contains invalid characters.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	device_name	${EXCEEDED}	Error: This field is empty or it contains invalid characters.	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=ppp_idle_timeout
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION}
	CLI:Set	type=analog_modem device_name=test_dev
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ppp_idle_timeout	${EMPTY}	Error: ppp_idle_timeout: This field only accepts positive integers.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ppp_idle_timeout	${EMPTY}	Error: This field only accepts positive integers.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ppp_idle_timeout	~!@	Error: ppp_idle_timeout: This field only accepts positive integers.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ppp_idle_timeout	~!@	Error: This field only accepts positive integers.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ppp_idle_timeout	${EXCEEDED}	Error: ppp_idle_timeout: This field only accepts positive integers.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ppp_idle_timeout	${EXCEEDED}	Error: This field only accepts positive integers.	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=phone_number
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION}
	CLI:Set	type=analog_modem device_name=test_dev
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	phone_number	a	Error: phone_number: This field contains invalid characters.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	phone_number	a	Error: This field contains invalid characters.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	phone_number	~!@	Error: phone_number: This field contains invalid characters.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	phone_number	~!@	Error: This field contains invalid characters.	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=local_ipv4_address/remote_ipv4_address
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION}
	CLI:Set	type=analog_modem device_name=test_dev ppp_ipv4_address=local_ipv4
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	remote_ipv4_address	~!@	Error: remote_ipv4_address: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	remote_ipv4_address	~!@	Error: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	remote_ipv4_address	${EXCEEDED}	Error: remote_ipv4_address: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	remote_ipv4_address	${EXCEEDED}	Error: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=local_ipv6_address/remote_ipv6_address
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION}
	CLI:Set	type=analog_modem device_name=test_dev ppp_ipv6_address=local_ipv6
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	remote_ipv6_address	~!@	Error: remote_ipv6_address: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	remote_ipv6_address	~!@	Error: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	remote_ipv6_address	${EXCEEDED}	Error: remote_ipv6_address: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	remote_ipv6_address	${EXCEEDED}	Error: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=authentication_protocol
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION}
	CLI:Set	type=analog_modem device_name=test_dev ppp_authentication=by_local_system
	CLI:Test Set Field Options	authentication_protocol	chap	eap	pap
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	authentication_protocol	${EMPTY}	Error: Missing value for parameter: authentication_protocol
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	authentication_protocol	a	Error: Invalid value: a for parameter: authentication_protocol
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	authentication_protocol	${EMPTY}	Error: Missing value: authentication_protocol
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	authentication_protocol	a	Error: Invalid value: a
	[Teardown]	CLI:Cancel	Raw

####Type: bonding
Test invalid values for field=link_down_delay
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION}
	CLI:Set Field	type	bonding
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	link_down_delay	-1	Error: link_down_delay: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	link_down_delay	-1	Error: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	link_down_delay	~!@	Error: link_down_delay: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	link_down_delay	~!@	Error: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	link_down_delay	${EXCEEDED}	Error: link_down_delay: Exceeded the maximum size for this field.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	link_down_delay	${EXCEEDED}	Error: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=link_up_delay
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION}
	CLI:Set	type=bonding
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	link_up_delay	-1	Error: link_up_delay: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	link_up_delay	-1	Error: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	link_up_delay	~!@	Error: link_up_delay: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	link_up_delay	~!@	Error: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	link_up_delay	${EXCEEDED}	Error: link_up_delay: Exceeded the maximum size for this field.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	link_up_delay	${EXCEEDED}	Error: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=monitoring_frequency
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION}
	CLI:Set	type=bonding
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	monitoring_frequency	~!@	Error: monitoring_frequency: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	monitoring_frequency	~!@	Error: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	monitoring_frequency	${EXCEEDED}	Error: monitoring_frequency: Exceeded the maximum size for this field.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	monitoring_frequency	${EXCEEDED}	Error: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=arp_target
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION}
	CLI:Set	type=bonding
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	arp_target	~!@	Error: arp_target: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	arp_target	~!@	Error: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	arp_target	${EXCEEDED}	Error: arp_target: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	arp_target	${EXCEEDED}	Error: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

####Type: bridge
Test valudation for field=enable_spanning_tree_protocol
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION}
	CLI:Set	type=bridge
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	enable_spanning_tree_protocol	${EMPTY}	Error: Missing value for parameter: enable_spanning_tree_protocol
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	enable_spanning_tree_protocol	a	Error: Invalid value: a for parameter: enable_spanning_tree_protocol
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	enable_spanning_tree_protocol	${EMPTY}	Error: Missing value: enable_spanning_tree_protocol
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	enable_spanning_tree_protocol	a	Error: Invalid value: a
	CLI:Test Set Field Options	enable_spanning_tree_protocol	no	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=hello_time
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION}
	CLI:Set	type=bridge
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	hello_time	-1	Error: hello_time: Valid value is a number between 1 and 10.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	hello_time	-1	Error: Valid value is a number between 1 and 10.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	hello_time	~!@	Error: hello_time: Valid value is a number between 1 and 10.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	hello_time	~!@	Error: Valid value is a number between 1 and 10.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	hello_time	${EXCEEDED}	Error: hello_time: Exceeded the maximum size for this field.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	hello_time	${EXCEEDED}	Error: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=max_age
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION}
	CLI:Set	type=bridge
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	max_age	1	Error: max_age: Valid value is a number between 6 and 40.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	max_age	1	Error: Valid value is a number between 6 and 40.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	max_age	~!@	Error: max_age: Valid value is a number between 6 and 40.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	max_age	~!@	Error: Valid value is a number between 6 and 40.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	max_age	${EXCEEDED}	Error: max_age: Exceeded the maximum size for this field.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	max_age	${EXCEEDED}	Error: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=forward_delay
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION}
	CLI:Set	type=bridge
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	forward_delay	1	Error: forward_delay: Valid value is a number between 4 and 30.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	forward_delay	1	Error: Valid value is a number between 4 and 30.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	forward_delay	~!@	Error: forward_delay: Valid value is a number between 4 and 30.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	forward_delay	~!@	Error: Valid value is a number between 4 and 30.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	forward_delay	${EXCEEDED}	Error: forward_delay: Exceeded the maximum size for this field.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	forward_delay	${EXCEEDED}	Error: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

####Type: mobile_broadband_gsm
Test invalid values for field=sim-1_phone_number
	[Tags]  EXCLUDEIN3_2
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION}
	CLI:Set	type=mobile_broadband_gsm
	CLI:Test Set Field Invalid Options	sim-1_phone_number	${WORD}	Error: sim-1_phone_number: This field contains invalid characters.	yes
	CLI:Test Set Field Invalid Options	sim-1_phone_number	${POINTS}	Error: sim-1_phone_number: This field contains invalid characters.	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=sim-1_personal_identification_number
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION}
	CLI:Set	type=mobile_broadband_gsm
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	sim-1_personal_identification_number	${WORD}	Error: sim-1_personal_identification_number: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	personal_identification_number	${WORD}	Error: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	sim-1_personal_identification_number	${POINTS}	Error: sim-1_personal_identification_number: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	personal_identification_number	${POINTS}	Error: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	sim-1_personal_identification_number	${EXCEEDED}	Error: sim-1_personal_identification_number: Exceeded the maximum size for this field.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	personal_identification_number	${EXCEEDED}	Error: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=sim-1_apn_configuration
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Field Options	sim-1_apn_configuration	automatic	manual
	[Teardown]	CLI:Cancel	Raw

#Adding name=mobile	type BUG
Test valid values for mobile_broadbrand_gsm
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION}
	CLI:Set	name=mobile type=mobile_broadband_gsm
	CLI:Commit
	CLI:Enter Path	/settings/network_connections/mobile
	CLI:Test Not Show Command	type: connType.-4

Test valid values for field=ethernet_link_mode
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
	Skip If	${IS_NSR}	Feature is not available in NSR
	Skip If	${IS_VM}	Feature is not available in VMs
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Field Options	ethernet_link_mode	@{LINK_MODES}
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=ethernet_link_mode
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
	Skip If	${IS_NSR}	Feature is not available in NSR
	Skip If	${IS_VM}	Feature is not available in VMs
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Field Invalid Options	ethernet_link_mode	${EMPTY}
	...	Error: Missing value for parameter: ethernet_link_mode

	FOR			${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	ethernet_link_mode	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: ethernet_link_mode
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=system_priority
	[Tags]	EXCLUDEIN3_2
	${INVALID_VALUES}=	Create List	65536	-5	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
	...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
	...	${EIGHT_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
	...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}
	${EXCEEDED_VALUES}=	Create List	${ELEVEN_WORD}	${ELEVEN_POINTS}	${EXCEEDED}
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION} type=bonding bonding_mode=802.3ad(lacp)

	FOR			${INVALID_VALUE}	IN	@{INVALID_VALUES}
		CLI:Test Set Field Invalid Options	system_priority	${INVALID_VALUE}
		...	Error: system_priority: Validation error.	save=yes
	END
	FOR			${EXCEEDED_VALUE}	IN	@{EXCEEDED_VALUES}
		CLI:Test Set Field Invalid Options	system_priority	${EXCEEDED_VALUE}
		...	Error: system_priority: Exceeded the maximum size for this field.	save=yes
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=user_port_key
	[Tags]	EXCLUDEIN3_2
	${INVALID_VALUES}=	Create List	1024	-5	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
	...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
	...	${EIGHT_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
	...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}
	${EXCEEDED_VALUES}=	Create List	${ELEVEN_WORD}	${ELEVEN_POINTS}	${EXCEEDED}
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION} type=bonding bonding_mode=802.3ad(lacp)

	FOR			${INVALID_VALUE}	IN	@{INVALID_VALUES}
		CLI:Test Set Field Invalid Options	user_port_key	${INVALID_VALUE}
		...	Error: user_port_key: Validation error.	save=yes
	END
	FOR			${EXCEEDED_VALUE}	IN	@{EXCEEDED_VALUES}
		CLI:Test Set Field Invalid Options	user_port_key	${EXCEEDED_VALUE}
		...	Error: user_port_key: Exceeded the maximum size for this field.	save=yes
	END
	[Teardown]	CLI:Cancel	Raw

Test Valid Values For Field=lacp_rate
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION} type=bonding bonding_mode=802.3ad(lacp)
	CLI:Test Set Field Options	lacp_rate	fast	slow
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=lacp_rate
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Field Invalid Options	lacp_rate	${EMPTY}
	...	Error: Missing value for parameter: lacp_rate

	FOR			${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	lacp_rate	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: lacp_rate
	END
	[Teardown]	CLI:Cancel	Raw

Test Valid Values For Field=aggregation_selection_logic
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION} type=bonding bonding_mode=802.3ad(lacp)
	CLI:Test Set Field Options	aggregation_selection_logic	bandwith	count	stable
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=aggregation_selection_logic
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Field Invalid Options	aggregation_selection_logic	${EMPTY}
	...	Error: Missing value for parameter: aggregation_selection_logic

	FOR			${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	aggregation_selection_logic	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: aggregation_selection_logic
	END
	[Teardown]	CLI:Cancel	Raw

Test Valid Values For Field=transmit_hash_policy
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION} type=bonding bonding_mode=802.3ad(lacp)
	CLI:Test Set Field Options	transmit_hash_policy	layer_2
	...	layers_2_and_3_and_encap	layers_3_and_4_and_encap	layers_2_and_3
	...	layers_3_and_4
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=transmit_hash_policy
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Field Invalid Options	transmit_hash_policy	${EMPTY}
	...	Error: Missing value for parameter: transmit_hash_policy

	FOR			${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	transmit_hash_policy	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: transmit_hash_policy
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=enable_ip_passthrough
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION} type=${GSM_TYPE}
	CLI:Test Set Field Options	enable_ip_passthrough	no	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=enable_ip_passthrough
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Test Set Field Invalid Options	enable_ip_passthrough	${EMPTY}
	...	Error: Missing value for parameter: enable_ip_passthrough

	FOR			${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	enable_ip_passthrough	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: enable_ip_passthrough
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=ethernet_connection
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION} type=${GSM_TYPE} enable_ip_passthrough=yes
	CLI:Test Set Field Options	ethernet_connection	@{ETHERNET_CONNECTIONS}
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=ethernet_connection
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION} type=${GSM_TYPE} enable_ip_passthrough=yes
	CLI:Test Set Field Invalid Options	ethernet_connection	${EMPTY}
	...	Error: Missing value for parameter: ethernet_connection

	FOR			${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	ethernet_connection	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: ethernet_connection
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=mac_address
	[Tags]	EXCLUDEIN3_2
	Skip If	not ${HAS_SECONDARY_CONNECTION}	IP Passthrough fields cannot be validated without a secondary connection
	${SECONDARY_CONNECTION}=	Get From List	${ETHERNET_CONNECTIONS}	1
	${CONNECTION_UPPER}=	Convert To Uppercase	${SECONDARY_CONNECTION}
	SUITE:Set Network Connection To No Ipv4 Address	${CONNECTION_UPPER}
	${INVALID_VALUES}=	Create List	00:00:00:00:00:00:00	AA:AA:AA:AA:AA:FG	1A:A1:AA:a1:b2
	...	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
	...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
	...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
	...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
	...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
	...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION} type=${GSM_TYPE} enable_ip_passthrough=yes
	CLI:Set	ethernet_connection=${SECONDARY_CONNECTION}

	FOR			${INVALID_VALUE}	IN	@{INVALID_VALUES}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Field Invalid Options	mac_address	${INVALID_VALUE}
		...	Error: mac_address: Validation error.	save=yes
		...	ELSE	CLI:Test Set Field Invalid Options	mac_address	${INVALID_VALUE}
		...	Error: Validation error.	save=yes
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=port_intercepts
	[Tags]	EXCLUDEIN3_2
	Skip If	not ${HAS_SECONDARY_CONNECTION}	IP Passthrough fields cannot be validated without a secondary connection
	${SECONDARY_CONNECTION}=	Get From List	${ETHERNET_CONNECTIONS}	1
	${CONNECTION_UPPER}=	Convert To Uppercase	${SECONDARY_CONNECTION}
	SUITE:Set Network Connection To No Ipv4 Address	${CONNECTION_UPPER}
	${INVALID_VALUES}=	Create List	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
	...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
	...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
	...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION} type=${GSM_TYPE} enable_ip_passthrough=yes
	CLI:Set	ethernet_connection=${SECONDARY_CONNECTION}

	FOR			${INVALID_VALUE}	IN	@{INVALID_VALUES}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Field Invalid Options	port_intercepts	${INVALID_VALUE}
		...	Error: port_intercepts: This field can be empty, or be port numbers (comma separated)	save=yes
		...	ELSE	CLI:Test Set Field Invalid Options	port_intercepts	${INVALID_VALUE}
		...	Error: This field can be empty, or be port numbers (comma separated)	save=yes
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field port_id
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/network_connections/ETH0
	CLI:Set	enable_lldp=yes
	CLI:Test Set Validate Normal Fields	port_id	interface_name	interface_index
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field port_id
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/network_connections/ETH0
	CLI:Set	enable_lldp=yes
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Validate Invalid Options	port_id	${VALUE}	Error: Invalid value: ${VALUE} for parameter: port_id
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field port_description
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/network_connections/ETH0
	CLI:Set	enable_lldp=yes
	CLI:Test Set Validate Normal Fields	port_description	interface_name	interface_description
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field port_description
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/network_connections/ETH0
	CLI:Set	enable_lldp=yes
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Validate Invalid Options	port_description	${VALUE}	Error: Invalid value: ${VALUE} for parameter: port_description
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field hotspot_ipv4_mode
	[Tags]	NON-CRITICAL
	[Documentation]	Test all possible values for field hotspot_ipv4_mode
	CLI:Enter Path	/settings/network_connections/hotspot
	CLI:Set	wifi_security=wpa2_personal psk=${SECRET}
	CLI:Test Set Validate Normal Fields	hotspot_ipv4_mode	no_ipv4_address	server
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field hotspot_ipv4_mode
	[Tags]	NON-CRITICAL
	[Documentation]	Test field hotspot_ipv4_mode does not accept values other than predefined ones
	CLI:Enter Path	/settings/network_connections/hotspot
	CLI:Set	wifi_security=wpa2_personal psk=${SECRET}
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Validate Invalid Options	hotspot_ipv4_mode	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: hotspot_ipv4_mode
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field hotspot_ipv6_mode
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
	[Documentation]	Test all possible values for field hotspot_ipv6_mode
	CLI:Enter Path	/settings/network_connections/hotspot
	CLI:Test Set Validate Normal Fields	hotspot_ipv6_mode	no_ipv6_address	dhcpv6_prefix_delegation
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field hotspot_ipv6_mode
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
	[Documentation]	Test field hotspot_ipv6_mode does not accept vlues other than predefined ones
	CLI:Enter Path	/settings/network_connections/hotspot
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Validate Invalid Options	hotspot_ipv6_mode	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: hotspot_ipv6_mode
	END
	[Documentation]
	[Teardown]	CLI:Cancel	Raw

Test valid values for field psk
	[Tags]	NON-CRITICAL
	[Documentation]	Test pre-shared key for wifi has to have more than 8 characters and accepts points
	@{VALID_PSKS}	Create List	${SECRET}	${QA_PASSWORD}	V3ryC0mpl3x!
	CLI:Enter Path	/settings/network_connections/hotspot
	FOR	${PSK}	IN	@{VALID_PSKS}
		CLI:Set	wifi_security=wpa2_personal psk=${SECRET}
		CLI:Test Set Field Valid Options	psk	${PSK}	passwordfield=yes
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field psk
	[Tags]	NON-CRITICAL
	[Documentation]	Test psk for wifi security cannot have less than 8 characters and more than 63.
	@{INVALID_PSKS}	Create List	psk	secret	abcdef	123456	a1b2c3	${EMPTY}	${EXCEEDED}
	CLI:Enter Path	/settings/network_connections/hotspot
	CLI:Set	wifi_security=wpa2_personal psk=${SECRET}
	FOR	${PSK}	IN	@{INVALID_PSKS}
		CLI:Test Set Validate Invalid Options	psk	${PSK}
		...	Error: psk: Validation error.
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field region
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
	[Documentation]	Test all possible values for region field
	CLI:Enter Path	/settings/network_connections/hotspot
	CLI:Test Set Validate Normal Fields	region
	...	00  au  bm  ci  dm  gb  hk  it  kz  md  mu  om  pw  si  tr  vi
	...	ad  aw  bn  cl  do  gd  hn  jm  lb  me  mv  pa  py  sk  tt  vn
	...	ae  az  bo  cn  dz  ge  hr  jo  lc  mf  mw  pe  qa  sn  tw  vu
	...	af  ba  br  co  ec  gf  ht  jp  li  mh  mx  pf  re  sr  tz  wf
	...	ai  bb  bs  cr  ee  gh  hu  ke  lk  mk  my  pg  ro  sv  ua  ws
	...	al  bd  bt  cu  eg  gl  id  kh  ls  mn  ng  ph  rs  sy  ug  ye
	...	am  be  by  cx  es  gp  ie  kn  lt  mo  ni  pk  ru  tc  us  yt
	...	an  bf  bz  cy  et  gr  il  kp  lu  mp  nl  pl  rw  td  uy  za
	...	ar  bg  ca  cz  fi  gt  in  kr  lv  mq  no  pm  sa  tg  uz  zw
	...	as  bh  cf  de  fm  gu  ir  kw  ma  mr  np  pr  se  th  vc
	...	at  bl  ch  dk  fr  gy  is  ky  mc  mt  nz  pt  sg  tn  ve
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field region
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
	[Documentation]	Test field region does not accept values other than the predefined ones
	CLI:Enter Path	/settings/network_connections/hotspot
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Validate Invalid Options	region	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: region
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field shared_ipv4_bitmask
	[Tags]	NON-CRITICAL
	[Documentation]	Test shared_ipv4_bitmask accepts integer values from 0 to 32
	CLI:Enter Path	/settings/network_connections/hotspot
	@{VALID_NETMASKS}	Create List	0	8	16	24	32
	FOR	${MASK}	IN	@{VALID_NETMASKS}
		CLI:Set	wifi_security=wpa2_personal psk=${SECRET}
		CLI:Test Set Field Valid Options	shared_ipv4_bitmask	${MASK}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field shared_ipv4_bitmask
	[Tags]	NON-CRITICAL
	[Documentation]	Test shared_ipv4_bitmask field does not accept values with less than ten digits points, letters and
	...	special characters. Also tests that any value with more than ten digits are not accepted.
	CLI:Enter Path	/settings/network_connections/hotspot
	CLI:Set	wifi_security=wpa2_personal psk=${SECRET}
	Remove Values From List	${ALL_VALUES}	${ONE_NUMBER}	${TWO_NUMBER}	${ELEVEN_NUMBER}	${EXCEEDED}
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Validate Invalid Options	shared_ipv4_bitmask	${VALUE}
		...	Error: netmask: Invalid mask.
	END
	@{VALUES_EXCEED}	Create List	${ELEVEN_NUMBER}	${EXCEEDED}
	FOR	${VALUE}	IN	@{VALUES_EXCEED}
		CLI:Test Set Validate Invalid Options	shared_ipv4_bitmask	${VALUE}
		...	Error: netmask: Exceeded the maximum size for this field.
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field wifi_band
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
	[Documentation]	Test field wifi_band accepts all possible values
	CLI:Enter Path	/settings/network_connections/hotspot
	CLI:Test Set Validate Normal Fields	wifi_band	2_4ghz	5ghz
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field wifi_band
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
	[Documentation]	Test field wifi_band does not accepts other values than predefined ones
	CLI:Enter Path	/settings/network_connections/hotspot
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Validate Invalid Options	wifi_band	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: wifi_band
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field wifi_security
	[Tags]	NON-CRITICAL
	[Documentation]	Test field wifi_security accepts all possible values acconding to NG version
	CLI:Enter Path	/settings/network_connections/hotspot
	CLI:Test Set Validate Normal Fields	wifi_security	disabled	wpa2_personal
	IF	'${NGVERSION}' >= '5.2'
		#workaround because when wifi_security=wpa2_enterprise, radius secret should be set at the same time
		CLI:Set	wifi_security=wpa2_enterprise radius_secret=${SECRET}
		${OUTPUT}	CLI:Show	user=Yes
		Should Contain	${OUTPUT}	wifi_security = wpa2_enterprise
	END
	Run Keyword If	'${NGVERSION}' > '5.8'	CLI:Test Set Validate Normal Fields	wifi_security	wpa3_personal
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field wifi_security
	[Tags]	NON-CRITICAL
	[Documentation]	Test wifi_security field does not accept other values than predefined ones
	CLI:Enter Path	/settings/network_connections/hotspot
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Validate Invalid Options	wifi_security	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: wifi_security
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field wifi_ssid
	[Tags]	NON-CRITICAL
	[Documentation]	Test wifi_ssid field accepts all types of values and as many digits as wanted
	CLI:Enter Path	/settings/network_connections/hotspot
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Valid Options	wifi_ssid	${VALUE}	passwordfield=yes
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field wifi_ssid
	[Tags]	NON-CRITICAL
	[Documentation]	Test wifi_ssid field cannot be empty
	CLI:Enter Path	/settings/network_connections/hotspot
	${EXPECTED_ERROR}	CLI:Write	set wifi_ssid=${EMPTY}	Raw
	Should Contain	${EXPECTED_ERROR}	Error: wifi_ssid: Validation error.
	[Teardown]	CLI:Cancel	Raw

Test valid values for field radius_port
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
	[Documentation]	Test field radius_port accepts only integers with less than 10 digits
	CLI:Enter Path	/settings/network_connections/hotspot
	@{VALID_PORTS}	Create List	${ONE_NUMBER}	${TWO_NUMBER}	${THREE_NUMBER}	1812
	FOR	${PORT}	IN	@{VALID_PORTS}
		CLI:Set	wifi_security=wpa2_enterprise radius_secret=${SECRET}
		CLI:Set	radius_port=${PORT} radius_secret=${SECRET}
		${OUTPUT}	CLI:Show
		Should Contain	${OUTPUT}	radius_port = ${PORT}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field radius_port
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
	[Documentation]	Test radius port does not accepts letters, points and special characters
	CLI:Enter Path	/settings/network_connections/hotspot
	Remove Values From List	${ALL_VALUES}	${ONE_NUMBER}	${TWO_NUMBER}	${THREE_NUMBER}	${ELEVEN_WORD}
	...	${ELEVEN_NUMBER}	${ELEVEN_POINTS}	${EXCEEDED}
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Set	wifi_security=wpa2_enterprise radius_secret=${SECRET}
		${OUTPUT}	CLI:Write	set radius_port=${VALUE} radius_secret=${SECRET}	Raw
		Should Contain	${OUTPUT}	Error: radius_port: Invalid port.
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field radius_secret
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
	[Documentation]	Test field radius_secret accepts any string with less than 63 characters
	@{VALID_SECRETS}	Create List	psk	secret	${SECRET}	${QA_PASSWORD}	V3ryC0mpl3x!
	CLI:Enter Path	/settings/network_connections/hotspot
	FOR	${VALUE}	IN	@{VALID_SECRETS}
		CLI:Set	wifi_security=wpa2_enterprise radius_secret=${SECRET}
		CLI:Test Set Field Valid Options	radius_secret	${VALUE}	passwordfield=yes
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field radius_secret
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
	[Documentation]	Test field radius_secret cannot be empty nor have more than 63 characters
	@{INVALID_SECRETS}	Create List	${EMPTY}	${EXCEEDED}
	CLI:Enter Path	/settings/network_connections/hotspot
	CLI:Set	wifi_security=wpa2_enterprise radius_secret=${SECRET}
	FOR	${VALUE}	IN	@{INVALID_SECRETS}
		CLI:Test Set Validate Invalid Options	radius_secret	${VALUE}
		...	Error: radius_secret: Validation error.
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field radius_server
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
	[Documentation]	Test radius_server field accepts IP addresses
	CLI:Enter Path	/settings/network_connections/hotspot
	${VALID_SERVERS}	Create List	${RADIUSSERVER}	${RADIUSSERVER2}	${RADIUSSERVER3}
	FOR	${VALUE}	IN	@{VALID_SERVERS}
		CLI:Set	wifi_security=wpa2_enterprise radius_secret=${SECRET}
		CLI:Set	radius_server=${VALUE} radius_secret=${SECRET}
		${OUTPUT}	CLI:Show
		Should Contain	${OUTPUT}	radius_server = ${VALUE}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field radius_server
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
	[Documentation]	Test radius server does not accpet strings, points and special characters
	Remove Values From List	${ALL_VALUES}	${EXCEEDED}
	CLI:Enter Path	/settings/network_connections/hotspot
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Set	wifi_security=wpa2_enterprise radius_secret=${SECRET}
		${OUTPUT}	CLI:Write	set radius_server=${VALUE} radius_secret=${SECRET}	Raw
		Should Contain	${OUTPUT}	Error: radius_server: Invalid IP address.
	END

	[Teardown]	CLI:Cancel	Raw

Test valid values for field wpa2_method
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
	[Documentation]	Test field wpa2_method accepts all possible values
	CLI:Enter Path	/settings/network_connections/hotspot
	CLI:Set	wifi_security=wpa2_enterprise radius_secret=${SECRET}
	#workaround because when wifi_security=wpa2_enterprise, radius secret should be set at the same time
	CLI:Set	wpa2_method=peap radius_secret=${SECRET}
	${OUTPUT}	CLI:Show	user=Yes
	Should Contain	${OUTPUT}	wpa2_method = peap
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field wpa2_method
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
	[Documentation]	Test field wpa2_method does not accept other values than the predfined ones
	CLI:Enter Path	/settings/network_connections/hotspot
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Validate Invalid Options	wpa2_method	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: wpa2_method
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field hidden_network
	[Tags]	NON-CRITICAL
	[Documentation]	Test hidden network can be only yes or no
	CLI:Enter Path	/settings/network_connections
	CLI:Add
	CLI:Set	type=wifi
	CLI:Test Set Validate Normal Fields	hidden_network	yes	no
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field hidden_network
	[Tags]	NON-CRITICAL
	[Documentation]	Test hidden_network field does not accept any other value other than yes or no
	CLI:Enter Path	/settings/network_connections
	CLI:Add
	CLI:Set	type=wifi
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Validate Invalid Options	hidden_network	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: hidden_network
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field wifi_bssid
	[Tags]	NON-CRITICAL
	[Documentation]	Test wifi_bssid field accepts numbers, letters, points and special characters
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=wifi
	FOR	${VALUE}	IN	@{ALL_VALUES}
		#workaround
		#CLI:Test Set Field Valid Options	wifi_bssid	${VALUE}	revert=no	passwordfield=yes
		CLI:Set	wifi_bssid=${VALUE}
		${OUTPUT}	CLI:Show
		Should Contain	${OUTPUT}	wifi_ssid = *
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field wpa2_password
	[Tags]	NON-CRITICAL
	[Documentation]	Test wpa2_password field accepts numbers, letters, points and special characters
	CLI:Enter Path	/settings/network_connections
	CLI:Add
	CLI:Set	type=wifi wifi_security=wpa2_enterprise
	FOR	${VALUE}	IN	@{ALL_VALUES}
		#CLI:Test Set Field Valid Options	wpa2_password	${VALUE}	revert=no	passwordfield=yes
		#workaround
		CLI:Set	wpa2_password=${VALUE}
		${OUTPUT}	CLI:Show
		Should Contain	${OUTPUT}	wpa2_password = *
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field wpa2_password
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
	[Documentation]	Test wpa2_passwotd cannot be empty
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	connect_automatically=no
	CLI:Set	type=wifi wifi_security=wpa2_enterprise wpa2_username=${DEFAULT_USERNAME}
	CLI:Set	wpa2_password=${EMPTY}
	${EXPECTED_ERROR}	CLI:Commit	Raw
	Should Contain	${EXPECTED_ERROR}	Error: wpa2_password: Validation error.
	[Teardown]	CLI:Cancel	Raw

Test valid values for field wpa2_phase2_auth
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
	[Documentation]	Test field wpa2_phase2_auth accepts all possible values
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=wifi wifi_security=wpa2_enterprise
	CLI:Test Set Validate Normal Fields	wpa2_phase2_auth	mschapv2
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field wpa2_phase2_auth
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
	[Documentation]	Test field wpa2_phase2_auth does not accept values other than predefined ones
	CLI:Enter Path	/settings/network_connections
	CLI:Add
	CLI:Set	type=wifi wifi_security=wpa2_enterprise
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Validate Invalid Options	wpa2_phase2_auth	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: wpa2_phase2_auth
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field wpa2_username
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
	[Documentation]	Test wpa2_username accepts numbers, letters, points and special characters
	CLI:Enter Path	/settings/network_connections
	CLI:Add
	CLI:Set	type=wifi wifi_security=wpa2_enterprise
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Valid Options	wpa2_username	${VALUE}	revert=no
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field wpa2_username
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
	[Documentation]	Test wpa2_username cannot be empty
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	connect_automatically=no
	CLI:Set	type=wifi wifi_security=wpa2_enterprise wpa2_password=${FACTORY_PASSWORD}
	CLI:Set	wpa2_username=${EMPTY}
	${EXPECTED_ERROR}	CLI:Commit	Raw
	Should Contain	${EXPECTED_ERROR}	Error: wpa2_username: Validation error.
	[Teardown]	CLI:Cancel	Raw

Test valid values for field wpa2_validate_server_certificate
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
	[Documentation]	Test wpa2_validate_server_certificate option can only be yes or no
	CLI:Enter Path	/settings/network_connections
	CLI:Add
	CLI:Set	type=wifi wifi_security=wpa2_enterprise
	CLI:Test Set Validate Normal Fields	wpa2_validate_server_certificate	yes	no
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field wpa2_validate_server_certificate
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
	[Documentation]	Test wpa2_validate_server_certificate field does not accept values other than yes or no
	CLI:Enter Path	/settings/network_connections
	CLI:Add
	CLI:Set	type=wifi wifi_security=wpa2_enterprise
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Validate Invalid Options	wpa2_validate_server_certificate	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: wpa2_validate_server_certificate
	END
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	${IS_NSR}=	CLI:Is Net SR
	Set Suite Variable	${IS_NSR}
	${IS_VM}=	CLI:Is VM System
	Set Suite Variable	${IS_VM}
	CLI:Enter Path	/settings/network_connections/
	CLI:Delete If Exists	mobile	${CONNECTION}

	${HAS_ETH1}=	SUITE:Check If Device Has ETH1 With IP
	Set Suite Variable	${HAS_ETH1}	${HAS_ETH1}
	${LINK_MODES}=	CLI:Get Supported Ethernet Link Modes
	Set Suite Variable	${LINK_MODES}
	${CONNECTIONS}=	CLI:Get Builtin Network Connections	Yes
	Set Suite Variable	${CONNECTIONS}
	${NUMBER_OF_CONNECTIONS}=	Get Length	${CONNECTIONS}
	${HAS_SECONDARY_CONNECTION}=	Run Keyword And Return Status
	...	Should Be True	${NUMBER_OF_CONNECTIONS} >= 2
	Set Suite Variable	${HAS_SECONDARY_CONNECTION}
	${ETHERNET_CONNECTIONS}	Create List
	Set Suite Variable	${ETHERNET_CONNECTIONS}
	FOR	${ELEMENT}	IN	@{CONNECTIONS}
		IF	'${NGVERSION}' >= '5.6'
			${ELEMENT}	Convert To Uppercase	${ELEMENT}
		END
		Append To list	${ETHERNET_CONNECTIONS}	${ELEMENT}
	END

SUITE:Teardown
	CLI:Enter Path	/settings/network_connections/
	CLI:Delete If Exists	mobile	${CONNECTION}
	CLI:Close Connection

CLI:Get Type
	[Arguments]	${OUTPUT}
	${TYPE}=	Fetch From Right	${OUTPUT}	type:${SPACE}
	${TYPE}=	Fetch From Right	${TYPE}	${\n}
	[Return]	${TYPE}

SUITE:Set Network Connection To No Ipv4 Address
	[Arguments]	${CONNECTION}
	CLI:Enter Path	/settings/network_connections/${CONNECTION}
	CLI:Set	ipv4_mode=no_ipv4_address
	CLI:Commit

SUITE:Check If Device Has ETH1 With IP
	CLI:Enter Path	/settings/network_connections
	${CONNECTIONS}	CLI:Show
	${HAS_ETH1}	Run Keyword And Return Status	Should Match Regexp	${CONNECTIONS}	eth1\\s+up\\s+192.168
	[Return]	${HAS_ETH1}
