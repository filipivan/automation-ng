*** Settings ***
Resource        ../../init.robot
Documentation   Configuration tests through the CLI about AT for 802.1x authentication to be available for Wifi AP
Metadata        Version  1.0
Metadata        Executed At  ${HOST}
Force Tags      CLI  EXCLUDEIN3_2  EXCLUDEIN4_2   WIFI
Default Tags    CLI  SSH

Suite Setup     SUITE:Setup
Suite Teardown  SUITE:Teardown

*** Test Cases ***
Test Wifi Security With 802.1x Authentication And Check If On Root File Was Configured
    [Setup]                      SUITE:Enable Wifi Security With 802.1x Authentication
    Set Client Configuration     prompt=#
    CLI:Write                    shell sudo su -
    ${OUTPUT}=                   CLI:Write   cat /etc/NetworkManager/system-connections/hotspot
    Should Contain               ${OUTPUT}  [802-1x]  auth-server-addr = 222.222.222.222   auth-server-port = 4321
                                 ...  auth-server-shared-secret = testing321
    [Teardown]                   SUITE:Set Client Configurations And Exit

Test Disable Wifi Security And Check If On Root File Was Not Configured
    [Setup]                      SUITE:Disable Wifi Security
    Set Client Configuration     prompt=#
    CLI:Write                    shell sudo su -
    ${OUTPUT}=                   CLI:Write   cat /etc/NetworkManager/system-connections/hotspot
    Should Not Contain           ${OUTPUT}  [802-1x]  auth-server-addr = 222.222.222.222   auth-server-port = 4321
                                 ...  auth-server-shared-secret = testing321
    [Teardown]                   SUITE:Set Client Configurations And Exit

*** Keywords ***
SUITE:Setup
    CLI:Open
    SUITE:Check If The System Has Hotspot
    Skip If    '${HAS_HOTSPOT}' == '${FALSE}'    System doesn't have hotspot network
    SUITE:Disable Wifi Security

SUITE:Teardown
    Run Keyword If	${HAS_HOTSPOT}	SUITE:Disable Wifi Security
    CLI:Close Connection

SUITE:Check If The System Has Hotspot
    ${OUTPUT}=              CLI:Enter Path  /settings/network_connections/hotspot    RAW_Mode=Yes
    ${HAS_HOTSPOT}=         Run Keyword And Return Status   Should Not Contain  ${OUTPUT}
    ...  Error: Invalid path: hotspot
    Set Suite Variable      ${HAS_HOTSPOT}

SUITE:Set Client Configurations And Exit
    Set Client Configuration  prompt=]#
    CLI:Write  exit

SUITE:Enable Wifi Security With 802.1x Authentication
    CLI:Enter Path          /settings/network_connections/hotspot
    CLI:Write               set wifi_security=wpa2_enterprise radius_server=222.222.222.222 radius_port=4321 radius_secret=testing321
    CLI:Commit
    CLI:Test Show Command   wifi_security = wpa2_enterprise  wpa2_method = peap  radius_server = 222.222.222.222
    ...  radius_port = 4321  radius_secret = ********
    [Teardown]              CLI:Revert  Raw

SUITE:Disable Wifi Security
    CLI:Enter Path          /settings/network_connections/hotspot
    CLI:Write               set wifi_security=disabled
    CLI:Commit
    CLI:Test Show Command   wifi_security = disabled
    [Teardown]              CLI:Revert  Raw