*** Settings ***
Resource	../../init.robot
Documentation	Tests for heartbeat for cellular module functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	 EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	NON-CRITICAL
Default Tags	CLI	SSH

Suite Setup 	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CELLULAR_CONNECTION}	GSM_CELLULAR_CONNECTION
${TYPE} 	mobile_broadband_gsm
${STRONG_PASSWORD}	${QA_PASSWORD}

*** Test Cases ***
Test case to Add new GSM connection
    Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
    Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
    CLI:Switch Connection    default
    SUITE:Add GSM Connection
    SUITE:Check task is automatically created
    SUITE:Connection Down to validate Ensure Connection Up functionality

Test case to Decrease the time for the health check
    Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
    Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
    CLI:Switch Connection    default
    CLI:Enter Path   /settings/scheduler
    CLI:Write  clone ${CELLULAR_CONNECTION}_healthCheck
    CLI:Set  task_name=heart-beat_cloned
    CLI:Commit
    CLI:Switch Connection   root_session
    CLI:Enter Path    /etc/cron.d/
    CLI:Write   sed -i 's/^.*root/\\t*\\t*\\t*\\t*\\t*\\troot/' heart-beat_cloned
    CLI:Write   /etc/init.d/ccm restart
    SUITE:Health Check worked as expected

Test case for Change non-reachable IP Address in Health Monitoring
    Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
    Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
    CLI:Switch Connection    default
    CLI:Enter Path  /settings/network_connections/${CELLULAR_CONNECTION}
    CLI:Set     ip_address=1.2.3.4
    CLI:Commit
    CLI:Switch Connection    root_session
    ${OUTPUT}=    CLI:Write    cat /var/lib/cron/schedule.tasks
    Should Not Contain   ${OUTPUT}    Errors
    ${OUTPUT}=    CLI:Write   cat /var/local/EVT/nodegrid
    Should Contain  ${OUTPUT}   Event ID 147:
    CLI:Switch Connection    default

Test if integration with failover feature is not raising false-positives
    Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
    Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
    CLI:Switch Connection    default
    CLI:Enter Path  /settings/network_connections/${CELLULAR_CONNECTION}
    CLI:Enter Path   /settings/network_settings/
    CLI:Set     enable_network_failover=yes primary_connection=ETH0 secondary_connection=${CELLULAR_CONNECTION}
    CLI:Commit
    CLI:Enter Path   /settings/network_connections/${CELLULAR_CONNECTION}
    CLI:Set   ip_address=8.8.8.8
    CLI:Commit
    CLI:Write      down_connection    ${CELLULAR_CONNECTION}
    CLI:Switch Connection   root_session
    ${OUTPUT}=    CLI:Write   cat /var/local/EVT/nodegrid
    Should Not Contain    ${OUTPUT}     Event ID 144:

Test case to Unable Network Failover
    Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
    Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
    CLI:Switch Connection    default
    CLI:Enter Path    /settings/network_settings/
    CLI:Set  enable_network_failover=no
    CLI:Commit

Test case to delete the connection and schedular task
    Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
    Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
    CLI:Delete Network Connections	${CELLULAR_CONNECTION}
    CLI:Enter Path  /settings/scheduler
    CLI:Write    delete heart-beat_cloned
    CLI:Commit
    ${OUTPUT}=    CLI:Show
    Should Not Contain    ${OUTPUT}    heart-beat_cloned

*** Keywords ***
SUITE:Setup
    CLI:Open
    CLI:Switch Connection	default
    ${HAS_WMODEM_SUPPORT}=	CLI:Has Wireless Modem Support
    Set Suite Variable	${HAS_WMODEM_SUPPORT}
    Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
    ${HAS_WMODEM}=	CLI:Has Wireless Modem With SIM Card
    Set Suite Variable	${HAS_WMODEM}	${HAS_WMODEM}
    Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
    ${HAS_WMODEM}	${MODEM_INFO}=	CLI:Get Wireless Modems SIM Card Info	0
    Set Suite Variable	${HAS_WMODEM}
    Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
    Set Suite Variable	${MODEM_INFO}
    CLI:Delete Network Connections	${CELLULAR_CONNECTION}
    CLI:Change User Password From Shell	root	${STRONG_PASSWORD}	${ROOT_PASSWORD}
    CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${STRONG_PASSWORD}	${STRONG_PASSWORD}
    CLI:Connect As Root    PASSWORD=${STRONG_PASSWORD}

SUITE:Teardown
	Skip if	not ${HAS_WMODEM_SUPPORT}
	Skip if	not ${HAS_WMODEM}
	Run Keywords	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}	${STRONG_PASSWORD}	AND
    ...	CLI:Change User Password From Shell	root	${ROOT_PASSWORD}	${STRONG_PASSWORD}
    CLI:Open
    CLI:Delete Network Connections	${CELLULAR_CONNECTION}
    CLI:Close Connection

SUITE:Add GSM Connection
    CLI:Enter Path	/settings/network_connections/
    CLI:Add
    CLI:Set	name=${CELLULAR_CONNECTION} type=${TYPE}
    CLI:Set	ethernet_interface=${MODEM_INFO["interface"]} ipv4_mode=dhcp ipv6_mode=no_ipv6_address
    Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Set	sim-1_apn_configuration=manual
    CLI:Set	sim-1_access_point_name=${MODEM_INFO["apn"]}
    CLI:Set	 enable_connection_health_monitoring=yes
    CLI:Set  ensure_connection_is_up=yes
    CLI:Commit

SUITE:Connection Down to validate Ensure Connection Up functionality
    CLI:Enter Path	/settings/network_connections/
    CLI:Write      down_connection    ${CELLULAR_CONNECTION}
    CLI:Commit
    CLI:Write   show
    ${OUTPUT}=    CLI:Show
    Should Contain    ${OUTPUT}    ${CELLULAR_CONNECTION}

SUITE:Check task is automatically created
    CLI:Enter Path  /settings/scheduler
    CLI:Write   show
    ${OUTPUT}=    CLI:Show
    Should Contain    ${OUTPUT}     ${CELLULAR_CONNECTION}_healthCheck

SUITE:Health Check worked as expected
    ${OUTPUT}=    CLI:Write   cat /var/lib/cron/schedule.tasks
    Should Not Contain   ${OUTPUT}    Errors
    ${OUTPUT}=  CLI:Write    cat /var/local/EVT/nodegrid
    Should Contain    ${OUTPUT}    Event ID 140:
    Should Contain    ${OUTPUT}    Event ID 146:
    CLI:Switch Connection    default