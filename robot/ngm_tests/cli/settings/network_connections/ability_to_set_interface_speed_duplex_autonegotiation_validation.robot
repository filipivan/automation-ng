*** Settings ***
Resource	../../init.robot
Documentation	Tests for set interface speed,duplex and autonegotiation settings validation
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	 EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
Default Tags	CLI	SSH

Suite Setup 	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${INTF_CONNECTED}    ETH0
@{AVAILABLE_SPEEDS}    100m|full  100m|half  10m|full   10m|half   1g|full    auto

*** Test Cases ***
Test case to validate link mode field exist and supported modes
    CLI:Switch Connection	peer_session
    CLI:Enter Path	/settings/network_connections/${INTF_CONNECTED}
    CLI:Test Set Available Fields   Link Mode:
    CLI:Cancel
    CLI:Switch Connection   root_session
    ${OUTPUT}=    CLI:Write   ethtool eth0
    Should Contain    ${OUTPUT}  Supported link modes:   10baseT/Half 10baseT/Full  100baseT/Half 100baseT/Full   1000baseT/Full

Test case to set speed and duplex
    CLI:Switch Connection	peer_session
    CLI:Enter Path	/settings/network_connections/ETH0
     FOR	${AVAILABLE_SPEED}  IN  @{AVAILABLE_SPEEDS}
        CLI:Set  ethernet_link_mode=${AVAILABLE_SPEED}
        ${OUTPUT}=   CLI:Commit
        Should Not Contain   ${OUTPUT}   Error
    END

*** Keywords ***
SUITE:Setup
    CLI:Open  session_alias=peer_session
	${HOST_IS_NSR}=	CLI:Is Net SR
	Skip If	${HOST_IS_NSR}	Host should not be a NSR
    CLI:Connect As Root

SUITE:Teardown
    CLI:Close Connection


