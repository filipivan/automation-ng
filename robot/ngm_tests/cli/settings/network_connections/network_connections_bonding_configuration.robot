*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Network Connections Configurations for type Bridge through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN5_0
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CUSTOM_MAC_1}	00:11:22:33:44:55
${CUSTOM_MAC_2}	00:ff:ee:dd:bb:aa

${BOND_INTERFACE}	eth1
${BOND_NAME}	test_bond

*** Test Cases ***
Test Set ETH0 as Primary Bonding Interface
	${HAS_ETH1}	CLI:Has Interface	eth1
	Skip If	not ${HAS_ETH1}
	${IP}	SUITE:Check Interface	eth0
	SUITE:Add Bonding Connection	bonding_eth0	${IP}	eth0
	SUITE:Delete Interface	bonding_eth0
	[Teardown]	CLI:Cancel	raw

Test Set ETH1 as Primary Bonding Interface
	[Tags]	NON-CRITICAL	BUG_NG_8803	BUG_NG_8741	#new_test
	${IP}	SUITE:Check Interface	eth1
	SUITE:Add Bonding Connection	bonding_eth1	${IP}	eth1
	SUITE:Delete Interface	bonding_eth1
	[Teardown]	CLI:Cancel	raw

Test Set BACKPLANE0 as Primary Bonding Interface
	[Tags]	NON-CRITICAL	#new_test
	${IP}	SUITE:Check Interface	backplane0
	SUITE:Add Bonding Connection	bonding_backplane0	${IP}	backplane0
	SUITE:Delete Interface	bonding_backplane0
	[Teardown]	CLI:Cancel	raw

Test Set BACKPLANE1 as Primary Bonding Interface
	[Tags]	NON-CRITICAL	#new_test
	${IP}	SUITE:Check Interface	backplane1
	SUITE:Add Bonding Connection	bonding_backplane1	${IP}	backplane1
	SUITE:Delete Interface	bonding_backplane1
	[Teardown]	CLI:Cancel	raw

Test Set HOTSPOT as Primary Bonding Interface
	[Tags]	NON-CRITICAL	#new_test
	${IP}	SUITE:Check Interface	hotspot
	SUITE:Add Bonding Connection	bonding_hotspot	${IP}	hotspot
	SUITE:Delete Interface	bonding_hotspot
	[Teardown]	CLI:Cancel	raw

Test Change Primary Bonding Interface
	[Tags]	NON-CRITICAL	BUG_NG_8803	BUG_NG_8741	#new_test
	CLI:Delete If Exists	bonding_eth0	bonding_eth1
	${IP}	SUITE:Check Interface	eth1
	SUITE:Add Bonding Connection	bonding_eth0	${HOST}	eth0
	SUITE:Edit Bonding Primary Interface	eth0	eth1	${IP}
	SUITE:Delete Interface	bonding_eth0
	[Teardown]	CLI:Cancel	raw

Test add bonding connection with custom MAC address
	[Documentation]	Test adds a bonding connection with only ${BOND_INTERFACE} as slave and ${CUSTOM_MAC_1} as MAC
	...	 address, then check if this custom address was really applied in NG context with show command
	[Tags]	NON-CRITICAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	SUITE:Add Bonding With Custom MAC	${CUSTOM_MAC_1}
	@{REQUIRED_INFO}	Create List	${BOND_NAME}	bonding	bond0	${CUSTOM_MAC_1}
	${HAS_CUSTOM_MAC}	CLI:Show	/settings/network_connections
	CLI:Should Contain All	${HAS_CUSTOM_MAC}	${REQUIRED_INFO}
	[Teardown]	CLI:Delete If Exists	${BOND_NAME}

Test check if custom MAC address is configured correctly with ifconfig
	[Documentation]	Test adds a bonding connection with only ${BOND_INTERFACE} as slave and ${CUSTOM_MAC_1} as MAC
	...	 address, then check if this custom address was really applied in network context with ifconfig command
	[Tags]	NON-CRITICAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	SUITE:Add Bonding With Custom MAC	${CUSTOM_MAC_1}
	CLI:Connect As Root
	${BOND0_CONFIG}	CLI:Write	ifconfig bond0
	Should Contain	${BOND0_CONFIG}	ether ${CUSTOM_MAC_1}
	CLI:Close Current Connection
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Delete If Exists	${BOND_NAME}

Test edit custom MAC from bonding interface
	[Documentation]	Test adds a bonding connection with ${CUSTOM_MAC_1} and then edits it to have ${CUSTOM_MAC_2}
	...	and checks, using show command, that it has the new address and not the old one. After that checks it was updated
	...	also using ifconfig command
	[Tags]	NON-CRITICAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	SUITE:Add Bonding With Custom MAC	${CUSTOM_MAC_1}
	CLI:Enter Path	/settings/network_connections/${BOND_NAME}
	CLI:Set	bond_mac_address=${CUSTOM_MAC_2}
	CLI:Commit
	@{REQUIRED_INFO}	Create List	${BOND_NAME}	bonding	bond0	${CUSTOM_MAC_2}
	CLI:Enter Path	/settings/network_connections
	${HAS_CUSTOM_MAC}	CLI:Show
	CLI:Should Contain All	${HAS_CUSTOM_MAC}	${REQUIRED_INFO}
	Should Not Contain	${HAS_CUSTOM_MAC}	${CUSTOM_MAC_1}
	CLI:Connect As Root
	${BOND0_CONFIG}	CLI:Write	ifconfig bond0
	Should Contain	${BOND0_CONFIG}	ether ${CUSTOM_MAC_2}
	Should Not Contain	${BOND0_CONFIG}	ether ${CUSTOM_MAC_1}
	CLI:Close Current Connection
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Delete If Exists	${BOND_NAME}

Test edit bonding for failover MAC
	[Documentation]	Test adds a bonding connection with ${CUSTOM_MAC_1} and then edits it to have the MAC address from the
	...	interface that is currently active dinamically.
	...	Test checks, using show command, that it has the new address and not the old one. After that checks it was updated
	...	also using ifconfig command
	[Tags]	NON-CRITICAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	${MAC_INTERFACE}	CLI:Get Interface MAC Address	${BOND_INTERFACE}
	SUITE:Add Bonding With Custom MAC	${CUSTOM_MAC_1}
	CLI:Enter Path	/settings/network_connections/${BOND_NAME}
	CLI:Set	bond_mac_configuration=bond_fail-over-mac bond_fail-over-mac_policy=follow_active_interface
	CLI:Commit
	@{REQUIRED_INFO}	Create List	${BOND_NAME}	bonding	bond0	${MAC_INTERFACE}
	${HAS_CUSTOM_MAC}	CLI:Show	/settings/network_connections
	CLI:Should Contain All	${HAS_CUSTOM_MAC}	${REQUIRED_INFO}
	Should Not Contain	${HAS_CUSTOM_MAC}	${CUSTOM_MAC_1}
	[Teardown]	CLI:Delete If Exists	${BOND_NAME}

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Enter Path	/settings/network_connections/
	write	delete ${BOND_NAME}
	Sleep	10s
	Read Until Prompt
	CLI:Delete If Exists	bonding_eth0	bonding_hotspot	bonding_backplane1	bonding_backplane0	bonding_eth1
	CLI:Close Connection

SUITE:Check Interface
	[Arguments]	${INTERFACE}
	${HAS_INTERFACE}	CLI:Has Interface	${INTERFACE}
	Skip If	not ${HAS_INTERFACE}	Host Device doesn't have ${INTERFACE}
	${HAS_IP}	${IP}	SUITE:Get Interface Ip	${INTERFACE}
	Skip If	not ${HAS_IP}	Interface ${INTERFACE} doesn't have IP address
	[Return]	${IP}

SUITE:Get Interface Ip
	[Arguments]	${INTERFACE}
	${IP}	CLI:Get Interface IP Address	${INTERFACE}
	${HAS_IP}	Run Keyword And Return Status	Should Match Regexp	${IP}	192.168.[0-9][0-9]?[0-9]?.[0-9][0-9]?[0-9]?
	[Return]	${HAS_IP}	${IP}

SUITE:Delete Interface
	[Arguments]	${INTERFACE}
	CLI:Enter Path	/settings/network_connections/
	Write	delete ${INTERFACE}
	Read Until	]#

SUITE:Add Bonding Connection
	[Arguments]	${NAME}	${IP}	${PRIMARY_INTERFACE}=eth0	${SECONDARY_INTERFACE}=eth1	${BONDING_MODE}=active_backup
	CLI:Enter Path	/settings/network_connections
	CLI:Add
	CLI:Set	name=${NAME} type=bonding bonding_mode=${BONDING_MODE}
	CLI:Set	ipv4_mode=static ipv4_address=${HOST} ipv4_gateway=${GATEWAY} ipv4_bitmask=24
	CLI:Set	primary_interface=${PRIMARY_INTERFACE} secondary_interface=${SECONDARY_INTERFACE}

	Run Keyword If	${NGVERSION} == 3.2	CLI:Set	bond_mac_policy=primary_interf
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Set	bond_fail-over-mac_policy=primary_interface
	Run Keyword If	not '${BONDING_MODE}' == 'active_backup' and ${NGVERSION} >= 4.2
	...	CLI:Set	slave(s)="${PRIMARY_INTERFACE} ${SECONDARY_INTERFACE}"

	Set Client Configuration	timeout=180s
	CLI:Write	commit; up_connection ${NAME}
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}

	Wait Until Keyword Succeeds	5x	10s	SUITE:Check Bonding Creation	${PRIMARY_INTERFACE}	${IP}

SUITE:Edit Bonding Primary Interface
	[Arguments]	${NAME}	${PRIMARY_INTERFACE}	${IP}
	CLI:Enter Path	/settings/network_connections/bonding_${NAME}
	CLI:Set	primary_interface=${PRIMARY_INTERFACE}
	Set Client Configuration	timeout=180s
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Wait Until Keyword Succeeds	5x	10s	SUITE:Check Bonding Creation	${PRIMARY_INTERFACE}	${IP}	${NAME}

SUITE:Check Bonding Creation
	[Arguments]	${INTERFACE}	${IP}	${NAME}=${INTERFACE}
	CLI:Enter Path	/settings/network_connections/
	${OUTPUT}	CLI:Show
	@{LINES}	Split To Lines	${OUTPUT}	2
	FOR	${LINE}	IN	@{LINES}
		${STATUS}	Run Keyword and Return Status	Should Contain	${LINE}	bonding_${NAME}
		${INTERFACE_LINE}	Run Keyword If	${STATUS}	Set Variable	${LINE}
		Exit For Loop If	${STATUS}
	END
	Should Match Regexp	${INTERFACE_LINE}	(\\s)*bonding_${NAME}(\\s)+connected(\\s)+bonding(\\s)+${INTERFACE}(\\s)+up(\\s)+${IP}(.)*

SUITE:Add Bonding With Custom MAC
	[Arguments]	${MAC_ADDR}
	CLI:Enter Path	/settings/network_connections
	CLI:Add
	CLI:Set	name=${BOND_NAME} type=bonding bonding_mode=round-robin slave(s)=${BOND_INTERFACE}
	CLI:Set	bond_mac_configuration=bond_custom_mac bond_mac_address=${MAC_ADDR}
	CLI:Commit