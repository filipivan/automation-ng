*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Network Connections Configurations through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW	NEED-REVIEW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${MANUAL_GSM}	manual_gsm
${AUTOMATIC_GSM}	automatic_gsm

*** Test Cases ***
Configure ETH1 with ipv4 static mode
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	CLI:Configure INTERFACE with IPv4 Address	192.168.11.1	24	ETH1

Configure ETH1 with ipv4 dhcp mode
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	CLI:Enter Path	/settings/network_connections/ETH1
	CLI:Set	ipv4_mode=dhcp
	CLI:Commit
	CLI:Test Show Command	ipv4_mode = dhcp

Configure ETH1 with ipv4 static mode and GW
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	CLI:Enter Path	/settings/network_connections/ETH1
	CLI:Configure INTERFACE with IPv4 Address	192.168.11.1	24	ETH1	192.168.11.254

Configure ETH1 with no_ipv4_address mode
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	CLI:Enter Path	/settings/network_connections/ETH1
	CLI:Set	ipv4_mode=no_ipv4_address
	CLI:Commit
	CLI:Test Show Command	ipv4_mode = no_ipv4_address

Configure ETH1 with ipv6 static mode
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	CLI:Configure INTERFACE with IPv6 Address	fd38:24b8:1110:7777::5	8	ETH1

Configure ETH1 with ipv6 dhcp mode
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	CLI:Enter Path	/settings/network_connections/ETH1
	CLI:Set	ipv6_mode=stateful_dhcpv6
	CLI:Commit
	CLI:Test Show Command	ipv6_mode = stateful_dhcpv6

Configure ETH1 with ipv6 static mode and GW
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	CLI:Configure INTERFACE with IPv6 Address	fd38:24b8:1110:7064::1	8	ETH1	fd38:24b8:1110:7064::ffff

Configure ETH1 with ipv6 address_auto_configuration mode
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	CLI:Enter Path	/settings/network_connections/ETH1
	CLI:Set	ipv6_mode=address_auto_configuration
	CLI:Commit
	CLI:Test Show Command	ipv6_mode = address_auto_configuration

Configure ETH1 with no_ipv6_address mode
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	CLI:Enter Path	/settings/network_connections/ETH1
	CLI:Set	ipv6_mode=no_ipv6_address
	CLI:Commit
	CLI:Test Show Command	ipv6_mode = no_ipv6_address

Configure ETH1 not to auto-connect
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	CLI:Enter Path	/settings/network_connections/ETH1
	CLI:Set	connect_automatically=no
	CLI:Commit
	CLI:Test Show Command	connect_automatically = no

Configure ETH1 to auto-connect
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	CLI:Enter Path	/settings/network_connections/ETH1
	CLI:Set	connect_automatically=yes
	CLI:Commit
	CLI:Test Show Command	connect_automatically = yes

Configure ETH1 with lldp
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	CLI:Enter Path	/settings/network_connections/ETH1
	CLI:Set	enable_lldp=yes
	CLI:Commit
	CLI:Test Show Command	enable_lldp = yes

Configure ETH1 with no lldp
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	CLI:Enter Path	/settings/network_connections/ETH1
	CLI:Set	enable_lldp=no
	CLI:Commit
	CLI:Test Show Command	enable_lldp = no

Configure ETH1 as primary connection
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	CLI:Enter Path	/settings/network_connections/ETH1
	CLI:Set	ipv4_mode=dhcp
	CLI:Set	set_as_primary_connection=yes
	CLI:Commit
	CLI:Test Show Command	set_as_primary_connection = yes

Configure ETH1 not as primary connection
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	CLI:Enter Path	/settings/network_connections/ETH1

	Run Keyword If	'${NGVERSION}' > '3.2'	CLI:Set	ipv4_default_route_metric= ipv6_default_route_metric=

	CLI:Set	set_as_primary_connection=no
	CLI:Commit
	CLI:Test Show Command	set_as_primary_connection = no

Configure ETH1 with no ipv4 dns server
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	CLI:Enter Path	/settings/network_connections/ETH1
	CLI:Set	ipv4_dns_server=${EMPTY}
	CLI:Commit
	CLI:Test Show Command	ipv4_dns_server =

Configure ETH1 with ipv4 dns server
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	CLI:Enter Path	/settings/network_connections/ETH1
	CLI:Set	ipv4_dns_server=8.8.8.8
	CLI:Commit
	CLI:Test Show Command	ipv4_dns_server = 8.8.8.8

Configure ETH1 with no ipv4 dns search
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	CLI:Enter Path	/settings/network_connections/ETH1
	CLI:Set	ipv4_dns_search=${EMPTY}
	CLI:Commit
	CLI:Test Show Command	ipv4_dns_search =

Configure ETH1 with ipv4 dns search
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	CLI:Enter Path	/settings/network_connections/ETH1
	CLI:Set	ipv4_dns_search=zpesystems.net
	CLI:Commit
	CLI:Test Show Command	ipv4_dns_search = zpesystems.net

Configure ETH1 with no ipv6 dns server
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	CLI:Enter Path	/settings/network_connections/ETH1
	CLI:Set	ipv6_dns_server=${EMPTY}
	CLI:Commit
	CLI:Test Show Command	ipv6_dns_server =

Configure ETH1 with ipv6 dns server
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	CLI:Enter Path	/settings/network_connections/ETH1
	CLI:Set	ipv6_mode=stateful_dhcpv6 ipv6_dns_server=2620:0:ccc::2
	CLI:Commit
	CLI:Test Show Command	ipv6_dns_server = 2620:0:ccc::2

Configure ETH1 with no ipv6 dns search
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	CLI:Enter Path	/settings/network_connections/ETH1
	CLI:Set	ipv6_dns_search=${EMPTY}
	CLI:Commit
	CLI:Test Show Command	ipv6_dns_search =

Configure ETH1 with ipv6 dns search
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	CLI:Enter Path	/settings/network_connections/ETH1
	CLI:Set	ipv6_mode=stateful_dhcpv6 ipv6_dns_search=zpesystems.net
	CLI:Commit
	CLI:Test Show Command	ipv6_dns_search = zpesystems.net

Test Configure GSM Connection - Manual SIM-1 APN Configuration
	Skip If	'${NGVERSION}' < '5.6'	Feature is only implemented on 5.6+
	Skip If	not ${HAS_MODEM}	Device does not have wireless modem
	Skip If	not ${HAS_SIM_CARD}	Device does not have a avaliable sim card
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${MANUAL_GSM} type=mobile_broadband_gsm
	CLI:Set	sim-1_apn_configuration=manual sim-1_user_name=test-user sim-1_password=test sim-1_access_point_name=test
	CLI:Commit
	${OUTPUT}	CLI:Show
	Should Contain	${OUTPUT}	${MANUAL_GSM}
	${OUTPUT}	CLI:Write	shell sudo cat /etc/sim-conf.ini
	Should Contain	${OUTPUT}	apn-auto = no
	Should Contain	${OUTPUT}	user-name = test-user
	[Teardown]	CLI:Delete If Exists	${MANUAL_GSM}

Test Configure GSM Connection - Automatic SIM-1 APN Configuration
	Skip If	'${NGVERSION}' < '5.6'	Feature is only implemented on 5.6+
	Skip If	not ${HAS_MODEM}	Device does not have wireless modem
	Skip If	not ${HAS_SIM_CARD}	Device does not have a avaliable sim card
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${AUTOMATIC_GSM} type=mobile_broadband_gsm
	CLI:Set	sim-1_apn_configuration=automatic
	CLI:Commit
	${OUTPUT}	CLI:Show
	Should Contain	${OUTPUT}	${AUTOMATIC_GSM}
	${OUTPUT}	CLI:Write	shell sudo cat /etc/sim-conf.ini
	Should Contain	${OUTPUT}	apn-auto = yes
	[Teardown]	CLI:Delete If Exists	${AUTOMATIC_GSM}

*** Keywords ***
SUITE:Setup
	CLI:Open
	${HAS_ETH1}=	SUITE:Check If Device Has ETH1 With IP
	Set Suite Variable	${HAS_ETH1}	${HAS_ETH1}
	${HAS_MODEM}	CLI:Has Wireless Modem Support
	Set Suite Variable	${HAS_MODEM}
	${HAS_SIM_CARD}	Run Keyword If	${HAS_MODEM}	CLI:Has Wireless Modem With SIM Card
	Set Suite Variable	${HAS_SIM_CARD}

SUITE:Teardown
	Run Keyword If	${HAS_ETH1}	SUITE:Unset Configured Settings
	CLI:Close Connection

SUITE:Unset Configured Settings
	CLI:Enter Path	/settings/network_connections/ETH1
	CLI:Set	ipv6_dns_search=${EMPTY}
	CLI:Set	ipv6_dns_server=${EMPTY}
	CLI:Set	ipv4_dns_search=${EMPTY}
	CLI:Set	ipv4_dns_server=${EMPTY}
	CLI:Commit
	Run Keyword If	${NGVERSION} > 3.2	CLI:Set	ipv6_default_route_metric=${EMPTY}
	Run Keyword If	${NGVERSION} > 3.2	CLI:Set	ipv4_default_route_metric=${EMPTY}
	CLI:Enter Path	/settings/network_connections/
	${IS_5.6+}	Evaluate	${NGVERSION} >= 5.6
	Run Keyword If	${HAS_MODEM} and ${HAS_SIM_CARD} and ${IS_5.6+}	CLI:Delete If Exists	${MANUAL_GSM}
	Run Keyword If	${HAS_MODEM} and ${HAS_SIM_CARD} and ${IS_5.6+}	CLI:Delete If Exists	${AUTOMATIC_GSM}

SUITE:Check If Device Has ETH1 With IP
	CLI:Enter Path	/settings/network_connections
	${CONNECTIONS}	CLI:Show
	${HAS_ETH1}	Run Keyword And Return Status	Should Match Regexp	${CONNECTIONS}	eth1\\s+up\\s+192.168
	[Return]	${HAS_ETH1}