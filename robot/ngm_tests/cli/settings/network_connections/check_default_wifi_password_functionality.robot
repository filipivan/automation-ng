*** Settings ***
Resource	    ../../init.robot
Documentation	This is the functionality tests for check default password in the wifi.
Metadata	    Version	1.0
Metadata	    Executed At	${HOST}
Force Tags	    CLI  EXCLUDEIN3_2  EXCLUDEIN4_2   NEED-REVIEW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test PSK and Serial Number
    ${SERIAL_NUMBER}=   SUITE:Get Serial Number
    ${PSK}=             SUITE:Get PSK
    Should Contain      ${SERIAL_NUMBER}    ${PSK}

*** Keywords ***
SUITE:Setup
	CLI:Open
	${HAS_WIFI}	CLI:Has Wireless Modem Support
	Skip If	not ${HAS_WIFI}	Device doesn't have wifi

SUITE:Teardown
	CLI:Close Connection

SUITE:Get Serial Number
	${dict}=            CLI:Get System About Dictionary
	${SERIAL_NUMBER}=	Get From Dictionary	${dict}	'serial_number'
	Log                 ${SERIAL_NUMBER}
	Should be Integer	${SERIAL_NUMBER}
    [Return]            ${SERIAL_NUMBER}

SUITE:Get PSK
	CLI:Connect As Root
	${OUTPUT}=  CLI:Write  llconf ini -f /etc/NetworkManager/system-connections/hotspot get wifi-security/psk
    Log         ${OUTPUT}
    ${PSK}=     Get Line  ' ${OUTPUT}'   0
    Log         ${PSK}
    [Return]    ${PSK}
