*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Network Connections of type bonding... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN5_0	NON-CRITICAL
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${BONDING_NAME}	bonding_test
@{BOND_MODES}	802.3ad(lacp)	adaptive_load_balancing	broadcast	xor_load_balancing	active_backup
...	adaptive_transmit_load_balancing	round-robin
@{INTERFACES}	eth0	eth1	backplane0	backplane1	hotspot
@{INVALID_INTERFACES}	bond0	br0	vti	backplane0.100	pppoe
@{ARP_MODES}	active	all	backup	none
@{VALID_NUMERIC}	${THREE_NUMBER}	${EIGHT_NUMBER}	0
@{INVALID_NUMERIC}	${THREE_WORD}	${THREE_POINTS}
@{VALID_MAC_ADDRESS}	00:11:22:33:44:55	00:ff:ee:dd:cc:bb:aa
@{INVALID_MAC_ADDRESS}	123:456:789:0aa	aa:bb:cc:dd:ee:ff:gg
@{INVALID_MODES}	${ELEVEN_WORD}	${ELEVEN_NUMBER}	${ELEVEN_POINTS}	${EXCEEDED}

*** Test Cases ***
Test show Command
	CLI:Enter Path	/settings/network_connections/${BONDING_NAME}
	CLI:Test Show Command	name:	type:	connect_automatically\ =	set_as_primary_connection\ =	enable_lldp\ =	bonding_mode\ =
	...	primary_interface\ =	secondary_interface\ =	link_monitoring\ =	monitoring_frequency\ =	link_up_delay\ =
	...	link_down_delay\ =	arp_target\ =	arp_validate\ =	ipv4_mode\ =	ipv4_dns_server\ =	ipv4_dns_search\ =	ipv6_mode\ =
	...	ipv6_dns_server\ =	ipv6_dns_search\ =
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Show Command	bond_mac_policy\ =
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Show Command	description\ =	slave(s)\ =	bond_fail-over-mac_policy\ =	system_priority\ =
	...	actor_mac_address\ =	user_port_key\ =	lacp_rate\ =	aggregation_selection_logic\ =	transmit_hash_policy\ =
	...	ipv4_default_route_metric\ =	ipv4_ignore_obtained_default_gateway\ =	ipv6_default_route_metric\ =	ipv6_ignore_obtained_default_gateway\ =
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Show Command	interface:
	Run Keyword If	'${NGVERSION}' == '5.0'	CLI:Test Show Command	ipv4_address\ =	ipv4_bitmask\ =	ipv4_gateway\ =
	Run Keyword If	'${NGVERSION}' >= '5.2'	CLI:Test Show Command	ipv4_ignore_obtained_dns_server\ =	ipv6_ignore_obtained_dns_server\ =
	Run Keyword If	'${NGVERSION}' >= '5.4'	CLI:Test Show Command	block_unsolicited_incoming_packets\ =

Test Available Fields After Set Command
	CLI:Test Set Available Fields	connect_automatically	set_as_primary_connection	enable_lldp	bonding_mode	primary_interface
	...	secondary_interface	link_monitoring	monitoring_frequency	link_up_delay	link_down_delay	arp_target	arp_validate	ipv4_mode
	...	ipv4_dns_server	ipv4_dns_search	ipv6_mode	ipv6_dns_server	ipv6_dns_search
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Available Fields	bond_mac_policy
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Available Fields	description	slave(s)	bond_fail-over-mac_policy	system_priority
	...	actor_mac_address	user_port_key	lacp_rate	aggregation_selection_logic	transmit_hash_policy
	...	ipv4_default_route_metric	ipv4_ignore_obtained_default_gateway	ipv6_default_route_metric	ipv6_ignore_obtained_default_gateway
	Run Keyword If	'${NGVERSION}' == '5.0'	CLI:Test Set Available Fields	ipv4_address	ipv4_bitmask	ipv4_gateway
	Run Keyword If	'${NGVERSION}' >= '5.2'	CLI:Test Set Available Fields	ipv4_ignore_obtained_dns_server	ipv6_ignore_obtained_dns_server
	Run Keyword If	'${NGVERSION}' >= '5.4'	CLI:Test Set Available Fields	block_unsolicited_incoming_packets
	[Teardown]	CLI:Cancel	Raw

Test valid values for bonding_mode
	[Documentation]	Test field bonding_mode can be configured with all possible values
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding
	CLI:Test Set Validate Normal Fields	bonding_mode	@{BOND_MODES}
	[Teardown]	CLI:Cancel	Raw

Test invalid values for bonding_mode
	[Documentation]	Test inserting invalid values in bonding_mode field (such as random string, number and special characters)
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding
	FOR	${MODE}	IN	@{INVALID_MODES}
		CLI:Test Set Validate Invalid Options	bonding_mode	${MODE}
		...	Error: Invalid value: ${MODE} for parameter: bonding_mode
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for primary_interface
	[Documentation]	Test checks interfaces listed in NG and then compares all possible values within a list of avaliable
	...	interfaces. If this interface is avaliable it is tested as a valid value for primary_interface field
	${DEVICE_INTERFACES}	CLI:Ls	/system/network_statistics/
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding bonding_mode=active_backup
	FOR	${INTERFACE}	IN	@{INTERFACES}
		${INTERFACE_AVALIABLE}	Run Keyword And Return Status	Should Contain	${DEVICE_INTERFACES}	${INTERFACE}
		IF	${INTERFACE_AVALIABLE}
			CLI:Test Set Field Valid Options	primary_interface	${INTERFACE}	revert=no
		END
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for primary_interface
	[Documentation]	Test tries all possible values in a list of interfaces that cannot be part of bonding and check
	...	error is raised. OBS.: field is only taken into consideration if bonding mode is active backup
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding bonding_mode=active_backup
	FOR	${INTERFACE}	IN	@{INVALID_INTERFACES}
		CLI:Test Set Field Invalid Options	primary_interface	${INTERFACE}
		...	Error: Invalid value: ${INTERFACE} for parameter: primary_interface
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for secondary_interface
	[Documentation]	Test checks interfaces listed in NG and then compares all possible values within a list of avaliable
	...	interfaces. If this interface is avaliable it is tested as a valid value for secondary_interface field
	${DEVICE_INTERFACES}	CLI:Ls	/system/network_statistics/
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding bonding_mode=active_backup
	FOR	${INTERFACE}	IN	@{INTERFACES}
		${INTERFACE_AVALIABLE}	Run Keyword And Return Status	Should Contain	${DEVICE_INTERFACES}	${INTERFACE}
		IF	${INTERFACE_AVALIABLE}
			CLI:Test Set Field Valid Options	secondary_interface	${INTERFACE}	revert=no
		END
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for secondary_interface
	[Documentation]	Test tries all possible values in a list of interfaces that cannot be part of bonding and check
	...	error is raised. OBS.: field is only taken into consideration if bonding mode is active backup
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding bonding_mode=active_backup
	FOR	${INTERFACE}	IN	@{INVALID_INTERFACES}
		CLI:Test Set Field Invalid Options	secondary_interface	${INTERFACE}
		...	Error: Invalid value: ${INTERFACE} for parameter: secondary_interface
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for slave(s)
	[Documentation]	Test checks commom interfaces works configured as slaves
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding
	FOR	${INTERFACE}	IN	@{INTERFACES}
		CLI:Test Set Field Valid Options	slave(s)	${INTERFACE}	revert=no
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for slave(s)
	[Documentation]	Test invalid values for slaves interface (such as values with commas and special characters) raise
	...	expected error message after commit. OBS.:round-robin mode is selected to avoid entering more information.
	[Tags]	EXCLUDEIN3_2
	@{INVALID_VALUES}	Create List	eth0,eth1	${ELEVEN_POINTS}
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=test type=bonding bonding_mode=round-robin
	FOR	${VALUE}	IN	@{INVALID_VALUES}
		CLI:Set	slave(s)=${VALUE}
		${ERROR_EXPECTED}	CLI:Commit	Raw
		Should Contain	${ERROR_EXPECTED}	This field must be a list of interfaces separated by space enclosed in quotation marks
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for link_monitoring
	[Documentation]	Test field link_monitoring can be configured with all possible values (arp and mii)
	@{LINK_MONITORING_MODES}	Create List	arp	mii
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding
	CLI:Test Set Validate Normal Fields	link_monitoring	@{LINK_MONITORING_MODES}
	[Teardown]	CLI:Cancel	Raw

Test invalid values for link_monitoring
	[Documentation]	Test inserting invalid values in link_monitoring field (such as random string, number and special characters)
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding
	FOR	${MODE}	IN	@{INVALID_MODES}
		CLI:Test Set Validate Invalid Options	link_monitoring	${MODE}
		...	Error: Invalid value: ${MODE} for parameter: link_monitoring
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for arp_target
	[Documentation]	Test inserting valid values (like IP and FQDN addresses) in arp_target field
	@{VALID_IPS}	Create List	1.2.3.4	dummysite.com.br
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding
	FOR	${IP}	IN	@{VALID_IPS}
		CLI:Test Set Field Valid Options	arp_target	${IP}	revert=no
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for arp_target
	[Documentation]	Test inserting invalid values (like invalid IP addresses, random words and points) for arp_target
	...	raise expected error message after commit. OBS.: round-robin mode is chosen to avoid entering more info.
	@{INVALID_IPS}	Create List	256.256.256.256	${ELEVEN_WORD}	${ELEVEN_NUMBER}	${POINTS}
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=test type=bonding bonding_mode=round-robin
	FOR	${IP}	IN	@{INVALID_IPS}
		CLI:Set	arp_target=${IP}
		${ERROR_EXPECTED}	CLI:Commit	Raw
		Should Contain	${ERROR_EXPECTED}	Error: arp_target: Validation error
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for arp_validate
	[Documentation]	Test field arp_validate can be configured with all possible values
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding
	CLI:Test Set Validate Normal Fields	arp_validate	@{ARP_MODES}
	[Teardown]	CLI:Cancel	Raw

Test invalid values for arp_validate
	[Documentation]	Test inserting invalid values (like random string, numbers and points) for arp_validate
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding
	FOR	${MODE}	IN	@{INVALID_MODES}
		CLI:Test Set Validate Invalid Options	arp_validate	${MODE}
		...	Error: Invalid value: ${MODE} for parameter: arp_validate
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for monitoring_frequency
	[Documentation]	Test inserting valid values (like three and eight digit number and zero) to monitoring_frequency field
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding
	FOR	${FREQUENCY}	IN	@{VALID_NUMERIC}
		CLI:Test Set Field Valid Options	monitoring_frequency	${FREQUENCY}	revert=no
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for monitoring_frequency
	[Documentation]	Test inserting valid values (like random string, numbers and special characters) to
	...	monitoring_frequency field, also tests a huge number triggers a specific error message
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding
	FOR	${FREQUENCY}	IN	@{INVALID_NUMERIC}
		CLI:Set	monitoring_frequency=${FREQUENCY}
		${ERROR_EXPECTED}	CLI:Commit	Raw
		Should Contain	${ERROR_EXPECTED}	Error: monitoring_frequency: Validation error.
	END
	CLI:Set	monitoring_frequency=${EXCEEDED}
	${EXCEED_ERROR}	CLI:Commit	Raw
	Should Contain	${EXCEED_ERROR}	Error: monitoring_frequency: Exceeded the maximum size for this field.
	[Teardown]	CLI:Cancel	Raw

Test valid values for link_up_delay
	[Documentation]	Test inserting valid values (like three and eight digit number and zero) to link_up_delay field
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding
	FOR	${DELAY}	IN	@{VALID_NUMERIC}
		CLI:Test Set Field Valid Options	link_up_delay	${DELAY}	revert=no
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for link_up_delay
	[Documentation]	Test inserting valid values (like random string, numbers and special characters) to
	...	link_up_delay field, also tests a huge number triggers a specific error message
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding
	FOR	${DELAY}	IN	@{INVALID_NUMERIC}
		CLI:Set	link_up_delay=${DELAY}
		${ERROR_EXPECTED}	CLI:Commit	Raw
		Should Contain	${ERROR_EXPECTED}	Error: link_up_delay: Validation error.
	END
	CLI:Set	link_up_delay=${EXCEEDED}
	${EXCEED_ERROR}	CLI:Commit	Raw
	Should Contain	${EXCEED_ERROR}	Error: link_up_delay: Exceeded the maximum size for this field.
	[Teardown]	CLI:Cancel	Raw

Test valid values for link_down_delay
	[Documentation]	Test inserting valid values (like three and eight digit number and zero) to link_down_delay field
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding
	FOR	${DELAY}	IN	@{VALID_NUMERIC}
		CLI:Test Set Field Valid Options	link_down_delay	${DELAY}	revert=no
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for link_down_delay
	[Documentation]	Test inserting valid values (like random string, numbers and special characters) to
	...	link_down_delay field, also tests a huge number triggers a specific error message
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding
	FOR	${DELAY}	IN	@{INVALID_NUMERIC}
		CLI:Set	link_down_delay=${DELAY}
		${ERROR_EXPECTED}	CLI:Commit	Raw
		Should Contain	${ERROR_EXPECTED}	Error: link_down_delay: Validation error.
	END
	CLI:Set	link_down_delay=${EXCEEDED}
	${EXCEED_ERROR}	CLI:Commit	Raw
	Should Contain	${EXCEED_ERROR}	Error: link_down_delay: Exceeded the maximum size for this field.
	[Teardown]	CLI:Cancel	Raw

Test valid values for bond_mac_configuration
	[Documentation]	Test field bond_mac_configuration can be configured with all possible values
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	@{MAC_ADDRESS_MODES}	Create List	bond_custom_mac	bond_fail-over-mac
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding
	CLI:Test Set Validate Normal Fields	bond_mac_configuration	@{MAC_ADDRESS_MODES}
	[Teardown]	CLI:Cancel	Raw

Test invalid values for bond_mac_configuration
	[Documentation]	Test inserts invalid values (like random string, numbers and points) for bond_mac_configuration
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN	5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding
	FOR	${MODE}	IN	@{INVALID_MODES}
		CLI:Test Set Validate Invalid Options	bond_mac_configuration	${MODE}
		...	Error: Invalid value: ${MODE} for parameter: bond_mac_configuration
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for bond_fail-over-mac_policy
	[Documentation]	Test field bond_fail-over-mac_policy can be configured with all possible values
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	@{MAC_FAILOVER_MODES}	Create List	current_active_interface  follow_active_interface   primary_interface
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding
	CLI:Test Set Validate Normal Fields	bond_fail-over-mac_policy	@{MAC_FAILOVER_MODES}
	[Teardown]	CLI:Cancel	Raw

Test invalid values for bond_fail-over-mac_policy
	[Documentation]	Test inserts invalid values (like random string, numbers and points) for bond_fail-over-mac_policy
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding
	FOR	${MODE}	IN	@{INVALID_MODES}
		CLI:Test Set Validate Invalid Options	bond_fail-over-mac_policy	${MODE}
		...	Error: Invalid value: ${MODE} for parameter: bond_fail-over-mac_policy
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for bond_mac_address
	[Documentation]	Test tries some valid MAC addresses (6-pair hexa divided by ":") for bond_mac_address field. This
	...	field is only configurable if bond_mac_configuration=bond_custom_mac. OBS.: round-robin mode was chosen to avoid
	...	entering more information.
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding bonding_mode=round-robin bond_mac_configuration=bond_custom_mac
	FOR	${MAC}	IN	@{VALID_MAC_ADDRESS}
		CLI:Test Set Field Valid Options	bond_mac_address	${MAC}	revert=no
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for bond_mac_address
	[Documentation]	Test tries invalid values (like 7-pair hexa and other variants) for bond_mac_address field and
	...	expects  correct error message after commit
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding bonding_mode=round-robin bond_mac_configuration=bond_custom_mac
	FOR	${MAC}	IN	@{INVALID_MAC_ADDRESS}
		CLI:Set	bond_mac_address=${MAC}
		${ERROR_EXPECTED}	CLI:Commit	Raw
		Should Contain	${ERROR_EXPECTED}	Error: bond_mac_address: Validation error.
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for transmit_hash_policy
	[Documentation]	Test field transmit_hash_policy can be configured with all possible values
	[Tags]	EXCLUDEIN3_2
	@{HASH_POLICIES}	Create List	layer_2	layers_2_and_3_and_encap	layers_3_and_4_and_encap	layers_2_and_3	layers_3_and_4
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding
	CLI:Test Set Validate Normal Fields	transmit_hash_policy	@{HASH_POLICIES}
	[Teardown]	CLI:Cancel	Raw

Test invalid values for transmit_hash_policy
	[Documentation]	Test inserting invalid values (like random string, numbers and points) for transmit_hash_policy
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding
	FOR	${MODE}	IN	@{INVALID_MODES}
		CLI:Test Set Validate Invalid Options	transmit_hash_policy	${MODE}
		...	Error: Invalid value: ${MODE} for parameter: transmit_hash_policy
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for system_priority
	[Documentation]	Test inserting valid values for system_priority field when bonding_mode is 802.3ad(lacp).
	...	System priority has to be an integer between 0 and 65535.
	[Tags]	EXCLUDEIN3_2
	@{VALID_PRIOS}	Create List	0	333	65535
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding bonding_mode=802.3ad(lacp)
	FOR	${PRIO}	IN	@{VALID_PRIOS}
		CLI:Test Set Field Valid Options	system_priority	${PRIO}	revert=no
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for system_priority
	[Documentation]	Test inserting invalid values (like out-of-range and words) for system_priority field when
	...	bonding_mode is 802.3ad(lacp) raise expected error message after the commit. System priority has to be
	...	an integer between 0 and 65535
	[Tags]	EXCLUDEIN3_2
	@{INVALID_PRIOS}	Create List	-1	65536	${EIGHT_WORD}
	CLI:Add
	CLI:Set	type=bonding bonding_mode=802.3ad(lacp)
	FOR	${PRIO}	IN	@{INVALID_PRIOS}
		CLI:Set	system_priority=${PRIO}
		${ERROR_EXPECTED}	CLI:Commit	Raw
		Should Contain	${ERROR_EXPECTED}	Error: system_priority: Validation error.
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for actor_mac_address
	[Documentation]	Test tries some valid MAC addresses (6-pair hexa divided by ":") for actor_mac_address field when
	...	bonding mode is 802.3ad(lacp)
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding bonding_mode=802.3ad(lacp)
	FOR	${MAC}	IN	@{VALID_MAC_ADDRESS}
		CLI:Test Set Field Valid Options	actor_mac_address	${MAC}	revert=no
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for actor_mac_address
	[Documentation]	Test tries invalid values (like 7-pair hexa and other variants) for actor_mac_address field and
	...	expects  correct error message after commit. Due to bug NG-12317 this currently field does not have validation
	[Tags]	EXCLUDEIN3_2	BUG_NG_12317
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=test type=bonding bonding_mode=802.3ad(lacp)
	FOR	${MAC}	IN	@{INVALID_MAC_ADDRESS}
		CLI:Set	actor_mac_address=${MAC}
		${ERROR_EXPECTED}	CLI:Commit	Raw
		Should Contain	${ERROR_EXPECTED}	Error: actor_mac_address: Validation error.
	END
	[Teardown]	Run Keywords	CLI:Delete If Exists	test	AND	CLI:Cancel	Raw

Test valid values for user_port_key
	[Documentation]	Test valid user_port_key when bonding mode is 802.3ad(lacp). Key must be between 0 and 1022, 1023 is reserved
	[Tags]	EXCLUDEIN3_2
	@{VALID_KEYS}	Create List	0	1	1022
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding bonding_mode=802.3ad(lacp)
	FOR	${KEY}	IN	@{VALID_KEYS}
		CLI:Test Set Field Valid Options	user_port_key	${KEY}	revert=no
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for user_port_key
	[Documentation]	Test invalid user_port_key (like reserved and out-of-range) when bonding mode is 802.3ad(lacp).
	...	Key must be between 0 and 1022, 1023 is reserved
	[Tags]	EXCLUDEIN3_2
	@{INVALID_KEYS}	Create List	1023	3333
	CLI:Add
	CLI:Set	type=bonding bonding_mode=802.3ad(lacp)
	FOR	${KEY}	IN	@{INVALID_KEYS}
		CLI:Set	user_port_key=${KEY}
		${ERROR_EXPECTED}	CLI:Commit	Raw
		Should Contain	${ERROR_EXPECTED}	Error: user_port_key: Validation error.
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for lacp_rate
	[Documentation]	Test field lacp_rate can be configured with all possible values
	[Tags]	EXCLUDEIN3_2
	@{VALID_RATES}	Create List	fast	slow
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding
	CLI:Test Set Validate Normal Fields	lacp_rate	@{VALID_RATES}
	[Teardown]	CLI:Cancel	Raw

Test invalid values for lacp_rate
	[Documentation]	Test invalid values (like random string, number and special characters) for field lacp_rate
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding
	FOR	${MODE}	IN	@{INVALID_MODES}
		CLI:Test Set Validate Invalid Options	lacp_rate	${MODE}
		...	Error: Invalid value: ${MODE} for parameter: lacp_rate
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for aggregation_selection_logic
	[Documentation]	Test field aggregation_selection_logic can be configured with all possible values
	[Tags]	EXCLUDEIN3_2
	@{VALID_LOGICS}	Create List	bandwith	count	stable
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding
	CLI:Test Set Validate Normal Fields	aggregation_selection_logic	@{VALID_LOGICS}
	[Teardown]	CLI:Cancel	Raw

Test invalid values for aggregation_selection_logic
	[Documentation]	Test inserting invalid values (like random string, numbers and special characters) in
	...	aggregation_selection_logic field
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bonding
	FOR	${MODE}	IN	@{INVALID_MODES}
		CLI:Test Set Validate Invalid Options	aggregation_selection_logic	${MODE}
		...	Error: Invalid value: ${MODE} for parameter: aggregation_selection_logic
	END
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	${HAS_ETH1}	CLI:Has Interface	eth1
	Skip If	not ${HAS_ETH1}
	CLI:Enter Path	/settings/network_connections/
	CLI:Delete If Exists	${BONDING_NAME}
	SUITE:Add Bonding Connection	${BONDING_NAME}

SUITE:Teardown
	CLI:Enter Path	/settings/network_connections/
	Write	delete ${BONDING_NAME}
	Sleep	10s
	Read Until	]#
	CLI:Close Connection

SUITE:Add Bonding Connection
	[Arguments]	${NAME}	${BONDING_MODE}=active_backup	${PRIMARY_INTERFACE}=eth0	${SECONDARY_INTERFACE}=eth0
	CLI:Enter Path	/settings/network_connections
	CLI:Add
	CLI:Set	name=${NAME} type=bonding bonding_mode=${BONDING_MODE}
	CLI:Set	ipv4_mode=static ipv4_address=${HOST} ipv4_gateway=${GATEWAY} ipv4_bitmask=24
	CLI:Set	primary_interface=${PRIMARY_INTERFACE} secondary_interface=${SECONDARY_INTERFACE}

	Run Keyword If	${NGVERSION} == 3.2	CLI:Set	bond_mac_policy=primary_interf
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Set	bond_fail-over-mac_policy=primary_interface
	Run Keyword If	not '${BONDING_MODE}' == 'active_backup' and ${NGVERSION} >= 4.2
	...	CLI:Set	slave(s)="${PRIMARY_INTERFACE} ${SECONDARY_INTERFACE}"

	Set Client Configuration	timeout=180s
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}

	CLI:Test Show Command	${NAME}