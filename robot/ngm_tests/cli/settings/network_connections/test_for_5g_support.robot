*** Settings ***
Documentation	Test to verify 5G support
Metadata        Version	1.0
Metadata        Executed At	    ${HOST}
Resource        ../../../init.robot
Force Tags      CLI     SSH     SHOW    NON-CRITICAL
Default Tags	EXCLUDEIN3_2        EXCLUDEIN4_2        EXCLUDEIN5_0        EXCLUDEIN5_2        EXCLUDEIN5_4

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${ROOT_PASSWORD}            ${ROOT_PASSWORD}
${ADMIN_PASSWORD}           ${DEFAULT_PASSWORD}
${STRONG_PASSWORD}          ${QA_PASSWORD}
${CURRENT_ADMIN_PASSWORD}   ${DEFAULT_PASSWORD}
${CONNECTION_NAME}          test5g
${IP_ADDRESS}               8.8.8.8
${CDC-WDM}                  cdc-wdm
${WWAN}                     wwan
${CELLULAR_CONNECTION}	    ${GSM_CELLULAR_CONNECTION}
${TYPE}	                    ${GSM_TYPE}
${5G_MODULE}                ${EMPTY}

*** Test Cases ***
Test case to verify if device is a NSR
    [Documentation]     Check if the device is NSR
    ${SYSTEM_NAME}      CLI:Get System Name
    ${IS_NSR}           CLI:Is Net SR
    set suite variable      ${IS_NSR}
    Skip If	not ${IS_NSR}	This device is not a NSR
    Log	    This device is a NSR

Test case to verify if device has GSM
    [Documentation]    Checks if the device has GSM module
    Skip If	not ${IS_NSR}	This device is not a NSR
	${HAS_GSM}	${MODEM_INFO}=	CLI:Get Wireless Modems SIM Card Info	0
	Set Suite Variable	${HAS_GSM}
	Skip If	not ${HAS_GSM}	No wireless modem with SIM card available
	Set Suite Variable	${MODEM_INFO}
	Log     This device has GSM

Test case to verify if modem has 5G module
    [Documentation]  Checks if the device has 5G module (EM9191)
    Skip If	not ${IS_NSR}	This device is not a NSR
    Skip If	not ${HAS_GSM}	No wireless modem with SIM card available
    ${HAS_5G_MODULE}    SUITE:Verify if contains 5G module
    set suite variable  ${HAS_5G_MODULE}
    Skip If	not ${HAS_5G_MODULE}	No 5G module available
    Log     This device has 5G Module

Test case to create a GSM Connection
    [Documentation]     Create a new GSM connection using the GSM module
    Skip If	not ${IS_NSR}	This device is not a NSR
    Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
    Skip If	not ${HAS_5G_MODULE}	No 5G module available
    CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION_NAME} type=${TYPE} ethernet_interface=${MODEM_INFO["interface"]} sim-1_access_point_name=${MODEM_INFO["apn"]} enable_global_positioning_system=yes polling_time=1
	CLI:Commit
	CLI:Test Show Command	${CONNECTION_NAME}

Test case to validate GSM connectivity
    [Documentation]     Tests GSM connection connectivity to an external IP
    Skip If	not ${IS_NSR}	This device is not a NSR
    Skip If	not ${HAS_GSM}	This automation test environment has no GSM connection
    Skip If	not ${HAS_5G_MODULE}	No 5G module available
    ${INTERFACE_NAME}  set variable  ${MODEM_INFO["interface"]}
    ${NUMBER_OF_INTERFACE}    remove string  ${INTERFACE_NAME}    ${CDC-WDM}
    ${SOURCE_INTERFACE}  set variable   ${WWAN}${NUMBER_OF_INTERFACE}
    log     ${SOURCE_INTERFACE}
    CLI:Test Ping   ${IP_ADDRESS}	10	60   ${SOURCE_INTERFACE}

*** Keywords ***
SUITE:Setup
    CLI:Open    ${USERNAME}   ${ADMIN_PASSWORD}
    SUITE:Change passwords of admin and root to strong passwords
    CLI:Switch Connection	default

SUITE:Teardown
    CLI:Enter Path	/settings/network_connections
	CLI:Delete If Exists	${CONNECTION_NAME}
    SUITE:Change passwords of admin and root to default passwords
    CLI:Close Connection

### MY SUITES ###
SUITE:Change passwords of admin and root to strong passwords
    [Documentation]     Change admin user and root user password to strong password
    CLI:Change User Password From Shell  root    ${STRONG_PASSWORD}  ${ROOT_PASSWORD}
	CLI:Change User Password From Shell  ${USERNAME}     ${STRONG_PASSWORD}  ${STRONG_PASSWORD}
	${CURRENT_ADMIN_PASSWORD}  set variable   ${STRONG_PASSWORD}
	Set Suite Variable      ${CURRENT_ADMIN_PASSWORD}

SUITE:Change passwords of admin and root to default passwords
    [Documentation]     Change admin user and root user password to default password
    CLI:Change User Password From Shell	    root    ${ROOT_PASSWORD}	    ${STRONG_PASSWORD}
	CLI:Change User Password From Shell     admin	${ADMIN_PASSWORD}	    ${ROOT_PASSWORD}
	${CURRENT_ADMIN_PASSWORD}  set variable   ${ADMIN_PASSWORD}
	Set Suite Variable      ${CURRENT_ADMIN_PASSWORD}

SUITE:Verify if contains 5G module
    [Documentation]     Checks if the device has 5G module (EM9191)
    CLI:Enter Path	/system/wireless_modem/
    ${OUTPUT}     CLI:Ls    user=Yes
    @{LINES}      Split String    ${OUTPUT}     /
    FOR    ${LINE}    IN    @{LINES}
         IF     '${LINE}'!='${EMPTY}'
            log   ${LINE}
            CLI:Enter Path  ${LINE}
            ${OUTPUT_SLOT}  CLI:Show
            ${CONTAINS_5G_MODULE}   Run Keyword And Return Status      Should Be True      """EM9191""" in """${OUTPUT_SLOT}"""
            IF  ${CONTAINS_5G_MODULE}
                ${5G_MODULE}  set variable  ${LINE}
                Set Suite Variable  ${5G_MODULE}
                return from keyword  ${True}
            END
            CLI:Enter Path	/system/wireless_modem/
         END
	END
	return from keyword  ${False}