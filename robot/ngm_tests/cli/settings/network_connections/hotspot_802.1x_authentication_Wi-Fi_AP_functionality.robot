*** Settings ***
Resource	../../init.robot
Documentation	Functionality tests through the CLI about AT for 802.1x authentication to be available for Wifi AP
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	WIFI	NON-CRITICAL	BUG_NG_11496
Default Tags	CLI	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
@{ADMINSIDES}	admin_left	admin_right
${CLIENT_NAME}	test_client
${WIFI_SSID}	test_802.1x
${HOTSPOT_STATIC_IPV4}	10.0.200.1
${CLIENT_STATIC_IPV4}	10.0.200.10
${GATEWAY_STATIC_IPV4}	10.0.200.1
${BITMASK_STATIC_IPV4}	23
@{MODE_IPV4}	static	no_ipv4_address
${CLIENT_HOSTNAME}	NodegridClient
${HOTSPOT_STATIC_IPV6}	fe80::1
${CLIENT_STATIC_IPV6}	fe80::10
${GATEWAY_STATIC_IPV6}	fe80::1
${BITMASK_STATIC_IPV6}	64
@{HOTSPOT_MODE_IPV4}	server	no_ipv4_address
@{MODE_IPV6}	address_auto_configuration	stateful_dhcpv6	no_ipv6_address	static
@{HOTSPOT_MODE_IPV6}	no_ipv6_address	dhcpv6_prefix_delegation
${COUNTRY_NAME}	US
${STATE_NAME}	CA
${LOCALITY_NAME}	FREMONT
${ORGANIZATION_NAME}	ZPE
${UNIT_NAME}	ZPE-FREMONT
${CHALLENGE_PASSWORD}	test

*** Test Cases ***
Test 802.1x Authentication Wifi AP By Default
	Skip If		not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	[Setup]	Run Keywords	Skip If		not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	...	AND	Skip If		${HAS_WIFI} == ${FALSE}	System doesn't have WIFI network UP	AND
	...	SUITE:Configure Hotspot With 802.1x Authentication	AND
	...	SUITE:Configure Client With 802.1x Authentication
	SUITE:Check If Hotspot Is Up
	SUITE:Check If Client Is Up
	SUITE:Get Client, Hotspot IP And MAC Client
	SUITE:Test Ping Through Wlan Interface	admin_left	${CLIENT_IP}
	SUITE:Test Ping Through Wlan Interface	admin_right	${HOTSPOT_IP}
	SUITE:Test Ping Through Wlan Interface	admin_right	google.com
	SUITE:Check Client On Tracking Hotspot Tabble
	[Teardown]	Run Keywords	Skip If		not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	...	AND	SUITE:Delete Client With 802.1x Authentication	AND
	...	SUITE:Disable Wifi Security

Test 802.1x Authentication Wifi AP With Static Ipv4
	Skip If		not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	[Setup]	Run Keywords	Skip If		not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	...	AND	Skip If		${HAS_WIFI} == ${FALSE}	System doesn't have WIFI network UP	AND
	...	SUITE:Configure Hotspot With 802.1x Authentication	SHARED_IPV4=${HOTSPOT_STATIC_IPV4}
	...	BITMASK_IPV4=${BITMASK_STATIC_IPV4}	AND	SUITE:Configure Client With 802.1x Authentication
	...	IPV4_MODE=${MODE_IPV4}[0]	IPV4_ADDRESS=${CLIENT_STATIC_IPV4}	IPV4_BITMASK=${BITMASK_STATIC_IPV4}
	...	IPV4_GATEWAY=${GATEWAY_STATIC_IPV4}
	SUITE:Check If Hotspot Is Up
	SUITE:Check If Client Is Up
	SUITE:Get Client, Hotspot IP And MAC Client
	SUITE:Test Ping Through Wlan Interface	admin_left	${CLIENT_IP}
	SUITE:Test Ping Through Wlan Interface	admin_right	${HOTSPOT_IP}
	SUITE:Test Ping Through Wlan Interface	admin_right	google.com
	SUITE:Check Client On Tracking Hotspot Tabble
	[Teardown]	Run Keywords	Skip If		not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	...	AND	SUITE:Delete Client With 802.1x Authentication	AND
	...	SUITE:Disable Wifi Security

Test 802.1x Authentication Wifi AP With Ipv6 Address Auto Configuration
	Skip If		not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	[Tags]	BUG_NG_5045	NON-CRITICAL	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6
	[Setup]	Run Keywords	Skip If		not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	...	AND	Skip If		${HAS_WIFI} == ${FALSE}	System doesn't have WIFI network UP	AND
	...	SUITE:Configure Hotspot With 802.1x Authentication	HOTSPOT_MODE_IPV6=${HOTSPOT_MODE_IPV6}[1]
	...	AND	SUITE:Configure Client With 802.1x Authentication	IPV4_MODE=${MODE_IPV4}[1]	IPV6_MODE=${MODE_IPV6}[0]
	SUITE:Check If Hotspot Is Up
	SUITE:Check If Client Is Up
	SUITE:Get Client And Hotspot IPv6 And MAC Client
	SUITE:Test Ping Through Wlan Interface	admin_left	${CLIENT_IP6}
	SUITE:Test Ping Through Wlan Interface	admin_right	${HOTSPOT_IP6}
	SUITE:Test Ping Through Wlan Interface	admin_right	google.com
	SUITE:Check Client On Tracking Hotspot Tabble
	[Teardown]	Run Keywords	Skip If		not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	...	AND	SUITE:Delete Client With 802.1x Authentication	AND
	...	SUITE:Disable Wifi Security

Test 802.1x Authentication Wifi AP With Stateful DHCPv6
	Skip If		not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	[Tags]	BUG_NG_5045	NON-CRITICAL	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6
	[Setup]	Run Keywords	Skip If		not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	...	AND	Skip If		${HAS_WIFI} == ${FALSE}	System doesn't have WIFI network UP	AND
	...	SUITE:Configure Hotspot With 802.1x Authentication	HOTSPOT_MODE_IPV6=${HOTSPOT_MODE_IPV6}[1]
	...	AND	SUITE:Configure Client With 802.1x Authentication	IPV4_MODE=${MODE_IPV4}[1]	IPV6_MODE=${MODE_IPV6}[1]
	SUITE:Check If Hotspot Is Up
	SUITE:Check If Client Is Up
	SUITE:Get Client And Hotspot IPv6 And MAC Client
	SUITE:Test Ping Through Wlan Interface	admin_left	${CLIENT_IP6}
	SUITE:Test Ping Through Wlan Interface	admin_right	${HOTSPOT_IP6}
	SUITE:Test Ping Through Wlan Interface	admin_right	google.com
	SUITE:Check Client On Tracking Hotspot Tabble
	[Teardown]	Run Keywords	Skip If		not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	...	AND	SUITE:Delete Client With 802.1x Authentication	AND
	...	SUITE:Disable Wifi Security

Test 802.1x Authentication Wifi AP With Static Ipv6
	Skip If		not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	[Tags]	BUG_NG_5045	NON-CRITICAL	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6
	[Setup]	Run Keywords	Skip If		not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	...	AND	Skip If		${HAS_WIFI} == ${FALSE}	System doesn't have WIFI network UP	AND
	...	SUITE:Configure Hotspot With 802.1x Authentication	HOTSPOT_MODE_IPV6=${HOTSPOT_MODE_IPV6}[1]
	...	AND	SUITE:Configure Client With 802.1x Authentication	IPV4_MODE=${MODE_IPV4}[1]	IPV6_MODE=${MODE_IPV6}[3]
	...	IPV6_ADDRESS=${CLIENT_STATIC_IPV6}	IPV6_BITMASK=${BITMASK_STATIC_IPV6}	IPV6_GATEWAY=${GATEWAY_STATIC_IPV6}
	SUITE:Check If Hotspot Is Up
	SUITE:Check If Client Is Up
	SUITE:Get Client And Hotspot IPv6 And MAC Client
	SUITE:Test Ping Through Wlan Interface	admin_left	${CLIENT_IP6}
	SUITE:Test Ping Through Wlan Interface	admin_right	${HOTSPOT_IP6}
	SUITE:Test Ping Through Wlan Interface	admin_right	google.com
	SUITE:Check Client On Tracking Hotspot Tabble
	[Teardown]	Run Keywords	Skip If		not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	...	AND	SUITE:Delete Client With 802.1x Authentication	AND
	...	SUITE:Disable Wifi Security

Test 802.1x Authentication Wifi AP With WPA2 Validate Server Certificate
	Skip If		not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	[Setup]	Run Keywords	Skip If		not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	...	AND	Skip If		${HAS_WIFI} == ${FALSE}	System doesn't have WIFI network UP	AND
	...	SUITE:Configure WPA2 Validate Server Certificate On Radius Server	AND
	...	SUITE:Configure WPA2 Validate Server Certificate On Client	AND
	...	SUITE:Configure Hotspot With 802.1x Authentication	AND
	...	SUITE:Configure Client With 802.1x Authentication	WPA2_SERVER=yes
	SUITE:Check If Hotspot Is Up
	SUITE:Check If Client Is Up
	SUITE:Get Client, Hotspot IP And MAC Client
	SUITE:Test Ping Through Wlan Interface	admin_left	${CLIENT_IP}
	SUITE:Test Ping Through Wlan Interface	admin_right	${HOTSPOT_IP}
	SUITE:Test Ping Through Wlan Interface	admin_right	google.com
	SUITE:Check Client On Tracking Hotspot Tabble
	[Teardown]	Run Keywords	Skip If		not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	...	AND	SUITE:Delete Client With 802.1x Authentication	AND
	...	SUITE:Disable Wifi Security

*** Keywords ***
SUITE:Setup
	Skip If		not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	CLI:Open	session_alias=admin_left	HOST_DIFF=${HOST}
	CLI:Open	session_alias=admin_right	HOST_DIFF=${HOSTSHARED}
	SUITE:Check If The System Has Wifi
	SUITE:Delete Client With 802.1x Authentication
	SUITE:Disable Wifi Security

SUITE:Teardown
	Skip If		not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	CLI:Close Connection

SUITE:Check If The System Has Wifi
	${OUTPUT}=	CLI:Enter Path	/settings/network_connections/hotspot	RAW_Mode=Yes
	${HAS_HOTSPOT}=	Run Keyword And Return Status	Should Not Contain	${OUTPUT}
	...	Error: Invalid path: hotspot
	Run Keyword If	${HAS_HOTSPOT} == ${FALSE}	SUITE:Without Wifi
	Skip If		${HAS_HOTSPOT} == ${FALSE}	System doesn't have hotspot network
	FOR	${SIDE}	IN	@{ADMINSIDES}
		CLI:Switch Connection	${SIDE}
		Set Client Configuration	prompt=#
		CLI:Write	shell sudo su -
		${OUTPUT}=	Run Keyword And Continue On Failure	CLI:Write	nmcli c up hotspot	Raw
		Run Keyword If	'${SIDE}' == 'admin_left'	SUITE:Check If Left Has Wifi	${OUTPUT}
		Run Keyword If	'${SIDE}' == 'admin_right'	SUITE:Check If Right Has Wifi	${OUTPUT}
		Run Keyword If	'${SIDE}' == 'admin_left'	SUITE:Set Client Configurations And Exit Without Pass
	END
	${HAS_WIFI}=	Set Variable	True
	Set Suite Variable	${HAS_WIFI}
	Run Keyword If	${HAS_WIFI_LEFT} == ${FALSE}	SUITE:Without Wifi
	Run Keyword If	${HAS_WIFI_RIGHT} == ${FALSE}	SUITE:Without Wifi
	[Teardown]	SUITE:Set Client Configurations And Exit Without Pass

SUITE:Without Wifi
	${HAS_WIFI}=	Set Variable	False
	Set Suite Variable	${HAS_WIFI}

SUITE:Check If Left Has Wifi
	[Arguments]	${OUTPUT}
	${HAS_WIFI_LEFT}=	Run Keyword And Return Status	Should Not Contain	${OUTPUT}	Error
	Set Suite Variable	${HAS_WIFI_LEFT}

SUITE:Check If Right Has Wifi
	[Arguments]	${OUTPUT}
	${HAS_WIFI_RIGHT}=	Run Keyword And Return Status	Should Not Contain	${OUTPUT}	Error
	Set Suite Variable	${HAS_WIFI_RIGHT}

SUITE:Set Client Configurations And Exit Without Pass
	Set Client Configuration	prompt=]#
	CLI:Write	exit

SUITE:Disable Wifi Security
	Skip If	${HAS_WIFI} == ${FALSE}	System doesn't have WIFI network UP
	FOR	${SIDE}	IN	@{ADMINSIDES}
		CLI:Switch Connection	${SIDE}
		CLI:Enter Path	/settings/network_connections/hotspot
		CLI:Write	set wifi_security=disabled wifi_ssid=Nodegrid hotspot_ipv6_mode=no_ipv6_address
		CLI:Write	set hotspot_ipv4_mode=server shared_ipv4_address=192.168.162.1 shared_ipv4_bitmask=24
		CLI:Commit
		CLI:Test Show Command	wifi_security = disabled
	END
	[Teardown]	CLI:Revert	Raw

SUITE:Configure Hotspot With 802.1x Authentication
	Skip If		${HAS_WIFI} == ${FALSE}	System doesn't have WIFI network UP
	[Arguments]	${SHARED_IPV4}=${EMPTY}	${BITMASK_IPV4}=${EMPTY}	${MODE_IPV6}=${EMPTY}
	...	${HOTSPOT_MODE_IPV4}=${EMPTY}	${HOTSPOT_MODE_IPV6}=${EMPTY}
	CLI:Switch Connection	admin_left
	Run Keywords	SUITE:Status On Hotspot Root File	AND	Should Contain	'${HAS_CONF}'	'False'
	CLI:Enter Path	/settings/network_connections/hotspot
	CLI:Set	wifi_ssid=${WIFI_SSID} wifi_security=wpa2_enterprise radius_server=${RADIUSSERVER2} radius_port=${RADIUSSERVER2_PORT} radius_secret=${RADIUSSERVER2_SECRET}
	Run Keyword If	'${SHARED_IPV4}' != '${EMPTY}'	CLI:Set	shared_ipv4_address=${SHARED_IPV4}
	Run Keyword If	'${BITMASK_IPV4}' != '${EMPTY}'	CLI:Set	shared_ipv4_bitmask=${BITMASK_IPV4}
	Run Keyword If	'${HOTSPOT_MODE_IPV4}' != '${EMPTY}'	CLI:Set	hotspot_ipv4_mode=${HOTSPOT_MODE_IPV4}
	Run Keyword If	'${MODE_IPV6}' != '${EMPTY}'	CLI:Set	description="test ipv6"
	Run Keyword If	'${HOTSPOT_MODE_IPV6}' != '${EMPTY}'	CLI:Set	hotspot_ipv6_mode=${HOTSPOT_MODE_IPV6}
	CLI:Commit
	CLI:Test Show Command	wifi_security = wpa2_enterprise	wpa2_method = peap	radius_server = ${RADIUSSERVER2}
	...	radius_port = ${RADIUSSERVER2_PORT}	radius_secret = ********
	Run Keywords	SUITE:Status On Hotspot Root File	AND	Should Be True	'${HAS_CONF}'
	[Teardown]	CLI:Cancel	Raw

SUITE:Status On Hotspot Root File
	Skip If		${HAS_WIFI} == ${FALSE}	System doesn't have WIFI network UP
	Set Client Configuration	prompt=#
	CLI:Write	shell sudo su -
	${OUTPUT}=	CLI:Write	cat /etc/NetworkManager/system-connections/hotspot
	${HAS_CONF}=	Run Keyword And Return Status	Should Contain	${OUTPUT}
	...	[802-1x]	auth-server-addr = ${RADIUSSERVER2}
	...	auth-server-port = ${RADIUSSERVER2_PORT}
	...	auth-server-shared-secret = ${RADIUSSERVER2_SECRET}
	Set Suite Variable	${HAS_CONF}
	[Teardown]	SUITE:Set Client Configurations And Exit

SUITE:Set Client Configurations And Exit
	Skip If		${HAS_WIFI} == ${FALSE}	System doesn't have WIFI network UP
	Set Client Configuration	prompt=]#
	CLI:Write	exit

SUITE:Check If Hotspot Is Up
	Skip If		${HAS_WIFI} == ${FALSE}	System doesn't have WIFI network UP
	CLI:Enter Path	/settings/network_connections/
	CLI:Write	down_connection hotspot
	${OUTPUT}=	CLI:Show
	Should Match Regexp	${OUTPUT}	hotspot\\s+not active
	CLI:Write	up_connection hotspot
	${OUTPUT}=	CLI:Show
	Should Match Regexp	${OUTPUT}	hotspot\\s+\\w+\\s+\\w+\\s+\\w+\\s+up
	[Teardown]	CLI:Cancel	Raw

SUITE:Configure Client With 802.1x Authentication
	Skip If		${HAS_WIFI} == ${FALSE}	System doesn't have WIFI network UP
	[Arguments]	${IPV4_MODE}=${EMPTY}	${IPV4_ADDRESS}=${EMPTY}	${IPV4_BITMASK}=${EMPTY}
	...	${IPV4_GATEWAY}=${EMPTY}	${IPV6_MODE}=${EMPTY}	${IPV6_ADDRESS}=${EMPTY}
	...	${IPV6_BITMASK}=${EMPTY}	${IPV6_GATEWAY}=${EMPTY}	${WPA2_SERVER}=${EMPTY}
	CLI:Switch Connection	admin_right
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	hostname=${CLIENT_HOSTNAME}
	CLI:Commit
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CLIENT_NAME} type=wifi ethernet_interface=wlan0 connect_automatically=no
	CLI:Set	wifi_ssid=${WIFI_SSID} wifi_security=wpa2_enterprise
	CLI:Set	wpa2_username=${RADIUSSERVER2_USER_TEST1} wpa2_password=${RADIUSSERVER2_USER_TEST1_PASSWORD}
	Run Keyword If	'${IPV4_MODE}' != '${EMPTY}'	CLI:Set	ipv4_mode=${IPV4_MODE}
	Run Keyword If	'${IPV4_ADDRESS}' != '${EMPTY}'	CLI:Set	ipv4_address=${IPV4_ADDRESS}
	Run Keyword If	'${IPV4_BITMASK}' != '${EMPTY}'	CLI:Set	ipv4_bitmask=${IPV4_BITMASK}
	Run Keyword If	'${IPV4_GATEWAY}' != '${EMPTY}'	CLI:Set	ipv4_gateway=${IPV4_GATEWAY}
	Run Keyword If	'${IPV6_MODE}' != '${EMPTY}'	CLI:Set	ipv6_mode=${IPV6_MODE} enable_lldp=yes
	Run Keyword If	'${IPV6_ADDRESS}' != '${EMPTY}'	CLI:Set	ipv6_address=${IPV6_ADDRESS}
	Run Keyword If	'${IPV6_BITMASK}' != '${EMPTY}'	CLI:Set	ipv6_prefix_length=${IPV6_BITMASK}
	Run Keyword If	'${IPV6_GATEWAY}' != '${EMPTY}'	CLI:Set	ipv6_gateway=${IPV6_GATEWAY}
	Run Keyword If	'${WPA2_SERVER}' != '${EMPTY}'	CLI:Set	wpa2_validate_server_certificate=yes
	CLI:Commit
	[Teardown]	CLI:Cancel	Raw

SUITE:Check If Client Is Up
	Skip If		${HAS_WIFI} == ${FALSE}	System doesn't have WIFI network UP
	CLI:Enter Path	/settings/network_connections/
	CLI:Write	down_connection hotspot
	${OUTPUT}=	CLI:Show
	Should Match Regexp	${OUTPUT}	hotspot\\s+not active
	CLI:Write	up_connection ${CLIENT_NAME}
	${OUTPUT}=	CLI:Show
	Should Match Regexp	${OUTPUT}	${CLIENT_NAME}\\s+\\w+\\s+\\w+\\s+\\w+\\s+up
	[Teardown]	CLI:Cancel	Raw

SUITE:Delete Client With 802.1x Authentication
	Skip If		${HAS_WIFI} == ${FALSE}	System doesn't have WIFI network UP
	CLI:Switch Connection	admin_right
	CLI:Enter Path	/settings/network_connections/
	CLI:Delete If Exists	${CLIENT_NAME}
	CLI:Commit
	${OUTPUT}=	CLI:Show
	Should Not Contain	${OUTPUT}	${CLIENT_NAME}
	[Teardown]	CLI:Cancel	Raw

SUITE:Get Client, Hotspot IP And MAC Client 
	Skip If		${HAS_WIFI} == ${FALSE}	System doesn't have WIFI network UP
	CLI:Switch Connection	admin_left
	${LINE}	${LINE2}=	SUITE:Get Line On Root Mode
	${IP}=	Get Substring	${LINE}	40	58
	${HOTSPOT_IP}=	Remove String Using Regexp	${IP}	(?:\\/..\\s*)
	Set Suite Variable	${HOTSPOT_IP}
	SUITE:Set Client Configurations And Exit
	CLI:Switch Connection	admin_right
	${LINE}	${LINE2}=	SUITE:Get Line On Root Mode
	${IP}=	Get Substring	${LINE}	40	58
	${CLIENT_IP}=	Remove String Using Regexp	${IP}	(?:\\/..\\s*)
	${CLIENT_MAC}=	Get Substring	${LINE2}	40	57
	Set Suite Variable	${CLIENT_IP}
	Set Suite Variable	${CLIENT_MAC}
	[Teardown]	SUITE:Set Client Configurations And Exit

SUITE:Get Line On Root Mode
	Set Client Configuration	prompt=#
	CLI:Write	shell sudo su -
	${OUTPUT}=	CLI:Write	nmcli d show | grep -A10 'wlan0'	Raw
	@{GET_LINES}=	Split To Lines	${OUTPUT}
	${LINE}=	Get From List	${GET_LINES}	7
	${LINE2}=	Get From List	${GET_LINES}	2
	[Return]	${LINE}	${LINE2}

SUITE:Test Ping Through Wlan Interface
	Skip If		${HAS_WIFI} == ${FALSE}	System doesn't have WIFI network UP
	[Arguments]	${SIDE}	${IP}
	CLI:Switch Connection	${SIDE}
	${STATUS}=	Run Keyword And Return Status	CLI:Test Ping
	...	IP_ADDRESS=${IP}	SOURCE_INTERFACE=wlan0
	Run Keyword If	'${STATUS}' == 'False'	Fail	Ping Should Work
	[Teardown]	CLI:Cancel	Raw

SUITE:Check Client On Tracking Hotspot Tabble
	Skip If		${HAS_WIFI} == ${FALSE}	System doesn't have WIFI network UP
	CLI:Switch Connection	admin_left
	CLI:Enter Path	/system/hotspot/
	Sleep	3s
	${OUTPUT}=	CLI:Show
	${GET_LINES}=	Get Lines Containing String	${OUTPUT}	${CLIENT_HOSTNAME}
	@{MATCHS}=	Get Regexp Matches	${GET_LINES}	..:..:..:..:..:..
	${CLINT_MAC_ON_HOTSPOT}=	Convert To Upper Case	${MATCHS}[0]
	Should Be Equal	${CLINT_MAC_ON_HOTSPOT}	${CLIENT_MAC}
	[Teardown]	CLI:Cancel	Raw

SUITE:Get Client And Hotspot IPv6 And MAC Client
	Skip If		${HAS_WIFI} == ${FALSE}	System doesn't have WIFI network UP
	CLI:Switch Connection	admin_left
	${LINE}	${LINE2}=	SUITE:Get Line With ipv6 On Root Mode
	${IP6}=	Get Substring	${LINE}	40	73
	${HOTSPOT_IP6}=	Remove String Using Regexp	${IP6}	(?:\\/..\\s*)
	Set Suite Variable	${HOTSPOT_IP6}
	SUITE:Set Client Configurations And Exit
	CLI:Switch Connection	admin_right
	${LINE}	${LINE2}=	SUITE:Get Line With ipv6 On Root Mode
	${IP6}=	Get Substring	${LINE}	40	73
	${CLIENT_IP6}=	Remove String Using Regexp	${IP6}	(?:\\/..\\s*)
	${CLIENT_MAC}=	Get Substring	${LINE2}	40	57
	Set Suite Variable	${CLIENT_IP6}
	Set Suite Variable	${CLIENT_MAC}
	[Teardown]	SUITE:Set Client Configurations And Exit

SUITE:Get Line With ipv6 On Root Mode
	Skip If		${HAS_WIFI} == ${FALSE}	System doesn't have WIFI network UP
	Set Client Configuration	prompt=#
	CLI:Write	shell sudo su -
	${OUTPUT}=	CLI:Write	nmcli d show | grep -A15 'wlan0'	Raw
	@{GET_LINES}=	Split To Lines	${OUTPUT}
	${LINE}=	Get From List	${GET_LINES}	10
	${LINE2}=	Get From List	${GET_LINES}	2
	[Return]	${LINE}	${LINE2}

SUITE:Configure WPA2 Validate Server Certificate On Radius Server
	Skip If		${HAS_WIFI} == ${FALSE}	System doesn't have WIFI network UP
	${LIST}	Create List	Signature ok	${COUNTRY_NAME}	${STATE_NAME}	${LOCALITY_NAME}	${EMAIL_DESTINATION2}
	...	${UNIT_NAME}
	CLI:Open	USERNAME=root	PASSWORD=${ROOT_PASSWORD}	session_alias=radius_server	HOST_DIFF=${RADIUSSERVER2}
	CLI:Write	mv /etc/freeradius/certs /etc/freeradius/certs.back	Raw
	CLI:Write	mkdir -p /etc/freeradius/certs/new && cd /etc/freeradius/certs/new	Raw
	CLI:Write	openssl genrsa -out ca.key 1024	Raw
	Set Client Configuration	prompt=:
	CLI:Write	openssl req -new -key ca.key -out ca.csr	Raw
	CLI:Write	${COUNTRY_NAME}	Raw
	CLI:Write	${STATE_NAME}	Raw
	CLI:Write	${LOCALITY_NAME}	Raw
	CLI:Write	${ORGANIZATION_NAME}	Raw
	CLI:Write	${UNIT_NAME}	Raw
	CLI:Write	${ORGANIZATION_NAME}	Raw
	CLI:Write	${EMAIL_DESTINATION2}	Raw
	CLI:Write	${QA_PASSWORD}	Raw
	Set Client Configuration	prompt=#
	CLI:Write	${ORGANIZATION_NAME}	Raw
	CLI:Write	openssl x509 -days 1095 -signkey ca.key -in ca.csr -req -out ca.pem	Raw
	CLI:Write	echo -ne '01' > ca.serial	Raw
	CLI:Write	openssl genrsa -out server.key 1024	Raw
	Set Client Configuration	prompt=:
	CLI:Write	openssl req -new -key server.key -out server.csr	Raw
	CLI:Write	${COUNTRY_NAME}	Raw
	CLI:Write	${STATE_NAME}	Raw
	CLI:Write	${LOCALITY_NAME}	Raw
	CLI:Write	${ORGANIZATION_NAME}	Raw
	CLI:Write	${UNIT_NAME}	Raw
	CLI:Write	${LOCALITY_NAME}	Raw
	CLI:Write	${EMAIL_DESTINATION2}	Raw
	CLI:Write	${QA_PASSWORD}	Raw
	Set Client Configuration	prompt=#
	CLI:Write	${ORGANIZATION_NAME}	Raw
	${OUTPUT}=	CLI:Write	openssl x509 -days 730 -CA ca.pem -CAkey ca.key -CAserial ca.serial -in server.csr -req -out server.pem	Raw
	Run Keyword And Continue On Failure	CLI:Should Contain All	${OUTPUT}	${LIST}
	CLI:Write	cp server.{pem,key} ../	Raw
	CLI:Write	cp ca.pem ../	Raw
	CLI:Write	openssl dhparam -out /etc/freeradius/certs/dh 1024	Raw
	CLI:Write	/etc/init.d/freeradius restart	Raw
	[Teardown]	SUITE:CLI:Switch Connection And Set Client Configurations

SUITE:CLI:Switch Connection And Set Client Configurations
	Skip If		${HAS_WIFI} == ${FALSE}	System doesn't have WIFI network UP
	CLI:Switch Connection	admin_left
	Set Client Configuration	prompt=]#

SUITE:Configure WPA2 Validate Server Certificate On Client
	Skip If		${HAS_WIFI} == ${FALSE}	System doesn't have WIFI network UP
	${LIST}=	Create List	rehash:	warning:	ca-certificates.crt	certificate	CRL
	Set Client Configuration	prompt=#
	CLI:Write	shell sudo su -
	Set Client Configuration	prompt=?
	Set Client Configuration	timeout=5s
	${ECDSA}=	Run Keyword And Return Status	CLI:Write
	...	scp root@192.168.3.8:/etc/freeradius/certs/new/ca.pem /etc/ssl/certs/radius_ca.pem	Raw
	Set Client Configuration	timeout=30s
	Run Keyword If	'${ECDSA}' == 'True'	SUITE:ECDSA
	Run Keyword If	'${ECDSA}' == 'False'	Set Client Configuration	prompt=#
	CLI:Write	${ROOT_PASSWORD}	Raw
	CLI:Write	openssl rehash /etc/ssl/certs	Raw
	${OUTPUT}=	CLI:Write	openssl rehash /etc/ssl/certs	Raw
	Run Keyword And Continue On Failure	CLI:Should Contain All	${OUTPUT}	${LIST}
	[Teardown]	SUITE:Set Client Configurations And Exit

SUITE:ECDSA
	Set Client Configuration	prompt=:
	CLI:Write	yes	Raw
	Set Client Configuration	prompt=#