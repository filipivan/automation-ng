*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Network Connections Configurations for type Bridge through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN5_0
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
@{AVAILABLE_INTERFACES}
${CUSTOM_MAC_1}	00:11:22:33:44:55
${CUSTOM_MAC_2}	00:ff:ee:dd:bb:aa
${BRIDGE_NAME}	test_bridge
${BRIDGE_INTERFACE}	eth1
*** Test Cases ***
Test Set ETH0 as Bridge Interfaces
	[Tags]	NON-CRITICAL	#new_test
	${IP}	SUITE:Check Interface	eth0
	Append To List	${AVAILABLE_INTERFACES}	eth0
	SUITE:Add Bridge Interface	eth0	${IP}
	SUITE:Delete Interface	bridge_eth0

Test Set ETH1 as Bridge Interfaces
	[Tags]	NON-CRITICAL	#new_test
	${IP}	SUITE:Check Interface	eth1
	Append To List	${AVAILABLE_INTERFACES}	eth1
	SUITE:Add Bridge Interface	eth1	${IP}
	SUITE:Delete Interface	bridge_eth1

Test Set BACKPLANE0 as Bridge Interfaces
	[Tags]	NON-CRITICAL	#new_test
	${IP}	SUITE:Check Interface	backplane0
	Append To List	${AVAILABLE_INTERFACES}	backplane0
	SUITE:Add Bridge Interface	backplane0	${IP}
	SUITE:Delete Interface	bridge_backplane0

Test Set BACKPLANE1 as Bridge Interfaces
	[Tags]	NON-CRITICAL	#new_test
	${IP}	SUITE:Check Interface	backplane1
	Append To List	${AVAILABLE_INTERFACES}	backplane1
	SUITE:Add Bridge Interface	backplane1	${IP}
	SUITE:Delete Interface	bridge_backplane1

Test Set HOTSPOT as Bridge Interfaces
	[Tags]	NON-CRITICAL	#new_test
	${IP}	SUITE:Check Interface	hotspot
	Append To List	${AVAILABLE_INTERFACES}	hotspot
	SUITE:Add Bridge Interface	hotspot	${IP}
	SUITE:Delete Interface	bridge_hotspot

Test Change Bridge Interfaces
	[Tags]	NON-CRITICAL	NUG_NG_8803	#new_test
	${IP}	SUITE:Check Interface	eth1
	CLI:Delete If Exists	bridge_eth0
	SUITE:Add Bridge Interface	eth0	${HOST}
	SUITE:Edit Bridge Interfaces	eth0	eth1	${IP}
	SUITE:Delete Interface	bridge_eth0

Test Set Multiple Interfaces On Same Bridge
	[Tags]	NON-CRITICAL	BUG_NG_8739	#new_test
	Skip If	True	BUG_NG_8739	#new_test
	SUITE:Add Bridge Interface	MULTIPLE	MULTIPLE=yes
	SUITE:Delete Interface	bridge_MULTIPLE

Test add bridge connection with custom MAC address
	[Documentation]	Test adds a bridge connection with only ${BRIDGE_INTERFACE} as slave and ${CUSTOM_MAC_1} as MAC
	...	 address, then check if this custom address was really applied in NG context with show command
	[Tags]	NON-CRITICAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	SUITE:Add Bridge With Custom MAC	${CUSTOM_MAC_1}
	@{REQUIRED_INFO}	Create List	${BRIDGE_NAME}	bridge	br0	${CUSTOM_MAC_1}
	${HAS_CUSTOM_MAC}	CLI:Show	/settings/network_connections
	CLI:Should Contain All	${HAS_CUSTOM_MAC}	${REQUIRED_INFO}
	[Teardown]	CLI:Delete If Exists	${BRIDGE_NAME}

Test check if custom MAC address is configured correctly with ifconfig
	[Documentation]	Test adds a bridge connection with only ${BRIDGE_INTERFACE} as slave and ${CUSTOM_MAC_1} as MAC
	...	 address, then check if this custom address was really applied in network context with ifconfig command
	[Tags]	NON-CRITICAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	SUITE:Add Bridge With Custom MAC	${CUSTOM_MAC_1}
	CLI:Connect As Root
	${BR0_CONFIG}	CLI:Write	ifconfig br0
	Should Contain	${BR0_CONFIG}	ether ${CUSTOM_MAC_1}
	CLI:Close Current Connection
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Delete If Exists	${BRIDGE_NAME}

Test edit custom MAC from bridge interface
	[Documentation]	Test adds a bridge connection with ${CUSTOM_MAC_1} and then edits it to have ${CUSTOM_MAC_2}
	...	and checks, using show command, that it has the new address and not the old one. After that checks it was updated
	...	also using ifconfig command
	[Tags]	NON-CRITICAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	SUITE:Add Bridge With Custom MAC	${CUSTOM_MAC_1}
	CLI:Enter Path	/settings/network_connections/${BRIDGE_NAME}
	CLI:Set	bridge_mac_address=${CUSTOM_MAC_2}
	CLI:Commit
	@{REQUIRED_INFO}	Create List	${BRIDGE_NAME}	bridge	br0	${CUSTOM_MAC_2}
	CLI:Enter Path	/settings/network_connections
	${HAS_CUSTOM_MAC}	CLI:Show
	CLI:Should Contain All	${HAS_CUSTOM_MAC}	${REQUIRED_INFO}
	Should Not Contain	${HAS_CUSTOM_MAC}	${CUSTOM_MAC_1}
	CLI:Connect As Root
	${BR0_CONFIG}	CLI:Write	ifconfig br0
	Should Contain	${BR0_CONFIG}	ether ${CUSTOM_MAC_2}
	Should Not Contain	${BR0_CONFIG}	ether ${CUSTOM_MAC_1}
	CLI:Close Current Connection
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Delete If Exists	${BRIDGE_NAME}

Test edit bridge to use MAC from first interface
	[Documentation]	Test adds a bridge connection with ${CUSTOM_MAC_1} and then edits it to have the MAC address from the
	...	first interface. Test checks, using show command, that it has the new address and not the old one. After that
	...	checks it was updated also using ifconfig command
	[Tags]	NON-CRITICAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	${MAC_INTERFACE}	CLI:Get Interface MAC Address	${BRIDGE_INTERFACE}
	SUITE:Add Bridge With Custom MAC	${CUSTOM_MAC_1}
	CLI:Enter Path	/settings/network_connections/${BRIDGE_NAME}
	CLI:Set	bridge_mac_configuration=use_mac_from_first_interface
	CLI:Commit
	@{REQUIRED_INFO}	Create List	${BRIDGE_NAME}	bridge	br0	${MAC_INTERFACE}
	${HAS_CUSTOM_MAC}	CLI:Show	/settings/network_connections
	CLI:Should Contain All	${HAS_CUSTOM_MAC}	${REQUIRED_INFO}
	Should Not Contain	${HAS_CUSTOM_MAC}	${CUSTOM_MAC_1}
	[Teardown]	CLI:Delete If Exists	${BRIDGE_NAME}

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Cancel	raw
	CLI:Enter Path	/settings/network_connections/
	write	delete bridge_eth0
	Sleep	10s
	Read Until Prompt
	write	delete ${BRIDGE_NAME}
	Sleep	10s
	Read Until Prompt
	CLI:Delete If Exists	bridge_MULTIPLE	bridge_eth0	bridge_hotspot	bridge_backplane1	bridge_backplane0	bridge_eth1
	CLI:Close Connection

SUITE:Add Bridge Interface
	[Arguments]	${INTERFACE}	${IP}=127.0.0.1	${MULTIPLE}=no
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=bridge name=bridge_${INTERFACE}
	Run Keyword If	'${MULTIPLE}'=='no'	CLI:Set	bridge_interfaces=${INTERFACE}
	...	ELSE	SUITE:Set Multiple Bridge Interfaces	${AVAILABLE_INTERFACES}
	CLI:Commit
	Wait Until Keyword Succeeds	5x	5s	SUITE:Check Bridge Creation	${INTERFACE}	${IP}

SUITE:Edit Bridge Interfaces
	[Arguments]	${INTERFACE}	${NEW_INTERFACES}	${IP}
	CLI:Enter Path	/settings/network_connections/bridge_${INTERFACE}
	CLI:Set	bridge_interfaces=${NEW_INTERFACES}
	CLI:Commit
	CLI:Enter Path	/settings/network_connections/
	Wait Until Keyword Succeeds	6x	10s	SUITE:Check Bridge Creation	${INTERFACE}	${IP}

SUITE:Set Multiple Bridge Interfaces
	[Arguments]	${INTERFACE}
	${INTERFACES_NAME}	Set Variable	${EMPTY}
	FOR	${INTERFACE}	IN	@{INTERFACE}
		${INTERFACES_NAME}	Set Variable	${INTERFACE}${INTERFACES_NAME}${SPACE}
	END
#	CLI:Set	bridge_interfaces=${INTERFACES_NAME}	#BUG_NG_8739

SUITE:Check Bridge Creation
	[Arguments]	${INTERFACE}	${IP}
	${OUTPUT}	CLI:Show
	@{LINES}	Split To Lines	${OUTPUT}	2
	FOR	${LINE}	IN	@{LINES}
		${STATUS}	Run Keyword and Return Status	Should Contain	${LINE}	bridge_${INTERFACE}
		${INTERFACE_LINE}	Run Keyword If	${STATUS}	Set Variable	${LINE}
		Exit For Loop If	${STATUS}
	END
	Should Match Regexp	${INTERFACE_LINE}	bridge_${INTERFACE}(\\s)+connected(\\s)+bridge(\\s)+br[0-9]+(\\s)+up(\\s)+${IP}(.)*

SUITE:Check Interface
	[Arguments]	${INTERFACE}
	${HAS_INTERFACE}	CLI:Has Interface	${INTERFACE}
	Skip If	not ${HAS_INTERFACE}	Host Device doesn't have ${INTERFACE}
	${HAS_IP}	${IP}	SUITE:Get Interface Ip	${INTERFACE}
	Skip If	not ${HAS_IP}	Interface ${INTERFACE} doesn't have IP address
	[Return]	${IP}

SUITE:Get Interface Ip
	[Arguments]	${INTERFACE}
	${IP}	CLI:Get Interface IP Address	${INTERFACE}
	${HAS_IP}	Run Keyword And Return Status	Should Match Regexp	${IP}	192.168.[0-9][0-9]?[0-9]?.[0-9][0-9]?[0-9]?
	[Return]	${HAS_IP}	${IP}

SUITE:Delete Interface
	[Arguments]	${INTERFACE}
	CLI:Enter Path	/settings/network_connections/
	Write	delete ${INTERFACE}
	Read Until	]#

SUITE:Delete Bridge Interface
	[Arguments]	${INTERFACE}
	CLI:Enter Path	/settings/network_connections/
	CLI:Delete	bridge_${INTERFACE}

SUITE:Add Bridge With Custom MAC
	[Arguments]	${MAC_ADDR}
	CLI:Enter Path	/settings/network_connections
	CLI:Add
	CLI:Set	name=${BRIDGE_NAME} type=bridge bridge_interfaces=${BRIDGE_INTERFACE}
	CLI:Set	bridge_mac_configuration=bridge_custom_mac bridge_mac_address=${MAC_ADDR}
	CLI:Commit