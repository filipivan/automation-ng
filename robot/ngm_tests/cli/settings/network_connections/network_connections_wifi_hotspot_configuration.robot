*** Settings ***
Documentation	Testing Wi-Fi configuration for hotspot and client
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags	CLI	NON-CRITICAL
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SSID_NAME}	testWIFI_automation
${PRE-SHARED_KEY}	${QA_PASSWORD}
${SERVER}	${RADIUSSERVER3}
${SECRET}	${RADIUSSERVER3_SECRET}
${USERNAME}	${RADIUSSERVER2_USER_TEST1}
${PASSWORD}	${RADIUSSERVER2_USER_TEST1_PASSWORD}
${CLIENT_NAME}	hostshared_client
${HOTSPOT_IP}	192.168.162.1

*** Test Cases ***
Test hotspot with security disabled with 2.4Ghz
	[Documentation]	Test hotspot could be configured with security as disabled and band as 2.4Ghz
	SUITE:Configure Hotspot	disabled	2_4ghz

Test hotspot with WPA2 Personal 2.4Ghz
	[Documentation]	Test hotspot could be configured with security as wpa2_personal and band as 2.4Ghz
	SUITE:Configure Hotspot	wpa2_personal	2_4ghz

Test hotspot with WPA2 Enterprise 2.4Ghz
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
	[Documentation]	Test hotspot could be configured with security as wpa2_enterprise and band as 2.4Ghz
	SUITE:Configure Hotspot	wpa2_enterprise	2_4ghz

Test hotspot with WPA3 2.4Ghz
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	[Documentation]	Test hotspot could be configured with security as wpa3_personal and band as 2.4Ghz
	SUITE:Configure Hotspot	wpa3_personal	2_4ghz

Test hotspot with security disabled with 5Ghz
	[Documentation]	Test hotspot could be configured with security as disabled and band as 5Ghz
	SUITE:Configure Hotspot	disabled	5ghz

Test hotspot with WPA2 Personal 5Ghz
	[Documentation]	Test hotspot could be configured with security as wpa2_personal and band as 5Ghz
	SUITE:Configure Hotspot	wpa2_personal	5ghz

Test hotspot with WPA2 Enterprise 5Ghz
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
	[Documentation]	Test hotspot could be configured with security as wpa2_enterprise and band as 5Ghz
	SUITE:Configure Hotspot	wpa2_enterprise	5ghz

Test hotspot with WPA3 5Ghz
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	[Documentation]	Test hotspot could be configured with security as wpa3_personal and band as 5Ghz
	SUITE:Configure Hotspot	wpa3_personal	5ghz

Test wifi client with security disabled
	[Documentation]	Test wifi client can be configured with security as disabled
	SUITE:Configure WiFi Client	disabled
	[Teardown]	CLI:Delete If Exists	${CLIENT_NAME}

Test wifi client with WPA2 Personal
	[Documentation]	Test wifi client can be configured with security as wpa2_personal
	SUITE:Configure WiFi Client	wpa2_personal
	[Teardown]	CLI:Delete If Exists	${CLIENT_NAME}

Test wifi client with WPA2 Enterprise
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
	[Documentation]	Test wifi client can be configured with security as wpa2_enterprise
	SUITE:Configure WiFi Client	wpa2_enterprise
	[Teardown]	CLI:Delete If Exists	${CLIENT_NAME}

Test wifi client with WPA3
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8
	[Documentation]	Test wifi client can be configured with security as wpa3_personal
	SUITE:Configure WiFi Client	wpa3_personal
	[Teardown]	CLI:Delete If Exists	${CLIENT_NAME}

*** Keywords ***
SUITE:Setup
	CLI:Open	session_alias=server

SUITE:Teardown
	CLI:Delete If Exists	${CLIENT_NAME}
	CLI:Close Connection

SUITE:Configure Hotspot
	[Documentation]	Keyword configures the hotspot connection and all its necessary fields according to ${SECURITY} option.
	...	Is also possible to specify the desired Wifi band using ${BAND} argument and the region using ${REGION}.
	[Arguments]	${SECURITY}=disabled	${BAND}=2_4ghz	${REGION}=00
	CLI:Switch Connection	server
	CLI:Enter Path	/settings/network_connections/hotspot/
	IF	'${SECURITY}' == 'wpa2_enterprise'
		CLI:Set	wifi_security=${SECURITY} radius_secret=${SECRET} radius_server=${SERVER} wpa2_method=peap radius_port=1812
		CLI:Set	wifi_ssid=${SSID_NAME} region=${REGION} wifi_band=${BAND} radius_secret=${SECRET}
	ELSE
		CLI:Set	wifi_security=${SECURITY} psk=${PRE-SHARED_KEY}
		CLI:Set	wifi_ssid=${SSID_NAME} region=${REGION} wifi_band=${BAND}
	END
	CLI:Commit

SUITE:Configure WiFi Client
	[Documentation]	Keyword configures the wifi connection and all its necessary fields according to ${SECURITY} option.
	[Arguments]	${SECURITY}=disabled
	CLI:Switch Connection	server
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=wifi name=${CLIENT_NAME}
	CLI:Set	wifi_ssid=${SSID_NAME} wifi_bssid=${CLIENT_NAME}
	CLI:Set	hidden_network=no connect_automatically=no
	CLI:Set	wifi_security=${SECURITY}
	IF	'${SECURITY}' == 'wpa2_enterprise'
		CLI:Set	wpa2_method=peap wpa2_phase2_auth=mschapv2
		CLI:Set	wpa2_username=${USERNAME} wpa2_password=${PASSWORD}
		CLI:Set	wpa2_validate_server_certificate=no
	ELSE
		CLI:Set	psk=${PRE-SHARED_KEY}
	END
	CLI:Commit