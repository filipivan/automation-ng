*** Settings ***
Resource	../../init.robot
Documentation	Tests defining a /31 network between two point-to-point links
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	DEPENDENCE_SWITCH	NON-CRITICAL	NEED-REVIEW
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${LINK1_IP_ADDRESS}	10.31.31.0
${LINK2_IP_ADDRESS}	10.31.31.1
${VLAN}	vlan100
${BACKPLANE}	backplane0
${LINK1_INTERFACE}	${SLASH31_HOST_INTERFACE}
${LINK2_INTERFACE}	${SLASH31_SHARED_INTERFACE}

*** Test Cases ***
Test Ping Link 2 From Link 1
	Skip If	not ${SLASH31_AVAILABLE}	/31 tests not configured/available for this testing build
	CLI:Switch Connection	link1_session
	CLI:Test Ping	${LINK2_IP_ADDRESS}	5	SOURCE_INTERFACE=${SOURCE_IF_LINK1}

Test Ping Link 1 From Link 2
	Skip If	not ${SLASH31_AVAILABLE}	/31 tests not configured/available for this testing build
	CLI:Switch Connection	link2_session
	CLI:Test Ping	${LINK1_IP_ADDRESS}	5	SOURCE_INTERFACE=${SOURCE_IF_LINK2}

*** Keywords ***
SUITE:Setup
	Skip If	not ${SLASH31_AVAILABLE}	/31 tests not configured/available for this testing build
	CLI:Open	HOST_DIFF=${HOST}	session_alias=link1_session
	CLI:Open	HOST_DIFF=${HOSTSHARED}	session_alias=link2_session
	SUITE:Configure /31 Network Between Link 1 And Link 2

SUITE:Teardown
	Skip If	not ${SLASH31_AVAILABLE}	/31 tests not configured/available for this testing build
	SUITE:Reset /31 Network Configuration
	CLI:Close Connection

SUITE:Configure /31 Network Between Link 1 And Link 2
	Run Keyword If	${SLASH31_USES_SFP}
	...	SUITE:Setup Switch Interface	link1_session	${LINK1_INTERFACE}	${LINK1_IP_ADDRESS}
	...	ELSE	SUITE:Setup Non Switch Interface  link1_session	${LINK1_INTERFACE}	${LINK1_IP_ADDRESS}
	Run Keyword If	${SLASH31_USES_SFP}
	...	Set Suite Variable	${SOURCE_IF_LINK1}	${BACKPLANE}.100
	...	ELSE	Set Suite Variable	${SOURCE_IF_LINK1}	${LINK1_INTERFACE}

	Run Keyword If	${SLASH31_USES_SFP}
	...	SUITE:Setup Switch Interface	link2_session	${LINK2_INTERFACE}	${LINK2_IP_ADDRESS}
	...	ELSE	SUITE:Setup Non Switch Interface  link2_session	${LINK2_INTERFACE}	${LINK2_IP_ADDRESS}
	Run Keyword If	${SLASH31_USES_SFP}
	...	Set Suite Variable	${SOURCE_IF_LINK2}	${BACKPLANE}.100
	...	ELSE	Set Suite Variable	${SOURCE_IF_LINK2}	${LINK2_INTERFACE}
	
	SUITE:Send 1 Packet After Stablishing Connection

SUITE:Setup Non Switch Interface
	[Arguments]	${SESSION_ALIAS}	${INTERFACE}	${IP_ADDRESS}
	CLI:Switch Connection	${SESSION_ALIAS}
	${INTERFACE_UPPER}=	Convert To Uppercase	${INTERFACE}
	CLI:Configure INTERFACE with IPv4 Address	${IP_ADDRESS}	31	${INTERFACE_UPPER}

SUITE:Setup Switch Interface
	[Arguments]	${SESSION_ALIAS}	${INTERFACE}	${IP_ADDRESS}
	CLI:Switch Connection	${SESSION_ALIAS}
	CLI:Delete Network Connections	${VLAN}
	CLI:Delete VLAN	100
	CLI:Enable Switch Interface	${INTERFACE}
	${BACKPLANE_UPPER}=	Convert To Uppercase	${BACKPLANE}
	CLI:Add VLAN	100	UNTAGGED_PORTS=${BACKPLANE},${INTERFACE}
	CLI:Edit VLAN	100	TAGGED_PORTS=${BACKPLANE},${INTERFACE}
	CLI:Add Static IPv4 Network Connection	${VLAN}	${IP_ADDRESS}	vlan
	...	${BACKPLANE}	31	100

SUITE:Reset /31 Network Configuration
	Run Keyword If	${SLASH31_USES_SFP}
	...	SUITE:Reset Switch Interface Configuration	link1_session	${LINK1_INTERFACE}
	...	ELSE	SUITE:Reset Non Switch Interface Configuration	link1_session	${LINK1_INTERFACE}

	Run Keyword If	${SLASH31_USES_SFP}
	...	SUITE:Reset Switch Interface Configuration	link2_session	${LINK2_INTERFACE}
	...	ELSE	SUITE:Reset Non Switch Interface Configuration	link2_session	${LINK2_INTERFACE}

SUITE:Reset Non Switch Interface Configuration
	[Arguments]	${SESSION_ALIAS}	${INTERFACE}
	CLI:Switch Connection	${SESSION_ALIAS}
	${INTERFACE_UPPER}=	Convert To Uppercase	${INTERFACE}
	SUITE:Configure Interface With Ipv4 Mode DHCP	${INTERFACE_UPPER}

SUITE:Reset Switch Interface Configuration
	[Arguments]	${SESSION_ALIAS}	${INTERFACE}
	CLI:Switch Connection	${SESSION_ALIAS}
	${BACKPLANE_UPPER}=	Convert To Uppercase	${BACKPLANE}
	CLI:Delete Network Connections	${VLAN}
	CLI:Delete VLAN	100
	CLI:Disable Switch Interface	${INTERFACE}
	SUITE:Configure Interface With Ipv4 Mode DHCP	${BACKPLANE_UPPER}

SUITE:Configure Interface With Ipv4 Mode DHCP
	[Arguments]	${INTERFACE}
	CLI:Enter Path	/settings/network_connections/${INTERFACE}
	CLI:Set	ipv4_mode=dhcp
	CLI:Commit

SUITE:Send 1 Packet After Stablishing Connection
	CLI:Switch Connection	link1_session
	Run Keyword And Ignore Error	CLI:Test Ping	${LINK2_IP_ADDRESS}	1	SOURCE_INTERFACE=${SOURCE_IF_LINK1}
	CLI:Switch Connection	link2_session
	Run Keyword And Ignore Error	CLI:Test Ping	${LINK1_IP_ADDRESS}	1	SOURCE_INTERFACE=${SOURCE_IF_LINK2}