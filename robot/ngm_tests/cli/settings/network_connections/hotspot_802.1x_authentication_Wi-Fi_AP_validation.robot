*** Settings ***
Resource        ../../init.robot
Documentation   Validation tests through the CLI about AT for 802.1x authentication to be available for Wifi AP
Metadata        Version  1.0
Metadata        Executed At  ${HOST}
Force Tags      CLI  EXCLUDEIN3_2  EXCLUDEIN4_2  WIFI
Default Tags    CLI  SSH

Suite Setup     SUITE:Setup
Suite Teardown  SUITE:Teardown

*** Variables ***
@{VALID_IPS}            192.168.15.60  192.168.16.85  192.168.17.55  0.0.0.0  255.255.255.255
@{INVALID_IPS}          20.20  10.10.10  300.300.300.300  -1.-1.-1.-1  0,5.0,5.0,5.0,5  0.00.00.00  ${EMPTY}
@{ALL_VALUES}=          ${WORD}  ${ONE_WORD}  ${TWO_WORD}  ${THREE_WORD}  ${SEVEN_WORD}  ${EIGHT_WORD}  ${ELEVEN_WORD}
                        ...  ${NUMBER}  ${ONE_NUMBER}  ${TWO_NUMBER}  ${THREE_NUMBER}  ${SEVEN_NUMBER}  ${EIGHT_NUMBER}
                        ...  ${ELEVEN_NUMBER}  ${WORD_AND_NUMBER}  ${NUMBER_AND_WORD}  ${POINTS}  ${ONE_POINTS}
                        ...  ${TWO_POINTS}  ${THREE_POINTS}  ${SEVEN_POINTS}  ${EIGHT_POINTS}  ${ELEVEN_POINTS}
                        ...  ${WORD_AND_POINTS}  ${NUMBER_AND_POINTS}  ${POINTS_AND_NUMBER}  ${POINTS_AND_WORD}
@{VALID_PASSWORDS}      .NGAutom!2#     .ZPE5ystems!2020    .ZPE5ystems!2021    .ZPE5ystems!2020.ZPE5ystems!2021

*** Test Cases ***
Test Set Field Options
    Skip If    ${HAS_HOTSPOT} == ${FALSE}    System doesn't have hotspot network
    CLI:Enter Path      /settings/network_connections/hotspot/
    CLI:Write                   set wifi_security=disabled
    CLI:Test Show Command       wifi_security = disabled
    CLI:Write                   set wifi_security=wpa2_enterprise radius_secret=testing321
    CLI:Test Show Command       wifi_security = wpa2_enterprise  radius_secret = ********
    CLI:Write                   set wifi_security=wpa2_personal psk=testing321
    CLI:Test Show Command       wifi_security = wpa2_personal   psk = ********
    [Teardown]          CLI:Revert

Test Show Command
    Skip If    ${HAS_HOTSPOT} == ${FALSE}    System doesn't have hotspot network
    CLI:Enter Path              /settings/network_connections/hotspot/
    CLI:Write                   set wifi_security=wpa2_enterprise radius_secret=testing321
    CLI:Test Show Command       wpa2_method = peap  radius_server = 127.0.0.1  radius_port = 1812
                                ...  radius_secret = ********
    [Teardown]                  CLI:Revert  Raw

Test Valid Values For Field=wpa2_method
    Skip If      ${HAS_HOTSPOT} == ${FALSE}    System doesn't have hotspot network
    CLI:Enter Path         /settings/network_connections/hotspot
    CLI:Write              set wifi_security=wpa2_enterprise radius_secret=testing321 wpa2_method=peap
    CLI:Test Show Command  wpa2_method = peap
    [Teardown]          CLI:Revert  Raw

Test Invalid Values For Field=wpa2_method
    Skip If    ${HAS_HOTSPOT} == ${FALSE}    System doesn't have hotspot network
    CLI:Enter Path                      /settings/network_connections/hotspot
    CLI:Write                           set wifi_security=wpa2_enterprise radius_secret=testing321
    CLI:Test Set Field Invalid Options  wpa2_method  ${EMPTY}
                                        ...  Error: Missing value for parameter: wpa2_method
    FOR		    ${INVALID_VALUE}  IN  @{ALL_VALUES}
		CLI:Test Set Field Invalid Options  wpa2_method  ${INVALID_VALUE}
		...     Error: Invalid value: ${INVALID_VALUE} for parameter: wpa2_method
    END
    [Teardown]                          CLI:Revert  Raw

Test Valid Values For Field=radius_server
    Skip If    ${HAS_HOTSPOT} == ${FALSE}    System doesn't have hotspot network
    CLI:Enter Path  /settings/network_connections/hotspot
    FOR		    ${VALID_IP}  IN  @{VALID_IPS}
		CLI:Write                 set wifi_security=wpa2_enterprise radius_server=${VALID_IP} radius_secret=testing321
		CLI:Test Show Command     radius_server = ${VALID_IP}
    END
    [Teardown]      CLI:Revert  Raw

Test Invalid Values For Field=radius_server
    Skip If    ${HAS_HOTSPOT} == ${FALSE}    System doesn't have hotspot network
    CLI:Enter Path                      /settings/network_connections/hotspot
    FOR		  ${INVALID_IP}  IN  @{INVALID_IPS}
		Write                 set wifi_security=wpa2_enterprise radius_server=${INVALID_IP} radius_secret=testing321
		${OUTPUT}=            CLI:Read Until Prompt   Raw
		Should Contain        ${OUTPUT}  Error: radius_server: Invalid IP address.
    END
    [Teardown]                          CLI:Revert  Raw

Test Valid Values For Field=radius_port
    Skip If    ${HAS_HOTSPOT} == ${FALSE}    System doesn't have hotspot network
    ${VALID_NUMBERS}=    Create List     ${ONE_NUMBER}  ${TWO_NUMBER}  ${THREE_NUMBER}
    CLI:Enter Path                      /settings/network_connections/hotspot
    FOR		    ${VALID_NUMBER}   IN  @{VALID_NUMBERS}
		CLI:Write                 set wifi_security=wpa2_enterprise radius_port=${VALID_NUMBER} radius_secret=testing321
		CLI:Test Show Command     radius_port = ${VALID_NUMBER}
    END
    [Teardown]                     CLI:Revert  Raw

Test Invalid Values For Field=radius_port
    Skip If    ${HAS_HOTSPOT} == ${FALSE}    System doesn't have hotspot network
    ${INVALID_NUMBERS}=   Create List     ${POINTS}   ${ONE_POINTS}   ${TWO_POINTS}
    ...     ${THREE_POINTS}     ${SEVEN_POINTS}   ${WORD_AND_POINTS}    ${NUMBER_AND_POINTS}    ${POINTS_AND_NUMBER}
    ...     ${POINTS_AND_WORD}   ${EIGHT_POINTS}   ${EMPTY}
    CLI:Enter Path                      /settings/network_connections/hotspot
    FOR		    ${INVALID_NUMBER}     IN   @{INVALID_NUMBERS}
		Write                 set wifi_security=wpa2_enterprise port_number=${INVALID_NUMBER} radius_secret=testing321
		${OUTPUT}=            CLI:Read Until Prompt   Raw
		Should Contain        ${OUTPUT}  Error: Invalid parameter name: port_number
    END
    [Teardown]                          CLI:Revert  Raw

Test Valid Values For Field=radius_secret
    Skip If    ${HAS_HOTSPOT} == ${FALSE}    System doesn't have hotspot network
    CLI:Enter Path                      /settings/network_connections/hotspot
    FOR		    ${VALID_PASSWORD}   IN  @{VALID_PASSWORDS}
		CLI:Write                 set wifi_security=wpa2_enterprise radius_secret=${VALID_PASSWORD}
		CLI:Test Show Command     radius_secret = ********
    END
    [Teardown]                          CLI:Revert  Raw

Test Invalid Values For Field=radius_secret
    Skip If    ${HAS_HOTSPOT} == ${FALSE}    System doesn't have hotspot network
    CLI:Enter Path                      /settings/network_connections/hotspot
    Write                 set wifi_security=wpa2_enterprise radius_secret=${EMPTY}
    ${OUTPUT}=            CLI:Read Until Prompt   Raw
    Should Contain        ${OUTPUT}  Error: radius_secret: Validation error.
    [Teardown]                          CLI:Revert  Raw

*** Keywords ***
SUITE:Setup
    CLI:Open
    SUITE:Check If The System Has Hotspot

SUITE:Teardown
    CLI:Close Connection

SUITE:Check If The System Has Hotspot
    ${OUTPUT}=              CLI:Enter Path  /settings/network_connections/hotspot    RAW_Mode=Yes
    ${HAS_HOTSPOT}=         Run Keyword And Return Status   Should Not Contain  ${OUTPUT}
    ...  Error: Invalid path: hotspot
    Set Suite Variable      ${HAS_HOTSPOT}