*** Settings ***
Resource	../../init.robot
Documentation	Tests multiple loopback interfaces configuration
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
Default Tags	CLI	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${LOOPBACK1}	test-loopback1
${LOOPBACK1_IPV4_IP}	10.20.30.1
${LOOPBACK1_IPV6_IP}	fe80::1
${LOOPBACK2}	test-loopback2
${LOOPBACK2_IPV4_IP}	10.20.30.2
${LOOPBACK2_IPV6_IP}	fe80::2
${LOOPBACK3}	test-loopback3
${LOOPBACK3_IPV4_IP}	10.20.30.3
${LOOPBACK3_IPV6_IP}	fe80::3

*** Test Cases ***
Test add Loopback interface Ipv4
	CLI:Enter Path	/settings/network_connections
	CLI:Add
	CLI:Set	name=${LOOPBACK1} type=loopback ipv6_mode=no_ipv6_address
	CLI:Set	ipv4_mode=static ipv4_address=${LOOPBACK1_IPV4_IP} ipv4_bitmask=32
	CLI:Commit
	CLI:Test Show Command Regexp
	...	${LOOPBACK1}\\s{2,}connected\\s{2,}loopback\\s{2,}loopback\\d+\\s{2,}up\\s{2,}${LOOPBACK1_IPV4_IP}/32
	[Teardown]	CLI:Cancel	Raw

Test add Loopback interface Ipv6
	CLI:Enter Path	/settings/network_connections
	CLI:Add
	CLI:Set	name=${LOOPBACK2} type=loopback ipv4_mode=no_ipv4_address
	CLI:Set	ipv6_mode=static ipv6_address=${LOOPBACK2_IPV6_IP} ipv6_prefix_length=128
	CLI:Commit
	CLI:Test Show Command Regexp
	...	${LOOPBACK2}\\s{2,}connected\\s{2,}loopback\\s{2,}loopback\\d+\\s{2,}up\\s{2,}${LOOPBACK2_IPV6_IP}/128
	[Teardown]	CLI:Cancel	Raw

Test add Loopback interface Ipv4 and Ipv6
	CLI:Enter Path	/settings/network_connections
	CLI:Add
	CLI:Set	name=${LOOPBACK3} type=loopback ipv6_mode=static ipv6_address=${LOOPBACK3_IPV6_IP} ipv6_prefix_length=128
	CLI:Set	ipv4_mode=static ipv4_address=${LOOPBACK3_IPV4_IP} ipv4_bitmask=32
	CLI:Commit
	CLI:Test Show Command Regexp
	...	${LOOPBACK3}\\s{2,}connected\\s{2,}loopback\\s{2,}loopback\\d+\\s{2,}up\\s{2,}${LOOPBACK3_IPV4_IP}/32\\s{2,}${LOOPBACK3_IPV6_IP}/128
	[Teardown]	CLI:Cancel	Raw

Test edit Loopback interface from Ipv4 to Ipv6
	CLI:Enter Path	/settings/network_connections/${LOOPBACK1}
	CLI:Set	ipv6_mode=static ipv6_address=${LOOPBACK1_IPV6_IP} ipv6_prefix_length=128
	CLI:Set	ipv4_mode=no_ipv4_address
	CLI:Commit
	CLI:Enter Path	/settings/network_connections/
	CLI:Test Show Command Regexp
	...	${LOOPBACK1}\\s{2,}connected\\s{2,}loopback\\s{2,}loopback\\d+\\s{2,}up\\s{2,}${LOOPBACK1_IPV6_IP}/128
	[Teardown]	CLI:Cancel	Raw

Test edit Loopback interface from Ipv6 to Ipv4
	CLI:Enter Path	/settings/network_connections/${LOOPBACK2}
	CLI:Set	ipv4_mode=static ipv4_address=${LOOPBACK2_IPV4_IP} ipv4_bitmask=32
	CLI:Set	ipv6_mode=no_ipv6_address
	CLI:Commit
	CLI:Enter Path	/settings/network_connections/
	CLI:Test Show Command Regexp
	...	${LOOPBACK2}\\s{2,}connected\\s{2,}loopback\\s{2,}loopback\\d+\\s{2,}up\\s{2,}${LOOPBACK2_IPV4_IP}/32
	[Teardown]	CLI:Cancel	Raw

Test edit Loopback interface from Ipv4 and Ipv6 to only Ipv4
	CLI:Enter Path	/settings/network_connections/${LOOPBACK3}
	CLI:Set	ipv6_mode=no_ipv6_address
	CLI:Commit
	CLI:Enter Path	/settings/network_connections/
	CLI:Test Show Command Regexp
	...	${LOOPBACK3}\\s{2,}connected\\s{2,}loopback\\s{2,}loopback\\d+\\s{2,}up\\s{2,}${LOOPBACK3_IPV4_IP}/32
	[Teardown]	CLI:Cancel	Raw

Test edit Loopback interface from Ipv4 to Ipv4 and Ipv6 again
	CLI:Enter Path	/settings/network_connections/${LOOPBACK3}
	CLI:Set	ipv6_mode=static ipv6_address=${LOOPBACK3_IPV6_IP} ipv6_prefix_length=128
	CLI:Commit
	CLI:Enter Path	/settings/network_connections/
	CLI:Test Show Command Regexp
	...	${LOOPBACK3}\\s{2,}connected\\s{2,}loopback\\s{2,}loopback\\d+\\s{2,}up\\s{2,}${LOOPBACK3_IPV4_IP}/32\\s{2,}${LOOPBACK3_IPV6_IP}/128
	[Teardown]	CLI:Cancel	Raw

Test edit Loopback interface from Ipv4 and Ipv6 to only Ipv6
	CLI:Enter Path	/settings/network_connections/${LOOPBACK3}
	CLI:Set	ipv4_mode=no_ipv4_address
	CLI:Commit
	CLI:Enter Path	/settings/network_connections/
	CLI:Test Show Command Regexp
	...	${LOOPBACK3}\\s{2,}connected\\s{2,}loopback\\s{2,}loopback\\d+\\s{2,}up\\s{2,}${LOOPBACK3_IPV6_IP}/128
	[Teardown]	CLI:Cancel	Raw

Test down connection Loopback interfaces
	CLI:Enter Path	/settings/network_connections
	CLI:Write	down_connection ${LOOPBACK1}
	CLI:Write	down_connection ${LOOPBACK2}
	CLI:Write	down_connection ${LOOPBACK3}
	CLI:Test Show Command Regexp
	...	${LOOPBACK1}\\s{2,}not active
	CLI:Test Show Command Regexp
	...	${LOOPBACK2}\\s{2,}not active
	CLI:Test Show Command Regexp
	...	${LOOPBACK3}\\s{2,}not active
	[Teardown]	CLI:Cancel	Raw

Test up connection Loopback interfaces
	CLI:Enter Path	/settings/network_connections
	CLI:Write	up_connection ${LOOPBACK1}
	CLI:Write	up_connection ${LOOPBACK2}
	CLI:Write	up_connection ${LOOPBACK3}
	CLI:Test Show Command Regexp
	...	${LOOPBACK1}\\s{2,}connected
	CLI:Test Show Command Regexp
	...	${LOOPBACK2}\\s{2,}connected
	CLI:Test Show Command Regexp
	...	${LOOPBACK3}\\s{2,}connected
	[Teardown]	CLI:Cancel	Raw

Test delete Loopback interfaces
	CLI:Enter Path	/settings/network_connections
	CLI:Delete	${LOOPBACK1}	${LOOPBACK2}	${LOOPBACK3}
	CLI:Test Not Show Command	${LOOPBACK1}	${LOOPBACK2}	${LOOPBACK3}
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Delete Loopback Interfaces

SUITE:Teardown
	SUITE:Delete Loopback Interfaces
	CLI:Close Connection

SUITE:Delete Loopback Interfaces
	CLI:Enter Path	/settings/network_connections
	CLI:Delete If Exists	${LOOPBACK1}	${LOOPBACK2}	${LOOPBACK3}
	CLI:Test Not Show Command	${LOOPBACK1}	${LOOPBACK2}	${LOOPBACK3}