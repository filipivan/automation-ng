*** Settings ***
Documentation	Testing ----> Secondary DNS Server
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Resource	../../init.robot
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup		SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${GATEWAY_IP}	${GATEWAY}
${DNS_SERVER_IPV4_IP1}	8.8.8.8
${DNS_SERVER_IPV4_IP2}	75.75.75.75
${DNS_SERVER_HOSTNAME1}	dns.google
${NET_MASK}	24
${DNS_SERVER_IPV6_IP}	123::123
${DNS_SERVER_HOSTNAME2}	cdns.comcast.net
${DNS_IPV6_ADDRESS}	00::0f
${DNS_IPV6_PREFIX_LENGTH}	64
${INTERFACE}	ETH0

*** Test Cases ***
Test Case To Add And Validate DNS
	${INTERFACE_PARAMETERS}=	 CLI:Show
	Should Contain	${INTERFACE_PARAMETERS}	 type:	ipv4_dns_server	ipv4_dns_search	 ipv6_dns_server	 ipv6_dns_search
	SUITE:Add dns server configuration
	CLI:Close Connection
	CLI:Open
	Wait Until Keyword Succeeds	6x	10s	SUITE:Check and verify that DNS server and DNS search seen on network:settings

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/network_connections/${INTERFACE}

SUITE:Teardown
	SUITE:take off the dns server configuration
	CLI:Close Connection

SUITE:Add dns server configuration
	CLI:Enter Path	/settings/network_connections/${INTERFACE}
	CLI:Set	ipv4_dns_server=${DNS_SERVER_IPV4_IP1},${DNS_SERVER_IPV4_IP2}
	CLI:Set	ipv4_dns_search=${DNS_SERVER_HOSTNAME1}
	Run keyword If	'${NGVERSION}' >= '4.2'	CLI:Set	ipv6_mode=static
	Run keyword If	'${NGVERSION}' >= '4.2'	CLI:Set	ipv6_address=${DNS_IPV6_ADDRESS}
	Run keyword If	'${NGVERSION}' >= '4.2'	CLI:Set	ipv6_prefix_length=${DNS_IPV6_PREFIX_LENGTH}
	Run keyword If	'${NGVERSION}' >= '4.2'	CLI:Set	ipv6_dns_server=${DNS_SERVER_IPV6_IP}
	Run keyword If	'${NGVERSION}' >= '4.2'	CLI:Set	ipv6_dns_search=${DNS_SERVER_HOSTNAME2}
	CLI:Commit

SUITE:take off the dns server configuration
	CLI:Enter Path	/settings/network_connections/${INTERFACE}
	CLI:Set	 ipv4_mode=dhcp
	CLI:Set	 ipv4_dns_server=${EMPTY}
	CLI:Set	 ipv4_dns_search=${EMPTY}
	Run keyword If	'${NGVERSION}' >= '4.2'	CLI:Set	 ipv6_dns_server=${EMPTY}
	Run keyword If	'${NGVERSION}' >= '4.2'	CLI:Set	 ipv6_dns_search=${EMPTY}
	CLI:Commit

SUITE:Check and verify that DNS server and DNS search seen on network:settings
	CLI:Enter Path	/settings/network_settings/
	${OUTPUT}=	CLI:Show
	Run keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	${DNS_SERVER_IPV6_IP}
	Should Contain	${OUTPUT}	${DNS_SERVER_IPV4_IP1}
	Should Contain	${OUTPUT}	${DNS_SERVER_IPV4_IP2}
	Should Contain	${OUTPUT}	${DNS_SERVER_HOSTNAME1}
	Run keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	${DNS_SERVER_HOSTNAME2}
