*** Settings ***
Documentation	Testing LTE Uptime Limit Script
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot
Force Tags      CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
Default Tags   	CLI	SSH	SHOW

Suite Setup  	SUITE:Setup
Suite Teardown  SUITE:Teardown

*** Variables ***
${FTP}	${FTPSERVER2_URL}
${FOLDER}	/files/
${SCRIPT_NAME}	GSMUptimeLimit_Event143_sample.sh
${SCRIPT_NAME_CLI}	gsmuptimelimit_event143_sample.sh
${STRONG_PASSWORD}	${QA_PASSWORD}

*** Test Cases ***
Test if GSM goes down after 30s
	[Tags]	NON-CRITICAL
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available

	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	type=mobile_broadband_gsm mobile_interface=${INTERFACE} connect_automatically=no connect_automatically=no
	Run keyword if	'${NGVERSION}' >= '5.6'	CLI:Set	sim-1_apn_configuration=automatic
	...	ELSE	CLI:Set	sim-1_access_point_name=vzwinternet
	CLI:Commit
	CLI:Write	up_connection gsm-${INTERFACE}
	Wait Until Keyword Succeeds	3x	15s	SUITE:Check GSM
	Sleep	30s
	Wait Until Keyword Succeeds	3x	15s	SUITE:Check GSM	down	not active

Test if GSM goes down after 30s second time
	[Tags]	NON-CRITICAL	BUG_NG_10089
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available

	CLI:Enter Path	/settings/network_connections/
	CLI:Write	up_connection gsm-${INTERFACE}
	Wait Until Keyword Succeeds	3x	15s	SUITE:Check GSM
	Sleep	30s
	Wait Until Keyword Succeeds	3x	15s	SUITE:Check GSM	down	not active

*** Keywords ***
SUITE:Setup
	CLI:Open
	${HAS_WMODEM_SUPPORT}=	CLI:Has Wireless Modem Support
	Set Suite Variable	${HAS_WMODEM_SUPPORT}
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	${HAS_WMODEM}=	CLI:Has Wireless Modem With SIM Card
	Set Suite Variable	${HAS_WMODEM}	${HAS_WMODEM}
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	CLI:Enter Path	/settings/network_connections
	CLI:Change User Password From Shell	root	${STRONG_PASSWORD}	${ROOT_PASSWORD}
	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${STRONG_PASSWORD}	${STRONG_PASSWORD}
	CLI:Connect As Root	PASSWORD=${STRONG_PASSWORD}
	SUITE:Add Script to event 143
	${INTERFACE}	SUITE:Take a connected&registered Interface
	Set Suite Variable	${INTERFACE}

SUITE:Teardown
	Skip if	not ${HAS_WMODEM_SUPPORT}
	Skip if	not ${HAS_WMODEM}
	Run Keywords	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}	${STRONG_PASSWORD}	AND
	...	CLI:Change User Password From Shell	root	${ROOT_PASSWORD}	${STRONG_PASSWORD}
	CLI:Close Connection
	CLI:Open
	CLI:Enter Path	/settings/network_connections
	Run Keyword If	'${INTERFACE}' == '${EMPTY}'	CLI:Delete If Exists	gsm-${INTERFACE}
	SUITE:Remove Script from Event 143
	CLI:Close Connection

SUITE:Add Script to event 143
	SUITE:Wget Script
	CLI:Enter Path	/settings/auditing/event_list/143/
	CLI:Set	action_script=${SCRIPT_NAME_CLI}
	CLI:Commit

SUITE:Remove Script from Event 143
	CLI:Enter Path	/settings/auditing/event_list/143/
	CLI:Set	action_script=${EMPTY}
	CLI:Commit
	CLI:Connect As Root
	CLI:Enter Path	/etc/scripts/auditing
	CLI:Write	rm ${SCRIPT_NAME}

SUITE:Wget Script
	CLI:Enter Path	/etc/scripts/auditing
	${OUTPUT}	CLI:Write	wget ${FTP}${FOLDER}${SCRIPT_NAME} --password=${FTPSERVER2_PASSWORD} --user=${FTPSERVER2_USER}
	Should Not Contain	${OUTPUT}	No such file
	CLI:Write	chmod +x ${SCRIPT_NAME}
	CLI:Switch Connection	default

SUITE:Get GSM Interfaces
	${OUTPUT}	CLI:Write	nmcli d | grep [[:space:]]gsm[[:space:]] | cut -d " " -f 1	no	yes
	${OUTPUT}	Split to Lines	${OUTPUT}
	[return]	${OUTPUT}

SUITE:Check GSM
	[Arguments]	${STATE}=up	${STATUS}=connected${SPACE}
	CLI:Enter Path	/settings/network_connections/
	${OUTPUT}	CLI:Show
	Should Contain	${OUTPUT}	gsm-${INTERFACE}${SPACE}${SPACE}${STATUS}${SPACE}${SPACE}mobile${SPACE}broadband${SPACE}gsm${SPACE}${SPACE}${INTERFACE}${SPACE}${SPACE}${SPACE}${SPACE}${STATE}

SUITE:Take a connected&registered Interface
	CLI:Switch Connection	default
	CLI:Enter Path	/system/wireless_modem
	${OUTPUT}	CLI:Show
	${MODEMS}	Split to lines	${OUTPUT}	2	-1
	${INTERFACE}	Set Variable	${EMPTY}
	Set Suite Variable	${INTERFACE}
	FOR	${I}	IN	@{MODEMS}
		${CONTAINS}	Evaluate	"${SPACE}Registered${SPACE}" in """${I}""" and "${SPACE}Disconnected${SPACE}" in """${I}"""
		IF	${CONTAINS}
			${INTERFACE}	${TRASH}	Split String	${I}	${SPACE}Disconnected	1
			${TRASH}	${INTERFACE}	Split String From Right	${INTERFACE}	cdc-	1
			${INTERFACE}	Remove String	cdc-${INTERFACE}	${SPACE}
			Exit for Loop If	${TRUE}
		END
	END
	Run Keyword If	'${INTERFACE}' == '${EMPTY}'	Skip	There is no registered interface
	LOG	${INTERFACE}
	[Return]	${INTERFACE}