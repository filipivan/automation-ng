*** Settings ***
Resource	../../init.robot
Documentation	Tests bonding network connections functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	NON-CRITICAL	NEED-REVIEW	EXCLUDEIN5_0
Default Tags	CLI	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${BONDING_RR}	test-bonding-rr
${BONDING_ACTIVE_BACKUP}	test-bonding-active-backup
${BONDING_BROADCAST}	test-bonding-broadcast
${BONDING_LACP}	test-bonding-lacp
${BONDING_LOAD_BALANCING}	test-bonding-load-balancing
${BONDING_TRANSMIT_LOAD_BALANCING}	test-bonding-transmit-load-balancing
${BONDING_XOR_LOAD_BALANCING}	test-bonding-xor-load-balancing

*** Test Cases ***
Test Round-Robin bonding connection with eth0 down
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	Skip If	not ${HAS_ETH1_CONNECTED}	Unit does not have eth1 interface connected to network
	Skip If	not ${HAS_ETH1_IN_HOST_SUBNET}	ETH1 Dhcp client didn't get the ipv4 in the same subnet like the ETH0 interface
	SUITE:Add Bonding Connection	${BONDING_RR}	round-robin
	SUITE:Set Interface State On Shell	eth1	up
	SUITE:Set Interface State On Shell	eth0	down
	SUITE:Test Ping From Peer

Test Round-Robin bonding connection with eth1 down
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	Skip If	not ${HAS_ETH1_CONNECTED}	Unit does not have eth1 interface connected to network
	Skip If	not ${HAS_ETH1_IN_HOST_SUBNET}	ETH1 Dhcp client didn't get the ipv4 in the same subnet like the ETH0 interface
	SUITE:Set Interface State On Shell	eth0	up
	SUITE:Set Interface State On Shell	eth1	down
	SUITE:Test Ping From Peer
	[Teardown]	SUITE:Test Teardown	${BONDING_RR}

Test Active Backup bonding connection with eth0 down
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	Skip If	not ${HAS_ETH1_CONNECTED}	Unit does not have eth1 interface connected to network
	Skip If	not ${HAS_ETH1_IN_HOST_SUBNET}	ETH1 Dhcp client didn't get the ipv4 in the same subnet like the ETH0 interface
	SUITE:Add Bonding Connection	${BONDING_ACTIVE_BACKUP}	active_backup
	SUITE:Set Interface State On Shell	eth1	up
	SUITE:Set Interface State On Shell	eth0	down
	SUITE:Test Ping From Peer

Test Active Backup bonding connection with eth1 down
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	Skip If	not ${HAS_ETH1_CONNECTED}	Unit does not have eth1 interface connected to network
	Skip If	not ${HAS_ETH1_IN_HOST_SUBNET}	ETH1 Dhcp client didn't get the ipv4 in the same subnet like the ETH0 interface
	SUITE:Set Interface State On Shell	eth0	up
	SUITE:Set Interface State On Shell	eth1	down
	SUITE:Test Ping From Peer
	[Teardown]	SUITE:Test Teardown	${BONDING_ACTIVE_BACKUP}

Test Broadcast bonding connection with eth0 down
	[Tags]	EXCLUDEIN3_2	BUG_NG_7147
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	Skip If	not ${HAS_ETH1_CONNECTED}	Unit does not have eth1 interface connected to network
	Skip If	not ${HAS_ETH1_IN_HOST_SUBNET}	ETH1 Dhcp client didn't get the ipv4 in the same subnet like the ETH0 interface
	SUITE:Add Bonding Connection	${BONDING_BROADCAST}	broadcast
	SUITE:Set Interface State On Shell	eth1	up
	SUITE:Set Interface State On Shell	eth0	down
	SUITE:Test Ping From Peer

Test Broadcast bonding connection with eth1 down
	[Tags]	EXCLUDEIN3_2	BUG_NG_7147
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	Skip If	not ${HAS_ETH1_CONNECTED}	Unit does not have eth1 interface connected to network
	Skip If	not ${HAS_ETH1_IN_HOST_SUBNET}	ETH1 Dhcp client didn't get the ipv4 in the same subnet like the ETH0 interface
	SUITE:Set Interface State On Shell	eth0	up
	SUITE:Set Interface State On Shell	eth1	down
	SUITE:Test Ping From Peer
	[Teardown]	SUITE:Test Teardown	${BONDING_BROADCAST}

Test LACP bonding connection with eth0 down
	[Tags]	EXCLUDEIN3_2	BUG_NG_7147
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	Skip If	not ${HAS_ETH1_CONNECTED}	Unit does not have eth1 interface connected to network
	Skip If	not ${HAS_ETH1_IN_HOST_SUBNET}	ETH1 Dhcp client didn't get the ipv4 in the same subnet like the ETH0 interface
	SUITE:Add Bonding Connection	${BONDING_LACP}	802.3ad(lacp)
	SUITE:Set Interface State On Shell	eth1	up
	SUITE:Set Interface State On Shell	eth0	down
	SUITE:Test Ping From Peer

Test LACP bonding connection with eth1 down
	[Tags]	EXCLUDEIN3_2	BUG_NG_7147
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	Skip If	not ${HAS_ETH1_CONNECTED}	Unit does not have eth1 interface connected to network
	Skip If	not ${HAS_ETH1_IN_HOST_SUBNET}	ETH1 Dhcp client didn't get the ipv4 in the same subnet like the ETH0 interface
	SUITE:Set Interface State On Shell	eth0	up
	SUITE:Set Interface State On Shell	eth1	down
	SUITE:Test Ping From Peer
	[Teardown]	SUITE:Test Teardown	${BONDING_LACP}

Test Load Balancing bonding connection with eth0 down
	[Tags]	EXCLUDEIN3_2	BUG_NG_7147
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	Skip If	not ${HAS_ETH1_CONNECTED}	Unit does not have eth1 interface connected to network
	Skip If	not ${HAS_ETH1_IN_HOST_SUBNET}	ETH1 Dhcp client didn't get the ipv4 in the same subnet like the ETH0 interface
	SUITE:Add Bonding Connection	${BONDING_LOAD_BALANCING}	adaptive_load_balancing
	SUITE:Set Interface State On Shell	eth1	up
	SUITE:Set Interface State On Shell	eth0	down
	SUITE:Test Ping From Peer

Test Load Balancing bonding connection with eth1 down
	[Tags]	EXCLUDEIN3_2	BUG_NG_7147
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	Skip If	not ${HAS_ETH1_CONNECTED}	Unit does not have eth1 interface connected to network
	Skip If	not ${HAS_ETH1_IN_HOST_SUBNET}	ETH1 Dhcp client didn't get the ipv4 in the same subnet like the ETH0 interface
	SUITE:Set Interface State On Shell	eth0	up
	SUITE:Set Interface State On Shell	eth1	down
	SUITE:Test Ping From Peer
	[Teardown]	SUITE:Test Teardown	${BONDING_LOAD_BALANCING}

Test Transmit Load Balancing bonding connection with eth0 down
	[Tags]	EXCLUDEIN3_2	BUG_NG_7147
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	Skip If	not ${HAS_ETH1_CONNECTED}	Unit does not have eth1 interface connected to network
	Skip If	not ${HAS_ETH1_IN_HOST_SUBNET}	ETH1 Dhcp client didn't get the ipv4 in the same subnet like the ETH0 interface
	SUITE:Add Bonding Connection	${BONDING_TRANSMIT_LOAD_BALANCING}	adaptive_transmit_load_balancing
	SUITE:Set Interface State On Shell	eth1	up
	SUITE:Set Interface State On Shell	eth0	down
	SUITE:Test Ping From Peer

Test Transmit Load Balancing bonding connection with eth1 down
	[Tags]	EXCLUDEIN3_2	BUG_NG_7147
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	Skip If	not ${HAS_ETH1_CONNECTED}	Unit does not have eth1 interface connected to network
	Skip If	not ${HAS_ETH1_IN_HOST_SUBNET}	ETH1 Dhcp client didn't get the ipv4 in the same subnet like the ETH0 interface
	SUITE:Set Interface State On Shell	eth0	up
	SUITE:Set Interface State On Shell	eth1	down
	SUITE:Test Ping From Peer
	[Teardown]	SUITE:Test Teardown	${BONDING_TRANSMIT_LOAD_BALANCING}

Test XOR Load Balancing bonding connection with eth0 down
	[Tags]	EXCLUDEIN3_2	BUG_NG_7147
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	Skip If	not ${HAS_ETH1_CONNECTED}	Unit does not have eth1 interface connected to network
	Skip If	not ${HAS_ETH1_IN_HOST_SUBNET}	ETH1 Dhcp client didn't get the ipv4 in the same subnet like the ETH0 interface
	SUITE:Add Bonding Connection	${BONDING_XOR_LOAD_BALANCING}	xor_load_balancing
	SUITE:Set Interface State On Shell	eth1	up
	SUITE:Set Interface State On Shell	eth0	down
	SUITE:Test Ping From Peer

Test XOR Load Balancing bonding connection with eth1 down
	[Tags]	EXCLUDEIN3_2	BUG_NG_7147
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	Skip If	not ${HAS_ETH1_CONNECTED}	Unit does not have eth1 interface connected to network
	Skip If	not ${HAS_ETH1_IN_HOST_SUBNET}	ETH1 Dhcp client didn't get the ipv4 in the same subnet like the ETH0 interface
	SUITE:Set Interface State On Shell	eth0	up
	SUITE:Set Interface State On Shell	eth1	down
	SUITE:Test Ping From Peer
	[Teardown]	SUITE:Test Teardown	${BONDING_XOR_LOAD_BALANCING}

Test Block Unsolicited Incoming Packets
	[Documentation]	Test verifies if iperf3 works on the bonding interface and then it enables Block Unsolicited Incoming Packets
	...	on the bonding interface and tests if iperf3 get blocked
	Skip If	'${NGVERSION}' < '5.4'
	SUITE:Add Bonding Connection	bonding_eth0
	SUITE:Iperf3 On Source
	SUITE:Iperf3 On Destination	${HOST}	${HOSTPEER}
	SUITE:Set Block Unsolicited Incoming Packets	yes
	SUITE:Iperf3 On Source
	SUITE:Iperf3 On Destination	${HOST}	${HOSTPEER}	True

Test Unblock Unsolicited Incoming Packets
	[Documentation]	Test verifies if iperf3 works again after unsetting Block Unsolicited Incoming Packets
	Skip If	'${NGVERSION}' < '5.4'
	SUITE:Set Block Unsolicited Incoming Packets	no
	SUITE:Iperf3 On Source
	SUITE:Iperf3 On Destination	${HOST}	${HOSTPEER}
	CLI:Write	shell sudo killall iperf3
	[Teardown]	SUITE:Delete Interface	bonding_eth0

Test Set as Primary Connection
	[Documentation]	Test verifies if bonding interface becomes the primary one after setting it as primary interface
	Skip If	not ${HAS_ETH1}	Device doesn't have ETH1

	SUITE:Add Bonding Connection	bonding_eth1	PRIMARY_INTERFACE=eth1
	Wait Until Keyword Succeeds	2x	3s	SUITE:Check Primary Interface	False
	SUITE:Set Primary Interface	bonding_eth1	yes
	Wait Until Keyword Succeeds	5x	3s	SUITE:Check Primary Interface	True

Test Unset as Primary Connection
	[Documentation]	Test verifies if bonding interface is removed from primary one after unsetting it as primary interface
	Skip If	not ${HAS_ETH1}	Device doesn't have ETH1

	SUITE:Set Primary Interface	bonding_eth1	no
	Wait Until Keyword Succeeds	2x	3s	SUITE:Check Primary Interface	False

*** Keywords ***
SUITE:Setup
	CLI:Open	session_alias=host_session
	${IS_VM}	CLI:Is VM System
	Set Suite Variable	${IS_VM}
	Skip If	${IS_VM}
	${HAS_ETH1}	${ETH1_IP}	SUITE:Has Eth1
	Set Suite Variable	${HAS_ETH1}
	Run Keyword If	${HAS_ETH1}	Set Suite Variable	${ETH1_IP}

	IF	${HAS_ETH1}
		${HAS_ETH1_CONNECTED}=	CLI:Is Interface Carrier State Up	eth1
		Set Suite Variable	${HAS_ETH1_CONNECTED}
		Skip If	not ${HAS_ETH1_CONNECTED}	Unit does not have eth1 interface connected to network
		Run Keyword If	${HAS_ETH1_CONNECTED}	SUITE:Enable DHCP IPv4 Mode In Network Connection	ETH1
		Set Client Configuration	timeout=60s
		CLI:Delete If Exists	${BONDING_RR}	${BONDING_ACTIVE_BACKUP}
		...	${BONDING_BROADCAST}	${BONDING_LACP}	${BONDING_LOAD_BALANCING}
		...	${BONDING_TRANSMIT_LOAD_BALANCING}	${BONDING_XOR_LOAD_BALANCING}
		Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}

		SUITE:Set ETH0 As Primary Connection

		${HAS_ETH1_IN_HOST_SUBNET}=	SUITE:Check If ETH1 Is On The Same /24 ETH0 Subnet
		Set Suite Variable	${HAS_ETH1_IN_HOST_SUBNET}
	END

	CLI:Connect As Root
	CLI:Open	HOST_DIFF=${HOSTPEER}	session_alias=peer_session
	CLI:Switch Connection	host_session

SUITE:Teardown
	Skip If	${IS_VM}
	CLI:Close Connection
	CLI:Open	session_alias=host_session
	CLI:Enter Path	/settings/network_connections

	Set Client Configuration	timeout=60s
	CLI:Delete If Exists	${BONDING_RR}	${BONDING_ACTIVE_BACKUP}
	...	${BONDING_BROADCAST}	${BONDING_LACP}	${BONDING_LOAD_BALANCING}
	...	${BONDING_TRANSMIT_LOAD_BALANCING}	${BONDING_XOR_LOAD_BALANCING}	bonding_eth1	bonding_eth0
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}

	SUITE:Set Interface State On Shell	eth0	up
	SUITE:Set Interface State On Shell	eth1	up
	Write	shell sudo killall iperf3
	Read Until Prompt
	CLI:Close Connection

SUITE:Test Teardown
	[Arguments]	${CONNECTION}
	Skip If	not ${HAS_ETH1}	Model has no eth1 network interface
	Skip If	not ${HAS_ETH1_CONNECTED}	Unit does not have eth1 interface connected to network
	Skip If	not ${HAS_ETH1_IN_HOST_SUBNET}	ETH1 Dhcp client didn't get the ipv4 in the same subnet like the ETH0 interface
	SUITE:Set Interface State On Shell	eth0	up
	SUITE:Set Interface State On Shell	eth1	up
	CLI:Enter Path	/settings/network_connections
	CLI:Delete If Exists	${CONNECTION}

SUITE:Set ETH0 As Primary Connection
	CLI:Enter Path	/settings/network_connections/ETH0
	CLI:Set	set_as_primary_connection=yes
	CLI:Commit

SUITE:Add Bonding Connection
	[Arguments]	${NAME}	${BONDING_MODE}=active_backup	${PRIMARY_INTERFACE}=eth0	${SECONDARY_INTERFACE}=eth1
	CLI:Enter Path	/settings/network_connections
	CLI:Add
	CLI:Set	name=${NAME} type=bonding bonding_mode=${BONDING_MODE}
	CLI:Set	ipv4_mode=static ipv4_address=${HOST} ipv4_gateway=${GATEWAY} ipv4_bitmask=24
	CLI:Set	primary_interface=${PRIMARY_INTERFACE} secondary_interface=${SECONDARY_INTERFACE}

	Run Keyword If	${NGVERSION} == 3.2	CLI:Set	bond_mac_policy=primary_interf
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Set	bond_fail-over-mac_policy=primary_interface
	Run Keyword If	not '${BONDING_MODE}' == 'active_backup' and ${NGVERSION} >= 4.2
	...	CLI:Set	slave(s)="${PRIMARY_INTERFACE} ${SECONDARY_INTERFACE}"

	Set Client Configuration	timeout=180s
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}

	CLI:Test Show Command	${NAME}

SUITE:Set Interface State On Shell
	[Documentation]
	...	Changes interface's state on shell
	...
	...	Interface is the interface name on system (e.g 'eth0', 'eth1')
	...
	...	State is either 'up' or 'down'
	[Arguments]	${INTERFACE}	${STATE}
	CLI:Connect As Root
	Write	ip link set dev ${INTERFACE} ${STATE}
	CLI:Switch Connection	host_session

SUITE:Test Ping From Peer
	CLI:Switch Connection	peer_session
	Run Keyword And Ignore Error	CLI:Test Ping	${HOST}	1
	CLI:Test Ping	${HOST}	3
	CLI:Switch Connection	host_session

SUITE:Get Interface Ipv4 Address
	[Arguments]	${INTERFACE}
	${OUTPUT}=	CLI:Show	/settings/network_connections/
	@{GET_LINES}=	Split To Lines	${OUTPUT}
	FOR	${INDEX}	${LINE}	IN ENUMERATE	@{GET_LINES}
		${STATUS}=	Run Keyword And Return Status	Should Contain	${LINE}	${INTERFACE}
		${NUMBER_LINE}=	Run Keyword If	'${STATUS}' == 'True'	Set Variable	${INDEX}
		Run Keyword If	'${STATUS}' == 'True'	Exit For Loop
	END
	${LINE}=	Get From List	${GET_LINES}	${NUMBER_LINE}
	${IPV4}=	Should Match Regexp	${LINE}	\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}
	[Return]	${IPV4}

SUITE:Check If ETH1 Is On The Same /24 ETH0 Subnet
	CLI:Switch Connection	host_session
	${ETH0_IPV4}=	SUITE:Get Interface Ipv4 Address	ETH0
	${ETH1_IPV4}=	SUITE:Get Interface Ipv4 Address	ETH1
	${STATUS}=	Run Keyword And Return Status	Wait Until Keyword Succeeds	3x	3s
	...	CLI:Test Ping	${GATEWAY}	NUM_PACKETS=5	TIMEOUT=15	SOURCE_INTERFACE=eth1
	[Return]	${STATUS}

SUITE:Enable DHCP IPv4 Mode In Network Connection
	[Arguments]	${CONNECTION}
	CLI:Enter Path	/settings/network_connections/${CONNECTION}
	CLI:Show
	CLI:Set	ipv4_mode=dhcp
	CLI:Commit
	CLI:Enter Path	/settings/network_connections/
	CLI:Test Show Command	${CONNECTION}
	CLI:Write	up_connection ${CONNECTION}

SUITE:Iperf3 On Source
	Set Client Configuration	prompt=~#
	CLI:Write	shell sudo su -
	Write	iperf3 -s -p 5201
	Read Until	Server listening on 5201

SUITE:Iperf3 On Destination
	[Arguments]	${IP_SOURCE}	${IP_DESTINATION}	${FAIL}=False
	CLI:Open	root	root	HOST_DIFF=${IP_DESTINATION}
	Set Client Configuration	timeout=30
	Write	iperf3 -c ${IP_SOURCE} -4 -p 5201 -t 3 -i 1 --bind ${IP_DESTINATION}
	Run Keyword If	${FAIL}	Sleep	45s
	Run Keyword If	${FAIL}	CLI:Write	\x03
	${OUTPUT}	CLI:Read Until Prompt
	Run Keyword If	${FAIL}	Should Not Contain	${OUTPUT}	/sec	Mbits	KBytes	0.00-
	...	ELSE	Should Contain	${OUTPUT}	iperf Done.
	LOG	${OUTPUT}
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Switch Connection	host_session
	CLI:Write	\x03
	CLI:Write	killall iperf3
	Set Client Configuration	prompt=]#
	CLI:Write	exit

SUITE:Set Block Unsolicited Incoming Packets
	[Arguments]	${STATUS}
	CLI:Enter Path	/settings/network_connections/bonding_eth0
	CLI:Set	block_unsolicited_incoming_packets=${STATUS}
	CLI:Commit

SUITE:Has Eth1
	${HAS_ETH1}	CLI:Has ETH1
	Return From Keyword If	not ${HAS_ETH1}	False	0.0.0.0
	${HAS_IP}	${IP}	SUITE:Get Interface Ip	eth1
	Return From Keyword If	not ${HAS_IP}	False	0.0.0.0
	[Return]	${HAS_IP}	${IP}

SUITE:Get Interface Ip
	[Arguments]	${INTERFACE}
	${IP}	CLI:Get Interface IP Address	${INTERFACE}
	${HAS_IP}	Run Keyword And Return Status	Should Match Regexp	${IP}	192.168.[0-9][0-9]?[0-9]?.[0-9][0-9]?[0-9]?
	[Return]	${HAS_IP}	${IP}

SUITE:Check Primary Interface
	[Arguments]	${STATUS}
	${OUTPUT}	CLI:Write	shell sudo ip r
	${LINES}	Split To Lines	${OUTPUT}
	FOR	${LINE}	IN	@{LINES}
		${BOND}	Run Keyword And Return Status	Should Contain	${LINE}	bond0
		${INTERFACE}	Run Keyword If	${BOND}	Set Variable	${LINE}
		Exit For Loop If	${BOND}
	END
	${TRASH}	${INTERFACE}	Split String From Right	${INTERFACE}	metric${SPACE}	1
	${INTERFACE}	Remove String	${INTERFACE}	${SPACE}
	Run keyword If	${STATUS}	Should Be Equal	${INTERFACE}	90
	Run keyword If	not ${STATUS}	Should Not Be Equal	${INTERFACE}	90

SUITE:Delete Interface
	[Arguments]	${INTERFACES}
	CLI:Switch Connection	host_session
	CLI:Enter Path	/settings/network_connections/
	FOR	${INTERFACE}	IN	${INTERFACES}
		Write	delete ${INTERFACE}
		Read Until	]#
	END

SUITE:Set Primary Interface
	[Arguments]	${BOND}	${STATUS}
	CLI:Enter Path	/settings/network_connections/${BOND}
	CLI:Set	set_as_primary_connection=${STATUS}
	CLI:Commit