*** Settings ***
Resource	../../init.robot
Documentation	Functionality test cases about DHCP Relay through CLI.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL
Default Tags	CLI	SSH	NETWORK	DHCP RELAY
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
#VLAN Confugration Variables
${VLAN100_CLIENT}	${SWITCH_NETPORT_HOST_INTERFACE1}
${VLAN100_RELAY}	${SWITCH_NETPORT_SHARED_INTERFACE1}
${VLAN200_SERVER}	${SWITCH_NETPORT_HOST_INTERFACE2}
${VLAN200_RELAY}	${SWITCH_NETPORT_SHARED_INTERFACE2}

#DHCPv4 Related Variables
${NETMASK}	255.255.255.0

${VLAN100_SUBNET}	100.100.100.0
${IP100_RELAY}	100.100.100.1
${VLAN100_IP_START}	100.100.100.100
${VLAN100_IP_END}	100.100.100.150

${VLAN200_SUBNET}	200.200.200.0
${IP200_RELAY}	200.200.200.1
${IP200_SERVER}	200.200.200.2

#DHCPv6 Related Variables
${VLAN100_PREFIX}	2001:db8:caff::
${IPV6100_RELAY}	2001:db8:caff::1
${VLAN100_IPV6_START}	2001:db8:caff::100
${VLAN100_IPV6_END}	2001:db8:caff::200

${VLAN200_PREFIX}	2001:db8:cafe::
${IPV6200_RELAY}	2001:db8:cafe::1
${IPV6200_SERVER}	2001:db8:cafe::2

*** Test Cases ***
Test Client Get IPv4 Address via DHCPv4 Relay
	[Setup]	SUITE:Setup DHCPv4 Relay Environment
	CLI:Switch Connection	server_client
	CLI:Enter Path	/settings/network_connections/
	Set Client Configuration	timeout=60
	CLI:Write	up_connection vlan100
	${CONNECTIONS}	CLI:Show
	Should Contain	${CONNECTIONS}	${VLAN100_IP_START}
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	[Teardown]	SUITE:Delete Configuration

Test Client Get IPv6 Address via DHCPv6 Relay
	[Setup]	SUITE:Setup DHCPv6 Relay Environment
	CLI:Switch Connection	server_client
	CLI:Enter Path	/settings/network_connections/
	Set Client Configuration	timeout=60
	CLI:Write	up_connection vlan100
	${CONNECTIONS}	CLI:Show
	Should Contain	${CONNECTIONS}	${VLAN100_IPV6_END}
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}

*** Keywords ***
SUITE:Setup
	Skip If	not ${HAS_HOSTSHARED}
	CLI:Open	session_alias=server_client
	CLI:Open	session_alias=relay	HOST_DIFF=${HOSTSHARED}

SUITE:Setup DHCPv4 Relay Environment
	Skip If	not ${HAS_HOSTSHARED}
	SUITE:Configure VLAN
	SUITE:Configure Connections In Server
	SUITE:Configure Connections In Relay
	SUITE:Configure Connections In Client
	SUITE:Configure DHCP Servers
	SUITE:Configure Static Route
	SUITE:Configure DHCP Relay

SUITE:Setup DHCPv6 Relay Environment
	Skip If	not ${HAS_HOSTSHARED}
	SUITE:Configure VLAN
	SUITE:Configure Connections In Server	IPV6=${TRUE}
	SUITE:Configure Connections In Relay	IPV6=${TRUE}
	SUITE:Configure Connections In Client	IPV6=${TRUE}
	SUITE:Configure DHCP Servers	IPV6=${TRUE}
	SUITE:Configure Static Route	IPV6=${TRUE}
	SUITE:Configure DHCP Relay	IPV6=${TRUE}

SUITE:Teardown
	Skip If	not ${HAS_HOSTSHARED}
	SUITE:Delete Configuration
	CLI:Close Connection

SUITE:Delete Configuration
	SUITE:Delete DHCP Relay
	SUITE:Delete Static Route
	SUITE:Delete DHCP Servers
	SUITE:Delete Connections In Relay
	SUITE:Delete Connections In Server/Client
	SUITE:Delete VLAN Configuration

SUITE:Configure VLAN
	CLI:Switch Connection	server_client
	CLI:Enter Path	/settings/switch_interfaces/
	CLI:Edit	${VLAN100_CLIENT},${VLAN200_SERVER}
	CLI:Set	status=enabled
	CLI:Commit

	CLI:Enter Path	/settings/switch_vlan/
	CLI:Add
	CLI:Set	untagged_ports=${VLAN100_CLIENT},backplane0 tagged_ports= vlan=100
	CLI:Commit
	CLI:Edit	100
	CLI:Set	untagged_ports= tagged_ports=${VLAN100_CLIENT},backplane0
	CLI:Commit
	CLI:Add
	CLI:Set	untagged_ports=${VLAN200_SERVER},backplane1 tagged_ports= vlan=200
	CLI:Commit
	CLI:Edit	200
	CLI:Set	untagged_ports= tagged_ports=${VLAN200_SERVER},backplane1
	CLI:Commit
#####################################################################
	CLI:Switch Connection	relay
	CLI:Enter Path	/settings/switch_interfaces/
	CLI:Edit	${VLAN100_RELAY},${VLAN200_RELAY}
	CLI:Set	status=enabled
	CLI:Commit

	CLI:Enter Path	/settings/switch_vlan/
	CLI:Add
	CLI:Set	tagged_ports= untagged_ports=${VLAN100_RELAY},backplane0 vlan=100
	CLI:Commit
	CLI:Edit	100
	CLI:Set	untagged_ports= tagged_ports=${VLAN100_RELAY},backplane0
	CLI:Commit
	CLI:Add
	CLI:Set	tagged_ports= untagged_ports=${VLAN200_RELAY},backplane1 vlan=200
	CLI:Commit
	CLI:Edit	200
	CLI:Set	untagged_ports= tagged_ports=${VLAN200_RELAY},backplane1
	CLI:Commit

SUITE:Delete VLAN Configuration
	CLI:Switch Connection	server_client
	CLI:Enter Path	/settings/switch_interfaces/
	CLI:Edit	${VLAN100_CLIENT},${VLAN200_SERVER}
	CLI:Set	status=disabled
	CLI:Commit
	CLI:Delete VLAN	100
	CLI:Delete VLAN	200
#####################################################################
	CLI:Switch Connection	relay
	CLI:Enter Path	/settings/switch_interfaces/
	CLI:Edit	${VLAN100_RELAY},${VLAN200_RELAY}
	CLI:Set	status=disabled
	CLI:Commit
	CLI:Delete VLAN	100
	CLI:Delete VLAN	200

SUITE:Configure Connections In Relay
	[Arguments]	${IPV6}=${FALSE}
	CLI:Switch Connection	relay
	CLI:Enter Path	/settings/network_connections
	CLI:Add
	CLI:Set	name=vlan200 type=vlan ethernet_interface=backplane1 vlan_id=200
	IF	not ${IPV6}
		CLI:Set	ipv4_mode=static ipv4_address=${IP200_RELAY} ipv4_bitmask=24
	ELSE
		CLI:Set	ipv6_mode=static ipv6_address=${IPV6200_RELAY} ipv6_prefix_length=64
	END
	CLI:Commit
###########################################################################
	CLI:Enter Path	/settings/network_connections
	CLI:Add
	CLI:Set	name=vlan100 type=vlan ethernet_interface=backplane0 vlan_id=100
	IF	not ${IPV6}
		CLI:Set	ipv4_mode=static ipv4_address=${IP100_RELAY} ipv4_bitmask=24
	ELSE
		CLI:Set	ipv6_mode=static ipv6_address=${IPV6100_RELAY} ipv6_prefix_length=64
	END
	CLI:Commit

SUITE:Delete Connections In Relay
	CLI:Switch Connection	relay
	CLI:Enter Path	/settings/network_connections/
	CLI:Delete If Exists	vlan100
	CLI:Delete If Exists	vlan200

SUITE:Configure Connections In Server
	[Arguments]	${IPV6}=${FALSE}
	CLI:Switch Connection	server_client
	CLI:Enter Path	/settings/network_connections
	CLI:Add
	CLI:Set	name=vlan200 type=vlan ethernet_interface=backplane1 vlan_id=200
	IF	not ${IPV6}
		CLI:Set	ipv4_mode=static ipv4_address=${IP200_SERVER} ipv4_bitmask=24
	ELSE
		CLI:Set	ipv6_mode=static ipv6_address=${IPV6200_SERVER} ipv6_prefix_length=64
	END
	CLI:Commit

SUITE:Configure Connections In Client
	[Arguments]	${IPV6}=${FALSE}
	CLI:Switch Connection	server_client
	CLI:Enter Path	/settings/network_connections
	CLI:Add
	CLI:Set	name=vlan100 type=vlan ethernet_interface=backplane0 vlan_id=100
	IF	not ${IPV6}
		CLI:Set	ipv4_mode=dhcp
	ELSE
		CLI:Set	ipv6_mode=stateful_dhcpv6
	END
	CLI:Commit
	CLI:Write	down_connection vlan100

SUITE:Delete Connections In Server/Client
	CLI:Switch Connection	server_client
	CLI:Enter Path	/settings/network_connections/
	CLI:Delete If Exists	vlan100
	CLI:Delete If Exists	vlan200

SUITE:Configure DHCP Servers
	[Arguments]	${IPV6}=${FALSE}
	CLI:Switch Connection	server_client
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	IF	not ${IPV6}
		CLI:Set	protocol=dhcp4 subnet=${VLAN100_SUBNET} netmask=${NETMASK} router_ip=${IP100_RELAY}
		CLI:Commit
		CLI:Enter Path	${VLAN100_SUBNET}|${NETMASK}/network_range
		CLI:Add
		CLI:Set	ip_address_start=${VLAN100_IP_START} ip_address_end=${VLAN100_IP_END}
		CLI:Commit

		CLI:Enter Path	/settings/dhcp_server/
		CLI:Add
		CLI:Set	 protocol=dhcp4 subnet=${VLAN200_SUBNET} netmask=${NETMASK}
		CLI:Commit
	ELSE
		CLI:Set	protocol=dhcp6 prefix=${VLAN100_PREFIX} length=64
		CLI:Commit
		CLI:Enter Path	${VLAN100_PREFIX}|64/network_range
		CLI:Add
		CLI:Set	ip_address_start=${VLAN100_IPV6_START} ip_address_end=${VLAN100_IPV6_END}
		CLI:Commit

		CLI:Enter Path	/settings/dhcp_server/
		CLI:Add
		CLI:Set	 protocol=dhcp6 prefix=${VLAN200_PREFIX} length=64
		CLI:Commit
	END

SUITE:Delete DHCP Servers
	CLI:Switch Connection	server_client
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Delete If Exists	${VLAN100_SUBNET}|${NETMASK}
	CLI:Delete If Exists	${VLAN200_SUBNET}|${NETMASK}
	CLI:Delete If Exists	${VLAN100_PREFIX}|64
	CLI:Delete If Exists	${VLAN200_PREFIX}|64

SUITE:Configure Static Route
	[Arguments]	${IPV6}=${FALSE}
	CLI:Switch Connection	server_client
	CLI:Enter Path	/settings/static_routes/
	CLI:Add
	IF	not ${IPV6}
		CLI:Set	connection=vlan200 type=ipv4 destination_ip=${VLAN100_SUBNET}
		CLI:Set	destination_bitmask=24 gateway_ip=${IP200_RELAY}
	ELSE
		CLI:Set	connection=vlan200 type=ipv6 destination_ip=${VLAN100_PREFIX}
		CLI:Set	destination_bitmask=64 gateway_ip=${IPV6200_RELAY}
	END
	CLI:Commit

SUITE:Delete Static Route
	CLI:Switch Connection	server_client
	CLI:Enter Path	/settings/static_routes/
	CLI:Delete If Exists	route1

SUITE:Configure DHCP Relay
	[Arguments]	${IPV6}=${FALSE}
	CLI:Switch Connection	relay
	CLI:Enter Path	/settings/dhcp_relay/
	CLI:Add
	IF	not ${IPV6}
		CLI:Set	protocol=dhcpv4 servers=${IP200_SERVER} interfaces=backplane0.100,backplane1.200
	ELSE
		CLI:Set	protocol=dhcpv6 server_interfaces=backplane1.200 client_interfaces=backplane0.100
	END
	CLI:Commit

SUITE:Delete DHCP Relay
	CLI:Switch Connection	relay
	CLI:Enter Path	/settings/dhcp_relay/
	CLI:Delete If Exists	relay1