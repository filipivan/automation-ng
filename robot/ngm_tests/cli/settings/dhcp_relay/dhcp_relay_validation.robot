*** Settings ***
Resource	../../init.robot
Documentation	Validation test cases about DHCP Relay through CLI.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4
Default Tags	CLI	SSH	NETWORK	DHCP RELAY
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
@{INVALID_OPTIONS}	${WORD}	${NUMBER}	${POINTS}
@{VALID_OPTIONS}	${WORD}	${NUMBER}	${POINTS}	${WORD_AND_NUMBER}	${WORD_AND_POINTS}	${NUMBER_AND_WORD}
*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/dhcp_relay/
	CLI:Test Available Commands	acknowledge_alarm_state	config_start	factory_settings	set
	...	add	create_csr	hostname	shell
	...	apply_settings	delete	import_settings	show
	...	cd	diagnostic_data	ls	show_settings
	...	change_password	event_system_audit	pwd	shutdown
	...	cloud_enrollment	event_system_clear	quit	software_upgrade
	...	commit	exec	reboot	system_certificate
	...	config_confirm	exit	revert	system_config_check
	...	config_revert	export_settings	save_settings	whoami

Test avaliable fields to set
	CLI:Enter Path	/settings/dhcp_relay/
	CLI:Add
	CLI:Test Set Available Fields	enable_option_82	interfaces	protocol	servers
	[Teardown]	CLI:Cancel	Raw

Test valid values to enable_option_82 field
	CLI:Enter Path	/settings/dhcp_relay/
	CLI:Add
	CLI:Test Set Validate Normal Fields	enable_option_82	yes	no
	[Teardown]	CLI:Cancel	Raw

Test invalid values to enable_option_82 field
	CLI:Enter Path	/settings/dhcp_relay/
	CLI:Add
	FOR	${INVALID_OPTION}	IN	@{INVALID_OPTIONS}
		CLI:Test Set Validate Invalid Options	enable_option_82	${INVALID_OPTION}
		...	Error: Invalid value: ${INVALID_OPTION} for parameter: enable_option_82
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values to incoming_option_82_policy field
	CLI:Enter Path	/settings/dhcp_relay/
	CLI:Add
	CLI:Set	enable_option_82=yes
	CLI:Test Set Validate Normal Fields	incoming_option_82_policy
	...	append_option_82	discard_packet	forward_packet	replace_option_82
	[Teardown]	CLI:Cancel	Raw

Test invalid values to incoming_option_82_policy field
	CLI:Enter Path	/settings/dhcp_relay/
	CLI:Add
	CLI:Set	enable_option_82=yes
	FOR	${INVALID_OPTION}	IN	@{INVALID_OPTIONS}
		CLI:Test Set Validate Invalid Options	incoming_option_82_policy	${INVALID_OPTION}
		...	Error: Invalid value: ${INVALID_OPTION} for parameter: incoming_option_82_policy
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values to interfaces field
	CLI:Enter Path	/settings/dhcp_relay/
	CLI:Add
	FOR	${VALID_OPTION}	IN	@{VALID_OPTIONS}
		CLI:Test Set Field Valid Options	interfaces	${VALID_OPTION}	no
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values to interfaces field
	CLI:Enter Path	/settings/dhcp_relay/
	CLI:Add
	CLI:Test Set Field Invalid Options	interfaces	!@#$%¨&&**()-kdsh
	...	Error: interfaces: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

Test valid values to protocol field
	CLI:Enter Path	/settings/dhcp_relay/
	CLI:Add
	CLI:Test Set Validate Normal Fields	protocol	dhcpv4	dhcpv6
	[Teardown]	CLI:Cancel	Raw

Test invalid values to protocol field
	CLI:Enter Path	/settings/dhcp_relay/
	CLI:Add
	FOR	${INVALID_OPTION}	IN	@{INVALID_OPTIONS}
		CLI:Test Set Validate Invalid Options	protocol	${INVALID_OPTION}
		...	Error: Invalid value: ${INVALID_OPTION} for parameter: protocol
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values to servers field
	CLI:Enter Path	/settings/dhcp_relay/
	CLI:Add
	FOR	${VALID_OPTION}	IN	@{VALID_OPTIONS}
		CLI:Test Set Field Valid Options	servers	${VALID_OPTION}	no
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values to servers field
	CLI:Enter Path	/settings/dhcp_relay/
	CLI:Add
	CLI:Test Set Field Invalid Options	servers	!@#$%¨&&**()-kdsh
	...	Error: servers: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

Test valid values to server_interfaces field
	CLI:Enter Path	/settings/dhcp_relay/
	CLI:Add
	CLI:Set	protocol=dhcpv6
	FOR	${VALID_OPTION}	IN	@{VALID_OPTIONS}
		CLI:Test Set Field Valid Options	server_interfaces	${VALID_OPTION}	no
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values to server_interfaces field
	CLI:Enter Path	/settings/dhcp_relay/
	CLI:Add
	CLI:Set	protocol=dhcpv6
	CLI:Test Set Field Invalid Options	server_interfaces	!@#$%¨&&**()-kdsh
	...	Error: server_interfaces: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

Test valid values to client_interfaces field
	CLI:Enter Path	/settings/dhcp_relay/
	CLI:Add
	CLI:Set	protocol=dhcpv6
	FOR	${VALID_OPTION}	IN	@{VALID_OPTIONS}
		CLI:Test Set Field Valid Options	client_interfaces	${VALID_OPTION}	no
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values to client_interfaces field
	CLI:Enter Path	/settings/dhcp_relay/
	CLI:Add
	CLI:Set	protocol=dhcpv6
	CLI:Test Set Field Invalid Options	client_interfaces	!@#$%¨&&**()-kdsh
	...	Error: client_interfaces: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection