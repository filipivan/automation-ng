*** Settings ***
Resource	../../init.robot
Documentation	Configuration test cases about DHCP Relay through CLI.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4
Default Tags	CLI	SSH	NETWORK	DHCP RELAY
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
${DUMMY_INTERFACE}	eth0
${DUMMY_SERVER}	100.100.100.100

*** Test Cases ***
Test cancel command when adding a new relay
	CLI:Enter Path	/settings/dhcp_relay/
	CLI:Add
	CLI:Set	enable_option_82=no protocol=dhcpv4 interfaces=${DUMMY_INTERFACE} servers=${DUMMY_SERVER}
	CLI:Cancel
	SUITE:Show Configured Relays	None

Test add new relay
	CLI:Enter Path	/settings/dhcp_relay/
	CLI:Add
	CLI:Set	enable_option_82=no protocol=dhcpv4 interfaces=${DUMMY_INTERFACE} servers=${DUMMY_SERVER}
	CLI:Commit
	SUITE:Show Configured Relays	relay1
	[Teardown]	CLI:Cancel	Raw

Test delete relay
	CLI:Enter Path	/settings/dhcp_relay/
	CLI:Delete	relay1
	CLI:Commit
	SUITE:Show Configured Relays	None
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection

SUITE:Show Configured Relays
	[Arguments]	${EXPECTED}=None
	CLI:Enter Path	/settings/dhcp_relay/
	${RELAYS}	CLI:Show	user=Yes
	Run Keyword If	'${EXPECTED}' == 'None'	Should Not Contain	${RELAYS}	relay
	Run Keyword If	'${EXPECTED}' != 'None'	Should Contain	${RELAYS}	${EXPECTED}