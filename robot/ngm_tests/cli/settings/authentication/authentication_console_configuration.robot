*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authentication Console... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test if fallback is on
	CLI:Enter Path	/settings/authentication/console
	${STATUS}=	Run Keyword And Return Status	CLI:Test Show Command	admin_and_root_fallback_to_local_on_console = yes
	Run Keyword If	${STATUS} == False	Run Keywords	CLI:Set Field	admin_and_root_fallback_to_local_on_console	yes
	...	AND	CLI:Commit

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection