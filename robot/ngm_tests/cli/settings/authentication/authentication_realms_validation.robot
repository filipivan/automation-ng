*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authentication Servers > Realms validation of commands, fields and values for fields
Metadata	Version 1.0
Metadata	Executed At ${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
Default Tags	CLI SSH SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
@{INVALID_VALUES}	${WORD}	${NUMBER}	${POINTS}
*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/authentication/realms/
	CLI:Test Available Commands	apply_settings	diagnostic_data	factory_settings	reboot	show_settings
...	cd	event_system_audit	hostname	revert	shutdown
...	change_password	event_system_clear	import_settings	save_settings	software_upgrade
...	cloud_enrollment	exec	ls	set	system_certificate
...	commit	exit	pwd	shell	system_config_check
...	create_csr	export_settings	quit	show	whoami
	[Teardown]	CLI:Cancel	Raw

Test visible fields for show command
	CLI:Enter Path	/settings/authentication/realms/
	CLI:Test Show Command	enable_authentication_server_selection_based_on_realms
	...	Realms allow the user to select authentication server when logging in with the notation user@server or server\\user
	[Teardown]	CLI:Cancel	Raw

Test fields available to set
	CLI:Enter Path	/settings/authentication/realms/
	CLI:Test Set Available Fields	enable_authentication_server_selection_based_on_realms
	[Teardown]	CLI:Cancel	Raw

Test valid values for enable_authentication_server_selection_based_on_realms field
	CLI:Enter Path	/settings/authentication/realms/
	CLI:Test Set Validate Normal Fields	enable_authentication_server_selection_based_on_realms	yes	no
	[Teardown]	CLI:Cancel	Raw

Test invalid values for enable_authentication_server_selection_based_on_realms field
	CLI:Enter Path	/settings/authentication/realms/
	FOR	${INVALID_VALUE}	IN	@{INVALID_VALUES}
		CLI:Test Set Validate Invalid Options	enable_authentication_server_selection_based_on_realms	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: enable_authentication_server_selection_based_on_realms
	END
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection