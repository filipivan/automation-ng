*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authentication Console... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${REMOTE_SERVER}	192.168.2.88
${METHOD}	radius
${ACCOUNTING_SERVER}	test1
${SECRET}	secret

*** Test Cases ***
Create Radius Server
	CLI:Enter Path	/settings/authentication/servers
	CLI:Add
	CLI:Set	method=radius status=enabled remote_server=192.168.2.88 radius_accounting_server=192.168.2.88 fallback_if_denied_access=yes radius_secret=secret radius_enable_servicetype=no
	CLI:Commit
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	radius	ignore_case=True
	### BEGIN - Test created because of a bug that is in trello #718, link https://trello.com/c/XOGcEi5I
	${RADIUS}=	Get Lines Containing String	${OUTPUT}	radius	case_insensitive=True
	${STRING}=	Split String	${RADIUS}
	Set Suite Variable	${DIR}	${STRING}[0]
	${OUTPUT}=	CLI:Enter Path	/settings/authentication/servers/${DIR}
	Should Not Contain	${OUTPUT}	Json-CRITICAL
	CLI:Test Show Command	method = radius	status = enabled	fallback_if_denied_access = yes
	...	remote_server = 192.168.2.88	radius_accounting_server = 192.168.2.88
	...	radius_timeout = 2	radius_retries = 2	radius_enable_servicetype = no
	### END - Test created because of a bug that is in trello #718, link https://trello.com/c/XOGcEi5I

Test fields for servicetype
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	CLI:Test Not Show Command	radius_service_type_administrative	radius_service_type_login
	...	radius_service_type_callback_login	radius_service_type_callback_framed	radius_service_type_outbound
	...	radius_service_type_framed
	CLI:Test Show Command	radius_enable_servicetype = no
	CLI:Set Field	radius_enable_servicetype	yes
	CLI:Commit
	CLI:Test Show Command	radius_service_type_administrative	radius_service_type_login
	...	radius_service_type_callback_login	radius_service_type_callback_framed	radius_service_type_outbound
	...	radius_service_type_framed
	CLI:Set	radius_service_type_administrative=user1 radius_service_type_login=user1 radius_service_type_callback_login=user1 radius_service_type_callback_framed=user1 radius_service_type_outbound=user1 radius_service_type_framed=user1
	CLI:Commit
	CLI:Test Show Command	radius_service_type_administrative = user1	radius_service_type_login = user1
	...	radius_service_type_callback_login = user1	radius_service_type_callback_framed = user1	radius_service_type_outbound = user1
	...	radius_service_type_framed = user1
	CLI:Set Field	radius_enable_servicetype	no
	CLI:Commit
	CLI:Test Show Command	radius_enable_servicetype = no

Test status field
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Normal Fields	status	enabled	disabled
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Normal Fields	status	enabled	disabled

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	status	${EMPTY}	Error: Missing value for parameter: status
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	status	${EMPTY}	Error: Missing value: status

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	status	${WORD}	Error: Invalid value: ${WORD} for parameter: status
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	status	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	status	${POINTS}	Error: Invalid value: ${POINTS} for parameter: status
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	status	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Revert

Test fallback_if_denied_access field
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Normal Fields	fallback_if_denied_access	no
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Normal Fields	fallback_if_denied_access	no

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${EMPTY}	Error: Missing value for parameter: fallback_if_denied_access
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${EMPTY}	Error: Missing value: fallback_if_denied_access

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${WORD}	Error: Invalid value: ${WORD} for parameter: fallback_if_denied_access
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${POINTS}	Error: Invalid value: ${POINTS} for parameter: fallback_if_denied_access
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Revert

Test remote_server field
	Skip If	'${NGVERSION}' == '3.2'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	CLI:Test Set Field Invalid Options	remote_server	${EMPTY}	Error: remote_server: Invalid IP address or host name.
	CLI:Test Set Field Invalid Options	remote_server	${WORD}
	CLI:Test Set Field Invalid Options	remote_server	${NUMBER}
	CLI:Test Set Field Invalid Options	remote_server	${POINTS}	Error: remote_server: Invalid IP address or host name.
	CLI:Test Set Field Invalid Options	remote_server	${ONE_WORD}
	CLI:Test Set Field Invalid Options	remote_server	${ONE_NUMBER}
	CLI:Test Set Field Invalid Options	remote_server	${ONE_POINTS}	Error: remote_server: Invalid IP address or host name.
	CLI:Test Set Field Invalid Options	remote_server	${EXCEEDED}}	Error: remote_server: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test radius_accounting_server field
	Skip If	'${NGVERSION}' == '3.2'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	Run Keyword If	'${NGVERSION}' <= '5.0'	CLI:Test Set Field Invalid Options	radius_accounting_server	${EMPTY}	Error: radius_accounting_server: Invalid IP address or host name.
	CLI:Test Set Field Invalid Options	radius_accounting_server	${WORD}
	CLI:Test Set Field Invalid Options	radius_accounting_server	${NUMBER}
	CLI:Test Set Field Invalid Options	radius_accounting_server	${POINTS}	Error: radius_accounting_server: Invalid IP address or host name.
	CLI:Test Set Field Invalid Options	radius_accounting_server	${ONE_WORD}
	CLI:Test Set Field Invalid Options	radius_accounting_server	${ONE_NUMBER}
	CLI:Test Set Field Invalid Options	radius_accounting_server	${ONE_POINTS}	Error: radius_accounting_server: Invalid IP address or host name.
	CLI:Test Set Field Invalid Options	radius_accounting_server	${EXCEEDED}	Error: radius_accounting_server: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test radius_timeout field
	Skip If	'${NGVERSION}' == '3.2'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	CLI:Test Set Field Invalid Options	radius_timeout	${EMPTY}	Error: radius_timeout: This field only accepts integers.
	CLI:Test Set Field Invalid Options	radius_timeout	${WORD}	Error: radius_timeout: Exceeded the maximum size for this field.
	CLI:Test Set Field Invalid Options	radius_timeout	${NUMBER}	Error: radius_timeout: Exceeded the maximum size for this field.
	CLI:Test Set Field Invalid Options	radius_timeout	${POINTS}	Error: radius_timeout: Exceeded the maximum size for this field.
	CLI:Test Set Field Invalid Options	radius_timeout	${ONE_WORD}	Error: radius_timeout: This field only accepts integers.
	CLI:Test Set Field Invalid Options	radius_timeout	${ONE_NUMBER}
	CLI:Test Set Field Invalid Options	radius_timeout	${ONE_POINTS}	Error: radius_timeout: This field only accepts integers.
	CLI:Test Set Field Invalid Options	radius_timeout	${EXCEEDED}	Error: radius_timeout: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test radius_retries field
	Skip If	'${NGVERSION}' == '3.2'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	CLI:Test Set Field Invalid Options	radius_retries	${EMPTY}	Error: radius_retries: This field only accepts integers.
	CLI:Test Set Field Invalid Options	radius_retries	${WORD}	Error: radius_retries: Exceeded the maximum size for this field.
	CLI:Test Set Field Invalid Options	radius_retries	${NUMBER}	Error: radius_retries: Exceeded the maximum size for this field.
	CLI:Test Set Field Invalid Options	radius_retries	${POINTS}	Error: radius_retries: Exceeded the maximum size for this field.
	CLI:Test Set Field Invalid Options	radius_retries	${ONE_WORD}	Error: radius_retries: This field only accepts integers.
	CLI:Test Set Field Invalid Options	radius_retries	${ONE_NUMBER}
	CLI:Test Set Field Invalid Options	radius_retries	${ONE_POINTS}	Error: radius_retries: This field only accepts integers.
	CLI:Test Set Field Invalid Options	radius_retries	${EXCEEDED}	Error: radius_retries: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test radius_enable_servicetype field
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	CLI:Test Set Validate Normal Fields	radius_enable_servicetype	no	yes

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	radius_enable_servicetype	${EMPTY}	Error: Missing value for parameter: radius_enable_servicetype
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	radius_enable_servicetype	${EMPTY}	Error: Missing value: radius_enable_servicetype

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	radius_enable_servicetype	${WORD}	Error: Invalid value: ${WORD} for parameter: radius_enable_servicetype
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	radius_enable_servicetype	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	radius_enable_servicetype	${POINTS}	Error: Invalid value: ${POINTS} for parameter: radius_enable_servicetype
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	radius_enable_servicetype	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Revert

Test radius_service_type_login field
	Skip If	'${NGVERSION}' == '3.2'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	CLI:Set	radius_enable_servicetype=yes
	CLI:Test Set Field Invalid Options	radius_service_type_login	${EMPTY}
	CLI:Test Set Field Invalid Options	radius_service_type_login	${WORD}
	CLI:Test Set Field Invalid Options	radius_service_type_login	${NUMBER}	Error: radius_service_type_login: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
	CLI:Test Set Field Invalid Options	radius_service_type_login	${POINTS}	Error: radius_service_type_login: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
	CLI:Test Set Field Invalid Options	radius_service_type_login	${ONE_WORD}
	CLI:Test Set Field Invalid Options	radius_service_type_login	${ONE_NUMBER}	Error: radius_service_type_login: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
	CLI:Test Set Field Invalid Options	radius_service_type_login	${ONE_POINTS}	Error: radius_service_type_login: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
	CLI:Test Set Field Invalid Options	radius_service_type_login	${EXCEEDED}	Error: radius_service_type_login: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test radius_service_type_framed field
	Skip If	'${NGVERSION}' == '3.2'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	CLI:Set	radius_enable_servicetype=yes
	CLI:Test Set Field Invalid Options	radius_service_type_framed	${EMPTY}
	CLI:Test Set Field Invalid Options	radius_service_type_framed	${WORD}
	CLI:Test Set Field Invalid Options	radius_service_type_framed	${NUMBER}	Error: radius_service_type_framed: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
	CLI:Test Set Field Invalid Options	radius_service_type_framed	${POINTS}	Error: radius_service_type_framed: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
	CLI:Test Set Field Invalid Options	radius_service_type_framed	${ONE_WORD}
	CLI:Test Set Field Invalid Options	radius_service_type_framed	${ONE_NUMBER}	Error: radius_service_type_framed: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
	CLI:Test Set Field Invalid Options	radius_service_type_framed	${ONE_POINTS}	Error: radius_service_type_framed: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
	CLI:Test Set Field Invalid Options	radius_service_type_framed	${EXCEEDED}	Error: radius_service_type_framed: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test radius_service_type_callback_login field
	Skip If	'${NGVERSION}' == '3.2'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	CLI:Set	radius_enable_servicetype=yes
	CLI:Test Set Field Invalid Options	radius_service_type_callback_login	${EMPTY}
	CLI:Test Set Field Invalid Options	radius_service_type_callback_login	${WORD}
	CLI:Test Set Field Invalid Options	radius_service_type_callback_login	${NUMBER}	Error: radius_service_type_callback_login: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
	CLI:Test Set Field Invalid Options	radius_service_type_callback_login	${POINTS}	Error: radius_service_type_callback_login: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
	CLI:Test Set Field Invalid Options	radius_service_type_callback_login	${ONE_WORD}
	CLI:Test Set Field Invalid Options	radius_service_type_callback_login	${ONE_NUMBER}	Error: radius_service_type_callback_login: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
	CLI:Test Set Field Invalid Options	radius_service_type_callback_login	${ONE_POINTS}	Error: radius_service_type_callback_login: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
	CLI:Test Set Field Invalid Options	radius_service_type_callback_login	${EXCEEDED}	Error: radius_service_type_callback_login: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test radius_service_type_callback_framed field
	Skip If	'${NGVERSION}' == '3.2'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	CLI:Set	radius_enable_servicetype=yes
	CLI:Test Set Field Invalid Options	radius_service_type_callback_framed	${EMPTY}
	CLI:Test Set Field Invalid Options	radius_service_type_callback_framed	${WORD}
	CLI:Test Set Field Invalid Options	radius_service_type_callback_framed	${NUMBER}	Error: radius_service_type_callback_framed: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
	CLI:Test Set Field Invalid Options	radius_service_type_callback_framed	${POINTS}	Error: radius_service_type_callback_framed: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
	CLI:Test Set Field Invalid Options	radius_service_type_callback_framed	${ONE_WORD}
	CLI:Test Set Field Invalid Options	radius_service_type_callback_framed	${ONE_NUMBER}	Error: radius_service_type_callback_framed: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
	CLI:Test Set Field Invalid Options	radius_service_type_callback_framed	${ONE_POINTS}	Error: radius_service_type_callback_framed: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
	CLI:Test Set Field Invalid Options	radius_service_type_callback_framed	${EXCEEDED}	Error: radius_service_type_callback_framed: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test radius_service_type_outbound field
	Skip If	'${NGVERSION}' == '3.2'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	CLI:Set	radius_enable_servicetype=yes
	CLI:Test Set Field Invalid Options	radius_service_type_outbound	${EMPTY}
	CLI:Test Set Field Invalid Options	radius_service_type_outbound	${WORD}
	CLI:Test Set Field Invalid Options	radius_service_type_outbound	${NUMBER}	Error: radius_service_type_outbound: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
	CLI:Test Set Field Invalid Options	radius_service_type_outbound	${POINTS}	Error: radius_service_type_outbound: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
	CLI:Test Set Field Invalid Options	radius_service_type_outbound	${ONE_WORD}
	CLI:Test Set Field Invalid Options	radius_service_type_outbound	${ONE_NUMBER}	Error: radius_service_type_outbound: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
	CLI:Test Set Field Invalid Options	radius_service_type_outbound	${ONE_POINTS}	Error: radius_service_type_outbound: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
	CLI:Test Set Field Invalid Options	radius_service_type_outbound	${EXCEEDED}	Error: radius_service_type_outbound: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test radius_service_type_administrative field
	Skip If	'${NGVERSION}' == '3.2'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	CLI:Set	radius_enable_servicetype=yes
	CLI:Test Set Field Invalid Options	radius_service_type_administrative	${EMPTY}
	CLI:Test Set Field Invalid Options	radius_service_type_administrative	${WORD}
	CLI:Test Set Field Invalid Options	radius_service_type_administrative	${NUMBER}	Error: radius_service_type_administrative: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
	CLI:Test Set Field Invalid Options	radius_service_type_administrative	${POINTS}	Error: radius_service_type_administrative: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
	CLI:Test Set Field Invalid Options	radius_service_type_administrative	${ONE_WORD}
	CLI:Test Set Field Invalid Options	radius_service_type_administrative	${ONE_NUMBER}	Error: radius_service_type_administrative: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
	CLI:Test Set Field Invalid Options	radius_service_type_administrative	${ONE_POINTS}	Error: radius_service_type_administrative: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
	CLI:Test Set Field Invalid Options	radius_service_type_administrative	${EXCEEDED}	Error: radius_service_type_administrative: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test create radius
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Set	method=${METHOD} remote_server=${REMOTE_SERVER} radius_accounting_server=${ACCOUNTING_SERVER} radius_secret=${SECRET}
	CLI:Commit
	[Teardown]	CLI:Delete If Exists	1

Test remote_server field with valid values
	Skip If	'${NGVERSION}' == '3.2'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/
	@{LIST}=	Create List	${WORD}	${NUMBER}	${ONE_WORD}	${ONE_NUMBER}	${REMOTE_SERVER}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Set	method=${METHOD} radius_accounting_server=${ACCOUNTING_SERVER} remote_server=${VALUE}
		CLI:Commit
		CLI:Delete If Exists	1
	END
	[Teardown]	CLI:Cancel	Raw

Test radius_accounting_server field valid values
	CLI:Enter Path	/settings/authentication/servers/
	@{LIST}=	Create List	${WORD}	${NUMBER}	${ONE_WORD}	${ONE_NUMBER}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Set	method=${METHOD} remote_server=${REMOTE_SERVER} radius_accounting_server=${VALUE}
		CLI:Commit
		CLI:Delete If Exists	1
	END
	[Teardown]	CLI:Cancel	Raw

Test radius_timeout field with valid values
	CLI:Enter Path	/settings/authentication/servers/
	@{LIST}=	Create List	${ONE_NUMBER}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Set	method=${METHOD} remote_server=${REMOTE_SERVER} radius_accounting_server=${ACCOUNTING_SERVER} radius_timeout=${VALUE}
		CLI:Commit
		CLI:Delete If Exists	1
	END
	[Teardown]	CLI:Cancel	Raw

Test radius_retries field with valid values
	CLI:Enter Path	/settings/authentication/servers/
	@{LIST}=	Create List	${ONE_NUMBER}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Set	method=${METHOD} remote_server=${REMOTE_SERVER} radius_accounting_server=${ACCOUNTING_SERVER} radius_retries=${VALUE}
		CLI:Commit
		CLI:Delete If Exists	1
	END
	[Teardown]	CLI:Cancel	Raw

Test radius_service_type_login field with valid values
	CLI:Enter Path	/settings/authentication/servers/
	@{LIST}=	Create List	${EMPTY}	${WORD}	${ONE_WORD}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Set	method=${METHOD} remote_server=${REMOTE_SERVER} radius_accounting_server=${ACCOUNTING_SERVER} radius_enable_servicetype=yes radius_service_type_login=${VALUE}
		CLI:Commit
		CLI:Delete If Exists	1
	END
	[Teardown]	CLI:Cancel	Raw

Test radius_service_type_framed field with valid values
	CLI:Enter Path	/settings/authentication/servers/
	@{LIST}=	Create List	${EMPTY}	${WORD}	${ONE_WORD}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Set	method=${METHOD} remote_server=${REMOTE_SERVER} radius_accounting_server=${ACCOUNTING_SERVER} radius_enable_servicetype=yes radius_service_type_framed=${VALUE}
		CLI:Commit
		CLI:Delete If Exists	1
	END
	[Teardown]	CLI:Cancel	Raw

Test radius_service_type_callback_login field with valid values
	CLI:Enter Path	/settings/authentication/servers/
	@{LIST}=	Create List	${EMPTY}	${WORD}	${ONE_WORD}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Set	method=${METHOD} remote_server=${REMOTE_SERVER} radius_accounting_server=${ACCOUNTING_SERVER} radius_enable_servicetype=yes radius_service_type_callback_login=${VALUE}
		CLI:Commit
		CLI:Delete If Exists	1
	END
	[Teardown]	CLI:Cancel	Raw

Test radius_service_type_callback_framed field with valid values
	CLI:Enter Path	/settings/authentication/servers/
	@{LIST}=	Create List	${EMPTY}	${WORD}	${ONE_WORD}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Set	method=${METHOD} remote_server=${REMOTE_SERVER} radius_accounting_server=${ACCOUNTING_SERVER} radius_enable_servicetype=yes radius_service_type_callback_framed=${VALUE}
		CLI:Commit
		CLI:Delete If Exists	1
	END
	[Teardown]	CLI:Cancel	Raw

Test radius_service_type_outbound field with valid values
	CLI:Enter Path	/settings/authentication/servers/
	@{LIST}=	Create List	${EMPTY}	${WORD}	${ONE_WORD}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Set	method=${METHOD} remote_server=${REMOTE_SERVER} radius_accounting_server=${ACCOUNTING_SERVER} radius_enable_servicetype=yes radius_service_type_outbound=${VALUE}
		CLI:Commit
		CLI:Delete If Exists	1
	END
	[Teardown]	CLI:Cancel	Raw

Test radius_service_type_administrative field with WORD | ONE_WORD
	CLI:Enter Path	/settings/authentication/servers/
	@{LIST}=	Create List	${EMPTY}	${WORD}	${ONE_WORD}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Set	method=${METHOD} remote_server=${REMOTE_SERVER} radius_accounting_server=${ACCOUNTING_SERVER} radius_enable_servicetype=yes radius_service_type_administrative=${VALUE}
		CLI:Commit
		CLI:Delete If Exists	1
	END
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Delete All Radius Providers

SUITE:Teardown
	SUITE:Delete All Radius Providers
	CLI:Close Connection

SUITE:Delete All Radius Providers
	CLI:Enter Path	/settings/authentication/servers
	${Output}=	CLI:Show
	${RADIUS}=	Get Lines Containing String	${Output}	radius	case_insensitive=True
	@{LINES}=	Split to Lines	${RADIUS}
	FOR		${LINE}	IN	@{Lines}
		${OutputText}=	CLI:Show
		${RADIUSLINES}=	Get Lines Containing String	${OutputText}	radius	case_insensitive=True
		@{Liness}=	Split to Lines	${RADIUSLINES}
		${Lines}=	Replace String Using Regexp	${Liness}[0]	\\s\\s*	--
		@{Details}=	Split String	${Lines}	--
		CLI:Delete	${Details}[1]
	END