*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authentication Console... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${REMOTE_SERVER}	1.1.1.1
${METHOD}	kerberos

*** Test Cases ***
Test status field
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set method=${METHOD}
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Normal Fields	status	enabled	disabled
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Normal Fields	status	enabled	disabled

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	status	${EMPTY}	Error: Missing value for parameter: status
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	status	${EMPTY}	Error: Missing value: status

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	status	${WORD}	Error: Invalid value: ${WORD} for parameter: status
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	status	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	status	${POINTS}	Error: Invalid value: ${POINTS} for parameter: status
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	status	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Cancel

Test fallback_if_denied_access field
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set method=${METHOD}
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Normal Fields	fallback_if_denied_access	yes	no
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Normal Fields	fallback_if_denied_access	yes	no

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${EMPTY}	Error: Missing value for parameter: fallback_if_denied_access
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${EMPTY}	Error: Missing value: fallback_if_denied_access

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${WORD}	Error: Invalid value: ${WORD} for parameter: fallback_if_denied_access
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${POINTS}	Error: Invalid value: ${POINTS} for parameter: fallback_if_denied_access
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Cancel

Test remote_server field with valid values
	CLI:Enter Path	/settings/authentication/servers/
	@{LIST}=	Create List	${WORD}	${NUMBER}	${ONE_WORD}	${ONE_NUMBER}	${REMOTE_SERVER}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Write	set method=${METHOD}
		CLI:Test Set Field Invalid Options	remote_server	${VALUE}	save=yes
		CLI:Delete If Exists	1
	END
	[Teardown]	CLI:Cancel	Raw

Test remote_server field with invalid values
	Skip If	'${NGVERSION}' < '4.0'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set method=${METHOD}
	CLI:Test Set Field Invalid Options	remote_server	${EMPTY}	Error: remote_server: Invalid IP address or host name.	yes
	CLI:Test Set Field Invalid Options	remote_server	${POINTS}	Error: remote_server: Invalid IP address or host name.	yes
	CLI:Test Set Field Invalid Options	remote_server	${ONE_POINTS}	Error: remote_server: Invalid IP address or host name.	yes
	CLI:Test Set Field Invalid Options	remote_server	${EXCEEDED}	Error: remote_server: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection