*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authentication Servers... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Variables ***
${METHOD}	radius

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Test Available Commands	add	cd	change_password	commit	delete	down_rule	event_system_audit	event_system_clear	exit	hostname	ls	pwd	quit	reboot	revert	set	shell	show	shutdown	up_rule	whoami

Test visible fields for show command
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Test Show Command	index	method	remote server	status	fallback

Test available fields to set command
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Test Set Available Fields	fallback_if_denied_access	kerberos_domain_name	kerberos_realm_domain_name
	...	ldap_ad_base	ldap_ad_database_password	ldap_ad_database_username	ldap_ad_group_attribute	ldap_ad_login_attribute
	...	ldap_ad_search_filter	ldap_ad_secure	method	radius_accounting_server	radius_enable_servicetype	radius_retries
	...	radius_secret	radius_timeout	remote_server	status	tacacs+_accounting_server	tacacs+_enable_user-level
	...	tacacs+_retries	tacacs+_secret	tacacs+_service	tacacs+_timeout	tacacs+_version
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Available Fields	global_catalog_server
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Available Fields	search_nested_groups
	CLI:Cancel

Test status field
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set method=${METHOD}
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Normal Fields	status	enabled	disabled
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Normal Fields	status	enabled	disabled

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	status	${EMPTY}	Error: Missing value for parameter: status
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	status	${EMPTY}	Error: Missing value: status

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	status	${WORD}	Error: Invalid value: ${WORD} for parameter: status
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	status	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	status	${POINTS}	Error: Invalid value: ${POINTS} for parameter: status
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	status	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Cancel

Test invalid values for fields
	CLI:Enter Path	/settings/authentication/servers/
	@{COMMANDS}=	Create List	fallback_if_denied_access	ldap_ad_secure	method	radius_enable_servicetype
	...	status	tacacs+_enable_user-level	tacacs+_service	tacacs+_version
	Run Keyword If	'${NGVERSION}' >= '4.2'	Append To List	${COMMANDS}	global_catalog_server
	Run Keyword If	'${NGVERSION}' >= '4.2'	Append To List	${COMMANDS}	search_nested_groups
	CLI:Add
	SUITE:Set Invalid Values Error Messages	${COMMANDS}
	CLI:Cancel

Test available options for fields = fallback_if_denied_access,ldap_ad_secure, method, radius_enable_servicetype, status, tacacs+_enable_user-level, tacacs+_service, global_catalog_server
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Test Set Field Options	fallback_if_denied_access	no	yes
	CLI:Test Set Field Options	ldap_ad_secure	off	on	start_tls
	CLI:Test Set Field Options	method	kerberos	ldap_or_ad	radius	tacacs+
	CLI:Test Set Field Options	radius_enable_servicetype	no	yes
	CLI:Test Set Field Options	status	disabled	enabled
	CLI:Test Set Field Options	tacacs+_enable_user-level	no	yes
	CLI:Test Set Field Options	tacacs+_service	ppp	raccess	shell
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Options	global_catalog_server	no	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Options	search_nested_groups	no	yes
	CLI:Cancel

Test available options for field=tacacs+_version
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	@{OPTIONS}=	Create List	v0	v0_v1	v1	v1_v0
	Write Bare	set tacacs+_version=\t\t
	Write Bare	\t\t
	Log to Console	\nset tacacs+_version=tab-tab
	${OUTPUT}=	CLI:Read Until Prompt
	FOR		${OPTION}	IN	@{OPTIONS}
		Should Contain	${OUTPUT}	${OPTION}
	END
	${OUTPUT}=	CLI:Read Until Prompt
	Log to Console	\n${OPTIONS}[0] newline
	write bare	\n
	CLI:Read Until Prompt	Raw
	CLI:Cancel

Test commands after error for fields fallback_if_denied_access,ldap_ad_secure, method, radius_enable_servicetype, status, tacacs+_enable_user-level, tacacs+_service, tacacs+_version
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Commands After Set Error	fallback_if_denied_access	${POINTS}	cancel	commit	ls	save	set	show
	CLI:Commands After Set Error	ldap_ad_secure	${POINTS}	cancel	commit	ls	save	set	show
	CLI:Commands After Set Error	method	${POINTS}	cancel	commit	ls	save	set	show
	CLI:Commands After Set Error	radius_enable_servicetype	${POINTS}	cancel	commit	ls	save	set	show
	CLI:Commands After Set Error	tacacs+_enable_user-level	${POINTS}	cancel	commit	ls	save	set	show
	CLI:Commands After Set Error	tacacs+_service	${POINTS}	cancel	commit	ls	save	set	show
	CLI:Commands After Set Error	tacacs+_version	${POINTS}	cancel	commit	ls	save	set	show
	CLI:Commands After Set Error	status	${POINTS}	cancel	commit	ls	save	set	show
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Commands After Set Error	global_catalog_server	${POINTS}	cancel	commit	ls	save	set	show
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Commands After Set Error	search_nested_groups	${POINTS}	cancel	commit	ls	save	set	show
	CLI:Cancel

Test adding empty record
	Skip If	'${NGVERSION}' == '3.2'	Error messages not implemented in 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Test Set Field Invalid Options	remote_server	${EMPTY}	Error: remote_server: Invalid IP address or host name.	yes
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	CLI:Revert

Test validation for field=remote_server
	Skip If	'${NGVERSION}' == '3.2'	Error messages not implemented in 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Test Set Field Invalid Options	remote_server	~!@	Error: remote_server: Invalid IP address or host name.	yes
	CLI:Test Set Field Invalid Options	remote_server	${EXCEEDED}	Error: remote_server: Exceeded the maximum size for this field.	yes
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	CLI:Revert

Test validation for field=ldap_port
	Skip If	'${NGVERSION}' == '3.2'	Error messages not implemented in 3.2
	CLI:Enter Path	/settings/authentication/servers/

	CLI:Add
	CLI:Set Field	remote_server	test
	CLI:Test Set Field Invalid Options	ldap_port	~!@	Error: ldap_port: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	ldap_port	${EXCEEDED}	Error: ldap_port: Exceeded the maximum size for this field.	yes
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	CLI:Revert

Test validation for field=ldap_ad_database_password/ldap_ad_login_attribute/ldap_ad_group_attribute
	Skip If	'${NGVERSION}' == '3.2'	Error messages not implemented in 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Set Field	remote_server	test
	CLI:Test Set Field Invalid Options	ldap_ad_database_password	${EXCEEDED}	Error: ldap_ad_database_password: Exceeded the maximum size for this field.	yes
	CLI:Set Field	ldap_ad_database_password	${EMPTY}
	CLI:Test Set Field Invalid Options	ldap_ad_login_attribute	${EXCEEDED}	Error: ldap_ad_login_attribute: Exceeded the maximum size for this field.	yes
	CLI:Set Field	ldap_ad_login_attribute	${EMPTY}
	CLI:Test Set Field Invalid Options	ldap_ad_group_attribute	${EXCEEDED}	Error: ldap_ad_group_attribute: Exceeded the maximum size for this field.	yes
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	CLI:Revert

Test validation for field=radius_accounting_server
	Skip If	'${NGVERSION}' == '3.2'	Error messages not implemented in 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set remote_server=test method=radius
	Run Keyword If	'${NGVERSION}' <= '5.0'	CLI:Test Set Field Invalid Options	radius_accounting_server	${EMPTY}	Error: radius_accounting_server: Invalid IP address or host name.	yes
	CLI:Test Set Field Invalid Options	radius_accounting_server	${POINTS}	Error: radius_accounting_server: Invalid IP address or host name.	yes
	CLI:Test Set Field Invalid Options	radius_accounting_server	${EXCEEDED}	Error: radius_accounting_server: Exceeded the maximum size for this field.	yes
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	CLI:Revert

Test validation for fields=radius_timeout/radius_retries
	Skip If	'${NGVERSION}' == '3.2'	Error messages not implemented in 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set remote_server=test method=radius radius_accounting_server=radiusserver
	CLI:Test Set Field Invalid Options	radius_timeout	${ONE_WORD}	Error: radius_timeout: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	radius_timeout	${EXCEEDED}	Error: radius_timeout: Exceeded the maximum size for this field.	yes
	CLI:Set Field	radius_timeout	2
	CLI:Test Set Field Invalid Options	radius_retries	${ONE_WORD}	Error: radius_retries: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	radius_retries	${EXCEEDED}	Error: radius_retries: Exceeded the maximum size for this field.	yes
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	CLI:Revert

Test validation for field=radius_secret
	Skip If	'${NGVERSION}' == '3.2'	Error messages not implemented in 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set remote_server=test method=radius radius_accounting_server=radiusserver
	CLI:Test Set Field Invalid Options	radius_secret	${EXCEEDED}	Error: radius_secret: Exceeded the maximum size for this field.	yes
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	CLI:Revert

Test validation for field=tacacs+_accounting_server
	Skip If	'${NGVERSION}' == '3.2'	Error messages not implemented in 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set remote_server=test method=tacacs+
	CLI:Test Set Field Invalid Options	tacacs+_accounting_server	${POINTS}	Error: tacacs+_accounting_server: Invalid IP address or host name.	yes
	CLI:Test Set Field Invalid Options	tacacs+_accounting_server	${EXCEEDED}	Error: tacacs+_accounting_server: Exceeded the maximum size for this field.	yes
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	CLI:Revert

Test validation for fields=tacacs+_timeout/tacacs+_retries
	Skip If	'${NGVERSION}' == '3.2'	Error messages not implemented in 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set remote_server=test method=tacacs+ tacacs+_accounting_server=tacacsserver
	CLI:Test Set Field Invalid Options	tacacs+_timeout	${ONE_WORD}	Error: tacacs+_timeout: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	tacacs+_timeout	${EXCEEDED}	Error: tacacs+_timeout: Exceeded the maximum size for this field.	yes
	CLI:Set Field	tacacs+_timeout	2
	CLI:Test Set Field Invalid Options	tacacs+_retries	${ONE_WORD}	Error: tacacs+_retries: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	tacacs+_retries	${EXCEEDED}	Error: tacacs+_retries: Exceeded the maximum size for this field.	yes
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	CLI:Revert

Test validation for fields=tacacs+_secret
	Skip If	'${NGVERSION}' == '3.2'	Error messages not implemented in 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set remote_server=test method=tacacs+ tacacs+_accounting_server=tacacsserver
	CLI:Test Set Field Invalid Options	tacacs+_secret	${EXCEEDED}	Error: tacacs+_secret: Exceeded the maximum size for this field.	yes
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	CLI:Revert

Test validation for field=radius_service_type
	Skip If	'${NGVERSION}' == '3.2'	Error messages not implemented in 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set remote_server=test method=radius radius_accounting_server=radiusserver radius_enable_servicetype=yes
	${TYPES}=	Create List	radius_service_type_administrative	radius_service_type_callback_framed	radius_service_type_callback_login	radius_service_type_framed	radius_service_type_login
	FOR		${TYPE}	IN	@{TYPES}
		CLI:Test Set Field Invalid Options	${TYPE}	${POINTS}	Error: ${TYPE}: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
		CLI:Test Set Field Invalid Options	${TYPE}	${EXCEEDED}	Error: ${TYPE}: Exceeded the maximum size for this field.	yes
		CLI:Set Field	${TYPE}	${EMPTY}
	END
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	CLI:Revert

Test validation for field=tacacs+_user_level
	Skip If	'${NGVERSION}' == '3.2'	Error messages not implemented in 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set remote_server=test method=tacacs+ tacacs+_accounting_server=tacacsserver tacacs+_enable_user-level=yes
	FOR		${i}	IN RANGE	1	16
		CLI:Test Set Field Invalid Options	tacacs+_user_level_${i}	${POINTS}	Error: tacacs+_user_level_${i}: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
		CLI:Test Set Field Invalid Options	tacacs+_user_level_${i}	${EXCEEDED}	Error: tacacs+_user_level_${i}: Exceeded the maximum size for this field.	yes
		CLI:Set Field	tacacs+_user_level_${i}	${EMPTY}
	END
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	CLI:Revert

Test visibility for field=group_base
	Skip If	'${NGVERSION}' == '3.2'	Only in 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Set Field	search_nested_groups	no
	CLI:Test Set Unavailable Fields	group_base
	CLI:Set Field	search_nested_groups	yes
	CLI:Test Set Available Fields	group_base
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	CLI:Revert

Test validation for field=group_base
	Skip If	'${NGVERSION}' == '3.2'	Only in 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set remote_server=test method=ldap_or_ad search_nested_groups=yes
	CLI:Test Set Field Invalid Options	group_base	${POINTS}	Error: group_base: Validation error.	yes
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	CLI:Revert

Test Enter Local Authentication
	CLI:Enter Path	/settings/authentication/servers/
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	local	ignore_case=True
	${LOCAL}=	Get Lines Containing String	${Output}	local	case_insensitive=True
	${STRING}=	Split String	${LOCAL}
	Set Suite Variable	${DIR}	${STRING}[0]
	${OUTPUT}=	CLI:Enter Path	/settings/authentication/servers/${DIR}
	Should Not Contain	${OUTPUT}	Json-CRITICAL

*** Keywords ***
SUITE:Set Invalid Values Error Messages
	[Arguments]	${COMMANDS}
	FOR		${COMMAND}	IN	@{COMMANDS}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${WORD}	Error: Invalid value: ${WORD} for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value: ${COMMAND}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${WORD}	Error: Invalid value: ${WORD}
	END