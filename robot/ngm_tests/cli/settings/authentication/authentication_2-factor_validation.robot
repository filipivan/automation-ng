*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authentication Console 2-factor validation
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI		EXCLUDEIN3_2	EXCLUDEIN4_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${OTP_NAME}	test_OTP
${NAME}	rsa_method
${REST_URL}	https://rsa.zpesystems.com:5555/mfa/v1_1/authn
${CLIENT_KEY}	08wd3zndor551a518i65taze0fgh6o0t9q3mc5dn6xz5mqur9xw54826vi8xa962
${CLIENT_ID}	rsa.zpesystems.com
${READ_TIMEOUT}	120 	#	30 <= read_timeout <= 240
${CONNECT_TIMEOUT}	20	#	10 <= connect_timeout <= 120
${MAX_RETRIES}	3	#	1 <= max_retries <= 5
${REPLICAS}	rsa-am-replica1.zpesystems.com, rsa-am-replica2.zpesystems.com:4444
${TENANT_ID}	rsaready
${POLICY_ID}	low-mfa-policy

*** Test Cases ***
Test method field
	SUITE:Delete all 2-factor Authentication
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Add
	CLI:Test Set Validate Normal Fields	method	rsa
	Run Keyword If	${NGVERSION} >= 5.8	CLI:Test Set Validate Normal Fields	method	otp
	CLI:Test Set Field Invalid Options	method	${EMPTY}	Error: Missing value for parameter: method
	CLI:Test Set Field Invalid Options	method	${WORD}	Error: Invalid value: ${WORD} for parameter: method
	CLI:Test Set Field Invalid Options	method	${POINTS}	Error: Invalid value: ${POINTS} for parameter: method
	[Teardown]	CLI:Cancel	Raw

Test status field
	SUITE:Delete all 2-factor Authentication
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Add
	CLI:Set	method=rsa
	CLI:Test Set Validate Normal Fields	status	enabled	disabled
	CLI:Test Set Field Invalid Options	status	${EMPTY}	Error: Missing value for parameter: status
	CLI:Test Set Field Invalid Options	status	${WORD}	Error: Invalid value: ${WORD} for parameter: status
	CLI:Test Set Field Invalid Options	status	${POINTS}	Error: Invalid value: ${POINTS} for parameter: status
	[Teardown]	CLI:Cancel	Raw

Test enable_replicas field
	SUITE:Delete all 2-factor Authentication
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Add
	CLI:Set	method=rsa
	CLI:Test Set Field Invalid Options	enable_replicas	${EMPTY}	Error: Missing value for parameter: enable_replicas
	CLI:Test Set Field Invalid Options	enable_replicas	${POINTS}	Error: Invalid value: ${POINTS} for parameter: enable_replicas
	CLI:Test Set Field Options	enable_replicas		yes		no
	[Teardown]	CLI:Cancel	Raw

Test replicas field with invalid values
	SUITE:Delete all 2-factor Authentication
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Add
	CLI:Set	method=rsa
	CLI:Set Field	name	${NAME}
	CLI:Set Field	client_key	${CLIENT_KEY}
	CLI:Set Field	rest_url	${REST_URL}
	CLI:Set Field	client_id	${CLIENT_ID}
	CLI:Set Field  	enable_replicas 	yes
	CLI:Test Set Field Invalid Options	replicas	${POINTS}	Error: replicas: This field contains invalid characters. 	yes
	CLI:Test Set Field Invalid Options	replicas	${EXCEEDED}	Error: replicas: This field contains invalid characters. 	yes

	CLI:Test Set Field Invalid Options	replicas	1.1.1.1:~!@	Error: replicas: Port number must be a positive integer. 	yes
	CLI:Test Set Field Invalid Options	replicas	1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16	Error: replicas: Exceeded number of entries. Maximum number of entries is 15. 	yes
	[Teardown]	CLI:Cancel	Raw

Test enable_cloud_authentication_service field
	SUITE:Delete all 2-factor Authentication
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Add
	CLI:Set	method=rsa
	CLI:Test Set Field Invalid Options	enable_cloud_authentication_service	${EMPTY}	Error: Missing value for parameter: enable_cloud_authentication_service
	CLI:Test Set Field Invalid Options	enable_cloud_authentication_service	${POINTS}	Error: Invalid value: ${POINTS} for parameter: enable_cloud_authentication_service
	CLI:Test Set Field Options	enable_cloud_authentication_service	yes	no
	[Teardown]	CLI:Cancel	Raw

Test visibility of fields under enable_cloud_authentication_service
	SUITE:Delete all 2-factor Authentication
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Add
	CLI:Set	method=rsa
	CLI:Test Not Show Command	policy_id	tenant_id
	CLI:Set Field	enable_cloud_authentication_service	yes
	CLI:Test Show Command	policy_id	tenant_id
	[Teardown]	CLI:Cancel	Raw

Test policy_id field with invalid values
	SUITE:Delete all 2-factor Authentication
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Add
	CLI:Set	method=rsa
	CLI:Set	name=${NAME} client_key=${CLIENT_KEY} rest_url=${REST_URL} client_id=${CLIENT_ID} enable_cloud_authentication_service=yes tenant_id=${TENANT_ID}
	CLI:Test Set Field Invalid Options	policy_id	${EXCEEDED}121212121212121212121212121212	Error: policy_id: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test tenant_id field with invalid values
	SUITE:Delete all 2-factor Authentication
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Add
	CLI:Set	method=rsa
	CLI:Set	name=${NAME} client_key=${CLIENT_KEY} rest_url=${REST_URL} client_id=${CLIENT_ID} enable_cloud_authentication_service=yes policy_id=${POLICY_ID}
	CLI:Test Set Field Invalid Options	tenant_id	${EXCEEDED}121212121212121212121212121212	Error: tenant_id: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test client_id field with invalid values
	SUITE:Delete all 2-factor Authentication
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Add
	CLI:Set	method=rsa
	CLI:Set	name=${NAME} client_key=${CLIENT_KEY} rest_url=${REST_URL}
	@{LIST}=	Create List	${EMPTY}	${POINTS}	${ONE_POINTS}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Test Set Field Invalid Options	client_id	${VALUE}	Error: client_id: Validation error.	yes
	END
	CLI:Test Set Field Invalid Options	client_id	${EXCEEDED}	Error: client_id: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test read_timeout field with invalid values
	SUITE:Delete all 2-factor Authentication
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Add
	CLI:Set	method=rsa
	CLI:Set	name=${NAME} client_key=${CLIENT_KEY} client_id=${CLIENT_ID} rest_url=${REST_URL}
	@{LIST}=	Create List	${WORD}	${NUMBER}	${POINTS}	${EXCEEDED}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Test Set Field Invalid Options	read_timeout	${VALUE}	Error: read_timeout: Exceeded the maximum size for this field.	yes
	END
	CLI:Test Set Field Invalid Options	read_timeout	${ONE_NUMBER}	Error: read_timeout: The value is out of range.	yes
	[Teardown]	CLI:Cancel	Raw

Test read_timeout field with invalid values 2
	SUITE:Delete all 2-factor Authentication
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Add
	CLI:Set	method=rsa
	CLI:Set	name=${NAME} client_key=${CLIENT_KEY} client_id=${CLIENT_ID} rest_url=${REST_URL}
	@{LIST}=	Create List	${EMPTY}	${ONE_WORD}	${ONE_POINTS}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Test Set Field Invalid Options	read_timeout	${VALUE}	Error: read_timeout: This field only accepts integers.	yes
	END
	[Teardown]	CLI:Cancel	Raw

Test connect_timeout field with invalid values
	SUITE:Delete all 2-factor Authentication
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Add
	CLI:Set	method=rsa
	CLI:Set	name=${NAME} client_key=${CLIENT_KEY} client_id=${CLIENT_ID} rest_url=${REST_URL}
	@{LIST}=	Create List	${WORD}	${NUMBER}	${POINTS}	${EXCEEDED}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Test Set Field Invalid Options	connect_timeout	${VALUE}	Error: connect_timeout: Exceeded the maximum size for this field.	yes
	END
	CLI:Test Set Field Invalid Options	connect_timeout	${ONE_NUMBER}	Error: connect_timeout: The value is out of range.	yes
	[Teardown]	CLI:Cancel	Raw

Test connect_timeout field with invalid values 2
	SUITE:Delete all 2-factor Authentication
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Add
	CLI:Set	method=rsa
	CLI:Set	name=${NAME} client_key=${CLIENT_KEY} client_id=${CLIENT_ID} rest_url=${REST_URL}
	@{LIST}=	Create List	${EMPTY}	${ONE_WORD}	${ONE_POINTS}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Test Set Field Invalid Options	connect_timeout	${VALUE}	Error: connect_timeout: This field only accepts integers.	yes
	END
	[Teardown]	CLI:Cancel	Raw

Test max_retries field with invalid values
	SUITE:Delete all 2-factor Authentication
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Add
	CLI:Set	method=rsa
	CLI:Set	name=${NAME} client_key=${CLIENT_KEY} client_id=${CLIENT_ID} rest_url=${REST_URL}
	@{LIST}=	Create List	${WORD}	${NUMBER}	${POINTS}	${EXCEEDED}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Test Set Field Invalid Options	max_retries	${VALUE}	Error: max_retries: Exceeded the maximum size for this field.	yes
	END
	[Teardown]	CLI:Cancel	Raw

Test max_retries field with invalid values 2
	SUITE:Delete all 2-factor Authentication
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Add
	CLI:Set	method=rsa
	CLI:Set Field	name=${NAME} client_key=${CLIENT_KEY} client_id=${CLIENT_ID} rest_url=${REST_URL}
	@{LIST}=	Create List	${EMPTY}	${ONE_WORD}	${ONE_POINTS}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Test Set Field Invalid Options	max_retries	${VALUE}	Error: max_retries: This field only accepts integers.	yes
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for otp_type
	[Documentation]
	[Tags]	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	NON-CRITICAL
	CLI:Enter Path	/settings/authentication/2-factor
	CLI:Add
	CLI:Test Set Validate Normal Fields	otp_type	time_based	counter_based
	[Teardown]	CLI:Cancel	Raw

Test invalid values for otp_type
	[Documentation]
	[Tags]	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	NON-CRITICAL
	CLI:Enter Path	/settings/authentication/2-factor
	CLI:Add
	@{VALUES}	Create List	${WORD}	${NUMBER}	${POINTS}
	FOR	${VALUE}	IN	@{VALUES}
		CLI:Test Set Field Invalid Options	otp_type	${VALUE}	Error: Invalid value: ${VALUE} for parameter: otp_type
	END
	[Teardown]	CLI:Cancel	Raw


Test valid values for enforce_otp_setup
	[Documentation]
	[Tags]	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	NON-CRITICAL
	CLI:Enter Path	/settings/authentication/2-factor
	CLI:Add
	CLI:Test Set Validate Normal Fields	enforce_otp_setup	yes	no
	[Teardown]	CLI:Cancel	Raw

Test invalid values for enforce_otp_setup
	[Documentation]
	[Tags]	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	NON-CRITICAL
	CLI:Enter Path	/settings/authentication/2-factor
	CLI:Add
	@{VALUES}	Create List	${WORD}	${NUMBER}	${POINTS}
	FOR	${VALUE}	IN	@{VALUES}
		CLI:Test Set Field Invalid Options	enforce_otp_setup	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: enforce_otp_setup
	END
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Open
	SUITE:Delete all 2-factor Authentication
	CLI:Close Connection

SUITE:Delete all 2-factor Authentication
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Delete If Exists	${NAME}

SUITE:Create 2-factor Authentication
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Delete If Exists	${NAME}
	CLI:Add
	CLI:Set Field	name	${NAME}
	CLI:Set Field	rest_url	${REST_URL}
	CLI:Set Field	client_key	${CLIENT_KEY}
	CLI:Set Field	client_id	${CLIENT_ID}
	CLI:Commit