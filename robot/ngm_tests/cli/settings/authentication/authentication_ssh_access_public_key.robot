*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authentication SSH public key...
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	NON-CRITICAL	NEED-REVIEW
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${REMOTE_SERVER}	192.168.2.88
${METHOD1}	tacacs+
${METHOD2}	ldap_or_ad
${SECRET}	secret
${BASE}	dc=zpe,dc=net
${DATABASE_USERNAME}	cn=admin,dc=zpe,dc=net
${DATABASE_PASSWORD}	administrator
${LDAP_PORT}	default

${DELETE_KNOWNHOST}	rm -rf .ssh/known_hosts
${USER}	jane
${PASSWD}	jane123
${SSH_COPYID}	ssh-copy-id -f ${USER}@${HOST}
${LOGIN_COPYID}	ssh '${USER}@${HOST}'
${USER_TACACS}	tacacs1
${PASSWD_TACACS}	tacacs
${SSH_COPYID_TACACS}	ssh-copy-id -f ${USER_TACACS}@${HOST}
${LOGIN_COPYID_TACACS}	ssh '${USER_TACACS}@${HOST}'

*** Test Cases ***
Test generate public key and login with it into Nodegrid for LDAP
	Skip If	${HAS_NOPEER}	Has no peer
	CLI:Switch Connection	${USER}_session
	CLI:Write	${DELETE_KNOWNHOST}
	Write	${SSH_COPYID}
	${OUTPUT}=	Read Until	Are you sure
	Write	yes	# just when appears message to accpet certificate
	${OUTPUT}=	Read Until	Password:
	${OUTPUT}=	CLI:Write	${USER}
	Should Contain	${OUTPUT}	Number of key(s)
	Set Client Configuration	prompt=$
	CLI:Write	${LOGIN_COPYID}
	CLI:Write	ls -la /
	Write	exit
	Set Client Configuration	prompt=#
	CLI:Read Until Prompt
	[Teardown]	Run Keywords	CLI:Connect As Root
	...	AND	CLI:Write	${DELETE_KNOWNHOST}

Test generate public key and login with it into Nodegrid for TACACS
	Skip If	${HAS_NOPEER}	Has no peer
	CLI:Switch Connection	${USER}_session
	Set Client Configuration	prompt=#
	CLI:Write	${DELETE_KNOWNHOST}
	Write	${SSH_COPYID_TACACS}
	${OUTPUT}=	Read Until	Are you sure
	Write	yes	# just when appears message to accpet certificate
	${OUTPUT}=	Read Until	Password:
	${OUTPUT}=	CLI:Write	${USER_TACACS}
	Should Contain	${OUTPUT}	Number of key(s)
	Set Client Configuration	prompt=$
	CLI:Write	${LOGIN_COPYID_TACACS}
	CLI:Write	ls -la /
	Write	exit
	Set Client Configuration	prompt=#
	CLI:Read Until Prompt

*** Keywords ***
SUITE:Setup
	${HAS_NOPEER}=	Run Keyword And Return Status	Variable Should Not Exist	${HOSTPEER}
	Set Suite Variable	${HAS_NOPEER}	${HAS_NOPEER}
	Skip If	${HAS_NOPEER}	Has no peer
	CLI:Open	root	${ROOT_PASSWORD}	session_alias=${USER}_session	HOST_DIFF=${HOSTPEER}
	CLI:Open
	SUITE:Delete Remote Servers
	CLI:Enter Path	/settings/authentication/servers
	CLI:Add
	CLI:Write	set method=${METHOD2} status=enabled remote_server=${REMOTE_SERVER} fallback_if_denied_access=yes ldap_ad_base=${BASE} ldap_ad_database_username=${DATABASE_USERNAME} ldap_ad_database_password=${DATABASE_PASSWORD} ldap_port=${LDAP_PORT}
	CLI:Commit
	CLI:Add
	CLI:Write	set method=${METHOD1} status=enabled remote_server=${REMOTE_SERVER} fallback_if_denied_access=yes tacacs+_secret=${SECRET} authorize_ssh_pkey_users=yes
	CLI:Commit
	CLI:Add User	${USER}	${PASSWD}
	CLI:Add User	${USER_TACACS}	${PASSWD_TACACS}
	CLI:Enter Path	/settings/authorization/user/profile
	CLI:Write	set startup_application=shell
	CLI:Commit
	CLI:Connect As Root
	CLI:Write	${DELETE_KNOWNHOST}
	CLI:Close Current Connection

SUITE:Teardown
	Skip If	${HAS_NOPEER}	Has no peer
	CLI:Switch Connection	${USER}_session
	Run Keyword If Any Tests Failed	CLI:Write	^Ec
	Run Keyword If Any Tests Failed	CLI:Cancel	Raw
	CLI:Write	ls
	CLI:Write	${DELETE_KNOWNHOST}
	CLI:Switch Connection	default
	SUITE:Delete Remote Servers	ldap or ad
	SUITE:Delete Remote Servers	tacacs
	CLI:Delete Users	${USER}
	CLI:Delete Users	${USER_TACACS}
	CLI:Enter Path	/settings/authorization/user/profile
	CLI:Write	set startup_application=cli
	CLI:Commit
	Run Keyword If Any Tests Failed	CLI:Connect As Root
	Run Keyword If Any Tests Failed	CLI:Write	${DELETE_KNOWNHOST}
	CLI:Close Connection

SUITE:Delete Remote Servers
	[Arguments]	${TYPE}=ldap or ad
	CLI:Enter Path	/settings/authentication/servers
	${OUTPUT}=	CLI:Show
	${LDAPS}=	Get Lines Containing String	${OUTPUT}	${TYPE}	case_insensitive=True
	@{LINES}=	Split to Lines	${LDAPS}
	FOR		${LINE}	IN	@{Lines}
		@{INDEX}=	Split String	${LINE}
		CLI:Delete If Exists	${INDEX}[0]
	END
	CLI:Commit
