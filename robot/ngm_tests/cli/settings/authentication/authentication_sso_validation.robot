*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authentication SSO Validation... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NAME}	idp_test
${ENTITY_ID}	nodegrid_automation
${x.509}	remote_server
${SSO_URL}	${SSOSERVER_URL}${SSOSERVER_SERVICE_PATH}
${ISSUER}	${SSOSERVER_URL}${SSOSERVER_ISSUER_PATH}
${URL}	${FTPSERVER2_URL}${SSOSERVER_CRT_PATH}
${FTP_USERNAME}	${FTPSERVER2_USER}
${FTP_PASSWORD}	${FTPSERVER2_PASSWORD}

*** Test Cases ***
Test ls command
	CLI:Enter Path	/settings/authentication/
	CLI:Test Ls Command	servers	console	sso
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Ls Command	default_group

Test available commands after send tab-tab
	CLI:Enter Path	/settings/authentication/sso/
	CLI:Test Available Commands	add	delete	import_metadata	save_settings	software_upgrade
	...	apply_settings	event_system_audit	ls	set	system_certificate	cd	event_system_clear	pwd
	...	shell	system_config_check	change_password	exit	quit	show	whoami	commit	factory_settings
	...	reboot	show_settings	create_csr	hostname	revert	shutdown

Test visible fields for show command
	CLI:Enter Path	/settings/authentication/sso/
	CLI:Test Show Command Regexp	name\\s+status

Test available fields to set command
	CLI:Enter Path	/settings/authentication/sso/
	CLI:Add
	CLI:Test Set Available Fields	certificate_name	icon	name	status
	...	entity_id	issuer	sso_url	x.509_certificate
	[Teardown]	CLI:Cancel	Raw

Test default values when adding
	CLI:Enter Path	/settings/authentication/sso/
	CLI:Add
	CLI:Test Show Command	name =	status = disabled	entity_id =	sso_url =	issuer =	x.509_certificate = local_system
	...	certificate_name =	Certificate must be previously copied to /var/sw directory.	icon = sso.png
	[Teardown]	CLI:Cancel	Raw

Test possible values for fields status, x.509_certificate and icon
	CLI:Enter Path	/settings/authentication/sso/
	CLI:Add
	CLI:Test Set Field Options	x.509_certificate	local_system	remote_server
	CLI:Test Set Field Options	status	enabled	disabled
	CLI:Test Set Field Options	icon	duo.png	okta.png	pingid.png	sso.png
	[Teardown]	CLI:Cancel	Raw

Test validation for field=name
	CLI:Enter Path	/settings/authentication/sso/
	CLI:Add
	CLI:Test Set Field Invalid Options	name	${EMPTY}	Error: name: Field must not be empty.Error: certificate_name: Incorrect filename information.	yes
	CLI:Test Set Field Invalid Options	name	${POINTS}	Error: name: This field contains invalid characters.Error: certificate_name: Incorrect filename information.	yes
	[Teardown]	CLI:Cancel	Raw

Test validation for field=entity_id
	CLI:Enter Path	/settings/authentication/sso/
	CLI:Add
	CLI:Write	set name=${NAME} x.509_certificate=${X.509} sso_url=${SSO_URL} url=${URL} username=${FTP_USERNAME} password=${FTP_PASSWORD} entity_id=${ENTITY_ID} issuer=${ISSUER}
	CLI:Test Set Field Invalid Options	entity_id	${EMPTY}	Error: entity_id: Field must not be empty.	yes
	CLI:Test Set Field Invalid Options	entity_id	${EXCEEDED}1234567890123456789	Error: entity_id: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test validation for field=sso_url
	CLI:Enter Path	/settings/authentication/sso/
	CLI:Add
	CLI:Write	set name=${NAME} x.509_certificate=${X.509} url=${URL} username=${FTP_USERNAME} password=${FTP_PASSWORD} entity_id=${ENTITY_ID} issuer=${ISSUER}
	CLI:Test Set Field Invalid Options	sso_url	${EMPTY}	Error: sso_url: Field must not be empty.	yes
	CLI:Test Set Field Invalid Options	sso_url	${EXCEEDED}1234567890123456789	Error: sso_url: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test validation for field=issuer
	CLI:Enter Path	/settings/authentication/sso/
	CLI:Add
	CLI:Write	set name=${NAME} x.509_certificate=${X.509} sso_url=${SSO_URL} url=${URL} username=${FTP_USERNAME} password=${FTP_PASSWORD} entity_id=${ENTITY_ID} issuer=${ISSUER}
	CLI:Test Set Field Invalid Options	issuer	${EMPTY}	Error: issuer: Field must not be empty.	yes
	CLI:Test Set Field Invalid Options	issuer	${EXCEEDED}1234567890123456789	Error: issuer: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test validation for field=x.509_certificate (remote_server)
	CLI:Enter Path	/settings/authentication/sso/
	CLI:Add
	CLI:Write	set name=${NAME} entity_id=${ENTITY_ID} issuer=${ISSUER}
	CLI:Set Field	x.509_certificate	remote_server

	CLI:Test Set Field Invalid Options	url	${EMPTY}	Error: Incorrect URL information.	yes
	CLI:Test Set Field Invalid Options	url	${WORD}	Error: Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>	yes
	CLI:Test Set Field Invalid Options	url	${POINTS}	Error: Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>	yes

	CLI:Set Field	url	${URL}
	CLI:Test Set Field Invalid Options	username	${EMPTY}	Error: Protocol requires username/password.  yes
	CLI:Set Field	username	ftpuser
	CLI:Test Set Field Invalid Options	password	${EMPTY}	Error: File Transfer Failed	yes
	[Teardown]	CLI:Cancel	Raw

Test validation for field=x.509_certificate (local_system)
	CLI:Enter Path	/settings/authentication/sso/
	CLI:Add
	CLI:Write	set name=${NAME} entity_id=${ENTITY_ID} issuer=${ISSUER}
	CLI:Set Field	x.509_certificate	local_system
	CLI:Test Set Field Invalid Options	certificate_name	${EMPTY}	Error: certificate_name: Incorrect filename information.	yes
	CLI:Test Set Field Invalid Options	certificate_name	${POINTS}	Error: certificate_name: Incorrect filename information.	yes
	[Teardown]	CLI:Cancel	Raw

Test fields for import_metadata
	CLI:Enter Path	/settings/authentication/sso
	CLI:Write	import_metadata
	CLI:Test Show Command	name =	status = disabled	entity_id =	metadata = local_system	metadata_file =
	...	XML file must be previously copied to /var/sw directory.	icon = sso.png
	[Teardown]	CLI:Cancel	Raw

Test validation for field=name for import_metadata
	CLI:Enter Path	/settings/authentication/sso/
	CLI:Write	import_metadata
	CLI:Test Set Field Invalid Options	name	${EMPTY}	Error: metadata_file: Incorrect filename information.Error: name: Field must not be empty.	yes
	CLI:Test Set Field Invalid Options	name	${POINTS}	Error: metadata_file: Incorrect filename information.Error: name: This field contains invalid characters.	yes
	[Teardown]	CLI:Cancel	Raw

Test possible values for fields status, metadata and icon
	CLI:Enter Path	/settings/authentication/sso/
	CLI:Write	import_metadata
	CLI:Test Set Field Options	metadata	local_system	remote_server
	CLI:Test Set Field Options	status	enabled	disabled
	CLI:Test Set Field Options	icon	duo.png	okta.png	pingid.png	sso.png
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/authentication/sso/
	CLI:Delete If Exists	${NAME}

SUITE:Teardown
	CLI:Enter Path	/settings/authentication/sso/
	CLI:Delete If Exists	${NAME}
	CLI:Close Connection