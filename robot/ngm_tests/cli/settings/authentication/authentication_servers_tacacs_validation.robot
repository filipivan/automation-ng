*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authentication Console... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	CLI:Open	startping=no
Suite Teardown	Close All Connections

*** Variables ***
${REMOTE_SERVER}	1.1.1.1
${METHOD}	tacacs+

*** Test Cases ***
Test status field
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set method=${METHOD}
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Normal Fields	status	enabled	disabled
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Normal Fields	status	enabled	disabled

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	status	${EMPTY}	Error: Missing value for parameter: status
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	status	${EMPTY}	Error: Missing value: status

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	status	${WORD}	Error: Invalid value: ${WORD} for parameter: status
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	status	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	status	${POINTS}	Error: Invalid value: ${POINTS} for parameter: status
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	status	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Cancel

Test fallback_if_denied_access field
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set method=${METHOD}
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Normal Fields	fallback_if_denied_access	yes	no
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Normal Fields	fallback_if_denied_access	yes	no

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${EMPTY}	Error: Missing value for parameter: fallback_if_denied_access
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${EMPTY}	Error: Missing value: fallback_if_denied_access

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${WORD}	Error: Invalid value: ${WORD} for parameter: fallback_if_denied_access
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${POINTS}	Error: Invalid value: ${POINTS} for parameter: fallback_if_denied_access
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Cancel

Test remote_server field
	Skip If	'${NGVERSION}' < '4.0'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set method=${METHOD}
	CLI:Test Set Field Invalid Options	remote_server	${EMPTY}	Error: remote_server: Invalid IP address or host name.	yes
	CLI:Test Set Field Invalid Options	remote_server	${POINTS}	Error: remote_server: Invalid IP address or host name.	yes
	CLI:Test Set Field Invalid Options	remote_server	${ONE_POINTS}	Error: remote_server: Invalid IP address or host name.	yes
	CLI:Test Set Field Invalid Options	remote_server	${EXCEEDED}	Error: remote_server: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test tacacs+_accounting_server field
	Skip If	'${NGVERSION}' < '4.0'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set method=${METHOD}
	CLI:Write	set remote_server=${REMOTE_SERVER}
	CLI:Test Set Field Invalid Options	tacacs+_accounting_server	${POINTS}	Error: tacacs+_accounting_server: Invalid IP address or host name.	yes
	CLI:Test Set Field Invalid Options	tacacs+_accounting_server	${ONE_POINTS}	Error: tacacs+_accounting_server: Invalid IP address or host name.	yes
	CLI:Test Set Field Invalid Options	tacacs+_accounting_server	${EXCEEDED}	Error: tacacs+_accounting_server: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test tacacs_port field
	Skip If	'${NGVERSION}' < '4.0'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set method=${METHOD}
	CLI:Write	set remote_server=${REMOTE_SERVER}
	CLI:Test Set Field Invalid Options	tacacs_port	${EMPTY}	Error: tacacs_port: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	tacacs_port	${WORD}	Error: tacacs_port: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	tacacs_port	${POINTS}	Error: tacacs_port: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	tacacs_port	${ONE_WORD}	Error: tacacs_port: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	tacacs_port	${ONE_POINTS}	Error: tacacs_port: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	tacacs_port	${EXCEEDED}	Error: tacacs_port: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test tacacs+_service field
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set method=${METHOD}
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Normal Fields	tacacs+_service	ppp	raccess	shell
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Normal Fields	tacacs+_service	ppp	raccess	shell

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tacacs+_service	${EMPTY}	Error: Missing value for parameter: tacacs+_service
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tacacs+_service	${EMPTY}	Error: Missing value: tacacs+_service

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tacacs+_service	${WORD}	Error: Invalid value: ${WORD} for parameter: tacacs+_service
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tacacs+_service	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tacacs+_service	${POINTS}	Error: Invalid value: ${POINTS} for parameter: tacacs+_service
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tacacs+_service	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Cancel

Test tacacs+_timeout field
	Skip If	'${NGVERSION}' < '4.0'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set method=${METHOD}
	CLI:Write	set remote_server=${REMOTE_SERVER}
	CLI:Test Set Field Invalid Options	tacacs+_timeout	${EMPTY}	Error: tacacs+_timeout: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	tacacs+_timeout	${WORD}	Error: tacacs+_timeout: Exceeded the maximum size for this field.	yes
	CLI:Test Set Field Invalid Options	tacacs+_timeout	${NUMBER}	Error: tacacs+_timeout: Exceeded the maximum size for this field.	yes
	CLI:Test Set Field Invalid Options	tacacs+_timeout	${POINTS}	Error: tacacs+_timeout: Exceeded the maximum size for this field.	yes
	CLI:Test Set Field Invalid Options	tacacs+_timeout	${ONE_WORD}	Error: tacacs+_timeout: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	tacacs+_timeout	${ONE_POINTS}	Error: tacacs+_timeout: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	tacacs+_timeout	${EXCEEDED}	Error: tacacs+_timeout: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test tacacs+_retries field
	Skip If	'${NGVERSION}' < '4.0'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set method=${METHOD}
	CLI:Write	set remote_server=${REMOTE_SERVER}
	CLI:Test Set Field Invalid Options	tacacs+_retries	${EMPTY}	Error: tacacs+_retries: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	tacacs+_retries	${WORD}	Error: tacacs+_retries: Exceeded the maximum size for this field.	yes
	CLI:Test Set Field Invalid Options	tacacs+_retries	${NUMBER}	Error: tacacs+_retries: Exceeded the maximum size for this field.	yes
	CLI:Test Set Field Invalid Options	tacacs+_retries	${POINTS}	Error: tacacs+_retries: Exceeded the maximum size for this field.	yes
	CLI:Test Set Field Invalid Options	tacacs+_retries	${ONE_WORD}	Error: tacacs+_retries: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	tacacs+_retries	${ONE_POINTS}	Error: tacacs+_retries: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	tacacs+_retries	${EXCEEDED}	Error: tacacs+_retries: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test tacacs+_version field
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set method=${METHOD}
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Normal Fields	tacacs+_version	v0	v0_v1	v1	v1_v0
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Normal Fields	tacacs+_version	v0	v0_v1	v1	v1_v0

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tacacs+_version	${EMPTY}	Error: Missing value for parameter: tacacs+_version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tacacs+_version	${EMPTY}	Error: Missing value: tacacs+_version

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tacacs+_version	${WORD}	Error: Invalid value: ${WORD} for parameter: tacacs+_version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tacacs+_version	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tacacs+_version	${POINTS}	Error: Invalid value: ${POINTS} for parameter: tacacs+_version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tacacs+_version	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Cancel

Test tacacs+_enable_user-level field
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set method=${METHOD}
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Normal Fields	tacacs+_enable_user-level	yes	no
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Normal Fields	tacacs+_enable_user-level	yes	no

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tacacs+_enable_user-level	${EMPTY}	Error: Missing value for parameter: tacacs+_enable_user-level
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tacacs+_enable_user-level	${EMPTY}	Error: Missing value: tacacs+_enable_user-level

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tacacs+_enable_user-level	${WORD}	Error: Invalid value: ${WORD} for parameter: tacacs+_enable_user-level
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tacacs+_enable_user-level	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tacacs+_enable_user-level	${POINTS}	Error: Invalid value: ${POINTS} for parameter: tacacs+_enable_user-level
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tacacs+_enable_user-level	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Cancel

Test tacacs+_user_level fields
	Skip If	'${NGVERSION}' < '4.0'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set method=${METHOD}
	CLI:Write	set remote_server=${REMOTE_SERVER}
	CLI:Write	set tacacs+_enable_user-level=yes
	CLI:Show
	FOR		${INDEX}	IN RANGE	1	15
		CLI:Test Set Field Invalid Options	tacacs+_user_level_${INDEX}	${EMPTY}
		CLI:Test Set Field Invalid Options	tacacs+_user_level_${INDEX}	${WORD}
		CLI:Test Set Field Invalid Options	tacacs+_user_level_${INDEX}	${NUMBER}	Error: tacacs+_user_level_${INDEX}: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
		CLI:Test Set Field Invalid Options	tacacs+_user_level_${INDEX}	${POINTS}	Error: tacacs+_user_level_${INDEX}: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
		CLI:Test Set Field Invalid Options	tacacs+_user_level_${INDEX}	${ONE_WORD}
		CLI:Test Set Field Invalid Options	tacacs+_user_level_${INDEX}	${ONE_NUMBER}	Error: tacacs+_user_level_${INDEX}: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
		CLI:Test Set Field Invalid Options	tacacs+_user_level_${INDEX}	${ONE_POINTS}	Error: tacacs+_user_level_${INDEX}: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
		CLI:Test Set Field Invalid Options	tacacs+_user_level_${INDEX}	${EXCEEDED}	Error: tacacs+_user_level_${INDEX}: Exceeded the maximum size for this field.	yes
		CLI:Set Field	tacacs+_user_level_${INDEX}	${EMPTY}
	END
	[Teardown]	CLI:Cancel	Raw