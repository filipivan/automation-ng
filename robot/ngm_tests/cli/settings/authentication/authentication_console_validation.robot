*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authentication Console... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

*** Test Cases ***
Test ls command
	CLI:Enter Path	/settings/authentication/
	CLI:Test Ls Command	servers	console
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Ls Command	default_group

Test show_settings command
	CLI:Enter Path	/settings/authentication/
	Write	show_settings
	${OUTPUT}=	CLI:Read Until Prompt
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Match Regexp	${OUTPUT}	/settings/authentication/servers/1 status=enabled
	Should Match Regexp	${OUTPUT}	/settings/authentication/console admin_and_root_fallback_to_local_on_console=yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Match Regexp	${OUTPUT}	/settings/authentication/default_group default_group_for_remote_users=user

Test available commands after send tab-tab
	CLI:Enter Path	/settings/authentication/console/
	CLI:Test Available Commands	cd	change_password	commit	event_system_audit	event_system_clear	exit
	...	hostname	ls	pwd	quit	reboot	revert	set	shell	show	show_settings	shutdown	whoami

Test visible fields for show command
	CLI:Enter Path	/settings/authentication/console/
	CLI:Test Show Command	admin_and_root_fallback_to_local_on_console

Test available fields to set command
	CLI:Test Set Available Fields	admin_and_root_fallback_to_local_on_console

Test correct values for field=admin_and_root_fallback_to_local_on_console
	CLI:Test Set Field Options	admin_and_root_fallback_to_local_on_console	no	yes

Test incorrect values for field=admin_and_root_fallback_to_local_on_console
	CLI:Enter Path	/settings/authentication/console/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	admin_and_root_fallback_to_local_on_console	${WORD}	Error: Invalid value: ${WORD} for parameter: admin_and_root_fallback_to_local_on_console
	Run Keyword If	'${NGVERSION}' < '4.2'	CLI:Test Set Field Invalid Options	admin_and_root_fallback_to_local_on_console	${WORD}	Error: Invalid value: ${WORD}

Test empty values for field=admin_and_root_fallback_to_local_on_console
	CLI:Enter Path	/settings/authentication/console/
	Run Keyword If	'${NGVERSION}' > '4.2'	CLI:Test Set Field Invalid Options	admin_and_root_fallback_to_local_on_console	${EMPTY}	Error: Missing value for parameter: admin_and_root_fallback_to_local_on_console
	Run Keyword If	'${NGVERSION}' < '4.2'	CLI:Test Set Field Invalid Options	admin_and_root_fallback_to_local_on_console	${EMPTY}	Error: Missing value: admin_and_root_fallback_to_local_on_console

Test validation for fields admin_and_root_fallback_to_local_on_console
	CLI:Commands After Set Error	admin_and_root_fallback_to_local_on_console	${POINTS}	cd	change_password	commit	event_system_audit	event_system_clear	exit	hostname	ls	pwd	quit	revert
	CLI:Revert

*** Keywords ***
SUITE:Setup
    [Tags]		BUG_NG_8206
	CLI:Open
	CLI:Enter Path	/settings/authentication/servers
	${ERROR}	CLI:Write	delete 1	Raw
	Run Keyword If	'${NGVERSION}' > '4.2'	Should Contain	${ERROR}	Error: Cannot delete local authentication server
	#Error message is diffrent on v<=4.2
	CLI:Delete If Exists	2

SUITE:Teardown
	CLI:Close Connection