*** Settings ***
Resource	../../init.robot
Documentation	Tests> radius server configuration with optional accounting server
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI  	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${REMOTE_SERVER}	192.168.3.13
${METHOD}	radius
${ACCOUNTING_SERVER}	test1

*** Test Cases ***
Test case to add radius server with accounting server as optional
    CLI:Switch Connection	default
    CLI:Enter Path	/settings/authentication/servers/
    CLI:Add
    CLI:Set  method=${METHOD}
    CLI:Set  fallback_if_denied_access=yes
    CLI:Set  radius_secret=secret
    CLI:Set  remote_server=${REMOTE_SERVER}
    CLI:Set    radius_accounting_server=${EMPTY}
    ${OUTPUT}=    CLI:Commit
    Should Not Contain    ${OUTPUT}    Error

Test case to check the line for accounting server removed from config. file
    CLI:Switch Connection   root_session
    CLI:Enter Path	/etc/pam.d/servers/
    ${OUTPUT}=   CLI:Write  cat server1.conf
    Should Not Contain   ${OUTPUT}    acct

Test case to add accounting server and check configuration file
    CLI:Switch Connection	default
    CLI:Enter Path   /settings/authentication/servers/1/
    CLI:Set  radius_accounting_server=${ACCOUNTING_SERVER}
    CLI:Commit
    CLI:Switch Connection   root_session
    CLI:Enter Path	/etc/pam.d/servers/
    ${OUTPUT}=   CLI:Write  cat server1.conf
    Should Contain   ${OUTPUT}   acct

*** Keywords ***
SUITE:Setup
    CLI:Open
    CLI:Switch Connection	default
    CLI:Connect As Root

SUITE:Teardown
    CLI:Switch Connection	default
    CLI:Enter Path   /settings/authentication/servers/
    CLI:Write  delete 1
    CLI:Commit
    CLI:Close Connection
