*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authentication Console... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${REMOTE_SERVER}	192.168.2.88
${METHOD}	radius
${ACCOUNTING_SERVER}	test1

*** Test Cases ***
Test status field
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Set	method=${METHOD}
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Normal Fields	status	enabled	disabled
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Normal Fields	status	enabled	disabled

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	status	${EMPTY}	Error: Missing value for parameter: status
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	status	${EMPTY}	Error: Missing value: status

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	status	${WORD}	Error: Invalid value: ${WORD} for parameter: status
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	status	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	status	${POINTS}	Error: Invalid value: ${POINTS} for parameter: status
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	status	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Cancel	Raw

Test fallback_if_denied_access field
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Set	method=${METHOD}
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Normal Fields	fallback_if_denied_access	yes	no
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Normal Fields	fallback_if_denied_access	yes	no

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${EMPTY}	Error: Missing value for parameter: fallback_if_denied_access
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${EMPTY}	Error: Missing value: fallback_if_denied_access

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${WORD}	Error: Invalid value: ${WORD} for parameter: fallback_if_denied_access
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${POINTS}	Error: Invalid value: ${POINTS} for parameter: fallback_if_denied_access
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Cancel	Raw

Test remote_server field
	Skip If	'${NGVERSION}' == '3.2'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Set	method=${METHOD} radius_accounting_server=${ACCOUNTING_SERVER}
	CLI:Test Set Field Invalid Options	remote_server	${EMPTY}	Error: remote_server: Invalid IP address or host name.	yes
	CLI:Test Set Field Invalid Options	remote_server	${POINTS}	Error: remote_server: Invalid IP address or host name.	yes
	CLI:Test Set Field Invalid Options	remote_server	${ONE_POINTS}	Error: remote_server: Invalid IP address or host name.	yes
	CLI:Test Set Field Invalid Options	remote_server	${EXCEEDED}}	Error: remote_server: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test radius_accounting_server field
	Skip If	'${NGVERSION}' == '3.2'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Set	method=${METHOD} remote_server=${REMOTE_SERVER}
	Run Keyword If	'${NGVERSION}' <= '5.0'	CLI:Test Set Field Invalid Options	radius_accounting_server	${EMPTY}	Error: radius_accounting_server: Invalid IP address or host name.	yes
	CLI:Test Set Field Invalid Options	radius_accounting_server	${POINTS}	Error: radius_accounting_server: Invalid IP address or host name.	yes
	CLI:Test Set Field Invalid Options	radius_accounting_server	${ONE_POINTS}	Error: radius_accounting_server: Invalid IP address or host name.	yes
	CLI:Test Set Field Invalid Options	radius_accounting_server	${EXCEEDED}	Error: radius_accounting_server: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test radius_port field
	Skip If	'${NGVERSION}' < '4.1'	Only in 4.1 and up
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set method=${METHOD} remote_server=${REMOTE_SERVER} radius_accounting_server=${ACCOUNTING_SERVER}
	CLI:Test Set Field Invalid Options	radius_port	${EMPTY}	Error: radius_port: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	radius_port	${WORD}	Error: radius_port: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	radius_port	${POINTS}	Error: radius_port: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	radius_port	${ONE_WORD}	Error: radius_port: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	radius_port	${ONE_POINTS}	Error: radius_port: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	radius_port	${EXCEEDED}	Error: radius_port: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test radius_accounting_port field
	Skip If	'${NGVERSION}' < '4.1'	Only in 4.1 and up
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Write	set method=${METHOD} remote_server=${REMOTE_SERVER} radius_accounting_server=${ACCOUNTING_SERVER}
	CLI:Test Set Field Invalid Options	radius_accounting_port	${EMPTY}	Error: radius_accounting_port: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	radius_accounting_port	${WORD}	Error: radius_accounting_port: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	radius_accounting_port	${POINTS}	Error: radius_accounting_port: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	radius_accounting_port	${ONE_WORD}	Error: radius_accounting_port: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	radius_accounting_port	${ONE_POINTS}	Error: radius_accounting_port: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	radius_accounting_port	${EXCEEDED}	Error: radius_accounting_port: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test radius_timeout field
	Skip If	'${NGVERSION}' == '3.2'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Set	method=${METHOD} remote_server=${REMOTE_SERVER} radius_accounting_server=${ACCOUNTING_SERVER}
	CLI:Test Set Field Invalid Options	radius_timeout	${EMPTY}	Error: radius_timeout: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	radius_timeout	${WORD}	Error: radius_timeout: Exceeded the maximum size for this field.	yes
	CLI:Test Set Field Invalid Options	radius_timeout	${NUMBER}	Error: radius_timeout: Exceeded the maximum size for this field.	yes
	CLI:Test Set Field Invalid Options	radius_timeout	${POINTS}	Error: radius_timeout: Exceeded the maximum size for this field.	yes
	CLI:Test Set Field Invalid Options	radius_timeout	${ONE_WORD}	Error: radius_timeout: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	radius_timeout	${ONE_POINTS}	Error: radius_timeout: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	radius_timeout	${EXCEEDED}	Error: radius_timeout: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test radius_retries field
	Skip If	'${NGVERSION}' == '3.2'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Set	method=${METHOD} remote_server=${REMOTE_SERVER} radius_accounting_server=${ACCOUNTING_SERVER}
	CLI:Test Set Field Invalid Options	radius_retries	${EMPTY}	Error: radius_retries: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	radius_retries	${WORD}	Error: radius_retries: Exceeded the maximum size for this field.	yes
	CLI:Test Set Field Invalid Options	radius_retries	${NUMBER}	Error: radius_retries: Exceeded the maximum size for this field.	yes
	CLI:Test Set Field Invalid Options	radius_retries	${POINTS}	Error: radius_retries: Exceeded the maximum size for this field.	yes
	CLI:Test Set Field Invalid Options	radius_retries	${ONE_WORD}}	Error: radius_retries: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	radius_retries	${ONE_POINTS}	Error: radius_retries: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	radius_retries	${EXCEEDED}	Error: radius_retries: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test radius_enable_servicetype field
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Set	method=${METHOD} remote_server=${REMOTE_SERVER} radius_accounting_server=${ACCOUNTING_SERVER}
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Normal Fields	radius_enable_servicetype	no	yes
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Normal Fields	radius_enable_servicetype	no	yes

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	radius_enable_servicetype	${EMPTY}	Error: Missing value for parameter: radius_enable_servicetype
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	radius_enable_servicetype	${EMPTY}	Error: Missing value: radius_enable_servicetype

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	radius_enable_servicetype	${WORD}	Error: Invalid value: ${WORD} for parameter: radius_enable_servicetype
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	radius_enable_servicetype	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	radius_enable_servicetype	${POINTS}	Error: Invalid value: ${POINTS} for parameter: radius_enable_servicetype
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	radius_enable_servicetype	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Cancel	Raw

Test radius_service_type_login field
	Skip If	'${NGVERSION}' == '3.2'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Set	method=${METHOD} remote_server=${REMOTE_SERVER} radius_accounting_server=${ACCOUNTING_SERVER} radius_enable_servicetype=yes
	CLI:Test Set Field Invalid Options	radius_service_type_login	${NUMBER}	Error: radius_service_type_login: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	radius_service_type_login	${POINTS}	Error: radius_service_type_login: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	radius_service_type_login	${ONE_NUMBER}	Error: radius_service_type_login: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	radius_service_type_login	${ONE_POINTS}	Error: radius_service_type_login: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	radius_service_type_login	${EXCEEDED}	Error: radius_service_type_login: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test radius_service_type_framed field
	Skip If	'${NGVERSION}' == '3.2'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Set	method=${METHOD} remote_server=${REMOTE_SERVER} radius_accounting_server=${ACCOUNTING_SERVER} radius_enable_servicetype=yes
	CLI:Test Set Field Invalid Options	radius_service_type_framed	${NUMBER}	Error: radius_service_type_framed: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	radius_service_type_framed	${POINTS}	Error: radius_service_type_framed: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	radius_service_type_framed	${ONE_NUMBER}	Error: radius_service_type_framed: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	radius_service_type_framed	${ONE_POINTS}	Error: radius_service_type_framed: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	radius_service_type_framed	${EXCEEDED}	Error: radius_service_type_framed: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test radius_service_type_callback_login field
	Skip If	'${NGVERSION}' == '3.2'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Set	method=${METHOD} remote_server=${REMOTE_SERVER} radius_accounting_server=${ACCOUNTING_SERVER} radius_enable_servicetype=yes
	CLI:Test Set Field Invalid Options	radius_service_type_callback_login	${NUMBER}	Error: radius_service_type_callback_login: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	radius_service_type_callback_login	${POINTS}	Error: radius_service_type_callback_login: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	radius_service_type_callback_login	${ONE_NUMBER}	Error: radius_service_type_callback_login: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	radius_service_type_callback_login	${ONE_POINTS}	Error: radius_service_type_callback_login: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	radius_service_type_callback_login	${EXCEEDED}	Error: radius_service_type_callback_login: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test radius_service_type_callback_framed field
	Skip If	'${NGVERSION}' == '3.2'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Set	method=${METHOD} remote_server=${REMOTE_SERVER} radius_accounting_server=${ACCOUNTING_SERVER} radius_enable_servicetype=yes
	CLI:Test Set Field Invalid Options	radius_service_type_callback_framed	${NUMBER}	Error: radius_service_type_callback_framed: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	radius_service_type_callback_framed	${POINTS}	Error: radius_service_type_callback_framed: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	radius_service_type_callback_framed	${ONE_NUMBER}	Error: radius_service_type_callback_framed: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	radius_service_type_callback_framed	${ONE_POINTS}	Error: radius_service_type_callback_framed: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	radius_service_type_callback_framed	${EXCEEDED}	Error: radius_service_type_callback_framed: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test radius_service_type_outbound field
	Skip If	'${NGVERSION}' == '3.2'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Set	method=${METHOD} remote_server=${REMOTE_SERVER} radius_accounting_server=${ACCOUNTING_SERVER} radius_enable_servicetype=yes
	CLI:Test Set Field Invalid Options	radius_service_type_outbound	${NUMBER}	Error: radius_service_type_outbound: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	radius_service_type_outbound	${POINTS}	Error: radius_service_type_outbound: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	radius_service_type_outbound	${ONE_NUMBER}	Error: radius_service_type_outbound: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	radius_service_type_outbound	${ONE_POINTS}	Error: radius_service_type_outbound: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	radius_service_type_outbound	${EXCEEDED}	Error: radius_service_type_outbound: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test radius_service_type_administrative field with invalid values
	Skip If	'${NGVERSION}' == '3.2'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Set	method=${METHOD} remote_server=${REMOTE_SERVER} radius_accounting_server=${ACCOUNTING_SERVER} radius_enable_servicetype=yes
	CLI:Test Set Field Invalid Options	radius_service_type_administrative	${NUMBER}	Error: radius_service_type_administrative: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	radius_service_type_administrative	${POINTS}	Error: radius_service_type_administrative: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	radius_service_type_administrative	${ONE_NUMBER}	Error: radius_service_type_administrative: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	radius_service_type_administrative	${ONE_POINTS}	Error: radius_service_type_administrative: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	radius_service_type_administrative	${EXCEEDED}	Error: radius_service_type_administrative: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection
