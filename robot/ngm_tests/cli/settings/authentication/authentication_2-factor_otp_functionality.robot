*** Settings ***
Resource	../../init.robot
Documentation	Tests 2-factor authentication using One-Time Password functionality in CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	NON-CRITICAL
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
${LOCAL_USER}	test
${LOCAL_PASSWD}	testzpe2022
${TOTP_METHOD}	test_TOTP
${HOTP_METHOD}	test_HOTP
${AUTH_SERVER}	192.168.2.115
${ITSELF_NAME}	device_itself
${TYPE_ITSELF}	device_console

*** Test Cases ***
############### TOTP #################

Test normal login if 2-factor TOTP is not enforced for admin
	[Documentation]	Test enables TOTP method and set the 2-factor method to be applied in local authentication, but the
	...	enforce for admin is not enabled. This way admin should bypass 2-factor and login normally.
	SUITE:Enable/Disable 2-factor Method	${TOTP_METHOD}	enabled
	SUITE:Select Method As Local Authentication	${TOTP_METHOD}	ENFORCE_ADMIN=no
	CLI:Open	session_alias=test_admin
	CLI:Close Current Connection
	[Teardown]	CLI:Switch Connection	default

Test login and configure TOTP for test user
	[Documentation]	Test tries to login with a local account and checks the user has to configure OTP at fisrt login and
	...	also checks user can login successfully using OTP verification code.
	${OLD_CODE}	@{EMERGENCY_CODES}	SUITE:Login With OTP	${LOCAL_USER}	${LOCAL_PASSWD}
	Set Suite Variable	${OLD_CODE}
	Set Suite Variable	@{EMERGENCY_CODES}
	[Teardown]	CLI:Switch Connection	default

Test login with four TOTP emergency codes in less than 30 seconds is not allowed
	[Documentation]	Test local user can login with 3 emergency codes generated at TOTP configuration. And when it tries to
	...	login with the fourth one in less than 30 seconds, the TOTP generator does not allow and test fails.
	Reverse List	${EMERGENCY_CODES}
	FOR	${EMERGENCY_CODE}	IN	@{EMERGENCY_CODES}[1:4]
		SUITE:Login With OTP	${LOCAL_USER}	${LOCAL_PASSWD}	${EMERGENCY_CODE}
	END
	${WORKED}	Run Keyword And Return Status	SUITE:Login With OTP	${LOCAL_USER}	${LOCAL_PASSWD}	${EMERGENCY_CODE}[4]
	IF	${WORKED}
		Fail	Login with four different emergency codes in less than 30 seconds should not be allowed!
	END
	[Teardown]	CLI:Switch Connection	default

Test reset TOTP code for local user via admin
	[Documentation]	Test resets OTP verification code for local user, removes the old token and then configures the new one.
	CLI:Enter Path	/settings/local_accounts/
	CLI:Write	reset_user_otp_token ${LOCAL_USER}
	CLI:Switch Connection	otp_server
	SUITE:Remove OTP Token From Server	${LOCAL_USER}_token
	SUITE:Login With OTP	${LOCAL_USER}	${LOCAL_PASSWD}

Test login with local account after TOTP is configured
	[Documentation]	Test tries to login with a local account and checks the TOTP is already configured and user can login.
	SUITE:Login With OTP	${LOCAL_USER}	${LOCAL_PASSWD}

Test login with already expired TOTP code
	[Documentation]	Test tries to login with an already expired TOTP code and checks Login fails.
	${OTP_FAILED}	Run Keyword And Return Status	SUITE:Login With OTP	${LOCAL_USER}	${LOCAL_PASSWD}	${OLD_CODE}
	Pass Execution If	'${OTP_FAILED}' == 'False'	Local user cannot login with expired OTP code as expected!
	CLI:Close Current Connection
	[Teardown]	CLI:Switch Connection	default

Test local user resets its own TOTP token
	[Documentation]	Test login with a local account with TOTP and resets the token awaiting for the expected message
	SUITE:Login With OTP	${LOCAL_USER}	${LOCAL_PASSWD}	END_SESSION=${FALSE}
	${OUTPUT}	CLI:Write	reset_otp_token
	Should Contain	${OUTPUT}	OTP token was reset

Test generate new TOTP token for local account
	[Documentation]	Test uses generate_otp_token and proceeds with TOTP configuration for local account
	Write	generate_otp_token
	${OUTPUT}	Read Until	Verification code:
	${SECRET_KEY}	@{EMERGENCY_CODES}	SUITE:Configure OTP Token In Server	${OUTPUT}	${LOCAL_USER}_token
	Set Suite Variable	@{EMERGENCY_CODES}
	${CODE}	SUITE:Get OTP Verification Code	${LOCAL_USER}_token
	CLI:Switch Connection	root_session
	CLI:Write	${CODE}
	CLI:Close Current Connection

Test login with TOTP emergency codes
	[Documentation]	Test local user can login with emergency codes generated at TOTP configuration.
	Reverse List	${EMERGENCY_CODES}
	FOR	${EMERGENCY_CODE}	IN	@{EMERGENCY_CODES}[1:]
		SUITE:Login With OTP	${LOCAL_USER}	${LOCAL_PASSWD}	${EMERGENCY_CODE}
		Sleep	10s
	END

Test scarce TOTP emergency codes
	[Documentation]	Test tries to login with already used emergency codes and check it fails since emergency codes are
	...	one-time use.
	FOR	${EMERGENCY_CODE}	IN	@{EMERGENCY_CODES}[1:]
		${CODE_USED}	Run Keyword And Return Status	SUITE:Login With OTP	${LOCAL_USER}	${LOCAL_PASSWD}	${EMERGENCY_CODE}
		IF	${CODE_USED}
			Fail	Emergency code should not be able to login twice!
		END
	END

Test enforce TOTP for admin user
	[Documentation]	Test configures the condition to enforce OTP setup for Admin and Root users and them login with admin
	...	user using TOTP verification token.
	SUITE:Login With OTP	${DEFAULT_USERNAME}	${FACTORY_PASSWORD}	IS_ADMIN=${TRUE}
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	SUITE:Select Method As Local Authentication	${TOTP_METHOD}
	...	AND	Set Suite Variable	${ADMIN_ENFORCED}	${TRUE}

Test enforce TOTP for root user
	[Documentation]	Test configures the condition to enforce OTP setup for Admin and Root users and them login with root
	...	user using TOTP verification token.
	SUITE:Login With OTP	${ROOT_DEFAULT_USERNAME}	${ROOT_FACTORY_PASSWORD}	IS_ADMIN=${TRUE}
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	SUITE:Select Method As Local Authentication	${TOTP_METHOD}
	...	AND	Set Suite Variable	${ADMIN_ENFORCED}	${TRUE}

Test login with local user configuring both OTP and new password
	[Documentation]	Test resets TOTP token and configures the local user to have password change at login time and checks
	...	the correct flow happens (first configure TOTP code and then ask for new password).
	CLI:Enter Path	/settings/local_accounts/
	CLI:Write	reset_user_otp_token ${LOCAL_USER}
	CLI:Enter Path	${LOCAL_USER}
	CLI:Set	password_change_at_login=yes
	CLI:Commit
	SUITE:Login With OTP	${LOCAL_USER}	${LOCAL_PASSWD}	END_SESSION=${FALSE}	RAW_MODE=${TRUE}
	Read Until	New password:
	Write	${QA_PASSWORD}
	Read Until	Retype new password:
	CLI:Write	${QA_PASSWORD}
	Set Suite Variable	${ADMIN_ENFORCED}	${FALSE}
	[Teardown]	CLI:Switch Connection	default

Test login with local user configuring new password with TOTP configured
	[Documentation]	Test resets TOTP token and configures the local user to have password change at login time and checks
	...	the correct flow happens (first enters TOTP verofocation code and then ask for new password).
	CLI:Enter Path	/settings/local_accounts/${LOCAL_USER}
	CLI:Set	password_change_at_login=yes
	CLI:Commit
	SUITE:Login With OTP	${LOCAL_USER}	${QA_PASSWORD}	END_SESSION=${FALSE}	RAW_MODE=${TRUE}
	Read Until	New password:
	Write	${LOCAL_PASSWD}
	${OUTPUT}	Read Until	Retype new password:
	CLI:Write	${LOCAL_PASSWD}
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/local_accounts/${LOCAL_USER}
	CLI:Set	password_change_at_login=no
	CLI:Commit
	[Teardown]	CLI:Switch Connection	default

Test sudo command asks for TOTP code
	[Documentation]	Test configures shell access to local accounts and sets sudo permission to no. Then logins with local
	...	account, goes to shell and executes sudo commands and checks it asks for validation code.
	SUITE:Enable/Disable Shell Access In User Group	no
	SUITE:Login With OTP	${LOCAL_USER}	${LOCAL_PASSWD}	END_SESSION=${FALSE}	RAW_MODE=${TRUE}
	Write	shell
	Read Until	~$
	Write	sudo su admin
	${OUTPUT}	Read Until Regexp	(Password:)|(/home/${LOCAL_USER}\\$)
	${DID_NOT_ASK}	Run Keyword And Return Status	Should Contain	${OUTPUT}	/home/
	IF	${DID_NOT_ASK}
		Fail	Should ask for Verification code when executing sudo command!
	ELSE
		Write	${LOCAL_PASSWD}
		Read Until	Verification code:
	END
	[Teardown]	Run Keywords	CLI:Close Current Connection	AND	CLI:Switch Connection	default

Test accessing a managed device with TOTP authentication via connect command
	[Documentation]	Test adds a device which the localhost itself and then tries to access it using connect command, but
	...	since TOTP is enforced, the connection should fail once Nodegrid is not able to handle 2-factor authentication for
	...	managed devices.
	CLI:Add Device	${ITSELF_NAME}	${TYPE_ITSELF}	127.0.0.1	${LOCAL_USER}	${LOCAL_PASSWD}
	CLI:Enter Path	/access/${ITSELF_NAME}
	Write	connect
	Read Until	Login error: Authentication failed.
	CLI:Disconnect Device
	[Teardown]	CLI:Cancel	Raw

Test accessing a managed device with TOTP authentication via ssh command
	[Documentation]	Test tries to access it using ssh command directly to device, but since TOTP is enforced, the
	...	connection should fail once Nodegrid is not able to handle 2-factor authentication for managed devices.
	CLI:Connect As Root
	Write	ssh admin:${ITSELF_NAME}@${HOST}
	Read Until Regexp	(P|p)assword:
	Write	${FACTORY_PASSWORD}
	Read Until	Login error: Authentication failed.
	CLI:Close Current Connection
	[Teardown]	CLI:Switch Connection	default

############### HOTP #################

Test normal login if 2-factor HOTP is not enforced for admin
	[Documentation]	Test enables HOTP method and set the 2-factor method to be applied in local authentication, but the
	...	enforce for admin is not enabled. This way admin should bypass 2-factor and login normally.
	[Setup]	Run Keywords	SUITE:Select Method As Local Authentication	none	AND	SUITE:Enable/Disable 2-factor Method	${TOTP_METHOD}	disabled
	SUITE:Enable/Disable 2-factor Method	${HOTP_METHOD}	enabled
	SUITE:Select Method As Local Authentication	${HOTP_METHOD}	ENFORCE_ADMIN=no
	CLI:Open	session_alias=test_admin
	CLI:Close Current Connection
	[Teardown]	CLI:Switch Connection	default

Test login and configure HOTP for test user
	[Documentation]	Test tries to login with a local account and checks the user has to configure OTP at fisrt login and
	...	also checks user can login successfully using OTP verification code.
	${OLD_CODE}	@{EMERGENCY_CODES}	SUITE:Login With OTP	${LOCAL_USER}	${LOCAL_PASSWD}	OTP_TYPE=hotp
	Set Suite Variable	${OLD_CODE}
	Set Suite Variable	@{EMERGENCY_CODES}
	[Teardown]	CLI:Switch Connection	default

Test login with four HOTP emergency codes in less than 30 seconds is not allowed
	[Documentation]	Test local user can login with 3 emergency codes generated at HOTP configuration. And when it tries to
	...	login with the fourth one in less than 30 seconds, the HOTP generator does not allow and test fails.
	Reverse List	${EMERGENCY_CODES}
	FOR	${EMERGENCY_CODE}	IN	@{EMERGENCY_CODES}[1:4]
		${COUNTER}	SUITE:Get HOTP Current Counter
		SUITE:Login With OTP	${LOCAL_USER}	${LOCAL_PASSWD}	${EMERGENCY_CODE}	OTP_TYPE=hotp	COUNTER=${COUNTER}
	END
	${COUNTER}	SUITE:Get HOTP Current Counter
	${WORKED}	Run Keyword And Return Status	SUITE:Login With OTP	${LOCAL_USER}	${LOCAL_PASSWD}	${EMERGENCY_CODE}[4]	OTP_TYPE=hotp	COUNTER=${COUNTER}
	IF	${WORKED}
		Fail	Login with four different emergency codes in less than 30 seconds should not be allowed!
	END
	[Teardown]	CLI:Switch Connection	default

Test reset HOTP code for local user via admin
	[Documentation]	Test resets HOTP verification code for local user, removes the old token and then configures the new one.
	CLI:Enter Path	/settings/local_accounts/
	CLI:Write	reset_user_otp_token ${LOCAL_USER}
	CLI:Switch Connection	otp_server
	SUITE:Login With OTP	${LOCAL_USER}	${LOCAL_PASSWD}	OTP_TYPE=hotp

Test login with local account after HOTP is configured
	[Documentation]	Test tries to login with a local account and checks the HOTP is already configured and user can login.
	${COUNTER}	SUITE:Get HOTP Current Counter
	SUITE:Login With OTP	${LOCAL_USER}	${LOCAL_PASSWD}	OTP_TYPE=hotp	COUNTER=${COUNTER}

Test login with already expired HOTP code
	[Documentation]	Test tries to login with an already expired HOTP code and checks Login fails.
	${OTP_FAILED}	Run Keyword And Return Status	SUITE:Login With OTP	${LOCAL_USER}	${LOCAL_PASSWD}	${OLD_CODE}	OTP_TYPE=hotp
	Pass Execution If	'${OTP_FAILED}' == 'False'	Local user cannot login with expired OTP code as expected!
	CLI:Close Current Connection
	[Teardown]	CLI:Switch Connection	default

Test local user resets its own HOTP token
	[Documentation]	Test login with a local account with HOTP and resets the token awaiting for the expected message
	${COUNTER}	SUITE:Get HOTP Current Counter
	SUITE:Login With OTP	${LOCAL_USER}	${LOCAL_PASSWD}	END_SESSION=${FALSE}	OTP_TYPE=hotp	COUNTER=${COUNTER}
	${OUTPUT}	CLI:Write	reset_otp_token
	Should Contain	${OUTPUT}	OTP token was reset

Test generate new HOTP token for local account
	[Documentation]	Test uses generate_otp_token and proceeds with HOTP configuration for local account
	Write	generate_otp_token
	${OUTPUT}	Read Until	Verification code:
	${SECRET_KEY}	@{EMERGENCY_CODES}	SUITE:Configure OTP Token In Server	${OUTPUT}	OTP_TYPE=hotp
	Set Suite Variable	@{EMERGENCY_CODES}
	${CODE}	SUITE:Get OTP Verification Code	OTP_TYPE=hotp	SECRET_KEY=${SECRET_KEY}
	CLI:Switch Connection	root_session
	CLI:Write	${CODE}
	CLI:Close Current Connection

Test login with HOTP emergency codes
	[Documentation]	Test local user can login with emergency codes generated at HOTP configuration.
	Reverse List	${EMERGENCY_CODES}
	FOR	${EMERGENCY_CODE}	IN	@{EMERGENCY_CODES}[1:]
		${COUNTER}	SUITE:Get HOTP Current Counter
		SUITE:Login With OTP	${LOCAL_USER}	${LOCAL_PASSWD}	${EMERGENCY_CODE}	OTP_TYPE=hotp	COUNTER=${COUNTER}
		Sleep	10s
	END

Test scarce HOTP emergency codes
	[Documentation]	Test tries to login with already used emergency codes and check it fails since emergency codes are
	...	one-time use.
	FOR	${EMERGENCY_CODE}	IN	@{EMERGENCY_CODES}[1:]
		${COUNTER}	SUITE:Get HOTP Current Counter
		${CODE_USED}	Run Keyword And Return Status	SUITE:Login With OTP	${LOCAL_USER}	${LOCAL_PASSWD}	${EMERGENCY_CODE}	OTP_TYPE=hotp	COUNTER=${COUNTER}
		IF	${CODE_USED}
			Fail	Emergency code should not be able to login twice!
		END
	END

Test enforce HOTP for admin user
	[Documentation]	Test configures the condition to enforce OTP setup for Admin and Root users and them login with admin
	...	user using HOTP verification token.
	SUITE:Login With OTP	${DEFAULT_USERNAME}	${FACTORY_PASSWORD}	IS_ADMIN=${TRUE}	OTP_TYPE=hotp
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	SUITE:Select Method As Local Authentication	${HOTP_METHOD}
	...	AND	Set Suite Variable	${ADMIN_ENFORCED}	${TRUE}

Test enforce HOTP for root user
	[Documentation]	Test configures the condition to enforce OTP setup for Admin and Root users and them login with root
	...	user using HOTP verification token.
	SUITE:Login With OTP	${ROOT_DEFAULT_USERNAME}	${ROOT_FACTORY_PASSWORD}	IS_ADMIN=${TRUE}	OTP_TYPE=hotp
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	SUITE:Select Method As Local Authentication	${HOTP_METHOD}
	...	AND	Set Suite Variable	${ADMIN_ENFORCED}	${TRUE}

Test login with local user configuring both HOTP and new password
	[Documentation]	Test resets HOTP token and configures the local user to have password change at login time and checks
	...	the correct flow happens (first configure HOTP code and then ask for new password).
	CLI:Enter Path	/settings/local_accounts/
	CLI:Write	reset_user_otp_token ${LOCAL_USER}
	CLI:Enter Path	${LOCAL_USER}
	CLI:Set	password_change_at_login=yes
	CLI:Commit
	SUITE:Login With OTP	${LOCAL_USER}	${LOCAL_PASSWD}	END_SESSION=${FALSE}	RAW_MODE=${TRUE}	OTP_TYPE=hotp
	Read Until	New password:
	Write	${QA_PASSWORD}
	Read Until	Retype new password:
	CLI:Write	${QA_PASSWORD}
	Set Suite Variable	${ADMIN_ENFORCED}	${FALSE}
	[Teardown]	CLI:Switch Connection	default

Test login with local user configuring new password with HOTP configured
	[Documentation]	Test resets HOTP token and configures the local user to have password change at login time and checks
	...	the correct flow happens (first enters HOTP verifocation code and then ask for new password).
	CLI:Enter Path	/settings/local_accounts/${LOCAL_USER}
	CLI:Set	password_change_at_login=yes
	CLI:Commit
	${COUNTER}	SUITE:Get HOTP Current Counter
	SUITE:Login With OTP	${LOCAL_USER}	${QA_PASSWORD}	END_SESSION=${FALSE}	RAW_MODE=${TRUE}	OTP_TYPE=hotp	COUNTER=${COUNTER}
	Read Until	New password:
	Write	${LOCAL_PASSWD}
	${OUTPUT}	Read Until	Retype new password:
	CLI:Write	${LOCAL_PASSWD}
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/local_accounts/${LOCAL_USER}
	CLI:Set	password_change_at_login=no
	CLI:Commit
	[Teardown]	CLI:Switch Connection	default

Test sudo command asks for HOTP code
	[Documentation]	Test configures shell access to local accounts and sets sudo permission to no. Then logins with local
	...	account, goes to shell and executes sudo commands and checks it asks for validation code.
	SUITE:Enable/Disable Shell Access In User Group	no
	${COUNTER}	SUITE:Get HOTP Current Counter
	SUITE:Login With OTP	${LOCAL_USER}	${LOCAL_PASSWD}	END_SESSION=${FALSE}	RAW_MODE=${TRUE}	OTP_TYPE=hotp	COUNTER=${COUNTER}
	Write	shell
	Read Until	~$
	Write	sudo su admin
	${OUTPUT}	Read Until Regexp	(Password:)|(/home/${LOCAL_USER}\\$)
	${DID_NOT_ASK}	Run Keyword And Return Status	Should Contain	${OUTPUT}	/home/
	IF	${DID_NOT_ASK}
		Fail	Should ask for Verification code when executing sudo command!
	ELSE
		Write	${LOCAL_PASSWD}
		Read Until	Verification code:
	END
	[Teardown]	Run Keywords	CLI:Close Current Connection	AND	CLI:Switch Connection	default

Test accessing a managed device with HOTP authentication via connect command
	[Documentation]	Test adds a device which the localhost itself and then tries to access it using connect command, but
	...	since HOTP is enforced, the connection should fail once Nodegrid is not able to handle 2-factor authentication for
	...	managed devices.
	[Setup]	CLI:Delete All Devices
	CLI:Add Device	${ITSELF_NAME}	${TYPE_ITSELF}	127.0.0.1	${LOCAL_USER}	${LOCAL_PASSWD}
	CLI:Enter Path	/access/${ITSELF_NAME}
	Write	connect
	Read Until	Login error: Authentication failed.
	CLI:Disconnect Device
	[Teardown]	CLI:Cancel	Raw

Test accessing a managed device with HOTP authentication via ssh command
	[Documentation]	Test tries to access it using ssh command directly to device, but since HOTP is enforced, the
	...	connection should fail once Nodegrid is not able to handle 2-factor authentication for managed devices.
	CLI:Connect As Root
	Write	ssh admin:${ITSELF_NAME}@${HOST}
	Read Until Regexp	(P|p)assword:
	Write	${FACTORY_PASSWORD}
	Read Until	Login error: Authentication failed.
	CLI:Close Current Connection
	[Teardown]	CLI:Switch Connection	default

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Open	root	root	session_alias=otp_server	TYPE=root	HOST_DIFF=${AUTH_SERVER}
	SUITE:Remove OTP Token From Server	${LOCAL_USER}_token${SPACE}
	SUITE:Remove OTP Token From Server	${DEFAULT_USERNAME}_token
	SUITE:Remove OTP Token From Server	${ROOT_DEFAULT_USERNAME}_token
	CLI:Switch Connection	default
	CLI:Change Hostname	${HOSTNAME_NODEGRID}	#workaround added to avoid tests from failing if hostname is not default
	CLI:Delete Users	${LOCAL_USER}
	CLI:Delete All Devices
	SUITE:Select Method As Local Authentication	none
	SUITE:Delete OTP 2-factor Method	${TOTP_METHOD}
	SUITE:Delete OTP 2-factor Method	${HOTP_METHOD}
	SUITE:Add Local User	${LOCAL_USER}	${LOCAL_PASSWD}
	SUITE:Add OTP 2-factor Method	${TOTP_METHOD}
	SUITE:Add OTP 2-factor Method	${HOTP_METHOD}	TYPE=counter_based
	Set Suite Variable	${ADMIN_ENFORCED}	${FALSE}

SUITE:Teardown
	SUITE:Remove OTP Token From Server	${ROOT_DEFAULT_USERNAME}_token
	SUITE:Reset Root Token
	SUITE:Remove OTP Token From Server	${DEFAULT_USERNAME}_token
	SUITE:Remove OTP Token From Server	${LOCAL_USER}_token
	IF	${ADMIN_ENFORCED}
		SUITE:Login With OTP	${DEFAULT_USERNAME}	${FACTORY_PASSWORD}
	ELSE
		CLI:Open
	END
	CLI:Delete Users	${LOCAL_USER}
	CLI:Delete All Devices
	SUITE:Select Method As Local Authentication	none
	SUITE:Delete OTP 2-factor Method	${TOTP_METHOD}
	SUITE:Delete OTP 2-factor Method	${HOTP_METHOD}
	CLI:Close Connection

SUITE:Add Local User
	[Documentation]	Keywords add a local account into nodegrid. This keyword is more generic than CLI:Add User because
	...	user_group and password_change_at_login paramenters are configurable.
	[Arguments]	${USER}	${PASSWD}	${GROUP}=user	${REQUIRE_CHANGE}=no
	CLI:Enter Path	/settings/local_accounts
	CLI:Add
	CLI:Set	username=${USER} password=${PASSWD} user_group=${GROUP}
	CLI:Set	password_change_at_login=${REQUIRE_CHANGE}
	CLI:Commit
	CLI:Connect As Root
	Write	ssh ${USER}@${HOST}
	${SSH_OUTPUT}	Read Until Regexp	((P|p)assword:)|(\\?)
	Log	${SSH_OUTPUT}
	${ASKED_CONFIRM}	Run Keyword And Return Status	Should Contain	${SSH_OUTPUT}	?
	IF	${ASKED_CONFIRM}
		Write	yes
		Read Until	Password:
	END
	CLI:Write	${PASSWD}
	CLI:Close Current Connection
	[Teardown]	CLI:Switch Connection	default

SUITE:Add OTP 2-factor Method
	[Documentation]	Keyword adds a 2-factor method type OTP, configuring name, status, otp type and enforce otp setup at
	...	first login or not.
	[Arguments]	${NAME}	${STATUS}=disabled	${TYPE}=time_based	${ENFORCE}=yes
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Add
	CLI:Set	name=${NAME} method=otp status=${STATUS} otp_type=${TYPE}
	CLI:Set	enforce_otp_setup=${ENFORCE}
	CLI:Commit

SUITE:Delete OTP 2-factor Method
	[Documentation]	Keyword deletes OTP 2-factor method if exists.
	[Arguments]	${METHOD}
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Delete If Exists	${METHOD}

SUITE:Enable/Disable 2-factor Method
	[Documentation]	Keyword changes the status of a specified 2-factor authentication method
	[Arguments]	${METHOD}	${STATUS}
	CLI:Enter Path	/settings/authentication/2-factor/${METHOD}
	CLI:Set	status=${STATUS}
	CLI:Commit

SUITE:Select Method As Local Authentication
	[Documentation]	Keyword sets specified 2-factor authentication method to be used in local authentication server.
	...	Keyword also has the option to enforce this 2-factor authentication for admin and root users.
	[Arguments]	${METHOD}	${ENFORCE_ADMIN}=no
	CLI:Enter Path	/settings/authentication/servers/1
	CLI:Set	2-factor_authentication=${METHOD}
	CLI:Set	apply_2-factor_auth_for_admin_and_root_users=${ENFORCE_ADMIN}
	CLI:Commit

SUITE:Login With OTP
	[Documentation]
	[Arguments]	${USER}	${PASSWD}	${OTP_CODE}=${EMPTY}	${END_SESSION}=${TRUE}	${IS_ADMIN}=${FALSE}
	...	${RAW_MODE}=${FALSE}	${OTP_TYPE}=totp	${COUNTER}=0
	CLI:Switch Connection	default
	CLI:Connect As Root
	IF	${IS_ADMIN}
		CLI:Switch Connection	default
		IF	'${OTP_TYPE}' == 'totp'
			SUITE:Select Method As Local Authentication	${TOTP_METHOD}	yes
		ELSE
			SUITE:Select Method As Local Authentication	${HOTP_METHOD}	yes
		END
		CLI:Switch Connection	root_session
	END
	Write	ssh ${USER}@${HOST}
	${SSH_OUTPUT}	Read Until Regexp	((P|p)assword:)|(\\?)
	Log	${SSH_OUTPUT}
	${ASKED_CONFIRM}	Run Keyword And Return Status	Should Contain	${SSH_OUTPUT}	?
	IF	${ASKED_CONFIRM}
		Write	yes
		Read Until	Password:
	END
	Write	${PASSWD}
	${OUTPUT}	Read Until Regexp	(Verification code:)|(Password:)
	Log	${OUTPUT}
	IF	not ${RAW_MODE}
		${LOGGED}	Run Keyword And Return Status	Should Contain	${OUTPUT}	Verification code:
		Run Keyword If	not ${LOGGED}	Fail	${OUTPUT}
	END
	${HAS_TO_CONFIGURE}	Run Keyword And Return Status	Should Contain	${OUTPUT}	One-Time Password (OTP) Setup
	IF	${HAS_TO_CONFIGURE}
		IF	'${OTP_TYPE}' == 'totp'
			${SECRET_KEY}	@{EMERGENCY_CODES}	SUITE:Configure OTP Token In Server	${OUTPUT}	${USER}_token	OTP_TYPE=${OTP_TYPE}
		ELSE
			${SECRET_KEY}	@{EMERGENCY_CODES}	SUITE:Configure OTP Token In Server	${OUTPUT}	OTP_TYPE=${OTP_TYPE}
			Set Suite Variable	${SECRET_KEY}
		END
	END
	IF	'${OTP_CODE}' == '${EMPTY}'
		IF	'${OTP_TYPE}' == 'totp'
			${CODE}=	SUITE:Get OTP Verification Code	${USER}_token
		ELSE
			${CODE}=	SUITE:Get OTP Verification Code	OTP_TYPE=hotp	COUNTER=${COUNTER}	SECRET_KEY=${SECRET_KEY}
		END
	ELSE
		${CODE}=	Set Variable	${OTP_CODE}
	END
	CLI:Switch Connection	root_session
	Write	${CODE}
	${PROMPT}	Read Until Regexp	(~#)|(]#)|((P|p)assword:)
	Log	${PROMPT}
	IF	not ${RAW_MODE}
		${LOGGED}	Run Keyword And Return Status	Should Contain	${PROMPT}	${USER}@${HOSTNAME_NODEGRID}
		Run Keyword If	not ${LOGGED}	Fail	${PROMPT}
	ELSE
		Write	${PASSWD}
	END
	IF	${END_SESSION}
		CLI:Close Current Connection
	END
	Return From Keyword If	${HAS_TO_CONFIGURE}	${CODE}	@{EMERGENCY_CODES}
	[Return]	${CODE}

SUITE:Configure OTP Token In Server
	[Documentation]	Keyword configures the token for a user and returns the secret key and also the emergency scratch codes.
	[Arguments]	${OUTPUT}	${TOKEN_NAME}=${EMPTY}	${OTP_TYPE}=totp
	CLI:Switch Connection	otp_server
	${SECRET_KEY_LINE}	Get Lines Containing String	${OUTPUT}	Enter this text code (secret key)
	${SECRET_KEY}	Fetch From Right	${SECRET_KEY_LINE}	Enter this text code (secret key):${SPACE}
	${EMERGENCY_CODES_LINE}	Get Lines Containing String	${OUTPUT}	Emergency scratch codes
	${EMERGENCY_CODES_STRING}	Fetch From Right	${EMERGENCY_CODES_LINE}	Emergency scratch codes:${SPACE}
	@{EMERGENCY_CODES}	Split String	${EMERGENCY_CODES_STRING}	separator=${SPACE}
	IF	'${OTP_TYPE}' == 'totp'
		Write	otp-cli add ${TOKEN_NAME} ${SECRET_KEY}
		Read Until	Password:
		${TOKEN_ADDED}	CLI:Write Bare	\n
		Log	${TOKEN_ADDED}
		Should Contain	${TOKEN_ADDED}	Created [/root/otp-cli/tokens/${TOKEN_NAME}]
	END
	[Return]	${SECRET_KEY}	@{EMERGENCY_CODES}

SUITE:Remove OTP Token From Server
	[Documentation]	Keyword lists tokens present in OTP server and removes if this specified token if it exists
	[Arguments]	${TOKEN_NAME}
	CLI:Switch Connection	otp_server
	${TOKENS}	CLI:Write	otp-cli list
	${HAS_TOKEN}	Run Keyword And Return Status	Should Contain	${TOKENS}	${TOKEN_NAME}
	IF	${HAS_TOKEN}
		${TOKEN_REMOVED}	CLI:Write	otp-cli remove ${TOKEN_NAME}
		Log	${TOKEN_REMOVED}
		Should Contain	${TOKEN_REMOVED}	Removed file [/root/otp-cli/tokens/${TOKEN_NAME}]
	END

SUITE:Get OTP Verification Code
	[Documentation]	Keyword gets the verification code generated by OTP server from a given token
	[Arguments]	${TOKEN_NAME}=${EMPTY}	${OTP_TYPE}=totp	${COUNTER}=0	${SECRET_KEY}=${EMPTY}
	CLI:Switch Connection	otp_server
	IF	'${OTP_TYPE}' == 'totp'
		${OTP_CODE}	CLI:Write	otp-cli clip ${TOKEN_NAME}	user=Yes
		Log	${OTP_CODE}
	ELSE
		${OTP_CODE}	CLI:Write	oathtool -c ${COUNTER} -b --hotp ${SECRET_KEY}	user=Yes
		Log	${OTP_CODE}
	END
	[Return]	${OTP_CODE}

SUITE:Reset Root Token
	[Documentation]	Keyword resets the otp token for root user, since reset_user_otp_token does not work once root user is
	...	not listed in local accounts.
	CLI:Connect As Root
	Set Client Configuration	prompt=]#
	CLI:Write	cli
	${TOKEN_RESETED}	CLI:Write	reset_otp_token	Raw
	Log	${TOKEN_RESETED}
	CLI:Close Current Connection

SUITE:Enable/Disable Shell Access In User Group
	[Documentation]	Keyword sets shell access for user group and sudo permission.
	[Arguments]	${ACCESS}=no
	CLI:Enter Path	/settings/authorization/user/profile
	CLI:Set	shell_access=yes sudo_permission=${ACCESS}
	CLI:Commit

SUITE:Get HOTP Current Counter
	[Documentation]
	[Arguments]	${USER}=${LOCAL_USER}
	CLI:Switch Connection	default
	CLI:Connect As Root	session_alias=get_counter
	${COUNTER}	CLI:Write	cat /home/${USER}/.${HOTP_METHOD}.secret | grep COUNTER	user=Yes
	${IS_1ST_LOGIN}	Run Keyword And Return Status	Should Contain	${COUNTER}	No such file
	IF	${IS_1ST_LOGIN}
		${COUNTER}	CLI:Write	cat /tmp/${USER}.${HOTP_METHOD}.secret | grep COUNTER	user=Yes
	END
	CLI:Close Current Connection
	${COUNTER}	Remove String	${COUNTER}	" HOTP_COUNTER${SPACE}
	[Return]	${COUNTER}