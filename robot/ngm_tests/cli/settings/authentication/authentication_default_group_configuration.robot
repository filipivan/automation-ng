*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authentication Servers... through the CLI
Metadata	Version 1.0
Metadata	Executed At ${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI SSH SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test correct values for field=default_group_for_remote_users
	Skip If	'${NGVERSION}' == '3.2'	Not implemented in 3.2
	CLI:Enter Path	/settings/authentication/default_group/
	Write Bare	set default_group_for_remote_users=\t\t\t\n
	CLI:Read Until Prompt
	${RESULT}=	CLI:Read Until Prompt	Raw
	Should Contain	${RESULT}	${DEFAULT_USERNAME}
	Should Contain	${RESULT}	user
	[Teardown]	CLI:Revert

Test invalid values for field=default_group_for_remote_users
	[Documentation]	Checks valid and invalid values on v5.6+. On v4.2 to v5.4 it's not working and won't be fixed as per comment from Davi on BUG_NG_9836
	Skip If	'${NGVERSION}' <= '5.4'	Not working or implemented on v5.4 and previous
	CLI:Enter Path	/settings/authentication/default_group/
	CLI:Set Field	default_group_for_remote_users	def_group
	${OUTPUT}	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: Invalid value: def_group for parameter: default_group_for_remote_users
	CLI:Test Not Show Command	def_group
	CLI:Test Show Command	default_group_for_remote_users = ${EMPTY}
	[Teardown]	CLI:Revert

Resetting default group
	Skip If	'${NGVERSION}' == '3.2'	Not implemented in 3.2
	CLI:Enter Path	/settings/authentication/default_group/
	CLI:Set Field	default_group_for_remote_users	user
	CLI:Commit
	[Teardown]	CLI:Revert

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection