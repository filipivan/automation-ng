*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authentication SSO Configuration... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NAME}	idp_test
${ENTITY_ID}	nodegrid_automation
${x.509}	remote_server
${SSO_URL}	${SSOSERVER_URL}${SSOSERVER_SERVICE_PATH}
${ISSUER}	${SSOSERVER_URL}${SSOSERVER_ISSUER_PATH}
${URL}	${FTPSERVER2_URL}${SSOSERVER_CRT_PATH}
${URL_XML}	${FTPSERVER2_URL}${SSOSERVER_XML_PATH}
${FTP_USERNAME}	${FTPSERVER2_USER}
${FTP_PASSWORD}	${FTPSERVER2_PASSWORD}

*** Test Cases ***
Test valid configuration
	CLI:Enter Path	/settings/authentication/sso
	CLI:Add
	CLI:Set	name=${NAME} entity_id=${ENTITY_ID} sso_url=${SSO_URL} issuer=${ISSUER} x.509_certificate=${x.509} url=${URL} username=${FTP_USERNAME} password=${FTP_USERNAME}
	CLI:Commit
	CLI:Test Show Command Regexp	${NAME}\\s+Disabled
	[Teardown]	CLI:Cancel	Raw

Test drilldown on valid configuration
	CLI:Enter Path	/settings/authentication/sso/
	CLI:Test Ls Command	${NAME}
	CLI:Enter Path	${NAME}
	CLI:Test Show Command	name: ${NAME}	status = disabled	entity_id = ${ENTITY_ID}	sso_url = ${SSO_URL}
	...	issuer = ${ISSUER}	icon = sso.png

Test available commands after send tab-tab on valid configuration
	CLI:Enter Path	/settings/authentication/sso/
	CLI:Test Ls Command	${NAME}
	CLI:Enter Path	${NAME}
	CLI:Test Available Commands	apply_settings	certificate	create_csr	exit	ls	reboot	set	show_settings	system_certificate
	...	cancel	change_password	event_system_audit	factory_settings	pwd	revert	shell	shutdown	system_config_check
	...	cd	commit	event_system_clear	hostname	quit	save_settings	show	software_upgrade	whoami

Test available editing field on certificate command
	CLI:Enter Path	/settings/authentication/sso/${NAME}
	CLI:Write	certificate
	# in v.5.0x, it does not shows ceritficate_name NG-3404
	#CLI:Test Set Available Fields	certificate_name	x.509_certificate
	 Run Keyword If	'${NGVERSION}' >= '4.0' and '${NGVERSION}' < '5.0'  CLI:Test Set Available Fields	certificate_name	x.509_certificate
     Run Keyword If	'${NGVERSION}' <='4.0' or '${NGVERSION}' == '5.0'  CLI:Test Set Available Fields	certificate  x.509_certificate
	[Teardown]	CLI:Cancel	Raw

Test available editing field
	CLI:Enter Path	/settings/authentication/sso/${NAME}
	CLI:Test Set Available Fields	entity_id	issuer	status	icon	sso_url

Test editing field
	CLI:Enter Path	/settings/authentication/sso/${NAME}
	CLI:Set Field	status	enabled
	CLI:Commit
	CLI:Enter Path	..
	CLI:Test Show Command Regexp	${NAME}\\s+Enabled
	[Teardown]	CLI:Cancel	Raw

Test duplicate name validation
	CLI:Enter Path	/settings/authentication/sso
	CLI:Add
	CLI:Set	x.509_certificate=${X.509} url=${URL} username=${FTP_USERNAME} password=${FTP_PASSWORD} entity_id=${ENTITY_ID} issuer=${ISSUER} sso_url=${SSO_URL}
	CLI:Test Set Field Invalid Options	name	${NAME}	Error: name: Identity Provider name already exists.	yes
	[Teardown]	CLI:Cancel	Raw

Test enabling two different Identity Providers
	CLI:Enter Path	/settings/authentication/sso
	CLI:Add
	CLI:Set	name=${NAME}2 x.509_certificate=${X.509} url=${URL} username=${FTP_USERNAME} password=${FTP_PASSWORD} entity_id=${ENTITY_ID} issuer=${ISSUER} sso_url=${SSO_URL}
		#No message displayed with 	Error: status: Only one Identity Provider can be 2_enable at a time.
	#CLI:Test Set Field Invalid Options	status	enabled	Error: status: Only one Identity Provider can be enabled at a time.	yes
	CLI:Test Set Field Invalid Options	status	enabled
	[Teardown]	CLI:Cancel	Raw

Test adding Identity Provider with xml file
	CLI:Enter Path	/settings/authentication/sso
	CLI:Write	import_metadata
	CLI:Set	name=${NAME}_xml entity_id=${ENTITY_ID}_xml metadata=remote_server url=${URL_XML} username=${FTP_USERNAME} password=${FTP_PASSWORD}
	CLI:Commit
	CLI:Test Show Command Regexp	${NAME}_xml\\s+Disabled

Test to check for validation of fields
	CLI:Enter Path	/settings/authentication/sso/${NAME}_xml
	CLI:Test Show Command	name: ${NAME}_xml	entity_id = ${ENTITY_ID}_xml	sso_url = ${SSO_URL}
	...	issuer = ${ISSUER}	icon = sso.png

Test deleting valid configuration
	CLI:Enter Path	/settings/authentication/sso
	CLI:Delete	${NAME},${NAME}_xml
	CLI:Test Not Show Command	${NAME}	${NAME}_xml

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/authentication/sso/
	CLI:Delete If Exists	${NAME}	${NAME}_xml	${NAME}2

SUITE:Teardown
	CLI:Enter Path	/settings/authentication/sso/
	CLI:Delete If Exists	${NAME}	${NAME}_xml	${NAME}2
	CLI:Close Connection