*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authentication Servers authentication default group validation
Metadata	Version 1.0
Metadata	Executed At ${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI SSH SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test available commands after send tab-tab
	Skip If	'${NGVERSION}' == '3.2'	Not implemented in 3.2
	CLI:Enter Path	/settings/authentication/default_group/
	CLI:Test Available Commands	apply_settings	hostname	shell
	...	cd	ls	show
	...	change_password	pwd	show_settings
	...	commit	quit	shutdown
	...	event_system_audit	reboot	software_upgrade
	...	event_system_clear	revert	system_certificate
	...	exit	save_settings	system_config_check
	...	factory_settings	set	whoami
	[Teardown]	CLI:Revert

Test visible fields for show command
	Skip If	'${NGVERSION}' == '3.2'	Not implemented in 3.2
	Run Keyword If	'${NGVERSION}' >= '4.2'	Set Tags	BUG_1242
	CLI:Enter Path	/settings/authentication/default_group/
	CLI:Test Show Command	default_group_for_remote_users =	This group will be assigned to remote authenticated users that do not have any assigned local authorization group.
	[Teardown]	CLI:Revert

Test ls command for default_group
	Skip If	'${NGVERSION}' == '3.2'	Not implemented in 3.2
	CLI:Enter Path	/settings/authentication/default_group/
	CLI:Test Ls Command	${EMPTY}
	[Teardown]	CLI:Revert

Test fields available to set
	Skip If	'${NGVERSION}' == '3.2'	Not implemented in 3.2
	CLI:Enter Path	/settings/authentication/default_group/
	CLI:Test Set Available Fields	default_group_for_remote_users
	[Teardown]	CLI:Revert

Test default_group_for_remote_users field
	Skip If	'${NGVERSION}' <= '5.4'	Not working or implemented on v5.4 and previous
	CLI:Enter Path	/settings/authentication/default_group/
	CLI:Test Set Validate Normal Fields	default_group_for_remote_users	user	${DEFAULT_USERNAME}	${EMPTY}
	@{VALUES}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${EXCEEDED}
	FOR		${VALUE}	IN	@{VALUES}
		CLI:Test Set Validate Invalid Options	default_group_for_remote_users	${VALUE}
		...		Error: Invalid value: ${VALUE} for parameter: default_group_for_remote_users
	END
	[Teardown]	CLI:Revert

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection