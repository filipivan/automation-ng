*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authentication Servers... through the CLI
Metadata	Version 1.0
Metadata	Executed At ${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI SSH SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NAME}	def_group

*** Test Cases ***
Test adding new group and seeing if it shows as an option
	Skip If	'${NGVERSION}' < '4.0'	Not implemented in 3.2
	CLI:Enter Path	/settings/authorization/
	CLI:Delete If Exists Confirm	${NAME}
	CLI:Add
	CLI:Set Field	name	${NAME}
	CLI:Commit

	CLI:Enter Path	/settings/authentication/default_group/
	Write Bare	set default_group_for_remote_users=\t\t\t\n
	${RESULT}=	CLI:Read Until Prompt	Raw
	Should Contain	${RESULT}	${NAME}
	CLI:Read Until Prompt
	CLI:Revert

	CLI:Enter Path	/settings/authorization/
	CLI:Delete If Exists Confirm	${NAME}

	CLI:Enter Path	/settings/authentication/default_group/
	Write Bare	set default_group_for_remote_users=\t\t\t\n
	${RESULT}=	CLI:Read Until Prompt	Raw
	Should Not Contain	${RESULT}	${NAME}
	CLI:Read Until Prompt
	[Teardown]	CLI:Revert

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection