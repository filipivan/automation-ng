*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authentication Console... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
#Suite Teardown	SUITE:Teardown

*** Variables ***
${AD_SECURE}	start_tls

*** Test Cases ***
Configure AD Provider domain base dn, no security and no search username and failback
	CLI:Enter Path	/settings/authentication/servers
	CLI:Add
	CLI:Set	method=ldap_or_ad status=enabled remote_server=${AD_SERVER} ldap_ad_base=${AD_BASE_DOMAIN} ldap_ad_secure=${AD_SECURE} fallback_if_denied_access=yes ldap_ad_group_attribute=${AD_GROUP_ATRRIB} ldap_ad_login_attribute=${ADUSERATRRIB}
	CLI:Commit
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	ldap or ad	ignore_case=True
	Should Contain	${OUTPUT}	${AD_SERVER}
	### BEGIN - Test created because of a bug that is in trello #718, link https://trello.com/c/XOGcEi5I
	${OUTPUT}=	CLI:Enter Path	/settings/authentication/servers/1
	Should Not Contain	${OUTPUT}	Json-CRITICAL
	CLI:Enter Path	/settings/authentication/servers/
	### END - Test created because of a bug that is in trello #718, link https://trello.com/c/XOGcEi5I
	Close Connection

Valid AD User authentication without AD Browsing (EXPECTED LOGIN FAILURE)
	Log to Console	Valid LDAP User authentication
	${OUTPUT}=	Run Keyword And Expect Error	Authentication failed for user '${AD_BROWSE_USER}'.
	...	CLI:Open	${AD_BROWSE_USER}	${AD_BROWSE_PASSWORD}	startping=no	timeout=60s
	Log to Console	${OUTPUT}
	Close Connection

Invalid AD User authentication without AD Browsing
	Log to Console	Invalid LDAP user Authentication
	${OUTPUT}=	Run Keyword And Expect Error	Authentication failed for user '${AD_INVALID_USER}'.
	...	CLI:Open	${AD_INVALID_USER}	${AD_BROWSE_PASSWORD}	startping=no	timeout=60s
	Log to Console	${OUTPUT}
	Close Connection

Test local Failback
	Log to Console	local admin authentication is allowed due to failback
	Log to Console	Valid LDAP User authentication
	CLI:Open	startping=no	timeout=60s
	${OUTPUT}=	CLI:Write	whoami
	Should Contain	${OUTPUT}	${DEFAULT_USERNAME}
	Close Connection

Configure AD Provider domain base dn, no security and with search username
	CLI:Open	startping=no	timeout=60s
	SUITE:Delete all LDAP Providers
	CLI:Enter Path	/settings/authentication/servers
	CLI:Add
	CLI:Set	method=ldap_or_ad status=enabled remote_server=${AD_SERVER} ldap_ad_base=${AD_BASE_DOMAIN} ldap_ad_secure=${AD_SECURE} fallback_if_denied_access=yes ldap_ad_database_username=${AD_BROWSE_USER_DN} ldap_ad_database_password=${AD_BROWSE_PASSWORD} ldap_ad_group_attribute=${AD_GROUP_ATRRIB} ldap_ad_login_attribute=${AD_USER_ATRRIB}
	CLI:Commit
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	ldap or ad	ignore_case=True
	Should Contain	${OUTPUT}	${AD_SERVER}
	### BEGIN - Test created because of a bug that is in trello #718, link https://trello.com/c/XOGcEi5I
	${OUTPUT}=	CLI:Enter Path	/settings/authentication/servers/1
	Should Not Contain	${OUTPUT}	Json-CRITICAL
	CLI:Enter Path	/settings/authentication/servers/
	### END - Test created because of a bug that is in trello #718, link https://trello.com/c/XOGcEi5I
	Close Connection

Valid AD User authentication with AD Browsing
	${VALIDATE_AD_USER_AUTHENTICATION}=	Set Variable If	'${VALIDATE_LDAP_USER_AUTHENTICATION}'=='None'	True	${VALIDATE_AD_USER_AUTHENTICATION}
	Skip If	${VALIDATE_AD_USER_AUTHENTICATION} == False	AD Tests Disabled
	CLI:Open	${AD_BROWSE_USER}	${AD_BROWSE_PASSWORD}	startping=no	timeout=60s
	${OUTPUT}=	CLI:Write	whoami
	Should Contain	${OUTPUT}	${AD_BROWSE_USER}
	Close Connection

Invalid AD User authentication with AD Browsing
	Log to Console	\nInvalid LDAP user Authentication
	${OUTPUT}=	Run Keyword And Expect Error	Authentication failed for user '${AD_INVALID_USER}'.
	...	CLI:Open	${AD_INVALID_USER}	${AD_BROWSE_PASSWORD}	startping=no	timeout=60s
	Log to Console	${OUTPUT}
	Close Connection

Assign External Group testgroup to Admin Group
	CLI:Open	startping=no	timeout=60s
	CLI:Enter Path	/settings/authorization/${DEFAULT_USERNAME}/remote_groups/
	CLI:Set Field	remote_groups	${AD_ADMIN_GROUP}
	CLI:Commit
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	testgroup
	Close Connection

Login with user member testgroup and verify admin permissions
	${VALIDATE_AD_USER_AUTHENTICATION}=	Set Variable If	'${VALIDATE_LDAP_USER_AUTHENTICATION}'=='None'	True	${VALIDATE_AD_USER_AUTHENTICATION}
	Skip If	${VALIDATE_AD_USER_AUTHENTICATION} == False	AD Tests Disabled
	CLI:Open	${AD_ADMIN_USER}	${AD_ADMIN_PASSWORD}	startping=no	timeout=60s
	@{ROOTFOLDER}=	Create List	access/	system/	settings/
	${OUTPUT}=	CLI:Write	ls
	CLI:Should Contain All	${OUTPUT}	${ROOTFOLDER}
	@{SYSTEMFOLDER}=	Create List	toolkit/	open_sessions/	device_sessions/	event_list/	routing_table/	system_usage/	discovery_logs/	lldp/	network_statistics/	usb_devices/

	${IS_NSC}	CLI:Is Serial Console System
	Run Keyword If	${IS_NSC}	Append To List	${SYSTEMFOLDER}	serial_statistics/	serial_ports_summary/

	${OUTPUT}=	CLI:Write	ls /system
	CLI:Should Contain All	${OUTPUT}	${SYSTEMFOLDER}
	[Teardown]	Close Connection

Login with user not member of testgroup and verify limited permissions
	${VALIDATE_AD_USER_AUTHENTICATION}=	Set Variable If	'${VALIDATE_LDAP_USER_AUTHENTICATION}'=='None'	True	${VALIDATE_AD_USER_AUTHENTICATION}
	Skip If	${VALIDATE_AD_USER_AUTHENTICATION} == False	AD Tests Disabled
	CLI:Open	${AD_BROWSE_USER}	${AD_BROWSE_PASSWORD}	startping=no	timeout=60s
	@{ROOTFOLDER}=	Create List	access/	system/
	${OUTPUT}=	CLI:Write	ls
	CLI:Should Contain All	${OUTPUT}	${ROOTFOLDER}
	Should Not Contain	${OUTPUT}	settings/

	@{SYSTEMFOLDER}=	Create List	toolkit/	open_sessions/	device_sessions/	event_list/	routing_table/	system_usage/	discovery_logs/	serial_statistics/	erial_ports_summary/	lldp/	network_statistics/	usb_devices/
	${OUTPUT}=	CLI:Write	ls /system
	Should Contain	${OUTPUT}	about/
	CLI:Should Not Contain All	${OUTPUT}	${ROOTFOLDER}
	[Teardown]	Close Connection

Configure AD Provider domain base dn, tls security and with search username, and with Global Catalog Server
	Skip If	'${NGVERSION}' < '4.0'	3.2 Official does not have this feature yet
	CLI:Open	startping=no	timeout=60s
	SUITE:Delete all LDAP Providers
	CLI:Enter Path	/settings/authentication/servers
	CLI:Add
	CLI:Write	set method=ldap_or_ad status=enabled remote_server=${AD_SERVER} ldap_ad_base=${AD_BASE_DOMAIN} ldap_ad_secure=${AD_SECURE} fallback_if_denied_access=yes ldap_ad_database_username=${AD_BROWSE_USER_DN} ldap_ad_database_password=${AD_BROWSE_PASSWORD} ldap_ad_group_attribute=${AD_GROUP_ATRRIB} ldap_ad_login_attribute=${AD_USER_ATRRIB} global_catalog_server=yes
	CLI:Commit
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	ldap or ad	ignore_case=True
	Should Contain	${OUTPUT}	${AD_SERVER}
	${OUTPUT}=	CLI:Enter Path	/settings/authentication/servers/1
	CLI:Test Show Command	method = ldap_or_ad	status = enabled	fallback_if_denied_access = yes
	...	remote_server = 192.168.2.227	ldap_ad_secure = start_tls	ldap_ad_login_attribute = sAMAccountName
	...	ldap_ad_database_username = zpe@zpesystems.local	ldap_ad_group_attribute = memberOf	global_catalog_server = yes
	Close Connection

Valid AD User authentication with AD Browsing and with Global Catalog Server
	Skip If	'${NGVERSION}' < '4.0'	3.2 Official does not have this feature yet
	${VALIDATE_LDAP_USER_AUTHENTICATION}=	Set Variable If	'${VALIDATE_LDAP_USER_AUTHENTICATION}'=='None'	True	${VALIDATE_LDAP_USER_AUTHENTICATION}
	Skip If	${VALIDATE_LDAP_USER_AUTHENTICATION} == False	LDAP Tests Disabled

	CLI:Open	${AD_BROWSE_USER}	${AD_BROWSE_PASSWORD}	startping=no	timeout=60s
	${OUTPUT}=	CLI:Write	whoami
	Should Contain	${OUTPUT}	${AD_BROWSE_USER}
	Close Connection

Delete all LDAP Providers
	CLI:Open	startping=no	timeout=60s
	SUITE:Delete all LDAP Providers
	Close Connection

*** Keywords ***
SUITE:Setup
	Set Tags	NON-CRITICAL
#	CLIENT:Start Target Ping
	SUITE:Ping AD Server
	Remove Tags	NON-CRITICAL
	CLI:Open	startping=no
	${IS_VM}	CLI:Is VM System
	Skip If	${IS_VM}	The current system cannot authenticate new users in less then 30s due to low processing power
	SUITE:Delete all LDAP Providers

SUITE:Delete all LDAP Providers
	CLI:Enter Path	/settings/authentication/servers
	${Output}=	CLI:Show
	${LDAPS}=	Get Lines Containing String	${Output}	ldap or ad	case_insensitive=True
	@{LINES}=	Split to Lines	${LDAPS}
	FOR		${LINE}	IN	@{Lines}
		${OutputText}=	CLI:Show
		${LDAPLINES}=	Get Lines Containing String	${OutputText}	ldap or ad	case_insensitive=True
		@{Liness}=	Split to Lines	${LDAPLINES}
		${Lines}=	Replace String Using Regexp	${Liness}[0]	\\s\\s*	--
		@{Details}=	Split String	${Lines}	--
		CLI:Write	delete ${Details}[1]
	END
	CLI:Commit

SUITE:Ping AD Server
	${OS}=	CLIENT:Get Operating System
	Log	Client OS:\n ${OS}	INFO	console=yes
	Run Keyword If	'macOS' in '''${OS}'''	SUITE:Start Target Ping MacOS
	Run Keyword If	'Linux' in '''${OS}'''	SUITE:Start Target Ping Linux
	Run Keyword If	'Microsoft' in '''${OS}'''	SUITE:Start Target Ping Windows

SUITE:Start Target Ping MacOS
	${result}=	Run Process	ping	-c 5	${AD_SERVER}
	Log	AD Server Response file:\n ${result.stdout}	INFO	console=yes
	Should Not Contain	${result.stdout}	100.0% packet loss
#	${AD_PID}=	Start Process	ping	-c 5	${LDAPSERVER}	stdout=ad_out.log
#	Sleep	5s
#	Terminate Process	handle=${AD_PID}	kill=true
#	${result_file} =	OperatingSystem.Get File	ad_out.log
#	Log	AD Server Response file:\n ${result_file}	INFO	console=yes
#	Should Not Contain	${result_file}	100.0% packet loss

SUITE:Start Target Ping Windows
	${result}=	Run Process	ping	${AD_SERVER}
	Log	AD Server Response file:\n ${result.stdout}	INFO	console=yes
	Should Not Contain	${result.stdout}	Destination host unreachable.

SUITE:Start Target Ping Linux
	${result}=	Run Process	ping	-c 5	${AD_SERVER}
	Log	AD Server Response file:\n ${result.stdout}	INFO	console=yes
	Should Not Contain	${result.stdout}	100.0% packet loss

SUITE:Check Serial Ports Card
	CLI:Enter Path	/settings/slots/
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	Serial Rolled Expansion Card