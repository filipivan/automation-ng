*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authentication Console... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${REMOTE_SERVER}	1.1.1.1
${METHOD}	kerberos

*** Test Cases ***
Create Kerberos Server
	CLI:Open	startping=no
	SUITE:Delete All Kerberos Providers
	CLI:Enter Path	/settings/authentication/servers
	CLI:Add
	CLI:Write	set method=kerberos status=enabled remote_server=184.173.23.21
	CLI:Commit
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	kerberos	ignore_case=True
	### BEGIN - Test created because of a bug that is in trello #718, link https://trello.com/c/XOGcEi5I
	${OUTPUT}=	CLI:Enter Path	/settings/authentication/servers/1
	Should Not Contain	${OUTPUT}	Json-CRITICAL
	CLI:Enter Path	/settings/authentication/servers/
	### END - Test created because of a bug that is in trello #718, link https://trello.com/c/XOGcEi5I

Test status field
	CLI:Enter Path	/settings/authentication/servers/1
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Normal Fields	status	enabled	disabled
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Normal Fields	status	enabled	disabled

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	status	${EMPTY}	Error: Missing value for parameter: status
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	status	${EMPTY}	Error: Missing value: status

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	status	${WORD}	Error: Invalid value: ${WORD} for parameter: status
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	status	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	status	${POINTS}	Error: Invalid value: ${POINTS} for parameter: status
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	status	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Revert

Test fallback_if_denied_access field
	CLI:Enter Path	/settings/authentication/servers/1
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Normal Fields	fallback_if_denied_access	yes	no
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Normal Fields	fallback_if_denied_access	yes	no

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${EMPTY}	Error: Missing value for parameter: fallback_if_denied_access
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${EMPTY}	Error: Missing value: fallback_if_denied_access

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${WORD}	Error: Invalid value: ${WORD} for parameter: fallback_if_denied_access
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${POINTS}	Error: Invalid value: ${POINTS} for parameter: fallback_if_denied_access
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Revert

Test remote_server field with invalid values
	Skip If	'${NGVERSION}' < '4.0'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/1
	CLI:Test Set Validate Invalid Options	remote_server	${EMPTY}	Error: remote_server: Invalid IP address or host name.
	CLI:Test Set Validate Invalid Options	remote_server	${WORD}
	CLI:Test Set Validate Invalid Options	remote_server	${NUMBER}
	CLI:Test Set Validate Invalid Options	remote_server	${POINTS}	Error: remote_server: Invalid IP address or host name.
	CLI:Test Set Validate Invalid Options	remote_server	${ONE_WORD}
	CLI:Test Set Validate Invalid Options	remote_server	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	remote_server	${ONE_POINTS}	Error: remote_server: Invalid IP address or host name.
	CLI:Test Set Validate Invalid Options	remote_server	${EXCEEDED}	Error: remote_server: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	remote_server	${REMOTE_SERVER}
	[Teardown]	CLI:Revert

Test editing kerberos_realm_domain_name field
	CLI:Enter Path	/settings/authentication/servers/1
	CLI:Test Set Validate Invalid Options	kerberos_realm_domain_name	${EMPTY}
	CLI:Test Set Validate Invalid Options	kerberos_realm_domain_name	${WORD}
	CLI:Test Set Validate Invalid Options	kerberos_realm_domain_name	${NUMBER}
	CLI:Test Set Validate Invalid Options	kerberos_realm_domain_name	${POINTS}
	CLI:Test Set Validate Invalid Options	kerberos_realm_domain_name	${ONE_WORD}
	CLI:Test Set Validate Invalid Options	kerberos_realm_domain_name	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	kerberos_realm_domain_name	${ONE_POINTS}
	CLI:Test Set Validate Invalid Options	kerberos_realm_domain_name	${EXCEEDED}
	[Teardown]	CLI:Revert

Test editing kerberos_domain_name field
	CLI:Enter Path	/settings/authentication/servers/1
	CLI:Test Set Validate Invalid Options	kerberos_domain_name	${EMPTY}
	CLI:Test Set Validate Invalid Options	kerberos_domain_name	${WORD}
	CLI:Test Set Validate Invalid Options	kerberos_domain_name	${NUMBER}
	CLI:Test Set Validate Invalid Options	kerberos_domain_name	${POINTS}
	CLI:Test Set Validate Invalid Options	kerberos_domain_name	${ONE_WORD}
	CLI:Test Set Validate Invalid Options	kerberos_domain_name	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	kerberos_domain_name	${ONE_POINTS}
	CLI:Test Set Validate Invalid Options	kerberos_domain_name	${EXCEEDED}
	[Teardown]	CLI:Revert

Test kerberos_realm_domain_name field
	CLI:Enter Path	/settings/authentication/servers/
	@{LIST}=	Create List	${EMPTY}	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${EXCEEDED}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Write	set method=${METHOD}
		CLI:Write	set remote_server=${REMOTE_SERVER}
		CLI:Test Set Field Invalid Options	kerberos_realm_domain_name	${VALUE}	save=yes
		CLI:Delete If Exists	1
	END
	[Teardown]	CLI:Cancel	Raw

Test kerberos_domain_name field
	CLI:Enter Path	/settings/authentication/servers/
	@{LIST}=	Create List	${EMPTY}	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${EXCEEDED}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Write	set method=${METHOD}
		CLI:Write	set remote_server=${REMOTE_SERVER}
		CLI:Test Set Field Invalid Options	kerberos_realm_domain_name	${VALUE}	save=yes
		CLI:Delete If Exists	1
	END
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Delete all Kerberos Providers

SUITE:Teardown
	SUITE:Delete all Kerberos Providers
	CLI:Close Connection

SUITE:Delete All Kerberos Providers
	CLI:Enter Path	/settings/authentication/servers
	${Output}=	CLI:Show
	${KERBEROS}=	Get Lines Containing String	${Output}	kerberos	case_insensitive=True
	@{LINES}=	Split to Lines	${KERBEROS}
	FOR		${LINE}	IN	@{Lines}
		${OUTPUTTEXT}=	CLI:Show
		${KERBEROSLINES}=	Get Lines Containing String	${OUTPUTTEXT}	kerberos	case_insensitive=True
		@{Liness}=	Split to Lines	${KERBEROSLINES}
		${Lines}=	Replace String Using Regexp	${Liness}[0]	\\s\\s*	--
		@{Details}=	Split String	${Lines}	--
		CLI:Delete	${Details}[1]
	END
	CLI:Commit