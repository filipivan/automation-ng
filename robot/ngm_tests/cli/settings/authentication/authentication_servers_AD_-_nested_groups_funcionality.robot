*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authentication Servers > AD > Nested Groups feature ... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	NON-CRITICAL	NEED-REVIEW
Default Tags	CLI	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

#Structure:
#	Nested-TestGroup-Level1	- testuser
#	- Nested-TestGroup-Level2	- testuser2
#	- - Nested-TestGroup-Level3	- testuser3
#	- - - Nested-TestGroup-Level4
#	- - - - Nested-TestGroup-Level5	- testuser5, QATestGroup
#
#	If permissions are changed on Nested-TestGroup-Level1, all of the groups below will also be affected by the change.
#	If permissions are changed on Nested-TestGroup-Level5, all of the groups above will not be affected by the change.

*** Variables ***
${AD_SECURE}	on
${TEST_NESTED_GROUP}	TestNestedGroup
${DEVICE_NAME}	dummy_device_nested_groups

*** Test Cases ***
Configure AD Provider
	CLI:Enter Path	/settings/authentication/servers
	CLI:Add
	CLI:Set	method=ldap_or_ad status=enabled remote_server=${AD_SERVER} ldap_ad_base=${AD_BASE_DOMAIN} ldap_ad_secure=${AD_SECURE}
	CLI:Set	ldap_ad_database_username=${AD_BROWSE_USER_DN} ldap_ad_database_password=${AD_BROWSE_PASSWORD} fallback_if_denied_access=yes
	CLI:Set	ldap_ad_group_attribute=${AD_GROUP_ATRRIB} ldap_ad_login_attribute=${ADUSERATRRIB} search_nested_groups=yes group_base=${AD_GROUP_BASE}
	CLI:Commit
	CLI:Test Show Command	${AD_SERVER}

Set up device permissions for group Nested-TestGroup-Level5
	CLI:Enter Path	/settings/authorization/
	CLI:Delete If Exists Confirm	${TEST_NESTED_GROUP}
	CLI:Add
	CLI:Set Field	name	${TEST_NESTED_GROUP}
	CLI:Commit
	CLI:Test Show Command	${TEST_NESTED_GROUP}

	#	Permissions are only going to be changed in Nested-TestGroup-Level5
	CLI:Enter Path	/settings/authorization/${TEST_NESTED_GROUP}/remote_groups
	CLI:Set Field	remote_groups	Nested-TestGroup-Level5
	CLI:Commit
	CLI:Test Show Command	remote_groups = Nested-TestGroup-Level5

	CLI:Add Device	${DEVICE_NAME}	device_console
	CLI:Enter Path	/settings/authorization/${TEST_NESTED_GROUP}/devices
	CLI:Add
	CLI:Set Field	devices	${DEVICE_NAME}
	Write Bare	commit\n
	Run Keyword If	'${NGVERSION}' >= '4.0'	Read Until	:
	Run Keyword If	'${NGVERSION}' >= '4.0'	Write Bare	yes\n
	CLI:Read Until Prompt

	CLI:Enter Path	/settings/services
	CLI:Set Field	device_access_per_user_group_authorization	yes
	CLI:Commit

Test device permission for member of group Nested-TestGroup-Level3
	Set Tags	NON-CRITICAL
	CLI:Open	${AD_ADMIN_USER}	${AD_ADMIN_PASSWORD}	startping=no	timeout=60s
	CLI:Enter Path	/access/
	CLI:Test Not Show Command	${DEVICE_NAME}
	Close Connection

Test device permission for member of group Nested-TestGroup-Level5
	CLI:Open	testuser5	Passw0rd	startping=no	timeout=60s
	CLI:Enter Path	/access/
	CLI:Test Show Command	${DEVICE_NAME}
	Close Connection

#Should have device permissions
Test device permission for member of nested group in group Nested-TestGroup-Level5
	CLI:Open	QAuser1	Passw0rd	startping=no	timeout=60s
	CLI:Enter Path	/access/
	CLI:Test Show Command	${DEVICE_NAME}
	Close Connection

#If remote_group is removed, all members of Nested-TestGroup-Level[x] should not be able to see device from group
Test removal of permissions for remote_group Nested-TestGroup-Level5
	CLI:Open	timeout=60s
	CLI:Enter Path	/settings/authorization/${TEST_NESTED_GROUP}/remote_groups
	CLI:Set Field	remote_groups	${EMPTY}
	CLI:Commit
	CLI:Test Not Show Command	remote_groups = Nested-TestGroup-Level

Test (no) device permission for member of group Nested-TestGroup-Level5
	CLI:Open	testuser5	Passw0rd	startping=no	timeout=60s
	CLI:Enter Path	/access/
	CLI:Test Not Show Command	${DEVICE_NAME}
	Close Connection

Test (no) device permission for member of nested group in group Nested-TestGroup-Level5
	CLI:Open	QAuser1	Passw0rd	startping=no	timeout=60s
	CLI:Enter Path	/access/
	CLI:Test Not Show Command	${DEVICE_NAME}
	Close Connection

##Should not have device permission
Test (no) device permission for member of group Nested-TestGroup-Level3
	Set Tags	NON-CRITICAL
	CLI:Open	${AD_ADMIN_USER}	${AD_ADMIN_PASSWORD}	startping=no	timeout=60s
	CLI:Enter Path	/access/
	CLI:Test Not Show Command	${DEVICE_NAME}
	Close Connection

#		#
#	Test cases below: Testing authorization permissions in group profile	#
#		#

Add group permissions for Nested-TestGroup-Level5: track_system=yes
	CLI:Open	timeout=60s
	CLI:Enter Path	/settings/authorization/${TEST_NESTED_GROUP}/profile
	CLI:Set Field	track_system_information	yes
	CLI:Commit

	CLI:Enter Path	/settings/authorization/${TEST_NESTED_GROUP}/remote_groups
	CLI:Set Field	remote_groups	Nested-TestGroup-Level5
	CLI:Commit
	CLI:Test Show Command	remote_groups = Nested-TestGroup-Level5
	Close Connection

Test permission for member of Nested-TestGroup-Level5: track_system=yes
	CLI:Open	testuser5	Passw0rd	startping=no	timeout=60s
	CLI:Enter Path	/system/
	CLI:Test Ls Command	event_list	routing_table	system_usage	discovery_logs
	...	lldp	network_statistics	usb_devices
	Close Connection

Test permission for member of group in nested group Nested-TestGroup-Level5: track_system=yes
	CLI:Open	QAuser1	Passw0rd	startping=no	timeout=60s
	CLI:Enter Path	/system/
	CLI:Test Ls Command	event_list	routing_table	system_usage	discovery_logs
	...	lldp	network_statistics	usb_devices
	Close Connection

#Should not have track system permission
Test permission for member of group Nested-TestGroup-Level3: track_system=yes
	CLI:Open	testuser3	Passw0rd	startping=no	timeout=60s
	CLI:Enter Path	/system/
	${STATUS}=	Run Keyword And Return Status	CLI:Test Ls Command	event_list routing_table	system_usage	discovery_logs
	Run Keyword If	${STATUS}	Fail	Should Not Contain System Track Information
	Close Connection

Add group permissions for Nested-TestGroup-Level5: track_system=no
	CLI:Open	timeout=60s
	CLI:Enter Path	/settings/authorization/${TEST_NESTED_GROUP}/remote_groups
	CLI:Set Field	remote_groups	Nested-TestGroup-Level5
	CLI:Commit
	CLI:Test Show Command	remote_groups = Nested-TestGroup-Level5
	Close Connection

Test permission for member of Nested-TestGroup-Level5: track_system=no
	CLI:Open	testuser5	Passw0rd	startping=no	timeout=60s
	CLI:Enter Path	system
	${STATUS}=	Run Keyword And Return Status	CLI:Test Ls Command	event_list routing_table	system_usage	discovery_logs
	Run Keyword If	${STATUS}	Fail	Should Not Contain System Track Information
	Close Connection

Test permission for member of group in nested group Nested-TestGroup-Level5: track_system=no
	CLI:Open	QAuser1	Passw0rd	startping=no	timeout=60s
	CLI:Enter Path	system
	${STATUS}=	Run Keyword And Return Status	CLI:Test Ls Command	event_list routing_table	system_usage	discovery_logs
	Run Keyword If	${STATUS}	Fail	Should Not Contain System Track Information
	Close Connection

Add group permissions for Nested-TestGroup-Level3: track_system=yes
	CLI:Open	timeout=60s
	CLI:Enter Path	/settings/authorization/${TEST_NESTED_GROUP}/profile
	CLI:Set Field	track_system_information	yes
	CLI:Commit

	CLI:Enter Path	/settings/authorization/${TEST_NESTED_GROUP}/remote_groups
	CLI:Set Field	remote_groups	Nested-TestGroup-Level3
	CLI:Commit
	CLI:Test Show Command	remote_groups = Nested-TestGroup-Level3
	Close Connection

#Should have track system permission
Test permission for member of group Nested-TestGroup-Level3: track_system=yes (remote_group:Lv3)
	CLI:Open	testuser3	Passw0rd	startping=no	timeout=60s
	CLI:Enter Path	/system/
	CLI:Test Ls Command	event_list	routing_table	system_usage	discovery_logs
	...	lldp	network_statistics	usb_devices
	Close Connection

#Should also have track system permission
Test permission for member of group in nested group Nested-TestGroup-Level5: track_system=yes (remote_group:Lv3)
	CLI:Open	QAuser1	Passw0rd	startping=no	timeout=60s
	CLI:Enter Path	/system/
	CLI:Test Ls Command	event_list	routing_table	system_usage	discovery_logs
	...	lldp	network_statistics	usb_devices
	Close Connection

*** Keywords ***
SUITE:Setup
	CLI:Open
	${IS_VM}	CLI:Is VM System
	Skip If	${IS_VM}	The current system cannot authenticate new users in less then 30s due to low processing power
	CLI:Delete Devices	${DEVICE_NAME}
	SUITE:Delete all LDAP Providers

SUITE:Teardown
	CLI:Open	timeout=60s
	CLI:Delete Devices	${DEVICE_NAME}
	CLI:Enter Path	/settings/authorization/
	CLI:Delete If Exists Confirm	${TEST_NESTED_GROUP}
	CLI:Enter Path	/settings/services
	CLI:Set Field	device_access_per_user_group_authorization	yes
	CLI:Commit
	SUITE:Delete all LDAP Providers
	CLI:Close Connection

SUITE:Delete all LDAP Providers
	CLI:Enter Path	/settings/authentication/servers
	${OUTPUT}=	CLI:Show
	${LDAPS}=	Get Lines Containing String	${OUTPUT}	ldap or ad	case_insensitive=True
	@{LINES}=	Split to Lines	${LDAPS}
	FOR		${LINE}	IN	@{Lines}
		${OutputText}=	CLI:Show
		${LDAPLINES}=	Get Lines Containing String	${OutputText}	ldap or ad	case_insensitive=True
		@{Liness}=	Split to Lines	${LDAPLINES}
		${Lines}=	Replace String Using Regexp	${Liness}[0]	\\s\\s*	--
		@{Details}=	Split String	${Lines}	--
		CLI:Write	delete ${Details}[1]
		CLI:Commit
	END

