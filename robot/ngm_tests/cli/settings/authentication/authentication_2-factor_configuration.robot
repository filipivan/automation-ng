*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authentication Console... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${OTP_NAME}	test_OTP
${NAME}	rsa_method
${REST_URL}	https://rsa.zpesystems.com:5555/mfa/v1_1/authn
${CLIENT_KEY}	08wd3zndor551a518i65taze0fgh6o0t9q3mc5dn6xz5mqur9xw54826vi8xa962
${CLIENT_ID}	rsa.zpesystems.com
${READ_TIMEOUT}	120 	#	30 <= read_timeout <= 240
${CONNECT_TIMEOUT}	20	#	10 <= connect_timeout <= 120
${MAX_RETRIES}	3	#	1 <= max_retries <= 5
${REPLICAS}	rsa.zpesystems.com,192.168.2.229
${REPLICAS_PORTS}	rsa.zpesystems.com:4444,rsa.zpesystems.com:3333
${POLICY_ID}	low-mfa-policy
${TENANT_ID}	rsaready

*** Test Cases ***
Test method field
	SUITE:Create 2-factor Authentication	rsa
	CLI:Enter Path	/settings/authentication/2-factor/${NAME}
	IF	${NGVERSION} <= 5.6
		CLI:Test Set Validate Invalid Options	method	${EMPTY}	Error: Missing value for parameter: method
		CLI:Test Set Validate Invalid Options	method	${WORD}	Error: Invalid value: ${WORD} for parameter: method
		CLI:Test Set Validate Invalid Options	method	${POINTS}	Error: Invalid value: ${POINTS} for parameter: method
	ELSE
		CLI:Test Set Validate Invalid Options	method	${EMPTY}	Error: Invalid parameter name: method
	END
	[Teardown]	CLI:Revert

Test status field
	SUITE:Create 2-factor Authentication	rsa
	CLI:Enter Path	/settings/authentication/2-factor/${NAME}
	CLI:Test Set Validate Normal Fields	status	enabled	disabled
	CLI:Test Set Validate Invalid Options	status	${EMPTY}	Error: Missing value for parameter: status
	CLI:Test Set Validate Invalid Options	status	${WORD}	Error: Invalid value: ${WORD} for parameter: status
	CLI:Test Set Validate Invalid Options	status	${POINTS}	Error: Invalid value: ${POINTS} for parameter: status
	[Teardown]	CLI:Revert

Test editing rest_url field
	SUITE:Create 2-factor Authentication	rsa
	CLI:Enter Path	/settings/authentication/2-factor/${NAME}
	@{LIST}=	Create List	${WORD}	${NUMBER}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${REST_URL}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Test Set Validate Invalid Options	rest_url	${VALUE}
	END
	CLI:Test Set Validate Invalid Options	rest_url	${EXCEEDED}	Error: rest_url: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test editing replicas field with valid values
	SUITE:Create 2-factor Authentication	rsa
	CLI:Enter Path	/settings/authentication/2-factor/${NAME}
	@{LIST}=	Create List	${WORD}	${NUMBER}	${ONE_WORD}	${ONE_NUMBER}	${REPLICAS}	${REPLICAS_PORTS}
	CLI:Set Field  enable_replicas 	yes
	FOR		${VALUE}	IN	@{LIST}
		CLI:Set Field  replicas	${VALUE}
	END
	[Teardown]	CLI:Revert

Test editing tenant_id field with valid values
	SUITE:Create 2-factor Authentication	rsa
	CLI:Enter Path	/settings/authentication/2-factor/${NAME}
	@{LIST}=	Create List	${WORD}	${NUMBER}	${ONE_WORD}	${ONE_NUMBER}	${TENANT_ID}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Set	enable_cloud_authentication_service=yes policy_id=${POLICY_ID} tenant_id=${VALUE}
	END
	[Teardown]	CLI:Revert

Test editing policy_id field with valid values
	SUITE:Create 2-factor Authentication	rsa
	CLI:Enter Path	/settings/authentication/2-factor/${NAME}
	@{LIST}=	Create List	${WORD}	${NUMBER}	${ONE_WORD}	${ONE_NUMBER}	${POLICY_ID}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Set	enable_cloud_authentication_service=yes policy_id=${VALUE} tenant_id=${TENANT_ID}
	END
	[Teardown]	CLI:Revert

Test editing client_id field with valid values
	SUITE:Create 2-factor Authentication	rsa
	CLI:Enter Path	/settings/authentication/2-factor/${NAME}
	@{LIST}=	Create List	${WORD}	${NUMBER}	${ONE_WORD}	${ONE_NUMBER}	${CLIENT_ID}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Test Set Validate Invalid Options	client_id	${VALUE}
	END
	[Teardown]	CLI:Revert

Test client_id field with invalid values
	SUITE:Create 2-factor Authentication	rsa
	CLI:Enter Path	/settings/authentication/2-factor/${NAME}
	@{LIST}=	Create List	${EMPTY}	${POINTS}	${ONE_POINTS}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Test Set Validate Invalid Options	client_id	${VALUE}	Error: client_id: Validation error.
	END
	CLI:Test Set Validate Invalid Options	client_id	${EXCEEDED}	Error: client_id: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test editing read_timeout field with valid values
	SUITE:Create 2-factor Authentication	rsa
	CLI:Enter Path	/settings/authentication/2-factor/${NAME}
	CLI:Test Set Validate Invalid Options	read_timeout	${READ_TIMEOUT}
	[Teardown]	CLI:Revert

Test read_timeout field with invalid values
	SUITE:Create 2-factor Authentication	rsa
	CLI:Enter Path	/settings/authentication/2-factor/${NAME}
	@{LIST}=	Create List	${WORD}	${NUMBER}	${POINTS}	${EXCEEDED}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Test Set Validate Invalid Options	read_timeout	${VALUE}	Error: read_timeout: Exceeded the maximum size for this field.
	END
	CLI:Test Set Validate Invalid Options	read_timeout	${ONE_NUMBER}	Error: read_timeout: The value is out of range.
	[Teardown]	CLI:Revert

Test read_timeout field with invalid values 2
	SUITE:Create 2-factor Authentication	rsa
	CLI:Enter Path	/settings/authentication/2-factor/${NAME}
	@{LIST}=	Create List	${EMPTY}	${ONE_WORD}	${ONE_POINTS}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Test Set Validate Invalid Options	read_timeout	${VALUE}	Error: read_timeout: This field only accepts integers.
	END
	[Teardown]	CLI:Revert

Test editing connect_timeout field with valid values
	SUITE:Create 2-factor Authentication	rsa
	CLI:Enter Path	/settings/authentication/2-factor/${NAME}
	CLI:Test Set Validate Invalid Options	connect_timeout	${CONNECT_TIMEOUT}
	[Teardown]	CLI:Revert

Test connect_timeout field with invalid values
	SUITE:Create 2-factor Authentication	rsa
	CLI:Enter Path	/settings/authentication/2-factor/${NAME}
	@{LIST}=	Create List	${WORD}	${NUMBER}	${POINTS}	${EXCEEDED}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Test Set Validate Invalid Options	connect_timeout	${VALUE}	Error: connect_timeout: Exceeded the maximum size for this field.
	END
	CLI:Test Set Validate Invalid Options	connect_timeout	${ONE_NUMBER}	Error: connect_timeout: The value is out of range.
	[Teardown]	CLI:Revert

Test connect_timeout field with invalid values 2
	SUITE:Create 2-factor Authentication	rsa
	CLI:Enter Path	/settings/authentication/2-factor/${NAME}
	@{LIST}=	Create List	${EMPTY}	${ONE_WORD}	${ONE_POINTS}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Test Set Validate Invalid Options	connect_timeout	${VALUE}	Error: connect_timeout: This field only accepts integers.
	END
	[Teardown]	CLI:Revert

Test editing max_retries field with valid values
	SUITE:Create 2-factor Authentication	rsa
	CLI:Enter Path	/settings/authentication/2-factor/${NAME}
	@{LIST}=	Create List	${MAX_RETRIES}	${ONE_NUMBER}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Test Set Validate Invalid Options	max_retries	${VALUE}
	END
	[Teardown]	CLI:Revert

Test max_retries field with invalid values
	SUITE:Create 2-factor Authentication	rsa
	CLI:Enter Path	/settings/authentication/2-factor/${NAME}
	@{LIST}=	Create List	${WORD}	${NUMBER}	${POINTS}	${EXCEEDED}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Test Set Validate Invalid Options	max_retries	${VALUE}	Error: max_retries: Exceeded the maximum size for this field.
	END
	[Teardown]	CLI:Revert

Test max_retries field with invalid values 2
	SUITE:Create 2-factor Authentication	rsa
	CLI:Enter Path	/settings/authentication/2-factor/${NAME}
	@{LIST}=	Create List	${EMPTY}	${ONE_WORD}	${ONE_POINTS}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Test Set Validate Invalid Options	max_retries	${VALUE}	Error: max_retries: This field only accepts integers.
	END
	[Teardown]	CLI:Revert

Test adding a 2-factor authentication
	SUITE:Delete all 2-factor Authentication
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Add
	CLI:Set	method=rsa
	CLI:Set	name=${NAME} client_key=${CLIENT_KEY} client_id=${CLIENT_ID} rest_url=${REST_URL}
	CLI:Test Show Command	method = rsa	status = disabled	rest_url = ${REST_URL}
	...	client_key = **************************************************************	client_id = ${CLIENT_ID}
	...	read_timeout = ${READ_TIMEOUT}	connect_timeout = ${CONNECT_TIMEOUT}	max_retries = ${MAX_RETRIES}
	CLI:Commit
	CLI:Test Show Command Regexp	\\s+${NAME}\\s+RSA\\s+Disabled
	[Teardown]	CLI:Delete If Exists	${NAME}

Test rest_url field
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Delete If Exists  ${NAME}
	@{LIST}=	Create List	${WORD}	${NUMBER}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${REST_URL}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Set	name=${NAME} client_key=${CLIENT_KEY} client_id=${CLIENT_ID} rest_url=${VALUE}
		CLI:Commit
		CLI:Delete If Exists	${NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test client_id field with valid values
	CLI:Enter Path	/settings/authentication/2-factor/
	@{LIST}=	Create List	${WORD}	${NUMBER}	${ONE_WORD}	${ONE_NUMBER}	${CLIENT_ID}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Set	method=rsa
		CLI:Set	name=${NAME} client_key=${CLIENT_KEY} rest_url=${REST_URL} client_id=${VALUE}
		CLI:Commit
		CLI:Delete If Exists	${NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test read_timeout field with valid values
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Add
	CLI:Set	method=rsa
	CLI:Set	name=${NAME} client_key=${CLIENT_KEY} client_id=${CLIENT_ID} rest_url=${REST_URL} read_timeout=${READ_TIMEOUT}
	CLI:Commit
	CLI:Delete	${NAME}
	[Teardown]	CLI:Cancel	Raw

Test connect_timeout field with valid values
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Add
	CLI:Set	method=rsa
	CLI:Set	name=${NAME} client_key=${CLIENT_KEY} client_id=${CLIENT_ID} rest_url=${REST_URL} connect_timeout=${CONNECT_TIMEOUT}
	CLI:Commit
	CLI:Delete	${NAME}
	[Teardown]	CLI:Cancel	Raw

Test max_retries field with valid values
	CLI:Enter Path	/settings/authentication/2-factor/
	@{LIST}=	Create List	${MAX_RETRIES}	${ONE_NUMBER}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Set	method=rsa
		CLI:Set	name=${NAME} client_key=${CLIENT_KEY} client_id=${CLIENT_ID} rest_url=${REST_URL} max_retries=${VALUE}
		CLI:Commit
		CLI:Delete	${NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Delete 2-factor Authentication
	CLI:Open
	SUITE:Delete all 2-factor Authentication

Test add duplicate 2-factor method
	[Documentation]	Test adds two equal 2-factor authentication method and expects an error message.
	[Tags]	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	NON-CRITICAL
	CLI:Enter Path	/settings/authentication/2-factor
	CLI:Add
	CLI:Set	name=${OTP_NAME} method=otp otp_type=time_based status=enabled
	CLI:Commit
	CLI:Enter Path	/settings/authentication/2-factor
	CLI:Add
	CLI:Set	name=${OTP_NAME} method=otp otp_type=time_based status=disabled
	${EXPECTED_ERROR}	CLI:Commit	Raw
	Should Contain	${EXPECTED_ERROR}	Error: name: Duplicated second factor name
	[Teardown]	CLI:Cancel	Raw

Test enabling more than one 2-factor method
	[Documentation]	Test adds a 2-factor method and sets it as enabled, but since there is a method already enabled,
	...	expects an error.
	[Tags]	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	NON-CRITICAL
	CLI:Enter Path	/settings/authentication/2-factor
	CLI:Add
	CLI:Set	name=${OTP_NAME}_2 method=otp otp_type=time_based status=enabled
	${EXPECTED_ERROR}	CLI:Commit	Raw
	Should Contain	${EXPECTED_ERROR}	Error: status: Only one method can be enabled at time
	[Teardown]	CLI:Cancel	Raw

Test enabled 2-factor methods are shown in local server
	[Documentation]	Test checks that 2-factor method can be select as authentication server if is enabled.
	[Tags]	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	NON-CRITICAL
	CLI:Enter Path	/settings/authentication/servers/1
	CLI:Set	apply_2-factor_auth_for_admin_and_root_users=no
	@{OPTIONS}=	Create List	none	${OTP_NAME}
	${2-FACTOR_OPTIONS}	CLI:Test Field Options	2-factor_authentication	@{OPTIONS}
	[Teardown]	CLI:Cancel	Raw

Test cannot disable 2-factor authentication that is used by authentication server
	[Documentation]	Test tries to disable a 2-factor method when is used by local authentication and expects error message.
	[Tags]	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	NON-CRITICAL
	CLI:Enter Path	/settings/authentication/servers/1
	CLI:Set	apply_2-factor_auth_for_admin_and_root_users=no
	CLI:Set	2-factor_authentication=${OTP_NAME}
	CLI:Commit
	CLI:Enter Path	/settings/authentication/2-factor/${OTP_NAME}
	${EXPECTED_ERROR}	CLI:Write	set status=disabled	Raw
	Should Contain	${EXPECTED_ERROR}	Error: status: This method is in use by an authentication server
	[Teardown]	CLI:Cancel	Raw

Test deleting 2-factor method used in local server
	[Documentation]	Test deletes 2-factor method when is in use by local authentication server and expects error message.
	[Tags]	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	NON-CRITICAL
	CLI:Enter Path	/settings/authentication/2-factor/
	${EXPECTED_ERROR}	CLI:Write	delete ${OTP_NAME}	Raw
	Should Contain	${EXPECTED_ERROR}	Error: This method is in use by an authentication server
	[Teardown]	CLI:Cancel	Raw

Test disabled 2-factor methods are not show in local server
	[Documentation]	Test disables 2-factor method and checks is not shown as an option in local server configuration.
	[Tags]	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	NON-CRITICAL
	CLI:Enter Path	/settings/authentication/servers/1
	CLI:Set	2-factor_authentication=none
	CLI:Set	apply_2-factor_auth_for_admin_and_root_users=no
	CLI:Commit
	CLI:Enter Path	/settings/authentication/2-factor/${OTP_NAME}
	CLI:Set	status=disabled
	CLI:Commit
	CLI:Enter Path	/settings/authentication/servers/1
	${OUTPUT}	CLI:Write Bare	set 2-factor_authentication=\t\t\n
	Should Contain	${OUTPUT}	none
	[Teardown]	CLI:Cancel	Raw

Test errors returned by reset and generate commands when 2-factor is disabled
	[Documentation]	Test tries executing commands related to OTP when there is no OTP method configured.
	[Tags]	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	NON-CRITICAL
	${OUTPUT}	CLI:Write	reset_otp_token	Raw
	Should Contain	${OUTPUT}	Error: OTP not available
	${OUTPUT}	CLI:Write	generate_otp_token	Raw
	Should Contain	${OUTPUT}	Error: OTP not available
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	SUITE:Delete all 2-factor Authentication
	CLI:Close Connection

SUITE:Delete all 2-factor Authentication
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Delete If Exists	${NAME}
	CLI:Delete If Exists	${OTP_NAME}

SUITE:Create 2-factor Authentication
	[Arguments]	${METHOD}
	CLI:Enter Path	/settings/authentication/2-factor/
	CLI:Delete If Exists	${NAME}
	CLI:Add
	CLI:Set	method=${METHOD}
	CLI:Set	name=${NAME} rest_url=${REST_URL} client_key=${CLIENT_KEY} client_id=${CLIENT_ID} enable_replicas=yes replicas=${REPLICAS}
	CLI:Commit