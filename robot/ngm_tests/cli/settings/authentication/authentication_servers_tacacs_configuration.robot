*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authentication Console... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	CLI:Open	startping=no
Suite Teardown	Close All Connections

*** Variables ***
${REMOTE_SERVER}	1.1.1.1
${METHOD}	tacacs+

*** Test Cases ***
Create Tacacs Server
	CLI:Open	startping=no
	Delete All TACACS+ Providers
	CLI:Enter Path	/settings/authentication/servers
	CLI:Add
	CLI:Write	set method=tacacs+ status=enabled remote_server=192.168.2.88 tacacs+_accounting_server=192.168.2.88 fallback_if_denied_access=yes tacacs+_secret=secret tacacs+_service=raccess tacacs+_version=v0_v1
	CLI:Commit
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	tacacs+	ignore_case=True
	### BEGIN - Test created because of a bug that is in trello #718, link https://trello.com/c/XOGcEi5I
	${TACACS}=	Get Lines Containing String	${Output}	tacacs+	case_insensitive=True
	${STRING}=	Split String	${TACACS}
	Set Suite Variable	${DIR}	${STRING}[0]
	${OUTPUT}=	CLI:Enter Path	/settings/authentication/servers/${DIR}
	Should Not Contain	${OUTPUT}	Json-CRITICAL
	CLI:Test Show Command	method = tacacs+	status = enabled	fallback_if_denied_access = yes
	...	remote_server = 192.168.2.88	tacacs+_accounting_server = 192.168.2.88
	...	tacacs+_service = raccess	tacacs+_timeout = 2	tacacs+_retries = 2	tacacs+_version = v0_v1
	...	tacacs+_enable_user-level = no
	### END - Test created because of a bug that is in trello #718, link https://trello.com/c/XOGcEi5I

Test fields for user-level
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	CLI:Test Not Show Command	tacacs+_user_level_1	tacacs+_user_level_2	tacacs+_user_level_3
	...	tacacs+_user_level_4	tacacs+_user_level_5	tacacs+_user_level_6	tacacs+_user_level_7
	...	tacacs+_user_level_8	tacacs+_user_level_9	tacacs+_user_level_10	tacacs+_user_level_11
	...	tacacs+_user_level_12	tacacs+_user_level_13	tacacs+_user_level_14	tacacs+_user_level_15
	CLI:Test Show Command	tacacs+_enable_user-level = no
	CLI:Set Field	tacacs+_enable_user-level	yes
	CLI:Commit
	CLI:Test Show Command	tacacs+_user_level_1	tacacs+_user_level_2	tacacs+_user_level_3
	...	tacacs+_user_level_4	tacacs+_user_level_5	tacacs+_user_level_6	tacacs+_user_level_7
	...	tacacs+_user_level_8	tacacs+_user_level_9	tacacs+_user_level_10	tacacs+_user_level_11
	...	tacacs+_user_level_12	tacacs+_user_level_13	tacacs+_user_level_14	tacacs+_user_level_15
	CLI:Write	set tacacs+_user_level_1=user1 tacacs+_user_level_2=user1 tacacs+_user_level_3=user1
	CLI:Write	set tacacs+_user_level_4=user1 tacacs+_user_level_5=user1 tacacs+_user_level_6=user1 tacacs+_user_level_7=user1
	CLI:Write	set tacacs+_user_level_8=user1 tacacs+_user_level_9=user1 tacacs+_user_level_10=user1 tacacs+_user_level_11=user1
	CLI:Write	set tacacs+_user_level_12=user1 tacacs+_user_level_13=user1 tacacs+_user_level_14=user1 tacacs+_user_level_15=user1
	CLI:Commit
	CLI:Test Show Command	tacacs+_user_level_1 = user1	tacacs+_user_level_2 = user1	tacacs+_user_level_3 = user1
	...	tacacs+_user_level_4 = user1	tacacs+_user_level_5 = user1	tacacs+_user_level_6 = user1	tacacs+_user_level_7 = user1
	...	tacacs+_user_level_8 = user1	tacacs+_user_level_9 = user1	tacacs+_user_level_10 = user1	tacacs+_user_level_11 = user1
	...	tacacs+_user_level_12 = user1	tacacs+_user_level_13 = user1	tacacs+_user_level_14 = user1	tacacs+_user_level_15 = user1
	CLI:Set Field	tacacs+_enable_user-level	no
	CLI:Test Show Command	tacacs+_enable_user-level = no

Test status field
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Normal Fields	status	enabled	disabled
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Normal Fields	status	enabled	disabled


	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	status	${EMPTY}	Error: Missing value for parameter: status
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	status	${EMPTY}	Error: Missing value: status


	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	status	${WORD}	Error: Invalid value: ${WORD} for parameter: status
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	status	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	status	${POINTS}	Error: Invalid value: ${POINTS} for parameter: status
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	status	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Revert

Test fallback_if_denied_access field
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Normal Fields	fallback_if_denied_access	yes	no
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Normal Fields	fallback_if_denied_access	yes	no

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${EMPTY}	Error: Missing value for parameter: fallback_if_denied_access
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${EMPTY}	Error: Missing value: fallback_if_denied_access

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${WORD}	Error: Invalid value: ${WORD} for parameter: fallback_if_denied_access
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${POINTS}	Error: Invalid value: ${POINTS} for parameter: fallback_if_denied_access
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	fallback_if_denied_access	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Revert

Test remote_server field
	Skip If	'${NGVERSION}' < '4.0'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	CLI:Test Set Validate Invalid Options	remote_server	${EMPTY}	Error: remote_server: Invalid IP address or host name.
	CLI:Test Set Validate Invalid Options	remote_server	${WORD}
	CLI:Test Set Validate Invalid Options	remote_server	${NUMBER}
	CLI:Test Set Validate Invalid Options	remote_server	${POINTS}	Error: remote_server: Invalid IP address or host name.
	CLI:Test Set Validate Invalid Options	remote_server	${ONE_WORD}
	CLI:Test Set Validate Invalid Options	remote_server	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	remote_server	${ONE_POINTS}	Error: remote_server: Invalid IP address or host name.
	CLI:Test Set Validate Invalid Options	remote_server	${EXCEEDED}	Error: remote_server: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	remote_server	${REMOTE_SERVER}
	[Teardown]	CLI:Revert

Test tacacs+_accounting_server field
	Skip If	'${NGVERSION}' < '4.0'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	CLI:Test Set Validate Invalid Options	tacacs+_accounting_server	${EMPTY}
	CLI:Test Set Validate Invalid Options	tacacs+_accounting_server	${WORD}
	CLI:Test Set Validate Invalid Options	tacacs+_accounting_server	${NUMBER}
	CLI:Test Set Validate Invalid Options	tacacs+_accounting_server	${POINTS}	Error: tacacs+_accounting_server: Invalid IP address or host name.
	CLI:Test Set Validate Invalid Options	tacacs+_accounting_server	${ONE_WORD}
	CLI:Test Set Validate Invalid Options	tacacs+_accounting_server	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	tacacs+_accounting_server	${ONE_POINTS}	Error: tacacs+_accounting_server: Invalid IP address or host name.
	CLI:Test Set Validate Invalid Options	tacacs+_accounting_server	${EXCEEDED}	Error: tacacs+_accounting_server: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test tacacs_port field
	Skip If	'${NGVERSION}' < '4.0'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	CLI:Test Set Validate Invalid Options	tacacs_port	${EMPTY}	Error: tacacs_port: This field only accepts integers.
	CLI:Test Set Validate Invalid Options	tacacs_port	${WORD}	Error: tacacs_port: This field only accepts integers.
	CLI:Test Set Validate Invalid Options	tacacs_port	${NUMBER}
	CLI:Test Set Validate Invalid Options	tacacs_port	${POINTS}	Error: tacacs_port: This field only accepts integers.
	CLI:Test Set Validate Invalid Options	tacacs_port	${ONE_WORD}	Error: tacacs_port: This field only accepts integers.
	CLI:Test Set Validate Invalid Options	tacacs_port	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	tacacs_port	${ONE_POINTS}	Error: tacacs_port: This field only accepts integers.
	CLI:Test Set Validate Invalid Options	tacacs_port	${EXCEEDED}	Error: tacacs_port: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test tacacs+_service field
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Normal Fields	tacacs+_service	ppp	raccess	shell
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Normal Fields	tacacs+_service	ppp	raccess	shell


	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tacacs+_service	${EMPTY}	Error: Missing value for parameter: tacacs+_service
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tacacs+_service	${EMPTY}	Error: Missing value: tacacs+_service

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tacacs+_service	${WORD}	Error: Invalid value: ${WORD} for parameter: tacacs+_service
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tacacs+_service	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tacacs+_service	${POINTS}	Error: Invalid value: ${POINTS} for parameter: tacacs+_service
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tacacs+_service	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Revert

Test tacacs+_timeout field
	Skip If	'${NGVERSION}' < '4.0'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	CLI:Test Set Validate Invalid Options	tacacs+_timeout	${EMPTY}	Error: tacacs+_timeout: This field only accepts integers.
	CLI:Test Set Validate Invalid Options	tacacs+_timeout	${WORD}	Error: tacacs+_timeout: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	tacacs+_timeout	${NUMBER}	Error: tacacs+_timeout: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	tacacs+_timeout	${POINTS}	Error: tacacs+_timeout: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	tacacs+_timeout	${ONE_WORD}	Error: tacacs+_timeout: This field only accepts integers.
	CLI:Test Set Validate Invalid Options	tacacs+_timeout	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	tacacs+_timeout	${ONE_POINTS}	Error: tacacs+_timeout: This field only accepts integers.
	CLI:Test Set Validate Invalid Options	tacacs+_timeout	${EXCEEDED}	Error: tacacs+_timeout: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test tacacs+_retries field
	Skip If	'${NGVERSION}' < '4.0'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	CLI:Test Set Validate Invalid Options	tacacs+_retries	${EMPTY}	Error: tacacs+_retries: This field only accepts integers.
	CLI:Test Set Validate Invalid Options	tacacs+_retries	${WORD}	Error: tacacs+_retries: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	tacacs+_retries	${NUMBER}	Error: tacacs+_retries: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	tacacs+_retries	${POINTS}	Error: tacacs+_retries: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	tacacs+_retries	${ONE_WORD}	Error: tacacs+_retries: This field only accepts integers.
	CLI:Test Set Validate Invalid Options	tacacs+_retries	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	tacacs+_retries	${ONE_POINTS}	Error: tacacs+_retries: This field only accepts integers.
	CLI:Test Set Validate Invalid Options	tacacs+_retries	${EXCEEDED}	Error: tacacs+_retries: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test tacacs+_version field
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Normal Fields	tacacs+_version	v0	v0_v1	v1	v1_v0
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Normal Fields	tacacs+_version	v0	v0_v1	v1	v1_v0

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tacacs+_version	${EMPTY}	Error: Missing value for parameter: tacacs+_version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tacacs+_version	${EMPTY}	Error: Missing value: tacacs+_version

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tacacs+_version	${WORD}	Error: Invalid value: ${WORD} for parameter: tacacs+_version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tacacs+_version	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tacacs+_version	${POINTS}	Error: Invalid value: ${POINTS} for parameter: tacacs+_version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tacacs+_version	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Revert

Test tacacs+_enable_user-level field
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Normal Fields	tacacs+_enable_user-level	yes	no
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Normal Fields	tacacs+_enable_user-level	yes	no

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tacacs+_enable_user-level	${EMPTY}	Error: Missing value for parameter: tacacs+_enable_user-level
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tacacs+_enable_user-level	${EMPTY}	Error: Missing value: tacacs+_enable_user-level

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tacacs+_enable_user-level	${WORD}	Error: Invalid value: ${WORD} for parameter: tacacs+_enable_user-level
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tacacs+_enable_user-level	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tacacs+_enable_user-level	${POINTS}	Error: Invalid value: ${POINTS} for parameter: tacacs+_enable_user-level
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tacacs+_enable_user-level	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Revert

Test tacacs+_user_level fields
	Skip If	'${NGVERSION}' < '4.0'	Will not be fix to 3.2
	CLI:Enter Path	/settings/authentication/servers/${DIR}
	CLI:Write	set tacacs+_enable_user-level=yes
	CLI:Show
	FOR		${INDEX}	IN RANGE	1	15
		CLI:Test Set Validate Invalid Options	tacacs+_user_level_${INDEX}	${EMPTY}
		CLI:Test Set Validate Invalid Options	tacacs+_user_level_${INDEX}	${WORD}
		CLI:Test Set Validate Invalid Options	tacacs+_user_level_${INDEX}	${NUMBER}	Error: tacacs+_user_level_${INDEX}: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
		CLI:Test Set Validate Invalid Options	tacacs+_user_level_${INDEX}	${POINTS}	Error: tacacs+_user_level_${INDEX}: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
		CLI:Test Set Validate Invalid Options	tacacs+_user_level_${INDEX}	${ONE_WORD}
		CLI:Test Set Validate Invalid Options	tacacs+_user_level_${INDEX}	${ONE_NUMBER}	Error: tacacs+_user_level_${INDEX}: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
		CLI:Test Set Validate Invalid Options	tacacs+_user_level_${INDEX}	${ONE_POINTS}	Error: tacacs+_user_level_${INDEX}: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _
		CLI:Test Set Validate Invalid Options	tacacs+_user_level_${INDEX}	${EXCEEDED}	Error: tacacs+_user_level_${INDEX}: Exceeded the maximum size for this field.
		CLI:Set Field	tacacs+_user_level_${INDEX}	${EMPTY}
	END
	[Teardown]	CLI:Revert

Test remote_server field with valid values
	Delete All TACACS+ Providers
	CLI:Enter Path	/settings/authentication/servers/
	@{LIST}=	Create List	${WORD}	${NUMBER}	${ONE_WORD}	${ONE_NUMBER}	${REMOTE_SERVER}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Write	set method=${METHOD}
		CLI:Test Set Field Invalid Options	remote_server	${VALUE}	save=yes
		CLI:Delete If Exists	1
	END
	[Teardown]	CLI:Cancel	Raw

Test tacacs+_accounting_server field with valid values
	Delete All TACACS+ Providers
	CLI:Enter Path	/settings/authentication/servers/
	@{LIST}=	Create List	${EMPTY}	${WORD}	${NUMBER}	${ONE_WORD}	${ONE_NUMBER}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Write	set method=${METHOD}
		CLI:Write	set remote_server=${REMOTE_SERVER}
		CLI:Test Set Field Invalid Options	tacacs+_accounting_server	${VALUE}	save=yes
		CLI:Delete If Exists	1
	END
	[Teardown]	CLI:Cancel	Raw

Test tacacs_port field with valid values
	Delete All TACACS+ Providers
	CLI:Enter Path	/settings/authentication/servers/
	@{LIST}=	Create List	${NUMBER}	${ONE_NUMBER}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Write	set method=${METHOD}
		CLI:Write	set remote_server=${REMOTE_SERVER}
		CLI:Test Set Field Invalid Options	tacacs_port	${VALUE}	save=yes
		CLI:Delete If Exists	1
	END
	[Teardown]	CLI:Cancel	Raw

Test tacacs+_timeout field with valid values
	Delete All TACACS+ Providers
	CLI:Enter Path	/settings/authentication/servers/
	@{LIST}=	Create List	${ONE_NUMBER}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Write	set method=${METHOD}
		CLI:Write	set remote_server=${REMOTE_SERVER}
		CLI:Test Set Field Invalid Options	tacacs+_timeout	${VALUE}	save=yes
		CLI:Delete If Exists	1
	END
	[Teardown]	CLI:Cancel	Raw

Test tacacs+_retries field with valid values
	Delete All TACACS+ Providers
	CLI:Enter Path	/settings/authentication/servers/
	@{LIST}=	Create List	${ONE_NUMBER}
	FOR		${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Write	set method=${METHOD}
		CLI:Write	set remote_server=${REMOTE_SERVER}
		CLI:Test Set Field Invalid Options	tacacs+_retries	${VALUE}	save=yes
		CLI:Delete If Exists	1
	END
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
Delete All TACACS+ Providers
	CLI:Enter Path	/settings/authentication/servers
	${Output}=	CLI:Show
	${TACACS}=	Get Lines Containing String	${Output}	TACACS+	case_insensitive=True
	@{LINES}=	Split to Lines	${TACACS}
	FOR		${LINE}	IN	@{Lines}
		${OutputText}=	CLI:Show
		${TACACSLINES}=	Get Lines Containing String	${OutputText}	TACACS+	case_insensitive=True
		@{Liness}=	Split to Lines	${TACACSLINES}
		${Lines}=	Replace String Using Regexp	${Liness}[0]	\\s\\s*	--
		@{Details}=	Split String	${Lines}	--
		CLI:Delete	${Details}[1]
	END