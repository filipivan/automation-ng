*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authentication radius servers functionality... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${REMOTE_SERVER}	192.168.3.8
${METHOD}	radius
${ACCOUNTING_SERVER}	user
${SECRET}	secret

*** Test Cases ***
Create a RADIUS server
	CLI:Enter Path	/settings/authentication/servers/
	CLI:Add
	CLI:Set	method=${METHOD} remote_server=${REMOTE_SERVER} radius_accounting_server=${REMOTE_SERVER} radius_secret=${SECRET} status=enabled fallback_if_denied_access=yes radius_enable_servicetype=no
	CLI:Commit

Valid RADIUS User authentication
	CLI:Open	radius1	radius1	startping=no
	${OUTPUT}=	CLI:Write	whoami
	Should Contain	${OUTPUT}	radius1
	[Teardown]  Close Connection

Invalid RADIUS User authentication
	Log to Console	Invalid RADIUS user Authentication
	${OUTPUT}=	Run Keyword And Expect Error	Authentication failed for user 'invaliduser'.
	...	CLI:Open	invaliduser	invaliduser	startping=no
	Log to Console	${OUTPUT}
	[Teardown]  Close Connection

Test local Failback
	Log to Console	local admin authentication is allowed due to failback
	Log to Console	Valid RADIUS User authentication
	CLI:Open	startping=no
	${OUTPUT}=	CLI:Write	whoami
	Should Contain	${OUTPUT}	${DEFAULT_USERNAME}
	[Teardown]  Close Connection

Valid RADIUS User Group authentication without Group
	${ALL_PATHS}=	Create List	toolkit/	cluster_peers/	open_sessions/	device_sessions/	event_list/
	...	routing_table/	system_usage/	discovery_logs/	lldp/	ipsec_table/	wireguard/
	...	hotspot/	qos/	dhcp/	network_statistics/	usb_devices/	bluetooth/	scheduler_logs/
	...	hw_monitor/	zpe_cloud/
	CLI:Open	radUser1	radUser1	startping=no
	${OUTPUT_WRITE}=	CLI:Write	whoami
	CLI:Enter Path	/system
	${OUTPUT_LS}=	CLI:Ls
	${STATUS1}=	Run Keyword And Return Status	Should Contain	${OUTPUT_LS}	about/
	${STATUS2}=	Run Keyword And Return Status	CLI:Should Not Contain All	about/	${ALL_PATHS}
	Run Keyword If	not ${STATUS1}	Fail	Should Contain About
	Run Keyword If	not ${STATUS2}	Fail	Should Not Contain System Track Information
	Should Contain	${OUTPUT_WRITE}	radUser1
	[Teardown]  Close Connection

Add RADIUS Group
	CLI:Open
	CLI:Enter Path	/settings/authorization/
	CLI:Delete If Exists Confirm	radGroup1
	CLI:Add
	CLI:Set	name=radGroup1
	CLI:Commit
	CLI:Enter Path	/settings/authorization/radGroup1/profile
	CLI:Set	track_system_information=yes
	CLI:Commit
	[Teardown]  Close Connection

Valid RADIUS User Group authentication
	${ALL_PATHS}=	Create List	toolkit/	cluster_peers/	open_sessions/	device_sessions/	event_list/
	...	routing_table/	system_usage/	discovery_logs/	lldp/	ipsec_table/	wireguard/
	...	hotspot/	qos/	dhcp/	network_statistics/	usb_devices/	bluetooth/	scheduler_logs/
	...	hw_monitor/	zpe_cloud/
	CLI:Open	radUser1	radUser1	startping=no
	${OUTPUT_WRITE}=	CLI:Write	whoami
	CLI:Enter Path	/system
	${OUTPUT_LS}=	CLI:Ls
	${STATUS1}=	Run Keyword And Return Status	Should Contain	${OUTPUT_LS}	about/
	${STATUS2}=	Run Keyword And Return Status	CLI:Should Not Contain All	about/	${ALL_PATHS}
	Run Keyword If	not ${STATUS1}	Fail	Should Contain About
	Run Keyword If	 not ${STATUS2}	Fail	Should Not Contain System Track Information
	Should Contain	${OUTPUT_WRITE}	radUser1
	[Teardown]  Close Connection

#						#
#	Radius VSA tests	#
#						#
Add RADIUS non-VSA Group
	CLI:Open
	CLI:Enter Path	/settings/authorization/
	CLI:Delete If Exists Confirm	filter-grp1
	CLI:Add
	CLI:Set	name=filter-grp1
	CLI:Commit
	CLI:Enter Path	/settings/authorization/filter-grp1/profile
	CLI:Set	track_system_information=yes
	CLI:Commit
	[Teardown]  Close Connection

Add RADIUS VSA Group
	CLI:Open
	CLI:Enter Path  /settings/authorization/
	CLI:Delete If Exists Confirm  vsa-grp1
	CLI:Add
	CLI:Set	name=vsa-grp1
	CLI:Commit
	CLI:Enter Path	/settings/authorization/vsa-grp1/profile
	CLI:Set	software_upgrade_and_reboot_system=yes
	CLI:Commit
	[Teardown]  Close Connection

Valid RADIUS User VSA Group authentication
	[Tags]	EXCLUDEIN3_2
	${ALL_PATHS}=	Create List	toolkit/	cluster_peers/	open_sessions/	device_sessions/	event_list/
	...	routing_table/	system_usage/	discovery_logs/	lldp/	ipsec_table/	wireguard/
	...	hotspot/	qos/	dhcp/	network_statistics/	usb_devices/	bluetooth/	scheduler_logs/
	...	hw_monitor/	zpe_cloud/
	CLI:Open	radUserVsa	radUserVsa	startping=no
	${OUTPUT_WRITE}=	CLI:Write	whoami
	CLI:Enter Path	/system
	${OUTPUT_LS}=	CLI:Ls
	${STATUS1}=	Run Keyword And Return Status	Should Contain	${OUTPUT_LS}	about/
	${STATUS2}=	Run Keyword And Return Status	CLI:Should Not Contain All 	about/	 ${ALL_PATHS}
	Run Keyword If	not ${STATUS1}	Fail	Should Contain About
	Run Keyword If	not ${STATUS2}	Fail	Should Not Contain System Track Information
	Should Contain	${OUTPUT_WRITE}	 radUserVsa
	[Teardown]  Close Connection

Test fallback to group permissions RADIUS VSA
	[Tags]	EXCLUDEIN3_2
	${ALL_PATHS}=	Create List	toolkit/	cluster_peers/	open_sessions/	device_sessions/	event_list/
	...	routing_table/	system_usage/	discovery_logs/	lldp/	ipsec_table/	wireguard/
	...	hotspot/	qos/	dhcp/	network_statistics/	usb_devices/	bluetooth/	scheduler_logs/
	...	hw_monitor/	zpe_cloud/
	CLI:Open
	CLI:Enter Path  /settings/authorization/
	CLI:Delete If Exists Confirm	vsa-grp1
	Close Connection
	CLI:Open	radUserVsa	radUserVsa	startping=no
	${OUTPUT_WRITE}=	CLI:Write	whoami
	CLI:Enter Path	/system
	${OUTPUT_LS}=	CLI:Ls
	${STATUS1}=	Run Keyword And Return Status	Should Contain	${OUTPUT_LS}	about/
	${STATUS2}=	Run Keyword And Return Status	CLI:Should Not Contain All	about/	${ALL_PATHS}
	Run Keyword If	not ${STATUS1}	Fail	Should Contain About
	Run Keyword If	not ${STATUS2}	Fail	Should Not Contain System Track Information
	Should Contain	${OUTPUT_WRITE}    radUserVsa
	[Teardown]  Close Connection

Delete RADIUS Server
	CLI:Open
	[Teardown]  Close Connection

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Delete All RADIUS Providers

SUITE:Teardown
	CLI:Open
	SUITE:Delete all RADIUS Providers
	CLI:Enter Path	/settings/authorization/
	CLI:Delete If Exists Confirm	filter-grp1	vsa-grp1	radGroup1
	CLI:Close Connection

SUITE:Delete All RADIUS Providers
	CLI:Enter Path	/settings/authentication/servers
	${Output}=	CLI:Show
	${RADIUS}=	Get Lines Containing String	${Output}	radius	case_insensitive=True
	@{LINES}=	Split to Lines	${RADIUS}
	FOR		${LINE}	IN	@{Lines}
		${OutputText}=	CLI:Show
		${RADIUSLINES}=	Get Lines Containing String	${OutputText}	radius	case_insensitive=True
		@{Liness}=	Split to Lines	${RADIUSLINES}
		${Lines}=	Replace String Using Regexp	${Liness}[0]	\\s\\s*	--
		@{Details}=	Split String	${Lines}	--
		CLI:Delete	${Details}[1]
	END
