*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authentication Servers > Realms functionality through the CLI as Realm and as NG auth step selector
Metadata	Version 1.0
Metadata	Executed At ${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	NON-CRITICAL
Default Tags	CLI SSH SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
${SERVER115}	192.168.2.115
${SECRET}	${RADIUSSERVER_SECRET}

${REALM_115}	newrealm

${COMMON_USER}	${RADIUSSERVER_USER1}
${COMMON_PASS}	${RADIUSSERVER_PASS1}

${TEST_ZPE}	testzpe	#user definied only in server 88, but acessible via newrealm redirection
*** Test Cases ***
Test normal logins should work with NG realm enabled
	SUITE:Set Server Selection Based On Realms	yes
	CLI:Open
	CLI:Close Current Connection
	SUITE:Check Requests In RADIUS Server	${TRUE}	admin
	CLI:Open	${COMMON_USER}	${COMMON_PASS}
	CLI:Close Current Connection
	SUITE:Check Requests In RADIUS Server	${TRUE}	${COMMON_USER}
########################################################################################################################

Test legacy logins with user@server syntax and NG realm enabled work
	SUITE:Set Server Selection Based On Realms	yes
	CLI:Open	${COMMON_USER}@${SERVER_115}	${COMMON_PASS}
	CLI:Close Current Connection
	SUITE:Check Requests In RADIUS Server	${TRUE}	${COMMON_USER}

Test legacy logins with server\\user syntax and NG realm enabled work
	SUITE:Set Server Selection Based On Realms	yes
	CLI:Open	${SERVER_115}\\${COMMON_USER}	${COMMON_PASS}
	CLI:Close Current Connection
	SUITE:Check Requests In RADIUS Server	${TRUE}	${COMMON_USER}

Test RADIUS realms logins with user@realm syntax and NG realm enabled do not work
	SUITE:Set Server Selection Based On Realms	yes
	${LOGIN}	Run Keyword And Return Status	CLI:Open	${TEST_ZPE}@${REALM_115}	${TEST_ZPE}
	IF	${LOGIN}
		CLI:Close Current Connection
		Fail	RADIUS Realms should not work when NG Auth Step Selector is enabled
	END
	SUITE:Check Requests In RADIUS Server	${TRUE}	${TEST_ZPE}

Test RADIUS realms logins with realm\\user syntax and NG realm enabled do not work
	SUITE:Set Server Selection Based On Realms	yes
	${LOGIN}	Run Keyword And Return Status	CLI:Open	${REALM_115}\\${TEST_ZPE}	${TEST_ZPE}
	IF	${LOGIN}
		CLI:Close Current Connection
		Fail	RADIUS Realms should not work when NG Auth Step Selector is enabled
	END
	SUITE:Check Requests In RADIUS Server	${TRUE}	${TEST_ZPE}
########################################################################################################################
Test normal logins should work with NG realm disabled
	SUITE:Set Server Selection Based On Realms	no
	CLI:Open
	CLI:Close Current Connection
	SUITE:Check Requests In RADIUS Server	${FALSE}	admin
	CLI:Open	${COMMON_USER}	${COMMON_PASS}
	CLI:Close Current Connection
	SUITE:Check Requests In RADIUS Server	${FALSE}	${COMMON_USER}
########################################################################################################################
Test legacy logins with user@server syntax and NG realm disabled do not work
	SUITE:Set Server Selection Based On Realms	no
	${LOGIN}	Run Keyword And Return Status	CLI:Open	${COMMON_USER}@${SERVER_115}	${COMMON_PASS}
	IF	${LOGIN}
		CLI:Close Current Connection
		Fail	NG Auth Step Selector should not work when RADIUS realm is enabled
	END
	SUITE:Check Requests In RADIUS Server	${FALSE}	${COMMON_USER}@${SERVER_115}

Test legacy logins with server\\user syntax and NG realm disabled do not work
	SUITE:Set Server Selection Based On Realms	no
	${LOGIN}	Run Keyword And Return Status	CLI:Open	${SERVER_115}\\${COMMON_USER}	${COMMON_PASS}
	IF	${LOGIN}
		CLI:Close Current Connection
		Fail	NG Auth Step Selector should not work when RADIUS realm is enabled
	END
	SUITE:Check Requests In RADIUS Server	${FALSE}	${SERVER_115}\\${COMMON_USER}

Test RADIUS realms logins with user@realm syntax and NG realm disabled works
	SUITE:Set Server Selection Based On Realms	no
	SUITE:Delete RADIUS server	2
	CLI:Open	${TEST_ZPE}@${REALM_115}	${TEST_ZPE}
	CLI:Close Current Connection
	SUITE:Check Requests In RADIUS Server	${FALSE}	${TEST_ZPE}@${REALM_115}

Test RADIUS realms logins with realm\\user syntax and NG realm disabled works
	SUITE:Set Server Selection Based On Realms	no
	${LOGIN}	Run Keyword And Return Status	CLI:Open	${REALM_115}\\${TEST_ZPE}	${TEST_ZPE}
	CLI:Close Current Connection
	SUITE:Check Requests In RADIUS Server	${FALSE}	${REALM_115}\\${TEST_ZPE}

########################################################################################################################
*** Keywords ***
SUITE:Setup
	CLI:Open	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}	session_alias=admin_session
	SUITE:Configure RADIUS server	${SERVER115}
	SUITE:Run Freeradius Process In Foreground

SUITE:Teardown
	SUITE:Delete RADIUS server	1
	CLI:Close Connection

SUITE:Set Server Selection Based On Realms
	[Arguments]	${OPTION}
	CLI:Switch Connection	admin_session
	CLI:Enter Path	/settings/authentication/realms
	CLI:Set	enable_authentication_server_selection_based_on_realms=${OPTION}
	CLI:Commit

SUITE:Configure RADIUS server
	[Arguments]	${IP_ADDRESS}
	CLI:Switch Connection	admin_session
	CLI:Enter Path	/settings/authentication/servers
	CLI:Add
	CLI:Set	method=radius fallback_if_denied_access=yes remote_server=${IP_ADDRESS}
	CLI:Set	radius_accounting_server=${IP_ADDRESS} radius_secret=${SECRET}
	CLI:Commit

SUITE:Delete RADIUS server
	[Arguments]	${INDEX}
	CLI:Switch Connection	admin_session
	CLI:Enter Path	/settings/authentication/servers
	CLI:Delete	${INDEX}
	CLI:Commit

SUITE:Run Freeradius Process In Foreground
	CLI:Open	root	${ROOT_PASSWORD}	server115_session	HOST_DIFF=${SERVER_115}	TYPE=shell
	${PROCESSES}	CLI:Write	ps -ef | grep freeradius
	Should Contain	${PROCESSES}	freerad
	${PID}	CLI:Write	pidof freeradius	user=Yes
	Log	${PID}
	CLI:Write	kill ${PID}
	Write	/usr/sbin/freeradius -X
	Read Until	Ready to process requests.

SUITE:Check Requests In RADIUS Server
	[Arguments]	${NG_REALM}	${EXPECTED_USER-NAME}
	${CONTAINS_AT}	Run Keyword And Return Status	Should Contain	${EXPECTED_USER-NAME}	@
	${CONTAINS_BS}	Run Keyword And Return Status	Should Contain	${EXPECTED_USER-NAME}	\\
	CLI:Switch Connection	server115_session
	${REQUEST}	Read Until	Ready to process requests.
	Should Contain	${REQUEST}	User-Name = "${EXPECTED_USER-NAME}"
	IF	${NG_REALM}
		#When NG Realm is enabled, the user-name will be stripped and will not contain '@'s and '\'s
		Should Contain	${REQUEST}	No '@' in User-Name = "${EXPECTED_USER-NAME}", looking up realm NULL
		Should Contain	${REQUEST}	No '\\' in User-Name = "${EXPECTED_USER-NAME}", looking up realm NULL
		Should Contain	${REQUEST}	No such realm "NULL"
	ELSE
		${CONTAINS_SERVER}	Run Keyword And Return Status	Should Contain	${EXPECTED_USER-NAME}	${SERVER115}
		${CONTAINS_REALM}	Run Keyword And Return Status	Should Contain	${EXPECTED_USER-NAME}	${REALM_115}
		IF	${CONTAINS_SERVER}
			#When RADIUS Realm is enabled, it will get the server as a realm and will not find it
			Should Contain	${REQUEST}	No such realm "${SERVER115}"
		ELSE IF	${CONTAINS_REALM}
			Should Contain	${REQUEST}	Reply-Message = "Hello, ${TEST_ZPE}"
			Should Contain	${REQUEST}	Found realm "${REALM_115}"
		END
	END