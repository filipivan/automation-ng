*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authentication Console... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	CLI:Open	startping=no
Suite Teardown	Close All Connections

*** Variables ***
${REMOTE_SERVER}	192.168.2.88
${METHOD}	tacacs+
${SECRET}	secret

*** Test Cases ***
Create a TACACS+ Server
	CLI:Open	startping=no
	Delete All TACACS+ Providers
	CLI:Enter Path	/settings/authentication/servers
	CLI:Add
	CLI:Write	set method=${METHOD}
	CLI:Write	set status=enabled
	CLI:Write	set remote_server=${REMOTE_SERVER}
	CLI:Write	set tacacs+_accounting_server=${REMOTE_SERVER}
	CLI:Write	set fallback_if_denied_access=yes
	CLI:Write	set tacacs+_secret=${SECRET}
	CLI:Write	set tacacs+_service=raccess
	CLI:Write	set tacacs+_version=v0_v1
	CLI:Commit

Valid TACACS+ User authentication
	CLI:Open	tacacs1	tacacs1	startping=no
	Write	whoami
	${OUTPUT}=	CLI:Read Until Prompt
	Should Contain	${OUTPUT}	tacacs1
	Close Connection

Invalid TACACS+ User authentication
	Log to Console	Invalid TACACS+ user Authentication
	${OUTPUT}=	Run Keyword And Expect Error	Authentication failed for user 'invaliduser'.
	...	CLI:Open	invaliduser	invaliduser	startping=no
	Log to Console	${OUTPUT}
	Close Connection

Test local Failback
	Log to Console	local admin authentication is allowed due to failback
	Log to Console	Valid TACACS+ User authentication
	CLI:Open	startping=no
	write	whoami
	${OUTPUT}=	CLI:Read Until Prompt
	Should Contain	${OUTPUT}	${DEFAULT_USERNAME}
	Close Connection

Delete TACACS+ Server
	CLI:Open
	Delete all TACACS+ Providers
	Close Connection

*** Keywords ***
Delete All TACACS+ Providers
	CLI:Enter Path	/settings/authentication/servers
	${Output}=	CLI:Show
	${TACACS}=	Get Lines Containing String	${Output}	TACACS+	case_insensitive=True
	@{LINES}=	Split to Lines	${TACACS}
	FOR		${LINE}	IN	@{Lines}
		${OutputText}=	CLI:Show
		${TACACSLINES}=	Get Lines Containing String	${OutputText}	TACACS+	case_insensitive=True
		@{Liness}=	Split to Lines	${TACACSLINES}
		${Lines}=	Replace String Using Regexp	${Liness}[0]	\\s\\s*	--
		@{Details}=	Split String	${Lines}	--
		CLI:Delete	${Details}[1]
	END