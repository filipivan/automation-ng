*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authentication Console... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

#Suite Setup	SUITE:Setup
#Suite Teardown	SUITE:Teardown

*** Variables ***
${LDAPPORT}	389
${LDAPSERVER}	192.168.2.88
${LDAP_BASE_DOMAINONLY}	dc=zpe,dc=net
${LDAPBROWSEUSERDN}	cn=muser1,ou=Marketing,dc=zpe,dc=net
${LDAPBROWSEUSER}	muser1
${LDAPBROWSEPASSWORD}	password
${LDAPINVALIDUSER}	InvalidUser
${LDAPGROUPATRRIB}	memberUid
${LDAPUSERATRRIB}	cn

*** Test Cases ***
Configure LDAP Provider domain base dn, no security and no search username and failback
	CLI:Open	startping=no
	SUITE:Delete all LDAP Providers
	CLI:Enter Path	/settings/authentication/servers
	CLI:Add
	CLI:Set	method=ldap_or_ad status=enabled remote_server=${LDAPSERVER} ldap_ad_base=${LDAP_BASE_DOMAINONLY} ldap_ad_secure=off fallback_if_denied_access=yes ldap_ad_group_attribute=${LDAPGROUPATRRIB} ldap_ad_login_attribute=${LDAPUSERATRRIB}
	CLI:Commit
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	ldap or ad	ignore_case=True
	Should Contain	${OUTPUT}	${LDAPSERVER}
	### BEGIN - Test created because of a bug that is in trello #718, link https://trello.com/c/XOGcEi5I
	${OUTPUT}=	CLI:Enter Path	/settings/authentication/servers/1
	Should Not Contain	${OUTPUT}	Json-CRITICAL
	CLI:Enter Path	/settings/authentication/servers/
	### END - Test created because of a bug that is in trello #718, link https://trello.com/c/XOGcEi5I
	Close Connection

Valid LDAP User authentication without LDAP Browsing
	${VALIDATE_LDAP_USER_AUTHENTICATION}=	Set Variable If	'${VALIDATE_LDAP_USER_AUTHENTICATION}'=='None'	True	${VALIDATE_LDAP_USER_AUTHENTICATION}
	Skip If	${VALIDATE_LDAP_USER_AUTHENTICATION} == False	LDAP Tests Disabled
	Log to Console	\nValid LDAP User authentication
	CLI:Open	${LDAPBROWSEUSER}	${LDAPBROWSEPASSWORD}	startping=no
	${OUTPUT}=	CLI:Write	whoami
	Should Contain	${OUTPUT}	${LDAPBROWSEUSER}
	Close Connection

Invalid LDAP User authentication without LDAP Browsing
	Log to Console	Invalid LDAP user Authentication
	${OUTPUT}=	Run Keyword And Expect Error	Authentication failed for user '${LDAPINVALIDUSER}'.
	...	CLI:Open	${LDAPINVALIDUSER}	${LDAPBROWSEPASSWORD}	startping=no
	Log to Console	${OUTPUT}
	Close Connection

Test local Failback
	Log to Console	local admin authentication is allowed due to failback
	Log to Console	Valid LDAP User authentication
	CLI:Open	startping=no
	${OUTPUT}=	CLI:Write	whoami
	Should Contain	${OUTPUT}	${DEFAULT_USERNAME}
	Close Connection

Configure LDAP Provider domain base dn, no security and with search username
	CLI:Open	startping=no
	SUITE:Delete all LDAP Providers
	CLI:Enter Path	/settings/authentication/servers
#	CLI:Read Until Prompt
	CLI:Add
	CLI:Set	method=ldap_or_ad status=enabled remote_server=${LDAPSERVER} ldap_ad_base=${LDAP_BASE_DOMAINONLY} ldap_ad_secure=off fallback_if_denied_access=yes ldap_ad_database_username=${LDAPBROWSEUSERDN} ldap_ad_database_password=${LDAPBROWSEPASSWORD} ldap_ad_group_attribute=${LDAPGROUPATRRIB} ldap_ad_login_attribute=${LDAPUSERATRRIB}
	CLI:Commit
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	ldap or ad	ignore_case=True
	Should Contain	${OUTPUT}	${LDAPSERVER}
	### BEGIN - Test created because of a bug that is in trello #718, link https://trello.com/c/XOGcEi5I
	${OUTPUT}=	CLI:Enter Path	/settings/authentication/servers/1
	Should Not Contain	${OUTPUT}	Json-CRITICAL
	CLI:Enter Path	/settings/authentication/servers/
	### END - Test created because of a bug that is in trello #718, link https://trello.com/c/XOGcEi5I
	Close Connection

Valid LDAP User authentication with LDAP Browsing
	${VALIDATE_LDAP_USER_AUTHENTICATION}=	Set Variable If	'${VALIDATE_LDAP_USER_AUTHENTICATION}'=='None'	True	${VALIDATE_LDAP_USER_AUTHENTICATION}
	Skip If	${VALIDATE_LDAP_USER_AUTHENTICATION} == False	LDAP Tests Disabled
	CLI:Open	${LDAPBROWSEUSER}	${LDAPBROWSEPASSWORD}	startping=no
	${OUTPUT}=	CLI:Write	whoami
	Should Contain	${OUTPUT}	${LDAPBROWSEUSER}
	Close Connection

Invalid LDAP User authentication with LDAP Browsing
	Log to Console	\nInvalid LDAP user Authentication
	${OUTPUT}=	Run Keyword And Expect Error	Authentication failed for user '${LDAPINVALIDUSER}'.
	...	CLI:Open	${LDAPINVALIDUSER}	${LDAPBROWSEPASSWORD}	startping=no
	Log to Console	${OUTPUT}
	Close Connection

Delete all LDAP Providers
	CLI:Open	startping=no
	SUITE:Delete all LDAP Providers
	Close Connection

*** Keywords ***
SUITE:Delete all LDAP Providers
	CLI:Enter Path	/settings/authentication/servers
	${Output}=	CLI:Show
	${LDAPS}=	Get Lines Containing String	${Output}	ldap or ad	case_insensitive=True
	@{LINES}=	Split to Lines	${LDAPS}
	FOR		${LINE}	IN	@{Lines}
		${OUTPUTTEXT}=	CLI:Show
		${LDAPLINES}=	Get Lines Containing String	${OUTPUTTEXT}	ldap or ad	case_insensitive=True
		@{Liness}=	Split to Lines	${LDAPLINES}
		${Lines}=	Replace String Using Regexp	${Liness}[0]	\\s\\s*	--
		@{Details}=	Split String	${Lines}	--
		CLI:Write	delete ${Details}[1]
	END
	CLI:Commit

