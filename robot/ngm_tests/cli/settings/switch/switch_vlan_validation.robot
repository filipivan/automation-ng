*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Switch Vlan page, ls, show, add, etc... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test available commands after send tab-tab
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_vlan
	CLI:Test Available Commands	delete	ls	shell	system_config_check	add	event_system_audit
	...	pwd	show	whoami	apply_settings	event_system_clear	quit	show_settings	cd	exit	reboot	shutdown
	...	change_password	factory_settings	revert	software_upgrade	commit	hostname	save_settings
	...	system_certificate

Test visible fields for show command
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_vlan
	CLI:Test Show Command Regexp	vlan\\s+untagged ports\\s+tagged ports

Test show_settings command
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_vlan/
	${OUTPUT}=	CLI:Write	show_settings
	Should Match Regexp	${OUTPUT}	/settings/switch_vlan/1 vlan=\\d+

Test set available fields
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_vlan/
	CLI:Add
	CLI:Test Set Available Fields	vlan	tagged_ports	untagged_ports
	[Teardown]	CLI:Cancel	Raw

Test adding empty vlan
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_vlan/
	CLI:Add
	${OUTPUT}=	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: vlan: Field must not be empty.
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=vlan
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_vlan/
	CLI:Add
	Run Keyword If	${NGVERSION} <= 4.2	CLI:Test Set Field Invalid Options	vlan	${POINTS}	Error: vlan: Please provide a VLAN ID between 0 and 4094	yes
	...	ELSE	CLI:Test Set Field Invalid Options	vlan	${POINTS}	Error: vlan: Please provide a VLAN ID between 1 and 4094	yes
	Run Keyword If	${NGVERSION} <= 4.2	CLI:Test Set Field Invalid Options	vlan	${EXCEEDED}	Error: vlan: Please provide a VLAN ID between 0 and 4094	yes
	...	ELSE	CLI:Test Set Field Invalid Options	vlan	${EXCEEDED}	Error: vlan: Please provide a VLAN ID between 1 and 4094	yes
	[Teardown]	CLI:Cancel	Raw

Test empty field for vlan
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_vlan/
	CLI:Add
	CLI:Test Set Field Invalid Options	tagged_ports	sfp0	Error: vlan: Field must not be empty.	yes
	CLI:Test Set Field Invalid Options	untagged_ports	sfp1	Error: vlan: Field must not be empty.	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=tagged_ports
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_vlan/
	CLI:Add
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	tagged_ports	${EMPTY}	Error: Missing value: tagged_ports
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	tagged_ports	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: tagged_ports
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	tagged_ports	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=untagged_ports
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_vlan/
	CLI:Add
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	untagged_ports	${EMPTY}	Error: Missing value: untagged_ports
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	untagged_ports	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: untagged_ports
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	untagged_ports	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}
	[Teardown]	CLI:Cancel	Raw

Test available values for field=tagged_ports/untagged_ports
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_vlan/
	CLI:Add
	Run Keyword If	not ${IS_BSR}	CLI:Test Set Validate Normal Fields	tagged_ports	backplane0	backplane1	sfp0	sfp1
	Run Keyword If	not ${IS_BSR}	CLI:Test Set Validate Normal Fields	untagged_ports	backplane0	backplane1	sfp0	sfp1
	Run Keyword If	${IS_BSR}	CLI:Test Set Validate Normal Fields	tagged_ports	backplane0	netS1
	Run Keyword If	${IS_BSR}	CLI:Test Set Validate Normal Fields	untagged_ports	backplane0	netS1
	[Teardown]	CLI:Cancel	Raw

Test port being both tagged and untagged
	Skip If	not ${IS_SR}	Switch only in SR systems
	Skip If	${IS_BSR}	BSRs avaliable have only backplane0
	CLI:Enter Path	/settings/switch_vlan/
	CLI:Add
	CLI:Set	vlan=2 tagged_ports=backplane1
	CLI:Test Show Command	vlan = 2	tagged_ports = backplane1	untagged_ports =
	CLI:Set Field	untagged_ports	backplane1
	CLI:Test Show Command	vlan = 2	tagged_ports = backplane1	untagged_ports = backplane1
	${OUTPUT}=	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: Following ports are both tagged and untagged: backplane1
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	${IS_SR}=	CLI:Is SR System
	Set Suite Variable	${IS_SR}
	${IS_BSR}=	CLI:Is Bold SR
	Set Suite Variable	${IS_BSR}

SUITE:Teardown
	CLI:Close Connection