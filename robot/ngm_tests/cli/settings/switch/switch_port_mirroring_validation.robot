*** Settings ***
Resource	../../init.robot
Documentation	Validate fields related to the port mirroring feature thru CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	DEPENDENCE_SWITCH	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
@{ALL_VALUES}	${ONE_WORD}	${ELEVEN_WORD}	${NUMBER}	${ELEVEN_NUMBER}	${POINTS}	${ELEVEN_POINTS}
...	${EXCEEDED}	${WORD_AND_NUMBER}	${WORD_AND_POINTS}	${NUMBER_AND_WORD}	${NUMBER_AND_POINTS}
...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}	${WORDS_SPACES}	${WORDS_TWO_SPACES}	${WORDS_WITH_BARS}
${DEFAULT_SESSION_NAME}	test_port_mirroring
${DEFAULT_SOURCE}	backplane1
*** Test Cases ***
Test avaliable commands after send tab-tab
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_port_mirroring/
	CLI:Test Available Commands	acknowledge_alarm_state	diagnostic_data	factory_settings	save_settings
	...	add	disable	hostname	shell
	...	apply_settings	edit	import_settings	show
	...	cd	enable	ls	show_settings
	...	change_password	event_system_audit	pwd	shutdown
	...	cloud_enrollment	event_system_clear	quit	software_upgrade
	...	commit	exec	reboot	system_certificate
	...	create_csr	exit	rename	system_config_check
	...	delete	export_settings	revert
	...	system_certificate

Test avaliable set fields
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_port_mirroring/
	CLI:Add
	CLI:Test Set Available Fields	destination	direction	session_name	sources	status
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=sources
	Skip If	not ${IS_SR}	Switch only in SR systems
	@{SWITCH_INTERFACES}	CLI:Get Switch Interfaces
	Remove Values From List	${SWITCH_INTERFACES}	${EMPTY}
	CLI:Enter Path	/settings/switch_port_mirroring
	FOR	${INTERFACE}	IN	@{SWITCH_INTERFACES}
		CLI:Add
		#routine to check which network card the interface belongs (port mirroring is only accept in the same card)
		${SFP_INTERFACE}	Run Keyword And Return Status	Should Contain	${INTERFACE}	sfp
		${NETS1_INTERFACE}	Run Keyword And Return Status	Should Contain	${INTERFACE}	netS1-
		${NETS2_INTERFACE}	Run Keyword And Return Status	Should Contain	${INTERFACE}	netS2-
		${NETS3_INTERFACE}	Run Keyword And Return Status	Should Contain	${INTERFACE}	netS3-
		#routine to avoid port mirroring with the same source and destination (not allowed)
		Run Keyword If	${SFP_INTERFACE} and '${INTERFACE}' == 'sfp0'	CLI:Set	destination=sfp1
		Run Keyword If	${SFP_INTERFACE} and '${INTERFACE}' == 'sfp1'	CLI:Set	destination=sfp0
		Run Keyword If	${NETS1_INTERFACE} and '${INTERFACE}' == 'netS1-1'	CLI:Set	destination=netS1-2
		Run Keyword If	${NETS1_INTERFACE} and '${INTERFACE}' != 'netS1-1'	CLI:Set	destination=netS1-1
		Run Keyword If	${NETS2_INTERFACE} and '${INTERFACE}' == 'netS2-1'	CLI:Set	destination=netS2-2
		Run Keyword If	${NETS2_INTERFACE} and '${INTERFACE}' != 'netS2-1'	CLI:Set	destination=netS2-1
		Run Keyword If	${NETS3_INTERFACE} and '${INTERFACE}' == 'netS3-1'	CLI:Set	destination=netS3-2
		Run Keyword If	${NETS3_INTERFACE} and '${INTERFACE}' != 'netS3-1'	CLI:Set	destination=netS3-1
		CLI:Set	session_name=${DEFAULT_SESSION_NAME}
		CLI:Set	sources=${INTERFACE}
		CLI:Commit
		CLI:Delete	${DEFAULT_SESSION_NAME}
		CLI:Commit
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=sources
	Skip If	not ${IS_SR}	Switch only in SR systems
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Add
		CLI:Set	session_name=${DEFAULT_SESSION_NAME}
		CLI:Test Set Validate Invalid Options	sources	"${VALUE}"	Error: Invalid value: ${VALUE} for parameter: sources
		CLI:Cancel	Raw
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=destination
	Skip If	not ${IS_SR}	Switch only in SR systems
	@{SWITCH_INTERFACES}	CLI:Get Switch Interfaces
	Remove Values From List	${SWITCH_INTERFACES}	${EMPTY}
	CLI:Enter Path	/settings/switch_port_mirroring
	FOR	${INTERFACE}	IN	@{SWITCH_INTERFACES}
		CLI:Add
		#routine to check which network card the interface belongs (port mirroring is only accept in the same card)
		${SFP_INTERFACE}	Run Keyword And Return Status	Should Contain	${INTERFACE}	sfp
		${NETS1_INTERFACE}	Run Keyword And Return Status	Should Contain	${INTERFACE}	netS1-
		${NETS2_INTERFACE}	Run Keyword And Return Status	Should Contain	${INTERFACE}	netS2-
		${NETS3_INTERFACE}	Run Keyword And Return Status	Should Contain	${INTERFACE}	netS3-
		#routine to avoid port mirroring with the same source and destination (not allowed)
		Run Keyword If	${SFP_INTERFACE} and '${INTERFACE}' == 'sfp0'	CLI:Set	sources=sfp1
		Run Keyword If	${SFP_INTERFACE} and '${INTERFACE}' == 'sfp1'	CLI:Set	sources=sfp0
		Run Keyword If	${NETS1_INTERFACE} and '${INTERFACE}' == 'netS1-1'	CLI:Set	sources=netS1-2
		Run Keyword If	${NETS1_INTERFACE} and '${INTERFACE}' != 'netS1-1'	CLI:Set	sources=netS1-1
		Run Keyword If	${NETS2_INTERFACE} and '${INTERFACE}' == 'netS2-1'	CLI:Set	sources=netS2-2
		Run Keyword If	${NETS2_INTERFACE} and '${INTERFACE}' != 'netS2-1'	CLI:Set	sources=netS2-1
		Run Keyword If	${NETS3_INTERFACE} and '${INTERFACE}' == 'netS3-1'	CLI:Set	sources=netS3-2
		Run Keyword If	${NETS3_INTERFACE} and '${INTERFACE}' != 'netS3-1'	CLI:Set	sources=netS3-1
		CLI:Set	session_name=${DEFAULT_SESSION_NAME}
		CLI:Set	destination=${INTERFACE}
		CLI:Commit
		CLI:Delete	${DEFAULT_SESSION_NAME}
		CLI:Commit
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=destination
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_port_mirroring/
	CLI:Add
	CLI:Set	session_name=${DEFAULT_SESSION_NAME}
	CLI:Test Set Validate Invalid Options	destination	${EMPTY}	Error: Missing value for parameter: destination
	CLI:Cancel	Raw

	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Add
		CLI:Set	session_name=${DEFAULT_SESSION_NAME}
		CLI:Test Set Validate Invalid Options	destination	"${VALUE}"	Error: Invalid value: ${VALUE} for parameter: destination
		CLI:Cancel	Raw
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=direction
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_port_mirroring/
	CLI:Add
	CLI:Test Set Field Options	direction	ingress	egress	both
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=direction
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_port_mirroring/
	CLI:Add
	CLI:Set	session_name=${DEFAULT_SESSION_NAME}
	CLI:Test Set Validate Invalid Options	direction	${EMPTY}	Error: Missing value for parameter: direction
	CLI:Cancel	Raw

	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Add
		CLI:Set	session_name=${DEFAULT_SESSION_NAME}
		CLI:Test Set Validate Invalid Options	direction	"${VALUE}"	Error: Invalid value: ${VALUE} for parameter: direction
		CLI:Cancel	Raw
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=session_name
	Skip If	not ${IS_SR}	Switch only in SR systems
	@{VALID_SESSION_NAMES}	Create List	${ONE_WORD}	${ELEVEN_WORD}	${NUMBER}	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}
	FOR	${VALUE}	IN	@{VALID_SESSION_NAMES}
		CLI:Enter Path	/settings/switch_port_mirroring/
		CLI:Add
		CLI:Set	session_name=${VALUE}
		CLI:Commit
		CLI:Delete	${VALUE}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=session_name
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_port_mirroring/
	CLI:Add
	CLI:Test Set Field Invalid Options	session_name	${EMPTY}	Error: session_name: Field must not be empty.	yes
	CLI:Cancel	Raw

	@{INVALID_SESSION_NAMES}	Create List	${POINTS}	${ELEVEN_POINTS}	${EXCEEDED}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
	...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}	${WORDS_SPACES}	${WORDS_TWO_SPACES}	${WORDS_WITH_BARS}
	FOR	${VALUE}	IN	@{INVALID_SESSION_NAMES}
		CLI:Enter Path	/settings/switch_port_mirroring/
		CLI:Add
		CLI:Set	session_name="${VALUE}"
		${ERROR}	CLI:Commit	Raw
		Should Contain	${ERROR}	Error: session_name: Session name is invalid
		Should Contain	${ERROR}	It must not be empty, maximum allowed length is 64 characters and must only contains a-z, A-Z, 0-9 and _ characters.
		CLI:Cancel
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=status
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_port_mirroring/
	CLI:Add
	CLI:Test Set Field Options	status	enabled	disabled
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=status
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_port_mirroring/
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Add
		CLI:Test Set Field Invalid Options	status	"${VALUE}"	Error: Invalid value: ${VALUE} for parameter: status
		CLI:Cancel
	END
	[Teardown]	CLI:Cancel	Raw
*** Keywords ***
SUITE:Setup
	CLI:Open
	${IS_SR}	CLI:Is SR System
	Set Suite Variable	${IS_SR}

SUITE:Teardown
	CLI:Enter Path	/settings/switch_port_mirroring/
	CLI:Delete All
	CLI:Close Connection