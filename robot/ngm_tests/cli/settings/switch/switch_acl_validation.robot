*** Settings ***
Resource        ../../init.robot
Documentation   Tests of validation on Switch ACL through the CLI
Metadata        Version	1.0
Metadata        Executed At	${HOST}
Force Tags      CLI  DEPENDENCE_SWITCH  EXCLUDEIN3_2  EXCLUDEIN4_2

Suite Setup	    SUITE:Setup
Suite Teardown  SUITE:Teardown

*** Variables ***
${ACL_ING_NAME}         test_switch_acl_ingress
${ACL_EGR_NAME}         test_switch_acl_egress
${ACL_ING_DIRECTION}    ingress
${ACL_EGR_DIRECTION}    egress
@{NUMBERS_FOR_RULE}     0   255
@{VALID_IPS}            192.168.15.60  192.168.16.85  192.168.17.55  0.0.0.0  255.255.255.255
@{INVALID_IPS}          20.20   10.10.10   300.300.300.300   -1.-1.-1.-1    0,5.0,5.0,5.0,5  0.00.00.00
@{ALL_VALUES}=	        ${WORD}     ${ONE_WORD}     ${TWO_WORD}     ${THREE_WORD}
                        ...     ${SEVEN_WORD}   ${EIGHT_WORD}   ${ELEVEN_WORD}  ${NUMBER}   ${ONE_NUMBER}
                        ...     ${TWO_NUMBER}   ${THREE_NUMBER}     ${SEVEN_NUMBER}     ${EIGHT_NUMBER}
                        ...     ${ELEVEN_NUMBER}    ${WORD_AND_NUMBER}  ${NUMBER_AND_WORD}  ${POINTS}
                        ...     ${ONE_POINTS}   ${TWO_POINTS}   ${THREE_POINTS}	${SEVEN_POINTS}
                        ...     ${EIGHT_POINTS}     ${ELEVEN_POINTS}    ${WORD_AND_POINTS}  ${NUMBER_AND_POINTS}
                        ...     ${POINTS_AND_NUMBER}    ${POINTS_AND_WORD}
@{VALID_MACS}            aa:aa:45:66:77:67/16  12:54:bc:aa:aa:aa/16  bb:bb:45:66:77:67/32  ff:ff:ff:ff:ff:ff/32
                         ...  ff:ff:ff:ff:ff:ff  00:00:00:00:00:00  00:00:00
@{INVALID_MACS}          20.20a:30.30b:f1.1:c40.1:00:00  -20.20a:-30.30b:f1.1:c40.1:00:00  -ff:-ff:ff:ff:ff:ff
                         ...  aa:aa:45:66
${NUMBER_TWO}            2

*** Test Cases ***
Test Show Command On Aba Switch ACL
    Skip If           ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Enter Path              /settings/switch_acl/
    CLI:Test Show Command       name        interfaces        direction         number of rules

Test Add Command On Aba Switch ACL
    Skip If   ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Enter Path      /settings/switch_acl/
    CLI:Add
    ${OUTPUT}=          CLI:Show
    Should Contain      ${OUTPUT}       {switch_acl}
    [Teardown]          CLI:Cancel      Raw

Test TabTab On Fiel Direction
    Skip If                   ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Enter Path                      /settings/switch_acl/
    CLI:Add
    CLI:Test Set Field Options Raw      direction   egress   ingress
    [Teardown]                          CLI:Cancel      Raw

Test Ls Command On Aba Switch ACL And On ACLs Inserted
    Skip If       ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Test Ls Command     ${ACL_ING_NAME}     ${ACL_EGR_NAME}
    CLI:Enter Path          /settings/switch_acl/${ACL_ING_NAME}/
    CLI:Test Ls Command     rules     list_name
    CLI:Enter Path          /settings/switch_acl/${ACL_EGR_NAME}/
    CLI:Test Ls Command     rules     list_name

Test Pwd Command on Rules Tab
    Skip If       ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Enter Path          /settings/switch_acl/${ACL_ING_NAME}/rules/
    CLI:Test Pwd Command    /settings/switch_acl/${ACL_ING_NAME}/rules
    CLI:Enter Path          /settings/switch_acl/${ACL_EGR_NAME}/rules/
    CLI:Test Pwd Command    /settings/switch_acl/${ACL_EGR_NAME}/rules

Test Show Table Of Rules Inside The ACL Created And Fields on Add Rule
    Skip If       ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Enter Path          /settings/switch_acl/${ACL_ING_NAME}/rules/
    CLI:Test Show Command   rule id  action  source mac  destination mac  source ip  destination ip  vlan id
    CLI:Add
    CLI:Test Show Command   rule_id = 0  action = permit  source_mac =  destination_mac =  source_ip =  destination_ip =
    ...  vlan_id =
    [Teardown]              CLI:Cancel      Raw

Test Valid Values For Field=rule_id
    ${VALID_NUMBER_OF_RETRIES}=  create list  ${ONE_NUMBER}
    ...  ${TWO_NUMBER}  ${THREE_NUMBER}  @{NUMBERS_FOR_RULE}
    Skip If   ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Enter Path      /settings/switch_acl/${ACL_ING_NAME}/rules/
    FOR	                ${VALID_NUMBER_OF_RETRIE}   IN  @{VALID_NUMBER_OF_RETRIES}
		CLI:Add
		CLI:Test Set Validate Normal Fields  rule_id  ${VALID_NUMBER_OF_RETRIE}
		CLI:Commit
		CLI:Delete If Exists                ${VALID_NUMBER_OF_RETRIE}
    END
    [Teardown]          CLI:Cancel  Raw

Test Invalid Values For Field=rule_id
    ${INVALID_NUMBER_OF_RETRIES}=  create list  ${WORD_AND_NUMBER}  ${NUMBER_AND_WORD}  ${POINTS}
    ...	 ${ONE_POINTS}  ${TWO_POINTS}  ${THREE_POINTS}  ${SEVEN_POINTS}
    ...	 ${EIGHT_POINTS}  ${ELEVEN_POINTS}  ${WORD_AND_POINTS}  ${NUMBER_AND_POINTS}
    ...	 ${POINTS_AND_NUMBER}  ${POINTS_AND_WORD}
    Skip If   ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Enter Path      /settings/switch_acl/${ACL_ING_NAME}/rules/
    FOR	                ${INVALID_NUMBER_OF_RETRIE}     IN  @{INVALID_NUMBER_OF_RETRIES}
		CLI:Add
		Run keyword If	'${NGVERSION}' >= '5.2'	CLI:Test Set Field Invalid Options  rule_id  ${INVALID_NUMBER_OF_RETRIE}
		...	ERROR=Error: rule_id: The value must be between 0 and 255.  save=yes
		...	ELSE	CLI:Test Set Field Invalid Options  rule_id  ${INVALID_NUMBER_OF_RETRIE}
		...	Error: rule_id: This field only accepts integers.  save=yes
		CLI:Cancel  Raw
    END
    CLI:Add
     Run keyword If	'${NGVERSION}' >= '5.4'	CLI:Test Set Field Invalid Options  rule_id  20.20
    ...  Error: rule_id: The value must be between 0 and 255.  save=yes
    ...	ELSE	CLI:Test Set Field Invalid Options  rule_id  20.20
    ...	Error: rule_id: This field only accepts integers.	save=yes
    [Teardown]          CLI:Cancel  Raw

Test Valid Values For Field=action
    Skip If   ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Enter Path      /settings/switch_acl/${ACL_ING_NAME}/rules/
    CLI:Add
    ${OUTPUT}=          CLI:Test Set Field Options  action  deny    permit
    Log                 ${OUTPUT}
    should be true      '${OUTPUT}' == 'None'
    [Teardown]          CLI:Cancel  Raw

Test Invalid Values For Field=action
    Skip If    ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Enter Path      /settings/switch_acl/${ACL_ING_NAME}/rules/
    CLI:Add
    CLI:Test Set Field Invalid Options  action  ${EMPTY}
    ...  Error: Missing value for parameter: action
    FOR	                ${INVALID_VALUE}    IN  @{ALL_VALUES}
		CLI:Test Set Field Invalid Options  action  ${INVALID_VALUE}
		...     Error: Invalid value: ${INVALID_VALUE} for parameter: action
    END
    [Teardown]          CLI:Cancel  Raw

Test Valid Values For Field=source_mac
    Skip If    ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Enter Path  /settings/switch_acl/${ACL_ING_NAME}/rules/
    FOR	            ${VALID_MAC}        IN  @{VALID_MACS}
		CLI:Add
		CLI:Test Set Validate Normal Fields  source_mac  ${VALID_MAC}
		CLI:Commit
		CLI:Delete If Exists                ${VALID_MAC}
    END
    [Teardown]      CLI:Cancel  Raw

Test Invalid Values For Field=source_mac
    ${INVALID_NUMBER_OF_RETRIES}=  create list  ${WORD_AND_NUMBER}  ${NUMBER_AND_WORD}  ${POINTS}
    ...	 ${ONE_POINTS}  ${TWO_POINTS}  ${THREE_POINTS}  ${SEVEN_POINTS}
    ...	 ${EIGHT_POINTS}  ${ELEVEN_POINTS}  ${WORD_AND_POINTS}  ${NUMBER_AND_POINTS}
    ...	 ${POINTS_AND_NUMBER}  ${POINTS_AND_WORD}  @{INVALID_IPS}  @{INVALID_MACS}
    Skip If    ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Enter Path  /settings/switch_acl/${ACL_ING_NAME}/rules/
    FOR	            ${INVALID_MAC}      IN  @{INVALID_NUMBER_OF_RETRIES}
		CLI:Add
		CLI:Test Set Field Invalid Options  source_mac  ${INVALID_MAC}
		...  ERROR=Error: source_mac: Invalid MAC Address  save=yes
		CLI:Cancel  Raw
    END
    [Teardown]      CLI:Cancel  Raw

Test Valid Values For Field=destination_mac
    Skip If    ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Enter Path  /settings/switch_acl/${ACL_ING_NAME}/rules/
    FOR	            ${VALID_MAC}        IN  @{VALID_MACS}
		CLI:Add
		CLI:Test Set Validate Normal Fields  destination_mac  ${VALID_MAC}
		CLI:Commit
		CLI:Delete If Exists                ${VALID_MAC}
    END
    [Teardown]      CLI:Cancel  Raw

Test Invalid Values For Field=destination_mac
    ${INVALID_NUMBER_OF_RETRIES}=  create list  ${WORD_AND_NUMBER}  ${NUMBER_AND_WORD}  ${POINTS}
    ...     ${ONE_POINTS}  ${TWO_POINTS}  ${THREE_POINTS}  ${SEVEN_POINTS}
    ...     ${EIGHT_POINTS}  ${ELEVEN_POINTS}  ${WORD_AND_POINTS}  ${NUMBER_AND_POINTS}
    ...     ${POINTS_AND_NUMBER}  ${POINTS_AND_WORD}  @{INVALID_IPS}  @{INVALID_MACS}
    Skip If    ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Enter Path  /settings/switch_acl/${ACL_ING_NAME}/rules/
    FOR	            ${INVALID_MAC}      IN  @{INVALID_NUMBER_OF_RETRIES}
		CLI:Add
		CLI:Test Set Field Invalid Options  destination_mac  ${INVALID_MAC}
		...  ERROR=Error: destination_mac: Invalid MAC Address  save=yes
		CLI:Cancel  Raw
    END
    [Teardown]      CLI:Cancel  Raw

Test Valid Values For Field=source_ip
    Skip If    ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Enter Path  /settings/switch_acl/${ACL_ING_NAME}/rules/
    FOR	            ${VALID_IP}        IN  @{VALID_IPS}
		CLI:Add
		CLI:Test Set Validate Normal Fields  source_ip  ${VALID_IP}
		CLI:Commit
		CLI:Delete If Exists                ${VALID_IP}
    END
    [Teardown]      CLI:Cancel  Raw

Test Invalid Values For Field=source_ip
    ${INVALID_NUMBER_OF_RETRIES}=  create list  ${WORD_AND_NUMBER}  ${NUMBER_AND_WORD}  ${POINTS}
    ...     ${ONE_POINTS}  ${TWO_POINTS}  ${THREE_POINTS}  ${SEVEN_POINTS}
    ...     ${EIGHT_POINTS}  ${ELEVEN_POINTS}  ${WORD_AND_POINTS}  ${NUMBER_AND_POINTS}
    ...     ${POINTS_AND_NUMBER}  ${POINTS_AND_WORD}  @{INVALID_IPS}  @{INVALID_MACS}
    Skip If    ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Enter Path  /settings/switch_acl/${ACL_ING_NAME}/rules/
    FOR	            ${INVALID_IP}      IN  @{INVALID_NUMBER_OF_RETRIES}
		CLI:Add
		CLI:Test Set Field Invalid Options  source_ip  ${INVALID_IP}
		...  ERROR=Error: source_ip: Invalid IP address.  save=yes
		CLI:Cancel  Raw
	END
    [Teardown]      CLI:Cancel  Raw

Test Valid Values For Field=destination_ip
    Skip If    ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Enter Path  /settings/switch_acl/${ACL_ING_NAME}/rules/
    FOR	            ${VALID_IP}        IN  @{VALID_IPS}
		CLI:Add
		CLI:Test Set Validate Normal Fields  destination_ip  ${VALID_IP}
		CLI:Commit
		CLI:Delete If Exists                ${VALID_IP}
    END
    [Teardown]      CLI:Cancel  Raw

Test Invalid Values For Field=destination_ip
    ${INVALID_NUMBER_OF_RETRIES}=  create list  ${WORD_AND_NUMBER}  ${NUMBER_AND_WORD}  ${POINTS}
    ...     ${ONE_POINTS}  ${TWO_POINTS}  ${THREE_POINTS}  ${SEVEN_POINTS}
    ...     ${EIGHT_POINTS}  ${ELEVEN_POINTS}  ${WORD_AND_POINTS}  ${NUMBER_AND_POINTS}
    ...     ${POINTS_AND_NUMBER}  ${POINTS_AND_WORD}  @{INVALID_IPS}  @{INVALID_MACS}
    Skip If    ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Enter Path  /settings/switch_acl/${ACL_ING_NAME}/rules/
    FOR	            ${INVALID_IP}      IN  @{INVALID_NUMBER_OF_RETRIES}
		CLI:Add
		CLI:Test Set Field Invalid Options  destination_ip  ${INVALID_IP}
		...  ERROR=Error: destination_ip: Invalid IP address.  save=yes
		CLI:Cancel  Raw
	END
    [Teardown]      CLI:Cancel  Raw

Test Valid Values For Field=vlan_id
    ${VALID_NUMBER_OF_RETRIES}=  create list  ${ONE_NUMBER}  ${NUMBER_TWO}
    Skip If    ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Enter Path  /settings/switch_acl/${ACL_ING_NAME}/rules/
    FOR	            ${VALID_NUMBER_OF_RETRIE}   IN  @{VALID_NUMBER_OF_RETRIES}
		CLI:Add
		CLI:Test Set Validate Normal Fields  vlan_id  ${VALID_NUMBER_OF_RETRIE}
		CLI:Commit
		CLI:Delete If Exists                ${VALID_NUMBER_OF_RETRIE}
	END
    [Teardown]      CLI:Cancel  Raw

Test Invalid Values For Field=vlan_id
    ${INVALID_NUMBER_OF_RETRIES}=  create list  ${WORD_AND_NUMBER}  ${NUMBER_AND_WORD}  ${POINTS}
    ...     ${ONE_POINTS}  ${TWO_POINTS}  ${THREE_POINTS}  ${SEVEN_POINTS}
    ...     ${EIGHT_POINTS}  ${ELEVEN_POINTS}  ${WORD_AND_POINTS}  ${NUMBER_AND_POINTS}
    ...     ${POINTS_AND_NUMBER}  ${POINTS_AND_WORD}  @{INVALID_IPS}
    Skip If    ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Enter Path  /settings/switch_acl/${ACL_ING_NAME}/rules/
    FOR	            ${INVALID_NUMBER_OF_RETRIE}     IN  @{INVALID_NUMBER_OF_RETRIES}
		CLI:Add
		CLI:Test Set Field Invalid Options  vlan_id  ${INVALID_NUMBER_OF_RETRIE}
		...  ERROR=Error: vlan_id: Validation error.  save=yes
		CLI:Cancel  Raw
	END
    [Teardown]      CLI:Cancel  Raw

Test Valid Values For Field=acl_ingress on Interface Sfp0
    Skip If    ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Enter Path      /settings/switch_interfaces/sfp0/
    ${OUTPUT}=          CLI:Test Set Field Options   acl_ingress   none    ${ACL_ING_NAME}
    Log                 ${OUTPUT}
    should be true      '${OUTPUT}' == 'None'
    [Teardown]          CLI:Cancel  Raw

Test Invalid Values For Field=acl_ingress on Interface Sfp0
    Skip If    ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Enter Path      /settings/switch_interfaces/sfp0/
    CLI:Test Set Field Invalid Options   acl_ingress   ${EMPTY}
    ...  Error: Missing value for parameter: acl_ingress
    FOR	                ${INVALID_VALUE}    IN  @{ALL_VALUES}
		CLI:Test Set Field Invalid Options  acl_ingress  ${INVALID_VALUE}
		...     Error: Invalid value: ${INVALID_VALUE} for parameter: acl_ingress
	END
    [Teardown]          CLI:Cancel  Raw

Test Valid Values For Field=acl_egress on Interface Sfp0
    Skip If    ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Enter Path      /settings/switch_interfaces/sfp0/
    ${OUTPUT}=          CLI:Test Set Field Options   acl_egress   none    ${ACL_EGR_NAME}
    Log                 ${OUTPUT}
    should be true      '${OUTPUT}' == 'None'
    [Teardown]          CLI:Cancel  Raw

Test Invalid Values For Field=acl_egress on Interface Sfp0
    Skip If    ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Enter Path      /settings/switch_interfaces/sfp0/
    CLI:Test Set Field Invalid Options   acl_egress   ${EMPTY}
    ...  Error: Missing value for parameter: acl_egress
    FOR	                ${INVALID_VALUE}    IN  @{ALL_VALUES}
		CLI:Test Set Field Invalid Options  acl_egress  ${INVALID_VALUE}
		...     Error: Invalid value: ${INVALID_VALUE} for parameter: acl_egress
	END
    [Teardown]          CLI:Cancel  Raw

*** Keywords ***
SUITE:Setup
    CLI:Open
    SUITE:Check If Is NSR
    Skip If   ${IS_NSR} == ${FALSE}      System is not NSR
    SUITE:Delete ACL    ${ACL_ING_NAME}
    SUITE:Delete ACL    ${ACL_EGR_NAME}
    SUITE:Add ACL       ${ACL_ING_NAME}     ${ACL_ING_DIRECTION}
    SUITE:Add ACL       ${ACL_EGR_NAME}     ${ACL_EGR_DIRECTION}

SUITE:Teardown
    Run Keyword If      ${IS_NSR} == ${FALSE}      CLI:Close Connection
    Skip If   ${IS_NSR} == ${FALSE}      System is not NSR
    SUITE:Delete ACL    ${ACL_ING_NAME}
    SUITE:Delete ACL    ${ACL_EGR_NAME}
    CLI:Close Connection

SUITE:Check If Is NSR
    ${IS_NSR}=      CLI:Is Net SR
    Set Suite Variable   ${IS_NSR}
    [Teardown]   Skip If    ${IS_NSR} == ${FALSE}      System is not NSR

SUITE:Add ACL
    [Arguments]                     ${ACL_NAME}     ${ACL_DIRECTION}
    CLI:Enter Path                  /settings/switch_acl/
    CLI:Add
    CLI:Set	                        name=${ACL_NAME} direction=${ACL_DIRECTION}
    CLI:Commit
    CLI:Test Show Command           ${ACL_NAME}

SUITE:Delete ACL
    [Arguments]                     ${ACL_NAME}
    CLI:Enter Path                  /settings/switch_acl/
    CLI:Delete If Exists            ${ACL_NAME}
    CLI:Test Not Show Command       ${ACL_NAME}