*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Switch Interfaces page, ls, show, add, etc... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test configuring vlans
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_vlan/1
	Run Keyword If	not ${IS_BSR}	CLI:Set	untagged_ports=backplane0,sfp0
	Run Keyword If	${IS_BSR}	CLI:Set	untagged_ports=backplane0,netS1
	CLI:Commit
	CLI:Enter Path	/settings/switch_vlan/
	${VLAN2}=	Run Keyword And Return Status	CLI:Test Ls Command	2
	Run Keyword If	${VLAN2}	CLI:Enter Path	/settings/switch_vlan/2
	Run Keyword If	${VLAN2}	CLI:Write	set untagged_ports=backplane1 tagged_ports=sfp1
	Run Keyword If	${VLAN2}	CLI:Commit
	Run Keyword If	${VLAN2} == False and not ${IS_BSR}	CLI:Add
	Run Keyword If	${VLAN2} == False and not ${IS_BSR}	CLI:Write	set vlan=2 untagged_ports=backplane1 tagged_ports=sfp1
	Run Keyword If	${VLAN2} == False and not ${IS_BSR}	CLI:Commit
	[Teardown]	CLI:Cancel	Raw
Test edit command with no arguments
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_interfaces/
	${OUTPUT}=	CLI:Write	edit	Raw
	Should Contain	${OUTPUT}	Error: Not enough arguments

Test show for available fields to edit
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_interfaces/
	Run Keyword If	not ${IS_BSR}	CLI:Edit	sfp0
	Run Keyword If	${IS_BSR}	CLI:Edit	netS1
	Run Keyword If	'${NGVERSION}' < '5.4' and not ${IS_BSR}	CLI:Test Show Command	selected items: sfp0	status	speed	vlan:	port_vlan_id
	...	enable_lldp_advertising_and_reception_through_this_interface
	Run Keyword If	'${NGVERSION}' < '5.4' and ${IS_BSR}	CLI:Test Show Command	selected items: netS1	status	speed	vlan:	port_vlan_id
	Run Keyword If	'${NGVERSION}' >= '5.4'	CLI:Test Show Command	selected items: sfp0	status	description
	...	speed	port_vlan_id	jumbo_frame	acl_ingress	acl_egress
	...	enable_lldp_advertising_and_reception_through_this_interface	mstp_status	mstp_bpdu_guard
	[Teardown]	CLI:Cancel	Raw

Test set available fields
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_interfaces/
	Run Keyword If	not ${IS_BSR}	CLI:Edit	sfp0
	Run Keyword If	${IS_BSR}	CLI:Edit	netS1
	Run Keyword If	'${NGVERSION}' < '5.4' and not ${IS_BSR}	CLI:Test Set Available Fields	enable_lldp_advertising_and_reception_through_this_interface	speed
	...	status	port_vlan_id	jumbo_frame
	Run Keyword If	'${NGVERSION}' < '5.4' and ${IS_BSR}	CLI:Test Set Available Fields	speed	status	port_vlan_id
	Run Keyword If	'${NGVERSION}' >= '5.4'	CLI:Test Set Available Fields	enable_lldp_advertising_and_reception_through_this_interface	speed
	...	status	port_vlan_id	jumbo_frame	acl_ingress	acl_egress	description
	...	mstp_bpdu_guard	mstp_status
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=port_vlan_id
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_interfaces/
	Run Keyword If	not ${IS_BSR}	CLI:Edit	sfp0
	Run Keyword If	${IS_BSR}	CLI:Edit	netS1
	CLI:Test Set Field Invalid Options	port_vlan_id	${EMPTY}	Error: port_vlan_id: Field must not be empty.	yes
	CLI:Test Set Field Invalid Options	port_vlan_id	~!@	Error: port_vlan_id: This field only accepts integers.	yes
	CLI:Test Set Field Invalid Options	port_vlan_id	${EXCEEDED}	Error: port_vlan_id: Invalid PVID: VLAN ${EXCEEDED} not configured	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for dropdown/checkbox fields
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_interfaces/
	Run Keyword If	not ${IS_BSR}	CLI:Edit	sfp0
	Run Keyword If	${IS_BSR}	CLI:Edit	netS1
	${COMMANDS}=	Create List	speed	status
	Run Keyword If	not ${IS_BSR}	Append To List	${COMMANDS}	enable_lldp_advertising_and_reception_through_this_interface	jumbo_frame
	FOR		${COMMAND}	IN	@{COMMANDS}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	a	Error: Invalid value: a for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' < '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value: ${COMMAND}
		Run Keyword If	'${NGVERSION}' < '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	a	Error: Invalid value: a
	END
	[Teardown]	CLI:Cancel	Raw

Test available values for dropdown/checkbox fields
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_interfaces/
	Run Keyword If	not ${IS_BSR}	CLI:Edit	sfp0
	Run Keyword If	${IS_BSR}	CLI:Edit	netS1
	Run Keyword If	not ${IS_BSR}	CLI:Test Set Field Options	enable_lldp_advertising_and_reception_through_this_interface	yes	no
	CLI:Test Set Field Options	speed	100m	10m	1g	auto
	CLI:Test Set Field Options	status	disabled	enabled
	Run Keyword If	not ${IS_BSR}	CLI:Test Set Field Options	jumbo_frame	enabled	disabled
	[Teardown]	CLI:Cancel	Raw

Test entering interface directory
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_interfaces/
	Run Keyword If	not ${IS_BSR}	CLI:Test Ls Command	sfp0	sfp1
	Run Keyword If	${IS_BSR}	CLI:Test Ls Command	netS1	netS2	netS3	netS4
	Run Keyword If	not ${IS_BSR}	CLI:Enter Path	sfp0
	Run Keyword If	${IS_BSR}	CLI:Enter Path	netS1
	Run Keyword If	not ${IS_BSR}	CLI:Test Show Command	interface: sfp0	status	speed	vlan:	port_vlan_id
	...	enable_lldp_advertising_and_reception_through_this_interface	jumbo_frame
	Run Keyword If	${IS_BSR}	CLI:Test Show Command	interface: netS1	status	speed	vlan:	port_vlan_id	jumbo frame: Enabled
	Run Keyword If	not ${IS_BSR}	CLI:Test Set Available Fields	enable_lldp_advertising_and_reception_through_this_interface	speed	port_vlan_id
	...	status	jumbo_frame
	Run Keyword If	${IS_BSR}	CLI:Test Set Available Fields	speed	port_vlan_id	status

Test reset to original switch_interface
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_vlan/1
	Run Keyword If	not ${IS_BSR}	CLI:Set	untagged_ports=backplane0,sfp0
	Run Keyword If	${IS_BSR}	CLI:Set	untagged_ports=backplane0,netS1
	CLI:Commit
	CLI:Enter Path	/settings/switch_vlan/
	${VLAN2}=	Run Keyword And Return Status	CLI:Test Ls Command	2
	Run Keyword If	${VLAN2}	CLI:Enter Path	/settings/switch_vlan/2
	${STATUS}=	Run Keyword And Return Status	Run Keyword If	${VLAN2}	CLI:Set Field	untagged_ports	backplane1,sfp1
	Run Keyword If	${STATUS} == False and not ${IS_BSR}	CLI:Set Field	tagged_ports	''
	Run Keyword If	${STATUS} == False and not ${IS_BSR}	CLI:Set Field	untagged_ports	backplane1,sfp1
	Run Keyword If	${VLAN2}	CLI:Commit
	Run Keyword If	${VLAN2} == False and not ${IS_BSR}	CLI:Add
	Run Keyword If	${VLAN2} == False and not ${IS_BSR}	CLI:Write	set vlan=2 untagged_ports=backplane1,sfp1
	Run Keyword If	${VLAN2} == False and not ${IS_BSR}	CLI:Commit

*** Keywords ***
SUITE:Setup
	CLI:Open
	${IS_SR}=	CLI:Is SR System
	Set Suite Variable	${IS_SR}
	${IS_BSR}=	CLI:Is Bold SR
	Set Suite Variable	${IS_BSR}

SUITE:Teardown
	CLI:Close Connection