*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Switch Vlan functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	DEPENDENCE_SWITCH	NON-CRITICAL
Default Tags	CLI	SSH	SHOW	VLAN

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${HOST1}	${HOST}
${BITMASK_SIZE}	24
${VLAN100}	BACKPLANE0-100
${VLAN200}	BACKPLANE1-200
${VLAN100_IP1}	10.1.100.100
${VLAN100_IP2}	10.1.100.200
${VLAN200_IP1}	10.1.200.100
${VLAN200_IP2}	10.1.200.200

*** Test Cases ***
Test Ping Host 2 In VLAN100 From Host 1
	Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	Skip If	not ${IS_NSR}	VLAN only in NSR
	CLI:Switch Connection	host1_session
	CLI:Test Ping	${VLAN100_IP2}	5	30	SOURCE_INTERFACE=backplane0.100

Test Ping Host 1 In VLAN100 From Host 2
	Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	Skip If	not ${IS_NSR}	VLAN only in NSR
	CLI:Switch Connection	host2_session
	CLI:Test Ping	${VLAN100_IP1}	5	30	SOURCE_INTERFACE=backplane0.100

Test Ping Host 2 In VLAN200 From Host 1
	Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	Skip If	not ${IS_NSR}	VLAN only in NSR
	CLI:Switch Connection	host1_session
	CLI:Test Ping	${VLAN200_IP2}	5	30	SOURCE_INTERFACE=backplane1.200

Test Ping Host 1 In VLAN200 From Host 2
	Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	Skip If	not ${IS_NSR}	VLAN only in NSR
	CLI:Switch Connection	host2_session
	CLI:Test Ping	${VLAN200_IP1}	5	30	SOURCE_INTERFACE=backplane1.200

*** Keywords ***
SUITE:Setup
	Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	Set Suite Variable	${VLAN_HOST_SWITCH1}	${SWITCH_NETPORT_HOST_INTERFACE1}
	Set Suite Variable	${VLAN_HOST_SWITCH2}	${SWITCH_NETPORT_HOST_INTERFACE2}
	Set Suite Variable	${VLAN_SHARED_SWITCH1}	${SWITCH_NETPORT_SHARED_INTERFACE1}
	Set Suite Variable	${VLAN_SHARED_SWITCH2}	${SWITCH_NETPORT_SHARED_INTERFACE2}
	Set Suite Variable	${VLAN100_INTERFACES1}	${VLAN_HOST_SWITCH1},backplane0
	Set Suite Variable	${VLAN100_INTERFACES2}	${VLAN_SHARED_SWITCH1},backplane0
	Set Suite Variable	${VLAN200_INTERFACES1}	${VLAN_HOST_SWITCH2},backplane1
	Set Suite Variable	${VLAN200_INTERFACES2}	${VLAN_SHARED_SWITCH2},backplane1
	Set Suite Variable	${HOST2}	${HOSTSHARED}

	CLI:Open
	${IS_NSR}=	CLI:Is Net SR
	Set Suite Variable	${IS_NSR}
	Skip If	not ${IS_NSR}	VLAN only in NSR
	CLI:Close Connection
	SUITE:Teardown Host 1
	SUITE:Teardown Host 2
	SUITE:Setup Host 1 VLANs
	SUITE:Setup Host 2 VLANs
	SUITE:Send 1 Packet After Stablishing Connection

SUITE:Teardown
	Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	Skip If	not ${IS_NSR}	VLAN only in NSR
	SUITE:Teardown Host 1
	SUITE:Teardown Host 2
	CLI:Close Connection

SUITE:Setup Host 1 VLANs
	CLI:Open	HOST_DIFF=${HOST1}	session_alias=host1_session
	# Add as untagged for removing them from other VLANS
	CLI:Add VLAN	100	UNTAGGED_PORTS=${VLAN100_INTERFACES1}
	CLI:Edit VLAN	100	TAGGED_PORTS=${VLAN100_INTERFACES1}
	CLI:Add VLAN	200	UNTAGGED_PORTS=${VLAN200_INTERFACES1}
	CLI:Edit VLAN	200	TAGGED_PORTS=${VLAN200_INTERFACES1}
	CLI:Enable Switch Interface	INTERFACE=${VLAN_HOST_SWITCH1}
	CLI:Enable Switch Interface	INTERFACE=${VLAN_HOST_SWITCH2}

	Set Client Configuration	timeout=60s
	CLI:Add Static IPv4 Network Connection	${VLAN100}	${VLAN100_IP1}	vlan
	...	backplane0	${BITMASK_SIZE}	100
	CLI:Add Static IPv4 Network Connection	${VLAN200}	${VLAN200_IP1}	vlan
	...	backplane1	${BITMASK_SIZE}	200
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}

SUITE:Setup Host 2 VLANs
	CLI:Open	HOST_DIFF=${HOST2}	session_alias=host2_session
	# Add as untagged for removing them from other VLANS
	CLI:Add VLAN	100	UNTAGGED_PORTS=${VLAN100_INTERFACES2}
	CLI:Edit VLAN	100	TAGGED_PORTS=${VLAN100_INTERFACES2}
	CLI:Add VLAN	200	UNTAGGED_PORTS=${VLAN200_INTERFACES2}
	CLI:Edit VLAN	200	TAGGED_PORTS=${VLAN200_INTERFACES2}
	CLI:Enable Switch Interface	INTERFACE=${VLAN_SHARED_SWITCH1}
	CLI:Enable Switch Interface	INTERFACE=${VLAN_SHARED_SWITCH2}

	Set Client Configuration	timeout=60s
	CLI:Add Static IPv4 Network Connection	${VLAN100}	${VLAN100_IP2}	vlan
	...	backplane0	${BITMASK_SIZE}	100
	CLI:Add Static IPv4 Network Connection	${VLAN200}	${VLAN200_IP2}	vlan
	...	backplane1	${BITMASK_SIZE}	200
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}

SUITE:Teardown Host 1
	CLI:Open	HOST_DIFF=${HOST1}	session_alias=host1_session
	CLI:Delete VLAN	100
	CLI:Delete VLAN	200
	CLI:Delete Network Connections	${VLAN100}
	CLI:Delete Network Connections	${VLAN200}

	Set Client Configuration	timeout=60s
	SUITE:Set Network Connection To DHCP Ipv4	BACKPLANE0
	SUITE:Set Network Connection To DHCP Ipv4	BACKPLANE1
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}

SUITE:Teardown Host 2
	CLI:Open	HOST_DIFF=${HOST2}	session_alias=host2_session
	CLI:Delete VLAN	100
	CLI:Delete VLAN	200
	CLI:Delete Network Connections	${VLAN100}
	CLI:Delete Network Connections	${VLAN200}

	Set Client Configuration	timeout=60s
	SUITE:Set Network Connection To DHCP Ipv4	BACKPLANE0
	SUITE:Set Network Connection To DHCP Ipv4	BACKPLANE1
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}

SUITE:Set Network Connection To DHCP Ipv4
	[Arguments]	${INTERFACE}
	CLI:Enter Path	/settings/network_connections/${INTERFACE}
	CLI:Set	ipv4_mode=dhcp
	CLI:Commit

SUITE:Send 1 Packet After Stablishing Connection
	CLI:Switch Connection	host1_session
	Run Keyword And Ignore Error	CLI:Test Ping	${VLAN100_IP2}	1	30	SOURCE_INTERFACE=backplane0.100
	Run Keyword And Ignore Error	CLI:Test Ping	${VLAN200_IP2}	1	30	SOURCE_INTERFACE=backplane1.200