*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Switch Vlan page, ls, show, add, etc... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test reset to original
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_vlan/1
	Run Keyword If	${IS_BSR}	CLI:Set Field	untagged_ports	backplane0,netS1,netS2,netS3,netS4
	...	ELSE	CLI:Set Field	untagged_ports	backplane0,sfp0
	CLI:Commit
	CLI:Enter Path	/settings/switch_vlan/
	${VLAN2}=	Run Keyword And Return Status	CLI:Test Ls Command	2
	Run Keyword If	${VLAN2} and not ${IS_BSR}	CLI:Enter Path	/settings/switch_vlan/2
	Run Keyword If	${VLAN2} and not ${IS_BSR}	CLI:Set Field	untagged_ports	backplane1,sfp1

	Run Keyword If	${VLAN2} and not ${IS_BSR}	CLI:Commit
	Run Keyword If	${VLAN2} == False and not ${IS_BSR}	CLI:Add
	Run Keyword If	${VLAN2} == False and not ${IS_BSR}	CLI:Write	set vlan=2 untagged_ports=backplane1,sfp1
	Run Keyword If	${VLAN2} == False and not ${IS_BSR}	CLI:Commit

Test adding valid vlan
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_vlan/
	CLI:Add
	Run Keyword If	${IS_BSR}	CLI:Set	vlan=4 tagged_ports=netS2,netS3 untagged_ports=backplane0
	...	ELSE	CLI:Set	vlan=4 tagged_ports=backplane1,sfp0,sfp1 untagged_ports=backplane0
	CLI:Commit
	Run Keyword If	${IS_BSR}	CLI:Test Show Command Regexp	4\\s+backplane0\\s+netS2,\\snetS3
	...	ELSE	CLI:Test Show Command Regexp	4\\s+backplane0\\s+backplane1,\\ssfp0,\\ssfp1

Test editing vlan
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_vlan/
	CLI:Test Ls Command	4
	Run Keyword If	not ${IS_BSR}	Run Keywords	CLI:Edit	4	AND	CLI:Test Show Command	vlan: 4	tagged_ports = backplane1,sfp0,sfp1	untagged_ports = backplane0
	...	ELSE	Run Keywords	CLI:Enter Path	4	AND	CLI:Test Show Command	vlan: 4	tagged_ports = netS2,netS3	untagged_ports = backplane0
	Run Keyword If	not ${IS_BSR}	CLI:Write	set tagged_ports=backplane0,sfp0,sfp1 untagged_ports=backplane1
	...	ELSE	CLI:Write	set tagged_ports=backplane0,netS4 untagged_ports=netS2
	CLI:Commit
	Run Keyword If	not ${IS_BSR}	Run Keywords	CLI:Enter Path	4	AND	CLI:Test Show Command	vlan: 4	tagged_ports = backplane0,sfp0,sfp1	untagged_ports = backplane1
	...	ELSE	CLI:Test Show Command	vlan: 4	tagged_ports = backplane0,netS4	untagged_ports = netS2

Test deleting valid vlan
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_vlan/
	Run Keyword If	not ${IS_BSR} and '${NGVERSION}' >= '5.0'	CLI:Test Show Command Regexp	4\\s+backplane1\\s+backplane0,\\ssfp0,\\ssfp1
	...	ELSE	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Show Command Regexp	4\\s+netS2\\s+backplane0,\\snetS4
	CLI:Delete	4

#A tagged port can belong to multiple VLANs.
Test checking tagged port is not removed from other VLANs
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_vlan/
	CLI:Add
	Run Keyword If	not ${IS_BSR}	CLI:Write	set vlan=3 tagged_ports=sfp1 untagged_ports=backplane1
	...	ELSE	CLI:Write	set vlan=3 tagged_ports=netS1 untagged_ports=netS2
	CLI:Commit
	Run Keyword If	not ${IS_BSR}	CLI:Test Show Command Regexp	3\\s+backplane1\\s+sfp1
	...	ELSE	CLI:Test Show Command Regexp	3\\s+netS2\\s+netS1
	CLI:Add
	Run Keyword If	not ${IS_BSR}	CLI:Write	set vlan=4 tagged_ports=sfp1 untagged_ports=sfp0
	...	ELSE	CLI:Write	set vlan=4 tagged_ports=netS1 untagged_ports=netS3
	CLI:Commit
	Run Keyword If	not ${IS_BSR}	CLI:Test Show Command Regexp	3\\s+backplane1\\s+sfp1
	...	ELSE	CLI:Test Show Command Regexp	3\\s+netS2\\s+netS1
	Run Keyword If	not ${IS_BSR}	CLI:Test Show Command Regexp	4\\s+sfp0\\s+sfp1
	...	ELSE	CLI:Test Show Command Regexp	4\\s+netS3\\s+netS1
	CLI:Delete	3,4

#An untagged port can belong to only one VLAN. If the port is set as untagged in a VLAN, it will be removed from the untagged of other VLANs.
Test checking untagged port is removed from other VLANs
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_vlan/
	CLI:Add
	Run Keyword If	not ${IS_BSR}	CLI:Write	set vlan=3 tagged_ports=backplane0,sfp0,sfp1 untagged_ports=backplane1
	...	ELSE	CLI:Write	set vlan=3 tagged_ports=netS1 untagged_ports=netS4
	CLI:Commit
	Run Keyword If	not ${IS_BSR}	CLI:Test Show Command Regexp	3\\s+backplane1\\s+backplane0,\\ssfp0,\\ssfp1
	...	ELSE	CLI:Test Show Command Regexp	3\\s+netS4\\s+netS1
	CLI:Add
	Run Keyword If	not ${IS_BSR}	CLI:Write	set vlan=4 tagged_ports=backplane0,sfp0,sfp1 untagged_ports=backplane1
	...	ELSE	CLI:Write	set vlan=4 tagged_ports=backplane0 untagged_ports=netS4
	CLI:Commit
	Run Keyword If	not ${IS_BSR}	CLI:Test Show Command Regexp	3\\s+backplane0,\\ssfp0,\\ssfp1
	...	ELSE	CLI:Test Show Command Regexp	3\\s+netS1
	Run Keyword If	not ${IS_BSR}	CLI:Test Show Command Regexp	4\\s+backplane1\\s+backplane0,\\ssfp0,\\ssfp1
	...	ELSE	CLI:Test Show Command Regexp	4\\s+netS4\\s+backplane0
	CLI:Delete	3,4
	[Teardown]	CLI:Cancel	Raw
#When a port is added as Untagged, the PVID will also be set.
Test set pvid if port is untagged
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_vlan/
	CLI:Add
	Run Keyword If	not ${IS_BSR}	CLI:Write	set vlan=3 tagged_ports=sfp1 untagged_ports=sfp0
	...	ELSE	CLI:Write	set vlan=3 tagged_ports=netS1 untagged_ports=netS2
	CLI:Commit

	CLI:Enter Path	/settings/switch_interfaces/
	Run Keyword If	not ${IS_BSR}	CLI:Test Show Command Regexp	sfp0\\s+(Enabled)|(Disabled)\\s+\\w+\\s+3
	...	ELSE	CLI:Test Show Command Regexp	netS2\\s+(Enabled)|(Disabled)\\s+\\w+\\s+3

	CLI:Enter Path	/settings/switch_vlan/
	[Teardown]	CLI:Delete	3

#If vlan with untagged port is deleted, their pvids will become 1 (default)
Test deleting vlan with untagged ports
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_vlan/
	CLI:Add
	Run Keyword If	not ${IS_BSR}	CLI:Write	set vlan=3 untagged_ports=sfp0,sfp1
	...	ELSE	CLI:Write	set vlan=3 untagged_ports=netS3,netS4
	CLI:Commit

	CLI:Enter Path	/settings/switch_interfaces/
	Run Keyword If	not ${IS_BSR}	CLI:Test Show Command Regexp	sfp0\\s+(Enabled)|(Disabled)\\s+\\w+\\s+3
	...	ELSE	CLI:Test Show Command Regexp	netS3\\s+(Enabled)|(Disabled)\\s+\\w+\\s+3
	Run Keyword If	not ${IS_BSR}	CLI:Test Show Command Regexp	sfp1\\s+(Enabled)|(Disabled)\\s+\\w+\\s+3
	...	ELSE	CLI:Test Show Command Regexp	netS4\\s+(Enabled)|(Disabled)\\s+\\w+\\s+3
	CLI:Enter Path	/settings/switch_vlan/
	CLI:Delete	3

	CLI:Enter Path	/settings/switch_interfaces/
	Run Keyword If	not ${IS_BSR}	CLI:Test Show Command Regexp	sfp0\\s+(Enabled)|(Disabled)\\s+\\w+\\s+1
	...	ELSE	CLI:Test Show Command Regexp	netS3\\s+(Enabled)|(Disabled)\\s+\\w+\\s+1
	Run Keyword If	not ${IS_BSR}	CLI:Test Show Command Regexp	sfp1\\s+(Enabled)|(Disabled)\\s+\\w+\\s+1
	...	ELSE	CLI:Test Show Command Regexp	netS4\\s+(Enabled)|(Disabled)\\s+\\w+\\s+1

Test reset to original vlan configuration
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_vlan/1
	Run Keyword If	${IS_BSR}	CLI:Set Field	untagged_ports	backplane0,netS1,netS2,netS3,netS4
	...	ELSE	CLI:Set Field	untagged_ports	backplane0,sfp0
	CLI:Commit
	CLI:Enter Path	/settings/switch_vlan/
	${VLAN2}=	Run Keyword And Return Status	CLI:Test Ls Command	2
	Run Keyword If	${VLAN2} and not ${IS_BSR}	CLI:Enter Path	/settings/switch_vlan/2
	Run Keyword If	${VLAN2} and not ${IS_BSR}	CLI:Set Field	untagged_ports	backplane1,sfp1

	Run Keyword If	${VLAN2} and not ${IS_BSR}	CLI:Commit
	Run Keyword If	${VLAN2} == False and not ${IS_BSR}	CLI:Add
	Run Keyword If	${VLAN2} == False and not ${IS_BSR}	CLI:Write	set vlan=2 untagged_ports=backplane1,sfp1
	Run Keyword If	${VLAN2} == False and not ${IS_BSR}	CLI:Commit

Test entering vlan directory
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_vlan/
	CLI:Test Ls Command	1
	CLI:Enter Path	1
	CLI:Test Show Command	vlan: 1	tagged_ports	untagged_ports
	CLI:Test Set Available Fields	tagged_ports	untagged_ports
	CLI:Test Validate Unmodifiable Field	vlan

Test adding duplicate vlan
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_vlan/
	CLI:Test Ls Command	1
	CLI:Add
	CLI:Test Set Field Invalid Options	vlan	1	Error: vlan: Entry already exists.	yes
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	${IS_SR}=	CLI:Is SR System
	Set Suite Variable	${IS_SR}
	${IS_BSR}=	CLI:Is Bold SR
	Set Suite Variable	${IS_BSR}

SUITE:Teardown
	CLI:Close Connection