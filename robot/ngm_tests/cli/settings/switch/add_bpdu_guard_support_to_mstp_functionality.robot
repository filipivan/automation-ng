*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Switch >bpdu guard support to mstp functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI DEPENDENCE_SWITCH    EXCLUDEIN3_2    EXCLUDEIN4_2    EXCLUDEIN5_0    EXCLUDEIN5_2
Default Tags	CLI	SSH	SHOW	VLAN    MSTP

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${BITMASK_SIZE}	24
${VLAN10_IP1}	10.1.1.8
${VLAN10_IP2}	10.1.1.9
${INTERFACE}    backplane0
${VLAN_ID}  10
${VLAN_CONNECTION}  vlan10
${NUM_PACKETS}  10
${TIMEOUT}  60

*** Test Cases ***
Test Ping Host1 In VLAN10 From Host2 w/o BPDU Guard
    Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	Skip If	not ${IS_NSR}	VLAN only in NSR
	CLI:Switch Connection	host1_session
	CLI:Test Ping	${VLAN10_IP2}	5	30	SOURCE_INTERFACE=backplane0.10

Test Ping Host2 In VLAN10 From Host1 w/o BPDU Guard
    Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	Skip If	not ${IS_NSR}	VLAN only in NSR
	CLI:Switch Connection	host2_session
	CLI:Test Ping	${VLAN10_IP1}	5	30	SOURCE_INTERFACE=backplane0.10

Test Enable MSTP In Host and Hostshared in Global and enable stp and bpdu guard on to host
    Skip If	not ${IS_NSR}	VLAN and MSTP only in NSR
    CLI:Switch Connection	host1_session
    CLI:Enable MSTP On Global
    CLI:Enable MSTP On Switch Interface    ${VLAN_HOST_SWITCH1}
    CLI:Set  mstp_bpdu_guard=on
    CLI:Commit
    CLI:Switch Connection	host2_session
    CLI:Enable MSTP On Global

Test Ping Host1 In VLAN10 From Host2 w/ BPDU Guard
    Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	Skip If	not ${IS_NSR}	VLAN only in NSR
	CLI:Switch Connection	host1_session
	CLI:Test Ping	${VLAN10_IP2}	5	30	SOURCE_INTERFACE=backplane0.10	INSTABLE_MODE=Yes

Test Ping Host2 In VLAN10 From Host1 w/ BPDU Guard
    Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	Skip If	not ${IS_NSR}	VLAN only in NSR
	CLI:Switch Connection	host2_session
	CLI:Test Ping	${VLAN10_IP1}	5	30	SOURCE_INTERFACE=backplane0.10

Test case to Enable mstp on switch interface to hostshared and test ping does not work
    Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	Skip If	not ${IS_NSR}	VLAN only in NSR
    CLI:Switch Connection	host2_session
    CLI:Enable MSTP On Switch Interface    ${VLAN_SHARED_SWITCH1}
    Set Client Configuration	prompt=#
    CLI:Write	shell sudo su -
	Set Client Configuration	timeout=120s
	${OUTPUT}=	CLI:Write	ping ${VLAN10_IP1} -W ${TIMEOUT} -c ${NUM_PACKETS} -B
	Set Client Configuration	prompt=]#
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Write	exit
	Should Not Contain	${OUTPUT}	${NUM_PACKETS} received

Test case to Disable MSTP and BPDU gaurd
    Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	Skip If	not ${IS_NSR}	VLAN only in NSR
	CLI:Switch Connection	host1_session
    CLI:Enter Path	/settings/switch_global
    CLI:Set  stp_status=disabled
    CLI:Commit
    CLI:Enter Path  /settings/switch_interfaces/${VLAN_HOST_SWITCH1}
    CLI:Set  mstp_status=disabled
    CLI:Set  mstp_bpdu_guard=off
    CLI:Commit
    CLI:Switch Connection	host2_session
    CLI:Enter Path	/settings/switch_global
    CLI:Set  stp_status=disabled
    CLI:Commit
    CLI:Enter Path  /settings/switch_interfaces/${VLAN_SHARED_SWITCH1}
    CLI:Set  mstp_status=disabled
    CLI:Commit

*** Keywords ***
SUITE:Setup
	Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	Set Suite Variable	${VLAN_HOST_SWITCH1}	${SWITCH_NETPORT_HOST_INTERFACE1}
	Set Suite Variable	${VLAN_SHARED_SWITCH1}	${SWITCH_NETPORT_SHARED_INTERFACE1}
	Set Suite Variable	${VLAN10_INTERFACES1}	${VLAN_HOST_SWITCH1},backplane0
	Set Suite Variable	${VLAN10_INTERFACES2}	${VLAN_SHARED_SWITCH1},backplane0
    Set Suite Variable	${HOST2}	${HOSTSHARED}
    CLI:Open
	${IS_NSR}=	CLI:Is Net SR
	Set Suite Variable	${IS_NSR}
	Skip If	not ${IS_NSR}	VLAN only in NSR
    CLI:Open	HOST_DIFF=${HOST2}
	${IS_HOST2_NSR}=	CLI:Is Net SR
	Set Suite Variable	${IS_HOST2_NSR}
	Skip If	not ${IS_HOST2_NSR}	VLAN only in NSR
	CLI:Close Connection
	SUITE:Teardown Host 1
	SUITE:Teardown Host 2
	SUITE:Setup Host 1 VLAN
	SUITE:Setup Host 2 VLAN
	SUITE:Send 1 Packet After Stablishing Connection

SUITE:Teardown
	Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	Skip If	not ${IS_NSR}	VLAN only in NSR
	Skip If	not ${IS_HOST2_NSR}	VLAN only in NSR
	SUITE:Teardown Host 1
	SUITE:Teardown Host 2
	CLI:Close Connection

SUITE:Teardown Host 1
	CLI:Open	HOST_DIFF=${HOST}	session_alias=host1_session
	CLI:Delete VLAN	10
	CLI:Delete Network Connections	${VLAN_CONNECTION}
	CLI:Disable Switch Interface   ${SWITCH_NETPORT_HOST_INTERFACE1}

SUITE:Teardown Host 2
	CLI:Open	HOST_DIFF=${HOST2}	session_alias=host2_session
	CLI:Delete VLAN	10
	CLI:Delete Network Connections	${VLAN_CONNECTION}
	CLI:Disable Switch Interface   ${SWITCH_NETPORT_SHARED_INTERFACE1}

SUITE:Setup Host 1 VLAN
    CLI:Open	HOST_DIFF=${HOST}	session_alias=host1_session
    CLI:Enable Switch Interface	${SWITCH_NETPORT_HOST_INTERFACE1}
    CLI:Add Static IPv4 Network Connection	${VLAN_CONNECTION}	${VLAN10_IP1}	vlan
	...	backplane0	${BITMASK_SIZE}	10
    CLI:Add VLAN	10	TAGGED_PORTS=${VLAN10_INTERFACES1}

SUITE:Setup Host 2 VLAN
    CLI:Open	HOST_DIFF=${HOST2}	session_alias=host2_session
    CLI:Enable Switch Interface	${SWITCH_NETPORT_SHARED_INTERFACE1}
    CLI:Add Static IPv4 Network Connection	${VLAN_CONNECTION}	${VLAN10_IP2}	vlan
	...	backplane0	${BITMASK_SIZE}	10
    CLI:Add VLAN	10	TAGGED_PORTS=${VLAN10_INTERFACES2}

SUITE:Send 1 Packet After Stablishing Connection
	CLI:Switch Connection	host1_session
	Run Keyword And Ignore Error	CLI:Test Ping	${VLAN10_IP2}	1	30	SOURCE_INTERFACE=backplane0.10

CLI:Enable MSTP On Global
    [Documentation]	Enable MSTP on switch Global tab and set mode MSTP
    CLI:Enter Path	/settings/switch_global
    CLI:Set  stp_status=enabled
    CLI:Set  stp_mode=mstp
    CLI:Commit

CLI:Enable MSTP On Switch Interface
    [Arguments]	${INTERFACE}
    CLI:Enter Path  /settings/switch_interfaces/${INTERFACE}
    CLI:Set  mstp_status=enabled
    CLI:Commit

