*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Switch Backplane page, ls, show, add, etc... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test available commands after send tab-tab
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_backplane
	CLI:Test Available Commands	event_system_audit	ls	save_settings	shutdown
	...	apply_settings	event_system_clear	pwd	set	software_upgrade
	...	cd	exit	quit	shell	system_certificate
	...	change_password	factory_settings	reboot	show	system_config_check
	...	commit	hostname	revert	show_settings	whoami
	Run Keyword If	not ${IS_BSR}	CLI:Test Available Commands	acknowledge_alarm_state

Test visible fields for show command
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_backplane/
	CLI:Test Show Command	--- backplane0 ---
	Run Keyword If	not ${IS_BSR}	CLI:Test Show Command	backplane0_jumbo_frame	--- backplane1 ---	backplane1_jumbo_frame

Test show_settings command
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_backplane/
	${OUTPUT}=	CLI:Write	show_settings
	Should Match Regexp	${OUTPUT}	/settings/switch_backplane backplane0_port_vlan_id=\\d+
	Run Keyword If	not ${IS_BSR}	Should Match Regexp	${OUTPUT}	/settings/switch_backplane backplane0_jumbo_frame=(enabled)|(disabled)
	Run Keyword If	not ${IS_BSR}	Should Match Regexp	${OUTPUT}	/settings/switch_backplane backplane1_port_vlan_id=\\d+
	Run Keyword If	not ${IS_BSR}	Should Match Regexp	${OUTPUT}	/settings/switch_backplane backplane1_jumbo_frame=(enabled)|(disabled)

Test set available fields
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_backplane/
	CLI:Test Set Available Fields	backplane0_port_vlan_id
	Run Keyword If	not ${IS_BSR}	CLI:Test Set Available Fields	backplane1_port_vlan_id	backplane0_jumbo_frame	backplane1_jumbo_frame
	CLI:Cancel	Raw

Test invalid values for field=backplane0_port_vlan_id
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_backplane/
	CLI:Test Set Field Invalid Options	backplane0_port_vlan_id	${EMPTY}	Error: backplane0_port_vlan_id: Field must not be empty.
	CLI:Test Set Field Invalid Options	backplane0_port_vlan_id	${POINTS}	Error: backplane0_port_vlan_id: This field only accepts integers.
	CLI:Test Set Field Invalid Options	backplane0_port_vlan_id	${EXCEEDED}	Error: backplane0_port_vlan_id: Invalid PVID: VLAN ${EXCEEDED} not configured
	CLI:Cancel	Raw

Test invalid values for field=backplane1_port_vlan_id
	Skip If	not ${IS_SR}	Switch only in SR systems
	Skip If	${IS_BSR}	In BSR jumbo frame is a read-only option
	CLI:Enter Path	/settings/switch_backplane/
	CLI:Test Set Field Invalid Options	backplane1_port_vlan_id	${EMPTY}	Error: backplane1_port_vlan_id: Field must not be empty.
	CLI:Test Set Field Invalid Options	backplane1_port_vlan_id	${POINTS}	Error: backplane1_port_vlan_id: This field only accepts integers.
	CLI:Test Set Field Invalid Options	backplane1_port_vlan_id	${EXCEEDED}	Error: backplane1_port_vlan_id: Invalid PVID: VLAN ${EXCEEDED} not configured
	CLI:Cancel	Raw

Test invalid values for field=backplane0_jumbo_frame/backplane1_jumbo_frame
	Skip If	not ${IS_SR}	Switch only in SR systems
	Skip If	${IS_BSR}	In BSR jumbo frame is a read-only option
	CLI:Enter Path	/settings/switch_backplane/
	${COMMANDS}=	Create List	backplane0_jumbo_frame	backplane1_jumbo_frame
	FOR		${COMMAND}	IN	@{COMMANDS}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value: ${COMMAND}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}
	END

Test valid options for field=backplane0_jumbo_frame/backplane1_jumbo_frame
	Skip If	not ${IS_SR}	Switch only in SR systems
	Skip If	${IS_BSR}	BSR has no jumbo frame option
	CLI:Enter Path	/settings/switch_backplane/
	CLI:Test Set Field Options	backplane0_jumbo_frame	enabled	disabled
	CLI:Test Set Field Options	backplane1_jumbo_frame	enabled	disabled

*** Keywords ***
SUITE:Setup
	CLI:Open
	${IS_SR}=	CLI:Is SR System
	Set Suite Variable	${IS_SR}
	${IS_BSR}	CLI:Is Bold SR
	Set Suite Variable	${IS_BSR}
	SUITE:Configure Vlans

SUITE:Teardown
	CLI:Close Connection

SUITE:Configure Vlans
	Skip If	not ${IS_SR}	Switch only in SR systems
	CLI:Enter Path	/settings/switch_vlan/1
	Run Keyword If	not ${IS_BSR}	CLI:Set	untagged_ports=backplane0,sfp0
	Run Keyword If	${IS_BSR}	CLI:Set	untagged_ports=backplane0,netS1
	CLI:Commit
	CLI:Enter Path	/settings/switch_vlan/
	${VLAN2}=	Run Keyword And Return Status	CLI:Test Ls Command	2
	Run Keyword If	${VLAN2}	CLI:Enter Path	/settings/switch_vlan/2
	Run Keyword If	${VLAN2}	CLI:Set Field	untagged_ports	backplane1,sfp1
	Run Keyword If	${VLAN2}	CLI:Commit
	Run Keyword If	${VLAN2} == False and not ${IS_BSR}	CLI:Add
	Run Keyword If	${VLAN2} == False and not ${IS_BSR}	CLI:Write	set vlan=2 untagged_ports=backplane1,sfp1
	Run Keyword If	${VLAN2} == False and not ${IS_BSR}	CLI:Commit
