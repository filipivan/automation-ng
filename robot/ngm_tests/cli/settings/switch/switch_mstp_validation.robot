*** Settings ***
Resource	../../init.robot
Documentation	Test for validating the field from STP feature
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	    DEPENDENCE_SWITCH	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
@{ALL_VALUES}=	        ${WORD}     ${ONE_WORD}     ${TWO_WORD}     ${THREE_WORD}
                        ...     ${SEVEN_WORD}   ${EIGHT_WORD}   ${ELEVEN_WORD}  ${NUMBER}   ${ONE_NUMBER}
                        ...     ${TWO_NUMBER}   ${THREE_NUMBER}     ${SEVEN_NUMBER}     ${EIGHT_NUMBER}
                        ...     ${ELEVEN_NUMBER}    ${WORD_AND_NUMBER}  ${NUMBER_AND_WORD}  ${POINTS}
                        ...     ${ONE_POINTS}   ${TWO_POINTS}   ${THREE_POINTS}	${SEVEN_POINTS}
                        ...     ${EIGHT_POINTS}     ${ELEVEN_POINTS}    ${WORD_AND_POINTS}  ${NUMBER_AND_POINTS}
                        ...     ${POINTS_AND_NUMBER}    ${POINTS_AND_WORD}
@{DEFAULT_VLANS_AVALIABLE}	1	2
${DEFAULT_MSTP_INSTANCE}	0
@{VALID_COSTS}	1	65535	Auto
${DEFAULT_INTERFACE}	netS1-1
@{VALID_RSTP_PRIORITIES}	0	112	128	144	16	160	176	192	208	224	240	32	48	64	80	96

*** Test Cases ***
###Under settings/switch_global###
Test valid values for field stp_status
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_global/
	CLI:Test Set Validate Normal Fields	stp_status	enabled	disabled
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field stp_status
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_global/
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	stp_status	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: stp_status
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field stp_mode
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_global/
	CLI:Test Set Validate Normal Fields	stp_mode	mstp
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field stp_mode
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_global/
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	stp_mode	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: stp_mode
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field stp_hello_time
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_global/
	FOR	${VALUE}	IN RANGE	9
		${VALUE}	Evaluate	${VALUE}+1 # BUG NG_...
		CLI:Test Set Validate Normal Fields	stp_hello_time	${VALUE}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field stp_hello_time
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_global/
	@{INVALID_HELLO_TIMES}	Remove Values From List	${ALL_VALUES}	${ONE_NUMBER}	${TWO_NUMBER}
	FOR	${VALUE}	IN	@{INVALID_HELLO_TIMES}
		CLI:Test Set Field Invalid Options	stp_hello_time	${VALUE}
		...	Error: switch.global.stp.hello_time: The value must be between 0 and 10.
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field stp_forward_delay
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_global/
	FOR	${VALUE}	IN RANGE	26	#Has to be a number between 4 and 30
		${VALUE}	Evaluate	${VALUE}+4
		CLI:Test Set Validate Normal Fields	stp_forward_delay	${VALUE}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field stp_forward_delay
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_global/
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	stp_forward_delay	${VALUE}
		...	Error: stp_forward_delay: The value must be between 4 and 30.
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field stp_max_age
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_global/
	FOR	${VALUE}	IN RANGE	34
		${VALUE}	Evaluate	${VALUE}+6
		CLI:Test Set Validate Normal Fields	stp_max_age	${VALUE}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field stp_max_age
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_global/
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	stp_max_age	${VALUE}
		...	Error: stp_max_age: The value must be between 6 and 40.
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field stp_tx_hold_count
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_global/
	FOR	${VALUE}	IN RANGE	9	#Has to be a number between 1 and 10
		${VALUE}	Evaluate	${VALUE}+1
		CLI:Test Set Validate Normal Fields	stp_tx_hold_count	${VALUE}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field stp_tx_hold_count
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_global/
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	stp_tx_hold_count	${VALUE}
		...	Error: stp_tx_hold_count: The value must be between 1 and 10.
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for mstp_region_name
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_global/
	CLI:Set	stp_mode=mstp
	CLI:Commit
	@{VALID_REGION_NAMES}	Remove Values From List	${ALL_VALUES}	${EXCEEDED}
	FOR	${REGION_NAME}	IN	${VALID_REGION_NAMES}
		CLI:Test Set Validate Normal Fields	mstp_region_name	${REGION_NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for mstp_region_name
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_global/
	CLI:Set	stp_mode=mstp
	CLI:Commit
	CLI:Test Set Field Invalid Options	mstp_region_name	${EXCEEDED}	Error: mstp_region_name: Field is limited to 32 characters.
	[Teardown]	CLI:Cancel	Raw

Test valid values for mstp_revision
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_global/
	CLI:Set	stp_mode=mstp
	CLI:Commit
	@{VALID_MSTP_REVISIONS}	Create List	0	1	12345	65534	65535
	FOR	${MSTP_REVISION}	IN	@{VALID_MSTP_REVISIONS}
		CLI:Test Set Validate Normal Fields	mstp_revision	${MSTP_REVISION}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for mstp_revision
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_global/
	CLI:Set	stp_mode=mstp
	CLI:Commit
	${INVALID_MSTP_REVISIONS}	Remove Values From List	${ALL_VALUES}	${ONE_NUMBER}	${TWO_NUMBER}	${THREE_NUMBER}
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	mstp_revision	${VALUE}
		...	Error: mstp_revision: The value must be between 0 and 65535.
	END
	[Teardown]	CLI:Cancel	Raw

###Under settings/switch_mstp###
Test available commands after sending Tab-Tab
	CLI:Enter Path	/settings/switch_mstp
	CLI:Test Available Commands	acknowledge_alarm_state  diagnostic_data	ls	show_settings
	...	add	 event_system_audit	pwd	 shutdown
	...	apply_settings	event_system_clear	quit	software_upgrade
	...	cd	exec	reboot	system_certificate
	...	change_password	exit	revert	system_config_check
	...	cloud_enrollment	export_settings	save_settings	whoami
	...	commit	factory_settings	set
	...	create_csr	hostname	shell
	...	delete	import_settings	show

Test valid values for field mst_instance_id
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_mstp/
	CLI:Add
	FOR	${VALUE}	IN RANGE	10
		${VALUE}	Evaluate	${VALUE}+1
		CLI:Set	mst_instance_id=${VALUE}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field mst_instance_id
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	@{INVALID_INSTANCES}	Remove Values From List	${ALL_VALUES}	${NUMBER}	${TWO_NUMBER}	${THREE_NUMBER}
	CLI:Enter Path	/settings/switch_mstp/
	CLI:Add
	FOR	${VALUE}	IN	@{INVALID_INSTANCES}
		CLI:Set	mst_instance_id=${VALUE}
		${ERROR}	CLI:Commit	Raw
		Should Contain	${ERROR}	Error: mst_instance_id: The value must be between 1 and 255.
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field priority
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_mstp/
	CLI:Add
	CLI:Test Set Field Options	mstp_priority	0	12288 	16384	20480	24576	28672	32768 	36864	4096	40960	45056
	...	49152  53248  57344  61440  8192
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field priority
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_mstp/
	CLI:Add
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	mstp_priority	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: mstp_priority
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field vlan
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_mstp/
	FOR	${VLAN}	IN	@{DEFAULT_VLANS_AVALIABLE}
		CLI:Add
		CLI:Set	mst_instance_id=1
		CLI:Set	vlan=${VLAN}
		${OUTPUT}	Run Keyword And Return Status	CLI:Commit
		Should Be True	${OUTPUT}
		CLI:Delete	1
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field vlan
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	@{INVALID_VLANS}	Remove Values From List	${ALL_VALUES}	${NUMBER}
	CLI:Enter Path	/settings/switch_mstp/
	CLI:Add
	FOR	${VALUE}	IN	@{INVALID_VLANS}
		CLI:Set	vlan=${VALUE}
		${ERROR}	CLI:Commit	Raw
		Should Contain	${ERROR}	Error: vlan: Invalid PVID: VLAN ${VALUE} not configured
	END
	[Teardown]	CLI:Cancel	Raw

###Under settings/switch_mstp/<instance>/interfaces/<interface>###
Test valid values for field mstp_cost
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	${SWITCH_INTERFACES}	CLI:Get Switch Interfaces
	${INTERFACE_PRESENT}	Run Keyword And Return Status	Should Contain	${SWITCH_INTERFACES}	${DEFAULT_INTERFACE}
	Skip If	not ${INTERFACE_PRESENT}	The default interface: ${DEFAULT_INTERFACE} is not present in this device
	CLI:Enter Path	/settings/switch_mstp/0/interfaces/${DEFAULT_INTERFACE}
	FOR	${COST}	IN	@{VALID_COSTS}
		CLI:Test Set Validate Normal Fields	mstp_cost	${COST}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field stp_cost
    [Tags]  NON_CRITICAL    BUG_NG-10008
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	${SWITCH_INTERFACES}	CLI:Get Switch Interfaces
	${INTERFACE_PRESENT}	Run Keyword And Return Status	Should Contain	${SWITCH_INTERFACES}	${DEFAULT_INTERFACE}
	Skip If	not ${INTERFACE_PRESENT}	The default interface: ${DEFAULT_INTERFACE} is not present in this device
	CLI:Enter Path	/settings/switch_mstp/0/interfaces/${DEFAULT_INTERFACE}
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	mstp_cost	${VALUE}
		...	Error: mstp_cost: The value must be auto or between 0 and 65535
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field mstp_priority
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	${SWITCH_INTERFACES}	CLI:Get Switch Interfaces
	${INTERFACE_PRESENT}	Run Keyword And Return Status	Should Contain	${SWITCH_INTERFACES}	${DEFAULT_INTERFACE}
	Skip If	not ${INTERFACE_PRESENT}	The default interface: ${DEFAULT_INTERFACE} is not present in this device
	CLI:Enter Path	/settings/switch_mstp/0/interfaces/${DEFAULT_INTERFACE}
	FOR	${PRIORITY}	IN	@{VALID_RSTP_PRIORITIES}
		CLI:Test Set Validate Normal Fields	mstp_priority	${PRIORITY}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field mstp_priority
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	${SWITCH_INTERFACES}	CLI:Get Switch Interfaces
	${INTERFACE_PRESENT}	Run Keyword And Return Status	Should Contain	${SWITCH_INTERFACES}	${DEFAULT_INTERFACE}
	Skip If	not ${INTERFACE_PRESENT}	The default interface: ${DEFAULT_INTERFACE} is not present in this device
	CLI:Enter Path	/settings/switch_mstp/0/interfaces/${DEFAULT_INTERFACE}
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	mstp_priority	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: mstp_priority
	END
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	${IS_NSR}	CLI:Is Net SR
	Set Suite Variable	${IS_NSR}
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR

SUITE:Teardown
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Close Connection