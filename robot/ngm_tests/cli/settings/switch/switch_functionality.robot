*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Switch Backplane page, ls, show, add, etc... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	NON-CRITICAL	NEED_REVIEW	    EXCLUDEIN4_2	DEPENDENCE_SWITCH	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CONNECTION_NAME}	backplane0_automation
${IPV4_ADDRESS_1}	1.1.1.1
${IPV4_ADDRESS_2}	1.1.1.2
${PEER_IP}	${HOSTPEER}
${NETWORK_INTERFACE}	netS3-1	#needs to check in automation environment which one is being used
${IS_INTERFACE}
${STATISTIC}

*** Test Cases ***
Test Switching Configuration
	Skip If	${NOT_NSR}	Switch only in NSR
	SUITE:Add Connection	first_device	${CONNECTION_NAME}	${IPV4_ADDRESS_1}
	SUITE:Add Connection	second_device	${CONNECTION_NAME}	${IPV4_ADDRESS_2}
	SUITE:Enable Switch Interface	first_device	${NETWORK_INTERFACE}
	SUITE:Enable Switch Interface	second_device	${NETWORK_INTERFACE}
	${RX_PACKETS}=	SUITE:Get Switch Statistics
	CLI:Open	root	root	session_alias=root_first_device
	Write	ping ${IPV4_ADDRESS_2} -c 20
	CLI:Switch Connection	first_device
	Wait Until Keyword Succeeds	1m	10s	SUITE:Check Switch Statistics	${RX_PACKETS}
	[Teardown]	Run Keywords	SUITE:Delete Connection	first_device	${CONNECTION_NAME}
	...	AND	SUITE:Delete Connection	second_device	${CONNECTION_NAME}
	...	AND	SUITE:Disable Switch Interface	first_device	${NETWORK_INTERFACE}
	...	AND	SUITE:Disable Switch Interface	second_device	${NETWORK_INTERFACE}

*** Keywords ***
SUITE:Setup
	CLI:Open	session_alias=first_device
	CLI:Open	session_alias=second_device	HOST_DIFF=${PEER_IP}
	CLI:Switch Connection	first_device
	${OUTPUT}=	CLI:Write	show /system/about/
	${NOT_NSR}=	Run Keyword And Return Status	Should Not Contain	${OUTPUT}	system: Nodegrid Services Router
	Set Suite Variable	${NOT_NSR}
	SUITE:Delete Connection	first_device	${CONNECTION_NAME}
	SUITE:Delete Connection	second_device	${CONNECTION_NAME}
	SUITE:Disable Switch Interface	first_device	${NETWORK_INTERFACE}
	SUITE:Disable Switch Interface	second_device	${NETWORK_INTERFACE}

SUITE:Teardown
	CLI:Close Connection

SUITE:Add Connection
	[Arguments]	${ALIAS}	${CONNECTION}	${IP_ADDRESS}
	CLI:Switch Connection	${ALIAS}
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION} type=ethernet ethernet_interface=backplane0 ipv4_mode=static ipv4_address=${IP_ADDRESS} ipv4_bitmask=24
	CLI:Commit
	CLI:Test Show Command	${CONNECTION}
	CLI:Write	up_connection ${CONNECTION}
	CLI:Show

SUITE:Delete Connection
	[Arguments]	 ${ALIAS}	${CONNECTION}
	CLI:Switch Connection	${ALIAS}
	CLI:Enter Path	/settings/network_connections/
	CLI:Delete If Exists	${CONNECTION}
	CLI:Commit

SUITE:Enable Switch Interface
	[Arguments]	${ALIAS}	${INTERFACE}
	CLI:Switch Connection	${ALIAS}
	CLI:Enter Path	/settings/switch_interfaces/${INTERFACE}
	CLI:Set	status=enabled speed=1g
	CLI:Commit

SUITE:Disable Switch Interface
	[Arguments]	${ALIAS}	${INTERFACE}
	CLI:Switch Connection	${ALIAS}
	CLI:Enter Path	/settings/switch_interfaces/${INTERFACE}
	CLI:Set	status=disabled
	CLI:Commit

SUITE:Check Switch Statistics
	[Arguments]	${CURRENT_RX_PACKETS}
	CLI:Enter Path	/system/switch_statistics/
	${OUTPUT}=	CLI:Show
	@{GET_STATISTICS}=	Split To Lines	${OUTPUT}
	${COUNT}=	Set Variable	0
	FOR		${INTERFACE}	IN	@{GET_STATISTICS}
		${IS_INTERFACE}=	Run Keyword and Return Status	Should Match Regexp	${INTERFACE}	${NETWORK_INTERFACE}\\s+(?s).*
		${LINE}=	Get From List	${GET_STATISTICS}	${COUNT}
		Run Keyword If	${IS_INTERFACE} == ${TRUE}	Set Suite Variable	${STATISTIC}	${LINE}
		${COUNT}=	Evaluate	${COUNT}+1
	END
	@{ELEMENTS}=	Split String	${STATISTIC}	${SPACE}
	${RX_PACKETS}=	Get From List	${ELEMENTS}	18
	${RX_PACKETS_INT}=	Convert To Integer	${RX_PACKETS}
	${CURRENT_RX_PACKETS_INT}=	Convert To Integer	${CURRENT_RX_PACKETS}
	Should Be True	${RX_PACKETS_INT} > ${CURRENT_RX_PACKETS_INT}

SUITE:Get Switch Statistics
	CLI:Enter Path	/system/switch_statistics/
	${OUTPUT}=	CLI:Show
	@{GET_STATISTICS}=	Split To Lines	${OUTPUT}
	${COUNT}=	Set Variable	0
	FOR		${INTERFACE}	IN	@{GET_STATISTICS}
		${IS_INTERFACE}=	Run Keyword and Return Status	Should Match Regexp	${INTERFACE}	${NETWORK_INTERFACE}\\s+(?s).*
		${LINE}=	Get From List	${GET_STATISTICS}	${COUNT}
		Run Keyword If	${IS_INTERFACE} == ${TRUE}	Set Suite Variable	${STATISTIC}	${LINE}
		${COUNT}=	Evaluate	${COUNT}+1
	END
	@{ELEMENTS}=	Split String	${STATISTIC}	${SPACE}
	${RX_PACKETS}=	Get From List	${ELEMENTS}	18
	[Return]	${RX_PACKETS}