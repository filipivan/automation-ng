*** Settings ***
Resource	../../init.robot
Documentation	Test configurations related to the port mirroring rules thru CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	NON-CRITICAL	DEPENDENCE_SWITCH	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
${SOURCE_PORT}	netS1-1
${DESTINATION_PORT}	netS1-2
${MIRRORING_RULE_NAME}	netS1_egress
*** Test Cases ***
Test cancel command when adding a new mirroring rule
	Skip If	not ${IS_NSR}	Port mirroring is only supported in NSR
	CLI:Enter Path	/settings/switch_port_mirroring/
	CLI:Add
	CLI:Set	session_name=${MIRRORING_RULE_NAME}
	CLI:Cancel
	${MIRRORING_RULES}	CLI:Show
	Should Not Contain	${MIRRORING_RULES}	${MIRRORING_RULE_NAME}
	[Teardown]	CLI:Cancel	Raw

Test add new mirroring rule
	Skip If	not ${IS_NSR}	Port mirroring is only supported in NSR
	CLI:Enter Path	/settings/switch_port_mirroring/
	CLI:Add
	CLI:Set	session_name=${MIRRORING_RULE_NAME} destination=${DESTINATION_PORT} direction=egress status=enabled sources=${SOURCE_PORT}
	CLI:Save
	${MIRRORING_RULES}	CLI:Show
	Should Contain	${MIRRORING_RULES}	${MIRRORING_RULE_NAME}
	[Teardown]	CLI:Cancel	Raw

Test add duplicate mirroring rule
	Skip If	not ${IS_NSR}	Port mirroring is only supported in NSR
	CLI:Enter Path	/settings/switch_port_mirroring/
	CLI:Add
	CLI:Set	session_name=${MIRRORING_RULE_NAME} destination=backplane0 direction=egress status=enabled sources=backplane1
	${DUPLICATE}	CLI:Commit	Raw
	Should Contain	${DUPLICATE}	Error: session_name: Entry already exists.
	[Teardown]	CLI:Cancel	Raw

Test add mirroring rule with the same traffic
	Skip If	not ${IS_NSR}	Port mirroring is only supported in NSR
	CLI:Enter Path	/settings/switch_port_mirroring/
	CLI:Add
	CLI:Set	direction=egress sources=${SOURCE_PORT} session_name=duplicate_traffic destination=${DESTINATION_PORT}
	${DUPLICATE_TRAFFIC}	CLI:Commit	Raw
	Should Contain	${DUPLICATE_TRAFFIC}	Ingress/egress traffic may only be individually mirrored to a single destination
	[Teardown]	CLI:Cancel	Raw

Test add mirroring rule with source and destination from different network cards
	Skip If	not ${IS_NSR}	Port mirroring is only supported in NSR
	CLI:Enter Path	/settings/switch_port_mirroring/
	CLI:Add
	CLI:Set	direction=both sources=${SOURCE_PORT} session_name=invalid destination=sfp1
	${DIFFERENT_CARDS}	CLI:Commit	Raw
	Should Contain	${DIFFERENT_CARDS}	Error: sources: Destination (sfp1) and source (${SOURCE_PORT}) interfaces dont belong to the same network card
	[Teardown]	CLI:Cancel	Raw

Test disable mirroring rule
	Skip If	not ${IS_NSR}	Port mirroring is only supported in NSR
	CLI:Enter Path	/settings/switch_port_mirroring/
	CLI:Write	disable ${MIRRORING_RULE_NAME}
	CLI:Commit
	${RULE_DISABLED}	CLI:Show
	Should Match Regexp	${RULE_DISABLED}	\\s+${MIRRORING_RULE_NAME}\\s+${DESTINATION_PORT}\\s+Disabled
	[Teardown]	CLI:Cancel	Raw

Test enable mirroring rule
	Skip If	not ${IS_NSR}	Port mirroring is only supported in NSR
	CLI:Enter Path	/settings/switch_port_mirroring/
	CLI:Write	enable ${MIRRORING_RULE_NAME}
	CLI:Commit
	${RULE_DISABLED}	CLI:Show
	Should Match Regexp	${RULE_DISABLED}	\\s+${MIRRORING_RULE_NAME}\\s+${DESTINATION_PORT}\\s+Enabled
	[Teardown]	CLI:Cancel	Raw

Test revert rule configuration
	Skip If	not ${IS_NSR}	Port mirroring is only supported in NSR
	CLI:Enter Path	/settings/switch_port_mirroring/
	CLI:Write	disable ${MIRRORING_RULE_NAME}
	${TEMPORARY_DISABLED}	CLI:Show
	Should Match Regexp	${TEMPORARY_DISABLED}	\\s+${MIRRORING_RULE_NAME}\\s+${DESTINATION_PORT}\\s+Disabled
	CLI:Revert
	${ENABLED_AGAIN}	CLI:Show
	Should Match Regexp	${ENABLED_AGAIN}	\\s+${MIRRORING_RULE_NAME}\\s+${DESTINATION_PORT}\\s+Enabled
	CLI:Commit
	[Teardown]	CLI:Cancel	Raw

Test edit rule with valid configurations
	Skip If	not ${IS_NSR}	Port mirroring is only supported in NSR
	CLI:Enter Path	/settings/switch_port_mirroring/
	CLI:Edit	${MIRRORING_RULE_NAME}
	CLI:Set	direction=both status=disabled sources=netS1-3,netS1-4
	CLI:Commit
	${RULE_EDITED}	CLI:Show
	Should Match Regexp	${RULE_EDITED}	\\s+${MIRRORING_RULE_NAME}\\s+${DESTINATION_PORT}\\s+Disabled\\s+netS1-3, netS1-4\\s+Both
	CLI:Edit	${MIRRORING_RULE_NAME}
	CLI:Set	direction=egress status=enabled sources=${SOURCE_PORT}
	CLI:Commit
	${EDITED_TO_ORIGINAL}	CLI:Show
	Should Match Regexp	${EDITED_TO_ORIGINAL}	\\s+${MIRRORING_RULE_NAME}\\s+${DESTINATION_PORT}\\s+Enabled\\s+${SOURCE_PORT}\\s+Egress
	[Teardown]	CLI:Cancel	Raw

Test edit rule with duplicated traffic
	Skip If	not ${IS_NSR}	Port mirroring is only supported in NSR
#first, creating a new rule to have a conflict
	CLI:Enter Path	/settings/switch_port_mirroring/
	CLI:Add
	CLI:Set	session_name=conflict_rule destination=netS1-12 sources=${SOURCE_PORT} direction=ingress
	CLI:Commit
#now editing the first rule to direction=both
	CLI:Edit	${MIRRORING_RULE_NAME}
	CLI:Set	direction=both
	${ERROR}	CLI:Commit	Raw
	Should Contain	${ERROR}	Error: sources: Traffic mirroring of ${SOURCE_PORT}/ingress is already in use by session conflict_rule.
	Should Contain	${ERROR}	Ingress/egress traffic may only be individually mirrored to a single destination
	CLI:Cancel
	CLI:Delete	conflict_rule
	CLI:Commit
	[Teardown]	CLI:Cancel	Raw

Test rename rule
	Skip If	not ${IS_NSR}	Port mirroring is only supported in NSR
	CLI:Enter Path	/settings/switch_port_mirroring/
	CLI:Write	rename ${MIRRORING_RULE_NAME}
	CLI:Set	new_name=renamed_rule
	CLI:Commit
	${RULE_RENAMED}	CLI:Show
	Should Contain	${RULE_RENAMED}	renamed_rule
	CLI:Write	rename renamed_rule
	CLI:Set	new_name=${MIRRORING_RULE_NAME}
	CLI:Commit
	${ORIGINAL_RULE_NAME}	CLI:Show
	Should Contain	${ORIGINAL_RULE_NAME}	${MIRRORING_RULE_NAME}
	[Teardown]	CLI:Cancel	Raw

Test delete mirroring rule
	Skip If	not ${IS_NSR}	Port mirroring is only supported in NSR
	CLI:Enter Path	/settings/switch_port_mirroring/
	CLI:Delete	${MIRRORING_RULE_NAME}
	CLI:Commit
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	${IS_NSR}=	CLI:Is Net SR
	Set Suite Variable	${IS_NSR}
	Skip If	not ${IS_NSR}	Port mirroring is only supported in NSR

SUITE:Teardown
	CLI:Close Connection