*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Switch_lag_functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI DEPENDENCE_SWITCH    EXCLUDEIN3_2    EXCLUDEIN4_2    EXCLUDEIN5_0    EXCLUDEIN5_2
Default Tags	CLI	SSH	SHOW	LAG

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${BITMASK_SIZE}	24
${VLAN10_IP1}	10.1.1.8
${VLAN10_IP2}	10.1.1.9
${INTERFACE}    backplane0
${VLAN_ID}  10
${VLAN_CONNECTION}  vlan10
${NUM_PACKETS}  10
${TIMEOUT}  60
${LAG_NAME}   lag1
${LAG_TYPE1}     static
${LAG_TYPE2}    lacp
${LAG_ID}   1
${LACP_PRIORITY}    65535

*** Test Cases ***
Test Lag with static type
    Skip If	not ${IS_NSR}	LAG only in NSR
    SUITE:Disable Switch Interfaces
    SUITE:Setup Host 1 VLAN
    SUITE:Setup Host 2 VLAN
    SUITE:Add Lag Configuration with static type
    SUITE:Enable Switch Interfaces
    CLI:Switch Connection	host1_session
    CLI:Test Ping At Start	${VLAN10_IP2}	5	30	SOURCE_INTERFACE=backplane0.10

Test lag_load_balance types
    Skip If	not ${IS_NSR}	LAG only in NSR
    SUITE:Check Source and Destination MAC type
    SUITE:Check Source and Destination MAC and IP type
    SUITE:Check Source and Destination MAC and IP and TCP/UDP Ports type
    SUITE:Check Source and Destination IP type

Test ping after disabling any switch interface
    Skip If	not ${IS_NSR}	LAG only in NSR
    CLI:Switch Connection	host2_session
    CLI:Disable Switch Interface   ${SWITCH_NETPORT_SHARED_INTERFACE1}
    CLI:Switch Connection	host1_session
    CLI:Test Ping	${VLAN10_IP2}	5	30	SOURCE_INTERFACE=backplane0.10

Test case to delete the static type lag configuration
    Skip If	not ${IS_NSR}	LAG only in NSR
    CLI:Enter Path  /settings/switch_lag/
    CLI:Delete  ${LAG_NAME}
    CLI:Switch Connection	host2_session
    CLI:Enter Path  /settings/switch_lag/
    CLI:Delete  ${LAG_NAME}

Test Lag with lacp type
    Skip If	not ${IS_NSR}	LAG only in NSR
    SUITE:Add Lag Configuration with LACP type
    CLI:Switch Connection	host1_session
    CLI:Test Ping At Start	${VLAN10_IP2}	5	30	SOURCE_INTERFACE=backplane0.10

Test lag_load_balance types
    Skip If	not ${IS_NSR}	LAG only in NSR
    SUITE:Check Source and Destination MAC type
    SUITE:Check Source and Destination MAC and IP type
    SUITE:Check Source and Destination MAC and IP and TCP/UDP Ports type
    SUITE:Check Source and Destination IP type

Test ping after disabling any switch interface
    Skip If	not ${IS_NSR}	LAG only in NSR
    CLI:Switch Connection	host2_session
    CLI:Enable Switch Interface   ${SWITCH_NETPORT_SHARED_INTERFACE1}
    CLI:Disable Switch Interface   ${SWITCH_NETPORT_SHARED_INTERFACE2}
    CLI:Switch Connection	host2_session
    Sleep	2s
    CLI:Test Ping At Start	${VLAN10_IP1}	5	30	SOURCE_INTERFACE=backplane0.10

Test case to Delete the lacp lag configuration
    Skip If	not ${IS_NSR}	LAG only in NSR
    CLI:Enter Path  /settings/switch_lag/
    CLI:Delete  ${LAG_NAME}
    CLI:Switch Connection	host1_session
    CLI:Enter Path  /settings/switch_lag/
    CLI:Delete  ${LAG_NAME}

*** Keywords ***
SUITE:Setup
    Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
    Set Suite Variable	${VLAN_HOST_SWITCH1}	${SWITCH_NETPORT_HOST_INTERFACE1}
    Set Suite Variable	${VLAN_SHARED_SWITCH1}	${SWITCH_NETPORT_SHARED_INTERFACE1}
    Set Suite Variable	${VLAN_HOST_SWITCH2}	${SWITCH_NETPORT_HOST_INTERFACE2}
    Set Suite Variable	${VLAN_SHARED_SWITCH2}	${SWITCH_NETPORT_SHARED_INTERFACE2}
    Set Suite Variable	${VLAN10_INTERFACES1}	${VLAN_HOST_SWITCH1},${VLAN_HOST_SWITCH2},backplane0
    Set Suite Variable	${VLAN10_INTERFACES2}	${VLAN_SHARED_SWITCH1},${VLAN_SHARED_SWITCH2},backplane0
    Set Suite Variable	${HOST2}	${HOSTSHARED}
    ${SAME_CARD_HOST}	SUITE:Check If Netports Belong To Same Card	${VLAN_HOST_SWITCH1}	${VLAN_HOST_SWITCH2}
    ${SAME_CARD_SHARED}	SUITE:Check If Netports Belong To Same Card	${VLAN_SHARED_SWITCH1}	${VLAN_SHARED_SWITCH2}
    Skip If	not ${SAME_CARD_HOST}	Ports need to belong to the same card.
    Skip If	not ${SAME_CARD_SHARED}	Ports need to belong to the same card.
    CLI:Open
    ${IS_NSR}=	CLI:Is Net SR
    Set Suite Variable	${IS_NSR}
    Skip If	not ${IS_NSR}	LAG only in NSR
    CLI:Close Connection
    CLI:Open	HOST_DIFF=${HOST2}	session_alias=host2_session
    ${IS_NSR}=	CLI:Is Net SR
    Set Suite Variable	${IS_NSR}
    Skip If	not ${IS_NSR}	LAG only in NSR
    SUITE:Teardown Host 1
    SUITE:Teardown Host 2

SUITE:Teardown
    Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
    CLI:Open
    ${IS_NSR}=	CLI:Is Net SR
    Set Suite Variable	${IS_NSR}
    Run keyword If	${IS_NSR}	SUITE:Teardown Host 1
    CLI:Open	HOST_DIFF=${HOST}	session_alias=host1_session
    Run keyword If	${IS_NSR}	SUITE:Teardown Host 2
    CLI:Close Connection

SUITE:Teardown Host 1
    CLI:Open	HOST_DIFF=${HOST}	session_alias=host1_session
    CLI:Delete VLAN	10
    CLI:Delete Network Connections	${VLAN_CONNECTION}
    CLI:Disable Switch Interface   ${SWITCH_NETPORT_HOST_INTERFACE1}
    CLI:Disable Switch Interface   ${SWITCH_NETPORT_HOST_INTERFACE2}

SUITE:Teardown Host 2
    CLI:Open	HOST_DIFF=${HOST2}	session_alias=host2_session
    CLI:Delete VLAN	10
    CLI:Delete Network Connections	${VLAN_CONNECTION}
    CLI:Disable Switch Interface   ${SWITCH_NETPORT_SHARED_INTERFACE1}
    CLI:Disable Switch Interface   ${SWITCH_NETPORT_SHARED_INTERFACE2}

SUITE:Disable Switch Interfaces
    CLI:Switch Connection	host1_session
    CLI:Disable Switch Interface   ${SWITCH_NETPORT_HOST_INTERFACE1}
    CLI:Disable Switch Interface   ${SWITCH_NETPORT_HOST_INTERFACE2}
    CLI:Switch Connection	host2_session
    CLI:Disable Switch Interface   ${SWITCH_NETPORT_SHARED_INTERFACE1}
    CLI:Disable Switch Interface   ${SWITCH_NETPORT_SHARED_INTERFACE2}

SUITE:Setup Host 1 VLAN
    CLI:Open	HOST_DIFF=${HOST}	session_alias=host1_session
    CLI:Add Static IPv4 Network Connection	${VLAN_CONNECTION}	${VLAN10_IP1}	vlan
	...	backplane0	${BITMASK_SIZE}	10
    CLI:Add VLAN	10	TAGGED_PORTS=${VLAN10_INTERFACES1}

SUITE:Setup Host 2 VLAN
    CLI:Open	HOST_DIFF=${HOST2}	session_alias=host2_session
    CLI:Add Static IPv4 Network Connection	${VLAN_CONNECTION}	${VLAN10_IP2}	vlan
	...	backplane0	${BITMASK_SIZE}	10
    CLI:Add VLAN	10	TAGGED_PORTS=${VLAN10_INTERFACES2}

SUITE:Add Lag Configuration with static type
    CLI:Switch Connection	host1_session
    CLI:Enter Path  /settings/switch_lag/
    CLI:Add
    CLI:Set  name=${LAG_NAME} id=${LAG_ID} type=${LAG_TYPE1}
    CLI:Set  ports=${VLAN_HOST_SWITCH1},${VLAN_HOST_SWITCH2}
    CLI:Commit
    ${OUTPUT}   CLI:Show
    Should Contain  ${OUTPUT}   ${LAG_NAME}
    CLI:Switch Connection	host2_session
    CLI:Enter Path  /settings/switch_lag/
    CLI:Add
    CLI:Set  name=${LAG_NAME} id=${LAG_ID} type=${LAG_TYPE1}
    CLI:Set  ports=${VLAN_SHARED_SWITCH1},${VLAN_SHARED_SWITCH2}
    CLI:Commit
    ${OUTPUT}   CLI:Show
    Should Contain  ${OUTPUT}   ${LAG_NAME}

SUITE:Enable Switch Interfaces
    CLI:Switch Connection	host1_session
    CLI:Enable Switch Interface   ${SWITCH_NETPORT_HOST_INTERFACE1}
    CLI:Enable Switch Interface   ${SWITCH_NETPORT_HOST_INTERFACE2}
    CLI:Switch Connection	host2_session
    CLI:Enable Switch Interface   ${SWITCH_NETPORT_SHARED_INTERFACE1}
    CLI:Enable Switch Interface   ${SWITCH_NETPORT_SHARED_INTERFACE2}

SUITE:Check Source and Destination MAC type
    Skip If	not ${IS_NSR}	LAG only in NSR
    CLI:Enter Path  /settings/switch_global/
    CLI:Set  lag_load_balance=source_and_destination_mac
    CLI:Commit
    CLI:Switch Connection	host2_session
    CLI:Enter Path  /settings/switch_global/
    CLI:Set  lag_load_balance=source_and_destination_mac
    CLI:Commit
    CLI:Switch Connection	host1_session
	CLI:Test Ping	${VLAN10_IP2}	5	30	SOURCE_INTERFACE=backplane0.10

SUITE:Check Source and Destination MAC and IP type
    Skip If	not ${IS_NSR}	LAG only in NSR
    CLI:Enter Path  /settings/switch_global/
    CLI:Set  lag_load_balance=source_and_destination_mac_and_ip_
    CLI:Commit
    CLI:Switch Connection	host2_session
    CLI:Enter Path  /settings/switch_global/
    CLI:Set  lag_load_balance=source_and_destination_mac_and_ip_
    CLI:Commit
    CLI:Switch Connection	host1_session
	CLI:Test Ping	${VLAN10_IP2}	5	30	SOURCE_INTERFACE=backplane0.10

SUITE:Check Source and Destination MAC and IP and TCP/UDP Ports type
    Skip If	not ${IS_NSR}	LAG only in NSR
    CLI:Enter Path  /settings/switch_global/
    CLI:Set  lag_load_balance=source_and_destination_mac_and_ip_and_tcp|udp_ports
    CLI:Commit
    CLI:Switch Connection	host2_session
    CLI:Enter Path  /settings/switch_global/
    CLI:Set  lag_load_balance=source_and_destination_mac_and_ip_and_tcp|udp_ports
    CLI:Commit
    CLI:Switch Connection	host1_session
	CLI:Test Ping	${VLAN10_IP2}	5	30	SOURCE_INTERFACE=backplane0.10

SUITE:Check Source and Destination IP type
    Skip If	not ${IS_NSR}	LAG only in NSR
    CLI:Enter Path  /settings/switch_global/
    CLI:Set  lag_load_balance=source_and_destination_ip
    CLI:Commit
    CLI:Switch Connection	host2_session
    CLI:Enter Path  /settings/switch_global/
    CLI:Set  lag_load_balance=source_and_destination_ip
    CLI:Commit
    CLI:Switch Connection	host1_session
	CLI:Test Ping	${VLAN10_IP2}	5	30	SOURCE_INTERFACE=backplane0.10

SUITE:Add Lag Configuration with LACP type
    CLI:Switch Connection	host1_session
    CLI:Enter Path  /settings/switch_lag/
    CLI:Add
    CLI:Set  name=${LAG_NAME} id=1 type=${LAG_TYPE2}
    CLI:Set  lacp_system_priority=${LACP_PRIORITY}
    CLI:Set  ports=${VLAN_HOST_SWITCH1},${VLAN_HOST_SWITCH2}
    CLI:Commit
    ${OUTPUT}   CLI:Show
    Should Contain  ${OUTPUT}    ${LAG_NAME}
    CLI:Switch Connection	host2_session
    CLI:Enter Path  /settings/switch_lag/
    CLI:Add
    CLI:Set  name=${LAG_NAME} id=1 type=${LAG_TYPE2}
    CLI:Set  lacp_system_priority=${LACP_PRIORITY}
    CLI:Set  ports=${VLAN_SHARED_SWITCH1},${VLAN_SHARED_SWITCH2}
    CLI:Commit
    ${OUTPUT}   CLI:Show
    Should Contain  ${OUTPUT}   ${LAG_NAME}

CLI:Test Ping At Start
	[Documentation]  Test ping other ip address from current nodegrid (should be
	...	connected as admin)
	[Arguments]	${IP_ADDRESS}	${NUM_PACKETS}=10	${TIMEOUT}=60
	...	${SOURCE_INTERFACE}=${EMPTY}	${IPV6}=No	${NO_STICKY}=No

	${PING_COMMAND}=	Set Variable	ping ${IP_ADDRESS} -W ${TIMEOUT} -c ${NUM_PACKETS} -B
	IF	'${NO_STICKY}' != 'No'
		${PING_COMMAND}=	Set Variable	ping ${IP_ADDRESS} -W ${TIMEOUT} -c ${NUM_PACKETS}
	END
	${PING_COMMAND}=	Run Keyword If	'${SOURCE_INTERFACE}' != '${EMPTY}'
	...	Catenate	${PING_COMMAND}	${SPACE}-I ${SOURCE_INTERFACE}
	...	ELSE	Set Variable	${PING_COMMAND}
	${PING_COMMAND}=	Run Keyword If	'${IPV6}' != 'No'
	...	Catenate	${PING_COMMAND}	${SPACE}-6
	...	ELSE	Set Variable	${PING_COMMAND}

	Set Client Configuration	prompt=#
	CLI:Write	shell sudo su -
	Set Client Configuration	timeout=120s
	${OUTPUT}=	CLI:Write	${PING_COMMAND}
	Set Client Configuration	prompt=]#
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Write	exit

	Should Not Contain	${OUTPUT}	0 received

SUITE:Check If Netports Belong To Same Card
	[Arguments]	${PORT1}	${PORT2}
	${PORT_NUMBER1}	Fetch From Right	${PORT1}	netS
	${SLOT_PORT1}	Fetch From Left	${PORT_NUMBER1}	-
	${PORT_NUMBER2}	Fetch From Right	${PORT2}	netS
	${SLOT_PORT2}	Fetch From Left	${PORT_NUMBER2}	-
	${ARE_EQUAL}	Run Keyword And Return Status	Should Be Equal	${SLOT_PORT1}	${SLOT_PORT2}
	[Return]	${ARE_EQUAL}