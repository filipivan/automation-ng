*** Settings ***
Resource	../../init.robot
Documentation	Test the functionality of port mirroring feature thru CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	NON-CRITICAL	DEPENDENCE_SWITCH	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
${NETS_HOST}	${SWITCH_NETPORT_HOST_INTERFACE1}
${NETS_SHARED}	${SWITCH_NETPORT_SHARED_INTERFACE1}

${SFP_HOST}	${SWITCH_SFP_HOST_INTERFACE1}
${SFP_SHARED}	${SWITCH_SFP_SHARED_INTERFACE1}

${IP_ADDRESS_HOST}	10.1.100.100
${IP_ADDRESS_SHARED}	10.1.100.200

${BOTH_TRAFFIC}	both_from_bp0_to_bp1
${INGRESS_TRAFFIC}	rx_from_bp0_to_bp1
${EGRESS_TRAFFIC}	tx_from_bp0_to_bp1
*** Test Cases ***
Test sfp vlan is properly set and has traffic
	Skip If	not ${IS_SR}	Port mirroring feature is only for NSR
	SUITE:Setup VLANS	${SFP_HOST}	${SFP_SHARED}
	SUITE:Setup Network Connections
	SUITE:Monitor Traffic Via Tcpdump	backplane0

Test that backplane1 has no traffic before mirroring
	Skip If	not ${IS_SR}	Port mirroring feature is only for NSR
	${HAS_TRAFFIC}	Run Keyword and Return Status	SUITE:Monitor Traffic Via Tcpdump
	Should Not Be True	${HAS_TRAFFIC}

Test that backplane1 is mirroring traffic from backplane0 in both directions using sfp vlan
	Skip If	not ${IS_SR}	Port mirroring feature is only for NSR
	SUITE:Add Mirroring Rule	backplane1	both	${BOTH_TRAFFIC}	backplane0	enabled
	${BOTH}	SUITE:Monitor Traffic Via Tcpdump
	Should Contain	${BOTH}	ICMP echo reply
	Should Contain	${BOTH}	ICMP echo request
	[Teardown]	SUITE:Delete All Mirroring Rules

Test that backplane1 is mirroring only egress traffic from backplane0 using sfp vlan
	Skip If	not ${IS_SR}	Port mirroring feature is only for NSR
	SUITE:Add Mirroring Rule	backplane1	egress	${EGRESS_TRAFFIC}	backplane0	enabled
	${EGRESS}	SUITE:Monitor Traffic Via Tcpdump
	Should Not Contain	${EGRESS}	ICMP echo reply
	Should Contain	${EGRESS}	ICMP echo request
	[Teardown]	SUITE:Delete All Mirroring Rules

Test that backplane1 is mirroring only ingress traffic from backplane0 using sfp vlan
	Skip If	not ${IS_SR}	Port mirroring feature is only for NSR
	SUITE:Add Mirroring Rule	backplane1	ingress	${INGRESS_TRAFFIC}	backplane0	enabled
	${INGRESS}	SUITE:Monitor Traffic Via Tcpdump
	Should Contain	${INGRESS}	ICMP echo reply
	Should Not Contain	${INGRESS}	ICMP echo request
	[Teardown]	SUITE:Delete All Mirroring Rules

Test netS vlan is properly set and has traffic
	Skip If	not ${IS_SR}	Port mirroring feature is only for NSR
	[Setup]	SUITE:Delete Configuration
	SUITE:Setup VLANS	${NETS_HOST}	${NETS_SHARED}
	SUITE:Setup Network Connections
	SUITE:Monitor Traffic Via Tcpdump	backplane0

Test that backplane1 is mirroring traffic from backplane0 in both directions using netS vlan
	Skip If	not ${IS_SR}	Port mirroring feature is only for NSR
	SUITE:Add Mirroring Rule	backplane1	both	${BOTH_TRAFFIC}	backplane0	enabled
	${BOTH}	SUITE:Monitor Traffic Via Tcpdump
	Should Contain	${BOTH}	ICMP echo reply
	Should Contain	${BOTH}	ICMP echo request
	[Teardown]	SUITE:Delete All Mirroring Rules

Test that backplane1 is mirroring only egress traffic from backplane0 using netS vlan
	Skip If	not ${IS_SR}	Port mirroring feature is only for NSR
	SUITE:Add Mirroring Rule	backplane1	egress	${EGRESS_TRAFFIC}	backplane0	enabled
	${EGRESS}	SUITE:Monitor Traffic Via Tcpdump
	Should Not Contain	${EGRESS}	ICMP echo reply
	Should Contain	${EGRESS}	ICMP echo request
	[Teardown]	SUITE:Delete All Mirroring Rules

Test that backplane1 is mirroring only ingress traffic from backplane0 using netS vlan
	Skip If	not ${IS_SR}	Port mirroring feature is only for NSR
	SUITE:Add Mirroring Rule	backplane1	ingress	${INGRESS_TRAFFIC}	backplane0	enabled
	${INGRESS}	SUITE:Monitor Traffic Via Tcpdump
	Should Contain	${INGRESS}	ICMP echo reply
	Should Not Contain	${INGRESS}	ICMP echo request
	[Teardown]	SUITE:Delete All Mirroring Rules

*** Keywords ***
SUITE:Setup
	CLI:Open
	${IS_SR}=	CLI:Is SR System
	Set Suite Variable	${IS_SR}
	Skip If	not ${IS_SR}	Port mirroring feature is only for NSR
	Skip If	not ${HAS_HOSTSHARED}	The current build does not have a shared device
	CLI:Open	session_alias=shared	HOST_DIFF=${HOSTSHARED}

SUITE:Teardown
	Skip If	not ${IS_SR}	Port mirroring feature is only for NSR
	SUITE:Delete Configuration
 	CLI:Close Connection

SUITE:Setup VLANS
	[Arguments]	${HOST_PORT}	${SHARED_PORT}
	CLI:Switch Connection	default
	CLI:Add VLAN	100	UNTAGGED_PORTS=backplane0,${HOST_PORT}
	CLI:Edit VLAN	100	TAGGED_PORTS=backplane0,${HOST_PORT}
	CLI:Enable Switch Interface	INTERFACE=${HOST_PORT}

	CLI:Switch Connection	shared
	CLI:Add VLAN	100	UNTAGGED_PORTS=backplane0,${SHARED_PORT}
	CLI:Edit VLAN	100	TAGGED_PORTS=backplane0,${SHARED_PORT}
	CLI:Enable Switch Interface	INTERFACE=${SHARED_PORT}

	[Teardown]	CLI:Cancel	Raw

SUITE:Delete VLANS
	CLI:Switch Connection	default
	CLI:Delete VLAN	100
	CLI:Disable Switch Interface	${SFP_HOST}

	CLI:Switch Connection	shared
	CLI:Delete VLAN	100
	CLI:Disable Switch Interface	${SFP_SHARED}

SUITE:Setup Network Connections
	CLI:Switch Connection	default
	CLI:Add Static IPv4 Network Connection	vlan100	${IP_ADDRESS_HOST}	TYPE=vlan	INTERFACE=backplane0	VLAN_ID=100

	CLI:Switch Connection	shared
	CLI:Add Static IPv4 Network Connection	vlan100	${IP_ADDRESS_SHARED}	TYPE=vlan	INTERFACE=backplane0	VLAN_ID=100

SUITE:Delete Connections
	CLI:Switch Connection	default
	CLI:Delete Network Connections	vlan100

	CLI:Switch Connection	shared
	CLI:Delete Network Connections	vlan100

SUITE:Monitor Traffic Via Tcpdump
	[Arguments]	${INTERFACE}=backplane1
	CLI:Switch Connection	shared
	CLI:Connect As Root	HOST_DIFF=${HOSTSHARED}
	Write	ping ${IP_ADDRESS_HOST} -I backplane0.100 -W 40
	#the reason is not using CLI:Test Ping is because the ping should continue being executed in shared while
	#tcpdump is being executed in default (host)
	CLI:Switch Connection	default
	CLI:Connect As Root
	${RESULT}	CLI:Write	timeout 40 tcpdump --number -c 20 -i ${INTERFACE} icmp
	Should Contain	${RESULT}	20 packets captured
	Should Contain	${RESULT}	0 packets dropped by kernel
	[Return]	${RESULT}

SUITE:Add Mirroring Rule
	[Arguments]	${DESTINATION}	${DIRECTION}	${NAME}	${SOURCES}	${STATUS}
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/switch_port_mirroring
	CLI:Add
	CLI:Set	destination=${DESTINATION} direction=${DIRECTION} session_name=${NAME} sources=${SOURCES} status=${STATUS}
	CLI:Commit

SUITE:Delete All Mirroring Rules
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/switch_port_mirroring
	CLI:Delete All

SUITE:Delete Configuration
	SUITE:Delete Connections
	SUITE:Delete VLANS