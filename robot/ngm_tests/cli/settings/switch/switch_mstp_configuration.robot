*** Settings ***
Resource	../../init.robot
Documentation	Tests for configuring NSR switch with MSTP related attributes
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	    DEPENDENCE_SWITCH	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SWITCH_INTERFACE}	sfp0

*** Test Cases ***
Test add new MSTP instance
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_mstp
	CLI:Add
	CLI:Set	mst_instance_id=255 vlan=1 mstp_priority=4096
	CLI:Commit
	${INSTANCES}	CLI:Show
	Should Contain	${INSTANCES}	255

Test change instance vlan and priority with valid values
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_mstp/255/vlan_priority
	CLI:Set	vlan=1-2 mstp_priority=32768
	CLI:Commit
	${INSTANCE_255}	CLI:Show Settings
	Should Contain	${INSTANCE_255}	/settings/switch_mstp/255/vlan_priority vlan=1-2
	Should Contain	${INSTANCE_255}	/settings/switch_mstp/255/vlan_priority mstp_priority=32768

Test change instance vlan and priority with invalid values
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_mstp/255/vlan_priority
	${ERROR_PVID}	CLI:Write	set vlan=32	Raw
	Should Contain	${ERROR_PVID}	Error: vlan: Invalid PVID: VLAN 32 not configured
	${ERROR_PRIORITY}	CLI:Write	set mstp_priority=1234	Raw
	Should Contain	${ERROR_PRIORITY}	Error: Invalid value: 1234 for parameter: mstp_priority
	[Teardown]	CLI:Cancel	Raw

Add duplicate instance
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_mstp
	CLI:Add
	CLI:Set	mst_instance_id=255
	${ERROR}	CLI:Commit	Raw
	Should Contain	${ERROR}	Error: mst_instance_id: Entry already exists.
	[Teardown]	CLI:Cancel	Raw

Add instance with same vlans
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_mstp
	CLI:Add
	CLI:Set	mst_instance_id=254 vlan=2	#the same vlan as instance 255
	CLI:Commit
	CLI:Enter Path	/settings/switch_mstp/254/vlan_priority
	${VLAN_254}	CLI:Show Settings
	Should Contain	${VLAN_254}	/settings/switch_mstp/254/vlan_priority vlan=2
	CLI:Enter Path	/settings/switch_mstp/255/vlan_priority
	${VLAN_255}	CLI:Show Settings
	Should Contain	${VLAN_255}	/settings/switch_mstp/255/vlan_priority vlan=1
	Should Not Contain	${VLAN_255}	/settings/switch_mstp/255/vlan_priority vlan=1-2

Test delete vlan that belongs to an instance
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Add VLAN	100
	CLI:Enter Path	/settings/switch_mstp/254/vlan_priority
	CLI:Set	vlan=2,100
	CLI:Commit
	CLI:Delete VLAN	100
	CLI:Enter Path	/settings/switch_mstp/254/vlan_priority
	${ONLY_VLAN2}	CLI:Show Settings
	Should Contain	${ONLY_VLAN2}	/settings/switch_mstp/254/vlan_priority vlan=2
	Should Not Contain	${ONLY_VLAN2}	100
	[Teardown]	CLI:Cancel	Raw

Test edit STP cost and priority of a interface in an instance
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_mstp/255/interfaces/${SWITCH_INTERFACE}
	CLI:Set	mstp_cost=4096 mstp_priority=32
	CLI:Commit
	CLI:Enter Path	/settings/switch_mstp/255/interfaces/${SWITCH_INTERFACE}
	${SWITCH_INTERFACE_255}	CLI:Show Settings
	Should Contain	${SWITCH_INTERFACE_255}	/settings/switch_mstp/255/interfaces/${SWITCH_INTERFACE} mstp_priority=32
	Should Contain	${SWITCH_INTERFACE_255}	/settings/switch_mstp/255/interfaces/${SWITCH_INTERFACE} mstp_cost=4096

Test delete MSTP instance
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	SUITE:Delete MSTP Instance	255
	SUITE:Delete MSTP Instance	254

Test delete default MSTP instance
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	CLI:Enter Path	/settings/switch_mstp
	CLI:Delete If Exists	0
	${INSTANCES}	CLI:Show
	Should Contain	${INSTANCES}	0

*** Keywords ***

SUITE:Setup
	CLI:Open
	${IS_NSR}	CLI:Is Net SR
	Set Suite Variable	${IS_NSR}
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	SUITE:Delete MSTP Instance	255
	SUITE:Delete MSTP Instance	254

SUITE:Teardown
	Skip If	not ${IS_NSR}	STP Protocol is only supported in NSR
	SUITE:Delete MSTP Instance	255
	SUITE:Delete MSTP Instance	254
	CLI:Close Connection

SUITE:Delete MSTP Instance
	[Arguments]	${MSTP_ID}
	CLI:Enter Path	/settings/switch_mstp
	CLI:Delete If Exists	${MSTP_ID}
	${INSTANCES}	CLI:Show
	Should Not Contain	${INSTANCES}	${MSTP_ID}