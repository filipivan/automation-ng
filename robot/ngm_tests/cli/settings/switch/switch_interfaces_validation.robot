*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Switch Interfaces page, ls, show, add, etc... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	DEPENDENCE_SWITCH	EXCLUDEIN3_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
@{CRED_AUTH_METHOD}     MD5   TLS    PEAP    TTLS
@{CRED_INNER_AUTH}      MD5   PAP    CHAP    MSCHAP    MSCHAPV2
@{CRED_NAMES}           test_cred_md5  test_cred_tls  test_cred_peap_md5  test_cred_peap_mschapv2  test_cred_ttls_md5
                        ...  test_cred_ttls_pap  test_cred_ttls_chap  test_cred_ttls_mschap  test_cred_ttls_mschapv2
@{CRED_ANONYMOUS}       test_cred_anonymous_0   test_cred_anonymous_1   test_cred_anonymous_2   test_cred_anonymous_3
                        ...  test_cred_anonymous_4  test_cred_anonymous_5  test_cred_anonymous_6
@{PROF_TYPES}           internal_eap_server   radius_server  supplicant
@{PROF_NAMES}           test_prof_internal_eap_server  test_prof_radius_server   test_prof_supplicant
${RETRANSMIT_INTERVAL}  15
@{ALL_VALUES}=	        ${WORD}     ${ONE_WORD}     ${TWO_WORD}     ${THREE_WORD}
                        ...     ${SEVEN_WORD}   ${EIGHT_WORD}   ${ELEVEN_WORD}  ${NUMBER}   ${ONE_NUMBER}
                        ...     ${TWO_NUMBER}   ${THREE_NUMBER}     ${SEVEN_NUMBER}     ${EIGHT_NUMBER}
                        ...     ${ELEVEN_NUMBER}    ${WORD_AND_NUMBER}  ${NUMBER_AND_WORD}  ${POINTS}
                        ...     ${ONE_POINTS}   ${TWO_POINTS}   ${THREE_POINTS}	${SEVEN_POINTS}
                        ...     ${EIGHT_POINTS}     ${ELEVEN_POINTS}    ${WORD_AND_POINTS}  ${NUMBER_AND_POINTS}
                        ...     ${POINTS_AND_NUMBER}    ${POINTS_AND_WORD}

*** Test Cases ***
Test available commands after send tab-tab
    Skip If   ${HAS_SWITCH} == ${FALSE}  System is not from SR family with Switch
	CLI:Enter Path	/settings/switch_interfaces
	CLI:Test Available Commands	edit	hostname	revert	shutdown
	...	apply_settings	event_system_audit	ls	save_settings	software_upgrade
	...	cd	event_system_clear	pwd	shell	system_certificate
	...	change_password	exit	quit	show	system_config_check
	...	commit	factory_settings	reboot	show_settings	whoami

Test visible fields for show command
    Skip If       ${HAS_SWITCH} == ${FALSE}  System is not from SR family with Switch
	CLI:Enter Path	/settings/switch_interfaces
	CLI:Test Show Command Regexp	interface\\s+status\\s+description\\s+speed\\s+port\\s+vlan\\sid\\s+jumbo\\sframe
	Run Keyword If	${HAS_SFP}	CLI:Test Show Command Regexp	sfp0\\s+Enabled\\s+\\w+\\s+\\w+\\s+\\d+\\s+(Enabled)|(Disabled)
	Run Keyword If	${HAS_NETS}	CLI:Test Show Command Regexp	${NETS_NAME}\\s+Enabled\\s+\\w+\\s+\\w+\\s+\\d+\\s+(Enabled)|(Disabled)

Test show_settings command
    Skip If       ${HAS_SWITCH} == ${FALSE}  System is not from SR family with Switch
    Skip If       ${HAS_SFP} == ${FALSE}    System is from SR family, but doesn't have sfp interface
	CLI:Enter Path	/settings/switch_interfaces/
	Run Keyword If	${HAS_SFP}	SUITE:Check Settings For Sfp
	Run Keyword If	${HAS_NETS}	SUITE:Check Settings For netS

Test Show Column "802.1x status" On Switch Interfaces Table
    [Tags]                  CLI    EXCLUDEIN3_2  EXCLUDEIN4_2
    Skip If       ${IS_SR} == ${FALSE}    System is not NSR
    CLI:Enter Path          /settings/switch_interfaces/
    ${OUTPUT}=              CLI:Show
    Should Match Regexp     ${OUTPUT}   802.1x\\s+status

Test Show "enable_802.1x = no" On sfp0 Switch Interface
    [Tags]                  CLI    EXCLUDEIN3_2  EXCLUDEIN4_2
    Skip If       ${IS_SR} == ${FALSE}    System is not NSR
    CLI:Enter Path          /settings/switch_interfaces/sfp0/
    CLI:Test Show Command   enable_802.1x = no

Test Invalid Set On "enable_802.1x" On sfp0 Switch Interface
    [Tags]                      CLI    EXCLUDEIN3_2  EXCLUDEIN4_2
    Skip If       ${IS_SR} == ${FALSE}    System is not NSR
    CLI:Enter Path          /settings/switch_interfaces/sfp0/
    CLI:Test Set Field Invalid Options   enable_802.1x   yes
    ...  Error: 802.1x_profile: 802.1x Profile needs to be configured first

Test valid Set On "enable_802.1x" On sfp0 Switch Interface
    [Tags]                      CLI      EXCLUDEIN4_2
    Skip If           ${IS_SR} == ${FALSE}    System is not NSR
    SUITE:Add Configurations To 802.1x Network
    SUITE:Enable Interface And 802.1x On Switch Interfaces    sfp0   ${PROF_NAMES}[0]
    [Teardown]     Run Keywords   Skip If   ${IS_SR} == ${FALSE}   System is not NSR   AND
    ...   SUITE:Delete Configurations If Exists To 802.1x Network

Test Show "enable_802.1x = no" On netSx Switch Interface
    [Tags]                      CLI      EXCLUDEIN4_2
    Skip If       ${IS_SR} == ${FALSE}    System is not NSR
    Skip If       ${HAS_NETS} == ${FALSE}  System is NSR, but doesn't have net interface
    CLI:Enter Path          /settings/switch_interfaces/${NETS_NAME}
    CLI:Test Show Command   enable_802.1x = no

Test Invalid Set On "enable_802.1x" On netSx Switch Interface
    [Tags]                      CLI    EXCLUDEIN4_2
    Skip If       ${IS_SR} == ${FALSE}    System is not NSR
    Skip If       ${HAS_NETS} == ${FALSE}  System is NSR, but doesn't have net interface
    CLI:Enter Path          /settings/switch_interfaces/${NETS_NAME}
    CLI:Test Set Field Invalid Options   enable_802.1x   yes
    ...  Error: 802.1x_profile: 802.1x Profile needs to be configured first
    [Teardown]     Run Keywords   Skip If   ${IS_SR} == ${FALSE}   System is not NSR   AND
    ...   SUITE:Delete Configurations If Exists To 802.1x Network

Test valid Set On "enable_802.1x" On netSx Switch Interface
    [Tags]                      CLI      EXCLUDEIN4_2
    Skip If           ${IS_SR} == ${FALSE}    System is not NSR
    Skip If           ${HAS_NETS} == ${FALSE}  System is NSR, but doesn't have net interface
    SUITE:Add Configurations To 802.1x Network
    SUITE:Enable Interface And 802.1x On Switch Interfaces    ${NETS_NAME}   ${PROF_NAMES}[0]
    [Teardown]     Run Keywords   Skip If   ${IS_SR} == ${FALSE}   System is not NSR   AND
    ...   SUITE:Delete Configurations If Exists To 802.1x Network

Test valid values to field mstp_status
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	Skip If	not ${IS_SR}	MSTP just in NSR
	CLI:Enter Path	/settings/switch_interfaces/${NETS_NAME}
	CLI:Test Set Validate Normal Fields	mstp_status	enabled	disabled

Test invalid values to field mstp_status
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	Skip If	not ${IS_SR}	MSTP just in NSR
	CLI:Enter Path	/settings/switch_interfaces/${NETS_NAME}
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	mstp_status	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: mstp_status
	END

Test valid values to field mstp_bpdu_guard
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	Skip If	not ${IS_SR}	MSTP just in NSR
	CLI:Enter Path	/settings/switch_interfaces/${NETS_NAME}
	CLI:Test Set Validate Normal Fields	mstp_bpdu_guard	on	off

Test invalid values to field mstp_bpdu_guard
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	Skip If	not ${IS_SR}	MSTP just in NSR
	CLI:Enter Path	/settings/switch_interfaces/${NETS_NAME}
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	mstp_bpdu_guard	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: mstp_bpdu_guard
	END

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Check System Type
	SUITE:Check If System Has Switch And Interfaces
	Skip If    ${IS_SR} == ${FALSE}      System is not NSR
	Run Keyword If	${HAS_NETS}	SUITE:Get Interface Net's Name
    Run Keyword If	not ${IS_BSR}	SUITE:Delete Configurations If Exists To 802.1x Network

SUITE:Teardown
    Run Keyword If      ${IS_SR} == ${FALSE}  CLI:Close Connection
    Skip If   ${IS_SR} == ${FALSE}     System is not NSR
    Run Keyword If	not ${IS_BSR}	SUITE:Delete Configurations If Exists To 802.1x Network
    CLI:Close Connection

SUITE:Check If System Has Switch And Interfaces
    ${OUTPUT}=              CLI:Enter Path  /settings/switch_interfaces/    RAW_Mode=Yes
    ${HAS_SWITCH}=          Run Keyword And Return Status   Should Not Contain  ${OUTPUT}
    ...     Error: Invalid path: switch_interfaces
    Set Suite Variable      ${HAS_SWITCH}
    ${OUTPUT}=              CLI:Ls
    ${INTERFACES}=          Remove String Using Regexp       ${OUTPUT}    ['/']
    ${HAS_SFP}=             Run Keyword And Return Status   Should Contain  ${INTERFACES}   sfp
    Set Suite Variable      ${HAS_SFP}
    ${HAS_NETS}=            Run Keyword And Return Status   Should Contain  ${INTERFACES}   netS
    Set Suite Variable      ${HAS_NETS}

SUITE:Check System Type
    ${IS_SR}=      CLI:Is SR System
    Set Suite Variable   ${IS_SR}
    ${IS_BSR}=	CLI:Is Bold SR
    Set Suite Variable	${IS_BSR}
    [Teardown]   Skip If    ${IS_SR} == ${FALSE}      System is not NSR

SUITE:Get Interface Net's Name
    CLI:Enter Path          /settings/switch_interfaces/
    ${OUTPUT}=              CLI:Ls
    ${INTERFACES}=          Remove String Using Regexp    ${OUTPUT}    ['/']
    @{GET_LINES}=           Split To Lines   ${INTERFACES}
    ${NETS_NAME}=           Get From List    ${GET_LINES}    1
    Set Suite Variable      ${NETS_NAME}

SUITE:Delete Configurations If Exists To 802.1x Network
    CLI:Cancel                                  Raw
    Run Keyword If	${HAS_SFP}	SUITE:Disable 802.1x On Switch Interfaces    sfp0
    Run Keyword If	${HAS_NETS}	SUITE:Disable 802.1x On Switch Interfaces    ${NETS_NAME}
    SUITE:Delete 802.1x Credentials
    SUITE:Delete 802.1x Profiles
    [Teardown]       CLI:Cancel  Raw

SUITE:Disable 802.1x On Switch Interfaces
    [Arguments]                 ${INTERFACE}
    CLI:Enter Path              /settings/switch_interfaces/${INTERFACE}
    CLI:Set                     status=disabled enable_802.1x=no
    CLI:Commit
    [Teardown]                  CLI:Cancel  Raw

SUITE:Delete 802.1x Credentials
    FOR	    ${CRED_NAME}     IN   @{CRED_NAMES}
		CLI:Enter Path          /settings/802.1x/credentials/
		CLI:Delete If Exists    ${CRED_NAME}
		CLI:Commit
		CLI:Cancel  Raw
	END
    [Teardown]         CLI:Cancel  Raw

SUITE:Delete 802.1x Profiles
    FOR	    ${PROF_NAME}     IN   @{PROF_NAMES}
		CLI:Enter Path          /settings/802.1x/profiles/
		CLI:Delete If Exists    ${PROF_NAME}
		CLI:Commit
		CLI:Cancel  Raw
	END
    [Teardown]         CLI:Cancel  Raw

SUITE:Enable Interface And 802.1x On Switch Interfaces
    [Arguments]                 ${INTERFACE}    ${PROF_NAME}
    CLI:Enter Path              /settings/switch_interfaces/${INTERFACE}
    CLI:Set                     status=enabled enable_802.1x=yes 802.1x_profile=${PROF_NAME}
    CLI:Commit
    [Teardown]                  CLI:Cancel  Raw

SUITE:Add 802.1x Credential
    [Arguments]            ${CRED_NAME}   ${CRED_AUTH_METHOD}   ${CRED_ANONYMOUS}=${EMPTY}  ${CRED_INNER_AUTH}=${EMPTY}
    CLI:Enter Path         /settings/802.1x/credentials/
    CLI:Add
    CLI:Set                username=${CRED_NAME} password=${QA_PASSWORD}
    CLI:Set                authentication_method=${CRED_AUTH_METHOD}
    Run Keyword If         '${CRED_AUTH_METHOD}' == 'PEAP'
    ...  CLI:Set                anonymous_identity=${CRED_ANONYMOUS} inner_authentication=${CRED_INNER_AUTH}
    Run Keyword If         '${CRED_AUTH_METHOD}' == 'TTLS'
    ...  CLI:Set                anonymous_identity=${CRED_ANONYMOUS} inner_authentication=${CRED_INNER_AUTH}
    CLI:Commit
    CLI:Test Show Command  ${CRED_NAME}
    [Teardown]             CLI:Cancel  Raw

SUITE:Add 802.1x Profiles
    ${SELECT_USERS}=         Catenate    SEPARATOR=,    @{CRED_NAMES}
    FOR	    ${PROF_TYPE}     IN   @{PROF_TYPES}
		CLI:Enter Path     /settings/802.1x/profiles/
		CLI:Add
		Run Keyword If     '${PROF_TYPE}' == 'internal_eap_server'  Run Keywords
		...  CLI:Set  name=${PROF_NAMES}[0] type=${PROF_TYPE} retransmit_interval=${RETRANSMIT_INTERVAL}  AND
		...  CLI:Set  select_users=${CRED_NAMES}[0]
		...  AND   CLI:Commit
		Run Keyword If     '${PROF_TYPE}' == 'radius_server'   Run Keywords
		...  CLI:Set  name=${PROF_NAMES}[1] type=${PROF_TYPE}  AND
		...  CLI:Set  ip_address=${RADIUSSERVER2} port_number=${RADIUSSERVER2_PORT}
		...  AND   CLI:Set   shared_secret=${RADIUSSERVER2_SECRET} retransmit_interval=${RETRANSMIT_INTERVAL}
		...  AND   CLI:Commit
		Run Keyword If     '${PROF_TYPE}' == 'supplicant'    Run Keywords
		...  CLI:Set  name=${PROF_NAMES}[2] type=${PROF_TYPE} user=${CRED_NAMES}[0]
		...  AND   CLI:Commit
		CLI:Cancel  Raw
	END
    [Teardown]         CLI:Cancel  Raw

SUITE:Add Configurations To 802.1x Network
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[2]  ${CRED_AUTH_METHOD}[2]  ${CRED_ANONYMOUS}[0]  ${CRED_INNER_AUTH}[0]
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[5]  ${CRED_AUTH_METHOD}[3]  ${CRED_ANONYMOUS}[3]  ${CRED_INNER_AUTH}[1]
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[6]  ${CRED_AUTH_METHOD}[3]  ${CRED_ANONYMOUS}[4]  ${CRED_INNER_AUTH}[2]
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[7]  ${CRED_AUTH_METHOD}[3]  ${CRED_ANONYMOUS}[5]  ${CRED_INNER_AUTH}[3]
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[8]  ${CRED_AUTH_METHOD}[3]  ${CRED_ANONYMOUS}[6]  ${CRED_INNER_AUTH}[4]
    SUITE:Add 802.1x Profiles

SUITE:Check Settings For Sfp
	${OUTPUT}=	CLI:Write	show_settings
	Should Match Regexp	${OUTPUT}	/settings/switch_interfaces/sfp0 status=(enabled)|(disabled)
	Should Match Regexp	${OUTPUT}	/settings/switch_interfaces/sfp0 speed=auto|100m|10g|10m|1g
	Should Match Regexp	${OUTPUT}	/settings/switch_interfaces/sfp0 port_vlan_id=\\d+
	Should Match Regexp	${OUTPUT}	/settings/switch_interfaces/sfp0 jumbo_frame=(disabled)|(enabled)
	Should Match Regexp	${OUTPUT}	/settings/switch_interfaces/sfp0 enable_lldp_advertising_and_reception_through_this_interface=no|yes

SUITE:Check Settings For netS
	${OUTPUT}=	CLI:Write	show_settings
	Should Match Regexp	${OUTPUT}	/settings/switch_interfaces/${NETS_NAME}status=(enabled)|(disabled)
	Should Match Regexp	${OUTPUT}	/settings/switch_interfaces/${NETS_NAME} speed=auto|100m|10g|10m|1g
	Should Match Regexp	${OUTPUT}	/settings/switch_interfaces/${NETS_NAME} port_vlan_id=\\d+
	Should Match Regexp	${OUTPUT}	/settings/switch_interfaces/${NETS_NAME} jumbo_frame=(disabled)|(enabled)
	Should Match Regexp	${OUTPUT}	/settings/switch_interfaces/${NETS_NAME} enable_lldp_advertising_and_reception_through_this_interface=no|yes