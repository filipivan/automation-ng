*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Switch_lag_configuration
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI DEPENDENCE_SWITCH   EXCLUDEIN3_2    EXCLUDEIN4_2    EXCLUDEIN5_0    EXCLUDEIN5_2
Default Tags	CLI	SSH	SHOW	LAG

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${VALID_ID}     110
${LAG1}     lag1
${LAG2}    lag2
${TYPE1}    static
${TYPE2}    lacp
${LACP_PRIORITY}    65535

*** Test Cases ***
Test case to Add LAG configuration
    Skip If	not ${IS_NSR}	LAG only in NSR
    CLI:Enter Path  /settings/switch_lag/
    CLI:Add
    CLI:Set  name=${LAG1} id=1 type=static
    CLI:Set  ports=backplane0,backplane1
    CLI:Commit
    ${OUTPUT}   CLI:Show
    Should Contain  ${OUTPUT}   ${LAG1}

Test case to edit LAG configuration
    Skip If	not ${IS_NSR}	LAG only in NSR
    ${ARE_IN_SAME_CARD}	SUITE:Check If Both Switch Interfaces Are In The Same Card
    Skip If	not ${ARE_IN_SAME_CARD}	LAG can only be created with interfaces from the same network card
    CLI:Enter Path  /settings/switch_lag/${LAG1}
    CLI:Set  name=${LAG2}
    CLI:Set  ports=${SWITCH_NETPORT_HOST_INTERFACE1},${SWITCH_NETPORT_HOST_INTERFACE2}
    CLI:Commit
    ${OUTPUT}   CLI:Show
    Should Contain  ${OUTPUT}   ${LAG2}
    CLI:Enter Path  /settings/switch_lag/
    CLI:Add
    CLI:Set  name=${LAG1} id=2 type=lacp
    CLI:Set  lacp_system_priority=${LACP_PRIORITY}
    CLI:Commit
    ${OUTPUT}   CLI:Show
    Should Contain  ${OUTPUT}   ${LAG1}

Test case to Delete LAG configuration
    Skip If	not ${IS_NSR}	LAG only in NSR
    CLI:Enter Path  /settings/switch_lag/
    CLI:Delete If Exists	${LAG1}
    CLI:Delete If Exists	${LAG2}

*** Keywords ***
SUITE:Setup
    CLI:Open
	${IS_NSR}=	CLI:Is Net SR
	Set Suite Variable	${IS_NSR}
	Skip If	not ${IS_NSR}	LAG only in NSR

SUITE:Teardown
	CLI:Close Connection

SUITE:Check If Both Switch Interfaces Are In The Same Card
	${1ST_INTERFACE_CARD}=	Fetch From Left	${SWITCH_NETPORT_HOST_INTERFACE1}	-
	${2ND_INTERFACE_CARD}=	Fetch From Left	${SWITCH_NETPORT_HOST_INTERFACE2}	-
	IF	'${1ST_INTERFACE_CARD}' == '${2ND_INTERFACE_CARD}'
		Return From Keyword	${TRUE}
	END
	[Return]	${FALSE}