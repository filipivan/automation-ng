*** Settings ***
Resource        ../../init.robot
Documentation   Tests of configuration on Switch ACL through the CLI
Metadata        Version	1.0
Metadata        Executed At	${HOST}
Force Tags      CLI   DEPENDENCE_SWITCH   EXCLUDEIN3_2    EXCLUDEIN4_2

Suite Setup	    SUITE:Setup
Suite Teardown  SUITE:Teardown

*** Variables ***
${ACL_ING_NAME}         test_switch_acl_ingress
${ACL_EGR_NAME}         test_switch_acl_egress
${ACL_ING_DIRECTION}    ingress
${ACL_EGR_DIRECTION}    egress
${NUMBER_ZERO}          0
${NUMBER_ONE}           1
${NUMBER_TWO}           2
@{VALID_IPS}            192.168.15.60  192.168.16.85  192.168.17.55  0.0.0.0  255.255.255.255
${DEFAULT_ACTION}       permit
${DENY_ACTION}          deny
${NONE_DIRECTION}       none

*** Test Cases ***
Test If The Same Configuration Already Exists In Another Rule
    Skip If                   ${IS_NSR} == ${FALSE}      System is not NSR
    ${NUMBER_OF_RETRIES}=  create list  ${NUMBER_ZERO}  ${NUMBER_ONE}
    SUITE:Add Rule                      ${ACL_ING_NAME}  ${NUMBER_ZERO}
    CLI:Add
    CLI:Test Set Field Invalid Options  rule_id  ${NUMBER_ONE}
    ...  ERROR=Error: The same configuration already exists in rule ${NUMBER_ZERO}  save=yes
    [Teardown]  Run Keywords            CLI:Cancel  Raw  AND  CLI:Delete If Exists  ${NUMBER_ZERO}  ${NUMBER_ONE}

Test Delete Rule Configurated On Feature ACL
    Skip If             ${IS_NSR} == ${FALSE}      System is not NSR
    SUITE:Add Rule                ${ACL_ING_NAME}  ${NUMBER_ZERO}
    CLI:Test Show Command Regexp  ${NUMBER_ZERO}\\s+permit
    CLI:Delete                    ${NUMBER_ZERO}
    CLI:Test Not Show Command     ${NUMBER_ZERO}
    [Teardown]  Run Keywords      CLI:Cancel  Raw  AND  CLI:Delete If Exists  ${NUMBER_ZERO}  ${NUMBER_ONE}

Test Configuration Down And Up Rule On Feature ACL
    Skip If         ${IS_NSR} == ${FALSE}      System is not NSR
    SUITE:Add Rule            ${ACL_ING_NAME}  ${NUMBER_ZERO}
    SUITE:Add Rule            ${ACL_ING_NAME}  ${NUMBER_ONE}    ${DENY_ACTION}
    ${FIRST_LINE}  ${SECOND_LINE}  ${THIRD_LINE}=  SUITE:Gets The First, Second And Third Rows Of A Simple Table
    Should Match Regexp       ${FIRST_LINE}    ${NUMBER_ZERO}\\s+permit
    Should Match Regexp       ${SECOND_LINE}   ${NUMBER_ONE}\\s+deny
    CLI:Write                 down_rule ${NUMBER_ZERO}
    ${FIRST_LINE}  ${SECOND_LINE}  ${THIRD_LINE}=  SUITE:Gets The First, Second And Third Rows Of A Simple Table
    Should Match Regexp       ${FIRST_LINE}    ${NUMBER_ZERO}\\s+deny
    Should Match Regexp       ${SECOND_LINE}   ${NUMBER_ONE}\\s+permit
    CLI:Write                 up_rule ${NUMBER_ONE}
    ${FIRST_LINE}  ${SECOND_LINE}  ${THIRD_LINE}=  SUITE:Gets The First, Second And Third Rows Of A Simple Table
    Should Match Regexp       ${FIRST_LINE}    ${NUMBER_ZERO}\\s+permit
    Should Match Regexp       ${SECOND_LINE}   ${NUMBER_ONE}\\s+deny
    [Teardown]  Run Keywords  CLI:Cancel  Raw  AND  CLI:Delete If Exists  ${NUMBER_ZERO}  ${NUMBER_ONE}

Test Delete Rule Between Another Two Rules
    Skip If           ${IS_NSR} == ${FALSE}      System is not NSR
    SUITE:Add Rule              ${ACL_ING_NAME}  ${NUMBER_ZERO}
    SUITE:Add Rule              ${ACL_ING_NAME}  ${NUMBER_ONE}    ${DENY_ACTION}
    SUITE:Add Rule              ${ACL_ING_NAME}  ${NUMBER_TWO}  ${DENY_ACTION}    ${VALID_IPS}[0]
    ${FIRST_LINE}  ${SECOND_LINE}  ${THIRD_LINE}=  SUITE:Gets The First, Second And Third Rows Of A Simple Table
    Should Match Regexp         ${FIRST_LINE}    ${NUMBER_ZERO}\\s+permit
    Should Match Regexp         ${SECOND_LINE}   ${NUMBER_ONE}\\s+deny
    Should Match Regexp         ${THIRD_LINE}    ${NUMBER_TWO}\\s+deny\\s+${VALID_IPS}[0]
    CLI:Delete                  ${NUMBER_ONE}
    ${FIRST_LINE}  ${SECOND_LINE}  ${THIRD_LINE}=  SUITE:Gets The First, Second And Third Rows Of A Simple Table
    Should Match Regexp         ${FIRST_LINE}    ${NUMBER_ZERO}\\s+permit
    Should Match Regexp         ${SECOND_LINE}   ${NUMBER_TWO}\\s+deny\\s+${VALID_IPS}[0]
    [Teardown]  Run Keywords    CLI:Cancel  Raw  AND  CLI:Delete If Exists  ${NUMBER_ZERO}  ${NUMBER_ONE}  ${NUMBER_TWO}

Test Configuration ACL Ingress And ACL Egress On Interface Sfp0
    Skip If    ${IS_NSR} == ${FALSE}      System is not NSR
    SUITE:Configuration ACL Ingress And ACL Egress On Interface Sfp0    ${ACL_ING_NAME}   ${ACL_EGR_NAME}
    ${FIRST_LINE}  ${SECOND_LINE}  ${THIRD_LINE}=  SUITE:Gets The First, Second And Third Rows Of A Simple Table
    Should Match Regexp     ${FIRST_LINE}    ${ACL_ING_NAME}\\s+${ACL_ING_DIRECTION}\\s+sfp0\\s+${NUMBER_ZERO}
    Should Match Regexp     ${SECOND_LINE}   ${ACL_EGR_NAME}\\s+${ACL_EGR_DIRECTION}\\s+sfp0\\s+${NUMBER_ZERO}
    SUITE:Configuration ACL Ingress And ACL Egress On Interface Sfp0    ${NONE_DIRECTION}    ${NONE_DIRECTION}
    ${FIRST_LINE}  ${SECOND_LINE}  ${THIRD_LINE}=  SUITE:Gets The First, Second And Third Rows Of A Simple Table
    Should Match Regexp     ${FIRST_LINE}    ${ACL_ING_NAME}\\s+${ACL_ING_DIRECTION}\\s+${NUMBER_ZERO}
    Should Match Regexp     ${SECOND_LINE}   ${ACL_EGR_NAME}\\s+${ACL_EGR_DIRECTION}\\s+${NUMBER_ZERO}
    [Teardown]              SUITE:Teardown Test Configuration ACL Ingress And ACL Egress On Interface Sfp0

*** Keywords ***
SUITE:Setup
    CLI:Open
    SUITE:Check If Is NSR
    Skip If   ${IS_NSR} == ${FALSE}      System is not NSR
    SUITE:Delete ACL    ${ACL_ING_NAME}
    SUITE:Delete ACL    ${ACL_EGR_NAME}
    SUITE:Add ACL       ${ACL_ING_NAME}     ${ACL_ING_DIRECTION}
    SUITE:Add ACL       ${ACL_EGR_NAME}     ${ACL_EGR_DIRECTION}

SUITE:Teardown
    Run Keyword If      ${IS_NSR} == ${FALSE}      CLI:Close Connection
    Skip If   ${IS_NSR} == ${FALSE}      System is not NSR
    SUITE:Delete ACL    ${ACL_ING_NAME}
    SUITE:Delete ACL    ${ACL_EGR_NAME}
    CLI:Close Connection

SUITE:Add ACL
    [Arguments]             ${ACL_NAME}     ${ACL_DIRECTION}
    CLI:Enter Path          /settings/switch_acl/
    CLI:Add
    CLI:Set	                name=${ACL_NAME} direction=${ACL_DIRECTION}
    CLI:Commit
    CLI:Test Show Command   ${ACL_NAME}

SUITE:Delete ACL
    [Arguments]                 ${ACL_NAME}
    CLI:Enter Path              /settings/switch_acl/
    CLI:Delete If Exists        ${ACL_NAME}
    CLI:Test Not Show Command   ${ACL_NAME}

SUITE:Add Rule
    [Arguments]      ${ACL_NAME}  ${RULE_NUMBER}  ${ACTION}=${DEFAULT_ACTION}  ${SOURCE_IP}=${EMPTY}
    CLI:Enter Path   /settings/switch_acl/${ACL_NAME}/rules/
    CLI:Add
    CLI:Set          rule_id=${RULE_NUMBER} action=${ACTION} source_ip=${SOURCE_IP}
    CLI:Commit

SUITE:Gets The First, Second And Third Rows Of A Simple Table
    ${OUTPUT}=          CLI:Show
    @{GET_LINES}=       Split To Lines   ${OUTPUT}
    ${FIRST_LINE}=      Get From List    ${GET_LINES}    2
    ${SECOND_LINE}=     Get From List    ${GET_LINES}    3
    ${THIRD_LINE}=      Get From List    ${GET_LINES}    4
    [Return]            ${FIRST_LINE}    ${SECOND_LINE}     ${THIRD_LINE}

SUITE:Configuration ACL Ingress And ACL Egress On Interface Sfp0
    [Arguments]      ${ING_DIRECTION}   ${EGR_DIRECTION}
    CLI:Enter Path   /settings/switch_interfaces/sfp0
    CLI:Set	         acl_ingress=${ING_DIRECTION} acl_egress=${EGR_DIRECTION}
    CLI:Commit
    CLI:Enter Path   /settings/switch_acl

SUITE:Teardown Test Configuration ACL Ingress And ACL Egress On Interface Sfp0
    Skip If       ${IS_NSR} == ${FALSE}      System is not NSR
    CLI:Enter Path          /settings/switch_acl
    CLI:Cancel              Raw
    CLI:Delete If Exists    ${ACL_ING_NAME}   ${ACL_EGR_NAME}

SUITE:Check If Is NSR
    ${IS_NSR}=      CLI:Is Net SR
    Set Suite Variable   ${IS_NSR}
    [Teardown]   Skip If    ${IS_NSR} == ${FALSE}      System is not NSR