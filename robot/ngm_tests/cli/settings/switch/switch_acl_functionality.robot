*** Settings ***
Resource	../../init.robot
Documentation	Tests of functionality on Switch ACL through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	DEPENDENCE_SWITCH	EXCLUDEIN3_2	EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${ACL_EGR_NAME}	test_switch_acl_egress
${ACL_EGR_DIRECTION}	egress
${NUMBER_ZERO}	0
${VLAN_ID}	3
${DEFAULT_ACTION}	permit
${DENY_ACTION}	deny
${CONNECTION_NAME}	interface_test_acl
${LEFT_IP}	10.0.150.1
${RIGHT_IP}	10.0.150.2

*** Test Cases ***
Test Ping Between Two SR Connected To Each Other By Switch And With ACL Rule
	Skip If	${ACL_AVAILABLE} == ${FALSE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	CLI:Open	session_alias=HOST	HOST_DIFF=${HOST}
	${STATUS}=	Run Keyword And Return Status	CLI:Test Ping	${RIGHT_IP}
	Run Keyword If	${STATUS}	Fail	Ping Should Not Work
	CLI:Open	session_alias=PEER	HOST_DIFF=${HOSTSHARED}
	${STATUS}=	Run Keyword And Return Status	CLI:Test Ping	${LEFT_IP}
	Run Keyword If	${STATUS}	Fail	Ping Should Not Work

Test Ping Between Two SR Connected To Each Other By Switch And Without ACL Rule
	Skip If	${ACL_AVAILABLE} == ${FALSE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	CLI:Open	session_alias=HOST	HOST_DIFF=${HOST}
	SUITE:Delete ACL	${ACL_EGR_NAME}
	CLI:Test Ping	${RIGHT_IP}
	CLI:Open	session_alias=PEER	HOST_DIFF=${HOSTSHARED}
	CLI:Test Ping	${LEFT_IP}

*** Keywords ***
SUITE:Setup
	Skip If	${ACL_AVAILABLE} == ${FALSE}	Tests not configured/available for this testing build
	Set Suite Variable	${NET_PORT_HOST}	${SWITCH_NETPORT_HOST_INTERFACE1}
	Set Suite Variable	${NET_PORT_PEER}	${SWITCH_NETPORT_SHARED_INTERFACE1}

	CLI:Open	session_alias=HOST
	${HOST_IS_NSR}=	CLI:Is Net SR
	Set Suite Variable	${HOST_IS_NSR}
	CLI:Open	session_alias=PEER	HOST_DIFF=${HOSTSHARED}
	${PEER_IS_NSR}=	CLI:Is Net SR
	Set Suite Variable	${PEER_IS_NSR}
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR

	CLI:Switch Connection	HOST
	SUITE:Delete ACL	${ACL_EGR_NAME}
	SUITE:Delete VLAN On Swich And Network Connections	${VLAN_ID}	${CONNECTION_NAME}
	SUITE:Add ACL	${ACL_EGR_NAME}	${ACL_EGR_DIRECTION}
	SUITE:Add Rule	${ACL_EGR_NAME}	${NUMBER_ZERO}	${DENY_ACTION}	${RIGHT_IP}
	SUITE:Enable Or Disable Swith Interface	enabled	${NET_PORT_HOST}
	SUITE:Add VLAN On Swich And Network Connections	${VLAN_ID}	${NET_PORT_HOST}	${CONNECTION_NAME}
	...	${LEFT_IP}	${ACL_EGR_NAME}
	CLI:Switch Connection	PEER
	SUITE:Delete ACL	${ACL_EGR_NAME}
	SUITE:Delete VLAN On Swich And Network Connections	${VLAN_ID}	${CONNECTION_NAME}
	SUITE:Add ACL	${ACL_EGR_NAME}	${ACL_EGR_DIRECTION}
	SUITE:Enable Or Disable Swith Interface	enabled	${NET_PORT_PEER}
	SUITE:Add VLAN On Swich And Network Connections	${VLAN_ID}	${NET_PORT_PEER}	${CONNECTION_NAME}
	...	${RIGHT_IP}	${ACL_EGR_NAME}

SUITE:Teardown
	Skip If	${ACL_AVAILABLE} == ${FALSE}	Tests not configured/available for this testing build
	Run Keyword If	'${HOST_IS_NSR}' == 'False' or '${PEER_IS_NSR}' == 'False'	CLI:Close Connection
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	CLI:Switch Connection	HOST
	SUITE:Delete ACL	${ACL_EGR_NAME}
	SUITE:Delete VLAN On Swich And Network Connections	${VLAN_ID}	${CONNECTION_NAME}
	SUITE:Enable Or Disable Swith Interface	disabled	${NET_PORT_HOST}
	CLI:Switch Connection	PEER
	SUITE:Delete ACL	${ACL_EGR_NAME}
	SUITE:Delete VLAN On Swich And Network Connections	${VLAN_ID}	${CONNECTION_NAME}
	SUITE:Enable Or Disable Swith Interface	disabled	${NET_PORT_PEER}
	CLI:Close Connection

SUITE:Add ACL
	[Arguments]	${ACL_NAME}	${ACL_DIRECTION}
	CLI:Enter Path	/settings/switch_acl/
	CLI:Add
	CLI:Set		name=${ACL_NAME} direction=${ACL_DIRECTION}
	CLI:Commit
	CLI:Test Show Command	${ACL_NAME}

SUITE:Add Rule
	[Arguments]	${ACL_NAME}	${RULE_NUMBER}	${ACTION}=${DEFAULT_ACTION}	${SOURCE_IP}=${EMPTY}
	CLI:Enter Path	/settings/switch_acl/${ACL_NAME}/rules/
	CLI:Add
	CLI:Set	rule_id=${RULE_NUMBER} action=${ACTION} destination_ip=${SOURCE_IP}/24
	CLI:Commit

SUITE:Delete ACL
	[Arguments]	${ACL_NAME}
	CLI:Enter Path	/settings/switch_acl/
	CLI:Delete If Exists	${ACL_NAME}
	CLI:Test Not Show Command	${ACL_NAME}

SUITE:Add VLAN On Swich And Network Connections
	[Arguments]	${VLAN_ID}	${NET_PORT}	${CONNECTION_NAME}	${CONNECTION_IPV4}	${ACL_NAME}
	CLI:Add VLAN	${VLAN_ID}	backplane0,${NET_PORT}
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CONNECTION_NAME} type=vlan ipv4_mode=static ipv4_address=${CONNECTION_IPV4}
	CLI:Set	ipv4_bitmask=24 ethernet_interface=backplane0 vlan_id=${VLAN_ID}
	CLI:Commit
	CLI:Enter Path	/settings/switch_interfaces/${NET_PORT}
	CLI:Set	acl_egress=${ACL_NAME}
	CLI:Commit

SUITE:Delete VLAN On Swich And Network Connections
	[Arguments]	${VLAN_ID}	${CONNECTION_NAME}
	CLI:Enter Path	/settings/switch_vlan/
	CLI:Delete If Exists	${VLAN_ID}
	CLI:Enter Path	/settings/network_connections/
	CLI:Delete If Exists	${CONNECTION_NAME}

SUITE:Enable Or Disable Swith Interface
	[Arguments]	${STATUS}	${NET_PORT}
	CLI:Enter Path	/settings/switch_interfaces/
	CLI:Edit	${NET_PORT}
	CLI:Set	status=${STATUS}
	CLI:Commit
