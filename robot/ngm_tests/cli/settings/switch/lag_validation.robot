*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Switch_lag_validation
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI DEPENDENCE_SWITCH   NON-CRITICAL    EXCLUDEIN3_2    EXCLUDEIN4_2    EXCLUDEIN5_0    EXCLUDEIN5_2	BUG_NG_12073
Default Tags	CLI	SSH	SHOW	LAG

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
@{INVALID_IDS}   0  111  -2
@{INVALID_LACP_SYSTEM_PRIORITIES}    0  65536  -1
${SWITCH_INTERFACE}     netS1-1
${LAG1}     lag1

*** Test Cases ***
Test case to validate port selection
    Skip If	not ${IS_NSR}	LAG only in NSR
    CLI:Enter Path  /settings/switch_lag/
    CLI:Add
    CLI:Set  name=${LAG1} id=1 type=static
    CLI:set    ports=backplane0,backplane1,${SWITCH_INTERFACE}
    Write    commit
    ${OUTPUT}=     Read Until Prompt
    Should Contain    ${OUTPUT}    Error: ports: LAG with backplane0 and backplane1 cannot be aggregated with any other port.
    CLI:Set  name=${LAG1} id=1 type=static
    CLI:Set    ports=sfp0,sfp1,${SWITCH_INTERFACE}
    Write    commit
    ${OUTPUT}=     Read Until Prompt
    Should Contain    ${OUTPUT}   Error: ports: LAG with SFP0 and SFP1 cannot be aggregated with any other port.
    CLI:Set  name=${LAG1} id=1 type=static
    CLI:Set    ports=netS1-1,netS1-2,netS1-4,netS1-6,netS1-7,netS1-8,netS1-9,netS1-10,netS1-12
    Write    commit
    ${OUTPUT}=     Read Until Prompt
    Should Contain    ${OUTPUT}   Error: ports: Exceeded number of ports. Maximum number of ports is 8.
    CLI:Cancel  Raw

Test case to validate id field with invalid inputs
    Skip If	not ${IS_NSR}	LAG only in NSR
    CLI:Set  name=${LAG1} type=static
    CLI:Set  ports=sfp0,sfp1
    FOR		    ${INVALID_ID}  IN  @{INVALID_IDS}
       CLI:Set    id=${INVALID_ID}
       Write    commit
       ${OUTPUT}=     Read Until Prompt
       Should Contain    ${OUTPUT}    Error: id: The value must be between 1 and 110.
    END
    CLI:Cancel  Raw

Test case to validate system priority field with invalid inputs
    Skip If	not ${IS_NSR}	LAG only in NSR
    CLI:Enter Path  /settings/switch_lag/
    CLI:Add
    CLI:Set  name=${LAG1} id=1
    CLI:Set   type=lacp
    FOR		    ${INVALID_LACP_SYSTEM_PRIORITIE}  IN  @{INVALID_LACP_SYSTEM_PRIORITIES}
       CLI:Set    lacp_system_priority=${INVALID_LACP_SYSTEM_PRIORITIE}
       Write    commit
       ${OUTPUT}=     Read Until Prompt
       Should Contain    ${OUTPUT}    Error: lacp_system_priority: The value must be between 1 and 65535.
    END
    CLI:Cancel  Raw

*** Keywords ***
SUITE:Setup
    CLI:Open
    ${IS_NSR}=	CLI:Is Net SR
    Set Suite Variable	${IS_NSR}
    Skip If	not ${IS_NSR}	LAG only in NSR

SUITE:Teardown
    CLI:Close Connection
