*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Scheduler Commands... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${TASK_NAME}	task_name
${TASK_STATUS}	enabled
${TASK_DESCRIPTION}	task_description
${EDIT_TASK_DESCRIPTION}	edit_task_description
${COMMAND_HOSTNAME}	hostname
${COMMAND_WHOAMI}	whoami
${USER_DAEMON}	daemon
${USER_SPMAUTH}	spmauth
${ERR_USER}	err_user
${LOCK_USER}	lock_user
${LOCK_USER_TASK}	lock_user_task

*** Test Cases ***
Test editing fields for task
	[Setup]	SUITE:Add Task	${TASK_NAME}
	CLI:Enter Path	/settings/scheduler/${TASK_NAME}
	${LIST}=	Create List	task( |_)name: ${TASK_NAME}	task_description = ${TASK_DESCRIPTION}	user = daemon
	...	command_to_execute = ${COMMAND_WHOAMI}	minute = *	hour = *	day_of_month = *	month = *	day_of_week = *
	Run Keyword If	${NGVERSION} >= 5.0	Append To List	${LIST}	task_status = enabled
	CLI:Test Show Command Regexp	@{LIST}
	CLI:Set Field	task_description	${EDIT_TASK_DESCRIPTION}
	CLI:Commit
	${LIST}=	Create List	task( |_)name: ${TASK_NAME}	task_description = ${EDIT_TASK_DESCRIPTION}	user = daemon
	...	command_to_execute = ${COMMAND_WHOAMI}	minute = *	hour = *	day_of_month = *	month = *	day_of_week = *
	Run Keyword If	${NGVERSION} >= 5.0	Append To List	${LIST}	task_status = enabled
	CLI:Test Show Command Regexp	@{LIST}
	CLI:Set Field	user	${USER_${USER_SPMAUTH}}
	CLI:Commit
	${LIST}=	Create List	task( |_)name: ${TASK_NAME}	task_description = ${EDIT_TASK_DESCRIPTION}	user = ${USER_${USER_SPMAUTH}}
	...	command_to_execute = ${COMMAND_WHOAMI}	minute = *	hour = *	day_of_month = *	month = *	day_of_week = *
	Run Keyword If	${NGVERSION} >= 5.0	Append To List	${LIST}	task_status = enabled
	CLI:Test Show Command Regexp	@{LIST}
	[Teardown]	SUITE:Delete Task	${TASK_NAME}

Test editing invalid values for field=minute
	[Setup]	SUITE:Add Task	${TASK_NAME}
	CLI:Enter Path	/settings/scheduler/${TASK_NAME}
	Run Keyword If	${NGVERSION} < 5.0	CLI:Test Show Command Regexp	task( |_)name: ${TASK_NAME}	task_description = ${TASK_DESCRIPTION}	user = daemon
	...	command_to_execute = ${COMMAND_WHOAMI}	minute = *	hour = *	day_of_month = *	month = *	day_of_week = *
	Run Keyword If	${NGVERSION} >= 5.0	CLI:Test Show Command Regexp	task( |_)name: ${TASK_NAME}	task_status = ${TASK_STATUS}	task_description = ${TASK_DESCRIPTION}
	...	user = daemon	command_to_execute = ${COMMAND_WHOAMI}	minute = *	hour = *	day_of_month = *	month = *	day_of_week = *
	CLI:Test Set Field Invalid Options	minute	${ONE_WORD}	Error: minute: Invalid entry. This field may start with number or * and it may contain only numbers [0-59], ,, -, /.
	CLI:Set Field	minute	${ONE_NUMBER}
	CLI:Commit
	CLI:Enter Path	/settings/scheduler/
	CLI:Test Show Command Regexp	\\s+${TASK_NAME}\\s+${TASK_DESCRIPTION}\\s+daemon\\s+${COMMAND_WHOAMI}
	[Teardown]	SUITE:Delete Task	${TASK_NAME}

Test adding task with locked user=regular_user
	[Setup]	CLI:Add User	${LOCK_USER}	${LOCK_USER}
	CLI:Write	lock ${LOCK_USER}
	CLI:Test Show Command Regexp	\\s+${LOCK_USER}\\s+Locked\\s+user

	CLI:Enter Path	/settings/scheduler/
	CLI:Delete If Exists	${LOCK_USER_TASK}
	CLI:Add
	CLI:Write	set task_name=${LOCK_USER_TASK} command_to_execute=${COMMAND_WHOAMI} user=${LOCK_USER}
	${OUTPUT}=	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: user: User does not exist or is locked in local accounts
	CLI:Cancel	Raw
	[Teardown]	Run Keywords	SUITE:Delete Task	${LOCK_USER_TASK}
	...	AND	CLI:Delete Users	${LOCK_USER}

Test locking user after adding task
	[Setup]	CLI:Add User	${ERR_USER}	${ERR_USER}
	CLI:Test Show Command Regexp	\\s+${ERR_USER}\\s+Unlocked\\s+user

	CLI:Enter Path	/settings/scheduler/
	CLI:Delete If Exists	err_task
	CLI:Add
	CLI:Write	set task_name=err_task command_to_execute=hostname>>/tmp/err_out user=${ERR_USER}
	${OUTPUT}=	CLI:Commit
	CLI:Test Ls Command	err_task

	CLI:Enter Path	/settings/local_accounts/
	CLI:Write	lock ${ERR_USER}
	CLI:Test Show Command Regexp	\\s+${ERR_USER}\\s+Locked\\s+user
	CLI:Enter Path	/system/scheduler_logs
	Wait Until Keyword Succeeds	14x	5s	CLI:Test Show Command Regexp	\\s+err_task\\s+${ERR_USER}\\s+\\d+\\/\\d+-\\d+:\\d+:\\d+\\s+\\d+\\s+Task Ended\\s+\\(1\\)Operation not permitted
	[Teardown]	Run Keywords	SUITE:Delete Task	err_task
	...	AND	CLI:Delete Users	${ERR_USER}

Test deleting user after adding task
	[Setup]	CLI:Add User	deleted_user	user
	CLI:Test Show Command Regexp	\\s+deleted_user\\s+Unlocked\\s+user

	CLI:Enter Path	/settings/scheduler/
	CLI:Delete If Exists	deleted_task
	CLI:Add
	CLI:Write	set task_name=deleted_task command_to_execute=hostname>>/tmp/deleted_out user=deleted_user
	${OUTPUT}=	CLI:Commit
	CLI:Test Ls Command	deleted_task

	CLI:Delete Users	deleted_user
	${OUTPUT}=	CLI:Show
	Should Not Contain	${OUTPUT}	deleted_user
	CLI:Enter Path	/system/scheduler_logs
	CLI:Test Not Show Command Regexp	deleted_task
	[Teardown]	Run Keywords	SUITE:Delete Task	deleted_task
	...	AND	CLI:Delete Users	deleted_user

Test adding task with unlocked local user=regular_user
	[Setup]	CLI:Add User	regular_user	regular_user
	CLI:Test Show Command Regexp	\\s+regular_user\\s+Unlocked\\s+user

	CLI:Enter Path	/settings/scheduler/
	CLI:Delete If Exists	regular_user_task
	CLI:Add
	CLI:Write	set task_name=regular_user_task command_to_execute=${COMMAND_WHOAMI}>>/tmp/regular_user_out user=regular_user
	${OUTPUT}=	CLI:Commit
	CLI:Test Ls Command	regular_user_task
	CLI:Enter Path	/system/scheduler_logs
	Wait Until Keyword Succeeds	14x	5s	CLI:Test Show Command Regexp	\\s+regular_user_task\\s+regular_user.*Task Started	\\s+regular_user_task\\s+regular_user.*Task Ended
	[Teardown]	Run Keywords	SUITE:Delete Task	regular_user_task
	...	AND	CLI:Delete Users	regular_user

Test adding tasks with user=daemon/snmp/admin
	CLI:Enter Path	/settings/scheduler/
	CLI:Delete If Exists	daemon_task
	CLI:Add
	CLI:Set	task_name=daemon_task command_to_execute=${COMMAND_WHOAMI}>>/tmp/daemon_out user=daemon
	${OUTPUT}=	CLI:Commit
	CLI:Test Ls Command	daemon_task
	CLI:Delete If Exists	daemon_task

	CLI:Delete If Exists	snmp_task
	CLI:Add
	CLI:Set	task_name=snmp_task command_to_execute=${COMMAND_WHOAMI}>>/tmp/${USER_SPMAUTH}_out user=${USER_SPMAUTH}
	${OUTPUT}=	CLI:Commit
	CLI:Test Ls Command	snmp_task
	CLI:Delete If Exists	snmp_task

	CLI:Delete If Exists	admin_task
	CLI:Add
	CLI:Set	task_name=admin_task command_to_execute=${COMMAND_WHOAMI}>>/tmp/admin_out user=admin
	${OUTPUT}=	CLI:Commit
	CLI:Test Ls Command	admin_task
	CLI:Test Show Command Regexp	\\s+admin_task\\s+admin.*${COMMAND_WHOAMI}>>/tmp/admin_out
	CLI:Delete If Exists	daemon_task
	[Teardown]	CLI:Cancel	Raw

Test CLI command for admin/daemon
	CLI:Enter Path	/settings/scheduler/
	CLI:Delete If Exists	cli_task
	CLI:Add
	CLI:Set	task_name=cli_task command_to_execute=cli\\ ${COMMAND_WHOAMI}>>/tmp/cli_out user=admin
	${OUTPUT}=	CLI:Commit
	CLI:Test Ls Command	cli_task

	CLI:Delete If Exists	daemoncli_task
	CLI:Add
	CLI:Set	task_name=daemoncli_task command_to_execute=cli\\ ${COMMAND_WHOAMI}>>/tmp/daemoncli_out user=daemon
	${OUTPUT}=	CLI:Commit
	CLI:Test Ls Command	daemoncli_task
	[Teardown]	CLI:Cancel	Raw

Test CLI script for admin
	SUITE:Make Script
	CLI:Enter Path	/settings/scheduler/
	CLI:Delete If Exists	cliscript_task
	CLI:Add
	CLI:Set	task_name=cliscript_task command_to_execute=cli\\ -f\\ test_script.sh>>/tmp/cliscript_out user=${DEFAULT_USERNAME}
	${OUTPUT}=	CLI:Commit
	CLI:Test Ls Command	cliscript_task
	[Teardown]	CLI:Cancel	Raw

Test resetting task logs
	CLI:Enter Path	/settings/scheduler/
	CLI:Add
	CLI:Set	task_name=daemon_task command_to_execute=${COMMAND_WHOAMI} user=${USER_DAEMON}
	CLI:Commit
	CLI:Enter Path	/system/scheduler_logs
	Wait Until Keyword Succeeds	14x	5s	CLI:Test Ls Command	daemon_task
	${OUTPUT}=	CLI:Show	RAW_Mode=Yes
	Should Match Regexp	${OUTPUT}	\\s+daemon_task\\s+
	CLI:Test Ls Command	daemon_task
	CLI:Write	reset_task_log
	${OUTPUT}=	CLI:Test Ls Command	${EMPTY}
	Should Not Contain	${OUTPUT}	\\s+daemon_task\\s+

Test valid day_of_week/day_of_month/minute/hour/month
	Set Default Configuration	loglevel=INFO
	Set Default Configuration	prompt=#
	Set Default Configuration	newline=\n
	Set Default Configuration	width=400
	Set Default Configuration	height=600
	Set Default Configuration	timeout=300
	Open Connection	${HOST}	alias=root_connection
	Login	root	${ROOT_PASSWORD}	loglevel=INFO
	${DATE}=	CLI:Write	date "+%u-%m-%d-%H-%M"
	${LINES}=	Split To Lines	${DATE}	0	-1
	${DATE}=	Split String	${LINES}[0]	-
	Set Suite Variable	${DAY_OF_WEEK}	${DATE}[0]
	Set Suite Variable	${MONTH}	${DATE}[1]
	Set Suite Variable	${DAY_OF_MONTH}	${DATE}[2]
	Set Suite Variable	${HOUR}	${DATE}[3]
	Set Suite Variable	${MINUTE}	${DATE}[4]
	Close Connection
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/scheduler/

	${MINUTE}=	Convert To Integer	${MINUTE}
	${MINUTE}=	Evaluate	${MINUTE}+1
	Set Suite Variable	${MINUTE}

	Run Keyword If	'${MINUTE}' == '60'	SUITE:Change Hour
	Run Keyword If	${MINUTE} < 10	SUITE:Add Leading Zero

	#In linux, Sunday is day 7, not day 0.
	Run Keyword If	'${DAY_OF_WEEK}' == '7'	Set Suite Variable	${DAY_OF_WEEK}	0

	CLI:Delete If Exists	date_task
	CLI:Add
	CLI:Set	task_name=date_task command_to_execute=hostname>>/tmp/date_out hour=${HOUR} minute=${MINUTE} day_of_month=${DAY_OF_MONTH} day_of_week=${DAY_OF_WEEK} month=${MONTH}
	${OUTPUT}=	CLI:Commit
	CLI:Test Ls Command	date_task

	CLI:Enter Path	/system/scheduler_logs
	Wait Until Keyword Succeeds	14x	5000ms	CLI:Test Ls Command	date_task
	CLI:Test Show Command Regexp	\\s+date_task\\s+daemon\\s+${MONTH}/${DAY_OF_MONTH}-${HOUR}:${MINUTE}:\\d+\\s+
	SUITE:Check Output	date	nodegrid
	CLI:Write	reset_task_log

Test task_name field with valid values
	CLI:Enter Path	/settings/scheduler/
	@{LIST}=	Create List	${WORD}	${ONE_WORD}
	FOR	${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Set	command_to_execute=${COMMAND_HOSTNAME}
		CLI:Test Set Field Invalid Options	task_name	${VALUE}	save=yes
		CLI:Delete If Exists	${VALUE}
	END
	[Teardown]	CLI:Cancel	Raw

Test task_description field with valid values
	CLI:Enter Path	/settings/scheduler/
	@{LIST}=	Create List	${EMPTY}	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${EXCEEDED}
	FOR	${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Set	task_name=${TASK_NAME}
		CLI:Set	command_to_execute=${COMMAND_HOSTNAME}
		CLI:Test Set Field Invalid Options	task_description	${VALUE}	save=yes
		CLI:Delete If Exists	${TASK_NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test user field with valid values
	CLI:Enter Path	/settings/scheduler/
	CLI:Add
	CLI:Set	task_name=${TASK_NAME}
	CLI:Set	command_to_execute=${COMMAND_HOSTNAME}
	CLI:Test Set Field Invalid Options	user	${USER_DAEMON}	save=yes
	CLI:Delete If Exists	${TASK_NAME}
	[Teardown]	CLI:Cancel	Raw

Test command_to_execute field with valid values
	CLI:Enter Path	/settings/scheduler/
	@{LIST}=	Create List	${WORD}	${NUMBER}	${POINTS}	${EXCEEDED}
	FOR	${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Set	task_name=${TASK_NAME}
		CLI:Test Set Field Invalid Options	command_to_execute	${VALUE}	save=yes
		CLI:Delete If Exists	${TASK_NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test minute field with valid values
	CLI:Enter Path	/settings/scheduler/
	@{LIST}=	Create List	${ONE_NUMBER}	0	59
	FOR	${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Set	task_name=${TASK_NAME}
		CLI:Set	command_to_execute=${COMMAND_HOSTNAME}
		CLI:Test Set Field Invalid Options	minute	${VALUE}	save=yes
		CLI:Delete If Exists	${TASK_NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test hour field with valid values
	CLI:Enter Path	/settings/scheduler/
	@{LIST}=	Create List	${ONE_NUMBER}	0	1	23
	FOR	${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Set	task_name=${TASK_NAME}
		CLI:Set	command_to_execute=${COMMAND_HOSTNAME}
		CLI:Test Set Field Invalid Options	hour	${VALUE}	save=yes
		CLI:Delete If Exists	${TASK_NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test day_of_month field with valid values
	CLI:Enter Path	/settings/scheduler/
	@{LIST}=	Create List	${ONE_NUMBER}	1	31	30
	FOR	${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Set	task_name=${TASK_NAME}
		CLI:Set	command_to_execute=${COMMAND_HOSTNAME}
		CLI:Test Set Field Invalid Options	day_of_month	${VALUE}	save=yes
		CLI:Delete If Exists	${TASK_NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test month field with valid values
	CLI:Enter Path	/settings/scheduler/
	@{LIST}=	Create List	${ONE_NUMBER}	1	12
	FOR	${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Set	task_name=${TASK_NAME}
		CLI:Set	command_to_execute=${COMMAND_HOSTNAME}
		CLI:Test Set Field Invalid Options	month	${VALUE}	save=yes
		CLI:Delete If Exists	${TASK_NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test day_of_week field with valid values
	CLI:Enter Path	/settings/scheduler/
	@{LIST}=	Create List	${ONE_NUMBER}	0	6
	FOR	${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Set	task_name=${TASK_NAME}
		CLI:Set	command_to_execute=${COMMAND_HOSTNAME}
		CLI:Test Set Field Invalid Options	day_of_week	${VALUE}	save=yes
		CLI:Delete If Exists	${TASK_NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test task_status with valid values
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/scheduler/
	@{LIST}=	Create List	enabled	disabled
	FOR	${VALUE}	IN	@{LIST}
		CLI:Add
		CLI:Set	task_name=${TASK_NAME}
		CLI:Set	command_to_execute=${COMMAND_HOSTNAME}
		CLI:Test Set Field Valid Options	task_status	${VALUE}	no
		CLI:Commit
		CLI:Delete If Exists	${TASK_NAME}
	END
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Disable Password Complexity Check

SUITE:Teardown
	SUITE:Delete All Schedule Tasks
	SUITE:Delete Script
	CLI:Close Connection

SUITE:Delete All Schedule Tasks
	CLI:Enter Path	/settings/scheduler/
	${OUTPUT}=	CLI:Show
	${TASK_LIST}=	Create List
	${TASKS}=	Split To Lines	${OUTPUT}	2	-1
	${LENGTH}=	Get Length	${TASKS}
	Return From Keyword If	'${LENGTH}' == '0'
	FOR	${TASK}	IN	@{TASKS}
		Log	Start Loop for ${TASK}	INFO
		${TASK_SPLIT}=	Split String	${TASK}
		append to list	${TASK_LIST}	${TASK_SPLIT}[0]
	END
	${OUT_STRING}=	Catenate	SEPARATOR=,	@{TASK_LIST}
	CLI:Write	delete ${OUT_STRING}
	CLI:Commit

SUITE:Check Output
	[Arguments]	${TASK_USER}	${FILE_OUTPUT}=${TASK_USER}
	Set Default Configuration	loglevel=INFO
	Set Default Configuration	prompt=#
	Set Default Configuration	newline=\n
	Set Default Configuration	width=400
	Set Default Configuration	height=600
	Set Default Configuration	timeout=300
	Open Connection	${HOST}	alias=root_connection
	Login	root	${ROOT_PASSWORD}	loglevel=INFO
	CLI:Enter Path	/tmp
	${OUTPUT}=	CLI:Write	cat ${TASK_USER}_out
	${STATUS}=	Run Keyword And Return Status	Should Contain	${OUTPUT}	${FILE_OUTPUT}
	Run Keyword If	${STATUS}	CLI:Write	rm ${TASK_USER}_out
	Close Connection
	CLI:Switch Connection	default

SUITE:Add Leading Zero
	${MINUTE}=	Convert To String	${MINUTE}
	${MINUTE}=	Catenate	SEPARATOR=	0	${MINUTE}
	Set Suite Variable	${MINUTE}

SUITE:Change Hour
	${HOUR}=	Convert To Integer	${HOUR}
	${HOUR}=	Evaluate	${HOUR}+1
	Set Suite Variable	${MINUTE}	0
	Set Suite variable	${HOUR}

SUITE:Make Script
	CLI:Connect As Root
	Write	shell su ${DEFAULT_USERNAME}
	Write	cd ../${DEFAULT_USERNAME}
	Write	/bin/echo ${COMMAND_WHOAMI} > test_script.sh
	Close Connection
	CLI:Switch Connection	default

SUITE:Disable Password Complexity Check
	CLI:Enter Path	/settings/password_rules/
	CLI:Set	check_password_complexity=no
	CLI:Commit

SUITE:Delete Script
	CLI:Connect As Root
	Write	shell su ${DEFAULT_USERNAME}
	Write	cd ../${DEFAULT_USERNAME}
	Write	rm test_script.sh
	#Write	y
	Close Connection
	CLI:Switch Connection	default

SUITE:Add Task
	[Arguments]	${TASK_NAME}=edit_task
	CLI:Enter Path	/settings/scheduler/
	CLI:Delete If Exists	${TASK_NAME}
	CLI:Add
	CLI:Set	task_name=${TASK_NAME} command_to_execute=${COMMAND_WHOAMI} task_description=${TASK_DESCRIPTION}
	CLI:Commit
	CLI:Test Show Command Regexp	\\s+${TASK_NAME}\\s+${TASK_DESCRIPTION}\\s+daemon\\s+${COMMAND_WHOAMI}

SUITE:Delete Task
	[Arguments]	${TASK_NAME}=edit_task
	CLI:Enter Path	/settings/scheduler/
	CLI:Delete If Exists	${TASK_NAME}
	CLI:Enter Path	/system/scheduler_logs/
	CLI:Write	reset_task_log

