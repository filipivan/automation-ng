*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Scheduler Settings... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${TASK_NAME}	task_name
${TASK_DESCRIPTION}	task_description
${COMMAND}	hostname

*** Test Cases ***
Test show_settings command
	CLI:Enter Path	/settings/scheduler/
	Write	show_settings
	${OUTPUT}=	CLI:Read Until Prompt
	Should Not Contain	${OUTPUT}	/settings/scheduler/

Test show_settings for scheduler
	CLI:Enter Path	/settings/scheduler/
	CLI:Add
	CLI:Set	task_name=${TASK_NAME} task_description=${TASK_DESCRIPTION} command_to_execute=${COMMAND}
	CLI:Commit
	Write	show_settings
	${OUTPUT}=	CLI:Read Until Prompt
	Should Match Regexp	${OUTPUT}	/settings/scheduler/${TASK_NAME} task( |_)name=${TASK_NAME}
	Run Keyword If	${NGVERSION} >= 5.0	Should Match Regexp	${OUTPUT}	/settings/scheduler/${TASK_NAME} task_status=enabled
	Should Match Regexp	${OUTPUT}	/settings/scheduler/${TASK_NAME} task_description=${TASK_DESCRIPTION}
	Should Match Regexp	${OUTPUT}	/settings/scheduler/${TASK_NAME} user=daemon
	Should Match Regexp	${OUTPUT}	/settings/scheduler/${TASK_NAME} command_to_execute=${COMMAND}
	Should Match Regexp	${OUTPUT}	/settings/scheduler/${TASK_NAME} minute=\\*
	Should Match Regexp	${OUTPUT}	/settings/scheduler/${TASK_NAME} hour=\\*
	Should Match Regexp	${OUTPUT}	/settings/scheduler/${TASK_NAME} day_of_month=\\*
	Should Match Regexp	${OUTPUT}	/settings/scheduler/${TASK_NAME} month=\\*
	Should Match Regexp	${OUTPUT}	/settings/scheduler/${TASK_NAME} day_of_week=\\*
	[Teardown]	CLI:Delete If Exists	${TASK_NAME}

Test visible fields for show command
	CLI:Enter Path	/settings/scheduler/
	${LIST}=	Create List	task name	user	command to execute	task description
	Run Keyword If	${NGVERSION} >= 5.0	Append To List	${LIST}	task status
	CLI:Test Show Command	@{LIST}

Test available commands after send tab-tab
	CLI:Enter Path	/settings/scheduler/
	${LIST}=	Create List	cd	hostname	set	change_password	ls	shell	commit	pwd	show	event_system_audit	quit	add	delete
	...	event_system_clear	reboot	shutdown	exit	revert	whoami	system_certificate	system_config_check	factory_settings
	Run Keyword If	${NGVERSION} >= 5.0	Append To List	${LIST}	apply_settings	clone	cloud_enrollment	create_csr	diagnostic_data	disable	enable	exec
	...	export_settings	import_settings	save_settings	show_settings	software_upgrade
	CLI:Test Available Commands	@{LIST}

Test available set fields for adding task
	CLI:Enter Path	/settings/scheduler/
	${LIST}=	Create List	task_name	task_description	user	command_to_execute	minute	hour	day_of_month	month	day_of_week
	Run Keyword If	${NGVERSION} >= 5.0	Append To List	${LIST}	task_status
	CLI:Add
	CLI:Test Set Available Fields	@{LIST}
	[Teardown]	CLI:Cancel	Raw

Test available show fields for adding task
	CLI:Enter Path	/settings/scheduler/
	${LIST}=	Create List	task_name =	task_description =	user = daemon	command_to_execute =	minute = *	hour = *
	...	day_of_month = *	month = *	day_of_week = *
	Run Keyword If	${NGVERSION} >= 5.0	Append To List	${LIST}	task_status =	enabled
	CLI:Add
	CLI:Test Show Command	@{LIST}
	[Teardown]	CLI:Cancel	Raw

Test valid value for fields task_name and task_description
	CLI:Enter Path	/settings/scheduler/
	CLI:Add
	CLI:Set	task_name=${TASK_NAME} task_description=${TASK_DESCRIPTION} command_to_execute=${COMMAND}
	CLI:Commit
	CLI:Test Show Command Regexp	\\s+${TASK_NAME}\\s+${TASK_DESCRIPTION}\\s+daemon\\s+${COMMAND}
	[Teardown]	CLI:Delete If Exists	${TASK_NAME}

Test task_name empty field
	CLI:Enter Path	/settings/scheduler/
	CLI:Add
	CLI:Write	set command_to_execute=${COMMAND}
	CLI:Test Set Field Invalid Options	task_name	${EMPTY}	Error: task_name: Field must not be empty.	yes
	CLI:Cancel
	[Teardown]	CLI:Delete If Exists	${TASK_NAME}

Test cloning with empty task_name
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	[Setup]	SUITE:Add Task	${TASK_NAME}
	CLI:Enter Path	/settings/scheduler/
	CLI:Write	clone ${TASK_NAME}
	CLI:Test Set Field Invalid Options	task_name	${EMPTY}	Error: task_name: Field must not be empty.	yes
	CLI:Cancel
	[Teardown]	CLI:Delete If Exists	${TASK_NAME}

Test cloning with duplicate task_name
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	[Setup]	SUITE:Add Task	${TASK_NAME}
	CLI:Enter Path	/settings/scheduler/
	CLI:Write	clone ${TASK_NAME}
	CLI:Test Set Field Invalid Options	task_name	${TASK_NAME}	Error: task_name: Cannot clone task. Either name already exists or name is incorrect.	yes
	CLI:Cancel
	[Teardown]	CLI:Delete If Exists	${TASK_NAME}

Test task_name field
	CLI:Enter Path	/settings/scheduler/
	CLI:Add
	CLI:Write	set command_to_execute=${COMMAND}
	@{LIST}=	Create List	${NUMBER}	${POINTS}	${ONE_NUMBER}	${ONE_POINTS}	${EXCEEDED}
	FOR	${VALUE}	IN	@{LIST}
		Run Keyword If	${NGVERSION} > 4.0	CLI:Test Set Field Invalid Options	task_name	${VALUE}	Error: task_name: This field contains invalid characters.	yes
		Run Keyword If	${NGVERSION} == 4.0	CLI:Test Set Field Invalid Options	task_name	${VALUE}	Error: task_name: Validation error.	yes
	END
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete If Exists	${TASK_NAME}

Test user field
	CLI:Enter Path	/settings/scheduler/
	CLI:Add
	CLI:Set	task_name=${TASK_NAME} command_to_execute=${COMMAND}
	CLI:Test Set Field Invalid Options	user	${EMPTY}	Error: user: Field must not be empty.	yes
	@{LIST}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${EXCEEDED}
	FOR	${VALUE}	IN	@{LIST}
		CLI:Test Set Field Invalid Options	user	${VALUE}	Error: user: User does not exist or is locked in local accounts	yes
	END
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete If Exists	${TASK_NAME}

Test command_to_execute field
	CLI:Enter Path	/settings/scheduler/
	CLI:Add
	CLI:Set	task_name=${TASK_NAME}
	CLI:Test Set Field Invalid Options	command_to_execute	${EMPTY}	Error: command_to_execute: Field must not be empty.	yes
	@{LIST}=	Create List	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}
	FOR	${VALUE}	IN	@{LIST}
		CLI:Test Set Field Invalid Options	command_to_execute	${VALUE}	Error: command_to_execute: Command length must be between 4 and 1024	yes
	END
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete If Exists	${TASK_NAME}

Test minute field
	CLI:Enter Path	/settings/scheduler/
	CLI:Add
	CLI:Set	task_name=${TASK_NAME} command_to_execute=${COMMAND}
	@{LIST}=	Create List	${EMPTY}	${WORD}	${NUMBER}	${ONE_WORD}	${ONE_POINTS}	${EXCEEDED}	-1	60	61
	FOR	${VALUE}	IN	@{LIST}
		Run Keyword If	${NGVERSION} > 4.0	CLI:Test Set Field Invalid Options	minute	${VALUE}	Error: minute: Invalid entry. This field may start with number or * and it may contain only numbers [0-59], ,, -, /.	yes
		Run Keyword If	${NGVERSION} == 4.0	CLI:Test Set Field Invalid Options	minute	${VALUE}	Error: minute: Invalid entry. This field may start with number or * and it may contain only numbers, ,, -, /.	yes
	END
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete If Exists	${TASK_NAME}

Test hour field
	CLI:Enter Path	/settings/scheduler/
	CLI:Add
	CLI:Set	task_name=${TASK_NAME} command_to_execute=${COMMAND}
	@{LIST}=	Create List	${EMPTY}	${WORD}	${NUMBER}	${ONE_WORD}	${ONE_POINTS}	${EXCEEDED}	-1	24	25
	FOR	${VALUE}	IN	@{LIST}
		Run Keyword If	${NGVERSION} > 4.0	CLI:Test Set Field Invalid Options	hour	${VALUE}	Error: hour: Invalid entry. This field may start with number or * and it may contain only numbers [0-23], ,, -, /.	yes
		Run Keyword If	${NGVERSION} == 4.0	CLI:Test Set Field Invalid Options	hour	${VALUE}	Error: hour: Invalid entry. This field may start with number or * and it may contain only numbers, ,, -, /.	yes
	END
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete If Exists	${TASK_NAME}

Test day_of_month field
	CLI:Enter Path	/settings/scheduler/
	CLI:Add
	CLI:Set	task_name=${TASK_NAME} command_to_execute=${COMMAND}
	@{LIST}=	Create List	${EMPTY}	${WORD}	${NUMBER}	${ONE_WORD}	${ONE_POINTS}	${EXCEEDED}	-1	0	32
	FOR	${VALUE}	IN	@{LIST}
		Run Keyword If	${NGVERSION} > 4.0	CLI:Test Set Field Invalid Options	day_of_month	${VALUE}	Error: day_of_month: Invalid entry. This field may start with number or * and it may contain only numbers [1-31], ,, -, /.	yes
		Run Keyword If	${NGVERSION} == 4.0	CLI:Test Set Field Invalid Options	day_of_month	${VALUE}	Error: day_of_month: Invalid entry. This field may start with number or * and it may contain only numbers, ,, -, /.	yes
	END
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete If Exists	${TASK_NAME}

Test month field
	CLI:Enter Path	/settings/scheduler/
	CLI:Add
	CLI:Set	task_name=${TASK_NAME} command_to_execute=${COMMAND}
	@{LIST}=	Create List	${EMPTY}	${WORD}	${NUMBER}	${ONE_WORD}	${ONE_POINTS}	${EXCEEDED}	0	-1	13
	FOR	${VALUE}	IN	@{LIST}
		CLI:Test Set Field Invalid Options	month	${VALUE}	Error: month: Invalid entry. This field may start with number or *. It may contain only numbers, ,, -, /.(Jan=1, Feb=2, ..., Dec=12)	yes
	END
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete If Exists	${TASK_NAME}

Test day_of_week field
	CLI:Enter Path	/settings/scheduler/
	CLI:Add
	CLI:Set	task_name=${TASK_NAME} command_to_execute=${COMMAND}
	@{LIST}=	Create List	${EMPTY}	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_POINTS}	${EXCEEDED}	-1	7
	FOR	${VALUE}	IN	@{LIST}
		CLI:Test Set Field Invalid Options	day_of_week	${VALUE}	Error: day_of_week: Invalid entry. This field may start with number or *. It may contain only numbers, ,, -, /.(Sun=0, Mon=1, ..., Sat=6)	yes
	END
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete If Exists	${TASK_NAME}

Test task_status field
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/scheduler/
	CLI:Add
	CLI:Set	task_name=${TASK_NAME} command_to_execute=${COMMAND}
	CLI:Test Set Field Invalid Options	task_status	${EMPTY}	Error: Missing value for parameter: task_status
	@{LIST}=	Create List	${WORD}	${NUMBER}	${ONE_WORD}	${ONE_POINTS}	${EXCEEDED}	0	-1	13
	FOR	${VALUE}	IN	@{LIST}
		CLI:Test Set Field Invalid Options	task_status	${VALUE}	Error: Invalid value: ${VALUE} for parameter: task_status
	END
	[Teardown]	CLI:Cancel

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Delete All Schedule Tasks
	CLI:Enter Path	/settings/scheduler/
	CLI:Delete If Exists	${TASK_NAME}
	@{LIST_USER}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${EXCEEDED}
	FOR	${VALUE}	IN	@{LIST_USER}
		CLI:Delete Users	${VALUE}
	END

SUITE:Teardown
	CLI:Open
	CLI:Enter Path	/settings/scheduler/
	CLI:Delete If Exists	${TASK_NAME}
	CLI:Close Connection

SUITE:Delete All Schedule Tasks
	CLI:Enter Path	/settings/scheduler/
	${OUTPUT}=	CLI:Show
	${TASK_LIST}=	Create List
	${TASKS}=	Split To Lines	${OUTPUT}	2	-1
	${LENGTH}=	Get Length	${TASKS}
	Return From Keyword If	'${LENGTH}' == '0'
	FOR	${TASK}	IN	@{TASKS}
		Log	Start Loop for ${TASK}	INFO
		${TASK_SPLIT}=	Split String	${TASK}
		append to list	${TASK_LIST}	${TASK_SPLIT}[0]
	END
	${OUT_STRING}=	Catenate	SEPARATOR=,	@{TASK_LIST}
	CLI:Write	delete ${OUT_STRING}
	CLI:Commit

SUITE:Add Task
	[Arguments]	${TASK_NAME}=edit_task
	CLI:Enter Path	/settings/scheduler/
	CLI:Delete If Exists	${TASK_NAME}
	CLI:Add
	CLI:Set	task_name=${TASK_NAME} command_to_execute=${COMMAND} task_description=${TASK_DESCRIPTION}
	CLI:Commit
	CLI:Test Show Command Regexp	\\s+${TASK_NAME}\\s+${TASK_DESCRIPTION}\\s+daemon\\s+${COMMAND}