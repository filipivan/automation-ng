*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Scheduler functionality through CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${TASK_NAME}	task_name
${CLONE_TASK_NAME}	clone_task_name
${TASK_DESCRIPTION}	task_description
${COMMAND}	hostname

*** Test Cases ***
Test clone schedule
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	[Setup]	SUITE:Add Task	${TASK_NAME}
	SUITE:Clone Task	${TASK_NAME}	${CLONE_TASK_NAME}
	[Teardown]	CLI:Delete If Exists	${TASK_NAME}	${CLONE_TASK_NAME}

Test disable/enable schedule
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	[Setup]	SUITE:Add Task	${TASK_NAME}
	SUITE:Clone Task	${TASK_NAME}	${CLONE_TASK_NAME}
	CLI:Enter Path	/settings/scheduler/
	SUITE:Disable task
	SUITE:Enable task
	[Teardown]	CLI:Delete If Exists	${TASK_NAME}	${CLONE_TASK_NAME}

Test run command using scheduler
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	[Setup]	SUITE:Create shell file
	SUITE:Add Task	${TASK_NAME}	./export_settings.sh	admin	*/1
	CLI:Connect As Root
	CLI:Write	rm /home/admin/scheduled_export.conf
	CLI:Write	rm /var/lib/cron/schedule.tasks
	CLI:Enter Path	/home/admin
	SUITE:Check schedule working
	CLI:Write	rm -rf /home/admin/*
	CLI:Write	rm -rf /var/lib/cron/schedule.tasks
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/scheduler/

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Cancel	raw
	SUITE:Delete All Schedule Tasks
	CLI:Close Connection

SUITE:Create shell file
	CLI:Write	shell echo "cli export_settings /settings/services --include-empty --plain-password --file /home/admin/scheduled_export.conf" > export_settings.sh
	CLI:Write	shell chmod +x export_settings.sh

SUITE:Delete All Schedule Tasks
	CLI:Enter Path	/settings/scheduler/
	${OUTPUT}=	CLI:Show
	${TASK_LIST}=	Create List
	${TASKS}=	Split To Lines	${OUTPUT}	2	-1
	${LENGTH}=	Get Length	${TASKS}
	Return From Keyword If	'${LENGTH}' == '0'
	FOR	${TASK}	IN	@{TASKS}
		Log	Start Loop for ${TASK}	INFO
		${TASK_SPLIT}=	Split String	${TASK}
		append to list	${TASK_LIST}	${TASK_SPLIT}[0]
	END
	${OUT_STRING}=	Catenate	SEPARATOR=,	@{TASK_LIST}
	CLI:Write	delete ${OUT_STRING}
	CLI:Commit

SUITE:Disable task
	CLI:Write	disable ${TASK_NAME}
	CLI:Test Show Command Regexp	\\s+${CLONE_TASK_NAME}\\s+${TASK_DESCRIPTION}\\s+daemon\\s+${COMMAND}\\s+Enabled\\s+${TASK_NAME}\\s+${TASK_DESCRIPTION}\\s+daemon\\s+${COMMAND}\\s+Disabled

SUITE:Enable task
	CLI:Write	enable ${TASK_NAME}
	CLI:Test Show Command Regexp	\\s+${CLONE_TASK_NAME}\\s+${TASK_DESCRIPTION}\\s+daemon\\s+${COMMAND}\\s+Enabled\\s+${TASK_NAME}\\s+${TASK_DESCRIPTION}\\s+daemon\\s+${COMMAND}\\s+Enabled

SUITE:Check schedule working
	FOR	${i}	IN RANGE	3
		Wait Until Keyword Succeeds	65s	5s	SSHLibrary.File Should Exist	/home/admin/scheduled_export.conf
		${OUTPUT}=	CLI:Write	cat /var/lib/cron/schedule.tasks
		Should Match Regexp	${OUTPUT}	(((.*Task-Start;;${TASK_NAME};;\\(0\\)(\\n)?))+|(((.*Task-End;;${TASK_NAME};;\\(0\\)(\\n)?))+))+
		CLI:Write	rm scheduled_export.conf
		${OUTPUT}=	SSHLibrary.File Should Not Exist	/home/admin/scheduled_export.conf
		Continue For Loop If	${OUTPUT}
	END

SUITE:Add Task
	[Arguments]	${TASK_NAME}	${COMMAND}=${COMMAND}	${USER}=daemon	${MINUTE}=*
	CLI:Enter Path	/settings/scheduler/
	CLI:Delete If Exists	${TASK_NAME}
	CLI:Add
	CLI:Set	task_name=${TASK_NAME} command_to_execute=${COMMAND} task_description=${TASK_DESCRIPTION} user=${USER} minute=${MINUTE}
	CLI:Commit
	CLI:Test Show Command Regexp	\\s+${TASK_NAME}\\s+${TASK_DESCRIPTION}\\s+${USER}\\s+${COMMAND}

SUITE:Clone Task
	[Arguments]	${TASK_NAME}	${CLONE_TASK_NAME}
	CLI:Enter Path	/settings/scheduler/
	CLI:Delete If Exists	${CLONE_TASK_NAME}
	CLI:Write  clone ${TASK_NAME}
	CLI:Set	task_name=${CLONE_TASK_NAME}
	CLI:Commit
	CLI:Test Show Command Regexp	\\s+${TASK_NAME}\\s+${TASK_DESCRIPTION}\\s+daemon\\s+${COMMAND}\\s+Enabled