*** Settings ***
Resource	../../init.robot
Documentation	Tests configuration when modifying at NETWORK :: STATIC ROUTES
Metadata	Version	1.0
Metadata	Executed At ${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	NETWORK	STATIC_ROUTES	CRUD	REVIEWED
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DESTINATION_IP}	192.168.16.20
${DESTINATION_BITMASK}	24
${GATEWAY_IP}	192.168.16.1
${METRICO}	10

${IPV6_DESTINATION_IP}	fe11::c111:1111:a111:c111
${IPV6_GATEWAY_IP}	fe22::c222:2222:a222:c222

*** Test Cases ***
Test show_settings command
	[Setup]	CLI:Enter Path	/settings/static_routes/
	${RESULT}=	Run Keyword And Return Status	CLI:Test Ls Command	/
	Run Keyword If	${RESULT} == False	SUITE:Show Settings Should Not Contain

Test Validate values for ipv4
	${PROTOCOL_IPV4}	Set Variable	_ipv4_route1
	${TYPE}	Set Variable	ipv4
	FOR	${CONNECTION}	IN	@{CONNECTIONS}
		SUITE:Validate Values for Routes	${PROTOCOL_IPV4}	${TYPE}	${DESTINATION_IP}	${GATEWAY_IP}	${CONNECTION}
	END
	[Teardown]	CLI:Revert	Raw

Test Validate values for ipv6
	${PROTOCOL_IPV6}	Set Variable	_ipv6_route1
	${TYPE}	Set Variable	ipv6
	FOR	${CONNECTION}	IN	@{CONNECTIONS}
		SUITE:Validate Values for Routes	${PROTOCOL_IPV6}	${TYPE}	${IPV6_DESTINATION_IP}	${IPV6_GATEWAY_IP}	${CONNECTION}
	END
	[Teardown]	CLI:Revert	Raw

Test defining EMPTY values
	Skip If	'${NGVERSION}' == '3.2'	Error messages are not shown in 3.2 (Bug 664)
	[Setup]	CLI:Enter Path	/settings/static_routes/
	SUITE:Set Empty Value	ipv4	${DESTINATION_IP}
	SUITE:Set Empty Value	ipv6	${IPV6_DESTINATION_IP}
	[Teardown]	SUITE:Test Teardown

Test defining Invalid Values
	Skip If	'${NGVERSION}' == '3.2'	Error messages are not shown in 3.2 (Bug 664)
	CLI:Enter Path	/settings/static_routes/
	FOR	${CONNECTION}	IN	@{CONNECTIONS}
		SUITE:Set Invalid Values	ipv4	${CONNECTION}	${DESTINATION_IP}	${GATEWAY_IP}	${WORD}
		SUITE:Set Invalid Values	ipv6	${CONNECTION}	${IPV6_DESTINATION_IP}	${IPV6_GATEWAY_IP}	${WORD}
		SUITE:Set Invalid Values	ipv4	${CONNECTION}	${DESTINATION_IP}	${GATEWAY_IP}	${ONE_WORD}
		SUITE:Set Invalid Values	ipv6	${CONNECTION}	${IPV6_DESTINATION_IP}	${IPV6_GATEWAY_IP}	${ONE_WORD}
		SUITE:Set Invalid Values	ipv4	${CONNECTION}	${DESTINATION_IP}	${GATEWAY_IP}	${ONE_NUMBER}
		SUITE:Set Invalid Values	ipv6	${CONNECTION}	${IPV6_DESTINATION_IP}	${IPV6_GATEWAY_IP}	${ONE_NUMBER}
		SUITE:Set Invalid Values	ipv4	${CONNECTION}	${DESTINATION_IP}	${GATEWAY_IP}	${ONE_POINTS}
		SUITE:Set Invalid Values	ipv6	${CONNECTION}	${IPV6_DESTINATION_IP}	${IPV6_GATEWAY_IP}	${ONE_POINTS}
	END
	[Teardown]	SUITE:Test Teardown

Test defining NUMBER values
	Skip If	'${NGVERSION}' == '3.2'	Error messages are not shown in 3.2 (Bug 664)
	CLI:Enter Path	/settings/static_routes/
	SUITE:Set Invalid Value NUMBER	ipv4	${DESTINATION_IP}	${GATEWAY_IP}
	SUITE:Set Invalid Value NUMBER	ipv6	${IPV6_DESTINATION_IP}	${IPV6_GATEWAY_IP}
	[Teardown]	SUITE:Test Teardown

Test defining POINTS values
	Skip If	'${NGVERSION}' == '3.2'	Error messages are not shown in 3.2 (Bug 664)
	CLI:Enter Path	/settings/static_routes/
	SUITE:Set Invalid Value POINTS	ipv4	${DESTINATION_IP}	${GATEWAY_IP}
	SUITE:Set Invalid Value POINTS	ipv6	${IPV6_DESTINATION_IP}	${IPV6_GATEWAY_IP}
	[Teardown]	SUITE:Test Teardown

Test defining EXCEEDED values
	Skip If	'${NGVERSION}' == '3.2'	Error messages are not shown in 3.2 (Bug 664)
	CLI:Enter Path	/settings/static_routes/
	SUITE:Set Invalid Value EXCEEDED	ipv4	${DESTINATION_IP}	${GATEWAY_IP}
	SUITE:Set Invalid Value EXCEEDED	ipv6	${IPV6_DESTINATION_IP}	${IPV6_GATEWAY_IP}
	[Teardown]	SUITE:Test Teardown

Test valid field options
	${CONNECTIONS}=	CLI:Get Builtin Network Connections
	CLI:Enter Path	/settings/static_routes/
	CLI:Add
	CLI:Test Set Available Fields	connection	destination_ip	metric	destination_bitmask	gateway_ip	type
	Run Keyword If	${HAS_ETH1}	CLI:Test Set Field Options	connection	@{CONNECTIONS}
	CLI:Test Set Field Options	type	ipv4	ipv6
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	@{CONNECTIONS}=	CLI:Test Get Connections
	Set Suite Variable	@{CONNECTIONS}
	SUITE:Delete Static Routes
	${HAS_ETH1}=	CLI:Has ETH1
	Set Suite Variable	${HAS_ETH1}
	CLI:Enter Path	/settings/static_routes/
	FOR	${CONNECTION}	IN	@{CONNECTIONS}
		SUITE:Add Routes	${CONNECTION}	${DESTINATION_IP}	${GATEWAY_IP}	ipv6	ipv4
		SUITE:Add Routes	${CONNECTION}	${IPV6_DESTINATION_IP}	${IPV6_GATEWAY_IP}	ipv4	ipv6
	END

SUITE:Teardown
	SUITE:Delete Static Routes
	CLI:Close Connection

SUITE:Delete Static Routes
	CLI:Enter Path	/settings/
	CLI:Write	delete static_routes/ -
	CLI:Commit

SUITE:Show Settings Should Not Contain
	${OUTPUT}=	CLI:Show Settings
	Should Not Contain	${OUTPUT}	/settings/static_routes/

SUITE:Validate Values for Routes
	[Arguments]	${PROTOCOL}	${TYPE}	${DESTINATION_IP}	${GATEWAY_IP}	${CONNECTION}
	${PATH}	Set Variable	/settings/static_routes/${CONNECTION}${PROTOCOL}
	CLI:Enter Path	${PATH}
	CLI:Test Validate Unmodifiable Field	connection	${CONNECTION}
	CLI:Test Validate Unmodifiable Field	type	${TYPE}
	CLI:Test Set Validate Normal Fields	destination_ip	${DESTINATION_IP}
	CLI:Test Set Validate Normal Fields	destination_bitmask	${DESTINATION_BITMASK}
	CLI:Test Set Validate Normal Fields	gateway_ip	${GATEWAY_IP}
	CLI:Test Set Validate Normal Fields	metric	${METRICO}

SUITE:Set Empty Value
	[Arguments]	${TYPE}	${DESTINATION_IP}
	FOR	${CONNECTION}	IN	@{CONNECTIONS}
		CLI:Add
		Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Field Invalid Options	connection	${EMPTY}	Error: Missing value for parameter: connection
		Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Field Invalid Options	connection	${EMPTY}	Error: Missing value: connection
		Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Field Invalid Options	type	${EMPTY}	Error: Missing value for parameter: type
		Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Field Invalid Options	type	${EMPTY}	Error: Missing value: type

		CLI:Set	connection=${CONNECTION} type=${TYPE} destination_bitmask=${DESTINATION_BITMASK}
		CLI:Test Set Field Invalid Options	destination_ip	${EMPTY}	Error: destination_ip: Invalid IP address.	yes
		CLI:Set	destination_ip=${DESTINATION_IP}
		CLI:Test Set Field Invalid Options	destination_bitmask	${EMPTY}	Error: destination_bitmask: Invalid mask.	yes
		CLI:Set	destination_bitmask=${DESTINATION_BITMASK} metric=${EMPTY} gateway_ip=${EMPTY}
		CLI:Commit
	END

SUITE:Set Invalid Values
	[Arguments]	${TYPE}	${CONNECTION}	${DESTINATION_IP}	${GATEWAY_IP}	${VALUE}
	CLI:Add
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Field Invalid Options	connection	${VALUE}	Error: Invalid value: ${VALUE} for parameter: connection
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Field Invalid Options	connection	${VALUE}	Error: Invalid value: ${VALUE}
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Field Invalid Options	type	${VALUE}	Error: Invalid value: ${VALUE} for parameter: type
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Field Invalid Options	type	${VALUE}	Error: Invalid value: ${VALUE}

	CLI:Set	connection=${CONNECTION} type=${TYPE} destination_bitmask=${DESTINATION_BITMASK}
	CLI:Test Set Field Invalid Options	destination_ip	${VALUE}	Error: destination_ip: Invalid IP address.	yes
	CLI:Set	destination_ip=${DESTINATION_IP}
	Run Keyword If	not '${VALUE}' == '1'	CLI:Test Set Field Invalid Options	destination_bitmask	${VALUE}	Error: destination_bitmask: Invalid mask.	yes
	Run Keyword If	not '${TYPE}' == 'ipv6' or not '${VALUE}' == '1'	CLI:Set	destination_bitmask=${DESTINATION_BITMASK}
	CLI:Test Set Field Invalid Options	gateway_ip	${VALUE}	Error: gateway_ip: Invalid IP address.	yes
	CLI:Set	gateway_ip=${GATEWAY_IP}
	Run Keyword If	not '${VALUE}' == '1'	CLI:Test Set Field Invalid Options	metric	${VALUE}	Error: metric: Validation error.	yes
	Run Keyword If	'${VALUE}' == '1'	CLI:Test Set Field Invalid Options	metric	${VALUE}
	Run Keyword If	not '${VALUE}' == '1'	CLI:Set	metric=${METRICO}
	CLI:Commit

SUITE:Set Invalid Value NUMBER
	[Arguments]  ${TYPE}	${DESTINATION_IP}	${GATEWAY_IP}
	FOR	${CONNECTION}	IN	@{CONNECTIONS}
		CLI:Add
		Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Field Invalid Options	connection	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: connection
		Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Field Invalid Options	connection	${NUMBER}	Error: Invalid value: ${NUMBER}
		Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Field Invalid Options	type	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: type
		Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Field Invalid Options	type	${NUMBER}	Error: Invalid value: ${NUMBER}

		CLI:Set	connection=${CONNECTION} type=${TYPE} destination_bitmask=${DESTINATION_BITMASK}
		CLI:Test Set Field Invalid Options	destination_ip	${NUMBER}	Error: destination_ip: Invalid IP address.	yes
		CLI:Set	destination_ip=${DESTINATION_IP}
		CLI:Test Set Field Invalid Options	destination_bitmask	${NUMBER}	Error: destination_bitmask: Invalid mask.	yes
		CLI:Test Set Field Invalid Options	destination_bitmask	${ELEVEN_NUMBER}	Error: destination_bitmask: Exceeded the maximum size for this field.	yes
		CLI:Set	destination_bitmask=${DESTINATION_BITMASK}
		CLI:Test Set Field Invalid Options	gateway_ip	${NUMBER}	Error: gateway_ip: Invalid IP address.	yes
		CLI:Set	gateway_ip=${GATEWAY_IP}
		CLI:Test Set Field Invalid Options	metric	${ELEVEN_NUMBER}	Error: metric: Exceeded the maximum size for this field.	yes
		CLI:Set	metric=${METRICO}
		CLI:Commit
	END

SUITE:Set Invalid Value POINTS
	[Arguments]	${TYPE}	${DESTINATION_IP}	${GATEWAY_IP}
	FOR	${CONNECTION}	IN	@{CONNECTIONS}
		CLI:Add
		Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Field Invalid Options	connection	${POINTS}	Error: Invalid value: ${POINTS} for parameter: connection
		Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Field Invalid Options	connection	${POINTS}	Error: Invalid value: ${POINTS}
		Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Field Invalid Options	type	${POINTS}	Error: Invalid value: ${POINTS} for parameter: type
		Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Field Invalid Options	type	${POINTS}	Error: Invalid value: ${POINTS}

		CLI:Set	connection=${CONNECTION} type=${TYPE} destination_bitmask=${DESTINATION_BITMASK}
		CLI:Test Set Field Invalid Options	destination_ip	${POINTS}	Error: destination_ip: Invalid IP address.	yes
		CLI:Set	destination_ip=${DESTINATION_IP}
		CLI:Test Set Field Invalid Options	destination_bitmask	${POINTS}	Error: destination_bitmask: Invalid mask.	yes
		CLI:Test Set Field Invalid Options	destination_bitmask	${TWO_POINTS}	Error: destination_bitmask: Invalid mask.	yes
		CLI:Test Set Field Invalid Options	destination_bitmask	${ELEVEN_POINTS}	Error: destination_bitmask: Invalid mask.	yes
		CLI:Set	destination_bitmask=${DESTINATION_BITMASK}
		CLI:Test Set Field Invalid Options	gateway_ip	${POINTS}	Error: gateway_ip: Invalid IP address.	yes
		CLI:Set	gateway_ip=${GATEWAY_IP}
		CLI:Test Set Field Invalid Options	metric	${ELEVEN_POINTS}	Error: metric: Exceeded the maximum size for this field.	yes
		CLI:Test Set Field Invalid Options	metric	${TWO_POINTS}	Error: metric: Validation error.	yes
		CLI:Set	metric=${METRICO}
		CLI:Commit
	END

SUITE:Set Invalid Value EXCEEDED
	[Arguments]	${TYPE}	${DESTINATION_IP}	${GATEWAY_IP}
	FOR	${CONNECTION}	IN	@{CONNECTIONS}
		CLI:Add
		Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Field Invalid Options	connection	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: connection
		Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Field Invalid Options	connection	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
		Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Field Invalid Options	type	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: type
		Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Field Invalid Options	type	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}

		CLI:Set	connection=${CONNECTION} type=${TYPE} destination_bitmask=${DESTINATION_BITMASK}
		CLI:Test Set Field Invalid Options	destination_ip	${EXCEEDED}	Error: destination_ip: Exceeded the maximum size for this field.	yes
		CLI:Set	destination_ip=${DESTINATION_IP}
		CLI:Test Set Field Invalid Options	destination_bitmask	${EXCEEDED}	Error: destination_bitmask: Exceeded the maximum size for this field.	yes
		CLI:Set	destination_bitmask=${DESTINATION_BITMASK}
		CLI:Test Set Field Invalid Options	gateway_ip	${EXCEEDED}	Error: gateway_ip: Exceeded the maximum size for this field.	yes
		CLI:Set	gateway_ip=${GATEWAY_IP}
		CLI:Test Set Field Invalid Options	metric	${EXCEEDED}	Error: metric: Exceeded the maximum size for this field.	yes
		CLI:Set	metric=${METRICO}
		CLI:Commit
	END

SUITE:Add Routes
	[Arguments]	${CONNECTION}	${DESTINATION_IP}	${GATEWAY_IP}	@{OPTIONS}
	CLI:Add
	CLI:Set	connection=${CONNECTION} destination_ip=${DESTINATION_IP} destination_bitmask=${DESTINATION_BITMASK} gateway_ip=${GATEWAY_IP} metric=${METRICO}
	CLI:Test Set Field Options	type	@{OPTIONS}
	CLI:Save

SUITE:Test Teardown
	CLI:Cancel	Raw
	SUITE:Delete Static Routes
