*** Settings ***
Resource	../../init.robot
Documentation	Tests functionality when at NETWORK :: STATIC ROUTES
Metadata	Version	1.0
Metadata	Executed At ${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	NETWORK	STATIC_ROUTES	CRUD	REVIEWED
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DESTINATION_IP}	192.168.16.20
${DESTINATION_BITMASK}	24
${GATEWAY_IP}	192.168.16.1
${METRICO}	10

${IPV6_DESTINATION_IP}	fe11::c111:1111:a111:c111
${IPV6_GATEWAY_IP}	fe22::c222:2222:a222:c222

*** Test Cases ***
Test defining RIGHT values for ipv4
	CLI:Enter Path	/settings/static_routes/
	FOR	${CONNECTION}	IN	@{CONNECTIONS}
		SUITE:Add Routes	${CONNECTION}	${DESTINATION_IP}	${GATEWAY_IP}	ipv6	ipv4
	END
	[Teardown]	CLI:Cancel	Raw

Test defining RIGHT values for ipv6
	CLI:Enter Path	/settings/static_routes/
	FOR	${CONNECTION}	IN	@{CONNECTIONS}
		SUITE:Add Routes	${CONNECTION}	${IPV6_DESTINATION_IP}	${IPV6_GATEWAY_IP}	ipv4	ipv6
	END
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	@{CONNECTIONS}=	CLI:Test Get Connections
	Set Suite Variable	@{CONNECTIONS}
	SUITE:Delete Static Routes

SUITE:Teardown
	SUITE:Delete Static Routes
	CLI:Close Connection

SUITE:Add Routes
	[Arguments]	${CONNECTION}	${DESTINATION_IP}	${GATEWAY_IP}	@{OPTIONS}
	CLI:Add
	CLI:Set	connection=${CONNECTION} destination_ip=${DESTINATION_IP} destination_bitmask=${DESTINATION_BITMASK} gateway_ip=${GATEWAY_IP} metric=${METRICO}
	CLI:Test Set Field Options	type	@{OPTIONS}
	CLI:Save

SUITE:Delete Static Routes
	CLI:Enter Path	/settings/
	CLI:Write	delete static_routes/ -
	CLI:Commit