*** Settings ***
Resource	../../init.robot
Documentation	Tests Validate Fields when Adding at NETWORK :: STATIC ROUTES
Metadata	Version	1.0
Metadata	Executed At ${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	NETWORK	STATIC_ROUTES	ADD	REVIEWED
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DESTINATION_IP}	192.168.16.20
${DESTINATION_BITMASK}	24
${GATEWAY_IP}	192.168.16.1
${METRICO}	10

${IPV6_DESTINATION_IP}	fe11::c111:1111:a111:c111
${IPV6_GATEWAY_IP}	fe22::c222:2222:a222:c222

*** Test Cases ***
Test ls and show commands
	[Setup]	CLI:Enter Path	/settings/static_routes/
	${OUTPUT}=	CLI:Ls
	@{LINES}=	Split to Lines	${OUTPUT}
	${LENGTH_LINES}=	Get Length	${LINES}
	${NAMES}=	Create List
	FOR	${INDEX}	IN RANGE	0	${LENGTH_LINES-3}
		${NAME}=	Get From List	${LINES}	${INDEX}
		${LENGTH_NAME}=	Get Length	${NAME}
		${NEW_NAME}=	Get Substring	${NAME}	0	${LENGTH_NAME-1}
		Append to List	${NAMES}	${NEW_NAME}
	END
	FOR	${NAME}	IN	@{NAMES}
		CLI:Enter Path	/settings/static_routes/${NAME}
		${OUTPUT}=	CLI:Show
		@{connection_type_index}=	Split String	${NAME}	_
		Should Contain	${OUTPUT}	index: ${connection_type_index}[2]
		Should Contain	${OUTPUT}	connection: ${connection_type_index}[0]
		Should Contain	${OUTPUT}	type: ${connection_type_index}[1]
		Should Contain	${OUTPUT}	destination_ip =
		Should Contain	${OUTPUT}	destination_bitmask =
		Should Contain	${OUTPUT}	gateway_ip =
		Should Contain	${OUTPUT}	metric =
	END

Test available commands after send tab-tab
	[Setup]	CLI:Enter Path	/settings/static_routes/
	@{COMMANDS}	Create List	add		apply_settings	cd	change_password	commit	delete	event_system_audit
	...	event_system_clear	exit	factory_settings	hostname	ls	pwd	quit	reboot	revert	save_settings
	...	set	shell	show	show_settings	shutdown	software_upgrade	system_certificate	system_config_check	whoami
	Run Keyword If	'${NGVERSION}' == '4.2'	Append To List	${COMMANDS}	cloud_enrollment	create_csr
	...	diagnostic_data
	Run Keyword If	'${NGVERSION}' > '4.2'	Append To List	${COMMANDS}	cloud_enrollment	create_csr
	...	diagnostic_data	exec	export_settings	import_settings
	CLI:Test Available Commands	@{COMMANDS}

Test show_settings command for valid ipv4
	CLI:Enter Path	/settings/static_routes/
	SUITE:Test Show_settings Command	ipv4	_ipv4_route1	${DESTINATION_BITMASK}	${GATEWAY_IP}

Test show_settings command for valid ipv6
	CLI:Enter Path	/settings/static_routes/
	SUITE:Test Show_settings Command	ipv6	_ipv6_route1	${DESTINATION_BITMASK}	${GATEWAY_IP}

Test Invalid Values
	Skip If	'${NGVERSION}' == '3.2'	Error messages are not shown in 3.2 (Bug 664)
	${PROTOCOL_IPV4}	Set Variable	_ipv4_route1
	${PROTOCOL_IPV6}	Set Variable	_ipv6_route1
	FOR	${CONNECTION}	IN	@{CONNECTIONS}
		SUITE:Test Set Validate Invalid Options	${CONNECTION}	${PROTOCOL_IPV4}	${WORD}
		SUITE:Test Set Validate Invalid Options NUMBER	${CONNECTION}	${PROTOCOL_IPV4}
		SUITE:Test Set Validate Invalid Options	${CONNECTION}	${PROTOCOL_IPV4}	${POINTS}
		SUITE:Test Set Validate Invalid Options	${CONNECTION}	${PROTOCOL_IPV4}	${ONE_WORD}
		SUITE:Test Set Validate Invalid Options	${CONNECTION}	${PROTOCOL_IPV4}	${ONE_POINTS}
		SUITE:Test Set Validate Invalid Options ONE_NUMBER	${CONNECTION}	${PROTOCOL_IPV4}
		SUITE:Test Set Validate Invalid Options EXCEEDED ipv4	${CONNECTION}	${PROTOCOL_IPV4}
		SUITE:Test Set Validate Invalid Options EMPTY	${CONNECTION}	${PROTOCOL_IPV4}
		SUITE:Test Set Validate Invalid Options	${CONNECTION}	${PROTOCOL_IPV6}	${WORD}
		SUITE:Test Set Validate Invalid Options NUMBER	${CONNECTION}	${PROTOCOL_IPV6}
		SUITE:Test Set Validate Invalid Options	${CONNECTION}	${PROTOCOL_IPV6}	${POINTS}
		SUITE:Test Set Validate Invalid Options	${CONNECTION}	${PROTOCOL_IPV6}	${ONE_WORD}
		SUITE:Test Set Validate Invalid Options ONE_NUMBER	${CONNECTION}	${PROTOCOL_IPV6}
		SUITE:Test Set Validate Invalid Options	${CONNECTION}	${PROTOCOL_IPV6}	${ONE_POINTS}
		SUITE:Test Set Validate Invalid Options EXCEEDED ipv6	${CONNECTION}	${PROTOCOL_IPV6}
		SUITE:Test Set Validate Invalid Options EMPTY	${CONNECTION}	${PROTOCOL_IPV6}
	END
	[Teardown]	SUITE:Test Teardown

*** Keywords ***
SUITE:Setup
	CLI:Open
	@{CONNECTIONS}=	CLI:Test Get Connections
	Set Suite Variable	@{CONNECTIONS}
	SUITE:Delete Static Routes
	${HAS_ETH1}=	CLI:Has ETH1
	Set Suite Variable	${HAS_ETH1}
	CLI:Enter Path	/settings/static_routes/
	FOR	${CONNECTION}	IN	@{CONNECTIONS}
		SUITE:Add Routes	${CONNECTION}	${DESTINATION_IP}	${GATEWAY_IP}	ipv6	ipv4
		SUITE:Add Routes	${CONNECTION}	${IPV6_DESTINATION_IP}	${IPV6_GATEWAY_IP}	ipv4	ipv6
	END

SUITE:Teardown
	SUITE:Delete Static Routes
	CLI:Close Connection

SUITE:Test Teardown
	CLI:Cancel	Raw
	SUITE:Delete Static Routes

SUITE:Delete Static Routes
	CLI:Enter Path	/settings/
	CLI:Write	delete static_routes/ -
	CLI:Commit

SUITE:Show Settings Should Not Contain
	${OUTPUT}=	CLI:Show Settings
	Should Not Contain	${OUTPUT}	/settings/static_routes/

SUITE:Test Show_settings Command
	[Arguments]	${TYPE}	${PROTOCOL}	${DESTINATION_BITMASK}	${GATEWAY_IP}
	${OUTPUT}=	CLI:Show Settings
	FOR	${CONNECTION}	IN	@{CONNECTIONS}
		${ROUTE}	Set Variable	${CONNECTION}${PROTOCOL}
		Run Keyword If	'${NGVERSION}' == '4.2'	Should Match Regexp	${OUTPUT}	/settings/static_routes/${ROUTE} index=route1
		Run Keyword If	${NGVERSION} >= 4.2	Should Match Regexp	${OUTPUT}	/settings/static_routes/${ROUTE} connection=${CONNECTION}
		Run Keyword If	${NGVERSION} >= 4.2	Should Match Regexp	${OUTPUT}	/settings/static_routes/${ROUTE} type=${TYPE}
		Run Keyword If	'${TYPE}' == 'ipv4'	Should Match Regexp	${OUTPUT}	/settings/static_routes/${ROUTE} destination_ip=${DESTINATION_IP}
		...	ELSE	Should Match Regexp	${OUTPUT}	/settings/static_routes/${ROUTE} destination_ip=${IPV6_DESTINATION_IP}
		Should Match Regexp	${OUTPUT}	/settings/static_routes/${ROUTE} destination_bitmask=${DESTINATION_BITMASK}
		Run Keyword If	'${TYPE}' == 'ipv4'	Should Match Regexp	${OUTPUT}	/settings/static_routes/${ROUTE} gateway_ip=${GATEWAY_IP}
		...	ELSE	Should Match Regexp	${OUTPUT}	/settings/static_routes/${ROUTE} gateway_ip=${IPV6_GATEWAY_IP}
		Should Match Regexp	${OUTPUT}	/settings/static_routes/${ROUTE} metric=${METRICO}
	END

SUITE:Test Set Validate Invalid Options
	[Arguments]	${CONNECTION}	${PROTOCOL}	${TESTING}
	${PATH}	Set Variable	/settings/static_routes/${CONNECTION}${PROTOCOL}
	CLI:Enter Path	${PATH}
	CLI:Test Set Validate Invalid Options	destination_ip	${TESTING}	Error: destination_ip: Invalid IP address.
	CLI:Test Set Validate Invalid Options	destination_bitmask	${TESTING}	Error: destination_bitmask: Invalid mask.
	CLI:Test Set Validate Invalid Options	gateway_ip	${TESTING}	Error: gateway_ip: Invalid IP address.
	CLI:Test Set Validate Invalid Options	metric	${TESTING}	Error: metric: Validation error.

SUITE:Test Set Validate Invalid Options NUMBER
	[Arguments]	${CONNECTION}	${PROTOCOL}
	${PATH}	Set Variable	/settings/static_routes/${CONNECTION}${PROTOCOL}
	CLI:Enter Path	${PATH}
	CLI:Test Set Validate Invalid Options	destination_ip	${NUMBER}	Error: destination_ip: Invalid IP address.
	CLI:Test Set Validate Invalid Options	destination_bitmask	${NUMBER}	Error: destination_bitmask: Invalid mask.
	CLI:Test Set Validate Invalid Options	destination_bitmask	${ELEVEN_NUMBER}	Error: destination_bitmask: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	gateway_ip	${NUMBER}	Error: gateway_ip: Invalid IP address.
	CLI:Test Set Validate Invalid Options	metric	${NUMBER}

SUITE:Test Set Validate Invalid Options ONE_NUMBER
	[Arguments]	${CONNECTION}	${PROTOCOL}
	${PATH}	Set Variable	/settings/static_routes/${CONNECTION}${PROTOCOL}
	CLI:Enter Path	${PATH}
	CLI:Test Set Validate Invalid Options	destination_ip	${ONE_NUMBER}	Error: destination_ip: Invalid IP address.
	CLI:Test Set Validate Invalid Options	destination_bitmask	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	gateway_ip	${ONE_NUMBER}	Error: gateway_ip: Invalid IP address.
	CLI:Test Set Validate Invalid Options	metric	${ONE_NUMBER}

SUITE:Test Set Validate Invalid Options EXCEEDED ipv4
	[Arguments]	${CONNECTION}	${PROTOCOL}
	${PATH}	Set Variable	/settings/static_routes/${CONNECTION}${PROTOCOL}
	CLI:Enter Path	${PATH}
	CLI:Test Set Validate Invalid Options	destination_ip	${EXCEEDED}	Error: destination_ip: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	destination_bitmask	${EXCEEDED}	Error: destination_bitmask: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	gateway_ip	${EXCEEDED}	Error: gateway_ip: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	metric	${EXCEEDED}	Error: metric: Exceeded the maximum size for this field.

SUITE:Test Set Validate Invalid Options EXCEEDED ipv6
	[Arguments]	${CONNECTION}	${PROTOCOL}
	${PATH}	Set Variable	/settings/static_routes/${CONNECTION}${PROTOCOL}
	CLI:Enter Path	${PATH}
	CLI:Test Set Validate Invalid Options	destination_ip	${EXCEEDED}	Error: destination_ip: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	destination_bitmask	${EXCEEDED}	Error: destination_bitmask: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	gateway_ip	${EXCEEDED}	Error: gateway_ip: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	metric	${EXCEEDED}	Error: metric: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	metric	${ELEVEN_WORD}	Error: metric: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	metric	${ELEVEN_NUMBER}	Error: metric: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	metric	${ELEVEN_POINTS}	Error: metric: Exceeded the maximum size for this field.

SUITE:Test Set Validate Invalid Options EMPTY
	[Arguments]	${CONNECTION}	${PROTOCOL}
	${PATH}	Set Variable	/settings/static_routes/${CONNECTION}${PROTOCOL}
	CLI:Enter Path	${PATH}
	CLI:Test Set Validate Invalid Options	gateway_ip	${EMPTY}
	CLI:Test Set Validate Invalid Options	metric	${EMPTY}
	CLI:Test Set Validate Invalid Options	destination_ip	${EMPTY}	Error: destination_ip: Invalid IP address.
	CLI:Test Set Validate Invalid Options	destination_bitmask	${EMPTY}	Error: destination_bitmask: Invalid mask.
	CLI:Test Set Validate Invalid Options	gateway_ip	${EMPTY}
	CLI:Test Set Validate Invalid Options	metric	${EMPTY}

SUITE:Add Routes
	[Arguments]	${CONNECTION}	${DESTINATION_IP}	${GATEWAY_IP}	@{OPTIONS}
	CLI:Add
	CLI:Set	connection=${CONNECTION} destination_ip=${DESTINATION_IP} destination_bitmask=${DESTINATION_BITMASK} gateway_ip=${GATEWAY_IP} metric=${METRICO}
	CLI:Test Set Field Options	type	@{OPTIONS}
	CLI:Save