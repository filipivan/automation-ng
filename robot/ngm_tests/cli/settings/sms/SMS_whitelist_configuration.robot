*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > SMS Whitelist page, ls, show, add, etc... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test adding valid phone
	Skip If	${NOT_NSR}	SMS Only in SR family
	CLI:Enter Path	/settings/sms_whitelist/
	CLI:Add
	CLI:Set	name=test_phone phone_number=+120300302121
	CLI:Commit
	CLI:Test Show Command Regexp	test_phone\\s+\\+120300302121

Test adding duplicate phone
	Skip If	${NOT_NSR}	SMS Only in SR family
	CLI:Enter Path	/settings/sms_whitelist/
	CLI:Add
	CLI:Set	name=test_phone phone_number=+120300302121
	${OUTPUT}=	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: name: Entry already exists.
	[Teardown]	CLI:Cancel	Raw

Test editing phone
	Skip If	${NOT_NSR}	SMS Only in SR family
	CLI:Enter Path	/settings/sms_whitelist/
	CLI:Test Ls Command	test_phone
	CLI:Enter Path	test_phone
	CLI:Test Show Command	name: test_phone	phone_number = +120300302121
	CLI:Test Validate Unmodifiable Field	name
	CLI:Set Field	phone_number	+12
	CLI:Commit
	CLI:Test Show Command	name: test_phone	phone_number = +12

Test deleting valid phone
	Skip If	${NOT_NSR}	SMS Only in SR family
	CLI:Enter Path	/settings/sms_whitelist/
	CLI:Test Show Command Regexp	test_phone\\s+\\+12
	CLI:Delete	test_phone

*** Keywords ***
SUITE:Setup
	CLI:Open
	${OUTPUT}=	CLI:Write	show /system/about/
	${NOT_NSR}=	Run Keyword And Return Status	Should Not Contain	${OUTPUT}	system: Nodegrid Services Router
	Set Suite Variable	${NOT_NSR}

SUITE:Teardown
	CLI:Close Connection

