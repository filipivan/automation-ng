*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > SMS Settings page, ls, show, add, etc... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test available commands after send tab-tab
	Skip If	not ${IS_NSR}	SMS Only in SR family
	CLI:Enter Path	/settings/sms_settings
	CLI:Test Available Commands	apply_settings	hostname	shell	cd	ls	show	change_password	pwd	show_settings
	...	commit	quit	shutdown	event_system_audit	reboot	software_upgrade	event_system_clear	revert
	...	system_certificate	exit	save_settings	system_config_check	factory_settings	set	whoami

Test visible fields for show command
	Skip If	not ${IS_NSR}	SMS Only in SR family
	CLI:Enter Path	/settings/sms_settings
	${STATUS}=	Run Keyword And Return Status	CLI:Test Show Command	enable_actions_via_incoming_sms = yes
	Run Keyword If	${STATUS}	CLI:Test Show Command	enable_actions_via_incoming_sms = yes	password = ********
	...	allow_apn	allow_simswap	allow_connect_and_disconnect	allow_mstatus	allow_reset	allow_info
	...	allow_factorydefault	allow_reboot
	...	ELSE	CLI:Test Show Command	enable_actions_via_incoming_sms = no

Test show_settings command
	Skip If	not ${IS_NSR}	SMS Only in SR family
	CLI:Enter Path	/settings/sms_settings
	${STATUS}=	Run Keyword And Return Status	CLI:Test Show Command	enable_actions_via_incoming_sms = yes
	${OUTPUT}=	CLI:Write	show_settings
	Should Match Regexp	${OUTPUT}	/settings/sms_settings enable_actions_via_incoming_sms=yes|no
	Run Keyword If	${STATUS}	Should Match Regexp	${OUTPUT}	/settings/sms_settings password=\\*+
	Run Keyword If	${STATUS}	Should Match Regexp	${OUTPUT}	/settings/sms_settings allow_apn=yes|no
	Run Keyword If	${STATUS}	Should Match Regexp	${OUTPUT}	/settings/sms_settings allow_simswap=yes|no
	Run Keyword If	${STATUS}	Should Match Regexp	${OUTPUT}	/settings/sms_settings allow_connect_and_disconnect=yes|no
	Run Keyword If	${STATUS}	Should Match Regexp	${OUTPUT}	/settings/sms_settings allow_mstatus=yes|no
	Run Keyword If	${STATUS}	Should Match Regexp	${OUTPUT}	/settings/sms_settings allow_reset=no|yes
	Run Keyword If	${STATUS}	Should Match Regexp	${OUTPUT}	/settings/sms_settings allow_info=yes|no
	Run Keyword If	${STATUS}	Should Match Regexp	${OUTPUT}	/settings/sms_settings allow_factorydefault=no|yes
	Run Keyword If	${STATUS}	Should Match Regexp	${OUTPUT}	/settings/sms_settings allow_reboot=yes|no

Test set available fields
	Skip If	not ${IS_NSR}	SMS Only in SR family
	CLI:Enter Path	/settings/sms_settings/
	CLI:Set Field	enable_actions_via_incoming_sms	yes
	CLI:Test Set Available Fields	allow_mstatus	allow_apn	password	allow_connect_and_disconnect	allow_reboot
	...	enable_actions_via_incoming_sms	allow_reset	allow_factorydefault	allow_simswap	allow_info
	[Teardown]	CLI:Revert

Test invalid values for fields
	Skip If	not ${IS_NSR}	SMS Only in SR family
	CLI:Enter Path	/settings/sms_settings/
	CLI:Set Field	enable_actions_via_incoming_sms	yes
	${COMMANDS}=	Create List		allow_mstatus	allow_apn	allow_connect_and_disconnect	allow_reboot
	...	allow_reset	allow_factorydefault	allow_simswap	allow_info	enable_actions_via_incoming_sms
	FOR	${COMMAND}	IN	@{COMMANDS}
		Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${COMMAND}	a	Error: Invalid value: a for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value: ${COMMAND}
		Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${COMMAND}	a	Error: Invalid value: a
	END
	[Teardown]	CLI:Revert

Test possible values for fields
	Skip If	not ${IS_NSR}	SMS Only in SR family
	CLI:Enter Path	/settings/sms_settings/
	CLI:Test Set Field Options	enable_actions_via_incoming_sms	no	yes
	CLI:Commit
	CLI:Set Field	enable_actions_via_incoming_sms	yes
	CLI:Commit
	CLI:Test Set Field Options	allow_mstatus	no	yes
	CLI:Test Set Field Options	allow_apn	no	yes
	CLI:Test Set Field Options	allow_connect_and_disconnect	no	yes
	CLI:Test Set Field Options	allow_reboot	no	yes
	CLI:Test Set Field Options	allow_factorydefault	no	yes
	CLI:Test Set Field Options	allow_simswap	no	yes
	CLI:Test Set Field Options	allow_info	no	yes
	[Teardown]	Run Keywords	Skip If	not ${IS_NSR}	SMS Only in SR family
	...	AND	CLI:Set Field	enable_actions_via_incoming_sms	no
	...	AND	CLI:Commit

Test possible fields for field=enable_actions_via_incoming_sms
	Skip If	not ${IS_NSR}	SMS Only in SR family
	CLI:Enter Path	/settings/sms_settings/
	CLI:Set Field	enable_actions_via_incoming_sms	no
	CLI:Commit
	CLI:Test Not Show Command	apn	simswap	connect_and_disconnect	allow_mstatus	allow_reset	allow_info	allow_factorydefault	allow_reboot
	CLI:Set Field	enable_actions_via_incoming_sms	yes
	CLI:Commit
	CLI:Test Show Command	allow_apn	allow_simswap	connect_and_disconnect	allow_mstatus	allow_reset	allow_info	allow_factorydefault	allow_reboot
	[Teardown]	CLI:Revert

*** Keywords ***
SUITE:Setup
	CLI:Open
	${IS_NSR}	CLI:Is Net SR
	Set Suite Variable	${IS_NSR}

SUITE:Teardown
	CLI:Close Connection

