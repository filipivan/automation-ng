*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > SMS Whitelist page, ls, show, add, etc... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test available commands after send tab-tab
	Skip If	${NOT_NSR}	SMS Only in SR family
	CLI:Enter Path	/settings/sms_whitelist/
	CLI:Test Available Commands	add	factory_settings	shell	apply_settings	hostname	show	cd	ls
	...	show_settings	change_password	pwd	shutdown	commit	quit	software_upgrade	delete	reboot
	...	system_certificate	event_system_audit	revert	system_config_check	event_system_clear	save_settings	whoami
	...	exit	set

Test visible fields for show command
	Skip If	${NOT_NSR}	SMS Only in SR family
	CLI:Enter Path	/settings/sms_whitelist/
	CLI:Test Show Command	name	phone number

Test show_settings command
	Skip If	${NOT_NSR}	SMS Only in SR family
	CLI:Enter Path	/settings/sms_whitelist/
	${STATUS}=	Run Keyword And Return Status	CLI:Test Show Command Regexp	\\d+
	Write	show_settings
	${OUTPUT}=	CLI:Read Until Prompt
	Run Keyword If	${STATUS}	Should Match Regexp	${OUTPUT}	/settings/sms_whitelist/.+ name=.+
	Run Keyword If	${STATUS}	Should Match Regexp	${OUTPUT}	/settings/sms_whitelist/.+ phone_number=\\+\\d+

Test set available fields
	Skip If	${NOT_NSR}	SMS Only in SR family
	CLI:Enter Path	/settings/sms_whitelist/
	CLI:Add
	CLI:Test Set Available Fields	name	phone_number
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=name
	Skip If	${NOT_NSR}	SMS Only in SR family
	CLI:Enter Path	/settings/sms_whitelist/
	CLI:Add
	CLI:Set Field	phone_number	+120300302121
	CLI:Test Set Field Invalid Options	name	${EMPTY}	Error: name: This field is empty or it contains invalid characters.	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=phone_number
	Skip If	${NOT_NSR}	SMS Only in SR family
	CLI:Enter Path	/settings/sms_whitelist/
	CLI:Add
	CLI:Set Field	name	test_phone
	CLI:Test Set Field Invalid Options	phone_number	${EMPTY}	Error: phone_number: This field should be in +<country code><phone number> format (phone number - only digits).	yes
	CLI:Test Set Field Invalid Options	phone_number	+a	Error: phone_number: This field should be in +<country code><phone number> format (phone number - only digits).	yes
	CLI:Test Set Field Invalid Options	phone_number	120300302121	Error: phone_number: This field should be in +<country code><phone number> format (phone number - only digits).	yes
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	${OUTPUT}=	CLI:Write	show /system/about/
	${NOT_NSR}=	Run Keyword And Return Status	Should Not Contain	${OUTPUT}	system: Nodegrid Services Router
	Set Suite Variable	${NOT_NSR}

SUITE:Teardown
	CLI:Close Connection

