*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Dial Up > Devices... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10	EXCLUDEIN6_2	#Disabled on version 4.2+
Default Tags	CLI	SSH	SHOW

Suite Setup	Setup
Suite Teardown	CLI:Close Connection

*** Variables ***
@{COMMANDS}=	event_system_audit	revert	event_system_clear	set	add	exit	shell	cd	hostname	show	change_password	ls	show_settings	commit	pwd	shutdown	delete	quit	whoami	drop_session	reboot

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Test Available Commands	@{COMMANDS}

Test visible fields for show command
	CLI:Test Show Command	name	state	status	device name	session info

Test available commands after add command
	CLI:Add
	CLI:Test Available Commands	cancel	commit	ls	save	set	show

Test available values for field speed
	CLI:Test Set Field Options	speed	38400	9600	57600	19200	115200

Test available values for field status
	CLI:Test Set Field Options	status	disabled	enabled

Test available values for field ppp_ipv4_address
	CLI:Test Set Field Options	ppp_ipv4_address	no_ipv4_address	local_ipv4	remote_ipv4

Test available values for field ppp_ipv6_address
	CLI:Test Set Field Options	ppp_ipv6_address	no_ipv6_address	local_ipv6	remote_ipv6

Test available values for field ppp_authentication
	CLI:Test Set Field Options	ppp_authentication	none	by_local_system	by_remote_peer

*** Keywords ***
Setup
	CLI:Open
	CLI:Enter Path	/settings/dial_up/devices/
