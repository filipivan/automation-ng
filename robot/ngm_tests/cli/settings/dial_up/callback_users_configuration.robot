*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Dial Up > Callback Users... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
@{COMMANDS}=	event_system_audit	revert	event_system_clear	set	add	exit	shell	cd	hostname	show
...	change_password	ls	show_settings	commit	pwd	shutdown	delete	quit	whoami	reboot
${PATH_ID}	1
${CALLBACK_USER}	user
${CALLBACK_NUMBER}	1

*** Test Cases ***
Test callback_user field with right values
	CLI:Enter Path	/settings/dial_up/callback_users/
	@{VALUES}=	Create List	${WORD}	${NUMBER}	${ONE_WORD}	${ONE_NUMBER}	${EXCEEDED}
	FOR		${VALUE}	IN	@{VALUES}
		CLI:Add
		CLI:Write	set callback_number=${CALLBACK_NUMBER}
		CLI:Test Set Field Invalid Options	callback_user	${VALUE}	${EMPTY}	yes
	END
	SUITE:Delete Callback Users
	[Teardown]	CLI:Revert  Raw

Test callback_number field with valid values
	CLI:Enter Path	/settings/dial_up/callback_users/
	@{VALUES}=	Create List	${NUMBER}	${ONE_NUMBER}	${EXCEEDED}
	FOR		${VALUE}	IN	@{VALUES}
		CLI:Add
		CLI:Write	set callback_user=${CALLBACK_USER}
		CLI:Test Set Field Invalid Options	callback_number	${VALUE}	${EMPTY}	yes
		Run Keyword If	'${NGVERSION}' >= '4.2'   CLI:Delete If Exists	1
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Delete If Exists	${CALLBACK_USER}
	END
	[Teardown]	CLI:Cancel  Raw

Test adding valid values
	CLI:Enter Path	/settings/dial_up/callback_users/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Delete If Exists	1
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Delete If Exists	testuser
	CLI:Add
	CLI:Set Field	callback_user	testuser
	CLI:Set Field	callback_number	12
	CLI:Test Show Command	callback_user = testuser	callback_number = 12
	${OUTPUT}=	CLI:Commit	yes
	Should Not Contain	${OUTPUT}	Error
	CLI:Test Show Command Regexp	\\s+testuser\\s+12
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Delete	1
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Delete	testuser

Test editing valid callback_user
	CLI:Enter Path	/settings/dial_up/callback_users/
	CLI:Add
	CLI:Write	set callback_user=testuser callback_number=12
	CLI:Commit
	CLI:Test Show Command Regexp	\\s+testuser\\s+12
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Enter Path	1
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Enter Path	testuser
	CLI:Test Show Command	callback_user: testuser	callback_number = 12
	CLI:Test Set Available Fields	callback_number
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	callback_number	a	Error: callback_number: This field cannot be empty and only accepts digits and ,(comma).
	CLI:Set Field	callback_number	15
	CLI:Test Show Command	callback_user: testuser	callback_number = 15
	CLI:Enter Path	/settings/dial_up/callback_users/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Delete	1
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Delete	testuser

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Delete Callback Users

SUITE:Teardown
	SUITE:Delete Callback Users
	CLI:Close Connection

SUITE:Delete Callback Users
	CLI:Enter Path	/settings/dial_up/callback_users/
	CLI:Delete All