*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Dial Up > Services... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	CLI:Close Connection

*** Variables ***
@{COMMANDS}=	exit	set	hostname	shell	cd	ls	show	change_password	pwd	show_settings	commit	quit	shutdown
...	event_system_audit	reboot	whoami	event_system_clear	revert

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/dial_up/services/
	CLI:Test Available Commands	@{COMMANDS}

Test visible fields for show command
	CLI:Test Show Command	login_session	ppp_connection

Test available set fields
	CLI:Test Set Available Fields	login_session	ppp_connection

Test login_session field
	CLI:Enter Path	/settings/dial_up/services/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	login_session	callback	disabled	enabled
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	login_session	callback	disabled	enabled

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	login_session	${EMPTY}	Error: Missing value for parameter: login_session
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	login_session	${EMPTY}	Error: Missing value: login_session

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	login_session	${WORD}	Error: Invalid value: ${WORD} for parameter: login_session
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	login_session	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	login_session	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: login_session
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	login_session	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	login_session	${POINTS}	Error: Invalid value: ${POINTS} for parameter: login_session
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	login_session	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	login_session	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: login_session
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	login_session	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	login_session	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: login_session
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	login_session	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	login_session	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: login_session
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	login_session	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	login_session	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: login_session
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	login_session	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test ppp_connection field
	CLI:Enter Path	/settings/dial_up/services/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	ppp_connection	callback	disabled	enabled
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	ppp_connection	callback	disabled	enabled

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ppp_connection	${EMPTY}	Error: Missing value for parameter: ppp_connection
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ppp_connection	${EMPTY}	Error: Missing value: ppp_connection

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ppp_connection	${WORD}	Error: Invalid value: ${WORD} for parameter: ppp_connection
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ppp_connection	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ppp_connection	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: ppp_connection
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ppp_connection	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ppp_connection	${POINTS}	Error: Invalid value: ${POINTS} for parameter: ppp_connection
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ppp_connection	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ppp_connection	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: ppp_connection
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ppp_connection	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ppp_connection	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: ppp_connection
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ppp_connection	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ppp_connection	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: ppp_connection
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ppp_connection	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ppp_connection	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: ppp_connection
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ppp_connection	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test if revert command works
	${OUTPUT}=	CLI:Revert
	Should Not Contain	${OUTPUT}	Error

*** Keywords ***
SUITE:Setup
	CLI:Open
	Run Keyword If	'${NGVERSION}' >= '4.2'	Remove Values From List	${COMMANDS}	show_settings
