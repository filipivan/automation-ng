*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Dial Up > Devices... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10	EXCLUDEIN6_2
Default Tags	CLI	SSH	SHOW

Suite Setup	Setup
Suite Teardown	CLI:Close Connection

*** Variables ***
@{COMMANDS}=	event_system_audit	revert	event_system_clear	set	add	exit	shell	cd	hostname	show	change_password	ls	show_settings	commit	pwd	shutdown	delete	quit	whoami	drop_session	reboot

*** Test Cases ***
Test add device
	Set Tags	CRITICAL
	Write	ls
	${OUTPUT}=	CLI:Read Until Prompt
	${DUMMY_DEVICE_CONSOLE_NAME}=	Set Variable	a_name
	${CONTAINS_NAME}=	Evaluate	"${DUMMY_DEVICE_CONSOLE_NAME}" in """${OUTPUT}"""
	Run Keyword If	${CONTAINS_NAME}	CLI:Delete Device	${DUMMY_DEVICE_CONSOLE_NAME}
	CLI:Add Device	${DUMMY_DEVICE_CONSOLE_NAME}
	CLI:Delete Device	${DUMMY_DEVICE_CONSOLE_NAME}

*** Keywords ***
CLI:Add Device
	[Arguments]	${DEVICE}
	CLI:Enter Path	/settings/dial_up/devices/
	${DEVICE_NAME}=	Set Variable	dummydeviceconsole
	CLI:Add

	Write	set name=${DEVICE} device_name=${DEVICE_NAME}
	CLI:Read Until Prompt

	CLI:Commit

	Write	cd ${DEVICE}
	CLI:Read Until Prompt

	${list}	Create List	event_system_clear	revert	exit	set	cancel	hostname	shell	cd	ls	show	change_password	pwd	commit	quit	shutdown	event_system_audit
	...	reboot	whoami	show_settings
	CLI:Test Available Commands	@{list}

	CLI:Test Show Command	name	status	${DEVICE_NAME}	speed	phone_number	ppp_idle_timeout	init_chat	ppp_ipv4_address	ppp_ipv6_address	ppp_authentication

	CLI:Test Set Available Fields	device_name	ppp_authentication	ppp_ipv6_address	init_chat	ppp_idle_timeout	speed	phone_number	ppp_ipv4_address	status

	Write	cd ..
	CLI:Read Until Prompt

CLI:Delete Device
	[Arguments]	${DEVICE}
	Write	delete ${DEVICE}
	CLI:Read Until Prompt

	CLI:Commit

*** Keywords ***
Setup
	CLI:Open
