*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Dial Up > Callback Users... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Variables ***
@{COMMANDS}=	event_system_audit	revert	event_system_clear	set	add	exit	shell	cd	hostname	show
...	change_password	ls	show_settings	commit	pwd	shutdown	delete	quit	whoami	reboot
${PATH_ID}	1
${CALLBACK_USER}	user
${CALLBACK_NUMBER}	1

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/dial_up/callback_users/
	Run Keyword If	'${NGVERSION}' >= '4.2'	Remove Values From List	${COMMANDS}	show_settings
	CLI:Test Available Commands	@{COMMANDS}

Test visible fields for show command
	CLI:Test Show Command	callback_user	callback number

Test available commands after add record
	CLI:Enter Path	/settings/dial_up/callback_users/
	CLI:Add
	CLI:Test Available Commands	cancel	commit	ls	save	set	show
	[Teardown]	CLI:Cancel	Raw

Test available fields for set command
	CLI:Enter Path	/settings/dial_up/callback_users/
	CLI:Add
	CLI:Test Set Available Fields	callback_number	callback_user
	[Teardown]	CLI:Cancel	Raw

Test error after commit empty record
	CLI:Enter Path	/settings/dial_up/callback_users/
	CLI:Add
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Message After Commit	Error: callback_user: This field is empty or it contains invalid characters.
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Message After Commit	Error: callback_user: This field is empty or it contains invalid characters.Error: callback number: This field cannot be empty and only accepts digits and ,(comma).
	[Teardown]	CLI:Cancel	Raw

Test callback_user field with wrong values
	CLI:Enter Path	/settings/dial_up/callback_users/
	CLI:Add
	CLI:Write	set callback_number=${CALLBACK_NUMBER}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	callback_user	${EMPTY}	Error: callback_user: This field is empty or it contains invalid characters.	yes
	Run Keyword If	${NGVERSION} == '3.2'	CLI:Test Set Field Invalid Options	callback_user	${EMPTY}	Error: callback_user: Field must not be empty.	yes
	@{VALUES}=	Create List	${POINTS}	${ONE_POINTS}
	FOR		${VALUE}	IN	@{VALUES}
		CLI:Test Set Field Invalid Options	callback_user	${VALUE}	Error: callback_user: This field is empty or it contains invalid characters.	yes
	END
	[Teardown]	CLI:Cancel	Raw

Test callback_number field
	# 3.2 is not showing error. 3.2 will not be fixed
	Run Keyword If	'${NGVERSION}' == '3.2'	Set Tags
	CLI:Enter Path	/settings/dial_up/callback_users/
	CLI:Add
	CLI:Write	set callback_user=${CALLBACK_USER}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	callback_number	${EMPTY}	Error: callback_number: This field cannot be empty and only accepts digits and ,(comma).	yes
	Run Keyword If	${NGVERSION} == '3.2'	CLI:Test Set Field Invalid Options	callback_number	${EMPTY}	Error: callback number: This field cannot be empty and only accepts digits and ,(comma).	yes

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	callback_number	${WORD}	Error: callback_number: This field cannot be empty and only accepts digits and ,(comma).	yes
	Run Keyword If	${NGVERSION} == '3.2'	CLI:Test Set Field Invalid Options	callback_number	${WORD}	Error: callback number: This field cannot be empty and only accepts digits and ,(comma).	yes

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	callback_number	${POINTS}	Error: callback_number: This field cannot be empty and only accepts digits and ,(comma).	yes
	Run Keyword If	${NGVERSION} == '3.2'	CLI:Test Set Field Invalid Options	callback_number	${POINTS}	Error: callback number: This field cannot be empty and only accepts digits and ,(comma).	yes

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	callback_number	${ONE_WORD}	Error: callback_number: This field cannot be empty and only accepts digits and ,(comma).	yes
	Run Keyword If	${NGVERSION} == '3.2'	CLI:Test Set Field Invalid Options	callback_number	${ONE_WORD}	Error: callback number: This field cannot be empty and only accepts digits and ,(comma).	yes

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	callback_number	${ONE_POINTS}	Error: callback_number: This field cannot be empty and only accepts digits and ,(comma).	yes
	Run Keyword If	${NGVERSION} == '3.2'	CLI:Test Set Field Invalid Options	callback_number	${ONE_POINTS}	Error: callback number: This field cannot be empty and only accepts digits and ,(comma).	yes
	[Teardown]	CLI:Cancel	Raw
