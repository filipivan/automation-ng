*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Dial Up... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW
Suite Setup	CLI:Open
Suite Teardown	Close Connection

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/dial_up/
	CLI:Test Available Commands	add	delete	hostname	revert	show_settings	whoami
	...	apply_settings	event_system_audit	ls	save_settings	shutdown
	...	cd	event_system_clear	pwd	set	software_upgrade
	...	change_password	exit	quit	shell	system_certificate
	...	commit	factory_settings	reboot	show	system_config_check

Test ls command
	CLI:Enter Path	/settings/dial_up/
	CLI:Test Ls Command	services	callback_users
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Ls Command	devices

Test show_settings command
	CLI:Enter Path	/settings/dial_up/
	Write	show_settings
	${OUTPUT}=	CLI:Read Until Prompt
	Should Match Regexp	${OUTPUT}	/settings/dial_up/services login_session=disabled|enabled
	Should Match Regexp	${OUTPUT}	/settings/dial_up/services ppp_connection=disabled|enabled
