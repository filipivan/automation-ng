*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Wireless Modem page, ls, show, etc... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_0	EXCLUDEIN4_1	EXCLUDEIN4_2	EXCLUDEIN4_3	NON-CRITICAL	NEED-REVIEW
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${MODEM}	Channel-B
${FILE}	SWI9X50C_01.08.04.00_GENERIC_002.012_000.zip

*** Test Cases ***
Test reset modem
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	${NO_WMODEM}	No wmodem present
	CLI:Enter Path	/settings/wireless_modem/global/
	CLI:Connect As Root
	${MODEM_2RESET}=	Catenate	SEPARATOR=,	@{MODEMS}
	CLI:Switch Connection	root_session
	${BEFORE}=	CLI:Write	mmcli -L
	${QTD_BEFORE}=	Fetch From Right	${BEFORE}	Found
	${QTD_BEFORE}=	Fetch From Left	${QTD_BEFORE}	modems
	CLI:Switch Connection	default
	CLI:Reset	${MODEM_2RESET}
	SUITE:Test Modem Reset	${QTD_BEFORE}
	CLI:Switch Connection	root_session
	CLI:Close Current Connection
	[Teardown]	CLI:Switch Connection	default

Test delete file
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	${NO_WMODEM}	No wmodem present
	FOR	${MODEM}	IN	@{MODEMS}
		CLI:Enter Path	/settings/wireless_modem/global/${MODEM}/firmware
		CLI:Test Show Command	01.08.04.00_GENERIC
		CLI:Delete Confirm	01.08.04.00_GENERIC
		CLI:Test Not Show Command	01.08.04.00_GENERIC
	END

Test upgrade zip file
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	${NO_WMODEM}	No wmodem present
	FOR	${MODEM}	IN	@{MODEMS}
		CLI:Enter Path	/settings/wireless_modem/global/${MODEM}/firmware
		CLI:Write	upgrade
		CLI:Set	file_location=local_system filename=${FILE}
		Write	commit
		Set Client Configuration	timeout=5s
		${OUTPUT}=	Wait Until Keyword Succeeds	3m	5s	CLI:Read Until Prompt
		Set Client Configuration	timeout=30s
		CLI:Test Show Command	01.08.04.00_GENERIC
	END

*** Keywords ***
SUITE:Setup
	CLI:Open
	${HAS_WMODEM_SUPPORT}=	CLI:Has Wireless Modem Support
	Set Suite Variable	${HAS_WMODEM_SUPPORT}
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	${NO_WMODEM}=	SUITE:Check If Modem Exists
	Set Suite Variable	${NO_WMODEM}
	Skip If	${NO_WMODEM}	No wmodem present

SUITE:Teardown
	CLI:Close Connection

SUITE:Check If Modem Exists
	CLI:Enter Path	/settings/wireless_modem/global
	${MODEMS}=	CLI:Write	ls	user=Yes
	${NO_WMODEM}=	Run Keyword And Return Status	Should Be Equal	${MODEMS}	${EMPTY}
	${MODEMS}=	Split String	${MODEMS}	/
	Remove Values From List	${MODEMS}	${EMPTY}
	Set Suite Variable	${MODEMS}
	[Return]	${NO_WMODEM}

SUITE:Test Modem Reset
	[Arguments]	${QTD_BEFORE}
	CLI:Switch Connection	root_session
	${AFTER}=	CLI:Write	mmcli -L
	${QTD_AFTER}=	Fetch From Right	${AFTER}	Found
	${QTD_AFTER}=	Fetch From Left	${QTD_AFTER}	modems
	Should Be Equal	${QTD_BEFORE}	${QTD_AFTER}


