*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Wireless Modem page, ls, show, etc... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${FTP_FAKE}	ftp://1.1.1.1:21

*** Test Cases ***
Test folders
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	CLI:Enter Path	/settings/wireless_modem
	CLI:Test Ls Command	global
	CLI:Test Not Show Command	slot

Test available commands after send tab-tab
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	CLI:Enter Path	/settings/wireless_modem/global
	CLI:Test Available Commands	apply_settings	hostname	shell	factory_settings	shell
	...	apply_settings	hostname	show	commit	quit	software_upgrade	cd	ls	show_settings
	...	change_password	pwd	shutdown	reboot	system_certificate	event_system_audit	reset	system_config_check
	...	event_system_clear	revert	whoami	exit	save_settings
	Run Keyword If	${HAS_POWERSUPPLY}	CLI:Test Available Commands	acknowledge_alarm_state

Test visible fields for show command
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	CLI:Enter Path	/settings/wireless_modem/global
	CLI:Test Show Command Regexp	slot\\s+interface\\s+state\\s+firmware\\sversion

Test show_settings command
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	CLI:Enter Path	/settings/wireless_modem/global
	${OUTPUT}=	CLI:Write	show_settings
	Should Not Match Regexp	${OUTPUT}	/settings/wireless_modem/global

Test ls command
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	CLI:Enter Path	/settings/wireless_modem/global
	${MODEMS}=	CLI:Ls

Test modem drilldown
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	${NO_WMODEM}	No wmodem present
	FOR	${MODEM}	IN	@{MODEMS}
		CLI:Enter Path	/settings/wireless_modem/global/${MODEM}
		CLI:Test Ls Command  firmware
		CLI:Test Not Show Command 	index
	END

Test show in firmware folder
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	${NO_WMODEM}	No wmodem present
	FOR	${MODEM}	IN	@{MODEMS}
		CLI:Enter Path	/settings/wireless_modem/global/${MODEM}/firmware
		CLI:Test Show Command Regexp	build\\sid\\s+type\\s+unique\\sid
	END

Test show in upgrade action
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	${NO_WMODEM}	No wmodem present
	FOR	${MODEM}	IN	@{MODEMS}
		CLI:Enter Path	/settings/wireless_modem/global/${MODEM}/firmware
		CLI:Write	upgrade
		CLI:Test Show Command	file_location = local_system	filename =	File must be previously copied to /var/sw directory.
		CLI:Cancel
	END

Test validation for upgrade fields
	[Tags]	NON-CRITICAL	BUG_NG_11516
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	${NO_WMODEM}	No wmodem present
	FOR	${MODEM}	IN	@{MODEMS}
		CLI:Enter Path	/settings/wireless_modem/global/${MODEM}/firmware
		CLI:Write	upgrade
		CLI:Test Set Available Fields	filename	file_location
		CLI:Test Set Field Options	file_location	local_system	remote_server
		CLI:Set  file_location=local_system filename=${EMPTY}
		${OUTPUT}=	CLI:Commit	Raw
		Should Contain	${OUTPUT}	Error: Incorrect filename information.

		CLI:Set	file_location=remote_server url=${EMPTY}
		${OUTPUT}=	CLI:Commit	Raw
		Should Contain	${OUTPUT}	Error: Incorrect URL information.

		CLI:Set Field	url	invalid_url
		${OUTPUT}=	CLI:Commit	Raw
		Should Contain	${OUTPUT}	Error: Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>

		CLI:Set Field	url	https://invalid/invalid
		${OUTPUT}=	CLI:Commit	Raw
		Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	Error: File Transfer Failed

		CLI:Set Field	url	${FTP_FAKE}
		${OUTPUT}=	CLI:Commit	Raw
		Should Contain	${OUTPUT}	Error: Protocol requires username/password.
		CLI:Cancel
	END

*** Keywords ***
SUITE:Setup
	CLI:Open
	${HAS_WMODEM_SUPPORT}=	CLI:Has Wireless Modem Support
	Set Suite Variable	${HAS_WMODEM_SUPPORT}
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	${NO_WMODEM}=	SUITE:Check If Modem Exists
	Set Suite Variable 	${NO_WMODEM}
	${HAS_POWERSUPPLY}=	CLI:Has Dual Power or Fans
	Set Suite Variable	${HAS_POWERSUPPLY}	${HAS_POWERSUPPLY}

SUITE:Teardown
	CLI:Close Connection

SUITE:Check If Modem Exists
	CLI:Enter Path	/settings/wireless_modem/global
	${MODEMS}=	CLI:Write	ls	user=Yes
	${NO_WMODEM}=	Run Keyword And Return Status	Should Be Equal	${MODEMS}	${EMPTY}
	${MODEMS}=	Split String	${MODEMS}	/
	Remove Values From List	${MODEMS}	${EMPTY}
	Set Suite Variable	${MODEMS}
	[Return]	${NO_WMODEM}

