*** Settings ***
Resource	../../../init.robot
Documentation	Tests Settings > Devices > device Custom Fields... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	NEED-REVIEW
Default Tags	SSH
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE_NAME}	device_custom_fields_configuration
${FIELD_NAME}	name
${FIELD_VALUE}	value

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/custom_fields
	CLI:Test Available Commands	add	exit	set	apply_settings	factory_settings	shell	cd
	...	hostname	show	change_password	ls	show_settings	commit	pwd	shutdown
	...	delete	quit	software_upgrade	edit	reboot	system_certificate
	...	event_system_audit	revert	system_config_check	event_system_clear	save_settings	whoami

Test visible fields for show command
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/custom_fields
	Run Keyword If	${NGVERSION} <= 5.0	CLI:Test Show Command	field name	field value
	...	ELSE	CLI:Test Show Command	field_name	field_value

Test available fields to set
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/custom_fields
	CLI:Add
	CLI:Test Set Available Fields	field_name	field_value
	[Teardown]	CLI:Cancel	Raw

Test valid values for fields
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/custom_fields
	CLI:Add
	CLI:Set Field	field_name	${FIELD_NAME}
	CLI:Set Field	field_value	${FIELD_VALUE}
	CLI:Commit
	CLI:Test Ls Command	${FIELD_NAME}
	CLI:Test Show Command	${FIELD_NAME}	${FIELD_VALUE}
	[Teardown]	CLI:Cancel	Raw

Test show command for valid custom field directory
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/custom_fields/${FIELD_NAME}
	Run Keyword If	'${NGVERSION}' <= '5.0'	CLI:Test Show Command	field name: ${FIELD_NAME}	field value: ${FIELD_VALUE}
	...	ELSE	CLI:Test Show Command	field_name: ${FIELD_NAME}	field_value: ${FIELD_VALUE}

Test edit command for valid custom field
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/custom_fields
	CLI:Edit	${FIELD_NAME}
	CLI:Test Show Command	field_value = ${FIELD_VALUE}
	CLI:Set Field	field_value	edited_value
	CLI:Commit
	CLI:Test Show Command	${FIELD_NAME}	edited_value
	[Teardown]	CLI:Cancel	Raw

Test delete command for valid custom field
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/custom_fields
	CLI:Delete	${FIELD_NAME}

Test EMPTY value for field_name
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/custom_fields/
	CLI:Add
	CLI:Set Field	field_value	${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	field_name	${EMPTY}	Error: field name: Field must not be empty.	yes
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	field_name	${EMPTY}	Error: field_name: Field must not be empty.	yes
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	field_name	name{	Error: field_name: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

Test EMPTY value for field_value
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/custom_fields/
	CLI:Add
	CLI:Set Field	field_name	${FIELD_VALUE}
	#3.2 Lets you commit if field_value is empty, 4.0 does not, 4.1 does
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	field_value	${EMPTY}	Error: field_value: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

Test with fields=field_name | field_value
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/custom_fields/
	#	Getting errors with	${POINTS} on FOR loop, but on CLI it works | "!@-$%^*#()" turns into "!@-$%^|#()" <it turns '*' in '|'> on robot test
	@{TEST_VARIABLES}=	Create List	${WORD}	${NUMBER}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${TWO_WORD}	${TWO_NUMBER}	${TWO_POINTS}
	FOR	${TEST_VARIABLES}	IN	@{TEST_VARIABLES}
		CLI:Add
		CLI:Test Set Field Invalid Options	field_name	${TEST_VARIABLES}
		CLI:Test Set Field Invalid Options	field_value	${TEST_VARIABLES}	save=yes
		CLI:Delete If Exists	${TEST_VARIABLES}
	END
	[Teardown]	CLI:Cancel	Raw

Test EXCEEDED value for field_name | field_value
	Skip If	'${NGVERSION}' == '3.2'	Error message not configured on 3.2
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/custom_fields/
	CLI:Add
	CLI:Set Field	field_value	${FIELD_VALUE}
	${SUPER_EXCEEDING_STRING}	Set Variable	${EXCEEDED}${EXCEEDED}${EXCEEDED}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Set	field_name=${SUPER_EXCEEDING_STRING}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Set	field_value=${SUPER_EXCEEDING_STRING}
	Write	commit
	${OUTPUT}	Read Until Prompt
	Should Contain	${OUTPUT}	Error: field_name: Validation error.
	Should Contain	${OUTPUT}	Error: field_value: Validation error.
	[Teardown]	CLI:Cancel	Raw

Test with duplicate entry
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/custom_fields/
	CLI:Add
	CLI:Set	field_name=${FIELD_NAME} field_value=${FIELD_VALUE}
	CLI:Save
	CLI:Add
	CLI:Set	field_name=${FIELD_NAME} field_value=${FIELD_VALUE}
	${OUTPUT}=	CLI:Commit	Yes
	Run Keyword If	'${NGVERSION}' >= '4.0'	Should Contain	${OUTPUT}	Error: field_name: Entry already exists.
	Run Keyword If	'${NGVERSION}' < '4.0'	Should Not Contain	${OUTPUT}	Error: field name: Entry already exists.
	[Teardown]	CLI:Cancel	Raw

*** Keyword ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/devices
	@{DEVICELIST}=	CLI:Get Devices List	all	${PROTECTED_DEVICE_TYPES}
	CLI:Delete Devices	@{DEVICELIST}
	CLI:Delete If Exists Confirm	${DEVICE_NAME}
	CLI:Add Device	${DEVICE_NAME}	ilo
	${OUTPUT}=	CLI:Test Show Command
	Should Contain	${OUTPUT}	${DEVICE_NAME}

SUITE:Teardown
	CLI:Enter Path	/settings/devices
	CLI:Delete If Exists Confirm	${DEVICE_NAME}
	CLI:Add Device	${DEVICE_NAME}	ilo
	CLI:Close Connection