*** Settings ***
Resource	../../../init.robot
Documentation	Tests Settings > Devices > device > commands
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NUMBER_ZERO}	0

*** Test Cases ***
Test available commands in add command form
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands
	CLI:Add
	CLI:Test Available Commands	cancel	commit	ls	save	show	set
	CLI:Cancel

Create Custom Command File
	[Tags]	EXCLUDEIN3_2
	CLI:Create File Remotely	/etc/scripts/custom_commands/test.py	"def CustomTest(dev):"
	CLI:Append To File Remotely	/etc/scripts/custom_commands/test.py	" print('hello')"

Check custom command
	[Setup]	SUITE:Add Custom Command
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/access/${DUMMY_DEVICE_CONSOLE_NAME}
	${ret}=	CLI:Write	CustomTest
	Should Contain	${ret}	hello
	[Teardown]	SUITE:Delete Custom Command

Test with add function - custom_commands
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands
	CLI:Add
	CLI:Test Set Validate Normal Fields	command	kvm	outlet	custom_commands
	CLI:Set Field	command	custom_commands
	CLI:Test Set Validate Normal Fields	enabled	no	yes

Test with custom_command_scriptINDEX
	[Documentation]	Checks valid and invalid values on v5.6+. On v5.4 and previous it's not working and won't be fixed as per comment from Davi on BUG_NG_9836
	[Tags]	EXCLUDEIN3_2

	FOR	${INDEX}	IN RANGE	1	10
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	custom_command_script${INDEX}	${EMPTY}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	custom_command_script${INDEX}	${WORD}	Error: Invalid value: ${WORD} for parameter: custom_command_script${INDEX}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	custom_command_script${INDEX}	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: custom_command_script${INDEX}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	custom_command_script${INDEX}	${POINTS}	Error: Invalid value: ${POINTS} for parameter: custom_command_script${INDEX}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	custom_command_script${INDEX}	${NUMBER_ZERO}	Error: Invalid value: ${NUMBER_ZERO} for parameter: custom_command_script${INDEX}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	custom_command_script${INDEX}	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: custom_command_script${INDEX}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	custom_command_script${INDEX}	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: custom_command_script${INDEX}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	custom_command_script${INDEX}	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: custom_command_script${INDEX}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	custom_command_script${INDEX}	${TWO_NUMBER}	Error: Invalid value: ${TWO_NUMBER} for parameter: custom_command_script${INDEX}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	custom_command_script${INDEX}	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: custom_command_script${INDEX}
		CLI:Test Set Validate Normal Fields	custom_command_script${INDEX}	test.py
	END
	[Teardown]	CLI:Cancel	Raw

Test with custom_command_enabledINDEX
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands
	CLI:Add
	CLI:Test Set Validate Normal Fields	command	custom_commands	kvm	outlet
	CLI:Set Field	command	custom_commands
	CLI:Test Set Validate Normal Fields	enabled	no	yes

	FOR	${INDEX}	IN RANGE	1	10
		CLI:Test Set Validate Normal Fields	custom_command_enabled${INDEX}	no	yes
		CLI:Test Set Validate Invalid Options	custom_command_enabled${INDEX}	${EMPTY}	Error: Missing value for parameter: custom_command_enabled${INDEX}
		CLI:Test Set Validate Invalid Options	custom_command_enabled${INDEX}	${WORD}	Error: Invalid value: ${WORD} for parameter: custom_command_enabled${INDEX}
		CLI:Test Set Validate Invalid Options	custom_command_enabled${INDEX}	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: custom_command_enabled${INDEX}
		CLI:Test Set Validate Invalid Options	custom_command_enabled${INDEX}	${POINTS}	Error: Invalid value: ${POINTS} for parameter: custom_command_enabled${INDEX}
		CLI:Test Set Validate Invalid Options	custom_command_enabled${INDEX}	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: custom_command_enabled${INDEX}
		CLI:Test Set Validate Invalid Options	custom_command_enabled${INDEX}	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: custom_command_enabled${INDEX}
		CLI:Test Set Validate Invalid Options	custom_command_enabled${INDEX}	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: custom_command_enabled${INDEX}
		CLI:Test Set Validate Invalid Options	custom_command_enabled${INDEX}	${TWO_WORD}	Error: Invalid value: ${TWO_WORD} for parameter: custom_command_enabled${INDEX}
		CLI:Test Set Validate Invalid Options	custom_command_enabled${INDEX}	${TWO_NUMBER}	Error: Invalid value: ${TWO_NUMBER} for parameter: custom_command_enabled${INDEX}
		CLI:Test Set Validate Invalid Options	custom_command_enabled${INDEX}	${TWO_POINTS}	Error: Invalid value: ${TWO_POINTS} for parameter: custom_command_enabled${INDEX}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	custom_command_enabled${INDEX}	${ONE_WORD} ${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: custom_command_enabled${INDEX}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	custom_command_enabled${INDEX}	${ONE_WORD} ${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: custom_command_enabled${INDEX}
		CLI:Test Set Validate Invalid Options	custom_command_enabled${INDEX}	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: custom_command_enabled${INDEX}
	END
	[Teardown]	CLI:Cancel	Raw

Test with custom_command_labelINDEX
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands
	CLI:Add
	CLI:Test Set Validate Normal Fields	command	custom_commands	kvm	outlet
	CLI:Set Field	command	custom_commands
	CLI:Test Set Validate Normal Fields	enabled	no	yes

	FOR	${INDEX}	IN RANGE	1	10
		CLI:Test Set Validate Normal Fields	custom_command_enabled${INDEX}	no	yes
		CLI:Test Set Validate Invalid Options	custom_command_label${INDEX}	${EMPTY}
		CLI:Test Set Validate Invalid Options	custom_command_label${INDEX}	${WORD}
		CLI:Test Set Validate Invalid Options	custom_command_label${INDEX}	${NUMBER}
		CLI:Test Set Validate Invalid Options	custom_command_label${INDEX}	${POINTS}
		CLI:Test Set Validate Invalid Options	custom_command_label${INDEX}	${ONE_WORD}
		CLI:Test Set Validate Invalid Options	custom_command_label${INDEX}	${ONE_NUMBER}
		CLI:Test Set Validate Invalid Options	custom_command_label${INDEX}	${ONE_POINTS}
		CLI:Test Set Validate Invalid Options	custom_command_label${INDEX}	${TWO_WORD}
		CLI:Test Set Validate Invalid Options	custom_command_label${INDEX}	${TWO_NUMBER}
		CLI:Test Set Validate Invalid Options	custom_command_label${INDEX}	${TWO_POINTS}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	custom_command_label${INDEX}	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument for command: set
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	custom_command_label${INDEX}	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument: set
		CLI:Test Set Validate Invalid Options	custom_command_label${INDEX}	${EXCEEDED}
	END
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/devices
	@{DEVICELIST}=	CLI:Get Devices List	all	${PROTECTED_DEVICE_TYPES}
	CLI:Delete Devices	@{DEVICELIST}
	${OUTPUT}=	CLI:Test Show Command
	Run Keyword If	$DUMMY_DEVICE_CONSOLE_NAME in $OUTPUT	CLI:Delete Devices	${DUMMY_DEVICE_CONSOLE_NAME}
	CLI:Add Device	${DUMMY_DEVICE_CONSOLE_NAME}	ilo
	${OUTPUT}=	CLI:Test Show Command
	Should Contain	${OUTPUT}	${DUMMY_DEVICE_CONSOLE_NAME}

SUITE:Teardown
	@{DEVICELIST}=	CLI:Get Devices List	all	${PROTECTED_DEVICE_TYPES}
	CLI:Delete Devices	@{DEVICELIST}
	CLI:Delete If Exists	${DUMMY_DEVICE_CONSOLE_NAME}
	CLI:Close Connection

SUITE:Add Custom Command
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands
	Skip If	'${NGVERSION}' == '3.2'	Disabled on 3.2
	CLI:Add
	CLI:Set Field	command	custom_commands
	CLI:Set Field	custom_command_enabled1	yes
	CLI:Set Field	custom_command_label1	CustomTest
	CLI:Set Field	custom_command_script1	test.py
	CLI:Commit

SUITE:Delete Custom Command
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands
	CLI:Delete If Exists	custom_commands