*** Settings ***
Resource	../../../init.robot
Documentation	Tests Settings > Devices > device > Commands... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	SSH

Suite Setup	CLI:Suite Setup
Suite Teardown	Delete Device And Close Connection

*** Variables ***
${NUMBER_ZERO}	0

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands
	CLI:Test Available Commands	add	factory_settings	shell
	...	apply_settings	hostname	show
	...	cd	ls	show_settings
	...	change_password	pwd	shutdown
	...	commit	quit	software_upgrade
	...	delete	reboot	system_certificate
	...	event_system_audit	revert	system_config_check
	...	event_system_clear	save_settings	whoami
	...	exit	set

Test visible fields for show command
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/
	CLI:Test Show Command Regexp	command\\s+command status\\s+protocol\\s+protocol status
	...	certificate\\s+enabled\\s+none\\s+not applicable
	...	console\\s+enabled\\s+ssh\\s+enabled
	...	data logging\\s+disabled\\s+none\\s+not applicable
	...	event logging\\s+disabled\\s+ssh\\s+enabled
	...	power\\s+enabled\\s+ipmi\\s+enabled
	...	web\\s+enabled\\s+http/s\\s+enabled

Test ls command
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands
	CLI:Test Ls Command	certificate	console	data_logging	event_logging	power	web

Test certificate directory
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/certificate
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Show Command	command: cert	enabled = yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Show Command	command: certificate	enabled = yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Unavailable Fields	cycle_interval	event_log_frequecy	protocol	username
	...	enable_data_logging_alerts	event_log_unit	server	web_url	enable_event_logging_alerts	outlet	share	password	type_extension
	CLI:Test Set Field Options	enabled	no	yes
	[Teardown]	CLI:Revert	Raw

Test data_logging directory
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/data_logging/
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Show Command	command: datalog	enabled = no
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Show Command	command: data logging	enabled = no
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Unavailable Fields	cycle_interval	event_log_frequecy	protocol	username
	...	enable_data_logging_alerts	event_log_unit	server	web_url	enable_event_logging_alerts	outlet	share	password	type_extension
	CLI:Test Set Field Options	enabled	no	yes
	[Teardown]	CLI:Revert	Raw

Test event_logging directory
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/event_logging/
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Show Command	command: eventlog	enabled = no
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Show Command	command: event logging	enabled = no
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Unavailable Fields	cycle_interval	event_log_frequecy	protocol	username
	...	enable_data_logging_alerts	event_log_unit	server	web_url	enable_event_logging_alerts	outlet	share	password	type_extension
	CLI:Test Set Field Options	enabled	no	yes
	[Teardown]	CLI:Revert	Raw

Test power directory
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/power
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Show Command	command: power	enabled = yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Show Command	command: power	enabled = yes	protocol: ipmi
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Unavailable Fields	cycle_interval	event_log_frequecy	protocol	username
	...	enable_data_logging_alerts	event_log_unit	server	web_url	enable_event_logging_alerts	outlet	share	password	type_extension
	CLI:Test Set Field Options	enabled	no	yes
	[Teardown]	CLI:Revert	Raw

Test web directory
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/web
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Show Command	command: web	enabled = yes	web_url = http://%IP
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Show Command	command: web	enabled = yes	web_url = http://%IP	protocol: http/s
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Unavailable Fields	cycle_interval	event_log_frequecy	protocol	username
	...	enable_data_logging_alerts	event_log_unit	server	enable_event_logging_alerts	outlet	share	password	type_extension
	CLI:Test Set Field Options	enabled	no	yes
	[Teardown]	CLI:Revert	Raw

Test console directory
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/console
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Show Command	command: console	enabled = yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Show Command	command: console	enabled = yes	protocol: ssh
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Unavailable Fields	cycle_interval	event_log_frequecy	protocol	username
	...	enable_data_logging_alerts	event_log_unit	server	web_url	enable_event_logging_alerts	outlet	share	password	type_extension
	CLI:Test Set Field Options	enabled	no	yes
	[Teardown]	CLI:Revert	Raw

Test available fields to set
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands
	CLI:Add
	CLI:Test Set Available Fields	command	username	type_extension	type_extension	share	server	password	outlet	enabled	cycle_interval
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Unavailable Fields	protocol
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test possible values when setting fields
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands
	CLI:Add
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Options	command	custom_commands	kvm	outlet
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Options	command	kvm	outlet
	CLI:Test Set Field Options	enabled	no	yes
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test invalid values for fields
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands
	CLI:Add
	${COMMANDS}=	Create List	command	enabled
	FOR	${COMMAND}	IN	@{COMMANDS}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	a	Error: Invalid value: a for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value: ${COMMAND}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${COMMAND}	a	Error: Invalid value: a
	END
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=custom_commands
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/
	CLI:Add
	${OUTPUT}=	CLI:Show
	CLI:Test Show Command	command = custom_commands	enabled = yes	protocol: none	outlet	cycle_interval = 3
	...	type_extension	server	share	username	password = ********

	FOR	${i}	IN RANGE	1	11
		Should Contain	${OUTPUT}	custom_command_script${i}	custom_command_enabled${i} = no	custom_command_label${i} = customcommand${i}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	custom_command_enabled${i}	${EMPTY}	Error: Missing value for parameter: custom_command_enabled${i}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	custom_command_enabled${i}	a	Error: Invalid value: a for parameter: custom_command_enabled${i}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	custom_command_enabled${i}	${EMPTY}	Error: Missing value: custom_command_enabled${i}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	custom_command_enabled${i}	a	Error: Invalid value: a
		CLI:Test Set Field Options	custom_command_enabled${i}	yes	no
		CLI:Test Set Field Invalid Options	custom_command_label${i}	~!@	Error: custom_command_label${i}: Validation error.	yes
		CLI:Set	custom_command_label${i}=customcommand${i}
	END
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test custom commands directory
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/
	CLI:Add
	CLI:Set	command=custom_commands
	CLI:Commit
	CLI:Test Show Command Regexp	custom\\scommands\\s+enabled\\s+none\\s+not\\sapplicable
	CLI:Enter Path	custom_commands
	CLI:Test Show Command	command: custom commands	enabled = yes
	${OUTPUT}=	CLI:Show
	FOR	${i}	IN RANGE	1	11
		Should Contain	${OUTPUT}	custom_command_script${i}	custom_command_enabled${i} = no	custom_command_label${i} = customcommand${i}
	END
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Unavailable Fields	command	protocol
	CLI:Set	enabled=no
	CLI:Commit
	CLI:Enter Path	..
	CLI:Test Show Command Regexp	custom\\scommands\\s+disabled\\s+none\\s+not\\sapplicable
	CLI:Delete	custom_commands
	[Teardown]	CLI:Revert	Raw

Test kvm directory
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/
	CLI:Add
	CLI:Set	command=kvm
	CLI:Commit
	CLI:Test Show Command Regexp	kvm\\s+enabled\\s+none\\s+not\\sapplicable
	CLI:Enter Path	kvm
	CLI:Test Show Command	command: kvm	enabled = yes	type_extension
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Unavailable Fields	command	protocol
	CLI:Set	enabled=no
	CLI:Commit
	CLI:Enter Path	..
	CLI:Test Show Command Regexp	kvm\\s+disabled\\s+none\\s+not\\sapplicable
	CLI:Delete	kvm
	[Teardown]	CLI:Revert	Raw

Test outlet directory
	Skip If	'${NGVERSION}' >= '4.2'	In >=4.2 there is a validation error if user does not select an outlet
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/
	CLI:Add
	CLI:Set	command=outlet
	CLI:Commit
	CLI:Test Show Command Regexp	outlet\\s+disabled\\s+none\\s+not\\sapplicable
	CLI:Enter Path	outlet
	CLI:Test Show Command	command: outlet	enabled = no
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Unavailable Fields	command
	CLI:Enter Path	..
	CLI:Delete	outlet

Test with add function - kvm
	[Documentation]	Checks valid and invalid values on v5.6+. On v4.2 to v5.4 it's not working and won't be fixed as per comment from Davi on BUG_NG_9836
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands
	CLI:Add
	CLI:Test Set Validate Normal Fields	command	outlet	kvm
	CLI:Set	command=kvm
	CLI:Test Set Validate Normal Fields	enabled	no	yes

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	type_extension	${EMPTY}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	type_extension	${EMPTY}	Error: Missing value: type_extension

	Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	type_extension	${WORD}	Error: Invalid value: ${WORD} for parameter: type_extension
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	type_extension	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	type_extension	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: type_extension
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	type_extension	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	type_extension	${POINTS}	Error: Invalid value: ${POINTS} for parameter: type_extension
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	type_extension	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	type_extension	${NUMBER_ZERO}	Error: Invalid value: ${NUMBER_ZERO} for parameter: type_extension
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	type_extension	${NUMBER_ZERO}	Error: Invalid value: ${NUMBER_ZERO}

	Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	type_extension	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: type_extension
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	type_extension	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	type_extension	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: type_extension
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	type_extension	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	type_extension	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: type_extension
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	type_extension	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	type_extension	${TWO_NUMBER}	Error: Invalid value: ${TWO_NUMBER} for parameter: type_extension
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	type_extension	${TWO_NUMBER}	Error: Invalid value: ${TWO_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	type_extension	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: type_extension
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	type_extension	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}

	CLI:Test Set Validate Normal Fields	type_extension	cimc_ucs.py	idrac.py	ilo.py	imm.py	mergedpoint_ems.py	quanta.py	supermicro.py
	[Teardown]	CLI:Cancel	Raw

Test adding outlet command with no outlets
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/
	CLI:Add
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	command	outlet	Error: At least one outlet must be selected.	yes
	[Teardown]	CLI:Cancel	Raw

Test with add function - outlet
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands
	CLI:Add
	CLI:Test Set Validate Normal Fields	command	kvm	outlet
	CLI:Set	command=outlet
	CLI:Set	enabled=yes

	CLI:Test Set Validate Invalid Options	cycle_interval	${EMPTY}
	CLI:Test Set Validate Invalid Options	cycle_interval	${WORD}
	CLI:Test Set Validate Invalid Options	cycle_interval	${NUMBER}
	CLI:Test Set Validate Invalid Options	cycle_interval	${POINTS}

	CLI:Test Set Validate Invalid Options	cycle_interval	${ONE_WORD}
	CLI:Test Set Validate Invalid Options	cycle_interval	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	cycle_interval	${ONE_POINTS}

	CLI:Test Set Validate Invalid Options	cycle_interval	${TWO_WORD}
	CLI:Test Set Validate Invalid Options	cycle_interval	${TWO_NUMBER}
	CLI:Test Set Validate Invalid Options	cycle_interval	${TWO_POINTS}

	[Teardown]	CLI:Cancel	Raw

*** Keyword ***
CLI:Suite Setup
	CLI:Open
	CLI:Enter Path	/settings/devices
	@{DEVICELIST}=	CLI:Get Devices List	all	${PROTECTED_DEVICE_TYPES}
	CLI:Delete Devices	@{DEVICELIST}
	${OUTPUT}=	CLI:Test Show Command
	Run Keyword If	$DUMMY_DEVICE_CONSOLE_NAME in $OUTPUT	CLI:Delete Device	${DUMMY_DEVICE_CONSOLE_NAME}
	CLI:Add Device	${DUMMY_DEVICE_CONSOLE_NAME}	ilo
	${OUTPUT}=	CLI:Test Show Command
	Should Contain	${OUTPUT}	${DUMMY_DEVICE_CONSOLE_NAME}

Delete Device And Close Connection
	@{DEVICELIST}=	CLI:Get Devices List	all	${PROTECTED_DEVICE_TYPES}
	CLI:Delete Devices	@{DEVICELIST}
	CLI:Close Connection