*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Devices... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	SSH

Suite Setup	CLI:Suite Setup
Suite Teardown	Delete Device And Close Connection

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/devices
	${LIST}=	Create List	delete	reboot	edit	rename	add	event_system_audit	revert	event_system_clear	set	cd	exit	shell	change_password	hostname	show	clone	ls	show_settings	commit	pwd	shutdown	quit	whoami
	${ret}=	CLI:Is Serial Console
	Run Keyword If	'${ret}' == '1'	Append To List	${LIST}	bounce_dtr	default
	CLI:Test Available Commands	@{LIST}

Test visible fields for show command
	CLI:Enter Path	/settings/devices
	CLI:Test Show Command	name	connected through	type	access	monitoring

Test validating empty new record
	CLI:Enter Path	/settings/devices/
	CLI:Add
	${OUTPUT}=	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error
	${OUTPUT}=	CLI:Cancel
	Should Not Contain	${OUTPUT}	Error

Test validation for device name
	CLI:Enter Path	/settings/devices/
	CLI:Add
	Run Keyword If	'${NGVERSION}' >= '4.2' or '${HOST}' == '192.168.2.34'	CLI:Test Set Field Invalid Options	name	${POINTS}	Error: name: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2' and '${HOST}' != '192.168.2.34'	CLI:Test Set Field Invalid Options	name	${POINTS}	Error: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2' or '${HOST}' == '192.168.2.34'	CLI:Test Set Field Invalid Options	name	${EXCEEDED}	Error: name: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2' and '${HOST}' != '192.168.2.34'	CLI:Test Set Field Invalid Options	name	${EXCEEDED}	Error: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid name for rename command
	CLI:Enter Path	/settings/devices/
	CLI:Add Device	original_name	device_console
	CLI:Add Device	copy_name	device_console
	CLI:Enter Path	/settings/devices/
	${OUTPUT}=	CLI:Write	rename original_name
	Should Contain	${OUTPUT}	{devices}
	CLI:Test Show Command	current name: original_name	new_name
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	new_name	${POINTS}	Error: new_name: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	new_name	${POINTS}	Error: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	new_name	copy_name	Error: new_name: Failed to rename device. Check if name is unique.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	new_name	copy_name	Error: Failed to rename device. Check if name is unique.	yes
	[Teardown]	CLI:Cancel	Raw

Test valid name for rename command
	CLI:Enter Path	/settings/devices/
	${OUTPUT}=	CLI:Write	rename original_name
	Should Contain	${OUTPUT}	{devices}
	CLI:Test Show Command	current name: original_name	new_name
	CLI:Set Field	new_name	rename_name
	CLI:Commit
	CLI:Test Show Command	rename_name
	CLI:Delete Devices	rename_name	copy_name

*** Keyword ***
Delete Device And Close Connection
	@{DEVICELIST}=	CLI:Get Devices List	all	${PROTECTED_DEVICE_TYPES}
	CLI:Delete Devices	@{DEVICELIST}
	CLI:Close Connection

CLI:Suite Setup
	CLI:Open
	CLI:Enter Path	/settings/devices
	@{DEVICELIST}=	CLI:Get Devices List	all	${PROTECTED_DEVICE_TYPES}
	CLI:Delete Devices	@{DEVICELIST}
