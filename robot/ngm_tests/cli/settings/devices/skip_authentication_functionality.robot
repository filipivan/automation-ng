*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Devices... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	BUG_NG_9920	NON-CRITICAL
Default Tags	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE_1}	device1
${DEVICE_TYPE}	device_console
${IP_ALIAS}	${IP_ALIAS_IPV4}
${IP_ALIAS_PORT}	233
${DEVICE_IP}	${HOST}
${ANY_USER}	anything

*** Test Cases ***
Test field skip_authentication_to_access_device=no
	[Tags]	NEED-REVIEW
	[Setup]	Run Keywords	CLI:Delete Devices	${DEVICE_1}	AND	CLI:Add Device	${DEVICE_1}	${DEVICE_TYPE}	${DEVICE_IP}
	CLI:Enter Path	/settings/devices/
	CLI:Edit	${DEVICE_1}
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	skip_authentication_to_access_device = no
	CLI:Cancel

	CLI:Enter Path	/access/${DEVICE_1}
	Write	connect
	${CONNECTION}=	Read Until	cli ]
	@{FIELDS}=	Create List	Enter	Ec.	to	help	cli
	FOR	${FIELD}	IN	@{FIELDS}
		Should Contain	${CONNECTION}	${FIELD}
	END
	Write Bare	\n
	Read Until Regexp	((L)|(l))ogin:
	CLI:Disconnect Device


	Set Default Configuration	prompt=[
	Open Connection		${HOST}	alias=device-alias
	Login	${DEFAULT_USERNAME}:${DEVICE_1}	${DEFAULT_PASSWORD}
	${CONNECTION}=	Read Until	cli ]
	@{FIELDS}=	Create List	Enter	Ec.	to	help	cli
	FOR	${FIELD}	IN	@{FIELDS}
		Should Contain	${CONNECTION}	${FIELD}
	END
	Write Bare	\n
	Read Until Regexp	((L)|(l))ogin:
	Set Default Configuration	prompt=#
	CLI:Disconnect Device
	CLI:Close Current Connection
	[Teardown]	CLI:Switch Connection	default

Test field skip_authentication_to_access_device=yes (NONE authentication)
	[Tags]	NEED-REVIEW
	[Setup]	CLI:Delete Devices	${DEVICE_1}
	CLI:Add Device	${DEVICE_1}	${DEVICE_TYPE}	${DEVICE_IP}
	CLI:Enter Path	/settings/devices/
	CLI:Edit	${DEVICE_1}
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	skip_authentication_to_access_device = no
	CLI:Set	skip_authentication_to_access_device=yes
	CLI:Commit

	CLI:Enter Path	/access/${DEVICE_1}
	Write	connect
	${CONNECTION}=	Read Until	cli ]
	@{FIELDS}=	Create List	Enter	Ec.	to	help	cli
	FOR	${FIELD}	IN	@{FIELDS}
		Should Contain	${CONNECTION}	${FIELD}
	END
	Write Bare	\n
	Read Until Regexp	((L)|(l))ogin:
	CLI:Disconnect Device

	${CONNECTION}	SUITE:Connect To Device
	${CONTAINS}	Evaluate	"Are you sure you want to continue connecting" in """${CONNECTION}"""
	Run Keyword If	${CONTAINS}	Write	yes
	IF	${CONTAINS}
		${CONNECTION}	Read Until	cli ]
	END
	@{FIELDS}=	Create List	Enter	Ec.	to	help	cli
	FOR	${FIELD}	IN	@{FIELDS}
		Should Match Regexp	${CONNECTION}	(.)*${FIELD}(.)*
	END
	Write Bare	\n
	Read Until Regexp	((L)|(l))ogin:
	CLI:Disconnect Device
	CLI:Close Current Connection
	[Teardown]	CLI:Switch Connection	default

Test field skip_authentication_to_access_device field=yes with ip_alias
	[Tags]	NEED-REVIEW
	[Setup]	CLI:Delete Devices	${DEVICE_1}
	CLI:Add Device	${DEVICE_1}	${DEVICE_TYPE}	${DEVICE_IP}
	CLI:Enter Path	/settings/devices/
	CLI:Edit	${DEVICE_1}
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	skip_authentication_to_access_device = no
	CLI:Set	skip_authentication_to_access_device=yes
	CLI:Set	enable_ip_alias=yes
	CLI:Set	ip_alias=${IP_ALIAS}
	CLI:Set	interface=eth0
	CLI:Set	ip_alias_telnet=yes
	CLI:Set	ip_alias_telnet_port=${IP_ALIAS_PORT}
	CLI:Commit

	CLI:Enter Path	/access/${DEVICE_1}
	Write	connect
	${CONNECTION}	Read Until	cli ]
	@{FIELDS}=	Create List	Enter	Ec.	to	help	cli
	FOR	${FIELD}	IN	@{FIELDS}
		Should Contain	${CONNECTION}	${FIELD}
	END
	Write Bare	\n
	Read Until Regexp	((L)|(l))ogin:
	CLI:Disconnect Device

	${CONNECTION}	SUITE:Connect To Device
	${CONTAINS}	Evaluate	"Are you sure you want to continue connecting" in """${CONNECTION}"""
	Run Keyword If	${CONTAINS}	Write	yes
	IF	${CONTAINS}
		${CONNECTION}	Read Until	cli ]
	END
	@{FIELDS}=	Create List	Enter	Ec.	to	help	cli
	FOR	${FIELD}	IN	@{FIELDS}
		Should Match Regexp	${CONNECTION}	(.)*${FIELD}(.)*
	END
	Write Bare	\n
	Read Until Regexp	((L)|(l))ogin:
	CLI:Disconnect Device
	CLI:Close Current Connection
	[Teardown]	CLI:Switch Connection	default

*** Keyword ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/devices
	@{DEVICELIST}=	CLI:Get Devices List	all	${PROTECTED_DEVICE_TYPES}
	CLI:Delete Devices	@{DEVICELIST}

SUITE:Teardown
	CLI:Open
	@{DEVICELIST}=	CLI:Get Devices List	all	${PROTECTED_DEVICE_TYPES}
	CLI:Delete Devices	@{DEVICELIST}
	CLI:Close Connection

SUITE:Connect To Device
	Open Connection		${HOSTPEER}	alias=device-alias
	Login	${DEFAULT_USERNAME}	${FACTORY_PASSWORD}
	CLI:Connect As Root
	Set Default Configuration	prompt=[
	${OUTPUT}	SUITE:Open SSH Connection From Peer	${DEFAULT_USERNAME}:${DEVICE_1}	${HOST}
	[Return]	${OUTPUT}

SUITE:Open SSH Connection From Peer
	[Arguments]	${USERNAME}	${HOST_DIFF}	${IPV6}=No	${INTERFACE}=eth0
	Run Keyword If	'${IPV6}' == 'No'	Write	ssh ${USERNAME}@${HOST_DIFF}
	Run Keyword If	'${IPV6}' == 'Yes'	Write	ssh -6 ${USERNAME}@${HOST_DIFF}%${INTERFACE}
	${OUTPUT}=	Read Until Regexp	(cli ]|Are you sure you want to continue connecting \\(.*\\)\\?)
	Log To Console	\n${OUTPUT}
	Should Not Contain	${OUTPUT}	No route to host
	Should Not Contain	${OUTPUT}	Connection refused
	[Return]	${OUTPUT}