*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Devices... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE}	test-dev.ice
${EXPIRED_LICENSE}	${FIFTY_DEVICES_ACCESS_LICENSE_EXPIRED}
${DEVICE_NO_LICENSE}	test-dev.no-license

*** Test Cases ***
Test add device
	CLI:Enter Path	/settings/devices
	${OUTPUT}=	CLI:Test Show Command
	Run Keyword If	$DUMMY_DEVICE_CONSOLE_NAME in $OUTPUT	CLI:Delete Device	${DUMMY_DEVICE_CONSOLE_NAME}
	CLI:Add Device	${DUMMY_DEVICE_CONSOLE_NAME}	ilo
	${OUTPUT}=	CLI:Test Show Command
	Should Contain	${OUTPUT}	${DUMMY_DEVICE_CONSOLE_NAME}
	[Teardown]	CLI:Cancel	Raw

Test show_settings
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}
	Write	show_settings
	${OUTPUT}=	Read Until Prompt

	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access name=${DUMMY_DEVICE_CONSOLE_NAME}
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access type=ilo
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access web_url=http://%IP
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access launch_url_via_html5=yes
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access credential=set_now
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access password=\\*+
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access enable_device_state_detection_based_on_network_traffic=no
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access enable_hostname_detection=no
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access multisession=yes
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access read-write_multisession=no
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access enable_send_break=no
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access icon=terminal.png
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access mode=enabled
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access expiration=never
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access end_point=appliance
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access skip_authentication_to_access_device=no
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access escape_sequence=\\^Ec
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access power_control_key=\\^O
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access show_text_information=yes
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access enable_ip_alias=no
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access enable_second_ip_alias=no
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access allow_ssh_protocol=yes
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access allow_telnet_protocol=no
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access allow_binary_socket=no
	#Run Keyword If	'${NGVERSION}' >= '4.2'	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/management name=${DUMMY_DEVICE_CONSOLE_NAME}
	Run Keyword If	'${NGVERSION}' >= '4.2' and '${NGVERSION}' < '5.0'	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/management name=${DUMMY_DEVICE_CONSOLE_NAME}
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/management ssh_and_telnet=yes
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/management credential=use_specific
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/management password=\\*+
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/management ipmi=yes
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/management credential=use_specific
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/management password=\\*+
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/management monitoring_ipmi=no
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/management monitoring_nominal=no
	#Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/logging name=${DUMMY_DEVICE_CONSOLE_NAME}
	Run Keyword If  '${NGVERSION}' > '5.0'  Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/logging
	Run Keyword If  '${NGVERSION}' == '3.2'  Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/logging name=${DUMMY_DEVICE_CONSOLE_NAME}

	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/logging data_logging=no
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/logging event_logging=no
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/certificate command=certificate
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/certificate command=cert
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/certificate enabled=yes

	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/console command=console
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/console enabled=yes
	Run Keyword If	'${NGVERSION}' == '4.2'	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/console protocol=ssh
	Run Keyword If	'${NGVERSION}' > '5.0'	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/console launch_local_application=no

	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/data_logging command=\"{0,1}data( |_)logging\"{0,1}
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/data_logging command=datalog
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/data_logging enabled=no
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/event_logging command=eventlog
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/event_logging command=\"{0,1}event( |_)logging\"{0,1}

	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/event_logging enabled=no
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/power command=power
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/power enabled=yes
	Run Keyword If	'${NGVERSION}' =='4.2'	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/power protocol=ipmi

	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/web command=web
	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/web enabled=yes
	Run Keyword If	'${NGVERSION}' == '4.2'	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/web protocol=http/s

	Should Match Regexp	${OUTPUT}	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands/web web_url=http://%IP

Test if endpoint field was set
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Set Field	type	kvm_dsr
	${OUTPUT}=	CLI:Commit
	Should Not Contain	${OUTPUT}	Error

	CLI:Set Field	end_point	kvm_port
	${OUTPUT}=	CLI:Commit
	Should Not Contain	${OUTPUT}	Error

	${OUTPUT}=	CLI:Test Show Command	end_point
	Should Contain	${OUTPUT}	kvm_port
	[Teardown]	CLI:Cancel	Raw

Test controls of settings/devices/commands of kvm_dsr
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands
	CLI:Test Available Commands	add

Test add device with duplicate name
	CLI:Enter Path	/settings/devices/
	CLI:Add
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	name	${DUMMY_DEVICE_CONSOLE_NAME}	Error: name: Entry already exists.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	name	${DUMMY_DEVICE_CONSOLE_NAME}	Error: Entry already exists.	yes
	[Teardown]	CLI:Cancel	Raw

Test delete device
	CLI:Delete Device	${DUMMY_DEVICE_CONSOLE_NAME}

Test add device with exceeded licenses
	[Setup]	CLI:Delete All Devices
	CLI:Add Device	device_clone	device_console	127.0.0.1	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}
	CLI:Enter Path	/settings/devices/
	CLI:Write	clone device_clone
	CLI:Set	name=clone number_of_clones=${${NUM_LICENSES} - 1}
	CLI:Set	increment_ip_address_on_every_cloned_device=yes
	CLI:Commit

	CLI:Enter Path	/settings/devices/
	${OUTPUT}=	CLI:Add
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${OUTPUT}	Cannot add more devices. There are not enough licenses available or the system reached the maximum number of supported devices.
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	name	${DEVICE_NO_LICENSE}	Error: No license available.	yes
	[Teardown]	CLI:Cancel	Raw

Test add device with expired licenses
	[Setup]	CLI:Delete All Devices
	CLI:Add Device	device_clone	device_console	127.0.0.1	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}
	CLI:Add License Key	${EXPIRED_LICENSE}
	CLI:Enter Path	/settings/devices/
	CLI:Write	clone device_clone
	CLI:Set	name=clone number_of_clones=${${NUM_LICENSES} - 1}
	CLI:Set	increment_ip_address_on_every_cloned_device=yes
	CLI:Commit

	CLI:Enter Path	/settings/devices/
	${OUTPUT}=	CLI:Add
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${OUTPUT}	Cannot add more devices. There are not enough licenses available or the system reached the maximum number of supported devices.
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	name	${DEVICE_NO_LICENSE}	Error: No license available.	yes
	[Teardown]	CLI:Cancel	Raw

Test add ilo device
	[Setup]	CLI:Delete All Devices
	CLI:Add Device	${DUMMY_DEVICE_CONSOLE_NAME}	ilo
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands
	CLI:Test Available Commands	add
	[Teardown]	CLI:Cancel	Raw

Test add pdu_pm3000 device
	[Setup]	CLI:Delete All Devices
	CLI:Add Device	${DUMMY_DEVICE_CONSOLE_NAME}	pdu_pm3000
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Not Available Commands	add
	[Teardown]	CLI:Cancel	Raw

Test add ipmi_2.0 device
	[Setup]	CLI:Delete All Devices
	CLI:Add Device	${DUMMY_DEVICE_CONSOLE_NAME}	ipmi_2.0
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/commands
	CLI:Test Available Commands	add
	[Teardown]	CLI:Cancel	Raw

*** Keyword ***
SUITE:Setup
	CLI:Open
	CLI:Delete All Devices
	CLI:Delete All License Keys Installed
	${NUM_LICENSES}=	CLI:Get Number Of Available Access License Keys Installed
	Set Suite Variable	${NUM_LICENSES}

SUITE:Teardown
	CLI:Delete All Devices
	CLI:Close Connection