*** Settings ***
Resource	../../../init.robot
Documentation	Tests data logging on managed devices in Nodegrid
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW	EXCLUDEIN3_2	EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE_NAME}	test_data_logging
${DEVICE_TYPE}	device_console
${LOGPATH}	/var/local/DB/DLS_1

*** Test Cases ***
Test device logging console access
	[Tags]	BUG_NG_9920	NON-CRITICAL
	SUITE:Add Device With Data Logging
	CLI:Connect Device	${DEVICE_NAME}
	CLI:Disconnect Device
	SUITE:Get Data Logs	-- Console up --	-- Console down --
	[Teardown]	SUITE:Test Teardown

Test device logging commands executed in console
	SUITE:Add Device With Data Logging
	CLI:Enter Path	/access/${DEVICE_NAME}
	CLI:Write	connect
	CLI:Write Bare	\n
	CLI:Ls
	CLI:Disconnect Device
	Wait Until Keyword Succeeds	5x	5s	SUITE:Get Data Logs	access/	settings/	system/
	[Teardown]	SUITE:Test Teardown

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Connect As Root
	CLI:Switch Connection	default

SUITE:Teardown
	CLI:Switch Connection	default
	CLI:Close Connection

SUITE:Add Device With Data Logging
	CLI:Switch Connection	default
	CLI:Add Device	${DEVICE_NAME}	${DEVICE_TYPE}	${HOSTPEER}
	...	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}	on-demand
	CLI:Enter Path	/settings/devices
	CLI:Edit	${DEVICE_NAME}
	CLI:Set	data_logging=yes
	CLI:Commit

SUITE:Get Data Logs
	[Arguments]	@{EXPECTED}
	CLI:Switch Connection	root_session
	${DATA_LOG_FILE}	CLI:Write	cat ${LOGPATH}
	CLI:Should Contain All	${DATA_LOG_FILE}	${EXPECTED}

SUITE:Erase Data From Logs
	CLI:Switch Connection	root_session
	CLI:Write	echo -n > ${LOGPATH}

SUITE:Test Teardown
	CLI:Switch Connection	default
	Run Keyword And Ignore Error	CLI:Disconnect Device
	SUITE:Erase Data From Logs
	CLI:Switch Connection	default
	CLI:Cancel	Raw
	Set Client Configuration	timeout=90s
	CLI:Delete Device	${DEVICE_NAME}
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}