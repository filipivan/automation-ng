*** Settings ***
Resource	../../../init.robot
Documentation	Tests Settings > Managed Devices... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NUMBER_ZERO}	0
${RIGHT}	ab
${MAX_CHARACTERS}	ABCDEFGHIJKLMNOPQRSTUVWYXZ!@#$%&

*** Test Cases ***
Test with field=data_logging
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/logging

	CLI:Test Set Validate Normal Fields	data_logging	no	yes
	CLI:Test Set Validate Normal Fields	enable_data_logging_alerts	no	yes
	FOR	${INDEX}	IN RANGE	1	5
		CLI:Test Set Validate Invalid Options	data_string_${INDEX}	${EMPTY}
		CLI:Test Set Validate Invalid Options	data_string_${INDEX}	${WORD}
		CLI:Test Set Validate Invalid Options	data_string_${INDEX}	${NUMBER}
		CLI:Test Set Validate Invalid Options	data_string_${INDEX}	${POINTS}
		CLI:Test Set Validate Invalid Options	data_string_${INDEX}	${ONE_WORD}	Error: data_string_${INDEX}: The length must be between 3 and 32.
		CLI:Test Set Validate Invalid Options	data_string_${INDEX}	${ONE_NUMBER}	Error: data_string_${INDEX}: The length must be between 3 and 32.
		CLI:Test Set Validate Invalid Options	data_string_${INDEX}	${ONE_POINTS}	Error: data_string_${INDEX}: The length must be between 3 and 32.
		CLI:Test Set Validate Invalid Options	data_string_${INDEX}	${TWO_WORD}	Error: data_string_${INDEX}: The length must be between 3 and 32.
		CLI:Test Set Validate Invalid Options	data_string_${INDEX}	${TWO_NUMBER}	Error: data_string_${INDEX}: The length must be between 3 and 32.
		CLI:Test Set Validate Invalid Options	data_string_${INDEX}	${TWO_POINTS}	Error: data_string_${INDEX}: The length must be between 3 and 32.
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	data_string_${INDEX}	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument for command: set
		Run Keyword If	'${NGVERSION}' < '4.2'	CLI:Test Set Validate Invalid Options	data_string_${INDEX}	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument: set
		CLI:Test Set Validate Invalid Options	data_string_${INDEX}	${EXCEEDED}	Error: data_string_${INDEX}: The length must be between 3 and 32.
		CLI:Test Set Validate Normal Fields	data_string_${INDEX}	${MAX_CHARACTERS}
	END
	[Teardown]

Test with field=data_script_INDEX
	[Documentation]	Checks valid and invalid values on v5.6+. On v4.2 to v5.4 it's not working and won't be fixed as per comment from Davi on BUG_NG_9836
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/logging

	CLI:Write	set data_logging=yes
	CLI:Write	set enable_data_logging_alerts=yes

	FOR	${INDEX}	IN RANGE	1	5
	#	ERRORS IN COMMENT BECAUSE THEY ARE NOT SHOWING/TRIGGERED IN THE CLI
		Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	data_script_${INDEX}	${EMPTY}
	#	Error: Invalid value: ${EMPTY} for parameter: data_script_${INDEX}
		Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	data_script_${INDEX}	${EMPTY}	Error: Missing value: data_script_${INDEX}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	data_script_${INDEX}	${WORD}	Error: Invalid value: ${WORD} for parameter: data_script_${INDEX}
	#	Error: Invalid value: ${WORD} for parameter: data_script_${INDEX}
		Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	data_script_${INDEX}	${WORD}	Error: Invalid value: ${WORD}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	data_script_${INDEX}	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: data_script_${INDEX}
	#	Error: Invalid value: ${NUMBER} for parameter: data_script_${INDEX}
		Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	data_script_${INDEX}	${NUMBER}	Error: Invalid value: ${NUMBER}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	data_script_${INDEX}	${POINTS}	Error: Invalid value: ${POINTS} for parameter: data_script_${INDEX}
	#	Error: Invalid value: ${POINTS} for parameter: data_script_${INDEX}
		Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	data_script_${INDEX}	${POINTS}	Error: Invalid value: ${POINTS}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	data_script_${INDEX}	${NUMBER_ZERO}	Error: Invalid value: ${NUMBER_ZERO} for parameter: data_script_${INDEX}
	#	Error: Invalid value: ${NUMBER_ZERO} for parameter: data_script_${INDEX}
		Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	data_script_${INDEX}	${NUMBER_ZERO}	Error: Invalid value: ${NUMBER_ZERO}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	data_script_${INDEX}	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: data_script_${INDEX}
	#	Error: Invalid value: ${ONE_NUMBER} for parameter: data_script_${INDEX}
		Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	data_script_${INDEX}	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	data_script_${INDEX}	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: data_script_${INDEX}
	#	Error: Invalid value: ${ONE_WORD} for parameter: data_script_${INDEX}
		Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	data_script_${INDEX}	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	data_script_${INDEX}	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: data_script_${INDEX}
	#	Error: Invalid value: ${ONE_POINTS} for parameter: data_script_${INDEX}
		Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	data_script_${INDEX}	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	data_script_${INDEX}	${TWO_NUMBER}	Error: Invalid value: ${TWO_NUMBER} for parameter: data_script_${INDEX}
		
		Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	data_script_${INDEX}	${TWO_NUMBER}	Error: Invalid value: ${TWO_NUMBER}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	data_script_${INDEX}	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: data_script_${INDEX}
	#	Error: Invalid value: ${EXCEEDED} for parameter: data_script_${INDEX}
		Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	data_script_${INDEX}	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	END
	CLI:Test Set Validate Normal Fields	data_script_${INDEX}	PowerCycleDevice_sample.sh	ShutdownDevice_sample.sh

	[Teardown]	CLI:Revert	Raw

Test with field=event_logging & enable_event_logging_alerts
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/logging

	CLI:Test Set Validate Normal Fields	event_logging	no	yes
	CLI:Test Set Validate Normal Fields	enable_event_logging_alerts	no	yes
	[Teardown]

Test with event_string_INDEX
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/logging

	FOR	${INDEX}	IN RANGE	1	5
		CLI:Test Set Validate Invalid Options	event_string_${INDEX}	${EMPTY}
		CLI:Test Set Validate Invalid Options	event_string_${INDEX}	${WORD}
		CLI:Test Set Validate Invalid Options	event_string_${INDEX}	${NUMBER}
		CLI:Test Set Validate Invalid Options	event_string_${INDEX}	${POINTS}
		CLI:Test Set Validate Invalid Options	event_string_${INDEX}	${ONE_WORD}	Error: event_string_${INDEX}: The length must be between 3 and 32.
		CLI:Test Set Validate Invalid Options	event_string_${INDEX}	${ONE_NUMBER}	Error: event_string_${INDEX}: The length must be between 3 and 32.
		CLI:Test Set Validate Invalid Options	event_string_${INDEX}	${ONE_POINTS}	Error: event_string_${INDEX}: The length must be between 3 and 32.
		CLI:Test Set Validate Invalid Options	event_string_${INDEX}	${TWO_WORD}	Error: event_string_${INDEX}: The length must be between 3 and 32.
		CLI:Test Set Validate Invalid Options	event_string_${INDEX}	${TWO_NUMBER}	Error: event_string_${INDEX}: The length must be between 3 and 32.
		CLI:Test Set Validate Invalid Options	event_string_${INDEX}	${TWO_POINTS}	Error: event_string_${INDEX}: The length must be between 3 and 32.
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	event_string_${INDEX}	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument for command: set
		Run Keyword If	'${NGVERSION}' < '4.2'	CLI:Test Set Validate Invalid Options	event_string_${INDEX}	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument: set
		CLI:Test Set Validate Invalid Options	event_string_${INDEX}	${EXCEEDED}	Error: event_string_${INDEX}: The length must be between 3 and 32.
		CLI:Test Set Validate Normal Fields	event_string_${INDEX}	${MAX_CHARACTERS}
	END
	[Teardown]	CLI:Revert	Raw

Test with field=event_script_INDEX
	[Documentation]	Checks valid and invalid values on v5.6+. On v4.2 to v5.4 it's not working and won't be fixed as per comment from Davi on BUG_NG_9836
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/logging

	CLI:Write	set event_logging=yes
	CLI:Write	set enable_event_logging_alerts=yes

	FOR	${INDEX}	IN RANGE	1	5
	#	ERRORS IN COMMENT BECAUSE THEY ARE NOT SHOWING/TRIGGERED IN THE CLI
		Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	event_script_${INDEX}	${EMPTY}
	#	Error: Invalid value: ${EMPTY} for parameter: data_script_${INDEX}
		Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	event_script_${INDEX}	${EMPTY}	Error: Missing value: event_script_${INDEX}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	event_script_${INDEX}	${WORD}	Error: Invalid value: ${WORD} for parameter: event_script_${INDEX}
	#	Error: Invalid value: ${WORD} for parameter: data_script_${INDEX}
		Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	event_script_${INDEX}	${WORD}	Error: Invalid value: ${WORD}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	event_script_${INDEX}	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: event_script_${INDEX}
	#	Error: Invalid value: ${NUMBER} for parameter: data_script_${INDEX}
		Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	event_script_${INDEX}	${NUMBER}	Error: Invalid value: ${NUMBER}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	event_script_${INDEX}	${POINTS}	Error: Invalid value: ${POINTS} for parameter: event_script_${INDEX}
	#	Error: Invalid value: ${POINTS} for parameter: data_script_${INDEX}
		Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	event_script_${INDEX}	${POINTS}	Error: Invalid value: ${POINTS}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	event_script_${INDEX}	${NUMBER_ZERO}	Error: Invalid value: ${NUMBER_ZERO} for parameter: event_script_${INDEX}
	#	Error: Invalid value: ${NUMBER_ZERO} for parameter: data_script_${INDEX}
		Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	event_script_${INDEX}	${NUMBER_ZERO}		Error: Invalid value: ${NUMBER_ZERO}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	event_script_${INDEX}	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: event_script_${INDEX}
	#	Error: Invalid value: ${ONE_NUMBER} for parameter: data_script_${INDEX}
		Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	event_script_${INDEX}	${ONE_NUMBER}		Error: Invalid value: ${ONE_NUMBER}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	event_script_${INDEX}	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: event_script_${INDEX}
	#	Error: Invalid value: ${ONE_WORD} for parameter: data_script_${INDEX}
		Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	event_script_${INDEX}	${ONE_WORD}		Error: Invalid value: ${ONE_WORD}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	event_script_${INDEX}	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: event_script_${INDEX}
	#	Error: Invalid value: ${ONE_POINTS} for parameter: data_script_${INDEX}
		Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	event_script_${INDEX}	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	event_script_${INDEX}	${TWO_NUMBER}	Error: Invalid value: ${TWO_NUMBER} for parameter: event_script_${INDEX}
	#	Error: Invalid value: ${TWO_NUMBER} for parameter: data_script_${INDEX}
		Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	event_script_${INDEX}	${TWO_NUMBER}	Error: Invalid value: ${TWO_NUMBER}
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	event_script_${INDEX}	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: event_script_${INDEX}
	#	Error: Invalid value: ${EXCEEDED} for parameter: data_script_${INDEX}
		Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	event_script_${INDEX}	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
		CLI:Test Set Validate Normal Fields	event_script_${INDEX}	PowerCycleDevice_sample.sh	ShutdownDevice_sample.sh
	END
	[Teardown]	CLI:Revert	Raw

Test with field=event_log_frequency
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/logging

	CLI:Write	set event_logging=yes
	CLI:Write	set enable_event_logging_alerts=yes

	${LABEL}=	Run Keyword If	'${NGVERSION}' >= '4.2'	Set Variable	event_log_frequency
	...	ELSE	Set Variable	event_log_frequecy
	CLI:Test Set Validate Invalid Options	${LABEL}	${EMPTY}	Error: ${LABEL}: Validation error.
	CLI:Test Set Validate Invalid Options	${LABEL}	${WORD}	Error: ${LABEL}: Validation error.
	CLI:Test Set Validate Invalid Options	${LABEL}	${NUMBER}
	CLI:Test Set Validate Invalid Options	${LABEL}	${POINTS}	Error: ${LABEL}: Validation error.
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	${LABEL}	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument for command: set
	Run Keyword If	'${NGVERSION}' < '4.2'	CLI:Test Set Validate Invalid Options	${LABEL}	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument: set
	CLI:Test Set Validate Invalid Options	${LABEL}	${EXCEEDED}	Error: ${LABEL}: Exceeded the maximum size for this field.
	CLI:Test Set Validate Normal Fields	${LABEL}	24

	[Teardown]	CLI:Revert	Raw

Test with field=event_log_unit
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/logging

	CLI:Write	set event_logging=yes
	CLI:Write	set enable_event_logging_alerts=yes

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	event_log_unit	${EMPTY}	Error: Missing value for parameter: event_log_unit
	Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	event_log_unit	${EMPTY}	Error: Missing value: event_log_unit

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	event_log_unit	${WORD}	Error: Invalid value: ${WORD} for parameter: event_log_unit
	Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	event_log_unit	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	event_log_unit	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: event_log_unit
	Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	event_log_unit	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	event_log_unit	${POINTS}	Error: Invalid value: ${POINTS} for parameter: event_log_unit
	Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	event_log_unit	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	event_log_unit	${NUMBER_ZERO}	Error: Invalid value: ${NUMBER_ZERO} for parameter: event_log_unit
	Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	event_log_unit	${NUMBER_ZERO}	Error: Invalid value: ${NUMBER_ZERO}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	event_log_unit	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: event_log_unit
	Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	event_log_unit	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	event_log_unit	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: event_log_unit
	Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	event_log_unit	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	event_log_unit	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: event_log_unit
	Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	event_log_unit	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	event_log_unit	${TWO_NUMBER}	Error: Invalid value: ${TWO_NUMBER} for parameter: event_log_unit
	Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	event_log_unit	${TWO_NUMBER}	Error: Invalid value: ${TWO_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	event_log_unit	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: event_log_unit
	Run Keyword If	${NGVERSION} < 4.2	CLI:Test Set Validate Invalid Options	event_log_unit	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}

	CLI:Test Set Validate Normal Fields	event_log_unit	hours	minutes

	[Teardown]	CLI:Revert	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/devices
	@{DEVICELIST}=	CLI:Get Devices List	all	${PROTECTED_DEVICE_TYPES}
	CLI:Delete Devices	@{DEVICELIST}
	${OUTPUT}=	CLI:Test Show Command
	Run Keyword If	$DUMMY_DEVICE_CONSOLE_NAME in $OUTPUT	CLI:Delete Devices	${DUMMY_DEVICE_CONSOLE_NAME}
	CLI:Add Device	${DUMMY_DEVICE_CONSOLE_NAME}	ilo
	${OUTPUT}=	CLI:Test Show Command
	Should Contain	${OUTPUT}	${DUMMY_DEVICE_CONSOLE_NAME}

SUITE:Teardown
	@{DEVICELIST}=	CLI:Get Devices List	all	${PROTECTED_DEVICE_TYPES}
	CLI:Delete Devices	@{DEVICELIST}
	CLI:Close Connection