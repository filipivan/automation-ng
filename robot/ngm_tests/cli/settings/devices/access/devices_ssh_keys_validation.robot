*** Settings ***
Resource	../../../init.robot
Documentation	Tests device access through ssh keys validation
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2
Default Tags	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE_NAME}	test-dev.ice
${DEVICE_TYPE}	device_console
${DEVICE_IP}	127.0.0.1
@{ALL_VALUES}	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}

*** Test Cases ***
Test Available Commands After Sending Tab-Tab With SSH Disabled
	SUITE:Disable SSH Pre Shared Keys
	CLI:Enter Path	/
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/access
	CLI:Test Not Available Commands	ssh_keys

Test Available Commands After Sending Tab-Tab With SSH Enabled
	SUITE:Enable SSH Pre Shared Keys
	CLI:Test Available Commands	ssh_keys

Test Available Commands After Sending Tab-Tab
	SUITE:Enable SSH Pre Shared Keys
	CLI:Write	ssh_keys
	CLI:Test Available Commands	commit	ls	show
	...	generate_key_pair	return	set
	[Teardown]	SUITE:Return

Test Set Available Fields
	SUITE:Enable SSH Pre Shared Keys
	CLI:Write	ssh_keys
	CLI:Test Set Available Fields	ssh_key_type
	[Teardown]	SUITE:Return

Test Available Commands After Sending Tab-Tab After generate_key_pair Command
	SUITE:Enable SSH Pre Shared Keys
	CLI:Write	ssh_keys
	CLI:Write	generate_key_pair
	CLI:Test Available Commands	commit	ls	show
	...	generate_key_pair	return	set	send_public_key
	[Teardown]	SUITE:Return

Test Set Available Fields After generate_key_pair_command
	SUITE:Enable SSH Pre Shared Keys
	CLI:Write	ssh_keys
	CLI:Write	generate_key_pair
	CLI:Test Set Available Fields	ssh_key_type	ip_address	port	username
	...	password_use
	[Teardown]	SUITE:Return

Test Valid Values For Field=ssh_key_type
	SUITE:Enable SSH Pre Shared Keys
	CLI:Write	ssh_keys
	CLI:Test Set Field Options	ssh_key_type	ecdsa_384
	...	ed25519	rsa_2048	ecdsa_256	ecdsa_521	rsa_1024	rsa_4096
	[Teardown]	SUITE:Return

Test Invalid Values For Field=ssh_key_type
	SUITE:Enable SSH Pre Shared Keys
	CLI:Write	ssh_keys
	CLI:Test Set Field Invalid Options	ssh_key_type	${EMPTY}
	...	Error: Missing value for parameter: ssh_key_type

	FOR	${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	ssh_key_type	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: ssh_key_type
	END
	[Teardown]	SUITE:Return

Test Valid Values For Field=password_use
	SUITE:Enable SSH Pre Shared Keys
	CLI:Write	ssh_keys
	CLI:Test Set Field Options	password_use	use_same_as_access	use_specific
	[Teardown]	SUITE:Return

Test Invalid Values For Field=password_use
	SUITE:Enable SSH Pre Shared Keys
	CLI:Write	ssh_keys
	CLI:Test Set Field Invalid Options	password_use	${EMPTY}
	...	Error: Missing value for parameter: password_use

	FOR	${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	password_use	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: password_use
	END
	[Teardown]	SUITE:Return

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Delete Devices	${DEVICE_NAME}
	CLI:Add Device	${DEVICE_NAME}	${DEVICE_TYPE}	${DEVICE_IP}	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}
	SUITE:Enable SSH Pre Shared Keys

SUITE:Teardown
	CLI:Delete Devices	${DEVICE_NAME}
	CLI:Close Connection

SUITE:Enable SSH Pre Shared Keys
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/access
	CLI:Set	allow_pre-shared_ssh_key=yes
	CLI:Commit

SUITE:Disable SSH Pre Shared Keys
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/access
	CLI:Set	allow_pre-shared_ssh_key=no
	CLI:Commit

SUITE:Return
	CLI:Write	return	Raw