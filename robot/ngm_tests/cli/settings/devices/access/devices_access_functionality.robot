*** Settings ***
Resource	../../../init.robot
Documentation	Tests devices access fucntionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE}	test-peer-device
${SSH_PORT}	12345
${TELNET_PORT}	23456

*** Test Cases ***
Test Direct Access with Device Alias
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	BUG_NG_9920
	Run Keyword If	'${NGVERSION}' == '5.8'	Set Tags	NON-CRITICAL
	CLI:Switch Connection	default
	${SHOW_ALIAS}=	CLI:Write	show /settings/devices/${DEVICE}/access alias
	${MATCH}	${DEVICE_ALIAS}=	Should Match Regexp	${SHOW_ALIAS}	alias: (\\w+)
	CLI:Open Device	${DEVICE_ALIAS}

Test Direct Access with Device Name
	[Tags]	BUG_NG_9920
	Run Keyword If	'${NGVERSION}' == '5.8'	Set Tags	NON-CRITICAL
	CLI:Switch Connection	default
	CLI:Open Device	${DEVICE}

Test Direct Access with Telnet Port
	[Tags]	BUG_NG_9920
	Run Keyword If	'${NGVERSION}' == '5.8'	Set Tags	NON-CRITICAL
	CLI:Switch Connection	default
	CLI:Open Device	${TELNET_PORT}

Test Direct Access with SSH Port
	[Tags]	BUG_NG_9902
	Run Keyword If	'${NGVERSION}' == '5.8'	Set Tags	NON-CRITICAL
	CLI:Switch Connection	default
	CLI:Open Device	${SSH_PORT}

*** Keyword ***
SUITE:Setup
	CLI:Open
	CLI:Delete Devices	${DEVICE}
	CLI:Add Device	${DEVICE}	device_console	${HOSTPEER}	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}
	...	ADDITIONAL_FIELDS=allow_ssh_protocol=yes ssh_port=${SSH_PORT} allow_telnet_protocol=yes telnet_port=${TELNET_PORT}

SUITE:Teardown
	CLI:Switch Connection	default
	CLI:Delete Devices	${DEVICE}
	CLI:Close Connection