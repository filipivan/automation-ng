*** Settings ***
Resource	../../../init.robot
Documentation	Tests Settings > Devices > device > Access ... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${COORDINATES}	90,90
${EXPIRATION_DATE}	2018-01-11
${IP_ADDRESS}	1.1.1.1
${PORT}	2001
${ESCAPE_SEQUENCE_RIGHT}	^aa
${ESCAPE_SEQUENCE_WRONG_NUMBER}	^1a
${ESCAPE_SEQUENCE_WRONG_POINTS}	^@a
${POWER_CONTROL_KEY_RIGHT}	^a
${POWER_CONTROL_KEY_NUMBER}	^1
${POWER_CONTROL_KEY_POINTS}	^@
${VM_NAME}	test_vm-field

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access
	CLI:Test Available Commands	apply_settings	hostname	shell	cd	ls
	...	show	change_password	pwd	show_settings	commit	quit	shutdown
	...	event_system_audit	reboot	software_upgrade	event_system_clear
	...	revert	system_certificate	exit	save_settings	system_config_check
	...	factory_settings	set	whoami

Test visible fields for show command
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access
	CLI:Test Show Command	name: ${DUMMY_DEVICE_CONSOLE_NAME}
	CLI:Test Show Command	type = ilo	ip_address
	...	address_location	coordinates	web_url = http://%IP	launch_url_via_html5 = yes
	...	username	credential = set_now	password = ********
	...	enable_hostname_detection = no	multisession = yes	read-write_multisession = no	enable_send_break = no
	...	icon = terminal.png	mode = enabled	expiration = never	skip_authentication_to_access_device = no
	...	escape_sequence = ^Ec	power_control_key = ^O	show_text_information = yes	enable_ip_alias = no	enable_second_ip_alias = no
	...	allow_ssh_protocol = yes	ssh_port	allow_telnet_protocol = no	allow_binary_socket = no
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Show Command	enable_device_state_detection_based_on_network_traffic = no	end_point = appliance
	Run Keyword If	${NGVERSION} >= 5.0	CLI:Test Show Command	allow_pre-shared_ssh_key
	Run Keyword If	${NGVERSION} >= 5.0	CLI:Test Show Command Regexp	alias: DeviceAlias\\d+

Test available fields to set
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access
	CLI:Test Set Available Fields	address_location	allow_binary_socket	allow_ssh_protocol	allow_telnet_protocol
	...	coordinates	credential	enable_device_state_detection_based_on_network_traffic	enable_hostname_detection
	...	enable_ip_alias	enable_second_ip_alias	enable_send_break	end_point	escape_sequence	expiration
	...	icon	ip_address	launch_url_via_html5	mode	multisession	password	power_control_key
	...	read-write_multisession	show_text_information	skip_authentication_to_access_device	ssh_port
	...	type	username	web_url
	Run Keyword If	${NGVERSION} >= 5.0	CLI:Test Set Available Fields	allow_pre-shared_ssh_key

Test possible values when setting fields
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Test Set Field Options	type	pdu_servertech	cimc_ucs	drac	ipmi_2.0	pdu_baytech	console_server_acs
	...	idrac6	kvm_dsr	pdu_eaton	virtual_console_kvm	console_server_acs6000	kvm_mpu	pdu_enconnex	virtual_console_vmware
	...	console_server_digicp	ilom	kvm_raritan	pdu_mph2	console_server_lantronix	imm	netapp	pdu_pm3000	console_server_opengear
	...	infrabox	pdu_raritan	device_console	ipmi_1.5	pdu_apc	ilo
	#This needs to be moved in a seperate test case
	#CLI:Test Set Unavailable Fields	type	usb
	CLI:Test Set Field Options	launch_url_via_html5	yes	no
	CLI:Test Set Field Options	credential	set_now	ask_during_login
	CLI:Test Set Field Options	enable_hostname_detection	no	yes
	CLI:Test Set Field Options	multisession	no	yes
	CLI:Test Set Field Options	enable_send_break	no	yes
	CLI:Test Set Field Options	icon	apple_black.png	ibm_black.png	outlet.png	storage_blue.png	terminal.png
	...	windows_color.png	arista.png	juniper.png	pdu.png	storage_grey.png	ups.png	cisco_color.png	linux_black.png
	...	router_green.png	storage_grey_dark.png	usb.png	dell.png	linux_color.png	serial_console.png	supermicro.png
	...	vm.png	emc.png	netapp.png	server.png	switch.png	vmware.png	hp.png	oracle.png	server_grey.png	switch_purple.png	windows_black.png
	CLI:Test Set Field Options	mode	enabled	disabled	on-demand
	#discovered is not a possiable value error: Setting Mode to Discovered is reserved for the discovery process only.
	CLI:Test Set Field Options	expiration	never	date	days
	CLI:Test Set Field Options	skip_authentication_to_access_device	no	yes
	CLI:Test Set Field Options	show_text_information	yes	no
	CLI:Test Set Field Options	allow_ssh_protocol	yes	no
	CLI:Test Set Field Options	allow_telnet_protocol	no	yes
	CLI:Test Set Field Options	allow_binary_socket	no	yes
	[Teardown]	CLI:Revert

Test name field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Test Validate Unmodifiable Field	name	${DUMMY_DEVICE_CONSOLE_NAME}
	[Teardown]	CLI:Revert

Test type field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	type	ilo	imm	drac
	...	idrac6	ipmi_1.5	ipmi_2.0	ilom	cimc_ucs	netapp	device_console	infrabox
	...	virtual_console_vmware	virtual_console_kvm	console_server_acs	console_server_opengear
	...	console_server_acs6000	console_server_digicp	console_server_lantronix	pdu_apc	pdu_eaton
	...	pdu_mph2	pdu_pm3000	pdu_raritan	pdu_servertech	pdu_enconnex	pdu_baytech	kvm_dsr	kvm_mpu
	...	kvm_aten	kvm_raritan	pdu_cyberpower	console_server_nodegrid	pdu_geist
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	type	ilo	imm	drac
	...	idrac6	ipmi_1.5	ipmi_2.0	ilom	cimc_ucs	netapp	device_console	infrabox
	...	virtual_console_vmware	virtual_console_kvm	console_server_acs	console_server_opengear
	...	console_server_acs6000	console_server_digicp	console_server_lantronix	pdu_apc	pdu_eaton
	...	pdu_mph2	pdu_pm3000	pdu_raritan	pdu_servertech	pdu_enconnex	pdu_baytech	kvm_dsr	kvm_mpu
	...	kvm_aten	kvm_raritan

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	type	${EMPTY}	Error: Missing value for parameter: type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	type	${EMPTY}	Error: Missing value: type

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	type	${WORD}	Error: Invalid value: ${WORD} for parameter: type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	type	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	type	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	type	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	type	${POINTS}	Error: Invalid value: ${POINTS} for parameter: type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	type	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	type	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	type	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	type	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	type	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	type	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	type	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	type	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	type	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test ip_address field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Test Set Validate Invalid Options	ip_address	${EMPTY}
	CLI:Test Set Validate Invalid Options	ip_address	${WORD}
	CLI:Test Set Validate Invalid Options	ip_address	${NUMBER}
	CLI:Test Set Validate Invalid Options	ip_address	${POINTS}	Error: ip_address: Validation error.
	CLI:Test Set Validate Invalid Options	ip_address	${ONE_WORD}
	CLI:Test Set Validate Invalid Options	ip_address	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	ip_address	${ONE_POINTS}	Error: ip_address: Validation error.
	CLI:Test Set Validate Invalid Options	ip_address	${EXCEEDED}	Error: ip_address: Validation error.
	CLI:Test Set Validate Invalid Options	ip_address	${IP_ADDRESS}
	[Teardown]	CLI:Revert

Test address_location field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Test Set Validate Invalid Options	address_location	${EMPTY}
	CLI:Test Set Validate Invalid Options	address_location	${WORD}
	CLI:Test Set Validate Invalid Options	address_location	${NUMBER}
	CLI:Test Set Validate Invalid Options	address_location	${POINTS}
	CLI:Test Set Validate Invalid Options	address_location	${ONE_WORD}
	CLI:Test Set Validate Invalid Options	address_location	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	address_location	${ONE_POINTS}
	CLI:Test Set Validate Invalid Options	address_location	${EXCEEDED}
	[Teardown]	CLI:Revert

Test coordinates field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Test Set Validate Invalid Options	coordinates	${EMPTY}
	CLI:Test Set Validate Invalid Options	coordinates	${WORD}	Error: coordinates: Valid ranges: Latitude [-90.0,90.0], Longitude [-180.0,180.0].
	CLI:Test Set Validate Invalid Options	coordinates	${NUMBER}	Error: coordinates: Valid ranges: Latitude [-90.0,90.0], Longitude [-180.0,180.0].
	CLI:Test Set Validate Invalid Options	coordinates	${POINTS}	Error: coordinates: Valid ranges: Latitude [-90.0,90.0], Longitude [-180.0,180.0].
	CLI:Test Set Validate Invalid Options	coordinates	${ONE_WORD}	Error: coordinates: Valid ranges: Latitude [-90.0,90.0], Longitude [-180.0,180.0].
	CLI:Test Set Validate Invalid Options	coordinates	${ONE_NUMBER}	Error: coordinates: Valid ranges: Latitude [-90.0,90.0], Longitude [-180.0,180.0].
	CLI:Test Set Validate Invalid Options	coordinates	${ONE_POINTS}	Error: coordinates: Valid ranges: Latitude [-90.0,90.0], Longitude [-180.0,180.0].
	CLI:Test Set Validate Invalid Options	coordinates	${EXCEEDED}	Error: coordinates: Valid ranges: Latitude [-90.0,90.0], Longitude [-180.0,180.0].
	CLI:Test Set Validate Invalid Options	coordinates	${COORDINATES}
	[Teardown]	CLI:Revert

Test web_url field
	[Tags]	NON-CRITICAL	BUG_NG_9689
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Test Set Validate Invalid Options	web_url	${EMPTY}
	CLI:Test Set Validate Invalid Options	web_url	${WORD}
	CLI:Test Set Validate Invalid Options	web_url	${NUMBER}
	CLI:Test Set Validate Invalid Options	web_url	${POINTS}
	CLI:Test Set Validate Invalid Options	web_url	${ONE_WORD}
	CLI:Test Set Validate Invalid Options	web_url	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	web_url	${ONE_POINTS}
	CLI:Test Set Validate Invalid Options	web_url	${EXCEEDED}
	...	Error: spmanager.server.${DUMMY_DEVICE_CONSOLE_NAME}.url: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test launch_url_via_html5 field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	launch_url_via_html5	yes	no
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	launch_url_via_html5	yes	no

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	launch_url_via_html5	${EMPTY}	Error: Missing value for parameter: launch_url_via_html5
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	launch_url_via_html5	${EMPTY}	Error: Missing value: launch_url_via_html5

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	launch_url_via_html5	${WORD}	Error: Invalid value: ${WORD} for parameter: launch_url_via_html5
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	launch_url_via_html5	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	launch_url_via_html5	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: launch_url_via_html5
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	launch_url_via_html5	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	launch_url_via_html5	${POINTS}	Error: Invalid value: ${POINTS} for parameter: launch_url_via_html5
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	launch_url_via_html5	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	launch_url_via_html5	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: launch_url_via_html5
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	launch_url_via_html5	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	launch_url_via_html5	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: launch_url_via_html5
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	launch_url_via_html5	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	launch_url_via_html5	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: launch_url_via_html5
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	launch_url_via_html5	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	launch_url_via_html5	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: launch_url_via_html5
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	launch_url_via_html5	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test username field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Test Set Validate Invalid Options	username	${EMPTY}
	CLI:Test Set Validate Invalid Options	username	${WORD}
	CLI:Test Set Validate Invalid Options	username	${NUMBER}
	CLI:Test Set Validate Invalid Options	username	${POINTS}
	CLI:Test Set Validate Invalid Options	username	${ONE_WORD}
	CLI:Test Set Validate Invalid Options	username	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	username	${ONE_POINTS}
	CLI:Test Set Validate Invalid Options	username	${EXCEEDED}	Error: username: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test credential field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	credential	set_now	ask_during_login
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	credential	set_now	ask_during_login

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	credential	${EMPTY}	Error: Missing value for parameter: credential
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	credential	${EMPTY}	Error: Missing value: credential

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	credential	${WORD}	Error: Invalid value: ${WORD} for parameter: credential
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	credential	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	credential	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: credential
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	credential	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	credential	${POINTS}	Error: Invalid value: ${POINTS} for parameter: credential
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	credential	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	credential	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: credential
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	credential	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	credential	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: credential
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	credential	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	credential	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: credential
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	credential	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	credential	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: credential
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	credential	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test enable_device_state_detection_based_on_network_traffic field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	enable_device_state_detection_based_on_network_traffic	yes	no

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_device_state_detection_based_on_network_traffic	${EMPTY}	Error: Missing value for parameter: enable_device_state_detection_based_on_network_traffic

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_device_state_detection_based_on_network_traffic	${WORD}	Error: Invalid value: ${WORD} for parameter: enable_device_state_detection_based_on_network_traffic

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_device_state_detection_based_on_network_traffic	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: enable_device_state_detection_based_on_network_traffic

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_device_state_detection_based_on_network_traffic	${POINTS}	Error: Invalid value: ${POINTS} for parameter: enable_device_state_detection_based_on_network_traffic

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_device_state_detection_based_on_network_traffic	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: enable_device_state_detection_based_on_network_traffic

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_device_state_detection_based_on_network_traffic	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: enable_device_state_detection_based_on_network_traffic

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_device_state_detection_based_on_network_traffic	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: enable_device_state_detection_based_on_network_traffic

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_device_state_detection_based_on_network_traffic	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: enable_device_state_detection_based_on_network_traffic
	[Teardown]	CLI:Revert

Test enable_hostname_detection field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	enable_hostname_detection	yes	no
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	enable_hostname_detection	yes	no

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_hostname_detection	${EMPTY}	Error: Missing value for parameter: enable_hostname_detection
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_hostname_detection	${EMPTY}	Error: Missing value: enable_hostname_detection

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_hostname_detection	${WORD}	Error: Invalid value: ${WORD} for parameter: enable_hostname_detection
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_hostname_detection	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_hostname_detection	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: enable_hostname_detection
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_hostname_detection	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_hostname_detection	${POINTS}	Error: Invalid value: ${POINTS} for parameter: enable_hostname_detection
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_hostname_detection	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_hostname_detection	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: enable_hostname_detection
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_hostname_detection	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_hostname_detection	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: enable_hostname_detection
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_hostname_detection	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_hostname_detection	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: enable_hostname_detection
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_hostname_detection	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_hostname_detection	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: enable_hostname_detection
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_hostname_detection	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test multisession field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	multisession	yes	no
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	multisession	yes	no

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	multisession	${EMPTY}	Error: Missing value for parameter: multisession
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	multisession	${EMPTY}	Error: Missing value: multisession

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	multisession	${WORD}	Error: Invalid value: ${WORD} for parameter: multisession
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	multisession	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	multisession	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: multisession
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	multisession	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	multisession	${POINTS}	Error: Invalid value: ${POINTS} for parameter: multisession
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	multisession	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	multisession	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: multisession
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	multisession	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	multisession	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: multisession
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	multisession	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	multisession	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: multisession
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	multisession	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	multisession	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: multisession
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	multisession	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test read-write_multisession field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	read-write_multisession	yes	no
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	read-write_multisession	yes	no

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	read-write_multisession	${EMPTY}	Error: Missing value for parameter: read-write_multisession
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	read-write_multisession	${EMPTY}	Error: Missing value: read-write_multisession

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	read-write_multisession	${WORD}	Error: Invalid value: ${WORD} for parameter: read-write_multisession
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	read-write_multisession	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	read-write_multisession	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: read-write_multisession
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	read-write_multisession	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	read-write_multisession	${POINTS}	Error: Invalid value: ${POINTS} for parameter: read-write_multisession
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	read-write_multisession	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	read-write_multisession	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: read-write_multisession
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	read-write_multisession	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	read-write_multisession	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: read-write_multisession
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	read-write_multisession	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	read-write_multisession	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: read-write_multisession
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	read-write_multisession	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	read-write_multisession	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: read-write_multisession
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	read-write_multisession	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test enable_send_break field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	enable_send_break	yes	no
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	enable_send_break	yes	no

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_send_break	${EMPTY}	Error: Missing value for parameter: enable_send_break
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_send_break	${EMPTY}	Error: Missing value: enable_send_break

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_send_break	${WORD}	Error: Invalid value: ${WORD} for parameter: enable_send_break
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_send_break	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_send_break	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: enable_send_break
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_send_break	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_send_break	${POINTS}	Error: Invalid value: ${POINTS} for parameter: enable_send_break
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_send_break	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_send_break	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: enable_send_break
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_send_break	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_send_break	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: enable_send_break
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_send_break	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_send_break	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: enable_send_break
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_send_break	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_send_break	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: enable_send_break
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_send_break	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test break_sequence field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Write	set enable_send_break=yes
	CLI:Test Set Validate Invalid Options	break_sequence	${EMPTY}	Error: break_sequence: This field must have at least two characteres.
	CLI:Test Set Validate Invalid Options	break_sequence	${WORD}
	CLI:Test Set Validate Invalid Options	break_sequence	${NUMBER}
	CLI:Test Set Validate Invalid Options	break_sequence	${POINTS}
	CLI:Test Set Validate Invalid Options	break_sequence	${ONE_WORD}	Error: break_sequence: This field must have at least two characteres.
	CLI:Test Set Validate Invalid Options	break_sequence	${ONE_NUMBER}	Error: break_sequence: This field must have at least two characteres.
	CLI:Test Set Validate Invalid Options	break_sequence	${ONE_POINTS}	Error: break_sequence: This field must have at least two characteres.
	CLI:Test Set Validate Invalid Options	break_sequence	${EXCEEDED}	Error: break_sequence: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test icon field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Test Set Validate Normal Fields	icon	apple_black.png	arista.png	cisco_color.png
	...	dell.png	emc.png	hp.png	ibm_black.png	juniper.png	linux_black.png	linux_color.png
	...	netapp.png	oracle.png	outlet.png	pdu.png	router_green.png	serial_console.png	server.png	server_grey.png
	...	storage_blue.png	storage_grey.png	storage_grey_dark.png	supermicro.png	switch.png	switch_purple.png
	...	terminal.png	ups.png	usb.png	vm.png	vmware.png	windows_black.png	windows_color.png
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	icon	fortinet.png	kvm.png
	Run Keyword If	'${NGVERSION}' >= '4.2' and '${NGVERSION}' <= '5.0'	CLI:Test Set Validate Normal Fields	icon	temphum.png
	Run Keyword If	'${NGVERSION}' >= '5.2'	CLI:Test Set Validate Normal Fields	icon	temperature-humidity.png

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	icon	${EMPTY}	Error: Missing value for parameter: icon
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	icon	${EMPTY}	Error: Missing value: icon

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	icon	${WORD}	Error: Invalid value: ${WORD} for parameter: icon
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	icon	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	icon	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: icon
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	icon	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	icon	${POINTS}	Error: Invalid value: ${POINTS} for parameter: icon
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	icon	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	icon	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: icon
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	icon	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	icon	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: icon
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	icon	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	icon	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: icon
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	icon	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	icon	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: icon
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	icon	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test mode field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	mode	disabled	enabled	on-demand
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	mode	disabled	enabled	on-demand

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	mode	discovered	Error: mode: Setting Mode to Discovered is reserved for the discovery process only.
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	mode	discovered	Error: mode: Setting Mode to Discovered is reserved for the discovery process only.

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	mode	${EMPTY}	Error: Missing value for parameter: mode
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	mode	${EMPTY}	Error: Missing value: mode

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	mode	${WORD}	Error: Invalid value: ${WORD} for parameter: mode
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	mode	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	mode	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: mode
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	mode	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	mode	${POINTS}	Error: Invalid value: ${POINTS} for parameter: mode
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	mode	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	mode	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: mode
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	mode	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	mode	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: mode
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	mode	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	mode	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: mode
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	mode	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	mode	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: mode
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	mode	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}

	[Teardown]	CLI:Revert

Test expiration field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	expiration	date	days	never
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	expiration	date	days	never

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	expiration	${EMPTY}	Error: Missing value for parameter: expiration
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	expiration	${EMPTY}	Error: Missing value: expiration

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	expiration	${WORD}	Error: Invalid value: ${WORD} for parameter: expiration
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	expiration	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	expiration	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: expiration
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	expiration	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	expiration	${POINTS}	Error: Invalid value: ${POINTS} for parameter: expiration
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	expiration	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	expiration	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: expiration
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	expiration	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	expiration	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: expiration
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	expiration	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	expiration	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: expiration
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	expiration	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	expiration	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: expiration
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	expiration	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test expiration_date field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Write	set expiration=date
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	expiration_date	${EMPTY}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	expiration_date	${EMPTY}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	expiration_date	${WORD}	Error: expiration_date: Valid Date format: YYYY-MM-DD
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	expiration_date	${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	expiration_date	${NUMBER}	Error: expiration_date: Valid Date format: YYYY-MM-DD
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	expiration_date	${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	expiration_date	${POINTS}	Error: expiration_date: Valid Date format: YYYY-MM-DD
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	expiration_date	${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	expiration_date	${ONE_WORD}	Error: expiration_date: Valid Date format: YYYY-MM-DD
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	expiration_date	${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	expiration_date	${ONE_NUMBER}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	expiration_date	${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	expiration_date	${ONE_POINTS}	Error: expiration_date: Valid Date format: YYYY-MM-DD
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	expiration_date	${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	expiration_date	${EXCEEDED}	Error: expiration_date: Valid Date format: YYYY-MM-DD
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	expiration_date	${EXCEEDED}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	expiration_date	${EXPIRATION_DATE}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	expiration_date	${EXPIRATION_DATE}
	[Teardown]	CLI:Revert

Test duration field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Write	set expiration=days
	CLI:Test Set Validate Invalid Options	duration	${EMPTY}
	CLI:Test Set Validate Invalid Options	duration	${WORD}	Error: duration: Validation error.
	CLI:Test Set Validate Invalid Options	duration	${NUMBER}
	CLI:Test Set Validate Invalid Options	duration	${POINTS}	Error: duration: Validation error.
	CLI:Test Set Validate Invalid Options	duration	${ONE_WORD}	Error: duration: Validation error.
	CLI:Test Set Validate Invalid Options	duration	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	duration	${ONE_POINTS}	Error: duration: Validation error.
	CLI:Test Set Validate Invalid Options	duration	${EXCEEDED}	Error: duration: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test skip_authentication_to_access_device field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	skip_authentication_to_access_device	yes	no
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	skip_authentication_to_access_device	yes	no

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	skip_authentication_to_access_device	${EMPTY}	Error: Missing value for parameter: skip_authentication_to_access_device
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	skip_authentication_to_access_device	${EMPTY}	Error: Missing value: skip_authentication_to_access_device

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	skip_authentication_to_access_device	${WORD}	Error: Invalid value: ${WORD} for parameter: skip_authentication_to_access_device
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	skip_authentication_to_access_device	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	skip_authentication_to_access_device	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: skip_authentication_to_access_device
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	skip_authentication_to_access_device	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	skip_authentication_to_access_device	${POINTS}	Error: Invalid value: ${POINTS} for parameter: skip_authentication_to_access_device
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	skip_authentication_to_access_device	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	skip_authentication_to_access_device	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: skip_authentication_to_access_device
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	skip_authentication_to_access_device	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	skip_authentication_to_access_device	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: skip_authentication_to_access_device
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	skip_authentication_to_access_device	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	skip_authentication_to_access_device	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: skip_authentication_to_access_device
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	skip_authentication_to_access_device	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	skip_authentication_to_access_device	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: skip_authentication_to_access_device
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	skip_authentication_to_access_device	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test escape_sequence field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Test Set Validate Invalid Options	escape_sequence	${EMPTY}	Error: escape_sequence: This field must have at least two characters and it should start with ^ (control).
	CLI:Test Set Validate Invalid Options	escape_sequence	${WORD}	Error: escape_sequence: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	escape_sequence	${NUMBER}	Error: escape_sequence: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	escape_sequence	${POINTS}	Error: escape_sequence: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	escape_sequence	${ONE_WORD}	Error: escape_sequence: This field must have at least two characters and it should start with ^ (control).
	CLI:Test Set Validate Invalid Options	escape_sequence	${ONE_NUMBER}	Error: escape_sequence: This field must have at least two characters and it should start with ^ (control).
	CLI:Test Set Validate Invalid Options	escape_sequence	${ONE_POINTS}	Error: escape_sequence: This field must have at least two characters and it should start with ^ (control).
	CLI:Test Set Validate Invalid Options	escape_sequence	${EXCEEDED}	Error: escape_sequence: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	escape_sequence	${ESCAPE_SEQUENCE_WRONG_POINTS}	Error: escape_sequence: This field must have at least two characters and it should start with ^ (control).
	CLI:Test Set Validate Invalid Options	escape_sequence	${ESCAPE_SEQUENCE_WRONG_NUMBER}	Error: escape_sequence: This field must have at least two characters and it should start with ^ (control).
	CLI:Test Set Validate Invalid Options	escape_sequence	${ESCAPE_SEQUENCE_RIGHT}
	CLI:Test Set Validate Invalid Options	escape_sequence	~!	Error: escape_sequence: This field must have at least two characters and it should start with ^ (control).
	[Teardown]	CLI:Revert

Test power_control_key field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Test Set Validate Invalid Options	power_control_key	${EMPTY}	Error: power_control_key: This field must have two characters and it should start with ^ (control).
	CLI:Test Set Validate Invalid Options	power_control_key	${WORD}	Error: power_control_key: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	power_control_key	${NUMBER}	Error: power_control_key: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	power_control_key	${POINTS}	Error: power_control_key: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	power_control_key	${ONE_WORD}	Error: power_control_key: This field must have two characters and it should start with ^ (control).
	CLI:Test Set Validate Invalid Options	power_control_key	${ONE_NUMBER}	Error: power_control_key: This field must have two characters and it should start with ^ (control).
	CLI:Test Set Validate Invalid Options	power_control_key	${ONE_POINTS}	Error: power_control_key: This field must have two characters and it should start with ^ (control).
	CLI:Test Set Validate Invalid Options	power_control_key	${EXCEEDED}	Error: power_control_key: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	power_control_key	${POWER_CONTROL_KEY_POINTS}	Error: power_control_key: This field must have two characters and it should start with ^ (control).
	CLI:Test Set Validate Invalid Options	power_control_key	${POWER_CONTROL_KEY_NUMBER}	Error: power_control_key: This field must have two characters and it should start with ^ (control).
	CLI:Test Set Validate Invalid Options	power_control_key	${POWER_CONTROL_KEY_RIGHT}
	CLI:Test Set Validate Invalid Options	power_control_key	~!	Error: power_control_key: This field must have two characters and it should start with ^ (control).

	[Teardown]	CLI:Revert

Test show_text_information field/
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	show_text_information	yes	no
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	show_text_information	yes	no

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	show_text_information	${EMPTY}	Error: Missing value for parameter: show_text_information
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	show_text_information	${EMPTY}	Error: Missing value: show_text_information

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	show_text_information	${WORD}	Error: Invalid value: ${WORD} for parameter: show_text_information
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	show_text_information	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	show_text_information	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: show_text_information
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	show_text_information	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	show_text_information	${POINTS}	Error: Invalid value: ${POINTS} for parameter: show_text_information
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	show_text_information	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	show_text_information	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: show_text_information
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	show_text_information	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	show_text_information	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: show_text_information
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	show_text_information	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	show_text_information	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: show_text_information
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	show_text_information	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	show_text_information	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: show_text_information
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	show_text_information	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test enable_ip_alias field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_ip_alias	${EMPTY}	Error: Missing value for parameter: enable_ip_alias
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_ip_alias	${EMPTY}	Error: Missing value: enable_ip_alias

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_ip_alias	${WORD}	Error: Invalid value: ${WORD} for parameter: enable_ip_alias
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_ip_alias	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_ip_alias	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: enable_ip_alias
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_ip_alias	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_ip_alias	${POINTS}	Error: Invalid value: ${POINTS} for parameter: enable_ip_alias
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_ip_alias	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_ip_alias	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: enable_ip_alias
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_ip_alias	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_ip_alias	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: enable_ip_alias
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_ip_alias	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_ip_alias	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: enable_ip_alias
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_ip_alias	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_ip_alias	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: enable_ip_alias
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_ip_alias	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test ip_alias field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	Run Keyword If	${NGVERSION} >= 5.0	CLI:Test Set Validate Invalid Options Multiple Fields	enable_ip_alias=yes ip_alias=${EMPTY}	Error: ip_alias: Field must not be empty.
	Run Keyword If	${NGVERSION} < 5.0	CLI:Test Set Validate Invalid Options Multiple Fields	enable_ip_alias=yes ip_alias=${EMPTY}
	CLI:Test Set Validate Invalid Options Multiple Fields	enable_ip_alias=yes ip_alias=${WORD}	Error: ip_alias: Validation error.
	CLI:Test Set Validate Invalid Options Multiple Fields	enable_ip_alias=yes ip_alias=${NUMBER}	Error: ip_alias: Validation error.
	CLI:Test Set Validate Invalid Options Multiple Fields	enable_ip_alias=yes ip_alias=${POINTS}	Error: ip_alias: Validation error.
	CLI:Test Set Validate Invalid Options Multiple Fields	enable_ip_alias=yes ip_alias=${ONE_WORD}	Error: ip_alias: Validation error.
	CLI:Test Set Validate Invalid Options Multiple Fields	enable_ip_alias=yes ip_alias=${ONE_NUMBER}	Error: ip_alias: Validation error.
	CLI:Test Set Validate Invalid Options Multiple Fields	enable_ip_alias=yes ip_alias=${ONE_POINTS}	Error: ip_alias: Validation error.
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options Multiple Fields	enable_ip_alias=yes ip_alias=${EXCEEDED}	Error: ip_alias: Validation error.
	CLI:Test Set Validate Invalid Options Multiple Fields	enable_ip_alias=yes ip_alias=${IP_ADDRESS}
	[Teardown]	CLI:Revert

Test interface field
	Skip If	${NGVERSION} == 4.2	Bug NG-3921 is only fixed in 5.0+
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Write	set enable_ip_alias=yes ip_alias=${IP_ADDRESS}
	${HAS_ETH1}=	CLI:Has ETH1
	Run Keyword If	'${HAS_ETH1}' == '${TRUE}'	CLI:Test Set Validate Normal Fields	interface	eth0	eth1	loopback
	...	ELSE	CLI:Test Set Validate Normal Fields	interface	eth0	loopback

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	interface	${EMPTY}	Error: Missing value for parameter: interface
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	interface	${EMPTY}	Error: Missing value: interface

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	interface	${WORD}	Error: Invalid value: ${WORD} for parameter: interface
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	interface	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	interface	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: interface
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	interface	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	interface	${POINTS}	Error: Invalid value: ${POINTS} for parameter: interface
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	interface	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	interface	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: interface
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	interface	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	interface	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: interface
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	interface	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	interface	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: interface
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	interface	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	interface	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: interface
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	interface	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test ip_alias_telnet field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Write	set enable_ip_alias=yes ip_alias=${IP_ADDRESS}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	ip_alias_telnet	yes	no
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	ip_alias_telnet	yes	no

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ip_alias_telnet	${EMPTY}	Error: Missing value for parameter: ip_alias_telnet
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ip_alias_telnet	${EMPTY}	Error: Missing value: ip_alias_telnet

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ip_alias_telnet	${WORD}	Error: Invalid value: ${WORD} for parameter: ip_alias_telnet
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ip_alias_telnet	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ip_alias_telnet	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: ip_alias_telnet
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ip_alias_telnet	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ip_alias_telnet	${POINTS}	Error: Invalid value: ${POINTS} for parameter: ip_alias_telnet
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ip_alias_telnet	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ip_alias_telnet	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: ip_alias_telnet
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ip_alias_telnet	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ip_alias_telnet	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: ip_alias_telnet
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ip_alias_telnet	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ip_alias_telnet	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: ip_alias_telnet
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ip_alias_telnet	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ip_alias_telnet	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: ip_alias_telnet
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ip_alias_telnet	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test ip_alias_telnet_port field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Write	set enable_ip_alias=yes ip_alias=${IP_ADDRESS}
	CLI:Write	set ip_alias_telnet=yes
	CLI:Test Set Validate Invalid Options	ip_alias_telnet_port	${EMPTY}
	CLI:Test Set Validate Invalid Options	ip_alias_telnet_port	${WORD}	Error: ip_alias_telnet_port: Validation error.
	CLI:Test Set Validate Invalid Options	ip_alias_telnet_port	${NUMBER}
	CLI:Test Set Validate Invalid Options	ip_alias_telnet_port	${POINTS}	Error: ip_alias_telnet_port: Validation error.
	CLI:Test Set Validate Invalid Options	ip_alias_telnet_port	${ONE_WORD}	Error: ip_alias_telnet_port: Validation error.
	CLI:Test Set Validate Invalid Options	ip_alias_telnet_port	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	ip_alias_telnet_port	${ONE_POINTS}	Error: ip_alias_telnet_port: Validation error.
	CLI:Test Set Validate Invalid Options	ip_alias_telnet_port	${EXCEEDED}	Error: ip_alias_telnet_port: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test ip_alias_binary field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Write	set enable_ip_alias=yes ip_alias=${IP_ADDRESS}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	ip_alias_binary	yes	no
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	ip_alias_binary	yes	no

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ip_alias_binary	${EMPTY}	Error: Missing value for parameter: ip_alias_binary
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ip_alias_binary	${EMPTY}	Error: Missing value: ip_alias_binary

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ip_alias_binary	${WORD}	Error: Invalid value: ${WORD} for parameter: ip_alias_binary
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ip_alias_binary	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ip_alias_binary	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: ip_alias_binary
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ip_alias_binary	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ip_alias_binary	${POINTS}	Error: Invalid value: ${POINTS} for parameter: ip_alias_binary
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ip_alias_binary	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ip_alias_binary	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: ip_alias_binary
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ip_alias_binary	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ip_alias_binary	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: ip_alias_binary
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ip_alias_binary	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ip_alias_binary	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: ip_alias_binary
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ip_alias_binary	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ip_alias_binary	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: ip_alias_binary
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ip_alias_binary	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test ip_alias_binary_port field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Write	set enable_ip_alias=yes ip_alias=${IP_ADDRESS}
	CLI:Write	set ip_alias_binary=yes
	CLI:Test Set Validate Invalid Options	ip_alias_binary_port	${EMPTY}
	CLI:Test Set Validate Invalid Options	ip_alias_binary_port	${WORD}	Error: ip_alias_binary_port: Validation error.
	CLI:Test Set Validate Invalid Options	ip_alias_binary_port	${NUMBER}
	CLI:Test Set Validate Invalid Options	ip_alias_binary_port	${POINTS}	Error: ip_alias_binary_port: Validation error.
	CLI:Test Set Validate Invalid Options	ip_alias_binary_port	${ONE_WORD}	Error: ip_alias_binary_port: Validation error.
	CLI:Test Set Validate Invalid Options	ip_alias_binary_port	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	ip_alias_binary_port	${ONE_POINTS}	Error: ip_alias_binary_port: Validation error.
	CLI:Test Set Validate Invalid Options	ip_alias_binary_port	${EXCEEDED}	Error: ip_alias_binary_port: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test enable_second_ip_alias field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Write	set enable_ip_alias=yes ip_alias=${IP_ADDRESS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_second_ip_alias	${EMPTY}	Error: Missing value for parameter: enable_second_ip_alias
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_second_ip_alias	${EMPTY}	Error: Missing value: enable_second_ip_alias

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_second_ip_alias	${WORD}	Error: Invalid value: ${WORD} for parameter: enable_second_ip_alias
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_second_ip_alias	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_second_ip_alias	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: enable_second_ip_alias
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_second_ip_alias	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_second_ip_alias	${POINTS}	Error: Invalid value: ${POINTS} for parameter: enable_second_ip_alias
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_second_ip_alias	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_second_ip_alias	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: enable_second_ip_alias
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_second_ip_alias	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_second_ip_alias	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: enable_second_ip_alias
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_second_ip_alias	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_second_ip_alias	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: enable_second_ip_alias
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_second_ip_alias	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	enable_second_ip_alias	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: enable_second_ip_alias
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	enable_second_ip_alias	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test sec_ip_alias field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	Run Keyword If	${NGVERSION} >= 5.0	CLI:Test Set Validate Invalid Options Multiple Fields	enable_second_ip_alias=yes sec_ip_alias=${EMPTY}	Error: sec_ip_alias: Field must not be empty.
	Run Keyword If	${NGVERSION} < 5.0	CLI:Test Set Validate Invalid Options Multiple Fields	enable_second_ip_alias=yes sec_ip_alias=${EMPTY}
	CLI:Test Set Validate Invalid Options Multiple Fields	enable_second_ip_alias=yes sec_ip_alias=${WORD}	Error: sec_ip_alias: Validation error.
	CLI:Test Set Validate Invalid Options Multiple Fields	enable_second_ip_alias=yes sec_ip_alias=${NUMBER}	Error: sec_ip_alias: Validation error.
	CLI:Test Set Validate Invalid Options Multiple Fields	enable_second_ip_alias=yes sec_ip_alias=${POINTS}	Error: sec_ip_alias: Validation error.
	CLI:Test Set Validate Invalid Options Multiple Fields	enable_second_ip_alias=yes sec_ip_alias=${ONE_WORD}	Error: sec_ip_alias: Validation error.
	CLI:Test Set Validate Invalid Options Multiple Fields	enable_second_ip_alias=yes sec_ip_alias=${ONE_NUMBER}	Error: sec_ip_alias: Validation error.
	CLI:Test Set Validate Invalid Options Multiple Fields	enable_second_ip_alias=yes sec_ip_alias=${ONE_POINTS}	Error: sec_ip_alias: Validation error.
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options Multiple Fields	enable_second_ip_alias=yes sec_ip_alias=${EXCEEDED}	Error: sec_ip_alias: Validation error.
	CLI:Test Set Validate Invalid Options Multiple Fields	enable_second_ip_alias=yes sec_ip_alias=${IP_ADDRESS}
	[Teardown]	CLI:Revert

Test sec_interface field
	Skip If	${NGVERSION} == 4.2	Bug NG-3921 is only fixed in 5.0+
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Write	set enable_second_ip_alias=yes sec_ip_alias=${IP_ADDRESS}
	${HAS_ETH1}=	CLI:Has ETH1
	Run Keyword If	'${HAS_ETH1}' == '${TRUE}'	CLI:Test Set Validate Normal Fields	sec_interface	eth0	eth1	loopback
	...	ELSE	CLI:Test Set Validate Normal Fields	sec_interface	eth0	loopback

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	sec_interface	${EMPTY}	Error: Missing value for parameter: sec_interface
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	sec_interface	${EMPTY}	Error: Missing value: sec_interface

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	sec_interface	${WORD}	Error: Invalid value: ${WORD} for parameter: sec_interface
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	sec_interface	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	sec_interface	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: sec_interface
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	sec_interface	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	sec_interface	${POINTS}	Error: Invalid value: ${POINTS} for parameter: sec_interface
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	sec_interface	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	sec_interface	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: sec_interface
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	sec_interface	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	sec_interface	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: sec_interface
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	sec_interface	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	sec_interface	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: sec_interface
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	sec_interface	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	sec_interface	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: sec_interface
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	sec_interface	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test sec_ip_alias_telnet field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Write	set enable_second_ip_alias=yes sec_ip_alias=${IP_ADDRESS}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	sec_ip_alias_telnet	yes	no
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	sec_ip_alias_telnet	yes	no

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_telnet	${EMPTY}	Error: Missing value for parameter: sec_ip_alias_telnet
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_telnet	${EMPTY}	Error: Missing value: sec_ip_alias_telnet

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_telnet	${WORD}	Error: Invalid value: ${WORD} for parameter: sec_ip_alias_telnet
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_telnet	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_telnet	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: sec_ip_alias_telnet
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_telnet	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_telnet	${POINTS}	Error: Invalid value: ${POINTS} for parameter: sec_ip_alias_telnet
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_telnet	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_telnet	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: sec_ip_alias_telnet
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_telnet	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_telnet	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: sec_ip_alias_telnet
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_telnet	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_telnet	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: sec_ip_alias_telnet
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_telnet	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_telnet	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: sec_ip_alias_telnet
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_telnet	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test sec_ip_alias_telnet_port field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Write	set enable_second_ip_alias=yes sec_ip_alias=${IP_ADDRESS}
	CLI:Write	set sec_ip_alias_telnet=yes
	CLI:Test Set Validate Invalid Options	sec_ip_alias_telnet_port	${EMPTY}
	CLI:Test Set Validate Invalid Options	sec_ip_alias_telnet_port	${WORD}	Error: sec_ip_alias_telnet_port: Validation error.
	CLI:Test Set Validate Invalid Options	sec_ip_alias_telnet_port	${NUMBER}
	CLI:Test Set Validate Invalid Options	sec_ip_alias_telnet_port	${POINTS}	Error: sec_ip_alias_telnet_port: Validation error.
	CLI:Test Set Validate Invalid Options	sec_ip_alias_telnet_port	${ONE_WORD}	Error: sec_ip_alias_telnet_port: Validation error.
	CLI:Test Set Validate Invalid Options	sec_ip_alias_telnet_port	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	sec_ip_alias_telnet_port	${ONE_POINTS}	Error: sec_ip_alias_telnet_port: Validation error.
	CLI:Test Set Validate Invalid Options	sec_ip_alias_telnet_port	${EXCEEDED}	Error: sec_ip_alias_telnet_port: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test sec_ip_alias_binary field
	CLI:Write	set enable_second_ip_alias=yes sec_ip_alias=${IP_ADDRESS}
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	sec_ip_alias_binary	yes	no
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	sec_ip_alias_binary	yes	no

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_binary	${EMPTY}	Error: Missing value for parameter: sec_ip_alias_binary
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_binary	${EMPTY}	Error: Missing value: sec_ip_alias_binary

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_binary	${WORD}	Error: Invalid value: ${WORD} for parameter: sec_ip_alias_binary
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_binary	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_binary	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: sec_ip_alias_binary
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_binary	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_binary	${POINTS}	Error: Invalid value: ${POINTS} for parameter: sec_ip_alias_binary
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_binary	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_binary	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: sec_ip_alias_binary
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_binary	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_binary	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: sec_ip_alias_binary
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_binary	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_binary	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: sec_ip_alias_binary
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_binary	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_binary	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: sec_ip_alias_binary
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	sec_ip_alias_binary	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test sec_ip_alias_binary_port field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Write	set enable_second_ip_alias=yes sec_ip_alias=${IP_ADDRESS}
	CLI:Write	set sec_ip_alias_binary=yes
	CLI:Test Set Validate Invalid Options	sec_ip_alias_binary_port	${EMPTY}
	CLI:Test Set Validate Invalid Options	sec_ip_alias_binary_port	${WORD}	Error: sec_ip_alias_binary_port: Validation error.
	CLI:Test Set Validate Invalid Options	sec_ip_alias_binary_port	${NUMBER}
	CLI:Test Set Validate Invalid Options	sec_ip_alias_binary_port	${POINTS}	Error: sec_ip_alias_binary_port: Validation error.
	CLI:Test Set Validate Invalid Options	sec_ip_alias_binary_port	${ONE_WORD}	Error: sec_ip_alias_binary_port: Validation error.
	CLI:Test Set Validate Invalid Options	sec_ip_alias_binary_port	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	sec_ip_alias_binary_port	${ONE_POINTS}	Error: sec_ip_alias_binary_port: Validation error.
	CLI:Test Set Validate Invalid Options	sec_ip_alias_binary_port	${EXCEEDED}	Error: sec_ip_alias_binary_port: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test allow_ssh_protocol field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	allow_ssh_protocol	yes	no
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	allow_ssh_protocol	yes	no

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	allow_ssh_protocol	${EMPTY}	Error: Missing value for parameter: allow_ssh_protocol
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	allow_ssh_protocol	${EMPTY}	Error: Missing value: allow_ssh_protocol

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	allow_ssh_protocol	${WORD}	Error: Invalid value: ${WORD} for parameter: allow_ssh_protocol
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	allow_ssh_protocol	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	allow_ssh_protocol	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: allow_ssh_protocol
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	allow_ssh_protocol	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	allow_ssh_protocol	${POINTS}	Error: Invalid value: ${POINTS} for parameter: allow_ssh_protocol
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	allow_ssh_protocol	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	allow_ssh_protocol	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: allow_ssh_protocol
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	allow_ssh_protocol	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	allow_ssh_protocol	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: allow_ssh_protocol
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	allow_ssh_protocol	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	allow_ssh_protocol	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: allow_ssh_protocol
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	allow_ssh_protocol	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	allow_ssh_protocol	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: allow_ssh_protocol
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	allow_ssh_protocol	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test ssh_port field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Write	set allow_ssh_protocol=yes
	CLI:Test Set Validate Invalid Options	ssh_port	${EMPTY}
	Run Keyword If	${NGVERSION} < 5.2	CLI:Test Set Validate Invalid Options	ssh_port	${WORD}	Error: ssh_port: Must be a unique number between 2001 and 65000.
	Run Keyword If	${NGVERSION} >= 5.2	CLI:Test Set Validate Invalid Options	ssh_port	${WORD}	Error: ssh_port: Port must be a number between 2001 and 65000, and not already in use.

	Run Keyword If	${NGVERSION} < 5.2	CLI:Test Set Validate Invalid Options	ssh_port	${NUMBER}	Error: ssh_port: Must be a unique number between 2001 and 65000.
	Run Keyword If	${NGVERSION} >= 5.2	CLI:Test Set Validate Invalid Options	ssh_port	${NUMBER}	Error: ssh_port: Port must be a number between 2001 and 65000, and not already in use.

	Run Keyword If	${NGVERSION} < 5.2	CLI:Test Set Validate Invalid Options	ssh_port	${POINTS}	Error: ssh_port: Must be a unique number between 2001 and 65000.
	Run Keyword If	${NGVERSION} >= 5.2	CLI:Test Set Validate Invalid Options	ssh_port	${POINTS}	Error: ssh_port: Port must be a number between 2001 and 65000, and not already in use.

	Run Keyword If	${NGVERSION} < 5.2	CLI:Test Set Validate Invalid Options	ssh_port	${ONE_WORD}	Error: ssh_port: Must be a unique number between 2001 and 65000.
	Run Keyword If	${NGVERSION} >= 5.2	CLI:Test Set Validate Invalid Options	ssh_port	${ONE_WORD}	Error: ssh_port: Port must be a number between 2001 and 65000, and not already in use.

	Run Keyword If	${NGVERSION} < 5.2	CLI:Test Set Validate Invalid Options	ssh_port	${ONE_NUMBER}	Error: ssh_port: Must be a unique number between 2001 and 65000.
	Run Keyword If	${NGVERSION} >= 5.2	CLI:Test Set Validate Invalid Options	ssh_port	${ONE_NUMBER}	Error: ssh_port: Port must be a number between 2001 and 65000, and not already in use.

	Run Keyword If	${NGVERSION} < 5.2	CLI:Test Set Validate Invalid Options	ssh_port	${ONE_POINTS}	Error: ssh_port: Must be a unique number between 2001 and 65000.
	Run Keyword If	${NGVERSION} >= 5.2	CLI:Test Set Validate Invalid Options	ssh_port	${ONE_POINTS}	Error: ssh_port: Port must be a number between 2001 and 65000, and not already in use.

	CLI:Test Set Validate Invalid Options	ssh_port	${EXCEEDED}	Error: ssh_port: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	ssh_port	${PORT}
	[Teardown]	CLI:Revert

Test allow_telnet_protocol field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	allow_telnet_protocol	yes	no
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	allow_telnet_protocol	yes	no

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	allow_telnet_protocol	${EMPTY}	Error: Missing value for parameter: allow_telnet_protocol
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	allow_telnet_protocol	${EMPTY}	Error: Missing value: allow_telnet_protocol

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	allow_telnet_protocol	${WORD}	Error: Invalid value: ${WORD} for parameter: allow_telnet_protocol
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	allow_telnet_protocol	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	allow_telnet_protocol	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: allow_telnet_protocol
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	allow_telnet_protocol	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	allow_telnet_protocol	${POINTS}	Error: Invalid value: ${POINTS} for parameter: allow_telnet_protocol
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	allow_telnet_protocol	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	allow_telnet_protocol	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: allow_telnet_protocol
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	allow_telnet_protocol	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	allow_telnet_protocol	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: allow_telnet_protocol
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	allow_telnet_protocol	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	allow_telnet_protocol	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: allow_telnet_protocol
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	allow_telnet_protocol	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	allow_telnet_protocol	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: allow_telnet_protocol
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	allow_telnet_protocol	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test telnet_port field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Write	set allow_telnet_protocol=yes
	CLI:Test Set Validate Invalid Options	telnet_port	${EMPTY}
	Run Keyword If	${NGVERSION} < 5.2	CLI:Test Set Validate Invalid Options	telnet_port	${WORD}	Error: telnet_port: Must be a unique number between 2001 and 65000.
	Run Keyword If	${NGVERSION} >= 5.2	CLI:Test Set Validate Invalid Options	telnet_port	${WORD}	Error: telnet_port: Port must be a number between 2001 and 65000, and not already in use.

	Run Keyword If	${NGVERSION} < 5.2	CLI:Test Set Validate Invalid Options	telnet_port	${NUMBER}	Error: telnet_port: Must be a unique number between 2001 and 65000.
	Run Keyword If	${NGVERSION} >= 5.2	CLI:Test Set Validate Invalid Options	telnet_port	${NUMBER}	Error: telnet_port: Port must be a number between 2001 and 65000, and not already in use.

	Run Keyword If	${NGVERSION} < 5.2	CLI:Test Set Validate Invalid Options	telnet_port	${POINTS}	Error: telnet_port: Must be a unique number between 2001 and 65000.
	Run Keyword If	${NGVERSION} >= 5.2	CLI:Test Set Validate Invalid Options	telnet_port	${POINTS}	Error: telnet_port: Port must be a number between 2001 and 65000, and not already in use.

	Run Keyword If	${NGVERSION} < 5.2	CLI:Test Set Validate Invalid Options	telnet_port	${ONE_WORD}	Error: telnet_port: Must be a unique number between 2001 and 65000.
	Run Keyword If	${NGVERSION} >= 5.2	CLI:Test Set Validate Invalid Options	telnet_port	${ONE_WORD}	Error: telnet_port: Port must be a number between 2001 and 65000, and not already in use.

	Run Keyword If	${NGVERSION} < 5.2	CLI:Test Set Validate Invalid Options	telnet_port	${ONE_NUMBER}	Error: telnet_port: Must be a unique number between 2001 and 65000.
	Run Keyword If	${NGVERSION} >= 5.2	CLI:Test Set Validate Invalid Options	telnet_port	${ONE_NUMBER}	Error: telnet_port: Port must be a number between 2001 and 65000, and not already in use.

	Run Keyword If	${NGVERSION} < 5.2	CLI:Test Set Validate Invalid Options	telnet_port	${ONE_POINTS}	Error: telnet_port: Must be a unique number between 2001 and 65000.
	Run Keyword If	${NGVERSION} >= 5.2	CLI:Test Set Validate Invalid Options	telnet_port	${ONE_POINTS}	Error: telnet_port: Port must be a number between 2001 and 65000, and not already in use.

	CLI:Test Set Validate Invalid Options	telnet_port	${EXCEEDED}	Error: telnet_port: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	telnet_port	${PORT}
	[Teardown]	CLI:Revert

Test allow_binary_socket field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	allow_binary_socket	yes	no
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	allow_binary_socket	yes	no

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	allow_binary_socket	${EMPTY}	Error: Missing value for parameter: allow_binary_socket
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	allow_binary_socket	${EMPTY}	Error: Missing value: allow_binary_socket

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	allow_binary_socket	${WORD}	Error: Invalid value: ${WORD} for parameter: allow_binary_socket
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	allow_binary_socket	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	allow_binary_socket	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: allow_binary_socket
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	allow_binary_socket	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	allow_binary_socket	${POINTS}	Error: Invalid value: ${POINTS} for parameter: allow_binary_socket
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	allow_binary_socket	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	allow_binary_socket	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: allow_binary_socket
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	allow_binary_socket	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	allow_binary_socket	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: allow_binary_socket
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	allow_binary_socket	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	allow_binary_socket	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: allow_binary_socket
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	allow_binary_socket	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	allow_binary_socket	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: allow_binary_socket
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	allow_binary_socket	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test tcp_socket_port field
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Write	set allow_binary_socket=yes
	CLI:Test Set Validate Invalid Options	tcp_socket_port	${EMPTY}
	Run Keyword If	${NGVERSION} < 5.2	CLI:Test Set Validate Invalid Options	tcp_socket_port	${WORD}	Error: tcp_socket_port: Must be a unique number between 2001 and 65000.
	Run Keyword If	${NGVERSION} >= 5.2	CLI:Test Set Validate Invalid Options	tcp_socket_port	${WORD}	Error: tcp_socket_port: Port must be a number between 2001 and 65000, and not already in use.

	Run Keyword If	${NGVERSION} < 5.2	CLI:Test Set Validate Invalid Options	tcp_socket_port	${NUMBER}	Error: tcp_socket_port: Must be a unique number between 2001 and 65000.
	Run Keyword If	${NGVERSION} >= 5.2	CLI:Test Set Validate Invalid Options	tcp_socket_port	${NUMBER}	Error: tcp_socket_port: Port must be a number between 2001 and 65000, and not already in use.

	Run Keyword If	${NGVERSION} < 5.2	CLI:Test Set Validate Invalid Options	tcp_socket_port	${POINTS}	Error: tcp_socket_port: Must be a unique number between 2001 and 65000.
	Run Keyword If	${NGVERSION} >= 5.2	CLI:Test Set Validate Invalid Options	tcp_socket_port	${POINTS}	Error: tcp_socket_port: Port must be a number between 2001 and 65000, and not already in use.

	Run Keyword If	${NGVERSION} < 5.2	CLI:Test Set Validate Invalid Options	tcp_socket_port	${ONE_WORD}	Error: tcp_socket_port: Must be a unique number between 2001 and 65000.
	Run Keyword If	${NGVERSION} >= 5.2	CLI:Test Set Validate Invalid Options	tcp_socket_port	${ONE_WORD}	Error: tcp_socket_port: Port must be a number between 2001 and 65000, and not already in use.

	Run Keyword If	${NGVERSION} < 5.2	CLI:Test Set Validate Invalid Options	tcp_socket_port	${ONE_NUMBER}	Error: tcp_socket_port: Must be a unique number between 2001 and 65000.
	Run Keyword If	${NGVERSION} >= 5.2	CLI:Test Set Validate Invalid Options	tcp_socket_port	${ONE_NUMBER}	Error: tcp_socket_port: Port must be a number between 2001 and 65000, and not already in use.

	Run Keyword If	${NGVERSION} < 5.2	CLI:Test Set Validate Invalid Options	tcp_socket_port	${ONE_POINTS}	Error: tcp_socket_port: Must be a unique number between 2001 and 65000.
	Run Keyword If	${NGVERSION} >= 5.2	CLI:Test Set Validate Invalid Options	tcp_socket_port	${ONE_POINTS}	Error: tcp_socket_port: Port must be a number between 2001 and 65000, and not already in use.

	CLI:Test Set Validate Invalid Options	tcp_socket_port	${EXCEEDED}	Error: tcp_socket_port: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	tcp_socket_port	${PORT}
	[Teardown]	CLI:Revert

Test Valid Values For Field=allow_pre-shared_ssh_key
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Test Set Field Options	allow_pre-shared_ssh_key	no	yes
	[Teardown]	CLI:Revert	Raw

Test Invalid Values For Field=allow_pre-shared_ssh_key
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	${ALL_VALUES}=	Create List	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
	...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
	...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
	...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
	...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
	...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}	${EXCEEDED}
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Test Set Field Invalid Options	allow_pre-shared_ssh_key	${EMPTY}
	...	Error: Missing value for parameter: allow_pre-shared_ssh_key

	FOR	${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	allow_pre-shared_ssh_key	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: allow_pre-shared_ssh_key
	END
	[Teardown]	CLI:Revert	Raw

Test valid values for map_to_virtual_machine field
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL	BUG_NG_10464
	Skip If	not ${HAS_USBS}	The device being tested has no USB ports
	CLI:Enter Path	/settings/devices/${USB_DEVICE}/access
	CLI:Set	map_to_virtual_machine=yes virtual_machine_name=${VM_NAME}
	CLI:Commit
	CLI:Set	map_to_virtual_machine=no
	CLI:Commit
	[Teardown]	CLI:Cancel	Raw

Test invalid values for map_to_virtual_machine field
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL
	Skip If	not ${HAS_USBS}	The device being tested has no USB ports
	CLI:Enter Path	/settings/devices/${USB_DEVICE}/access
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Test Set Field Invalid Options	map_to_virtual_machine	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: map_to_virtual_machine
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for virtual_machine_name field
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL
	Skip If	not ${HAS_USBS}	The device being tested has no USB ports
	CLI:Enter Path	/settings/devices/${USB_DEVICE}/access
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Set	map_to_virtual_machine=yes virtual_machine_name=${VALUE}
		CLI:Commit
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for virtual_machine_name field
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL	BUG_NG_10464
	Skip If	not ${HAS_USBS}	The device being tested has no USB ports
	CLI:Enter Path	/settings/devices/${USB_DEVICE}/access
	${OUTPUT}	CLI:Write	set map_to_virtual_machine=yes virtual_machine_name=${EMPTY}	Raw
	Should Contain	${OUTPUT}	Error: virtual_machine_name: Field must not be empty.
	[Teardown]	CLI:Cancel	Raw

*** Keyword ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/devices
	@{DEVICELIST}=	CLI:Get Devices List	all	${PROTECTED_DEVICE_TYPES}
	CLI:Delete Devices	@{DEVICELIST}
	${OUTPUT}=	CLI:Test Show Command
	Run Keyword If	$DUMMY_DEVICE_CONSOLE_NAME in $OUTPUT	CLI:Delete Device	${DUMMY_DEVICE_CONSOLE_NAME}
	CLI:Add Device	${DUMMY_DEVICE_CONSOLE_NAME}	ilo
	${OUTPUT}=	CLI:Test Show Command
	Should Contain	${OUTPUT}	${DUMMY_DEVICE_CONSOLE_NAME}
	${DEVICES}	CLI:Ls	/settings/devices
	${HAS_USBS}	Run Keyword And Return Status	Should Contain	${DEVICES}	usbS
	Set Suite Variable	${HAS_USBS}
	${USB_DEVICE}	Set Variable If	${HAS_USBS}	usbS0-3	${EMPTY}
	Run Keyword If	${HAS_USBS}	Set Suite Variable	${USB_DEVICE}

SUITE:Teardown
	@{DEVICELIST}=	CLI:Get Devices List	all	${PROTECTED_DEVICE_TYPES}
	CLI:Delete Devices	@{DEVICELIST}
	CLI:Close Connection