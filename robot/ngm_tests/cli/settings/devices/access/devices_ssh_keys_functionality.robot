*** Settings ***
Resource	../../../init.robot
Documentation	Tests device access through ssh keys validation
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL	NEED-REVIEW
Default Tags	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Test Teardown	SUITE:Test Teardown

Test Template	SUITE:Devices SSH Keys Functionality Template

*** Variables ***
${DEVICE_NAME}	test-dev.ice
${DEVICE_TYPE}	device_console
${DEVICE_IP}	${SSHKEYS_SERVER}
${DEVICE_USERNAME}	${SSHKEYS_SERVER_USER}
${DEVICE_PASSWORD}	${SSHKEYS_SERVER_PASSWORD}

*** Test Cases ***
Test SSH Authentication To Device Using ssh_key_type=ecdsa_384	ecdsa_384
Test SSH Authentication To Device Using ssh_key_type=ed25519	ed25519
Test SSH Authentication To Device Using ssh_key_type=rsa_2048	rsa_2048
Test SSH Authentication To Device Using ssh_key_type=ecdsa_256	ecdsa_256
Test SSH Authentication To Device Using ssh_key_type=ecdsa_521	ecdsa_521
Test SSH Authentication To Device Using ssh_key_type=rsa_1024	rsa_1024
Test SSH Authentication To Device Using ssh_key_type=rsa_4096	rsa_4096

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Delete Devices	${DEVICE_NAME}
	SUITE:Add Device	${DEVICE_NAME}	${DEVICE_TYPE}	${DEVICE_IP}
	...	${DEVICE_USERNAME}
	SUITE:Enable SSH Pre Shared Keys
	SUITE:SSH To Device
	SUITE:Generate Backup File For Authorized Keys On Device

SUITE:Teardown
	CLI:Enter Path	/settings/devices/
	CLI:Delete If Exists Confirm	${DEVICE_NAME}
	SUITE:Remove Backup FIle
	CLI:Close Connection

SUITE:Test Teardown
	CLI:Disconnect Device	Yes
	SUITE:Clear SSH Configuration On Device

SUITE:Add Device
	[Arguments]	${DEVICE_NAME}	${DEVICE_TYPE}	${DEVICE_IP}	${DEVICE_USERNAME}
	CLI:Add Device	${DEVICE_NAME}	${DEVICE_TYPE}	${DEVICE_IP}	${DEVICE_USERNAME}
	CLI:Enter Path	/settings/devices/
	${OUTPUT}	CLI:Show
	Should Contain	${OUTPUT}	${DEVICE_NAME}

SUITE:Devices SSH Keys Functionality Template
	[Arguments]	${SSH_KEY_TYPE}
	SUITE:Generate And Send SSH Keys	${SSH_KEY_TYPE}
	CLI:Connect Device	${DEVICE_NAME}	\#	Yes

SUITE:SSH To Device
	CLI:Open	USERNAME=${DEVICE_USERNAME}	PASSWORD=${DEVICE_PASSWORD}
	...	session_alias=device_ssh_session	TYPE=root	HOST_DIFF=${DEVICE_IP}

SUITE:Enable SSH Pre Shared Keys
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/access
	CLI:Set	allow_pre-shared_ssh_key=yes
	CLI:Commit

SUITE:Disable SSH Pre Shared Keys
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/access
	CLI:Set	allow_pre-shared_ssh_key=no
	CLI:Commit

SUITE:Generate Backup File For Authorized Keys On Device
	CLI:Switch Connection	device_ssh_session
	${OUTPUT}=	CLI:Write	cd /root/.ssh/
	${HAS_USER_SSH_FOLDER}=	Run Keyword And Return Status
	...	Should Not Contain	${OUTPUT}	No such file or directory
	Run Keyword If	not ${HAS_USER_SSH_FOLDER}	Run Keywords
	...	Set Suite Variable	${HAD_KEYS_FILE}	${HAS_USER_SSH_FOLDER}	AND
	...	Return From Keyword

	${OUTPUT}=	CLI:Write	ls authorized_keys	No	Yes
	${HAD_KEYS_FILE}=	Run Keyword And Return Status
	...	Should Be Equal	${OUTPUT}	authorized_keys
	Set Suite Variable	${HAD_KEYS_FILE}
	Run Keyword If	${HAD_KEYS_FILE}
	...	CLI:Write	cp /root/.ssh/authorized_keys /root/.ssh/authorized_keys.bak

SUITE:Generate And Send SSH Keys
	[Arguments]	${SSH_KEY_TYPE}
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/access
	CLI:Write	ssh_keys
	CLI:Set	ssh_key_type=${SSH_KEY_TYPE}
	CLI:Write	generate_key_pair
	CLI:Set	ip_address=%IP username=%USER
	CLI:Set	password_use=use_specific password=${DEVICE_PASSWORD}
	CLI:Write	send_public_key
	[Teardown]	SUITE:Return

SUITE:Clear SSH Configuration On Device
	CLI:Switch Connection	device_ssh_session
	Run Keyword If	${HAD_KEYS_FILE}
	...	CLI:Write	cp /root/.ssh/authorized_keys.bak /root/.ssh/authorized_keys
	...	ELSE	CLI:Write	rm /root/.ssh/authorized_keys

SUITE:Remove Backup FIle
	CLI:Switch Connection	device_ssh_session
	Run Keyword If	${HAD_KEYS_FILE}	CLI:Write	rm /root/.ssh/authorized_keys.bak

SUITE:Return
	CLI:Write	return	Raw