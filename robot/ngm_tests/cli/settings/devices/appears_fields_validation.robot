*** Settings ***
Resource	../../init.robot
Documentation	Test appears and disappears fields with all devices types
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	MANAGED_DEVICES	DEVICES	FIELDS	REVIEWED
Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection
*** Variables ***
@{VALUES}	${ONE_WORD}	${WORD}	${ONE_NUMBER}	${NUMBER}	${ONE_POINTS}	${POINTS}	${EXCEEDED}
...	${WORD_AND_NUMBER}	${WORD_AND_POINTS}	${POINTS_AND_NUMBER}
${DEVICE_NAME}	device_validation_test

*** Test Cases ***
Test set avaliable fields
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Test Set Available Fields	name	type	ip_address	enable_device_state_detection_based_on_network_traffic
	...	multisession	read-write_multisession	enable_send_break	address_location
	...	coordinates	web_url	launch_url_via_html5	icon	mode	skip_authentication_to_access_device
	...	escape_sequence	power_control_key	show_text_information	enable_ip_alias	enable_second_ip_alias
	...	allow_ssh_protocol	ssh_port	expiration	allow_telnet_protocol	allow_binary_socket	end_point
	...	enable_hostname_detection	vm_manager	port	username	credential	password	chassis_id	blade_id
	[Teardown]	CLI:Cancel	Raw

Test values for field=allow_binary_socket
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Test Set Validate Normal Fields	allow_binary_socket	yes	no
	[Teardown]	CLI:Cancel	Raw

Test values for field=allow_pre-shared_ssh_key=
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Test Set Validate Normal Fields	allow_pre-shared_ssh_key	yes	no
	[Teardown]	CLI:Cancel	Raw

Test values for field=allow_ssh_protocol
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Test Set Validate Normal Fields	allow_ssh_protocol	yes	no
	[Teardown]	CLI:Cancel	Raw
#Test values for field=ssh_port
Test values for field=allow_telnet_protocol
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Test Set Validate Normal Fields	allow_telnet_protocol	yes	no
	[Teardown]	CLI:Cancel	Raw

Test values for field=telnet_port
	CLI:Enter Path	/settings/devices/
	@{VALID_PORTS}	Create List	2001	22222	64999
	FOR	${PORT}	IN	@{VALID_PORTS}
		CLI:Add
		CLI:Set	name=${DEVICE_NAME} allow_telnet_protocol=yes
		CLI:Set	telnet_port=${PORT}
		CLI:Commit
		CLI:Delete If Exists Confirm	${DEVICE_NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test values for field=coordinates
	CLI:Enter Path	/settings/devices/
	@{VALID_COORDINATES}	Create List	"${ONE_NUMBER},${ONE_NUMBER}"	"-${TWO_NUMBER},-${THREE_NUMBER}"	"${TWO_NUMBER},-${ONE_NUMBER}"
	FOR	${COORDINATE}	IN	@{VALID_COORDINATES}
		CLI:Add
		CLI:Set	name=${DEVICE_NAME}
		CLI:Set	coordinates=${COORDINATE}
		CLI:Commit
		CLI:Delete If Exists Confirm	${DEVICE_NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test values for field=credentials
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Test Set Validate Normal Fields	credential	ask_during_login	set_now
	[Teardown]	CLI:Cancel	Raw

Test values for field=enable_device_state_detection_based_on_network_traffic
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Test Set Validate Normal Fields	enable_device_state_detection_based_on_network_traffic	yes	no
	[Teardown]	CLI:Cancel	Raw

Test values for field=enable_hostname_detection
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Test Set Validate Normal Fields	enable_hostname_detection	yes	no
	[Teardown]	CLI:Cancel	Raw

Test values for field=enable_ip_alias
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Test Set Validate Normal Fields	enable_ip_alias	yes	no
	[Teardown]	CLI:Cancel	Raw

Test values for field=ip_alias_telnet
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Set	enable_ip_alias=yes
	CLI:Test Set Validate Normal Fields	ip_alias_telnet	yes	no
	[Teardown]	CLI:Cancel	Raw
Test values for field=ip_alias_telnet_port
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Set	enable_ip_alias=yes
	CLI:Test Set Validate Normal Fields	ip_alias_telnet_port	yes	no
	[Teardown]	CLI:Cancel	Raw

Test values for field=ip_alias_binary
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Set	enable_ip_alias=yes
	CLI:Test Set Validate Normal Fields	ip_alias_binary	yes	no
	[Teardown]	CLI:Cancel	Raw

Test values for field=ip_browser_action
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Set	enable_ip_alias=yes
	CLI:Test Set Validate Normal Fields	ip_alias_browser_action	console	web
	[Teardown]	CLI:Cancel	Raw

#Test values for field=ip_alias

Test values for field=enable_second_ip_alias
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Set	enable_ip_alias=yes
	CLI:Test Set Validate Normal Fields	enable_second_ip_alias	yes	no
	[Teardown]	CLI:Cancel	Raw

Test values for field=sec_ip_alias_telnet
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Set	enable_ip_alias=yes enable_second_ip_alias=yes
	CLI:Test Set Validate Normal Fields	sec_ip_alias_telnet	yes	no
	[Teardown]	CLI:Cancel	Raw

Test values for field=sec_ip_alias_telnet_port
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Set	enable_ip_alias=yes enable_second_ip_alias=yes
	CLI:Test Set Validate Normal Fields	sec_ip_alias_telnet_port	yes	no
	[Teardown]	CLI:Cancel	Raw

Test values for field=sec_ip_alias_binary
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Set	enable_ip_alias=yes enable_second_ip_alias=yes
	CLI:Test Set Validate Normal Fields	sec_ip_alias_binary	yes	no
	[Teardown]	CLI:Cancel	Raw

Test values for field=sec_ip_browser_action
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Set	enable_ip_alias=yes enable_second_ip_alias=yes
	CLI:Test Set Validate Normal Fields	sec_ip_alias_browser_action	console	web
	[Teardown]	CLI:Cancel	Raw

#Test values for field=sec_ip_alias

Test values for field=enable_send_break
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Test Set Validate Normal Fields	enable_send_break	yes	no
	[Teardown]	CLI:Cancel	Raw

Test values for field=break_sequence
	CLI:Enter Path	/settings/devices
	Remove Values From List	${VALUES}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${EXCEEDED}
	FOR	${VALUE}	IN	@{VALUES}
		CLI:Add
		CLI:Set	name=${DEVICE_NAME} enable_send_break=yes
		CLI:Set	break_sequence=${VALUE}
		CLI:Commit
		CLI:Delete If Exists Confirm	${DEVICE_NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test values for field=end_point
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/devices/
	CLI:Add
	@{ENDPOINTS}	Create List	appliance	kvm_port	serial_port
	Run Keyword If	${NGVERSION} > 3.2	Append To List	${ENDPOINTS}	appliance	serial_port
	CLI:Test Set Validate Normal Fields	end_point	@{ENDPOINTS}
	[Teardown]	CLI:Cancel	Raw

Test values for field=escape_sequence
	CLI:Enter Path	/settings/devices
	@{SEQUENCES}	Create List	^Ex	^ex	^bb
	FOR	${SEQUENCE}	IN	@{SEQUENCES}
		CLI:Add
		CLI:Set	name=${DEVICE_NAME}
		CLI:Set	escape_sequence=${SEQUENCE}
		CLI:Commit
		CLI:Delete If Exists Confirm	${DEVICE_NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test values for field=expiration
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Test Set Validate Normal Fields	expiration	date	days	never
	[Teardown]	CLI:Cancel	Raw

Test values for field=expiration_date
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/devices
	@{VALID_DATES}	Create List	1970-01-01	2022-01-20	0001-01-01
	FOR	${DATE}	IN	@{VALID_DATES}
		CLI:Add
		CLI:Set	name=${DEVICE_NAME} expiration=date
		CLI:Set	expiration_date=${DATE}
		CLI:Commit
		CLI:Delete If Exists Confirm	${DEVICE_NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test values for field=duration
	CLI:Enter Path	/settings/devices
	@{DURATIONS}	Create List	${ONE_NUMBER}	${TWO_NUMBER}	${THREE_NUMBER}	${EIGHT_NUMBER}
	FOR	${DURATION}	IN	@{DURATIONS}
		CLI:Add
		CLI:Set	name=${DEVICE_NAME} expiration=days
		CLI:Set	duration=${DURATION}
		CLI:Commit
		CLI:Delete If Exists Confirm	${DEVICE_NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test values for field=icon
	CLI:Enter Path	/settings/devices/
	CLI:Add
	@{ICON_IMAGES}	Create List	storage_blue.png
	...	storage_grey.png	pdu.png	storage_grey_dark.png	apple_black.png	ibm_black.png	supermicro.png
	...	arista.png	juniper.png	switch.png	switch_purple.png	linux_black.png	cisco_color.png	linux_color.png
	...	router_green.png	terminal.png	ups.png	netapp.png	usb.png	dell.png	serial_console.png	vm.png
	...	oracle.png	server.png	vmware.png	outlet.png	server_grey.png	windows_black.png	emc.png	windows_color.png
	Run Keyword If	${NGVERSION} > 3.2	Append To List	${ICON_IMAGES}	128technology.png	fortinet.png	paloaltofirewall.png
	...	passcode.png	hp.png	pincode.png	aruba.png	kvm.png	pinconfirm.png
	...	centos.png	cpi.png	sdwan.png	nodegrid.png
	...	paloalto.png	firewall.png	zpe.png
	Run Keyword If	${NGVERSION} > 5.0	Append To List	${ICON_IMAGES}	apc.png	perle.png	cloudgenix.png	lxc.png	schneider.png	docker.png	air_flow-temperature.png
	...	gpio.png	relay.png	paloalto2.png	temperature-humidity.png	dust_particle.png	signal_indicator.png	signal_tower.png
	CLI:Test Set Validate Normal Fields	icon	@{ICON_IMAGES}
	[Teardown]	CLI:Cancel	Raw

#Test values for field=ip_address

Test values for field=launch_url_via_html5
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Test Set Validate Normal Fields	launch_url_via_html5	yes	no
	[Teardown]	CLI:Cancel	Raw

Test values for field=method
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Test Set Validate Normal Fields	method	browser_extension_forwarder	internal_browser
	[Teardown]	CLI:Cancel	Raw

Test values for field=mode
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Test Set Validate Normal Fields	mode	disabled	enabled	on-demand
	[Teardown]	CLI:Cancel	Raw

Test values for field=multisession
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Test Set Validate Normal Fields	multisession	yes	no
	[Teardown]	CLI:Cancel	Raw

Test values for field=port
	CLI:Enter Path	/settings/devices/
	@{VALID_PORTS}	Create List	${ONE_NUMBER}	${EIGHT_NUMBER}	${EXCEEDED}
	FOR	${PORT}	IN	@{VALID_PORTS}
		CLI:Add
		CLI:Set	name=${DEVICE_NAME}
		CLI:Set	port=${PORT}
		CLI:Commit
		CLI:Delete If Exists Confirm	${DEVICE_NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test values for field=power_control_key
	CLI:Enter Path	/settings/devices
	@{KEYS}	Create List	^C	^Z	^X
	FOR	${KEY}	IN	@{KEYS}
		CLI:Add
		CLI:Set	name=${DEVICE_NAME}
		CLI:Set	power_control_key=${KEY}
		CLI:Commit
		CLI:Delete If Exists Confirm	${DEVICE_NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test values for field=read-write_multisession
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Test Set Validate Normal Fields	read-write_multisession	yes	no
	[Teardown]	CLI:Cancel	Raw

Test values for field=show_text_information
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Test Set Validate Normal Fields	show_text_information	yes	no
	[Teardown]	CLI:Cancel	Raw

Test values for field=skip_authentication_to_access_device
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Test Set Validate Normal Fields	skip_authentication_to_access_device	yes	no
	[Teardown]	CLI:Cancel	Raw

Test values for field=type
	CLI:Enter Path	/settings/devices/
	CLI:Add
	@{DEVICE_TYPES}	Create List		cimc_ucs	idrac6	kvm_raritan	pdu_mph2
	...	console_server_acs	ilo	netapp	pdu_pm3000
	...	console_server_acs6000	ilom	pdu_raritan
	...	console_server_digicp	imm	pdu_apc
	...	console_server_lantronix	infrabox	pdu_baytech	pdu_servertech
	...	console_server_opengear	ipmi_1.5	ipmi_2.0	virtual_console_kvm
	...	kvm_aten	pdu_eaton	virtual_console_vmware
	...	device_console	kvm_dsr	pdu_enconnex	drac	kvm_mpu

	Run Keyword If	${NGVERSION} > 3.2	Append To List	${DEVICE_TYPES}	pdu_rittal		console_server_nodegrid
	...	pdu_cpi	pdu_cyberpower	console_server_perle	console_server_raritan	pdu_geist
	Run Keyword If	${NGVERSION} > 5.0	Append To List	${DEVICE_TYPES}	nodegrid_ap	intel_bmc	pdu_tripplite	ups_apc	pdu_digital_loggers
	CLI:Test Set Validate Normal Fields	type	@{DEVICE_TYPES}
	[Teardown]	CLI:Cancel	Raw