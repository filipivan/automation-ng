*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Edit Devices... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	NEED-REVIEW
Default Tags	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE1}	device1
${DEVICE2}	device2
${IP_ALIAS_IPV6}	${IP_ALIAS_IPV6_2}
@{TESTING_DEVICES}	dev123ABC	dev143ABC	dev123CBC	devABC	dev143

*** Test Cases ***
Test edit with invalid regexp
	CLI:Enter Path	/settings/devices/
	${OUTPUT}=	Run Keyword If	'${NGVERSION}' <= '5.0'	CLI:Edit	--regexp *	Yes
	...	ELSE	CLI:Edit	*	Yes
	Run Keyword If	'${NGVERSION}' <= '5.0'	Should Contain	${OUTPUT}	Error: Missing or invalid regular expression
	Run Keyword If	'${NGVERSION}' >= '5.2'	Should Contain	${OUTPUT}	Error: Not enough arguments
	[Teardown]	CLI:Cancel	Raw

# Devices with 'dev<digit><anything>'
Test edit with regexp=dev[0-9]+.*
	SUITE:Add Multiple Devices
	${MATCH_DEVICES}=	Get Matches	${TESTING_DEVICES}	dev\\d+.*
	SUITE:Edit Multiple Devices	${MATCH_DEVICES}	dev[0-9]+.*
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	CLI:Delete All Devices

#All devices that don't have letters t,y,u,s,b (no tty or usb)
Test edit with regexp=[^tyusb]+
	SUITE:Add Multiple Devices
	SUITE:Edit Multiple Devices	${TESTING_DEVICES}	[^tyusb]+
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	CLI:Delete All Devices

Test edit device
	SUITE:Add Multiple Devices
	CLI:Enter Path	/settings/devices/
	${COMMA_SEPARATED_DEVICES}=	Catenate	SEPARATOR=,	@{TESTING_DEVICES}
	${OUTPUT}=	CLI:Edit	${COMMA_SEPARATED_DEVICES}
	Should Contain	${OUTPUT}	{devices}
	CLI:Write	set ip_address=127.0.0.23
	CLI:Show
	CLI:Commit

	SUITE:Show Str N Times	127.0.0.23	5
	CLI:Edit	--regexp .*143.*
	CLI:Write	set ip_address=127.0.0.24
	CLI:Show
	CLI:Commit

	SUITE:Show Str N Times	127.0.0.23	3
	SUITE:Show Str N Times	127.0.0.24	2
	CLI:Edit	--regexp .*143
	CLI:Write	set ip_address=127.0.0.25
	CLI:Show
	CLI:Commit

	SUITE:Show Str N Times	127.0.0.23	3
	SUITE:Show Str N Times	127.0.0.24	1
	SUITE:Show Str N Times	127.0.0.25	1
	CLI:Edit	--regexp [a-z]{3}[A-Z]+
	CLI:Write	set ip_address=127.0.0.26
	CLI:Show
	CLI:Commit

	SUITE:Show Str N Times	127.0.0.23	2
	SUITE:Show Str N Times	127.0.0.24	1
	SUITE:Show Str N Times	127.0.0.25	1
	SUITE:Show Str N Times	127.0.0.26	1
	CLI:Edit	--regexp [a-z]{3}[0-9]{3}[A-Z]{3}
	CLI:Write	set ip_address=127.0.0.27
	CLI:Show
	CLI:Commit

	SUITE:Show Str N Times	127.0.0.25	1
	SUITE:Show Str N Times	127.0.0.26	1
	SUITE:Show Str N Times	127.0.0.27	3
	CLI:Edit	--regexp [a-z]{3}.{3}[A-Z]{3}
	CLI:Write	set ip_address=127.0.0.28
	CLI:Show
	CLI:Commit

	SUITE:Show Str N Times	127.0.0.25	1
	SUITE:Show Str N Times	127.0.0.26	1
	SUITE:Show Str N Times	127.0.0.28	3
	CLI:Edit	--regexp [a-z0-9A-C]{6}
	CLI:Write	set ip_address=127.0.0.29
	CLI:Show
	CLI:Commit

	SUITE:Show Str N Times	127.0.0.29	2
	SUITE:Show Str N Times	127.0.0.28	3
	CLI:Edit	--regexp [a-z0-9]{3}[A-Z]{3}
	CLI:Write	set ip_address=127.0.0.30
	CLI:Show
	CLI:Commit

	SUITE:Show Str N Times	127.0.0.29	1
	SUITE:Show Str N Times	127.0.0.28	3
	SUITE:Show Str N Times	127.0.0.30	1
	CLI:Edit	dev143
	CLI:Write	set ip_address=127.0.0.31
	CLI:Show
	CLI:Commit

	SUITE:Show Str N Times	127.0.0.28	3
	SUITE:Show Str N Times	127.0.0.30	1
	SUITE:Show Str N Times	127.0.0.31	1
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	CLI:Delete All Devices

Test edit two devices with dot in names
	CLI:Enter Path	/settings/devices/
	CLI:Add Device	self.device	device_console	127.0.0.1	root	${ROOT_PASSWORD}	enabled
	CLI:Enter Path	/settings/devices/
	CLI:Add Device	self.device2	device_console	127.0.0.2	root	${ROOT_PASSWORD}	enabled
	CLI:Enter Path	/settings/devices/
	CLI:Edit	self.device,self.device2
	CLI:Set Field	ip_address	127.0.0.3
	CLI:Commit
	SUITE:Show Str N Times	127.0.0.3	2
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	CLI:Delete All Devices

Test local device edit with regexp=u*
	CLI:Enter Path	/settings/devices/
	CLI:Add Device	test_device	device_console	127.0.0.1	root	${ROOT_PASSWORD}	enabled
	CLI:Enter Path	/settings/devices/
	${OUTPUT}=	Run Keyword If	'${NGVERSION}' <= '5.0'	CLI:Edit	--regexp u*	Yes
	...	ELSE	CLI:Edit	u*	Yes
	Run Keyword If	'${NGVERSION}' >= '4.2' and '${NGVERSION}' <= '5.0'	Should Contain	${OUTPUT}	Error: entry not found
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${OUTPUT}	Error: Device not found.
	[Teardown]	CLI:Cancel	Raw

Test validation for type
	CLI:Enter Path	/settings/devices/
	CLI:Add Device	test_ilodevice	ilo
	CLI:Enter Path	/settings/devices/
	${OUTPUT}=	CLI:Edit	--regexp .*	Yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	Error: type: All selected devices shall have the same type
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${OUTPUT}	Error: All selected devices shall have the same type
	[Teardown]	CLI:Cancel	Raw

Test edit devices on IP Alias with IPv6
	[Setup]	SUITE:Add Devices	${DEVICE1}	${DEVICE2}
	CLI:Enter Path	/settings/devices/
	CLI:Edit	${DEVICE1},${DEVICE2}
	CLI:Set	enable_ip_alias=yes ip_alias=${IP_ALIAS_IPV6}
	CLI:Commit
	CLI:Enter Path	/settings/devices/${DEVICE1}/access/
	${OUTPUT}=	CLI:Show	ip_alias
	Should Contain	${OUTPUT}	${IP_ALIAS_IPV6}

	${LAST_PART}=	Fetch From Right	${IP_ALIAS_IPV6}	:
	${LAST_PART}=	Convert To Integer	${LAST_PART}	16
	${SUM}=	Evaluate	${LAST_PART}+1
	${SUM}=	Convert to Hex	${SUM}
	${SUM}=	Convert To Lowercase	${SUM}
	CLI:Enter Path	/settings/devices/${DEVICE2}/access/
	${OUTPUT}=	CLI:Write	show ip_alias	user=Yes
	${LAST_PART}=	Fetch From Right	${OUTPUT}	:
	Should Be Equal	${LAST_PART}	${SUM}

#All devices
Test edit data_logging field with regexp=ttyS.*
	Skip If	not ${HAS_DEVICES} or not ${HAS_SERIAL}	No serial ports available
	SUITE:Edit Multiple Devices	${SERIAL_DEVICES}	ttyS.*
	[Teardown]	CLI:Cancel	Raw

Test edit with regexp=.*
	Skip If	${HAS_DEVICES}
	[Setup]	CLI:Delete All Devices
	SUITE:Add devices	${DEVICE1}	${DEVICE2}
	SUITE:Edit Multiple Devices	${DEVICES_LIST}	.*
	[Teardown]	CLI:Cancel	Raw

*** Keyword ***
SUITE:Teardown
	CLI:Delete All Devices
	CLI:Close Connection

SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/devices/
	CLI:Delete All Devices
	${DEVICES_LIST}=	CLI:Get Devices List
	Set Suite Variable	${DEVICES_LIST}
	${HAS_DEVICES}=	Run Keyword And Return Status	Should Not Be Empty	${DEVICES_LIST}
	Set Suite Variable	${HAS_DEVICES}
	${HAS_SERIAL}=	CLI:Has Serial Ports
	Set Suite Variable	${HAS_SERIAL}
	${SERIAL_DEVICES}=	CLI:Get Serial Devices List
	Set Suite Variable	${SERIAL_DEVICES}

SUITE:Show Str N Times
	[Arguments]	${STR_TO_BE_CONTAINED}	${N}
	${OUTPUT}=	CLI:Show
	${LINES}=	Split To Lines	${OUTPUT}
	${QT_DEVICES_EDITED}=	Set Variable	0
	FOR	${LINE}	IN	@{LINES}
		${HAS_STR}=	Run Keyword And Return Status	Should Contain	${LINE}	${STR_TO_BE_CONTAINED}
		${QT_DEVICES_EDITED_INC}=	Evaluate	${QT_DEVICES_EDITED}+1
		${QT_DEVICES_EDITED}=	Set Variable If	${HAS_STR}	${QT_DEVICES_EDITED_INC}	${QT_DEVICES_EDITED}
	END
	Should Be Equal	'${QT_DEVICES_EDITED}'	'${N}'

SUITE:Add devices
	[Arguments]	${DEVICE1}	${DEVICE2}
	CLI:Delete If Exists Confirm	${DEVICE1}	${DEVICE2}
	CLI:Add Device	${DEVICE1}	device_console	${HOST}
	CLI:Add Device	${DEVICE2}	device_console	${HOST}

SUITE:Add Multiple Devices
	FOR	${DEVICE}	IN	@{TESTING_DEVICES}
		CLI:Add Device	${DEVICE}	device_console
	END

SUITE:Edit Multiple Devices
	[Arguments]	${DEVICES_LIST}	${ARGUMENTS}
	CLI:Enter Path	/settings/devices/
	${OUTPUT}=	CLI:Edit	--regexp ${ARGUMENTS}
	Should Contain	${OUTPUT}	{devices}
	${OUTPUT}=	CLI:Show
	FOR	${DEVICE}	IN	@{DEVICES_LIST}
		Should Match Regexp	${OUTPUT}	selected items: .*${DEVICE}
	END
	CLI:Set	data_logging=yes
	CLI:Commit

	FOR	${DEVICE}	IN	@{DEVICES_LIST}
		${OUTPUT}=	CLI:Show	/settings/devices/${DEVICE}/logging
		Should Contain	${OUTPUT}	data_logging = yes
	END
	${OUTPUT}=	CLI:Edit	--regexp ${ARGUMENTS}
	Should Contain	${OUTPUT}	{devices}
	${OUTPUT}=	CLI:Show
	FOR	${DEVICE}	IN	@{DEVICES_LIST}
		Should Match Regexp	${OUTPUT}	selected items: .*${DEVICE}
	END
	CLI:Set	data_logging=no
	CLI:Commit

	FOR	${DEVICE}	IN	@{DEVICES_LIST}
		${OUTPUT}=	CLI:Show	/settings/devices/${DEVICE}/logging
		Should Contain	${OUTPUT}	data_logging = no
	END