*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Clone Devices... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EXPIRED_LICENSE}	${FIFTY_DEVICES_ACCESS_LICENSE_EXPIRED}

*** Test Cases ***
Test clone device with duplicate name
	CLI:Enter Path	/settings/devices/
	CLI:Write	clone device_clone
	CLI:Set	number_of_clones=1
	CLI:Set	ip_address=127.0.0.1
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	name	device_clone	Error: Device name already exists.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	name	device_clone	Error: name: Device name already exists.	yes
	[Teardown]	CLI:Cancel	Raw

Test clone device with exceeded licenses
	CLI:Enter Path	/settings/devices/
	CLI:Write	clone device_clone
	CLI:Set	name=clone
	Run Keyword If	'${NGVERSION}' >= '4.2'		CLI:Test Set Field Invalid Options	number_of_clones	${NUM_LICENSES}	Error: number_of_clones: No license available.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	number_of_clones	${NUM_LICENSES}	Error: No licenses available.	yes
	[Teardown]	CLI:Cancel	Raw

Test clone device with expired licenses
	[Setup]	CLI:Add License Key	${EXPIRED_LICENSE}
	CLI:Enter Path	/settings/devices/
	CLI:Write	clone device_clone
	CLI:Set	name=clone
	Run Keyword If	'${NGVERSION}' >= '4.2'		CLI:Test Set Field Invalid Options	number_of_clones	${NUM_LICENSES}	Error: number_of_clones: No license available.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	number_of_clones	${NUM_LICENSES}	Error: No licenses available.	yes
	[Teardown]	CLI:Cancel	Raw

Test clone device
	CLI:Enter Path	/settings/devices/
	@{DEVICELIST}=	CLI:Get Devices List	all	${PROTECTED_DEVICE_TYPES}
	CLI:Delete Devices	@{DEVICELIST}
	CLI:Add Device	device_clone	device_console	127.0.0.1	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}
	CLI:Enter Path	/settings/devices/
	CLI:Write	clone device_clone
	CLI:Set	name=clone
	CLI:Set	number_of_clones=1
	CLI:Set	increment_ip_address_on_every_cloned_device=yes
	CLI:Commit
	CLI:Test Ls Command	clone
	CLI:Test Show Command Regexp	\\s+clone\\s+127.0.0.2\\s+device_console
	CLI:Delete Device	device_clone,clone
	[Teardown]	CLI:Cancel	Raw

*** Keyword ***
SUITE:Setup
	CLI:Open
	CLI:Delete All Devices
	CLI:Delete All License Keys Installed
	${NUM_LICENSES}=	CLI:Get Number Of Available Access License Keys Installed
	Set Suite Variable	${NUM_LICENSES}
	CLI:Add Device	device_clone	device_console	127.0.0.1	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}

SUITE:Teardown
	CLI:Delete All Devices
	CLI:Delete All License Keys Installed
	CLI:Close Connection