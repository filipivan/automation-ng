*** Settings ***
Resource	../../init.robot
Documentation	Tests for netflowd. Create a device, putting it as netflow and on-demand. 
...	The status should be: unknown or connected.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	NEED-REVIEW

Suite Setup	Create All Devices
Suite Teardown	Delete All Devices And Close Connection

*** Variables ***
${NM_NF_IPV4_VALID}	dev_ipv4_ip_valid
${IP_NF_IPV4_VALID}	${IP_NETFLOWD_IPV4_VALID}

${NM_NF_IPV6_VALID}	dev_ipv6_ip_valid 
${IP_NF_IPV6_VALID}	${IP_NETFLOWD_IPV6_VALID}

${NM_NF_IPV4_INVALID}	dev_ipv4_ip_invalid
${IP_NF_IPV4_INVALID}	${IP_NETFLOWD_IPV4_INVALID}

${NM_NF_IPV6_INVALID}	dev_ipv6_ip_invalid 
${IP_NF_IPV6_INVALID}	${IP_NETFLOWD_IPV6_INVALID}

${AL_HOST_IPV4_VALID}	d4va
${NM_HOST_IPV4_VALID}	Ipv4IpValid 
${IP_HOST_IPV4_VALID}	${IP_NETFLOWD_DOMAIN_IPV4}

${AL_HOST_IPV6_VALID}	d6va
${NM_HOST_IPV6_VALID}	Ipv6IpValid
${IP_HOST_IPV6_VALID}	${IP_NETFLOWD_DOMAIN_IPV6}

${AL_HOST_INVALID}	hinv
${NM_HOST_INVALID}	IpInvalid
${IP_HOST_INVALID}	${IP_NETFLOWD_DOMAIN_INVALID}

${DEV_TYPE}	device_console

${TIME_OUT}	240s 

*** Test Cases ***
Test Ping Ipv4 Valid IP 
	Run Keyword Unless	${EXECUTE_NETFLOWD_TESTS}	Skip	Netflowd tests are disabled
	Run Ping	${NM_NF_IPV4_VALID}	Connected 

Test Ping Ipv6 Valid IP 
	Run Keyword Unless	${EXECUTE_NETFLOWD_TESTS}	Skip	Netflowd tests are disabled
	Run Ping	${NM_NF_IPV6_VALID}	Connected 

Test Ping Ipv4 Invalid IP 
	Run Keyword Unless	${EXECUTE_NETFLOWD_TESTS}	Skip	Netflowd tests are disabled
	Run Ping	${NM_NF_IPV4_INVALID}	Unknown 

Test Ping Ipv6 Invalid IP 
	Run Keyword Unless	${EXECUTE_NETFLOWD_TESTS}	Skip	Netflowd tests are disabled
	Run Ping	${NM_NF_IPV6_INVALID}	Unknown 

Teste Ping Ipv4 Domain Name
	Run Keyword Unless	${EXECUTE_NETFLOWD_TESTS}	Skip	Netflowd tests are disabled
	Run Ping	${NM_HOST_IPV4_VALID}	Connected

Teste Ping Ipv6 Domain Name
	Run Keyword Unless	${EXECUTE_NETFLOWD_TESTS}	Skip	Netflowd tests are disabled
	Run Ping	${NM_HOST_IPV6_VALID}	Connected

Teste Ping Invalid Domain Name
	Run Keyword Unless	${EXECUTE_NETFLOWD_TESTS}	Skip	Netflowd tests are disabled
	Run Ping	${NM_HOST_INVALID}	Unknown

*** Keywords ***
Run Ping
	[Arguments]	${NM_DEV}	${STATUS}
	Log To Console	${NM_DEV}
	CLI:Enter Path	/settings/devices/${NM_DEV}/access
	CLI:Write	set enable_device_state_detection_based_in_net_flow=yes
	CLI:Commit
	#	Attention! To show the updated status, it is necessary to first enter the folder 
	#	ACCESS execute [CLI:show] and then enter the device and run [CLI:Test Show command status]
	Sleep	${CLI_DEFAULT_TIMEOUT}
	CLI:Enter Path	/access
	${OUTPUT}=	CLI:Show 
	CLI:Enter Path	/access/${NM_DEV}
	${OUTPUT}=	CLI:Test Show Command	status
	Should Contain	${OUTPUT}	${STATUS}

Create All Devices
	Run Keyword Unless	${EXECUTE_NETFLOWD_TESTS}	Skip	Netflowd tests are disabled

	CLI:Open	timeout=${TIME_OUT}

	Create Device	${NM_NF_IPV4_VALID}	${IP_NF_IPV4_VALID}
	Create Device	${NM_NF_IPV6_VALID}	${IP_NF_IPV6_VALID}
	Create Device	${NM_NF_IPV4_INVALID}	${IP_NETFLOWD_IPV4_INVALID}
	Create Device	${NM_NF_IPV6_INVALID}	${IP_NETFLOWD_IPV6_INVALID}
	Create Device	${NM_HOST_IPV4_VALID}	${NM_HOST_IPV4_VALID}
	Create Device	${NM_HOST_IPV6_VALID}	${NM_HOST_IPV6_VALID}
	Create Device	${NM_HOST_INVALID}	${NM_HOST_INVALID}

	Add Host	${AL_HOST_IPV4_VALID}	${NM_HOST_IPV4_VALID}	${IP_HOST_IPV4_VALID}
	Add Host	${AL_HOST_IPV6_VALID}	${NM_HOST_IPV6_VALID}	${IP_HOST_IPV6_VALID}
	Add Host	${AL_HOST_INVALID}	${NM_HOST_INVALID}	${IP_HOST_INVALID}

Create Device
	[Arguments]	${NM_DEV}	${IP}
	CLI:Add Device	${NM_DEV}	${DEV_TYPE}	${IP}	root	${ROOT_PASSWORD}	on-demand
	CLI:Commit

Delete All Devices And Close Connection
	Run Keyword Unless	${EXECUTE_NETFLOWD_TESTS}	Skip	Netflowd tests are disabled

	CLI:Delete Devices	${NM_NF_IPV4_VALID}	${NM_NF_IPV6_VALID}	${NM_NF_IPV4_INVALID}	${NM_NF_IPV6_INVALID}	${NM_HOST_IPV4_VALID}	${NM_HOST_IPV6_VALID}	${NM_HOST_INVALID}

	Delete Host	${IP_HOST_IPV4_VALID}
	Delete Host	${IP_HOST_IPV6_VALID}
	Delete Host	${IP_HOST_INVALID}

	CLI:Close Connection

Add Host
	[Arguments]	${ALIAS}	${NM_DEV}	${IP}

	CLI:Enter Path	/settings/hosts

	${OUTPUT}=	CLI:Show
	${LINES}=	Split To Lines	${OUTPUT}	2	-1
	FOR	${LINE}	IN	@{LINES}
		${RESULT}=	Run Keyword And Return Status	Should Contain	${LINE}	${IP}
		Log To Console	${IP} ${RESULT}
		Run Keyword If	${RESULT}	CLI:Delete	${IP}
	END
	CLI:Add
	CLI:Set Field	alias	${ALIAS}
	CLI:Set Field	hostname	${NM_DEV}
	CLI:Set Field	ip_address	${IP}
	CLI:Commit

Delete Host
	[Arguments]	${IP}
	CLI:Enter Path	/settings/hosts
	CLI:Delete	${IP}
	CLI:Commit
