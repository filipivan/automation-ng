*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Clone Devices... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test show command
	CLI:Enter Path	/settings/devices/
	CLI:Write	clone device_clone
	CLI:Test Show Command	clone from: device_clone	increment_ip_address_on_every_cloned_device	ip_address = 127.0.0.2
	...	name	number_of_clones = 1	mode = on-demand
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test available fields after Clone command
	CLI:Enter Path	/settings/devices/
	CLI:Write	clone device_clone
	CLI:Test Set Available Fields	increment_ip_address_on_every_cloned_device	ip_address	name	number_of_clones	mode
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=name
	CLI:Enter Path	/settings/devices/
	CLI:Write	clone device_clone
	${OUTPUT}=	CLI:Commit	Raw
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	Error: name: Validation error.
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${OUTPUT}	Error: Validation error.

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	name	${POINTS}	Error: name: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	name	${POINTS}	Error: Validation error.	yes

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	name	${EXCEEDED}	Error: name: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	name	${EXCEEDED}	Error: Validation error.	yes
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=number_of_clones
	CLI:Enter Path	/settings/devices/
	CLI:Write	clone device_clone
	CLI:Set	name=clone
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	number_of_clones	${WORD}	Error: number_of_clones: Must be a number between 1 and 5000.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	number_of_clones	${WORD}	Error: Must be a number between 1 and 999.	yes
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=ip_address
	CLI:Enter Path	/settings/devices/
	CLI:Write	clone device_clone
	CLI:Set	name=clone
	CLI:Set	number_of_clones=1
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ip_address	${ONE_POINTS}	Error: ip_address: Must be a valid IP Address	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_address	${ONE_POINTS}	Error: Must be a valid IP Address	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ip_address	${EXCEEDED}	Error: ip_address: Must be a valid IP Address	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_address	${EXCEEDED}	Error: Must be a valid IP Address	yes
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=mode
	CLI:Enter Path	/settings/devices/
	CLI:Write	clone device_clone
	CLI:Set	name=clone
	CLI:Set	number_of_clones=1
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	mode	${EMPTY}	Error: Missing value for parameter: mode
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	mode	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: mode
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	mode	${EMPTY}	Error: Missing value: mode
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	mode	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}
	CLI:Test Set Field Options	mode	disabled	enabled	on-demand
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=mode
	CLI:Enter Path	/settings/devices/
	CLI:Write	clone device_clone
	CLI:Test Set Field Options	mode	disabled	enabled	on-demand
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=increment_ip_address_on_every_cloned_device
	CLI:Enter Path	/settings/devices/
	CLI:Write	clone device_clone
	CLI:Set	name=clone
	CLI:Set	number_of_clones=1
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	increment_ip_address_on_every_cloned_device	${EMPTY}	Error: Missing value for parameter: increment_ip_address_on_every_cloned_device
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	increment_ip_address_on_every_cloned_device	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: increment_ip_address_on_every_cloned_device
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	increment_ip_address_on_every_cloned_device	${EMPTY}	Error: Missing value: increment_ip_address_on_every_cloned_device
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	increment_ip_address_on_every_cloned_device	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	CLI:Test Set Field Options	increment_ip_address_on_every_cloned_device	no	yes
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test valid values for field=increment_ip_address_on_every_cloned_device
	CLI:Enter Path	/settings/devices/
	CLI:Write	clone device_clone
	CLI:Test Set Field Options	increment_ip_address_on_every_cloned_device	no	yes
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

*** Keyword ***
SUITE:Setup
	CLI:Open
	CLI:Delete All Devices
	CLI:Add Device	device_clone	device_console	127.0.0.1	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}

SUITE:Teardown
	CLI:Delete All Devices
	CLI:Close Connection