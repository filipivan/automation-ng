*** Settings ***
Resource	../../../init.robot
Documentation	Tests Settings > Devices > Add device type pdu_pci and monitoring template validations... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI		EXCLUDEIN3_2	EXCLUDEIN4_0
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE: Setup
Suite Teardown	SUITE: Teardown

*** Variables ***
${DEVICE_NAME}	CPI_PDU
${DEVICE_TYPE}	pdu_cpi
${DEVICE_IP}	192.168.2.243

*** Keywords ***
SUITE: Setup
	CLI:Open
	CLI:Enter Path	/settings/devices/
	CLI:Show
#	CLI:Delete If Exists	${DEVICE_NAME}
	CLI:Add Device	${DEVICE_NAME}	${DEVICE_TYPE}	${DEVICE_IP}
	${OUTPUT}=	CLI:Test Show Command
	Should Contain	${OUTPUT}	${DEVICE_NAME}

SUITE: Teardown
	CLI:Delete Device  ${DEVICE_NAME}
	CLI:Close Connection

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/devices
	CLI:Test Available Commands	add	clone	event_system_audit	hostname	quit	save_settings	show_settings
	...	apply_settings	 commit	event_system_clear	ls	reboot	set	shutdown	whoami	cd	delete	exit	page
	...	shell	software_upgrade	change_password	edit	factory_settings	pwd	revert	show	system_certificate
	...	system_config_check	rename

Test visible fields for show command before set monitoring
	CLI:Enter Path	/settings/devices
	CLI:Test Show Command	name	connected through	type	access	monitoring
	...	${DEVICE_NAME}	${DEVICE_IP}	${DEVICE_TYPE}	enabled	disabled

Test validate Device::Management::Monitoring SNMP pdu_cpi_outlet template
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/management
	CLI:Test Set Field Options	monitoring_snmp	yes	no
	CLI:Set Field	monitoring_snmp	yes

	CLI:Test Set Validate Normal Fields	monitoring_snmp_template	pdu_cpi_outlet
	...	acu_tripplite	pdu_apc_rpdu2	pdu_cpi_outlet	pdu_enconnex	pdu_mph2	pdu_mph2_outlet
	...	pdu_rittal	pdu_servertech_outlet_sentry3	pdu_servertech_sentry4	ups_methode	pdu_apc_rpdu
	...	pdu_eaton	pdu_enconnex_outlet	pdu_mph2_inlet	pdu_pm3000	pdu_raritan_inlet
	...	pdu_servertech_sentry3	ups_eaton	ups_rfc1628	pdu_pm3000_outlet	pdu_baytech

	CLI:Set Field	monitoring_snmp_template	pdu_cpi_outlet
	CLI:Commit

	[Teardown]	CLI:Revert	Raw

Test visible fields for show command after set monitoring
	CLI:Enter Path	/settings/devices
	${OUTPUT}=   CLI:Show
#   ${CHECK}=	Run Keyword And Return Status	Should Not Contain	${OUTPUT}   usb_sensor
	${CHECK}=	Run Keyword And Return Status	Should Contain	${OUTPUT}   usb_sensor
    Run Keyword If  ${CHECK}  Wait Until Keyword Succeeds	1m	1s	CLI:Test Show Command	name	connected through	type	access	monitoring  ${DEVICE_NAME}	${DEVICE_IP}	${DEVICE_TYPE}	enabled	enabled
    ...     ELSE    Wait Until Keyword Succeeds	1m	1s	CLI:Test Show Command	name	connected through	type	access	monitoring  ${DEVICE_NAME}	${DEVICE_IP}	${DEVICE_TYPE}	enabled	unlicensed
