*** Settings ***
Resource	../../../init.robot
Documentation	Tests Settings > Devices > device > Management...session scripts STOP through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
Default Tags	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${test_device}	test
${username}		admin
${password}		admin
${mode}		on-demand
${session_script}	NG_CLI_set_sess_start_stop_timeout.sh
${default_idle_timeout}	300
${new_idle_timeout}	12000

*** Test Cases ***
Test case to Add dummy device and check ssh
	CLI:Add Device	${test_device}	device_console	${HOSTPEER}	${username}		${password}		${mode}
	CLI:Enter Path	/settings/devices/
	CLI:Write	edit ${test_device}
	CLI:Set	enable_device_state_detection_based_on_network_traffic=yes
	CLI:Commit
	SUITE:Check ssh to test device
	CLI:Open

Test case to create session script
	CLI:Connect As Root
	CLI:Enter Path	/etc/scripts/access/
	Write	cp Session_sample.sh ${session_script}
	CLI:Write	sed -i 's/^exit 0//' ${session_script}
	Write	cat >> ${session_script}
	Write	cli <<CMD
	Write	cd /settings/system_preferences/
	Write	set idle_timeout=12345
	Write	commit
	Write	CMD
	Write	cli show /settings/system_preferences/ idle_timeout
	Write	cli <<CMD
	Write	cd /settings/system_preferences/
	Write	set idle_timeout=${new_idle_timeout}
	Write	commit
	Write	CMD
	Write	cli show /settings/system_preferences/ idle_timeout
	Write	exit 0
	CLI:Close Connection
	CLI:Open

Test case to Add session script to device to Run on Device Down and check script executed on disabling device
	CLI:Enter Path	/settings/devices/test/management
	CLI:Set	on_device_down=${session_script}
	CLI:Commit
	SUITE:Disable device
	Sleep	30s
	SUITE:check Script executed

*** Keyword ***
SUITE:Setup
	CLI:Open
	CLI:Delete All Devices

SUITE:Teardown
	CLI:Open
	CLI:Delete All Devices
	SUITE:set idle timeout value to default
	SUITE:Remove session script
	CLI:Close Connection

SUITE:Check ssh to test device
	CLI:Connect As Root
	${OUTPUT}=	Write	ssh admin@${HOSTPEER}
	Write	${password}
	Should Not Contain	${OUTPUT}	error
	CLI:Close Connection

SUITE:set idle timeout value to default
	CLI:Enter Path	/settings/system_preferences/
	CLI:Set	idle_timeout=300
	CLI:Commit

SUITE:check Script executed
	CLI:Enter Path	/settings/system_preferences/
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	idle_timeout = ${new_idle_timeout}

SUITE:Disable device
	CLI:Enter Path	/settings/devices
	CLI:Write	edit ${test_device}
	CLI:Set	mode=disabled
	CLI:Commit

SUITE:Remove session script
	CLI:Connect As Root
	CLI:Enter Path	/etc/scripts/access/
	Write	rm ${session_script}