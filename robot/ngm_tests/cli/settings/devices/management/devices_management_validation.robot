*** Settings ***
Resource	../../../init.robot
Documentation	Tests Settings > Devices > device > Management... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
@{ALL_VALUES}	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}
${DEVICE}	test-dev.ice
${CONSOLE_SERVER_DEVICE}	test-con.sole-server

${GENERIC_USERNAME}	generic_username
${GENERIC_PASSWORD}	generic_password
${SSH_TELNET_USERNAME}	sshtelnet_username
${SSH_TELNET_PASSWORD}	sshtelnet_password
${IPMI_USERNAME}	ipmi_username
${IPMI_PASSWORD}	ipmi_password

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	CLI:Test Available Commands	apply_settings	hostname	shell	cd	ls	show	change_password	pwd	show_settings
	...	commit	quit	shutdown	event_system_audit	reboot	software_upgrade	event_system_clear	revert
	...	system_certificate	exit	save_settings	system_config_check	factory_settings	set	whoami

Test visible fields for show command
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Show Command	name: ${DEVICE}	ssh_and_telnet	credential	username	password
	...	ipmi	monitoring_ipmi	monitoring_nominal	on_session_start	on_session_stop	on_device_up	on_device_down
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Show Command	username	password
	...	monitoring_ipmi	monitoring_nominal	on_session_start	on_session_stop	on_device_up	on_device_down

	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Enter Path	/settings/devices/${CONSOLE_SERVER_DEVICE}/management
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Show Command	purge_disabled_end_point_ports

Test available fields to set
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	CLI:Test Set Available Fields	credential	on_device_down	password	ipmi	on_device_up	ssh_and_telnet
	...	monitoring_ipmi	on_session_start	username	monitoring_nominal	on_session_stop

	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Enter Path	/settings/devices/${CONSOLE_SERVER_DEVICE}/management
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Set Available Fields	purge_disabled_end_point_ports

Test available fields to set after enabling purge ports
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/devices/${CONSOLE_SERVER_DEVICE}/management
	CLI:Set	purge_disabled_end_point_ports=yes
	CLI:Test Set Available Fields	action

Test possible values when setting fields
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	IF	${NGVERSION} >= 4.2
		CLI:Test Set Field Options	ssh_and_telnet	yes	no
		CLI:Test Set Field Options	ipmi	no	yes
	END
	CLI:Test Set Field Options	monitoring_ipmi	no	yes
	CLI:Test Field Options	monitoring_nominal	no	yes

Test invalid values for fields
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	${COMMANDS}=	Create List	credential	ipmi
	...	monitoring_ipmi	monitoring_nominal
	Run Keyword If	${NGVERSION} >= 4.2	Append To List	${COMMANDS}	ssh_and_telnet
	FOR	${COMMAND}	IN	@{COMMANDS}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	a	Error: Invalid value: a for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value: ${COMMAND}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${COMMAND}	a	Error: Invalid value: a
	END
	[Teardown]	CLI:Revert	Raw

Test invalid values for field=monitoring_ipmi_interval
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	CLI:Set Field	monitoring_ipmi	yes
	CLI:Test Show Command	monitoring_ipmi = yes	monitoring_ipmi_template	monitoring_ipmi_interval = 120
	CLI:Test Set Field Invalid Options	monitoring_ipmi_interval	${EMPTY}	Error: monitoring_ipmi_interval: Validation error.
	CLI:Test Set Field Invalid Options	monitoring_ipmi_interval	0	Error: monitoring_ipmi_interval: Validation error.
	CLI:Test Set Field Invalid Options	monitoring_ipmi_interval	~!@	Error: monitoring_ipmi_interval: Validation error.
	CLI:Test Set Field Invalid Options	monitoring_ipmi_interval	${EXCEEDED}	Error: monitoring_ipmi_interval: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert	Raw

Test invalid values for field=monitoring_nominal_value
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	CLI:Test Set Field Invalid Options	monitoring_nominal	yes	Error: monitoring_nominal_value: Validation error.
	CLI:Write	set monitoring_nominal=yes monitoring_nominal_value=1
	CLI:Test Show Command	monitoring_nominal = yes	monitoring_nominal_name	monitoring_nominal_type = power	monitoring_nominal_value = 1	monitoring_nominal_interval = 120
	CLI:Test Set Field Invalid Options	monitoring_nominal_value	~!@	Error: monitoring_nominal_value: Validation error.
	CLI:Test Set Field Invalid Options	monitoring_nominal_value	${EXCEEDED}	Error: monitoring_nominal_value: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert	Raw

Test invalid values for field=monitoring_nominal_interval
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	CLI:Write	set monitoring_nominal=yes monitoring_nominal_value=1
	CLI:Test Set Field Invalid Options	monitoring_nominal_interval	${EMPTY}	Error: monitoring_nominal_interval: Validation error.
	CLI:Test Set Field Invalid Options	monitoring_nominal_interval	0	Error: monitoring_nominal_interval: Validation error.
	CLI:Test Set Field Invalid Options	monitoring_nominal_interval	~!@	Error: monitoring_nominal_interval: Validation error.
	CLI:Test Set Field Invalid Options	monitoring_nominal_interval	${EXCEEDED}	Error: monitoring_nominal_interval: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert	Raw

Test invalid values for field=monitoring_nominal_type
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	CLI:Write	set monitoring_nominal=yes monitoring_nominal_value=1
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	monitoring_nominal_type	${EMPTY}	Error: Missing value for parameter: monitoring_nominal_type
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	monitoring_nominal_type	a	Error: Invalid value: a for parameter: monitoring_nominal_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	monitoring_nominal_type	${EMPTY}	Error: Missing value: monitoring_nominal_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	monitoring_nominal_type	a	Error: Invalid value: a
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Options	monitoring_nominal_type	apparent_power	fanspeed	percent	temperature
	...	counter	frequency	power	timeleft	current	humidity	power_factor	voltage
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Options	monitoring_nominal_type	apparent_power	counter	current	fan_speed
	...	frequency	humidity	percent	power	power_factor	temperature	time_left	voltage
	[Teardown]	CLI:Revert	Raw

Test name field
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	CLI:Test Validate Unmodifiable Field	name	${DEVICE}
	[Teardown]	CLI:Revert	Raw

Test ssh_and_telnet field
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	ssh_and_telnet	yes	no
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	ssh_and_telnet	yes	no

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ssh_and_telnet	${EMPTY}	Error: Missing value for parameter: ssh_and_telnet
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ssh_and_telnet	${EMPTY}	Error: Missing value: ssh_and_telnet

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ssh_and_telnet	${WORD}	Error: Invalid value: ${WORD} for parameter: ssh_and_telnet
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ssh_and_telnet	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ssh_and_telnet	${POINTS}	Error: Invalid value: ${POINTS} for parameter: ssh_and_telnet
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ssh_and_telnet	${POINTS}	Error: Invalid value: ${POINTS}

Test credential field
	CLI:Enter Path	/settings/devices/${DEVICE}/access
	CLI:Set	username=${GENERIC_USERNAME} password=${GENERIC_PASSWORD}
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	credential	use_same_as_access	use_specific
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	credential	use_same_as_access	use_specific

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	credential	${EMPTY}	Error: Missing value for parameter: credential
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	credential	${EMPTY}	Error: Missing value: credential

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	credential	${WORD}	Error: Invalid value: ${WORD} for parameter: credential
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	credential	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	credential	${POINTS}	Error: Invalid value: ${POINTS} for parameter: credential
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	credential	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Revert	Raw

Test username with credential=use_specific field
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	CLI:Write	set ssh_and_telnet=yes
	CLI:Write	set credential=use_specific
	CLI:Test Set Validate Invalid Options	username	${EMPTY}
	CLI:Test Set Validate Invalid Options	username	${WORD}
	CLI:Test Set Validate Invalid Options	username	${NUMBER}
	CLI:Test Set Validate Invalid Options	username	${POINTS}
	CLI:Test Set Validate Invalid Options	username	${ONE_WORD}
	CLI:Test Set Validate Invalid Options	username	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	username	${ONE_POINTS}
	Run Keyword If	'${NGVERSION}' == '4.2' or '${NGVERSION}' == '5.0'	CLI:Test Set Validate Invalid Options	username	${EXCEEDED}	Error: name: Exceeded the maximum size for this field.
	...	ELSE	CLI:Test Set Validate Invalid Options	username	${EXCEEDED}	Error: username: Exceeded the maximum size for this field.

	[Teardown]	CLI:Revert	Raw

Test ipmi field
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	ipmi	yes	no
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	ipmi	yes	no

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ipmi	${EMPTY}	Error: Missing value for parameter: ipmi
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ipmi	${EMPTY}	Error: Missing value: ipmi

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ipmi	${WORD}	Error: Invalid value: ${WORD} for parameter: ipmi
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ipmi	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	ipmi	${POINTS}	Error: Invalid value: ${POINTS} for parameter: ipmi
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	ipmi	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Revert	Raw

Test credential field IPMI
	CLI:Enter Path	/settings/devices/${DEVICE}/access
	CLI:Set	username=${GENERIC_USERNAME} password=${GENERIC_PASSWORD}
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	credential	use_same_as_access	use_specific
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	credential	use_same_as_access	use_specific

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	credential	${EMPTY}	Error: Missing value for parameter: credential
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	credential	${EMPTY}	Error: Missing value: credential

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	credential	${WORD}	Error: Invalid value: ${WORD} for parameter: credential
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	credential	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	credential	${POINTS}	Error: Invalid value: ${POINTS} for parameter: credential
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	credential	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Revert	Raw

Test username field with credential=use_specific
	[Tags]	NON-CRITICAL
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	CLI:Write	set ipmi=yes
	CLI:Write	set credential=use_specific
	CLI:Test Set Validate Invalid Options	username	${EMPTY}
	CLI:Test Set Validate Invalid Options	username	${WORD}
	CLI:Test Set Validate Invalid Options	username	${NUMBER}
	CLI:Test Set Validate Invalid Options	username	${POINTS}
	CLI:Test Set Validate Invalid Options	username	${ONE_WORD}
	CLI:Test Set Validate Invalid Options	username	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	username	${ONE_POINTS}
	IF	${NGVERSION} == 5.0 or ${NGVERSION} == 4.2
		CLI:Test Set Validate Invalid Options	username	${EXCEEDED}
		...	Error: spmanager.server.test-dev%2Eice.ipmilogin: Exceeded the maximum size for this field.
	ELSE
		CLI:Test Set Validate Invalid Options	username	${EXCEEDED}	Error: username: Exceeded the maximum size for this field.
	END
	[Teardown]	CLI:Revert	Raw

Test monitoring_ipmi field
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	monitoring_ipmi	yes	no
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	monitoring_ipmi	yes	no

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	monitoring_ipmi	${EMPTY}	Error: Missing value for parameter: monitoring_ipmi
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	monitoring_ipmi	${EMPTY}	Error: Missing value: monitoring_ipmi

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	monitoring_ipmi	${WORD}	Error: Invalid value: ${WORD} for parameter: monitoring_ipmi
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	monitoring_ipmi	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	monitoring_ipmi	${POINTS}	Error: Invalid value: ${POINTS} for parameter: monitoring_ipmi
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	monitoring_ipmi	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Revert	Raw

Test monitoring_ipmi_template field
	[Tags]	NON-CRITICAL
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	CLI:Write	set monitoring_ipmi=yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	monitoring_ipmi_template	discover	${EMPTY}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	monitoring_ipmi_template	${EMPTY}
	...	Error: Missing value: monitoring_ipmi_template
	#no error message was displayed in 5.4- (BUG_8065) will not befixed for this versions
	Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	monitoring_ipmi_template	${WORD}	Error: Invalid value: ${WORD} for parameter: monitoring_ipmi_template
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	monitoring_ipmi_template	${WORD}	Error: Invalid value: ${WORD}
	#no error message was displayed in 5.4- (BUG_8065) will not befixed for this versions
	Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Test Set Validate Invalid Options	monitoring_ipmi_template	${POINTS}	Error: Invalid value: ${POINTS} for parameter: monitoring_ipmi_template
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	monitoring_ipmi_template	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Revert	Raw

Test monitoring_ipmi_interval field
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	CLI:Write	set monitoring_ipmi=yes
	CLI:Test Set Validate Invalid Options	monitoring_ipmi_interval	${EMPTY}	Error: monitoring_ipmi_interval: Validation error.
	CLI:Test Set Validate Invalid Options	monitoring_ipmi_interval	${WORD}	Error: monitoring_ipmi_interval: Validation error.
	CLI:Test Set Validate Invalid Options	monitoring_ipmi_interval	${NUMBER}
	CLI:Test Set Validate Invalid Options	monitoring_ipmi_interval	${POINTS}	Error: monitoring_ipmi_interval: Validation error.
	CLI:Test Set Validate Invalid Options	monitoring_ipmi_interval	${ONE_WORD}	Error: monitoring_ipmi_interval: Validation error.
	CLI:Test Set Validate Invalid Options	monitoring_ipmi_interval	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	monitoring_ipmi_interval	${ONE_POINTS}	Error: monitoring_ipmi_interval: Validation error.
	CLI:Test Set Validate Invalid Options	monitoring_ipmi_interval	${EXCEEDED}	Error: monitoring_ipmi_interval: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert	Raw

Test monitoring_nominal field
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	CLI:Write	set monitoring_nominal=yes monitoring_nominal_value=1
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	monitoring_nominal	yes	no
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	monitoring_nominal	yes	no

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	monitoring_nominal	${EMPTY}	Error: Missing value for parameter: monitoring_nominal
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	monitoring_nominal	${EMPTY}	Error: Missing value: monitoring_nominal

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	monitoring_nominal	${WORD}	Error: Invalid value: ${WORD} for parameter: monitoring_nominal
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	monitoring_nominal	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	monitoring_nominal	${POINTS}	Error: Invalid value: ${POINTS} for parameter: monitoring_nominal
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	monitoring_nominal	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Revert	Raw

Test monitoring_nominal_name field
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	CLI:Write	set monitoring_nominal=yes monitoring_nominal_value=1
	CLI:Test Set Validate Invalid Options	monitoring_nominal_name	${EMPTY}
	CLI:Test Set Validate Invalid Options	monitoring_nominal_name	${WORD}
	CLI:Test Set Validate Invalid Options	monitoring_nominal_name	${NUMBER}
	CLI:Test Set Validate Invalid Options	monitoring_nominal_name	${POINTS}
	CLI:Test Set Validate Invalid Options	monitoring_nominal_name	${ONE_WORD}
	CLI:Test Set Validate Invalid Options	monitoring_nominal_name	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	monitoring_nominal_name	${ONE_POINTS}
	CLI:Test Set Validate Invalid Options	monitoring_nominal_name	${EXCEEDED}	Error: monitoring_nominal_name: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert	Raw

Test monitoring_nominal_type field
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	CLI:Write	set monitoring_nominal=yes monitoring_nominal_value=1
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	monitoring_nominal_type	apparent_power	fanspeed	percent	temperature
	...	counter	frequency	power	timeleft	current	humidity	power_factor	voltage
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	monitoring_nominal_type	apparent_power	fan_speed	percent	temperature
	...	counter	frequency	power	time_left	current	humidity	power_factor	voltage

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	monitoring_nominal_type	${EMPTY}	Error: Missing value for parameter: monitoring_nominal_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	monitoring_nominal_type	${EMPTY}	Error: Missing value: monitoring_nominal_type

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	monitoring_nominal_type	${WORD}	Error: Invalid value: ${WORD} for parameter: monitoring_nominal_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	monitoring_nominal_type	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	monitoring_nominal_type	${POINTS}	Error: Invalid value: ${POINTS} for parameter: monitoring_nominal_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	monitoring_nominal_type	${POINTS}	Error: Invalid value: ${POINTS}
	[Teardown]	CLI:Revert	Raw

Test monitoring_nominal_value field
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	CLI:Write	set monitoring_nominal=yes monitoring_nominal_value=1
	CLI:Test Set Validate Invalid Options	monitoring_nominal_value	${EMPTY}	Error: monitoring_nominal_value: Validation error.
	CLI:Test Set Validate Invalid Options	monitoring_nominal_value	${WORD}	Error: monitoring_nominal_value: Validation error.
	CLI:Test Set Validate Invalid Options	monitoring_nominal_value	${NUMBER}
	CLI:Test Set Validate Invalid Options	monitoring_nominal_value	${POINTS}	Error: monitoring_nominal_value: Validation error.
	CLI:Test Set Validate Invalid Options	monitoring_nominal_value	${ONE_WORD}	Error: monitoring_nominal_value: Validation error.
	CLI:Test Set Validate Invalid Options	monitoring_nominal_value	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	monitoring_nominal_value	${ONE_POINTS}	Error: monitoring_nominal_value: Validation error.
	CLI:Test Set Validate Invalid Options	monitoring_nominal_value	${EXCEEDED}	Error: monitoring_nominal_value: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert	Raw

Test monitoring_nominal_interval field
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	CLI:Write	set monitoring_nominal=yes monitoring_nominal_value=1
	CLI:Test Set Validate Invalid Options	monitoring_nominal_interval	${EMPTY}	Error: monitoring_nominal_interval: Validation error.
	CLI:Test Set Validate Invalid Options	monitoring_nominal_interval	${WORD}	Error: monitoring_nominal_interval: Validation error.
	CLI:Test Set Validate Invalid Options	monitoring_nominal_interval	${NUMBER}
	CLI:Test Set Validate Invalid Options	monitoring_nominal_interval	${POINTS}	Error: monitoring_nominal_interval: Validation error.
	CLI:Test Set Validate Invalid Options	monitoring_nominal_interval	${ONE_WORD}	Error: monitoring_nominal_interval: Validation error.
	CLI:Test Set Validate Invalid Options	monitoring_nominal_interval	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	monitoring_nominal_interval	${ONE_POINTS}	Error: monitoring_nominal_interval: Validation error.
	CLI:Test Set Validate Invalid Options	monitoring_nominal_interval	${EXCEEDED}	Error: monitoring_nominal_interval: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert	Raw

Test valid values for field=purge_disabled_end_point_ports
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/devices/${CONSOLE_SERVER_DEVICE}/management
	CLI:Test Set Field Options	purge_disabled_end_point_ports	no	yes
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=purge_disabled_end_point_ports
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/devices/${CONSOLE_SERVER_DEVICE}/management
	CLI:Test Set Field Invalid Options	purge_disabled_end_point_ports	${EMPTY}
	...	Error: Missing value for parameter: purge_disabled_end_point_ports

	FOR	${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	purge_disabled_end_point_ports	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: purge_disabled_end_point_ports
	END
	[Teardown]	CLI:Revert	Raw

Test valid values for field=action
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/devices/${CONSOLE_SERVER_DEVICE}/management
	CLI:Set	purge_disabled_end_point_ports=yes
	CLI:Test Set Field Options	action	disable_ports	remove_ports
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=action
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/devices/${CONSOLE_SERVER_DEVICE}/management
	CLI:Set	purge_disabled_end_point_ports=yes
	CLI:Test Set Field Invalid Options	action	${EMPTY}
	...	Error: Missing value for parameter: action

	FOR	${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	action	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: action
	END
	[Teardown]	CLI:Revert	Raw

Test new alias are avaliable to set command
	[Documentation]	tests if the new username and password alias are shown as valid options to set
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	CLI:Set	ssh_and_telnet=yes ipmi=yes credential=use_specific
	CLI:Test Set Available Fields	username	password	username_ipmi	password_ipmi
	...	username_ssh_telnet	password_ssh_telnet
	[Teardown]	CLI:Cancel	Raw

Test username and password fields will change the only avaliable option
	[Documentation]	tests if the legacy behaviour still works: if only ssh/telnet is enable, it will change
	...	ssh/telnet credentials, if only ipmi is enabled, it will change ipmi credentials
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	CLI:Set	ssh_and_telnet=yes ipmi=no credential=use_specific
	CLI:Set	username=${SSH_TELNET_USERNAME} password=${SSH_TELNET_PASSWORD}
	CLI:Commit
	${SSH_TELNET_ONLY}	CLI:Show Settings	${TRUE}
	Should Contain	${SSH_TELNET_ONLY}	/settings/devices/${DEVICE}/management username=${SSH_TELNET_USERNAME}
	Should Contain	${SSH_TELNET_ONLY}	/settings/devices/${DEVICE}/management password=${SSH_TELNET_PASSWORD}

	CLI:Set	ssh_and_telnet=no ipmi=yes credential=use_specific
	CLI:Set	username=${IPMI_USERNAME} password=${IPMI_PASSWORD}
	CLI:Commit
	${IPMI_ONLY}	CLI:Show Settings	${TRUE}
	Should Contain	${IPMI_ONLY}	/settings/devices/${DEVICE}/management username=${IPMI_USERNAME}
	Should Contain	${IPMI_ONLY}	/settings/devices/${DEVICE}/management password=${IPMI_PASSWORD}
	[Teardown]	CLI:Cancel	Raw

Test username and password new aliases
	[Documentation]	tests if the new aliases work
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	CLI:Set	ssh_and_telnet=yes ipmi=yes credential=use_specific
	CLI:Set	username_ipmi=${IPMI_USERNAME} password_ipmi=${IPMI_PASSWORD}
	CLI:Set	username_ssh_telnet=${SSH_TELNET_USERNAME} password_ssh_telnet=${SSH_TELNET_PASSWORD}
	CLI:Commit
	${NEW_BEHAVIOUR}	CLI:Show Settings	${TRUE}
	Should Contain	${NEW_BEHAVIOUR}	/settings/devices/${DEVICE}/management username=${SSH_TELNET_USERNAME}
	Should Contain	${NEW_BEHAVIOUR}	/settings/devices/${DEVICE}/management password=${SSH_TELNET_PASSWORD}
	Should Contain	${NEW_BEHAVIOUR}	/settings/devices/${DEVICE}/management username=${IPMI_USERNAME}
	Should Contain	${NEW_BEHAVIOUR}	/settings/devices/${DEVICE}/management password=${IPMI_PASSWORD}

Test username and password will change only ssh/telnet if ipmi and ssh/telnet are enabled
	[Documentation]	tests if the legacy behaviour still works: if ssh/telnet and ipmi are enable,
	...	it will change ssh/telnet credentials
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
	CLI:Enter Path	/settings/devices/${DEVICE}/management
	CLI:Set	ssh_and_telnet=yes ipmi=yes credential=use_specific
	CLI:Set	username_ipmi=${IPMI_USERNAME} password_ipmi=${IPMI_PASSWORD}
	CLI:Set	username_ssh_telnet=${SSH_TELNET_USERNAME} password_ssh_telnet=${SSH_TELNET_PASSWORD}
	CLI:Commit
	CLI:Set	username=${GENERIC_USERNAME} password=${GENERIC_PASSWORD}
	CLI:Commit
	${LEGACY_BEHAVIOUR}	CLI:Show Settings	${TRUE}
	Should Contain	${LEGACY_BEHAVIOUR}	/settings/devices/${DEVICE}/management username=${GENERIC_USERNAME}
	Should Contain	${LEGACY_BEHAVIOUR}	/settings/devices/${DEVICE}/management password=${GENERIC_PASSWORD}
	Should Contain	${LEGACY_BEHAVIOUR}	/settings/devices/${DEVICE}/management username=${IPMI_USERNAME}
	Should Contain	${LEGACY_BEHAVIOUR}	/settings/devices/${DEVICE}/management password=${IPMI_PASSWORD}
	[Teardown]	CLI:Cancel	Raw

*** Keyword ***
SUITE:Setup
	CLI:Open
	CLI:Delete All Devices
	CLI:Add Device	${DEVICE}	ipmi_1.5
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Add Device	${CONSOLE_SERVER_DEVICE}	console_server_nodegrid

SUITE:Teardown
	CLI:Delete All Devices
	CLI:Close Connection