*** Settings ***
Resource	../../init.robot
Documentation	Tests search command for devices through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	NON-CRITICAL
Default Tags	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${LICENSE_KEY}	${FIVE_DEVICES_CLUSTERING_LICENSE_2}
${CLUSTER_WORKED}	${FALSE}
${HOSTNAME_CHANGED}	${FALSE}

*** Test Cases ***
Test Basic Search
#	Run Keyword If	${NGVERSION} <= 5.0	Set Tags	NON-CRITICAL	BUG_NG_8266
	CLI:Switch Connection	default
	CLI:Enter Path			/access/
	CLI:Test Search Command	pdu
	${SEARCH}				CLI:Show
	Should Contain			${SEARCH}				${PDU_SERVERTECH}[name]			${PDU_RARITAN}[name]
	Should Not Contain		${SEARCH}				test

Test Search with AND
#	Run Keyword If	${NGVERSION} <= 5.0	Set Tags	NON-CRITICAL	BUG_NG_8266
	CLI:Switch Connection	default
	CLI:Enter Path			/access/
	CLI:Test Search Command	"pdu AND raritan"
	${SEARCH}				CLI:Show
	Should Contain			${SEARCH}				${PDU_RARITAN}[name]
	Should Not Contain		${SEARCH}				${PDU_SERVERTECH}[name]			test

Test Search with OR
#	Run Keyword If	${NGVERSION} <= 5.0	Set Tags	NON-CRITICAL	BUG_NG_8266
	CLI:Switch Connection	default
	CLI:Enter Path			/access/
	CLI:Test Search Command	"pdu OR raritan"
	${SEARCH}				CLI:Show
	Should Contain			${SEARCH}				${PDU_RARITAN}[name]			${PDU_SERVERTECH}[name]
	Should Not Contain		${SEARCH}				test

Test Search with NOT
#	Run Keyword If	${NGVERSION} <= 5.0	Set Tags	NON-CRITICAL	BUG_NG_8266
	CLI:Switch Connection	default
	CLI:Enter Path			/access/
	CLI:Test Search Command	"NOT pdu"
	${SEARCH}				CLI:Show
	Should Contain			${SEARCH}				test
	Should Not Contain		${SEARCH}				${PDU_RARITAN}[name]			${PDU_SERVERTECH}[name]

Test Search with field=name
#	Run Keyword If	${NGVERSION} <= 5.0	Set Tags	NON-CRITICAL	BUG_NG_8266
	CLI:Switch Connection	default
	CLI:Enter Path			/access/
	CLI:Test Search Command	"name:pdu"
	${SEARCH}				CLI:Show
	Should Contain			${SEARCH}				${PDU_RARITAN}[name]
	Should Not Contain		${SEARCH}				test							${PDU_SERVERTECH}[name]

Test Search with field=type
#	Run Keyword If	${NGVERSION} <= 5.0	Set Tags	NON-CRITICAL	BUG_NG_8266
	CLI:Switch Connection	default
	CLI:Enter Path			/access/
	CLI:Test Search Command	"type:${PDU_RARITAN}[type]"
	${SEARCH}				CLI:Show
	Should Contain			${SEARCH}				${PDU_RARITAN}[name]
	Should Not Contain		${SEARCH}				test							${PDU_SERVERTECH}[name]

Test Search with field=ip_address
#	Run Keyword If	${NGVERSION} <= 5.0	Set Tags	NON-CRITICAL	BUG_NG_8266
	CLI:Switch Connection	default
	CLI:Enter Path			/access/
	CLI:Test Search Command	"ip:${HOSTPEER}"
	${SEARCH}				CLI:Show
	Should Contain			${SEARCH}				test
	Should Not Contain		${SEARCH}				${PDU_RARITAN}[name]			${PDU_SERVERTECH}[name]

Test Search Using Regex
#	Run Keyword If	${NGVERSION} <= 5.0	Set Tags	NON-CRITICAL	BUG_NG_8266
	CLI:Switch Connection	default
	CLI:Enter Path			/access/
	CLI:Test Search Command	"te*"
	${SEARCH}				CLI:Show
	Should Contain			${SEARCH}				${PDU_SERVERTECH}[name]			test
	Should Not Contain		${SEARCH}				${PDU_RARITAN}[name]

Test Advanced Search
#	Run Keyword If	${NGVERSION} <= 5.0	Set Tags	NON-CRITICAL	BUG_NG_8266
	CLI:Switch Connection	default
	CLI:Enter Path			/access/
	CLI:Test Search Command	"pdu AND NOT type:${PDU_RARITAN}[type]"
	${SEARCH}				CLI:Show
	Should Contain			${SEARCH}				${PDU_SERVERTECH}[name]
	Should Not Contain		${SEARCH}				test							${PDU_RARITAN}[name]

Test Search Peer Device
#	Run Keyword If	${NGVERSION} <= 5.0	Set Tags	NON-CRITICAL	BUG_NG_8266
#	[Tags]	NON-CRITICAL
	CLI:Switch Connection	default
	${CLUSTER_WORKED}	SUITE:Create Basic Cluster
	CLI:Enter Path			/access/
	Wait Until Keyword Succeeds	5x	45s	SUITE:Search Peer Device
	[Teardown]	CLI:Cancel	Raw

Test Search Only Peer Device
#	Run Keyword If	${NGVERSION} <= 5.0	Set Tags	NON-CRITICAL	BUG_NG_8266
#	[Tags]	NON-CRITICAL
	Skip If	not ${CLUSTER_WORKED}
	CLI:Switch Connection	default
	CLI:Enter Path  /access/
	CLI:Test Search Command	"name:peer"
	${SEARCH}				CLI:Show
	Should Contain			${SEARCH}	peer_device
	Should Not Contain		${SEARCH}	test
	[Teardown]	CLI:Cancel	Raw

Test Advanced Search When Peer Has Devices
#	Run Keyword If	${NGVERSION} <= 5.0	Set Tags	NON-CRITICAL	BUG_NG_8266
#	[Tags]	NON-CRITICAL
	Skip If	not ${CLUSTER_WORKED}
	CLI:Switch Connection	default
	CLI:Enter Path			/access/
	CLI:Test Search Command	"type:device_console AND peer"
	${SEARCH}				CLI:Show
	Should Contain			${SEARCH}				peer_device
	Should Not Contain		${SEARCH}				test	${PDU_RARITAN}[name]	${PDU_SERVERTECH}[name]
	[Teardown]	CLI:Cancel	Raw

*** Keyword ***
SUITE:Setup
	CLI:Open
	CLI:Open	session_alias=peer	HOST_DIFF=${HOSTPEER}
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/devices
	@{DEVICELIST}=	CLI:Get Devices List	all	${PROTECTED_DEVICE_TYPES}
	CLI:Delete Devices	@{DEVICELIST}
	Set Suite Variable	@{DEVICELIST}
	SUITE:Add Three Devices

SUITE:Teardown
	CLI:Switch Connection	default
	@{DEVICELIST}=	CLI:Get Devices List	all	${PROTECTED_DEVICE_TYPES}
	CLI:Delete Devices	@{DEVICELIST}
	Run Keyword If	${CLUSTER_WORKED}	SUITE:Delete Basic Cluster
	Run Keyword If	${HOSTNAME_CHANGED}	SUITE:Change Peer Hostname
	CLI:Close Connection

SUITE:Add Three Devices
	CLI:Add Device		${PDU_SERVERTECH}[name]	${PDU_SERVERTECH}[type]	${PDU_SERVERTECH}[ip]
	CLI:Add Device		${PDU_RARITAN}[name]	${PDU_RARITAN}[type]	${PDU_RARITAN}[ip]
	CLI:Add Device		test					device_console			${HOSTPEER}

SUITE:Create Basic Cluster
	CLI:Add License Key	${LICENSE_KEY}
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=yes type=coordinator allow_enrollment=yes psk=automationtest
	CLI:Set	cluster_mode=mesh enable_clustering_access=yes enable_peer_management=yes
	CLI:Set	enable_license_pool=yes lps_type=server
	Set Client Configuration	timeout=90s
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Switch Connection	peer
	${HOSTNAME_CHANGED}	Run Keyword And Return Status	CLI:Change Hostname	peer
	Set Suite Variable	${HOSTNAME_CHANGED}
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=yes type=peer coordinator_address=${HOST} psk=automationtest
	CLI:Set	enable_clustering_access=yes enable_peer_management=yes
	CLI:Set	enable_license_pool=yes lps_type=client
	CLI:Commit
	CLI:Add Device	peer_device	device_console	127.0.0.1
	CLI:Switch Connection	default
	Wait Until Keyword Succeeds	5x	45s	SUITE:Check If Peers Are Present On Coordinator
	Set Suite Variable	${CLUSTER_WORKED}	${TRUE}
	[Return]	${CLUSTER_WORKED}
	[Teardown]	CLI:Cancel	Raw

SUITE:Delete Basic Cluster
	CLI:Enter Path	/settings/cluster/cluster_peers/
	${PEERS}	CLI:Show
	${PEER_PRESENT}	Run Keyword And Return Status	Should Contain	${PEERS}	peer.${DOMAINAME_NODEGRID}
	Run Keyword If	${PEER_PRESENT}	CLI:Remove	peer.${DOMAINAME_NODEGRID}
	${PEER_REMOVED}	CLI:Show
	Should Not Contain	${PEER_REMOVED}	peer.${DOMAINAME_NODEGRID}
	CLI:Enter Path  				/settings/cluster/settings
	CLI:Edit
	CLI:Set							enable_cluster=no
	Set Client Configuration	timeout=120s
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Delete All License Keys Installed
	CLI:Switch Connection			peer
	CLI:Enter Path	/settings/devices/
	CLI:Delete If Exists Confirm	peer_device
	CLI:Enter Path  				/settings/cluster/settings
	CLI:Edit
	CLI:Set							enable_cluster=no
	Set Client Configuration	timeout=120s
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Switch Connection			default

SUITE:Change Peer Hostname
	CLI:Switch Connection	peer
	CLI:Change Hostname	${HOSTNAME_NODEGRID}

SUITE:Search Peer Device
	CLI:Test Search Command	"peer"
	${SEARCH}				CLI:Show
	Should Contain			${SEARCH}	peer_device

SUITE:Check If Peers Are Present On Coordinator
	${PEERS}	CLI:Show	/settings/cluster/cluster_peers
	Should Contain	${PEERS}	${HOSTPEER}
	Should Contain	${PEERS}	peer.${DOMAINAME_NODEGRID}
	Should Not Contain	${PEERS}	Offline
	Should Not Contain	${PEERS}	Error