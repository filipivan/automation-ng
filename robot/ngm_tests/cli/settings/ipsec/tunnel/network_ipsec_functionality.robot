*** Settings ***
Resource	../../../init.robot
Documentation	Tests Settings > Network IPsec Functionality tests through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${PRE_SHARED_TUNNEL_NAME}	ng-pre
${RSA_TUNNEL_NAME}	ng-rsa
${IPSEC_NAME_LEFT}	@NG1
${IPSEC_NAME_RIGHT}	@NG2
${IPSEC_SECRET}	automation-test
${RSA_KEY_BITSIZE}	4096

${NEWHOSTKEY_COMMAND_OUTPUT_REGEX}	Generated RSA key pair with CKAID (\\w+) was stored in the NSS database
${SHOWHOSTKEY_COMMAND_OUTPUT_REGEX}	(?:left|right)rsasigkey=(.*)

${ICMP_PLUS_IP_HEADER_SIZE}	28
${PING_PACKET_SIZE}	500
${EXPECTED_BYTES}	${${ICMP_PLUS_IP_HEADER_SIZE} + ${PING_PACKET_SIZE}}
${BYTES_RECEIVED_INT_LEFT}
${BYTES_SENT_INT_LEFT}

${IP_THRU_GATEWAY}	34.83.37.8
${FQDN_THRU_GATEWAY}	zpecloud.com
${TEST_IP}	8.8.8.8
${VTI_INTERFACE}	vti

${HOST_IPV6}	2001:db8:cafe::1
${HOSTSHARED_IPV6}	2001:db8:cafe::2
${EXPECTED_BYTES_IPV6}	548

${CUSTOM_PROFILE}	custom
${MTU_LENGTH}	20

${VLAN100_left}	${SWITCH_NETPORT_HOST_INTERFACE1}
${VLAN100_shared}	${SWITCH_NETPORT_SHARED_INTERFACE1}
${IP100_left}	10.20.30.1
${IP100_shared}	10.20.30.2

*** Test Cases ***
Test IPsec Using Pre Shared Key
	[Documentation]	Tests if after creating PSK tunnels in both sides, there are no traffic b/w them
	...	and then checks again after a ping to see bytes sent and received increasing
	[Setup]	SUITE:Setup And Up Pre Shared Key Tunnel On Both Units
	SUITE:Check Expected Bytes	admin_left	0	0	${IPSEC_NAME_RIGHT}
	SUITE:Check Expected Bytes	admin_right	0	0	${IPSEC_NAME_LEFT}
	SUITE:Ping Peer	${HOSTPEER}
	Wait Until Keyword Succeeds	3x	10s
	...	SUITE:Check Expected Bytes	admin_left	${EXPECTED_BYTES}	${EXPECTED_BYTES}	${IPSEC_NAME_RIGHT}
	Wait Until Keyword Succeeds	3x	10s
	...	SUITE:Check Expected Bytes	admin_right	${EXPECTED_BYTES}	${EXPECTED_BYTES}	${IPSEC_NAME_LEFT}
	[Teardown]	SUITE:Delete Tunnel	${PRE_SHARED_TUNNEL_NAME}

Test IPsec Using RSA Key
	[Documentation]	Tests if after creating RSA tunnels in both sides, there are no traffic b/w them
	...	and then checks again after a ping to see bytes sent and received increasing
	[Setup]	SUITE:Setup And Up RSA Key Tunnel On Both Units
	SUITE:Check Expected Bytes	admin_left	0	0	${IPSEC_NAME_RIGHT}
	SUITE:Check Expected Bytes	admin_right	0	0	${IPSEC_NAME_LEFT}
	SUITE:Ping Peer	${HOSTPEER}
	Wait Until Keyword Succeeds	3x	10s
	...	SUITE:Check Expected Bytes	admin_left	${EXPECTED_BYTES}	${EXPECTED_BYTES}	${IPSEC_NAME_RIGHT}
	Wait Until Keyword Succeeds	3x	10s
	...	SUITE:Check Expected Bytes	admin_right	${EXPECTED_BYTES}	${EXPECTED_BYTES}	${IPSEC_NAME_LEFT}
	[Teardown]	SUITE:Delete Tunnel	${RSA_TUNNEL_NAME}

Test IPsec tunnel with IPv6 Addresses
	[Tags]	EXCLUDEIN5_0	EXCLUDEIN5_2	NEED-REVIEW
	[Documentation]	Configure ETH0s from both sides to have a static IPv6 address and then
	...	creates a tunnel using this address, after that tests bytes sent and received
	...	thru the tunnel are increasing.
	Skip If	not ${HAS_HOSTSHARED}
	SUITE:Set Static IPv6 Addresses On Both Sides
	SUITE:Setup And Up Pre Shared Key Tunnel On Both Units	ipv6
	SUITE:Check Expected Bytes	admin_left	0	0	${IPSEC_NAME_RIGHT}
	SUITE:Check Expected Bytes	admin_shared	0	0	${IPSEC_NAME_LEFT}
	SUITE:Ping Peer	${HOSTSHARED_IPV6}
	Wait Until Keyword Succeeds	3x	10s
	...	SUITE:Check Expected Bytes	admin_left	${EXPECTED_BYTES_IPV6}	${EXPECTED_BYTES_IPV6}	${IPSEC_NAME_RIGHT}
	Wait Until Keyword Succeeds	3x	10s
	...	SUITE:Check Expected Bytes	admin_shared	${EXPECTED_BYTES_IPV6}	${EXPECTED_BYTES_IPV6}	${IPSEC_NAME_LEFT}
	SUITE:Set IPv6 Addresses Back to Default
	[Teardown]	SUITE:Delete Tunnel	${PRE_SHARED_TUNNEL_NAME}

Test on-demand functionality
	[Documentation]	Creates tunnels on-demand on both sides and check they are not Up
	...	after creation, then checks if the tunnel is Up after some traffic is generated thru it.
	SUITE:Create IPsec Tunnel With Pre Shared Key	admin_left	${PRE_SHARED_TUNNEL_NAME}
	...	${IPSEC_NAME_LEFT}	${IPSEC_NAME_RIGHT}	${HOST}	${HOSTPEER}	on_demand
	CLI:Enter Path	/settings/ipsec/tunnel/${PRE_SHARED_TUNNEL_NAME}
	CLI:Set	left_address=%defaultroute
	CLI:Commit
	SUITE:Create IPsec Tunnel With Pre Shared Key	admin_right	${PRE_SHARED_TUNNEL_NAME}
	...	${IPSEC_NAME_RIGHT}	${IPSEC_NAME_LEFT}	${HOSTPEER}	${HOST}	on_demand
	CLI:Enter Path	/settings/ipsec/tunnel/${PRE_SHARED_TUNNEL_NAME}
	CLI:Set	left_address=%defaultroute
	CLI:Commit
	CLI:Enter Path	/settings/ipsec/tunnel/
	Wait Until Keyword Succeeds	30s	3s	CLI:Test Show Command	Down
	CLI:Switch Connection	admin_left
	CLI:Test Ping	${HOSTPEER}
	CLI:Enter Path	/settings/ipsec/tunnel
	Wait Until Keyword Succeeds	30s	3s	CLI:Test Show Command	Up
	CLI:Switch Connection	admin_right
	CLI:Enter Path	/settings/ipsec/tunnel
	Wait Until Keyword Succeeds	30s	3s	CLI:Test Show Command	Up
	[Teardown]	SUITE:Delete Tunnel	${PRE_SHARED_TUNNEL_NAME}

Test ignore functionality
	[Documentation]	Creates tunnels type ignore on both sides and checks they are not Up
	...	after creation and are also not Up even if traffic is sent thru the tunnel. Checks
	...	if tunnel is Up only if start_tunnel command is used.
	SUITE:Create IPsec Tunnel With Pre Shared Key	admin_left	${PRE_SHARED_TUNNEL_NAME}
	...	${IPSEC_NAME_LEFT}	${IPSEC_NAME_RIGHT}	${HOST}	${HOSTPEER}	ignore
	SUITE:Create IPsec Tunnel With Pre Shared Key	admin_right	${PRE_SHARED_TUNNEL_NAME}
	...	${IPSEC_NAME_RIGHT}	${IPSEC_NAME_LEFT}	${HOSTPEER}	${HOST}	ignore
	Wait Until Keyword Succeeds	30s	3s	CLI:Test Show Command	Down
	CLI:Switch Connection	admin_left
	CLI:Test Ping	${HOSTPEER}
	CLI:Enter Path	/settings/ipsec/tunnel
	Wait Until Keyword Succeeds	30s	3s	CLI:Test Show Command	Down
	CLI:Switch Connection	admin_right
	CLI:Enter Path	/settings/ipsec/tunnel
	Wait Until Keyword Succeeds	30s	3s	CLI:Test Show Command	Down
	SUITE:Up Tunnel	admin_left	${PRE_SHARED_TUNNEL_NAME}
	SUITE:Up Tunnel	admin_right	${PRE_SHARED_TUNNEL_NAME}
	[Teardown]	SUITE:Delete Tunnel	${PRE_SHARED_TUNNEL_NAME}

Test Adding more than one IPsec tunnel using pre shared key
	[Documentation]	Tests adding two PSK tunnels, one b/w HOST and HOSTPEER and
	...	another one b/w HOST and HOSTSHARED.
	Skip If	not ${HAS_HOSTSHARED}	Test build does not have shared device
	SUITE:Setup And Up Pre Shared Key Tunnel On Both Units
	Wait Until Keyword Succeeds	30s	3s	CLI:Test Show Command	Up
	SUITE:Create IPsec Tunnel With Pre Shared Key	admin_left	second_tunnel
	...	${IPSEC_NAME_LEFT}	${IPSEC_NAME_RIGHT}	${HOST}	${HOSTSHARED}
	SUITE:Create IPsec Tunnel With Pre Shared Key	admin_shared	second_tunnel
	...	${IPSEC_NAME_RIGHT}	${IPSEC_NAME_LEFT}	${HOSTSHARED}	${HOST}
	Wait Until Keyword Succeeds	30s	3s	CLI:Test Show Command	Up
	[Teardown]	Run Keywords	SUITE:Delete Tunnel	${PRE_SHARED_TUNNEL_NAME}	 AND	SUITE:Delete Tunnel	second_tunnel

Test custom ike profile for tunnel
	[Documentation]	Creates an custom IKE profile that has a low MTU to test different IKE profiles
	...	so even if tunnel is up ping wont work.
	SUITE:Create Custom IKE Profile
	SUITE:Create IPsec Tunnel With Pre Shared Key	admin_left	${PRE_SHARED_TUNNEL_NAME}
	...	${IPSEC_NAME_LEFT}	${IPSEC_NAME_RIGHT}	${HOST}	${HOSTPEER}	PROFILE=${CUSTOM_PROFILE}
	SUITE:Create IPsec Tunnel With Pre Shared Key	admin_right	${PRE_SHARED_TUNNEL_NAME}
	...	${IPSEC_NAME_RIGHT}	${IPSEC_NAME_LEFT}	${HOSTPEER}	${HOST}	PROFILE=${CUSTOM_PROFILE}
	Wait Until Keyword Succeeds	30s	3s	CLI:Test Show Command	Up
	SUITE:Check Expected Bytes	admin_left	0	0	${IPSEC_NAME_RIGHT}
	SUITE:Check Expected Bytes	admin_right	0	0	${IPSEC_NAME_LEFT}
	CLI:Test Ping	${HOSTPEER}
	Sleep	5s
	SUITE:Check Expected Bytes	admin_left	0	0	${IPSEC_NAME_RIGHT}
	SUITE:Check Expected Bytes	admin_right	0	0	${IPSEC_NAME_LEFT}
	SUITE:Delete Custom IKE Profile
	[Teardown]	SUITE:Delete Tunnel	${PRE_SHARED_TUNNEL_NAME}

Test IPsec with custom interfaces
	[Documentation]	Tests IPSec tunnel using created interface, first an VLAN100 connection is crated and them
	...	this used as the left_address
	[Setup]	SUITE:Setup For Custom Interface
	SUITE:Create IPsec Tunnel With Pre Shared Key	admin_left	${PRE_SHARED_TUNNEL_NAME}
	...	${IPSEC_NAME_LEFT}	${IPSEC_NAME_RIGHT}	${IP100_left}	${IP100_shared}	LEFT_INTERFACE=%backplane0.100
	SUITE:Create IPsec Tunnel With Pre Shared Key	admin_shared	${PRE_SHARED_TUNNEL_NAME}
	...	${IPSEC_NAME_RIGHT}	${IPSEC_NAME_LEFT}	${IP100_shared}	${IP100_left}	LEFT_INTERFACE=%backplane0.100
	Wait Until Keyword Succeeds	30s	3s	CLI:Test Show Command	Up
	SUITE:Delete Tunnel	${PRE_SHARED_TUNNEL_NAME}
	[Teardown]	SUITE:Teardown For Custom Interface

Test Route Based Tunnels
	[Tags]	EXCLUDEIN5_0	EXLUDEIN5_2	EXCLUDEIN5_4
	[Documentation]	Tests route based tunnels by configuring static routes on both sides
	...	that makes all traffic, excpet for ${IP_THRU_GATEWAY}, to pass via IPsec tunnel.
	...	So a ping for this IP will not increase bytes traveling thru the tunnel, but
	...	Pinging any other IP address will.
	Skip If	not ${HAS_HOSTSHARED}	Test build does not have shared device
	SUITE:Set Global Configuration	admin_left	yes	yes
	SUITE:Create IPsec Tunnel With VTI	${PRE_SHARED_TUNNEL_NAME}	${IPSEC_SECRET}	${IPSEC_NAME_LEFT}
	...	${IPSEC_NAME_RIGHT}	${HOSTSHARED}
	SUITE:Create Static Route
	SUITE:Set Global Configuration	admin_shared	yes	yes
	SUITE:Create IPsec Tunnel With VTI	${PRE_SHARED_TUNNEL_NAME}	${IPSEC_SECRET}	${IPSEC_NAME_RIGHT}
	...	${IPSEC_NAME_LEFT}	${HOST}
	SUITE:Create Static Route
	CLI:Switch Connection	admin_shared
	CLI:Test Ping	${IP_THRU_GATEWAY}	TIMEOUT=60	NUM_PACKETS=5
	SUITE:Check VTI Config	left	${IPSEC_NAME_RIGHT}
	CLI:Switch Connection	admin_left
	CLI:Test Ping	${IP_THRU_GATEWAY}	TIMEOUT=60	NUM_PACKETS=5
	SUITE:Check VTI Config	shared	${IPSEC_NAME_LEFT}
	SUITE:Check Ping IP Outside Gateway
	[Teardown]	SUITE:Teardown For Route Based Tunnels

Test IPsec logging functionality
	[Documentation]	Test log file is generated when logging option is set in ipsec/global
	SUITE:Set Global Configuration	admin_left	yes	no
	SUITE:Set Global Configuration	admin_right	yes	no
	SUITE:Setup And Up Pre Shared Key Tunnel On Both Units
	SUITE:Ping Peer	${HOSTPEER}
	CLI:Switch Connection	root_left
	File Should Exist	/var/log/ipsec.log
	[Teardown]	SUITE:Delete Tunnel	${PRE_SHARED_TUNNEL_NAME}

*** Keywords ***
SUITE:Setup
	CLI:Open	session_alias=admin_left	HOST_DIFF=${HOST}
	CLI:Connect As Root	session_alias=root_left	HOST_DIFF=${HOST}
	CLI:Open	session_alias=admin_right	HOST_DIFF=${HOSTPEER}
	IF	${HAS_HOSTSHARED}
		CLI:Open	session_alias=admin_shared	HOST_DIFF=${HOSTSHARED}
		CLI:Connect As Root	session_alias=root_shared	HOST_DIFF=${HOSTSHARED}
	END
	SUITE:Delete Tunnel	${PRE_SHARED_TUNNEL_NAME}
	SUITE:Delete Tunnel	${RSA_TUNNEL_NAME}
	SUITE:Set Global Configuration	admin_right	no	no
	SUITE:Set Global Configuration	admin_left	no	no

SUITE:Teardown
	CLI:Open	session_alias=admin_left	HOST_DIFF=${HOST}
	CLI:Open	session_alias=admin_right	HOST_DIFF=${HOSTPEER}
	SUITE:Set Global Configuration	admin_left	no	no
	SUITE:Set Global Configuration	admin_right	no	no
	SUITE:Delete Tunnel	${PRE_SHARED_TUNNEL_NAME}
	SUITE:Delete Tunnel	${RSA_TUNNEL_NAME}
	CLI:Close Connection

SUITE:Delete Tunnel
	[Arguments]	${TUNNEL_NAME}
	CLI:Switch Connection	admin_right
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Delete If Exists	${TUNNEL_NAME}
	CLI:Commit
	CLI:Switch Connection	admin_left
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Delete If Exists	${TUNNEL_NAME}
	CLI:Commit
	IF	${HAS_HOSTSHARED}
		CLI:Open	session_alias=teardown_shared	HOST_DIFF=${HOSTSHARED}
		CLI:Enter Path	/settings/ipsec/tunnel/
		CLI:Delete If Exists	${TUNNEL_NAME}
		CLI:Commit
		CLI:Close Current Connection
	END

SUITE:Generate RSA Key
	[Arguments]	${HOST_ADDR}	${LEFT}=True
	CLI:Connect As Root	${HOST_ADDR}
	Set Client Configuration	prompt=:~#
	CLI:Write	rm /etc/ipsec/ipsec.d/${RSA_TUNNEL_NAME}.secrets
#	CLI:Write	ipsec initnss	Yes
	${OUTPUT}=	CLI:Write	ipsec newhostkey --bits ${RSA_KEY_BITSIZE} --output /etc/ipsec/ipsec.d/${RSA_TUNNEL_NAME}.secrets
	${MATCH}	${CKAID}=	Should Match Regexp	${OUTPUT}	${NEWHOSTKEY_COMMAND_OUTPUT_REGEX}
	${OUTPUT}=	Run Keyword If	${LEFT} == True
	...	CLI:Write	ipsec showhostkey --left --ckaid ${CKAID}
	...	ELSE	CLI:Write	ipsec showhostkey --right --ckaid ${CKAID}
	${MATCH}	${RSA_PUBLIC_KEY}=	Should Match Regexp	${OUTPUT}	${SHOWHOSTKEY_COMMAND_OUTPUT_REGEX}
	[Return]	${RSA_PUBLIC_KEY}

SUITE:Create IPsec Tunnel With Pre Shared Key
	[Arguments]	${CONNECTION}	${TUNNEL_NAME}	${LEFT_ID}	${RIGHT_ID}	${LEFT_ADDRESS}	${RIGHT_ADDRESS}
	...	${INITIATE}=start	${PROFILE}=nodegrid	${LEFT_INTERFACE}=ip_address
	CLI:Switch Connection	${CONNECTION}
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Add
	CLI:Set	name=${TUNNEL_NAME} initiate_tunnel=${INITIATE} ike_profile=${PROFILE}
	CLI:Set	authentication_method=pre-shared_key secret=${IPSEC_SECRET}
	CLI:Set	left_id=${LEFT_ID} left_address=${LEFT_INTERFACE} left_ip_address=${LEFT_ADDRESS}
	CLI:Set	right_id=${RIGHT_ID} right_address=${RIGHT_ADDRESS}
	CLI:Commit
	CLI:Test Show Command	${TUNNEL_NAME}
	[Teardown]	CLI:Cancel	Raw

SUITE:Create IPsec Tunnel With RSA Key
	[Arguments]	${CONNECTION}	${TUNNEL_NAME}	${LEFT_ID}	${RIGHT_ID}
	...	${LEFT_ADDRESS}	${RIGHT_ADDRESS}	${LEFT_RSA_KEY}	${RIGHT_RSA_KEY}
	CLI:Switch Connection	${CONNECTION}
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Add
	CLI:Set	name=${TUNNEL_NAME} initiate_tunnel=start ike_profile=nodegrid
	CLI:Set	authentication_method=rsa_key
	CLI:Set	left_public_key=${LEFT_RSA_KEY}
	CLI:Set	right_public_key=${RIGHT_RSA_KEY}
	CLI:Set	left_address=ip_address left_ip_address=${LEFT_ADDRESS}
	CLI:Set	left_id=${LEFT_ID} right_id=${RIGHT_ID}
	CLI:Set	right_address=${RIGHT_ADDRESS}
	CLI:Commit
	CLI:Show	${RSA_TUNNEL_NAME}
	CLI:Test Show Command	${TUNNEL_NAME}
	[Teardown]	CLI:Cancel	Raw

SUITE:Set Global Configuration
	[Arguments]	${CONNECTION}	${LOGGING}	${VIRTUAL_TUNNEL_INTERFACE}
	CLI:Switch Connection	${CONNECTION}
	CLI:Enter Path	/settings/ipsec/global/
	CLI:Set	enable_logging=${LOGGING} enable_virtual_tunnel_interface=${VIRTUAL_TUNNEL_INTERFACE}
	CLI:Commit

SUITE:Modificate Tunnel
	[Arguments]	${CONNECTION}	${TUNNEL_NAME}
	CLI:Switch Connection	${CONNECTION}
	CLI:Enter Path	/settings/ipsec/tunnel/${TUNNEL_NAME}
	CLI:Set	left_source_ip_address=${EMPTY}
	CLI:Commit

SUITE:Up Tunnel
	[Arguments]	${CONNECTION}	${TUNNEL_NAME}
	CLI:Switch Connection	${CONNECTION}
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Write	start_tunnel ${TUNNEL_NAME}
	Wait Until Keyword Succeeds	2m	3s	CLI:Test Show Command	Up

SUITE:Setup And Up Pre Shared Key Tunnel On Both Units
	[Arguments]	${IPVERSION}=ipv4
	IF	'${IPVERSION}' == 'ipv6'
		SUITE:Create IPsec Tunnel With Pre Shared Key	admin_left	${PRE_SHARED_TUNNEL_NAME}
		...	${IPSEC_NAME_LEFT}	${IPSEC_NAME_RIGHT}	${HOST_IPV6}	${HOSTSHARED_IPV6}
		SUITE:Create IPsec Tunnel With Pre Shared Key	admin_shared	${PRE_SHARED_TUNNEL_NAME}
		...	${IPSEC_NAME_RIGHT}	${IPSEC_NAME_LEFT}	${HOSTSHARED_IPV6}	${HOST_IPV6}
		SUITE:Up Tunnel	admin_left	${PRE_SHARED_TUNNEL_NAME}
		SUITE:Up Tunnel	admin_shared	${PRE_SHARED_TUNNEL_NAME}
	ELSE
		SUITE:Create IPsec Tunnel With Pre Shared Key	admin_left	${PRE_SHARED_TUNNEL_NAME}
		...	${IPSEC_NAME_LEFT}	${IPSEC_NAME_RIGHT}	${HOST}	${HOSTPEER}
		SUITE:Create IPsec Tunnel With Pre Shared Key	admin_right	${PRE_SHARED_TUNNEL_NAME}
		...	${IPSEC_NAME_RIGHT}	${IPSEC_NAME_LEFT}	${HOSTPEER}	${HOST}
		SUITE:Up Tunnel	admin_left	${PRE_SHARED_TUNNEL_NAME}
		SUITE:Up Tunnel	admin_right	${PRE_SHARED_TUNNEL_NAME}
	END
	[Teardown]	CLI:Cancel	Raw

SUITE:Setup And Up RSA Key Tunnel On Both Units
	${LEFT_KEY}=	SUITE:Generate RSA Key	${HOST}
	${RIGHT_KEY}=	SUITE:Generate RSA Key	${HOSTPEER}	False
	SUITE:Create IPsec Tunnel With RSA Key	admin_left	${RSA_TUNNEL_NAME}	${IPSEC_NAME_LEFT}
	...	${IPSEC_NAME_RIGHT}	${HOST}	${HOSTPEER}	${LEFT_KEY}	${RIGHT_KEY}
	SUITE:Create IPsec Tunnel With RSA Key	admin_right	${RSA_TUNNEL_NAME}	${IPSEC_NAME_RIGHT}
	...	${IPSEC_NAME_LEFT}	${HOSTPEER}	${HOST}	${RIGHT_KEY}	${LEFT_KEY}
	SUITE:Up Tunnel	admin_left	${RSA_TUNNEL_NAME}
	SUITE:Up Tunnel	admin_right	${RSA_TUNNEL_NAME}
	[Teardown]	CLI:Cancel	Raw

SUITE:Ping Peer
	[Arguments]	${IP_ADDRESS}
	CLI:Connect As Root
	Write	ping ${IP_ADDRESS} -c 1 -s ${PING_PACKET_SIZE}

SUITE:Wait Until Final Value of Bytes Received and Sent Are Greater Than Before
	[Arguments]	${TUNNEL_NAME}
	Wait Until Keyword Succeeds	1m	3s
	...	SUITE:Final Value of Bytes Received and Sent Should Be Greater Than Before	admin_left	${TUNNEL_NAME}	${BYTES_SENT_INT_LEFT}	${BYTES_RECEIVED_INT_LEFT}

SUITE:Check Initial Value Of Bytes Received and Sent
	[Arguments]	${CONNECTION}	${TUNNEL_NAME}
	CLI:Switch Connection	${CONNECTION}
	CLI:Enter Path	/system/ipsec_table/
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	${TUNNEL_NAME}
	@{GET_LINES}=	Split To Lines	${OUTPUT}
	${LINE}=	Get From List	${GET_LINES}	2
	${BYTES_RECEIVED}=	Get Substring	${LINE}	82	93

	${BYTES_SENT}=	Get Substring	${LINE}	66	81
	${BYTES_RECEIVED_INT}=	Convert To Integer	${BYTES_RECEIVED}
	${BYTES_SENT_INT}=	Convert To Integer	${BYTES_SENT}
	Set Suite Variable	${BYTES_RECEIVED_INT_LEFT}	${BYTES_RECEIVED_INT}
	Set Suite Variable	${BYTES_SENT_INT_LEFT}	${BYTES_SENT_INT}

SUITE:Final Value of Bytes Received and Sent Should Be Greater Than Before
	[Arguments]	${CONNECTION}	${TUNNEL_NAME}	${CURRENT_BYTES_SENT}	${CURRENT_BYTES_RECEIVED}
	CLI:Switch Connection	${CONNECTION}
	CLI:Enter Path	/system/ipsec_table/
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	${TUNNEL_NAME}
	@{GET_LINES}=	Split To Lines	${OUTPUT}
	${LINE}=	Get From List	${GET_LINES}	2
	${BYTES_RECEIVED}=	Get Substring	${LINE}	82	93
	${BYTES_RECEIVED_INT}=	Convert To Integer	${BYTES_RECEIVED}

	${BYTES_SENT}=	Get Substring	${LINE}	66	81
	${BYTES_SENT_INT}=	Convert To Integer	${BYTES_SENT}
	Should Be True	${BYTES_RECEIVED_INT} == ${EXPECTED_BYTES}
	Should Be True	${BYTES_SENT_INT} == ${EXPECTED_BYTES}

SUITE:Create IPsec Tunnel With VTI
	[Arguments]	${NAME}	${SECRET}	${LEFT_ID}	${RIGHT_ID}	${RIGHT_IP}
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Set	name=${NAME} custom_up|down_script=route_based_vti_updown.sh secret=${SECRET}
	CLI:Set	left_id=${LEFT_ID} left_address=%eth0 left_subnet=0.0.0.0/0 right_id=${RIGHT_ID}
	CLI:Set	right_address=${RIGHT_IP} right_subnet=0.0.0.0/0 enable_virtual_tunnel_interface=yes
	CLI:Set	vti_interface=${VTI_INTERFACE} automatically_create_vti_routes=no
	CLI:Set	share_vti_with_other_connections=yes vti_mark=-1
	CLI:Commit
	[Teardown]	CLI:Cancel	Raw

SUITE:Create Static Route
	CLI:Enter Path	/settings/static_routes/
	CLI:Add
	CLI:Set	 destination_ip=${FQDN_THRU_GATEWAY} gateway_ip=${GATEWAY} treat_destination_ip_as_fqdn=yes connection=ETH0
	CLI:Commit

SUITE:Check VTI Config
	[Arguments]	${CONNECTION}	${ID}
	CLI:Switch Connection	root_${CONNECTION}
	${LEFT_ROUTES}	CLI:Write	ip route
	Should Contain	${LEFT_ROUTES}	${IP_THRU_GATEWAY} via ${GATEWAY} dev eth0 metric 100
	Wait Until Keyword Succeeds	30s	5s	SUITE:Check Expected Bytes	admin_${CONNECTION}	0	0	${ID}

SUITE:Check Expected Bytes
	[Arguments]		${CONNECTION}	${RECEIVED_BYTES}	${SENT_BYTES}	${ID}
	CLI:Switch Connection	${CONNECTION}
	CLI:Enter Path	/system/ipsec_table
	${BYTES}	CLI:Show
	Should Match Regexp	${BYTES}	${RECEIVED_BYTES}\\s+${SENT_BYTES}\\s+${ID}

SUITE:Check Ping IP Outside Gateway
	CLI:Switch Connection	root_left
	CLI:Write	ping ${TEST_IP} -c 5
	Wait Until Keyword Succeeds	30s	5s	SUITE:Check Expected Bytes	admin_left	0	420	${IPSEC_NAME_RIGHT}
	Wait Until Keyword Succeeds	30s	5s	SUITE:Check Expected Bytes	admin_shared	420	0	${IPSEC_NAME_LEFT}

SUITE:Teardown For Route Based Tunnels
	FOR	${VALUE}	IN	left	shared
		CLI:Switch Connection	admin_${VALUE}
		CLI:Enter Path	/settings/static_routes
		CLI:Delete If Exists	ETH0_ipv4_fqdn_route1
		CLI:Commit
		Set Client Configuration	prompt=#
		CLI:Write	shell sudo su -
		CLI:Write	ip link del vti
		CLI:Write	nmcli c up ETH0
		Set Client Configuration	prompt=]#
		CLI:Write	exit
	END
	SUITE:Delete Tunnel	${PRE_SHARED_TUNNEL_NAME}

SUITE:Set Static IPv6 Addresses On Both Sides
	CLI:Switch Connection	admin_left
	CLI:Enter Path	/settings/network_connections/ETH0
	CLI:Set	ipv6_mode=static ipv6_address=${HOST_IPV6}
	CLI:Commit
	CLI:Switch Connection	admin_shared
	CLI:Enter Path	/settings/network_connections/ETH0
	CLI:Set	ipv6_mode=static ipv6_address=${HOSTSHARED_IPV6}
	CLI:Commit

SUITE:Set IPv6 Addresses Back to Default
	CLI:Switch Connection	admin_left
	CLI:Enter Path	/settings/network_connections/ETH0
	CLI:Set	ipv6_mode=no_ipv6_address
	CLI:Commit
	CLI:Switch Connection	admin_right
	CLI:Enter Path	/settings/network_connections/ETH0
	CLI:Set	ipv6_mode=no_ipv6_address
	CLI:Commit

SUITE:Create Custom IKE Profile
	FOR	${SESSION}	IN	left	right
		CLI:Switch Connection	admin_${SESSION}
		CLI:Enter Path	/settings/ipsec/ike_profile
		CLI:Add
		CLI:Set	profile_name=${CUSTOM_PROFILE} mtu=${MTU_LENGTH}
		CLI:Commit
	END
	[Teardown]	CLI:Cancel	Raw

SUITE:Delete Custom IKE Profile
	FOR	${SESSION}	IN	left	right
		CLI:Switch Connection	admin_${SESSION}
		CLI:Enter Path	/settings/ipsec/ike_profile
		CLI:Delete	${CUSTOM_PROFILE}
		CLI:Commit
	END

SUITE:Configure VLAN
	FOR	${SESSION}	IN	left	shared
		CLI:Switch Connection	admin_${SESSION}
		CLI:Enter Path	/settings/switch_interfaces/
		CLI:Edit	${VLAN100_${SESSION}}
		CLI:Set	status=enabled
		CLI:Commit
		CLI:Enter Path	/settings/switch_vlan/
		CLI:Add
		CLI:Set	untagged_ports=${VLAN100_${SESSION}},backplane0 tagged_ports= vlan=100
		CLI:Commit
		CLI:Edit	100
		CLI:Set	untagged_ports= tagged_ports=${VLAN100_${SESSION}},backplane0
		CLI:Commit
	END
	[Teardown]	CLI:Cancel	Raw

SUITE:Delete VLAN Configuration
	FOR	${SESSION}	IN	left	shared
		CLI:Switch Connection	admin_${SESSION}
		CLI:Enter Path	/settings/switch_interfaces/
		CLI:Edit	${VLAN100_${SESSION}}
		CLI:Set	status=disabled
		CLI:Commit
		CLI:Delete VLAN	100
	END

SUITE:Configure Connections
	FOR	${SESSION}	IN	left	shared
		CLI:Switch Connection	admin_${SESSION}
		CLI:Enter Path	/settings/network_connections
		CLI:Add
		CLI:Set	name=vlan100 type=vlan ethernet_interface=backplane0 vlan_id=100
		CLI:Set	ipv4_mode=static ipv4_address=${IP100_${SESSION}} ipv4_bitmask=24
		CLI:Commit
	END
	[Teardown]	CLI:Cancel	Raw

SUITE:Delete Connections
	FOR	${SESSION}	IN	left	shared
		CLI:Switch Connection	admin_${SESSION}
		CLI:Enter Path	/settings/network_connections/
		CLI:Delete If Exists	vlan100
	END

SUITE:Setup For Custom Interface
	SUITE:Configure VLAN
	SUITE:Configure Connections

SUITE:Teardown For Custom Interface
	SUITE:Delete Connections
	SUITE:Delete VLAN Configuration