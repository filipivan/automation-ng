*** Settings ***
Resource	../../../init.robot
Documentation	Tests Settings > Network IPsec monitoring configuration through the CLI
Metadata	    Version 	1.0
Metadata	    Executed At 	${HOST}
Force Tags	    CLI	EXCLUDEIN3_2	EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${PRE_SHARED_TUNNEL_NAME}	ng-pre
${IPSEC_NAME_LEFT}	@NG1
${IPSEC_NAME_RIGHT}	@NG2
${IPSEC_SECRET}	automation-test

${BYTES_RECEIVED_INT_LEFT}
${BYTES_SENT_INT_LEFT}
@{MONITORING_ACTIONS}  restart_ipsec  restart_tunnel  failover

*** Test Cases ***
Test Add IPsec Configuration With Monitoring
	FOR	  ${MONITORING_ACTION}  IN  @{MONITORING_ACTIONS}
		SUITE:Delete Tunnel	${PRE_SHARED_TUNNEL_NAME}
		SUITE:Create IPsec Tunnel With Pre Shared Key And Monitoring	admin_left	${PRE_SHARED_TUNNEL_NAME}
		...	${IPSEC_NAME_RIGHT}	${IPSEC_NAME_LEFT}	${HOST}	${HOSTPEER}  ${MONITORING_ACTION}
		CLI:Enter Path	/settings/ipsec/tunnel/
		Wait Until Keyword Succeeds	1m	1s	CLI:Test Show Command	${PRE_SHARED_TUNNEL_NAME}
		Run Keyword If    '${MONITORING_ACTION}' == 'failover'	 SUITE:Delete Tunnel	 failover
		SUITE:Delete Tunnel	${PRE_SHARED_TUNNEL_NAME}
		SUITE:Create IPsec Tunnel With Pre Shared Key And Monitoring	admin_right	${PRE_SHARED_TUNNEL_NAME}
		...	${IPSEC_NAME_RIGHT}	${IPSEC_NAME_LEFT}	${HOST}	${HOSTPEER}  ${MONITORING_ACTION}
		CLI:Enter Path	/settings/ipsec/tunnel/
		Wait Until Keyword Succeeds	1m	1s	CLI:Test Show Command	${PRE_SHARED_TUNNEL_NAME}
		Run Keyword If    '${MONITORING_ACTION}' == 'failover'	 SUITE:Delete Tunnel	 failover
	END

Test Delete IPsec Configuration With Monitoring
    FOR	  ${MONITORING_ACTION}  IN  @{MONITORING_ACTIONS}
		SUITE:Create IPsec Tunnel With Pre Shared Key And Monitoring	admin_left
		...  ${PRE_SHARED_TUNNEL_NAME}   ${IPSEC_NAME_RIGHT}	${IPSEC_NAME_LEFT}	${HOST}	${HOSTPEER}  ${MONITORING_ACTION}
		CLI:Test Show Command	${PRE_SHARED_TUNNEL_NAME}
		Run Keyword If    '${MONITORING_ACTION}' == 'failover'	 SUITE:Delete Tunnel	 failover
		SUITE:Delete Tunnel	${PRE_SHARED_TUNNEL_NAME}
		Wait Until Keyword Succeeds	1m	1s	CLI:Test Not Show Command	${PRE_SHARED_TUNNEL_NAME}
		SUITE:Create IPsec Tunnel With Pre Shared Key And Monitoring	admin_right
		...  ${PRE_SHARED_TUNNEL_NAME}   ${IPSEC_NAME_RIGHT}	${IPSEC_NAME_LEFT}	${HOST}	${HOSTPEER}  ${MONITORING_ACTION}
		CLI:Test Show Command	${PRE_SHARED_TUNNEL_NAME}
		Run Keyword If    '${MONITORING_ACTION}' == 'failover'	 SUITE:Delete Tunnel	 failover
		SUITE:Delete Tunnel	${PRE_SHARED_TUNNEL_NAME}
		Wait Until Keyword Succeeds	1m	1s	CLI:Test Not Show Command	${PRE_SHARED_TUNNEL_NAME}
	END

Test Edit IPsec Configuration With Monitoring
	SUITE:Create IPsec Tunnel With Pre Shared Key And Monitoring	admin_left
	...  ${PRE_SHARED_TUNNEL_NAME}   ${IPSEC_NAME_RIGHT}	${IPSEC_NAME_LEFT}	${HOST}	${HOSTPEER}	failover
	SUITE:Create IPsec Tunnel With Pre Shared Key And Monitoring	admin_right
	...  ${PRE_SHARED_TUNNEL_NAME}   ${IPSEC_NAME_RIGHT}	${IPSEC_NAME_LEFT}	${HOST}	${HOSTPEER}	failover
	FOR	  ${MONITORING_ACTION}  IN  @{MONITORING_ACTIONS}
		CLI:Switch Connection	admin_left
		CLI:Enter Path	/settings/ipsec/tunnel/${PRE_SHARED_TUNNEL_NAME}
		CLI:Set	 monitoring_action=${MONITORING_ACTION}
		CLI:Commit
		Wait Until Keyword Succeeds	1m	1s	CLI:Test Show Command	monitoring_action = ${MONITORING_ACTION}
	END

*** Keywords ***
SUITE:Setup
	CLI:Open	session_alias=admin_left	HOST_DIFF=${HOST}
	CLI:Open	session_alias=admin_right	HOST_DIFF=${HOSTPEER}  startping=no
	SUITE:Delete Tunnel	 ${PRE_SHARED_TUNNEL_NAME}
	SUITE:Set Global Configuration	admin_right	no	no
	SUITE:Set Global Configuration	admin_left	no	no

SUITE:Teardown
	SUITE:Delete Tunnel	${PRE_SHARED_TUNNEL_NAME}
	SUITE:Delete Tunnel	failover
	CLI:Close Connection

SUITE:Set Global Configuration
	[Arguments]	${CONNECTION}	${LOGGING}	${VIRTUAL_TUNNEL_INTERFACE}
	CLI:Switch Connection	${CONNECTION}
	CLI:Enter Path	/settings/ipsec/global/
	CLI:Set	enable_logging=${LOGGING} enable_virtual_tunnel_interface=${VIRTUAL_TUNNEL_INTERFACE}
	CLI:Commit

SUITE:Delete Tunnel
	[Arguments]	${TUNNEL_NAME}
	CLI:Switch Connection	admin_left
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Delete If Exists	${TUNNEL_NAME}
	CLI:Commit
	CLI:Switch Connection	admin_right
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Delete If Exists	${TUNNEL_NAME}
	CLI:Commit

SUITE:Create IPsec Tunnel With Pre Shared Key And Monitoring
	[Arguments]	${CONNECTION}	${TUNNEL_NAME}	${LEFT_ID}	${RIGHT_ID}
	...	${RIGHT_ADDRESS}	${LEFT_SOURCE_IP_ADDRESS}  ${MONITORING_ACTION}
	CLI:Switch Connection	${CONNECTION}
	Run Keyword If    '${MONITORING_ACTION}' == 'failover'	 SUITE:Set IPsec Tunnel With Monitoring  ${CONNECTION}
	...  failover	${LEFT_ID}	${RIGHT_ID}  ${RIGHT_ADDRESS}	${LEFT_SOURCE_IP_ADDRESS}  ${MONITORING_ACTION}
	Run Keyword If    '${MONITORING_ACTION}' == 'failover'	 CLI:Set  monitoring_action=restart_ipsec
	Run Keyword If    '${MONITORING_ACTION}' == 'failover'	 CLI:Commit
	SUITE:Set IPsec Tunnel With Monitoring  ${CONNECTION}	${TUNNEL_NAME}	${LEFT_ID}	${RIGHT_ID}
	...	${RIGHT_ADDRESS}	${LEFT_SOURCE_IP_ADDRESS}  ${MONITORING_ACTION}
	CLI:Commit
	CLI:Test Show Command	${TUNNEL_NAME}
	Run Keyword If    '${MONITORING_ACTION}' == 'failover'	 CLI:Write   cd ${TUNNEL_NAME}
	Run Keyword If    '${MONITORING_ACTION}' == 'failover'	 CLI:Test Show Command
	...  monitoring_failover_ipsec_tunnel = failover
	Run Keyword If    '${MONITORING_ACTION}' == 'failover'	 CLI:Write  cancel

SUITE:Set IPsec Tunnel With Monitoring
	[Arguments]	${CONNECTION}	${TUNNEL_NAME}	${LEFT_ID}	${RIGHT_ID}
	...	${RIGHT_ADDRESS}	${LEFT_SOURCE_IP_ADDRESS}  ${MONITORING_ACTION}
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Add
	CLI:Set  name=${TUNNEL_NAME} initiate_tunnel=start ike_profile=nodegrid
	CLI:Set  authentication_method=pre-shared_key secret=${IPSEC_SECRET}
	CLI:Set  left_id=${LEFT_ID} left_address=%defaultroute right_id=${RIGHT_ID}
	CLI:Set  right_address=${RIGHT_ADDRESS} left_source_ip_address=${LEFT_SOURCE_IP_ADDRESS} enable_monitoring=yes
	CLI:Set  monitoring_source_ip_address=${LEFT_SOURCE_IP_ADDRESS}
	CLI:Set  monitoring_destination_ip_address=${RIGHT_ADDRESS}
	CLI:Set  monitoring_action=${MONITORING_ACTION}

