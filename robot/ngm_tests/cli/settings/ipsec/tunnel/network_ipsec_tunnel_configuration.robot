*** Settings ***
Resource	../../../init.robot
Documentation	Tests Settings > Network IPsec > Tunnel Configurations through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL

Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NAME}	automation
${IPSEC_NAME_LEFT}	left
${IPSEC_NAME_RIGHT}	right
${IPSEC_SECRET}	automation

${NEW_SECRET}	very_safe_secret
${NEW_LEFT_ID}	NG-peer
${NEW_RIGHT_ID}	NG-host
*** Test Cases ***
Test Add IPsec Configuration
	[Documentation]	Adds a new tunnels in HOST and HOSTPEER checking their names show in the tunnel table
	[Setup]	SUITE:Delete Tunnel	${NAME}
	SUITE:Create IPsec Tunnel	admin_right	${NAME}	${IPSEC_NAME_RIGHT}	${IPSEC_NAME_LEFT}	${HOST}	${HOSTPEER}
	CLI:Enter Path	/settings/ipsec/tunnel/
	Wait Until Keyword Succeeds	1m	1s	CLI:Test Show Command	${NAME}
	SUITE:Create IPsec Tunnel	admin_left	${NAME}	${IPSEC_NAME_LEFT}	${IPSEC_NAME_RIGHT}	${HOSTPEER}	${HOST}
	CLI:Enter Path	/settings/ipsec/tunnel/
	Wait Until Keyword Succeeds	1m	1s	CLI:Test Show Command	${NAME}
	[Teardown]	SUITE:Delete Tunnel	${NAME}

Test Delete IPsec Configuration
	[Documentation]	Deletes tunnels in HOST and HOSTPEER checking their names do not show in the tunnel table
	[Setup]	Run Keywords	SUITE:Create IPsec Tunnel	admin_right	${NAME}	${IPSEC_NAME_RIGHT}	${IPSEC_NAME_LEFT}	${HOST}	${HOSTPEER}
	...	AND	SUITE:Create IPsec Tunnel	admin_left	${NAME}	${IPSEC_NAME_LEFT}	${IPSEC_NAME_RIGHT}	${HOSTPEER}	${HOST}
	CLI:Test Show Command	${NAME}
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Delete If Exists	${NAME}
	CLI:Commit
	Wait Until Keyword Succeeds	1m	1s	CLI:Test Not Show Command	${NAME}
	CLI:Switch Connection	admin_right
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Test Show Command	${NAME}
	CLI:Delete If Exists	${NAME}
	CLI:Commit
	Wait Until Keyword Succeeds	1m	1s	CLI:Test Not Show Command	${NAME}
	[Teardown]	SUITE:Delete Tunnel	${NAME}

Test Edit IPsec Configuration
	[Documentation]	Tests creating a tunnel and then change its config to check the changes were applied
	...	using show command and checking /etc/ipsec/ipsec.d/psk_secrets file
	[Setup]	Run Keywords	SUITE:Create IPsec Tunnel	admin_right	${NAME}	${IPSEC_NAME_RIGHT}	${IPSEC_NAME_LEFT}	${HOST}	${HOSTPEER}
	...	AND	SUITE:Create IPsec Tunnel	admin_left	${NAME}	${IPSEC_NAME_LEFT}	${IPSEC_NAME_RIGHT}	${HOSTPEER}	${HOST}
	CLI:Enter Path	/settings/ipsec/tunnel/${NAME}
	CLI:Set	 left_source_ip_address=${EMPTY}
	CLI:Commit
	CLI:Enter Path	/settings/ipsec/tunnel/${NAME}
	Wait Until Keyword Succeeds	1m	1s	CLI:Test Not Show Command	left_source_ip_address=${HOST}
	SUITE:Check psk_secrets File	${IPSEC_NAME_LEFT}	${IPSEC_NAME_RIGHT}	${IPSEC_SECRET}
	CLI:Switch Connection	admin_right
	CLI:Enter Path	/settings/ipsec/tunnel/${NAME}
	CLI:Set	left_id=${NEW_LEFT_ID} right_id=${NEW_RIGHT_ID} secret=${NEW_SECRET}
	CLI:Commit
	SUITE:Check psk_secrets File	${NEW_RIGHT_ID}	${NEW_LEFT_ID}	${NEW_SECRET}
	[Teardown]	SUITE:Delete Tunnel	${NAME}

Test start/stop tunnel
	[Documentation]	Creates tunnel and then tests stopping the tunnel and then starting the tunnel again
	[Setup]	Run Keywords	SUITE:Create IPsec Tunnel	admin_right	${NAME}	${IPSEC_NAME_RIGHT}	${IPSEC_NAME_LEFT}	${HOST}	${HOSTPEER}
	...	AND	SUITE:Create IPsec Tunnel	admin_left	${NAME}	${IPSEC_NAME_LEFT}	${IPSEC_NAME_RIGHT}	${HOSTPEER}	${HOST}
	CLI:Switch Connection	admin_left
	CLI:Enter Path	/settings/ipsec/tunnel
	Wait Until Keyword Succeeds	3x	30s	SUITE:Check Tunnel Up
	CLI:Write	stop_tunnel ${NAME}
	Wait Until Keyword Succeeds	5x	25s	SUITE:Check Tunnel Down
	CLI:Write	start_tunnel ${NAME}
	Wait Until Keyword Succeeds	3x	15s	SUITE:Check Tunnel Up
	[Teardown]	SUITE:Delete Tunnel	${NAME}

*** Keywords ***
SUITE:Setup
	CLI:Open	session_alias=admin_left	HOST_DIFF=${HOST}
	CLI:Open	session_alias=admin_right	HOST_DIFF=${HOSTPEER}
	CLI:Connect As Root	HOST_DIFF=${HOSTPEER}

SUITE:Teardown
	SUITE:Delete Tunnel	${NAME}
	CLI:Close Connection

SUITE:Delete Tunnel
	[Arguments]	${NAME}
	CLI:Switch Connection	admin_right
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Delete If Exists	${NAME}
	CLI:Commit
	CLI:Switch Connection	admin_left
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Delete If Exists	${NAME}
	CLI:Commit

SUITE:Create IPsec Tunnel
	[Arguments]	${CONNECTION}	${NAME}	${LEFT_ID}	${RIGHT_ID}	${RIGHT_ADDRESS}	${LEFT_SOURCE_IP_ADDRESS}
	CLI:Switch Connection	${CONNECTION}
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Add
	CLI:Set	name=${NAME} initiate_tunnel=start ike_profile=nodegrid authentication_method=pre-shared_key secret=${IPSEC_SECRET}
	CLI:Set	left_id=${LEFT_ID} left_address=%defaultroute right_id=${RIGHT_ID} right_address=${RIGHT_ADDRESS} left_source_ip_address=${LEFT_SOURCE_IP_ADDRESS}
	CLI:Commit

SUITE:Check psk_secrets File
	[Arguments]	${ID_LEFT}	${ID_RIGHT}	${SECRET}
	CLI:Switch Connection	root_session
	CLI:Enter Path	/etc/ipsec/ipsec.d/psk_secrets
	${PSK_SECRETS}	CLI:Write	cat ${NAME}.secrets
	Should Contain	${PSK_SECRETS}	${ID_RIGHT} ${ID_LEFT} : PSK "${SECRET}"

SUITE:Check Tunnel Up
	${TUNNEL_STATUS}	CLI:Show
	Should Not Contain	${TUNNEL_STATUS}	Down
	Should Contain	${TUNNEL_STATUS}	Up

SUITE:Check Tunnel Down
	${TUNNEL_STATUS}	CLI:Show
	Should Not Contain	${TUNNEL_STATUS}	Up
	Should Contain	${TUNNEL_STATUS}	Down