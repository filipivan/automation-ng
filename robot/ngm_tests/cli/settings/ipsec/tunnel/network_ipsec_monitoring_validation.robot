*** Settings ***
Resource	../../../init.robot
Documentation	Tests Settings > Network IPsec monitoring validation through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NAME}	automation
${IPSEC_SECRET}	automation
${IPSEC_NAME_RIGHT}	right
${IPSEC_NAME_LEFT}	left
@{VALID_IPS}	192.168.15.60	192.168.16.85
@{INVALID_IPS}	20.20	abcd	10.10.10	300.300.300.300	-1.-1.-1.-1	0,5.0,5.0,5.0,5	0.00.00.00
@{ALL_VALUES}=	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
	...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
	...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
	...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
	...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
	...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}

*** Test Cases ***
Test Show Command AT Monitoring Under Tunnel Path
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Add
	CLI:Test Show Command	enable_monitoring = no
	CLI:Write	set enable_monitoring=yes
	CLI:Test Show Command	enable_monitoring = yes	monitoring_source_ip_address =	monitoring_destination_ip_address =
	...	monitoring_number_of_retries = 3	monitoring_interval = 60	monitoring_action = restart_ipsec
	...	monitoring_failover_ipsec_tunnel =
	[Teardown]	CLI:Cancel	Raw

Test Available Commands AT Monitoring After Send Add Under Tunnel Path
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Add
	CLI:Write	set enable_monitoring=yes
	CLI:Test Available Commands	cancel	commit	ls	save	set	show
	[Teardown]	CLI:Cancel	Raw

Test Available Set Fields AT Monitoring Under Tunnel Path
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Add
	CLI:Write	set enable_monitoring=yes
	CLI:Test Set Available Fields	monitoring_action=	monitoring_destination_ip_address=	monitoring_failover_ipsec_tunnel=
	...	monitoring_interval=	monitoring_number_of_retries=	monitoring_source_ip_address=
	[Teardown]	CLI:Cancel	Raw

Test show_settings Command AT Monitoring Under Tunnel Path
	CLI:Enter Path	/settings/ipsec/tunnel/
	${OUTPUT}=	CLI:Show Settings
	@{SHOW_SETTINGS} =	Create List	/settings/ipsec/tunnel/${NAME} monitoring_source_ip_address=
	...	/settings/ipsec/tunnel/${NAME} monitoring_destination_ip_address=
	...	/settings/ipsec/tunnel/${NAME} monitoring_number_of_retries=3
	...	/settings/ipsec/tunnel/${NAME} monitoring_interval=60
	...	/settings/ipsec/tunnel/${NAME} monitoring_action=restart_ipsec
	FOR		${SETTING}	IN	@{SHOW_SETTINGS}
		Should Not Contain	${OUTPUT}	${SETTING}
	END

Test Valid Values For Field=enable_monitoring
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Add
	${OUTPUT}=	CLI:Test Set Field Options	enable_monitoring	yes	no
	Log	${OUTPUT}
	should be true	'${OUTPUT}' == 'None'
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	SUITE:Delete Tunnel With Monitoring	${NAME}

Test Invalid Values For Field=enable_monitoring
	SUITE:Add And Set IPSec Tunnel
	CLI:Test Set Field Invalid Options	enable_monitoring	${EMPTY}
	...	Error: Missing value for parameter: enable_monitoring
	FOR		${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	enable_monitoring	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: enable_monitoring
	END
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	SUITE:Delete Tunnel With Monitoring	${NAME}

Test Valid Values For Field=monitoring_destination_ip_address
	FOR		${VALID_IP}	IN	@{VALID_IPS}
		SUITE:Add And Set IPSec Tunnel
		CLI:Test Set Field Invalid Options	monitoring_destination_ip_address	${VALID_IP}	save=yes
	END
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	SUITE:Delete Tunnel With Monitoring	${NAME}

Test Invalid Values For Field=monitoring_destination_ip_address
	FOR		${INVALID_IP}	IN	@{INVALID_IPS}
		SUITE:Add And Set IPSec Tunnel
		CLI:Test Set Field Invalid Options	monitoring_destination_ip_address  ${INVALID_IP}
		...  ERROR=Error: monitoring_destination_ip_address: Invalid IP address.  save=yes
		CLI:Cancel	Raw
	END
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	SUITE:Delete Tunnel With Monitoring	${NAME}

Test Valid Values For Field=monitoring_source_ip_address
	FOR		${VALID_IP}	IN	@{VALID_IPS}
		SUITE:Add And Set IPSec Tunnel
		CLI:Test Set Field Invalid Options	monitoring_source_ip_address	${VALID_IP}	save=yes
		CLI:Cancel	Raw
	END
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	SUITE:Delete Tunnel With Monitoring	${NAME}

Test Invalid Values For Field=monitoring_source_ip_address
	FOR		${INVALID_IP}	IN	@{INVALID_IPS}
		SUITE:Add And Set IPSec Tunnel
		CLI:Test Set Field Invalid Options	monitoring_source_ip_address	${INVALID_IP}
		...	ERROR=Error: monitoring_source_ip_address: Invalid IP address.	save=yes
		CLI:Cancel	Raw
	END
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	SUITE:Delete Tunnel With Monitoring	${NAME}

Test Valid Values For Field=monitoring_number_of_retries
	${VALID_NUMBER_OF_RETRIES}=	create list	${NUMBER}	${ONE_NUMBER}
	...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
	FOR		${VALID_NUMBER_OF_RETRIE}	IN	@{VALID_NUMBER_OF_RETRIES}
		SUITE:Add And Set IPSec Tunnel
		CLI:Test Set Field Invalid Options	monitoring_number_of_retries	${VALID_NUMBER_OF_RETRIE}	save=yes
		CLI:Cancel	Raw
	END
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	SUITE:Delete Tunnel With Monitoring	${NAME}

Test Invalid Values For Field=monitoring_number_of_retries
	${INVALID_NUMBER_OF_RETRIES}=	create list	@{INVALID_IPS}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
	...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
	...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
	...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}
	FOR		${INVALID_NUMBER_OF_RETRIE}	IN	@{INVALID_NUMBER_OF_RETRIES}
		SUITE:Add And Set IPSec Tunnel
		CLI:Test Set Field Invalid Options	monitoring_number_of_retries	${INVALID_NUMBER_OF_RETRIE}
		...	ERROR=Error: monitoring_number_of_retries: This field only accepts positive integers.	save=yes
		CLI:Cancel	Raw
	END
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	SUITE:Delete Tunnel With Monitoring	${NAME}

Test Valid Values For Field=monitoring_interval
	${VALID_INTERVALS}=	create list	${NUMBER}	${ONE_NUMBER}
	...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
	FOR		${VALID_INTERVAL}	IN	@{VALID_INTERVALS}
		SUITE:Add And Set IPSec Tunnel
		CLI:Test Set Field Invalid Options	monitoring_interval	${VALID_INTERVAL}	save=yes
		CLI:Cancel	Raw
	END
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	SUITE:Delete Tunnel With Monitoring	${NAME}

Test Invalid Values For Field=monitoring_interval
	${INVALID_INTERVALS}=	create list	@{INVALID_IPS}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}
	...	${POINTS}	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
	...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
	...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}
	FOR		${INVALID_INTERVAL}	IN	@{INVALID_INTERVALS}
		SUITE:Add And Set IPSec Tunnel
		CLI:Test Set Field Invalid Options	monitoring_interval	${INVALID_INTERVAL}
		...	ERROR=Error: monitoring_interval: This field only accepts positive integers.	save=yes
		CLI:Cancel	Raw
	END
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	SUITE:Delete Tunnel With Monitoring	${NAME}

Test Valid Values For Field=monitoring_action
	SUITE:Add And Set IPSec Tunnel
	${OUTPUT}=	CLI:Test Set Field Options	monitoring_action	failover	restart_ipsec	restart_tunnel
	Log	${OUTPUT}
	should be true	'${OUTPUT}' == 'None'
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	SUITE:Delete Tunnel With Monitoring	${NAME}

Test Invalid Values For Field=monitoring_action
	SUITE:Add And Set IPSec Tunnel
	CLI:Test Set Field Invalid Options	monitoring_action	${EMPTY}
	...	Error: Missing value for parameter: monitoring_action
	FOR		${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	monitoring_action	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: monitoring_action
	END
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	SUITE:Delete Tunnel With Monitoring	${NAME}

Test Valid Values For Field=failover_ipsec_tunnel
	SUITE:Add And Set IPSec Tunnel
	CLI:Commit
	SUITE:Add And Set IPSec Tunnel
	CLI:Set	monitoring_action=failover
	CLI:Test Show Command	monitoring_failover_ipsec_tunnel = ${NAME}
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	SUITE:Delete Tunnel With Monitoring	${NAME}

Test Invalid Values For Field=failover_ipsec_tunnel
	SUITE:Add And Set IPSec Tunnel
	CLI:Commit
	SUITE:Add And Set IPSec Tunnel
	CLI:Set	monitoring_action=failover
	CLI:Test Set Field Invalid Options	monitoring_failover_ipsec_tunnel	${EMPTY}
	...	Error: Missing value for parameter: monitoring_failover_ipsec_tunnel
	FOR		${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	monitoring_failover_ipsec_tunnel	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: monitoring_failover_ipsec_tunnel
	END
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Delete Tunnel With Monitoring	${NAME}

SUITE:Teardown
	SUITE:Delete Tunnel With Monitoring	${NAME}
	CLI:Close Connection


SUITE:Delete Tunnel With Monitoring
	[Arguments]	${TUNNEL_NAME}
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Delete If Exists	${TUNNEL_NAME}
	CLI:Commit

SUITE:Add And Set IPSec Tunnel
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Add
	CLI:Set	name=${NAME} initiate_tunnel=start ike_profile=nodegrid
	CLI:Set	authentication_method=pre-shared_key secret=${IPSEC_SECRET} left_id=${IPSEC_NAME_LEFT}
	CLI:Set	left_address=%defaultroute right_id=${IPSEC_NAME_RIGHT} right_address=${HOST}
	CLI:Set	left_source_ip_address=${HOSTPEER} enable_monitoring=yes monitoring_source_ip_address=${HOST}
	CLI:Set	monitoring_destination_ip_address=${HOSTPEER}