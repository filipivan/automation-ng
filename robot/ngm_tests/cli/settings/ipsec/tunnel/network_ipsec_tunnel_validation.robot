*** Settings ***
Resource	../../../init.robot
Documentation	Tests Settings > Network IPsec > Tunnel Validation through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NAME}	automation
${IPSEC_SECRET}	automation
${IPSEC_NAME_RIGHT}	right
${RIGHT_IP}	10.20.30.40
${RSA_KEY}	0sAwEAAbyp1Fh9lQ/iRvKbGTuDB4V6xs5mFasYHSr0h+2nI83PjqLkRwHfdO77ZTUnR7CTnbKJC6fPkZByb/95pH3GpQMSR71v3RmBYCAysmSe3U61pvB3+RuN39ZeJMqyKA6yuJtY0boa2tRNEEppGvrje/SP7nQdyEvM3QdTwjUOSUsZ1kcQ5dFq7MF/vdgkQOaFebZ+ZEjHeCOPJU9btXSS0QHk1jfC2LzWmxFb5eGsU9udZ9VlVRl0xTSHgvdpEN7xbhPNdZ4UApCGc5c8N0wvHQed9l/FZ6Mq/MAToJxwSsS4WkJW24DfOEDTPRGlEYCceDmWG61HE9YQAwvnvJU6Lc2JqsVkw3FA1Qy84C6q2ttefcc+MquVJWhsxUFymc5VFnCmwiGc4zUriTJ3ncfQknLsJ4KBQ8HuEo3mVK44/wGNRsSLmvNUMpfNsbVA7Y2RLc2mBSJPBRLvVQa2D+ZColvxNFD5N9xCQpVlMSrAnodrUgLw6FEBBQGPgUqOZqURWUUXxw3uApu82chgz04Zx24M+5X5Nn8HehZmyNWjVYaBhg/I4+F4LOVYDw==
@{VALID_IPS}	${RIGHT_IP}	2001:db8:cafe::1

*** Test Cases ***
Test Ls Command
	[Documentation]	Tests the 3 subpaths under /settings/ipsec
	CLI:Enter Path	/settings/ipsec/
	CLI:Test Ls Command	tunnel/	ike_profile/	global/

Test Show Command Under Tunnel Path
	[Documentation]	Tests the default values of the fields under /settings/ipsec/tunnel
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Add
	CLI:Test Show Command	name =	initiate_tunnel = start	ike_profile = nodegrid	authentication_method = pre-shared_key
	...	secret =	left_id =	left_address = %defaultroute	left_ip_address =	left_source_ip_address =
	...	left_subnet =	right_id =	right_address =	right_source_ip_address =	right_subnet =
	[Teardown]	CLI:Cancel	Raw

Test Available Commands After Send tab-tab Under Tunnel Path
	[Documentation]	Tests avaliable commands to use under /settings/ipsec/tunnel
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Test Available Commands	add	commit	event_system_audit	factory_settings	quit	set	shutdown	system_certificate
	...	apply_settings	create_csr	event_system_clear	hostname	reboot	shell	software_upgrade	system_config_check
	...	cd	delete	exec	ls	revert	show	start_tunnel	whoami	cloud_enrollment	diagnostic_data	exit	pwd
	...	save_settings	show_settings	stop_tunnel

Test Available Commands After Send Add Under Tunnel Path
	[Documentation]	Tests avaliable commands to use under /settings/ipsec/tunnel when adding a tunnel
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Add
	CLI:Test Available Commands	cancel	commit	ls	save	set	show
	[Teardown]	CLI:Cancel	Raw

Test show_settings Command Under Tunnel Path
	[Documentation]	Tests settings under /settings/ipsec/tunnel
	CLI:Enter Path	/settings/ipsec/tunnel/
	${OUTPUT}=	CLI:Show Settings
	@{SHOW_SETTINGS} =	Create List	/settings/ipsec/tunnel/${NAME} name=	/settings/ipsec/tunnel/${NAME} initiate_tunnel=start
	...	/settings/ipsec/tunnel/${NAME} ike_profile=nodegrid	/settings/ipsec/tunnel/${NAME} authentication_method=pre-shared_key
	...	/settings/ipsec/tunnel/${NAME} secret=	/settings/ipsec/tunnel/${NAME} left_id=	/settings/ipsec/tunnel/${NAME} left_address=%defaultroute
	...	/settings/ipsec/tunnel/${NAME} right_id=	/settings/ipsec/tunnel/${NAME} right_address=	/settings/ipsec/tunnel/${NAME}
	FOR	${SETTING}	IN	@{SHOW_SETTINGS}
		Should Not Contain	${OUTPUT}	${SETTING}
	END

Test Available Set Fields Under Tunnel Path
	[Documentation]	Tests avaliable fields to set by default /settings/ipsec/tunnel
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Add
	CLI:TabTab Fill Set	initiate_tunnel	start	ignore	on_demand	start
	CLI:TabTab Fill Set	ike_profile	nodegrid	Cisco_ASA|cisco_asa	PaloAlto|paloalto	nodegrid
	CLI:TabTab Fill Set	authentication_method	pre-shared_key	pre-shared_key	rsa_key
	CLI:TabTab Fill Set	left_address	%defaultroute	%any	%eth0	ip_address
	[Teardown]	CLI:Cancel	Raw

Test valid values for field name
	[Documentation]	Tests that letters and combination of letters and numbers are valid as a tunnel name
	CLI:Enter Path	/settings/ipsec/tunnel
	FOR	${VALUE}	IN	${WORD}	${WORD_AND_NUMBER}
		CLI:Add
		CLI:Set	secret=${IPSEC_SECRET} right_address=${RIGHT_IP}
		CLI:Set	name=${VALUE}
		CLI:Commit
		CLI:Delete	${VALUE}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field name
	[Documentation]	Tests that name cannot be empty, cannot exceed maximum value,
	...	cannot be only numbers and cannot contain invalid characters
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Set	secret=${IPSEC_SECRET} right_address=${RIGHT_IP}
	CLI:Set	name=
	${OUTPUT}	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: name: Field must not be empty.
	CLI:Set	name=${EXCEEDED}
	${OUTPUT}	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: name: Exceeded the maximum size for this field.
	CLI:Set	name=${NUMBER}
	${OUTPUT}	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: name: Validation error
	CLI:Set	name=${POINTS}
	${OUTPUT}	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: name: This field contains invalid characters.
	[Teardown]	CLI:Cancel	Raw

Test valid values for field initiate_tunnel
	[Documentation]	Tests the three possibilities for initiate_tunnel
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Test Set Validate Normal Fields	initiate_tunnel	ignore	on_demand	start
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field initiate_tunnel
	[Documentation]	Tests initiate_tunnel cannot have letters, numbers and points
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Test Set Validate Invalid Options	initiate_tunnel	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: initiate_tunnel
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for ike_profile
	[Documentation]	Tests the default ike_profiles are valid
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Test Set Validate Normal Fields	ike_profile	nodegrid	Cisco_ASA	PaloAlto
	[Teardown]	CLI:Cancel	Raw

Test invalid values for ike_profile
	[Documentation]	Tests ike_profile cannot be set to some value not configured
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Test Set Validate Invalid Options	ike_profile	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: ike_profile
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for custom_up|down_script
	[Tags]	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4
	[Documentation]	Tests custom up/down scripts can be empty or the default shell script
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Test Set Validate Normal Fields	custom_up|down_script	${EMPTY}	route_based_vti_updown.sh
	[Teardown]	CLI:Cancel	Raw

Test invalid values for custom_up|down_script
	[Tags]	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4
	[Documentation]	Tests custom scripts cannot be set to some script not previously created
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Test Set Validate Invalid Options	custom_up|down_script	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: custom_up|down_script
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for authentication_method
	[Documentation]	Tests authentication mode could be via PSK or RSA keys
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Test Set Validate Normal Fields	authentication_method	pre-shared_key	rsa_key
	[Teardown]	CLI:Cancel	Raw

Test invalid values for authentication_method
	[Documentation]	Tests authentication_method cannot be set to other value than existing ones
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Test Set Validate Invalid Options	authentication_method	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: authentication_method
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for secret
	[Documentation]	Tests that secret field can have letters, numbers and points
	CLI:Enter Path	/settings/ipsec/tunnel
	FOR	${VALUE}	IN	${WORD}	${WORD_AND_NUMBER}	${NUMBER}	${POINTS}
		CLI:Add
		CLI:Set	name=${NAME} right_address=${RIGHT_IP} authentication_method=pre-shared_key
		CLI:Set	secret=${VALUE}
		CLI:Commit
		CLI:Delete	${NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for secret
	[Documentation]	Tests that secret cannot be empty
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Set	name=${NAME} right_address=${RIGHT_IP} authentication_method=pre-shared_key
	CLI:Set	secret=
	${OUTPUT}	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: secret: Field must not be empty.
	[Teardown]	CLI:Cancel	Raw

Test valid values for left_public_key
	[Documentation]	Tests that left_public_key only accepts valid ssh_keys
	CLI:Enter Path	/settings/ipsec/tunnel
	FOR	${VALUE}	IN	${WORD}	${WORD_AND_NUMBER}
		CLI:Add
		CLI:Set	name=${NAME} right_address=${RIGHT_IP} authentication_method=rsa_key
		CLI:Set	right_public_key=${RSA_KEY}
		CLI:Set	left_public_key=${RSA_KEY}
		CLI:Commit
		CLI:Delete	${NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for left_public_key
	[Documentation]	Tests that left_public_key cannot be anything but a valid ssh_key
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Set	name=${NAME} right_address=${RIGHT_IP} authentication_method=rsa_key
	CLI:Set	right_public_key=${RSA_KEY}
	${OUTPUT}	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: left_public_key: Field must not be empty.
	CLI:Set	left_public_key=${WORD}
	${OUTPUT}	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: left_public_key: Validation error.
	[Teardown]	CLI:Cancel	Raw

Test valid values for right_public_key
	[Documentation]	Tests that right_public_key only accepts valid ssh_keys
	CLI:Enter Path	/settings/ipsec/tunnel
	FOR	${VALUE}	IN	${WORD}	${WORD_AND_NUMBER}
		CLI:Add
		CLI:Set	name=${NAME} right_address=${RIGHT_IP} authentication_method=rsa_key
		CLI:Set	left_public_key=${RSA_KEY}
		CLI:Set	right_public_key=${RSA_KEY}
		CLI:Commit
		CLI:Delete	${NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for right_public_key
	[Documentation]	Tests that right_public_key cannot be anything but a valid ssh_key
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Set	name=${NAME} right_address=${RIGHT_IP} authentication_method=rsa_key
	CLI:Set	left_public_key=${RSA_KEY}
	${OUTPUT}	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: right_public_key: Field must not be empty.
	CLI:Set	right_public_key=${WORD}
	${OUTPUT}	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: right_public_key: Validation error.
	[Teardown]	CLI:Cancel	Raw

Test valid values for left_address
	[Documentation]	Tests all the possible interfaces (in a NGM) to set as left_address
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Test Set Validate Normal Fields	left_address	%any	%eth0	%loopback	%sit0	%defaultroute	%lo	%loopback0	ip_address
	[Teardown]	CLI:Cancel	Raw

Test invalid values for left_address
	[Documentation]	Tests left_address cannot be anything other than an IP address or a valid interface
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Test Set Validate Invalid Options	left_address	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: left_address
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for left_subnet
	[Documentation]	Tests that left_subnet field accepts syntaxes as netmask and CIDR
	CLI:Enter Path	/settings/ipsec/tunnel
	FOR	${VALUE}	IN	${RIGHT_IP}/24	${RIGHT_IP}/255.255.255.0
		CLI:Add
		CLI:Set	secret=${IPSEC_SECRET} right_address=${RIGHT_IP} name=${NAME}
		CLI:Set	left_subnet=${VALUE}
		CLI:Commit
		CLI:Delete	${NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for left_subnet
	[Documentation]	Tests that left_subnet does not accept letters, numbers and points
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Set	name=${NAME} right_address=${RIGHT_IP} secret=${IPSEC_SECRET}
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Set	left_subnet=${VALUE}
		${OUTPUT}	CLI:Commit	Raw
		Should Contain	${OUTPUT}	Error: left_subnet: Invalid subnet
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for left_id
	[Documentation]	Tests that left_id could be letters, numbers and points
	CLI:Enter Path	/settings/ipsec/tunnel
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Add
		CLI:Set	secret=${IPSEC_SECRET} right_address=${RIGHT_IP} name=${NAME}
		CLI:Set	left_id=${VALUE}
		CLI:Commit
		CLI:Delete	${NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for left_id
	[Documentation]	Tests that left_id cannot exceed the maximum value of the field
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Set	name=${NAME} right_address=${RIGHT_IP} secret=${IPSEC_SECRET}
	CLI:Set	left_id=${EXCEEDED}${EXCEEDED}${EXCEEDED}
	${OUTPUT}	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: left_id: Exceeded the maximum size for this field.
	[Teardown]	CLI:Cancel	Raw

Test valid values for left_ip_address
	[Documentation]	Tests that left_ip_address can accept IPv4 and IPv6 addresses
	CLI:Enter Path	/settings/ipsec/tunnel
	FOR	${VALUE}	IN	@{VALID_IPS}
		CLI:Add
		CLI:Set	secret=${IPSEC_SECRET} right_address=${RIGHT_IP} name=${NAME}
		CLI:Set	left_address=ip_address
		CLI:Set	left_ip_address=${VALUE}
		CLI:Commit
		CLI:Delete	${NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for left_ip_address
	[Documentation]	Tests that left_ip_address only accepts IPv4 and IPv6
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Set	name=${NAME} right_address=${RIGHT_IP} secret=${IPSEC_SECRET}
	CLI:Set	left_address=ip_address
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Set	left_ip_address=${VALUE}
		${OUTPUT}	CLI:Commit	Raw
		Should Contain	${OUTPUT}	Error: left_ip_address: Validation error.
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for left_source_ip_address
	[Documentation]	Tests that left_source_ip_address can accept IPv4 and IPv6 addresses
	CLI:Enter Path	/settings/ipsec/tunnel
	FOR	${VALUE}	IN	@{VALID_IPS}
		CLI:Add
		CLI:Set	secret=${IPSEC_SECRET} right_address=${RIGHT_IP} name=${NAME}
		CLI:Set	left_source_ip_address=${VALUE}
		CLI:Commit
		CLI:Delete	${NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for left_source_ip_address
	[Documentation]	Tests that left_source_ip_address only accepts IPv4 and IPv6
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Set	name=${NAME} right_address=${RIGHT_IP} secret=${IPSEC_SECRET}
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Set	left_source_ip_address=${VALUE}
		${OUTPUT}	CLI:Commit	Raw
		Should Contain	${OUTPUT}	Error: left_source_ip_address: Invalid IP address.
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for right_address
	[Documentation]	Tests that right_address accepts IPv4 and IPv6 addresses
	CLI:Enter Path	/settings/ipsec/tunnel
	FOR	${VALUE}	IN	@{VALID_IPS}
		CLI:Add
		CLI:Set	secret=${IPSEC_SECRET} name=${NAME}
		CLI:Set	right_address=${VALUE}
		CLI:Commit
		CLI:Delete	${NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for right_address
	[Documentation]	Tests that right_address only accepts IPv4 and IPv6
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Set	name=${NAME} secret=${IPSEC_SECRET}
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Set	right_address=${VALUE}
		${OUTPUT}	CLI:Commit	Raw
		Should Contain	${OUTPUT}	Error: right_address: Validation error.
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for right_subnet
	[Documentation]	Tests that right_subnet field accepts syntaxes as netmask and CIDR
	CLI:Enter Path	/settings/ipsec/tunnel
	FOR	${VALUE}	IN	${RIGHT_IP}/24	${RIGHT_IP}/255.255.255.0
		CLI:Add
		CLI:Set	secret=${IPSEC_SECRET} right_address=${RIGHT_IP} name=${NAME}
		CLI:Set	right_subnet=${VALUE}
		CLI:Commit
		CLI:Delete	${NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for right_subnet
	[Documentation]	Tests that right_subnet does not accept letters, numbers and points
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Set	name=${NAME} right_address=${RIGHT_IP} secret=${IPSEC_SECRET}
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Set	right_subnet=${VALUE}
		${OUTPUT}	CLI:Commit	Raw
		Should Contain	${OUTPUT}	Error: right_subnet: Invalid subnet
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for right_id
	[Documentation]	Tests that right_id can accept letters, numbers and points
	CLI:Enter Path	/settings/ipsec/tunnel
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Add
		CLI:Set	secret=${IPSEC_SECRET} right_address=${RIGHT_IP} name=${NAME}
		CLI:Set	right_id=${VALUE}
		CLI:Commit
		CLI:Delete	${NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for right_id
	[Documentation]	Tests that left_id cannot exceed the maximum value of the field
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Set	name=${NAME} right_address=${RIGHT_IP} secret=${IPSEC_SECRET}
	CLI:Set	right_id=${EXCEEDED}${EXCEEDED}${EXCEEDED}
	${OUTPUT}	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: right_id: Exceeded the maximum size for this field.
	[Teardown]	CLI:Cancel	Raw

Test valid values for right_source_ip_address
	[Documentation]	Tests that right_source_ip_address accepts IPv4 and IPv6 addresses
	CLI:Enter Path	/settings/ipsec/tunnel
	FOR	${VALUE}	IN	@{VALID_IPS}
		CLI:Add
		CLI:Set	secret=${IPSEC_SECRET} right_address=${RIGHT_IP} name=${NAME}
		CLI:Set	right_source_ip_address=${VALUE}
		CLI:Commit
		CLI:Delete	${NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for right_source_ip_address
	[Documentation]	Tests that right_source_ip_address does not accept letters, numbers and points
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Set	name=${NAME} right_address=${RIGHT_IP} secret=${IPSEC_SECRET}
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Set	right_source_ip_address=${VALUE}
		${OUTPUT}	CLI:Commit	Raw
		Should Contain	${OUTPUT}	Error: right_source_ip_address: Invalid IP address.
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for enable_virtual_tunnel_interface
	[Documentation]	Tests yes and no option for enable_virtual_tunnel_interface
	SUITE:Enable VTI In Global
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Test Set Validate Normal Fields	enable_virtual_tunnel_interface	yes	no
	[Teardown]	CLI:Cancel	Raw

Test invalid values for enable_virtual_tunnel_interface
	[Documentation]	Tests enable_virtual_tunnel_interface cannot be set to anything but yes or no
	SUITE:Enable VTI In Global
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Test Set Validate Invalid Options	enable_virtual_tunnel_interface	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: enable_virtual_tunnel_interface
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for vti_address
	[Documentation]	Tests that vti_address accepts IPv4 and IPv6 addresses with masks
	SUITE:Enable VTI In Global
	CLI:Enter Path	/settings/ipsec/tunnel
	FOR	${VALUE}	IN	${RIGHT_IP}/24	${RIGHT_IP}/255.255.255.0
		CLI:Add
		CLI:Set	secret=${IPSEC_SECRET} name=${NAME} right_address=${RIGHT_IP}
		CLI:Set	enable_virtual_tunnel_interface=yes vti_interface=${NAME}
		CLI:Set	vti_address=${VALUE}
		CLI:Commit
		CLI:Delete	${NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for vti_address
	[Documentation]	Tests that vti_address does not accept letters, numbers and points
	SUITE:Enable VTI In Global
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Set	name=${NAME} right_address=${RIGHT_IP} secret=${IPSEC_SECRET}
	CLI:Set	enable_virtual_tunnel_interface=yes vti_interface=${NAME}
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Set	vti_address=${VALUE}
		${OUTPUT}	CLI:Commit	Raw
		Should Contain	${OUTPUT}	Error: vti_address: Invalid IP address.
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for vti_interface
	[Documentation]	Tests that vti_interfaces accepts letters and numbers
	SUITE:Enable VTI In Global
	CLI:Enter Path	/settings/ipsec/tunnel
	FOR	${VALUE}	IN	${WORD}	${NUMBER}
		CLI:Add
		CLI:Set	name=${NAME} right_address=${RIGHT_IP} secret=${IPSEC_SECRET}
		CLI:Set	enable_virtual_tunnel_interface=yes vti_address=${RIGHT_IP}/24
		CLI:Set	vti_interface=${VALUE}
		CLI:Commit
		CLI:Delete	${NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for vti_interface
	[Documentation]	Tests that vti_interface does not accept points and cannot be empty
	SUITE:Enable VTI In Global
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Set	name=${NAME} right_address=${RIGHT_IP} secret=${IPSEC_SECRET}
	CLI:Set	enable_virtual_tunnel_interface=yes vti_address=${RIGHT_IP}/24
	FOR	${VALUE}	IN	${POINTS}
		CLI:Set	vti_interface=${VALUE}
		${OUTPUT}	CLI:Commit	Raw
		Should Contain	${OUTPUT}	Error: vti_interface: This field is empty or it contains invalid characters.
	END
	CLI:Set	vti_interface=${EMPTY}
	${OUTPUT}	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: vti_interface: Field must not be empty.
	[Teardown]	CLI:Cancel	Raw

Test valid values for automatically_create_vti_routes
	[Documentation]	Tests yes and no option for automatically_create_vti_routes
	SUITE:Enable VTI In Global
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Set	enable_virtual_tunnel_interface=yes
	CLI:Test Set Validate Normal Fields	automatically_create_vti_routes	yes	no
	[Teardown]	CLI:Cancel	Raw

Test invalid values for automatically_create_vti_routes
	[Documentation]	Tests automatically_create_vti_routes cannot be set to anything but yes or no
	SUITE:Enable VTI In Global
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Set	enable_virtual_tunnel_interface=yes
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Test Set Validate Invalid Options	automatically_create_vti_routes	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: automatically_create_vti_routes
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for share_vti_with_other_connections
	[Documentation]	Tests yes and no option for share_vti_with_other_connections
	SUITE:Enable VTI In Global
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Set	enable_virtual_tunnel_interface=yes
	CLI:Test Set Validate Normal Fields	share_vti_with_other_connections	yes	no
	[Teardown]	CLI:Cancel	Raw

Test invalid values for share_vti_with_other_connections
	[Documentation]	Tests share_vti_with_other_connections cannot be set to anything but yes or no
	SUITE:Enable VTI In Global
	CLI:Enter Path	/settings/ipsec/tunnel
	CLI:Add
	CLI:Set	enable_virtual_tunnel_interface=yes
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Test Set Validate Invalid Options	share_vti_with_other_connections	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: share_vti_with_other_connections
	END
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Enable VTI In Global
	CLI:Enter Path	/settings/ipsec/global
	CLI:Set	 enable_virtual_tunnel_interface=yes
	CLI:Commit

SUITE:Teardown
	CLI:Close Connection