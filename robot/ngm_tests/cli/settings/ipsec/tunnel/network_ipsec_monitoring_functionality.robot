*** Settings ***
Resource	../../../init.robot
Documentation	Tests Settings > Network IPsec monitoring functionality through the CLI
Metadata	Version	  1.0
Metadata	Executed At	  ${HOST}
Force Tags	CLI	EXCLUDEIN3_2  EXCLUDEIN4_2	NON-CRITICAL

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${PRE_SHARED_TUNNEL_NAME}	ng-pre
${RSA_TUNNEL_NAME}	ng-rsa
${IPSEC_NAME_LEFT}	@NG1
${IPSEC_NAME_RIGHT}	@NG2
${IPSEC_SECRET}	automation-test
${RSA_KEY_BITSIZE}	4096

${NEWHOSTKEY_COMMAND_OUTPUT_REGEX}	Generated RSA key pair with CKAID (\\w+) was stored in the NSS database
${SHOWHOSTKEY_COMMAND_OUTPUT_REGEX}	(?:left|right)rsasigkey=(.*)

${BYTES_RECEIVED_INT_LEFT}
${BYTES_SENT_INT_LEFT}
${FAILOVER_TUNNEL_NAME}   failover
@{MONITORING_ACTIONS}   restart_ipsec  restart_tunnel   failover
${LEFT_SOURCE_IP}  10.0.0.1
${RIGHT_SOURCE_IP}  10.0.0.2
${FAILOVER_LEFT_SOURCE_IP}  10.0.0.3
${FAILOVER_RIGHT_SOURCE_IP}  10.0.0.4

*** Test Cases ***
Test IPsec With Monitoring Using Pre Shared
	FOR	  ${MONITORING_ACTION}  IN  @{MONITORING_ACTIONS}
		SUITE:Setup Pre Shared Key Tunnel With Monitoring On Both Units  ${MONITORING_ACTION}
		SUITE:Check Initial Value Of Bytes Received and Sent  admin_right  ${PRE_SHARED_TUNNEL_NAME}
		SUITE:Creating Firewall For Drop Packets From Right Source Ip Address  ${PRE_SHARED_TUNNEL_NAME}
		SUITE:Wait Until Final Value of Bytes Received and Sent Are Greater Than Before	${PRE_SHARED_TUNNEL_NAME}
		SUITE:Wait Until Up Tunnel   ${PRE_SHARED_TUNNEL_NAME}
		SUITE:Deleting Firewall For Drop Packets From Right Source Ip Address  ${PRE_SHARED_TUNNEL_NAME}
		SUITE:Delete Tunnel	${PRE_SHARED_TUNNEL_NAME}
		SUITE:Delete Tunnel	failover
	END
    [Teardown]  SUITE:Delete All Tunnels And Firewall  ${PRE_SHARED_TUNNEL_NAME}

Test Monitoring Action Restart IPSec
	SUITE:Setup Pre Shared Key Tunnel With Monitoring On Both Units  restart_ipsec
    SUITE:Creating Firewall For Drop Packets From Right Source Ip Address  ${PRE_SHARED_TUNNEL_NAME}
    SUITE:Restart IPsec By Root
    SUITE:Check If After Number Of Retries The Tunnel Or IPSec Was Restarted  admin_right  ${PRE_SHARED_TUNNEL_NAME}
	[Teardown]  SUITE:Delete All Tunnels And Firewall  ${PRE_SHARED_TUNNEL_NAME}

Test Monitoring Action Restart Tunnel
	SUITE:Setup Pre Shared Key Tunnel With Monitoring On Both Units  restart_tunnel
	SUITE:Creating Firewall For Drop Packets From Right Source Ip Address  ${PRE_SHARED_TUNNEL_NAME}
    SUITE:Check If After Number Of Retries The Tunnel Or IPSec Was Restarted  admin_right  ${PRE_SHARED_TUNNEL_NAME}
	[Teardown]  SUITE:Delete All Tunnels And Firewall  ${PRE_SHARED_TUNNEL_NAME}

Test Monitoring Action failover
	SUITE:Setup Pre Shared Key Tunnel With Monitoring On Both Units  failover
    SUITE:Creating Firewall For Drop Packets From Right Source Ip Address  ${PRE_SHARED_TUNNEL_NAME}
    SUITE:Check If After Number Of Retries The IPSec Was Restarted and Just Failover Continued
    ...  admin_right  ${PRE_SHARED_TUNNEL_NAME}  ${FAILOVER_TUNNEL_NAME}
	SUITE:Creating Firewall For Drop Packets From Right Source Ip Address  ${FAILOVER_TUNNEL_NAME}
	[Teardown]  SUITE:Delete All Tunnels And Firewall  ${PRE_SHARED_TUNNEL_NAME}

Test IPsec With Monitoring Using RSA Key
	SUITE:Setup RSA Key Tunnel With Monitoring On Both Units   restart_tunnel
	SUITE:Check Initial Value Of Bytes Received and Sent    admin_right   ${RSA_TUNNEL_NAME}
	SUITE:Wait Until Final Value of Bytes Received and Sent Are Greater Than Before	${RSA_TUNNEL_NAME}
	SUITE:Creating Firewall For Drop Packets From Right Source Ip Address  ${RSA_TUNNEL_NAME}
    SUITE:Wait Until Up Tunnel   ${RSA_TUNNEL_NAME}
	[Teardown]  SUITE:Delete All Tunnels And Firewall  ${RSA_TUNNEL_NAME}

*** Keywords ***
SUITE:Setup
	CLI:Open	session_alias=admin_left	HOST_DIFF=${HOST}
	CLI:Open	session_alias=admin_right	HOST_DIFF=${HOSTPEER}
	SUITE:Delete Tunnel	failover
	SUITE:Delete Tunnel	${PRE_SHARED_TUNNEL_NAME}
	SUITE:Delete Tunnel	${RSA_TUNNEL_NAME}
	SUITE:Set Global Configuration	admin_right	no	no
	SUITE:Set Global Configuration	admin_left	no	no

SUITE:Teardown
	SUITE:Set Global Configuration	admin_left	yes	yes
	SUITE:Set Global Configuration	admin_right	yes	yes
	SUITE:Delete Tunnel	failover
	SUITE:Delete Tunnel	${PRE_SHARED_TUNNEL_NAME}
	SUITE:Delete Tunnel	${RSA_TUNNEL_NAME}
	SUITE:Deleting Firewall For Drop Packets From Right Source Ip Address  ${PRE_SHARED_TUNNEL_NAME}
	SUITE:Deleting Firewall For Drop Packets From Right Source Ip Address  ${FAILOVER_TUNNEL_NAME}
	SUITE:Deleting Firewall For Drop Packets From Right Source Ip Address  ${RSA_TUNNEL_NAME}
	CLI:Close Connection

SUITE:Delete Tunnel
	[Arguments]	${TUNNEL_NAME}
	CLI:Switch Connection	admin_right
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Delete If Exists	${TUNNEL_NAME}
	CLI:Commit
	CLI:Switch Connection	admin_left
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Delete If Exists	${TUNNEL_NAME}
	CLI:Commit

CLI:Delete If Exists On Show Command
	[Arguments]	@{VALUES}
	${OUTPUT}=    CLI:Show
	FOR		${VALUE}	IN	@{VALUES}
		${RESULT}=	Run Keyword And Return Status	Should Contain    ${OUTPUT}    @{VALUES}
		Run Keyword If	${RESULT}	CLI:Delete	0
	END

SUITE:Set Global Configuration
	[Arguments]	${CONNECTION}	${LOGGING}	${VIRTUAL_TUNNEL_INTERFACE}
	CLI:Switch Connection	${CONNECTION}
	CLI:Enter Path	/settings/ipsec/global/
	CLI:Set	enable_logging=${LOGGING} enable_virtual_tunnel_interface=${VIRTUAL_TUNNEL_INTERFACE}
	CLI:Commit

SUITE:Setup Pre Shared Key Tunnel With Monitoring On Both Units
	[Arguments]  ${MONITORING_ACTION}
	SUITE:Create IPsec Tunnel With Pre Shared Key And Monitoring  admin_left  ${PRE_SHARED_TUNNEL_NAME}
	...  ${IPSEC_NAME_LEFT}  ${IPSEC_NAME_RIGHT}  ${HOST}  ${HOSTPEER}  ${MONITORING_ACTION}  ${LEFT_SOURCE_IP}
	...  ${RIGHT_SOURCE_IP}  ${FAILOVER_LEFT_SOURCE_IP}  ${FAILOVER_RIGHT_SOURCE_IP}
	SUITE:Create IPsec Tunnel With Pre Shared Key And Monitoring  admin_right  ${PRE_SHARED_TUNNEL_NAME}
	...  ${IPSEC_NAME_RIGHT}  ${IPSEC_NAME_LEFT}  ${HOSTPEER}  ${HOST}  ${MONITORING_ACTION}   ${RIGHT_SOURCE_IP}
	...  ${LEFT_SOURCE_IP}  ${FAILOVER_RIGHT_SOURCE_IP}  ${FAILOVER_LEFT_SOURCE_IP}

SUITE:Setup RSA Key Tunnel With Monitoring On Both Units
    [Arguments]  ${MONITORING_ACTION}
    ${LEFT_KEY}=	SUITE:Generate RSA Key	${HOST}
	${RIGHT_KEY}=	SUITE:Generate RSA Key	${HOSTPEER}	False
	SUITE:Create IPsec Tunnel With RSA Key And Monitoring	admin_left	${RSA_TUNNEL_NAME}	${IPSEC_NAME_LEFT}
	...	${IPSEC_NAME_RIGHT}	${HOST}	${HOSTPEER}	${LEFT_KEY}	${RIGHT_KEY}  ${MONITORING_ACTION}  ${LEFT_SOURCE_IP}
	...  ${RIGHT_SOURCE_IP}
	SUITE:Create IPsec Tunnel With RSA Key And Monitoring	admin_right	${RSA_TUNNEL_NAME}	${IPSEC_NAME_RIGHT}
	...	${IPSEC_NAME_LEFT}	${HOSTPEER}	${HOST}	${RIGHT_KEY}	${LEFT_KEY}  ${MONITORING_ACTION}   ${RIGHT_SOURCE_IP}
	...  ${LEFT_SOURCE_IP}

SUITE:Create IPsec Tunnel With Pre Shared Key And Monitoring
	[Arguments]  ${CONNECTION}  ${TUNNEL_NAME}  ${LEFT_ID}  ${RIGHT_ID}
	...  ${LEFT_ADDRESS}  ${RIGHT_ADDRESS}  ${MONITORING_ACTION}  ${LEFT_SOURCE_IP}
	...  ${RIGHT_SOURCE_IP}  ${FAILOVER_LEFT_SOURCE_IP}  ${FAILOVER_RIGHT_SOURCE_IP}
	CLI:Switch Connection	${CONNECTION}
	Run Keyword If    '${MONITORING_ACTION}' == 'failover'	 SUITE:Set IPsec Tunnel With Monitoring  ${CONNECTION}
	...  ${FAILOVER_TUNNEL_NAME}  ${LEFT_ID}  ${RIGHT_ID}  ${LEFT_ADDRESS}  ${RIGHT_ADDRESS}  ${MONITORING_ACTION}
	...  ${LEFT_SOURCE_IP}  ${RIGHT_SOURCE_IP}  ${FAILOVER_LEFT_SOURCE_IP}  ${FAILOVER_RIGHT_SOURCE_IP}
	Run Keyword If    '${MONITORING_ACTION}' == 'failover'  Run Keyword If  '${CONNECTION}' == 'admin_left'
	...  CLI:Set  monitoring_action=restart_tunnel
	Run Keyword If    '${MONITORING_ACTION}' == 'failover'  CLI:Commit
	SUITE:Set IPsec Tunnel With Monitoring  ${CONNECTION}  ${TUNNEL_NAME}  ${LEFT_ID}  ${RIGHT_ID}
	...  ${LEFT_ADDRESS}  ${RIGHT_ADDRESS}  ${MONITORING_ACTION}  ${LEFT_SOURCE_IP}
	...  ${RIGHT_SOURCE_IP}  ${FAILOVER_LEFT_SOURCE_IP}  ${FAILOVER_RIGHT_SOURCE_IP}
	CLI:Commit
	CLI:Test Show Command	${TUNNEL_NAME}
	Run Keyword If    '${MONITORING_ACTION}' == 'failover'	 Run Keyword If  '${CONNECTION}' == 'admin_left'
	...  CLI:Write   cd ${TUNNEL_NAME}
	Run Keyword If    '${MONITORING_ACTION}' == 'failover'	 Run Keyword If  '${CONNECTION}' == 'admin_left'
	...  CLI:Test Show Command  monitoring_failover_ipsec_tunnel = failover
	Run Keyword If    '${MONITORING_ACTION}' == 'failover'	 Run Keyword If  '${CONNECTION}' == 'admin_left'
	...  CLI:Write  cancel
	Run Keyword If    '${MONITORING_ACTION}' == 'failover'   Run Keyword If  '${CONNECTION}' == 'admin_left'
	...  CLI:Enter Path	 /settings/ipsec/tunnel/
	Run Keyword If    '${MONITORING_ACTION}' == 'failover'	 Run Keyword If  '${CONNECTION}' == 'admin_left'
	...  CLI:Write   cd ${FAILOVER_TUNNEL_NAME}
	Run Keyword If    '${MONITORING_ACTION}' == 'failover'   Run Keyword If  '${CONNECTION}' == 'admin_left'
	...  CLI:Test Show Command  monitoring_failover_ipsec_tunnel = ${TUNNEL_NAME}
	Run Keyword If    '${MONITORING_ACTION}' == 'failover'	 Run Keyword If  '${CONNECTION}' == 'admin_left'
	...  CLI:Write  cancel

SUITE:Set IPsec Tunnel With Monitoring
	[Arguments]  ${CONNECTION}  ${TUNNEL_NAME}  ${LEFT_ID}  ${RIGHT_ID}
	...  ${LEFT_ADDRESS}  ${RIGHT_ADDRESS}  ${MONITORING_ACTION}  ${LEFT_SOURCE_IP}
	...  ${RIGHT_SOURCE_IP}  ${FAILOVER_LEFT_SOURCE_IP}  ${FAILOVER_RIGHT_SOURCE_IP}
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Add
	CLI:Set	name=${TUNNEL_NAME} initiate_tunnel=start ike_profile=nodegrid
	CLI:Set	authentication_method=pre-shared_key secret=${IPSEC_SECRET}
	CLI:Set	left_id=${LEFT_ID} left_address=ip_address left_ip_address=${LEFT_ADDRESS}
	CLI:Set	left_source_ip_address=${LEFT_SOURCE_IP} right_id=${RIGHT_ID} right_address=${RIGHT_ADDRESS}
	CLI:Set	right_source_ip_address=${RIGHT_SOURCE_IP}
	Run Keyword If    '${MONITORING_ACTION}' == 'failover'  Run Keyword If  '${TUNNEL_NAME}' == '${FAILOVER_TUNNEL_NAME}'
	...  CLI:Set  left_source_ip_address=${FAILOVER_LEFT_SOURCE_IP} right_source_ip_address=${FAILOVER_RIGHT_SOURCE_IP}
	Run Keyword If    '${CONNECTION}' == 'admin_left'
	...  CLI:Set  enable_monitoring=yes monitoring_source_ip_address=${LEFT_SOURCE_IP}
	Run Keyword If    '${CONNECTION}' == 'admin_left'
	...  CLI:Set  monitoring_destination_ip_address=${RIGHT_SOURCE_IP} monitoring_interval=5
	Run Keyword If    '${CONNECTION}' == 'admin_left'  Run Keyword If  '${TUNNEL_NAME}' == '${FAILOVER_TUNNEL_NAME}'
	...  CLI:Set  monitoring_source_ip_address=${FAILOVER_LEFT_SOURCE_IP} monitoring_destination_ip_address=${FAILOVER_RIGHT_SOURCE_IP}
	Run Keyword If    '${CONNECTION}' == 'admin_left'  CLI:Set  monitoring_action=${MONITORING_ACTION}

SUITE:Check Initial Value Of Bytes Received and Sent
	[Arguments]	${CONNECTION}	${TUNNEL_NAME}
	CLI:Switch Connection	${CONNECTION}
	CLI:Enter Path	/system/ipsec_table/
	Wait Until Keyword Succeeds	1m	3s	CLI:Test Show Command	${TUNNEL_NAME}
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	${TUNNEL_NAME}
	@{GET_LINES}=	Split To Lines	${OUTPUT}
	${LINE}=	Get From List	${GET_LINES}	2
	${BYTES_RECEIVED}=	Get Substring	${LINE}	82	93
	${BYTES_SENT}=	Get Substring	${LINE}	66	81
	${BYTES_RECEIVED_INT}=	Convert To Integer	${BYTES_RECEIVED}
	${BYTES_SENT_INT}=	Convert To Integer	${BYTES_SENT}
	Set Suite Variable	${BYTES_RECEIVED_INT_LEFT}	${BYTES_RECEIVED_INT}
	Set Suite Variable	${BYTES_SENT_INT_LEFT}	${BYTES_SENT_INT}

SUITE:Wait Until Final Value of Bytes Received and Sent Are Greater Than Before
	[Arguments]	${TUNNEL_NAME}
	Wait Until Keyword Succeeds	3m	3s
	...	SUITE:Final Value of Bytes Received and Sent Should Be Greater Than Before	admin_left	${TUNNEL_NAME}	${BYTES_SENT_INT_LEFT}	${BYTES_RECEIVED_INT_LEFT}

SUITE:Final Value of Bytes Received and Sent Should Be Greater Than Before
	[Arguments]	${CONNECTION}	${TUNNEL_NAME}	${CURRENT_BYTES_SENT}	${CURRENT_BYTES_RECEIVED}
	CLI:Switch Connection	${CONNECTION}
	CLI:Enter Path	/system/ipsec_table/
	${OUTPUT}=	CLI:Show
	@{GET_LINES}=	Split To Lines	${OUTPUT}
	${LINE}=	Get From List	${GET_LINES}	2
	${BYTES_RECEIVED}=	Get Substring	${LINE}  66  81
	${BYTES_RECEIVED_INT}=	Convert To Integer	${BYTES_RECEIVED}
	${BYTES_SENT}=	Get Substring	${LINE}  82  93
	${BYTES_SENT_INT}=	Convert To Integer	${BYTES_SENT}
#	Should Be True	${BYTES_RECEIVED_INT} != 0
	Should Be True  ${BYTES_SENT_INT} != 0

SUITE:Check If After Number Of Retries The Tunnel Or IPSec Was Restarted
	[Arguments]	${CONNECTION}	${TUNNEL_NAME}
	CLI:Switch Connection	${CONNECTION}
	CLI:Enter Path	/system/ipsec_table/
	Wait Until Keyword Succeeds	1m	3s	CLI:Test Show Command	168
	Wait Until Keyword Succeeds	1m	3s	CLI:Test Show Command	84

SUITE:Check If After Number Of Retries The IPSec Was Restarted and Just Failover Continued
	[Arguments]	${CONNECTION}	${TUNNEL_NAME}  ${FAILOVER_TUNNEL_NAME}
	CLI:Switch Connection	${CONNECTION}
	CLI:Enter Path	/system/ipsec_table/
	Wait Until Keyword Succeeds	1m	3s	CLI:Test Not Show Command  ${TUNNEL_NAME}
	Wait Until Keyword Succeeds	1m	3s	CLI:Test Show Command	${FAILOVER_TUNNEL_NAME}

SUITE:Generate RSA Key
	[Arguments]	${HOST_ADDR}	${LEFT}=True
	CLI:Connect As Root	${HOST_ADDR}
	Set Client Configuration	prompt=:~#
	CLI:Write	rm /etc/ipsec/ipsec.d/${RSA_TUNNEL_NAME}.secrets
	CLI:Write	ipsec initnss	Yes
	${OUTPUT}=	CLI:Write	ipsec newhostkey --bits ${RSA_KEY_BITSIZE} --output /etc/ipsec/ipsec.d/${RSA_TUNNEL_NAME}.secrets
	${MATCH}	${CKAID}=	Should Match Regexp	${OUTPUT}	${NEWHOSTKEY_COMMAND_OUTPUT_REGEX}
	${OUTPUT}=	Run Keyword If	${LEFT} == True
	...	CLI:Write	ipsec showhostkey --left --ckaid ${CKAID}
	...	ELSE	CLI:Write	ipsec showhostkey --right --ckaid ${CKAID}
	${MATCH}	${RSA_PUBLIC_KEY}=	Should Match Regexp	${OUTPUT}	${SHOWHOSTKEY_COMMAND_OUTPUT_REGEX}
	[Return]	${RSA_PUBLIC_KEY}

SUITE:Create IPsec Tunnel With RSA Key And Monitoring
	[Arguments]	${CONNECTION}	${TUNNEL_NAME}	${LEFT_ID}	${RIGHT_ID}
	...	${LEFT_ADDRESS}  ${RIGHT_ADDRESS}  ${LEFT_RSA_KEY}	${RIGHT_RSA_KEY}  ${MONITORING_ACTION}  ${LEFT_SOURCE_IP}
	...  ${RIGHT_SOURCE_IP}
	CLI:Switch Connection	${CONNECTION}
	CLI:Enter Path	/settings/ipsec/tunnel/
	CLI:Add
	CLI:Set	name=${TUNNEL_NAME} initiate_tunnel=start ike_profile=nodegrid
	CLI:Set	authentication_method=rsa_key
	CLI:Set	left_public_key=${LEFT_RSA_KEY}
	CLI:Set	right_public_key=${RIGHT_RSA_KEY}
	CLI:Set	left_id=${LEFT_ID} left_address=ip_address left_ip_address=${LEFT_ADDRESS}
	CLI:Set	left_source_ip_address=${LEFT_SOURCE_IP} right_id=${RIGHT_ID} right_address=${RIGHT_ADDRESS}
	CLI:Set	right_source_ip_address=${RIGHT_SOURCE_IP}
	Run Keyword If    '${CONNECTION}' == 'admin_left'
	...  CLI:Set  enable_monitoring=yes monitoring_source_ip_address=${LEFT_SOURCE_IP}
	Run Keyword If    '${CONNECTION}' == 'admin_left'
	...  CLI:Set  monitoring_destination_ip_address=${RIGHT_SOURCE_IP} monitoring_interval=5
	Run Keyword If    '${CONNECTION}' == 'admin_left'  CLI:Set  monitoring_action=${MONITORING_ACTION}
	CLI:Commit
	CLI:Show	${RSA_TUNNEL_NAME}
	CLI:Test Show Command	${TUNNEL_NAME}

SUITE:Wait Until Up Tunnel
	[Arguments]		${TUNNEL_NAME}
	CLI:Switch Connection	admin_right
	CLI:Enter Path	/settings/ipsec/tunnel/
	Wait Until Keyword Succeeds	1m	3s	CLI:Test Show Command	Up
	CLI:Switch Connection	admin_left
	CLI:Enter Path	/settings/ipsec/tunnel/
	Wait Until Keyword Succeeds	1m	3s	CLI:Test Show Command	Up

SUITE:Creating Firewall For Drop Packets From Right Source Ip Address
	[Arguments]		${TUNNEL_NAME}
	CLI:Switch Connection	admin_left
	CLI:Enter Path	 /settings/ipv4_firewall/chains/INPUT
	CLI:Add
	Run Keyword If    '${TUNNEL_NAME}' == '${PRE_SHARED_TUNNEL_NAME}'
	...  CLI:Set  target=DROP source_net4=${RIGHT_SOURCE_IP} rule_number=0
	Run Keyword If    '${TUNNEL_NAME}' == '${RSA_TUNNEL_NAME}'
	...  CLI:Set  target=DROP source_net4=${RIGHT_SOURCE_IP} rule_number=0
	CLI:Commit
	CLI:Test Show Command	${RIGHT_SOURCE_IP}

SUITE:Deleting Firewall For Drop Packets From Right Source Ip Address
	[Arguments]		${TUNNEL_NAME}
	CLI:Switch Connection	admin_left
	CLI:Enter Path	 /settings/ipv4_firewall/chains/INPUT
	Run Keyword If    '${TUNNEL_NAME}' == '${PRE_SHARED_TUNNEL_NAME}'
	...  CLI:Delete If Exists On Show Command	${RIGHT_SOURCE_IP}
	Run Keyword If    '${TUNNEL_NAME}' == '${RSA_TUNNEL_NAME}'
    ...  CLI:Delete If Exists On Show Command	${RIGHT_SOURCE_IP}

SUITE:Delete All Tunnels And Firewall
    [Arguments]		${TUNNEL_NAME}
    SUITE:Deleting Firewall For Drop Packets From Right Source Ip Address  ${TUNNEL_NAME}
    SUITE:Deleting Firewall For Drop Packets From Right Source Ip Address  ${FAILOVER_TUNNEL_NAME}
    SUITE:Delete Tunnel  ${TUNNEL_NAME}
	SUITE:Delete Tunnel	failover

SUITE:Restart IPsec By Root
    CLI:Open            session_alias=admin_left	HOST_DIFF=${HOST}
    CLI:Connect As Root
    Write               /etc/init.d/ipsec restart
    CLI:Open            session_alias=admin_right	HOST_DIFF=${HOSTPEER}  startping=no
    CLI:Connect As Root
    Write               /etc/init.d/ipsec restart
