*** Settings ***
Resource	../../../init.robot
Documentation	Tests Settings > Network IPsec > Global Validation through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test Show Command Under Global Path
	[Documentation]	Tests fields shown under ipsec/global path
	CLI:Enter Path	/settings/ipsec/global/
	CLI:Test Show Command	enable_virtual_tunnel_interface	enable_logging

Test Available Commands After Send tab-tab Under Global Path
	[Documentation]	Tests all possible commands under ipsec/global path
	CLI:Enter Path	/settings/ipsec/global/
	CLI:Test Available Commands	apply_settings	commit	event_system_audit	exit	ls	reboot	set	show_settings
	...	system_certificate	cd	create_csr	event_system_clear	factory_settings	pwd	revert	shell	shutdown
	...	system_config_check	cloud_enrollment	diagnostic_data	exec	hostname	quit	save_settings	show
	...	whoami	software_upgrade

Test valid values for enable_logging
	[Documentation]	Tests options yes and no for field enable_logging
	CLI:Enter Path	/settings/ipsec/global
	CLI:Test Set Validate Normal Fields	enable_logging	yes	no
	[Teardown]	CLI:Cancel	Raw

Test invalid values for enable_logging
	[Documentation]	Tests the error message returned when invalid values are set for enable_logging
	CLI:Enter Path	/settings/ipsec/global
	CLI:Test Set Field Invalid Options	enable_logging	${WORD}
	...	Error: Invalid value: ${WORD} for parameter: enable_logging
	CLI:Test Set Field Invalid Options	enable_logging	${NUMBER}
	...	Error: Invalid value: ${NUMBER} for parameter: enable_logging
	CLI:Test Set Field Invalid Options	enable_logging	${POINTS}
	...	Error: Invalid value: ${POINTS} for parameter: enable_logging
	[Teardown]	CLI:Cancel	Raw

Test valid values for enable_virtual_tunnel_interface
	[Documentation]	Tests options yes and no for field enable_virtual_tunnel_interface
	CLI:Enter Path	/settings/ipsec/global
	CLI:Test Set Validate Normal Fields	enable_virtual_tunnel_interface	yes	no
	[Teardown]	CLI:Cancel	Raw

Test invalid values for enable_virtual_tunnel_interface
	[Documentation]	Tests the error message returned when invalid values are set for enable_virtual_tunnel_interface
	CLI:Enter Path	/settings/ipsec/global
	CLI:Test Set Field Invalid Options	enable_virtual_tunnel_interface	${WORD}
	...	Error: Invalid value: ${WORD} for parameter: enable_virtual_tunnel_interface
	CLI:Test Set Field Invalid Options	enable_virtual_tunnel_interface	${NUMBER}
	...	Error: Invalid value: ${NUMBER} for parameter: enable_virtual_tunnel_interface
	CLI:Test Set Field Invalid Options	enable_virtual_tunnel_interface	${POINTS}
	...	Error: Invalid value: ${POINTS} for parameter: enable_virtual_tunnel_interface
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection