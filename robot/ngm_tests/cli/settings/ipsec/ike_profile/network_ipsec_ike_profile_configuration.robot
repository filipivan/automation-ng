*** Settings ***
Resource	../../../init.robot
Documentation	Tests Settings > Network IPsec > IKE Profile Configurations through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2

Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
${NAME}	automation

*** Test Cases ***
Test Add ike profile
	[Documentation]	Adds new ike_profile and checks it is shown in table
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Set	profile_name=${NAME} ike_version=ikev1
	CLI:Commit
	${PROFILES}	CLI:Show
	Should Contain	${PROFILES}	${NAME}
	[Teardown]	CLI:Delete If Exists	${NAME}

Test Edit ike profile
	[Documentation]	Edits ike_profile from version 1 to version 2 and check the changes with show command
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Set	profile_name=${NAME} ike_version=ikev1
	CLI:Commit
	${OUTPUT}	CLI:Show	${NAME}
	Should Contain	${OUTPUT}	profile_name: ${NAME}
	Should Contain	${OUTPUT}	ike_version = ikev1
	CLI:Set	${NAME} ike_version=ikev2
	CLI:Commit
	${OUTPUT}	CLI:Show	${NAME}
	Should Contain	${OUTPUT}	profile_name: ${NAME}
	Should Contain	${OUTPUT}	ike_version = ikev2
	[Teardown]	CLI:Delete If Exists	${NAME}

Test Delete ike profile
	[Documentation]	Deletes new ike_profile and checks it is not shown in table
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Set	profile_name=${NAME} ike_version=ikev1
	CLI:Commit
	CLI:Delete	${NAME}
	${OUTPUT}	CLI:Show
	Should Not Contain	${OUTPUT}	${NAME}

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection