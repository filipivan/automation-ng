*** Settings ***
Resource	../../../init.robot
Documentation	Tests Settings > Network IPsec > IKE Profile Validation through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
${NAME}	automation

*** Test Cases ***
Test Show Command Under Ike Profile Path
	[Tags]	NEED-REVIEW
	CLI:Enter Path	/settings/ipsec/ike_profile/
	CLI:Add
	CLI:Test Show Command	profile_name =	ike_version = ikev2	phase_1_mode = main	phase_1_encryption = 3des
	...	phase_1_authentication = md5	phase_1_diffie-hellman_group = group_2	phase_1_lifetime = 3600
	...	phase_2_authentication_protocol = esp	phase_2_encryption =	phase_2_authentication =
	...	phase_2_pfs_group = none	phase_2_lifetime = 28800	enable_dead_peer_detection = no	mtu =	custom_parameters =
	[Teardown]	CLI:Cancel	Raw

Test Available Commands After Send tab-tab Under Ike Profile Path
	CLI:Enter Path	/settings/ipsec/ike_profile/
	CLI:Test Available Commands	add	commit	event_system_audit	factory_settings	quit	set	shutdown	whoami
	...	apply_settings	create_csr	event_system_clear	hostname	reboot	shell	software_upgrade	cd	delete
	...	exec	ls	revert	show	system_certificate	cloud_enrollment	diagnostic_data	exit	pwd
	...	save_settings	show_settings	system_config_check

Test Available Commands After Send Add Under Ike Profile Path
	CLI:Enter Path	/settings/ipsec/ike_profile/
	CLI:Add
	CLI:Test Available Commands	cancel	commit	ls	save	set	show
	[Teardown]	CLI:Cancel	Raw

Test Available Set Fields Under Ike Profile Path
	CLI:Enter Path	/settings/ipsec/ike_profile/
	CLI:Add
	CLI:TabTab Fill Set	phase_1_mode	main	aggressive	main
	CLI:TabTab Fill Set	phase_1_encryption	3des	3des	aes	aes-ctr	aes-ctr192	aes-ctr256	aes-gcm	aes-gcm192	aes-gcm256	aes192	aes256
	CLI:TabTab Fill Set	phase_1_authentication	md5	md5	sha1	sha256	sha384	sha512
	[Teardown]	CLI:Cancel	Raw

Test valid values for profile_name
	[Documentation]	Tests that letters and combination of letters and numbers are valid as a ike_profile name
	CLI:Enter Path	/settings/ipsec/ike_profile
	FOR	${VALUE}	IN	${WORD}	${WORD_AND_NUMBER}
		CLI:Add
		CLI:Set	profile_name=${VALUE}
		CLI:Commit
		CLI:Delete	${VALUE}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for profile_name
	[Documentation]	Tests that profile_name cannot be empty and cannot contain invalid characters
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Set	profile_name=
	${OUTPUT}	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: profile_name: Field must not be empty.
	CLI:Set	profile_name=${NUMBER}
	${OUTPUT}	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: profile_name: Validation error
	[Teardown]	CLI:Cancel	Raw

Test valid values for ike_version
	[Documentation]	Tests the two possibilities for ike_version
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Test Set Validate Normal Fields	ike_version	ikev1	ikev2
	[Teardown]	CLI:Cancel	Raw

Test invalid values for ike_version
	[Documentation]	Tests ike_version can only be v1 and v2
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Test Set Validate Invalid Options	ike_version	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: ike_version
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for phase_1_mode
	[Documentation]	Tests the two possibilities for mode in ikev1
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Set	ike_version=ikev1
	CLI:Test Set Validate Normal Fields	phase_1_mode	main	aggressive
	[Teardown]	CLI:Cancel	Raw

Test invalid values for phase_1_mode
	[Documentation]	Tests ike_version can only be main or agressive
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Set	ike_version=ikev1
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Test Set Validate Invalid Options	phase_1_mode	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: phase_1_mode
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for phase_1_encryption
	[Documentation]	Tests all encryption possibilities for phase 1
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Test Set Validate Normal Fields	phase_1_encryption	3des	aes-cbc	aes-cbc256	aes-ctr192	aes-gcm	aes-gcm256
	...	aes256	aes	aes-cbc192	aes-ctr	aes-ctr256	aes-gcm192	aes192
	[Teardown]	CLI:Cancel	Raw

Test invalid values for phase_1_encryption
	[Documentation]	Tests only encryption algorithms are accepted in phase_1_encryption field
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Test Set Validate Invalid Options	phase_1_encryption	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: phase_1_encryption
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for phase_1_authentication
	[Documentation]	Tests all authentication possibilities for phase 1
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Test Set Validate Normal Fields	phase_1_authentication	md5	sha1	sha256	sha384	sha512
	[Teardown]	CLI:Cancel	Raw

Test invalid values for phase_1_authentication
	[Documentation]	Tests only authetication algorithms are accepted in phase_1_authentication field
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Test Set Validate Invalid Options	phase_1_authentication	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: phase_1_authentication
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for phase1_diffie-hellman_group
	[Documentation]	Tests all diffie-hellman groups possibilities for phase 1
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Test Set Validate Normal Fields	phase_1_diffie-hellman_group	group_14	group_15	group_16	group_17
	...	group_18	group_19	group_2	group_20	group_21	group_31	group_5
	[Teardown]	CLI:Cancel	Raw

Test invalid values for phase1_diffie-hellman_group
	[Documentation]	Tests only groups are allowed in diffie-hellman_group field
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Test Set Validate Invalid Options	phase_1_diffie-hellman_group	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: phase_1_diffie-hellman_group
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for phase1_lifetime
	[Documentation]	Tests that phase_1_lifetime accepts numbers from 0 to 99999999999 and can be empty
	CLI:Enter Path	/settings/ipsec/ike_profile
	FOR	${VALUE}	IN	${NUMBER}	${EMPTY}
		CLI:Add
		CLI:Set	profile_name=${NAME}
		CLI:Set	phase_1_lifetime=${VALUE}
		CLI:Commit
		CLI:Delete	${NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for phase_1_lifetime
	[Documentation]	Tests that phase_1_lifetime cannot exceed the maximum value and does not accept
	...	words and points
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Set	profile_name=${NAME}
	FOR	${VALUE}	IN	${EXCEEDED}	${WORD}	${POINTS}
		CLI:Set	phase_1_lifetime=${VALUE}
		${OUTPUT}	CLI:Commit	Raw
		Should Contain	${OUTPUT}	Error: phase_1_lifetime: Validation error
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for phase_2_authentication_protocol
	[Documentation]	Tests two possibilities for phase_2_authentication_protocol
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Test Set Validate Normal Fields	phase_2_authentication_protocol	ah	esp
	[Teardown]	CLI:Cancel	Raw

Test invalid values for phase_2_authentication_protocol
	[Documentation]	Tests phase_2_authentication_protocol accepts only the pre-defined protocols
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Test Set Validate Invalid Options	phase_2_authentication_protocol	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: phase_2_authentication_protocol
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for phase_2_encryption
	[Documentation]	Tests all encryption possibilities for phase 2
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Set	phase_2_authentication_protocol=esp
	CLI:Test Set Validate Normal Fields	phase_2_encryption	3des	aes192	aes_cbc	aes_cbc256	aes_ccm192	aes_ctr
	...	aes_ctr256	aes_gcm192	aes	aes256	aes_cbc192	aes_ccm	aes_ccm256	aes_ctr192	aes_gcm	aes_gcm256
	[Teardown]	CLI:Cancel	Raw

Test invalid values for phase_2_encryption
	[Documentation]	Tests only encryption algorithms are accepted in phase_2_encryption field
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Set	phase_2_authentication_protocol=esp
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Test Set Validate Invalid Options	phase_2_encryption	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: phase_2_encryption
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for phase_2_authentication
	[Documentation]	Tests all authentication possibilities for phase 2
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Test Set Validate Normal Fields	phase_2_authentication	md5	sha1	sha2_256	sha2_384	sha2_512
	[Teardown]	CLI:Cancel	Raw

Test invalid values for phase_2_authentication
	[Documentation]	Tests only authetication algorithms are accepted in phase_2_authentication field
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Test Set Validate Invalid Options	phase_2_authentication	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: phase_2_authentication
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for phase_2_pfs_group
	[Documentation]	Tests all pfs groups possibilities for phase 2
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Test Set Validate Normal Fields	phase_2_pfs_group	group_14  group_16  group_18  group_2   group_21  group_5
	...	group_15  group_17  group_19  group_20  group_31  none
	[Teardown]	CLI:Cancel	Raw

Test invalid values for phase_2_pfs_group
	[Documentation]	Tests only groups are allowed in pfs_group field
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Test Set Validate Invalid Options	phase_2_pfs_group	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: phase_2_pfs_group
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for phase_2_lifetime
	[Documentation]	Tests that phase_2_lifetime accepts numbers from 0 to 99999999999 and can be empty
	CLI:Enter Path	/settings/ipsec/ike_profile
	FOR	${VALUE}	IN	${NUMBER}	${EMPTY}
		CLI:Add
		CLI:Set	profile_name=${NAME}
		CLI:Set	phase_2_lifetime=${VALUE}
		CLI:Commit
		CLI:Delete	${NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for phase_2_lifetime
	[Documentation]	Tests that phase_2_lifetime cannot exceed the maximum value and does not accept
	...	words and points
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Set	profile_name=${NAME}
	FOR	${VALUE}	IN	${EXCEEDED}	${WORD}	${POINTS}
		CLI:Set	phase_2_lifetime=${VALUE}
		${OUTPUT}	CLI:Commit	Raw
		Should Contain	${OUTPUT}	Error: phase_2_lifetime: Validation error
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for enable_dead_peer_detection
	[Documentation]	Tests enable_dead_peer_detection field can be enabled or disabled
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Test Set Validate Normal Fields	enable_dead_peer_detection	yes	no
	[Teardown]	CLI:Cancel	Raw

Test invalid values for enable_dead_peer_detection
	[Documentation]	Tests enable_dead_peer_detection can only be yes or no
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Test Set Validate Invalid Options	enable_dead_peer_detection	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: enable_dead_peer_detection
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for dead_peer_detection_number_of_retries
	[Documentation]	Tests that dead_peer_detection_number_of_retries accepts numbers below 100000000000000000
	CLI:Enter Path	/settings/ipsec/ike_profile
	FOR	${VALUE}	IN	${NUMBER}	100000000000000000
		CLI:Add
		CLI:Set	profile_name=${NAME}
		CLI:Set	enable_dead_peer_detection=yes dead_peer_detection_interval=100
		CLI:Set	dead_peer_detection_number_of_retries=${VALUE}
		CLI:Commit
		CLI:Delete	${NAME}
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for dead_peer_detection_number_of_retries
	[Tags]	NON-CRITICAL	BUG_NG_9570
	[Documentation]	Tests that dead_peer_detection_number_of_retries cannot be letters and points
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Set	profile_name=${NAME}
	CLI:Set	enable_dead_peer_detection=yes dead_peer_detection_interval=100
	CLI:Set	dead_peer_detection_number_of_retries=${WORD}
	${OUTPUT}	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: dead_peer_detection_number_of_retries: Validation error
	CLI:Set	dead_peer_detection_number_of_retries=${POINTS}
	${OUTPUT}	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: dead_peer_detection_number_of_retries: Validation error
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	CLI:Delete If Exists	${NAME}

Test valid values for dead_peer_detection_interval
	[Documentation]	Tests that dead_peer_detection_interval accepts numbers
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Set	profile_name=${NAME}
	CLI:Set	enable_dead_peer_detection=yes dead_peer_detection_number_of_retries=100
	CLI:Set	dead_peer_detection_interval=${NUMBER}
	CLI:Commit
	CLI:Delete	${NAME}
	[Teardown]	CLI:Cancel	Raw

Test invalid values for dead_peer_detection_interval
	[Documentation]	Tests that dead_peer_detection_interval cannot be letters, points and cannot be empty
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Set	profile_name=${NAME}
	CLI:Set	enable_dead_peer_detection=yes dead_peer_detection_number_of_retries=100
	CLI:Set	dead_peer_detection_interval=${WORD}
	${OUTPUT}	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: dead_peer_detection_interval: Validation error
	CLI:Set	dead_peer_detection_interval=${POINTS}
	${OUTPUT}	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: dead_peer_detection_interval: Validation error
	CLI:Set	dead_peer_detection_interval=
	${OUTPUT}	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: dead_peer_detection_interval: Field must not be empty.
	[Teardown]	CLI:Cancel	Raw

Test valid values for dead_peer_detection_action
	[Documentation]	Tests all three possibilities dead_peer_detection_action field
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Set	profile_name=${NAME} enable_dead_peer_detection=yes
	CLI:Set	dead_peer_detection_interval=100 dead_peer_detection_number_of_retries=100
	CLI:Test Set Validate Normal Fields	dead_peer_detection_action	clear	hold	restart
	[Teardown]	CLI:Cancel	Raw

Test invalid values for dead_peer_detection_action
	[Documentation]	Tests only groups are allowed in pfs_group field
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Set	profile_name=${NAME} enable_dead_peer_detection=yes
	CLI:Set	dead_peer_detection_interval=100 dead_peer_detection_number_of_retries=100
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Test Set Validate Invalid Options	dead_peer_detection_action	${VALUE}
		...	Error: Invalid value: ${VALUE} for parameter: dead_peer_detection_action
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for mtu
	[Documentation]	Tests that mtu accepts numbers
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Set	profile_name=${NAME}
	CLI:Set	mtu=${NUMBER}
	CLI:Commit
	CLI:Delete	${NAME}
	[Teardown]	CLI:Cancel	Raw

Test invalid values for mtu
	[Documentation]	Tests that mtu cannot be letters, points and field size cannot be exceeded
	CLI:Enter Path	/settings/ipsec/ike_profile
	CLI:Add
	CLI:Set	profile_name=${NAME}
	CLI:Set	mtu=${EXCEEDED}
	${OUTPUT}	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: mtu: Validation error
	CLI:Set	mtu=${WORD}
	${OUTPUT}	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: mtu: Validation error
	CLI:Set	mtu=${POINTS}
	${OUTPUT}	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: mtu: Validation error
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection