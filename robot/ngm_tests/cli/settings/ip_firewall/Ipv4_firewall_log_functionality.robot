*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > IPV4 firewall log through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	NEED-REVIEW

Suite Setup	SUITE:Setup
Suite Teardown	CLI:Close Connection

*** Variables ***
@{IPV4_CHAINS_TYPES}	INPUT	FORWARD	OUTPUT

*** Test Cases ***
Test Firewall Logs To INPUT
	${LIST}=	Create List	nodegrid kernel	IN=eth0	SRC=${HOSTPEER}	DST=${HOST}	PROTO=ICMP
	Set Suite Variable	${LIST}
	SUITE:Enable IPv4 IP Forward	No
	SUITE:Delete If Exists Rule Number
	SUITE:Clean iptable logs file
	SUITE:Configure Firewall INPUT
	SUITE:Ping From Peer To Host
	SUITE:Check iptable.log and messages Log Files
	[Teardown]	SUITE:Teardown

Test Firewall Logs To FORWARD
	Skip If	not ${HAS_HOSTSHARED}
	${LIST}=	Create List	nodegrid kernel	IN=eth0	OUT=eth0	SRC=${HOSTPEER}	DST=${HOSTSHARED}	PROTO=ICMP
	Set Suite Variable	${LIST}
	SUITE:Enable IPv4 IP Forward	No
	SUITE:Delete If Exists Rule Number
	SUITE:Clean iptable logs file
	SUITE:Enable IPv4 IP Forward	Yes
	SUITE:Configure Firewall FORWARD
	SUITE:Configure NAT PREROUTING with DNAT rule To ICMP Protocol	${HOSTSHARED}
	SUITE:Ping From Peer To Host
	SUITE:Check iptable.log and messages Log Files
	[Teardown]	SUITE:Teardown

Test Firewall Logs To OUTPUT
	${LIST}=	Create List	nodegrid kernel	OUT=eth0	SRC=${HOST}	DST=${HOSTPEER}	PROTO=ICMP
	Set Suite Variable	${LIST}
	SUITE:Enable IPv4 IP Forward	No
	SUITE:Delete If Exists Rule Number
	SUITE:Clean iptable logs file
	SUITE:Configure Firewall OUTPUT
	CLI:Test Ping	${HOSTPEER}	NUM_PACKETS=5	TIMEOUT=30
	SUITE:Check iptable.log and messages Log Files
	[Teardown]	SUITE:Teardown

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Change Hostname	nodegrid

SUITE:Teardown
	CLI:Cancel	Raw
	SUITE:Delete If Exists Rule Number
	SUITE:Clean iptable logs file
	SUITE:Enable IPv4 IP Forward	No

SUITE:Delete If Exists Rule Number
	CLI:Enter Path	/settings/ipv4_nat/chains/PREROUTING/
	CLI:Delete If Exists	0
	CLI:Enter Path	/
	CLI:Enter Path	/settings/ipv4_firewall/chains/INPUT/
	CLI:Delete If Exists	1
	CLI:Enter Path	/settings/ipv4_firewall/chains/FORWARD/
	CLI:Delete If Exists	0
	CLI:Enter Path	/settings/ipv4_firewall/chains/OUTPUT/
	CLI:Delete If Exists	1

SUITE:Clean iptable logs file
	Set Client Configuration	prompt=#
	CLI:Write	shell sudo su -
	CLI:Write	> /var/log/iptables.log
	CLI:Write	> /var/log/messages
	CLI:Write	/etc/init.d/syslog restart
	${LOG_IPTABLES_CONTENT}=	CLI:Write	cat /var/log/iptables.log
	Set Client Configuration	prompt=]#
	CLI:Write	exit
	CLI:Should Not Contain All	${LOG_IPTABLES_CONTENT}	${LIST}

SUITE:Enable IPv4 IP Forward
	[Arguments]	${ACTION}
	CLI:Enter Path	/settings/network_settings/
	Run Keyword If	'${ACTION}' == 'Yes'	CLI:Set	enable_ipv4_ip_forward=yes
	...	ELSE	CLI:Set	enable_ipv4_ip_forward=no
	CLI:Commit

SUITE:Ping From Peer To Host
	CLI:Open	HOST_DIFF=${HOSTPEER}	session_alias=peer_session
	CLI:Test Ping	${HOST}	NUM_PACKETS=5	TIMEOUT=30
	CLI:Close Connection
	CLI:Open

SUITE:Check iptable.log and messages Log Files
	Sleep	2s
	Set Client Configuration	prompt=#
	CLI:Write	shell sudo su -
	${LOG_DIRECTORY_CONTENT}=	CLI:Write	ls /var/log
	${LOG_IPTABLES_CONTENT}=	CLI:Write	cat /var/log/iptables.log
	${LOG_MESSAGES_CONTENT}=	CLI:Write	cat /var/log/messages
	Set Client Configuration	prompt=]#
	CLI:Write	exit
	Should Contain	${LOG_DIRECTORY_CONTENT}	iptables.log
	CLI:Should Contain All	${LOG_IPTABLES_CONTENT}	${LIST}
	CLI:Should Not Contain All	${LOG_MESSAGES_CONTENT}	${LIST}

SUITE:Configure Firewall INPUT
	CLI:Enter Path	/settings/ipv4_firewall/chains/INPUT/
	CLI:Add
	CLI:Set	target=LOG rule_number=1 protocol=icmp input_interface=eth0 log_level=debug
	CLI:Commit
	${INPUT_CONTENT}=	CLI:Show
	Should Contain	${INPUT_CONTENT}	1

SUITE:Configure Firewall FORWARD
	CLI:Enter Path	/settings/ipv4_firewall/chains/FORWARD/
	CLI:Add
	CLI:Set	target=LOG rule_number=0 protocol=icmp input_interface=eth0 log_level=debug
	CLI:Commit
	${FORWARD_CONTENT}=	CLI:Show
	Should Contain	${FORWARD_CONTENT}	0

SUITE:Configure Firewall OUTPUT
	CLI:Enter Path	/settings/ipv4_firewall/chains/OUTPUT/
	CLI:Add
	CLI:Set	target=LOG rule_number=1 protocol=icmp output_interface=eth0 log_level=debug
	CLI:Commit
	${OUTPUT_CONTENT}=	CLI:Show
	Should Contain	${OUTPUT_CONTENT}	1

SUITE:Configure NAT PREROUTING with DNAT rule To ICMP Protocol
	[Arguments]	${DESTINATION}
	CLI:Enter Path	/settings/ipv4_nat/chains/PREROUTING/
	CLI:Add
	CLI:Set	target=DNAT rule_number=0 protocol=icmp to_destination=${DESTINATION} log_level=debug
	CLI:Commit