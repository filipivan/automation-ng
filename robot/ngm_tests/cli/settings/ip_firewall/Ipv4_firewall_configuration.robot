*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > IPV4 firewall... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW	SESSION

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${ipv4_chain}	chain_ipv4

*** Test Cases ***
Test adding new chain
	CLI:Enter Path	/settings/ipv4_firewall/chains
	CLI:Add
	CLI:Set Field	chain	test_chain
	CLI:Commit
	CLI:Test Show Command	test_chain
	CLI:Delete	test_chain

Test EMPTY value for field=target
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	target	${EMPTY}	Error: Missing value for parameter: target
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	target	${EMPTY}	Error: Missing value: target
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test values for field=target
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	@{VALUES}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${TWO_WORD}	${EXCEEDED}
	FOR	${VALUE}	IN	@{VALUES}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	target	${VALUE}	Error: Invalid value: ${VALUE} for parameter: target
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	target	${VALUE}	Error: Invalid value: ${VALUE}
	END
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test valid values for field=target
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Test Set Validate Normal Fields	target	ACCEPT	REJECT	DROP	LOG	RETURN
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test EMPTY value for field=source_net4
	[Tags]	NEED-REVIEW
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	source_net4	${EMPTY}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	source_net4	${EMPTY}
	[Teardown]	CLI:Cancel	Raw

Test values for field=source_net4
	Skip If	'${NGVERSION}' == '3.2'	No validation messages on 3.2
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	@{VALUES}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${TWO_WORD}	${EXCEEDED}
	FOR	${VALUE}	IN	@{VALUES}
		CLI:Test Set Field Invalid Options	source_net4	${VALUE}	Error: source_net4: Validation error.	yes
	END
	CLI:Test Set Field Invalid Options	source_net4	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument for command: set
	[Teardown]	CLI:Cancel	Raw

Test EMPTY value for destination_net4
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	target	ACCEPT
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	destination_net4	${EMPTY}	save=yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	destination_net4	${EMPTY}	save=yes
	[Teardown]

Test with field=destination_net4
	Skip If	'${NGVERSION}' == '3.2'	No validation messages on 3.2
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	@{VALUES}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${TWO_WORD}	${EXCEEDED}
	FOR	${VALUE}	IN	@{VALUES}
		CLI:Test Set Field Invalid Options	destination_net4	${VALUE}	Error: destination_net4: Validation error.	yes
	END
	CLI:Test Set Field Invalid Options	destination_net4	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument for command: set
	[Teardown]	CLI:Cancel	Raw

Test valid values - destination_net4 & source_net4
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	target	ACCEPT
	CLI:Test Set Validate Normal Fields	source_net4	162.163.18.1/255.255.255.0
	CLI:Test Set Validate Normal Fields	destination_net4	162.163.18.2/255.255.255.0
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test with field=reverse_match_for_source_ip|mask
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	target	ACCEPT
	CLI:Test Set Validate Normal Fields	reverse_match_for_source_ip|mask	yes	no
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test with field=reverse_match_for_destination_ip|mask
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	target	ACCEPT
	CLI:Test Set Validate Normal Fields	reverse_match_for_destination_ip|mask	yes	no
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test EMPTY value for input_interface
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	target	ACCEPT
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	input_interface	${EMPTY}	save=yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	input_interface	${EMPTY}	save=yes
	[Teardown]	CLI:Cancel	Raw

Test with field=input_interface
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	@{VALUES}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${TWO_WORD}	${EXCEEDED}
	FOR	${VALUE}	IN	@{VALUES}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	input_interface	${VALUE}	Error: Invalid value: ${VALUE} for parameter: input_interface
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	input_interface	${VALUE}	Error: Invalid value: ${VALUE}
	END
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test valid field=input_interface
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	input_interface	any	eth0	lo
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	input_interface	any	eth0
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test with field=reverse_match_for_input_interface
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	target	ACCEPT
	CLI:Test Set Validate Normal Fields	reverse_match_for_input_interface	yes	no
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test EMPTY value for output_interface
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	target	ACCEPT
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	output_interface	${EMPTY}	Error: Missing value for parameter: output_interface
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	output_interface	${EMPTY}	Error: Missing value: output_interface
	[Teardown]	CLI:Cancel	Raw

Test with field=output_interface
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	@{VALUES}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${TWO_WORD}	${EXCEEDED}
	FOR	${VALUE}	IN	@{VALUES}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	output_interface	${VALUE}	Error: Invalid value: ${VALUE} for parameter: output_interface
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	output_interface	${VALUE}	Error: Invalid value: ${VALUE}
	END
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test valid field=output_interface
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	output_interface	any	eth0	lo
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	output_interface	any	eth0
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test with field=reverse_match_for_output_interface
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	target	ACCEPT
	CLI:Test Set Validate Normal Fields	reverse_match_for_output_interface	yes	no
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test with enable state match
	Skip If	'${NGVERSION}' == '3.2'	BUG 998 - Fixed field enable_state_match to show on CLI only in 4.2+
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	target	ACCEPT
	CLI:Test Set Validate Normal Fields	enable_state_match	yes	no
	CLI:Set Field	enable_state_match	yes
	# Tests for child fields enable_state_match
	CLI:Test Set Validate Normal Fields	new	yes	no
	CLI:Test Set Validate Normal Fields	established	yes	no
	CLI:Test Set Validate Normal Fields	related	yes	no
	CLI:Test Set Validate Normal Fields	invalid	no	yes
	CLI:Test Set Validate Normal Fields	reverse_state_match	yes	no
	[Teardown]	CLI:Cancel	Raw

Test EMPTY value for fragments
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	target	ACCEPT
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	fragments	${EMPTY}	Error: Missing value for parameter: fragments
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	fragments	${EMPTY}	Error: Missing value: fragments
	[Teardown]	CLI:Cancel	Raw

Test with field=fragments
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	@{VALUES}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${TWO_WORD}	${EXCEEDED}
	FOR	${VALUE}	IN	@{VALUES}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	fragments	${VALUE}	Error: Invalid value: ${VALUE} for parameter: fragments
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	fragments	${VALUE}	Error: Invalid value: ${VALUE}
	END
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test valid values - fragments
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	target	ACCEPT
	CLI:Test Set Validate Normal Fields	fragments	all_packets_and_fragments	2nd_and_further_packets	unfragmented_packets_and_1st_packets
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test EMPTY value for log_level
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	target	ACCEPT
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	log_level	${EMPTY}	Error: Missing value for parameter: log_level
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	log_level	${EMPTY}	Error: Missing value: log_level
	[Teardown]	CLI:Cancel	Raw

Test with field=log_level
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	@{VALUES}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${TWO_WORD}	${EXCEEDED}
	FOR	${VALUE}	IN	@{VALUES}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	log_level	${VALUE}	Error: Invalid value: ${VALUE} for parameter: log_level
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	log_level	${VALUE}	Error: Invalid value: ${VALUE}
	END
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test valid values - log_level
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	target	ACCEPT
	CLI:Test Set Validate Normal Fields	log_level	debug	info	notice	warning	error	critical	alert	emergency
	[Teardown]	SUITE:Teardown IPv4 Firewall


Test with field=log_prefix
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	target	ACCEPT
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	log_prefix	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument for command: set
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	log_prefix	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument: set
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test VALUES with no validation for log_prefix
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	${VALUES}=	Create List	${EMPTY}	${WORD}	${NUMBER}	${POINTS}	${ONE_POINTS}	${ONE_WORD}	${ONE_NUMBER}	${TWO_WORD}	${TWO_NUMBER}	${TWO_POINTS}	${EXCEEDED}
	FOR	${VALUE}	IN	@{VALUES}
		CLI:Test Set Field Invalid Options	log_prefix	${VALUE}	save=yes
		CLI:Delete If Exists	0
		CLI:Add
	END
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test with field=log_options_from_the_tcp_packet_header | log_options_from_the_ip_packet_header | log_options_from_the_tcp_packet_header
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Test Set Validate Normal Fields	log_options_from_the_tcp_packet_header	yes	no
	CLI:Test Set Validate Normal Fields	log_options_from_the_ip_packet_header	yes	no
	CLI:Test Set Validate Normal Fields	log_options_from_the_tcp_packet_header	yes	no
	[Teardown]	SUITE:Teardown IPv4 Firewall


Test EMPTY value for reject_with
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	target	ACCEPT
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	reject_with	${EMPTY}	Error: Missing value for parameter: reject_with
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	reject_with	${EMPTY}	Error: Missing value: reject_with
	[Teardown]	CLI:Cancel	Raw

Test with field=reject_with
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	${VALUES}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_POINTS}	${ONE_WORD}	${ONE_NUMBER}	${TWO_WORD}	${TWO_NUMBER}	${TWO_POINTS}
	FOR	${VALUE}	IN	@{VALUES}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	reject_with	${VALUE}	Error: Invalid value: ${VALUE} for parameter: reject_with
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	reject_with	${VALUE}	Error: Invalid value: ${VALUE}
	END
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test valid values - reject_with
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Test Set Validate Normal Fields	reject_with	administratively_prohibited	host_prohibited	host_unreacheable	network_prohibited	network_unreacheable	port_unreacheable	protocol_unreacheable	tcp_reset
	[Teardown]	CLI:Cancel	Raw

Test EMPTY value for field protocol
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	target	ACCEPT
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	protocol	${EMPTY}	Error: Missing value for parameter: protocol
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	protocol	${EMPTY}	Error: Missing value: protocol
	[Teardown]	CLI:Cancel	Raw

Test with field=protocol
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	${VALUES}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_POINTS}	${ONE_WORD}	${ONE_NUMBER}	${TWO_WORD}	${TWO_NUMBER}	${TWO_POINTS}
	FOR	${VALUE}	IN	@{VALUES}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	protocol	${VALUE}	Error: Invalid value: ${VALUE} for parameter: protocol
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	protocol	${VALUE}	Error: Invalid value: ${VALUE}
	END
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test valid values - protocol
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Test Set Validate Normal Fields	protocol	icmp	numeric	tcp	udp
	[Teardown]	CLI:Cancel	Raw

Test EMPTY value for protocol_number
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Test Set Field Invalid Options	protocol_number	${EMPTY}	save=yes
	[Teardown]	CLI:Cancel	Raw

Test with protocol=numeric | protocol_number
	Skip If	'${NGVERSION}' == '3.2'	No validation messages on 3.2
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	protocol	numeric
	${VALUES}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_POINTS}	${ONE_WORD}	${TWO_WORD}	${TWO_POINTS}	${EXCEEDED}
	FOR	${VALUE}	IN	@{VALUES}
		CLI:Test Set Field Invalid Options	protocol_number	${VALUE}	Error: protocol_number: Invalid protocol number. Valid values are: udp, tcp, icmp/icmpv6, all or a number[0-255].	yes
	END
	CLI:Test Set Field Invalid Options	protocol_number	${ONE_WORD} ${ONE_WORD}	Error: protocol_number: Invalid protocol number. Valid values are: udp, tcp, icmp/icmpv6, all or a number[0-255].	yes
	[Teardown]	CLI:Cancel	Raw

Test ONE_NUMBER for protocol=numeric | protocol_number
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	protocol	numeric
	CLI:Test Set Field Invalid Options	protocol_number	${ONE_NUMBER}	save=yes
	[Teardown]

Test with protocol=tcp | source_port
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	protocol	tcp
	${VALUES}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_POINTS}	${ONE_WORD}	${TWO_WORD}	${TWO_POINTS}	${EXCEEDED}
	FOR	${VALUE}	IN	@{VALUES}
		CLI:Test Set Field Invalid Options	source_port	${VALUE}	Error: source_port: Valid format: a number[1-65536], range(<number>:<number>) or list of numbers and ranges with comma separator.	yes
	END
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	source_port	${ONE_WORD} ${ONE_WORD}	Error: source_port: Valid format: a number[1-65536], range(<number>:<number>) or list of numbers and ranges with comma separator.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	source_port	${ONE_WORD} ${ONE_WORD}	Error: source_port: Valid format: a number[1-65536], range(<number>:<number>) or list of numbers and ranges with comma separator.	yes
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test EMPTY value for protocol=tcp | source_port
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	protocol	tcp
	CLI:Test Set Field Invalid Options	source_port	${EMPTY}	save=yes
	[Teardown]	CLI:Cancel	Raw

Test ONE_NUMBER for protocol=tcp | source_port
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	protocol	numeric
	CLI:Test Set Field Invalid Options	source_port	${ONE_NUMBER}	save=yes
	[Teardown]

Test with protocol=tcp | destination_port
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	protocol	tcp
	CLI:Set Field	target	ACCEPT
	CLI:Set Field	source_port	16219
	${VALUES}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_POINTS}	${ONE_WORD}	${TWO_WORD}	${TWO_POINTS}	${EXCEEDED}
	FOR	${VALUE}	IN	@{VALUES}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	destination_port	${VALUE}	Error: destination_port: Multiport match (list of numbers/ranges) does not support match for both source and destination ports simultaneously.	yes
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	destination_port	${VALUE}	Error: source_port: Valid format: a number[1-65536], range(<number>:<number>) or list of numbers and ranges with comma separator.	yes
	END
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	destination_port	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument for command: set
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	destination_port	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument: set
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test EMPTY value protocol=tcp | destination_port
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	protocol	tcp
	CLI:Set Field	target	ACCEPT
	CLI:Test Set Field Invalid Options	destination_port	${EMPTY}	save=yes
	[Teardown]	CLI:Cancel	Raw


Test valid values - destination_port & source_port
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	target	ACCEPT
	CLI:Set Field	protocol	tcp
	CLI:Test Set Validate Normal Fields	source_port	16219
	CLI:Test Set Validate Normal Fields	destination_port	16220
	[Teardown]	SUITE:Teardown IPv4 Firewall


Test with protocol=tcp | tcp_flags multiple fields
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	protocol	tcp
	@{FIELDS}=	Create List	tcp_flag_ack	tcp_flag_fin	tcp_flag_psh	tcp_flag_rst	tcp_flag_syn	tcp_flag_urg
	FOR	${FIELD}	IN	@{FIELDS}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	${FIELD}	${EMPTY}	Error: Missing value for parameter: ${FIELD}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	${FIELD}	${EMPTY}	Error: Missing value: ${FIELD}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	${FIELD}	${WORD}	Error: Invalid value: ${WORD} for parameter: ${FIELD}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	${FIELD}	${WORD}	Error: Invalid value: ${WORD}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	${FIELD}	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: ${FIELD}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	${FIELD}	${NUMBER}	Error: Invalid value: ${NUMBER}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	${FIELD}	${POINTS}	Error: Invalid value: ${POINTS} for parameter: ${FIELD}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	${FIELD}	${POINTS}	Error: Invalid value: ${POINTS}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	${FIELD}	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: ${FIELD}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	${FIELD}	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	${FIELD}	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: ${FIELD}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	${FIELD}	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	${FIELD}	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: ${FIELD}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	${FIELD}	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	${FIELD}	${TWO_WORD}	Error: Invalid value: ${TWO_WORD} for parameter: ${FIELD}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	${FIELD}	${TWO_WORD}	Error: Invalid value: ${TWO_WORD}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	${FIELD}	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: ${FIELD}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	${FIELD}	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
		CLI:Test Set Validate Normal Fields	${FIELD}	any	set	unset
	END
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test valid values - reverse_match_for_tcp_flags
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	protocol	tcp
	CLI:Test Set Validate Normal Fields	reverse_match_for_tcp_flags	yes	no
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test with EMPTY value for source_udp_port
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	protocol	udp
	CLI:Test Set Field Invalid Options	source_udp_port ${EMPTY}	save=yes
	[Teardown]

Test with protocol=udp | source_udp_port
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	protocol	udp
	CLI:Set Field	destination_udp_port	19216
	${VALUES}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_POINTS}	${ONE_WORD}	${TWO_WORD}	${TWO_POINTS}	${EXCEEDED}
	FOR	${VALUE}	IN	@{VALUES}
		CLI:Test Set Field Invalid Options	source_udp_port	${VALUE}	Error: source_udp_port:	Valid format: a number[1-65536], range(<number>:<number>) or list of numbers and ranges with comma separator.
	END
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	source_udp_port	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument for command: set
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	source_udp_port	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument: set
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test with protocol=udp | destination_udp_port
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	protocol	udp
	CLI:Set Field	target	ACCEPT
	CLI:Set Field	source_udp_port	19217
	${VALUES}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_POINTS}	${ONE_WORD}	${TWO_WORD}	${TWO_POINTS}	${EXCEEDED}
	FOR	${VALUE}	IN	@{VALUES}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	destination_udp_port	${VALUE}	Error: destination_udp_port: Multiport match (list of numbers/ranges) does not support match for both source and destination ports simultaneously.	yes
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	destination_udp_port	${VALUE}	Error: source_udp_port: Valid format: a number[1-65536], range(<number>:<number>) or list of numbers and ranges with comma separator.	yes
	END
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	destination_udp_port	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument for command: set
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	destination_udp_port	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument: set
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test EMPTY for protocol=udp | destination_udp_port
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Test Set Field Invalid Options	destination_udp_port ${EMPTY}	save=yes
	[Teardown]

Test valid values - destination_udp_port
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	target	ACCEPT
	CLI:Set Field	protocol	udp
	CLI:Test Set Validate Normal Fields	destination_udp_port	162.163.18.1/255.255.255.0
	[Teardown]	SUITE:Teardown IPv4 Firewall


Test with protocol=icmp | reverse_match_for_icmp_type
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	protocol	icmp
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	icmp_type	${EMPTY}	Error: Missing value for parameter: icmp_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	icmp_type	${EMPTY}	Error: Missing value: icmp_type

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	icmp_type	${WORD}	Error: Invalid value: ${WORD} for parameter: icmp_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	icmp_type	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	icmp_type	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: icmp_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	icmp_type	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	icmp_type	${POINTS}	Error: Invalid value: ${POINTS} for parameter: icmp_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	icmp_type	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	icmp_type	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: icmp_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	icmp_type	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	icmp_type	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: icmp_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	icmp_type	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	icmp_type	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: icmp_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	icmp_type	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	icmp_type	${TWO_WORD}	Error: Invalid value: ${TWO_WORD} for parameter: icmp_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	icmp_type	${TWO_WORD}	Error: Invalid value: ${TWO_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	icmp_type	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: icmp_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	icmp_type	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	icmp_type	address_mask_reply	address_mask_request	any	bad_ip_header	communication_prohibited
	...	destination_unreachable	echo_reply	echo_request	fragmentation_needed	host_precedence_violation	host_prohibited	host_redirect
	...	host_unknown	host_unreachable	network_prohibited	network_redirect	network_unknown	network_unreachable	parameter_problem
	...	port_unreachable	precedence_cutoff	protocol_unreachable	redirect	required_option_missing	router_advertisement
	...	router_solicitation	source_quench	source_route_failed	time_exceeded	timestamp_request	timestamp_reply	tos_host_redirect
	...	tos_host_unreachable	tos_network_redirect	tos_network_unreachable	ttl_zero_during_reassembly	ttl_zero_during_transit

	CLI:Test Set Validate Normal Fields	reverse_match_for_icmp_type	yes	no
	[Teardown]	SUITE:Teardown IPv4 Firewall

Test with protocol=reverse_match_for_protocol | reverse_match_for_source_port | reverse_match_for_destination_port
	[Setup]	SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains/${ipv4_chain}
	CLI:Add
	CLI:Test Set Validate Normal Fields	reverse_match_for_protocol	yes	no
	CLI:Test Set Validate Normal Fields	reverse_match_for_source_port	yes	no
	CLI:Test Set Validate Normal Fields	reverse_match_for_destination_port	yes	no
	[Teardown]	SUITE:Teardown IPv4 Firewall

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/ipv4_firewall/chains
	CLI:Delete If Exists	${ipv4_chain}

SUITE:Teardown
	CLI:Enter Path	/settings/ipv4_firewall/chains
	CLI:Delete If Exists	${ipv4_chain}
	CLI:Close Connection

SUITE:Setup IPv4 Firewall
	CLI:Enter Path	/settings/ipv4_firewall/chains
	CLI:Delete If Exists	${ipv4_chain}
	CLI:Add
	CLI:Write	set chain=${ipv4_chain}
	CLI:Write	commit

SUITE:Teardown IPv4 Firewall
	CLI:Cancel
	CLI:Enter Path	/settings/ipv4_firewall/chains/
	CLI:Delete If Exists	${ipv4_chain}

SUITE:Set Invalid Values Error Messages
	[Arguments]	${COMMANDS}
	FOR	${COMMAND}	IN	@{COMMANDS}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value: ${COMMAND}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}
	END