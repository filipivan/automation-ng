*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > IPV6 firewall... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW	SESSION
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${ipv6_chain}	chain_ipv6
@{ALL_VALUES}=	${WORD}	${ONE_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
	...	${EIGHT_NUMBER}	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
	...	${ONE_POINTS}	${SEVEN_POINTS}	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
	...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}	${EMPTY}	${WORDS_SPACES}	${WORDS_TWO_SPACES}
	...	${WORDS_WITH_BARS}
@{IPV6_CHAINS_TYPES}	INPUT	FORWARD	OUTPUT

*** Test Cases ***
Enter IPv6 firewall
	CLI:Enter Path	/settings/ipv6_firewall
	CLI:Test Ls Command	chains	policy
	CLI:Enter Path	chains
	CLI:Test Ls Command	INPUT	FORWARD	OUTPUT
	CLI:Enter Path	FORWARD
	CLI:Add
	CLI:Test Available Commands	cancel	commit	ls	save	set	show
	CLI:Test Set Field Options	target	ACCEPT	DROP	LOG	REJECT	RETURN
	CLI:Cancel

Test show_settings command
	CLI:Enter Path	/settings/ipv6_firewall/
	Write	show_settings
	${OUTPUT}=	CLI:Read Until Prompt
	Should Match Regexp	${OUTPUT}	/settings/ipv6_firewall/policy INPUT=ACCEPT
	Should Match Regexp	${OUTPUT}	/settings/ipv6_firewall/policy OUTPUT=ACCEPT
	Should Match Regexp	${OUTPUT}	/settings/ipv6_firewall/policy FORWARD=ACCEPT

Test show command for chain directory
	CLI:Enter Path	/settings/ipv6_firewall/chains
	CLI:Test Show Command	chain	policy	packets	bytes	INPUT	FORWARD	OUTPUT

Test adding invalid value for chain
	Skip If	'${NGVERSION}' == '3.2'	Error not shown in 3.2
	CLI:Enter Path	/settings/ipv6_firewall/chains
	CLI:Add
	CLI:Test Set Field Invalid Options	chain	~=	Error: chain: This field has invalid chain name.	yes
	[Teardown]	CLI:Cancel	Raws

Test set fields for Input
	CLI:Enter Path	/settings/ipv6_firewall/chains/INPUT/
	CLI:Add
	CLI:Test Show Command	target	source_net6	destination_net6	protocol	protocol_number
	...	input_interface	output_interface	fragments	reverse_match_for_source_ip|mask
	...	reverse_match_for_destination_ip|mask	reverse_match_for_source_port	reverse_match_for_destination_port
	...	reverse_match_for_protocol	reverse_match_for_tcp_flags	reverse_match_for_icmp_type
	...	reverse_match_for_input_interface	reverse_match_for_output_interface	reject_with
	...	log_level	log_prefix	log_tcp_sequence_numbers	log_options_from_the_tcp_packet_header	log_options_from_the_ip_packet_header
	[Teardown]	CLI:Cancel	Raw

Test add fields for firewall
	CLI:Enter Path	/settings/ipv6_firewall/chains/INPUT/
	CLI:Add
	CLI:Test Set Field Options	target	ACCEPT	DROP	LOG	REJECT	RETURN
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Options	output_interface	any	eth0	lo
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Options	output_interface	any	eth0
	CLI:Test Set Field Options	fragments	2nd_and_further_packets	all_packets_and_fragments	unfragmented_packets_and_1st_packets
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Options	input_interface	any	eth0	lo
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Options	input_interface	any	eth0
	CLI:Test Set Field Options	log_options_from_the_ip_packet_header	no	yes
	CLI:Test Set Field Options	log_options_from_the_tcp_packet_header	no	yes
	CLI:Test Set Field Options	log_tcp_sequence_numbers	no	yes
	CLI:Test Set Field Options	protocol	icmp	numeric	tcp	udp
	CLI:Test Set Field Options	reject_with	address_unreachable	port_unreacheable	administratively_prohibited	tcp_reset	no_route
	CLI:Test Set Field Options	reverse_match_for_destination_ip|mask	no	yes
	CLI:Test Set Field Options	reverse_match_for_source_ip|mask	no	yes
	CLI:Test Set Field Options	reverse_match_for_source_port	no	yes
	CLI:Test Set Field Options	reverse_match_for_destination_port	no	yes
	CLI:Test Set Field Options	reverse_match_for_protocol	no	yes
	CLI:Test Set Field Options	reverse_match_for_tcp_flags	no	yes
	CLI:Test Set Field Options	reverse_match_for_icmp_type	no	yes
	CLI:Test Set Field Options	reverse_match_for_input_interface	no	yes
	CLI:Test Set Field Options	reverse_match_for_output_interface	no	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for other fields
	CLI:Enter Path	/settings/ipv6_firewall/chains/INPUT/
	${COMMANDS}=	Create List	target	protocol
	...	input_interface	output_interface	fragments	reverse_match_for_source_ip|mask
	...	reverse_match_for_destination_ip|mask	reverse_match_for_source_port	reverse_match_for_destination_port
	...	reverse_match_for_protocol	reverse_match_for_tcp_flags	reverse_match_for_icmp_type
	...	reverse_match_for_input_interface	reverse_match_for_output_interface	reject_with
	...	log_level	log_tcp_sequence_numbers	log_options_from_the_tcp_packet_header	log_options_from_the_ip_packet_header

	CLI:Add
	SUITE:Set Invalid Values Error Messages	${COMMANDS}
	[Teardown]	CLI:Cancel	Raw

Test invalid values for fields=source_net6
	Skip If	'${NGVERSION}' == '3.2'	Errors not shown in 3.2
	CLI:Enter Path	/settings/ipv6_firewall/chains/INPUT/
	CLI:Add
	CLI:Test Set Field Invalid Options	source_net6	a	Error: source_net6: Validation error.	yes
	CLI:Test Set Field Invalid Options	source_net6	~!@	Error: source_net6: Validation error.	yes
	CLI:Test Set Field Invalid Options	source_net6	${EXCEEDED}	Error: source_net6: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for fields=destination_net6
	Skip If	'${NGVERSION}' == '3.2'	Errors not shown in 3.2
	CLI:Enter Path	/settings/ipv6_firewall/chains/INPUT/
	CLI:Add
	CLI:Test Set Field Invalid Options	destination_net6	a	Error: destination_net6: Validation error.	yes
	CLI:Test Set Field Invalid Options	destination_net6	~!@	Error: destination_net6: Validation error.	yes
	CLI:Test Set Field Invalid Options	destination_net6	${EXCEEDED}	Error: destination_net6: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for fields=protocol_number
	Skip If	'${NGVERSION}' == '3.2'	Errors not shown in 3.2
	CLI:Enter Path	/settings/ipv6_firewall/chains/INPUT/
	CLI:Add
	CLI:Test Set Field Invalid Options	protocol_number	a	Error: protocol_number: Invalid protocol number. Valid values are: udp, tcp, icmp/icmpv6, all or a number[0-255].	yes
	CLI:Test Set Field Invalid Options	protocol_number	~!@	Error: protocol_number: Invalid protocol number. Valid values are: udp, tcp, icmp/icmpv6, all or a number[0-255].	yes
	CLI:Test Set Field Invalid Options	protocol_number	${EXCEEDED}	Error: protocol_number: Invalid protocol number. Valid values are: udp, tcp, icmp/icmpv6, all or a number[0-255].	yes
	[Teardown]	CLI:Cancel	Raw

Test available values for fields=tcp_flag
	CLI:Enter Path	/settings/ipv6_firewall/chains/INPUT/
	CLI:Add
	CLI:Set Field	protocol	tcp
	CLI:Test Set Field Options	tcp_flag_syn	any	set	unset
	CLI:Test Set Field Options	tcp_flag_ack	any	set	unset
	CLI:Test Set Field Options	tcp_flag_psh	any	set	unset
	CLI:Test Set Field Options	tcp_flag_fin	any	set	unset
	CLI:Test Set Field Options	tcp_flag_rst	any	set	unset
	CLI:Test Set Field Options	tcp_flag_urg	any	set	unset
	CLI:Test Set Field Options	reverse_match_for_tcp_flags	yes	no
	${COMMANDS}=	Create List	tcp_flag_ack	tcp_flag_psh	tcp_flag_syn	tcp_flag_fin	tcp_flag_rst	tcp_flag_urg	reverse_match_for_tcp_flags
	SUITE:Set Invalid Values Error Messages	${COMMANDS}
	[Teardown]	CLI:Cancel	Raw

Test invalid values for fields=source_port/destination_port
	CLI:Enter Path	/settings/ipv6_firewall/chains/INPUT/
	CLI:Add
	CLI:Set Field	protocol	tcp
	CLI:Test Set Field Invalid Options	source_port	~!@	Error: source_port: Valid format: a number[1-65536], range(<number>:<number>) or list of numbers and ranges with comma separator.	yes
	CLI:Test Set Field Invalid Options	source_port	${EXCEEDED}	Error: source_port: Valid format: a number[1-65536], range(<number>:<number>) or list of numbers and ranges with comma separator.	yes
	CLI:Set Field	source_port	${EMPTY}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	destination_port	~!@	Error: destination_port: Multiport match (list of numbers/ranges) does not support match for both source and destination ports simultaneously.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	destination_port	${EXCEEDED}	Error: destination_port: Multiport match (list of numbers/ranges) does not support match for both source and destination ports simultaneously.	yes
	[Teardown]	CLI:Cancel	Raw

Test available values for field=icmp_type
	CLI:Enter Path	/settings/ipv6_firewall/chains/INPUT/
	CLI:Add
	CLI:Set Field	protocol	icmp
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Options	icmp_type	address_unreachable	bad_header	beyond-scope	communication_prohibited
	...	destination_unreachable	echo_reply	echo_request	failed-policy	neighbour_advertisement	neighbour_solicitation	no_route	packet_too_big	parameter_problem
	...	port_unreachable	redirect	reject-route	router_advertisement	router_solicitation	time_exceeded	ttl_zero_during_reassembly	ttl_zero_during_transit
	...	unknown_header	unknown_option

	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Options	icmp_type	address_unreachable	bad_header
	...	destination_unreachable	echo_request	neighbour_solicitation
	...	packet_too_big	port_unreachable	router_advertisement	time_exceeded	ttl_zero_during_transit
	...	unknown_option	any	communication_prohibited	echo_reply
	...	neighbour_advertisement	no_route	parameter_problem	redirect
	...	router_solicitation	ttl_zero_during_reassembly	unknown_header
	CLI:Test Set Field Options	reverse_match_for_icmp_type	yes	no
	${COMMANDS}=	Create List	icmp_type	reverse_match_for_icmp_type
	SUITE:Set Invalid Values Error Messages	${COMMANDS}
	[Teardown]	CLI:Cancel	Raw

Test invalid values for fields=source_udp_port/destination_udp_port
	CLI:Enter Path	/settings/ipv6_firewall/chains/INPUT/
	CLI:Add
	CLI:Set Field	protocol	udp
	CLI:Test Set Field Invalid Options	source_udp_port	~!@	Error: source_udp_port: Valid format: a number[1-65536], range(<number>:<number>) or list of numbers and ranges with comma separator.	yes
	CLI:Test Set Field Invalid Options	source_udp_port	${EXCEEDED}	Error: source_udp_port: Valid format: a number[1-65536], range(<number>:<number>) or list of numbers and ranges with comma separator.	yes
	CLI:Set Field	source_udp_port	${EMPTY}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	destination_udp_port	~!@	Error: destination_udp_port: Multiport match (list of numbers/ranges) does not support match for both source and destination ports simultaneously.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	destination_udp_port	${EXCEEDED}	Error: destination_udp_port: Multiport match (list of numbers/ranges) does not support match for both source and destination ports simultaneously.	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for fields=description
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	FOR	${IPV6_CHAINS_TYPE}	IN	@{IPV6_CHAINS_TYPES}
		CLI:Enter Path	/settings/ipv6_firewall/chains/${IPV6_CHAINS_TYPE}/
		CLI:Add
		CLI:Test Set Field Invalid Options	description	${EXCEEDED}	Error: description: Validation error.	yes
		CLI:Cancel	Raw
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for fields=description
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	FOR	${IPV6_CHAINS_TYPE}	IN	@{IPV6_CHAINS_TYPES}
		SUITE:Test valid values for fields=description ipv6	${IPV6_CHAINS_TYPE}
	END
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/ipv6_firewall/chains
	CLI:Delete If Exists	${ipv6_chain}

SUITE:Teardown
	CLI:Enter Path	/settings/ipv6_firewall/chains
	CLI:Delete If Exists	${ipv6_chain}
	CLI:Close Connection

SUITE:Setup IPv6 Firewall
	CLI:Enter Path	/settings/ipv6_firewall/chains
	CLI:Delete If Exists	${ipv6_chain}
	CLI:Add
	CLI:Write	set chain=${ipv6_chain}
	CLI:Write	commit

SUITE:Teardown IPv6 Firewall
	CLI:Cancel
	CLI:Enter Path	/settings/ipv6_firewall/chains
	CLI:Delete If Exists	${ipv6_chain}

SUITE:Set Invalid Values Error Messages
	[Arguments]	${COMMANDS}
	FOR	${COMMAND}	IN	@{COMMANDS}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value: ${COMMAND}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}
	END

SUITE:Test valid values for fields=description ipv6
	[Arguments]	${PATH}
	CLI:Enter Path	/settings/ipv6_firewall/chains/${PATH}/
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Add
		CLI:Set	description="${VALUE}" rule_number=0
		CLI:Commit
		CLI:Test Show Command	${EMPTY}${VALUE}
		CLI:Delete	0
		CLI:Commit
	END