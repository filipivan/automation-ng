*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Date And Time... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/date_and_time/
	${list}=	Create List	cd	hostname	set	change_password	ls	shell	commit	pwd	show	event_system_audit	quit	event_system_clear	reboot	shutdown	exit	revert	whoami	show_settings
	CLI:Test Available Commands	@{list}

Test show_settings command
	CLI:Enter Path	/settings/date_and_time/
	Write	show_settings
	${OUTPUT}=	CLI:Read Until Prompt
	#show_settings does not shows date_and_time system_time
	#Run Keyword If	'${NGVERSION}' >= '4.0'	Should Match Regexp	${OUTPUT}	/settings/date_and_time system_time="\\w{3}\\s\\w{3}\\s+\\d+\\s\\d+:\\d+:\\d+\\s\\w{3}\\s\\d+"
	Should Match Regexp	${OUTPUT}	/settings/date_and_time date_and_time=network_time_protocol
	Should Match Regexp	${OUTPUT}	/settings/date_and_time server=.*ntp.org
	Should Match Regexp	${OUTPUT}	/settings/date_and_time zone=\\w

Test visible fields for show command
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Show Command	system_time	date_and_time	server	zone
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Show Command	system time	date_and_time	server	zone

Test available fields after set date_and_time = network_time_protocol
	CLI:Set Field	date_and_time	network_time_protocol
	CLI:Test Set Available Fields	date_and_time	server	zone
	CLI:Test Set Unavailable Fields	hour	month	year	day	minute	second

Test available value for field date_and_time
	CLI:Test Set Field Options	date_and_time	network_time_protocol	manual

Test invalid values for field date_and_time and field zone
	${COMMANDS}=	Create List
	Append To List	${COMMANDS}	date_and_time	zone
	SUITE:Set Invalid Values Error Messages	${COMMANDS}

Test available value for field zone
	#New timezones are not in 4.0 Official nor 4.1 Official yet
	CLI:Test Set Field Options	zone	gmt		utc	utc+3	utc+9	utc-3	utc-9	us|central	utc+1
	...	utc+4	utc-1	utc+6	utc-11	utc-6	us|mountain	utc+12	utc+7	utc-12	utc-7
	...	utc-4	us|eastern	utc+10	utc+5	utc-10	utc-5	us|hawaii	utc+11	us|pacific	utc+2	utc+8	utc-2	utc-8	us|alaska
	Run Keyword If 	'${NGVERSION}' >= '4.0' and '${HOST}' != '192.168.2.108' and '${HOST}' != '192.168.2.155'	CLI:Test Set Field Options	zone	africa|lagos
	...	asia|shanghai	europe|paris	africa|nairobi	asia|singapore	europe|sofia	america|buenos_aires
	...	asia|taipei		america|edmonton	asia|tokyo	us|alaska	america|halifax		australia|adelaide
	...	us|central	america|mexico_city   australia|brisbane    us|eastern	utc+5:30	america|regina	australia|darwin
	...	us|hawaii	america|sao_paulo     australia|eucla	us|mountain	america|toronto		australia|lord_howe
	...	us|pacific	america|vancouver     australia|perth	asia|dubai	europe|lisbon
	...	asia|hong_kong	europe|london	asia|seoul	europe|moscow

Test available fields after set date_and_time = manual
	CLI:Set Field	date_and_time	manual
	CLI:Test Set Available Fields	date_and_time	hour	month	year	day	minute	second	zone
	CLI:Test Set Unavailable Fields	server

Test invalid values for fields after set date_and_time = manual
	CLI:Set Field	date_and_time	manual
	${COMMANDS}=	Create List
	Append To List	${COMMANDS}	hour	month	year	day	minute	second	zone
	SUITE:Set Invalid Values Error Messages	${COMMANDS}

Test invalid values for field server
	CLI:Set Field	date_and_time	network_time_protocol
	Run Keyword If	'${NGVERSION}' > '4.0'	CLI:Test Set Field Invalid Options	server	~!	Error: server: Validation error.
	Run Keyword If	'${NGVERSION}' > '4.0'	CLI:Test Set Field Invalid Options	server	${EXCEEDED}		Error: server: Validation error.

Test revert command working
	${OUTPUT}=	CLI:Revert
	Should Not Contain	${OUTPUT}	Error

*** Keywords ***
SUITE:Set Invalid Values Error Messages
	[Arguments]	${COMMANDS}
	FOR	${COMMAND}	IN	@{COMMANDS}
		Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${COMMAND}	a	Error: Invalid value: a for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value: ${COMMAND}
		Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${COMMAND}	a	Error: Invalid value: a
	END