*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > System Preferences... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${COORDINATES}	90,90
${IDLE_TIMEOUT}	90
${PERCENTAGE}	90
${UNIT_IPV4_ADDRESS}	1.1.1.1
${UNIT_NETMASK}	255.255.255.0
${LOGO_DOWNLOAD_URL}	https://bit.ly/2R8BTxR
${USERNAME}	zpe
${PASSWORD}	zpesystems

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/system_preferences/
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Available Commands	show_settings
	CLI:Test Available Commands	hostname	shell	cd	ls	show	change_password	pwd	commit	quit	shutdown	event_system_audit	reboot	whoami	event_system_clear	revert

Test show_settings
	[Tags]	SHOW_SETTINGS
	Skip if	'${NGVERSION}' == '5.9'
	Run Keyword If	'${NGVERSION}' == '3.2'	Set Tags	NON-CRITICAL	#implementation is different in nightly/official
	CLI:Enter Path	/settings/system_preferences/
	Write	show_settings
	${OUTPUT}=	CLI:Read Until Prompt
	${VER_SION}=	Run Keyword If	${NGVERSION} != '3.2'	Replace String	${NGVERSION}	.	_
	Run Keyword If	${NGVERSION} == 3.2	Should Match Regexp	${OUTPUT}	/settings/system_preferences help_url=https://www.zpesystems.com/ng/v3_2/NodeGrid%20Manual%20v3.2.pdf
	...	ELSE	Run Keyword If	${NGVERSION} >= 5.2	Should Match Regexp	${OUTPUT}	/settings/system_preferences help_url=https://www.zpesystems.com/nodegrid/v${VER_SION}/NodegridManual${NGVERSION}.html
	...	ELSE	Should Match Regexp	${OUTPUT}	/settings/system_preferences help_url=https://www.zpesystems.com/ng/v${VER_SION}/NodegridManual${NGVERSION}.html

	Should Match Regexp	${OUTPUT}	/settings/system_preferences idle_timeout=\\d+
	Run Keyword If	${NGVERSION} >= 5.4	Should Match Regexp	${OUTPUT}	/settings/system_preferences logo_image=
	...	ELSE	Should Match Regexp	${OUTPUT}	/settings/system_preferences logo_image=use_default_logo_image
	Should Match Regexp	${OUTPUT}	/settings/system_preferences logo_download_path_is_absolute_path_name=no|yes
	Should Match Regexp	${OUTPUT}	/settings/system_preferences enable_banner=no|yes
	Should Match Regexp	${OUTPUT}	/settings/system_preferences enable_license_utilization_rate=yes|no
	Should Match Regexp	${OUTPUT}	/settings/system_preferences percentage_to_trigger_events=\\d+
	Should Match Regexp	${OUTPUT}	/settings/system_preferences unit_ipv4_address=\\d+.\\d+.\\d+.\\d+
	Should Match Regexp	${OUTPUT}	/settings/system_preferences unit_netmask=\\d+.\\d+.\\d+.\\d+
	Should Match Regexp	${OUTPUT}	/settings/system_preferences unit_interface=eth0
	Should Match Regexp	${OUTPUT}	/settings/system_preferences iso_url=http://ServerIPAddress/PATH/FILENAME.ISO
	Run Keyword If	${NGVERSION} >= 4.2	Should Match Regexp	${OUTPUT}	/settings/system_preferences	show_hostname_on_webui_header=(no|yes)

Test visible fields for show command
	CLI:Enter Path	/settings/system_preferences/
	Write	set enable_banner=yes
	CLI:Read Until Prompt
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Show Command	address_location	coordinates	help_url	idle_timeout	enable_banner	banner	enable_license_utilization_rate	percentage_to_trigger_events	unit_ipv4_address	unit_netmask	unit_interface	iso_url
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Show Command	address_location	coordinates	help_url	idle_timeout	logo_image_selection	enable_banner	banner	enable_license_utilization_rate	percentage_to_trigger_events	unit_ipv4_address	unit_netmask	unit_interface	iso_url
	Write	set enable_banner=no
	CLI:Read Until Prompt
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Show Command	address_location	coordinates	help_url	idle_timeout	enable_banner	enable_license_utilization_rate	percentage_to_trigger_events	unit_ipv4_address	unit_netmask	unit_interface	iso_url
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Show Command	address_location	coordinates	help_url	idle_timeout	logo_image_selection	enable_banner	banner	enable_license_utilization_rate	percentage_to_trigger_events	unit_ipv4_address	unit_netmask	unit_interface	iso_url
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Available Fields	show_hostname_on_webui_header
	[Teardown]	CLI:Revert

Test address_location field
	CLI:Enter Path	/settings/system_preferences/
	@{LIST}=	Create List	${EMPTY}	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${EXCEEDED}
	FOR	${VALUE}	IN	@{LIST}
		CLI:Test Set Validate Invalid Options	address_location	${VALUE}
	END
	[Teardown]	CLI:Revert

Test coordinates field
	CLI:Enter Path	/settings/system_preferences/
	CLI:Test Set Validate Invalid Options	coordinates	${EMPTY}
	CLI:Test Set Validate Invalid Options	coordinates	${WORD}	Error: coordinates: Valid ranges: Latitude [-90.0,90.0], Longitude [-180.0,180.0].
	CLI:Test Set Validate Invalid Options	coordinates	${NUMBER}	Error: coordinates: Valid ranges: Latitude [-90.0,90.0], Longitude [-180.0,180.0].
	CLI:Test Set Validate Invalid Options	coordinates	${POINTS}	Error: coordinates: Valid ranges: Latitude [-90.0,90.0], Longitude [-180.0,180.0].
	CLI:Test Set Validate Invalid Options	coordinates	${ONE_WORD}	Error: coordinates: Valid ranges: Latitude [-90.0,90.0], Longitude [-180.0,180.0].
	CLI:Test Set Validate Invalid Options	coordinates	${ONE_NUMBER}	Error: coordinates: Valid ranges: Latitude [-90.0,90.0], Longitude [-180.0,180.0].
	CLI:Test Set Validate Invalid Options	coordinates	${ONE_POINTS}	Error: coordinates: Valid ranges: Latitude [-90.0,90.0], Longitude [-180.0,180.0].
	CLI:Test Set Validate Invalid Options	coordinates	${EXCEEDED}	Error: coordinates: Valid ranges: Latitude [-90.0,90.0], Longitude [-180.0,180.0].
	CLI:Test Set Validate Invalid Options	coordinates	${COORDINATES}
	CLI:Test Set Validate Invalid Options	coordinates	*&)(%^^&%$	Error: coordinates: Valid ranges: Latitude [-90.0,90.0], Longitude [-180.0,180.0].
	CLI:Test Set Validate Invalid Options	coordinates	-91,-180	Error: coordinates: Valid ranges: Latitude [-90.0,90.0], Longitude [-180.0,180.0].
	CLI:Test Set Validate Invalid Options	coordinates	91,-180	Error: coordinates: Valid ranges: Latitude [-90.0,90.0], Longitude [-180.0,180.0].
	CLI:Test Set Validate Invalid Options	coordinates	-90,-181	Error: coordinates: Valid ranges: Latitude [-90.0,90.0], Longitude [-180.0,180.0].
	[Teardown]	CLI:Revert

Test help_url field
	CLI:Enter Path	/settings/system_preferences/
	SUITE:Test Set Validate Invalid Options	help_url	${EMPTY}	Error: help_url: Validation error.
	@{LIST}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}
	FOR	${VALUE}	IN	@{LIST}
		CLI:Test Set Validate Invalid Options	help_url	${VALUE}
	END
	SUITE:Test Set Validate Invalid Options	help_url	${EXCEEDED}	Error: help_url: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test idle_timeout field
	CLI:Enter Path	/settings/system_preferences/
	CLI:Test Set Validate Invalid Options	idle_timeout	${EMPTY}	Error: idle_timeout: The value for session timeout must be 90 seconds or higher. Use 0 to suspend idle time.
	CLI:Test Set Validate Invalid Options	idle_timeout	${WORD}	Error: idle_timeout: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	idle_timeout	${NUMBER}	Error: idle_timeout: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	idle_timeout	${POINTS}	Error: idle_timeout: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	idle_timeout	${ONE_WORD}	Error: idle_timeout: The value for session timeout must be 90 seconds or higher. Use 0 to suspend idle time.
	CLI:Test Set Validate Invalid Options	idle_timeout	${ONE_NUMBER}	Error: idle_timeout: The value for session timeout must be 90 seconds or higher. Use 0 to suspend idle time.
	CLI:Test Set Validate Invalid Options	idle_timeout	${ONE_POINTS}	Error: idle_timeout: The value for session timeout must be 90 seconds or higher. Use 0 to suspend idle time.
	CLI:Test Set Validate Invalid Options	idle_timeout	${EXCEEDED}	Error: idle_timeout: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	idle_timeout	${IDLE_TIMEOUT}
	CLI:Test Set Field Invalid Options	idle_timeout	-1	Error: idle_timeout: The value for session timeout must be 90 seconds or higher. Use 0 to suspend idle time.
	CLI:Test Set Field Invalid Options	idle_timeout	100000	Error: idle_timeout: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test logo_image field
	[Tags]	NON-CRITICAL
	CLI:Enter Path	/settings/system_preferences/
	Run Keyword If	${NGVERSION} >= 4.2	Wait Until Keyword Succeeds	3x	5s	CLI:Write	set logo_image=use_default_logo_image
	Run Keyword If	${NGVERSION} >= 4.2	Wait Until Keyword Succeeds	3x	5s	CLI:Write	set logo_image=remote_server logo_download_url=${LOGO_DOWNLOAD_URL}

	Run Keyword If	${NGVERSION} == 3.2	CLI:Write	set logo_image_selection=yes logo_image=use_default_logo_image
#	Problem With Remote Server
#	Run Keyword If	${NGVERSION} == 3.2	CLI:Write	set logo_image_selection=yes logo_image=remote_server url=${LOGO_DOWNLOAD_URL}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	logo_image	${EMPTY}	Error: Missing value for parameter: logo_image
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	logo_image_selection=yes logo_image	${EMPTY}	Error: Missing value: logo_image

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	logo_image	${WORD}	Error: Invalid value: ${WORD} for parameter: logo_image
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	logo_image_selection=yes logo_image	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	logo_image	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: logo_image
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	logo_image_selection=yes logo_image	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	logo_image	${POINTS}	Error: Invalid value: ${POINTS} for parameter: logo_image
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	logo_image_selection=yes logo_image	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	logo_image	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: logo_image
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	logo_image_selection=yes logo_image	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	logo_image	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: logo_image
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	logo_image_selection=yes logo_image	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	logo_image	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: logo_image
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	logo_image_selection=yes logo_image	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	logo_image	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: logo_image
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	logo_image_selection=yes logo_image	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test logo_image with remote server field enable and a false value to logo_download_url
	Skip If	${NGVERSION} == 3.2	Test separed to 3.2
	CLI:Enter Path	/settings/system_preferences/
	${OUTPUT}=	CLI:Write	set logo_image=remote_server logo_download_url=${EMPTY} logo_download_username=${USERNAME} logo_download_password=${PASSWORD}	Raw	Yes
	${OUTPUT}	Replace String	${OUTPUT}	\n	${EMPTY}
	Should Be Equal	${OUTPUT}	Error: logo_download_url: Incorrect URL information.

	${OUTPUT}=	CLI:Write	set logo_image=remote_server logo_download_url=${WORD} logo_download_username=${USERNAME} logo_download_password=${PASSWORD}	Raw	Yes
	${OUTPUT}	Replace String	${OUTPUT}	\n	${EMPTY}
	Should Be Equal	${OUTPUT}	Error: logo_download_url: Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>

	${OUTPUT}=	CLI:Write	set logo_image=remote_server logo_download_url=${NUMBER} logo_download_username=${USERNAME} logo_download_password=${PASSWORD}	Raw	Yes
	${OUTPUT}	Replace String	${OUTPUT}	\n	${EMPTY}
	Should Be Equal	${OUTPUT}	Error: logo_download_url: Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>

	${OUTPUT}=	CLI:Write	set logo_image=remote_server logo_download_url=${POINTS} logo_download_username=${USERNAME} logo_download_password=${PASSWORD}	Raw	Yes
	${OUTPUT}	Replace String	${OUTPUT}	\n	${EMPTY}
	Should Be Equal	${OUTPUT}	Error: logo_download_url: Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>

	${OUTPUT}=	CLI:Write	set logo_image=remote_server logo_download_url=${ONE_WORD} logo_download_username=${USERNAME} logo_download_password=${PASSWORD}	Raw	Yes
	${OUTPUT}	Replace String	${OUTPUT}	\n	${EMPTY}
	Should Be Equal	${OUTPUT}	Error: logo_download_url: Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>

	${OUTPUT}=	CLI:Write	set logo_image=remote_server logo_download_url=${ONE_NUMBER} logo_download_username=${USERNAME} logo_download_password=${PASSWORD}	Raw	Yes
	${OUTPUT}	Replace String	${OUTPUT}	\n	${EMPTY}
	Should Be Equal	${OUTPUT}	Error: logo_download_url: Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>

	${OUTPUT}=	CLI:Write	set logo_image=remote_server logo_download_url=${ONE_POINTS} logo_download_username=${USERNAME} logo_download_password=${PASSWORD}	Raw	Yes
	${OUTPUT}	Replace String	${OUTPUT}	\n	${EMPTY}
	Should Be Equal	${OUTPUT}	Error: logo_download_url: Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>

	${OUTPUT}=	CLI:Write	set logo_image=remote_server logo_download_url=${EXCEEDED} logo_download_username=${USERNAME} logo_download_password=${PASSWORD}	Raw	Yes
	${OUTPUT}	Replace String	${OUTPUT}	\n	${EMPTY}
	Should Be Equal	${OUTPUT}	Error: logo_download_url: Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>

Test logo_image with remote server field to 3.2
	Skip If	${NGVERSION} != 3.2	Test separed 3.2
	CLI:Enter Path	/settings/system_preferences/
	${OUTPUT}=	CLI:Write	set logo_image_selection=yes logo_image=remote_server url=${EMPTY} username=${USERNAME} password=${PASSWORD}	Raw	Yes
	Should Be Equal	${OUTPUT}	Error: url: Incorrect URL information.

	${OUTPUT}=	CLI:Write	set logo_image_selection=yes logo_image=remote_server url=${WORD} username=${USERNAME} password=${PASSWORD}	Raw	Yes
	Should Be Equal	${OUTPUT}	Error: url: Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>

	${OUTPUT}=	CLI:Write	set logo_image_selection=yes logo_image=remote_server url=${NUMBER} username=${USERNAME} password=${PASSWORD}	Raw	Yes
	Should Be Equal	${OUTPUT}	Error: url: Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>

	${OUTPUT}=	CLI:Write	set logo_image_selection=yes logo_image=remote_server url=${POINTS} username=${USERNAME} password=${PASSWORD}	Raw	Yes
	Should Be Equal	${OUTPUT}	Error: url: Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>

	${OUTPUT}=	CLI:Write	set logo_image_selection=yes logo_image=remote_server url=${ONE_WORD} username=${USERNAME} password=${PASSWORD}	Raw	Yes
	Should Be Equal	${OUTPUT}	Error: url: Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>

	${OUTPUT}=	CLI:Write	set logo_image_selection=yes logo_image=remote_server url=${ONE_NUMBER} username=${USERNAME} password=${PASSWORD}	Raw	Yes
	Should Be Equal	${OUTPUT}	Error: url: Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>

	${OUTPUT}=	CLI:Write	set logo_image_selection=yes logo_image=remote_server url=${ONE_POINTS} username=${USERNAME} password=${PASSWORD}	Raw	Yes
	Should Be Equal	${OUTPUT}	Error: url: Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>

	${OUTPUT}=	CLI:Write	set logo_image_selection=yes logo_image=remote_server url=${EXCEEDED} username=${USERNAME} password=${PASSWORD}	Raw	Yes
	Should Be Equal	${OUTPUT}	Error: url: Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>

Test enable_banner field
	CLI:Enter Path	/settings/system_preferences/
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Normal Fields	enable_banner	yes	no
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Normal Fields	enable_banner	yes	no

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_banner	${EMPTY}	Error: Missing value for parameter: enable_banner
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_banner	${EMPTY}	Error: Missing value: enable_banner

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_banner	${WORD}	Error: Invalid value: ${WORD} for parameter: enable_banner
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_banner	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_banner	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: enable_banner
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_banner	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_banner	${POINTS}	Error: Invalid value: ${POINTS} for parameter: enable_banner
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_banner	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_banner	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: enable_banner
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_banner	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_banner	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: enable_banner
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_banner	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_banner	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: enable_banner
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_banner	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_banner	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: enable_banner
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_banner	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test banner field
	CLI:Enter Path	/settings/system_preferences/
	CLI:Set Field	enable_banner	yes
	@{LIST}=	Create List	${EMPTY}	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${EXCEEDED}
	FOR	${VALUE}	IN	@{LIST}
		CLI:Test Set Validate Invalid Options	banner	${VALUE}
	END
	CLI:Test Show Command	enable_banner = yes
	CLI:Test Show Command Regexp	\\s*banner\\s=\\w*
	[Teardown]	CLI:Revert

Test enable_license_utilization_rate field
	CLI:Enter Path	/settings/system_preferences/
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Normal Fields	enable_license_utilization_rate	yes	no
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Normal Fields	enable_license_utilization_rate	yes	no

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_license_utilization_rate	${EMPTY}	Error: Missing value for parameter: enable_license_utilization_rate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_license_utilization_rate	${EMPTY}	Error: Missing value: enable_license_utilization_rate

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_license_utilization_rate	${WORD}	Error: Invalid value: ${WORD} for parameter: enable_license_utilization_rate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_license_utilization_rate	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_license_utilization_rate	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: enable_license_utilization_rate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_license_utilization_rate	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_license_utilization_rate	${POINTS}	Error: Invalid value: ${POINTS} for parameter: enable_license_utilization_rate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_license_utilization_rate	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_license_utilization_rate	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: enable_license_utilization_rate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_license_utilization_rate	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_license_utilization_rate	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: enable_license_utilization_rate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_license_utilization_rate	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_license_utilization_rate	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: enable_license_utilization_rate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_license_utilization_rate	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_license_utilization_rate	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: enable_license_utilization_rate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_license_utilization_rate	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test percentage_to_trigger_events: field
	CLI:Enter Path	/settings/system_preferences/
	CLI:Test Set Validate Invalid Options	percentage_to_trigger_events	${EMPTY}	Error: percentage_to_trigger_events: Validation error.
	CLI:Test Set Validate Invalid Options	percentage_to_trigger_events	${WORD}	Error: percentage_to_trigger_events: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	percentage_to_trigger_events	${NUMBER}	Error: percentage_to_trigger_events: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	percentage_to_trigger_events	${POINTS}	Error: percentage_to_trigger_events: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	percentage_to_trigger_events	${ONE_WORD}	Error: percentage_to_trigger_events: Validation error.
	CLI:Test Set Validate Invalid Options	percentage_to_trigger_events	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	percentage_to_trigger_events	${ONE_POINTS}	Error: percentage_to_trigger_events: Validation error.
	CLI:Test Set Validate Invalid Options	percentage_to_trigger_events	${EXCEEDED}	Error: percentage_to_trigger_events: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	percentage_to_trigger_events	${PERCENTAGE}
	CLI:Test Set Validate Invalid Options	percentage_to_trigger_events	100	Error: percentage_to_trigger_events: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	percentage_to_trigger_events	-1	Error: percentage_to_trigger_events: Validation error.
	[Teardown]	CLI:Revert

Test unit_ipv4_address: field
	CLI:Enter Path	/settings/system_preferences/
	CLI:Test Set Validate Invalid Options	unit_ipv4_address	${EMPTY}	Error: unit_ipv4_address: Invalid IP address.
	CLI:Test Set Validate Invalid Options	unit_ipv4_address	${WORD}	Error: unit_ipv4_address: Invalid IP address.
	CLI:Test Set Validate Invalid Options	unit_ipv4_address	${NUMBER}	Error: unit_ipv4_address: Invalid IP address.
	CLI:Test Set Validate Invalid Options	unit_ipv4_address	${POINTS}	Error: unit_ipv4_address: Invalid IP address.
	CLI:Test Set Validate Invalid Options	unit_ipv4_address	${ONE_WORD}	Error: unit_ipv4_address: Invalid IP address.
	CLI:Test Set Validate Invalid Options	unit_ipv4_address	${ONE_NUMBER}	Error: unit_ipv4_address: Invalid IP address.
	CLI:Test Set Validate Invalid Options	unit_ipv4_address	${ONE_POINTS}	Error: unit_ipv4_address: Invalid IP address.
	CLI:Test Set Validate Invalid Options	unit_ipv4_address	${EXCEEDED}	Error: unit_ipv4_address: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	unit_ipv4_address	${UNIT_IPV4_ADDRESS}
	[Teardown]	CLI:Revert

Test unit_netmask: field
	CLI:Enter Path	/settings/system_preferences/
	CLI:Test Set Validate Invalid Options	unit_netmask	${EMPTY}	Error: unit_netmask: Validation error.
	CLI:Test Set Validate Invalid Options	unit_netmask	${WORD}	Error: unit_netmask: Validation error.
	CLI:Test Set Validate Invalid Options	unit_netmask	${NUMBER}	Error: unit_netmask: Validation error.
	CLI:Test Set Validate Invalid Options	unit_netmask	${POINTS}	Error: unit_netmask: Validation error.
	CLI:Test Set Validate Invalid Options	unit_netmask	${ONE_WORD}	Error: unit_netmask: Validation error.
	CLI:Test Set Validate Invalid Options	unit_netmask	${ONE_NUMBER}	Error: unit_netmask: Validation error.
	CLI:Test Set Validate Invalid Options	unit_netmask	${ONE_POINTS}	Error: unit_netmask: Validation error.
	CLI:Test Set Validate Invalid Options	unit_netmask	${EXCEEDED}	Error: unit_netmask: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	unit_netmask	${UNIT_NETMASK}
	[Teardown]	CLI:Revert

Test unit_interface field
	CLI:Enter Path	/settings/system_preferences/
	Run Keyword If	${HAS_ETH1} == ${TRUE}	CLI:Test Set Validate Normal Fields	unit_interface	eth0	eth1
	...	ELSE	CLI:Test Set Validate Normal Fields	unit_interface	eth0

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	unit_interface	${EMPTY}	Error: Missing value for parameter: unit_interface
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	unit_interface	${EMPTY}	Error: Missing value: unit_interface

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	unit_interface	${WORD}	Error: Invalid value: ${WORD} for parameter: unit_interface
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	unit_interface	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	unit_interface	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: unit_interface
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	unit_interface	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	unit_interface	${POINTS}	Error: Invalid value: ${POINTS} for parameter: unit_interface
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	unit_interface	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	unit_interface	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: unit_interface
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	unit_interface	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	unit_interface	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: unit_interface
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	unit_interface	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	unit_interface	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: unit_interface
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	unit_interface	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	unit_interface	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: unit_interface
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	unit_interface	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test iso_url field
	CLI:Enter Path	/settings/system_preferences/
	@{LIST}=	Create List	${EMPTY}	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${EXCEEDED}
	FOR	${VALUE}	IN	@{LIST}
		CLI:Test Set Validate Invalid Options	iso_url	${VALUE}
	END
	[Teardown]	CLI:Revert

Test if revert command works
	${OUTPUT}=	CLI:Revert	Raw
	Should Not Contain	${OUTPUT}	Error

Test Valid Values For Field=show_hostname_on_webui_header
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/system_preferences
	CLI:Test Set Field Options	show_hostname_on_webui_header	no	yes
	[Teardown]	CLI:Revert	Raw

Test Invalid Values For Field=show_hostname_on_webui_header
	[Tags]	EXCLUDEIN3_2
	${ALL_VALUES}=	Create List	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
	...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
	...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
	...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
	...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
	...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}	${EXCEEDED}
	CLI:Enter Path	/settings/system_preferences
	CLI:Test Set Field Invalid Options	show_hostname_on_webui_header	${EMPTY}
	...	Error: Missing value for parameter: show_hostname_on_webui_header

	FOR	${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	show_hostname_on_webui_header	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: show_hostname_on_webui_header
	END
	[Teardown]	CLI:Revert	Raw

Test Available Fields After Enable show_hostname_on_webui_header
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/system_preferences
	CLI:Set	show_hostname_on_webui_header=yes
	CLI:Test Set Available Fields	webui_header_hostname_color
	[Teardown]	CLI:Revert	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	${HAS_ETH1}=	CLI:Has ETH1
	Set Suite Variable	${HAS_ETH1}

SUITE:Teardown
	CLI:Close Connection

SUITE:Test Set Validate Invalid Options
    [Arguments]    ${FIELD}    ${VALUE}=${EMPTY}    ${ERROR}=Error:    ${revert}=no
    ${SETOUTPUT}=    CLI:Write    set . ${FIELD}=${VALUE}    Raw    Yes
    Should Contain   ${SETOUTPUT}    ${ERROR}
    Run Keyword If    '${revert}' == 'yes'    CLI:Revert