*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Custom Fields... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${FIELD_NAME}	field_name
${FIELD_VALUE}	field_value

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/custom_fields/
	CLI:Test Available Commands	cd	delete	event_system_clear	ls	reboot	show	whoami	change_password	edit
	...	exit	pwd	revert	add	commit	event_system_audit	hostname	quit	shell	shutdown	show_settings

Test visible fields for show command
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Show Command	field_name	field_value
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Show Command	field name	field value

Test visible fields after show command while adding
	CLI:Add
	CLI:Test Show Command	field_name	field_value

Test available commands after Add operation
	CLI:Test Available Commands	cancel	commit	ls	save	set	show

Test cancel command working after error
	${OUTPUT}=	CLI:Cancel
	Should Not Contain	${OUTPUT}	Error

Test adding empty fields
	Run Keyword If	'${NGVERSION}' == '3.2'	Set Tags	#implementation is different in nightly/official
	CLI:Enter Path	/settings/custom_fields/
	CLI:Add
	CLI:Set	field_name=${EMPTY} field_value=${EMPTY}
	${OUTPUT}=	CLI:Commit	Yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	Error: field_name: Validation error.\nError: field_value: Validation error.
	Run Keyword If  '${NGVERSION}' == '3.2'	Should Match regexp	${OUTPUT}	Error: field(\_|\\s)name: Field must not be empty.
	#Error message in official/ nightly is different
	CLI:Set Field	field_value	${FIELD_VALUE}
	${OUTPUT}=	CLI:Commit	Yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	Error: field_name: Validation error.\nError: field_value: Validation error.
	# 3.2 has no validation for field_name EMPTY
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Match regexp	${OUTPUT}	Error: field(\_|\\s)name: Field must not be empty.
	[Teardown]	CLI:Cancel	Raw

Test field_name field
	CLI:Enter Path	/settings/custom_fields/
	CLI:Add
	@{LIST}=	Create List	${EMPTY}	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${EXCEEDED}
	FOR	${VALUE}	IN	@{LIST}
		CLI:Test Set Field Invalid Options	field_name	${VALUE}
	END
	[Teardown]	CLI:Cancel

Test field_value field
	CLI:Enter Path	/settings/custom_fields/
	CLI:Add
	@{LIST}=	Create List	${EMPTY}	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${EXCEEDED}
	FOR	${VALUE}	IN	@{LIST}
		CLI:Test Set Field Invalid Options	field_value	${VALUE}
	END
	[Teardown]	CLI:Cancel

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection