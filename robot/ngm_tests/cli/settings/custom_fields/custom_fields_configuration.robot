*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Custom Fields... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${FIELD_NAME}	field_name
${FIELD_VALUE}	field_value
${MAX_VALUE}	123456789012345678901234567890123456789012345678901234567890654

*** Test Cases ***
Test adding valid values
	[Setup]	SUITE:Delete Custom Fields
	CLI:Enter Path	/settings/custom_fields/
	CLI:Add
	CLI:Set	field_name=${FIELD_NAME} field_value=${FIELD_VALUE}
	CLI:Commit
	CLI:Test Show Command Regexp	\\s+${FIELD_NAME}\\s+${FIELD_VALUE}
	${PATH_4_2}	Run Keyword And Return Status	CLI:Enter Path	/settings/custom_fields/${FIELD_NAME}
	Run Keyword If	not ${PATH_4_2}	CLI:Enter Path	/settings/custom_fields/1	#in v4.2.18+ the path is different
	Run Keyword If	'${NGVERSION}' == 4.2	CLI:Test Show Command	field name: ${FIELD_NAME}	field value: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' == 3.2	CLI:Test Show Command	field_name = ${FIELD_NAME}	field_value =  ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Show Command	field_name: ${FIELD_NAME}	field_value = ${FIELD_VALUE}
	CLI:Enter Path	/settings/custom_fields
	${PATH_4_2}	Run Keyword And Return Status	CLI:Delete	${FIELD_NAME}
	Run Keyword If	not ${PATH_4_2}	CLI:Delete	1	#in v4.2.18+ the path is different
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	SUITE:Delete Custom Fields

Test adding duplicate entry
	Skip If	'${NGVERSION}' == '3.2'	No error is shown for 3.2
	CLI:Enter Path	/settings/custom_fields/
	CLI:Add
	CLI:Set	field_name=${FIELD_NAME} field_value=${FIELD_VALUE}
	CLI:Commit
	CLI:Add
	CLI:Set	field_name=${FIELD_NAME} field_value=${FIELD_VALUE}
	${OUTPUT}=	CLI:Commit	Yes
	Should Contain	${OUTPUT}	Error: field_name: Entry already exists.
	CLI:Cancel
	${PATH_4_2}	Run Keyword And Return Status	CLI:Delete	${FIELD_NAME}
	Run Keyword If	not ${PATH_4_2}	CLI:Delete	1	#in v4.2.18+ the path is different
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	SUITE:Delete Custom Fields

Test edit custom field
	[Setup]	SUITE:Delete Custom Fields
	CLI:Enter Path	/settings/custom_fields/
	SUITE:Add Custom Fields
	CLI:Test Show Command Regexp	\\s+${FIELD_NAME}\\s+${FIELD_VALUE}

	@{LIST}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}
	FOR	${VALUE}	IN	@{LIST}
		${PATH_4_2}	Run Keyword And Return Status	CLI:Edit	${FIELD_NAME}
		Run Keyword If	not ${PATH_4_2}	CLI:Edit	1	#in v4.2.18+ the path is different
		CLI:Set Field	field_value	${FIELD_VALUE}
		CLI:Commit	Raw
	END
	${PATH_4_2}	Run Keyword And Return Status	CLI:Edit	${FIELD_NAME}
	Run Keyword If	not ${PATH_4_2}	CLI:Edit	1	#in v4.2.18+ the path is different
	CLI:Set Field	field_value	${MAX_VALUE}
	${OUTPUT}=	CLI:Commit	Raw
	Should not Contain	${OUTPUT}	Error: field_value: Validation error.
	CLI:Cancel	Raw

	${PATH_4_2}	Run Keyword And Return Status	CLI:Edit	${FIELD_NAME}
	Run Keyword If	not ${PATH_4_2}	CLI:Edit	1	#in v4.2.18+ the path is different
	CLI:Set Field	field_value	${EMPTY}
	${OUTPUT}=	CLI:Commit	Raw
    Run Keyword If	'${NGVERSION}' == '3.2'	Should not Contain	${OUTPUT}	Error: field_value: Validation error.
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Delete Custom Fields

SUITE:Teardown
	SUITE:Delete Custom Fields
	CLI:Close Connection

SUITE:Add Custom Fields
	CLI:Enter Path	/settings/custom_fields/
	CLI:Delete If Exists	1
	CLI:Add
	CLI:Test Set Field Invalid Options	field_name	${FIELD_NAME}
	CLI:Test Set Field Invalid Options	field_value	${FIELD_VALUE}
	CLI:Commit

SUITE:Delete Custom Fields
	CLI:Enter Path	/settings/custom_fields/
	${OUTPUT}=	CLI:Write	ls	user=Yes
	${OUTPUT}=	Remove String	${OUTPUT}	\r	\n	${SPACE}
	${CUSTOMFIELDS}=	Split String	${OUTPUT}	/
	FOR	${CUSTOMFIELD}	IN	@{CUSTOMFIELDS}
		CLI:Delete If Exists	${CUSTOMFIELD}
		CLI:Delete If Exists	1
	END