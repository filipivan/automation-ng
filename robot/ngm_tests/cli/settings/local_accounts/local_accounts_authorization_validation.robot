*** Settings ***
Resource		../../init.robot
Documentation	Test login on CLI after add a user in multiples groups
Metadata		Version	1.0
Metadata		Executed At	${HOST}
Force Tags		CLI		NON-CRITICAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_1	EXCLUDEIN5_2	EXCLUDEIN5_4
Default Tags	CLI	SSH	SHOW

Suite Setup		SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USER_TEST}	test
${USER_PASSWD}	test
${USER_GROUPS}	user,admin
${READ_ONLY_GROUP}	test-read-only
${READ_WRITE_GROUP}	test-read-write

*** Test Cases ***
Test to add new user test
	[Documentation]		Create a new test user and add to multiple groups
	CLI:Open
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	CLI:Set Field	username	${USER_TEST}
	CLI:Set Field	password	${USER_PASSWD}
	CLI:Set Field	user_group	${USER_GROUPS}
	CLI:Commit
	CLI:Enter Path	${USER_TEST}
	CLI:Test Show Command	username: ${USER_TEST}
	CLI:Close Connection

Test login with user test in groups, user and admin
	[Documentation]		Test user login test after adding them to user and admin groups
	CLI:Open	USERNAME=${USER_TEST}	PASSWORD=${USER_PASSWD}
	CLI:Test Ls Command		access/		system/		settings/
	CLI:Close Connection

Test login with user test in groups, user and read-only
	[Documentation]		Test user login test after adding them to user and read-only groups
	CLI:Open
	SUITE:Add to group		${USER_TEST}	${READ_ONLY_GROUP}
	CLI:Close Connection

	CLI:Open	USERNAME=${USER_TEST}	PASSWORD=${USER_PASSWD}
	CLI:Test Ls Command		access/		system/
	CLI:Close Connection

	CLI:Open
	SUITE:Set default groups	${USER_TEST}
	CLI:Close Connection

Test login with user test in groups, user and read-write
	[Documentation]		Test user login test after adding them to user and read-write groups
	CLI:Open
	SUITE:Add to group		${USER_TEST}	${READ_WRITE_GROUP}
	CLI:Close Connection

	CLI:Open	USERNAME=${USER_TEST}	PASSWORD=${USER_PASSWD}
	CLI:Test Ls Command		access/		system/		settings/
	CLI:Close Connection

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:User exists
	SUITE:Group exists	${READ_ONLY_GROUP}
	SUITE:Group exists	${READ_WRITE_GROUP}
	SUITE:Create group read-only	${READ_ONLY_GROUP}
	SUITE:Create group read-write	${READ_WRITE_GROUP}
	CLI:Close Connection

SUITE:Teardown
	CLI:Open
	SUITE:User exists
	SUITE:Group exists	${READ_ONLY_GROUP}
	SUITE:Group exists	${READ_WRITE_GROUP}
	CLI:Close Connection

SUITE:User exists
	[Documentation]		Check if the test user exists
	CLI:Enter Path	/settings/local_accounts/
	${OUTPUT}	CLI:Show
	${RESULT}	run keyword and return status  should contain	${OUTPUT}	${USER_TEST}
	Run Keyword If	${RESULT}	CLI:Delete Users	${USER_TEST}

SUITE:Group exists
	[Documentation]		Check if a group exists
	[Arguments]			${NAME_OF_GROUP}
	CLI:Enter Path	/settings/authorization/
	${OUTPUT}	CLI:Show
	${RESULT}	run keyword and return status  should contain	${OUTPUT}	${NAME_OF_GROUP}
	Run Keyword If	${RESULT}	SUITE:Delete group	${NAME_OF_GROUP}

SUITE:Create group read-only
	[Documentation]		Create group read-only
	[Arguments]			${NAME_OF_GROUP}
	CLI:Enter Path	/settings/authorization/
	CLI:Add
	CLI:Set		name=${NAME_OF_GROUP}
	CLI:Commit

SUITE:Create group read-write
	[Documentation]		Create group read-write
	[Arguments]			${NAME_OF_GROUP}
	CLI:Enter Path	/settings/authorization/
	CLI:Add
	CLI:Set		name=${NAME_OF_GROUP}
	CLI:Commit
	CLI:Enter Path	/settings/authorization/${NAME_OF_GROUP}/profile/
	CLI:Set	track_system_information=yes
	CLI:Set	terminate_sessions=yes
	CLI:Set	software_upgrade_and_reboot_system=yes
	CLI:Set	configure_system=yes
	CLI:Set	configure_user_accounts=yes
	CLI:Set	apply_&_save_settings=yes
	CLI:Set	shell_access=yes
	CLI:Commit

SUITE:Add to group
	[Documentation]		Add a user to a group
	[Arguments]			${NAME_OF_USER}	${NAME_OF_GROUP}
	CLI:Enter Path	/settings/local_accounts/${NAME_OF_USER}/
	CLI:Set		user_group=user,${NAME_OF_GROUP}
	CLI:Commit

SUITE:Set default groups
	[Documentation]		Return user to default groups
	[Arguments]			${NAME_OF_USER}
	CLI:Enter Path	/settings/local_accounts/${NAME_OF_USER}
	CLI:Set		user_group=user
	CLI:Commit

SUITE:Delete group
	[Documentation]		Delete a group
	[Arguments]			${NAME_OF_GROUP}
	CLI:Enter Path	/settings/authorization/
	CLI:Delete Confirm	${NAME_OF_GROUP}