*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Local Accounts Commands... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${ACCOUNT_EXPIRATION_DATE}	2037-12-30

*** Test Cases ***
Test show valid user fields
	SUITE:User Exists
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	CLI:Set Field	username	sneezy
	CLI:Set Field	password	sneezy
	CLI:Set Field	password_change_at_login	yes
	CLI:Set Field	hash_format_password	no
	CLI:Set Field	account_expiration-date	${ACCOUNT_EXPIRATION_DATE}
	CLI:Commit
	CLI:Test Ls Command	sneezy
	CLI:Enter Path	sneezy
	CLI:Test Show Command	username: sneezy	password =	hash_format_password = no	account_expiration-date = ${ACCOUNT_EXPIRATION_DATE}
	...	password_change_at_login = yes	user_group = user
	[Teardown]	CLI:Cancel	Raw

Test correct values for adding user
	SUITE:User Exists
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	CLI:Set Field	username	sneezy
	CLI:Set Field	password	sneezy
	CLI:Commit
	CLI:Test Show Command	sneezy
	CLI:Delete Users	sneezy
	[Teardown]	CLI:Cancel	Raw

Test editing values for user
	SUITE:User Exists
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add User	sneezy	sneezy
	CLI:Test Show Command	sneezy
	CLI:Edit	sneezy
	CLI:Test Show Command	username: sneezy	password =	hash_format_password =	account_expiration-date =
	...	password_change_at_login = no	user_group = user
	CLI:Set Field	account_expiration-date	${ACCOUNT_EXPIRATION_DATE}
	CLI:Commit
	CLI:Enter Path	sneezy
	CLI:Test Show Command	username: sneezy	password =	hash_format_password =	account_expiration-date = ${ACCOUNT_EXPIRATION_DATE}
	...	password_change_at_login = no	user_group = user
	CLI:Delete Users	sneezy
	[Teardown]	CLI:Cancel	Raw

Test adding user with a dot in their username
	Skip If	'${NGVERSION}' < '4.0'	Feature not implemented in 3.2
	CLI:Enter Path	/settings/local_accounts/
	${RESULT}=	CLI:Check If User Exists	user.name
	Run Keyword If	${RESULT}	CLI:Delete Users	user.name
	CLI:Add
	CLI:Set Field	username	user.name
	CLI:Set Field	password	username
	CLI:Commit
	CLI:Test Show Command	user.name
	[Teardown]	CLI:Cancel	Raw

Test logging in with user=user.name
	Skip If	'${NGVERSION}' < '4.0'	Feature not implemented in 3.2
	Open Connection	${HOST}	alias=user_session
	Login	user.name	username	startping=no
	${OUTPUT}=	CLI:Write	whoami
	Should Contain	${OUTPUT}	user.name
	Close Connection
	[Teardown]	Run Keywords	CLI:Switch Connection	default
	...	AND	CLI:Delete Users	user.name

Test adding two users with same username
	Skip If	'${NGVERSION}' < '4.0'	Error message not implemented in 3.2
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add User	sneezy	sneezy
	CLI:Test Show Command	sneezy
	CLI:Add
	CLI:Set Field	username	sneezy
	CLI:Set Field	password	sneezy
	${OUTPUT}=	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: username: Entry already exists.
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test logging in with invalid username
	SUITE:User Exists
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add User	sneezy	sneezy
	Open Connection	${HOST}	alias=user_session
	${OUTPUT}=	Run Keyword And Expect Error	Authentication failed for user 'snooze'.	Login	snooze	sneezy	startping=no
	Log to Console	${OUTPUT}
	Close Connection
	[Teardown]	Run Keywords	CLI:Switch Connection	default
	...	AND	CLI:Delete Users	sneezy

Test logging in with invalid password
	SUITE:User Exists
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add User	sneezy	sneezy
	Open Connection	${HOST}	alias=user_session
	${OUTPUT}=	Run Keyword And Expect Error	Authentication failed for user 'sneezy'.	Login	sneezy	snooze	startping=no
	Log to Console	${OUTPUT}
	Close Connection
	[Teardown]	Run Keywords	CLI:Switch Connection	default
	...	AND	CLI:Delete Users	sneezy

Test logging in with valid user
	SUITE:User Exists
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add User	sneezy	sneezy
	Open Connection	${HOST}	alias=user_session
	Login	sneezy	sneezy	startping=no
	${OUTPUT}=	CLI:Write	whoami
	Should Contain	${OUTPUT}	sneezy
	Close Connection
	[Teardown]	Run Keywords	CLI:Switch Connection	default
	...	AND	CLI:Delete Users	sneezy

Test lock functionality
	SUITE:User Exists
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add User	sneezy	sneezy
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Show Command Regexp	sneezy\\s+Unlocked\\s+user
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Show Command Regexp	sneezy\\s+unlocked\\s+user
	CLI:Write	lock sneezy
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Show Command Regexp	sneezy\\s+Locked\\s+user
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Show Command Regexp	sneezy\\s+locked\\s+user
	CLI:Write	unlock sneezy
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Show Command Regexp	sneezy\\s+Unlocked\\s+user
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Show Command Regexp	sneezy\\s+unlocked\\s+user
	CLI:Delete Users	sneezy
	[Teardown]	CLI:Cancel	Raw

Test logging in with locked user
	SUITE:User Exists
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add User	sneezy	sneezy
	CLI:Write	lock sneezy
	Open Connection	${HOST}	alias=user_session
	${OUTPUT}=	Run Keyword And Expect Error	Authentication failed for user 'sneezy'.	Login	sneezy	sneezy	startping=no
	Log to Console	${OUTPUT}
	Close Connection
	[Teardown]	Run Keywords	CLI:Switch Connection	default
	...	AND	CLI:Delete Users	sneezy

Test deleting protected user
	Skip If	'${NGVERSION}' < '4.1'	Error message not implemented in 3.2,4.0
	CLI:Enter Path	/settings/local_accounts/
	Write	delete ${DEFAULT_USERNAME}
	${DELETE_QUESTION}=	Read Until	:
	Write	yes
	${OUTPUT}=	CLI:Read Until Prompt	Raw
	Should Contain	${OUTPUT}	Error: Warning: Protected users cannot be deleted

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:User Exists

SUITE:Teardown
	SUITE:User Exists
	CLI:Close Connection

SUITE:User Exists
	CLI:Enter Path	/settings/local_accounts/
	${RESULT}=	CLI:Check If User Exists	sneezy
	Run Keyword If	${RESULT}	CLI:Delete Users	sneezy