*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Local Accounts Settings... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${PASSWORD}	${QA_PASSWORD}
${USER}	username
${ACCOUNT_EXPIRATION_DATE}	2037-12-30
${API_ACCOUNT}		api_account_test

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/local_accounts/
	${list}=	Create List	cd	hostname	set	change_password	ls	shell	commit	pwd	show
	...	event_system_audit	quit	event_system_clear	reboot	shutdown	exit	revert
	...	whoami	show_settings	add	delete	lock
	CLI:Test Available Commands	@{list}

Test show_settings command
	CLI:Enter Path	/settings/local_accounts/
	${OUTPUT}=	CLI:Write	show_settings
	Should Match Regexp	${OUTPUT}	/settings/local_accounts/${DEFAULT_USERNAME} username=${DEFAULT_USERNAME}
	Should Match Regexp	${OUTPUT}	/settings/local_accounts/${DEFAULT_USERNAME} hash_format_password=no
	Should Match Regexp	${OUTPUT}	/settings/local_accounts/${DEFAULT_USERNAME} password_change_at_login=no
	Should Match Regexp	${OUTPUT}	/settings/local_accounts/${DEFAULT_USERNAME} user_group=${DEFAULT_USERNAME}

Test available set fields for adding user
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	CLI:Test Set Available Fields	account_expiration-date	hash_format_password	password	password_change_at_login
	...	user_group	username
	[Teardown]	CLI:Cancel	Raw

Test available show fields for adding user
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Show Command	username =	password =	hash_format_password = no
	...	account_expiration-date =	password_change_at_login =	user_group = user
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Show Command	username =	password =	hash_format_password = no
	...	account_expiration-date =	password_change_at_login = no	user_group = user
	[Teardown]	CLI:Cancel	Raw

Test correct values for adding user field=account_expiration-date
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	CLI:Test Set Field Valid Options	account_expiration-date	2019-09-29	no
	[Teardown]	CLI:Cancel	Raw

Test visible fields for show command
	${LIST}=	Create List	username	state	user group	${DEFAULT_USERNAME}
	Run Keyword If	'${NGVERSION}' == '3.2'	Append To List	${LIST}	unlocked
	...	ELSE	Append To List	${LIST}	Unlocked
	CLI:Test Show Command	@{LIST}

Test ls command
	CLI:Test Ls Command	${DEFAULT_USERNAME}

Test username field | username | empty - word
	Run Keyword If	'${NGVERSION}' == '3.2'	Set Tags	NON-CRITICAL	NEED-REVIEW
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	CLI:Set	password=${PASSWORD} username=${EMPTY}
	${OUTPUT}=	CLI:Write	commit	Raw	Yes
	Should Be Equal	${OUTPUT}	Error: username: Field must not be empty.

	CLI:Set Field	username	${WORD}
	CLI:Commit
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete Users	${WORD}

Test username field | username | number - points - one_word
	Run Keyword If	'${NGVERSION}' == '3.2'	Set Tags	NON-CRITICAL	NEED-REVIEW
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	CLI:Set	password=${PASSWORD} username=${NUMBER}
	${OUTPUT}=	CLI:Write	commit	Raw	Yes
	Should Be Equal	${OUTPUT}	Error: username: This field should contain only numbers, letters, ., -, and _, and also it should start with letter or _

	CLI:Set Field	username	${POINTS}
	${OUTPUT}=	CLI:Write	commit	Raw	Yes
	Should Be Equal	${OUTPUT}	Error: username: This field should contain only numbers, letters, ., -, and _, and also it should start with letter or _

	CLI:Set Field	username	${ONE_WORD}
	CLI:Commit
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete Users	${NUMBER}	${POINTS}	${ONE_WORD}

Test username field | username | one_number - one_points - exceeded
	Run Keyword If	'${NGVERSION}' == '3.2'	Set Tags	NON-CRITICAL	NEED-REVIEW
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	CLI:Set	password=${PASSWORD} username=${ONE_NUMBER}
	${OUTPUT}=	CLI:Write	commit	Raw	Yes
	Should Be Equal	${OUTPUT}	Error: username: This field should contain only numbers, letters, ., -, and _, and also it should start with letter or _

	CLI:Set	password=${PASSWORD} username=${ONE_POINTS}
	${OUTPUT}=	CLI:Write	commit	Raw	Yes
	Should Be Equal	${OUTPUT}	Error: username: This field should contain only numbers, letters, ., -, and _, and also it should start with letter or _

	CLI:Set	password=${PASSWORD} username=${EXCEEDED}
	${OUTPUT}=	CLI:Write	commit	Raw	Yes
	Should Contain	${OUTPUT}		Error: username: Exceeded the maximum size for this field.

	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete Users	${ONE_NUMBER}	${ONE_POINTS}	${EXCEEDED}

Test password field | invalid
	Run Keyword If	'${NGVERSION}' == '3.2'	Set Tags	NON-CRITICAL	NEED-REVIEW
	CLI:Enter Path	/settings/local_accounts
	CLI:Add
	CLI:Write	set username=${USER}
	CLI:Set Field	password	${EMPTY}
	${OUTPUT}=	CLI:Write	commit	Raw	Yes
	Should Be Equal	${OUTPUT}	Error: password: Password cannot be empty.
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete Users	${USER}

Test hash_format_password field
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	hash_format_password	yes	no
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	hash_format_password	yes	no

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	hash_format_password	${EMPTY}	Error: Missing value for parameter: hash_format_password
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	hash_format_password	${EMPTY}	Error: Missing value: hash_format_password

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	hash_format_password	${WORD}	Error: Invalid value: ${WORD} for parameter: hash_format_password
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	hash_format_password	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	hash_format_password	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: hash_format_password
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	hash_format_password	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	hash_format_password	${POINTS}	Error: Invalid value: ${POINTS} for parameter: hash_format_password
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	hash_format_password	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	hash_format_password	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: hash_format_password
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	hash_format_password	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	hash_format_password	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: hash_format_password
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	hash_format_password	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	hash_format_password	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: hash_format_password
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	hash_format_password	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	hash_format_password	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: hash_format_password
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	hash_format_password	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete Users	${USER}	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${EXCEEDED}

Test account_expiration-date field | exceeded
	Run Keyword If	'${NGVERSION}' == '3.2'	Set Tags	NON-CRITICAL	NEED-REVIEW
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	CLI:Write	set username=${USER}
	CLI:Write	set password=${PASSWORD}
	CLI:Set Field	account_expiration-date	${EXCEEDED}
	${OUTPUT}=	CLI:Write	commit	Raw	Yes
	Should Be Equal	${OUTPUT}	Error: account_expiration-date: Exceeded the maximum size for this field.

	CLI:Set Field	account_expiration-date	${ACCOUNT_EXPIRATION_DATE}
	CLI:Commit
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete Users	${USER}

Test account_expiration-date field | invalid
	Run Keyword If	'${NGVERSION}' == '3.2'	Set Tags	NON-CRITICAL	NEED-REVIEW
	CLI:Enter Path	/settings/local_accounts/
	@{ITEM}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}
	CLI:Add
	CLI:Write	set username=${USER}
	CLI:Write	set password=${PASSWORD}
	FOR			${ITEM}	IN	@{ITEM}
		CLI:Set Field	account_expiration-date	${ITEM}
		${OUTPUT}=	CLI:Write	commit	Raw	Yes
		Should Be Equal	${OUTPUT}	Error: account_expiration-date: Invalid date. This date should be in YYYY-MM-DD format, greater than present date and no more than 2037-12-31.
	END
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete Users	${USER}

Test password_change_at_login field
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	password_change_at_login	yes	no
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	password_change_at_login	yes	no

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	password_change_at_login	${EMPTY}	Error: Missing value for parameter: password_change_at_login
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	password_change_at_login	${EMPTY}	Error: Missing value: password_change_at_login

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	password_change_at_login	${WORD}	Error: Invalid value: ${WORD} for parameter: password_change_at_login
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	password_change_at_login	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	password_change_at_login	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: password_change_at_login
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	password_change_at_login	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	password_change_at_login	${POINTS}	Error: Invalid value: ${POINTS} for parameter: password_change_at_login
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	password_change_at_login	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	password_change_at_login	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: password_change_at_login
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	password_change_at_login	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	password_change_at_login	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: password_change_at_login
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	password_change_at_login	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	password_change_at_login	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: password_change_at_login
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	password_change_at_login	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	password_change_at_login	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: password_change_at_login
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	password_change_at_login	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete Users	${USER}	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${EXCEEDED}

Test user_group field
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	user_group	user	${DEFAULT_USERNAME}	user,${DEFAULT_USERNAME}	${DEFAULT_USERNAME},user
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	user_group	user	${DEFAULT_USERNAME}	user,${DEFAULT_USERNAME}	${DEFAULT_USERNAME},user

	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	user_group	${EMPTY}	Error: Invalid value: null

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	user_group	${WORD}	Error: Invalid value: ${WORD} for parameter: user_group
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	user_group	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	user_group	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: user_group
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	user_group	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	user_group	${POINTS}	Error: Invalid value: ${POINTS} for parameter: user_group
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	user_group	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	user_group	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: user_group
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	user_group	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	user_group	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: user_group
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	user_group	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	user_group	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: user_group
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	user_group	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	user_group	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: user_group
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	user_group	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Cancel	Raw

Test lock command with no arguments
	CLI:Enter Path	/settings/local_accounts/
	${OUTPUT}=	CLI:Write	lock	Yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	Error: Not enough arguments
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${OUTPUT}	Error: Missing arguments

Test unlock command with no arguments
	CLI:Enter Path	/settings/local_accounts/
	${OUTPUT}=	CLI:Write	unlock	Yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	Error: Not enough arguments
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${OUTPUT}	Error: Missing arguments

Test lock command with nonexistent account
	CLI:Enter Path	/settings/local_accounts/
	${OUTPUT}=	CLI:Write	lock invalid	Yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	Error: Invalid Target name: invalid. Please, use tab-tab to obtain available targets.
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${OUTPUT}	Error: Invalid Target: invalid. Press <tab> for a list of valid targets.

Test unlock command with nonexistent account
	CLI:Enter Path	/settings/local_accounts/
	${OUTPUT}=	CLI:Write	unlock invalid	Yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	Error: Invalid Target name: invalid. Please, use tab-tab to obtain available targets.
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${OUTPUT}	Error: Invalid Target: invalid. Press <tab> for a list of valid targets.

Test account_type=api_account
	[Tags]  CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
	CLI:Enter Path  	/settings/local_accounts
	CLI:Add
	CLI:Set  			username=${API_ACCOUNT} account_type=api_account
	SUITE:Check Warning And Key Length
	CLI:Commit
	${HAS_NEW_USER}=	CLI:Show
	Should Contain 		${HAS_NEW_USER}		${API_ACCOUNT}
	CLI:Enter Path  	/settings/local_accounts/${API_ACCOUNT}/
	${OUTPUT}=			CLI:Show
	Should Not Contain  ${OUTPUT}	${KEY}
	[Teardown]  CLI:Cancel  	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Delete Users	${USER}	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${EXCEEDED}	${API_ACCOUNT}

SUITE:Teardown
	CLI:Delete Users	${USER}	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${EXCEEDED}	${API_ACCOUNT}
	CLI:Close Connection

SUITE:Check Warning And Key Length
	${OUTPUT}=		CLI:Show
	Should Contain 		${OUTPUT}	Copy and store the API Key as it will not be possible to recover it after clicking on Save button.
	${KEY}=				Get Lines Containing String		${OUTPUT}		api_key:
	${KEY}=				Get Substring	${KEY}	9	#ignores the first 9 characters in the line ("api_key: ")
	Set Suite Variable 	${KEY}
	Length Should Be	${KEY} 		36