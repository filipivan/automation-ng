*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > API authetication using keys through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${REGULAR_ACCOUNT}	regular_account_test
${API_ACCOUNT}		api_account_test

*** Test Cases ***
Create a new API account
	CLI:Enter Path  	/settings/local_accounts
	CLI:Add
	CLI:Set  			username=${API_ACCOUNT} password=api_only account_type=api_account
	SUITE:Get API Key
	CLI:Commit
	${OUTPUT}=			CLI:Show
	Should Contain 		${OUTPUT}			${API_ACCOUNT}
	${CLI_SSH}=			Run Keyword and Return Status 	CLI:Open	USERNAME=${API_ACCOUNT}		PASSWORD=api_only
	Should Not Be True	${CLI_SSH}
	${CLI_SSH}=			Run Keyword and Return Status 	CLI:Open	USERNAME=${API_ACCOUNT}		PASSWORD=${KEY}
	Should Not Be True	${CLI_SSH}
	[Teardown]	CLI:Switch Connection	HOST

Reset the API key
	CLI:Enter Path  /settings/local_accounts/${API_ACCOUNT}/
	CLI:Write  		reset_api_key
	SUITE:Get API Key
	CLI:Commit
	${CLI_SSH}=			Run Keyword and Return Status 	CLI:Open	USERNAME=${API_ACCOUNT}		PASSWORD=${KEY}
	Should Not Be True	${CLI_SSH}
	CLI:Switch Connection 	HOST
	[Teardown]  CLI:Delete Users  ${API_ACCOUNT}

Change from a regular account to API account
	CLI:Enter Path  /settings/local_accounts
	CLI:Add
	CLI:Set  		username=${REGULAR_ACCOUNT} password=regular account_type=regular_account
	CLI:Commit
	${OUTPUT}=			CLI:Show
	Should Contain 		${OUTPUT}			${REGULAR_ACCOUNT}
	${CLI_SSH}=			Run Keyword and Return Status 	CLI:Open	USERNAME=${REGULAR_ACCOUNT}		PASSWORD=regular
	Should Be True		${CLI_SSH}
	CLI:Switch Connection 	HOST
	CLI:Enter Path  	/settings/local_accounts/
	CLI:Edit  			${REGULAR_ACCOUNT}
	CLI:Set  			account_type=api_account
	SUITE:Get API Key
	CLI:Commit
	${CLI_SSH}=			Run Keyword and Return Status 	CLI:Open	USERNAME=${REGULAR_ACCOUNT}		PASSWORD=regular
	Should Not Be True		${CLI_SSH}
	${CLI_SSH}=			Run Keyword and Return Status 	CLI:Open	USERNAME=${API_ACCOUNT}		PASSWORD=${KEY}
	Should Not Be True	${CLI_SSH}
	CLI:Switch Connection 	HOST

*** Keywords ***
SUITE:Setup
	CLI:Open	session_alias=HOST
	CLI:Delete Users  ${API_ACCOUNT}
	CLI:Delete Users  ${REGULAR_ACCOUNT}
SUITE:Teardown
	CLI:Delete Users  ${API_ACCOUNT}
	CLI:Delete Users  ${REGULAR_ACCOUNT}
	CLI:Close Connection

SUITE:Get API Key
	${HAS_API_KEY}=		CLI:Show
	Should Contain 		${HAS_API_KEY}		api_key
	${KEY}=				Get Lines Containing String		${HAS_API_KEY}		api_key:
	${KEY}=				Get Substring	${KEY}	9	#ignores the first 9 characters in the line ("api_key: ")
	Log to Console		${KEY}
	Set Suite Variable 	${KEY}