*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Local Accounts Settings... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USER}	sneezy
${PASSWD}	sneezy
${ACCOUNT_EXPIRATION_DATE}	2037-12-30

*** Test Cases ***
Test username field
	CLI:Enter Path	/settings/local_accounts/${USER}/
	CLI:Test Validate Unmodifiable Field	username	${USER}

Test hash_format_password field
	[Tags]	NON-CRITICAL	BUG_363	BUG_NG_8305
	#Works as expected Bug 363: The hash format checkbox just indicates if the password entered is in plain text or hash format.
	CLI:Enter Path	/settings/local_accounts/${USER}/
	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Normal Fields	hash_format_password	yes	no
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Normal Fields	hash_format_password	yes	no

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	hash_format_password	${EMPTY}	Error: Missing value for parameter: hash_format_password
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	hash_format_password	${EMPTY}	Error: Missing value: hash_format_password

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	hash_format_password	${WORD}	Error: Invalid value: ${WORD} for parameter: hash_format_password
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	hash_format_password	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	hash_format_password	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: hash_format_password
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	hash_format_password	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	hash_format_password	${POINTS}	Error: Invalid value: ${POINTS} for parameter: hash_format_password
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	hash_format_password	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	hash_format_password	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: hash_format_password
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	hash_format_password	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	hash_format_password	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: hash_format_password
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	hash_format_password	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	hash_format_password	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: hash_format_password
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	hash_format_password	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	hash_format_password	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: hash_format_password
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	hash_format_password	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test account_expiration-date field
	CLI:Enter Path	/settings/local_accounts/${USER}/
	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	account_expiration-date	${EMPTY}
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	account_expiration-date	${EMPTY}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	account_expiration-date	${WORD}	Error: account_expiration-date: Invalid date. This date should be in YYYY-MM-DD format, greater than present date and no more than 2037-12-31.
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	account_expiration-date	${WORD}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	account_expiration-date	${NUMBER}	Error: account_expiration-date: Invalid date. This date should be in YYYY-MM-DD format, greater than present date and no more than 2037-12-31.
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	account_expiration-date	${NUMBER}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	account_expiration-date	${POINTS}	Error: account_expiration-date: Invalid date. This date should be in YYYY-MM-DD format, greater than present date and no more than 2037-12-31.
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	account_expiration-date	${POINTS}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	account_expiration-date	${ONE_WORD}	Error: account_expiration-date: Invalid date. This date should be in YYYY-MM-DD format, greater than present date and no more than 2037-12-31.
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	account_expiration-date	${ONE_WORD}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	account_expiration-date	${ONE_NUMBER}	Error: account_expiration-date: Invalid date. This date should be in YYYY-MM-DD format, greater than present date and no more than 2037-12-31.
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	account_expiration-date	${ONE_NUMBER}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	account_expiration-date	${ONE_POINTS}	Error: account_expiration-date: Invalid date. This date should be in YYYY-MM-DD format, greater than present date and no more than 2037-12-31.
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	account_expiration-date	${ONE_POINTS}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	account_expiration-date	${EXCEEDED}	Error: account_expiration-date: Exceeded the maximum size for this field.
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	account_expiration-date	${EXCEEDED}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	account_expiration-date	${ACCOUNT_EXPIRATION_DATE}
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	account_expiration-date	${ACCOUNT_EXPIRATION_DATE}
	[Teardown]	CLI:Revert

Test password_change_at_login field
	CLI:Enter Path	/settings/local_accounts/${USER}/
	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Normal Fields	password_change_at_login	yes	no
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Normal Fields	password_change_at_login	yes	no

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	password_change_at_login	${EMPTY}	Error: Missing value for parameter: password_change_at_login
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	password_change_at_login	${EMPTY}	Error: Missing value: password_change_at_login

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	password_change_at_login	${WORD}	Error: Invalid value: ${WORD} for parameter: password_change_at_login
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	password_change_at_login	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	password_change_at_login	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: password_change_at_login
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	password_change_at_login	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	password_change_at_login	${POINTS}	Error: Invalid value: ${POINTS} for parameter: password_change_at_login
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	password_change_at_login	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	password_change_at_login	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: password_change_at_login
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	password_change_at_login	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	password_change_at_login	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: password_change_at_login
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	password_change_at_login	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	password_change_at_login	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: password_change_at_login
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	password_change_at_login	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	password_change_at_login	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: password_change_at_login
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	password_change_at_login	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test user_group field
	CLI:Enter Path	/settings/local_accounts/${USER}/
	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Normal Fields	user_group	user	${DEFAULT_USERNAME}	${DEFAULT_USERNAME},user
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Normal Fields	user_group	user	${DEFAULT_USERNAME}	${DEFAULT_USERNAME},user

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	user_group	${EMPTY}	Error: user_group: Field must not be empty.
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	user_group	${EMPTY}	Error: Invalid value: null

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	user_group	${WORD}	Error: Invalid value: ${WORD} for parameter: user_group
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	user_group	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	user_group	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: user_group
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	user_group	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	user_group	${POINTS}	Error: Invalid value: ${POINTS} for parameter: user_group
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	user_group	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	user_group	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: user_group
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	user_group	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	user_group	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: user_group
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	user_group	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	user_group	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: user_group
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	user_group	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Set Validate Invalid Options	user_group	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: user_group
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Set Validate Invalid Options	user_group	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test lock user
	CLI:Enter Path	/settings/local_accounts/
	CLI:Write	lock ${USER}
	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Show Command Regexp	${USER}\\s+Locked
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Show Command Regexp	${USER}\\s+locked

Test unlock user
	CLI:Enter Path	/settings/local_accounts/
	CLI:Write	unlock ${USER}
	Run Keyword If	"${NGVERSION}" >= "4.2"	CLI:Test Show Command Regexp	${USER}\\s+Unlocked
	Run Keyword If	"${NGVERSION}" == "3.2"	CLI:Test Show Command Regexp	${USER}\\s+unlocked

Test password field | valid
	[Setup]	CLI:Delete Users	${USER}
	CLI:Enter Path	/settings/local_accounts
	@{ITEMS}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}
	FOR			${ITEM}	IN	@{ITEMS}
		CLI:Add
		CLI:Write	set username=${USER} password=${ITEM}
		CLI:Commit
		CLI:Delete Users	${USER}
	END
	[Teardown]	CLI:Cancel	Raw

Test account_expiration-date field | empty
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	CLI:Set	username=${USER} password=${PASSWD} account_expiration-date=${EMPTY}
	CLI:Commit
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete Users	${USER}

Test valid value for hash_format_password
	#Works as expected Bug 363: The hash format checkbox just indicates if the password entered is in plain text or hash format.
	[Tags]	NON-CRITICAL	BUG_363	BUG_NG_8305
	[Setup]	CLI:Delete Users	${USER}
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	CLI:Set	username=${USER} password=${USER} hash_format_password=yes
	CLI:Commit
	CLI:Test Ls Command	${USER}
	CLI:Enter Path	${USER}
	CLI:Test Show Command	hash_format_password = yes
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete Users	${USER}

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Delete Users	${USER}
	CLI:Add User	${USER}	${PASSWD}

SUITE:Teardown
	CLI:Delete Users	${USER}
	CLI:Close Connection

