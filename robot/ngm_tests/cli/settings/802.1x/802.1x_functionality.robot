*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > functionality file about 802.1x Feature through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL	BUG_NG-9649

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
@{CRED_AUTH_METHOD}	MD5	TLS	PEAP	TTLS
@{CRED_INNER_AUTH}	MD5	PAP	CHAP	MSCHAP	MSCHAPV2
@{CRED_ANONYMOUS}	test_cred_anonymous_0	test_cred_anonymous_1	test_cred_anonymous_2	test_cred_anonymous_3
		...	test_cred_anonymous_4	test_cred_anonymous_5	test_cred_anonymous_6
@{PROF_TYPES}	internal_eap_server	radius_server	supplicant
${RETRANSMIT_INTERVAL}	15
${VLAN_ID}		30
${VLAN_NAME}		TEST_802_1X_VLAN
${CONNECTION_IPV4_LEFT}	10.0.200.30
${CONNECTION_IPV4_RIGHT}	10.0.200.32
@{PROF_INTERNAL_EAP_SERVERS}=	test_internal_eap_server_all	test_internal_eap_server_md5	test_internal_eap_server_tls
		...	test_internal_eap_server_peap_md5	test_internal_eap_server_peap_mschapv2
		...	test_internal_eap_server_ttls_md5	test_internal_eap_server_ttls_pap
		...	test_internal_eap_server_ttls_chap	test_internal_eap_server_ttls_mschap
		...	test_internal_eap_server_ttls_mschapv2
${PROF_RADIUS_SERVER}=	test_radius_authentication_server
@{PROF_SUPPLICANTS}=	test_supplicant_md5	test_supplicant_tls	test_supplicant_peap_md5
		...	test_supplicant_peap_mschapv2	test_supplicant_ttls_md5	test_supplicant_ttls_pap
		...	test_supplicant_ttls_chap	test_supplicant_ttls_mschap
		...	test_supplicant_ttls_mschapv2
@{SIMPLE_CREDS}	${RADIUSSERVER2_USER_TESTMD5}	${RADIUSSERVER2_USER_TESTTLS}
@{PEAP_CREDS}	${RADIUSSERVER2_USER_TESTPEAPMD5}	${RADIUSSERVER2_USER_TESTPEAPMSCH2}
@{TTLS_CREDS}	${RADIUSSERVER2_USER_TESTTTLSMD5}	${RADIUSSERVER2_USER_TESTTTLSPAP}
		...	${RADIUSSERVER2_USER_TESTTTLSCHAP}	${RADIUSSERVER2_USER_TESTTTLSMSCH}
		...	${RADIUSSERVER2_USER_TESTTTLSMSCH2}
@{CERTIFICATE_INFOR}	client certificate:	Certificate:	Data:	Subject Public Key Info:	Signature Algorithm:
		...	BEGIN CERTIFICATE	END CERTIFICATE	BEGIN ENCRYPTED PRIVATE KEY	END ENCRYPTED PRIVATE KEY
*** Test Cases ***
Test Authorize 802.1x Left With MD5 On Internal Eap Server And Right With Supplicant MD5
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Authorize 802.1x Session And Test Ping Between NSRs	test_internal_eap_server_md5	test_supplicant_md5
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw

Test Unauthorize 802.1x Left With MD5 On Internal Eap Server And Right With Supplicant MD5
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Unauthorize 802.1x Session And Test Ping Between NSRs
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw


Test Authorize 802.1x Left With PEAP And MD5 On Internal Eap Server And Right With Supplicant PEAP And MD5
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Authorize 802.1x Session And Test Ping Between NSRs	test_internal_eap_server_peap_md5
	...	test_supplicant_peap_md5
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw

Test Unauthorize 802.1x Left With PEAP And MD5 On Internal Eap Server And Right With Supplicant PEAP And MD5
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Unauthorize 802.1x Session And Test Ping Between NSRs
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw


Test Authorize 802.1x Left With PEAP And MSCHAPV2 On Internal Eap Server And Right With Supplicant PEAP And MSCHAPV2
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Authorize 802.1x Session And Test Ping Between NSRs	test_internal_eap_server_peap_mschapv2
	...	test_supplicant_peap_mschapv2
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw

Test Unauthorize 802.1x Left With PEAP And MSCHAPV2 On Internal Eap Server And Right With Supplicant PEAP And MSCHAPV2
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Unauthorize 802.1x Session And Test Ping Between NSRs
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw


Test Authorize 802.1x Left With TLS On Internal Eap Server And Right With Supplicant TLS
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Authorize 802.1x Session And Test Ping Between NSRs	test_internal_eap_server_tls	test_supplicant_tls
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw

Test Unauthorize 802.1x Left With TLS On Internal Eap Server And Right With Supplicant TLS
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Unauthorize 802.1x Session And Test Ping Between NSRs
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw

Test Generate TLS Certificates
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE: Generate TLS Certificates
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw


Test Authorize 802.1x Left With TTLS And MD5 On Internal Eap Server And Right With Supplicant TTLS And MD5
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Authorize 802.1x Session And Test Ping Between NSRs	test_internal_eap_server_ttls_md5
	...	test_supplicant_ttls_md5
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw

Test Unauthorize 802.1x Left With TTLS And MD5 On Internal Eap Server And Right With Supplicant TTLS And MD5
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Unauthorize 802.1x Session And Test Ping Between NSRs
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw


Test Authorize 802.1x Left With TTLS And PAP On Internal Eap Server And Right With Supplicant TTLS And PAP
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Authorize 802.1x Session And Test Ping Between NSRs	test_internal_eap_server_ttls_pap
	...	test_supplicant_ttls_pap
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw

Test Unauthorize 802.1x Left With TTLS And PAP On Internal Eap Server And Right With Supplicant TTLS And PAP
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Unauthorize 802.1x Session And Test Ping Between NSRs
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw


Test Authorize 802.1x Left With TTLS And CHAP On Internal Eap Server And Right With Supplicant TTLS And CHAP
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Authorize 802.1x Session And Test Ping Between NSRs	test_internal_eap_server_ttls_chap
	...	test_supplicant_ttls_chap
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw

Test Unauthorize 802.1x Left With TTLS And CHAP On Internal Eap Server And Right With Supplicant TTLS And CHAP
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Unauthorize 802.1x Session And Test Ping Between NSRs
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw


Test Authorize 802.1x Left With TTLS And MSCHAP On Internal Eap Server And Right With Supplicant TTLS And MSCHAP
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Authorize 802.1x Session And Test Ping Between NSRs	test_internal_eap_server_ttls_mschap
	...	test_supplicant_ttls_mschap
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw

Test Unauthorize 802.1x Left With TTLS And MSCHAP On Internal Eap Server And Right With Supplicant TTLS And MSCHAP
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Unauthorize 802.1x Session And Test Ping Between NSRs
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw


Test Authorize 802.1x Left With TTLS And MSCHAPV2 On Internal Eap Server And Right With Supplicant TTLS And MSCHAPV2
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Authorize 802.1x Session And Test Ping Between NSRs	test_internal_eap_server_ttls_mschapv2
	...	test_supplicant_ttls_mschapv2
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw

Test Unauthorize 802.1x Left With TTLS And MSCHAPV2 On Internal Eap Server And Right With Supplicant TTLS And MSCHAPV2
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Unauthorize 802.1x Session And Test Ping Between NSRs
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw


Test Authorize 802.1x Left With All Credentials On Internal Eap Server And Right With Supplicant MD5
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Authorize 802.1x Session And Test Ping Between NSRs	test_internal_eap_server_all	test_supplicant_md5
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw

Test Unauthorize 802.1x Left With All Credentials On Internal Eap Server And Right With Supplicant MD5
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Unauthorize 802.1x Session And Test Ping Between NSRs
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw


Test Authorize 802.1x Left With Radius Authentication Server And Right With Supplicant MD5
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Authorize 802.1x Session And Test Ping Between NSRs	test_radius_authentication_server
	...	test_supplicant_md5
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw

Test Unauthorize 802.1x Left With Radius Authentication Server And Right With Supplicant MD5
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Unauthorize 802.1x Session And Test Ping Between NSRs
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw


Test Authorize 802.1x Left With Radius Authentication Server And Right With Supplicant PEAP And MD5
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Authorize 802.1x Session And Test Ping Between NSRs	test_radius_authentication_server
	...	test_supplicant_peap_md5
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw

Test Unauthorize 802.1x Left With Radius Authentication Server And Right With Supplicant PEAP And MD5
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Unauthorize 802.1x Session And Test Ping Between NSRs
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw


Test Authorize 802.1x Left With Radius Authentication Server And Right With Supplicant PEAP And MSCHAPV2
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Authorize 802.1x Session And Test Ping Between NSRs	test_radius_authentication_server
	...	test_supplicant_peap_mschapv2
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw

Test Unauthorize 802.1x Left With Radius Authentication Server And Right With Supplicant PEAP And MSCHAPV2
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Unauthorize 802.1x Session And Test Ping Between NSRs
	[Teardown]	Run Keywords	SUITE:Skip If 802.1x Not Available	AND	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	Skip If	${STD802_1x_AVAILABLE} == ${FALSE}	Tests not configured/available for this testing build
	Set Suite Variable	${STD802_1x_HOST_INTERFACE}	${SWITCH_NETPORT_HOST_INTERFACE1}
	Set Suite Variable	${STD802_1x_SHARED_INTERFACE}	${SWITCH_NETPORT_SHARED_INTERFACE1}
	CLI:Open	session_alias=HOST
	${HOST_IS_NSR}=	CLI:Is Net SR
	Set Suite Variable	${HOST_IS_NSR}
	CLI:Open	session_alias=PEER	HOST_DIFF=${HOSTSHARED}
	${PEER_IS_NSR}=	CLI:Is Net SR
	Set Suite Variable	${PEER_IS_NSR}
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR

	SUITE:Delete Configurations If Exists To 802.1x Network
	SUITE:Add Configurations To 802.1x Network

SUITE:Teardown
	Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	Run Keyword If	'${HOST_IS_NSR}' == 'False' or '${PEER_IS_NSR}' == 'False'	CLI:Close Connection
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR
	SUITE:Delete Configurations If Exists To 802.1x Network
	CLI:Close Connection

SUITE:Skip If 802.1x Not Available
	Skip If	not ${STD802_1x_AVAILABLE}	Tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	802.1x only in NSR
	Skip If	not ${PEER_IS_NSR}	802.1x only in NSR

SUITE:Delete Configurations If Exists To 802.1x Network
	CLI:Switch Connection	HOST
	CLI:Cancel	Raw
	SUITE:Disable 802.1x On Switch Interfaces	${STD802_1x_HOST_INTERFACE}
	SUITE:Delete 802.1x Credentials
	SUITE:Delete 802.1x Profiles
	CLI:Delete Network Connections	${VLAN_NAME}
	CLI:Delete VLAN	${VLAN_ID}
	CLI:Switch Connection	PEER
	CLI:Cancel	Raw
	SUITE:Disable 802.1x On Switch Interfaces	${STD802_1x_SHARED_INTERFACE}
	SUITE:Delete 802.1x Credentials
	SUITE:Delete 802.1x Profiles
	CLI:Delete Network Connections	${VLAN_NAME}
	CLI:Delete VLAN	${VLAN_ID}
	SUITE:Disable Interfaces On Switch Interfaces
	[Teardown]	CLI:Cancel	Raw

SUITE:Add Configurations To 802.1x Network
	SUITE:Enable Interfaces On Switch Interfaces
	CLI:Switch Connection	HOST
	CLI:Add VLAN	${VLAN_ID}	backplane0,${STD802_1x_HOST_INTERFACE}
	CLI:Add Static IPv4 Network Connection	${VLAN_NAME}	${CONNECTION_IPV4_LEFT}	vlan	backplane0	24	${VLAN_ID}
	SUITE:Add 802.1x Credentials
	SUITE:Add 802.1x Profiles
	CLI:Switch Connection	PEER
	CLI:Add VLAN	${VLAN_ID}	backplane0,${STD802_1x_SHARED_INTERFACE}
	CLI:Add Static IPv4 Network Connection	${VLAN_NAME}	${CONNECTION_IPV4_RIGHT}	vlan	backplane0	24	${VLAN_ID}
	SUITE:Add 802.1x Credentials
	SUITE:Add 802.1x Profiles

SUITE:Add 802.1x Credentials
	CLI:Enter Path	/settings/802.1x/credentials/
	FOR	${INDEX}	${SIMPLE_CRED}	IN ENUMERATE	@{SIMPLE_CREDS}
		CLI:Add
		CLI:Set	username=${SIMPLE_CRED} password=${RADIUSSERVER2_USERS_PASSWORD} confirm_password=${RADIUSSERVER2_USERS_PASSWORD}
		CLI:Set	authentication_method=${CRED_AUTH_METHOD}[${INDEX}]
		CLI:Commit
	END
	FOR	${INDEX}	${PEAP_CRED}	IN ENUMERATE	@{PEAP_CREDS}
		CLI:Add
		CLI:Set	username=${PEAP_CRED} password=${RADIUSSERVER2_USERS_PASSWORD} confirm_password=${RADIUSSERVER2_USERS_PASSWORD}
		CLI:Set	authentication_method=${CRED_AUTH_METHOD}[2]
		CLI:Set	anonymous_identity=${CRED_ANONYMOUS}[${INDEX}]
		Run Keyword if	'${INDEX}' == '0'	CLI:Set	inner_authentication=${CRED_INNER_AUTH}[0]
		Run Keyword if	'${INDEX}' == '1'	CLI:Set	inner_authentication=${CRED_INNER_AUTH}[4]
		CLI:Commit
	END
	FOR	${INDEX}	${TTLS_CRED}	IN ENUMERATE	@{TTLS_CREDS}
		CLI:Add
		CLI:Set	username=${TTLS_CRED} password=${RADIUSSERVER2_USERS_PASSWORD} confirm_password=${RADIUSSERVER2_USERS_PASSWORD}
		CLI:Set	authentication_method=${CRED_AUTH_METHOD}[3]
		CLI:Set	inner_authentication=${CRED_INNER_AUTH}[${INDEX}]
		${INDEX2}=	Evaluate	${INDEX} + 2
		CLI:Set	anonymous_identity=${CRED_ANONYMOUS}[${INDEX2}]
		CLI:Commit
	END
	CLI:Test Show Command	@{SIMPLE_CREDS}	@{PEAP_CREDS}	@{TTLS_CREDS}
	[Teardown]	CLI:Cancel	Raw

SUITE:Delete 802.1x Credentials
	@{CRED_NAMES}=	Create List	@{SIMPLE_CREDS}	@{PEAP_CREDS}	@{TTLS_CREDS}
	FOR	${CRED_NAME}	IN	@{CRED_NAMES}
		CLI:Enter Path	/settings/802.1x/credentials/
		CLI:Delete If Exists	${CRED_NAME}
		CLI:Commit
		CLI:Cancel	Raw
	END
	[Teardown]	CLI:Cancel	Raw

SUITE:Add 802.1x Profiles
	@{CRED_NAMES}=	Create List	@{SIMPLE_CREDS}	@{PEAP_CREDS}	@{TTLS_CREDS}
	${SELECT_USERS}=	Catenate	SEPARATOR=,	@{CRED_NAMES}
	CLI:Enter Path	/settings/802.1x/profiles/
	FOR	${INDEX}	${PROF_INTERNAL_EAP_SERVER}	IN ENUMERATE	@{PROF_INTERNAL_EAP_SERVERS}
		CLI:Add
		CLI:Set	name=${PROF_INTERNAL_EAP_SERVER} type=${PROF_TYPES}[0] retransmit_interval=${RETRANSMIT_INTERVAL}
		Run Keyword if	'${INDEX}' == '0'	CLI:Set	select_users=${SELECT_USERS}
		${INDEX-1}=	Evaluate	${INDEX} - 1
		Run Keyword if	'${INDEX}' != '0'	CLI:Set	select_users=${CRED_NAMES}[${INDEX-1}]
		CLI:Commit
	END

	CLI:Add
	CLI:Set	name=${PROF_RADIUS_SERVER} type=${PROF_TYPES}[1]
	CLI:Set	ip_address=${RADIUSSERVER2}
	CLI:Set	port_number=${RADIUSSERVER2_PORT}
	CLI:Set	shared_secret=${RADIUSSERVER2_SECRET} retransmit_interval=${RETRANSMIT_INTERVAL}
	CLI:Commit

	FOR	${INDEX}	${PROF_SUPPLICANT}	IN ENUMERATE	@{PROF_SUPPLICANTS}
		CLI:Add
		CLI:Set	name=${PROF_SUPPLICANT} type=${PROF_TYPES}[2] user=${CRED_NAMES}[${INDEX}]
		CLI:Commit
	END
	CLI:Test Show Command	${PROF_INTERNAL_EAP_SERVER}	${PROF_RADIUS_SERVER}	@{PROF_SUPPLICANTS}
	[Teardown]	CLI:Cancel	Raw

SUITE:Delete 802.1x Profiles
	@{PROF_NAMES}=	Create List	@{PROF_INTERNAL_EAP_SERVERS}	${PROF_RADIUS_SERVER}	@{PROF_SUPPLICANTS}
	FOR	${PROF_NAME}	IN	@{PROF_NAMES}
		CLI:Enter Path	/settings/802.1x/profiles/
		CLI:Delete If Exists	${PROF_NAME}
		CLI:Commit
		CLI:Cancel	Raw
	END
	[Teardown]	CLI:Cancel	Raw

SUITE:Enable Interfaces On Switch Interfaces
	CLI:Switch Connection	HOST
	CLI:Enter Path	/settings/switch_interfaces/${STD802_1x_HOST_INTERFACE}
	CLI:Set	status=enabled
	CLI:Commit
	CLI:Switch Connection	PEER
	CLI:Enter Path	/settings/switch_interfaces/${STD802_1x_SHARED_INTERFACE}
	CLI:Set	status=enabled
	CLI:Commit
	[Teardown]	CLI:Cancel	Raw

SUITE:Disable Interfaces On Switch Interfaces
	CLI:Switch Connection	HOST
	CLI:Enter Path	/settings/switch_interfaces/${STD802_1x_HOST_INTERFACE}
	CLI:Set	status=disabled
	CLI:Commit
	CLI:Switch Connection	PEER
	CLI:Enter Path	/settings/switch_interfaces/${STD802_1x_SHARED_INTERFACE}
	CLI:Set	status=disabled
	CLI:Commit
	[Teardown]	CLI:Cancel	Raw

SUITE:Enable 802.1x On Switch Interfaces
	[Arguments]	${INTERFACE}	${PROF_NAME}
	CLI:Enter Path	/settings/switch_interfaces/${INTERFACE}
	CLI:Set	status=enabled enable_802.1x=yes 802.1x_profile=${PROF_NAME}
	CLI:Commit
	[Teardown]		CLI:Cancel	Raw

SUITE:Disable 802.1x On Switch Interfaces
	[Arguments]	${INTERFACE}
	CLI:Enter Path	/settings/switch_interfaces/${INTERFACE}
	CLI:Set	status=disabled enable_802.1x=no
	CLI:Commit
	[Teardown]		CLI:Cancel	Raw

SUITE:Wait Until 802.1x State Authorized On Swith Statistics
	[Arguments]	${INTERFACE}
	Wait Until Keyword Succeeds	1m	5s	SUITE:Check 802.1x State Authorized On Swith Statistics	${INTERFACE}
	[Teardown]		CLI:Cancel	Raw

SUITE:Check 802.1x State Authorized On Swith Statistics
	[Arguments]		${INTERFACE}
	CLI:Enter Path	/settings/switch_interfaces/
	${OUTPUT}=	CLI:Show
	Log To Console	${OUTPUT}
	CLI:Enter Path	/system/switch_statistics/
	${OUTPUT}=	CLI:Show
	Should Match Regexp	${OUTPUT}	${INTERFACE}\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+Authorized
	[Teardown]		CLI:Cancel	Raw

SUITE:Unauthorize 802.1x Session On Swith Statistics
	[Arguments]	${INTERFACE}
	CLI:Enter Path	/system/switch_statistics/
	CLI:Write	unauthorize_802.1x_session ${INTERFACE}
	CLI:Commit
	[Teardown]		CLI:Cancel	Raw

SUITE:Wait Until 802.1x State Unauthorized On Swith Statistics
	[Arguments]	${INTERFACE}
	Wait Until Keyword Succeeds	1m	5s	SUITE:Check 802.1x State Unauthorized On Swith Statistics	${INTERFACE}
	[Teardown]		CLI:Cancel	Raw

SUITE:Check 802.1x State Unauthorized On Swith Statistics
	[Arguments]		${INTERFACE}
	CLI:Enter Path	/settings/switch_interfaces/
	${OUTPUT}=	CLI:Show
	Log To Console	${OUTPUT}
	CLI:Enter Path	/system/switch_statistics/
	${OUTPUT}=	CLI:Show
	Should Match Regexp	${OUTPUT}	${INTERFACE}\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+Unauthorized
	[Teardown]		CLI:Cancel	Raw

SUITE:Authorize 802.1x Session And Test Ping Between NSRs
	[Arguments]	${LEFT_PROF_NAME}	${RIGHT_PROF_NAME}
	SUITE:Disable Interfaces On Switch Interfaces
	CLI:Switch Connection	HOST
	SUITE:Enable 802.1x On Switch Interfaces	${STD802_1x_HOST_INTERFACE}	${LEFT_PROF_NAME}
	CLI:Switch Connection	PEER
	SUITE:Enable 802.1x On Switch Interfaces	${STD802_1x_SHARED_INTERFACE}	${RIGHT_PROF_NAME}
	CLI:Switch Connection	HOST
	SUITE:Wait Until 802.1x State Authorized On Swith Statistics	${STD802_1x_HOST_INTERFACE}
	${STATUS}=	Run Keyword And Return Status	Wait Until Keyword Succeeds	3x	10s	CLI:Test Ping	${CONNECTION_IPV4_RIGHT}
	Skip If	'${STATUS}' == 'False'	Ping Should Work
	CLI:Switch Connection	PEER
	SUITE:Wait Until 802.1x State Authorized On Swith Statistics	${STD802_1x_SHARED_INTERFACE}
	${STATUS}=	Run Keyword And Return Status	Wait Until Keyword Succeeds	3x	10s	CLI:Test Ping	${CONNECTION_IPV4_LEFT}
	Skip If	'${STATUS}' == 'False'	Ping Should Work

SUITE:Unauthorize 802.1x Session And Test Ping Between NSRs
	CLI:Switch Connection	HOST
	SUITE:Unauthorize 802.1x Session On Swith Statistics	${STD802_1x_HOST_INTERFACE}
	SUITE:Wait Until 802.1x State Unauthorized On Swith Statistics	${STD802_1x_SHARED_INTERFACE}
	${STATUS}=	Run Keyword And Return Status	CLI:Test Ping	${CONNECTION_IPV4_RIGHT}
	Run Keyword If	'${STATUS}' == 'True'	Ping Should not Work
	CLI:Switch Connection	PEER
	SUITE:Unauthorize 802.1x Session On Swith Statistics	${STD802_1x_SHARED_INTERFACE}
	SUITE:Wait Until 802.1x State Unauthorized On Swith Statistics	${STD802_1x_SHARED_INTERFACE}
	${STATUS}=	Run Keyword And Return Status	CLI:Test Ping	${CONNECTION_IPV4_LEFT}
	Run Keyword If	'${STATUS}' == 'True'	Ping Should not Work

SUITE: Generate TLS Certificates
	@{COUNTRY_CODES}	Create List	US	BR	GB	IN
	@{STATES}	Create List	CA	SC	IRE	KA
	@{LOCALITIES}	Create List	Fremont	Dublin	Blumenau	Bangalore
	@{ORGANIZATIONS}	Create List	US Corporate HQ	ZPE Europe	ZPE Brazil	ZPE India (APAC)
	@{EMAIL_ADDRESS}	Create List	testeUS@zpesystems.com	testeBR@zpesystems.com	testeGB@zpesystems.com
	...	testeIN@zpesystems.com
	CLI:Switch Connection	HOST
	CLI:Enter Path	/settings/802.1x/credentials/
	FOR	${INDEX}	${COUNTRY_CODE}	IN ENUMERATE	@{COUNTRY_CODES}
		CLI:Write	certificate ${SIMPLE_CREDS}[1]
		CLI:Set	country_code=${COUNTRY_CODE} state=${STATES}[${INDEX}] locality=${LOCALITIES}[${INDEX}]
		CLI:Set	organization="${ORGANIZATIONS}[${INDEX}]" email_address=${EMAIL_ADDRESS}[${INDEX}]
		CLI:Set	input_password=${RADIUSSERVER2_USERS_PASSWORD} output_password=${RADIUSSERVER2_USERS_PASSWORD}
		${OUTPUT}=	CLI:Write	generate_certificate
		CLI:Should Contain All	${OUTPUT}	${CERTIFICATE_INFOR}
	END
	[Teardown]	CLI:Cancel	Raw