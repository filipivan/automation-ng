*** Settings ***
Resource        ../../init.robot
Documentation   Tests Settings > Credentials validation file about 802.1x Feature through the CLI
Metadata        Version	1.0
Metadata        Executed At	${HOST}
Force Tags      CLI    EXCLUDEIN3_2    EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
@{CRED_AUTH_METHOD}     MD5   TLS    PEAP    TTLS
@{CRED_INNER_AUTH}      MD5   PAP    CHAP    MSCHAP    MSCHAPV2
@{CRED_NAMES}           test_cred_md5  test_cred_tls  test_cred_peap_md5  test_cred_peap_mschapv2  test_cred_ttls_md5
                        ...  test_cred_ttls_pap  test_cred_ttls_chap  test_cred_ttls_mschap  test_cred_ttls_mschapv2
@{CRED_ANONYMOUS}       test_cred_anonymous_0   test_cred_anonymous_1   test_cred_anonymous_2   test_cred_anonymous_3
                        ...  test_cred_anonymous_4  test_cred_anonymous_5  test_cred_anonymous_6
@{VALID_IPS}            192.168.15.60  192.168.16.85  192.168.17.55  0.0.0.0  255.255.255.255
@{INVALID_IPS}          20.20   300.300.300.300   -1.-1.-1.-1    0,5.0,5.0,5.0,5  0.00.00.00
@{VALID_PASSWORDS}      .NGAutom!2#     .ZPE5ystems!2020    .ZPE5ystems!2021    .ZPE5ystems!2020.ZPE5ystems!2021
@{VALID_COUNTRY_CODES}  US  BR  GB  IN
@{VALID_STATES}         CA  SC  IRE  KA
@{VALID_LOCALITIES}     Fremont  Dublin  Blumenau  Bangalore
@{VALID_ORGANIZATIONS}  US Corporate HQ  ZPE Europe  ZPE Brazil  ZPE India (APAC)
@{VALID_EMAIL_ADDRESS}  testeUS@zpesystems.com  testeBR@zpesystems.com  testeGB@zpesystems.com  testeIN@zpesystems.com
@{CERTIFICATE_INFOR}    client certificate:  Certificate:  Data:  Subject Public Key Info:  Signature Algorithm:
                        ...  BEGIN CERTIFICATE  END CERTIFICATE  BEGIN ENCRYPTED PRIVATE KEY  END ENCRYPTED PRIVATE KEY
@{ALL_VALUES}=          ${WORD}     ${ONE_WORD}     ${TWO_WORD}     ${THREE_WORD}
                        ...     ${SEVEN_WORD}   ${EIGHT_WORD}   ${ELEVEN_WORD}  ${NUMBER}   ${ONE_NUMBER}
                        ...     ${TWO_NUMBER}   ${THREE_NUMBER}     ${SEVEN_NUMBER}     ${EIGHT_NUMBER}
                        ...     ${ELEVEN_NUMBER}    ${WORD_AND_NUMBER}  ${NUMBER_AND_WORD}  ${POINTS}
                        ...     ${ONE_POINTS}   ${TWO_POINTS}   ${THREE_POINTS}	${SEVEN_POINTS}
                        ...     ${EIGHT_POINTS}     ${ELEVEN_POINTS}    ${WORD_AND_POINTS}  ${NUMBER_AND_POINTS}
                        ...     ${POINTS_AND_NUMBER}    ${POINTS_AND_WORD}

*** Test Cases ***
Test Ls Command On Settings Path
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    CLI:Enter Path          /settings/
    CLI:Test Ls Command     802.1x/

Test Ls Command On Feature 802.1x Path
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    CLI:Enter Path          /settings/802.1x/
    CLI:Test Ls Command     credentials/

Test Available Commands After Send tab-tab Under Credentials Path
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    CLI:Enter Path                  /settings/802.1x/credentials/
    CLI:Test Available Commands     acknowledge_alarm_state  delete  import_settings  show_settings  add
    ...  diagnostic_data  ls  shutdown  apply_settings  event_system_audit  pwd  software_upgrade  cd
    ...  event_system_clear  quit  system_certificate  certificate  exec  reboot  system_config_check  change_password
    ...  exit  revert  whoami  cloud_enrollment  export_settings  save_settings  commit  factory_settings  shell
    ...  create_csr  hostname  show

Test Show Table On Credentials Path And Valid Fields
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    CLI:Enter Path          /settings/802.1x/credentials/
    ${OUTPUT}=              CLI:Show
    Should Match Regexp     ${OUTPUT}    username\\s+authentication\\s+method
    CLI:Add
    CLI:Test Show Command   username =  password = ********
    ...     authentication_method = MD5
    [Teardown]              CLI:Cancel  Raw

Test Valid Values For Field=username
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    ${VALID_USERNAMES}=	Create List	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
    ...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
    ...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
    ...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}
    FOR    ${VALID_USERNAME}   IN  @{VALID_USERNAMES}
		SUITE:Add 802.1x Credentials Without Commit  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]
		...  ${CRED_INNER_AUTH}[0]
		CLI:Test Set Validate Normal Fields     username    ${VALID_USERNAME}
		CLI:Commit
		CLI:Delete If Exists                    ${VALID_USERNAME}
    END
    [Teardown]    Run Keywords  CLI:Cancel  Raw  AND    Skip If    ${IS_NSR} == ${FALSE}  System is not NSR
    ...   AND  CLI:Delete If Exists  ${VALID_USERNAME}  ${CRED_NAMES}[0]

Test Invalid Values For Field=username
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    ${INVALID_USERNAMES}=   Create List     ${POINTS}   ${ONE_POINTS}   ${TWO_POINTS}
    ...     ${THREE_POINTS}     ${SEVEN_POINTS}   ${WORD_AND_POINTS}    ${NUMBER_AND_POINTS}    ${POINTS_AND_NUMBER}
    ...     ${POINTS_AND_WORD}   ${EIGHT_POINTS}
    SUITE:Add 802.1x Credentials Without Commit  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]
    ...  ${CRED_INNER_AUTH}[0]
    CLI:Test Set Field Invalid Options      username    ${EMPTY}
    ...     Error: username: Field must not be empty.  save=yes
    CLI:Cancel  Raw
    FOR    ${INVALID_USERNAME}     IN   @{INVALID_USERNAMES}
		SUITE:Add 802.1x Credentials Without Commit  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]
		...  ${CRED_INNER_AUTH}[0]
		CLI:Test Set Field Invalid Options      username    ${INVALID_USERNAME}
		...     Error: username: This field is empty or it contains invalid characters.  save=yes
		CLI:Cancel  Raw
    END
    [Teardown]	Run Keywords  CLI:Cancel  Raw  AND    Skip If    ${IS_NSR} == ${FALSE}  System is not NSR
    ...   AND  CLI:Delete If Exists   ${INVALID_USERNAME}    ${EMPTY}    ${CRED_NAMES}[0]

Test Valid Values For Field=password
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    FOR    ${VALID_PASSWORD}   IN  @{VALID_PASSWORDS}
		SUITE:Add 802.1x Credentials Without Commit  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]
		...  ${CRED_INNER_AUTH}[0]
		CLI:Set      password=${VALID_PASSWORD}
		CLI:Commit
		CLI:Delete If Exists                    ${CRED_NAMES}[0]
    END
    [Teardown]    Run Keywords  Skip If    ${IS_NSR} == ${FALSE}  System is not NSR   AND   CLI:Cancel  Raw
    ...   AND   CLI:Delete If Exists   ${CRED_NAMES}[0]

Test Invalid Values For Field=password
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    ${INVALID_PASSWORDS}     Create List     ${WORD}    ${EIGHT_WORD}   ${ELEVEN_WORD}  ${NUMBER}
                        ...     ${EIGHT_NUMBER}  ${ELEVEN_NUMBER}    ${WORD_AND_NUMBER}  ${NUMBER_AND_WORD}  ${POINTS}
                        ...     ${EIGHT_POINTS}     ${ELEVEN_POINTS}    ${WORD_AND_POINTS}  ${NUMBER_AND_POINTS}
                        ...     ${POINTS_AND_NUMBER}    ${POINTS_AND_WORD}
    FOR    ${INVALID_PASSWORD}     IN   @{INVALID_PASSWORDS}
		SUITE:Add 802.1x Credentials Without Commit  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]
		...  ${CRED_INNER_AUTH}[0]
		CLI:Set                 password=${INVALID_PASSWORD}
		${SHOWOUTPUT}=          CLI:Write    save    Raw    Yes
		Should Match Regexp         ${SHOWOUTPUT}
		...    Error: password: Password minimum uppercase characters requirement: 1|Error: password: Password minimum uppercase characters requirement: 1.
		CLI:Cancel  Raw
		CLI:Delete If Exists                    ${CRED_NAMES}[0]
    END
    ${INVALID_PASSWORDS}     Create List     ${ONE_WORD}   ${TWO_WORD}   ${THREE_WORD}   ${SEVEN_WORD}   ${ONE_NUMBER}
                        ...     ${TWO_NUMBER}   ${THREE_NUMBER}     ${SEVEN_NUMBER}
                        ...     ${ONE_POINTS}   ${TWO_POINTS}   ${THREE_POINTS}	${SEVEN_POINTS}
    FOR    ${INVALID_PASSWORD}     IN   @{INVALID_PASSWORDS}
		SUITE:Add 802.1x Credentials Without Commit  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]
		...  ${CRED_INNER_AUTH}[0]
		CLI:Set                 password=${INVALID_PASSWORD}
		${SHOWOUTPUT}=          CLI:Write    save    Raw    Yes
		Should Be Equal         ${SHOWOUTPUT}
		...    Error: password: The password is too short. Minimum length: 8.
		CLI:Cancel  Raw
		CLI:Delete If Exists                    ${CRED_NAMES}[0]
    END
    [Teardown]	 Run Keywords  Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND   CLI:Cancel  Raw
    ...   AND   CLI:Delete If Exists   ${CRED_NAMES}[0]

Test Valid Values For Field=authentication_method
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    CLI:Enter Path          /settings/802.1x/credentials/
    CLI:Add
    CLI:Test Set Validate Normal Fields    authentication_method   @{CRED_AUTH_METHOD}
    [Teardown]   Run Keywords  Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND   CLI:Cancel  Raw

Test Invalid Values For Field=authentication_method
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    FOR    ${INVALID_AUTH_METHOD}     IN   @{ALL_VALUES}
		SUITE:Add 802.1x Credentials Without Commit  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]
		...  ${CRED_INNER_AUTH}[0]
		CLI:Test Set Field Invalid Options      authentication_method    ${INVALID_AUTH_METHOD}
		...     Error: Invalid value: ${INVALID_AUTH_METHOD} for parameter: authentication_method   save=no
		CLI:Cancel  Raw
    END
    SUITE:Add 802.1x Credentials Without Commit  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]
    ...  ${CRED_INNER_AUTH}[0]
    CLI:Test Set Field Invalid Options      authentication_method     ${EMPTY}
    ...     Error: Missing value for parameter: authentication_method   save=no
    CLI:Cancel  Raw
    [Teardown]	Run Keywords  Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND   CLI:Cancel  Raw
    ...   AND   CLI:Delete If Exists   ${CRED_NAMES}[0]

Test Valid Values For Field=anonymous_identity
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    ${VALID_IDENTITYS}=     Create List     @{ALL_VALUES}   ${EMPTY}   @{VALID_IPS}
    FOR    ${VALID_IDENTITY}   IN  @{VALID_IDENTITYS}
		SUITE:Add 802.1x Credentials Without Commit  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]
		...  ${CRED_INNER_AUTH}[0]
		CLI:Test Set Validate Normal Fields     anonymous_identity    ${VALID_IDENTITY}
		CLI:Commit
		CLI:Delete If Exists                    ${CRED_NAMES}[0]
    END
    [Teardown]    Run Keywords  Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND   CLI:Cancel  Raw
    ...   AND   CLI:Delete If Exists   ${CRED_NAMES}[0]

Test Invalid Values For Field=anonymous_identity
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    # anonymous_identity id can be anything
    [Teardown]    CLI:Cancel  Raw

Test Valid Values For Field=inner_authentication
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    CLI:Enter Path          /settings/802.1x/credentials/
    CLI:Add
    CLI:Test Set Validate Normal Fields    inner_authentication    @{CRED_INNER_AUTH}
    [Teardown]    Run Keywords  Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND   CLI:Cancel  Raw

Test Invalid Values For Field=inner_authentication
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    FOR    ${INVALID_AUTH_INNER}     IN   @{ALL_VALUES}
		SUITE:Add 802.1x Credentials Without Commit  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]
		...  ${CRED_INNER_AUTH}[0]
		CLI:Test Set Field Invalid Options      inner_authentication    ${INVALID_AUTH_INNER}
		...     Error: Invalid value: ${INVALID_AUTH_INNER} for parameter: inner_authentication   save=no
		CLI:Cancel  Raw
    END
    SUITE:Add 802.1x Credentials Without Commit  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]
    ...  ${CRED_INNER_AUTH}[0]
    CLI:Test Set Field Invalid Options      inner_authentication     ${EMPTY}
    ...     Error: Missing value for parameter: inner_authentication   save=no
    CLI:Cancel  Raw
    [Teardown]	Run Keywords  Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND   CLI:Cancel  Raw
    ...   AND   CLI:Delete If Exists   ${CRED_NAMES}[0]

Test Invalid Values For Field=inner_authentication When authentication_method=PEAP
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    FOR    ${CRED_INNER}     IN   @{CRED_INNER_AUTH}
		SUITE:Add 802.1x Credentials Without Commit  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[2]  ${CRED_ANONYMOUS}[0]
		...  ${CRED_INNER}
		Run Keyword If         '${CRED_INNER}' == '${CRED_INNER_AUTH}[0]'
		...   CLI:Test Set Field Invalid Options      inner_authentication    ${CRED_INNER}
		...   ${EMPTY}   save=yes
		Run Keyword If         '${CRED_INNER}' == '${CRED_INNER_AUTH}[4]'
		...   CLI:Test Set Field Invalid Options      inner_authentication    ${CRED_INNER}
		...   ${EMPTY}   save=yes
		Run Keyword If         '${CRED_INNER}' != '${CRED_INNER_AUTH}[0]'
		...   Run Keyword If         '${CRED_INNER}' != '${CRED_INNER_AUTH}[4]'
		...   CLI:Test Set Field Invalid Options      inner_authentication    ${CRED_INNER}
		...   Error: inner_authentication: PEAP only accepts MSCHAPV2 and MD5.   save=yes
		CLI:Cancel  Raw
		CLI:Delete If Exists   ${CRED_NAMES}[0]
    END
    [Teardown]	Run Keywords  Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND   CLI:Cancel  Raw
    ...   AND   CLI:Delete If Exists   ${CRED_NAMES}[0]

Test Available Commands After Send tab-tab Under Certificate Path
    Skip If            ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[0]
    CLI:Enter Path               /settings/802.1x/credentials/
    CLI:Write                    certificate ${CRED_NAMES}[1]
    CLI:Test Available Commands  cancel  generate_certificate  set  commit  ls  show
    [Teardown]	Run Keywords     Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND   CLI:Cancel  Raw
    ...   AND   CLI:Delete If Exists   ${CRED_NAMES}[1]

Test Show Table On Credentials Certificate And Valid Fields
    Skip If            ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[0]
    CLI:Enter Path               /settings/802.1x/credentials/
    CLI:Write                    certificate ${CRED_NAMES}[1]
    CLI:Test Show Command        country_code =  state =  locality =  organization =  common_name: test_cred_tls
    ...  email_address =  input_password =  output_password =
    [Teardown]	Run Keywords     Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND   CLI:Cancel  Raw
    ...   AND   CLI:Delete If Exists   ${CRED_NAMES}[1]

Test Valid Values For Field=country_code
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[0]
    CLI:Enter Path               /settings/802.1x/credentials/
    FOR    ${VALID_COUNTRY_CODE}   IN  @{VALID_COUNTRY_CODES}
		CLI:Write    certificate ${CRED_NAMES}[1]
		CLI:Set      output_password=1 state=SC locality=Blumenau organization="ZPE Brazil" email_address=t@t.com input_password=1
		CLI:Set      country_code=${VALID_COUNTRY_CODE}
		${OUTPUT}=   CLI:Write    generate_certificate
		CLI:Should Contain All    ${OUTPUT}  ${CERTIFICATE_INFOR}
    END
    [Teardown]	Run Keywords     Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND   CLI:Cancel  Raw
    ...   AND   CLI:Delete If Exists   ${CRED_NAMES}[1]

Test Invalid Values For Field=country_code
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    ${INVALID_NUMBERS}=   Create List     ${POINTS}   ${ONE_POINTS}   ${TWO_POINTS}
    ...     ${THREE_POINTS}     ${SEVEN_POINTS}   ${WORD_AND_POINTS}    ${NUMBER_AND_POINTS}    ${POINTS_AND_NUMBER}
    ...     ${POINTS_AND_WORD}   ${EIGHT_POINTS}
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[0]
    CLI:Enter Path               /settings/802.1x/credentials/
    FOR    ${INVALID_NUMBER}     IN   @{INVALID_NUMBERS}
		CLI:Write    certificate ${CRED_NAMES}[1]
		CLI:Set      country_code=BR state=SC locality=Blumenau organization="ZPE Brazil" email_address=t@t.com input_password=1
		CLI:Set      output_password=1
		CLI:Test Set Field Invalid Options     country_code    ${INVALID_NUMBER}
		...   Error: country_code: Illegal Characters. Use i.e. US   save=commit
		CLI:Cancel  Raw
	END
    CLI:Write    certificate ${CRED_NAMES}[1]
    CLI:Set      country_code=BR state=SC locality=Blumenau organization="ZPE Brazil" email_address=t@t.com input_password=1
    CLI:Set      output_password=1
    CLI:Test Set Field Invalid Options      country_code    ${EMPTY}
    ...         Error: country_code: Field must not be empty.  save=commit
    CLI:Cancel  Raw
    [Teardown]	Run Keywords     Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND   CLI:Cancel  Raw
    ...   AND   CLI:Delete If Exists   ${CRED_NAMES}[1]

Test Valid Values For Field=state
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[0]
    CLI:Enter Path               /settings/802.1x/credentials/
    FOR    ${VALID_STATE}   IN  @{VALID_STATES}
		CLI:Write    certificate ${CRED_NAMES}[1]
		CLI:Set      country_code=BR locality=Blumenau organization="ZPE Brazil" email_address=t@t.com input_password=1 output_password=1
		CLI:Set      state=${VALID_STATE}
		${OUTPUT}=   CLI:Write    generate_certificate
		CLI:Should Contain All    ${OUTPUT}  ${CERTIFICATE_INFOR}
    END
    [Teardown]	Run Keywords     Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND   CLI:Cancel  Raw
    ...   AND   CLI:Delete If Exists   ${CRED_NAMES}[1]

Test Invalid Values For Field=state
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    ${INVALID_STATES}=   Create List     ${POINTS}   ${ONE_POINTS}   ${TWO_POINTS}
    ...     ${THREE_POINTS}     ${SEVEN_POINTS}   ${WORD_AND_POINTS}    ${NUMBER_AND_POINTS}    ${POINTS_AND_NUMBER}
    ...     ${POINTS_AND_WORD}   ${EIGHT_POINTS}
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[0]
    CLI:Enter Path               /settings/802.1x/credentials/
    FOR    ${INVALID_STATE}     IN   @{INVALID_STATES}
       CLI:Write    certificate ${CRED_NAMES}[1]
       CLI:Set      country_code=BR state=SC locality=Blumenau organization="ZPE Brazil" email_address=t@t.com input_password=1
       CLI:Set      output_password=1
       CLI:Test Set Field Invalid Options     state    ${INVALID_STATE}
       ...   Error: state: Illegal Characters.  save=commit
       CLI:Cancel  Raw
    END

    CLI:Write    certificate ${CRED_NAMES}[1]
    CLI:Set      country_code=BR state=SC locality=Blumenau organization="ZPE Brazil" email_address=t@t.com input_password=1
    CLI:Set      output_password=1
    CLI:Test Set Field Invalid Options      state    ${EMPTY}
    ...          Error: state: Field must not be empty.  save=commit
    CLI:Cancel  Raw
    [Teardown]	Run Keywords     Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND   CLI:Cancel  Raw
    ...   AND   CLI:Delete If Exists   ${CRED_NAMES}[1]

Test Valid Values For Field=locality
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[0]
    CLI:Enter Path               /settings/802.1x/credentials/
    FOR    ${VALID_LOCALITY}   IN  @{VALID_LOCALITIES}
		CLI:Write    certificate ${CRED_NAMES}[1]
		CLI:Set      country_code=BR state=SC organization="ZPE Brazil" email_address=t@t.com input_password=1 output_password=1
		CLI:Set      locality="${VALID_LOCALITY}"
		${OUTPUT}=   CLI:Write    generate_certificate
		CLI:Should Contain All    ${OUTPUT}  ${CERTIFICATE_INFOR}
    END
    [Teardown]	Run Keywords     Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND   CLI:Cancel  Raw
    ...   AND   CLI:Delete If Exists   ${CRED_NAMES}[1]

Test Invalid Values For Field=locality
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    ${INVALID_LOCALITY}=   Create List     ${POINTS}   ${ONE_POINTS}   ${TWO_POINTS}
    ...     ${THREE_POINTS}     ${SEVEN_POINTS}   ${WORD_AND_POINTS}    ${NUMBER_AND_POINTS}    ${POINTS_AND_NUMBER}
    ...     ${POINTS_AND_WORD}   ${EIGHT_POINTS}
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[0]
    CLI:Enter Path               /settings/802.1x/credentials/
    FOR    ${INVALID_LOCALITY}   IN   @{INVALID_LOCALITY}
		CLI:Write    certificate ${CRED_NAMES}[1]
		CLI:Set      country_code=BR state=SC locality=Blumenau organization="ZPE Brazil" email_address=t@t.com input_password=1
		CLI:Set      output_password=1
		CLI:Test Set Field Invalid Options     locality    "${INVALID_LOCALITY}"
		...   Error: locality: Illegal Characters. Use i.e Los Angeles   save=commit
		CLI:Cancel  Raw
    END
    CLI:Write    certificate ${CRED_NAMES}[1]
    CLI:Set      country_code=BR state=SC locality=Blumenau organization="ZPE Brazil" email_address=t@t.com input_password=1
    CLI:Set      output_password=1
    CLI:Test Set Field Invalid Options      locality    ${EMPTY}
    ...          Error: locality: Field must not be empty.  save=commit
    CLI:Cancel  Raw
    [Teardown]	Run Keywords     Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND   CLI:Cancel  Raw
    ...   AND   CLI:Delete If Exists   ${CRED_NAMES}[1]

Test Valid Values For Field=organization
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[0]
    CLI:Enter Path               /settings/802.1x/credentials/
    FOR    ${VALID_ORGANIZATION}   IN  @{VALID_ORGANIZATIONS}
		CLI:Write    certificate ${CRED_NAMES}[1]
		CLI:Set      country_code=BR state=SC locality=Blumenau email_address=t@t.com input_password=1 output_password=1
		CLI:Set      organization="${VALID_ORGANIZATION}"
		${OUTPUT}=   CLI:Write    generate_certificate
		CLI:Should Contain All    ${OUTPUT}  ${CERTIFICATE_INFOR}
    END
    [Teardown]	Run Keywords     Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND   CLI:Cancel  Raw
    ...   AND   CLI:Delete If Exists   ${CRED_NAMES}[1]

Test Invalid Values For Field=organization
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[0]
    CLI:Enter Path               /settings/802.1x/credentials/
    CLI:Write    certificate ${CRED_NAMES}[1]
    CLI:Set   country_code=BR state=SC locality=Blumenau organization="ZPE Brazil" email_address=t@t.com input_password=1 output_password=1
    CLI:Test Set Field Invalid Options      organization    ${EMPTY}
    ...          Error: organization: Field must not be empty.  save=commit
    CLI:Cancel  Raw
    [Teardown]	Run Keywords     Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND   CLI:Cancel  Raw
    ...   AND   CLI:Delete If Exists   ${CRED_NAMES}[1]

Test Valid Values For Field=email_address
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[0]
    CLI:Enter Path               /settings/802.1x/credentials/
    FOR    ${VALID_EMAIL}   IN  @{VALID_EMAIL_ADDRESS}
		CLI:Write    certificate ${CRED_NAMES}[1]
		CLI:Set      country_code=BR state=SC organization="ZPE Brazil" locality=Blumenau input_password=1 output_password=1
		CLI:Set      email_address="${VALID_EMAIL}"
		${OUTPUT}=   CLI:Write    generate_certificate
		CLI:Should Contain All    ${OUTPUT}  ${CERTIFICATE_INFOR}
    END
    [Teardown]	Run Keywords     Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND   CLI:Cancel  Raw
    ...   AND   CLI:Delete If Exists   ${CRED_NAMES}[1]

Test Invalid Values For Field=email_address
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    ${INVALID_EMAILS}=   Create List     ${POINTS}   ${ONE_POINTS}   ${TWO_POINTS}
    ...     ${THREE_POINTS}     ${SEVEN_POINTS}   ${WORD_AND_POINTS}    ${NUMBER_AND_POINTS}    ${POINTS_AND_NUMBER}
    ...     ${POINTS_AND_WORD}   ${EIGHT_POINTS}  @{INVALID_IPS}
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[0]
    CLI:Enter Path               /settings/802.1x/credentials/
    FOR    ${INVALID_EMAIL}   IN   @{INVALID_EMAILS}
		CLI:Write    certificate ${CRED_NAMES}[1]
		CLI:Set      country_code=BR state=SC organization="ZPE Brazil" locality=Blumenau input_password=1 output_password=1
		CLI:Set      output_password=1
		CLI:Test Set Field Invalid Options    email_address   "${INVALID_EMAIL}"
		...    Error: email_address: Invalid e-mail address   save=commit
		CLI:Cancel  Raw
    END
    CLI:Write    certificate ${CRED_NAMES}[1]
    CLI:Set      country_code=BR state=SC organization="ZPE Brazil" locality=Blumenau input_password=1 output_password=1
    CLI:Set      output_password=1
    CLI:Test Set Field Invalid Options      email_address    ${EMPTY}
    ...          Error: email_address: Field must not be empty.  save=commit
    CLI:Cancel  Raw
    [Teardown]	Run Keywords     Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND   CLI:Cancel  Raw
    ...   AND   CLI:Delete If Exists   ${CRED_NAMES}[1]

Test Valid Values For Field=input_password
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[0]
    CLI:Enter Path               /settings/802.1x/credentials/
    FOR    ${VALID_PASSWORD}   IN  @{VALID_PASSWORDS}
		CLI:Write    certificate ${CRED_NAMES}[1]
		CLI:Set      country_code=BR state=SC locality=Blumenau organization="ZPE Brazil" email_address=t@t.com input_password=1 output_password=1
		CLI:Set      input_password="${VALID_PASSWORD}"
		${OUTPUT}=   CLI:Write    generate_certificate
		CLI:Should Contain All    ${OUTPUT}  ${CERTIFICATE_INFOR}
    END
    [Teardown]	Run Keywords     Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND   CLI:Cancel  Raw
    ...   AND   CLI:Delete If Exists   ${CRED_NAMES}[1]

Test Invalid Values For Field=input_password
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    ${INVALID_PASSWORDS}=   Create List     ${POINTS}   ${ONE_POINTS}   ${TWO_POINTS}
    ...     ${THREE_POINTS}     ${SEVEN_POINTS}   ${WORD_AND_POINTS}    ${NUMBER_AND_POINTS}    ${POINTS_AND_NUMBER}
    ...     ${POINTS_AND_WORD}   ${EIGHT_POINTS}   @{INVALID_IPS}
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[0]
    CLI:Enter Path               /settings/802.1x/credentials/
    CLI:Write    certificate ${CRED_NAMES}[1]
    CLI:Set      country_code=BR state=SC organization="ZPE Brazil" locality=Blumenau email_address=t@t.com input_password=1 output_password=1
    CLI:Set      output_password=1
    CLI:Test Set Field Invalid Options      input_password    ${EMPTY}
    ...          Error: input_password: Field must not be empty.  save=commit
    [Teardown]	Run Keywords     Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND   CLI:Cancel  Raw
    ...   AND   CLI:Delete If Exists   ${CRED_NAMES}[1]

Test Valid Values For Field=output_password
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[0]
    CLI:Enter Path               /settings/802.1x/credentials/
    FOR    ${VALID_PASSWORD}   IN  @{VALID_PASSWORDS}
		CLI:Write    certificate ${CRED_NAMES}[1]
		CLI:Set      country_code=BR state=SC locality=Blumenau organization="ZPE Brazil" email_address=t@t.com input_password=1
		CLI:Set      output_password="${VALID_PASSWORD}"
		${OUTPUT}=   CLI:Write    generate_certificate
		CLI:Should Contain All    ${OUTPUT}  ${CERTIFICATE_INFOR}
    END
    [Teardown]	Run Keywords     Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND   CLI:Cancel  Raw
    ...   AND   CLI:Delete If Exists   ${CRED_NAMES}[1]

Test Invalid Values For Field=output_password
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    ${INVALID_PASSWORDS}=   Create List     ${POINTS}   ${ONE_POINTS}   ${TWO_POINTS}
    ...     ${THREE_POINTS}     ${SEVEN_POINTS}   ${WORD_AND_POINTS}    ${NUMBER_AND_POINTS}    ${POINTS_AND_NUMBER}
    ...     ${POINTS_AND_WORD}   ${EIGHT_POINTS}   @{INVALID_IPS}
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[0]
    CLI:Enter Path               /settings/802.1x/credentials/
    CLI:Write    certificate ${CRED_NAMES}[1]
    CLI:Set      country_code=BR state=SC organization="ZPE Brazil" locality=Blumenau email_address=t@t.com input_password=1 output_password=1
    CLI:Set      output_password=1
    CLI:Test Set Field Invalid Options      output_password    ${EMPTY}
    ...          Error: output_password: Field must not be empty.  save=commit
    [Teardown]	Run Keywords     Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND   CLI:Cancel  Raw
    ...   AND   CLI:Delete If Exists   ${CRED_NAMES}[1]

*** Keywords ***
SUITE:Setup
    CLI:Open
    SUITE:Check If Is NSR
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Delete Configurations If Exists To 802.1x Network

SUITE:Teardown
    Run Keyword If       ${IS_NSR} == ${FALSE}    CLI:Close Connection
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Delete Configurations If Exists To 802.1x Network
    CLI:Close Connection

SUITE:Check If Is NSR
    ${IS_NSR}=      CLI:Is Net SR
    Set Suite Variable   ${IS_NSR}
    [Teardown]   Skip If    ${IS_NSR} == ${FALSE}      System is not NSR

SUITE:Delete Configurations If Exists To 802.1x Network
    CLI:Cancel       Raw
    SUITE:Delete 802.1x Credentials
    [Teardown]       CLI:Cancel  Raw

SUITE:Add 802.1x Credential
    [Arguments]            ${CRED_NAME}   ${CRED_AUTH_METHOD}   ${CRED_ANONYMOUS}=${EMPTY}  ${CRED_INNER_AUTH}=${EMPTY}
    CLI:Enter Path         /settings/802.1x/credentials/
    CLI:Add
    CLI:Set                username=${CRED_NAME} password=${QA_PASSWORD}
    CLI:Set                authentication_method=${CRED_AUTH_METHOD}
    Run Keyword If         '${CRED_AUTH_METHOD}' == 'PEAP'
    ...  CLI:Set                anonymous_identity=${CRED_ANONYMOUS} inner_authentication=${CRED_INNER_AUTH}
    Run Keyword If         '${CRED_AUTH_METHOD}' == 'TTLS'
    ...  CLI:Set                anonymous_identity=${CRED_ANONYMOUS} inner_authentication=${CRED_INNER_AUTH}
    CLI:Commit
    CLI:Test Show Command  ${CRED_NAME}
    [Teardown]             CLI:Cancel  Raw

SUITE:Delete 802.1x Credentials
    FOR    ${CRED_NAME}     IN   @{CRED_NAMES}
		CLI:Enter Path          /settings/802.1x/credentials/
		CLI:Delete If Exists    ${CRED_NAME}
		CLI:Commit
		CLI:Cancel  Raw
	END
    [Teardown]         CLI:Cancel  Raw

SUITE:Add 802.1x Credentials Without Commit
    [Arguments]             ${CRED_NAME}  ${CRED_AUTH_METHOD}  ${CRED_ANONYMOUS}  ${CRED_INNER_AUTH}
    CLI:Enter Path          /settings/802.1x/credentials/
    CLI:Add
    CLI:Set                 username=${CRED_NAME} password=${QA_PASSWORD}
    CLI:Set                 authentication_method=${CRED_AUTH_METHOD} anonymous_identity=${CRED_ANONYMOUS}
    CLI:Set                 inner_authentication=${CRED_INNER_AUTH}