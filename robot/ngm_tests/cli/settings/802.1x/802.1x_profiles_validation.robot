*** Settings ***
Resource        ../../init.robot
Documentation   Tests Settings > Profiles validation file about 802.1x Feature through the CLI
Metadata        Version	1.0
Metadata        Executed At	${HOST}
Force Tags      CLI    EXCLUDEIN3_2    EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
@{CRED_AUTH_METHOD}     MD5   TLS    PEAP    TTLS
@{CRED_INNER_AUTH}      MD5   PAP    CHAP    MSCHAP    MSCHAPV2
@{CRED_NAMES}           test_cred_md5  test_cred_tls  test_cred_peap_md5  test_cred_peap_mschapv2  test_cred_ttls_md5
                        ...  test_cred_ttls_pap  test_cred_ttls_chap  test_cred_ttls_mschap  test_cred_ttls_mschapv2
@{CRED_ANONYMOUS}       test_cred_anonymous_0   test_cred_anonymous_1   test_cred_anonymous_2   test_cred_anonymous_3
                        ...  test_cred_anonymous_4  test_cred_anonymous_5  test_cred_anonymous_6
@{PROF_TYPES}           internal_eap_server   radius_server  supplicant
@{PROF_NAMES}           test_prof_internal_eap_server  test_prof_radius_server   test_prof_supplicant
${RETRANSMIT_INTERVAL}  15
@{VALID_IPS}            192.168.15.60  192.168.16.85  192.168.17.55  0.0.0.0  255.255.255.255
@{INVALID_IPS}          20.20  abcd   10.10.10   300.300.300.300   -1.-1.-1.-1    0,5.0,5.0,5.0,5  0.00.00.00
@{VALID_PASSWORDS}      .NGAutom!2#     .ZPE5ystems!2020    .ZPE5ystems!2021    .ZPE5ystems!2020.ZPE5ystems!2021
@{ALL_VALUES}=          ${WORD}     ${ONE_WORD}     ${TWO_WORD}     ${THREE_WORD}
                        ...     ${SEVEN_WORD}   ${EIGHT_WORD}   ${ELEVEN_WORD}  ${NUMBER}   ${ONE_NUMBER}
                        ...     ${TWO_NUMBER}   ${THREE_NUMBER}     ${SEVEN_NUMBER}
                        ...     ${ELEVEN_NUMBER}    ${WORD_AND_NUMBER}  ${NUMBER_AND_WORD}  ${POINTS}
                        ...     ${ONE_POINTS}   ${TWO_POINTS}   ${THREE_POINTS}	${SEVEN_POINTS}
                        ...     ${EIGHT_POINTS}     ${ELEVEN_POINTS}    ${WORD_AND_POINTS}  ${NUMBER_AND_POINTS}
                        ...     ${POINTS_AND_NUMBER}    ${POINTS_AND_WORD}

*** Test Cases ***
Test Ls Command On Feature 802.1x Path
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    CLI:Enter Path          /settings/802.1x/
    CLI:Test Ls Command     profiles/

Test Available Commands After Send tab-tab Under Profiles Path
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    CLI:Enter Path                  /settings/802.1x/profiles/
    CLI:Test Available Commands     acknowledge_alarm_state  delete  hostname  shell  add
    ...  diagnostic_data  import_settings  show  apply_settings  event_system_audit  ls  show_settings  cd
    ...  event_system_clear  pwd  shutdown  change_password  exec  quit  software_upgrade  cloud_enrollment
    ...  exit  reboot  system_certificate  commit  revert  system_config_check  create_csr  factory_settings
    ...  save_settings  whoami

Test Show Table On Profiles Path And Valid Fields
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    CLI:Enter Path          /settings/802.1x/profiles/
    ${OUTPUT}=              CLI:Show
    Should Match Regexp     ${OUTPUT}    name\\s+type
    CLI:Add
    CLI:Test Show Command   name =  type = internal_eap_server  select_users =  retransmit_interval = 3600
    CLI:Set                 type=${PROF_TYPES}[1]
    CLI:Test Show Command   name =  type = ${PROF_TYPES}[1]    ip_address =    port_number =   shared_secret = ********
    ...     retransmit_interval = 3600
    CLI:Set                 type=${PROF_TYPES}[2]
    CLI:Test Show Command   name =  type = ${PROF_TYPES}[2]    user =
    [Teardown]    Run Keywords  Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND   CLI:Cancel  Raw

Test Valid Values For Field=name
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    ${VALID_NAMES}=	Create List	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
    ...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]  ${CRED_INNER_AUTH}[0]
    FOR	    ${VALID_NAME}   IN  @{VALID_NAMES}
		SUITE:Add 802.1x Profiles Without Commit  ${PROF_NAMES}[0]  ${PROF_TYPES}[0]  ${RETRANSMIT_INTERVAL}
		...   ${CRED_NAMES}[0]  ${CRED_NAMES}[0]
		CLI:Test Set Validate Normal Fields     name    ${VALID_NAME}
		CLI:Commit
		CLI:Delete If Exists                   ${VALID_NAME}
	END
    [Teardown]    Run Keywords  Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND
    ...  SUITE:Delete Configurations If Exists To 802.1x Network  AND  CLI:Delete If Exists   ${VALID_NAME}

Test Invalid Values For Field=name
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]  ${CRED_INNER_AUTH}[0]
    SUITE:Add 802.1x Profiles Without Commit  ${PROF_NAMES}[0]  ${PROF_TYPES}[0]  ${RETRANSMIT_INTERVAL}
    ...   ${CRED_NAMES}[0]  ${CRED_NAMES}[0]
    CLI:Test Set Field Invalid Options      name    ${EMPTY}
    ...     Error: name: Field must not be empty.  save=yes
    CLI:Cancel  Raw
    [Teardown]    Run Keywords  Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND
    ...  SUITE:Delete Configurations If Exists To 802.1x Network

Test Valid Values For Field=type
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]  ${CRED_INNER_AUTH}[0]
    SUITE:Add 802.1x Profiles Without Commit  ${PROF_NAMES}[0]  ${PROF_TYPES}[0]  ${RETRANSMIT_INTERVAL}
    ...   ${CRED_NAMES}[0]  ${CRED_NAMES}[0]
    CLI:Test Set Validate Normal Fields    type    @{PROF_TYPES}
    [Teardown]    Run Keywords  Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND
    ...  SUITE:Delete Configurations If Exists To 802.1x Network

Test Invalid Values For Field=type
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]  ${CRED_INNER_AUTH}[0]
    FOR	    ${INVALID_TYPE}     IN   @{ALL_VALUES}
		SUITE:Add 802.1x Profiles Without Commit  ${PROF_NAMES}[0]  ${PROF_TYPES}[0]  ${RETRANSMIT_INTERVAL}
		...   ${CRED_NAMES}[0]  ${CRED_NAMES}[0]
		CLI:Test Set Field Invalid Options      type    ${INVALID_TYPE}
		...     Error: Invalid value: ${INVALID_TYPE} for parameter: type   save=no
		CLI:Cancel  Raw
	END
    SUITE:Add 802.1x Profiles Without Commit  ${PROF_NAMES}[0]  ${PROF_TYPES}[0]  ${RETRANSMIT_INTERVAL}
    ...   ${CRED_NAMES}[0]  ${CRED_NAMES}[0]
    CLI:Test Set Field Invalid Options      type     ${EMPTY}
    ...     Error: Missing value for parameter: type   save=no
    CLI:Cancel  Raw
    [Teardown]    Run Keywords  Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND
    ...  SUITE:Delete Configurations If Exists To 802.1x Network

Test Valid Values For Field=select_users
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]  ${CRED_INNER_AUTH}[0]
    SUITE:Add 802.1x Profiles Without Commit  ${PROF_NAMES}[0]  ${PROF_TYPES}[0]  ${RETRANSMIT_INTERVAL}
    ...   ${CRED_NAMES}[0]  ${CRED_NAMES}[0]
    CLI:Test Set Validate Normal Fields    select_users   ${CRED_NAMES}[0]
    [Teardown]    Run Keywords  Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND
    ...  SUITE:Delete Configurations If Exists To 802.1x Network

Test Invalid Values For Field=select_users
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]  ${CRED_INNER_AUTH}[0]
    FOR	    ${INVALID_USER}     IN   @{ALL_VALUES}
		SUITE:Add 802.1x Profiles Without Commit  ${PROF_NAMES}[0]  ${PROF_TYPES}[0]  ${RETRANSMIT_INTERVAL}
		...   ${CRED_NAMES}[0]  ${CRED_NAMES}[0]
		CLI:Test Set Field Invalid Options      select_users    ${INVALID_USER}
		...     Error: Invalid value: ${INVALID_USER} for parameter: select_users   save=no
		CLI:Cancel  Raw
	END
    SUITE:Add 802.1x Profiles Without Commit  ${PROF_NAMES}[0]  ${PROF_TYPES}[0]  ${RETRANSMIT_INTERVAL}
    ...   ${CRED_NAMES}[0]  ${CRED_NAMES}[0]
    CLI:Test Set Field Invalid Options      select_users     ${EMPTY}
    ...     Error: select_users: Field must not be empty.   save=yes
    CLI:Cancel  Raw
    [Teardown]    Run Keywords  Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND
    ...  SUITE:Delete Configurations If Exists To 802.1x Network

Test Valid Values For Field=retransmit_interval
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    ${VALID_INTERVALS}=	Create List	${NUMBER}   ${ONE_NUMBER}   ${TWO_NUMBER}   ${THREE_NUMBER}     ${SEVEN_NUMBER}
                        ...     ${EIGHT_NUMBER}
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]  ${CRED_INNER_AUTH}[0]
    FOR	    ${VALID_INTERVAL}   IN  @{VALID_INTERVALS}
		SUITE:Add 802.1x Profiles Without Commit  ${PROF_NAMES}[0]  ${PROF_TYPES}[0]  ${RETRANSMIT_INTERVAL}
		...   ${CRED_NAMES}[0]  ${CRED_NAMES}[0]
		CLI:Test Set Validate Normal Fields     retransmit_interval    ${VALID_INTERVAL}
		CLI:Commit
		CLI:Delete If Exists                    ${PROF_NAMES}[0]
	END
    [Teardown]    Run Keywords  Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND
    ...  SUITE:Delete Configurations If Exists To 802.1x Network

Test Invalid Values For Field=retransmit_interval
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    ${INVALID_INTERVALS}=   Create List     ${POINTS}   ${ONE_POINTS}   ${TWO_POINTS}
    ...     ${THREE_POINTS}     ${SEVEN_POINTS}   ${WORD_AND_POINTS}    ${NUMBER_AND_POINTS}    ${POINTS_AND_NUMBER}
    ...     ${POINTS_AND_WORD}   ${EIGHT_POINTS}    ${ELEVEN_NUMBER}
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]  ${CRED_INNER_AUTH}[0]
    SUITE:Add 802.1x Profiles Without Commit  ${PROF_NAMES}[0]  ${PROF_TYPES}[0]  ${RETRANSMIT_INTERVAL}
    ...   ${CRED_NAMES}[0]  ${CRED_NAMES}[0]
    CLI:Test Set Field Invalid Options      retransmit_interval    ${EMPTY}
    ...         Error: retransmit_interval: Validation error.  save=yes
    CLI:Delete If Exists                    ${PROF_NAMES}[0]
    CLI:Cancel  Raw
    FOR	    ${INVALID_INTERVAL}     IN   @{INVALID_INTERVALS}
		SUITE:Add 802.1x Profiles Without Commit  ${PROF_NAMES}[0]  ${PROF_TYPES}[0]  ${RETRANSMIT_INTERVAL}
		...   ${CRED_NAMES}[0]  ${CRED_NAMES}[0]
		CLI:Test Set Field Invalid Options     retransmit_interval    ${INVALID_INTERVAL}
		...    Error: retransmit_interval: Validation error.  save=yes
		CLI:Delete If Exists                    ${PROF_NAMES}[0]
		CLI:Cancel  Raw
	END
    [Teardown]    Run Keywords  Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND
    ...  SUITE:Delete Configurations If Exists To 802.1x Network

Test Valid Values For Field=ip_address
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    FOR	    ${VALID_IP}   IN  @{VALID_IPS}
		SUITE:Add 802.1x Profiles Without Commit  ${PROF_NAMES}[1]  ${PROF_TYPES}[1]  ${RETRANSMIT_INTERVAL}
		...   ${CRED_NAMES}[0]  ${CRED_NAMES}[0]
		CLI:Test Set Validate Normal Fields     ip_address    ${VALID_IP}
		CLI:Commit
		CLI:Delete If Exists                    ${PROF_NAMES}[1]
	END
    [Teardown]    Run Keywords  Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND
    ...  SUITE:Delete Configurations If Exists To 802.1x Network

Test Invalid Values For Field=ip_address
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Add 802.1x Profiles Without Commit  ${PROF_NAMES}[1]  ${PROF_TYPES}[1]  ${RETRANSMIT_INTERVAL}
    ...   ${CRED_NAMES}[0]  ${CRED_NAMES}[0]
    CLI:Test Set Field Invalid Options      ip_address    ${EMPTY}
    ...     Error: ip_address: Field must not be empty.   save=yes
    CLI:Delete If Exists                    ${PROF_NAMES}[1]
    CLI:Cancel  Raw
    FOR	    ${INVALID_IP}     IN   @{INVALID_IPS}
		SUITE:Add 802.1x Profiles Without Commit  ${PROF_NAMES}[1]  ${PROF_TYPES}[1]  ${RETRANSMIT_INTERVAL}
		...   ${CRED_NAMES}[0]  ${CRED_NAMES}[0]
		CLI:Test Set Field Invalid Options     ip_address    ${INVALID_IP}
		...    Error: ip_address: Invalid IP address.  save=yes
		CLI:Delete If Exists                    ${PROF_NAMES}[1]
		CLI:Cancel  Raw
	END
    [Teardown]    Run Keywords  Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND
    ...  SUITE:Delete Configurations If Exists To 802.1x Network

Test Valid Values For Field=port_number
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    ${VALID_NUMBERS}=    Create List     ${NUMBER}  ${ONE_NUMBER}  ${TWO_NUMBER}  ${THREE_NUMBER}   ${SEVEN_NUMBER}
    ...     ${EIGHT_NUMBER}    ${ELEVEN_NUMBER}   ${EMPTY}
    FOR	    ${VALID_NUMBER}   IN  @{VALID_NUMBERS}
		SUITE:Add 802.1x Profiles Without Commit  ${PROF_NAMES}[1]  ${PROF_TYPES}[1]  ${RETRANSMIT_INTERVAL}
		...   ${CRED_NAMES}[0]  ${CRED_NAMES}[0]
		CLI:Test Set Validate Normal Fields     port_number    ${VALID_NUMBER}
		CLI:Commit
		CLI:Delete If Exists                    ${PROF_NAMES}[1]
	END
    [Teardown]    Run Keywords  Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND
    ...  SUITE:Delete Configurations If Exists To 802.1x Network

Test Invalid Values For Field=port_number
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    ${INVALID_NUMBERS}=   Create List     ${POINTS}   ${ONE_POINTS}   ${TWO_POINTS}
    ...     ${THREE_POINTS}     ${SEVEN_POINTS}   ${WORD_AND_POINTS}    ${NUMBER_AND_POINTS}    ${POINTS_AND_NUMBER}
    ...     ${POINTS_AND_WORD}   ${EIGHT_POINTS}
    FOR	    ${INVALID_NUMBER}     IN   @{INVALID_NUMBERS}
		SUITE:Add 802.1x Profiles Without Commit  ${PROF_NAMES}[1]  ${PROF_TYPES}[1]  ${RETRANSMIT_INTERVAL}
		...   ${CRED_NAMES}[0]  ${CRED_NAMES}[0]
		CLI:Test Set Field Invalid Options     port_number    ${INVALID_NUMBER}
		...    Error: port_number: Validation error.   save=yes
		CLI:Delete If Exists                    ${PROF_NAMES}[1]
		CLI:Cancel  Raw
	END
    [Teardown]    Run Keywords  Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND
    ...  SUITE:Delete Configurations If Exists To 802.1x Network

Test Valid Values For Field=shared_secret
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    FOR	    ${VALID_SECRET}   IN  @{VALID_PASSWORDS}
		SUITE:Add 802.1x Profiles Without Commit  ${PROF_NAMES}[1]  ${PROF_TYPES}[1]  ${RETRANSMIT_INTERVAL}
		...   ${CRED_NAMES}[0]  ${CRED_NAMES}[0]
		CLI:Set      shared_secret=${VALID_SECRET}
		CLI:Commit
		CLI:Delete If Exists                    ${PROF_NAMES}[1]
	END
    [Teardown]    Run Keywords  Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND
    ...  SUITE:Delete Configurations If Exists To 802.1x Network

Test Invalid Values For Field=shared_secret
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    # shared_secret can be anything
    [Teardown]    CLI:Cancel  Raw

Test Valid Values For Field=user
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]  ${CRED_INNER_AUTH}[0]
    SUITE:Add 802.1x Profiles Without Commit  ${PROF_NAMES}[2]  ${PROF_TYPES}[2]  ${RETRANSMIT_INTERVAL}
    ...   ${CRED_NAMES}[0]  ${CRED_NAMES}[0]
    CLI:Test Set Validate Normal Fields     user    ${CRED_NAMES}[0]
    CLI:Commit
    [Teardown]    Run Keywords  Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND
    ...  SUITE:Delete Configurations If Exists To 802.1x Network

Test Invalid Values For Field=user
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]  ${CRED_ANONYMOUS}[0]  ${CRED_INNER_AUTH}[0]
    FOR	    ${INVALID_USER}     IN   @{ALL_VALUES}
		SUITE:Add 802.1x Profiles Without Commit  ${PROF_NAMES}[2]  ${PROF_TYPES}[2]  ${RETRANSMIT_INTERVAL}
		...   ${CRED_NAMES}[0]  ${CRED_NAMES}[0]
		CLI:Test Set Field Invalid Options      user    ${INVALID_USER}
		...     Error: Invalid value: ${INVALID_USER} for parameter: user   save=no
		CLI:Cancel  Raw
	END
    [Teardown]    Run Keywords  Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND
    ...  SUITE:Delete Configurations If Exists To 802.1x Network

*** Keywords ***
SUITE:Setup
    CLI:Open
    SUITE:Check If Is NSR
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Delete Configurations If Exists To 802.1x Network

SUITE:Teardown
    Run Keyword If       ${IS_NSR} == ${FALSE}    CLI:Close Connection
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Delete Configurations If Exists To 802.1x Network
    CLI:Close Connection

SUITE:Check If Is NSR
    ${IS_NSR}=      CLI:Is Net SR
    Set Suite Variable   ${IS_NSR}
    [Teardown]   Skip If    ${IS_NSR} == ${FALSE}      System is not NSR

SUITE:Delete Configurations If Exists To 802.1x Network
    CLI:Cancel                                  Raw
    SUITE:Delete 802.1x Credentials
    SUITE:Delete 802.1x Profiles
    [Teardown]       CLI:Cancel  Raw

SUITE:Add 802.1x Credential
    [Arguments]            ${CRED_NAME}   ${CRED_AUTH_METHOD}   ${CRED_ANONYMOUS}=${EMPTY}  ${CRED_INNER_AUTH}=${EMPTY}
    CLI:Enter Path         /settings/802.1x/credentials/
    CLI:Add
    CLI:Set                username=${CRED_NAME} password=${QA_PASSWORD}
    CLI:Set                authentication_method=${CRED_AUTH_METHOD}
    Run Keyword If         '${CRED_AUTH_METHOD}' == 'PEAP'
    ...  CLI:Set                anonymous_identity=${CRED_ANONYMOUS} inner_authentication=${CRED_INNER_AUTH}
    Run Keyword If         '${CRED_AUTH_METHOD}' == 'TTLS'
    ...  CLI:Set                anonymous_identity=${CRED_ANONYMOUS} inner_authentication=${CRED_INNER_AUTH}
    CLI:Commit
    CLI:Test Show Command  ${CRED_NAME}
    [Teardown]             CLI:Cancel  Raw

SUITE:Delete 802.1x Credentials
    FOR	    ${CRED_NAME}     IN   @{CRED_NAMES}
		CLI:Enter Path          /settings/802.1x/credentials/
		CLI:Delete If Exists    ${CRED_NAME}
		CLI:Commit
		CLI:Cancel  Raw
	END
    [Teardown]         CLI:Cancel  Raw

SUITE:Add 802.1x Profiles
    FOR	    ${PROF_TYPE}     IN   @{PROF_TYPES}
		CLI:Enter Path     /settings/802.1x/profiles/
		CLI:Add
		Run Keyword If     '${PROF_TYPE}' == 'internal_eap_server'  Run Keywords
		...  CLI:Set  name=${PROF_NAMES}[0] type=${PROF_TYPE} retransmit_interval=${RETRANSMIT_INTERVAL}  AND
		...  CLI:Set  select_users=${CRED_NAMES}[0]
		...  AND   CLI:Commit
		Run Keyword If     '${PROF_TYPE}' == 'radius_server'   Run Keywords
		...  CLI:Set  name=${PROF_NAMES}[1] type=${PROF_TYPE}  AND
		...  CLI:Set  ip_address=${RADIUSSERVER2} port_number=${RADIUSSERVER2_PORT}
		...  AND   CLI:Set   shared_secret=${RADIUSSERVER2_SECRET} retransmit_interval=${RETRANSMIT_INTERVAL}
		...  AND   CLI:Commit
		Run Keyword If     '${PROF_TYPE}' == 'supplicant'    Run Keywords
		...  CLI:Set  name=${PROF_NAMES}[2] type=${PROF_TYPE} user=${CRED_NAMES}[0]
		...  AND   CLI:Commit
		CLI:Cancel  Raw
	END
    [Teardown]         CLI:Cancel  Raw

SUITE:Delete 802.1x Profiles
    FOR	    ${PROF_NAME}     IN   @{PROF_NAMES}
		CLI:Enter Path          /settings/802.1x/profiles/
		CLI:Delete If Exists    ${PROF_NAME}
		CLI:Commit
		CLI:Cancel  Raw
	END
    [Teardown]         CLI:Cancel  Raw

SUITE:Add 802.1x Profiles Without Commit
    [Arguments]        ${PROF_NAME}  ${PROF_TYPE}  ${RETRANSMIT_INTERVAL}=${EMPTY}  ${SELECT_USERS}=${EMPTY}
                       ...   ${USER_SUPPLICANT}=${EMPTY}
    CLI:Enter Path     /settings/802.1x/profiles/
    CLI:Add
    CLI:Set            name=${PROF_NAME} type=${PROF_TYPE}
    Run Keyword If     '${PROF_TYPE}' == 'internal_eap_server'
    ...   CLI:Set  select_users=${SELECT_USERS} retransmit_interval=${RETRANSMIT_INTERVAL}
    Run Keyword If     '${PROF_TYPE}' == 'radius_server'   Run Keywords
    ...   CLI:Set  ip_address=${RADIUSSERVER2} port_number=${RADIUSSERVER2_PORT} shared_secret=${RADIUSSERVER2_SECRET}
    ...   AND   CLI:Set  retransmit_interval=${RETRANSMIT_INTERVAL}
    Run Keyword If     '${PROF_TYPE}' == 'supplicant'    CLI:Set    user=${USER_SUPPLICANT}