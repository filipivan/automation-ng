*** Settings ***
Resource        ../../init.robot
Documentation   Tests Settings > configuration file about 802.1x Feature through the CLI
Metadata        Version	1.0
Metadata        Executed At	${HOST}
Force Tags      CLI   EXCLUDEIN3_2    EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
@{CRED_AUTH_METHOD}     MD5   TLS    PEAP    TTLS
@{CRED_INNER_AUTH}      MD5   PAP    CHAP    MSCHAP    MSCHAPV2
@{CRED_NAMES}           test_cred_md5  test_cred_tls  test_cred_peap_md5  test_cred_peap_mschapv2  test_cred_ttls_md5
                        ...  test_cred_ttls_pap  test_cred_ttls_chap  test_cred_ttls_mschap  test_cred_ttls_mschapv2
@{CRED_ANONYMOUS}       test_cred_anonymous_0   test_cred_anonymous_1   test_cred_anonymous_2   test_cred_anonymous_3
                        ...  test_cred_anonymous_4  test_cred_anonymous_5  test_cred_anonymous_6
@{PROF_TYPES}           internal_eap_server   radius_server  supplicant
@{PROF_NAMES}           test_prof_internal_eap_server  test_prof_radius_server   test_prof_supplicant
${RETRANSMIT_INTERVAL}  15

*** Test Cases ***
Test Check If Status Is Enabled For Sfp0 Port Using EAP Server
    Skip If           ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Enable Interface And 802.1x On Switch Interfaces    sfp0   ${PROF_NAMES}[0]
    CLI:Enter Path              /settings/switch_interfaces/
    ${OUTPUT}=                  CLI:Show
    Run Keyword If  '${NGVERSION}' <= '5.2'  Should Match Regexp  ${OUTPUT}   sfp0\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+Enabled
    Run Keyword If  '${NGVERSION}' == '5.4'  Should Match Regexp  ${OUTPUT}   sfp0\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+Enabled
    Run Keyword If  '${NGVERSION}' >= '5.6'  Should Match Regexp  ${OUTPUT}   sfp0\\s+\\w+\\s+(This\\sis\\sa\\sAPI\\sAutomation\\sTest\\sDescription)*\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+Enabled
    [Teardown]     Run Keywords   Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND
    ...   SUITE:Delete Configurations If Exists To 802.1x Network

Test Check If Status Is Enabled For Net Port Using EAP Server
    Skip If           ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Add Configurations To 802.1x Network
    SUITE:Enable Interface And 802.1x On Switch Interfaces    ${NETS_NAME}   ${PROF_NAMES}[0]
    CLI:Enter Path              /settings/switch_interfaces/
    ${OUTPUT}=                  CLI:Show
    Run Keyword If  '${NGVERSION}' <= '5.2'  Should Match Regexp  ${OUTPUT}   ${NETS_NAME}\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+Enabled
    Run Keyword If  '${NGVERSION}' == '5.4'  Should Match Regexp  ${OUTPUT}   ${NETS_NAME}\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+Enabled
    Run Keyword If  '${NGVERSION}' >= '5.6'  Should Match Regexp  ${OUTPUT}   ${NETS_NAME}\\s+\\w+\\s+(This\\sis\\sa\\sAPI\\sAutomation\\sTest\\sDescription)*\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+Enabled
    [Teardown]     Run Keywords   Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND
    ...   SUITE:Delete Configurations If Exists To 802.1x Network

Test Check If Status Is Enabled For Sfp0 Port Using Radius Server
    Skip If           ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Add Configurations To 802.1x Network
    SUITE:Enable Interface And 802.1x On Switch Interfaces    sfp0   ${PROF_NAMES}[1]
    CLI:Enter Path              /settings/switch_interfaces/
    ${OUTPUT}=                  CLI:Show
    Run Keyword If  '${NGVERSION}' <= '5.2'  Should Match Regexp  ${OUTPUT}   sfp0\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+Enabled
    Run Keyword If  '${NGVERSION}' == '5.4'  Should Match Regexp  ${OUTPUT}   sfp0\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+Enabled
    Run Keyword If  '${NGVERSION}' >= '5.6'  Should Match Regexp  ${OUTPUT}   sfp0\\s+\\w+\\s+(This\\sis\\sa\\sAPI\\sAutomation\\sTest\\sDescription)*\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+Enabled
    [Teardown]     Run Keywords   Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND
    ...   SUITE:Delete Configurations If Exists To 802.1x Network

Test Check If Status Is Enabled For Net Port Using Radius Server
    Skip If           ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Add Configurations To 802.1x Network
    SUITE:Enable Interface And 802.1x On Switch Interfaces    ${NETS_NAME}   ${PROF_NAMES}[1]
    CLI:Enter Path              /settings/switch_interfaces/
    ${OUTPUT}=                  CLI:Show
    Run Keyword If  '${NGVERSION}' <= '5.2'  Should Match Regexp  ${OUTPUT}   ${NETS_NAME}\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+Enabled
    Run Keyword If  '${NGVERSION}' == '5.4'  Should Match Regexp  ${OUTPUT}   ${NETS_NAME}\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+Enabled
    Run Keyword If  '${NGVERSION}' >= '5.6'  Should Match Regexp  ${OUTPUT}   ${NETS_NAME}\\s+\\w+\\s+(This\\sis\\sa\\sAPI\\sAutomation\\sTest\\sDescription)*\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+Enabled
    [Teardown]     Run Keywords   Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND
    ...   SUITE:Delete Configurations If Exists To 802.1x Network

Test Check If Status Is Enabled For Sfp0 Port Using Supplicant Profile
    Skip If           ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Add Configurations To 802.1x Network
    SUITE:Enable Interface And 802.1x On Switch Interfaces    sfp0   ${PROF_NAMES}[2]
    CLI:Enter Path              /settings/switch_interfaces/
    ${OUTPUT}=                  CLI:Show
    Run Keyword If  '${NGVERSION}' <= '5.2'  Should Match Regexp  ${OUTPUT}   sfp0\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+Enabled
    Run Keyword If  '${NGVERSION}' == '5.4'  Should Match Regexp  ${OUTPUT}   sfp0\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+Enabled
    Run Keyword If  '${NGVERSION}' >= '5.6'  Should Match Regexp  ${OUTPUT}   sfp0\\s+\\w+\\s+(This\\sis\\sa\\sAPI\\sAutomation\\sTest\\sDescription)*\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+Enabled
    [Teardown]     Run Keywords   Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND
    ...   SUITE:Delete Configurations If Exists To 802.1x Network

Test Check If Status Is Enabled For Net Port Using Supplicant Profile
    Skip If           ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Add Configurations To 802.1x Network
    SUITE:Enable Interface And 802.1x On Switch Interfaces    ${NETS_NAME}   ${PROF_NAMES}[2]
    CLI:Enter Path              /settings/switch_interfaces/
    ${OUTPUT}=                  CLI:Show
    Run Keyword If  '${NGVERSION}' <= '5.2'  Should Match Regexp  ${OUTPUT}   ${NETS_NAME}\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+Enabled
    Run Keyword If  '${NGVERSION}' == '5.4'  Should Match Regexp  ${OUTPUT}   ${NETS_NAME}\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+Enabled
    Run Keyword If  '${NGVERSION}' >= '5.6'  Should Match Regexp  ${OUTPUT}   ${NETS_NAME}\\s+\\w+\\s+(This\\sis\\sa\\sAPI\\sAutomation\\sTest\\sDescription)*\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+\\w+\\s+Enabled
        [Teardown]     Run Keywords   Skip If   ${IS_NSR} == ${FALSE}   System is not NSR   AND
    ...   SUITE:Delete Configurations If Exists To 802.1x Network

*** Keywords ***
SUITE:Setup
    CLI:Open
    SUITE:Check If Is NSR
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Get Interface Net's Name
    SUITE:Delete Configurations If Exists To 802.1x Network
    SUITE:Add Configurations To 802.1x Network

SUITE:Teardown
    Run Keyword If       ${IS_NSR} == ${FALSE}    CLI:Close Connection
    Skip If    ${IS_NSR} == ${FALSE}    System is not NSR
    SUITE:Delete Configurations If Exists To 802.1x Network
    CLI:Close Connection

SUITE:Check If Is NSR
    ${IS_NSR}=      CLI:Is Net SR
    Set Suite Variable   ${IS_NSR}
    [Teardown]   Skip If    ${IS_NSR} == ${FALSE}      System is not NSR

SUITE:Delete Configurations If Exists To 802.1x Network
    CLI:Cancel                                  Raw
    SUITE:Disable 802.1x On Switch Interfaces    sfp0
    SUITE:Disable 802.1x On Switch Interfaces    ${NETS_NAME}
    SUITE:Delete 802.1x Credentials
    SUITE:Delete 802.1x Profiles
    [Teardown]       CLI:Cancel  Raw

SUITE:Add Configurations To 802.1x Network
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[0]  ${CRED_AUTH_METHOD}[0]
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[1]  ${CRED_AUTH_METHOD}[1]
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[2]  ${CRED_AUTH_METHOD}[2]  ${CRED_ANONYMOUS}[0]  ${CRED_INNER_AUTH}[0]
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[3]  ${CRED_AUTH_METHOD}[2]  ${CRED_ANONYMOUS}[1]  ${CRED_INNER_AUTH}[4]
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[4]  ${CRED_AUTH_METHOD}[3]  ${CRED_ANONYMOUS}[2]  ${CRED_INNER_AUTH}[0]
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[5]  ${CRED_AUTH_METHOD}[3]  ${CRED_ANONYMOUS}[3]  ${CRED_INNER_AUTH}[1]
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[6]  ${CRED_AUTH_METHOD}[3]  ${CRED_ANONYMOUS}[4]  ${CRED_INNER_AUTH}[2]
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[7]  ${CRED_AUTH_METHOD}[3]  ${CRED_ANONYMOUS}[5]  ${CRED_INNER_AUTH}[3]
    SUITE:Add 802.1x Credential  ${CRED_NAMES}[8]  ${CRED_AUTH_METHOD}[3]  ${CRED_ANONYMOUS}[6]  ${CRED_INNER_AUTH}[4]
    SUITE:Add 802.1x Profiles

SUITE:Add 802.1x Credential
	[Tags]	EXCLUDEIN_OFFICIAL
    [Arguments]            ${CRED_NAME}   ${CRED_AUTH_METHOD}   ${CRED_ANONYMOUS}=${EMPTY}  ${CRED_INNER_AUTH}=${EMPTY}
    CLI:Enter Path         /settings/802.1x/credentials/
    CLI:Add
    CLI:Set                username=${CRED_NAME} password=${QA_PASSWORD}
    CLI:Set                authentication_method=${CRED_AUTH_METHOD}
    CLI:Write	set confirm_password=${QA_PASSWORD}	raw	#workaround added because it was removed on nightly images, but is still not on official ones
    Run Keyword If         '${CRED_AUTH_METHOD}' == 'PEAP'
    ...  CLI:Set                anonymous_identity=${CRED_ANONYMOUS} inner_authentication=${CRED_INNER_AUTH}
    IF         '${CRED_AUTH_METHOD}' == 'TTLS'
    	CLI:Set                anonymous_identity=${CRED_ANONYMOUS} inner_authentication=${CRED_INNER_AUTH}
		IF	'${CRED_INNER_AUTH}' == 'MD5'
			${ERROR_MESSAGE}	CLI:Commit	Raw
			RUn Keyword If	'${NGVERSION}'>='5.2'	Should Contain	${ERROR_MESSAGE}	Error: inner_authentication: MD5 is not supported by TTLS
		ELSE
			CLI:Commit
		END
	END
	CLI:Commit	Raw
    CLI:Test Show Command  ${CRED_NAME}
    [Teardown]             CLI:Cancel  Raw

SUITE:Delete 802.1x Credentials
    FOR    ${CRED_NAME}     IN   @{CRED_NAMES}
       CLI:Enter Path          /settings/802.1x/credentials/
       CLI:Delete If Exists    ${CRED_NAME}
       CLI:Commit
       CLI:Cancel  Raw
    END
    [Teardown]         CLI:Cancel  Raw

SUITE:Add 802.1x Profiles
    ${SELECT_USERS}=         Catenate    SEPARATOR=,    @{CRED_NAMES}
    FOR    ${PROF_TYPE}     IN   @{PROF_TYPES}
       CLI:Enter Path     /settings/802.1x/profiles/
       CLI:Add
       Run Keyword If     '${PROF_TYPE}' == 'internal_eap_server'  Run Keywords
       ...  CLI:Set  name=${PROF_NAMES}[0] type=${PROF_TYPE} retransmit_interval=${RETRANSMIT_INTERVAL}  AND
       ...  CLI:Set  select_users=${CRED_NAMES}[0]
       ...  AND   CLI:Commit
       Run Keyword If     '${PROF_TYPE}' == 'radius_server'   Run Keywords
       ...  CLI:Set  name=${PROF_NAMES}[1] type=${PROF_TYPE}  AND
       ...  CLI:Set  ip_address=${RADIUSSERVER2} port_number=${RADIUSSERVER2_PORT}
       ...  AND   CLI:Set   shared_secret=${RADIUSSERVER2_SECRET} retransmit_interval=${RETRANSMIT_INTERVAL}
       ...  AND   CLI:Commit
       Run Keyword If     '${PROF_TYPE}' == 'supplicant'    Run Keywords
       ...  CLI:Set  name=${PROF_NAMES}[2] type=${PROF_TYPE} user=${CRED_NAMES}[0]
       ...  AND   CLI:Commit
       CLI:Cancel  Raw
    END
    [Teardown]         CLI:Cancel  Raw

SUITE:Delete 802.1x Profiles
    FOR    ${PROF_NAME}     IN   @{PROF_NAMES}
       CLI:Enter Path          /settings/802.1x/profiles/
       CLI:Delete If Exists    ${PROF_NAME}
       CLI:Commit
       CLI:Cancel  Raw
    END
    [Teardown]         CLI:Cancel  Raw

SUITE:Enable Interface And 802.1x On Switch Interfaces
    [Arguments]                 ${INTERFACE}    ${PROF_NAME}
    CLI:Enter Path              /settings/switch_interfaces/${INTERFACE}
    CLI:Set                     status=enabled enable_802.1x=yes 802.1x_profile=${PROF_NAME}
    CLI:Commit
    [Teardown]                  CLI:Cancel  Raw

SUITE:Disable 802.1x On Switch Interfaces
    [Arguments]                 ${INTERFACE}
    CLI:Enter Path              /settings/switch_interfaces/${INTERFACE}
    CLI:Set                     status=disabled enable_802.1x=no
    CLI:Commit
    [Teardown]                  CLI:Cancel  Raw

SUITE:Get Interface Net's Name
    CLI:Enter Path          /settings/switch_interfaces/
    ${OUTPUT}=              CLI:Ls
    ${INTERFACES}=          Remove String Using Regexp    ${OUTPUT}    ['/']
    @{GET_LINES}=           Split To Lines   ${INTERFACES}
    ${NETS_NAME}=           Get From List    ${GET_LINES}    4
    Set Suite Variable      ${NETS_NAME}