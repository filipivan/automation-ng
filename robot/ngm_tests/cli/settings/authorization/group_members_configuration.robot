*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authorization > group > Members ... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:OPEN
Suite Teardown	SUITE:TEARDOWN

*** Variables ***
${GROUP_NAME}	another_group

*** Test Cases ***
Test add members
	#Check if password complexity is off
	CLI:Enter Path	/settings/password_rules/
	${STATUS}=	Run Keyword and Return Status	CLI:Test Show Command	check_password_complexity = yes
	Run Keyword If	${STATUS}	Run Keywords	CLI:Set Field	check_password_complexity	no	AND	CLI:Commit

	CLI:Enter Path	/settings/local_accounts/
	CLI:Delete If Exists Confirm	group_user
	CLI:Add User	group_user	group
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/members
	CLI:Add
	CLI:Set Field	local_users	group_user
	CLI:Set Field	remote_users	remote1,remote2
	CLI:Commit
	CLI:Test Show Command	group_user	remote1	remote2
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/members/remote1
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Show Command	members: remote1
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Show Command	member: remote1
	CLI:Delete Users	group_user

Test delete members
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/members
	CLI:Delete	remote1
	CLI:Test Show Command	remote2
	CLI:Test Not Show Command	remote1
	CLI:Delete	remote2
	CLI:Test Not Show Command	remote2

Test adding duplicate remote_users
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/members
	CLI:Add
	CLI:Set Field	remote_users	remote_original
	CLI:Commit
	CLI:Test Show Command	remote_original
	CLI:Add
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	remote_users	remote_original	Error: remote_users: Entry already exists.	yes
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	remote_users	remote_original	Error: Entry already exists.	yes
	CLI:Cancel
	CLI:Delete	remote_original
	CLI:Test Not Show Command	remote_original
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	CLI:Delete If Exists	remote_original

Test adding duplicate user for local and remote user
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/members
	CLI:Add
	CLI:Set Field	local_users	${DEFAULT_USERNAME}
	CLI:Set Field	remote_users	${DEFAULT_USERNAME}
	CLI:Test Show Command	local_users = ${DEFAULT_USERNAME}	remote_users = ${DEFAULT_USERNAME}
	${OUTPUT}=	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Commit	Yes
	Run Keyword If	'${NGVERSION}' >= '4.0'	Should Contain	${OUTPUT}	Error: remote_users: Entry already exists.
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:OPEN
	CLI:Open
	CLI:Enter Path	/settings/authorization/
	@{GROUPS}=	CLI:Get All User Groups
	FOR	${GROUP}	IN	@{GROUPS}
		Run Keyword Unless	"${GROUP}" == "${DEFAULT_USERNAME}" or "${GROUP}" == "user"	CLI:Delete Confirm	${GROUP}
	END
	CLI:Enter Path	/settings/authorization/
	CLI:Add
	Write	set name=${GROUP_NAME}
	CLI:Read Until Prompt
	CLI:Commit
	CLI:Test Show Command	${GROUP_NAME}

SUITE:TEARDOWN
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/authorization/
	@{GROUPS}=	CLI:Get All User Groups
	FOR	${GROUP}	IN	@{GROUPS}
		Run Keyword Unless	"${GROUP}" == "${DEFAULT_USERNAME}" or "${GROUP}" == "user"	CLI:Delete Confirm	${GROUP}
	END
	CLI:Enter Path	/settings/local_accounts/
	CLI:Delete Users	grumpy
	CLI:Delete Users	slumpy
	CLI:Close Connection