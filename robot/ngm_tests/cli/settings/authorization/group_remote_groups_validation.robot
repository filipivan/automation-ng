*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authorization > group > Remote Groups ... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:OPEN
Suite Teardown	SUITE:TEARDOWN

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/authorization/another_group/remote_groups
	CLI:Test Available Commands	apply_settings	hostname	shell
	...	cd	ls	show	change_password	pwd	show_settings
	...	commit	quit	shutdown	event_system_audit	reboot	software_upgrade
	...	event_system_clear	revert	system_certificate	exit	save_settings	system_config_check
	...	factory_settings	set	whoami

Test show command for remote groups
	CLI:Enter Path	/settings/authorization/another_group/remote_groups
	CLI:Test Show Command	remote_groups

Test set command for remote groups
	CLI:Enter Path	/settings/authorization/another_group/remote_groups
	CLI:Test Set Available Fields	remote_groups

*** Keywords ***
SUITE:OPEN
	CLI:Open
	CLI:Enter Path	/settings/authorization/
	@{GROUPS}=	CLI:Get All User Groups
	FOR	${GROUP}	IN	@{GROUPS}
		Run Keyword Unless	"${GROUP}" == "${DEFAULT_USERNAME}" or "${GROUP}" == "user"	CLI:Delete Confirm	${GROUP}
	END
	CLI:Enter Path	/settings/authorization/
	CLI:Add
	Write	set name=another_group
	CLI:Read Until Prompt
	CLI:Commit
	CLI:Test Show Command	another_group

SUITE:TEARDOWN
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/authorization/
	@{GROUPS}=	CLI:Get All User Groups
	FOR	${GROUP}	IN	@{GROUPS}
		Run Keyword Unless	"${GROUP}" == "${DEFAULT_USERNAME}" or "${GROUP}" == "user"	CLI:Delete Confirm	${GROUP}
	END
	CLI:Enter Path	/settings/local_accounts/
	CLI:Delete Users	grumpy
	CLI:Delete Users	slumpy
	CLI:Close Connection
