*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authorization > group > Members ... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:OPEN
Suite Teardown	SUITE:TEARDOWN

*** Variables ***
${GROUP_NAME}	another_group

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/members
	CLI:Test Available Commands	add	factory_settings	shell	apply_settings	hostname	show
	...	cd	ls	show_settings	change_password	pwd	shutdown
	...	commit	quit	software_upgrade	delete	reboot	system_certificate
	...	event_system_audit	revert	system_config_check	event_system_clear	save_settings	whoami
	...	exit	set

Test show command for members
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/members
	CLI:Test Show Command	members

Test set command for members
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/members
	CLI:Add
	CLI:Test Set Available Fields	local_users	remote_users
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=local_users
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/members
	CLI:Add
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	local_users	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: local_users
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	local_users	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}
	CLI:Cancel

Test invalid values for field=remote_users
	Skip If	'${NGVERSION}' < '4.0'	Error message not implemented in 3.2
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/members
	CLI:Add
	CLI:Test Set Field Invalid Options	remote_users	${POINTS}	Error: remote_users: This field should contain only numbers, letters, ., -, and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	remote_users	${EXCEEDED}	Error: remote_users: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:OPEN
	CLI:Open
	CLI:Enter Path	/settings/authorization/
	@{GROUPS}=	CLI:Get All User Groups
	FOR	${GROUP}	IN	@{GROUPS}
		Run Keyword Unless	"${GROUP}" == "${DEFAULT_USERNAME}" or "${GROUP}" == "user"	CLI:Delete Confirm	${GROUP}
	END
	CLI:Enter Path	/settings/authorization/
	CLI:Add
	Write	set name=${GROUP_NAME}
	CLI:Read Until Prompt
	CLI:Commit
	CLI:Test Show Command	${GROUP_NAME}

SUITE:TEARDOWN
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/authorization/
	@{GROUPS}=	CLI:Get All User Groups
	FOR	${GROUP}	IN	@{GROUPS}
		Run Keyword Unless	"${GROUP}" == "${DEFAULT_USERNAME}" or "${GROUP}" == "user"	CLI:Delete Confirm	${GROUP}
	END
	CLI:Enter Path	/settings/local_accounts/
	CLI:Delete Users	grumpy
	CLI:Delete Users	slumpy
	CLI:Close Connection