*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authorization > group > Remote Groups ... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:OPEN
Suite Teardown	SUITE:TEARDOWN

*** Test Cases ***
Test valid values for remote groups
	CLI:Enter Path	/settings/authorization/another_group/remote_groups
	CLI:Set Field	remote_groups	remote_groups
	CLI:Commit
	CLI:Test Show Command	remote_groups = remote_groups

Test remove remote group
	CLI:Enter Path	/settings/authorization/another_group/remote_groups
	CLI:Set Field	remote_groups	${EMPTY}
	CLI:Commit
	CLI:Test Show Command	remote_groups =

*** Keywords ***
SUITE:OPEN
	CLI:Open
	CLI:Enter Path	/settings/authorization/
	@{GROUPS}=	CLI:Get All User Groups
	FOR	${GROUP}	IN	@{GROUPS}
		Run Keyword Unless	"${GROUP}" == "${DEFAULT_USERNAME}" or "${GROUP}" == "user"	CLI:Delete Confirm	${GROUP}
	END
	CLI:Enter Path	/settings/authorization/
	CLI:Add
	Write	set name=another_group
	CLI:Read Until Prompt
	CLI:Commit
	CLI:Test Show Command	another_group

SUITE:TEARDOWN
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/authorization/
	@{GROUPS}=	CLI:Get All User Groups
	FOR	${GROUP}	IN	@{GROUPS}
		Run Keyword Unless	"${GROUP}" == "${DEFAULT_USERNAME}" or "${GROUP}" == "user"	CLI:Delete Confirm	${GROUP}
	END
	CLI:Enter Path	/settings/local_accounts/
	CLI:Delete Users	grumpy
	CLI:Delete Users	slumpy
	CLI:Close Connection
