*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authorization > Group Name > Devices... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Open
Suite Teardown	SUITE:Teardown

*** Variables ***
${SESSION_ALIAS_SHELL}	shell_session
${SESSION_ALIAS_CLI}	cli_session
${SESSION_ALIAS_MENU}	menu_session
${DEVICE_NAME}	test
${DEVICE_TYPE}	device_console
${GROUP}	group_
${USERNAME_SHELL}	dummy
${PASS_SHELL}	dummy

*** Test Cases ***
Test startup with shell
	Skip If		${NGVERSION} == 3.2	Not implemented on 3.2
	SUITE:Set startup	shell
	CLI:Open	session_alias=${SESSION_ALIAS_SHELL}	TYPE=shell
	${LIST}=	Create List	backup	bin	boot	dev	etc	home	lib	lib64	media	mnt
	...	proc	run	sbin	software	sys	tmp	usr	var
	${OUTPUT}	CLI:Write	ls /
	FOR	${ELEMENT}	IN	@{LIST}
		Should Contain	${OUTPUT}	${ELEMENT}
	END
	[Teardown]	Run Keyword If	${NGVERSION} >= 4.2	SUITE:Set startup	cli	${TRUE}

Test startup with cli
	Skip If		${NGVERSION} == 3.2	Not implemented on 3.2 and 4.0
	SUITE:Set startup	shell
	CLI:Open	session_alias=${SESSION_ALIAS_SHELL}	TYPE=shell
	SUITE:Set startup	cli
	CLI:Open	session_alias=${SESSION_ALIAS_CLI}
	CLI:Test Ls Command	access/	system/	settings/
	[Teardown]	Run Keyword If	${NGVERSION} >= 4.2	SUITE:Set startup	cli	${TRUE}

Test menu-driven access to devices enable
	Skip If		${NGVERSION} == 3.2	Not implemented on 3.2 and 4.0
	CLI:Delete Devices	${DEVICE_NAME}
	CLI:Add Device	${DEVICE_NAME}	${DEVICE_TYPE}
	SUITE:Enable menu-driven access	yes
	CLI:Open	session_alias=${SESSION_ALIAS_MENU}	RAW_MODE=No	TYPE=menudriven
	CLI:Read Until Prompt
	${OUTPUT}=	CLI:Write	1
	Should Contain	${OUTPUT}	[Enter ^Ec? for help]
	[Teardown]	Run Keyword If	${NGVERSION} >= 4.2	SUITE:Enable menu-driven access	no	${TRUE}

Test menu-driven precedence in case of multiple groups
	Skip If		${NGVERSION} == 3.2	Not implemented on 3.2 and 4.0
	CLI:Delete Devices	${DEVICE_NAME}
	SUITE:Delete groups
	SUITE:Create groups
	CLI:Add User	${USERNAME_SHELL}	${PASS_SHELL}
	CLI:Enter Path	/settings/local_accounts/${USERNAME_SHELL}/
	CLI:Test Set Validate Invalid Options	user_group	group_1,group_2,group_3,
	CLI:Commit
	CLI:Add Device	${DEVICE_NAME}	${DEVICE_TYPE}
	CLI:Open	${USERNAME_SHELL}	${PASS_SHELL}	session_alias=${SESSION_ALIAS_MENU}	RAW_MODE=No	TYPE=menudriven
	CLI:Read Until Prompt
	${OUTPUT}=	CLI:Write	1
	Should Contain	${OUTPUT}	Enter an action:
	[Teardown]	Run Keyword If	${NGVERSION} >= 4.2	Run Keywords	SUITE:Enable menu-driven access	no	${TRUE}	AND	SUITE:Delete groups

*** Keywords ***
SUITE:Open
	CLI:Open

SUITE:Teardown
	CLI:Delete Devices	${DEVICE_NAME}
	CLI:Delete Users  ${USERNAME_SHELL}
#	Run Keyword If	${NGVERSION} >= 4.1	SUITE:Set startup	cli
	CLI:Close Connection

SUITE:Set startup
	[Arguments]	${TYPE}	${DEATH}=${FALSE}
	Run Keyword If	${DEATH}	CLI:Close Current Connection
	Run Keyword If	'${TYPE}' == 'cli'	CLI:Switch Connection	default
	CLI:Enter Path	/settings/authorization/
	@{GROUPS}=	CLI:Get All User Groups
	FOR		${GROUP}	IN	@{GROUPS}
		CLI:Enter Path	/settings/authorization/${GROUP}/profile
		CLI:Test Set Validate Normal Fields	startup_application	${TYPE}
		CLI:Commit
		CLI:Test Show Command	startup_application = ${TYPE}
	END

SUITE:Enable menu-driven access
	[Arguments]	${ENABLE}	${DEATH}=${FALSE}
	Run Keyword If	${DEATH}	CLI:Close Current Connection
	Run Keyword If	'${ENABLE}' == 'no'	CLI:Switch Connection	default
	CLI:Enter Path	/settings/authorization/
	@{GROUPS}=	CLI:Get All User Groups
	FOR		${GROUP}	IN	@{GROUPS}
		CLI:Enter Path	/settings/authorization/${GROUP}/profile
		CLI:Test Set Validate Normal Fields	menu-driven_access_to_devices	${ENABLE}
		CLI:Commit
		CLI:Test Show Command	menu-driven_access_to_devices = ${ENABLE}
	END

SUITE:Create groups
	FOR		${INDEX}	IN RANGE	1	4
		CLI:Enter Path	/settings/authorization/
		CLI:Add
		CLI:Test Set Field Invalid Options	name	${GROUP}${INDEX}
		CLI:Commit
		CLI:Enter Path	/settings/authorization/${GROUP}${INDEX}/profile/
		Run Keyword If	'${INDEX}' == '1'	CLI:Test Set Validate Normal Fields	menu-driven_access_to_devices	yes
		Run Keyword If	'${INDEX}' == '2'	CLI:Test Set Validate Normal Fields	startup_application	cli
		Run Keyword If	'${INDEX}' == '3'	CLI:Test Set Validate Normal Fields	startup_application	shell
		CLI:Commit
	END

SUITE:Delete groups
	CLI:Enter Path	/settings/authorization/
	@{GROUPS}=	CLI:Get All User Groups
	FOR	${GROUP}	IN	@{GROUPS}
		Run Keyword Unless	"${GROUP}" == "${DEFAULT_USERNAME}" or "${GROUP}" == "user"	CLI:Delete Confirm	${GROUP}
	END