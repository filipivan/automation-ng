*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authorization > Group Name > Devices > Other Devices... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Default Tags	CLI	SSH	SHOW	NON-CRITICAL	EXCLUDEIN3_2  EXCLUDEIN4_2 EXCLUDEIN5_0 EXCLUDEIN5_2	EXCLUDEIN5_4

Suite Setup	SUITE:SETUP
Suite Teardown	SUITE:TEARDOWN

*** Variables ***
${GROUP_NAME}	user

*** Test Cases ***
Test available commands after send tab-tab
	[Tags]	NG-9362	#NON-CRITICAL tag added because it's a new test
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/devices/OTHER_DEVICES
	CLI:Test Available Commands	acknowledge_alarm_state	apply_settings	cancel	cd	change_password	cloud_enrollment	commit	config_confirm
	...	config_revert	config_start	create_csr	diagnostic_data	event_system_audit	event_system_clear	exec	exit	export_settings
	...	factory_settings	hostname	import_settings	ls	pwd	quit	reboot	revert	save_settings	set	shell	show	show_settings
	...	shutdown	software_upgrade	system_certificate	system_config_check	whoami

Test visible fields for show command
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/devices/OTHER_DEVICES
	CLI:Test Show Command	mks = no	kvm = no	reset_device = no	sp_console = no	virtual_media = no	access_log_audit = no
	...	access_log_clear = no	event_log_audit = no	event_log_clear = no	sensors_data = no	monitoring = no	custom_commands = no
	...	targets: OTHER DEVICES	session = no_access	power = no_access	door = no_access

Test available set fields
	[Tags]	BUG_NG_9362	#NON-CRITICAL tag added because it's a new test
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/devices/
	CLI:Edit	OTHER_DEVICES
	CLI:Test Set Available Fields	access_log_audit	access_log_clear	custom_commands	door	event_log_audit	event_log_clear	kvm
	...	mks	monitoring	power	reset_device	sensors_data	session	sp_console	virtual_media
	CLI:Cancel	raw

*** Keywords ***
SUITE:SETUP
	CLI:Open

SUITE:TEARDOWN
	CLI:Close Connection