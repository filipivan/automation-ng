*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authorization > Group Name > Devices... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	NON-CRITICAL	BUG_NG_9704
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${GROUP_NAME}	another_group

*** Test Cases ***
Test functionality for device session=read-only
	CLI:Enter Path	/settings/services/
	CLI:Set Field	device_access_per_user_group_authorization	yes
	CLI:Commit
	CLI:Test Show Command	device_access_per_user_group_authorization = yes

	SUITE:Add User To Group
	CLI:Enter Path	/settings/devices/
	CLI:Delete If Exists Confirm	self_device
	CLI:Add Device	self_device	device_console	127.0.0.1	root	${ROOT_PASSWORD}

	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/devices
	CLI:Add
	CLI:Set Field	devices	self_device
	CLI:Set Field	session	read-only
	write bare	commit\n
	Run Keyword If	'${NGVERSION}' >= '4.2'	Read Until Regexp	\\(.*\\)\\s+:
	Run Keyword If	'${NGVERSION}' >= '4.2'	Write Bare	yes\n
	${OUTPUT}=	CLI:Read Until Prompt
	Should Not Contain	${OUTPUT}	Error

	Open Connection	${HOST}	alias=device_session
	Set Client Configuration	prompt=]
	Set Client Configuration	newline=\n
	Set Client Configuration	width=400
	Set Client Configuration	height=600
	Set Client Configuration	timeout=300
	${OUTPUT}=	Login	sneezy:self_device	sneezy
	Log to console	\nOpenen Session: ${OUTPUT}
	Set Client Configuration	prompt=--
	Write bare	\n\n
	${OUTPUT}=	CLI:Read Until Prompt
	Should Contain	${OUTPUT}	read-only
	Close Connection
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/services/
	CLI:Set Field	device_access_per_user_group_authorization	no
	CLI:Commit
	CLI:Delete Device	self_device
	CLI:Delete Users	sneezy
	[Teardown]	CLI:Close Connection

Test functionality for device session=read-write
	[Setup]	CLI:Open
	CLI:Enter Path	/settings/services/
	CLI:Set Field	device_access_per_user_group_authorization	yes
	CLI:Commit
	CLI:Test Show Command	device_access_per_user_group_authorization = yes

	SUITE:Add User To Group
	CLI:Enter Path	/settings/devices/
	CLI:Delete If Exists Confirm	self_device
	CLI:Add Device	self_device	device_console	127.0.0.1	root	${ROOT_PASSWORD}

	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/devices
	CLI:Add
	CLI:Set Field	devices	self_device
	CLI:Set Field	session	read-write
	write bare	commit\n
	Run Keyword If	'${NGVERSION}' >= '4.2'	Read Until Regexp	\\(.*\\)\\s+:
	Run Keyword If	'${NGVERSION}' >= '4.2'	Write	yes\n
	${OUTPUT}=	CLI:Read Until Prompt
	Should Not Contain	${OUTPUT}	Error

	Open Connection	${HOST}	alias=device_session
	Set Client Configuration	prompt=]
	Set Client Configuration	newline=\n
	Set Client Configuration	width=400
	Set Client Configuration	height=600
	Set Client Configuration	timeout=300
	${OUTPUT}=	Login	sneezy:self_device	sneezy
	Log to console	\nOpenen Session: ${OUTPUT}
	Set Client Configuration	prompt=~#
	Write bare	\n\n
	${OUTPUT}=	CLI:Read Until Prompt
	Should Contain	${OUTPUT}	root@nodegrid
	Close Connection
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/services/
	CLI:Set Field	device_access_per_user_group_authorization	no
	CLI:Commit
	CLI:Delete Device	self_device
	CLI:Delete Users	sneezy
	[Teardown]	CLI:Close Connection

Test functionality for device custom_commands=yes
	[Tags]	EXCLUDEIN3_2
	[Setup]	CLI:Open
	CLI:Enter Path	/settings/services/
	CLI:Set Field	device_access_per_user_group_authorization	yes
	CLI:Commit
	CLI:Test Show Command	device_access_per_user_group_authorization = yes

	SUITE:Add User To Group
	CLI:Enter Path	/settings/devices/
	CLI:Delete If Exists Confirm	self_device
	CLI:Add Device	self_device	device_console	127.0.0.1	root	${ROOT_PASSWORD}

	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/devices
	CLI:Add
	CLI:Set Field	devices	self_device
	CLI:Set Field	session	read-write
	CLI:Set Field	custom_commands	yes
	Write bare	commit\n
	Read Until Regexp	\\(.*\\)\\s+:
	Write Bare	yes\n
	${OUTPUT}=	CLI:Read Until Prompt
	Should Not Contain	${OUTPUT}	Error
	SUITE:Create Custom Commands File
	CLI:Enter Path	/settings/devices/self_device/commands
	CLI:Add
	CLI:Set Field	command	custom_commands
	CLI:Set Field	custom_command_enabled1	yes
	CLI:Set Field	custom_command_label1	CustomTest
	CLI:Set Field	custom_command_script1	test_device.py
	CLI:Commit

	Open Connection	${HOST}	alias=device_session
	Set Client Configuration	prompt=#
	Set Client Configuration	newline=\n
	Set Client Configuration	width=400
	Set Client Configuration	height=600
	Set Client Configuration	timeout=300
	${OUTPUT}=	Login	sneezy	sneezy
	Log to console	\nOpenen Session: ${OUTPUT}
	CLI:Enter Path	/access/self_device
	Run Keyword And Continue On Failure	CLI:Test Available Commands	CustomTest
	${ret}=	Run Keyword And Continue On Failure	CLI:Write	CustomTest
	Run Keyword And Continue On Failure	Should Contain	${ret}	hello
	Close Connection
	CLI:Switch Connection	default
	[Teardown]	CLI:Close Connection

Test functionality for device custom_commands=no
	[Tags]	EXCLUDEIN3_2
	[Setup]	CLI:Open
	CLI:Enter Path	/settings/services/
	CLI:Set Field	device_access_per_user_group_authorization	yes
	CLI:Commit
	CLI:Test Show Command	device_access_per_user_group_authorization = yes

	SUITE:Add User To Group
	CLI:Enter Path	/settings/devices/
	CLI:Delete If Exists Confirm	self_device
	CLI:Add Device	self_device	device_console	127.0.0.1	root	${ROOT_PASSWORD}

	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/devices
	CLI:Add
	CLI:Set Field	devices	self_device
	CLI:Set Field	session	read-write
	CLI:Set Field	custom_commands	no
	Write Bare	commit\n
	Read Until Regexp	\\(.*\\)\\s+:
	Write Bare	yes\n
	${OUTPUT}=	CLI:Read Until Prompt
	Should Not Contain	${OUTPUT}	Error
	SUITE:Create Custom Commands File
	CLI:Enter Path	/settings/devices/self_device/commands
	CLI:Add
	CLI:Set Field	command	custom_commands
	CLI:Set Field	custom_command_enabled1	yes
	CLI:Set Field	custom_command_label1	CustomTest
	CLI:Set Field	custom_command_script1	test_device.py
	CLI:Commit

	Open Connection	${HOST}	alias=device_session
	Set Client Configuration	prompt=#
	Set Client Configuration	newline=\n
	Set Client Configuration	width=400
	Set Client Configuration	height=600
	Set Client Configuration	timeout=300
	${OUTPUT}=	Login	sneezy	sneezy
	Log to console	\nOpenen Session: ${OUTPUT}
	CLI:Enter Path	/access/self_device
	CLI:Test Unavailable Commands	CustomTest
	Close Connection
	CLI:Switch Connection	default
	[Teardown]	CLI:Close Connection

Test functionality for group device power=no_access
	[Setup]	CLI:Open
	CLI:Enter Path	/settings/services/
	CLI:Set Field	device_access_per_user_group_authorization	yes
	CLI:Commit
	CLI:Test Show Command	device_access_per_user_group_authorization = yes

	SUITE:Add User To Group
	CLI:Enter Path	/settings/devices/
	CLI:Delete If Exists Confirm	self_device
	CLI:Add Device	self_device	pdu_servertech	192.168.2.111	admn	admn	enabled

	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/devices
	CLI:Add
	CLI:Set Field	devices	self_device
	CLI:Set Field	session	read-write
	CLI:Set Field	power	no_access
	write bare	commit\n
	Run Keyword If	'${NGVERSION}' >= '4.2'	Read Until Regexp	\\(.*\\)\\s+:
	Run Keyword If	'${NGVERSION}' >= '4.2'	Write Bare	yes\n
	${OUTPUT}=	CLI:Read Until Prompt
	Should Not Contain	${OUTPUT}	Error

	Open Connection	${HOST}	alias=device_session
	Set Client Configuration	prompt=#
	Set Client Configuration	newline=\n
	Set Client Configuration	width=400
	Set Client Configuration	height=600
	Set Client Configuration	timeout=300
	${OUTPUT}=	Login	sneezy	sneezy
	Log to console	\nOpenen Session: ${OUTPUT}
	CLI:Enter Path	/access/self_device
	CLI:Test Unavailable Commands	outlet_on	outlet_status	outlet_off	outlet_cycle
	Close Connection
	CLI:Switch Connection	default
	CLI:Delete Device	self_device
	[Teardown]	CLI:Close Connection

Test functionality for group device power=power_status
	[Setup]	CLI:Open
	CLI:Enter Path	/settings/services/
	CLI:Set Field	device_access_per_user_group_authorization	yes
	CLI:Commit
	CLI:Test Show Command	device_access_per_user_group_authorization = yes

	SUITE:Add User To Group
	CLI:Enter Path	/settings/devices/
	CLI:Delete If Exists Confirm	self_device
	CLI:Add Device	self_device	pdu_servertech	192.168.2.111	admn	admn	on-demand

	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/devices
	CLI:Add
	CLI:Set Field	devices	self_device
	CLI:Set Field	session	read-write
	CLI:Set Field	power	power_status
	write bare	commit\n
	Run Keyword If	'${NGVERSION}' >= '4.2'	Read Until Regexp	\\(.*\\)\\s+:
	Run Keyword If	'${NGVERSION}' >= '4.2'	Write Bare	yes\n
	${OUTPUT}=	CLI:Read Until Prompt
	Should Not Contain	${OUTPUT}	Error

	Open Connection	${HOST}	alias=device_session
	Set Client Configuration	prompt=#
	Set Client Configuration	newline=\n
	Set Client Configuration	width=400
	Set Client Configuration	height=600
	Set Client Configuration	timeout=300
	${OUTPUT}=	Login	sneezy	sneezy
	Log to console	\nOpenen Session: ${OUTPUT}
	CLI:Enter Path	/access/self_device
	CLI:Test Available Commands	outlet_status
	CLI:Test Unavailable Commands	outlet_on	outlet_off	outlet_cycle
	Close Connection
	CLI:Switch Connection	default
	CLI:Delete Device	self_device
	[Teardown]	CLI:Close Connection

Test functionality for group device power=power_control
	[Setup]	CLI:Open
	CLI:Enter Path	/settings/services/
	CLI:Set Field	device_access_per_user_group_authorization	yes
	CLI:Commit
	CLI:Test Show Command	device_access_per_user_group_authorization = yes

	SUITE:Add User To Group
	CLI:Enter Path	/settings/devices/
	CLI:Delete If Exists Confirm	self_device
	CLI:Add Device	self_device	pdu_servertech	192.168.2.111	admn	admn	on-demand

	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/devices
	CLI:Add
	CLI:Set Field	devices	self_device
	CLI:Set Field	session	read-write
	CLI:Set Field	power	power_control
	write bare	commit\n
	Run Keyword If	'${NGVERSION}' >= '4.2'	Read Until Regexp	\\(.*\\)\\s+:
	Run Keyword If	'${NGVERSION}' >= '4.2'	Write Bare	yes\n
	${OUTPUT}=	CLI:Read Until Prompt
	Should Not Contain	${OUTPUT}	Error

	Open Connection	${HOST}	alias=device_session
	Set Client Configuration	prompt=#
	Set Client Configuration	newline=\n
	Set Client Configuration	width=400
	Set Client Configuration	height=600
	Set Client Configuration	timeout=300
	${OUTPUT}=	Login	sneezy	sneezy
	Log to console	\nOpenen Session: ${OUTPUT}
	CLI:Enter Path	/access/self_device
	CLI:Test Available Commands	outlet_on	outlet_off	outlet_cycle
	Close Connection
	CLI:Switch Connection	default
	CLI:Delete Device	self_device
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Delete All Devices	AND	CLI:Close Connection

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Disable Password Complexity Check
	CLI:Enter Path	/settings/authorization/
	@{GROUPS}=	CLI:Get All User Groups
	FOR	${GROUP}	IN	@{GROUPS}
		Run Keyword Unless	"${GROUP}" == "${DEFAULT_USERNAME}" or "${GROUP}" == "user"	CLI:Delete Confirm	${GROUP}
	END
	CLI:Add
	Write	set name=${GROUP_NAME}
	CLI:Read Until Prompt
	CLI:Commit
	CLI:Test Show Command	${GROUP_NAME}

	#Precaution for bug 890
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	menu-driven_access_to_devices	no
	CLI:Set Field	custom_session_timeout	no
	CLI:Set Field	configure_user_accounts	no
	CLI:Commit

SUITE:Teardown
	CLI:Open
	CLI:Enter Path	/settings/local_accounts/
	CLI:Delete If Exists Confirm	sneezy
	CLI:Enter Path	/settings/devices/
	CLI:Delete If Exists Confirm	self_device

	#Precaution for bug 890
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	menu-driven_access_to_devices	no
	CLI:Set Field	custom_session_timeout	no
	CLI:Set Field	configure_user_accounts	no
	CLI:Commit

	CLI:Enter Path	/settings/authorization/
	CLI:Delete Confirm	${GROUP_NAME}
	${GROUPS}=	CLI:Ls
	Should Not Contain	${GROUPS}	${GROUP_NAME}
	CLI:Enter Path	/settings/services/
	CLI:Set Field	device_access_per_user_group_authorization	no
	CLI:Commit
	CLI:Close Connection

SUITE:Disable Password Complexity Check
	CLI:Enter Path	/settings/password_rules/
	CLI:Set	check_password_complexity=no
	CLI:Commit

SUITE:Add User To Group
	CLI:Enter Path	/settings/local_accounts/
	CLI:Delete If Exists Confirm	sneezy
	CLI:Add
	CLI:Set Field	username	sneezy
	CLI:Set Field	password	sneezy
	CLI:Set Field	user_group	${GROUP_NAME}
	CLI:Commit

SUITE:Create Custom Commands File
	CLI:Remove File Remotely	/etc/scripts/custom_commands/test_device.py
	CLI:Create File Remotely	/etc/scripts/custom_commands/test_device.py	"def CustomTest(dev):"
	CLI:Append To File Remotely	/etc/scripts/custom_commands/test_device.py	" print('hello')"
	CLI:Close Connection
	CLI:Open