*** Settings ***
Resource	../../init.robot
Documentation	Tests virsh (libvirt) support on CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${GROUP_NAME_VIRSH}	test-virsh-group
${USERNAME_VIRSH}	test-virsh-user
${PASSWORD_VIRSH}	${QA_PASSWORD}

*** Test Cases ***
Test Execute Virsh Without Shell
	[setup]	SUITE:Set Permissions	no	yes
	SUITE:Open Custom User Connection
	SUITE:Execute Virsh	Error: Invalid command: exec
	[Teardown]	SUITE:Close Custom User Connection

Test Execute Virsh Without Sudo
	[setup]	SUITE:Set Permissions	yes	no
	SUITE:Open Custom User Connection
	SUITE:Execute Virsh	Error: User must have sudo permission.
	[Teardown]	SUITE:Close Custom User Connection

Test Execute Virsh Without Shell or Sudo
	[setup]	SUITE:Set Permissions	no	no
	SUITE:Open Custom User Connection
	SUITE:Execute Virsh	Error: Invalid command: exec
	[Teardown]	SUITE:Close Custom User Connection

Test Execute Virsh With Shell and Sudo
	[setup]	SUITE:Set Permissions
	SUITE:Open Custom User Connection
	SUITE:Execute Virsh	no
	[Teardown]	SUITE:Close Custom User Connection

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Delete User And Group
	SUITE:Add User And Group

SUITE:Teardown
	SUITE:Delete User And Group
	CLI:Close Connection

SUITE:Open Custom User Connection
	CLI:Open	${USERNAME_VIRSH}	${PASSWORD_VIRSH}	session_alias=virsh_user_session

SUITE:Close Custom User Connection
	CLI:Switch Connection	virsh_user_session
	CLI:Close Current Connection
	CLI:Switch Connection	default

SUITE:Add Group
	CLI:Enter Path	/settings/authorization
	CLI:Add
	CLI:Set	name=${GROUP_NAME_VIRSH}
	CLI:Commit

SUITE:Add User And Group
	SUITE:Add Group
	CLI:Add User	${USERNAME_VIRSH}	${PASSWORD_VIRSH}	${GROUP_NAME_VIRSH}

SUITE:Delete User And Group
	CLI:Delete Users	${USERNAME_VIRSH}
	SUITE:Delete Group	${GROUP_NAME_VIRSH}

SUITE:Delete Group
	[Arguments]	${GROUP}
	CLI:Enter Path	/settings/authorization
	CLI:Delete If Exists Confirm	${GROUP}

SUITE:Set Permissions
	[Arguments]	${SHELL_ACCESS}=yes	${SUDO_PERMISSION}=yes
	CLI:Enter Path	/settings/authorization/${GROUP_NAME_VIRSH}/profile
	CLI:Set	shell_access=${SHELL_ACCESS} sudo_permission=${SUDO_PERMISSION}
	CLI:Commit

SUITE:Execute Virsh
	[Arguments]	${ERROR_MESSAGE}
	Write	exec virsh
	${OUTPUT}	Read Until Regexp	(#)
	IF	'${ERROR_MESSAGE}'=='no'
		Should Contain	${OUTPUT}	virsh #
		CLI:Write	quit
	ELSE
		Should Contain	${OUTPUT}	${ERROR_MESSAGE}
	END
