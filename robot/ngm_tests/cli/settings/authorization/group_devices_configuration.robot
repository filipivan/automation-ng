*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authorization > Group Name > Devices... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI		NON-CRITICAL	BUG_NG_9704
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:OPEN
Suite Teardown	SUITE:TEARDOWN

*** Variables ***
${GROUP_NAME}	another_group

*** Test Cases ***
Test adding device for group
	[Tags]	NON-CRITICAL	BUG_NG_9704
	CLI:Enter Path	/settings/devices/
	CLI:Delete If Exists Confirm	self_device
	CLI:Add Device	self_device	device_console	127.0.0.1	admn	admn	enabled
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/devices
	CLI:Add
	CLI:Set Field	devices	self_device
	CLI:Set Field	power	power_status
	CLI:Set Field	door	door_status
	CLI:Set Field	mks	yes
	CLI:Set Field	kvm	yes
	CLI:Set Field	sp_console	yes
	CLI:Set Field	session	read-write
	CLI:Set Field	reset_device	yes
	CLI:Set Field	virtual_media	yes
	CLI:Set Field	access_log_audit	yes
	CLI:Set Field	access_log_clear	yes
	CLI:Set Field	event_log_audit	yes
	CLI:Set Field	event_log_clear	yes
	CLI:Set Field	sensors_data	yes
	CLI:Set Field	monitoring	yes
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Set Field	custom_commands	yes
	Write Bare	commit\n
	Run Keyword If	'${NGVERSION}' >= '4.0'	Read Until	:
	Run Keyword If	'${NGVERSION}' >= '4.0'	Write Bare	yes\n
	CLI:Read Until Prompt
	${OUTPUT}	CLI:Show
	Should Contain	${OUTPUT}	devices = self_device
	Should Contain	${OUTPUT}	session = read-write
	[Teardown]	CLI:Cancel	Raw

Test edit device for group
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/devices
	CLI:Test Show Command Regexp	self_device\\s+read/write
	CLI:Edit	self_device
	CLI:Set Field	session	read-only
	Write Bare	commit\n
	Run Keyword If	'${NGVERSION}' >= '4.0'	Read Until	:
	Run Keyword If	'${NGVERSION}' >= '4.0'	Write Bare	yes\n
	CLI:Read Until Prompt
	CLI:Test Show Command Regexp	self_device\\s+read\\sonly
	[Teardown]	CLI:Cancel	Raw

Test Set New Authorization To Multiple Devices Using Regexp
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_1
	[Setup]	Run Keywords	CLI:Delete All Devices	AND	SUITE:Add Dummy Devices
	CLI:Enter Path	/settings/authorization/user/devices/
	CLI:Add
	CLI:Set	devices=dummy* access_log_audit=yes
	SUITE:Commit And Confirm
	${OUTPUT}=	CLI:Test Show Command Regexp	dummy0\\s+read\\/write\\s+-\\s+-\\s+control\\s+-\\s+-\\s+audit
	...	dummy1\\s+read\\/write\\s+-\\s+-\\s+control\\s+-\\s+-\\s+audit
	...	dummy2\\s+read\\/write\\s+-\\s+-\\s+control\\s+-\\s+-\\s+audit
	...	dummy3\\s+read\\/write\\s+-\\s+-\\s+control\\s+-\\s+-\\s+audit
	...	dummy4\\s+read\\/write\\s+-\\s+-\\s+control\\s+-\\s+-\\s+audit
	Log To Console	${OUTPUT}
	[Teardown]	CLI:Cancel	Raw

Test Edit Devices Using Regexp
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_1
	CLI:Enter Path	/settings/authorization/user/devices/
	CLI:Edit	dummy[0-2]
	CLI:Set	access_log_clear=yes
	SUITE:Commit And Confirm
	${OUTPUT}=	CLI:Test Show Command Regexp	dummy0\\s+read\\/write\\s+-\\s+-\\s+control\\s+-\\s+-\\s+audit\\/clear
	...	dummy1\\s+read\\/write\\s+-\\s+-\\s+control\\s+-\\s+-\\s+audit\\/clear
	...	dummy2\\s+read\\/write\\s+-\\s+-\\s+control\\s+-\\s+-\\s+audit\\/clear
	...	dummy3\\s+read\\/write\\s+-\\s+-\\s+control\\s+-\\s+-\\s+audit
	...	dummy4\\s+read\\/write\\s+-\\s+-\\s+control\\s+-\\s+-\\s+audit
	Log To Console	${OUTPUT}
	[Teardown]	CLI:Cancel	Raw

Test Delete Devices Using Regexp
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_1
	CLI:Enter Path	/settings/authorization/user/devices/
	CLI:Write	delete dummy[0-2]
	SUITE:Commit And Confirm
	${OUTPUT}=	CLI:Test Not Show Command	dummy0\\s+read\\/write\\s+-\\s+-\\s+control\\s+-\\s+-\\s+audit\\/clear
	...	dummy1\\s+read\\/write\\s+-\\s+-\\s+control\\s+-\\s+-\\s+audit\\/clear
	...	dummy2\\s+read\\/write\\s+-\\s+-\\s+control\\s+-\\s+-\\s+audit\\/clear
	Log To Console	${OUTPUT}
	${OUTPUT}=	CLI:Test Show Command Regexp	dummy3\\s+read\\/write\\s+-\\s+-\\s+control\\s+-\\s+-\\s+audit
	...	dummy4\\s+read\\/write\\s+-\\s+-\\s+control\\s+-\\s+-\\s+audit
	Log To Console	${OUTPUT}
	CLI:Write	delete dummy[2-4]
	SUITE:Commit And Confirm
	${OUTPUT}=	CLI:Test Not Show Command	dummy3\\s+read\\/write\\s+-\\s+-\\s+control\\s+-\\s+-\\s+audit
	...	dummy4\\s+read\\/write\\s+-\\s+-\\s+control\\s+-\\s+-\\s+audit
	Log To Console	${OUTPUT}
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:OPEN
	CLI:Open
	CLI:Delete All Devices
	CLI:Enter Path	/settings/authorization/
	@{GROUPS}=	CLI:Get All User Groups
	FOR	${GROUP}	IN	@{GROUPS}
		Run Keyword Unless	"${GROUP}" == "${DEFAULT_USERNAME}" or "${GROUP}" == "user"	CLI:Delete Confirm	${GROUP}
	END
	CLI:Add
	Write	set name=${GROUP_NAME}
	CLI:Read Until Prompt
	CLI:Commit
	CLI:Test Show Command	${GROUP_NAME}

	#Precaution for bug 890
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	menu-driven_access_to_devices	no
	CLI:Set Field	custom_session_timeout	no
	CLI:Set Field	configure_user_accounts	no
	CLI:Commit

SUITE:TEARDOWN
	CLI:Enter Path	/settings/local_accounts/
	CLI:Delete If Exists Confirm	sneezy
	CLI:Enter Path	/settings/devices/
	CLI:Delete If Exists Confirm	self_device

	#Precaution for bug 890
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	menu-driven_access_to_devices	no
	CLI:Set Field	custom_session_timeout	no
	CLI:Set Field	configure_user_accounts	no
	CLI:Commit

	CLI:Enter Path	/settings/authorization/
	CLI:Delete Confirm	${GROUP_NAME}
	${GROUPS}=	CLI:Ls
	Should Not Contain	${GROUPS}	${GROUP_NAME}
	CLI:Enter Path	/settings/services/
	CLI:Set Field	device_access_per_user_group_authorization	no
	CLI:Commit
	CLI:Delete All Devices
	CLI:Close Connection

SUITE:Add Dummy Devices
	FOR		${INDEX}	IN RANGE	5
		CLI:Add Device	DEVICE_NAME=dummy${INDEX}	DEVICE_TYPE=ilo	IP_ADDRESS=127.0.0.1
	END
	[Teardown]	CLI:Cancel	Raw

SUITE:Commit And Confirm
	Write	commit
	${OUTPUT}=	Read Until Regexp	(]#|:)
	${STATUS}=	Run Keyword And Return Status
	...	Should Contain	${OUTPUT}	:
	Run Keyword If	'${STATUS}' == 'True'	CLI:Write	yes
	Run Keyword If	'${STATUS}' == 'False'	Should Contain	${OUTPUT}	]#