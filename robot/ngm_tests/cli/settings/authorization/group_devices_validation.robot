*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authorization > Group Name > Devices... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:OPEN
Suite Teardown	SUITE:TEARDOWN

*** Variables ***
${GROUP_NAME}	another_group

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/devices
	CLI:Test Available Commands	add	commit	event_system_clear	ls	reboot	shell	shutdown	cd	delete	exit
	...	pwd	revert	show	whoami	change_password	event_system_audit	hostname	quit	set	show_settings

Test show device groups
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/devices
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Show Command	targets	session mode	mks	kvm	power mode	reset device	door mode
	...	access log	event log	sensors data	monitoring	virtual media	custom commands	#sp console
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Show Command	targets	session mode	mks	kvm	power mode	reset device	door mode
	...	access log	event log	sp console	sensors data	monitoring	virtual media

Test set fields for adding devices
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/devices
	CLI:Add
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Show Command	devices	session = read-write	power = power_control	door = door_control	mks = no
	...	kvm = no	reset_device = no	virtual_media = no	access_log_audit = no	sp_console = no
	...	access_log_clear = no	event_log_audit = no	event_log_clear = no	sensors_data = no	monitoring = no	custom_commands = no
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Show Command	devices	session = read-write	power = power_control	door = door_control
	...	mks	kvm	sp_console	reset_device	virtual_media	access_log_audit	access_log_clear	event_log_audit
	...	event_log_clear	sensors_data	monitoring
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Available Fields	access_log_audit	event_log_audit	power	access_log_clear	event_log_clear	virtual_media
	...	custom_commands	kvm	sensors_data	devices	mks	session	door	monitoring	reset_device	sp_console
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Available Fields	access_log_audit	event_log_audit	power	access_log_clear	event_log_clear	virtual_media
	...	kvm	sensors_data	devices	mks	session	door	monitoring	sp_console	reset_device
	CLI:Cancel

Test valid values for devices fields
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/devices
	CLI:Add
	CLI:Test Set Field Options	power	no_access	power_control	power_status
	CLI:Test Set Field Options	door	no_access	door_control	door_status
	CLI:Test Set Field Options	mks	no	yes
	CLI:Test Set Field Options	kvm	no	yes
	CLI:Test Set Field Options	sp_console	no	yes
	CLI:Test Set Field Options	reset_device	no	yes
	CLI:Test Set Field Options	virtual_media	no	yes
	CLI:Test Set Field Options	access_log_audit	no	yes
	CLI:Test Set Field Options	access_log_clear	no	yes
	CLI:Test Set Field Options	event_log_audit	no	yes
	CLI:Test Set Field Options	event_log_clear	no	yes
	CLI:Test Set Field Options	sensors_data	no	yes
	CLI:Test Set Field Options	monitoring	no	yes
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Options	custom_commands	no	yes
	CLI:Cancel

Test invalid values for device fields
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/devices
	CLI:Add
	${COMMANDS}=	Create List	access_log_audit	event_log_audit	power	access_log_clear	event_log_clear	virtual_media
	...	kvm	sensors_data	mks	session	door	monitoring	reset_device	sp_console
	Run Keyword If	'${NGVERSION}' >= '4.0'	Append To List	${COMMANDS}	custom_commands
	SUITE:SET INVALID VALUES ERROR MESSAGES	${COMMANDS}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	devices	${EMPTY}	Error: Invalid value: null
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	devices	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: devices
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	devices	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}
	CLI:Cancel

*** Keywords ***
SUITE:OPEN
	CLI:Open
	CLI:Enter Path	/settings/authorization/
	@{GROUPS}=	CLI:Get All User Groups
	FOR	${GROUP}	IN	@{GROUPS}
		Run Keyword Unless	"${GROUP}" == "${DEFAULT_USERNAME}" or "${GROUP}" == "user"	CLI:Delete Confirm	${GROUP}
	END
	CLI:Add
	Write	set name=${GROUP_NAME}
	CLI:Read Until Prompt
	CLI:Commit
	CLI:Test Show Command	${GROUP_NAME}

SUITE:TEARDOWN
	CLI:Enter Path	/settings/authorization/
	CLI:Delete Confirm	${GROUP_NAME}
	${GROUPS}=	CLI:Ls
	Should Not Contain	${GROUPS}	${GROUP_NAME}
	CLI:Close Connection

SUITE:SET INVALID VALUES ERROR MESSAGES
	[Arguments]	${COMMANDS}
	FOR		${COMMAND}	IN	@{COMMANDS}
		Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${COMMAND}	a	Error: Invalid value: a for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value: ${COMMAND}
		Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${COMMAND}	a	Error: Invalid value: a
	END