*** Settings ***
Resource	../../init.robot
Documentation	Tests FRR (previously Quagga) support on CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${GROUP_NAME_FRR}	test-frr-group
${USERNAME_FRR}	test-frr-user
${PASSWORD_FRR}	${QA_PASSWORD}

*** Test Cases ***
Test Execute FRR Without Required Permissions
	[Setup]	SUITE:Unset Required Permissions
	SUITE:Open Custom User Connection
	SUITE:Executing FRR Command Should Fail
	[Teardown]	SUITE:Close Custom User Connection

Test Execute FRR With Required Permissions
	[Setup]	SUITE:Set Required Permissions
	SUITE:Open Custom User Connection
	SUITE:Executing FRR Command Should Succeed
	SUITE:Run Some Commands On FRR Session
	[Teardown]	SUITE:Close Custom User Connection

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Change Hostname	nodegrid
	SUITE:Delete User And Group
	SUITE:Add User And Group

SUITE:Teardown
	SUITE:Delete User And Group
	CLI:Close Connection

SUITE:Open Custom User Connection
	CLI:Open	${USERNAME_FRR}	${PASSWORD_FRR}	session_alias=frr_user_session

SUITE:Close Custom User Connection
	CLI:Switch Connection	frr_user_session
	CLI:Close Current Connection
	CLI:Switch Connection	default

SUITE:Add Group
	CLI:Enter Path	/settings/authorization
	CLI:Add
	CLI:Set	name=${GROUP_NAME_FRR}
	CLI:Commit

SUITE:Add User And Group
	CLI:Add User	${USERNAME_FRR}	${PASSWORD_FRR}
	SUITE:Add Group
	CLI:Add User To Group	${USERNAME_FRR}	${GROUP_NAME_FRR}

SUITE:Delete User And Group
	CLI:Delete Users	${USERNAME_FRR}
	SUITE:Delete Group	${GROUP_NAME_FRR}

SUITE:Delete Group
	[Arguments]	${GROUP}
	CLI:Enter Path	/settings/authorization
	CLI:Delete If Exists Confirm	${GROUP}

SUITE:Set Required Permissions
	CLI:Enter Path	/settings/authorization/${GROUP_NAME_FRR}/profile
	CLI:Set	shell_access=yes sudo_permission=yes
	CLI:Commit

SUITE:Unset Required Permissions
	CLI:Enter Path	/settings/authorization/${GROUP_NAME_FRR}/profile
	CLI:Set	shell_access=no sudo_permission=no
	CLI:Commit

SUITE:Executing FRR Command Should Fail
	Set Client Configuration	prompt=${HOSTNAME_NODEGRID}\#
	Set Client Configuration	timeout=10s
	${HAS_FRR}=	Run Keyword And Return Status	CLI:Write	exec frr
	Run Keyword If	${HAS_FRR}	Fail	Custom user should not have FRR without required permissions

SUITE:Executing FRR Command Should Succeed
	Set Client Configuration	prompt=${HOSTNAME_NODEGRID}\#
	CLI:Write	exec frr

SUITE:Run Some Commands On FRR Session
	Write	show interface eth0
	${OUTPUT}=	Read Until Prompt
	Log To Console	\n${OUTPUT}\n
	Should Not Contain	${OUTPUT}	% Unknown command: show interface eth0

	Write	show version
	${OUTPUT}=	Read Until Prompt
	Log To Console	\n${OUTPUT}\n
	Should Not Contain	${OUTPUT}	% Unknown command: show version

	Write	show daemons
	${OUTPUT}=	Read Until Prompt
	Log To Console	\n${OUTPUT}\n
	Should Not Contain	${OUTPUT}	% Unknown command: show daemons