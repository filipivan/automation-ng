*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authorization Group Profile Sudo Permission through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	SUDO	EXCLUDEIN3_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${GROUP_NAME}	automation_group
${SUDO_GROUP_NAME}	automation_sudo_group
${USER}	automation_user
${SUDO_USER}	automation_sudo_user
${USER_PASSWORD}	${QA_PASSWORD}

*** Test Cases ***
Test Sudo Permission add=enabled
	SUITE:Add Group	${SUDO_GROUP_NAME}	yes
	SUITE:Add User	${SUDO_USER}	${SUDO_GROUP_NAME}
	${WHOAMI}=	CLI:Get User Whoami
	CLI:Should Be Equal	${WHOAMI}	${DEFAULT_USERNAME}
	CLI:Open	${SUDO_USER}	${USER_PASSWORD}	sudo_session
	Set Client Configuration	prompt=$
	CLI:Write	shell
	${WHOAMI}=	CLI:Get User Whoami
	CLI:Should Be Equal	${WHOAMI}	${SUDO_USER}
	Set Client Configuration	prompt=#
	CLI:Write	sudo su -
	${WHOAMI}=	CLI:Get User Whoami
	CLI:Should Be Equal	${WHOAMI}	root
	CLI:Close Connection	Yes
	CLI:Switch Connection	admin_session

Test Sudo Permission edit=disabled
	CLI:Enter Path	/settings/authorization/${SUDO_GROUP_NAME}/profile
	CLI:Set	shell_access=yes sudo_permission=no
	CLI:Commit
	${WHOAMI}=	CLI:Get User Whoami
	CLI:Should Be Equal	${WHOAMI}	${DEFAULT_USERNAME}
	CLI:Open	${SUDO_USER}	${USER_PASSWORD}	user_session
	Set Client Configuration	prompt=$
	CLI:Write	shell
	${WHOAMI}=	CLI:Get User Whoami
	CLI:Should Be Equal	${WHOAMI}	${SUDO_USER}
	Write	sudo su -
	Read Until	Password:
	${OUTPUT}=	CLI:Write	${USER_PASSWORD}
	Should Contain	${OUTPUT}	${SUDO_USER} is not in the sudoers file.  This incident will be reported.
	${WHOAMI}=	CLI:Get User Whoami
	CLI:Should Be Equal	${WHOAMI}	${SUDO_USER}
	CLI:Close Connection	Yes
	CLI:Switch Connection	admin_session
	[Teardown]	Run Keywords	CLI:Delete Users	${SUDO_USER}
	...	AND	SUITE:Delete Group	${SUDO_GROUP_NAME}

Test Sudo Permission add=disabled
	SUITE:Add Group	${GROUP_NAME}	no
	SUITE:Add User	${USER}	${GROUP_NAME}
	${WHOAMI}=	CLI:Get User Whoami
	CLI:Should Be Equal	${WHOAMI}	${DEFAULT_USERNAME}
	CLI:Open	${USER}	${USER_PASSWORD}	user_session
	Set Client Configuration	prompt=$
	CLI:Write	shell
	${WHOAMI}=	CLI:Get User Whoami
	CLI:Should Be Equal	${WHOAMI}	${USER}
	Write	sudo su -
	Read Until	Password:
	${OUTPUT}=	CLI:Write	${USER_PASSWORD}
	Should Contain	${OUTPUT}	${USER} is not in the sudoers file.  This incident will be reported.
	${WHOAMI}=	CLI:Get User Whoami
	CLI:Should Be Equal	${WHOAMI}	${USER}
	CLI:Close Connection	Yes
	CLI:Switch Connection	admin_session

Test Sudo Permission edit=enabled
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set	shell_access=yes sudo_permission=yes
	CLI:Commit
	${WHOAMI}=	CLI:Get User Whoami
	CLI:Should Be Equal	${WHOAMI}	${DEFAULT_USERNAME}
	CLI:Open	${USER}	${USER_PASSWORD}	user_session
	Set Client Configuration	prompt=$
	CLI:Write	shell
	${WHOAMI}=	CLI:Get User Whoami
	CLI:Should Be Equal	${WHOAMI}	${USER}
	Set Client Configuration	prompt=#
	Write	sudo su -
	${WHOAMI}=	CLI:Get User Whoami
	CLI:Should Be Equal	${WHOAMI}	root
	CLI:Close Connection	Yes
	CLI:Switch Connection	admin_session
	[Teardown]	Run Keywords	CLI:Delete Users	${USER}
	...	AND	SUITE:Delete Group	${GROUP_NAME}

*** Keywords ***
SUITE:Setup
	CLI:Open	session_alias=admin_session
	CLI:Delete Users	${USER}	${SUDO_USER}
	SUITE:Delete Group	${SUDO_GROUP_NAME}	${GROUP_NAME}

SUITE:Teardown
	CLI:Delete Users	${USER}	${SUDO_USER}
	SUITE:Delete Group	${SUDO_GROUP_NAME}	${GROUP_NAME}
	CLI:Close Connection

SUITE:Add Group
	[Arguments]	${GROUP_NAME}	${SUDO_PERMISSION}
	CLI:Enter Path	/settings/authorization/
	CLI:Add
	CLI:Set	name=${GROUP_NAME}
	CLI:Commit
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set	shell_access=yes sudo_permission=${SUDO_PERMISSION}
	CLI:Commit

SUITE:Add User
	[Arguments]	${USERNAME}	${USER_GROUP}
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	CLI:Set	username=${USERNAME} password=${USER_PASSWORD} user_group=${USER_GROUP}
	CLI:Commit

SUITE:Delete Group
	[Arguments]	@{GROUPS}
	CLI:Enter Path	/settings/authorization/
	FOR		${GROUP}	IN	@{GROUPS}
		CLI:Delete If Exists Confirm	${GROUP}
	END