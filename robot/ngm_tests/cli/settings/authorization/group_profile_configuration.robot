*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authorization Group Profile... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${GROUP_NAME}	automation_group

*** Test Cases ***
Test custom session timeout
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	menu-driven_access_to_devices	no
	CLI:Set Field	custom_session_timeout	yes
	${OUTPUT}=	CLI:Write	set timeout=5	yes
	Should Contain	${OUTPUT}	Error: timeout: The value for session timeout must be 90 seconds or higher. Use 0 to suspend idle time.
	${OUTPUT}=	CLI:Write	set timeout=!~	yes
	Should Contain	${OUTPUT}	Error: timeout: The value for session timeout must be 90 seconds or higher. Use 0 to suspend idle time.
	${OUTPUT}=	CLI:Write	set timeout=${EXCEEDED}	yes
	Should Contain	${OUTPUT}	Error: timeout: Exceeded the maximum size for this field.
	CLI:Set Field	timeout	300
	CLI:Commit
	CLI:Test Show Command	timeout = 300

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/authorization/
	@{GROUPS}=	CLI:Get All User Groups
	FOR	${GROUP}	IN	@{GROUPS}
		Run Keyword Unless	"${GROUP}" == "${DEFAULT_USERNAME}" or "${GROUP}" == "user"	CLI:Delete Confirm	${GROUP}
	END
	CLI:Enter Path	/settings/authorization/
	CLI:Add
	Write	set name=${GROUP_NAME}
	CLI:Read Until Prompt
	CLI:Commit
	CLI:Test Show Command	${GROUP_NAME}

SUITE:Teardown
	Run Keyword If	'${NGVERSION}' > '4.0'	CLI:Open
	CLI:Enter Path	/settings/authorization/
	@{GROUPS}=	CLI:Get All User Groups
	FOR	${GROUP}	IN	@{GROUPS}
		Run Keyword Unless	"${GROUP}" == "${DEFAULT_USERNAME}" or "${GROUP}" == "user"	CLI:Delete Confirm	${GROUP}
	END
	CLI:Delete Users	grumpy
	CLI:Delete Devices	grumpy_self
	CLI:Close Connection