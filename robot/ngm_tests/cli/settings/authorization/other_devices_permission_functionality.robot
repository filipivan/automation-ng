*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authorization > Group Name > Devices > Other Devices... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	SSH	SHOW	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4

Suite Setup	SUITE:SETUP
Suite Teardown	SUITE:TEARDOWN

*** Variables ***
${GROUP_NAME}	user
${TEST_USER_NAME}	test_other_devices
${DEVICE_CONSOLE_NAME}	test_device_console
${DEVICE_PDU_NAME}	test_pdu
${SCRIPT_NAME}	test
${SCRIPT_MSG}	test_custom_script

*** Test Cases ***
Test Visible Devices with device_access_per_user_group_authorization=no
	[Documentation]	Checks if devices are shown while feature is disabled
	CLI:Open	${TEST_USER_NAME}	${QA_PASSWORD}
	CLI:Enter Path	/access/
	${VISIBLE_DEVICES}	CLI:Show
	Should Contain	${VISIBLE_DEVICES}	${DEVICE_CONSOLE_NAME}
	Should Contain	${VISIBLE_DEVICES}	${DEVICE_PDU_NAME}
	[Teardown]	CLI:Close Connection

Test Visible Devices with device_access_per_user_group_authorization=yes
	[Documentation]	Checks if devices are not shown with feature enabled and the default configuration(which is not access at all)
	CLI:Open
	SUITE:Set Device Access Per User Group Authorization	yes
	CLI:Close Connection
	CLI:Open	${TEST_USER_NAME}	${QA_PASSWORD}
	CLI:Enter Path	/access/
	${VISIBLE_DEVICES}	CLI:Show
	Should Not Contain	${VISIBLE_DEVICES}	${DEVICE_CONSOLE_NAME}
	Should Not Contain	${VISIBLE_DEVICES}	${DEVICE_PDU_NAME}
	[Teardown]	CLI:Close Connection

Test Visible Devices with device_access_per_user_group_authorization=yes and session=read-only
	[Documentation]	Checks if you can connect but can't type on device after setting session=read-only
	[Tags]	NON-CRITICAL	BUG_NG_9902
	CLI:Open
	SUITE:Set Session	read-only
	CLI:Close Connection
	CLI:Open	${TEST_USER_NAME}	${QA_PASSWORD}
	CLI:Enter Path	/access/${DEVICE_CONSOLE_NAME}
	Write	connect
#	Read Until	[Enter '^Ec.' to cli ]
	Write Bare	\n
	Read Until	[read-only -- use ^E c ? for help]
	Write	\x05cf
	Read Until	[session is read-only]
	CLI:Disconnect Device
	[Teardown]	CLI:Close Connection

Test Visible Devices with device_access_per_user_group_authorization=yes and session=read-write
	[Documentation]	Checks if you can connect and type on device after setting session=read-write
	[Tags]	NON-CRITICAL	BUG_NG_9382	#will probably be failing because of this bug. after bug is solved, based on the solution we need to fix this test(probably typing ^Ecf instead of \n)
	CLI:Open
	SUITE:Set Session	read-write
	CLI:Close Connection
	CLI:Open	${TEST_USER_NAME}	${QA_PASSWORD}
	CLI:Connect Device	${DEVICE_CONSOLE_NAME}
	Write	\n
	Read Until	Login:
	CLI:Disconnect Device
	[Teardown]	CLI:Close Connection

Test Visible Devices with device_access_per_user_group_authorization=yes and power=no_access
	[Documentation]	Checks if Power Control doesn't appear after deactivating it on other_devices
	CLI:Open
	SUITE:Set Power
	CLI:Close Connection
	CLI:Open	${TEST_USER_NAME}	${QA_PASSWORD}
	CLI:Enter Path	/access/${DEVICE_PDU_NAME}
	CLI:Test Not Available Commands	outlet_status
	[Teardown]	CLI:Close Connection

Test Visible Devices with device_access_per_user_group_authorization=yes and power=power_status
	[Documentation]	Checks if Power status appears after activating it on other_devices
	CLI:Open
	SUITE:Set Power	power_status
	CLI:Close Connection
	CLI:Open	${TEST_USER_NAME}	${QA_PASSWORD}
	CLI:Enter Path	/access/${DEVICE_PDU_NAME}
	CLI:Test Available Commands	outlet_status
	[Teardown]	CLI:Close Connection

Test Visible Devices with device_access_per_user_group_authorization=yes and power=power_control
	[Documentation]	Checks if Power Control appears after activating it on other_devices
	CLI:Open
	SUITE:Set Power	power_control
	CLI:Close Connection
	CLI:Open	${TEST_USER_NAME}	${QA_PASSWORD}
	CLI:Enter Path	/access/${DEVICE_PDU_NAME}
	CLI:Test Available Commands	outlet_cycle	outlet_on	outlet_off	outlet_status
	[Teardown]	CLI:Close Connection

Test Visible Devices with device_access_per_user_group_authorization=yes and power=power_control and session=no_access
	[Documentation]	Checks if power_control is shown even with no_access to session
	CLI:Open
	SUITE:Set Power	power_control
	SUITE:Set Session
	CLI:Close Connection
	CLI:Open	${TEST_USER_NAME}	${QA_PASSWORD}
	CLI:Enter Path	/access/${DEVICE_PDU_NAME}
	CLI:Test Available Commands	outlet_cycle	outlet_on	outlet_off	outlet_status
	CLI:Test Not Available Commands	connect
	[Teardown]	CLI:Close Connection

Test access_log_audit
	[Documentation]	Checks if access_log_audit command appears after activating it on other_devices
	CLI:Open
	CLI:Enter Path	/settings/devices/${DEVICE_CONSOLE_NAME}/logging/
	CLI:Set	data_logging=yes
	CLI:Commit
	SUITE:Set access_log_audit	yes
	SUITE:Set Session	read-only
	CLI:Close Connection
	CLI:Open	${TEST_USER_NAME}	${QA_PASSWORD}
	CLI:Enter Path	/access/${DEVICE_CONSOLE_NAME}
	CLI:Test Available Commands	access_log_audit
	[Teardown]	CLI:Close Connection

Test access_log_clear
	[Documentation]	Checks if access_log_clear command appears after activating it on other_devices
	CLI:Open
	CLI:Enter Path	/settings/devices/${DEVICE_CONSOLE_NAME}/logging/
	CLI:Set	data_logging=yes
	CLI:Commit
	SUITE:Set access_log_clear	yes
	SUITE:Set Session	read-only
	CLI:Close Connection
	CLI:Open	${TEST_USER_NAME}	${QA_PASSWORD}
	CLI:Enter Path	/access/${DEVICE_CONSOLE_NAME}
	CLI:Test Available Commands	access_log_clear
	[Teardown]	CLI:Close Connection

Test Custom Commands
	[Documentation]	Checks if a custom command configured on device appears after activating it on other_devices
	CLI:Open	session_alias=default
	CLI:Connect As Root
	CLI:Enter Path	/etc/scripts/custom_commands
	CLI:Write	echo "def test(dev): print ('${SCRIPT_MSG}')" > ${SCRIPT_NAME}.py
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/devices/${DEVICE_CONSOLE_NAME}/commands/
	CLI:Add
	CLI:Set	custom_command_enabled1=yes custom_command_script1=${SCRIPT_NAME}.py custom_command_label1=${SCRIPT_NAME}
	CLI:Commit
	SUITE:Set Custom Commands	yes
	CLI:Close Connection
	CLI:Open	${TEST_USER_NAME}	${QA_PASSWORD}
	CLI:Enter Path	/access/${DEVICE_CONSOLE_NAME}
	CLI:Test Available Commands	${SCRIPT_NAME}
	${OUTPUT}	CLI:Write	${SCRIPT_NAME}
	Should Contain	${OUTPUT}	${SCRIPT_MSG}
	[Teardown]	CLI:Close Connection

*** Keywords ***
SUITE:SETUP
	Skip If	'${NGVERSION}' < '5.6'
	CLI:Open
	SUITE:Set Device Access Per User Group Authorization
	CLI:Add Device	${DEVICE_CONSOLE_NAME}	device_console	${HOSTPEER}	${USERNAME}	${FACTORY_PASSWORD}
	CLI:Add Device	${DEVICE_PDU_NAME}	pdu_raritan
	CLI:Add User	${TEST_USER_NAME}	${QA_PASSWORD}
	CLI:Close Connection

SUITE:TEARDOWN
	Skip If	'${NGVERSION}' < '5.6'
	CLI:Open
	CLI:Delete Device	test_device_console
	CLI:Delete Device	test_pdu
	SUITE:Delete User	${TEST_USER_NAME}
	SUITE:Set Device Access Per User Group Authorization
	Run Keyword And Ignore Error	Wait Until Keyword Succeeds	3x	10s	SUITE:Set Power
	Run Keyword And Ignore Error	Wait Until Keyword Succeeds	3x	10s	SUITE:Set Session	#ignoring due to BUG_NG_9704
	Run Keyword And Ignore Error	Wait Until Keyword Succeeds	3x	10s	SUITE:Set Custom Commands	#ignoring due to BUG_NG_9704
	Run Keyword And Ignore Error	Wait Until Keyword Succeeds	3x	10s	SUITE:Set access_log_audit	#ignoring due to BUG_NG_9704
	Run Keyword And Ignore Error	Wait Until Keyword Succeeds	3x	10s	SUITE:Set access_log_clear	#ignoring due to BUG_NG_9704
	CLI:Close Connection

SUITE:Set Device Access Per User Group Authorization
	[Arguments]	${VALUE}=no
	CLI:Enter Path	/settings/services/
	CLI:Set	device_access_per_user_group_authorization=${VALUE}
	CLI:Commit

SUITE:Set Session
	[Arguments]	${VALUE}=no_access
	CLI:Enter Path	/settings/authorization/user/devices
	CLI:Edit	OTHER_DEVICES
	Write	set session=${VALUE}
	Write	commit
	Read Until	this configuration will be enforced only if security :: services is configured to require authorization in order to access managed devices. (yes, no)
	CLI:Write	yes
	CLI:Commit

SUITE:Set Power
	[Arguments]	${VALUE}=no_access
	CLI:Enter Path	/settings/authorization/user/devices
	CLI:Edit	OTHER_DEVICES
	Write	set power=${VALUE}
	Write	commit
	Read Until	this configuration will be enforced only if security :: services is configured to require authorization in order to access managed devices. (yes, no)
	CLI:Write	yes
	CLI:Commit

SUITE:Delete User
	[Arguments]	${USERNAME}
	CLI:Enter Path	/settings/local_accounts/
	CLI:Delete If Exists Confirm	${USERNAME}

SUITE:Set Custom Commands
	[Arguments]	${VALUE}=no
	CLI:Enter Path	/settings/authorization/user/devices
	CLI:Edit	OTHER_DEVICES
	Write	set custom_commands=${VALUE}
	Write	commit
	Read Until	this configuration will be enforced only if security :: services is configured to require authorization in order to access managed devices. (yes, no)
	CLI:Write	yes
	CLI:Commit

SUITE:Set access_log_audit
	[Arguments]	${VALUE}=no
	CLI:Enter Path	/settings/authorization/user/devices
	CLI:Edit	OTHER_DEVICES
	Write	set access_log_audit=${VALUE}
	Write	commit
	Read Until	this configuration will be enforced only if security :: services is configured to require authorization in order to access managed devices. (yes, no)
	CLI:Write	yes
	CLI:Commit

SUITE:Set access_log_clear
	[Arguments]	${VALUE}=no
	CLI:Enter Path	/settings/authorization/user/devices
	CLI:Edit	OTHER_DEVICES
	Write	set access_log_clear=${VALUE}
	Write	commit
	Read Until	this configuration will be enforced only if security :: services is configured to require authorization in order to access managed devices. (yes, no)
	CLI:Write	yes
	CLI:Commit