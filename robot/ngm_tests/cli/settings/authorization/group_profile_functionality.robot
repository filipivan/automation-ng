*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authorization... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:OPEN
Suite Teardown	SUITE:TEARDOWN

*** Variables ***
${GROUP_NAME}	another_group

*** Test Cases ***
Test shell_access=yes
	${STATUS}=	Run Keyword And Return Status	SUITE:ADD USER AND ADD TO GROUP
	Run Keyword If	'${STATUS}' == 'False'	CLI:Cancel	Raw

	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	shell_access	yes
	CLI:Commit
	Open Connection	${HOST}	alias=test-alias
	Login	grumpy	grumpy
	Set Default Configuration	prompt=~$
	Write	shell\n
	${OUTPUT}=	Read Until	~$
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	WARNING: Improper use of shell commands could lead to data loss
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${OUTPUT}	WARNING: Use of shell commands implies advanced knowledge of the product and operating system.
	CLI:Write	exit
	Set Default Configuration	prompt=]#
	Close Connection
	CLI:Switch Connection	default
	CLI:Delete Users	grumpy
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Delete Users	grumpy

Test shell_access=no
	${STATUS}=	Run Keyword And Return Status	SUITE:ADD USER AND ADD TO GROUP
	Run Keyword If	'${STATUS}' == 'False'	CLI:Cancel	Raw

	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	shell_access	no
	CLI:Commit
	CLI:Open	grumpy	grumpy	test-alias
	${OUTPUT}=	CLI:Write	shell	Raw
	Should Contain	${OUTPUT}	Error: Invalid command: shell

	Close Connection
	CLI:Switch Connection	default
	CLI:Delete Users	grumpy
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Delete Users	grumpy

Test track system information=yes
	${STATUS}=	Run Keyword And Return Status	SUITE:ADD USER AND ADD TO GROUP
	Run Keyword If	'${STATUS}' == 'False'	CLI:Cancel	Raw

	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	track_system_information	yes
	CLI:Commit
	CLI:Open	grumpy	grumpy	test-alias
	CLI:Enter Path	/system/
	CLI:Test Ls Command	event_list	routing_table	system_usage	discovery_logs	serial_statistics
	...	serial_ports_summary	lldp	network_statistics	usb_devices
	Close Connection
	CLI:Switch Connection	default
	CLI:Delete Users	grumpy
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Delete Users	grumpy

Test track system information=no
	${STATUS}=	Run Keyword And Return Status	SUITE:ADD USER AND ADD TO GROUP
	Run Keyword If	'${STATUS}' == 'False'	CLI:Cancel	Raw

	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	track_system_information	no
	CLI:Commit
	CLI:Open	grumpy	grumpy	test-alias
	CLI:Enter Path	system
	${STATUS}=	Run Keyword And Return Status	CLI:Test Ls Command	event_list routing_table	system_usage	discovery_logs	serial_statistics
	Run Keyword If	${STATUS}	Fail	Should Not Contain System Track Information
	Close Connection
	CLI:Switch Connection	default
	CLI:Delete Users	grumpy
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Delete Users	grumpy

Test software upgrade and reboot=yes
	${STATUS}=	Run Keyword And Return Status	SUITE:ADD USER AND ADD TO GROUP
	Run Keyword If	'${STATUS}' == 'False'	CLI:Cancel	Raw

	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	software_upgrade_and_reboot_system	yes
	CLI:Commit
	CLI:Open	grumpy	grumpy	test-alias
	CLI:Enter Path	/system/toolkit
	CLI:Test Available Commands	reboot	shutdown	software_upgrade
	Close Connection
	CLI:Switch Connection	default
	CLI:Delete Users	grumpy
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Delete Users	grumpy

Test software upgrade and reboot=no
	${STATUS}=	Run Keyword And Return Status	SUITE:ADD USER AND ADD TO GROUP
	Run Keyword If	'${STATUS}' == 'False'	CLI:Cancel	Raw
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	software_upgrade_and_reboot_system	no
	CLI:Set Field	track_system_information	yes
	CLI:Commit
	CLI:Open	grumpy	grumpy	test-alias
	CLI:Enter Path	system/
	${STATUS}=	Run Keyword And Return Status	CLI:Test Ls Command	toolkit
	Run Keyword If	${STATUS}	Fail	Should Not Contain Toolkit
	Close Connection
	CLI:Switch Connection	default
	CLI:Delete Users	grumpy
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Delete Users	grumpy

Test terminate sessions=yes
	${STATUS}=	Run Keyword And Return Status	SUITE:ADD USER AND ADD TO GROUP
	Run Keyword If	'${STATUS}' == 'False'	CLI:Cancel	Raw

	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	terminate_sessions	yes
	CLI:Commit
	CLI:Open	grumpy	grumpy	test-alias
	CLI:Enter Path	/system/
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Ls Command	open_sessions	device_sessions
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Ls Command	cluster_peers	open_sessions	device_sessions
	Close Connection
	CLI:Switch Connection	default
	CLI:Delete Users	grumpy
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Delete Users	grumpy

Test terminate sessions=no
	${STATUS}=	Run Keyword And Return Status	SUITE:ADD USER AND ADD TO GROUP
	Run Keyword If	'${STATUS}' == 'False'	CLI:Cancel	Raw
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	terminate_sessions	no
	CLI:Commit
	CLI:Open	grumpy	grumpy	test-alias
	CLI:Enter Path	/system/
	${STATUS}=	Run Keyword If	'${NGVERSION}' == '3.2'	Run Keyword and Return Status	CLI:Test Ls Command	open_sessions	device_sessions
	...	ELSE	Run Keyword and Return Status	CLI:Test Ls Command	cluster_peers	open_sessions	device_sessions
	Run Keyword If	${STATUS}	Fail	Should Not Contain Sessions
	Close Connection
	CLI:Switch Connection	default
	CLI:Delete Users	grumpy
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Delete Users	grumpy

Test configure system=yes
	Run Keyword If 	'${NGVERSION}' == '3.2' 	Set Tags 	NON-CRITICAL		#Not in v3.2
	${STATUS}=	Run Keyword And Return Status	SUITE:ADD USER AND ADD TO GROUP
	Run Keyword If	'${STATUS}' == 'False'	CLI:Cancel	Raw

	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	configure_system	yes
	CLI:Commit
	CLI:Test Show Command	configure_system = yes
	CLI:Open	grumpy	grumpy	test-alias
	CLI:Enter Path	/settings/
	CLI:Test Ls Command	license/	system_preferences/	custom_fields/	system_logging/	date_and_time/	dial_up/
	...	devices/	types/	auto_discovery/	power_menu/	devices_session_preferences/	network_settings/
	...	network_connections/	static_routes/	hosts/	snmp/	dhcp_server/	authentication/	ipv4_firewall/
	...	ipv6_firewall/	ssl_vpn/	services/	auditing/
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Ls Command	cloud/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Ls Command	cluster/
	${RESULT}=	CLI:Ls
	Should Not Contain	${RESULT}	authorization/	local_accounts/

	#Bug 1086
	CLI:Enter Path	/settings/services/
	${RES}=	Run Keyword And Return Status	CLI:Set Field	enable_ftp_service	yes
	Run Keyword if	${RES}	Run Keywords	CLI:Set Field	enable_ftp_service	no	AND	CLI:Commit
	...	ELSE	Fail	Should Not Show Error

	Close Connection
	CLI:Switch Connection	default
	CLI:Delete Users	grumpy
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Delete Users	grumpy

Test configure system=no
	${STATUS}=	Run Keyword And Return Status	SUITE:ADD USER AND ADD TO GROUP
	Run Keyword If	'${STATUS}' == 'False'	CLI:Cancel	Raw

	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	configure_system	no
	CLI:Commit
	CLI:Open	grumpy	grumpy	test-alias
	${STATUS}=	Run Keyword And Return Status	CLI:Test Ls Command	settings
	Run Keyword If	${STATUS}	Fail	Should Not Contain Settings
	Close Connection
	CLI:Switch Connection	default
	CLI:Delete Users	grumpy
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Delete Users	grumpy

Test configure user account=yes
	${STATUS}=	Run Keyword And Return Status	SUITE:ADD USER AND ADD TO GROUP
	Run Keyword If	'${STATUS}' == 'False'	CLI:Cancel	Raw

	CLI:Add User	slumpy	slumpy
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	configure_system	no
	${OUTPUT}=	CLI:Write	set configure_user_accounts=yes	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${OUTPUT}	Warning: In order to enable [Configure User Accounts], [Configure System Settings] must be enabled too.
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	Error: In order to enable [Configure User Accounts], [Configure System Settings] must be enabled too.
	CLI:Set Field	configure_system	yes
	CLI:Set Field	configure_user_accounts	yes
	CLI:Commit
	CLI:Open	grumpy	grumpy	test-alias
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/members
	CLI:Add
	CLI:Set Field	local_users	slumpy
	CLI:Commit
	CLI:Test Show Command	slumpy
	CLI:Delete Users	slumpy
	Close Connection
	CLI:Switch Connection	default
	CLI:Delete Users	grumpy
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Delete Users	grumpy

Test configure user account=no
	${STATUS}=	Run Keyword And Return Status	SUITE:ADD USER AND ADD TO GROUP
	Run Keyword If	'${STATUS}' == 'False'	CLI:Cancel	Raw
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	configure_system	yes
	CLI:Set Field	configure_user_accounts	no
	CLI:Commit
	CLI:Open	grumpy	grumpy	test-alias
	CLI:Enter Path	settings
	${STATUS}=	Run Keyword And Return Status	CLI:Test Ls Command	authorization
	Run Keyword If	${STATUS}	Fail	Should Not Contain Authorization
	Close Connection
	CLI:Switch Connection	default
	CLI:Delete Users	grumpy
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Delete Users	grumpy

Test apply and save settings=yes
	${STATUS}=	Run Keyword And Return Status	SUITE:ADD USER AND ADD TO GROUP
	Run Keyword If	'${STATUS}' == 'False'	CLI:Cancel	Raw

	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	apply_&_save_settings	yes
	CLI:Commit
	CLI:Open	grumpy	grumpy	test-alias
	CLI:Enter Path	/system/toolkit
	CLI:Test Available Commands	save_settings	apply_settings
	Close Connection
	CLI:Switch Connection	default
	CLI:Delete Users	grumpy
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Delete Users	grumpy

Test apply and save settings=no
	${STATUS}=	Run Keyword And Return Status	SUITE:ADD USER AND ADD TO GROUP
	Run Keyword If	'${STATUS}' == 'False'	CLI:Cancel	Raw

	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	apply_&_save_settings	no
	CLI:Commit
	CLI:Open	grumpy	grumpy	test-alias
	${STATUS}=	Run Keyword And Return Status	CLI:Test Ls Command	toolkit
	Run Keyword If	${STATUS}	Fail	Should Not Contain Toolkit
	Close Connection
	CLI:Switch Connection	default
	CLI:Delete Users	grumpy
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Delete Users	grumpy

Test Restrict Configure System Permission to Read Only
	${STATUS}=	Run Keyword And Return Status	SUITE:ADD USER AND ADD TO GROUP
	Run Keyword If	'${STATUS}' == 'False'	CLI:Cancel	Raw

	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	configure_system	yes
	CLI:Set Field	restrict_configure_system_permission_to_read_only	yes
	CLI:Commit
	CLI:Open	grumpy	grumpy	test-alias
	CLI:Enter Path	/settings/
	CLI:Test Ls Command	license/	system_preferences/	custom_fields/	system_logging/	date_and_time/	dial_up/
	...	devices/	types/	auto_discovery/	power_menu/	devices_session_preferences/	network_settings/
	...	network_connections/	static_routes/	hosts/	snmp/	dhcp_server/	authentication/	ipv4_firewall/
	...	ipv6_firewall/	ssl_vpn/	services/	auditing/
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Ls Command	cloud/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Ls Command	cluster/
	CLI:Enter Path	/settings/services/
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Available Commands	cd	hostname	show	change_password
	...	ls	whoami	event_system_audit	pwd	exit	quit
	CLI:Test Unavailable Commands	${SPACE}set${SPACE}
	Close Connection
	CLI:Switch Connection	default
	CLI:Delete Users	grumpy
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Delete Users	grumpy

Test menu-driven access
	${STATUS}=	Run Keyword And Return Status	SUITE:ADD USER AND ADD TO GROUP
	Run Keyword If	'${STATUS}' == 'False'	CLI:Cancel	Raw

	CLI:Enter Path	/settings/devices/
	CLI:Delete If Exists Confirm	grumpy_self
	CLI:Add Device	grumpy_self	device_console	127.0.0.1
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/devices
	CLI:Add
	CLI:Set Field	devices	grumpy_self
	Write Bare	commit\n
	Run Keyword If	'${NGVERSION}' >= '4.2'	Read Until	:
	Run Keyword If	'${NGVERSION}' >= '4.2'	Write Bare	yes\n
	CLI:Read Until Prompt
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	menu-driven_access_to_devices	yes
	CLI:Commit
	Set Default Configuration	loglevel=INFO
	Set Default Configuration	prompt=Menu-driven Access
	Set Default Configuration	newline=\n
	Set Default Configuration	width=400
	Set Default Configuration	height=600
	Set Default Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Open Connection	${HOST}	alias=test-alias
	Login	grumpy	grumpy
	IF	${NGVERSION} == 3.2
		${OUTPUT}=	Read Until	?
	ELSE
		${OUTPUT}=	Read Until	Enter an action:
	END
	Run Keyword And Continue On Failure	Should Contain	${OUTPUT}	grumpy_self
	Set Default Configuration	prompt=]#
	Close Connection
	CLI:Switch Connection	default
	CLI:Delete Users	grumpy
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Delete Users	grumpy

Test menu-driven access precedence over startup_application
	Skip If	'${NGVERSION}' == '3.2'	Feature only in 4.2+
	${STATUS}=	Run Keyword And Return Status	SUITE:ADD USER AND ADD TO GROUP
	Run Keyword If	'${STATUS}' == 'False'	CLI:Cancel	Raw
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	startup_application	shell
	CLI:Set	menu-driven_access_to_devices=yes
	CLI:Commit
	Set Default Configuration	loglevel=INFO
	Set Default Configuration	prompt=Menu-driven Access
	Set Default Configuration	newline=\n
	Set Default Configuration	width=400
	Set Default Configuration	height=600
	Set Default Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Open Connection	${HOST}	alias=test-alias
	Login	grumpy	grumpy
	IF	${NGVERSION} == 3.2
		${OUTPUT}=	Read Until	?
	ELSE
		${OUTPUT}=	Read Until	Enter an action:
	END
	Run Keyword And Continue On Failure	Should Contain	${OUTPUT}	grumpy_self
	Set Default Configuration	prompt=]#
	Close Connection
	CLI:Switch Connection	default
	CLI:Delete Device	grumpy_self
	CLI:Delete Users	grumpy
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	menu-driven_access_to_devices	no
	CLI:Commit
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Delete If Exists Confirm	grumpy_self	AND	CLI:Delete Users	grumpy

Test startup_application=shell
	Skip If	'${NGVERSION}' == '3.2'	Feature only in 4.2+
	${STATUS}=	Run Keyword And Return Status	SUITE:ADD USER AND ADD TO GROUP
	Run Keyword If	'${STATUS}' == 'False'	CLI:Cancel	Raw
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	startup_application	shell
	CLI:Commit

	Set Default Configuration	prompt=WARNING:
	Set Default Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Open Connection	${HOST}	alias=test-alias
	Login	grumpy	grumpy
	${SHELL_OUTPUT}=	Read Until	~$
	Should Contain	${SHELL_OUTPUT}	grumpy@nodegrid:
	Write	ls /
	${SHELL_OUT}=	Read Until	~$
	Should Contain	${SHELL_OUT}	backup	boot	etc	lib
	Set Default Configuration	prompt=]#
	Write	cli
	${CLI_OUTPUT}=	Read Until	]#
	Should Contain	${CLI_OUTPUT}	[grumpy@nodegrid
	Write	ls /
	${CLI_OUT}=	Read Until	]#
	Should Contain	${CLI_OUT}	access/	system/
	Close Connection
	CLI:Switch Connection	default
	CLI:Delete Users	grumpy

Test startup_application=cli
	Skip If	'${NGVERSION}' == '3.2'	Feature only in 4.2+
	${STATUS}=	Run Keyword And Return Status	SUITE:ADD USER AND ADD TO GROUP
	Run Keyword If	'${STATUS}' == 'False'	CLI:Cancel	Raw
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	startup_application	cli
	CLI:Commit

	Set Default Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Open Connection	${HOST}	alias=test-alias
	Login	grumpy	grumpy
	Write	ls /
	${CLI_OUT}=	Read Until	]#
	Should Contain	${CLI_OUT}	access/	system/
	Close Connection
	CLI:Switch Connection	default
	CLI:Delete Users	grumpy

Test ssh-copy-id feature with startup_application=shell
	Skip If	'${NGVERSION}' == '3.2'	Feature only in 4.2+
	${STATUS}=	Run Keyword And Return Status	SUITE:ADD USER AND ADD TO GROUP
	Run Keyword If	'${STATUS}' == 'False'	CLI:Cancel	Raw
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Set Field	startup_application	shell
	CLI:Commit

	Write	shell
	${SHELL_OUTPUT}=	Read Until	~$
	Should Contain	${SHELL_OUTPUT}	${DEFAULT_USERNAME}@nodegrid:
	Log To Console	${SHELL_OUTPUT}

	Write	rm /home/${DEFAULT_USERNAME}/.ssh/id_rsa
	Read Until	~$
	#Gets rid of WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED! msg
	Write	ssh-keygen -f "/home/${DEFAULT_USERNAME}/.ssh/known_hosts" -R ${HOST}
	Read Until	~$
	Write	ssh-keygen
	# Enter file in which to save the key (/home/admin/.ssh/id_rsa):
	Read Until	:
	Write	\n
	# Enter passphrase (empty for no passphrase):
	Read Until	:
	Write	\n
	# Enter same passphrase again:
	Read Until	:
	Write	\n
	${KEY_OUTPUT}=	Read Until	~$
	Should Contain	${KEY_OUTPUT}	Your public key has been saved in /home/${DEFAULT_USERNAME}/.ssh/id_rsa.pub
	Log To Console	${KEY_OUTPUT}
	Write	ssh-copy-id grumpy@${HOST}
	# ECDSA key fingerprint is SHA256:/2zBg2c/gj5v79IVbnYdZpcTuBureLCUxKbKsn66HUM.
	# Are you sure you want to continue connecting (yes/no)?
	${ST}=	Run Keyword And Return Status	Read Until	(yes/no/[fingerprint])?
	Run Keyword If	${ST}	Write	yes
	Run Keyword If	${ST}	Read Until	Password:
	Write	grumpy
	${KEY_OUTPUT}=	Read Until	~$
	Should Contain	${KEY_OUTPUT}	Number of key(s) added: 1

	Write	ssh grumpy@${HOST}
	${SHELL_OUTPUT}=	Read Until	~$
	Should Not Contain	${SHELL_OUTPUT}	Password:
	Should Contain	${SHELL_OUTPUT}	grumpy@nodegrid:
	Write	exit
	${ADMIN_SHELL}=	Read Until	~$
	Should Contain	${ADMIN_SHELL}	Connection to ${HOST} closed.
	Write	exit
	CLI:Read Until Prompt
	[Teardown]	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Close Current Connection

*** Keywords ***
SUITE:OPEN
	CLI:Open
	CLI:Enter Path	/settings/authorization/
	@{GROUPS}=	CLI:Get All User Groups
	FOR	${GROUP}	IN	@{GROUPS}
		Run Keyword Unless	"${GROUP}" == "${DEFAULT_USERNAME}" or "${GROUP}" == "user"	CLI:Delete Confirm	${GROUP}
	END
	CLI:Enter Path	/settings/authorization/
	CLI:Add
	Write	set name=${GROUP_NAME}
	CLI:Read Until Prompt
	CLI:Commit
	CLI:Test Show Command	${GROUP_NAME}

SUITE:TEARDOWN
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Open
	CLI:Enter Path	/settings/authorization/
	@{GROUPS}=	CLI:Get All User Groups
	FOR	${GROUP}	IN	@{GROUPS}
		Run Keyword Unless	"${GROUP}" == "${DEFAULT_USERNAME}" or "${GROUP}" == "user"	CLI:Delete Confirm	${GROUP}
	END
	CLI:Delete Users	grumpy
	CLI:Delete Devices	grumpy_self
	CLI:Close Connection

SUITE:ADD USER AND ADD TO GROUP
	CLI:Enter Path	/settings/local_accounts/
	CLI:Delete If Exists Confirm	grumpy
	CLI:Add
	CLI:Set Field	username	grumpy
	CLI:Set Field	password	grumpy
	CLI:Set Field	user_group	${GROUP_NAME}
	CLI:Commit
	CLI:Test Show Command Regexp	grumpy\\s+Unlocked\\s+${GROUP_NAME}