*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authorization... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${GROUP_NAME}	test_another_group

*** Test Cases ***
Test add group
	CLI:Enter Path	/settings/authorization/
	CLI:Add
	CLI:Set	name=${GROUP_NAME}
	CLI:Commit
	CLI:Test Show Command	${GROUP_NAME}

Test available directories
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}
	CLI:Test Ls Command	members	profile	remote_groups	devices	outlets

Test show profile
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Test Show Command	track_system_information	terminate_sessions	software_upgrade_and_reboot_system
	...	configure_system	configure_user_accounts	apply_&_save_settings	shell_access
	...	restrict_configure_system_permission_to_read_only	menu-driven_access_to_devices	custom_session_timeout	email_events_to
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Show Command	startup_application

Test profile available fields to set
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Test Set Available Fields	track_system_information	terminate_sessions	software_upgrade_and_reboot_system
	...	configure_system	configure_user_accounts	apply_&_save_settings	shell_access
	...	restrict_configure_system_permission_to_read_only	menu-driven_access_to_devices	custom_session_timeout	email_events_to
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Available Fields	startup_application

Test set command options for profile page
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	CLI:Test Set Field Options	track_system_information	yes	no
	CLI:Test Set Field Options	terminate_sessions	yes	no
	CLI:Test Set Field Options	software_upgrade_and_reboot_system	yes	no
	CLI:Test Set Field Options	configure_system	yes	no
	CLI:Test Set Field Options	apply_&_save_settings	yes	no
	CLI:Test Set Field Options	shell_access	yes	no
	CLI:Test Set Field Options	menu-driven_access_to_devices	no	yes
	CLI:Test Set Field Options	custom_session_timeout	no	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Options	startup_application	shell	cli
	CLI:Set	configure_system=yes
	CLI:Commit
	CLI:Test Set Field Options	configure_user_accounts	yes	no
	CLI:Test Set Field Options	restrict_configure_system_permission_to_read_only	yes	no
	CLI:Set	configure_system=no
	CLI:Commit

Test invalid values for fields for profile page
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	${COMMANDS}=	Create List	track_system_information	terminate_sessions	software_upgrade_and_reboot_system
	...	configure_system	configure_user_accounts	apply_&_save_settings	shell_access	restrict_configure_system_permission_to_read_only
	...	menu-driven_access_to_devices	custom_session_timeout
	Run Keyword If	'${NGVERSION}' >= '4.2'	Append To List	${COMMANDS}	startup_application
	FOR	${COMMAND}	IN	@{COMMANDS}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value: ${COMMAND}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}
	END

Test fields that need configure_system
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/profile
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	configure_user_accounts	yes	Error: In order to enable [Configure User Accounts], [Configure System Settings] must be enabled too.
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	restrict_configure_system_permission_to_read_only	yes	Error: In order to enable this restriction, [Configure System Settings] must be enabled too
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	configure_user_accounts	yes	Warning: In order to enable [Configure User Accounts], [Configure System Settings] must be enabled too.
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	restrict_configure_system_permission_to_read_only	yes	Error: restrict_configure_system_permission_to_read_only: In order to enable this restriction, [Configure System Settings] must be enabled too.

Test show command in outlets directory
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/outlets
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Show Command	device	pdu id	outlet id	power mode
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Show Command	outlet	device	pdu id	outlet id	power mode

Test outlets field=outlets
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/outlets
	CLI:Add
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	outlets	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: outlets
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	outlets	${EMPTY}	Error: Invalid value: null
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	outlets	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}
	CLI:Cancel

Test outlets field=power
	CLI:Enter Path	/settings/authorization/${GROUP_NAME}/outlets
	CLI:Test Set Available Fields	outlets	power
	CLI:Add
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	power	${EMPTY}	Error: Missing value for parameter: power
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	power	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: power
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	power	${EMPTY}	Error: Missing value: power
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	power	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	CLI:Test Set Field Options	power	no_access	power_control	power_status
	CLI:Cancel

Test delete group and user
	CLI:Enter Path	/settings/authorization/
	CLI:Delete Confirm	${GROUP_NAME}
	${GROUPS}=	CLI:Ls
	Should Not Contain	${GROUPS}	${GROUP_NAME}
	CLI:Enter Path	/settings/local_accounts/

Test deleting protected group
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/settings/authorization/
	Write	delete ${DEFAULT_USERNAME}
	${DELETE_QUESTION}=	Read Until	:
	Log To Console	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\ndelete ${DEFAULT_USERNAME}
	Log To Console	\n${DELETE_QUESTION}
	${OUTPUT}=	CLI:Write	yes	Raw
	Should Contain	${OUTPUT}	Error: Warning: Protected groups cannot be deleted

Test changing values for admin group system permissions
	CLI:Enter Path	/settings/authorization/${DEFAULT_USERNAME}/profile
	CLI:Set	track_system_information=no
	CLI:Commit
	CLI:Test Show Command	track_system_information = yes

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/authorization/
	@{GROUPS}=	CLI:Get All User Groups
	FOR	${GROUP}	IN	@{GROUPS}
		Run Keyword Unless	"${GROUP}" == "${DEFAULT_USERNAME}" or "${GROUP}" == "user"	CLI:Delete Confirm	${GROUP}
	END

SUITE:Teardown
	CLI:Close Connection