*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Authorization... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:OPEN
Suite Teardown	CLI:Close Connection

*** Variables ***
${GROUP_NAME}	another_group

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/authorization/
	CLI:Test Available Commands	add	commit	event_system_clear	ls	reboot	shell	shutdown	cd	delete	exit	pwd
	...	revert	show	whoami	change_password	event_system_audit	hostname	quit	set	show_settings

Test show groups
	CLI:Enter Path	/settings/authorization/
	CLI:Delete If Exists Confirm	another_group
	CLI:Test Show Command Regexp	\\s+name\\s+=+\\s+${DEFAULT_USERNAME}\\s+user

Test validation for adding new group
	Skip If	'${NGVERSION}' == '3.2'	Error message not implemented in 3.2
	CLI:Enter Path	/settings/authorization/
	CLI:Add
	CLI:Test Set Field Invalid Options	name	${EMPTY}	Error: name: Field must not be empty.	yes
	CLI:Test Set Field Invalid Options	name	${POINTS}	Error: name: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	CLI:Test Set Field Invalid Options	name	${EXCEEDED}	Error: name: This field can be empty or should contain only numbers, letters, - and _, and also it should start with letter or _	yes
	[Teardown]	CLI:Cancel	Raw

Test duplicate name entry
	Skip If	'${NGVERSION}' == '3.2'	Error handling won't be fixed in 3.2
	CLI:Enter Path	/settings/authorization/
	CLI:Add
	CLI:Test Set Field Invalid Options	name	${DEFAULT_USERNAME}	Error: name: Entry already exists.	yes
	CLI:Cancel

Test set field for adding group
	CLI:Enter Path	/settings/authorization/
	CLI:Add
	CLI:Test Set Available Fields	name
	CLI:Cancel

Test show_settings command
	CLI:Enter Path	/settings/authorization/
	Write	show_settings
	${OUTPUT}=	CLI:Read Until Prompt
	#Run Keyword If	'${NGVERSION}' >= '4.2'	Should Match Regexp	${OUTPUT}	/settings/authorization/${DEFAULT_USERNAME}/members members=${DEFAULT_USERNAME},
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Match Regexp	${OUTPUT}	/settings/authorization/${DEFAULT_USERNAME}/members  memberRemote=${DEFAULT_USERNAME},
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Match Regexp	${OUTPUT}	/settings/authorization/${DEFAULT_USERNAME}/members member=${DEFAULT_USERNAME},
	Should Match Regexp	${OUTPUT}	/settings/authorization/${DEFAULT_USERNAME}/profile track_system_information=yes|no
	Should Match Regexp	${OUTPUT}	/settings/authorization/${DEFAULT_USERNAME}/profile terminate_sessions=yes|no
	Should Match Regexp	${OUTPUT}	/settings/authorization/${DEFAULT_USERNAME}/profile software_upgrade_and_reboot_system=yes|no
	Should Match Regexp	${OUTPUT}	/settings/authorization/${DEFAULT_USERNAME}/profile configure_system=yes|no
	Should Match Regexp	${OUTPUT}	/settings/authorization/${DEFAULT_USERNAME}/profile configure_user_accounts=yes|no
	Should Match Regexp	${OUTPUT}	/settings/authorization/${DEFAULT_USERNAME}/profile apply_&_save_settings=yes|no
	Should Match Regexp	${OUTPUT}	/settings/authorization/${DEFAULT_USERNAME}/profile shell_access=yes|no
	Should Match Regexp	${OUTPUT}	/settings/authorization/${DEFAULT_USERNAME}/profile restrict_configure_system_permission_to_read_only=no|yes
	Should Match Regexp	${OUTPUT}	/settings/authorization/${DEFAULT_USERNAME}/profile menu-driven_access_to_devices=no|yes
	Should Match Regexp	${OUTPUT}	/settings/authorization/${DEFAULT_USERNAME}/profile custom_session_timeout=no|yes
	Should Match Regexp	${OUTPUT}	/settings/authorization/user/profile track_system_information=no|yes
	Should Match Regexp	${OUTPUT}	/settings/authorization/user/profile terminate_sessions=no|yes
	Should Match Regexp	${OUTPUT}	/settings/authorization/user/profile software_upgrade_and_reboot_system=no|yes
	Should Match Regexp	${OUTPUT}	/settings/authorization/user/profile configure_system=no|yes
	Should Match Regexp	${OUTPUT}	/settings/authorization/user/profile configure_user_accounts=no|yes
	Should Match Regexp	${OUTPUT}	/settings/authorization/user/profile apply_&_save_settings=no|yes
	Should Match Regexp	${OUTPUT}	/settings/authorization/user/profile shell_access=no|yes
	Should Match Regexp	${OUTPUT}	/settings/authorization/user/profile restrict_configure_system_permission_to_read_only=no|yes
	Should Match Regexp	${OUTPUT}	/settings/authorization/user/profile menu-driven_access_to_devices=no|yes
	Should Match Regexp	${OUTPUT}	/settings/authorization/user/profile custom_session_timeout=no|yes
	Run Keyword If	'${NGVERSION}' > '4.2'	Should Match Regexp	${OUTPUT}	/settings/authorization/user/profile startup_application=cli|shell

#Admin group has all the system permissions set to yes
Test right values for admin group profile
	CLI:Enter Path	/settings/authorization/${DEFAULT_USERNAME}/profile
	CLI:Test Show Command	track_system_information = yes	terminate_sessions = yes	software_upgrade_and_reboot_system = yes
	...	configure_system = yes	configure_user_accounts = yes	apply_&_save_settings = yes	shell_access = yes

Test admin group's devices directory
	CLI:Enter Path	/settings/authorization/${DEFAULT_USERNAME}/devices
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Show Command Regexp	ALL\\s+read/write\\s+yes\\s+yes\\s+control\\s+yes\\s+yes\\s+audit/clear\\s+audit/clear\\s+yes\\s+yes\\s+yes\\s+yes\\s+yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Show Command Regexp	ALL\\s+read/write\\s+-\\s+-\\s+control\\s+yes\\s+-\\s+audit/clear\\s+audit/clear\\s+yes\\s+yes\\s+-\\s+-
	${OUTPUT}=	CLI:Write	add	Yes
	Should Contain	${OUTPUT}	Error: Invalid command/path

Test admin group's outlets directory
	CLI:Enter Path	/settings/authorization/${DEFAULT_USERNAME}/outlets
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Show Command Regexp	ALL\\s+ALL\\s+Control
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Show Command Regexp	ALL\\s+ALL\\s+control
	${OUTPUT}=	CLI:Write	add	Yes
	Should Contain	${OUTPUT}	Error: Invalid command/path

*** Keywords ***
SUITE:OPEN
	CLI:Open
	CLI:Enter Path	/settings/authorization/
	@{GROUPS}=	CLI:Get All User Groups
	FOR	${GROUP}	IN	@{GROUPS}
		Run Keyword Unless	"${GROUP}" == "${DEFAULT_USERNAME}" or "${GROUP}" == "user"	CLI:Delete Confirm	${GROUP}
	END