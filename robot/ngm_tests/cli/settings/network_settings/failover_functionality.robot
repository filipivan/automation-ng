*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Cellular failover feature through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	NON-CRITICAL	EXCLUDEIN3_2	REVIEWED
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CELLULAR_CONNECTION}	${GSM_CELLULAR_CONNECTION}
${GSM_TYPE}	${GSM_TYPE}
${STRONG_PASSWORD}	${QA_PASSWORD}
${TRIGGER_IP_ADDRESS}	1.2.3.5
${ETH0_CONNECTION}	eth0_connection
${ETH0_TYPE}	ethernet
${ETH0_ETHERNET_INTERFACE}	eth0
${ETH1_CONNECTION}	eth1_connection
${ETH1_ETHERNET_INTERFACE}	eth1

*** Test Cases ***
Cellular Failover and Dynamic DNS test - ETH0 and SIM 1 - Failover Triggered
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CELLULAR_CONNECTION} type=${GSM_TYPE} ethernet_interface=${MODEM_INFO["interface"]} ipv4_mode=dhcp ipv6_mode=no_ipv6_address sim-1_access_point_name=${MODEM_INFO["apn"]} enable_second_sim_card=yes active_sim_card=1 sim-2_access_point_name=${MODEM_INFO["apn"]}
	CLI:Commit
	CLI:Add
	CLI:Set	name=${ETH0_CONNECTION} type=${ETH0_TYPE} ethernet_interface=${ETH0_ETHERNET_INTERFACE} ipv4_mode=dhcp ipv6_mode=no_ipv6_address
	CLI:Commit
	CLI:Write	up_connection ${ETH0_CONNECTION}
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	enable_network_failover=yes primary_connection=${ETH0_CONNECTION} secondary_connection=${CELLULAR_CONNECTION} secondary_connection_sim_card=1 enable_primary_failover_by_ip_address=yes trigger=ip_address trigger_ip_address=${TRIGGER_IP_ADDRESS}
	CLI:Commit
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Connection	${CELLULAR_CONNECTION}	mobile broadband gsm	${MODEM_INFO["interface"]}
	CLI:Enter Path	/settings/network_connections/${CELLULAR_CONNECTION}
	${OUTPUT}=	CLI:Show
	Should Match Regexp	${OUTPUT}	\\s+enable_second_sim_card = yes\\s+active_sim_card = 1\\s+
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	SUITE:Disable Network Failover
	...	AND	SUITE:Delete Network Connections

Cellular Failover and Dynamic DNS test - ETH0 and SIM 2 - Failover Triggered
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CELLULAR_CONNECTION} type=${GSM_TYPE} ethernet_interface=${MODEM_INFO["interface"]} ipv4_mode=dhcp ipv6_mode=no_ipv6_address sim-1_access_point_name=${MODEM_INFO["apn"]} enable_second_sim_card=yes active_sim_card=2 sim-2_access_point_name=${MODEM_INFO["apn"]}
	CLI:Commit
	CLI:Add
	CLI:Set	name=${ETH0_CONNECTION} type=${ETH0_TYPE} ethernet_interface=${ETH0_ETHERNET_INTERFACE} ipv4_mode=dhcp ipv6_mode=no_ipv6_address
	CLI:Commit
	CLI:Write	up_connection ${ETH0_CONNECTION}
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	enable_network_failover=yes primary_connection=${ETH0_CONNECTION} secondary_connection=${CELLULAR_CONNECTION} secondary_connection_sim_card=2 enable_primary_failover_by_ip_address=yes trigger=ip_address trigger_ip_address=${TRIGGER_IP_ADDRESS}
	CLI:Commit
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Connection	${CELLULAR_CONNECTION}	mobile broadband gsm	${MODEM_INFO["interface"]}
	CLI:Enter Path	/settings/network_connections/${CELLULAR_CONNECTION}
	${OUTPUT}=	CLI:Show
	Should Match Regexp	${OUTPUT}	\\s+enable_second_sim_card = yes\\s+active_sim_card = 2\\s+
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	SUITE:Disable Network Failover
	...	AND	SUITE:Delete Network Connections

Network Failover and Dynamic DNS test - ETH0 and ETH1 - Failover Triggered
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	SUITE:Check If Eth1 Carrier State Up
	${HAS_ETH1}=	CLI:Has ETH1
	Skip If	not ${HAS_ETH1}	eth1 not avaliable
	Skip If	not ${ETH1_STATE}	eth1 not connected
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${ETH0_CONNECTION} type=${ETH0_TYPE} ethernet_interface=${ETH0_ETHERNET_INTERFACE} ipv4_mode=dhcp ipv6_mode=no_ipv6_address
	CLI:Commit
	CLI:Write	up_connection ${ETH0_CONNECTION}
	CLI:Add
	CLI:Set	name=${ETH1_CONNECTION} type=${ETH0_TYPE} ethernet_interface=${ETH1_ETHERNET_INTERFACE} ipv4_mode=dhcp ipv6_mode=no_ipv6_address
	CLI:Commit
	CLI:Write	up_connection ${ETH1_CONNECTION}
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	enable_network_failover=yes primary_connection=${ETH0_CONNECTION} secondary_connection=${ETH1_CONNECTION} trigger=ip_address trigger_ip_address=${TRIGGER_IP_ADDRESS}
	CLI:Commit
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Connection		${ETH1_CONNECTION}	${ETH0_TYPE}	${ETH1_ETHERNET_INTERFACE}
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	SUITE:Disable Network Failover
	...	AND	SUITE:Delete Network Connections

Cellular Failover and Dynamic DNS test - ETH0 and SIM 1 - Failover Not Triggered
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CELLULAR_CONNECTION} type=${GSM_TYPE} ethernet_interface=${MODEM_INFO["interface"]} ipv4_mode=dhcp ipv6_mode=no_ipv6_address sim-1_access_point_name=${MODEM_INFO["apn"]} enable_second_sim_card=yes active_sim_card=1 sim-2_access_point_name=${MODEM_INFO["apn"]}
	CLI:Commit
	CLI:Add
	CLI:Set	name=${ETH0_CONNECTION} type=${ETH0_TYPE} ethernet_interface=${ETH0_ETHERNET_INTERFACE} ipv4_mode=dhcp ipv6_mode=no_ipv6_address
	CLI:Commit
	CLI:Write	up_connection ${ETH0_CONNECTION}
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	enable_network_failover=yes primary_connection=${ETH0_CONNECTION} secondary_connection=${CELLULAR_CONNECTION} secondary_connection_sim_card=1 enable_primary_failover_by_ip_address=yes trigger=ip_address trigger_ip_address=${HOST}
	CLI:Commit
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Connection	${ETH0_CONNECTION}	${ETH0_TYPE}	${ETH0_ETHERNET_INTERFACE}
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	SUITE:Disable Network Failover
	...	AND	SUITE:Delete Network Connections

Cellular Failover and Dynamic DNS test - ETH0 and SIM 2 - Failover Not Triggered
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CELLULAR_CONNECTION} type=${GSM_TYPE} ethernet_interface=${MODEM_INFO["interface"]} ipv4_mode=dhcp ipv6_mode=no_ipv6_address sim-1_access_point_name=${MODEM_INFO["apn"]} enable_second_sim_card=yes active_sim_card=2 sim-2_access_point_name=${MODEM_INFO["apn"]}
	CLI:Commit
	CLI:Add
	CLI:Set	name=${ETH0_CONNECTION} type=${ETH0_TYPE} ethernet_interface=${ETH0_ETHERNET_INTERFACE} ipv4_mode=dhcp ipv6_mode=no_ipv6_address
	CLI:Commit
	CLI:Write	up_connection ${ETH0_CONNECTION}
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	enable_network_failover=yes primary_connection=${ETH0_CONNECTION} secondary_connection=${CELLULAR_CONNECTION} secondary_connection_sim_card=2 enable_primary_failover_by_ip_address=yes trigger=ip_address trigger_ip_address=${HOST}
	CLI:Commit
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Connection	${ETH0_CONNECTION}	${ETH0_TYPE}	${ETH0_ETHERNET_INTERFACE}
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	SUITE:Disable Network Failover
	...	AND	SUITE:Delete Network Connections

Network Failover and Dynamic DNS test - ETH0 and ETH1 - Failover Not Triggered
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	${HAS_ETH1}=	CLI:Has ETH1
	Skip If	not ${HAS_ETH1}	eth1 not avaliable
	Skip If	not ${ETH1_STATE}	eth1 not connected
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${ETH0_CONNECTION} type=${ETH0_TYPE} ethernet_interface=${ETH0_ETHERNET_INTERFACE} ipv4_mode=dhcp ipv6_mode=no_ipv6_address
	CLI:Commit
	CLI:Write	up_connection ${ETH0_CONNECTION}
	CLI:Add
	CLI:Set	name=${ETH1_CONNECTION} type=${ETH0_TYPE} ethernet_interface=${ETH1_ETHERNET_INTERFACE} ipv4_mode=dhcp ipv6_mode=no_ipv6_address
	CLI:Commit
	CLI:Write	up_connection ${ETH1_CONNECTION}
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	enable_network_failover=yes primary_connection=${ETH0_CONNECTION} secondary_connection=${ETH1_CONNECTION} trigger=ip_address trigger_ip_address=${HOST}
	CLI:Commit
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Connection		${ETH0_CONNECTION}	${ETH0_TYPE}	${ETH0_ETHERNET_INTERFACE}
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	SUITE:Disable Network Failover
	...	AND	SUITE:Delete Network Connections

*** Keywords ***
SUITE:Setup
	CLI:Open
	${HAS_WMODEM_SUPPORT}=	CLI:Has Wireless Modem Support
	Set Suite Variable	${HAS_WMODEM_SUPPORT}
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system

	${HAS_WMODEM}	${MODEM_INFO}=	CLI:Get Wireless Modems SIM Card Info	0
	Set Suite Variable	${HAS_WMODEM}
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	Set Suite Variable	${MODEM_INFO}

	CLI:Change User Password From Shell	root	${STRONG_PASSWORD}	${ROOT_PASSWORD}
	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${STRONG_PASSWORD}	${STRONG_PASSWORD}
	Wait Until Keyword Succeedss	1ms	1s	SUITE:Check Modem Up
	CLI:Switch Connection	default

SUITE:Teardown
	Run Keyword If	${HAS_WMODEM}	Run Keywords
	...	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}	${STRONG_PASSWORD}	AND
	...	CLI:Change User Password From Shell	root	${ROOT_PASSWORD}	${STRONG_PASSWORD}
	CLI:Close Connection

SUITE:Check Modem Up
	Switch Connection	default
	${OUTPUT}=	CLI:Get Wireless Modem Model
	Should Contain	${OUTPUT}	EM7565

SUITE:Delete Network Connections
	CLI:Delete Network Connections	${CELLULAR_CONNECTION}	${ETH0_CONNECTION}	${ETH1_CONNECTION}

SUITE:Disable Network Failover
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	enable_network_failover=no
	CLI:Commit

SUITE:Check Connection
	[Arguments]	${CONNECTION}	${TYPE}	${INTERFACE}
	CLI:Enter Path	/settings/network_connections/
	${OUTPUT}=	CLI:Show
	Should Match Regexp	${OUTPUT}	\\s+${CONNECTION}\\s+connected\\s+${TYPE}\\s+${INTERFACE}\\s+up\\s+\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\/\\d{0,2}\\s+

SUITE:Check If Eth1 Carrier State Up
	${ETH1_STATE}= 		CLI:Is Interface Carrier State Up	eth1
	Set Suite Variable		${ETH1_STATE}