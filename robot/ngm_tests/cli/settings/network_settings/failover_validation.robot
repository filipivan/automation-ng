*** Settings ***
Resource	../../init.robot
Documentation	Testing Failover
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	FAILOVER
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${IP_ADDRESS}	${HOST}
${TRIGGER_FAILED}	3
${TRIGGER_SUCCESSFUL}	3
${INTERVAL_RETRIES}	10
${SECONDARY_TRIGGER_FAILED}	2
${SECONDARY_TRIGGER_SUCCESSFUL}	2
${SECONDARY_INTERVAL_RETRIES}	8
${SERVER_NAME}	www.google.com
${SERVER_PORT}	65
${ZONE}	zone
${FAILOVER_HOSTNAME}	failoverhostname
${USER_NAME}	failover_user_name
@{ALGORITHM}	HMAC-MD5	HMAC-SHA1	HMAC-SHA224	HMAC-SHA256	HMAC-SHA384	HMAC-SHA512
${KEY_SIZE}	512


*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/network_settings/
	CLI:Test Available Commands	event_system_audit	event_system_clear	change_password	ls	reboot	shell	shutdown	commit	exit	pwd	revert	show	whoami	cd	hostname	quit	set	show_settings

Checking values for enable_network_failover
	CLI:Enter Path	/settings/network_settings/
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	enable_network_failover	${EMPTY}	Error: Missing value for parameter: enable_network_failover
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	enable_network_failover	${WORD}  Error: Invalid value: ${WORD} for parameter: enable_network_failover
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	enable_network_failover	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: enable_network_failover
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	enable_network_failover	${POINTS}	Error: Invalid value: ${POINTS} for parameter: enable_network_failover
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	enable_network_failover	${ONE_WORD}  Error: Invalid value: ${ONE_WORD} for parameter: enable_network_failover
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	enable_network_failover	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: enable_network_failover
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	enable_network_failover	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: enable_network_failover

	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	enable_network_failover	${EMPTY}	Error: Missing value: enable_network_failover
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	enable_network_failover	${WORD}  Error: Invalid value: ${WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	enable_network_failover	${NUMBER}	Error: Invalid value: ${NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	enable_network_failover	${POINTS}	Error: Invalid value: ${POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	enable_network_failover	${ONE_WORD}  Error: Invalid value: ${ONE_WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	enable_network_failover	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	enable_network_failover	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	CLI:Test Set Validate Invalid Options	enable_network_failover	no
	CLI:Test Set Validate Invalid Options Multiple Fields	${SETUP_FAILOVER}
	[Teardown]	CLI:Revert

Checking values for primary_settings
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	${SETUP_FAILOVER}
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	primary_connection	${EMPTY}	Error: Missing value for parameter: primary_connection
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	primary_connection	${WORD}  Error: Invalid value: ${WORD} for parameter: primary_connection
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	primary_connection	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: primary_connection
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	primary_connection	${POINTS}	Error: Invalid value: ${POINTS} for parameter: primary_connection
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	primary_connection	${ONE_WORD}  Error: Invalid value: ${ONE_WORD} for parameter: primary_connection
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	primary_connection	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: primary_connection
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	primary_connection	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: primary_connection

	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	primary_connection	${EMPTY}	Error: Missing value: primary_connection
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	primary_connection	${WORD}  Error: Invalid value: ${WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	primary_connection	${NUMBER}	Error: Invalid value: ${NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	primary_connection	${POINTS}	Error: Invalid value: ${POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	primary_connection	${ONE_WORD}  Error: Invalid value: ${ONE_WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	primary_connection	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	primary_connection	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	CLI:Test Set Validate Normal Fields	primary_connection	@{PRIMARY_NETS}
	[Teardown]	CLI:Revert

Checking values for secondary_settings
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	${SETUP_FAILOVER}
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	secondary_connection	${EMPTY}	Error: Missing value for parameter: secondary_connection
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	secondary_connection	${WORD}  Error: Invalid value: ${WORD} for parameter: secondary_connection
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	secondary_connection	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: secondary_connection
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	secondary_connection	${POINTS}	Error: Invalid value: ${POINTS} for parameter: secondary_connection
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	secondary_connection	${ONE_WORD}  Error: Invalid value: ${ONE_WORD} for parameter: secondary_connection
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	secondary_connection	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: secondary_connection
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	secondary_connection	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: secondary_connection

	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	secondary_connection	${EMPTY}	Error: Missing value: secondary_connection
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	secondary_connection	${WORD}  Error: Invalid value: ${WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	secondary_connection	${NUMBER}	Error: Invalid value: ${NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	secondary_connection	${POINTS}	Error: Invalid value: ${POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	secondary_connection	${ONE_WORD}  Error: Invalid value: ${ONE_WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	secondary_connection	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	secondary_connection	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	CLI:Test Set Validate Normal Fields	secondary_connection	@{SECONDARY_NETS}
	[Teardown]	CLI:Revert

Checking values for trigger
	[Tags]	NON-CRITICAL	NEED-REVIEW
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	${SETUP_FAILOVER}
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	trigger	${EMPTY}	Error: Missing value for parameter: trigger
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	trigger	${WORD}  Error: Invalid value: ${WORD} for parameter: trigger
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	trigger	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: trigger
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	trigger	${POINTS}	Error: Invalid value: ${POINTS} for parameter: trigger
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	trigger	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: trigger
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	trigger	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: trigger
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	trigger	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: trigger

	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	trigger	${EMPTY}	Error: Missing value: trigger
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	trigger	${WORD}  Error: Invalid value: ${WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	trigger	${NUMBER}	Error: Invalid value: ${NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	trigger	${POINTS}	Error: Invalid value: ${POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	trigger	${ONE_WORD}  Error: Invalid value: ${ONE_WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	trigger	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	trigger	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	CLI:Test Set Validate Normal Fields	trigger	ip_address  ipv4_default_gateway
	[Teardown]	CLI:Revert

Checking values for trigger | trigger_ip_address
	[Tags]	NON-CRITICAL	NEED-REVIEW     BUG_NG_9828
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	${SETUP_FAILOVER} trigger=ip_address
	CLI:Test Set Validate Invalid Options	trigger_ip_address	${EMPTY}    Error: trigger_ip_address: Invalid IP Address.
	CLI:Test Set Validate Invalid Options	trigger_ip_address	${WORD}  Error: trigger_ip_address: Validation error.
	CLI:Test Set Validate Invalid Options	trigger_ip_address	${NUMBER}	Error: trigger_ip_address: Validation error.
	CLI:Test Set Validate Invalid Options	trigger_ip_address	${POINTS}	Error: trigger_ip_address: Validation error.
	CLI:Test Set Validate Invalid Options	trigger_ip_address	${ONE_WORD}  Error: trigger_ip_address: Validation error.
	CLI:Test Set Validate Invalid Options	trigger_ip_address	${ONE_NUMBER}	Error: trigger_ip_address: Validation error.
	CLI:Test Set Validate Invalid Options	trigger_ip_address	${ONE_POINTS}	Error: trigger_ip_address: Validation error.
	CLI:Test Set Validate Normal Fields	trigger_ip_address	${IP_ADDRESS}
	[Teardown]	CLI:Revert

Checking values for trigger_failed_retries
   [Tags]	NON-CRITICAL	NEED-REVIEW
	Skip If	${NGVERSION} < 4.0	Not Implemented in 3.2
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	${SETUP_FAILOVER}
	CLI:Test Set Validate Invalid Options	trigger_failed_retries	${EMPTY}	Error: trigger_failed_retries: Validation error.
	CLI:Test Set Validate Invalid Options	trigger_failed_retries	${WORD}  Error: trigger_failed_retries: Validation error.
	CLI:Test Set Validate Invalid Options	trigger_failed_retries	${NUMBER}
	CLI:Test Set Validate Invalid Options	trigger_failed_retries	${POINTS}	Error: trigger_failed_retries: Validation error.
	CLI:Test Set Validate Invalid Options	trigger_failed_retries	${ONE_WORD}  Error: trigger_failed_retries: Validation error.
	CLI:Test Set Validate Invalid Options	trigger_failed_retries	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	trigger_failed_retries	${ONE_POINTS}	Error: trigger_failed_retries: Validation error.
	CLI:Test Set Validate Normal Fields	trigger_failed_retries	${TRIGGER_FAILED}
	[Teardown]	CLI:Revert

Checking values for trigger_successful_retries_to_recover
	Skip If	${NGVERSION} < 4.0	Not Implemented in 3.2
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	${SETUP_FAILOVER}
	CLI:Test Set Validate Invalid Options	trigger_successful_retries_to_recover	${EMPTY}	Error: trigger_successful_retries_to_recover: Validation error.
	CLI:Test Set Validate Invalid Options	trigger_successful_retries_to_recover	${WORD}  Error: trigger_successful_retries_to_recover: Validation error.
	CLI:Test Set Validate Invalid Options	trigger_successful_retries_to_recover	${NUMBER}
	CLI:Test Set Validate Invalid Options	trigger_successful_retries_to_recover	${POINTS}	Error: trigger_successful_retries_to_recover: Validation error.
	CLI:Test Set Validate Invalid Options	trigger_successful_retries_to_recover	${ONE_WORD}  Error: trigger_successful_retries_to_recover: Validation error.
	CLI:Test Set Validate Invalid Options	trigger_successful_retries_to_recover	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	trigger_successful_retries_to_recover	${ONE_POINTS}	Error: trigger_successful_retries_to_recover: Validation error.
	CLI:Test Set Validate Normal Fields	trigger_successful_retries_to_recover	${TRIGGER_SUCCESSFUL}
	[Teardown]	CLI:Revert

Checking values for interval_between_retries
	Skip If	${NGVERSION} < 4.0	Not Implemented in 3.2
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	${SETUP_FAILOVER}
	CLI:Test Set Validate Invalid Options	interval_between_retries	${EMPTY}	Error: interval_between_retries: Validation error.
	CLI:Test Set Validate Invalid Options	interval_between_retries	${WORD}  Error: interval_between_retries: Validation error.
	CLI:Test Set Validate Invalid Options	interval_between_retries	${NUMBER}
	CLI:Test Set Validate Invalid Options	interval_between_retries	${POINTS}	Error: interval_between_retries: Validation error.
	CLI:Test Set Validate Invalid Options	interval_between_retries	${ONE_WORD}  Error: interval_between_retries: Validation error.
	CLI:Test Set Validate Invalid Options	interval_between_retries	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	interval_between_retries	${ONE_POINTS}	Error: interval_between_retries: Validation error.
	CLI:Test Set Validate Normal Fields	interval_between_retries	${INTERVAL_RETRIES}
	[Teardown]	CLI:Revert

Checking values for enable_third_level_network_failover
	Skip If	${NGVERSION} < 4.0	Not Implemented in 3.2
	CLI:Enter Path	/settings/network_settings/
	Run Keyword If	${THIRD_IS_SAMECONN}	CLI:Set	${SETUP_FAILOVER} enable_third_level_network_failover=yes tertiary_connection=${PRIMARY}
	...	ELSE	CLI:Set	${SETUP_FAILOVER}
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	enable_third_level_network_failover	${EMPTY}	Error: Missing value for parameter: enable_third_level_network_failover
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	enable_third_level_network_failover	${WORD}  Error: Invalid value: ${WORD} for parameter: enable_third_level_network_failover
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	enable_third_level_network_failover	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: enable_third_level_network_failover
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	enable_third_level_network_failover	${POINTS}	Error: Invalid value: ${POINTS} for parameter: enable_third_level_network_failover
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	enable_third_level_network_failover	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: enable_third_level_network_failover
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	enable_third_level_network_failover	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: enable_third_level_network_failover
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	enable_third_level_network_failover	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: enable_third_level_network_failover

	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	enable_third_level_network_failover	${EMPTY}	Error: Missing value: ${NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	enable_third_level_network_failover	${WORD}  Error: Invalid value: ${WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	enable_third_level_network_failover	${NUMBER}	Error: Invalid value: ${NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	enable_third_level_network_failover	${POINTS}	Error: Invalid value: ${POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	enable_third_level_network_failover	${ONE_WORD}  Error: Invalid value: ${ONE_WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	enable_third_level_network_failover	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	enable_third_level_network_failover	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	CLI:Test Set Validate Normal Fields	enable_third_level_network_failover	no	yes
	[Teardown]	CLI:Revert

Checking values for tertiary_connection
	Skip If	${NGVERSION} < 4.0	Not Implemented in 3.2
	CLI:Enter Path	/settings/network_settings/
	Run Keyword If	${THIRD_IS_SAMECONN}	CLI:Set	${SETUP_FAILOVER} enable_third_level_network_failover=yes tertiary_connection=${PRIMARY}
	...	ELSE	CLI:Set	${SETUP_FAILOVER} enable_third_level_network_failover=yes
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tertiary_connection	${EMPTY}	Error: Missing value for parameter: tertiary_connection
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tertiary_connection	${WORD}  Error: Invalid value: ${WORD} for parameter: tertiary_connection
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tertiary_connection	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: tertiary_connection
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tertiary_connection	${POINTS}	Error: Invalid value: ${POINTS} for parameter: tertiary_connection
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tertiary_connection	${ONE_WORD}  Error: Invalid value: ${ONE_WORD} for parameter: tertiary_connection
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tertiary_connection	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: tertiary_connection
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	tertiary_connection	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: tertiary_connection

	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tertiary_connection	${EMPTY}	Error: Missing value: tertiary_connection
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tertiary_connection	${WORD}  Error: Invalid value: ${WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tertiary_connection	${NUMBER}	Error: Invalid value: ${NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tertiary_connection	${POINTS}	Error: Invalid value: ${POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tertiary_connection	${ONE_WORD}  Error: Invalid value: ${ONE_WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tertiary_connection	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	tertiary_connection	${ONE_POINTS}	Error: Invalid value: ${ONE_POINT}

	CLI:Test Set Validate Normal Fields	tertiary_connection	@{TERTIARY_NETS}
	[Teardown]	CLI:Revert

Checking values for secondary_trigger
	Skip If	${NGVERSION} < 4.0	Not Implemented in 3.2
	CLI:Enter Path	/settings/network_settings/
	Run Keyword If	${THIRD_IS_SAMECONN}	CLI:Set	${SETUP_FAILOVER} enable_third_level_network_failover=yes tertiary_connection=${PRIMARY}
	...	ELSE	CLI:Set	${SETUP_FAILOVER} enable_third_level_network_failover=yes
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	secondary_trigger	${EMPTY}	Error: Missing value for parameter: secondary_trigger
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	secondary_trigger	${WORD}  Error: Invalid value: ${WORD} for parameter: secondary_trigger
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	secondary_trigger	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: secondary_trigger
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	secondary_trigger	${POINTS}	Error: Invalid value: ${POINTS} for parameter: secondary_trigger
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	secondary_trigger	${ONE_WORD}	 Error: Invalid value: ${ONE_WORD} for parameter: secondary_trigger
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	secondary_trigger	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: secondary_trigger
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	secondary_trigger	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: secondary_trigger

	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	secondary_trigger	${EMPTY}	Error: Missing value: secondary_trigger
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	secondary_trigger	${WORD}  Error: Invalid value: ${WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	secondary_trigger	${NUMBER}	Error: Invalid value: ${NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	secondary_trigger	${POINTS}	Error: Invalid value: ${POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	secondary_trigger	${ONE_WORD}  Error: Invalid value: ${ONE_WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	secondary_trigger	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	secondary_trigger	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	CLI:Test Set Validate Normal Fields	secondary_trigger	secondary_ipv4_default_gateway
	CLI:Set	secondary_trigger=ip_address secondary_trigger_ip_address=8.8.8.8
	${OUTPUT}=	CLI:Write	show . secondary_trigger	user=Yes	lines=No
	Should Contain	${OUTPUT}	secondary_trigger = ip_address
	Should Contain	${OUTPUT}	secondary_trigger_ip_address = 8.8.8.8
	[Teardown]	CLI:Revert

Checking values for secondary_trigger_failed_retries
	Skip If	${NGVERSION} < 4.0	Not Implemented in 3.2
	CLI:Enter Path	/settings/network_settings/
	Run Keyword If	${THIRD_IS_SAMECONN}	CLI:Set	${SETUP_FAILOVER} enable_third_level_network_failover=yes tertiary_connection=${PRIMARY}
	...	ELSE	CLI:Set	${SETUP_FAILOVER} enable_third_level_network_failover=yes
	CLI:Test Set Validate Invalid Options	secondary_trigger_failed_retries	${EMPTY}	Error: secondary_trigger_failed_retries: Validation error.
	CLI:Test Set Validate Invalid Options	secondary_trigger_failed_retries	${WORD}  Error: secondary_trigger_failed_retries: Validation error.
	CLI:Test Set Validate Invalid Options	secondary_trigger_failed_retries	${NUMBER}
	CLI:Test Set Validate Invalid Options	secondary_trigger_failed_retries	${POINTS}	Error: secondary_trigger_failed_retries: Validation error.
	CLI:Test Set Validate Invalid Options	secondary_trigger_failed_retries	${ONE_WORD}  Error: secondary_trigger_failed_retries: Validation error.
	CLI:Test Set Validate Invalid Options	secondary_trigger_failed_retries	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	secondary_trigger_failed_retries	${ONE_POINTS}	Error: secondary_trigger_failed_retries: Validation error.
	CLI:Test Set Validate Normal Fields	secondary_trigger_failed_retries	${SECONDARY_TRIGGER_FAILED}
	[Teardown]	CLI:Revert

Checking values for secondary_trigger_successful_retries_to_recover
	Skip If	${NGVERSION} < 4.0	Not Implemented in 3.2
	CLI:Enter Path	/settings/network_settings/
	Run Keyword If	${THIRD_IS_SAMECONN}	CLI:Set	${SETUP_FAILOVER} enable_third_level_network_failover=yes tertiary_connection=${PRIMARY}
	...	ELSE	CLI:Set	${SETUP_FAILOVER} enable_third_level_network_failover=yes
	CLI:Test Set Validate Invalid Options	secondary_trigger_successful_retries_to_recover	${EMPTY}	Error: secondary_trigger_successful_retries_to_recover: Validation error.
	CLI:Test Set Validate Invalid Options	secondary_trigger_successful_retries_to_recover	${WORD}  Error: secondary_trigger_successful_retries_to_recover: Validation error.
	CLI:Test Set Validate Invalid Options	secondary_trigger_successful_retries_to_recover	${NUMBER}
	CLI:Test Set Validate Invalid Options	secondary_trigger_successful_retries_to_recover	${POINTS}	Error: secondary_trigger_successful_retries_to_recover: Validation error.
	CLI:Test Set Validate Invalid Options	secondary_trigger_successful_retries_to_recover	${ONE_WORD}	 Error: secondary_trigger_successful_retries_to_recover: Validation error.
	CLI:Test Set Validate Invalid Options	secondary_trigger_successful_retries_to_recover	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	secondary_trigger_successful_retries_to_recover	${ONE_POINTS}	Error: secondary_trigger_successful_retries_to_recover: Validation error.
	CLI:Test Set Validate Normal Fields	secondary_trigger_successful_retries_to_recover	${SECONDARY_TRIGGER_SUCCESSFUL}
	[Teardown]	CLI:Revert

Checking values for secondary_interval_between_retries
	Skip If	${NGVERSION} < 4.0	Not Implemented in 3.2
	CLI:Enter Path	/settings/network_settings/
		Run Keyword If	${THIRD_IS_SAMECONN}	CLI:Set	${SETUP_FAILOVER} enable_third_level_network_failover=yes tertiary_connection=${PRIMARY}
	...	ELSE	CLI:Set	${SETUP_FAILOVER} enable_third_level_network_failover=yes
	CLI:Test Set Validate Invalid Options	secondary_interval_between_retries	${EMPTY}	Error: secondary_interval_between_retries: Validation error.
	CLI:Test Set Validate Invalid Options	secondary_interval_between_retries	${WORD}	 Error: secondary_interval_between_retries: Validation error.
	CLI:Test Set Validate Invalid Options	secondary_interval_between_retries	${NUMBER}
	CLI:Test Set Validate Invalid Options	secondary_interval_between_retries	${POINTS}	Error: secondary_interval_between_retries: Validation error.
	CLI:Test Set Validate Invalid Options	secondary_interval_between_retries	${ONE_WORD}	 Error: secondary_interval_between_retries: Validation error.
	CLI:Test Set Validate Invalid Options	secondary_interval_between_retries	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	secondary_interval_between_retries	${ONE_POINTS}	Error: secondary_interval_between_retries: Validation error.
	CLI:Test Set Validate Normal Fields	secondary_interval_between_retries	${SECONDARY_INTERVAL_RETRIES}
	[Teardown]	CLI:Revert

Checking values for enable_dynamic_dns
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	${SETUP_FAILOVER}
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	enable_dynamic_dns	${EMPTY}	Error: Missing value for parameter: enable_dynamic_dns
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	enable_dynamic_dns	${WORD}  Error: Invalid value: ${WORD} for parameter: enable_dynamic_dns
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	enable_dynamic_dns	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: enable_dynamic_dns
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	enable_dynamic_dns	${POINTS}	Error: Invalid value: ${POINTS} for parameter: enable_dynamic_dns
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	enable_dynamic_dns	${ONE_WORD}  Error: Invalid value: ${ONE_WORD} for parameter: enable_dynamic_dns
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	enable_dynamic_dns	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: enable_dynamic_dns
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	enable_dynamic_dns	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: enable_dynamic_dns

	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	enable_dynamic_dns	${EMPTY}	Error: Missing value: enable_dynamic_dns
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	enable_dynamic_dns	${WORD}  Error: Invalid value: ${WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	enable_dynamic_dns	${NUMBER}	Error: Invalid value: ${NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	enable_dynamic_dns	${POINTS}	Error: Invalid value: ${POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	enable_dynamic_dns	${ONE_WORD}  Error: Invalid value: ${ONE_WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	enable_dynamic_dns	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	enable_dynamic_dns	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	CLI:Test Set Validate Normal Fields	enable_dynamic_dns	no	yes
	[Teardown]	CLI:Revert

Checking values for ddns_server_name
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	${SETUP_FAILOVER} enable_dynamic_dns=yes
	CLI:Test Set Validate Invalid Options	ddns_server_name	${EMPTY}
	CLI:Test Set Validate Invalid Options	ddns_server_name	${WORD}
	CLI:Test Set Validate Invalid Options	ddns_server_name	${NUMBER}
	CLI:Test Set Validate Invalid Options	ddns_server_name	${POINTS}	Error: ddns_server_name: Validation error.
	CLI:Test Set Validate Invalid Options	ddns_server_name	${ONE_WORD}
	CLI:Test Set Validate Invalid Options	ddns_server_name	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	ddns_server_name	${ONE_POINTS}	Error: ddns_server_name: Validation error.
	CLI:Test Set Validate Normal Fields	ddns_server_name	${SERVER_NAME}
	[Teardown]	CLI:Revert

Checking values for ddns_server_tcp_port
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	${SETUP_FAILOVER} enable_dynamic_dns=yes
	CLI:Test Set Validate Invalid Options	ddns_server_tcp_port	${EMPTY}	Error: ddns_server_tcp_port: Validation error.
	CLI:Test Set Validate Invalid Options	ddns_server_tcp_port	${WORD}  Error: ddns_server_tcp_port: Validation error.
	CLI:Test Set Validate Invalid Options	ddns_server_tcp_port	${NUMBER}
	CLI:Test Set Validate Invalid Options	ddns_server_tcp_port	${POINTS}	Error: ddns_server_tcp_port: Validation error.
	CLI:Test Set Validate Invalid Options	ddns_server_tcp_port	${ONE_WORD}  Error: ddns_server_tcp_port: Validation error.
	CLI:Test Set Validate Invalid Options	ddns_server_tcp_port	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	ddns_server_tcp_port	${ONE_POINTS}	Error: ddns_server_tcp_port: Validation error.
	CLI:Test Set Validate Normal Fields	ddns_server_tcp_port	${SERVER_PORT}
	[Teardown]	CLI:Revert

Checking values for zone
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	${SETUP_FAILOVER} enable_dynamic_dns=yes
	CLI:Test Set Validate Invalid Options	zone	${EMPTY}
	CLI:Test Set Validate Invalid Options	zone	${WORD}
	CLI:Test Set Validate Invalid Options	zone	${NUMBER}
	CLI:Test Set Validate Invalid Options	zone	${POINTS}	Error: zone: Validation error.
	CLI:Test Set Validate Invalid Options	zone	${ONE_WORD}
	CLI:Test Set Validate Invalid Options	zone	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	zone	${ONE_POINTS}	Error: zone: Validation error.
	CLI:Test Set Validate Normal Fields	zone	${ZONE}
	[Teardown]	CLI:Revert

Checking values for failover_hostname
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	${SETUP_FAILOVER} enable_dynamic_dns=yes
	CLI:Test Set Validate Invalid Options	failover_hostname	${EMPTY}
	CLI:Test Set Validate Invalid Options	failover_hostname	${WORD}
	CLI:Test Set Validate Invalid Options	failover_hostname	${NUMBER}
	CLI:Test Set Validate Invalid Options	failover_hostname	${POINTS}	Error: failover_hostname: Validation error.
	CLI:Test Set Validate Invalid Options	failover_hostname	${ONE_WORD}
	CLI:Test Set Validate Invalid Options	failover_hostname	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	failover_hostname	${ONE_POINTS}	Error: failover_hostname: Validation error.
	CLI:Test Set Validate Normal Fields	failover_hostname	${FAILOVER_HOSTNAME}
	[Teardown]	CLI:Revert

Checking values for user_name
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	${SETUP_FAILOVER} enable_dynamic_dns=yes
	${FIELD_USERNAME}=	Set Variable If	'${NGVERSION}' >= '4.0'	username	user_name
	CLI:Test Set Validate Invalid Options	${FIELD_USERNAME}	${EMPTY}
	CLI:Test Set Validate Invalid Options	${FIELD_USERNAME}	${WORD}
	CLI:Test Set Validate Invalid Options	${FIELD_USERNAME}	${NUMBER}
	CLI:Test Set Validate Invalid Options	${FIELD_USERNAME}	${POINTS}	Error: ${FIELD_USERNAME}: Validation error.
	CLI:Test Set Validate Invalid Options	${FIELD_USERNAME}	${ONE_WORD}
	CLI:Test Set Validate Invalid Options	${FIELD_USERNAME}	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	${FIELD_USERNAME}	${ONE_POINTS}	Error: ${FIELD_USERNAME}: Validation error.
	CLI:Test Set Validate Normal Fields	${FIELD_USERNAME}	${USERNAME}
	[Teardown]	CLI:Revert

Checking values for algorithm
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	${SETUP_FAILOVER} enable_dynamic_dns=yes
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	algorithm	${EMPTY}	Error: Missing value for parameter: algorithm
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	algorithm	${WORD}	 Error: Invalid value: ${WORD} for parameter: algorithm
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	algorithm	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: algorithm
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	algorithm	${POINTS}	Error: Invalid value: ${POINTS} for parameter: algorithm
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	algorithm	${ONE_WORD}	 Error: Invalid value: ${ONE_WORD} for parameter: algorithm
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	algorithm	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: algorithm
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	algorithm	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: algorithm

	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	algorithm	${EMPTY}	Error: Missing value: algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	algorithm	${WORD}  Error: Invalid value: ${WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	algorithm	${NUMBER}	Error: Invalid value: ${NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	algorithm	${POINTS}	Error: Invalid value: ${POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	algorithm	${ONE_WORD}  Error: Invalid value: ${ONE_WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	algorithm	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	algorithm	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}
	CLI:Test Set Validate Normal Fields	algorithm	@{ALGORITHM}
	[Teardown]	CLI:Revert

Checking values for key_size
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	${SETUP_FAILOVER} enable_dynamic_dns=yes
	CLI:Test Set Validate Invalid Options	key_size	${EMPTY}	Error: key_size: Validation error.
	CLI:Test Set Validate Invalid Options	key_size	${WORD}  Error: key_size: Validation error.
	CLI:Test Set Validate Invalid Options	key_size	${NUMBER}
	CLI:Test Set Validate Invalid Options	key_size	${POINTS}	Error: key_size: Validation error.
	CLI:Test Set Validate Invalid Options	key_size	${ONE_WORD}  Error: key_size: Validation error.
	CLI:Test Set Validate Invalid Options	key_size	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	key_size	${ONE_POINTS}	Error: key_size: Validation error.
	CLI:Test Set Validate Normal Fields	key_size	${KEY_SIZE}
	[Teardown]	CLI:Revert

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/network_connections/
	CLI:Delete If Exists	analog_modem
	CLI:Add
	CLI:Set	name=analog_modem type=analog_modem device_name=device ipv4_mode=static ipv4_address=${HOST} ipv4_bitmask=24 ipv4_gateway=${GATEWAY}
	CLI:Commit
	SUITE:Set Failover Secondary Interface
	Set Suite Variable	${SETUP_FAILOVER}	enable_network_failover=yes primary_connection=ETH0 secondary_connection=${SECONDARY_CONN}
	@{NETS}=	CLI:Test Get Connections
	Set Global Variable	@{NETS}	@{NETS}
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	${SETUP_FAILOVER}

	${PRIMARY}=	CLI:Write	show . primary_connection	user=Yes
	${PRIMARY}=	Fetch From Right	${PRIMARY}	${SPACE}
	Set Suite Variable	${PRIMARY}	${PRIMARY}
	${SECONDARY}=	CLI:Write	show . secondary_connection	user=Yes
	${SECONDARY}=	Fetch From Right	${SECONDARY}	${SPACE}
	Set Suite Variable	${SECONDARY}	${SECONDARY}

	${RESPONSE}=	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Write	set enable_third_level_network_failover=yes	Raw	user=Yes
	${THIRD_IS_SAMECONN}=	Run Keyword And Return Status	Should Be Equal As Strings	${RESPONSE}	Error: tertiary_connection: Cannot failover to the same connection.
	Set Suite Variable	${THIRD_IS_SAMECONN}	${THIRD_IS_SAMECONN}

	${PRIMARY_NETS}=	Copy List	${NETS}
	Remove Values From List	${PRIMARY_NETS}	${SECONDARY}
	Set Suite Variable	@{PRIMARY_NETS}	@{PRIMARY_NETS}

	${SECONDARY_NETS}=	Copy List	${NETS}
	Remove Values From List	${SECONDARY_NETS}	${PRIMARY}
	Set Suite Variable	@{SECONDARY_NETS}	@{SECONDARY_NETS}

	${TERTIARY_NETS}=	Copy List	${NETS}
	Run Keyword If	'${NGVERSION}' >= '4.0'	Remove Values From List	${TERTIARY_NETS}	${SECONDARY}
	Run Keyword If	'${NGVERSION}' >= '4.0'	Set Suite Variable	@{TERTIARY_NETS}	@{TERTIARY_NETS}
	CLI:Revert

SUITE:Teardown
	CLI:Enter Path	/settings/network_connections/
	CLI:Delete If Exists	analog_modem
	CLI:Close Connection

SUITE:Set Failover Secondary Interface
	${IS_VM}=	CLI:Is VM System
	${IS_NSC}=	CLI:Is Serial Console
	${IS_BSR}=	CLI:Is Bold SR
	${IS_GSR}=	CLI:Is Gate SR
	${IS_NSR}=	CLI:Is Net SR
	${SECONDARY_CONN}=	Run Keyword If	${IS_NSR}	Set Variable	ETH1
	...	ELSE	Run Keyword If	${IS_BSR}	Set Variable	BACKPLANE0
	...	ELSE	Run Keyword If	${IS_GSR}	Set Variable	BACKPLANE0
	...	ELSE	Run Keyword If	${IS_NSC}	Set Variable	ETH1
	...	ELSE	Run Keyword If	${IS_VM}	Set Variable	ETH1
	Set Suite Variable	${SECONDARY_CONN}	${SECONDARY_CONN}