*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Network Settings page, ls, show, set, etc... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	NON-CRITICAL	NEED-REVIEW
Default Tags	CLI	SSH	SHOW	NETWORK

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${HOSTNAME}	name
${DOMAIN_NAME}	name
${IPV4_LOOPBACK}	1.1.1.1
@{ALL_VALUES}	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}	${EXCEEDED}

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/network_settings/
	CLI:Test Available Commands	event_system_audit	event_system_clear	change_password	ls	reboot	shell
	...	shutdown	commit	exit	pwd	revert	show	whoami	cd	hostname	quit	set	show_settings

Test ls command
	CLI:Test Ls Command	Try show command instead...

Test available fields on show command
	CLI:Test Show Command	hostname =	domain_name =	dns server:	dns search:	enable_ipv4_ip_forward =
	...	enable_ipv6_ip_forward =	enable_network_failover =	hostname =
	Run Keyword If	'${NGVERSION}' > '3.2'	CLI:Test Show Command	reverse_path_filtering =	enable_multiple_routing_tables =

Test set available fields
	CLI:Test Set Available Fields	domain_name	enable_ipv4_ip_forward	enable_ipv6_ip_forward	enable_network_failover	hostname
	Run Keyword If	'${NGVERSION}' > '3.2'	CLI:Test Set Available Fields	reverse_path_filtering	enable_multiple_routing_tables
	[Teardown]	CLI:Revert

Test show_settings command
	CLI:Enter Path	/settings/network_settings/
	${OUTPUT}=	CLI:Write	show_settings
	Should Match Regexp	${OUTPUT}	/settings/network_settings hostname=nodegrid
	Should Match Regexp	${OUTPUT}	/settings/network_settings domain_name=localdomain
	Run Keyword If	'${NGVERSION}' > '3.2' and '${NGVERSION}' < '5.0'	Should Match Regexp	${OUTPUT}	/settings/network_settings dns server="192.168.2.205 75.75.75.75 75.75.76.76"
	Run Keyword If	'${NGVERSION}' >= '4.2' and '${NGVERSION}' < '5.0'	Should Match Regexp	${OUTPUT}	/settings/network_settings dns search=zpe.com|zpesystems.com
	Should Match Regexp	${OUTPUT}	/settings/network_settings enable_ipv4_ip_forward=no|yes
	Should Match Regexp	${OUTPUT}	/settings/network_settings enable_ipv6_ip_forward=no|yesTest Enable Failover
	Should Match Regexp	${OUTPUT}	/settings/network_settings enable_network_failover=no|yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Match Regexp	${OUTPUT}	/settings/network_settings reverse_path_filtering=strict_mode|loose_mode|disabled
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Match Regexp	${OUTPUT}	/settings/network_settings enable_multiple_routing_tables=yes|no
	Run Keyword If	'${NGVERSION}' >= '4.2' and '${NGVERSION}' < '5.0'	Should Match Regexp	${OUTPUT}	/settings/network_settings enable_bluetooth_network=yes|no

Test dns fields are empty
	${OUTPUT}=	CLI:Show
	${LINES}=	Split To Lines	${OUTPUT}
	FOR		${LINE}	IN	@{LINES}
		${IS_DNS_SEARCH}=	Run Keyword And Return Status	Should Contain	${LINE}	dns search:
		Run Keyword If	${IS_DNS_SEARCH}	Should Match Regexp	${LINE}	dns search: .+
	END
	FOR		${LINE}	IN	@{LINES}
		${IS_DNS_SERVER}=	Run Keyword And Return Status	Should Contain	${LINE}	dns server:
		Run Keyword If	${IS_DNS_SERVER}	Should Match Regexp	${LINE}	dns server:${SPACE}${SPACE}?.+
	END

Test hostname field
	CLI:Enter Path	/settings/network_settings/
	CLI:Test Set Validate Invalid Options	hostname	${EMPTY}	Error: hostname: Validation error.Error: domain_name: Validation error.
	CLI:Test Set Validate Invalid Options	hostname	${WORD}
	CLI:Test Set Validate Invalid Options	hostname	${NUMBER}
	CLI:Test Set Validate Invalid Options	hostname	${POINTS}	Error: hostname: Validation error.
	CLI:Test Set Validate Invalid Options	hostname	${ONE_WORD}
	CLI:Test Set Validate Invalid Options	hostname	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	hostname	${ONE_POINTS}	Error: hostname: Validation error.
	CLI:Test Set Validate Invalid Options	hostname	${EXCEEDED}	Error: hostname: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	hostname	${HOSTNAME}
	[Teardown]	CLI:Revert

Test domain_name field
	CLI:Enter Path	/settings/network_settings/
	CLI:Test Set Validate Invalid Options	domain_name	${EMPTY}
	CLI:Test Set Validate Invalid Options	domain_name	${WORD}
	CLI:Test Set Validate Invalid Options	domain_name	${NUMBER}
	CLI:Test Set Validate Invalid Options	domain_name	${POINTS}	Error: domain_name: Validation error.
	CLI:Test Set Validate Invalid Options	domain_name	${ONE_WORD}
	CLI:Test Set Validate Invalid Options	domain_name	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	domain_name	${ONE_POINTS}	Error: domain_name: Validation error.
	CLI:Test Set Validate Invalid Options	domain_name	${EXCEEDED}		Error: domain_name: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	domain_name	${DOMAIN_NAME}
	CLI:Test Set Validate Invalid Options	domain_name	-	Error: domain_name: Validation error.
	[Teardown]	CLI:Revert

Test enable_ipv4_ip_forward field
	CLI:Enter Path	/settings/network_settings/
	CLI:Test Set Validate Normal Fields	enable_ipv4_ip_forward	yes	no

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_ipv4_ip_forward	${EMPTY}	Error: Missing value for parameter: enable_ipv4_ip_forward
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_ipv4_ip_forward	${EMPTY}	Error: Missing value: enable_ipv4_ip_forward

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_ipv4_ip_forward	${WORD}	Error: Invalid value: ${WORD} for parameter: enable_ipv4_ip_forward
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_ipv4_ip_forward	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_ipv4_ip_forward	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: enable_ipv4_ip_forward
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_ipv4_ip_forward	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_ipv4_ip_forward	${POINTS}	Error: Invalid value: ${POINTS} for parameter: enable_ipv4_ip_forward
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_ipv4_ip_forward	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_ipv4_ip_forward	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: enable_ipv4_ip_forward
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_ipv4_ip_forward	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_ipv4_ip_forward	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: enable_ipv4_ip_forward
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_ipv4_ip_forward	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_ipv4_ip_forward	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: enable_ipv4_ip_forward
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_ipv4_ip_forward	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_ipv4_ip_forward	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: enable_ipv4_ip_forward
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_ipv4_ip_forward	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test enable_ipv6_ip_forward field
	CLI:Enter Path	/settings/network_settings/
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Normal Fields	enable_ipv6_ip_forward	yes	no
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Normal Fields	enable_ipv6_ip_forward	yes	no

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_ipv6_ip_forward	${EMPTY}	Error: Missing value for parameter: enable_ipv6_ip_forward
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_ipv6_ip_forward	${EMPTY}	Error: Missing value: enable_ipv6_ip_forward

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_ipv6_ip_forward	${WORD}	Error: Invalid value: ${WORD} for parameter: enable_ipv6_ip_forward
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_ipv6_ip_forward	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_ipv6_ip_forward	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: enable_ipv6_ip_forward
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_ipv6_ip_forward	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_ipv6_ip_forward	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: enable_ipv6_ip_forward
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_ipv6_ip_forward	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_ipv6_ip_forward	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: enable_ipv6_ip_forward
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_ipv6_ip_forward	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_ipv6_ip_forward	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: enable_ipv6_ip_forward
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_ipv6_ip_forward	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	enable_ipv6_ip_forward	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: enable_ipv6_ip_forward
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	enable_ipv6_ip_forward	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test ipv4_loopback field
	CLI:Enter Path	/settings/network_settings/
	CLI:Test Set Validate Invalid Options	ipv4_loopback	${EMPTY}
	CLI:Test Set Validate Invalid Options	ipv4_loopback	${WORD}	Error: ipv4_loopback: Invalid IP address.
	CLI:Test Set Validate Invalid Options	ipv4_loopback	${NUMBER}	Error: ipv4_loopback: Invalid IP address.
	CLI:Test Set Validate Invalid Options	ipv4_loopback	${POINTS}	Error: ipv4_loopback: Invalid IP address.
	CLI:Test Set Validate Invalid Options	ipv4_loopback	${ONE_WORD}	Error: ipv4_loopback: Invalid IP address.
	CLI:Test Set Validate Invalid Options	ipv4_loopback	${ONE_NUMBER}	Error: ipv4_loopback: Invalid IP address.
	CLI:Test Set Validate Invalid Options	ipv4_loopback	${ONE_POINTS}	Error: ipv4_loopback: Invalid IP address.
	CLI:Test Set Validate Invalid Options	ipv4_loopback	${EXCEEDED}	Error: ipv4_loopback: Invalid IP address.
	CLI:Test Set Validate Invalid Options	ipv4_loopback	${IPV4_LOOPBACK}
	CLI:Test Set Validate Invalid Options	ipv4_loopback	-	Error: ipv4_loopback: Invalid IP address.
	[Teardown]	CLI:Revert

Test multiple_routing_tables field
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_0
	CLI:Enter Path	/settings/network_settings/
	CLI:Test Set Validate Invalid Options	enable_multiple_routing_tables	${EMPTY}	Error: Missing value for parameter: enable_multiple_routing_tables
	CLI:Test Set Validate Invalid Options	enable_multiple_routing_tables	${WORD}	Error: Invalid value: ${WORD} for parameter: enable_multiple_routing_tables
	CLI:Test Set Validate Normal Fields	enable_multiple_routing_tables	no	yes
	CLI:Revert

Test reverse_path_filtering field
	Skip If	'${NGVERSION}' < '4.2'	Not implemented in v3.2 and v4.0
	CLI:Enter Path	/settings/network_settings/
	CLI:Test Set Validate Invalid Options	reverse_path_filtering	${EMPTY}	Error: Missing value for parameter: reverse_path_filtering
	CLI:Test Set Validate Invalid Options	reverse_path_filtering	${WORD}	Error: Invalid value: ${WORD} for parameter: reverse_path_filtering
	CLI:Test Set Validate Normal Fields	reverse_path_filtering	strict_mode	disabled	loose_mode
	CLI:Revert

Test Enable Failover
	CLI:Test Set Validate Invalid Options Multiple Fields
	...	enable_network_failover=yes primary_connection=ETH0 secondary_connection=${SECONDARY_CONN}
	[Teardown]	CLI:Revert

Set failover configurations
	Skip If	'${NGVERSION}' < '4.2'	Not implemented in v3.2
	CLI:Set	enable_network_failover=yes primary_connection=ETH0 secondary_connection=${SECONDARY_CONN}
	CLI:Test Set Validate Normal Fields	interval_between_retries	7
	CLI:Test Set Validate Normal Fields	trigger_failed_retries	7
	CLI:Test Set Validate Normal Fields	trigger_successful_retries_to_recover	7
	[Teardown]	CLI:Revert

Disable Failover
	CLI:Test Set Validate Normal Fields	enable_network_failover	no
	[Teardown]	CLI:Revert

Enable ipv4_ip_forward field
	CLI:Test Set Validate Normal Fields	enable_ipv4_ip_forward	yes
	CLI:Revert

Disable ipv4_ip_forward field
	CLI:Test Set Validate Normal Fields	enable_ipv4_ip_forward	no
	[Teardown]	CLI:Revert

Enable ipv6_ip_forward field
	CLI:Test Set Validate Normal Fields	enable_ipv6_ip_forward	yes
	[Teardown]	CLI:Revert

Disable ipv6_ip_forward field
	CLI:Test Set Validate Normal Fields	enable_ipv6_ip_forward	no
	[Teardown]	CLI:Revert

Test Valid Values For Field=enable_bluetooth_network
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/network_settings/
	CLI:Test Set Field Options	enable_bluetooth_network	no	yes
	[Teardown]	CLI:Revert	Raw

Test Invalid Values For Field=enable_bluetooth_network
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/network_settings/
	CLI:Test Set Field Invalid Options	enable_bluetooth_network	${EMPTY}
	...	Error: Missing value for parameter: enable_bluetooth_network

	FOR		${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	enable_bluetooth_network	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: enable_bluetooth_network
	END
	[Teardown]	CLI:Revert	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Set Failover Secondary Interface

SUITE:Teardown
	CLI:Close Connection

SUITE:Set Failover Secondary Interface
	${IS_VM}=	CLI:Is VM System
	${IS_NSC}=	CLI:Is Serial Console
	${IS_BSR}=	CLI:Is Bold SR
	${IS_GSR}=	CLI:Is Gate SR
	${IS_NSR}=	CLI:Is Net SR
	${SECONDARY_CONN}=	Run Keyword If	${IS_NSR}	Set Variable	ETH1
	...	ELSE	Run Keyword If	${IS_BSR}	Set Variable	BACKPLANE0
	...	ELSE	Run Keyword If	${IS_GSR}	Set Variable	BACKPLANE0
	...	ELSE	Run Keyword If	${IS_NSC}	Set Variable	ETH1
	...	ELSE	Run Keyword If	${IS_VM}	Set Variable	ETH1
	Set Suite Variable	${SECONDARY_CONN}	${SECONDARY_CONN}