*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Sim card failover feature through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	NON-CRITICAL	NEED-REVIEW	FAILOVER	EXCLUDEIN3_2	BUG_NG_7018
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CELLULAR_CONNECTION}	${GSM_CELLULAR_CONNECTION}
${TYPE}	${GSM_TYPE}
${STRONG_PASSWORD}	${QA_PASSWORD}

*** Test Cases ***
Sim card failover by Data Usage - Custom
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CELLULAR_CONNECTION} type=${TYPE} ethernet_interface=${MODEM_INFO["interface"]} ipv4_mode=dhcp
	CLI:Set	ipv6_mode=no_ipv6_address sim-1_access_point_name=${MODEM_INFO["apn"]} enable_second_sim_card=yes
	CLI:Set	sim-2_access_point_name=${MODEM_INFO["apn"]} enable_data_usage_monitoring=yes sim-1_data_limit_value=5 sim-1_data_warning=10
	CLI:Commit
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	enable_network_failover=yes primary_connection=${CELLULAR_CONNECTION} primary_connection_sim_card=1
	CLI:Set	secondary_connection=${CELLULAR_CONNECTION} secondary_connection_sim_card=2
	CLI:Set	enable_primary_failover_by_data_usage=yes primary_data_usage_threshold=custom primary_custom_data_usage_limit=5
	CLI:Set	enable_primary_failover_by_signal_strength=no enable_primary_failover_by_ip_address=no enable_primary_failover_by_schedule=no
	CLI:Commit
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Cellular Connection
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Sim Card Failover
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Cellular Connection
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete Network Connections	${CELLULAR_CONNECTION}

*** Keywords ***
SUITE:Setup
	CLI:Open
	${HAS_WMODEM_SUPPORT}=	CLI:Has Wireless Modem Support
	Set Suite Variable	${HAS_WMODEM_SUPPORT}
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system

	${HAS_WMODEM}	${MODEM_INFO}=	CLI:Get Wireless Modems SIM Card Info	0
	Set Suite Variable	${HAS_WMODEM}
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	Set Suite Variable	${MODEM_INFO}

	CLI:Change User Password From Shell	USER=root	NEW_PASSWORD=${STRONG_PASSWORD}	CURRENT_ROOT_PASSWORD=${ROOT_PASSWORD}
	CLI:Change User Password From Shell	USER=${DEFAULT_USERNAME}	NEW_PASSWORD=${STRONG_PASSWORD}	CURRENT_ROOT_PASSWORD=${STRONG_PASSWORD}
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Modem Up
	CLI:Switch Connection	default
	CLI:Delete Network Connections	${CELLULAR_CONNECTION}
	SUITE:Disable Network Failover

SUITE:Teardown
	Run Keyword If	not ${HAS_WMODEM_SUPPORT}	CLI:Close Connection
	Run Keyword If	not ${HAS_WMODEM}	CLI:Close Connection
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	Switch Connection	default
	SUITE:Disable Network Failover
	CLI:Delete Network Connections	${CELLULAR_CONNECTION}
	CLI:Change User Password From Shell	USER=root	NEW_PASSWORD=${ROOT_PASSWORD}	CURRENT_ROOT_PASSWORD=${STRONG_PASSWORD}
	CLI:Change User Password From Shell	USER=${DEFAULT_USERNAME}	NEW_PASSWORD=${DEFAULT_PASSWORD}	CURRENT_ROOT_PASSWORD=${ROOT_PASSWORD}
	CLI:Close Connection

SUITE:Check Modem Up
	Switch Connection	default
	${OUTPUT}=	CLI:Get Wireless Modem Model
	Should Contain	${OUTPUT}	EM7565

SUITE:Disable Network Failover
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	enable_network_failover=yes enable_primary_failover_by_ip_address=yes trigger=ipv4_default_gateway
	CLI:Commit
	CLI:Set	primary_connection=ETH0 primary_connection_sim_card=1 secondary_connection=BACKPLANE0 secondary_connection_sim_card=1
	CLI:Commit
	CLI:Set	enable_network_failover=no enable_ipv4_ip_forward=no enable_ipv6_ip_forward=no
	CLI:Commit

SUITE:Check Cellular Connection
	CLI:Enter Path	/settings/network_connections/
	${OUTPUT}=	CLI:Show
	Should Match Regexp	${OUTPUT}	\\s+${CELLULAR_CONNECTION}\\s+connected\\s+mobile\\sbroadband\\sgsm\\s+cdc-wdm0\\s+up\\s+\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\/\\d{0,2}\\s+

SUITE:Check Sim Card Failover
	CLI:Enter Path	/settings/network_connections/${CELLULAR_CONNECTION}
	${OUTPUT}=	CLI:Show
	Should Match Regexp	${OUTPUT}	\\s+enable_second_sim_card = yes\\s+active_sim_card = 2\\s+