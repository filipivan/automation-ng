*** Settings ***
Resource	../../init.robot
Documentation	Testing Failover
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI  NON-CRITICAL	NEED-REVIEW
Default Tags	CLI	SSH	FAILOVER

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${IP_TRIGGER}			${HOSTPEER}
${CELLULAR_CONNECTION}	${GSM_CELLULAR_CONNECTION}
${TYPE}					${GSM_TYPE}
${PASSWORD}	${QA_PASSWORD}

*** Test Cases ***
Test Failover using iptables rule in eth0 and eth1
	CLI:Switch Connection 	HOST
	SUITE:Check If Eth1 Carrier State Up
	${HAS_ETH1}=	CLI:Has ETH1
	Skip If	not ${HAS_ETH1}	eth1 not avaliable
	Skip If	not ${ETH1_STATE}	eth1 not connected

	CLI:Switch Connection	root_session
	CLI:Clear Event Logfile	 ${HOSTNAME_NODEGRID}	HOST
	SUITE:Configuring Firewall Rule
	SUITE:Configure Failover	ETH1
	CLI:Switch Connection 	root_session
	Wait Until Keyword Succeeds		30sec	1s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	144 	Network Failover Executed. Connection: ETH1. Interface: eth1
	SUITE:Delete The Firewall Rule
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds		30sec	1s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	145 	Network Failback Executed. Connection: ETH0. Interface: eth0

	[Teardown]  SUITE:Disable The Failover Configuration

Test Failover using iptables rule in eth0 and gsm
	CLI:Switch Connection 	HOST
	${HAS_WMODEM_SUPPORT}=	CLI:Has Wireless Modem Support
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system

	${HAS_WMODEM}	${MODEM_INFO}=	CLI:Get Wireless Modems SIM Card Info	0
	Set Suite Variable	${HAS_WMODEM}
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	Set Suite Variable	${MODEM_INFO}

	CLI:Change User Password From Shell	root	${PASSWORD}	${ROOT_PASSWORD}
	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${PASSWORD}	${PASSWORD}
	CLI:Switch Connection 			HOST
	SUITE:Create Cellular Connection
	SUITE:Check Cellular Connection

	CLI:Connect As Root	PASSWORD=${PASSWORD}
	CLI:Clear Event Logfile	 ${HOSTNAME_NODEGRID}	HOST
	SUITE:Configuring Firewall Rule
	SUITE:Configure Failover	${CELLULAR_CONNECTION}		Yes
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Cellular Connection
	CLI:Switch Connection 	root_session
	Wait Until Keyword Succeeds		1m	1s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	144 	Network Failover Executed. Connection: ${CELLULAR_CONNECTION}. Interface: ${MODEM_INFO["interface"]}
	SUITE:Delete The Firewall Rule
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds		30sec	1s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	145 	Network Failback Executed. Connection: ETH0. Interface: eth0
	SUITE:Disable The Failover Configuration

	[Teardown]  SUITE:Teardown For GSM

Test Failover using iptables rule in eth0 and backplane0
	CLI:Switch Connection 	HOST
	${BACKPLANE0}=	CLI:Has Interface 	backplane0
	Skip If 	${BACKPLANE0} == ${FALSE}	Device does not have the backplane0 interface

	CLI:Connect As Root
	CLI:Clear Event Logfile	 ${HOSTNAME_NODEGRID}	HOST
	SUITE:Configuring Firewall Rule
	SUITE:Configure Failover	BACKPLANE0
	CLI:Switch Connection 	root_session
	Wait Until Keyword Succeeds		30sec	1s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	144 	Network Failover Executed. Connection: BACKPLANE0. Interface: backplane0
	SUITE:Delete The Firewall Rule
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds		30sec	1s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	145 	Network Failback Executed. Connection: ETH0. Interface: eth0

	[Teardown]  SUITE:Disable The Failover Configuration

Test Failover using iptables rule in eth0 and backplane1
	CLI:Switch Connection 	HOST
	${BACKPLANE1}=	CLI:Has Interface 	backplane1
	Skip If 	${BACKPLANE1} == ${FALSE}	Device does not have the backplane1 interface

	CLI:Connect As Root
	CLI:Clear Event Logfile	 ${HOSTNAME_NODEGRID}	HOST
	SUITE:Configuring Firewall Rule
	SUITE:Configure Failover	BACKPLANE1
	CLI:Switch Connection 	root_session
	Wait Until Keyword Succeeds		30sec	1s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	144 	Network Failover Executed. Connection: BACKPLANE1. Interface: backplane1
	SUITE:Delete The Firewall Rule
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds		30sec	1s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	145 	Network Failback Executed. Connection: ETH0. Interface: eth0

	[Teardown]  SUITE:Disable The Failover Configuration
*** Keywords ***
SUITE:Setup
	CLI:Open	session_alias=HOST
	CLI:Connect As Root
	CLI:Switch Connection 	HOST

	SUITE:Delete The Firewall Rule
	SUITE:Disable The Failover Configuration
	CLI:Delete Network Connections	${CELLULAR_CONNECTION}

SUITE:Teardown
	Wait Until Keyword Succeeds	5x	3s	SUITE:Delete The Firewall Rule
	SUITE:Disable The Failover Configuration
	CLI:Close Connection

SUITE:Configuring Firewall Rule
	CLI:Switch Connection 	HOST
	CLI:Enter Path  	/settings/ipv4_firewall/chains/INPUT/
	CLI:Add
	CLI:Set  			target=DROP rule_number=1 source_net4=${IP_TRIGGER}
	CLI:Commit
	[Teardown]  		CLI:Cancel  Raw

SUITE:Configure Failover
	[Arguments]  ${SECONDARY}	${IS_GSM}=No
	CLI:Enter Path  	 /settings/network_settings/
	CLI:Set  			 enable_network_failover=yes primary_connection=ETH0 secondary_connection=${SECONDARY}
	CLI:Set 			 enable_primary_failover_by_ip_address=yes trigger=ip_address trigger_ip_address=${IP_TRIGGER}
	Run Keyword If 		'${IS_GSM}' != 'No'		CLI:Set  			enable_primary_failover_by_signal_strength=no
	Run Keyword If 		'${IS_GSM}' != 'No'		CLI:Set  			enable_primary_failover_by_data_usage=no
	Run Keyword If 		'${IS_GSM}' != 'No'		CLI:Set 			enable_primary_failover_by_schedule=no
	CLI:Commit

SUITE:Delete The Firewall Rule
	CLI:Switch Connection 		 HOST
	CLI:Enter Path  	 	 /settings/ipv4_firewall/chains/INPUT/
	CLI:Delete If Exists   	 1
	${OUTPUT}=			 	 CLI:Show
	Should Not Contain 		 ${OUTPUT}   DROP    ${IP_TRIGGER}
	[Teardown]  CLI:Cancel   Raw

SUITE:Disable The Failover Configuration
	CLI:Enter Path  	/settings/network_settings/
	CLI:Set  			enable_network_failover=no
	CLI:Commit

SUITE:Check If Eth1 Carrier State Up
	${ETH1_STATE}= 		CLI:Is Interface Carrier State Up	eth1
	Set Suite Variable		${ETH1_STATE}

SUITE:Check Cellular Connection
	CLI:Enter Path	/settings/network_connections/
	${OUTPUT}=	CLI:Show
	Should Contain 	${OUTPUT} 	${CELLULAR_CONNECTION}

SUITE:Create Cellular Connection
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	 name=${CELLULAR_CONNECTION} type=${TYPE} ethernet_interface=${MODEM_INFO["interface"]} ipv4_mode=dhcp
	CLI:Set  ipv6_mode=no_ipv6_address sim-1_access_point_name=${MODEM_INFO["apn"]}
	CLI:Commit
	CLI:Write  up_connection ${CELLULAR_CONNECTION}

SUITE:Teardown For GSM
	CLI:Switch Connection	 HOST
	SUITE:Disable The Failover Configuration
	CLI:Delete Network Connections	${CELLULAR_CONNECTION}
	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}	${PASSWORD}
	CLI:Change User Password From Shell	root	${ROOT_PASSWORD}	${PASSWORD}