*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Cellular failover feature through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	NON-CRITICAL	NEED-REVIEW	EXCLUDEIN3_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CELLULAR_CONNECTION}	${GSM_CELLULAR_CONNECTION}
${TYPE}	${GSM_TYPE}
${STRONG_PASSWORD}	${QA_PASSWORD}

*** Test Cases ***
Test Valid GSM Configuration - Primary SIM Card
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CELLULAR_CONNECTION} type=${TYPE} ethernet_interface=${MODEM_INFO["interface"]} ipv4_mode=dhcp ipv6_mode=no_ipv6_address sim-1_access_point_name=${MODEM_INFO["apn"]} enable_second_sim_card=yes active_sim_card=1 sim-2_access_point_name=${MODEM_INFO["apn"]}
	CLI:Commit
	CLI:Test Show Command	${CELLULAR_CONNECTION}
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	SUITE:Delete Network Connection

Test Primary SIM Card Enable
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CELLULAR_CONNECTION} type=${TYPE} ethernet_interface=${MODEM_INFO["interface"]} ipv4_mode=dhcp ipv6_mode=no_ipv6_address sim-1_access_point_name=${MODEM_INFO["apn"]} enable_second_sim_card=yes active_sim_card=1 sim-2_access_point_name=${MODEM_INFO["apn"]}
	CLI:Commit
	CLI:Test Show Command	${CELLULAR_CONNECTION}
	CLI:Enter Path	/settings/network_connections/${CELLULAR_CONNECTION}
	CLI:Test Show Command	active_sim_card = 1
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	SUITE:Delete Network Connection

Test Valid GSM Configuration - Second SIM Card
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CELLULAR_CONNECTION} type=${TYPE} ethernet_interface=${MODEM_INFO["interface"]} ipv4_mode=dhcp ipv6_mode=no_ipv6_address sim-1_access_point_name=${MODEM_INFO["apn"]} enable_second_sim_card=yes active_sim_card=2 sim-2_access_point_name=${MODEM_INFO["apn"]}
	CLI:Commit
	CLI:Test Show Command	${CELLULAR_CONNECTION}
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	SUITE:Delete Network Connection

Test Second SIM Card enable
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CELLULAR_CONNECTION} type=${TYPE} ethernet_interface=${MODEM_INFO["interface"]} ipv4_mode=dhcp ipv6_mode=no_ipv6_address sim-1_access_point_name=${MODEM_INFO["apn"]} enable_second_sim_card=yes active_sim_card=2 sim-2_access_point_name=${MODEM_INFO["apn"]}
	CLI:Commit
	CLI:Test Show Command	${CELLULAR_CONNECTION}
	CLI:Enter Path	/settings/network_connections/${CELLULAR_CONNECTION}
	CLI:Test Show Command	active_sim_card = 2
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	SUITE:Delete Network Connection

Test Change SIM Cards enable
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CELLULAR_CONNECTION} type=${TYPE} ethernet_interface=${MODEM_INFO["interface"]} ipv4_mode=dhcp ipv6_mode=no_ipv6_address sim-1_access_point_name=${MODEM_INFO["apn"]} enable_second_sim_card=yes active_sim_card=2 sim-2_access_point_name=${MODEM_INFO["apn"]}
	CLI:Commit
	CLI:Test Show Command	${CELLULAR_CONNECTION}
	CLI:Enter Path	/settings/network_connections/${CELLULAR_CONNECTION}
	CLI:Test Show Command	active_sim_card = 2
	CLI:Enter Path	/settings/network_connections/${CELLULAR_CONNECTION}
	CLI:Set	active_sim_card=1
	CLI:Commit
	CLI:Enter Path	/settings/network_connections/${CELLULAR_CONNECTION}
	CLI:Test Show Command	active_sim_card = 1
	CLI:Enter Path	/settings/network_connections/
	CLI:Test Show Command	${CELLULAR_CONNECTION}
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	SUITE:Delete Network Connection

Test All Data Monitors Configuration
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CELLULAR_CONNECTION} type=${TYPE} ethernet_interface=${MODEM_INFO["interface"]} ipv4_mode=dhcp ipv6_mode=no_ipv6_address sim-1_access_point_name=${MODEM_INFO["apn"]} enable_second_sim_card=yes active_sim_card=1 sim-2_access_point_name=${MODEM_INFO["apn"]} enable_data_usage_monitoring=yes sim-1_data_limit_value=0.15 sim-1_data_warning=20
	CLI:Commit
	CLI:Test Show Command	${CELLULAR_CONNECTION}
	CLI:Enter Path	/settings/network_connections/${CELLULAR_CONNECTION}
	CLI:Test Show Command	active_sim_card = 1	enable_data_usage_monitoring = yes	sim-1_data_limit_value = 0.15	sim-1_data_warning = 20
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	SUITE:Delete Network Connection

*** Keywords ***
SUITE:Setup
	CLI:Open
	${HAS_WMODEM_SUPPORT}=	CLI:Has Wireless Modem Support
	Set Suite Variable	${HAS_WMODEM_SUPPORT}
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system

	${HAS_WMODEM}	${MODEM_INFO}=	CLI:Get Wireless Modems SIM Card Info	0
	Set Suite Variable	${HAS_WMODEM}
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	Set Suite Variable	${MODEM_INFO}

	CLI:Change User Password From Shell	root	${STRONG_PASSWORD}	${ROOT_PASSWORD}
	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${STRONG_PASSWORD}	${STRONG_PASSWORD}
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Modem Up
	CLI:Switch Connection	default

SUITE:Teardown
	Run Keyword If	${HAS_WMODEM}	Run Keywords
	...	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}	${STRONG_PASSWORD}	AND
	...	CLI:Change User Password From Shell	root	${ROOT_PASSWORD}	${STRONG_PASSWORD}
	CLI:Close Connection

SUITE:Check Modem Up
	Switch Connection	default
	${OUTPUT}=	CLI:Get Wireless Modem Model
	Should Contain	${OUTPUT}	EM7565

SUITE:Delete Network Connection
	CLI:Enter Path	/settings/network_connections/
	CLI:Delete If Exists	${CELLULAR_CONNECTION}
	CLI:Commit