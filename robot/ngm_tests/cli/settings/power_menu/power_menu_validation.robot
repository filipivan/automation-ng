*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Power Menu... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${ZERO_NUMBER}	0
${RIGHT}	9

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/power_menu/
	${list}=	Create List	cd	hostname	set	change_password	ls	shell	commit	pwd	show	event_system_audit	quit
	...	event_system_clear	reboot	shutdown	exit	revert	whoami	system_certificate	system_config_check
	...	factory_settings
	CLI:Test Available Commands	@{list}

Test show_settings command
	CLI:Enter Path	/settings/power_menu/
	${OUTPUT}=	CLI:Write	show_settings
	Should Match Regexp	${OUTPUT}	/settings/power_menu exit_option=1
	Should Match Regexp	${OUTPUT}	/settings/power_menu exit_label=Exit
	Should Match Regexp	${OUTPUT}	/settings/power_menu status_option=2
	Should Match Regexp	${OUTPUT}	/settings/power_menu status_label=Status
	Should Match Regexp	${OUTPUT}	/settings/power_menu on_option=3
	Should Match Regexp	${OUTPUT}	/settings/power_menu on_label=On
	Should Match Regexp	${OUTPUT}	/settings/power_menu off_option=4
	Should Match Regexp	${OUTPUT}	/settings/power_menu off_label=Off
	Should Match Regexp	${OUTPUT}	/settings/power_menu cycle_option=5
	Should Match Regexp	${OUTPUT}	/settings/power_menu cycle_label=Cycle

Test with field=exit_option
	CLI:Enter Path	/settings/power_menu/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	exit_option	${EMPTY}	Error: Missing value for parameter: exit_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	exit_option	${EMPTY}	Error: Missing value: exit_option

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	exit_option	${WORD}	Error: Invalid value: ${WORD} for parameter: exit_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	exit_option	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	exit_option	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: exit_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	exit_option	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	exit_option	${POINTS}	Error: Invalid value: ${POINTS} for parameter: exit_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	exit_option	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	exit_option	${ZERO_NUMBER}	Error: Invalid value: ${ZERO_NUMBER} for parameter: exit_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	exit_option	${ZERO_NUMBER}	Error: Invalid value: ${ZERO_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	exit_option	${ONE_NUMBER}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	exit_option	${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	exit_option	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: exit_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	exit_option	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	exit_option	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: exit_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	exit_option	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	exit_option	${TWO_NUMBER}	Error: Invalid value: ${TWO_NUMBER} for parameter: exit_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	exit_option	${TWO_NUMBER}	Error: Invalid value: ${TWO_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	exit_option	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: exit_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	exit_option	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	exit_option	1	6	7	8	9
	[Teardown]	CLI:Revert	Raw

Test with field=exit_label
	CLI:Enter Path	/settings/power_menu
	CLI:Test Set Validate Invalid Options	exit_label	${EMPTY}	Error: exit_label: Validation error.
	CLI:Test Set Validate Invalid Options	exit_label	${WORD}
	CLI:Test Set Validate Invalid Options	exit_label	${NUMBER}
	CLI:Test Set Validate Invalid Options	exit_label	${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	exit_label	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument for command: set
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	exit_label	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument: set

	CLI:Test Set Validate Invalid Options	exit_label	${EXCEEDED}	Error: exit_label: Exceeded the maximum size for this field.
	CLI:Test Set Validate Normal Fields	exit_label	${ONE_WORD}
	[Teardown]	CLI:Revert	Raw

Test with field=status_option
	CLI:Enter Path	/settings/power_menu/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	status_option	${EMPTY}	Error: Missing value for parameter: status_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	status_option	${EMPTY}	Error: Missing value: status_option

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	status_option	${WORD}	Error: Invalid value: ${WORD} for parameter: status_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	status_option	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	status_option	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: status_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	status_option	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	status_option	${POINTS}	Error: Invalid value: ${POINTS} for parameter: status_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	status_option	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	status_option	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: status_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	status_option	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	status_option	${TWO_NUMBER}	Error: Invalid value: ${TWO_NUMBER} for parameter: status_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	status_option	${TWO_NUMBER}	Error: Invalid value: ${TWO_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	status_option	${ONE_NUMBER}	Error: exit_option: Must be a unique number between 1 and 9.Error: status_option: Must be a unique number between 1 and 9.
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	status_option	${ONE_NUMBER}	Error: exit_option: Must be a unique number between 1 and 9.Error: status_option: Must be a unique number between 1 and 9.

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	status_option	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: status_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	status_option	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	status_option	6	7	8	9

Test with field=status_label
	CLI:Enter Path	/settings/power_menu
	CLI:Test Set Validate Invalid Options	status_label	${EMPTY}	Error: status_label: Validation error.
	CLI:Test Set Validate Invalid Options	status_label	${EXCEEDED}	Error: status_label: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	status_label	${WORD}
	CLI:Test Set Validate Invalid Options	status_label	${NUMBER}
	CLI:Test Set Validate Invalid Options	status_label	${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	status_label	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument for command: set
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	status_label	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument: set

	CLI:Test Set Validate Normal Fields	status_label	${ONE_WORD}
	[Teardown]	CLI:Revert	Raw

Test with field=on_option
	CLI:Enter Path	/settings/power_menu
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	on_option	${EMPTY}	Error: Missing value for parameter: on_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	on_option	${EMPTY}	Error: Missing value: on_option

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	on_option	${WORD}	Error: Invalid value: ${WORD} for parameter: on_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	on_option	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	on_option	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: on_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	on_option	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	on_option	${POINTS}	Error: Invalid value: ${POINTS} for parameter: on_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	on_option	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	on_option	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: on_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	on_option	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	on_option	${TWO_NUMBER}	Error: Invalid value: ${TWO_NUMBER} for parameter: on_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	on_option	${TWO_NUMBER}	Error: Invalid value: ${TWO_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	on_option	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: on_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	on_option	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	on_option	3	6	7	8	9
	[Teardown]	CLI:Revert	Raw

Test with the field=on_label
	CLI:Enter Path	/settings/power_menu
	CLI:Test Set Validate Invalid Options	on_label	${EMPTY}	Error: on_label: Validation error.
	CLI:Test Set Validate Invalid Options	on_label	${EXCEEDED}	Error: on_label: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	on_label	${WORD}
	CLI:Test Set Validate Invalid Options	on_label	${NUMBER}
	CLI:Test Set Validate Invalid Options	on_label	${POINTS}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	on_label	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument for command: set
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	on_label	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument: set
	CLI:Test Set Validate Normal Fields	on_label	${ONE_WORD}
	[Teardown]	CLI:Revert	Raw

Test with the field=off_option
	CLI:Enter Path	/settings/power_menu
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	off_option	${EMPTY}	Error: Missing value for parameter: off_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	off_option	${EMPTY}	Error: Missing value: off_option

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	off_option	${WORD}	Error: Invalid value: ${WORD} for parameter: off_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	off_option	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	off_option	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: off_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	off_option	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	off_option	${POINTS}	Error: Invalid value: ${POINTS} for parameter: off_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	off_option	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	off_option	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: off_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	off_option	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	off_option	${TWO_NUMBER}	Error: Invalid value: ${TWO_NUMBER} for parameter: off_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	off_option	${TWO_NUMBER}	Error: Invalid value: ${TWO_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	off_option	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: off_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	off_option	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	off_option	4	6	7	8	9
	[Teardown]	CLI:Revert	Raw

Test with the field=off_label
	CLI:Enter Path	/settings/power_menu
	CLI:Test Set Validate Invalid Options	off_label	${EMPTY}	Error: off_label: Validation error.
	CLI:Test Set Validate Invalid Options	off_label	${EXCEEDED}	Error: off_label: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	off_label	${WORD}
	CLI:Test Set Validate Invalid Options	off_label	${NUMBER}
	CLI:Test Set Validate Invalid Options	off_label	${POINTS}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	off_label	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument for command: set
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	off_label	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument: set
	CLI:Test Set Validate Normal Fields	off_label	${ONE_WORD}
	[Teardown]	CLI:Revert	Raw

Test with the field=cycle_option
	CLI:Enter Path	/settings/power_menu
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	cycle_option	${EMPTY}	Error: Missing value for parameter: cycle_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	cycle_option	${EMPTY}	Error: Missing value: cycle_option

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	cycle_option	${WORD}	Error: Invalid value: ${WORD} for parameter: cycle_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	cycle_option	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	cycle_option	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: cycle_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	cycle_option	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	cycle_option	${POINTS}	Error: Invalid value: ${POINTS} for parameter: cycle_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	cycle_option	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	cycle_option	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: cycle_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	cycle_option	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	cycle_option	${TWO_NUMBER}	Error: Invalid value: ${TWO_NUMBER} for parameter: cycle_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	cycle_option	${TWO_NUMBER}	Error: Invalid value: ${TWO_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	cycle_option	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: cycle_option
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	cycle_option	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	cycle_option	5	6	7	8	9
	[Teardown]	CLI:Revert	Raw

Test with the field=cycle_label
	CLI:Enter Path	/settings/power_menu
	CLI:Test Set Validate Invalid Options	cycle_label	${EMPTY}	Error: cycle_label: Validation error.
	CLI:Test Set Validate Invalid Options	cycle_label	${EXCEEDED}	Error: cycle_label: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	cycle_label	${WORD}
	CLI:Test Set Validate Invalid Options	cycle_label	${NUMBER}
	CLI:Test Set Validate Invalid Options	cycle_label	${POINTS}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	cycle_label	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument for command: set
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	cycle_label	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument: set
	CLI:Test Set Validate Normal Fields	cycle_label	${ONE_WORD}
	[Teardown]	CLI:Revert	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Reset Power Menu

SUITE:Teardown
	SUITE:Reset Power Menu
	CLI:Close Connection

SUITE:Reset Power Menu
	CLI:Enter Path	/settings/power_menu/
	CLI:Set	exit_option=1 exit_label=Exit status_option=2 status_label=Status on_option=3 on_label=On off_option=4 off_label=Off cycle_option=5 cycle_label=Cycle
	CLI:Commit
	CLI:Test Show Command	exit_option = 1	exit_label = Exit	status_option = 2	status_label = Status
	...	on_option = 3	on_label = On	off_option = 4	off_label = Off	cycle_option = 5	cycle_label = Cycle