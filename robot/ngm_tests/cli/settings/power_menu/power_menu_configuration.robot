*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Power Menu... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${ZERO_NUMBER}	0
${RIGHT}	9

*** Test Cases ***
Test valid values for fields=exit_option/status_option/on_option/off_option/cycle_option
	CLI:Enter Path	/settings/power_menu/
	CLI:Set	exit_option=9 status_option=8 on_option=7 off_option=6 cycle_option=2
	CLI:Commit
	CLI:Test Show Command	exit_option = 9	exit_label = Exit	status_option = 8	status_label = Status
	...	on_option = 7	on_label = On	off_option = 6	off_label = Off	cycle_option = 2	cycle_label = Cycle
	SUITE:Reset Power Menu

Test valid values for fields=exit_label/status_label/on_label/off_label/cycle_label
	CLI:Enter Path	/settings/power_menu/
	CLI:Set	exit_label=ExitLabel status_label=StatusLabel on_label=OnLabel off_label=OffLabel cycle_label=CycleLabel
	CLI:Commit
	CLI:Test Show Command	exit_option = 1	exit_label = ExitLabel	status_option = 2	status_label = StatusLabel
	...	on_option = 3	on_label = OnLabel	off_option = 4	off_label = OffLabel	cycle_option = 5
	...	cycle_label = CycleLabel
	SUITE:Reset Power Menu

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Reset Power Menu

SUITE:Teardown
	SUITE:Reset Power Menu
	CLI:Close Connection

SUITE:Reset Power Menu
	CLI:Enter Path	/settings/power_menu/
	CLI:Write	set exit_option=1 exit_label=Exit status_option=2 status_label=Status on_option=3 on_label=On off_option=4 off_label=Off cycle_option=5 cycle_label=Cycle
	CLI:Commit
	CLI:Test Show Command	exit_option = 1	exit_label = Exit	status_option = 2	status_label = Status
	...	on_option = 3	on_label = On	off_option = 4	off_label = Off	cycle_option = 5	cycle_label = Cycle