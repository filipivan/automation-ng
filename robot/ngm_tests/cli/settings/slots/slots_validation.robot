*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Slots page, ls, show etc... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	NEED-REVIEW
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

#ONLY FOR NSR
*** Test Cases ***
Test available commands after send tab-tab
	Skip If	not ${IS_NSR}	Slots only in NSR
	CLI:Enter Path	/settings/slots/
	CLI:Test Available Commands	acknowledge_alarm_state	factory_settings	shell
	...	apply_settings	hostname	show
	...	cd	ls	show_settings
	...	change_password	pwd	shutdown
	...	commit	quit	software_upgrade
	...	event_system_audit	reboot	system_certificate
	...	event_system_clear	revert	system_config_check
	...	exit	save_settings	whoami

Test visible fields for show command
	Skip If	not ${IS_NSR}	Slots only in NSR
	CLI:Enter Path	/settings/slots/
	CLI:Test Show Command Regexp	slot number\\s+card sku\\s+card type
	CLI:Test Show Command	slot-1	slot-2	slot-3	slot-4	slot-5

Test show_settings command
	Skip If	not ${IS_NSR}	Slots only in NSR
	CLI:Enter Path	/settings/slots/
	Write	show_settings
	${OUTPUT}=	CLI:Read Until Prompt
	Should Not Contain	${OUTPUT}	/settings/slots/

Test available folders
	Skip If	not ${IS_NSR}	Slots only in NSR
	CLI:Enter Path	/settings/slots/
	CLI:Test Ls Command	slot-1	slot-2	slot-3	slot-4	slot-5

Test entering slot-1
	Skip If	not ${IS_NSR}	Slots only in NSR
	CLI:Enter Path	/settings/slots/slot-1
	CLI:Test Show Command	slot number:	card sku:	card type:

Test entering computer card folder if exists
	Skip If	not ${IS_NSR}	Slots only in NSR
	CLI:Enter Path	/settings/slots/
	${Output}=	CLI:Show
	${COMP}=	Get Lines Containing String	${Output}	Compute	case_insensitive=True

	Set Suite Variable	${COMP}
	@{SLOT}=	Run Keyword If	'${COMP}' != '${EMPTY}'	Split String	${COMP}[0]
	...	ELSE	Skip	No Computer Card available
	CLI:Enter Path	/settings/slots/${SLOT}[0]
	CLI:Test Show Command	slot number:	card type: NSR Compute Expansion Card	current status:
	...	initial_status	cpu temperature:	cpu fan speed:
	CLI:Test Available Commands	hostname	set
	...	apply_settings	ls	shell
	...	cd	power_off	show
	...	change_password	power_on	show_settings
	...	commit	pwd	shutdown
	...	event_system_audit	quit	software_upgrade
	...	event_system_clear	reboot	system_certificate
	...	exit	reset_button	system_config_check
	...	factory_settings	revert	whoami
	...	force_power_off	save_settings

Test initial_status field for computer card
	Skip If	not ${IS_NSR}	Slots only in NSR
	@{SLOT}=	Run Keyword If	'${COMP}' != '${EMPTY}'	Split String	${COMP}[0]
	...	ELSE	Skip	No Computer Card available
	CLI:Enter Path	/settings/slots/${SLOT}[0]
	CLI:Test Set Available Fields	initial_status
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	initial_status	${EMPTY}	Error: Missing value for parameter: initial_status
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	initial_status	a	Error: Invalid value: a for parameter: initial_status
	CLI:Test Set Field Options	initial_status	powered_on	powered_off

*** Keywords ***
SUITE:Setup
	CLI:Open
	${IS_NSR}=	CLI:Is Net SR
	Set Suite Variable	${IS_NSR}

SUITE:Teardown
	CLI:Close Connection