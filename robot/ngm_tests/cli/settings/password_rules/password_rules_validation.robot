*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Password Rules... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	 SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/password_rules/
	${list}=	Create List	cd	hostname	set	change_password	ls	shell	commit	pwd	show	event_system_audit	quit	event_system_clear	reboot	shutdown	exit	revert	whoami	show_settings
	CLI:Test Available Commands	@{list}

Test visible fields for show command field check_password_complexity=no
	CLI:Test Show Command	check_password_complexity	pwd_expiration_min_days	pwd_expiration_max_days	 pwd_expiration_warning_days

Test available fields field check_password_complexity=yes
	CLI:Set Field	check_password_complexity	yes
	CLI:Test Set Available Fields	min_digits	min_upper_case_characters	min_special_characters	minimum_size	passwords_in_history
	CLI:Revert

Test available fields for show command field check_password_complexity=yes
	CLI:Set Field	check_password_complexity	yes
	CLI:Test Show Command	min_digits	min_upper_case_characters	min_special_characters	minimum_size	passwords_in_history
	CLI:Revert

Test available value for field check_password_complexity
	CLI:Test Set Field Options	check_password_complexity	yes	no

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/password_rules/
	CLI:Set	pwd_expiration_min_days=0 pwd_expiration_max_days=99999 pwd_expiration_warning_days=7
	CLI:Commit
	CLI:Set	check_password_complexity=no
	CLI:Commit

SUITE:Teardown
	${OUTPUT}=	Run Keyword and Return Status	CLI:Enter Path	/settings/password_rules/
	Run Keyword If	'${OUTPUT}' == 'False'	Run Keywords	CLI:Cancel
	...	AND	CLI:Enter Path	/settings/password_rules/
	CLI:Set	pwd_expiration_min_days=0 pwd_expiration_max_days=99999 pwd_expiration_warning_days=7
	CLI:Commit
	CLI:Set	check_password_complexity=no
	CLI:Commit
	CLI:Close Connection