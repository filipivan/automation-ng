*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Password Rules... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${MINIMUM_SIZE}	8
${MINIMUM_DIGITS}	1
${MINIMUM_UPPERCASE}	4
${MINIMUM_SPECIAL}	4

*** Test Cases ***
Test incorrect values for field=pwd_expiration_min_days
	CLI:Test Set Field Invalid Options	pwd_expiration_min_days	a	Error: pwd_expiration_min_days: The value must be between 0 and 99999.Error: pwd_expiration_max_days: The maximum days must be greater than the minimum days, or empty.
	CLI:Test Set Field Invalid Options	pwd_expiration_min_days	999999	Error: pwd_expiration_min_days: Exceeded the maximum size for this field.

Test incorrect values for field=pwd_expiration_max_days
	CLI:Test Set Field Invalid Options	pwd_expiration_max_days	a	Error: pwd_expiration_min_days: The minimum days must be smaller than the maximum days, or empty.Error: pwd_expiration_max_days: The value must be between 0 and 99999.Error: pwd_expiration_warning_days: The warning days must be greater than 0 and less than the difference between the maximum and the minimum days, or empty.
	CLI:Test Set Field Invalid Options	pwd_expiration_max_days	999999	Error: pwd_expiration_max_days: Exceeded the maximum size for this field.

Test correct values for field=pwd_expiration_max_days
	${OUTPUT}=	CLI:Set	pwd_expiration_max_days=18
	CLI:Test Show Command	pwd_expiration_max_days = 18
	CLI:Set Field	pwd_expiration_max_days	99999
	CLI:Commit

Test correct values for field=pwd_expiration_min_days
	${OUTPUT}=	CLI:Set	pwd_expiration_min_days=10
	CLI:Test Show Command	pwd_expiration_min_days = 10
	CLI:Set Field	pwd_expiration_min_days	0
	CLI:Commit

Test incorrect values for field=minimum_size
	CLI:Set Field	check_password_complexity	yes
	CLI:Test Set Field Invalid Options	minimum_size	5	Error: minimum_size: The value must be between 6 and 30.
	CLI:Revert

Test correct values for field=minimum_size
	CLI:Set Field	check_password_complexity	yes
	CLI:Test Set Field Valid Options	minimum_size	6

Test incorrect values for field=pwd_expiration_warning_days
	CLI:Test Set Field Invalid Options	pwd_expiration_warning_days	a	Error: pwd_expiration_warning_days: The warning days must be greater than 0 and less than the difference between the maximum and the minimum days, or empty.
	CLI:Test Set Field Invalid Options	pwd_expiration_warning_days	999999	Error: pwd_expiration_warning_days: Exceeded the maximum size for this field.

Test correct values for field=pwd_expiration_warning_days
	${OUTPUT}=	CLI:Write	set pwd_expiration_warning_days=4	Raw
	Should Not Contain	${OUTPUT}	Error
	CLI:Test Show Command	pwd_expiration_warning_days = 4
	[Teardown]	CLI:Revert

Test incorrect values for field=min_digits
	CLI:Set Field	check_password_complexity	yes
	CLI:Test Set Field Invalid Options	min_digits	10	Error: min_digits: The value must be between 0 and
	[Teardown]	CLI:Revert

Test correct values for field=min_digits
	CLI:Set Field	check_password_complexity	yes
	CLI:Test Set Field Valid Options	min_digits	6
	[Teardown]	CLI:Revert

Test incorrect values for field=min_upper_case_characters
	CLI:Set Field	check_password_complexity	yes
	CLI:Test Set Field Invalid Options	min_upper_case_characters	a	Error: min_upper_case_characters: The value must be between 0 and
	[Teardown]	CLI:Revert

Test correct values for field=min_upper_case_characters
	CLI:Set Field	check_password_complexity	yes
	Run Keyword If	'${NGVERSION}' == '4.2' or '${NGVERSION}' == '5.0' or '${NGVERSION}' >= '5.6'	CLI:Test Set Field Valid Options	min_upper_case_characters	8
	...	ELSE	CLI:Test Set Field Valid Options	min_upper_case_characters	6
	[Teardown]	CLI:Revert

Test incorrect values for field=min_special_characters
	CLI:Set Field	check_password_complexity	yes
	CLI:Test Set Field Invalid Options	min_special_characters	a	Error: min_special_characters: The value must be between 0 and
	[Teardown]	CLI:Revert

Test correct values for field=min_special_characters
	CLI:Set Field	check_password_complexity	yes
	${OUTPUT}=	CLI:Set	min_digits=4 min_upper_case_characters=1 min_special_characters=2
	CLI:Test Show Command	min_digits = 4	min_upper_case_characters = 1	min_special_characters = 2
	[Teardown]	CLI:Revert

Test incorrect values for field=passwords_in_history
	CLI:Set Field	check_password_complexity	yes
	CLI:Test Set Field Invalid Options	passwords_in_history	${EMPTY}	Error: passwords_in_history: The value must be between 1 and 20.
	CLI:Test Set Field Invalid Options	passwords_in_history	a	Error: passwords_in_history: The value must be between 1 and 20.
	[Teardown]	CLI:Revert

Test correct values for field=passwords_in_history
	CLI:Set Field	check_password_complexity	yes
	${OUTPUT}=	CLI:Set	passwords_in_history=4
	CLI:Test Show Command	passwords_in_history = 4
	[Teardown]	CLI:Revert

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Set Passwd Rules	PWD_EXPIRATION_MIN_DAYS=0	PWD_EXPIRATION_MAX_DAYS=99999	PWD_EXPIRATION_WARNING_DAYS=7

SUITE:Teardown
	${OUTPUT}=	Run Keyword And Return Status	CLI:Enter Path	/settings/password_rules/
	Run Keyword If	'${OUTPUT}' == '${FALSE}'	Run Keywords	CLI:Cancel
	...	AND	CLI:Enter Path	/settings/password_rules/
	CLI:Set	pwd_expiration_min_days=0 pwd_expiration_max_days=99999 pwd_expiration_warning_days=7
	CLI:Commit
	CLI:Set	check_password_complexity=no
	CLI:Commit
	CLI:Delete Users	sneezy
	CLI:Close Connection

SUITE:Set Passwd Rules
	[Arguments]	${CHECK_PASSWORD_COMPLEXITY}=no	${MIN_UPPER_CASE_CHARACTERS}=0	${PWD_EXPIRATION_MAX_DAYS}=0	${MIN_DIGITS}=1
	...	${MINIMUM_SIZE}=6	${PWD_EXPIRATION_MIN_DAYS}=0	${MIN_SPECIAL_CHARACTERS}=1	${PASSWORDS_IN_HISTORY}=1
	...	${PWD_EXPIRATION_WARNING_DAYS}=${EMPTY}
	CLI:Enter Path	/settings/password_rules/
	CLI:Set	check_password_complexity=${CHECK_PASSWORD_COMPLEXITY} pwd_expiration_max_days=${PWD_EXPIRATION_MAX_DAYS} pwd_expiration_min_days=${PWD_EXPIRATION_MIN_DAYS} pwd_expiration_warning_days=${PWD_EXPIRATION_WARNING_DAYS}
	Run keyword If	'${CHECK_PASSWORD_COMPLEXITY}' != 'no'	CLI:Set	minimum_size=${MINIMUM_SIZE} min_upper_case_characters=${MIN_UPPER_CASE_CHARACTERS}
	Run keyword If	'${CHECK_PASSWORD_COMPLEXITY}' != 'no'	CLI:Set	passwords_in_history=${PASSWORDS_IN_HISTORY} min_digits=${MIN_DIGITS} min_special_characters=${MIN_SPECIAL_CHARACTERS}
	CLI:Commit