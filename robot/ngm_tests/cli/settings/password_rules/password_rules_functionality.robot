*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Password Rules... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI	SSH	SHOW

Suite Setup	 SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${MINIMUM_SIZE}	8
${MINIMUM_DIGITS}	1
${MINIMUM_UPPERCASE}	4
${MINIMUM_SPECIAL}	4
${TEST_USERNAME}	test_user
${PASSWD_1}	TESTING95164@#!$
${PASSWD_2}	@QAusers${QA_PASSWORD}

*** Test Cases ***
Test adding user with password complexity=minimum_size invalid
	[Setup]	SUITE:Set Passwd Rules	yes	MIN_UPPER_CASE_CHARACTERS=0	MIN_SPECIAL_CHARACTERS=0	MIN_DIGITS=0	MINIMUM_SIZE=${MINIMUM_SIZE}
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	CLI:Set	username=sneezy password=sneezy
	${OUTPUT}=	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: password: The password is too short. Minimum length: ${MINIMUM_SIZE}.
	[Teardown]	CLI:Cancel	Raw

Test adding user with password complexity=minimum_size invalid and other parameters right
	[Tags]	NON-CRITICAL	BUG_NG_10146
	[Setup]	SUITE:Set Passwd Rules	yes	MIN_UPPER_CASE_CHARACTERS=2	MIN_SPECIAL_CHARACTERS=2	MIN_DIGITS=2	MINIMUM_SIZE=12
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	CLI:Set	username=sneezy password=SN33zy@!
	${OUTPUT}=	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: password: The password is too short. Minimum length: 12.
	[Teardown]	CLI:Cancel	Raw

Test adding user with password complexity=minimum_size valid
	[Setup]	SUITE:Set Passwd Rules	yes	MIN_UPPER_CASE_CHARACTERS=0	MIN_SPECIAL_CHARACTERS=0	MIN_DIGITS=0	MINIMUM_SIZE=${MINIMUM_SIZE}
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	CLI:Set	username=sneezy password=sneezysneezy
	CLI:Commit
	CLI:Test Show Command	sneezy
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete Users	sneezy

Test adding user with password complexity=min_digits invalid
	[Setup]	SUITE:Set Passwd Rules	yes	0	MIN_SPECIAL_CHARACTERS=0	MINIMUM_SIZE=${MINIMUM_SIZE}
	...	MIN_DIGITS=${MINIMUM_DIGITS}
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	CLI:Set	username=sneezy password=sneezysneezy
	${OUTPUT}=	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: password: Password minimum digits requirement: ${MINIMUM_DIGITS}
	[Teardown]	CLI:Cancel	Raw

Test adding user with password complexity=min_digits valid
	[Setup]	SUITE:Set Passwd Rules	yes	0	MIN_SPECIAL_CHARACTERS=0	MINIMUM_SIZE=${MINIMUM_SIZE}
	...	MIN_DIGITS=${MINIMUM_DIGITS}
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	CLI:Set	username=sneezy password=sneezysneezy1
	CLI:Commit
	CLI:Test Show Command	sneezy
	CLI:Delete Users	sneezy
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete Users	sneezy

Test adding user with password complexity=min_upper_case_characters invalid
	[Setup]	SUITE:Set Passwd Rules	yes	MIN_DIGITS=0	MIN_SPECIAL_CHARACTERS=0	MINIMUM_SIZE=${MINIMUM_SIZE}	MIN_UPPER_CASE_CHARACTERS=${MINIMUM_UPPERCASE}
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	CLI:Set	username=sneezy password=sneezysneezy1
	${OUTPUT}=	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: password: Password minimum uppercase characters requirement: ${MINIMUM_UPPERCASE}
	[Teardown]	CLI:Cancel	Raw

Test adding user with password complexity=min_upper_case_characters valid
	[Setup]	SUITE:Set Passwd Rules	yes	MIN_DIGITS=0	MIN_SPECIAL_CHARACTERS=0	MINIMUM_SIZE=${MINIMUM_SIZE}	MIN_UPPER_CASE_CHARACTERS=${MINIMUM_UPPERCASE}
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	CLI:Set	username=sneezy password=SNEEzysneezy1
	${OUTPUT}=	CLI:Commit	Raw
	Should Not Contain	${OUTPUT}	Error
	CLI:Test Show Command	sneezy
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete Users	sneezy

Test adding user with password complexity=min_special_characters invalid
	[Setup]	SUITE:Set Passwd Rules	yes	0	MIN_DIGITS=0	MINIMUM_SIZE=${MINIMUM_SIZE}	MIN_SPECIAL_CHARACTERS=${MINIMUM_SPECIAL}
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	CLI:Set	username=sneezy password=SNEEzysneezy1
	${OUTPUT}=	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error: password: Password minimum special characters requirement: ${MINIMUM_SPECIAL}
	[Teardown]	CLI:Cancel	Raw

Test adding user with password complexity=min_special_characters valid
	[Setup]	SUITE:Set Passwd Rules	yes	0	MIN_DIGITS=0	MINIMUM_SIZE=${MINIMUM_SIZE}	MIN_SPECIAL_CHARACTERS=${MINIMUM_SPECIAL}
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	CLI:Set	username=sneezy password=SNEEzy!@!@1
	CLI:Commit
	CLI:Test Show Command	sneezy
	CLI:Delete Users	sneezy
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete Users	sneezy

Test Set Passwd On Login
	[Tags]	NON-CRITICAL
	[Setup]	SUITE:Set Passwd Rules	yes	5	MIN_DIGITS=5	MINIMUM_SIZE=10	MIN_SPECIAL_CHARACTERS=3
	SUITE:Add User	${TEST_USERNAME}	${PASSWD_1}	yes
	CLI:Connect As Root
	Write	ssh ${TEST_USERNAME}@${HOST}
	Set Client Configuration	timeout=30
	${OUTPUT}	Read Until Regexp	Are you sure you want to continue connecting (yes/no/[fingerprint])?|Password:
	${YOU_SURE}	Run Keyword And Return Status	Should Contain	${OUTPUT}	Are you sure you want to continue connecting
	IF	${YOU_SURE}
		Write	yes
		Read Until	Password:
	ELSE
		Should Contain	${OUTPUT}	Password:
	END
	Write	${PASSWD_1}
	Read Until	Current password:
	Write	${PASSWD_1}
	Read Until	New password:
	Write	${QA_PASSWORD}
	Read Until	BAD PASSWORD: is too simple
	Wait Until Keyword Succeeds	5x	5s	Read Until	Password:
	Write	${PASSWD_1}
	Read Until	Current password:
	Write	${PASSWD_1}
	Read Until	New password:
	Write	${PASSWD_2}
	Read Until	Retype new password:
	Write	${PASSWD_2}
	Read until	]#
	CLI:Switch Connection	Default
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Delete Users	${TEST_USERNAME}	AND	CLI:Close Connection

Test Invalid Passwd in History
	[Tags]	NON-CRITICAL
	CLI:Open
	SUITE:Set Passwd Rules	yes	PASSWORDS_IN_HISTORY=2
	SUITE:Add User	${TEST_USERNAME}	${PASSWD_1}
	CLI:Connect As Root
	CLI:Write	passwd ${TEST_USERNAME} -e
	Write	ssh ${TEST_USERNAME}@${HOST}
	Read Until	Password:
	Write	${PASSWD_1}
	Read Until	Current password:
	Write	${PASSWD_1}
	Read Until	New password:
	Write	${QA_PASSWORD}
	Read Until	Retype new password:
	Write	${QA_PASSWORD}
	Read until	]#
	Write Bare	\x04
	CLI:Read Until Prompt
	CLI:Write	passwd ${TEST_USERNAME} -e
	Write	ssh ${TEST_USERNAME}@${HOST}
	Read Until	Password:
	Write	${QA_PASSWORD}
	Read Until	Current password:
	Write	${QA_PASSWORD}
	Read Until	New password:
	Write	${PASSWD_1}
	Read Until	Retype new password:
	Write	${PASSWD_1}
	Read Until	Password has been already used. Choose another.
	Read Until	Password:
	Write	${QA_PASSWORD}
	Read Until	Current password:
	Write	${QA_PASSWORD}
	Read Until	New password:
	Write	${PASSWD_2}
	Read Until	Retype new password:
	Write	${PASSWD_2}
	Read Until	]#
	Write Bare	\x04
	[Teardown]	CLI:Close Connection

Test Valid Passwd in History
	[Tags]	NON-CRITICAL
	CLI:Open
	CLI:Connect As Root
	CLI:Write	passwd ${TEST_USERNAME} -e
	Write	ssh ${TEST_USERNAME}@${HOST}
	Read Until	Password:
	Write	${PASSWD_2}
	Read Until	Current password:
	Write	${PASSWD_2}
	Read Until	New password:
	Write	${PASSWD_1}
	Read Until	Retype new password:
	Write	${PASSWD_1}
	Read until	]#
	[Teardown]	CLI:Close Current Connection	SWITCH_DEFAULT=Yes

Test PWD_EXPIRATION_MAX_DAYS
	[Tags]	NON-CRITICAL	BUG_NG_3769	BUG_NG_10476	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10
	SUITE:Set Passwd Rules	yes	PWD_EXPIRATION_MAX_DAYS=100
	SUITE:Change Date
	CLI:Connect As Root
	Write	ssh ${TEST_USERNAME}@${HOST}
	Read Until	Password:
	Write	${PASSWD_1}
	Read Until	Current password:
	Write	${PASSWD_1}
	Read Until	New password:
	Write	${QA_PASSWORD}
	Read Until	Retype new password:
	Write	${QA_PASSWORD}
	Read until	]#

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/local_accounts/
	CLI:Delete If Exists Confirm	sneezy

SUITE:Teardown
	CLI:Switch Connection	default
	${OUTPUT}=	Run Keyword And Return Status	CLI:Enter Path	/settings/password_rules/
	Run Keyword If	'${OUTPUT}' == '${FALSE}'	Run Keywords	CLI:Cancel
	...	AND	SUITE:Set Passwd Rules	PWD_EXPIRATION_MIN_DAYS=0	PWD_EXPIRATION_MAX_DAYS=99999	PWD_EXPIRATION_WARNING_DAYS=7
	CLI:Set	check_password_complexity=no
	CLI:Delete Users	sneezy	${TEST_USERNAME}
	SUITE:Change Date	${FALSE}
	CLI:Close Connection

SUITE:Change Date
	[Arguments]	${MANUAL}=${TRUE}
	CLI:Enter Path  /settings/date_and_time
	Run Keyword If	${MANUAL}	CLI:Set	date_and_time=manual
	...	ELSE	CLI:Set	date_and_time=network_time_protocol
	Run Keyword If	${MANUAL}	CLI:Set	year=2037
	CLI:Commit

SUITE:Add User
	[Arguments]	${TEST_USERNAME}	${PASSWORD}	${PASSWORD_CHANGE_FIRST_LOGIN}=no
	CLI:Enter Path	/settings/local_accounts/
	CLI:Add
	CLI:Set	username=${TEST_USERNAME} password=${PASSWORD} password_change_at_login=${PASSWORD_CHANGE_FIRST_LOGIN}
	CLI:Commit

SUITE:Set Passwd Rules
	[Arguments]	${CHECK_PASSWORD_COMPLEXITY}=no	${MIN_UPPER_CASE_CHARACTERS}=0	${PWD_EXPIRATION_MAX_DAYS}=0	${MIN_DIGITS}=1
	...	${MINIMUM_SIZE}=6	${PWD_EXPIRATION_MIN_DAYS}=0	${MIN_SPECIAL_CHARACTERS}=1	${PASSWORDS_IN_HISTORY}=1
	...	${PWD_EXPIRATION_WARNING_DAYS}=${EMPTY}
	CLI:Enter Path	/settings/password_rules/
	CLI:Set	check_password_complexity=${CHECK_PASSWORD_COMPLEXITY} pwd_expiration_max_days=${PWD_EXPIRATION_MAX_DAYS} pwd_expiration_min_days=${PWD_EXPIRATION_MIN_DAYS} pwd_expiration_warning_days=${PWD_EXPIRATION_WARNING_DAYS}
	Run keyword If	'${CHECK_PASSWORD_COMPLEXITY}' != 'no'	CLI:Set	minimum_size=${MINIMUM_SIZE} min_upper_case_characters=${MIN_UPPER_CASE_CHARACTERS}
	Run keyword If	'${CHECK_PASSWORD_COMPLEXITY}' != 'no'	CLI:Set	passwords_in_history=${PASSWORDS_IN_HISTORY} min_digits=${MIN_DIGITS} min_special_characters=${MIN_SPECIAL_CHARACTERS}
	CLI:Commit