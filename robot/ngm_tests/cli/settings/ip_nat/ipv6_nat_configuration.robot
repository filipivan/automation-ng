*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > IPV6 nat Configuration (Add chain/rule, edit rule, delete chain/rule)... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI	SSH	SHOW	SESSION

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

*** Test Cases ***
Test adding new chain
	CLI:Enter Path	/settings/ipv6_nat/chains
	CLI:Add
	CLI:Set Field	chain	test_chain
	CLI:Commit
	CLI:Test Show Command Regexp	test_chain\\s+-\\s+-\\s+-

Test deleting chain
	CLI:Enter Path	/settings/ipv6_nat/chains
	CLI:Test Ls Command	test_chain
	CLI:Delete If Exists	test_chain
	CLI:Test Not Show Command  test_chain

Test adding valid nat rule
	CLI:Enter Path	/settings/ipv6_nat/chains/PREROUTING
	CLI:Add
	CLI:Set	target=ACCEPT input_interface=lo
	CLI:Commit
	CLI:Test Show Command Regexp	0\\s+ACCEPT\\s+lo

Test set fields for valid nat rule
	CLI:Enter Path	/settings/ipv6_nat/chains/PREROUTING/0
	CLI:Test Show Command  target = ACCEPT	source_net6	destination_net6	protocol = numeric	protocol_number
	...	input_interface = lo	fragments = all_packets_and_fragments	reverse_match_for_source_ip|mask = no
	...	reverse_match_for_destination_ip|mask = no	reverse_match_for_source_port = no	reverse_match_for_destination_port = no
	...	reverse_match_for_protocol = no		reverse_match_for_tcp_flags = no	reverse_match_for_icmp_type = no
	...	reverse_match_for_input_interface = no	enable_state_match = no	to_source	to_destination
	...	log_level = debug	log_prefix	log_tcp_sequence_numbers = no	log_options_from_the_tcp_packet_header = no
	...	log_options_from_the_ip_packet_header = no

Test editing valid nat rule
	CLI:Enter Path  /settings/ipv6_nat/chains/PREROUTING/0
	CLI:Set Field  target 	LOG
	CLI:Commit
	CLI:Test Show Command  target = LOG	source_net6	destination_net6	protocol = numeric	protocol_number
	...	input_interface = lo	fragments = all_packets_and_fragments	reverse_match_for_source_ip|mask = no
	...	reverse_match_for_destination_ip|mask = no	reverse_match_for_source_port = no	reverse_match_for_destination_port = no
	...	reverse_match_for_protocol = no		reverse_match_for_tcp_flags = no	reverse_match_for_icmp_type = no
	...	reverse_match_for_input_interface = no	enable_state_match = no	to_source	to_destination
	...	log_level = debug	log_prefix	log_tcp_sequence_numbers = no	log_options_from_the_tcp_packet_header = no
	...	log_options_from_the_ip_packet_header = no

Test deleting valid nat rule
	CLI:Enter Path	/settings/ipv6_nat/chains/PREROUTING/
	CLI:Delete 	0
	CLI:Test Not Show Command  0

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/ipv6_nat/chains
	CLI:Delete If Exists	test_chain

SUITE:Teardown
	CLI:Enter Path	/settings/ipv6_nat/chains
	CLI:Delete If Exists	test_chain
	CLI:Close Connection