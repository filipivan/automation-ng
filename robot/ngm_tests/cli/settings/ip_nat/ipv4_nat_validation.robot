*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > IPV4 nat... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	NEED-REVIEW
Default Tags	CLI	SSH	SHOW	SESSION

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${ipv4_chain}	chain_ipv4
@{ALL_VALUES}=	${WORD}	${ONE_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
	...	${EIGHT_NUMBER}	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
	...	${ONE_POINTS}	${SEVEN_POINTS}	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
	...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}	${EMPTY}	${WORDS_SPACES}	${WORDS_TWO_SPACES}
	...	${WORDS_WITH_BARS}
@{IPV4_CHAINS_TYPES}	PREROUTING	INPUT	OUTPUT	POSTROUTING

*** Test Cases ***
Enter IPv4 nat directory
	CLI:Enter Path	/settings/ipv4_nat
	CLI:Test Ls Command	chains	policy

	CLI:Enter Path	chains
	CLI:Test Ls Command	INPUT	OUTPUT	PREROUTING	POSTROUTING

Test show command for chain directory
	CLI:Enter Path	/settings/ipv4_nat/chains
	CLI:Test Show Command	chain	policy	packets	bytes	INPUT	OUTPUT	PREROUTING	POSTROUTING

Test show_settings command
	CLI:Enter Path	/settings/ipv4_nat/
	${OUTPUT}=	CLI:Show Settings
	Should Match Regexp	${OUTPUT}	/settings/ipv4_nat/policy PREROUTING=ACCEPT
	Should Match Regexp	${OUTPUT}	/settings/ipv4_nat/policy INPUT=ACCEPT
	Should Match Regexp	${OUTPUT}	/settings/ipv4_nat/policy OUTPUT=ACCEPT
	Should Match Regexp	${OUTPUT}	/settings/ipv4_nat/policy POSTROUTING=ACCEPT

Test adding invalid value for chain
	CLI:Enter Path	/settings/ipv4_nat/chains
	CLI:Add
	CLI:Test Set Field Invalid Options	chain	~=	Error: chain: This field has invalid chain name.	yes
	[Teardown]  CLI:Cancel

Test set fields for Input
	CLI:Enter Path	/settings/ipv4_nat/chains/INPUT/
	CLI:Add
	CLI:Test Show Command	target	source_net4	destination_net4	protocol	protocol_number
	...	input_interface	fragments	reverse_match_for_source_ip|mask
	...	reverse_match_for_destination_ip|mask	reverse_match_for_source_port	reverse_match_for_destination_port
	...	reverse_match_for_protocol	reverse_match_for_tcp_flags	reverse_match_for_icmp_type
	...	reverse_match_for_input_interface
	...	log_level	log_prefix	log_tcp_sequence_numbers	log_options_from_the_tcp_packet_header	log_options_from_the_ip_packet_header
	...	to_destination	to_source
	CLI:Cancel

Test set fields for Output
	CLI:Enter Path  /settings/ipv4_nat/chains/OUTPUT/
	CLI:Add
	CLI:Test Show Command	target	source_net4	destination_net4	protocol	protocol_number
	...	output_interface	fragments	reverse_match_for_source_ip|mask
	...	reverse_match_for_destination_ip|mask	reverse_match_for_source_port	reverse_match_for_destination_port
	...	reverse_match_for_protocol	reverse_match_for_tcp_flags	reverse_match_for_icmp_type
	...	reverse_match_for_output_interface
	...	log_level	log_prefix	log_tcp_sequence_numbers	log_options_from_the_tcp_packet_header	log_options_from_the_ip_packet_header
	...	to_destination	to_source
	CLI:Cancel

Test add fields for nat
	CLI:Enter Path	/settings/ipv4_nat/chains/INPUT/
	CLI:Add
	CLI:Test Set Field Options	target	ACCEPT	LOG	RETURN
	CLI:Test Set Field Options	fragments	2nd_and_further_packets	all_packets_and_fragments	unfragmented_packets_and_1st_packets
	CLI:Test Set Field Options	input_interface	any	eth0	lo
	CLI:Test Set Field Options	log_level	debug	info	notice	warning	critical	alert	emergency
	CLI:Test Set Field Options	log_options_from_the_ip_packet_header	no	yes
	CLI:Test Set Field Options	log_options_from_the_tcp_packet_header	no	yes
	CLI:Test Set Field Options	log_tcp_sequence_numbers	no	yes
	CLI:Test Set Field Options	protocol	icmp	numeric	tcp	udp
	CLI:Test Set Field Options	reverse_match_for_destination_ip|mask	no	yes
	CLI:Test Set Field Options	reverse_match_for_source_ip|mask	no	yes
	CLI:Test Set Field Options	reverse_match_for_source_port	no	yes
	CLI:Test Set Field Options	reverse_match_for_destination_port	no	yes
	CLI:Test Set Field Options	reverse_match_for_protocol	no	yes
	CLI:Test Set Field Options	reverse_match_for_tcp_flags	no	yes
	CLI:Test Set Field Options	reverse_match_for_icmp_type	no	yes
	CLI:Test Set Field Options	reverse_match_for_input_interface	no	yes
	CLI:Cancel

Test invalid values for fields
	CLI:Enter Path	/settings/ipv4_nat/chains/INPUT/
	${COMMANDS}=	Create List	target	protocol
	...	input_interface	fragments	reverse_match_for_source_ip|mask
	...	reverse_match_for_destination_ip|mask	reverse_match_for_source_port	reverse_match_for_destination_port
	...	reverse_match_for_protocol	reverse_match_for_tcp_flags	reverse_match_for_icmp_type
	...	reverse_match_for_input_interface	#reverse_match_for_output_interface
	...	log_level	log_tcp_sequence_numbers	log_options_from_the_tcp_packet_header	log_options_from_the_ip_packet_header
	CLI:Add
	SUITE:Set Invalid Values Error Messages	${COMMANDS}
	[Teardown]	CLI:Cancel	Raw

Test invalid values for fields=protocol_number
	[Setup]	SUITE:Setup IPv4 NAT
	CLI:Enter Path	/settings/ipv4_nat/chains/${ipv4_chain}
	CLI:Add
	CLI:Test Set Field Invalid Options	protocol_number	a	Error: protocol_number: Invalid protocol number. Valid values are: udp, tcp, icmp/icmpv6, all or a number[0-255].	yes
	CLI:Test Set Field Invalid Options	protocol_number	~!@	Error: protocol_number: Invalid protocol number. Valid values are: udp, tcp, icmp/icmpv6, all or a number[0-255].	yes
	CLI:Test Set Field Invalid Options	protocol_number	${EXCEEDED}	Error: protocol_number: Invalid protocol number. Valid values are: udp, tcp, icmp/icmpv6, all or a number[0-255].	yes
	[Teardown]	CLI:Cancel	Raw

Test available values for fields=tcp_flag
	CLI:Enter Path	/settings/ipv4_nat/chains/INPUT/
	CLI:Add
	CLI:Set Field	protocol	tcp
	CLI:Test Set Field Options	tcp_flag_syn	any	set	unset
	CLI:Test Set Field Options	tcp_flag_ack	any	set	unset
	CLI:Test Set Field Options	tcp_flag_psh	any	set	unset
	CLI:Test Set Field Options	tcp_flag_fin	any	set	unset
	CLI:Test Set Field Options	tcp_flag_rst	any	set	unset
	CLI:Test Set Field Options	tcp_flag_urg	any	set	unset
	CLI:Test Set Field Options	reverse_match_for_tcp_flags	yes	no
	${COMMANDS}=	Create List	tcp_flag_ack	tcp_flag_psh	tcp_flag_syn	tcp_flag_fin	tcp_flag_rst	tcp_flag_urg	reverse_match_for_tcp_flags
	SUITE:Set Invalid Values Error Messages	${COMMANDS}
	[Teardown]	CLI:Cancel	Raw

Test invalid values for fields=source_port/destination_port
	[Setup]	SUITE:Setup IPv4 NAT
	CLI:Enter Path	/settings/ipv4_nat/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field  protocol	tcp
	CLI:Test Set Field Invalid Options	source_port	~!@	Error: source_port: Valid format: a number[1-65536], range(<number>:<number>) or list of numbers and ranges with comma separator.	yes
	CLI:Test Set Field Invalid Options	source_port	${EXCEEDED}	Error: source_port: Valid format: a number[1-65536], range(<number>:<number>) or list of numbers and ranges with comma separator.	yes
	CLI:Set Field  source_port 	${EMPTY}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	destination_port	~!@	Error: destination_port: Multiport match (list of numbers/ranges) does not support match for both source and destination ports simultaneously.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	destination_port	${EXCEEDED}	Error: destination_port: Multiport match (list of numbers/ranges) does not support match for both source and destination ports simultaneously.	yes
	[Teardown]	CLI:Cancel	Raw

Test available values for field=icmp_type
	CLI:Enter Path	/settings/ipv4_nat/chains/INPUT/
	CLI:Add
	CLI:Set Field	protocol	icmp
	CLI:Test Set Field Options	icmp_type	address_mask_reply	host_unreachable	source_quench
	...	address_mask_request	network_prohibited	source_route_failed
	...	any	network_redirect	time_exceeded
	...	bad_ip_header	network_unknown	timestamp_reply
	...	communication_prohibited	network_unreachable	timestamp_request
	...	destination_unreachable	parameter_problem	tos_host_redirect
	...	echo_reply	port_unreachable	tos_host_unreachable
	...	echo_request	precedence_cutoff	tos_network_redirect
	...	fragmentation_needed	protocol_unreachable	tos_network_unreachable
	...	host_precedence_violation	redirect	ttl_zero_during_reassembly
	...	host_prohibited	required_option_missing	ttl_zero_during_transit
	...	host_redirect	router_advertisement
	...	host_unknown	router_solicitation
	CLI:Test Set Field Options	reverse_match_for_icmp_type	yes	no
	${COMMANDS}=	Create List	icmp_type	reverse_match_for_icmp_type
	SUITE:Set Invalid Values Error Messages	${COMMANDS}
	[Teardown]  CLI:Cancel	Raw

Test invalid values for fields=source_udp_port/destination_udp_port
	[Setup]	SUITE:Setup IPv4 NAT
	CLI:Enter Path	/settings/ipv4_nat/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	protocol	udp
	CLI:Test Set Field Invalid Options	source_udp_port	~!@	Error: source_udp_port: Valid format: a number[1-65536], range(<number>:<number>) or list of numbers and ranges with comma separator.	yes
	CLI:Test Set Field Invalid Options	source_udp_port	${EXCEEDED}	Error: source_udp_port: Valid format: a number[1-65536], range(<number>:<number>) or list of numbers and ranges with comma separator.	yes
	CLI:Set Field  source_udp_port	${EMPTY}
	CLI:Test Set Field Invalid Options	destination_udp_port	~!@	Error: destination_udp_port: Multiport match (list of numbers/ranges) does not support match for both source and destination ports simultaneously.	yes
	CLI:Test Set Field Invalid Options	destination_udp_port	${EXCEEDED}	Error: destination_udp_port: Multiport match (list of numbers/ranges) does not support match for both source and destination ports simultaneously.	yes
	[Teardown]	CLI:Cancel	Raw

Test values for field=source_net4
	Skip If		'${NGVERSION}' == '3.2'	No validation messages on 3.2
	[Setup]	SUITE:Setup IPv4 NAT
	CLI:Enter Path	/settings/ipv4_nat/chains/${ipv4_chain}
	@{VALUES}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${TWO_WORD} 	${EXCEEDED}
	FOR		${VALUE}	IN	@{VALUES}
		CLI:Add
		CLI:Test Set Field Invalid Options	source_net4	${VALUE}	Error: source_net4: Validation error.	yes
		CLI:Cancel	Raw
	END
	[Teardown]	CLI:Cancel  Raw

Test with field=destination_net4
	Skip If		'${NGVERSION}' == '3.2'	No validation messages on 3.2
	[Setup]	SUITE:Setup IPv4 NAT
	CLI:Enter Path 	/settings/ipv4_nat/chains/${ipv4_chain}
	CLI:Add
	@{VALUES}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${TWO_WORD} 	${EXCEEDED}
	FOR		${VALUE}	IN	@{VALUES}
		CLI:Test Set Field Invalid Options	destination_net4	${VALUE}	Error: destination_net4: Validation error.	yes
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values - destination_net4 & source_net4
	[Setup]	SUITE:Setup IPv4 NAT
	CLI:Enter Path	/settings/ipv4_nat/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	target	ACCEPT
	CLI:Test Set Validate Normal Fields	source_net4	162.163.18.1/255.255.255.0
	CLI:Test Set Validate Normal Fields	destination_net4	162.163.18.2/255.255.255.0
	[Teardown]	SUITE:Teardown IPv4 NAT

Test with enable state match
	[Setup]	SUITE:Setup IPv4 NAT
	CLI:Enter Path	/settings/ipv4_nat/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	target	ACCEPT
	CLI:Test Set Validate Normal Fields	enable_state_match	yes	no
	CLI:Set Field	enable_state_match	yes
	# Tests for child fields enable_state_match
	CLI:Test Set Validate Normal Fields	new	yes	no
	CLI:Test Set Validate Normal Fields	established	yes	no
	CLI:Test Set Validate Normal Fields	related	yes	no
	CLI:Test Set Validate Normal Fields	invalid	no	yes
	CLI:Test Set Validate Normal Fields	dnat	no	yes
	CLI:Test Set Validate Normal Fields	snat	no	yes
	CLI:Test Set Validate Normal Fields	reverse_state_match	yes	no
	[Teardown]	CLI:Cancel	Raw
#
Test with protocol=numeric | protocol_number
	Skip If		'${NGVERSION}' == '3.2'	No validation messages on 3.2
	[Setup]	SUITE:Setup IPv4 NAT
	CLI:Enter Path	/settings/ipv4_nat/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	protocol	numeric
	${VALUES}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_POINTS}	${ONE_WORD}	${TWO_WORD}	${TWO_POINTS}	${EXCEEDED}
	FOR		${VALUE}	IN	@{VALUES}
		CLI:Test Set Field Invalid Options	protocol_number	${VALUE}	Error: protocol_number: Invalid protocol number. Valid values are: udp, tcp, icmp/icmpv6, all or a number[0-255].	yes
	END
	CLI:Test Set Field Invalid Options	protocol_number	${ONE_WORD} ${ONE_WORD}	Error: protocol_number: Invalid protocol number. Valid values are: udp, tcp, icmp/icmpv6, all or a number[0-255].	yes
	[Teardown]	CLI:Cancel	Raw

Test with protocol=tcp | source_port
	[Setup]	SUITE:Setup IPv4 NAT
	CLI:Enter Path	/settings/ipv4_nat/chains/${ipv4_chain}
	CLI:Add
	CLI:Set Field	protocol	tcp
	${VALUES}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_POINTS}	${ONE_WORD}	${TWO_WORD}	${TWO_POINTS}	${EXCEEDED}
	FOR	${VALUE}	IN	@{VALUES}
		CLI:Test Set Field Invalid Options	source_port	${VALUE}	Error: source_port: Valid format: a number[1-65536], range(<number>:<number>) or list of numbers and ranges with comma separator.	yes
	END
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	source_port	${ONE_WORD} ${ONE_WORD}	Error: source_port: Valid format: a number[1-65536], range(<number>:<number>) or list of numbers and ranges with comma separator.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	source_port	${ONE_WORD} ${ONE_WORD}	Error: source_port: Valid format: a number[1-65536], range(<number>:<number>) or list of numbers and ranges with comma separator.	yes
	[Teardown]	SUITE:Teardown IPv4 NAT

Test with protocol=tcp | destination_port
	[Setup]	SUITE:Setup IPv4 NAT
	CLI:Enter Path 	/settings/ipv4_nat/chains/${ipv4_chain}
	CLI:Add
	CLI:Set	protocol=tcp target=ACCEPT source_port=16219
	${VALUES}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_POINTS}	${ONE_WORD}	${TWO_WORD}	${TWO_POINTS}	${EXCEEDED}
	FOR		${VALUE}	IN	@{VALUES}
		Run Keyword If	'${NGVERSION}' > '4.2'	CLI:Test Set Field Invalid Options	destination_port	${VALUE}	Error: destination_port: Multiport match (list of numbers/ranges) does not support match for both source and destination ports simultaneously.	yes
	END
	[Teardown]	SUITE:Teardown IPv4 NAT

Test valid values - destination_port & source_port
	[Setup]	SUITE:Setup IPv4 NAT
	CLI:Enter Path	/settings/ipv4_nat/chains/${ipv4_chain}
	CLI:Add
	CLI:Set	protocol=tcp target=ACCEPT
	CLI:Test Set Validate Normal Fields	source_port	16219
	CLI:Test Set Validate Normal Fields	destination_port	16220
	[Teardown]	SUITE:Teardown IPv4 NAT

Test with protocol=udp | source_udp_port
	[Setup]	SUITE:Setup IPv4 NAT
	CLI:Enter Path 	/settings/ipv4_nat/chains/${ipv4_chain}
	CLI:Add
	CLI:Set	protocol=udp destination_udp_port=19216
	${VALUES}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_POINTS}	${ONE_WORD}	${TWO_WORD}	${TWO_POINTS}	${EXCEEDED}
	FOR		${VALUE}	IN	@{VALUES}
		CLI:Test Set Field Invalid Options	source_udp_port	${VALUE}	Error: source_udp_port:  Valid format: a number[1-65536], range(<number>:<number>) or list of numbers and ranges with comma separator.
	END
	[Teardown]	SUITE:Teardown IPv4 NAT

Test with protocol=udp | destination_udp_port
	[Setup]	SUITE:Setup IPv4 NAT
	CLI:Enter Path	/settings/ipv4_nat/chains/${ipv4_chain}
	CLI:Add
	CLI:Set	protocol=udp destination_udp_port=19217 target=ACCEPT
	${VALUES}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_POINTS}	${ONE_WORD}	${TWO_WORD}	${TWO_POINTS}	${EXCEEDED}
	FOR	${VALUE}	IN	@{VALUES}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	destination_udp_port	${VALUE}	Error: destination_udp_port: Multiport match (list of numbers/ranges) does not support match for both source and destination ports simultaneously.	yes
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	destination_udp_port	${VALUE}	Error: source_udp_port: Valid format: a number[1-65536], range(<number>:<number>) or list of numbers and ranges with comma separator.	yes
	END
	[Teardown]	SUITE:Teardown IPv4 NAT

Test valid values - destination_udp_port
	[Setup]	SUITE:Setup IPv4 NAT
	CLI:Enter Path	/settings/ipv4_nat/chains/${ipv4_chain}
	CLI:Add
	CLI:Set	protocol=udp target=ACCEPT
	CLI:Test Set Validate Normal Fields	destination_udp_port	162.163.18.1/255.255.255.0
	[Teardown]	SUITE:Teardown IPv4 NAT

Test invalid values - to_destination
	CLI:Enter Path	/settings/ipv4_nat/chains/PREROUTING
	CLI:Add
	CLI:Set Field	target	ACCEPT
	CLI:Test Set Field Invalid Options	to_destination	${ONE_WORD}	Error: to_destination: Target must be DNAT.	yes
	CLI:Set Field	target	DNAT
	${VALUES}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_POINTS}	${ONE_WORD}	${TWO_WORD}	${TWO_POINTS}	${EXCEEDED}
	FOR		${VALUE}	IN	@{VALUES}
		Run Keyword If	'${NGVERSION}' >= '5.2'	CLI:Test Set Field Invalid Options	to_destination	${VALUE}
		...	Error: to_destination: Valid format: <IP address>[-<IP address>][:<port>[-<port>]]	yes
		Run Keyword If	'${NGVERSION}' <= '5.0'	CLI:Test Set Field Invalid Options	to_destination	${VALUE}	Error: to_destination: Invalid IP address	yes
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values - to_source
	CLI:Enter Path  /settings/ipv4_nat/chains/POSTROUTING
	CLI:Add
	CLI:Set Field	target	ACCEPT
	CLI:Test Set Field Invalid Options	to_source	${ONE_WORD}		Error: to_source: Target must be SNAT.	yes
	CLI:Set Field	target	SNAT
	${VALUES}=	Create List	${WORD}	${NUMBER}	${POINTS}	${ONE_POINTS}	${ONE_WORD}	${TWO_WORD}	${TWO_POINTS}	${EXCEEDED}
	FOR		${VALUE}	IN	@{VALUES}
		Run Keyword If	'${NGVERSION}' >= '5.2'	CLI:Test Set Field Invalid Options	to_source	${VALUE}
		...	Error: to_source: Valid format: <IP address>[-<IP address>][:<port>[-<port>]]	yes
		Run Keyword If	'${NGVERSION}' <= '5.0'	CLI:Test Set Field Invalid Options	to_source	${VALUE}	Error: to_source: Invalid IP address	yes
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values - to_ports with protocol tcp
	CLI:Enter Path	/settings/ipv4_nat/chains/PREROUTING
	CLI:Add
	CLI:Set	protocol=tcp target=ACCEPT
	CLI:Test Set Field Invalid Options	to_ports	${NUMBER}	Error: to_ports: Target must be MASQUERADE or REDIRECT.	yes
	CLI:Set Field	target	REDIRECT
	${VALUES}=	Create List	${WORD}	${POINTS}	${ONE_POINTS}	${ONE_WORD}	${TWO_WORD}	${TWO_POINTS}
	FOR		${VALUE}	IN	@{VALUES}
		CLI:Test Set Field Invalid Options	to_ports	${VALUE}	Error: to_ports: This field only accepts integers.	yes
	END
	CLI:Test Set Field Invalid Options	to_ports	${EXCEEDED}	Error: to_ports: The value is out of range.	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values - to_ports with protocol udp
	CLI:Enter Path	/settings/ipv4_nat/chains/PREROUTING
	CLI:Add
	CLI:Set	protocol=udp target=ACCEPT
	CLI:Test Set Field Invalid Options	to_ports	${NUMBER}	Error: to_ports: Target must be MASQUERADE or REDIRECT.	yes
	CLI:Set Field	target	REDIRECT
	${VALUES}=	Create List	${WORD}	${POINTS}	${ONE_POINTS}	${ONE_WORD}	${TWO_WORD}	${TWO_POINTS}
	FOR		${VALUE}	IN	@{VALUES}
		CLI:Test Set Field Invalid Options	to_ports	${VALUE}	Error: to_ports: This field only accepts integers.	yes
	END
	CLI:Test Set Field Invalid Options	to_ports	${EXCEEDED}	Error: to_ports: The value is out of range.	yes
	[Teardown]	CLI:Cancel	Raw

Test field - reverse_match_for_source_ip|mask
	CLI:Enter Path	/settings/ipv4_nat/chains/PREROUTING
	CLI:Add
	CLI:Set	target=ACCEPT source_net4=${EMPTY}
	CLI:Test Set Field Invalid Options	reverse_match_for_source_ip|mask	yes		Error: reverse_match_for_source_ip|mask: Source IP/Mask field cannot be empty.	yes
	[Teardown]	CLI:Cancel	Raw

Test field - reverse_match_for_destination_ip|mask
	CLI:Enter Path  /settings/ipv4_nat/chains/PREROUTING
	CLI:Add
	CLI:Set	target=ACCEPT source_net4=${EMPTY}
	CLI:Test Set Field Invalid Options	reverse_match_for_destination_ip|mask	yes		Error: reverse_match_for_destination_ip|mask: Destination IP/Mask field cannot be empty.	yes
	[Teardown]	CLI:Cancel	Raw

Test field - reverse_match_for_source_port
	CLI:Enter Path	/settings/ipv4_nat/chains/PREROUTING
	CLI:Add
	CLI:Set	target=ACCEPT source_net4=${EMPTY} protocol=tcp
	CLI:Test Set Field Invalid Options	reverse_match_for_source_port	yes		Error: reverse_match_for_source_port: Source Port field cannot be empty.	yes
	[Teardown]	CLI:Cancel	Raw

Test field - reverse_match_for_destination_port
	CLI:Enter Path	/settings/ipv4_nat/chains/PREROUTING
	CLI:Add
	CLI:Set	target=ACCEPT source_net4=${EMPTY} protocol=tcp
	CLI:Test Set Field Invalid Options	reverse_match_for_destination_port	yes		Error: reverse_match_for_destination_port: Destination Port field cannot be empty.	yes
	[Teardown]	CLI:Cancel	Raw

Test field - reverse_match_for_protocol
	CLI:Enter Path	/settings/ipv4_nat/chains/PREROUTING
	CLI:Add
	CLI:Set	target=ACCEPT source_net4=${EMPTY} protocol=numeric protocol_number=${EMPTY}
	CLI:Test Set Field Invalid Options	reverse_match_for_protocol	yes		Error: reverse_match_for_protocol: Protocol Number field cannot be empty.	yes
	[Teardown]	CLI:Cancel	Raw

Test field - reverse_match_for_tcp_flags
	CLI:Enter Path	/settings/ipv4_nat/chains/PREROUTING
	CLI:Add
	CLI:Set	target=ACCEPT protocol=tcp tcp_flag_ack=any
	CLI:Test Set Field Invalid Options	reverse_match_for_tcp_flags		yes		Error: reverse_match_for_tcp_flags: TCP Flag field cannot be set to Any.	yes
	[Teardown]	CLI:Cancel	Raw

Test field - reverse_match_for_input_interface
	CLI:Enter Path  /settings/ipv4_nat/chains/PREROUTING
	CLI:Add
	CLI:Set	target=ACCEPT input_interface=any
	CLI:Test Set Field Invalid Options	reverse_match_for_input_interface	yes		Error: reverse_match_for_input_interface: Input Interface field cannot be set to Any.	yes
	[Teardown]	CLI:Cancel	Raw

Test field - reverse_match_for_output_interface
	CLI:Enter Path	/settings/ipv4_nat/chains/POSTROUTING
	CLI:Add
	CLI:Set	target=ACCEPT output_interface=any
	CLI:Test Set Field Invalid Options	reverse_match_for_output_interface	yes		Error: reverse_match_for_output_interface: Output Interface field cannot be set to Any.	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for fields=description
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	#error message not implemented on 3.2 and 4.2
	FOR	${IPV4_CHAINS_TYPE}	IN	@{IPV4_CHAINS_TYPES}
		CLI:Enter Path	/settings/ipv4_nat/chains/${IPV4_CHAINS_TYPE}/
		CLI:Add
		CLI:Test Set Field Invalid Options	description	${EXCEEDED}	Error: description: Validation error.	yes
		CLI:Cancel	Raw
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for fields=description
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	FOR	${IPV4_CHAINS_TYPE}	IN	@{IPV4_CHAINS_TYPES}
		SUITE:Test valid values for fields=description	${IPV4_CHAINS_TYPE}
	END
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/ipv4_nat/chains
	CLI:Delete If Exists	${ipv4_chain}

SUITE:Teardown
	CLI:Enter Path	/settings/ipv4_nat/chains
	CLI:Delete If Exists	${ipv4_chain}
	CLI:Close Connection

SUITE:Setup IPv4 NAT
	CLI:Enter Path	/settings/ipv4_nat/chains
	CLI:Delete If Exists	${ipv4_chain}
	CLI:Add
	CLI:Set	chain=${ipv4_chain}
	CLI:Commit

SUITE:Teardown IPv4 NAT
	CLI:Cancel	Raw
	CLI:Enter Path	/settings/ipv4_nat/chains/
	CLI:Delete If Exists	${ipv4_chain}

SUITE:Set Invalid Values Error Messages
	[Arguments]	${COMMANDS}
	FOR	${COMMAND}	IN	@{COMMANDS}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	a	Error: Invalid value: a for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value: ${COMMAND}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${COMMAND}	a	Error: Invalid value: a
	END
SUITE:Test valid values for fields=description
	[Arguments]	${PATH}
	CLI:Enter Path	/settings/ipv4_nat/chains/${PATH}/
	FOR	${VALUE}	IN	@{ALL_VALUES}
		CLI:Add
		CLI:Set	description="${VALUE}" rule_number=0
		CLI:Commit
		CLI:Test Show Command	${EMPTY}${VALUE}
		CLI:Delete	0
		CLI:Commit
	END