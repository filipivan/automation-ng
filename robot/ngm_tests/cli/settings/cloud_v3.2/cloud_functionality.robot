*** Settings ***
Resource	../../init.robot
Documentation	Functionality test cases to Cloud Feature available only for v3.2 throught the CLI.
...	The other versios has been renamed to cluster.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI		NON-CRITICAL	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10	EXCLUDEIN6_2
Default Tags	CLI	SSH	CLOUD

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${PSK}	automationtest
${CONNECTION}	ETH0
${DUMMY_DEVICE_NAME_CONNECTED_IN_COORDINATOR}	dummy_device_connected_in_coordinator
${DUMMY_DEVICE_NAME_CONNECTED_IN_PEER}	dummy_device_connected_in_peer
${DUMMY_DEVICES_TYPE}	device_console
${DUMMY_DEVICES_IP_ADDRESS}	127.0.0.1
${DUMMY_DEVICES_MODE}	enabled
${INTERFACE}	eth0

*** Test Cases ***
Test Access Dummy Device Connected In Peer Using SSH Ipv4 Method From Peer Through Coordinator
	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	SUITE:Wait Until Peer Included In Host Access
	${OUTPUT_PEER}=	SUITE:Access Device Connected On Peer Side Using Ssh
	Should Contain	${OUTPUT_PEER}	${HOSTPEER}
	[Teardown]	SUITE:Teardown To Access Test Cases

Test Access Dummy Device Connected on Peer Via Coordinator
	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	SUITE:Wait Until Peer Included In Host Access
	${OUTPUT_PEER}=	SUITE:Access Device Connected On Peer Via Coordinator
	Should Contain	${OUTPUT_PEER}	${HOSTPEER}
	[Teardown]	SUITE:Teardown To Access Test Cases

Test Get Ipv6 From Coordinator And Peer
	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	CLI:Switch Connection	PEER
	${PEER_IPV6}=	CLI:Get Interface Ipv6 Address	${CONNECTION}
	Set Suite Variable	${PEER_IPV6}
	CLI:Switch Connection	HOST
	${HOST_IPV6}=	CLI:Get Interface Ipv6 Address	${CONNECTION}
	Set Suite Variable	${HOST_IPV6}

Test Access Peer With SSH Method Using Ipv6
	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	CLI:Switch Connection	HOST
	SUITE:Wait Until Peer Included In Host Access
	${OUTPUT_PEER}=	SUITE:Access Device Connected On Peer Side Using Ssh	${HOST_IPV6}
	Should Contain	${OUTPUT_PEER}	${PEER_IPV6}
	[Teardown]	SUITE:Teardown To Access Test Cases

Test Access Peer In Cluster Without License On Both Sides
	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	CLI:Switch Connection	HOST
	CLI:Delete All License Keys Installed
	CLI:Switch Connection	PEER
	CLI:Delete All License Keys Installed
	Wait Until Keyword Succeeds	60s	3s	SUITE:Peer Should Not Exist On Coordinator Access

Test Access Peer In Cluster With Expired License On Both Sides
	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	CLI:Switch Connection    PEER
	CLI:Add License Key	${FIVE_DEVICES_CLUSTERING_LICENSE_1_EXPIRED}
	CLI:Switch Connection    HOST
	CLI:Add License Key	${FIVE_DEVICES_CLUSTERING_LICENSE_3_EXPIRED}
	Sleep	30s
	Wait Until Keyword Succeeds	15s	3s	SUITE:Peer Should Not Exist On Coordinator Access

*** Keywords ***
SUITE:Setup
	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	SUITE:Setup Coordinator
	SUITE:Setup Peer

SUITE:Teardown
	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	SUITE:Teardown To Access Test Cases
	CLI:Open	session_alias=HOST
	SUITE:Dsable Cloud
	CLI:Delete All License Keys Installed
	CLI:Delete All Devices
	CLI:Open	session_alias=PEER	HOST_DIFF=${HOSTPEER}
	SUITE:Dsable Cloud
	CLI:Delete All License Keys Installed
	CLI:Delete All Devices
	CLI:Close Connection

SUITE:Dsable Cloud
	CLI:Enter Path	/settings/cloud/
	CLI:Set	enable_cloud=no
	Set Client Configuration	timeout=45s
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Set	auto_enroll=yes
	CLI:Set	auto_psk=nodegrid-key
	CLI:Set	enable_peer_management=no
	CLI:Commit
	CLI:Test Show Command
	...	enable_cloud = no
	...	auto_enroll = yes
	...	auto_psk = nodegrid-key
	...	enable_peer_management = no
	[Teardown]	CLI:Revert	Raw

SUITE:Setup Coordinator
	CLI:Open	session_alias=HOST
	SUITE:Dsable Cloud
	CLI:Delete All License Keys Installed
	CLI:Delete All Devices
	CLI:Add License Key	${TEN_DEVICES_CLUSTERING_LICENSE}
	CLI:Add Device	DEVICE_NAME=${DUMMY_DEVICE_NAME_CONNECTED_IN_COORDINATOR}	DEVICE_TYPE=${DUMMY_DEVICES_TYPE}	MODE=${DUMMY_DEVICES_MODE}
	...	IP_ADDRESS=${DUMMY_DEVICES_IP_ADDRESS}	USERNAME=${USERNAME}	PASSWORD=${DEFAULT_PASSWORD}
	SUITE:Enable Cloud As Coordinator

SUITE:Setup Peer
	CLI:Open	session_alias=PEER	HOST_DIFF=${HOSTPEER}
	SUITE:Dsable Cloud
	CLI:Delete All License Keys Installed
	CLI:Delete All Devices
	CLI:Add License Key	${TEN_DEVICES_CLUSTERING_LICENSE_2}
	CLI:Add Device	DEVICE_NAME=${DUMMY_DEVICE_NAME_CONNECTED_IN_PEER}	DEVICE_TYPE=${DUMMY_DEVICES_TYPE}	MODE=${DUMMY_DEVICES_MODE}
	...	IP_ADDRESS=${DUMMY_DEVICES_IP_ADDRESS}	USERNAME=${USERNAME}	PASSWORD=${DEFAULT_PASSWORD}
	SUITE:Enable Cloud As Peer

SUITE:Enable Cloud As Coordinator
	${EXPECTED_SETUP}	Create List
	...	enable_cloud = yes
	...	cloud_name = nodegrid
	...	type = coordinator
	...	allow_enrollment = yes
	...	polling_rate = 30
	...	psk =
	...	enable_clustering = yes
	...	auto_enroll = yes
	...	auto_psk = nodegrid-key
	...	auto_interval = 30
	...	enable_peer_management = yes
	CLI:Enter Path	/settings/cloud/
	CLI:Set	enable_cloud=yes type=coordinator psk=${PSK}
	CLI:Commit
	CLI:Set	enable_clustering=yes
	CLI:Commit
	CLI:Set	enable_peer_management=yes
	CLI:Commit
	CLI:Test Show Command	@{EXPECTED_SETUP}
	[Teardown]	CLI:Revert	Raw

SUITE:Enable Cloud As Peer
	${EXPECTED_SETUP}	Create List
	...	enable_cloud = yes
	...	cloud_name = nodegrid
	...	type = peer
	...	coordinator_address = ${HOST}
	...	psk =
	...	enable_clustering = yes
	...	auto_enroll = yes
	...	auto_psk = nodegrid-key
	...	enable_peer_management = yes
	CLI:Enter Path	/settings/cloud/
	CLI:Set	enable_cloud=yes type=peer psk=${PSK} coordinator_address=${HOST}
	CLI:Commit
	CLI:Set	enable_clustering=yes
	CLI:Commit
	CLI:Set	enable_peer_management=yes
	CLI:Commit
	CLI:Test Show Command	@{EXPECTED_SETUP}
	[Teardown]	CLI:Revert	Raw

SUITE:Peer Should Not Exist On Coordinator Access
	CLI:Switch Connection	HOST
	CLI:Enter Path	/access/
	${OUTPUT}=	CLI:Write	show
	Should Not Contain	${OUTPUT}	${HOSTNAME_NODEGRID}.${DOMAINAME_NODEGRID}

SUITE:Read Until Regexp
	[Arguments]	${PROMPT}
	Set Client Configuration	timeout=60s
	${OUTPUT}=	Read Until Regexp	${PROMPT}	loglevel=INFO
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Log To Console	${OUTPUT}
	[Return]	${OUTPUT}

SUITE:Wait Until Peer Included In Host Access
	CLI:Switch Connection	HOST
	Wait Until Keyword Succeeds	60s	3s	SUITE:Check If Peer Exist In Host Access
	Sleep	30s

SUITE:Check If Peer Exist In Host Access
	CLI:Enter Path	/access/
	${OUTPUT}=	CLI:Write	show
	Should Contain	${OUTPUT}	${HOSTNAME_NODEGRID}.${DOMAINAME_NODEGRID}

SUITE:Access Device Connected On Peer Side Using Ssh
	[Arguments]	${HOST_IPV6}=${EMPTY}
	CLI:Switch Connection	HOST
	${STATUS}=	Run Keyword And Return Status	Wait Until Keyword Succeeds	120s	30s	SUITE:SSH	${HOST_IPV6}
	Run Keyword if	not ${STATUS}	Fail	Couldn't Access The Device
	Write Bare	\n
	${OUTPUT}=	SUITE:Read Until Regexp	(]#|Are you sure you want to continue connecting \\(.*\\)\\?)
	${STATUS}=	Run Keyword And Return Status	should contain	${OUTPUT}	Are you sure you want to continue connecting (yes/no)?
	Run Keyword If	${STATUS}	CLI:Write	yes
	${OUTPUT_PEER}=	CLI:Write	show /settings/network_connections/
	CLI:Write	\x05c.	Raw
	${OUTPUT}=	CLI:Write	exit
	[Return]	${OUTPUT_PEER}

SUITE:SSH
	[Arguments]	${HOST_IPV6}=${EMPTY}
	Run Keyword If	'${HOST_IPV6}' == '${EMPTY}'	CLI:Write	shell sudo ssh-keygen -f "/home/root/.ssh/known_hosts" -R "${HOST}"
	Run Keyword If	'${HOST_IPV6}' == '${EMPTY}'	Write	shell sudo ssh ${DEFAULT_USERNAME}:${DUMMY_DEVICE_NAME_CONNECTED_IN_PEER}@${HOST}
	Run Keyword If	'${HOST_IPV6}' != '${EMPTY}'	CLI:Write	shell sudo ssh-keygen -f "/home/root/.ssh/known_hosts" -R "${HOST_IPV6}"
	Run Keyword If	'${HOST_IPV6}' != '${EMPTY}'	Write	shell sudo ssh ${DEFAULT_USERNAME}:${DUMMY_DEVICE_NAME_CONNECTED_IN_PEER}@${HOST_IPV6}%${INTERFACE}
	${OUTPUT}=	Read Until Regexp	(Password:|Are you sure you want to continue connecting \\(.*\\)\\?)	loglevel=DEBUG
	Log To Console	\n${OUTPUT}
	${STATUS}=	Run Keyword And Return Status	Should Contain	${OUTPUT}	Password:
	Run Keyword If	${STATUS}	Write	${DEFAULT_PASSWORD}
	Run Keyword If	not ${STATUS}	Write	yes
	Run Keyword If	not ${STATUS}	Read Until Regexp	Password:
	Run Keyword If	not ${STATUS}	Write	${DEFAULT_PASSWORD}
	Set Client Configuration	timeout=120s
	${OUTPUT}=	Read Until Regexp	cli ]	loglevel=DEBUG
	Log To Console	${OUTPUT}
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Should Contain	${OUTPUT}	cli ]

SUITE:Access Device Connected On Peer Via Coordinator
	CLI:Enter Path	/access/${HOSTNAME_NODEGRID}.${DOMAINAME_NODEGRID}/${DUMMY_DEVICE_NAME_CONNECTED_IN_PEER}
	Write	connect
	${STATUS}=	Run Keyword And Return Status	SUITE:Read Until Regexp	cli ]
	Run Keyword if	not ${STATUS}	Fail	Couldn't Access The Device
	Write Bare	\n
	${OUTPUT}=	SUITE:Read Until Regexp	(]#|Are you sure you want to continue connecting \\(.*\\)\\?)
	${STATUS}=	Run Keyword And Return Status	should contain	${OUTPUT}	Are you sure you want to continue connecting (yes/no)?
	Run Keyword If	${STATUS}	CLI:Write	yes
	${OUTPUT_PEER}=	CLI:Write	show /settings/network_connections/
	CLI:Write	\x05c.	Raw
	${OUTPUT}=	CLI:Write	exit
	[Return]	${OUTPUT_PEER}

SUITE:Teardown To Access Test Cases
	CLI:Write	\x05c.	Raw
	Write	exit
	CLI:Close Connection
	CLI:Open	session_alias=HOST
	CLI:Open	session_alias=PEER	HOST_DIFF=${HOSTPEER}