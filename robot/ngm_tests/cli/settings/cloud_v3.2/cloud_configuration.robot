*** Settings ***
Resource	../../init.robot
Documentation	Configuration test cases to Cloud Feature available only for v3.2.
...	The other versios has been renamed to cluster.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI		NON-CRITICAL	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10	EXCLUDEIN6_2
Default Tags	CLI	SSH	CLOUD

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${PSK}	automationtest

*** Test Cases ***
Test Enable Cloud As Coordinator
	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	${EXPECTED_SETUP}	Create List
	...	enable_cloud = yes
	...	cloud_name = nodegrid
	...	type = coordinator
	...	allow_enrollment = yes
	...	polling_rate = 30
	...	psk =
	...	enable_clustering = yes
	...	auto_enroll = yes
	...	auto_psk = nodegrid-key
	...	auto_interval = 30
	...	enable_peer_management = yes
	CLI:Enter Path	/settings/cloud/
	${OUTPUT}=	CLI:Set	enable_cloud=yes type=coordinator psk=${PSK}
	CLI:Commit
	CLI:Set	enable_clustering=yes
	CLI:Commit
	CLI:Set	enable_peer_management=yes
	CLI:Commit
	Should Not Contain	${OUTPUT}	Error: Please set this nodes type as coordinator or peer
	CLI:Test Show Command	@{EXPECTED_SETUP}
	[Teardown]	Run Keywords	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	...	AND		SUITE:Dsable Cloud

Test Enable Cloud As Peer
	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	${EXPECTED_SETUP}	Create List
	...	enable_cloud = yes
	...	cloud_name = nodegrid
	...	type = peer
	...	coordinator_address = localhost
	...	psk =
	...	enable_clustering = yes
	...	auto_enroll = yes
	...	auto_psk = nodegrid-key
	...	enable_peer_management = yes
	CLI:Enter Path	/settings/cloud/
	${OUTPUT}=	CLI:Set	enable_cloud=yes type=peer psk=${PSK} coordinator_address=${HOST}
	CLI:Commit
	CLI:Set	enable_clustering=yes
	CLI:Commit
	CLI:Set	enable_peer_management=yes
	CLI:Commit
	Should Not Contain	${OUTPUT}	Error: Please set this nodes type as coordinator or peer
	CLI:Test Show Command	@{EXPECTED_SETUP}
	[Teardown]	Run Keywords	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	...	AND	SUITE:Dsable Cloud

*** Keywords ***
SUITE:Setup
	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	CLI:Open

SUITE:Teardown
	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	SUITE:Dsable Cloud
	CLI:Close Connection

SUITE:Dsable Cloud
	CLI:Enter Path	/settings/cloud/
	CLI:Set	enable_cloud=no
	CLI:Set	auto_enroll=yes
	CLI:Set	auto_psk=nodegrid-key
	CLI:Set	enable_peer_management=no
	CLI:Commit
	CLI:Test Show Command
	...	enable_cloud =	#BUG: enable_cloud field works once each two times it is changed.
	...	auto_enroll = yes
	...	auto_psk = nodegrid-key
	...	enable_peer_management = no
	[Teardown]	CLI:Revert	Raw