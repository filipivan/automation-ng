*** Settings ***
Resource	../../init.robot
Documentation	Validation test cases to Cloud Feature available only for v3.2.
...	The other versios has been renamed to cluster.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10	EXCLUDEIN6_2
Default Tags	CLI	SSH	CLOUD

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test available commands after send tab-tab
	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	CLI:Enter Path	/settings/cloud/
	CLI:Test Available Commands
	...	apply_settings       hostname             shell
	...	cd                   ls                   show
	...	change_password      pwd                  show_settings
	...	commit               quit                 shutdown
	...	event_system_audit   reboot               software_upgrade
	...	event_system_clear   revert               system_certificate
	...	exit                 save_settings        system_config_check
	...	factory_settings     set                  whoami

Test ls Command
	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	CLI:Enter Path	/settings/cloud/
	CLI:Test Ls Command	Try show command instead...

Test show_settings command
	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	${LIST}=	Create List
	...	/settings/cloud enable_cloud=no
	...	/settings/cloud auto_enroll=yes
	...	/settings/cloud auto_psk=nodegrid-key
	...	/settings/cloud enable_peer_management=no
	CLI:Enter Path	/settings/cloud/
	Write	show_settings
	${OUTPUT}=	Wait Until Keyword Succeeds	0m	15s	CLI:Read Until Prompt
	CLI:Should Contain All	${OUTPUT}	${LIST}

Test Show Cloud Settings
	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	CLI:Enter Path	/settings/cloud/
	CLI:Test Show Command
	...	enable_cloud = no
	...	auto_enroll = yes
	...	auto_psk = nodegrid-key
	...	enable_peer_management = no

Test Cloud Set Available Fields
	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	CLI:Enter Path	/settings/cloud/
	CLI:Test Set Available Fields
	...	enable_cloud
	...	auto_enroll
	...	auto_psk
	...	enable_peer_management
	[Teardown]	Run Keywords	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	...	AND	CLI:Revert	Raw

Test show after enable_cloud
	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	CLI:Enter Path	/settings/cloud/
	CLI:Set	enable_cloud=yes
	CLI:Test Show Command
	...	enable_cloud = yes
	...	cloud_name = nodegrid
	...	type = disabled
	...	psk =
	...	auto_enroll = yes
	...	auto_psk =
	...	enable_peer_management = no
	[Teardown]	Run Keywords	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	...	AND	CLI:Revert	Raw

Test fields available for type=coordinator
	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	CLI:Enter Path	/settings/cloud/
	CLI:Set	enable_cloud=yes type=coordinator
	CLI:Test Show Command
	...	enable_cloud = yes
	...	cloud_name = nodegrid
	...	type = coordinator
	...	allow_enrollment = yes
	...	polling_rate = 30
	...	psk =
	...	auto_enroll = yes
	...	auto_psk =
	...	auto_interval = 30
	...	enable_peer_management = no
	CLI:Test Set Available Fields	polling_rate	psk
	CLI:Test Set Field Invalid Options	allow_enrollment	${EMPTY}	Error: Missing value: allow_enrollment
	CLI:Test Set Field Invalid Options	allow_enrollment	a	Error: Invalid value: a
	CLI:Test Set Field Options	allow_enrollment	no	yes
	[Teardown]	Run Keywords	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	...	AND	CLI:Revert	Raw

Test fields available for type=peer
	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	CLI:Enter Path	/settings/cloud/
	CLI:Set	enable_cloud=yes type=peer
	CLI:Test Show Command
	...	enable_cloud = yes
	...	cloud_name = nodegrid
	...	type = peer
	...	coordinator_address =
	...	psk =
	...	auto_enroll = yes
	...	auto_psk =
	...	enable_peer_management = no
	CLI:Test Set Available Fields	coordinator_address	psk
	[Teardown]	Run Keywords	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	...	AND	CLI:Revert	Raw

*** Keywords ***
SUITE:Setup
	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	CLI:Open

SUITE:Teardown
	Skip If	'${NGVERSION}' != '3.2'	Cloud path available only for v3.2.
	CLI:Close Connection