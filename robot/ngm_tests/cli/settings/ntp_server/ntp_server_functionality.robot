*** Settings ***
Resource	../../init.robot
Documentation	Tests ntp server functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	NON-CRITICAL	NEED-REVIEW
Default Tags	CLI	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NTP_SERVER_IP}	${HOST}
${NTP_CLIENT_IP}	${HOSTPEER}
${ACCESSIBLE_NETWORK}	${FRE_NETWORK}
${NOT_ACCESSIBLE_NETWORK}	10.12.34.56/30
${STRATUM}	10
${DEFAULT_NTP_SERVER}	pool.ntp.org

*** Test Cases ***
Test NTP server using time source from NTP and not allowing client network
	SUITE:Enable NTP Server	${NOT_ACCESSIBLE_NETWORK}
	SUITE:Set Server Source To NTP
	SUITE:Check That NTP Server Is Unreachable

Test NTP server using time source from NTP and allowing client network
	SUITE:Enable NTP Server	${ACCESSIBLE_NETWORK}
	SUITE:Set Server Source To NTP
	SUITE:Check That NTP Server Is Reachable

Test NTP server using manual time source and not allowing client network
	SUITE:Enable NTP Server	${NOT_ACCESSIBLE_NETWORK}
	SUITE:Set Server Source To Manual
	SUITE:Check That NTP Server Is Unreachable

Test NTP server using manual time source and allowing client network
	SUITE:Enable NTP Server	${ACCESSIBLE_NETWORK}
	SUITE:Set Server Source To Manual
	SUITE:Check That NTP Server Is Reachable	${STRATUM}

Test disabled NTP server
	SUITE:Disable NTP Server
	SUITE:Check That NTP Server Is Unreachable

*** Keywords ***
SUITE:Setup
	CLI:Open	HOST_DIFF=${NTP_SERVER_IP}	session_alias=server_session
	CLI:Open	HOST_DIFF=${NTP_CLIENT_IP}	session_alias=client_session
	SUITE:Set NTP Server On Client	${NTP_SERVER_IP}

SUITE:Teardown
	SUITE:Disable NTP Server
	SUITE:Set Server Source To NTP
	SUITE:Set NTP Server On Client	${DEFAULT_NTP_SERVER}
	CLI:Close Connection

SUITE:Set Server Source To Manual
	CLI:Switch Connection	server_session
	CLI:Enter Path	/settings/date_and_time
	CLI:Set	date_and_time=manual
	CLI:Commit

SUITE:Set Server Source To NTP
	CLI:Switch Connection	server_session
	CLI:Enter Path	/settings/date_and_time
	CLI:Set	date_and_time=network_time_protocol server=${DEFAULT_NTP_SERVER}
	CLI:Commit

SUITE:Disable NTP Server
	CLI:Switch Connection	server_session
	CLI:Enter Path	/settings/ntp_server
	CLI:Set	enable_ntp_server=no
	CLI:Commit

SUITE:Enable NTP Server
	[Arguments]	${ALLOWED_NETWORKS}=0.0.0.0
	CLI:Switch Connection	server_session
	CLI:Enter Path	/settings/ntp_server
	CLI:Set	enable_ntp_server=yes allowed_networks=${ALLOWED_NETWORKS}
	CLI:Commit

SUITE:Check That NTP Server Is Reachable
	[Arguments]	${EXPECTED_STRATUM}=${EMPTY}
	CLI:Switch Connection	client_session
	Set Client Configuration	prompt=~#
	CLI:Write	shell sudo su -
	Wait Until Keyword Succeeds	5x	3s	SUITE:Test If NTP Server Is Reachable For CLient
	[Teardown]	Run Keywords	Set Client Configuration	prompt=]#	AND	CLI:Write	exit

SUITE:NTP Server Should Be Reachable
	[Arguments]	${STRATUM}=${EMPTY}
	${OUTPUT}=	CLI:Write	chronyc sources -v
	Should Match Regexp	${OUTPUT}	\\^\\*\\s+${NTP_SERVER_IP}\\s+${STRATUM}

SUITE:Check That NTP Server Is Unreachable
	CLI:Switch Connection	client_session
	Set Client Configuration	prompt=~#
	CLI:Write	shell sudo su -
	Wait Until Keyword Succeeds	5x	1s	SUITE:Restart NTP Daemon On Client
	Sleep	20s
	SUITE:NTP Server Should Be Unreachable
	[Teardown]	Run Keywords	Set Client Configuration	prompt=]#	AND	CLI:Write	exit

SUITE:NTP Server Should Be Unreachable
	${OUTPUT}=	CLI:Write	chronyc sources -v
	Should Match Regexp	${OUTPUT}	\\^\\?\\s+${NTP_SERVER_IP}

SUITE:Set NTP Server On Client
	[Arguments]	${NTP_SERVER_IP}
	CLI:Switch Connection	client_session
	CLI:Enter Path	/settings/date_and_time
	CLI:Set	date_and_time=network_time_protocol server=${NTP_SERVER_IP}
	CLI:Commit

SUITE:Restart NTP Daemon On Client
	CLI:Write	/etc/init.d/chronyd restart
	${OUTPUT}=	CLI:Write	chronyc sources -v
	Should Not Contain	${OUTPUT}	506 Cannot talk to daemon

SUITE:Test If NTP Server Is Reachable For CLient
	SUITE:Restart NTP Daemon On Client
	Sleep	10s
	SUITE:NTP Server Should Be Reachable