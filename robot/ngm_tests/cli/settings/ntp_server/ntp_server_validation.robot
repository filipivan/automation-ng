*** Settings ***
Resource	../../init.robot
Documentation	Tests ntp server validation
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${ONE_IP}	10.20.30.40
${ONE_NETWORK}	10.25.45.65/16
${TWO_IPS}	10.20.30.40,10.21.31.41
${TWO_NETWORKS}	10.25.45.65/16,10.26.46.66/24
${ONE_IP_ONE_NETWORK}	10.20.30.40,10.25.45.65/20
${INVALID_IPS}	10.20.30.40,10.21.41.610
${INVALID_IPS2}	10.20.300.40,10.21.41.61
${INVALID_IP}	10.200.300.0
${INVALID_NETWORK}	10.20.300.0/16
${INVALID_NETWORKS}	10.25.45.65/24,10.26.460.0/24
${INVALID_NETWORKS2}	10.25.45.65/33,10.15.30.45/31
@{ALL_VALUES}	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}	${EXCEEDED}

*** Test Cases ***
Test available commands after sending Tab-Tab
	CLI:Enter Path	/settings/ntp_server
	CLI:Test Available Commands	apply_settings	commit	event_system_clear
	...	ls	revert	show	system_certificate	cd	create_csr	exit	pwd
	...	save_settings	show_settings	system_config_check	change_password
	...	diagnostic_data	factory_settings	quit	set	shutdown	whoami
	...	cloud_enrollment	event_system_audit	hostname	reboot	shell
	...	software_upgrade
	Run Keyword If	${NGVERSION} >= 5.0	CLI:Test Available Commands
	...	export_settings	import_settings	exec

Test available fields
	CLI:Enter Path	/settings/ntp_server
	CLI:Test Set Available Fields	enable_ntp_server

Test valid values for field=enable_ntp_server
	CLI:Enter Path	/settings/ntp_server
	CLI:Test Set Field Options	enable_ntp_server	no	yes
	[Teardown]	CLI:Revert	Raw

Test invalid values for field=enable_ntp_server
	CLI:Enter Path	/settings/ntp_server
	CLI:Test Set Field Invalid Options	enable_ntp_server	${EMPTY}
	...	Error: Missing value for parameter: enable_ntp_server

	FOR	${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	enable_ntp_server	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: enable_ntp_server
	END
	[Teardown]	CLI:Revert	Raw

Test available fields after enabling NTP server
	CLI:Enter Path	/settings/ntp_server
	CLI:Set	enable_ntp_server=yes
	CLI:Test Set Available Fields	allowed_networks
	[Teardown]	CLI:Revert	Raw

Test valid values for field=allowed_networks
	${VALID_VALUES}=	Create List	${ONE_IP}	${ONE_NETWORK}	${TWO_IPS}
	...	${TWO_NETWORKS}	${ONE_IP_ONE_NETWORK}
	CLI:Set	enable_ntp_server=yes

	FOR	${VALID_VALUE}	IN	@{VALID_VALUES}
		CLI:Test Set Validate Normal Fields	allowed_networks	${VALID_VALUE}
	END
	[Teardown]	CLI:Revert	Raw

Test invalid falues for field=allowed_networks
	${INVALID_VALUES}=	Copy List	${ALL_VALUES}
	Append To List	${INVALID_VALUES}	${EMPTY}	${INVALID_IPS}
	...	${INVALID_IPS2}	${INVALID_IP}	${INVALID_NETWORK}	${INVALID_NETWORKS}
	...	${INVALID_NETWORKS2}
	CLI:Set	enable_ntp_server=yes

	FOR	${INVALID_VALUE}	IN	@{INVALID_VALUES}
		CLI:Test Set Field Invalid Options	allowed_networks	${INVALID_VALUE}
		...	Error: allowed_network: Validation error.
	END
	[Teardown]	CLI:Revert	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection