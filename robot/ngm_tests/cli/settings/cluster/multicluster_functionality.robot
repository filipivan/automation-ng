*** Settings ***
Resource	../../init.robot
Documentation	Suite to test multicluster functionality using CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	NON-CRITICAL
Default Tags	CLI	SSH	CLUSTER	MULTICLUSTER

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
#variables related to cluster configuration
${DEFAULT_PSK}	nodegrid-key
${LOCAL_COORD_HOSTNAME}	supercoordinator
${LOCAL_CLUSTER_NAME}	local
${LOCAL_COORDINATOR_LICENSE}	${FIVE_DEVICES_CLUSTERING_LICENSE_2}
${REMOTE_COORD_HOSTNAME}	coordinator
${REMOTE_PEER_HOSTNAME}	peer
${REMOTE_DOMAIN}	remotedomain
${REMOTE_CLUSTER_NAME}	remote
${REMOTE_COORDINATOR_LICENSE}	${TEN_DEVICES_CLUSTERING_LICENSE}
#variables related to devices configuration
${DEVICES_LICENSE}	${FIFTY_DEVICES_ACCESS_LICENSE}
${SUPERCOORD_DUMMY}	supercoord_itself
${NG_ITSELF_NAME}	coord_itself
${REMOTE_PEER_DUMMY}	peer_itself
${PDU_NAME}	${DUMMY_PDU_SERVERTECH_NAME}
${PDU_DEVICE}	${PDU_SERVERTECH}
${PDU_OUTLETS}	AA3,AB3,AC3
${ILO_DEVICE_NAME}	ilo5
${PERLE_NAME}	perleSCR
${RULE_NAME}	discovery_ports
${SENSOR_NAME}	TemperatureHumidity
#variables related to menu-driven configuration
${GROUP}	menu_group
${MENU_USER}	menu_user
${MENU_PASSWD}	test1234

*** Test Cases ***
Test join remote cluster in supercoordinator
	[Documentation]	Test case goes to supercoordinator and joind remote cluster by specifying coordinators information
	CLI:Switch Connection	supercoordinator
	CLI:Enter Path	/settings/cluster/cluster_clusters
	CLI:Write	join
	CLI:Set	coordinator_address=${HOSTPEER} remote_cluster_name=${REMOTE_CLUSTER_NAME} psk=${DEFAULT_PSK}
	Set Client Configuration	timeout=120s
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}

Test add managed devices in remote cluster updates supercoordinator
	[Documentation]	Test case adds for devices:
	...	One device console which is remote_coordinator itself, one PDU, one ILO and one Console Server and checks if all
	...	of them are showing in supercoordinator.
	CLI:Switch Connection	remote_coordinator
	CLI:Add License Key	${DEVICES_LICENSE}

	#device itself (device_console)
	CLI:Add Device	${NG_ITSELF_NAME}	device_console	${HOST}	${DEFAULT_USERNAME}	${FACTORY_PASSWORD}	on-demand

	#pdu_servertech (outlets)
	CLI:Add Device	${PDU_NAME}	${PDU_DEVICE}[type]	${PDU_DEVICE}[ip]
	...	${PDU_DEVICE}[username]	${PDU_DEVICE}[password]	on-demand
	CLI:Enter Path	/settings/devices/${PDU_NAME}/management
	CLI:Set	snmp=yes
	IF	${NGVERSION} >= 5.4
		CLI:Set	snmp_community=private
	ELSE
		CLI:Set	snmp_commmunity=private
	END
	CLI:Enter Path	/settings/devices/${PDU_NAME}/commands/outlet
	CLI:Set	enabled=yes protocol=snmp
	CLI:Commit
	Wait Until Keyword Succeeds	10x	10s	SUITE:Discover PDU Outlets

	#console_server_perle (discover child ports)
	CLI:Add Device	${PERLE_NAME}	${PERLE_SCR_TYPE}	${PERLE_SCR_SERVER_IP}
	...	${PERLE_SCR_SERVER_USERNAME}	${PERLE_SCR_SERVER_PASSWORD}	on-demand
	CLI:Add Rule	${RULE_NAME}	console_server_ports	${PERLE_NAME}	ACTION=clone_mode_on-demand
	Wait Until Keyword Succeeds	5x	10s	SUITE:Discover Perle Server Ports

	Wait Until Keyword Succeeds	5x	10s	SUITE:Check Access Was Updated In Supercoordinator
	[Teardown]	SUITE:Delete Discovery Rule

Test license leasing
	[Documentation]	Test checks if the license from remote coordinator was leased to remote peer (sanity)
	Skip If	not ${HAS_HOSTSHARED}	remote cluster has only coordinator
	CLI:Switch Connection	remote_coordinator
	${LICENSES_COORD}	CLI:Show	/settings/license
	Should Contain	${LICENSES_COORD}	leased to
	Should Contain	${LICENSES_COORD}	${HOSTSHARED}
	CLI:Switch Connection	remote_peer
	${LICENSES_PEER}	CLI:Show	/settings/license
	Should Contain	${LICENSES_PEER}	leased from
	Should Contain	${LICENSES_PEER}	${HOSTPEER}

Test cross-cluster search in multicluster
	[Documentation]	Test searches for term "itself" to check if devices from all nodes are seen in supercoordinator,
	...	if the devices from remote coordinator and remote peer are seen in remote coordinator and remote peer (in mesh)
	IF	${HAS_HOSTSHARED}
		CLI:Switch Connection	remote_peer
		CLI:Enter Path	/access/
		CLI:Write	search itself
		${SEARCH}	CLI:Show
		@{DEVICES_ITSELF}	Create List	${NG_ITSELF_NAME}	${REMOTE_PEER_DUMMY}
		CLI:Should Contain All	${SEARCH}	${DEVICES_ITSELF}
	END

	CLI:Switch Connection	remote_coordinator
	CLI:Enter Path	/access/
	CLI:Write	search itself
	${SEARCH}	CLI:Show
	@{DEVICES_ITSELF}	Create List	${NG_ITSELF_NAME}	${REMOTE_PEER_DUMMY}
	CLI:Should Contain All	${SEARCH}	${DEVICES_ITSELF}

	CLI:Switch Connection	supercoordinator
	CLI:Enter Path	/access/
	CLI:Write	search itself
	${SEARCH}	CLI:Show
	@{DEVICES_ITSELF}	Create List	${SUPERCOORD_DUMMY}	${NG_ITSELF_NAME}	${REMOTE_PEER_DUMMY}
	CLI:Should Contain All	${SEARCH}	${DEVICES_ITSELF}

Test accessing managed devices from remote cluster in supercoordinator
	[Documentation]	Tests connecting in the devices from remote coordinator using supercoordinator
	CLI:Switch Connection	supercoordinator
	CLI:Connect Device	${REMOTE_CLUSTER_NAME}/${REMOTE_COORD_HOSTNAME}.${REMOTE_DOMAIN}/${NG_ITSELF_NAME}
	CLI:Disconnect Device
	CLI:Write	exit

	CLI:Connect Device	${REMOTE_CLUSTER_NAME}/${REMOTE_COORD_HOSTNAME}.${REMOTE_DOMAIN}/${PERLE_NAME}
	CLI:Disconnect Device
	CLI:Write	exit

Test outlet operations in multicluster
	[Documentation]	Tests outlet operation from pdu_servertech in remote coordinator from supercoordinator
	CLI:Switch Connection	supercoordinator
	#variable to device path: remote/coordinator.remotedomain/dummy_dev.ice_servertech
	${DEVICE_PATH}=	Set Variable	${REMOTE_CLUSTER_NAME}/${REMOTE_COORD_HOSTNAME}.${REMOTE_DOMAIN}/${PDU_NAME}
	CLI:Enter Path	/access/${DEVICE_PATH}

	Set Client Configuration	timeout=60s
	CLI:Write	outlet_off ${PDU_OUTLETS}
	${STATUS}	CLI:Write	outlet_status ${PDU_OUTLETS}	user=Yes
	Should Contain X Times	${STATUS}	off	3
	Should Not Contain	${STATUS}	on

	CLI:Write	outlet_on ${PDU_OUTLETS}
	${STATUS}	CLI:Write	outlet_status ${PDU_OUTLETS}	user=Yes
	Should Contain X Times	${STATUS}	on	3
	Should Not Contain	${STATUS}	off

	CLI:Write	outlet_off ${PDU_OUTLETS}
	${STATUS}	CLI:Write	outlet_status ${PDU_OUTLETS}	user=Yes
	Should Contain X Times	${STATUS}	off	3
	Should Not Contain	${STATUS}	on

	CLI:Write	outlet_cycle ${PDU_OUTLETS}
	${STATUS}	CLI:Write	outlet_status ${PDU_OUTLETS}	user=Yes
	Should Contain X Times	${STATUS}	on	3
	Should Not Contain	${STATUS}	off
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}

Test USB sensors in multicluster
	[Documentation]	Tests USB sensors status commands in multicluster
	CLI:Switch Connection	supercoordinator
	${HAS_SENSOR}	CLI:Has USB Sensor
	Skip If	not ${HAS_SENSOR}	This build does not have a USB sensor
	CLI:Enter Path	/access/${SENSOR_NAME}
	${OUTPUT}	CLI:Write	usbsensors_status
	Should Contain	${OUTPUT}	${SENSOR_NAME}:0:temperaturehumidity_cn0:
	Should Contain	${OUTPUT}	${SENSOR_NAME}:1:temperaturehumidity_cn1:

Test menu-driven cluster in multicluster
	[Documentation]	Test creates configuration in all avaliable nodes and tests search  for term "itself"
	...	and accessing devices using menu-driven
	[Setup]	SUITE:Setup Menu-Driven For Multicluster Topology
	@{CONNECTIONS}	Create List	${HOST}	${HOSTPEER}
	Run Keyword If	${HAS_HOSTSHARED}	Append To List	${CONNECTIONS}	${HOSTSHARED}
	FOR	${CONNECTION}	IN	@{CONNECTIONS}
		CLI:Open	${MENU_USER}	${MENU_PASSWD}	HOST_DIFF=${CONNECTION}	TYPE=menudrivencluster	session_alias=menudriven
		@{DEVICES_EXPECT}	Create List
		Run Keyword If	'${CONNECTION}' == '${HOSTPEER}'	Append To List	${DEVICES_EXPECT}	${NG_ITSELF_NAME}	${REMOTE_PEER_DUMMY}
		Run Keyword If	'${CONNECTION}' == '${HOSTSHARED}'	Append To List	${DEVICES_EXPECT}	${REMOTE_PEER_DUMMY}
		Run Keyword If	'${CONNECTION}' == '${HOST}'	Append To List	${DEVICES_EXPECT}	${NG_ITSELF_NAME}	${REMOTE_PEER_DUMMY}	${SUPERCOORD_DUMMY}
		Write	s
		Read Until	Enter term to search:
		${DEVICES_ITSELF}	CLI:Write	itself
		CLI:Should Contain All	${DEVICES_ITSELF}	${DEVICES_EXPECT}
		Write	1
		Read Until	[Enter '^Ec.' to cli ]
		Write Bare	\n\n\n\n
		Write	\x05c.
		Write	exit
		${OUTPUT}	Read Until Regexp	(Enter an action:)|(]#)
		${IN_JAIL}	Run Keyword And Return Status	Should Contain	${OUTPUT}	]#
		Run Keyword If	${IN_JAIL}	CLI:Write	exit	Raw
	END
	[Teardown]	SUITE:Delete Menu-Driven Configurations For Multicluster

Test removing license from supercoordinator and checking the access
	[Documentation]	Tests delete all the licenses and checks that devices from other nodes are not accessible
	[Setup]	Run Keywords	CLI:Switch Connection	supercoordinator	AND	CLI:Delete All License Keys Installed

	Wait Until Keyword Succeeds	30s	5s	SUITE:Check Devices From Remote Cluster In Supercoordinator

	[Teardown]	CLI:Add License Key	${LOCAL_COORDINATOR_LICENSE}

Test removing remote peers from topology
	[Documentation]	Checks removing peer from remote cluster updates the topology in supercoordinator
	Skip If	not ${HAS_HOSTSHARED}	The build does not have a shared device so remote cluster does not have a peer
	SUITE:Check Remote Nodes

	CLI:Switch Connection	remote_coordinator
	CLI:Enter Path	/settings/cluster/cluster_peers/
	Wait Until Keyword Succeeds	3x	3s	CLI:Remove If Exists	${REMOTE_PEER_HOSTNAME}.${REMOTE_DOMAIN}
	${REMOTE_PEERS}	CLI:Show
	Should Not Contain	${REMOTE_PEERS}	${REMOTE_PEER_HOSTNAME}.${REMOTE_DOMAIN}

	Wait Until Keyword Succeeds	100s	10s	SUITE:Check Remote Nodes	REMOVED=True

Test automatic enroll in multicluster
	[Documentation]	Test that peer enrolls automatically in cluster and it gets updates in supercoordinator
	Skip If	not ${HAS_HOSTSHARED}	The build does not have a shared device so remote cluster does not have a peer
	CLI:Switch Connection	remote_coordinator
	CLI:Enter Path	/settings/cluster/cluster_enrollment_range
	CLI:Add
	CLI:Set	ip_range_start=${HOSTSHARED} ip_range_end=${HOSTSHARED}
	CLI:Commit
	Wait Until Keyword Succeeds	10x	10s	SUITE:Check If Peers Are Present On Coordinator
	SUITE:Check Remote Nodes

#Test access in star mode

*** Keywords ***
SUITE:Setup
	[Documentation]	Keyword to delete previous configurations and then configure:
	...	supercoordinator with new hostname, add license and configure cluster as coordinator
	...	remote_coordinator with new hostname and domain, add license and configure cluster as coordinator
	...	remote_peer with new hostname and domain and configure cluster as a peer from remote_coordinator
	...	Keyword also checks if the peer was really enrolled in the remote cluster.
	CLI:Open	HOST_DIFF=${HOST}	session_alias=supercoordinator
	SUITE:Delete Cluster Configuration	supercoordinator
	CLI:Change Hostname	${LOCAL_COORD_HOSTNAME}
	SUITE:Create Cluster Configuration	supercoordinator
	CLI:Add License Key	${LOCAL_COORDINATOR_LICENSE}
	CLI:Add Device	${SUPERCOORD_DUMMY}	device_console	127.0.0.1	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}

	CLI:Open	HOST_DIFF=${HOSTPEER}	session_alias=remote_coordinator
	SUITE:Delete Cluster Configuration	remote_coordinator
	CLI:Delete Rules	${RULE_NAME}
	CLI:Change Hostname	${REMOTE_COORD_HOSTNAME}
	CLI:Change Domain Name	${REMOTE_DOMAIN}
	SUITE:Create Cluster Configuration	remote_coordinator	CLUSTER_NAME=${REMOTE_CLUSTER_NAME}
	CLI:Add License Key	${REMOTE_COORDINATOR_LICENSE}

	IF	${HAS_HOSTSHARED}
		CLI:Open	HOST_DIFF=${HOSTSHARED}	session_alias=remote_peer
		SUITE:Delete Cluster Configuration	remote_peer
		CLI:Change Hostname	${REMOTE_PEER_HOSTNAME}
		CLI:Change Domain Name	${REMOTE_DOMAIN}
		SUITE:Create Cluster Configuration	remote_peer	TYPE=peer	CLUSTER_NAME=${REMOTE_CLUSTER_NAME}
		Wait Until Keyword Succeeds	5x	10s	SUITE:Check If Peers Are Present On Coordinator
		CLI:Add Device	${REMOTE_PEER_DUMMY}	device_console	127.0.0.1	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}
	END

SUITE:Teardown
	[Documentation]	Keyword deletes the cluster configurations, hostnames, domains and devices from supercoordinator,
	...	remote coordinators and remote peer
	SUITE:Delete Cluster Configuration	supercoordinator
	CLI:Delete All Devices

	SUITE:Delete Cluster Configuration	remote_coordinator
	CLI:Delete All Devices

	CLI:Switch Connection	remote_peer
	SUITE:Delete Cluster Configuration	remote_peer
	CLI:Change Hostname	${HOSTNAME_NODEGRID}
	CLI:Change Domain Name	${DOMAINAME_NODEGRID}
	CLI:Delete All Devices

	CLI:Close Connection

SUITE:Create Cluster Configuration
	[Documentation]	Keyword creates cluster configuration for nodes specifying the type, psk, mode and cluster name
	...	the keyword changes the needed parameters depending on the node. Such as: if the type is coordinator the lps type is server
	...	The topology is the following: HOST device is the supercoordinator and joins a remote cluster. The remote cluster
	...	has the HOSTPEER as remote_coordinator and the HOSTSHARED as remote_peer (if the build has SHARED device).
	[Arguments]	${CONNECTION}	${TYPE}=coordinator	${PSK}=${DEFAULT_PSK}	${MODE}=mesh	${CLUSTER_NAME}=${LOCAL_CLUSTER_NAME}
	CLI:Switch Connection	${CONNECTION}
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	CLI:Set	enable_cluster=yes psk=${PSK} enable_license_pool=yes
	CLI:Set	type=${TYPE}
	IF	'${TYPE}' == 'coordinator'
		CLI:Set	cluster_name=${CLUSTER_NAME} cluster_mode=${MODE} lps_type=server
	ELSE
		CLI:Set	cluster_name=${CLUSTER_NAME} coordinator_address=${HOSTPEER} lps_type=client
	END
	CLI:Set	enable_clustering_access=yes enable_peer_management=yes
	Set Client Configuration	timeout=120s
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	[Teardown]	CLI:Cancel	Raw

SUITE:Delete Cluster Configuration
	[Documentation]	Keywords disjoin the remote cluster in the supercoordinator, removes the peer if is the remote
	...	coordinator and disbales the cluster configuration and changes back the hostnames and domains to default.
	[Arguments]	${CONNECTION}
	CLI:Switch Connection	${CONNECTION}
	IF	'${CONNECTION}' == 'supercoordinator'
		CLI:Enter Path	/settings/cluster/cluster_clusters
		CLI:Write	disjoin ${REMOTE_CLUSTER_NAME}	Raw
		CLI:Change Hostname	${HOSTNAME_NODEGRID}
	ELSE IF	'${CONNECTION}' == 'remote_coordinator'
		CLI:Enter Path	/settings/cluster/cluster_peers
		CLI:Remove If Exists	${REMOTE_PEER_HOSTNAME}.${REMOTE_DOMAIN}
		${PEERS}	CLI:Show
		Should Not Contain	${PEERS}	${REMOTE_PEER_HOSTNAME}.${REMOTE_DOMAIN}
		CLI:Change Hostname	${HOSTNAME_NODEGRID}
		CLI:Change Domain Name	${DOMAINAME_NODEGRID}
	END
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	CLI:Set	enable_cluster=no
	Set Client Configuration	timeout=120s
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Delete All License Keys Installed
	CLI:Delete All Devices

SUITE:Check If Peers Are Present On Coordinator
	[Documentation]	Keyword checks on the peer is online on coordiantor side
	CLI:Switch Connection	remote_coordinator
	${PEERS}	CLI:Show	/settings/cluster/cluster_peers
	Should Contain	${PEERS}	${HOSTSHARED}
	Should Contain	${PEERS}	${REMOTE_PEER_HOSTNAME}.${REMOTE_DOMAIN}
	Should Not Contain	${PEERS}	Offline
	Should Not Contain	${PEERS}	Error

SUITE:Check Access Was Updated In Supercoordinator
	[Documentation]	Checks that all devices added in the remote coordinator are showing in supercoordinator access page
	CLI:Switch Connection	supercoordinator
	CLI:Enter Path	/access
	${SUPERCOORD_DEVICES}	CLI:Ls
	@{DEVICES_EXPECTED}	Create List	${REMOTE_CLUSTER_NAME}	${SUPERCOORD_DUMMY}
	CLI:Should Contain All	${SUPERCOORD_DEVICES}	${DEVICES_EXPECTED}
	CLI:Enter Path	/access/${REMOTE_CLUSTER_NAME}
	${REMOTE_CLUSTER_DEVICES}	CLI:Ls
	Should Contain	${REMOTE_CLUSTER_DEVICES}	${REMOTE_COORD_HOSTNAME}.${REMOTE_DOMAIN}
	Should Contain	${REMOTE_CLUSTER_DEVICES}	${REMOTE_PEER_HOSTNAME}.${REMOTE_DOMAIN}

SUITE:Discover PDU Outlets
	[Documentation]	Keyword configures necessary steps to discover the outlets of Servertech PDU
	CLI:Discover Now	${PDU_NAME}
	CLI:Enter Path	/settings/auto_discovery/discovery_logs
	${LOGS}	CLI:Show
	@{PDU_INFO}	Create List	${PDU_NAME}	PDU Outlets	Outlets Discovered
	CLI:Should Contain All	${LOGS}	${PDU_INFO}

SUITE:Discover Perle Server Ports
	[Documentation]	Keyword adds a discovery rule and discover PerleSCR console server ports
	CLI:Discover Now	${PERLE_NAME}
	CLI:Enter Path	/settings/auto_discovery/discovery_logs
	${LOGS}	CLI:Show
	Should Contain	${LOGS}	${PERLE_NAME}	Console Server Ports	Device Cloned

SUITE:Setup Menu-Driven For Multicluster Topology
	[Documentation]	Keyword creates a group which has menu-driven as startup application and a new
	...	local user that belong to this group for every node in the topology
	@{CONNECTIONS}	Create List	supercoordinator	remote_coordinator
	Run Keyword If	${HAS_HOSTSHARED}	Append To List	${CONNECTIONS}	remote_peer

	FOR	${CONNECTION}	IN	@{CONNECTIONS}
		CLI:Switch Connection	${CONNECTION}
		CLI:Enter Path	/settings/authorization
		CLI:Add
		CLI:Set	name=${GROUP}
		CLI:Commit
		CLI:Enter Path	/settings/authorization/${GROUP}/profile
		CLI:Set	menu-driven_access_to_devices=yes
		CLI:Commit
		CLI:Enter Path	/settings/local_accounts
		CLI:Add
		CLI:Set	username=${MENU_USER} password=${MENU_PASSWD} user_group=${GROUP}
		CLI:Commit
	END

SUITE:Delete Menu-Driven Configurations For Multicluster
	[Documentation]	Keyword deletes the configuration created by SUITE:Setup Menu-Driven For Multicluster Topology
	...	keyword for every node in the topology
	@{CONNECTIONS}	Create List	supercoordinator	remote_coordinator
	Run Keyword If	${HAS_HOSTSHARED}	Append To List	${CONNECTIONS}	remote_peer

	FOR	${CONNECTION}	IN	@{CONNECTIONS}
		CLI:Switch Connection	${CONNECTION}
		CLI:Enter Path	/settings/authorization
		CLI:Delete Confirm	${GROUP}
		CLI:Enter Path	/settings/local_accounts
		CLI:Delete Confirm	${MENU_USER}
	END

SUITE:Check Devices From Remote Cluster In Supercoordinator
	[Documentation]	Keyword checks the remote cluster is seen in supercoordinator
	CLI:Enter Path	/access
	${DEVICES_NO_LICENSE}	CLI:Show
	Should Not Contain	${DEVICES_NO_LICENSE}	${REMOTE_CLUSTER_NAME} seems like a bug, investigate further

SUITE:Check Remote Nodes
	[Documentation]	Keyword checks if the nodes under remote cluster are seen in supercoordinator. The argument REMOVED
	...	is removed to determine if the remote peer is expected be seen by supercoordinator or not
	[Arguments]	${REMOVED}=False
	CLI:Switch Connection	supercoordinator
	${REMOTE_NODES}	CLI:Show	/access/${REMOTE_CLUSTER_NAME}
	Should Contain	${REMOTE_NODES}	${REMOTE_COORD_HOSTNAME}.${REMOTE_DOMAIN}
	IF	'${REMOVED}' == 'False'
		Should Contain	${REMOTE_NODES}	${REMOTE_PEER_HOSTNAME}.${REMOTE_DOMAIN}
	ELSE
		Should Not Contain	${REMOTE_NODES}	${REMOTE_PEER_HOSTNAME}.${REMOTE_DOMAIN}
	END

SUITE:Delete Discovery Rule
	[Documentation]	Deletes the discovery rule for perle console ports and confirms it was deleted
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/
	CLI:Delete If Exists Confirm	${RULE_NAME}
	${RULES}	CLI:Show
	Should Not Contain	${RULES}	${RULE_NAME}