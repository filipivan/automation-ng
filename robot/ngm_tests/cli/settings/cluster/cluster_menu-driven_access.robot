*** Settings ***
Resource	../../init.robot
Documentation	Functionality tests to access devices in cluster environment using menu-driven interface
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	NON-CRITICAL
Default Tags	CLI	SSH	CLUSTER

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
#licenses:
${CLUSTER_LICENSE}	${TEN_DEVICES_CLUSTERING_LICENSE}
${ACCESS_LICENSE}	${ONE_HUNDRED_DEVICES_ACCESS_LICENSE}
#hostnames:
${HOSTNAME_COORDINATOR}	coordinator
${HOSTNAME_PEER}	peer
#devices:
${DUMMY_COORDINATOR}	coordinator_itself
${DUMMY_PEER}	peer_itself
${DUMMY_CLONED}	another_dummy
#cluster configuration:
${PSK}	nodegrid-key
#menu-driven configuration:
${USER}	menu_user
${GROUP}	menu_group
${MENU_PASSWD}	test

*** Test Cases ***
Test login with menu-driven and check pagination
	CLI:Switch Connection	menudriven_coordinator
	CLI:Write	n	#only to display devices in alphabetical order
	${LAST_PAGE}	CLI:Write	N
	Should Contain	${LAST_PAGE}	${DUMMY_COORDINATOR}
	Should Not Contain	${LAST_PAGE}	${DUMMY_CLONED}
	${LAST_AGAIN}	CLI:Write	N	#tries to do next page in last page, should not change the output
	Should Be Equal	${LAST_PAGE}	${LAST_AGAIN}
	${FIRST_PAGE}	CLI:Write	P
	Should Not Contain	${FIRST_PAGE}	${DUMMY_COORDINATOR}
	Should Contain X Times	${FIRST_PAGE}	${DUMMY_CLONED}	51
	${FIRST_AGAIN}	CLI:Write	P	#tries to do previous page in first page, should not change the output
	Should Be Equal	${FIRST_PAGE}	${FIRST_AGAIN}

Test order display commands in menu-driven cluster
	${ORDER_BY_ID}	CLI:Write	i
	Should Contain	${ORDER_BY_ID}	${DUMMY_COORDINATOR}
	Should Contain X Times	${ORDER_BY_ID}	${DUMMY_CLONED}	50
	${ORDER_BY_NAME}	CLI:Write	n
	Should Contain	${ORDER_BY_NAME}	${DUMMY_CLONED}	51
	Should Not Contain	${ORDER_BY_NAME}	${DUMMY_COORDINATOR}

Test show local devices in coordinator and peer
#coordinator side
	CLI:Write	l	#just to go back to initial state
	${ID_ORDER}	CLI:Write	i
	${LOCAL_BY_ID}	CLI:Write	l
	Should Be Equal	${ID_ORDER}	${LOCAL_BY_ID}	# l command should not change menu-driven output (BUG NG-8418)
	Should Not Contain	${LOCAL_BY_ID}	${DUMMY_PEER}
	${NAME_ORDER}	CLI:Write	n
	${LOCAL_BY_NAME}	CLI:Write	l
	Should Be Equal	${NAME_ORDER}	${LOCAL_BY_NAME}
	Should Not Contain	${LOCAL_BY_NAME}	${DUMMY_PEER}
#peer side
	CLI:Switch Connection	menudriven_peer
	CLI:Write	l	#just to go back to initial state
	${ID_ORDER}	CLI:Write	i
	${LOCAL_BY_ID}	CLI:Write	l
	Should Be Equal	${ID_ORDER}	${LOCAL_BY_ID}	# l command should not change menu-driven output (BUG NG-8418)
	Should Not Contain	${LOCAL_BY_ID}	${DUMMY_COORDINATOR}
	${NAME_ORDER}	CLI:Write	n
	${LOCAL_BY_NAME}	CLI:Write	l
	Should Be Equal	${NAME_ORDER}	${LOCAL_BY_NAME}
	Should Not Contain	${LOCAL_BY_NAME}	${DUMMY_COORDINATOR}

Test peers in cluster via menu-driven cluster
	CLI:Switch Connection	menudriven_peer
	${PEERS_IN_PEER}	CLI:Write	p
	Should Contain	${PEERS_IN_PEER}	${HOSTNAME_COORDINATOR}.${DOMAINAME_NODEGRID}
	CLI:Switch Connection	menudriven_coordinator
	${PEERS_IN_COORDINATOR}	CLI:Write	p
	Should Contain	${PEERS_IN_COORDINATOR}	${HOSTNAME_PEER}.${DOMAINAME_NODEGRID}

Test search devices
	CLI:Switch Connection	menudriven_coordinator
	Write	s
	Read Until	Enter term to search:
	${DEVICES_ITSELF}	CLI:Write	itself
	Should Contain	${DEVICES_ITSELF}	${HOSTNAME_COORDINATOR}.${DOMAINAME_NODEGRID}
	Should Contain	${DEVICES_ITSELF}	${DUMMY_COORDINATOR}
	Should Contain	${DEVICES_ITSELF}	${HOSTNAME_PEER}.${DOMAINAME_NODEGRID}
	Should Contain	${DEVICES_ITSELF}	${DUMMY_PEER}
# Do a search with few results and see "N" and "P" does not work (BUG NG-8418)
	${NEXT_PAGE}	CLI:Write	N
	${PREVIOUS_PAGE}	CLI:Write	P
	Should Be Equal	${NEXT_PAGE}	${PREVIOUS_PAGE}
# Do a blank search and see "n" and "i" command does not change the order of devices (BUG NG-8418)
	Write	s
	Read Until	Enter term to search:
	CLI:Write	_	#all devices have "_" so the result should list all devices in both coordinator and peer
	${ALL_DEVICES_ID}	CLI:Write	i
	${ALL_DEVICES_NAME}	CLI:Write	n
	Should Be Equal	${ALL_DEVICES_ID}	${ALL_DEVICES_NAME}

Test connect to local device in coordinator
	CLI:Switch Connection	menudriven_coordinator
	CLI:Write	l	#just to go back to initial state
	CLI:Write	i
	CLI:Write	n
	${NEXT_PAGE}	CLI:Write	N
	Should Contain	${NEXT_PAGE}	52: ${DUMMY_COORDINATOR}
	Write	52
	Read Until	[Enter '^Ec.' to cli ]
	Write Bare	\n\n\n\n
	Read Until	[${DEFAULT_USERNAME}@${HOSTNAME_COORDINATOR} /]#
	CLI:Disconnect Device

Test connect to peer device in coordinator
	CLI:Switch Connection	menudriven_coordinator
	Write	s
	Read Until	Enter term to search:
	CLI:Write	${DUMMY_PEER}
	Write	1
	Read Until	[Enter '^Ec.' to cli ]
	Write Bare	\n\n\n\n
	Read Until	[${DEFAULT_USERNAME}@${HOSTNAME_PEER} /]#
	Write	\x05c.
	Read Until	[${USER}@${HOSTNAME_PEER} ${DUMMY_PEER}]#
	CLI:Write	exit

Test connect to local device in peer
	CLI:Switch Connection	menudriven_peer
	CLI:Write	l	#just to go back to initial state
	Write	1
	Read Until	[Enter '^Ec.' to cli ]
	Write Bare	\n\n\n\n
	Read Until	[${DEFAULT_USERNAME}@${HOSTNAME_PEER} /]#
	CLI:Disconnect Device

Test connect to coordinator device in peer
	CLI:Switch Connection	menudriven_peer
	Write	s
	Read Until	Enter term to search:
	CLI:Write	${DUMMY_COORDINATOR}
	Write	1
	Read Until	[Enter '^Ec.' to cli ]
	Write Bare	\n\n\n\n
	Read Until	[${DEFAULT_USERNAME}@${HOSTNAME_COORDINATOR} /]#
	Write	\x05c.
	Read Until	[${USER}@${HOSTNAME_COORDINATOR} ${DUMMY_COORDINATOR}]#
	CLI:Write	exit

Test command to exit menu-driven cluster
#coordinator side
	CLI:Switch Connection	menudriven_coordinator
	Write	q
#peer side
	CLI:Switch Connection	menudriven_peer
	Write	q


*** Keywords ***
SUITE:Setup
	CLI:Open	session_alias=coordinator_session
	CLI:Add License Key	${CLUSTER_LICENSE}
	CLI:Add License Key	${ACCESS_LICENSE}
	CLI:Change Hostname	${HOSTNAME_COORDINATOR}
	CLI:Add Device	${DUMMY_COORDINATOR}	device_console	127.0.0.1	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}
	${DEVICES}	CLI:Show	/access/
	Should Contain	${DEVICES}	${DUMMY_COORDINATOR}
	Should Contain	${DEVICES}	Connected
	CLI:Clone Device	${DUMMY_COORDINATOR}	${DUMMY_CLONED}	number_of_clones=51	#enough to cause pagination on menu-driven
	SUITE:Configure Cluster	coordinator
	SUITE:Configure User With Menu-Driven Access
	CLI:Open	${USER}	${MENU_PASSWD}	session_alias=menudriven_coordinator	TYPE=menudrivencluster

	CLI:Open	HOST_DIFF=${HOSTPEER}	session_alias=peer_session
	CLI:Change Hostname	${HOSTNAME_PEER}
	CLI:Add Device	${DUMMY_PEER}	device_console	127.0.0.1	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}
	SUITE:Configure Cluster	peer
	SUITE:Configure User With Menu-Driven Access
	Wait Until Keyword Succeeds	100s	5s	SUITE:Check If Coordinator Devices Appeared In Peer
	CLI:Open	${USER}	${MENU_PASSWD}	session_alias=menudriven_peer	TYPE=menudrivencluster	HOST_DIFF=${HOSTPEER}
	CLI:Switch Connection	coordinator_session
	SUITE:Wait Until Peer Included In Host Access

SUITE:Teardown
	SUITE:Disable Cluster
	CLI:Delete All Devices
	SUITE:Delete User With Menu-Driven Access
	CLI:Change Hostname	${HOSTNAME_NODEGRID}
	CLI:Delete All License Keys Installed

	CLI:Switch Connection	peer_session
	CLI:Delete All Devices
	SUITE:Delete User With Menu-Driven Access
	CLI:Change Hostname	${HOSTNAME_NODEGRID}

	CLI:Close Connection

SUITE:Configure Cluster
	[Arguments]	${NODE}
	Run Keyword If	'${NODE}' == 'coordinator'	CLI:Switch Connection	coordinator_session
	Run Keyword If	'${NODE}' == 'peer'	CLI:Switch Connection	peer_session
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=yes type=${NODE} psk=${PSK}
	Run Keyword If	'${NODE}' == 'coordinator'	CLI:Set	cluster_mode=mesh
	Run Keyword If	'${NODE}' == 'peer'	CLI:Set	coordinator_address=${HOST}
	CLI:Set	enable_clustering_access=yes enable_peer_management=yes enable_license_pool=yes
	Run Keyword If	'${NODE}' == 'coordinator'	CLI:Set	lps_type=server	ELSE	CLI:Set	lps_type=client
	Set Client Configuration	60s
	CLI:Commit
	Set Client Configuration	${CLI_DEFAULT_TIMEOUT}
	Wait Until Keyword Succeeds	90s	10s	SUITE:Check Cluster Peers	${NODE}

SUITE:Check Cluster Peers
	[Arguments]	${NODE}
	CLI:Enter Path	/settings/cluster/cluster_peers
	${PEERS}	CLI:Show
	Should Contain	${PEERS}	${HOSTNAME_COORDINATOR}.${DOMAINAME_NODEGRID}
	Run Keyword If	'${NODE}' == 'peer'	Should Contain	${PEERS}	${HOSTNAME_PEER}.${DOMAINAME_NODEGRID}
	Should Not Contain	${PEERS}	Error
	Should Not Contain	${PEERS}	Offline

SUITE:Disable Cluster
	CLI:Switch Connection	coordinator_session
	CLI:Enter Path	/settings/cluster/cluster_peers
	CLI:Remove If Exists	${HOSTNAME_PEER}.${DOMAINAME_NODEGRID}
	CLI:Remove If Exists	nodegrid.${DOMAINAME_NODEGRID}
	${PEERS}	CLI:Show
	Should Not Contain	${PEERS}	${HOSTNAME_PEER}.${DOMAINAME_NODEGRID}
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	Set Client Configuration	timeout=120s
	CLI:Set	enable_cluster=no
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}

SUITE:Check If Coordinator Devices Appeared In Peer
	CLI:Switch Connection	peer_session
	CLI:Enter Path	/access
	${PEER_DEVICES}	CLI:Show
	Should Contain	${PEER_DEVICES}	${HOSTNAME_COORDINATOR}.${DOMAINAME_NODEGRID}

SUITE:Configure User With Menu-Driven Access
	CLI:Enter Path	/settings/authorization
	CLI:Add
	CLI:Set	name=${GROUP}
	CLI:Commit
	CLI:Enter Path	/settings/authorization/${GROUP}/profile
	CLI:Set	menu-driven_access_to_devices=yes
	CLI:Commit
	CLI:Enter Path	/settings/local_accounts
	CLI:Add
	CLI:Set	username=${USER} password=${MENU_PASSWD} user_group=${GROUP}
	CLI:Commit

SUITE:Delete User With Menu-Driven Access
	CLI:Enter Path	/settings/authorization
	CLI:Delete Confirm	${GROUP}
	CLI:Enter Path	/settings/local_accounts
	CLI:Delete Confirm	${USER}

SUITE:Wait Until Peer Included In Host Access
	CLI:Switch Connection	coordinator_session
	Wait Until Keyword Succeeds	60s	3s	SUITE:Check If Peer Exist In Host Access
	Sleep	30s

SUITE:Check If Peer Exist In Host Access
	CLI:Enter Path	/access/
	${OUTPUT}=	CLI:Write	show
	Should Contain	${OUTPUT}	${HOSTNAME_PEER}.localdomain