*** Settings ***
Resource	../../init.robot
Documentation	Suite to test configurations of fields related to multicluster in CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	NON-CRITICAL
Default Tags	CLI	SSH	CLUSTER	MULTICLUSTER

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEFAULT_PSK}	nodegrid-key
${REMOTE_CLUSTER_NAME}	remote
${LOCAL_CLUSTER_NAME}	local
${COORD_ADDRESS}	${HOSTPEER}

*** Test Cases ***
Test to join
	[Documentation]	Test to join a remote coordinator in a local cluster
	CLI:Switch Connection	local_coord
	CLI:Enter Path	/settings/cluster/cluster_clusters
	CLI:Write	join
	CLI:Set	coordinator_address=${COORD_ADDRESS} psk=${DEFAULT_PSK} remote_cluster_name=${REMOTE_CLUSTER_NAME}
	CLI:Add
	${JOINED_CLUSTERS}	CLI:Show
	Should Contain	${JOINED_CLUSTERS}	${HOSTNAME_NODEGRID}.${DOMAINAME_NODEGRID}
	Should Contain	${JOINED_CLUSTERS}	${COORD_ADDRESS}
	[Teardown]	CLI:Cancel	Raw

Test to add duplicate
	[Documentation]	Test adding the same remote coordinator again fails
	CLI:Switch Connection	local_coord
	CLI:Enter Path	/settings/cluster/cluster_clusters
	CLI:Write	join
	CLI:Set	coordinator_address=${COORD_ADDRESS} psk=${DEFAULT_PSK} remote_cluster_name=${REMOTE_CLUSTER_NAME}
	${OUTPUT}	CLI:Add	Yes
	Should Contain	${OUTPUT}	Error: Invalid cluster name.
	[Teardown]	CLI:Cancel	Raw

Test to disjoin
	[Documentation]	Tests disjoin a remote cluster from supercoordinator
	CLI:Switch Connection	local_coord
	CLI:Enter Path	/settings/cluster/cluster_clusters
	CLI:Write	disjoin ${REMOTE_CLUSTER_NAME}
	CLI:Commit	Raw
	${JOINED_CLUSTERS}	CLI:Show
	Should Not Contain	${JOINED_CLUSTERS}	${HOSTNAME_NODEGRID}.${DOMAINAME_NODEGRID}
	Should Not Contain	${JOINED_CLUSTERS}	${COORD_ADDRESS}
	[Teardown]	CLI:Cancel	Raw

Test to cancel
	[Documentation]	Tests canceling join operation and checking no remote cluster was added
	CLI:Switch Connection	local_coord
	CLI:Enter Path	/settings/cluster/cluster_clusters
	CLI:Write	join
	CLI:Set	coordinator_address=${COORD_ADDRESS} psk=${DEFAULT_PSK} remote_cluster_name=${REMOTE_CLUSTER_NAME}
	CLI:Cancel
	${JOINED_CLUSTERS}	CLI:Show
	Should Not Contain	${JOINED_CLUSTERS}	${HOSTNAME_NODEGRID}.${DOMAINAME_NODEGRID}
	Should Not Contain	${JOINED_CLUSTERS}	${COORD_ADDRESS}
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	[Documentation]	Adds cluster configuration as coordinator in both host and hostpeer
	CLI:Open	session_alias=local_coord
	CLI:Open	HOST_DIFF=${HOSTPEER}	session_alias=remote_coord
	SUITE:Add Cluster Configuration	local_coord	${LOCAL_CLUSTER_NAME}
	SUITE:Add Cluster Configuration	remote_coord	${REMOTE_CLUSTER_NAME}

SUITE:Teardown
	[Documentation]	Deletes cluster configuration in host and hostpeer
	SUITE:Remove Cluster Configuration	local_coord
	SUITE:Remove Cluster Configuration	remote_coord
	CLI:Close Connection

SUITE:Add Cluster Configuration
	[Documentation]	With connection as argument, it creates cluster configuration as coordinator.
	[Arguments]	${CONNECTION}	${NAME}
	CLI:Switch Connection	${CONNECTION}
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=yes type=coordinator psk=${DEFAULT_PSK}
	CLI:Set	cluster_name=${NAME}
	Set Client Configuration	timeout=120s
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}

SUITE:Remove Cluster Configuration
	[Documentation]	With connection as argument, it removes the cluster configuration by setting enable_cluster=no
	[Arguments]	${CONNECTION}
	CLI:Switch Connection	${CONNECTION}
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=no
	Set Client Configuration	timeout=120s
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}