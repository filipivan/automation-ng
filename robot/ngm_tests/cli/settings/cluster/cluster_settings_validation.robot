*** Settings ***
Resource	../../init.robot
Documentation	Validation test cases about Cluster Feature available for v4.2+ throught CLI.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI	SSH	CLUSTER

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${PSK}	automationtest

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Test Available Commands
	...	apply_settings       event_system_clear   save_settings
	...	cd                   exit                 shell
	...	change_password      factory_settings     show
	...	cloud_enrollment     hostname             show_settings
	...	commit               ls                   shutdown
	...	create_csr           pwd                  software_upgrade
	...	diagnostic_data      quit                 system_certificate
	...	edit                 reboot               system_config_check
	...	event_system_audit   revert               whoami

Test show_settings command
	CLI:Enter Path	/settings/cluster/settings/
	${OUTPUT}=	CLI:Show Settings
	Should Match Regexp	${OUTPUT}	/settings/cluster/settings enable_cluster=(no|yes)
	Should Match Regexp	${OUTPUT}	/settings/cluster/settings auto_enroll=(no|yes)
	Should Match Regexp	${OUTPUT}	/settings/cluster/settings auto_psk=nodegrid-key
	Should Match Regexp	${OUTPUT}	/settings/cluster/settings enable_peer_management=(no|yes)
	Should Match Regexp	${OUTPUT}	/settings/cluster/settings enable_license_pool=(no|yes)

Test Show Cluster Settings
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Test Show Command Regexp
	...	enable_cluster = (no|yes)
	...	auto_enroll = (no|yes)
	...	auto_psk = nodegrid-key
	...	enable_peer_management = (no|yes)
	...	enable_license_pool = (no|yes)

Test Cloud Set Available Fields
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	CLI:Test Available Commands	cancel	commit	ls	save	set	show
	CLI:Test Set Available Fields
	...	enable_cluster
	...	auto_enroll
	...	auto_psk
	...	enable_peer_management
	...	enable_license_pool
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test commit after enable_cluster
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	CLI:Test Set Field Invalid Options	enable_cluster	yes	Error: type: Please set this nodes type as coordinator or peer	yes
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test fields available for type=coordinator
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	CLI:Set	enable_cluster=yes type=coordinator
	CLI:Test Show Command Regexp
	...	enable_cluster = (no|yes)
	...	cluster_name = nodegrid
	...	type = coordinator
	...	allow_enrollment = (no|yes)
	...	cluster_mode = (mesh|star)
	...	polling_rate = 30
	...	psk =
	...	enable_clustering_access = (no|yes)
	...	auto_enroll = (no|yes)
	...	auto_psk = nodegrid-key
	...	enable_peer_management = (no|yes)
	...	enable_license_pool = (no|yes)
	...	lps_type = client
	CLI:Test Set Available Fields	cluster_name	cluster_mode	polling_rate	psk	lps_type
	CLI:Test Set Field Invalid Options	allow_enrollment	${EMPTY}	Error: Missing value for parameter: allow_enrollment
	CLI:Test Set Field Invalid Options	allow_enrollment	a	Error: Invalid value: a for parameter: allow_enrollment
	CLI:Test Set Field Options	allow_enrollment	no	yes
	CLI:Test Set Field Options	enable_clustering_access	no	yes
	CLI:Test Set Field Options	enable_peer_management	no	yes
	CLI:Test Set Field Options	enable_license_pool	no	yes
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test fields available for type=peer
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	CLI:Set	enable_cluster=yes type=peer
	CLI:Test Show Command Regexp
	...	enable_cluster = (no|yes)
	...	cluster_name = nodegrid
	...	type = peer
	...	coordinator_address =
	...	psk =
	...	enable_clustering_access = (no|yes)
	...	auto_enroll = (no|yes)
	...	auto_psk = nodegrid-key
	...	enable_peer_management = (no|yes)
	...	enable_license_pool = (no|yes)
	CLI:Test Set Available Fields	coordinator_address	psk
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test invalid settings for peer
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	CLI:Test Set Field Invalid Options	enable_cluster	yes	Error: type: Please set this nodes type as coordinator or peer	yes
	CLI:Test Set Field Invalid Options	type	peer	Error: psk: Field must not be empty.Error: coordinator_address: Field must not be empty.	yes
	CLI:Test Set Field Invalid Options	psk	coordinator_address	Error: coordinator_address: Field must not be empty.	yes
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test invalid values for polling rate
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	CLI:Set	enable_cluster=yes type=coordinator psk=${PSK}
	Run Keyword If	${NGVERSION} == 4.0	CLI:Test Set Field Invalid Options	polling_rate	$%20	Error: Invalid peer.	yes
	Run Keyword If	${NGVERSION} > 4.0	CLI:Test Set Field Invalid Options	polling_rate	$%20	Error: polling_rate: Must be a number between 10 and 600.	yes
	Run Keyword If	${NGVERSION} > 4.0	CLI:Test Set Field Invalid Options	polling_rate	${EMPTY}	Error: polling_rate: Field must not be empty.	yes
	Run Keyword If	${NGVERSION} > 4.0	CLI:Test Set Field Invalid Options	polling_rate	1	Error: polling_rate: Must be a number between 10 and 600.	yes
	Run Keyword If	${NGVERSION} > 4.0	CLI:Test Set Field Invalid Options	polling_rate	601	Error: polling_rate: Must be a number between 10 and 600.	yes
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test invalid values for cluster_mode
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=yes type=coordinator psk=${PSK}
	CLI:Test Set Field Invalid Options	cluster_mode	${EMPTY}	Error: Missing value for parameter: cluster_mode
	CLI:Test Set Field Invalid Options	cluster_mode	a	Error: Invalid value: a for parameter: cluster_mode
	CLI:Test Set Validate Normal Fields	cluster_mode	mesh	star
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test validation for fields=enable_cluster/auto_enroll/enable_peer_management/type
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	${COMMANDS}	Create List	enable_cluster	auto_enroll	enable_peer_management
	FOR	${COMMAND}	IN	@{COMMANDS}
		CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value for parameter: ${COMMAND}
		CLI:Test Set Field Invalid Options	${COMMAND}	a	Error: Invalid value: a for parameter: ${COMMAND}
	END
	CLI:Test Set Field Options	enable_cluster	no	yes
	CLI:Test Set Field Options	auto_enroll	no	yes
	CLI:Test Set Field Options	enable_peer_management	no	yes
	CLI:Test Set Field Options	type	coordinator	disabled	peer
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test validation for field=enable_license_pool
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	CLI:Test Set Field Invalid Options	enable_license_pool	${EMPTY}	Error: Missing value for parameter: enable_license_pool
	CLI:Test Set Field Invalid Options	enable_license_pool	a	Error: Invalid value: a for parameter: enable_license_pool
	CLI:Test Set Field Options	enable_license_pool	no	yes
	CLI:Set Field	enable_license_pool	yes
	CLI:Test Show Command	enable_license_pool = yes	lps_type
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test validation for field=lps_type
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	CLI:Set Field	enable_license_pool	yes
	CLI:Test Set Field Invalid Options	lps_type	${EMPTY}	Error: Missing value for parameter: lps_type
	CLI:Test Set Field Invalid Options	lps_type	a	Error: Invalid value: a for parameter: lps_type
	CLI:Test Set Field Options	lps_type	client	server
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test validation for fields=renew_time
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	CLI:Write	set enable_license_pool=yes lps_type=server
	CLI:Test Set Field Invalid Options	renew_time	${EMPTY}	Error: renew_time: The value must be between 1 and 7.	yes
	CLI:Test Set Field Invalid Options	renew_time	0	Error: renew_time: The value must be between 1 and 7.	yes
	CLI:Test Set Field Invalid Options	renew_time	a	Error: renew_time: The value must be between 1 and 7.	yes
	CLI:Test Set Field Invalid Options	renew_time	${EXCEEDED}	Error: renew_time: The value must be between 1 and 7.Error: lease_time: The value must be between 1 and 30.	yes
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test validation for fields=lease_time
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	CLI:Write	set enable_license_pool=yes lps_type=server
	CLI:Test Set Field Invalid Options	lease_time	${EMPTY}	Error: lease_time: The value must be between 1 and 30.	yes
	CLI:Test Set Field Invalid Options	lease_time	0	Error: lease_time: The value must be between 1 and 30.	yes
	CLI:Test Set Field Invalid Options	lease_time	a	Error: lease_time: The value must be between 1 and 30.	yes
	CLI:Test Set Field Invalid Options	lease_time	${EXCEEDED}		Error: lease_time: The value must be between 1 and 30.	yes
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test adding peer with invalid coordinator
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	CLI:Write	set enable_cluster=yes type=peer psk=${PSK} enable_clustering_access=yes
	CLI:Test Set Field Invalid Options	coordinator_address	invalid	Error: coordinator_address: Invalid IP address.	yes
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	SUITE:Disable Cluster
	CLI:Close Connection

SUITE:Disable Cluster
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	Set Client Configuration	timeout=120s
	CLI:Set	enable_cluster=no auto_enroll=yes auto_psk=nodegrid-key enable_peer_management=no
	CLI:Commit
	CLI:Test Show Command
	...	enable_cluster = no
	...	auto_enroll = yes
	...	auto_psk = nodegrid-key
	...	enable_peer_management = no
	...	enable_license_pool = yes
	...	lps_type = client
	[Teardown]	CLI:Cancel	Raw