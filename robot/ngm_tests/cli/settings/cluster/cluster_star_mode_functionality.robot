*** Settings ***
Resource	../../init.robot
Documentation	Functionality test cases to cluster Feature in Star mode throught the CLI.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI		EXCLUDEIN3_2	NON-CRITICAL
Default Tags	CLI	SSH	cluster

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${PSK}	automationtest
${CONNECTION}	ETH0
${DUMMY_DEVICE_NAME_CONNECTED_IN_COORDINATOR}	dummy_device_connected_in_coordinator
${DUMMY_DEVICE_NAME_CONNECTED_IN_PEER}	dummy_device_connected_in_peer
${DUMMY_DEVICES_TYPE}	device_console
${DUMMY_DEVICES_IP_ADDRESS}	127.0.0.1
${DUMMY_DEVICES_MODE}	enabled
${INTERFACE}	eth0

*** Test Cases ***
Test Access Dummy Device Connected In Peer Using SSH Ipv4 Method Via Coordinator
	Wait Until Keyword Succeeds	50s	4x	SUITE:Check If Peers Are Present On Coordinator
	SUITE:Wait Until Peer Included In Host Access
	${OUTPUT_PEER}=	SUITE:Access Device Connected On Peer Side Using Ssh
	Should Contain	${OUTPUT_PEER}	${HOSTPEER}
	[Teardown]	SUITE:Teardown To Access Test Cases

Test Access Dummy Device Connected on Peer Via Coordinator
	SUITE:Wait Until Peer Included In Host Access
	${OUTPUT_PEER}=	SUITE:Access Device Connected On Peer Via Coordinator
	Should Contain	${OUTPUT_PEER}	${HOSTPEER}
	[Teardown]	SUITE:Teardown To Access Test Cases

Test Get Ipv6 From Coordinator And Peer
	CLI:Switch Connection	PEER
	${PEER_IPV6}=	SUITE:Get Link-local Interface IPV6 Address	PEER	${INTERFACE}
	Set Suite Variable	${PEER_IPV6}
	CLI:Switch Connection	HOST
	${HOST_IPV6}=	SUITE:Get Link-local Interface IPV6 Address	HOST	${INTERFACE}
	Set Suite Variable	${HOST_IPV6}

Test Access Peer With SSH Method Using Ipv6
	CLI:Switch Connection	HOST
	SUITE:Wait Until Peer Included In Host Access
	${OUTPUT_PEER}=	SUITE:Access Device Connected On Peer Side Using Ssh	${HOST_IPV6}
	Should Contain	${OUTPUT_PEER}	${PEER_IPV6}
	[Teardown]	SUITE:Teardown To Access Test Cases

Test Peer Cannot Access Coordinator In Star Mode With License On Both Sides
	CLI:Switch Connection	PEER
	Wait Until Keyword Succeeds	5x	5s	SUITE:Check Missing Coordinator In Peer Access

Test Peer Manangement
	CLI:Switch Connection	HOST
	CLI:Enter Path	/settings/cluster/cluster_management/
	CLI:Write	software_upgrade 1
	${DEFAULT}	Create List
	...	image_location = remote_server
	...	url =
	...	username =
	...	password =
	...	the_path_in_url_to_be_used_as_absolute_path_name = no
	...	format_partitions_before_upgrade = no
	...	if_downgrading = restore_configuration_saved_on_version_upgrade
	...	The system will reboot automatically to complete upgrade process.
	CLI:Test Show Command Regexp	@{DEFAULT}
	[Teardown]	CLI:Cancel	Raw

Test Lease License to Peer
	CLI:Switch Connection	PEER
	CLI:Delete All License Keys Installed
	CLI:Switch Connection	HOST
	Wait Until Keyword Succeeds	15x	5s	SUITE:Check If The License Has Been Removed From Peer
	Wait Until Keyword Succeeds	15x	5s	SUITE:Check If The License Has Been Leased To Peer

Test Access Peer In Star Mode With Leased License To Peer
	CLI:Switch Connection	HOST
	Wait Until Keyword Succeeds	15x	5s	SUITE:Check If The License Has Been Leased To Peer
	Wait Until Keyword Succeeds	50s	4x	SUITE:Check If Peers Are Present On Coordinator
	SUITE:Wait Until Peer Included In Host Access
	${OUTPUT_PEER}=	SUITE:Access Device Connected On Peer Via Coordinator
	Should Contain	${OUTPUT_PEER}	${HOSTPEER}
	[Teardown]	SUITE:Teardown To Access Test Cases

Test Access Peer In Star Mode Without License On Both Sides
	CLI:Switch Connection    PEER
	CLI:Delete All License Keys Installed
	CLI:Switch Connection    HOST
	CLI:Delete All License Keys Installed
	Sleep	30s
	Wait Until Keyword Succeeds	5x	5s	SUITE:Check Missing Peer In Host Access

Test Access Peer In Star Mode With Expired License On Both Sides
	CLI:Switch Connection    PEER
	CLI:Delete All License Keys Installed
	CLI:Add License Key	${FIVE_DEVICES_CLUSTERING_LICENSE_1_EXPIRED}
	CLI:Switch Connection    HOST
	CLI:Delete All License Keys Installed
	CLI:Add License Key	${FIVE_DEVICES_CLUSTERING_LICENSE_3_EXPIRED}
	Sleep	30s
	Wait Until Keyword Succeeds	5x	5s	SUITE:Check Missing Peer In Host Access

*** Keywords ***
SUITE:Setup
	CLI:Open	session_alias=HOST
	SUITE:Remove Peer From Coordinator
	SUITE:Disable Cluster
	CLI:Delete All License Keys Installed
	SUITE:Change Domain Name Coordinator
	CLI:Delete All Devices
	CLI:Add License Key	${TEN_DEVICES_CLUSTERING_LICENSE}
	CLI:Add Device	DEVICE_NAME=${DUMMY_DEVICE_NAME_CONNECTED_IN_COORDINATOR}	DEVICE_TYPE=${DUMMY_DEVICES_TYPE}	MODE=${DUMMY_DEVICES_MODE}
	...	IP_ADDRESS=${DUMMY_DEVICES_IP_ADDRESS}	USERNAME=${USERNAME}	PASSWORD=${DEFAULT_PASSWORD}
	SUITE:Enable Cluster Coordinator
	CLI:Open	session_alias=PEER	HOST_DIFF=${HOSTPEER}
	SUITE:Disable Cluster
	CLI:Delete All License Keys Installed
	SUITE:Change Domain Name Peer
	CLI:Delete All Devices
	CLI:Add Device	DEVICE_NAME=${DUMMY_DEVICE_NAME_CONNECTED_IN_PEER}	DEVICE_TYPE=${DUMMY_DEVICES_TYPE}	MODE=${DUMMY_DEVICES_MODE}
	...	IP_ADDRESS=${DUMMY_DEVICES_IP_ADDRESS}	USERNAME=${USERNAME}	PASSWORD=${DEFAULT_PASSWORD}
	SUITE:Enable Cluster Peer

SUITE:Teardown
	SUITE:Teardown To Access Test Cases
	CLI:Switch Connection    HOST
	SUITE:Remove Peer From Coordinator
	SUITE:Disable Cluster
	CLI:Delete All License Keys Installed
	CLI:Delete All Devices
	CLI:Switch Connection    PEER
	SUITE:Disable Cluster
	CLI:Delete All License Keys Installed
	SUITE:Change To Default Domain Name
	CLI:Delete All Devices
	CLI:Close Connection

SUITE:Remove Peer From Coordinator
	CLI:Switch Connection	HOST
	CLI:Enter Path	/settings/cluster/cluster_peers/
	CLI:Remove If Exists	${HOSTNAME_NODEGRID}.peer
	${OUTPUT}=	CLI:Show
	Should Not Contain	${OUTPUT}	${HOSTNAME_NODEGRID}.peer

SUITE:Disable Cluster
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	CLI:Set	enable_cluster=no auto_enroll=yes auto_psk=nodegrid-key enable_peer_management=no
	CLI:Set	enable_license_pool=yes lps_type=client
	Set Client Configuration	timeout=120s
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	${DEFAULT_SETUP}	Create List
	...	enable_cluster = no
	...	auto_enroll = yes
	...	auto_psk = nodegrid-key
	...	enable_peer_management = no
	...	enable_license_pool = yes
	...	lps_type = client
	CLI:Test Show Command Regexp	@{DEFAULT_SETUP}
	[Teardown]	CLI:Cancel	Raw

SUITE:Change Domain Name Coordinator
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	domain_name=coordinator
	CLI:Commit

SUITE:Change Domain Name Peer
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	domain_name=peer
	CLI:Commit

SUITE:Change To Default Domain Name
	CLI:Switch Connection	HOST
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	domain_name=${DOMAINAME_NODEGRID}
	CLI:Commit
	CLI:Switch Connection	PEER
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	domain_name=${DOMAINAME_NODEGRID}
	CLI:Commit

SUITE:Enable Cluster Coordinator
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	CLI:Set	enable_cluster=yes type=coordinator allow_enrollment=yes psk=${PSK}
	Set Client Configuration	timeout=120s
	CLI:Commit
	CLI:Edit
	CLI:Set	cluster_mode=star polling_rate=30 auto_enroll=yes auto_psk=nodegrid-key psk=${PSK}
	CLI:Commit
	CLI:Edit
	CLI:Set	enable_clustering_access=yes enable_peer_management=yes enable_license_pool=yes lps_type=server psk=${PSK}
	CLI:Commit
	CLI:Edit
	CLI:Set	renew_time=1 lease_time=7
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	${EXPECTED_SETUP}	Create List
	...	enable_cluster = yes
	...	cluster( |_)name: nodegrid
	...	type = coordinator
	...	allow_enrollment = yes
	...	polling_rate = 30
	...	psk = automationtest
	...	enable_clustering_access = yes
	...	auto_enroll = yes
	...	auto_psk = nodegrid-key
	...	auto_interval = 30
	...	enable_peer_management = yes
	CLI:Test Show Command Regexp	@{EXPECTED_SETUP}
	[Teardown]	CLI:Cancel	Raw

SUITE:Enable Cluster Peer
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	CLI:Set	enable_cluster=yes type=peer coordinator_address=${HOST} psk=${PSK}
	Set Client Configuration	timeout=120s
	CLI:Commit
	CLI:Edit
	CLI:Set	enable_clustering_access=yes enable_peer_management=yes enable_license_pool=yes lps_type=client psk=${PSK}
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	${EXPECTED_SETUP}	Create List
	...	enable_cluster = yes
	...	cluster( |_)name: nodegrid
	...	type = peer
	...	coordinator_address = ${HOST}
	...	psk =
	...	enable_clustering_access = yes
	...	auto_enroll = yes
	...	auto_psk = nodegrid-key
	...	enable_peer_management = yes
	...	enable_license_pool = yes
	...	lps_type = client
	CLI:Test Show Command Regexp	@{EXPECTED_SETUP}
	[Teardown]	CLI:Cancel	Raw

SUITE:Check Missing Peer In Host Access
	CLI:Switch Connection	HOST
	CLI:Enter Path	/access/
	${OUTPUT}=	CLI:Write	show
	Should Not Contain	${OUTPUT}	${HOSTNAME_NODEGRID}.peer

SUITE:Check If Peer Exist In Host Access
	CLI:Enter Path	/access/
	${OUTPUT}=	CLI:Write	show
	Should Contain	${OUTPUT}	${HOSTNAME_NODEGRID}.peer

SUITE:Check Missing Coordinator In Peer Access
	CLI:Switch Connection	PEER
	CLI:Enter Path	/access/
	${OUTPUT}=	CLI:Write	show
	Should Not Contain	${OUTPUT}	${HOSTNAME_NODEGRID}.coordinator

SUITE:Check If The License Has Been Leased To Peer
	CLI:Switch Connection	HOST
	CLI:Enter Path	/settings/license/
	CLI:Test Show Command Regexp	${HOSTPEER}\\s+leased to

SUITE:Check If The License Has Been Removed From Peer
	CLI:Switch Connection	PEER
	CLI:Enter Path	/settings/license/
	${OUTPUT}=	CLI:Write	show
	Should Not Contain	${OUTPUT}	clustering
	Should Not Contain	${OUTPUT}	leased to

SUITE:Read Until Regexp
	[Arguments]	${PROMPT}
	Set Client Configuration	timeout=60s
	${OUTPUT}=	Read Until Regexp	${PROMPT}	loglevel=INFO
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Log To Console	${OUTPUT}
	[Return]	${OUTPUT}

SUITE:Wait Until Peer Included In Host Access
	CLI:Switch Connection	HOST
	Wait Until Keyword Succeeds	60s	3s	SUITE:Check If Peer Exist In Host Access
	Sleep	30s

SUITE:Access Device Connected On Peer Side Using Ssh
	[Arguments]	${HOST_IPV6}=${EMPTY}
	CLI:Switch Connection	HOST
	${STATUS}=	Run Keyword And Return Status	Wait Until Keyword Succeeds	120s	30s	SUITE:SSH	${HOST_IPV6}
	Run Keyword if	not ${STATUS}	Fail	Couldn't Access The Device
	Write Bare	\n
	${OUTPUT}=	SUITE:Read Until Regexp	(]#|Are you sure you want to continue connecting \\(.*\\)\\?)
	${STATUS}=	Run Keyword And Return Status	should contain	${OUTPUT}	Are you sure you want to continue connecting (yes/no)?
	Run Keyword If	${STATUS}	CLI:Write	yes
	${OUTPUT_PEER}=	CLI:Write	shell ifconfig
	CLI:Write	\x05c.	Raw
	${OUTPUT}=	CLI:Write	exit
	[Return]	${OUTPUT_PEER}

SUITE:SSH
	[Arguments]	${HOST_IPV6}=${EMPTY}
	Run Keyword If	'${HOST_IPV6}' == '${EMPTY}'	Write	shell sudo ssh ${DEFAULT_USERNAME}:${DUMMY_DEVICE_NAME_CONNECTED_IN_PEER}@${HOST}
	Run Keyword If	'${HOST_IPV6}' != '${EMPTY}'	Write	shell sudo ssh ${DEFAULT_USERNAME}:${DUMMY_DEVICE_NAME_CONNECTED_IN_PEER}@${HOST_IPV6}%${INTERFACE}
	${OUTPUT}=	Read Until Regexp	(Password:|Are you sure you want to continue connecting \\(.*\\)\\?)	loglevel=DEBUG
	Log To Console	\n${OUTPUT}
	${STATUS}=	Run Keyword And Return Status	Should Contain	${OUTPUT}	Password:
	Run Keyword If	${STATUS}	Write	${DEFAULT_PASSWORD}
	Run Keyword If	not ${STATUS}	Write	yes
	Run Keyword If	not ${STATUS}	Read Until Regexp	Password:
	Run Keyword If	not ${STATUS}	Write	${DEFAULT_PASSWORD}
	Set Client Configuration	timeout=120s
	${OUTPUT}=	Read Until Regexp	cli ]	loglevel=DEBUG
	Log To Console	${OUTPUT}
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Should Contain	${OUTPUT}	cli ]

SUITE:Access Device Connected On Peer Via Coordinator
	CLI:Switch Connection	HOST
	CLI:Enter Path	/access/${HOSTNAME_NODEGRID}.peer/${DUMMY_DEVICE_NAME_CONNECTED_IN_PEER}
	Write	connect
	${STATUS}=	Run Keyword And Return Status	SUITE:Read Until Regexp	cli ]
	Run Keyword if	not ${STATUS}	Fail	Couldn't Access The Device
	Write Bare	\n
	${OUTPUT}=	SUITE:Read Until Regexp	(]#|Are you sure you want to continue connecting \\(.*\\)\\?)
	${STATUS}=	Run Keyword And Return Status	should contain	${OUTPUT}	Are you sure you want to continue connecting (yes/no)?
	Run Keyword If	${STATUS}	CLI:Write	yes
	${OUTPUT_PEER}=	CLI:Write	shell ifconfig
	CLI:Write	\x05c.	Raw
	${OUTPUT}=	CLI:Write	exit
	[Return]	${OUTPUT_PEER}

SUITE:Teardown To Access Test Cases
	CLI:Write	\x05c.	Raw
	Write	exit
	CLI:Close Connection
	CLI:Open	session_alias=HOST
	CLI:Open	session_alias=PEER	HOST_DIFF=${HOSTPEER}

SUITE:Get Link-local Interface IPV6 Address
	[Arguments]	${SESSION}	${INTERFACE}
	CLI:Switch Connection	${SESSION}
	${LINK_LOCAL_IPV6}	CLI:Write	shell ip a show eth0 | grep fe80 | cut -c 11-	user=Yes
	${LINK_LOCAL_IPV6}	Remove String Using Regexp	${LINK_LOCAL_IPV6}	/\\d\\d\\sscope\\slink\\snoprefixroute\\s
	[Return]	${LINK_LOCAL_IPV6}

SUITE:Check If Peers Are Present On Coordinator
	CLI:Switch Connection	HOST
	${PEERS}	CLI:Show	/settings/cluster/cluster_peers
	Should Contain	${PEERS}	${HOSTPEER}
	Should Contain	${PEERS}	${HOSTNAME_NODEGRID}.peer
	Should Not Contain	${PEERS}	Offline
	Should Not Contain	${PEERS}	Error