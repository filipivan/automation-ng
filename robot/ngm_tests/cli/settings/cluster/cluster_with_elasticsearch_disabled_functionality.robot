*** Settings ***
Resource	../../init.robot
Documentation	Test cluster feature integration with Profiling NG to mid or low enviroment feature
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	NON-CRITICAL
Default Tags	CLI	SSH	CLUSTER

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${LICENSE_KEY}	${FIVE_DEVICES_CLUSTERING_LICENSE_2}
${PSK}	nodegrid-key
${DEV_COORDINATOR}	test-dummy-coordinator
${DEV_PEER}	test-dummy-peer
${HOSTNAME_PEER}	peer1

*** Test Cases ***
Test if elasticsearch is enabled in both nodes
	CLI:Switch Connection	coordinator
	${ELASTICSEARCH_ENABLED_COORDINATOR}	CLI:Show	/settings/services enable_elasticsearch
	Should Contain	${ELASTICSEARCH_ENABLED_COORDINATOR}	enable_elasticsearch = yes

	CLI:Switch Connection	peer
	${ELASTICSEARCH_ENABLED_PEER}	CLI:Show	/settings/services enable_elasticsearch
	Should Contain	${ELASTICSEARCH_ENABLED_PEER}	enable_elasticsearch = yes

Test if cluster mesh mode is working with elasticsearch enabled
	SUITE:Configure Cluster On Coordinator	mesh
	CLI:Switch Connection	coordinator
	CLI:Add Device	${DEV_COORDINATOR}
	SUITE:Configure Cluster On Peer
	CLI:Switch Connection	peer
	CLI:Add Device	${DEV_PEER}
	Wait Until Keyword Succeeds	10x	30s	SUITE:Check If Peer Device Is Visible In Coordinator
	CLI:Enter Path	/access/
	CLI:Test Available Commands	search

Test if peer device is visible with elasticsearch disabled on peer
	CLI:Switch Connection	peer
	CLI:Enter Path	/settings/services
	CLI:Set	enable_elasticsearch=no
	CLI:Commit
	Wait Until Keyword Succeeds	100s	10s	SUITE:Check That Peer Device Does Not Show With Elasticsearch Disabled
	[Teardown]	SUITE:Set Elasticsearch Services Back To Default

Test that no device is found when elasticsearch is disabled on coordinator
	CLI:Switch Connection	coordinator
	CLI:Enter Path	/settings/services
	CLI:Set	enable_elasticsearch=no
	CLI:Commit
	Wait Until Keyword Succeeds	5x	30s	SUITE:Check No Device Is Found When Elasticearch Is Disabeles On Coordinator
	[Teardown]	SUITE:Set Elasticsearch Services Back To Default

*** Keywords ***
SUITE:Setup
	CLI:Open	session_alias=coordinator
	CLI:Open	HOST_DIFF=${HOSTPEER}	session_alias=peer	startping=no
	SUITE:Delete Cluster Configuration
	CLI:Delete All Devices
	CLI:Switch Connection	coordinator
	CLI:Delete All Devices

SUITE:Teardown
	SUITE:Delete Cluster Configuration
	CLI:Delete All Devices
	CLI:Switch Connection	coordinator
	CLI:Delete All Devices
	CLI:Close Connection

SUITE:Configure Cluster On Coordinator
	[Arguments]	${MODE}
	CLI:Switch Connection	coordinator
	Set Client Configuration	timeout=120s
	CLI:Add License Key	${LICENSE_KEY}
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=yes type=coordinator psk=${PSK} cluster_mode=${MODE}
	CLI:Set	enable_clustering_access=yes enable_peer_management=yes
	CLI:Set	enable_license_pool=yes lps_type=server
	CLI:Commit
	${COORDINATOR_PRESENT}	CLI:Show	/settings/cluster/cluster_peers
	Should Contain	${COORDINATOR_PRESENT}	Coordinator
	Should Contain	${COORDINATOR_PRESENT}	Local
	Should Contain	${COORDINATOR_PRESENT}	Online
	[Teardown]	CLI:Cancel	Raw

SUITE:Configure Cluster On Peer
	CLI:Switch Connection	peer
	CLI:Change Hostname	${HOSTNAME_PEER}

	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=yes type=peer psk=${PSK} coordinator_address=${HOST}
	CLI:Set	enable_clustering_access=yes enable_peer_management=yes
	CLI:Set	enable_license_pool=yes lps_type=client
	CLI:Commit
	CLI:Switch Connection	coordinator
	Wait Until Keyword Succeeds	5x	10s	SUITE:Check If Peer Is Present In Coordinator
	[Teardown]	CLI:Cancel	Raw

SUITE:Check If Peer Is Present In Coordinator
	${PEER_PRESENT}	CLI:Show	/settings/cluster/cluster_peers
	Should Contain	${PEER_PRESENT}	${HOSTNAME_PEER}
	Should Contain	${PEER_PRESENT}	${HOSTPEER}
	Should Not Contain	${PEER_PRESENT}	Error
	Should Not Contain	${PEER_PRESENT}	Offline

SUITE:Check If Peer Device Is Visible In Coordinator
	CLI:Switch Connection	coordinator
	CLI:Enter Path	/access/
	${VISIBLE_DEVICES}	CLI:Show
	Should Contain	${VISIBLE_DEVICES}	${HOSTNAME_PEER}
	CLI:Enter Path	${HOSTNAME_PEER}.${DOMAINAME_NODEGRID}
	${VISIBLE_UNDER_PEER}	CLI:Show
	Should Contain	${VISIBLE_UNDER_PEER}	${DEV_PEER}

SUITE:Delete Cluster Configuration
	CLI:Switch Connection	coordinator
	Wait Until Keyword Succeeds	3x	5s	SUITE:Make Sure Peer Was Removed From Coordinator
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=no enable_peer_management=no enable_license_pool=no
	CLI:Commit
	CLI:Delete All License Keys Installed
	CLI:Switch Connection	peer
	CLI:Change Hostname	${HOSTNAME_NODEGRID}
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}

SUITE:Make Sure Peer Was Removed From Coordinator
	CLI:Enter Path	/settings/cluster/cluster_peers
	CLI:Write	remove ${HOSTNAME_PEER}.${DOMAINAME_NODEGRID}	Raw
	CLI:Commit
	${PEERS}	CLI:Show
	Should Not Contain	${PEERS}	${HOSTNAME_PEER}.${DOMAINAME_NODEGRID}

SUITE:Set Elasticsearch Services Back To Default
	CLI:Switch Connection	coordinator
	CLI:Set	/settings/services enable_elasticsearch=yes
	CLI:Set	/settings/services enable_kibana=yes
	CLI:Commit

	CLI:Switch Connection	peer
	CLI:Set	/settings/services enable_elasticsearch=yes
	CLI:Set	/settings/services enable_kibana=yes
	CLI:Commit

SUITE:Check That Peer Device Does Not Show With Elasticsearch Disabled
	${RESULT}	Run Keyword And Return Status	SUITE:Check If Peer Device Is Visible In Coordinator
	Should Not Be True	${RESULT}

SUITE:Check No Device Is Found When Elasticearch Is Disabeles On Coordinator
	CLI:Switch Connection	coordinator
	CLI:Enter Path	/access/
	${VISIBLE_DEVICES}	CLI:Show
	Should Contain	${VISIBLE_DEVICES}	${HOSTNAME_PEER}
	Should Contain	${VISIBLE_DEVICES}	${DEV_COORDINATOR}
	CLI:Enter Path	${HOSTNAME_PEER}.${DOMAINAME_NODEGRID}
	${VISIBLE_UNDER_PEER}	CLI:Show
	Should Contain	${VISIBLE_UNDER_PEER}	${DEV_PEER}
	CLI:Enter Path	/access/
	${RESULT}	Run Keyword And Return Status	CLI:Test Search Command	${DEV_PEER}	${DEV_COORDINATOR}
	Should Not Be True	${RESULT}