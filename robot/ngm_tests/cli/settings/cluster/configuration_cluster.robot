*** Settings ***
Resource	../../init.robot
Documentation	Suite to test the configuration and error messages in cluster
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	NON-CRITICAL
Default Tags	CLI	SSH	CLUSTER

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
${PSK}	nodegrid-key
${PEER1_HOSTNAME}	peer1
${LICENSE}	${TEN_DEVICES_CLUSTERING_LICENSE}

*** Test Cases ***
Test automatic enroll in mesh mode
	[Documentation]	Test automatic enroll in mesh mode by creating an IP range that cointains peer address
	SUITE:Configure Cluster Enrollment IP	mesh
	Wait Until Keyword Succeeds	50s	4x	SUITE:Check If Peers Are Present On Coordinator
	[Teardown]	SUITE:Disable Enrollment Range

Test automatic enroll in star mode
	[Documentation]	Test automatic enroll in star mode by creating an IP range that cointains peer address
	SUITE:Configure Cluster Enrollment IP	star
	Wait Until Keyword Succeeds	50s	4x	SUITE:Check If Peers Are Present On Coordinator
	[Teardown]	SUITE:Disable Enrollment Range

Test correct configuration in mesh mode
	[Documentation]	Tests cluster basic configuration in mesh mode
	SUITE:Configure Cluster	mesh
	Wait Until Keyword Succeeds	50s	4x	SUITE:Check If Peers Are Present On Coordinator
	[Teardown]	SUITE:Delete Cluster Configuration

Test correct configuration in star mode
	[Documentation]	Tests cluster basic configuration in star mode
	SUITE:Configure Cluster	star
	Wait Until Keyword Succeeds	50s	4x	SUITE:Check If Peers Are Present On Coordinator
	[Teardown]	SUITE:Delete Cluster Configuration

Test please set node as coordinator or peer error message
	[Documentation]	Tests error message when user does not set node as coordinator or peer
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=yes
	${ERROR}	CLI:Commit	Raw
	Should Contain	${ERROR}	Error: type: Please set this nodes type as coordinator or peer
	[Teardown]	CLI:Cancel	Raw

Test cannot delete coordinator error message
	[Documentation]	Tests error message when user tries to delete coordinator node
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=yes type=coordinator psk=${PSK}
	Set Client Configuration	timeout=120s
	CLI:Commit
	CLI:Enter Path	/settings/cluster/cluster_peers
	${ERROR}	CLI:Write	remove coordinator.${DOMAINAME_NODEGRID}	Raw
	Should Contain	${ERROR}	Error: Cannot delete Coordinator. Disable Cluster under Cluster Settings instead.
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=no
	CLI:Commit

Test insufficient arguments error message
	[Documentation]	Tests error message when user configure coordinator, but does not enable enrollment
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=yes type=coordinator psk=${PSK}
	CLI:Set	allow_enrollment=no
	${ERROR}	CLI:Commit	Raw
	Should Contain	${ERROR}	Error: Insufficient arguments.
	[Teardown]	CLI:Cancel	Raw

Test could not connect and invalid coordinator error message
	[Documentation]	Tests error message when user sets a coordinator address of a device that is not a coordinator
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=yes type=peer psk=${PSK}
	CLI:Set	coordinator_address=${HOSTPEER}
	${ERROR}	CLI:Commit	Raw
	Should Contain	${ERROR}	Error: Invalid coordinator.
	[Teardown]	CLI:Cancel	Raw

Test Cluster must not have peers error message
	[Documentation]	Tests error message when user tries to change mode of an active cluster coordinator
	SUITE:Configure Cluster	mesh
	Wait Until Keyword Succeeds	50s	4x	SUITE:Check If Peers Are Present On Coordinator
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	cluster_mode=star
	${ERROR}	CLI:Commit	Raw
	Should Contain	${ERROR}	Error: Cluster must not have peers
	CLI:Cancel	Raw
	[Teardown]	SUITE:Delete Cluster Configuration

Test Pre-shared key does not match, invalid cluster name and must be coordinator error messages
	[Documentation]	Tests psk in peer does not match with psk in coordinator, then it inserts the right psk
	...	but the wrong name and expect the errors in this cases.
	CLI:Switch Connection	coordinator
	CLI:Add License Key	${LICENSE}
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=yes type=coordinator psk=${PSK} cluster_mode=mesh
	CLI:Set	enable_clustering_access=yes enable_peer_management=yes
	CLI:Set	enable_license_pool=yes lps_type=server
	Set Client Configuration	timeout=120s
	CLI:Commit
	${PEERS}	CLI:Show	/settings/cluster/cluster_peers
	Should Contain	${PEERS}	coordinator.${DOMAINAME_NODEGRID}
	CLI:Switch Connection	${PEER1_HOSTNAME}
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=yes type=peer coordinator_address=${HOST} psk=incorrect
	${ERROR}	CLI:Commit	Raw
	Should Contain	${ERROR}	Error: Pre-shared key does not match.
	CLI:Set	psk=${PSK} cluster_name=incorrect
	${ERROR}	CLI:Commit	Raw
	Should Contain	${ERROR}	Error: Invalid cluster name.
	CLI:Cancel	Raw
	[Teardown]	SUITE:Delete Cluster Configuration

Test invert coordinator and peer in mesh mode and test invalid operation on cluster error message
	[Documentation]	Creates a normal cluster mesh configuration and changes the peer to be coordinator
	...	then check if the coordinator became peer. Then tries to switch again and expects error.
	SUITE:Configure Cluster	mesh
	Wait Until Keyword Succeeds	50s	4x	SUITE:Check If Peers Are Present On Coordinator
	CLI:Switch Connection	${PEER1_HOSTNAME}
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	type=coordinator psk=${PSK} cluster_mode=mesh
	CLI:Commit
	${INVERSION_CONFIG}	CLI:Show
	Should Contain	${INVERSION_CONFIG}	cluster_mode = star
	CLI:Switch Connection	coordinator
	${COORDINATOR_BECAME_PEER}	CLI:Show	/settings/cluster/settings
	Should Contain	${COORDINATOR_BECAME_PEER}	coordinator_address = ${HOSTPEER}
	CLI:Switch Connection	${PEER1_HOSTNAME}
	CLI:Enter Path	/settings/cluster/cluster_peers
	${PEER_BECAME_COORDINATOR}	CLI:Show
	Should Contain	${PEER_BECAME_COORDINATOR}	coordinator.${DOMAINAME_NODEGRID}
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	type=peer
	${ERROR}	CLI:Commit	Raw
	Should Contain	${ERROR}	Error: Invalid operation on cluster
	CLI:Cancel	Raw
	[Teardown]	SUITE:Delete Cluster Inverted Configuration

*** Keywords ***
SUITE:Setup
	CLI:Open	session_alias=coordinator
	CLI:Change Hostname	coordinator
	CLI:Open	session_alias=${PEER1_HOSTNAME}	HOST_DIFF=${HOSTPEER}
	CLI:Change Hostname	${PEER1_HOSTNAME}

SUITE:Teardown
	CLI:Switch Connection	coordinator
	CLI:Change Hostname	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	${PEER1_HOSTNAME}
	CLI:Change Hostname	${HOSTNAME_NODEGRID}
	CLI:Close Connection

SUITE:Configure Cluster
	[Documentation]	Configures cluster in the mode determined by MODE variable and sets pre-shared-key as PSK
	[Arguments]	${MODE}
	CLI:Switch Connection	coordinator
	CLI:Add License Key	${LICENSE}
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=yes type=coordinator psk=${PSK} cluster_mode=${MODE}
	CLI:Set	enable_clustering_access=yes enable_peer_management=yes
	CLI:Set	enable_license_pool=yes lps_type=server
	Set Client Configuration	timeout=120s
	CLI:Commit
	${PEERS}	CLI:Show	/settings/cluster/cluster_peers
	Should Contain	${PEERS}	coordinator.${DOMAINAME_NODEGRID}
	CLI:Switch Connection	${PEER1_HOSTNAME}
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=yes type=peer coordinator_address=${HOST} psk=${PSK}
	CLI:Set	enable_clustering_access=yes enable_peer_management=yes
	CLI:Set	enable_license_pool=yes lps_type=client
	Set Client Configuration	timeout=120s
	CLI:Commit
	[Teardown]	CLI:Cancel	Raw

SUITE:Delete Cluster Configuration
	[Documentation]	Deletes cluster configuration
	CLI:Switch Connection	coordinator
	CLI:Enter Path	/settings/cluster/cluster_peers
	Wait Until Keyword Succeeds	3x	30s	SUITE:Remove Peers From Coordinator First
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=no
	CLI:Commit
	Set Client Configuration	timeout=30s
	${COORDINATOR_CONF}	CLI:Show
	Should Contain	${COORDINATOR_CONF}	enable_cluster = no
	CLI:Delete All License Keys Installed

SUITE:Check If Peers Are Present On Coordinator
	[Documentation]	Tests if peer was detected in coordinator side and if it is Online
	CLI:Switch Connection	coordinator
	${PEERS}	CLI:Show	/settings/cluster/cluster_peers
	Should Contain	${PEERS}	${HOSTPEER}
	Should Contain	${PEERS}	${PEER1_HOSTNAME}.${DOMAINAME_NODEGRID}
	Should Not Contain	${PEERS}	Offline
	Should Not Contain	${PEERS}	Error

SUITE:Remove Peers From Coordinator First
	[Documentation]	Removes peer from coordinator by using remove command on coordinator side
	CLI:Remove If Exists	${PEER1_HOSTNAME}.${DOMAINAME_NODEGRID}
	${PEERS}	CLI:Show	/settings/cluster/cluster_peers
	Should Not Contain	${PEERS}	${HOSTPEER}

SUITE:Configure Cluster Enrollment IP
	[Documentation]	Enables cluster, checks if enrollement_range path is visible and creates a range which includes
	...	peers address.
	[Arguments]	${MODE}
	CLI:Switch Connection	coordinator
	CLI:Add License Key	${LICENSE}
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=yes type=coordinator psk=${PSK} cluster_mode=${MODE}
	Set Client Configuration	timeout=120s
	CLI:Commit
	Wait Until Keyword Succeeds	50s	4x	SUITE:Check Enrollment Range Is Visible
	CLI:Enter Path	/settings/cluster/cluster_enrollment_range
	CLI:Add
	CLI:Set	ip_range_start=${HOSTPEER} ip_range_end=${HOSTPEER}
	CLI:Commit

SUITE:Disable Enrollment Range
	[Documentation]	Disables enrollment range
	CLI:Enter Path	/settings/cluster/cluster_enrollment_range
	CLI:Delete	1
	CLI:Commit
	SUITE:Delete Cluster Configuration

SUITE:Check Enrollment Range Is Visible
	[Documentation]	Verifies if cluster_enrollment_range path exists under /settings/cluster
	CLI:Enter Path	/settings/cluster
	${PATH_VISIBLE}	CLI:Ls
	Should Contain	${PATH_VISIBLE}	cluster_enrollment_range

SUITE:Delete Cluster Inverted Configuration
	[Documentation]	Used in test teardown where the coordinator and peer switched, so the operations are done
	...	in the opposite node as SUITE:Delete Cluster Configuration
	CLI:Switch Connection	${PEER1_HOSTNAME}
	CLI:Enter Path	/settings/cluster/cluster_peers
	CLI:Remove If Exists	coordinator.${DOMAINAME_NODEGRID}
	${PEERS}	CLI:Show	/settings/cluster/cluster_peers
	Should Not Contain	${PEERS}	${HOST}
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=no
	CLI:Commit
	CLI:Switch Connection	coordinator
	CLI:Delete All License Keys Installed