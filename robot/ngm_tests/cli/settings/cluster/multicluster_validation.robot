*** Settings ***
Resource	../../init.robot
Documentation	Suite to test validations of fields related to multicluster in CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	NON-CRITICAL
Default Tags	CLI	SSH	CLUSTER	MULTICLUSTER

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
${DEFAULT_PSK}	nodegrid-key
${CLUSTER_NAME}	nodegrid
${COORD_ADDRESS}	${HOSTPEER}
@{EXPECTED_COMMANDS}	acknowledge_alarm_state	create_key_pair	hostname	show
...	apply_settings	diagnostic_data	import_settings	show_settings
...	cd	disjoin	join	shutdown
...	change_password	event_system_audit	ls	software_upgrade
...	cloud_enrollment	event_system_clear	pwd	system_certificate
...	commit	exec	quit	system_config_check
...	config_confirm	exit	reboot	whoami
...	config_revert	export_settings	revert
...	config_start	factory_settings	save_settings
...	create_csr	help	shell

*** Test Cases ***
Test avaliable commands
	[Documentation]	Test enters clusters tab and check all the avaliable commands under this path
	CLI:Enter Path	/settings/cluster/cluster_clusters
	CLI:Test Available Commands	@{EXPECTED_COMMANDS}
	[Teardown]	CLI:Cancel	Raw

Test show command
	[Documentation]	Test enters clusters tab and uses show commmand to validate all the headers in clusters table
	CLI:Enter Path	/settings/cluster/cluster_clusters
	${OUTPUT}	CLI:Show
	@{EXPECTED_HEADERS}	Create List	node name	cluster name	address	type	status	peer status
	CLI:Should Contain All	${OUTPUT}	${EXPECTED_HEADERS}
	[Teardown]	CLI:Cancel	Raw

Test avaliable commands in join editor
	[Documentation]	Test checks all the avaliable commands when user enters edit mode after using join command
	CLI:Enter Path	/settings/cluster/cluster_clusters
	CLI:Write	join
	CLI:Test Available Commands	add	cancel	commit	ls	set	show
	[Teardown]	CLI:Cancel	Raw

Test avaliable fields to set in join editor
	[Documentation]	Test checks all the possible field to configure when user does a join operation
	CLI:Enter Path	/settings/cluster/cluster_clusters
	CLI:Write	join
	CLI:Test Set Available Fields	coordinator_address	psk	remote_cluster_name
	[Teardown]	CLI:Cancel	Raw

Test valid values for field coordinator_address
	[Documentation]	Test checks possible values for coordinator address. The test expects an error:
	...	Error: Could not connect to coordinator, since the values inserted are valid, but there is no
	...	real configuration with this addresses. Test also checks Error: Could not get local IP. error
	...	is not showing since this only shows when is an invalid IP.
	@{VALID_ADDRESS}=	Create List	1.1.1.1
	CLI:Enter Path	/settings/cluster/cluster_clusters
	CLI:Write	join
	CLI:Set	psk=${DEFAULT_PSK} remote_cluster_name=${CLUSTER_NAME}
	FOR	${VALUE}	IN	@{VALID_ADDRESS}
		CLI:Set	coordinator_address=${VALUE}
		${ERROR_MESSAGE}	CLI:Commit	Raw
		Should Contain	${ERROR_MESSAGE}	Error: Could not connect to coordinator.
		Should Not Contain	${ERROR_MESSAGE}	Error: Could not get local IP.
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field coordinator_address
	[Documentation]	Test checks for Error: Could not get local IP error message, since this only shows
	...	when coordiantor address value is invalid.
	@{INVALID_ADDRESS}=	Create List	${ELEVEN_NUMBER}	${ELEVEN_POINTS}	${EXCEEDED}	${POINTS_AND_WORD}
	CLI:Enter Path	/settings/cluster/cluster_clusters
	CLI:Write	join
	CLI:Set	psk=${DEFAULT_PSK} remote_cluster_name=${CLUSTER_NAME}
	FOR	${VALUE}	IN	@{INVALID_ADDRESS}
		CLI:Set	coordinator_address=${VALUE}
		${ERROR_MESSAGE}	CLI:Commit	Raw
		Should Not Contain	${ERROR_MESSAGE}	Error: Could not connect to coordinator.
		Should Contain	${ERROR_MESSAGE}	Error: Could not get local IP.
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field psk
	[Documentation]	Test psk valid values, which is basically any string without spaces.
	 ...	Expects Error: Could not connect to coordinator since there is no real configuration.
	@{VALID_PSKS}	Create List	${ELEVEN_WORD}	${ELEVEN_NUMBER}	${ELEVEN_POINTS}	${EXCEEDED}	${POINTS_AND_WORD}	${WORDS_UNDERSCORE}
	CLI:Enter Path	/settings/cluster/cluster_clusters
	CLI:Write	join
	CLI:Set	coordinator_address=1.1.1.1 remote_cluster_name=blablabla
	FOR	${VALUE}	IN	@{VALID_PSKS}
		CLI:Set	psk=${VALUE}
		${ERROR_MESSAGE}	CLI:Commit	Raw
		Should Contain	${ERROR_MESSAGE}	Error: Could not connect to coordinator.
	END
	[Teardown]	CLI:Cancel	Raw

Test wrong values for field psk
	[Documentation]	Test psks different from the ones configured so the Error: Pre-shared key does not match is shown
	...	when both address and name are correct.
	@{VALID_PSKS}	Create List	${ELEVEN_NUMBER}
	CLI:Enter Path	/settings/cluster/cluster_clusters
	CLI:Write	join
	CLI:Set	coordinator_address=${COORD_ADDRESS} remote_cluster_name=${CLUSTER_NAME}
	FOR	${VALUE}	IN	@{VALID_PSKS}
		CLI:Set	psk="${VALUE}"
		${ERROR_MESSAGE}	CLI:Commit	Raw
		Should Not Contain	${ERROR_MESSAGE}	Error: Could not connect to coordinator.
		Should Contain	${ERROR_MESSAGE}	Error: Pre-shared key does not match.
	END
	[Teardown]	CLI:Cancel	Raw

Test valid values for field remote_cluster_name
	[Documentation]	Test cluster_name valid values, which is basically any string without spaces.
	 ...	Expects Error: Could not connect to coordinator since there is no real configuration.
	@{VALID_NAMES}	Create List	${ELEVEN_WORD}	${ELEVEN_NUMBER}	${ELEVEN_POINTS}	${EXCEEDED}	${POINTS_AND_WORD}	${WORDS_UNDERSCORE}
	CLI:Enter Path	/settings/cluster/cluster_clusters
	CLI:Write	join
	CLI:Set	coordinator_address=1.1.1.1 psk=${DEFAULT_PSK}
	FOR	${VALUE}	IN	@{VALID_NAMES}
		CLI:Set	remote_cluster_name=${VALUE}
		${ERROR_MESSAGE}	CLI:Commit	Raw
		Should Contain	${ERROR_MESSAGE}	Error: Could not connect to coordinator.
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field remote_cluster_name
	[Documentation]	Test tries entering a remote cluster name which has spaces and expects Error: Invalid cluster name
	@{INVALID_NAMES}	Create List	${ELEVEN_WORD}	${ELEVEN_NUMBER}
	CLI:Enter Path	/settings/cluster/cluster_clusters
	CLI:Write	join
	CLI:Set	coordinator_address=${COORD_ADDRESS} psk=${DEFAULT_PSK}
	FOR	${VALUE}	IN	@{INVALID_NAMES}
		CLI:Set	remote_cluster_name="${VALUE}"
		${ERROR_MESSAGE}	CLI:Commit	Raw
		Should Not Contain	${ERROR_MESSAGE}	Error: Could not connect to coordinator.
		Should Contain	${ERROR_MESSAGE}	Error: Invalid cluster name.
	END
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	[Documentation]	Adds cluster configuration as coordinator in both host and hostpeer
	CLI:Open	session_alias=local_coord
	CLI:Open	session_alias=remote_coord
	SUITE:Add Cluster Configuration	local_coord
	SUITE:Add Cluster Configuration	remote_coord

SUITE:Teardown
	[Documentation]	Deletes cluster configuration in host and hostpeer
	SUITE:Remove Cluster Configuration	local_coord
	SUITE:Remove Cluster Configuration	remote_coord
	CLI:Close Connection

SUITE:Add Cluster Configuration
	[Documentation]	With connection as argument, it creates cluster configuration as coordinator.
	[Arguments]	${CONNECTION}
	CLI:Switch Connection	${CONNECTION}
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=yes type=coordinator psk=${DEFAULT_PSK}
	Set Client Configuration	timeout=120s
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}

SUITE:Remove Cluster Configuration
	[Documentation]	With connection as argument, it removes the cluster configuration by setting enable_cluster=no
	[Arguments]	${CONNECTION}
	CLI:Switch Connection	${CONNECTION}
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=no
	Set Client Configuration	timeout=120s
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}