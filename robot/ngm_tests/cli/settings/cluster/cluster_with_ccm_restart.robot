*** Settings ***
Resource	../../init.robot
Documentation	Validation of cluster functionality after a ccm restart
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	NON-CRITICAL
Default Tags	CLI	SSH	CLUSTER

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
${CLUSTER_LICENSE}	${TEN_DEVICES_CLUSTERING_LICENSE}
${DEVICE_COORDINATOR}	coordinator_dummy_device
${DEVICE_PEER}	peer_dummy_device
${PSK}	nodegrid-key
${MAX_TIME}	20s	#the maximum time check_ccm.sh should be active after ccm restart is 20s

*** Test Cases ***
Test cluster (sccpd) functionality before ccm restart
	CLI:Switch Connection	coordinator
	CLI:Add License Key	${CLUSTER_LICENSE}
	CLI:Change Hostname	coordinator
	CLI:Add Device	${DEVICE_COORDINATOR}
	SUITE:Configure Cluster Coordinator

	CLI:Switch Connection	peer
	CLI:Change Hostname	peer
	CLI:Add Device	${DEVICE_PEER}
	SUITE:Configure Cluster Peer

	SUITE:Check Peer Information On Coordinator
	[Teardown]	CLI:Cancel	Raw

Test processes ccm and sccpd are running before restart
	CLI:Switch Connection	root_session
	SUITE:Check CCM Processes Are Active

Test processes ccm and sccpd are running on coordinator after restart
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	5x	${MAX_TIME}	SUITE:Check CCM Processes Are Active

Test cluster is working correctly after CCM restart
	SUITE:Check Peer Information On Coordinator

*** Keywords ***
SUITE:Setup
	CLI:Open	session_alias=coordinator
	CLI:Delete License If Exists	${CLUSTER_LICENSE}
	CLI:Delete If Exists Confirm	${DEVICE_COORDINATOR}
	CLI:Connect As Root
	CLI:Open	session_alias=peer	HOST_DIFF=${HOSTPEER}
	CLI:Delete If Exists Confirm	${DEVICE_PEER}

SUITE:Teardown
	SUITE:Remove Cluster Thru Coordinator
	CLI:Enter Path	/settings/license
	CLI:Delete All License Keys Installed
	CLI:Change Hostname	${HOSTNAME_NODEGRID}
	CLI:Delete All Devices

	CLI:Switch Connection	peer
	CLI:Change Hostname	${HOSTNAME_NODEGRID}
	${CLUSTER_DISABLED}	CLI:Show	/settings/cluster/settings
	Should Contain	${CLUSTER_DISABLED}	enable_cluster = no
	CLI:Delete All Devices

	CLI:Close Connection

SUITE:Configure Cluster Coordinator
	CLI:Switch Connection	coordinator
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=yes type=coordinator psk=${PSK}
	CLI:Set	cluster_mode=star enable_clustering_access=yes
	CLI:Set	enable_peer_management=yes enable_license_pool=yes lps_type=server
	Set Client Configuration	timeout=60s
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	${COORDINATOR}	CLI:Show	/settings/cluster/cluster_peers
	Should Contain	${COORDINATOR}	coordinator.${DOMAINAME_NODEGRID}
	Should Contain	${COORDINATOR}	Online
	[Teardown]	CLI:Cancel	Raw

SUITE:Configure Cluster Peer
	CLI:Switch Connection	peer
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=yes type=peer coordinator_address=${HOST} psk=${PSK}
	CLI:Set	enable_clustering_access=yes enable_peer_management=yes
	CLI:Set	enable_license_pool=yes lps_type=client
	CLI:Commit
	[Teardown]	CLI:Cancel	Raw

SUITE:Check Peer Information On Coordinator
	CLI:Switch Connection	coordinator
	Wait Until Keyword Succeeds	5x	10s	SUITE:Peer Should Be Online
	Wait Until Keyword Succeeds	100s	10s	SUITE:Peer Device Should Show In Access

SUITE:Peer Should Be Online
	${PEERS}	CLI:Show	/settings/cluster/cluster_peers
	Should Not Contain	${PEERS}	Error
	Should Not Contain	${PEERS}	Offline

SUITE:Peer Device Should Show In Access
	CLI:Close Current Connection
	CLI:Open	session_alias=coordinator	#test authentication is working as expected
	${DEVICES}	CLI:Show	/access/
	${PEER_PRESENT}	Run Keyword And Return Status	Should Contain	${DEVICES}	peer.${DOMAINAME_NODEGRID}
	IF	${PEER_PRESENT}
		${PEER_DEVICES}	CLI:Show	/access/peer.${DOMAINAME_NODEGRID}
		Should Contain	${PEER_DEVICES}	${DEVICE_PEER}
	ELSE
		Fail	Peer is not on access table from coordinator
	END

SUITE:Check CCM Processes Are Active
	${CCM_ACTIVE_PROCESSES}	CLI:Write	ps -ef | grep ccm
	Should Contain	${CCM_ACTIVE_PROCESSES}	/bin/bash /usr/bin/check_ccm.sh
	Should Contain	${CCM_ACTIVE_PROCESSES}	/usr/sbin/ccm

SUITE:Remove Cluster Thru Coordinator
	CLI:Switch Connection	coordinator
	Set Client Configuration	timeout=120s
	CLI:Enter Path	/settings/cluster/cluster_peers
	CLI:Remove If Exists	peer.${DOMAINAME_NODEGRID}
	${PEER_REMOVED}	CLI:Show
	Should Not Contain	${PEER_REMOVED}	peer.${DOMAINAME_NODEGRID}
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	Set Client Configuration	timeout=120s
	CLI:Set	enable_cluster=no
	Set Client Configuration	timeout=60s
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}