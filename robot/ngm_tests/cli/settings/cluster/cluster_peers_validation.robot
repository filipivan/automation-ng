*** Settings ***
Resource	../../init.robot
Documentation	Validation test cases about Cluster Feature available for v4.2+ throught CLI.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI	SSH	CLUSTER

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/cluster/cluster_peers/
	CLI:Test Available Commands
	...	apply_settings       exit                 shell
	...	cd                   factory_settings     show
	...	change_password      hostname             show_settings
	...	cloud_enrollment     ls                   shutdown
	...	commit               pwd                  software_upgrade
	...	create_csr           quit                 system_certificate
	...	diagnostic_data      reboot               system_config_check
	...	event_system_audit   revert               whoami
	...	event_system_clear   save_settings

Test show_settings command
	CLI:Enter Path	/settings/cluster/cluster_peers/
	Run Keyword If	'${NGVERSION}' == '4.2'	Write	show_settings
	Run Keyword If	'${NGVERSION}' >= '5.0'	Write	show
	${OUTPUT}=	Wait Until Keyword Succeeds	5x	5s	CLI:Read Until Prompt
	Run Keyword If	'${NGVERSION}' == '4.2'	Should Contain	${OUTPUT}	/settings/cluster/cluster_peers notice="Cluster must be enabled. Cluster Peers is only available for Coordinator nodes in star mode."
	Run Keyword If	'${NGVERSION}' >= '5.0'	Should Contain	${OUTPUT}	notice: Cluster must be enabled. Cluster Peers is only available for Coordinator nodes in star mode.

Test Show Command
	CLI:Enter Path	/settings/cluster/cluster_peers/
	CLI:Test Show Command	notice: Cluster must be enabled. Cluster Peers is only available for Coordinator nodes in star mode.

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection