*** Settings ***
Resource	../../init.robot
Documentation	Suite to test the management and configuration of cluster peers thru the coordinator via shell/CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	NON-CRITICAL
Default Tags	CLI	SSH	CLUSTER

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${PSK}	nodegrid-key
${PEER1_HOSTNAME}	peer1
${PEER2_HOSTNAME}	peer2
${LICENSE}	${TEN_DEVICES_CLUSTERING_LICENSE}
*** Test Cases ***
##################MESH MODE#################################
Test if cluster is configured correctly in mesh mode using ansible
	Skip If	not ${HAS_HOSTSHARED}	This build does not have a shared device
	SUITE:Configure Cluster	mesh
	Wait Until Keyword Succeeds	50s	4x	SUITE:Check If Peers Are Present On Coordinator
	Wait Until Keyword Succeeds	45s	4x	SUITE:Check If Peers Are On Ansible Inventory List

Test if coordinator can get peer info via ansible in mesh mode
	Skip If	not ${HAS_HOSTSHARED}	This build does not have a shared device
	CLI:Switch Connection	coordinator
	Set Client Configuration	prompt=$
	CLI:Write	shell sudo su - ansible
	${PEER1_INFO}	CLI:Write	ansible-inventory --host ${PEER1_HOSTNAME}.${DOMAINAME_NODEGRID}
	Should Contain	${PEER1_INFO}	${HOSTPEER}
	${PEER2_INFO}	CLI:Write	ansible-inventory --host ${PEER2_HOSTNAME}.${DOMAINAME_NODEGRID}
	Should Contain	${PEER2_INFO}	${HOSTSHARED}
	[Teardown]	SUITE:Teardown For Ansible User

Test if peer is reachable in mesh mode
	Skip If	not ${HAS_HOSTSHARED}	This build does not have a shared device
	SUITE:Check If Peer Is Reachable Via Ansible
	CLI:Switch Connection	coordinator
	CLI:Test Ping	${HOSTPEER}	NUM_PACKETS=5	TIMEOUT=10

Test if coordinator can manage peer disk usage in mesh mode
	Skip If	not ${HAS_HOSTSHARED}	This build does not have a shared device
	CLI:Switch Connection	coordinator
	Set Client Configuration	prompt=$
	CLI:Write	shell sudo su - ansible
	${MEMORY_INFO}	CLI:Write	ansible peers -a "df -h"
	Should Contain	${MEMORY_INFO}	CHANGED
	[Teardown]	SUITE:Teardown For Ansible User

Test applying default playbook to change session timeout on peer in mesh mode
	Skip If	not ${HAS_HOSTSHARED}	This build does not have a shared device
	CLI:Switch Connection	${PEER2_HOSTNAME}
	${ORIGINAL_PEER2_TIMEOUT}	CLI:Show	/settings/system_preferences idle_timeout
	CLI:Switch Connection	coordinator
	Set Client Configuration	prompt=$
	CLI:Write	shell sudo su - ansible
	${OUT}	CLI:Write	ansible-playbook -l peers /etc/ansible/playbooks/import_settings.yml
	Should Not Contain	${OUT}	ERROR!
	Should Contain	${OUT}	changed: [${PEER1_HOSTNAME}.${DOMAINAME_NODEGRID}]
	Should Contain	${OUT}	changed: [${PEER2_HOSTNAME}.${DOMAINAME_NODEGRID}]
	Set Client Configuration	prompt=]
	CLI:Switch Connection	${PEER2_HOSTNAME}
	${NEW_PEER2_TIMEOUT}	CLI:Show	/settings/system_preferences idle_timeout
	Set Suite Variable	${NEW_PEER2_TIMEOUT}
	Should Not Be Equal	${ORIGINAL_PEER2_TIMEOUT}	${NEW_PEER2_TIMEOUT}
	Should Contain	${NEW_PEER2_TIMEOUT}	900
	[Teardown]	Run Keywords	SUITE:Teardown For Ansible User	AND	SUITE:Delete Cluster Configuration

#####################STAR MODE####################################
Test if cluster is configured correctly in star mode using ansible
	Skip If	not ${HAS_HOSTSHARED}	This build does not have a shared device
	SUITE:Configure Cluster	star
	Wait Until Keyword Succeeds	50s	4x	SUITE:Check If Peers Are Present On Coordinator
	Wait Until Keyword Succeeds	45s	4x	SUITE:Check If Peers Are On Ansible Inventory List

Test if coordinator can get peer info via ansible in star mode
	Skip If	not ${HAS_HOSTSHARED}	This build does not have a shared device
	CLI:Switch Connection	coordinator
	Set Client Configuration	prompt=$
	CLI:Write	shell sudo su - ansible
	${PEER1_INFO}	CLI:Write	ansible-inventory --host ${PEER1_HOSTNAME}.${DOMAINAME_NODEGRID}
	Should Contain	${PEER1_INFO}	${HOSTPEER}
	${PEER2_INFO}	CLI:Write	ansible-inventory --host ${PEER2_HOSTNAME}.${DOMAINAME_NODEGRID}
	Should Contain	${PEER2_INFO}	${HOSTSHARED}
	[Teardown]	SUITE:Teardown For Ansible User

Test if peer is reachable in star mode
	Skip If	not ${HAS_HOSTSHARED}	This build does not have a shared device
	SUITE:Check If Peer Is Reachable Via Ansible
	CLI:Switch Connection	coordinator
	CLI:Test Ping	${HOSTPEER}	NUM_PACKETS=5	TIMEOUT=10

Test if coordinator can manage peer disk usage in star mode
	Skip If	not ${HAS_HOSTSHARED}	This build does not have a shared device
	CLI:Switch Connection	coordinator
	Set Client Configuration	prompt=$
	CLI:Write	shell sudo su - ansible
	${MEMORY_INFO}	CLI:Write	ansible peers -a "df -h"
	Should Contain	${MEMORY_INFO}	CHANGED
	[Teardown]	SUITE:Teardown For Ansible User

Test applying default playbook to change session timeout on peer in star mode
	Skip If	not ${HAS_HOSTSHARED}	This build does not have a shared device
	CLI:Switch Connection	${PEER2_HOSTNAME}
	${ORIGINAL_PEER2_TIMEOUT}	CLI:Show	/settings/system_preferences idle_timeout
	CLI:Switch Connection	coordinator
	Set Client Configuration	prompt=$
	CLI:Write	shell sudo su - ansible
	${OUT}	CLI:Write	ansible-playbook -l peers /etc/ansible/playbooks/import_settings.yml
	Should Not Contain	${OUT}	ERROR!
	Should Contain	${OUT}	changed: [${PEER1_HOSTNAME}.${DOMAINAME_NODEGRID}]
	Should Contain	${OUT}	changed: [${PEER2_HOSTNAME}.${DOMAINAME_NODEGRID}]
	Set Client Configuration	prompt=]
	CLI:Switch Connection	${PEER2_HOSTNAME}
	${NEW_PEER2_TIMEOUT}	CLI:Show	/settings/system_preferences idle_timeout
	Set Suite Variable	${NEW_PEER2_TIMEOUT}
	Should Not Be Equal	${ORIGINAL_PEER2_TIMEOUT}	${NEW_PEER2_TIMEOUT}
	Should Contain	${NEW_PEER2_TIMEOUT}	900
	[Teardown]	SUITE:Teardown For Ansible User

*** Keywords ***
SUITE:Setup
	Skip If	not ${HAS_HOSTSHARED}	This build does not have a shared device
	CLI:Open	session_alias=coordinator
	CLI:Change Hostname	coordinator
	CLI:Open	session_alias=${PEER1_HOSTNAME}	HOST_DIFF=${HOSTPEER}
	CLI:Change Hostname	${PEER1_HOSTNAME}
	CLI:Open	session_alias=${PEER2_HOSTNAME}	HOST_DIFF=${HOSTSHARED}
	CLI:Change Hostname	${PEER2_HOSTNAME}

SUITE:Teardown
	Skip If	not ${HAS_HOSTSHARED}	This build does not have a shared device
	Wait until keyword Succeeds	2x	10s	SUITE:Delete Cluster Configuration
	SUITE:Disable Cluster On Peer	${PEER1_HOSTNAME}
	SUITE:Disable Cluster On Peer	${PEER2_HOSTNAME}
	CLI:Switch Connection	coordinator
	CLI:Change Hostname	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	${PEER1_HOSTNAME}
	CLI:Change Hostname	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	${PEER2_HOSTNAME}
	CLI:Change Hostname	${HOSTNAME_NODEGRID}
	CLI:Close Connection

SUITE:Configure Cluster
	[Arguments]	${MODE}
	CLI:Switch Connection	coordinator
	CLI:Add License Key	${LICENSE}
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=yes type=coordinator psk=${PSK} cluster_mode=${MODE}
	CLI:Set	enable_clustering_access=yes enable_peer_management=yes
	CLI:Set	enable_license_pool=yes lps_type=server
	Set Client Configuration	timeout=120s
	CLI:Commit
	${PEERS}	CLI:Show	/settings/cluster/cluster_peers
	Should Contain	${PEERS}	coordinator.${DOMAINAME_NODEGRID}
	CLI:Switch Connection	${PEER1_HOSTNAME}
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=yes type=peer coordinator_address=${HOST} psk=${PSK}
	CLI:Set	enable_clustering_access=yes enable_peer_management=yes
	CLI:Set	enable_license_pool=yes lps_type=client
	CLI:Commit
	CLI:Switch Connection	${PEER2_HOSTNAME}
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=yes type=peer coordinator_address=${HOST} psk=${PSK}
	CLI:Set	enable_clustering_access=yes enable_peer_management=yes
	CLI:Set	enable_license_pool=yes lps_type=client
	CLI:Commit
	[Teardown]	CLI:Cancel	Raw

SUITE:Delete Cluster Configuration
	Skip If	not ${HAS_HOSTSHARED}	This build does not have a shared device
	CLI:Switch Connection	coordinator
	CLI:Enter Path	/settings/cluster/cluster_peers
	Wait Until Keyword Succeeds	3x	30s	SUITE:Remove Peers From Coordinator First
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=no
	Set Client Configuration	timeout=120s
	CLI:Commit
	Set Client Configuration	timeout=30s
	${COORDINATOR_CONF}	CLI:Show
	Should Contain	${COORDINATOR_CONF}	enable_cluster = no
	CLI:Delete All License Keys Installed
	SUITE:Set Idle Timeout Back To Default On Peers

SUITE:Check If Peers Are Present On Coordinator
	CLI:Switch Connection	coordinator
	${PEERS}	CLI:Show	/settings/cluster/cluster_peers
	Should Contain	${PEERS}	${HOSTPEER}
	Should Contain	${PEERS}	${PEER1_HOSTNAME}.${DOMAINAME_NODEGRID}
	Should Contain	${PEERS}	${HOSTSHARED}
	Should Contain	${PEERS}	${PEER2_HOSTNAME}.${DOMAINAME_NODEGRID}
	Should Not Contain	${PEERS}	Offline
	Should Not Contain	${PEERS}	Error

SUITE:Check If Peers Are On Ansible Inventory List
	CLI:Switch Connection	coordinator
	Set Client Configuration	prompt=$
	CLI:Write	shell sudo su - ansible
	${LIST}	CLI:Write	ansible-inventory --list
	Should Contain	${LIST}	${PEER1_HOSTNAME}.${DOMAINAME_NODEGRID}
	Should Contain	${LIST}	${PEER2_HOSTNAME}.${DOMAINAME_NODEGRID}
	[Teardown]	SUITE:Teardown For Ansible User

SUITE:Check If Peer Is Reachable Via Ansible
	CLI:Switch Connection	coordinator
	Set Client Configuration	prompt=$
	CLI:Write	shell sudo su - ansible
	${PING_SUCCESSFULL}	CLI:Write	ansible peers -m ping
	Should Contain	${PING_SUCCESSFULL}	SUCCESS
	[Teardown]	SUITE:Teardown For Ansible User

SUITE:Disable Cluster On Peer
	[Arguments]	${PEER}
	CLI:Switch Connection	${PEER}
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	Set Client Configuration	timeout=120s
	CLI:Set	enable_cluster=no
	CLI:Commit
	${PEER_CONF}	CLI:Show
	Should Contain	${PEER_CONF}	enable_cluster = no

SUITE:Teardown For Ansible User
	CLI:Switch Connection	coordinator
	Set Client Configuration	prompt=]
	CLI:Write	exit

SUITE:Set Idle Timeout Back To Default On Peers
	CLI:Switch Connection	${PEER1_HOSTNAME}
	CLI:Set	/settings/system_preferences/ idle_timeout=300
	CLI:Commit
	CLI:Switch Connection	${PEER2_HOSTNAME}
	CLI:Set	/settings/system_preferences/ idle_timeout=300
	CLI:Commit

SUITE:Remove Peers From Coordinator First
#Method to try to avoid "Peer is already in the cluster" error
	CLI:Remove If Exists	${PEER1_HOSTNAME}.${DOMAINAME_NODEGRID}
	CLI:Remove If Exists	${PEER2_HOSTNAME}.${DOMAINAME_NODEGRID}
	${PEERS}	CLI:Show	/settings/cluster/cluster_peers
	Should Not Contain	${PEERS}	${HOSTPEER}
	Should Not Contain	${PEERS}	${HOSTSHARED}