*** Settings ***
Resource	../../init.robot
Documentation	Tests Netflow Streaming Telemetry validation
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NETFLOW_NAME}	flowexportertest
${IP_ADDRESS}	10.200.100.50
${DOMAIN_NAME}	s0m3-d0m41n.name.com
@{ALL_VALUES}=	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
	...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
	...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
	...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
	...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
	...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}

*** Test Cases ***
Test Available Commands After Sending Tab-Tab
	CLI:Enter Path	/settings/flow_exporter
	CLI:Test Available Commands	enable	reboot	add	event_system_audit	revert
	...	apply_settings	event_system_clear	save_settings	cd	exec	shell
	...	change_password	exit	show	cloud_enrollment	export_settings
	...	show_settings	commit	factory_settings	shutdown	create_csr
	...	hostname	software_upgrade	delete	import_settings	system_certificate
	...	diagnostic_data	ls	system_config_check	disable	pwd	whoami	edit	quit

Test Visible Fields For Show Command
	CLI:Test Show Command Regexp
	...	name\\s+status\\s+collector\\s+sampling rate\\s+interface\\s+aggregation fields

Test Available Commands After Add Command
	CLI:Enter Path	/settings/flow_exporter
	CLI:Add
	CLI:Test Available Commands	cancel	commit	ls	save	set	show
	[Teardown]	CLI:Cancel	Raw

Test Available Fields After Add Command
	CLI:Enter Path	/settings/flow_exporter
	CLI:Add
	CLI:Test Set Available Fields	name	enabled	interface	collector_address
	...	collector_port	protocol	active_timeout	inactive_timeout
	...	sampling_rate	aggregation
	[Teardown]	CLI:Cancel	Raw

Test Valid Values For Field=name
	${VALID_NAMES}=	Create List	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
	...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
	...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}

	FOR	${VALID_NAME}	IN	@{VALID_NAMES}
		SUITE:Test Set Valid Value For Field	name	${VALID_NAME}
	END
	[Teardown]	SUITE:Cancel And Remove All Flow Exporters

Test Invalid Values For Field=name
	${INVALID_NAMES}=	Create List	${POINTS}	${ONE_POINTS}	${TWO_POINTS}
	...	${THREE_POINTS}	${SEVEN_POINTS}	${EIGHT_POINTS}	${ELEVEN_POINTS}
	...	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}	${POINTS_AND_NUMBER}
	...	${POINTS_AND_WORD}

	FOR	${INVALID_NAME}	IN	@{INVALID_NAMES}
		SUITE:Test Set Invalid Value For Field	name	${INVALID_NAME}
	END
	[Teardown]	SUITE:Cancel And Remove All Flow Exporters

Test Valid Values For Field=enabled
	CLI:Enter Path	/settings/flow_exporter
	CLI:Add
	CLI:Test Set Field Options	enabled	yes	no
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=enabled
	CLI:Enter Path	/settings/flow_exporter
	CLI:Add
	CLI:Set	name=${NETFLOW_NAME} collector_address=${IP_ADDRESS}
	CLI:Test Set Field Invalid Options	enabled	${EMPTY}
	...	Error: Missing value for parameter: enabled

	FOR	${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	enabled	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: enabled
	END
	[Teardown]	SUITE:Cancel And Remove All Flow Exporters

Test Valid Values For Field=interface
	${CONNECTIONS}=	CLI:Get Builtin Network Connections	Yes
	CLI:Enter Path	/settings/flow_exporter
	CLI:Add
	CLI:Test Set Field Options	interface	@{CONNECTIONS}
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=interface
	CLI:Enter Path	/settings/flow_exporter
	CLI:Add
	CLI:Set	name=${NETFLOW_NAME} collector_address=${IP_ADDRESS}
	CLI:Test Set Field Invalid Options	interface	${EMPTY}
	...	Error: Missing value for parameter: interface

	FOR	${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	interface	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: interface
	END
	[Teardown]	SUITE:Cancel And Remove All Flow Exporters

Test Valid Values For Field=collector_address
	${VALID_ADDRESSES}=	Create List	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
	...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
	...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${IP_ADDRESS}
	...	${DOMAIN_NAME}

	FOR	${VALID_ADDRESS}	IN	@{VALID_ADDRESSES}
		SUITE:Test Set Valid Value For Field	collector_address	${VALID_ADDRESS}
	END
	[Teardown]	SUITE:Cancel And Remove All Flow Exporters

Test Invalid Values For Field=collector_address
	${INVALID_ADDRESSES}=	Create List	${POINTS}	${ONE_POINTS}	${TWO_POINTS}
	...	${THREE_POINTS}	${SEVEN_POINTS}	${EIGHT_POINTS}	${ELEVEN_POINTS}
	...	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}	${POINTS_AND_NUMBER}
	...	${POINTS_AND_WORD}

	FOR	${INVALID_ADDRESS}	IN	@{INVALID_ADDRESSES}
		SUITE:Test Set Invalid Value For Field	collector_address	${INVALID_ADDRESS}
	END
	[Teardown]	SUITE:Cancel And Remove All Flow Exporters

Test Valid Values For Field=protocol
	CLI:Enter Path	/settings/flow_exporter
	CLI:Add
	CLI:Test Set Field Options	protocol	ipfix	netflow_v5	netflow_v9
	Run Keyword If	'${NGVERSION}' >= '5.8'	CLI:Test Set Field Options	protocol	sflow
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=protocol
	CLI:Enter Path	/settings/flow_exporter
	CLI:Add
	CLI:Set	name=${NETFLOW_NAME} collector_address=${IP_ADDRESS}
	CLI:Test Set Field Invalid Options	protocol	${EMPTY}
	...	Error: Missing value for parameter: protocol

	FOR	${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	protocol	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: protocol
	END
	[Teardown]	SUITE:Cancel And Remove All Flow Exporters

Test Valid Values For Field=active_timeout
	${VALID_TIMEOUTS}=	Create List	${NUMBER}	${ONE_NUMBER}	${TWO_NUMBER}
	...	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}

	FOR	${VALID_TIMEOUT}	IN	@{VALID_TIMEOUTS}
		SUITE:Test Set Valid Value For Field	active_timeout	${VALID_TIMEOUT}
	END
	[Teardown]	SUITE:Cancel And Remove All Flow Exporters

Test Invalid Values For Field=active_timeout
	${INVALID_TIMEOUTS}=	Create List	${POINTS}	${ONE_POINTS}	${TWO_POINTS}
	...	${THREE_POINTS}	${SEVEN_POINTS}	${EIGHT_POINTS}	${ELEVEN_POINTS}
	...	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}	${POINTS_AND_NUMBER}
	...	${POINTS_AND_WORD}	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}

	FOR	${INVALID_TIMEOUT}	IN	@{INVALID_TIMEOUTS}
		SUITE:Test Set Invalid Value For Field	active_timeout	${INVALID_TIMEOUT}
		...	Error: active_timeout: Error on one of the Timeout fields.Error: inactive_timeout: Error on one of the Timeout fields.
	END
	[Teardown]	SUITE:Cancel And Remove All Flow Exporters

Test Valid Values For Field=inactive_timeout
	${VALID_TIMEOUTS}=	Create List	${NUMBER}	${ONE_NUMBER}	${TWO_NUMBER}
	...	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}

	FOR	${VALID_TIMEOUT}	IN	@{VALID_TIMEOUTS}
		SUITE:Test Set Valid Value For Field	inactive_timeout	${VALID_TIMEOUT}
	END
	[Teardown]	SUITE:Cancel And Remove All Flow Exporters

Test Invalid Values For Field=inactive_timeout
	${INVALID_TIMEOUTS}=	Create List	${POINTS}	${ONE_POINTS}	${TWO_POINTS}
	...	${THREE_POINTS}	${SEVEN_POINTS}	${EIGHT_POINTS}	${ELEVEN_POINTS}
	...	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}	${POINTS_AND_NUMBER}
	...	${POINTS_AND_WORD}	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}

	FOR	${INVALID_TIMEOUT}	IN	@{INVALID_TIMEOUTS}
		SUITE:Test Set Invalid Value For Field	inactive_timeout	${INVALID_TIMEOUT}
		...	Error: active_timeout: Error on one of the Timeout fields.Error: inactive_timeout: Error on one of the Timeout fields.
	END
	[Teardown]	SUITE:Cancel And Remove All Flow Exporters

Test Valid Values For Field=sampling_rate
	${VALID_SAMPLINGS}=	Create List	${NUMBER}	${ONE_NUMBER}	${TWO_NUMBER}
	...	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}

	FOR	${VALID_SAMPLING}	IN	@{VALID_SAMPLINGS}
		SUITE:Test Set Valid Value For Field	sampling_rate	${VALID_SAMPLING}
	END
	[Teardown]	SUITE:Cancel And Remove All Flow Exporters

Test Invalid Values For Field=sampling_rate
	${INVALID_SAMPLINGS}=	Create List	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD} ${POINTS}	${ONE_POINTS}
	...	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}	${EIGHT_POINTS}
	...	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
	...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}	${EMPTY}

	FOR	${INVALID_SAMPLING}	IN	@{INVALID_SAMPLINGS}
		SUITE:Test Set Invalid Value For Field	sampling_rate	${INVALID_SAMPLING}
		...	Error: sampling_rate: Validation error.
	END
	[Teardown]	SUITE:Cancel And Remove All Flow Exporters

Test Valid Values For Field=collector_port
	${VALID_SAMPLINGS}=	Create List	${NUMBER}	${ONE_NUMBER}	${TWO_NUMBER}
	...	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}

	FOR	${VALID_SAMPLING}	IN	@{VALID_SAMPLINGS}
		SUITE:Test Set Valid Value For Field	collector_port	${VALID_SAMPLING}
	END
	[Teardown]	SUITE:Cancel And Remove All Flow Exporters

Test Invalid Values For Field=collector_port
	${INVALID_SAMPLINGS}=	Create List	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD} ${POINTS}	${ONE_POINTS}
	...	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}	${EIGHT_POINTS}
	...	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
	...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}	${EMPTY}

	FOR	${INVALID_SAMPLING}	IN	@{INVALID_SAMPLINGS}
		SUITE:Test Set Invalid Value For Field	collector_port	${INVALID_SAMPLING}
		...	Error: collector_address: Error in Collector address or port.Error: collector_port: Error in Collector address or port.
	END
	[Teardown]	SUITE:Cancel And Remove All Flow Exporters

Test Valid Values For Field=aggregation
	CLI:Enter Path	/settings/flow_exporter
	CLI:Add
	CLI:Test Set Field Options Raw	aggregation
	...	as_path	label	src_host	tag2	class
	...	local_pref	src_host_coords	tcpflags	cos	lrg_comm	src_host_country
	...	timestamp_arrival	dst_as	med	src_host_pocode	tos	dst_host
	...	mpls_label_bottom	src_local_pref	tunnel_dst_host	dst_host_coords
	...	mpls_label_top	src_lrg_comm	tunnel_dst_mac	dst_host_country
	...	mpls_stack_depth	src_mac	tunnel_dst_port	dst_host_pocode	peer_dst_as
	...	src_mask	tunnel_proto	dst_mac	peer_dst_ip	src_med	tunnel_src_host
	...	dst_mask	peer_src_as	src_net	tunnel_src_mac	dst_net	proto	src_port
	...	tunnel_src_port	dst_port	sampling_rate	src_roa	tunnel_tos	dst_roa
	...	src_as	src_std_comm	vlan	etype	src_as_path	std_comm	vxlan
	...	ext_comm	src_ext_comm	tag
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=aggregation
	CLI:Enter Path	/settings/flow_exporter
	CLI:Add
	CLI:Set	name=${NETFLOW_NAME} collector_address=${IP_ADDRESS}

	FOR	${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	aggregation	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: aggregation
	END
	[Teardown]	SUITE:Cancel And Remove All Flow Exporters

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Delete All Netflow Exporters

SUITE:Teardown
	CLI:Delete All Netflow Exporters
	CLI:Close Connection

SUITE:Cancel And Remove All Flow Exporters
	CLI:Cancel	Raw
	CLI:Delete All Netflow Exporters

SUITE:Test Set Valid Value For Field
	[Arguments]	${NAME}	${VALUE}
	CLI:Enter Path	/settings/flow_exporter
	CLI:Add
	Run Keyword If	"${NAME}" == "name"	Run Keywords
	...		CLI:Test Set Field Invalid Options	name	${VALUE}	AND
	...		CLI:Test Set Field Invalid Options	collector_address	${IP_ADDRESS}	save=yes
	...	ELSE IF	"${NAME}" == "collector_address"	Run Keywords
	...		CLI:Set	name=${NETFLOW_NAME}	AND
	...		CLI:Test Set Field Invalid Options	collector_address	${VALUE}	save=yes
	...	ELSE	Run Keywords
	...		CLI:Set	name=${NETFLOW_NAME} collector_address=${IP_ADDRESS}	AND
	...		CLI:Test Set Field Invalid Options	${NAME}	${VALUE}	save=yes

	SUITE:Cancel And Remove All Flow Exporters

SUITE:Test Set Invalid Value For Field
	[Arguments]	${NAME}	${VALUE}	${ERROR}=${EMPTY}
	CLI:Enter Path	/settings/flow_exporter
	CLI:Add
	Run Keyword If	"${NAME}" == "name"	Run Keywords
	...		CLI:Test Set Field Invalid Options	name	${VALUE}	AND
	...		CLI:Test Set Field Invalid Options	collector_address	${IP_ADDRESS}
	...		Error: name: Validation error (Note: name cannot contain special characters).
	...		save=yes
	...	ELSE IF	"${NAME}" == "collector_address"	Run Keywords
	...		CLI:Set	name=${NETFLOW_NAME}	AND
	...		CLI:Test Set Field Invalid Options	collector_address	${VALUE}
	...		Error: collector_address: Error in Collector address or port.Error: collector_port: Error in Collector address or port.
	...		save=yes
	...	ELSE	Run Keywords
	...		CLI:Set	name=${NETFLOW_NAME} collector_address=${IP_ADDRESS}	AND
	...		CLI:Test Set Field Invalid Options	${NAME}	${VALUE}
	...		${ERROR}
	...		save=yes

	CLI:Cancel	Raw