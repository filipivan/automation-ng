*** Settings ***
Resource	../../init.robot
Documentation	Tests Netflow Streaming Telemetry configuration
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NETFLOW_NAME}	flowtestautomation
${WRONG_COLLECTOR}	s0m3-wr0ng-d0m41n.not.used
${INTERFACE}	eth0
${PROTOCOL}	ipfix
${AGGREGATION_VALUES1}	src_host,dst_host,vlan,proto,tos,src_port,src_mac,dst_port
${AGGREGATION_VALUES2}	proto,src_host,vlan,src_port
${SAMPLING_RATE1}	5
${SAMPLING_RATE2}	1

*** Test Cases ***
Test Add Netflow Exporter
	CLI:Enter Path	/settings/flow_exporter
	CLI:Add Flow Exporter	${NETFLOW_NAME}	${NETFLOW_COLLECTOR_SERVER}
	...	${NETFLOW_PORT}	${AGGREGATION_VALUES1}	${PROTOCOL}	${SAMPLING_RATE1}
	SUITE:Wait Until Netflow Table Is Matched	${NETFLOW_NAME}	Running	${NETFLOW_COLLECTOR_SERVER}
	...	${NETFLOW_PORT}	${INTERFACE}	${SAMPLING_RATE1}	${AGGREGATION_VALUES1}
	[Teardown]  CLI:Cancel	Raw

Test Edit Netflow Exporter Valid
	CLI:Enter Path	/settings/flow_exporter
	SUITE:Edit Exporter
	CLI:Set	collector_address=${NETFLOW_COLLECTOR_SERVER}
	CLI:Set	collector_port=${NETFLOW_PORT} interface=${INTERFACE}
	CLI:Set	aggregation=${AGGREGATION_VALUES2} sampling_rate=${SAMPLING_RATE2}
	CLI:Commit
	SUITE:Wait Until Netflow Table Is Matched	${NETFLOW_NAME}	Running	${NETFLOW_COLLECTOR_SERVER}
	...	${NETFLOW_PORT}	${INTERFACE}	${SAMPLING_RATE2}	${AGGREGATION_VALUES2}
	[Teardown]  CLI:Cancel	Raw

Test Add Duplicated Name Netflow Exporter
	CLI:Enter Path	/settings/flow_exporter
	CLI:Add
	CLI:Set	collector_address=${NETFLOW_COLLECTOR_SERVER}
	CLI:Test Set Field Invalid Options	name	${NETFLOW_NAME}
	...	Error: name: Entry already exists.	save=yes
	[Teardown]  CLI:Cancel	Raw

Test Disable Netflow Exporter
	SUITE:Disable Exporter
	SUITE:Wait Until Netflow Table Is Matched	${NETFLOW_NAME}	Disabled	${NETFLOW_COLLECTOR_SERVER}
	...	${NETFLOW_PORT}	${INTERFACE}	${SAMPLING_RATE2}	${AGGREGATION_VALUES2}
	[Teardown]  CLI:Revert	Raw

Test Enable Netflow Exporter
	SUITE:Enable Exporter
	SUITE:Wait Until Netflow Table Is Matched	${NETFLOW_NAME}	Running	${NETFLOW_COLLECTOR_SERVER}
	...	${NETFLOW_PORT}	${INTERFACE}	${SAMPLING_RATE2}	${AGGREGATION_VALUES2}
	[Teardown]  CLI:Revert	Raw

Test Delete Netflow Exporter
	SUITE:Delete Netflow Exporter
	[Teardown]  CLI:Revert	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Delete All Netflow Exporters

SUITE:Teardown
	CLI:Delete All Netflow Exporters
	CLI:Close Connection

SUITE:Wait Until Netflow Table Is Matched
	[Arguments]	${NAME}	${STATUS}	${COLLECTOR_ADDRESS}	${PORT}	${INTERFACE}	${SAMPLING_RATE}	${AGGREGATION}
	Wait Until Keyword Succeeds	5x	1s
	...	SUITE:Match Netflow Table	${NAME}	${STATUS}	${COLLECTOR_ADDRESS}	${PORT}	${INTERFACE}	${SAMPLING_RATE}	${AGGREGATION}

SUITE:Match Netflow Table
	[Arguments]  ${NAME}	${STATUS}	${COLLECTOR_ADDRESS}	${PORT}	${INTERFACE}	${SAMPLING_RATE}	${AGGREGATION}
	${AGGREGATION_VALUES}=	Split String	${AGGREGATION}	,
	${NUM_AGGREGATION}=	Get Length	${AGGREGATION_VALUES}
	CLI:Enter Path	/settings/flow_exporter
	CLI:Test Show Command Regexp
	...	${NAME}\\s{2,}${STATUS}\\s{2,}${COLLECTOR_ADDRESS}:${PORT}\\s{2,}${INTERFACE}\\s{2,}1/${SAMPLING_RATE}\\s{2,}${NUM_AGGREGATION}

# Workaround for future changes. Right now it's only possible to use a
# Number for editing and deleting flow exporters
SUITE:Enable Exporter
	CLI:Write	enable 1	Raw
	CLI:Write	enable ${NETFLOW_NAME}	Raw
	CLI:Commit	Raw
	CLI:Show

SUITE:Disable Exporter
	CLI:Write	disable 1	Raw
	CLI:Write	disable ${NETFLOW_NAME}	Raw
	CLI:Commit	Raw
	CLI:Show

SUITE:Delete Netflow Exporter
	${DELETED}=	Run Keyword And Return Status	CLI:Delete	${NETFLOW_NAME}
	Run Keyword If	${DELETED} == False	CLI:Delete	1

SUITE:Edit Exporter
	CLI:Edit	1	Raw
	CLI:Edit	${NETFLOW_NAME}	Raw