*** Settings ***
Resource	../../init.robot
Documentation	Tests Netflow Streaming Telemetry functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Test Teardown	SUITE:Test Teardown

*** Variables ***
${NETFLOW_NAME}	flowtestautomation
${AGGREGATION_VALUES}	src_host,dst_host,proto,tos,src_port,dst_port
${DATE_REGEX}	\\d{4}\\-\\d{2}\\-\\d{2}\\s+\\d{2}:\\d{2}:\\d{2}\\.\\d{3}
${NFDUMP_TABLE_REGEX}	${DATE_REGEX}\\s+\\d+\\.\\d{3}\\s+${HOST}\\s+\\d+\\s+\\d+\\s+\\d+\\s+\\d+\\s+\\d+

*** Test Cases ***
Test Using Ipfix Protocol
	[Setup]	SUITE:Test Setup	ipfix
	SUITE:Wait For Netflow Records To Contain Host Data

Test Using Netflow V5 Protocol
	[Setup]	SUITE:Test Setup	netflow_v5
	SUITE:Wait For Netflow Records To Contain Host Data

Test Using Netflow V9 Protocol
	[Setup]	SUITE:Test Setup	netflow_v9
	SUITE:Wait For Netflow Records To Contain Host Data

Test Using sFlow Protocol
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6
	[Setup]	SUITE:Test Setup	sflow	6363
	SUITE:sFlow Records Should Contain Host Data

*** Keywords ***
SUITE:Setup
	SUITE:Connect To Netflow Collector
	CLI:Open
	SUITE:Test Teardown

SUITE:Teardown
	CLI:Close Connection

SUITE:Connect To Netflow Collector
	Open Connection	${NETFLOW_COLLECTOR_SERVER}	prompt=$	alias=collector_session
	Set Client Configuration	timeout=20s
	Login	${NETFLOW_USERNAME}	${NETFLOW_PASSWD}

SUITE:Test Setup
	[Arguments]	${PROTOCOL}	${PORT}=${NETFLOW_PORT}
	CLI:Switch Connection	default
	CLI:Add Flow Exporter	${NETFLOW_NAME}	${NETFLOW_COLLECTOR_SERVER}
	...	${PORT}	${AGGREGATION_VALUES}	${PROTOCOL}
	SUITE:Setup Collector

SUITE:Test Teardown
	CLI:Switch Connection	default
	CLI:Delete All Netflow Exporters
	CLI:Switch Connection	collector_session
	Execute Command	systemctl stop nfdump	sudo=True	sudo_password=${NETFLOW_PASSWD}
	Execute Command	rm /var/cache/nfdump/*	sudo=True	sudo_password=${NETFLOW_PASSWD}

SUITE:Setup Collector
	CLI:Switch Connection	collector_session
	Execute Command	systemctl start nfdump	sudo=True	sudo_password=${NETFLOW_PASSWD}
	SUITE:Wait For nfdump Service To Start

SUITE:Wait For nfdump Service To Start
	Wait Until Keyword Succeeds	15s	3s	SUITE:nfdump Service Should Have Started
	Log To Console	\nnfdump service up\n

SUITE:nfdump Service Should Have Started
	CLI:Switch Connection	collector_session
	Write Bare	ps -ef | grep nfcapd\n
	${OUTPUT}=	Read Until Prompt
	Should Contain	${OUTPUT}	/var/cache/nfdump -p ${NETFLOW_PORT} -t 30
	Write Bare	netstat -n --udp --listen\n
	${OUTPUT}=	Read Until Prompt
	Should Match Regexp	${OUTPUT}	udp\\s+0\\s+0\\s+0.0.0.0:${NETFLOW_PORT}

SUITE:Wait For Netflow Records To Contain Host Data
	Wait Until Keyword Succeeds	180s	3s	SUITE:Netflow Records Should Contain Host Data

SUITE:Netflow Records Should Contain Host Data
	Log To Console	\nChecking Netflow records data\n
	CLI:Switch Connection	collector_session
	Write Bare	nfdump -R /var/cache/nfdump -A srcip\n
	${OUTPUT}=	Read Until Prompt
	Should Match Regexp	${OUTPUT}	${NFDUMP_TABLE_REGEX}

SUITE:sFlow Records Should Contain Host Data
	Log To Console	\nChecking sFlow records data\n
	CLI:Switch Connection	collector_session
	Write	mkdir /tmp/nfcap-sflow	#workaroud to create the folder in case it doesn't exists. in case it does, it'll only throw an error message
	Read Until Prompt
	Write	sfcapd -E -p 6363 -l /tmp/nfcap-sflow
	Sleep	10s
	Write	\x03
	Write	\x03
	Set Client Configuration	timeout=45s
	${OUTPUT}=	Read Until Prompt
	Run Keyword And Warn On Failure	Should Not Contain	${OUTPUT}	Could not open the requested socket: Address already in use
	Should Contain	${OUTPUT}	FLOW,127.0.0.1,1073741823,1073741823,
	Should Contain	${OUTPUT}	${HOST}
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}