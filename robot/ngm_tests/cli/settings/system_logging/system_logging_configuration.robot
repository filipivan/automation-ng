*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > System Logging... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

*** Test Cases ***
Test editing session_string
	CLI:Enter Path	/settings/system_logging/
	CLI:Set	enable_session_logging=yes enable_session_logging_alerts=yes session_string_1=Hello
	CLI:Test Show Command	enable_session_logging = yes	enable_session_logging_alerts = yes	session_string_1 = Hello
	...	session_string_2	session_string_3	session_string_4	session_string_5
	CLI:Commit
	CLI:Set Field	session_string_1	Hey
	CLI:Test Show Command	enable_session_logging = yes	enable_session_logging_alerts = yes	session_string_1 = Hey
	...	session_string_2	session_string_3	session_string_4	session_string_5
	CLI:Set Field	enable_session_logging	no
	CLI:Commit

Test disabling session logging
	CLI:Enter Path	/settings/system_logging/
	CLI:Set Field	enable_session_logging	no
	CLI:Commit
	CLI:Test Show Command	enable_session_logging = no

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection