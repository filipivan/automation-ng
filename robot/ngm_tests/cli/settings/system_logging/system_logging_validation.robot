*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > System Logging... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/system_logging/
	CLI:Test Available Commands	exit	set	hostname	shell	cd	ls	show	change_password	pwd	show_settings
	...	commit	quit	shutdown	event_system_audit	reboot	whoami	event_system_clear	revert

Test show_settings
	CLI:Enter Path	/settings/system_logging/
	${OUTPUT}=	CLI:Write	show_settings
	Should Match Regexp	${OUTPUT}	/settings/system_logging enable_session_logging=no|yes

Test visible fields for show command
	CLI:Enter Path	/settings/system_logging/
	CLI:Write	set enable_session_logging=no
	CLI:Test Show Command	enable_session_logging
	CLI:Test Not Show Command	enable_session_logging_alerts	session_string_1	session_string_2
	...	session_string_3	session_string_4	session_string_5

	CLI:Write	set enable_session_logging=yes enable_session_logging_alerts=no
	CLI:Test Show Command	enable_session_logging	enable_session_logging_alerts
	CLI:Test Not Show Command	session_string_1	session_string_2	session_string_3	session_string_4
	...	session_string_5

	CLI:Write	set enable_session_logging_alerts=yes
	CLI:Test Show Command	enable_session_logging	enable_session_logging_alerts	session_string_1
	...	session_string_2	session_string_3	session_string_4	session_string_5
	[Teardown]	CLI:Revert

Test validation for field enable_session_logging
	CLI:Enter Path	/settings/system_logging/
	CLI:Write	set enable_session_logging=yes	Raw
	[Teardown]	CLI:Revert

Test validation for field enable_session_logging_alerts
	CLI:Enter Path	/settings/system_logging/
	CLI:Write	set enable_session_logging_alerts=*(&()))	Raw
	[Teardown]	CLI:Revert

Test invalid values for field enable_session_logging
	CLI:Enter Path	/settings/system_logging/
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	enable_session_logging	${EMPTY}	Error: Missing value for parameter: enable_session_logging
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	enable_session_logging	${EMPTY}	Error: Missing value: enable_session_logging
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	enable_session_logging	*(&()))	Error: Invalid value: *(&())) for parameter: enable_session_logging
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	enable_session_logging	*(&()))	Error: Invalid value: *(&()))
	[Teardown]	CLI:Revert

Test invalid values for field enable_session_logging_alerts
	CLI:Enter Path	/settings/system_logging/
	CLI:Set Field	enable_session_logging	yes
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	enable_session_logging_alerts	${EMPTY}	Error: Missing value for parameter: enable_session_logging_alerts
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	enable_session_logging_alerts	${EMPTY}	Error: Missing value: enable_session_logging_alerts
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	enable_session_logging_alerts	*(&()))	Error: Invalid value: *(&())) for parameter: enable_session_logging_alerts
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	enable_session_logging_alerts	*(&()))	Error: Invalid value: *(&()))
	[Teardown]	CLI:Revert

Test available values for field enable_session_logging
	CLI:Enter Path	/settings/system_logging/
	CLI:Test Set Field Options	enable_session_logging	yes	no
	[Teardown]	CLI:Revert

Test available values for field enable_session_logging_alerts
	CLI:Enter Path	/settings/system_logging/
	CLI:Set Field	enable_session_logging	yes
	CLI:Test Set Field Options	enable_session_logging_alerts	no	yes
	[Teardown]	CLI:Revert

Test validation for session_string
	Skip If	'${NGVERSION}' < '4.1'	Error handling only implemented in 4.1
	CLI:Enter Path	/settings/system_logging/
	CLI:Set	enable_session_logging=yes enable_session_logging_alerts=yes
	CLI:Test Set Field Invalid Options	session_string_1	${EXCEEDED}	Error: session_string_1: Exceeded the maximum size for this field.
	CLI:Test Set Field Invalid Options	session_string_2	${EXCEEDED}	Error: session_string_2: Exceeded the maximum size for this field.
	CLI:Test Set Field Invalid Options	session_string_3	${EXCEEDED}	Error: session_string_3: Exceeded the maximum size for this field.
	CLI:Test Set Field Invalid Options	session_string_4	${EXCEEDED}	Error: session_string_4: Exceeded the maximum size for this field.
	CLI:Test Set Field Invalid Options	session_string_5	${EXCEEDED}	Error: session_string_5: Exceeded the maximum size for this field.
	CLI:Set Field	enable_session_logging	no
	CLI:Commit

Test if revert command works
	${OUTPUT}=	CLI:Revert	Raw
	Should Not Contain	${OUTPUT}	Error

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection

