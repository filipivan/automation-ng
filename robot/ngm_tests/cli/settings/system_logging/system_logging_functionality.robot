*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > System Logging Feature tests through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	DATA_LOG
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

*** Test Cases ***
Test Data Log Ls Command
	[Tags]	NON-CRITICAL
	SUITE:Enable Data Log
	CLI:Enter Path	/
	${OUTPUT}=	CLI:Test Available Commands	access_system_clear	access_system_audit
	Wait Until Keyword Succeeds	1m	1s	CLI:Write	access_system_clear
	CLI:Enter Path	/
	CLI:Ls
	@{DATA_LOG_LIST}=	Create List	cd /	ls	access/	system/	settings/
	Write	access_system_audit
	${OUTPUT}=	Read Until	(End)
	CLI:Write	q
	FOR	${ITEM}	IN	@{DATA_LOG_LIST}
		Should Contain	${OUTPUT}	${ITEM}
	END

Test Data Log Show Command
	[Tags]	NON-CRITICAL
	CLI:Enter Path	/
	${OUTPUT}=	CLI:Test Available Commands	access_system_clear	access_system_audit
	Wait Until Keyword Succeeds	1m	1s	CLI:Write	access_system_clear
	CLI:Enter Path	/
	CLI:Write	show settings/system_logging/
	@{DATA_LOG_LIST}=	Create List	cd /	show settings/system_logging/
	...	enable_session_logging = yes	enable_session_logging_alerts = no
	Write	access_system_audit
	${OUTPUT}=	Read Until	access_system_audit
	CLI:Write	q
	FOR	${ITEM}	IN	@{DATA_LOG_LIST}
		Should Contain	${OUTPUT}	${ITEM}
	END

Test Data Log Empty
	[Tags]	NON-CRITICAL
	SUITE:Enable Data Log
	CLI:Enter Path	/
	Wait Until Keyword Succeeds	1m	1s	CLI:Write	access_system_clear
	Write	access_system_audit
	${OUTPUT}=	Read Until	(End)
	CLI:Write	q
	@{DATA_LOG_LIST}=	Create List	cd /	ls	access/	system/	settings/	show settings/system_logging/
	...	enable_session_logging = yes	enable_session_logging_alerts = no	system_config_check	change_password
	...	diagnostic_data	hostname	revert	show_settings	whoami	cloud_enrollment	show
	...	event_system_audit	save_settings	shutdown	add	commit	event_system_clear	pwd	set	software_upgrade
	...	apply_settings	create_csr	exit	quit	shell	system_certificate	delete	factory_settings	reboot
	FOR	${ITEM}	IN	@{DATA_LOG_LIST}
		Should Not Contain	${OUTPUT}	${ITEM}
	END

Test editing session_string
	CLI:Enter Path	/settings/system_logging/
	CLI:Set	enable_session_logging=yes enable_session_logging_alerts=yes session_string_1=Hello
	CLI:Test Show Command	enable_session_logging = yes	enable_session_logging_alerts = yes	session_string_1 = Hello
	...	session_string_2	session_string_3	session_string_4	session_string_5
	CLI:Commit
	CLI:Set Field	session_string_1	Hey
	CLI:Test Show Command	enable_session_logging = yes	enable_session_logging_alerts = yes	session_string_1 = Hey
	...	session_string_2	session_string_3	session_string_4	session_string_5
	CLI:Set Field	enable_session_logging	no
	CLI:Commit

Test disabling session logging
	CLI:Enter Path	/settings/system_logging/
	CLI:Set Field	enable_session_logging	no
	CLI:Commit
	CLI:Test Show Command	enable_session_logging = no

Test enabling session logging
	CLI:Enter Path	/settings/system_logging/
	CLI:Set Field	enable_session_logging	yes
	CLI:Commit
	CLI:Test Show Command	enable_session_logging = yes

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection

SUITE:Enable Data Log
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Set	destination=local
	CLI:Commit
	CLI:Enter Path	/settings/auditing/settings/
	CLI:Set	datalog_destination=file
	CLI:Commit
	CLI:Enter Path	/settings/system_logging/
	CLI:Set	enable_session_logging=yes enable_session_logging_alerts=no
	CLI:Commit