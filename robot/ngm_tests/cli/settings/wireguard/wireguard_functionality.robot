*** Settings ***
Resource	../../init.robot
Documentation	Test Wireguard functionality by creating a tunnel between two NGs and test import export settings
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI		EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
Default Tags	CLI
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
${SP3}				 ${SPACE}${SPACE}${SPACE}
${PEER_NG1}          SEPARATOR=
...   [Peer]\n
...   ${SP3}PublicKey = <NG1 public key in ~/wireguard>\n
...   ${SP3}AllowedIPs = 10.0.10.1/24\n
...   ${SP3}Endpoint = ${HOST}:51820\n

${PEER_NG2}          SEPARATOR=
...   [Peer]\n
...   ${SP3}PublicKey = <NG2 public key in ~/wireguard>\n
...   ${SP3}AllowedIPs = 10.0.10.3/32\n

*** Test Cases ***
Test Wireguard Tunnel With Automatic Connection
	SUITE:Create Interfaces
	SUITE:Add Peers
	CLI:Switch Connection	admin_right
	Wait Until Keyword Succeeds	1 min	3x 	CLI:Test Ping  10.0.10.1	5
	CLI:Switch Connection	admin_left
	Wait Until Keyword Succeeds	1 min	3x 	CLI:Test Ping  10.0.10.3	5

Test Wireguard Tunnel With Manual Restart
	CLI:Enter Path	/settings/wireguard/
	CLI:Write	stop_tunnel ng1
	${TUNNEL_UP}	Run Keyword And Return Status	CLI:Test Ping  10.0.10.3	5	20
	Should Not Be True	${TUNNEL_UP}
	CLI:Write	start_tunnel ng1
	Wait Until Keyword Succeeds	1 min	3x 	CLI:Test Ping  10.0.10.3	5

Test Connection With a Device
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Set	name=ng1 ip_address=10.0.10.3 type=device_console
	CLI:Commit
	SUITE:Send 1 Packet After Stablishing Connection
	CLI:Connect Device  ng1
	CLI:Disconnect Device
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	SUITE:Delete Device

Test case to export the wiregaurd configuration and delete configuration
	SUITE:Export Wiregaurd Configuration
	SUITE:Delete Wiregaurd Configuration

Test case to Import back the configuration and see tunnels are still working
	SUITE:Import Wiregaurd Configuration back
	CLI:Switch Connection	admin_right
	Wait Until Keyword Succeeds	1 min	3x 	CLI:Test Ping  10.0.10.1	5
	CLI:Switch Connection	admin_left
	Wait Until Keyword Succeeds	1 min	3x 	CLI:Test Ping  10.0.10.3	5
	CLI:Enter Path	/settings/wireguard/
	CLI:Write	stop_tunnel ng1
	${TUNNEL_UP}	Run Keyword And Return Status	CLI:Test Ping  10.0.10.3	5	20
	Should Not Be True	${TUNNEL_UP}
	CLI:Write	start_tunnel ng1
	Wait Until Keyword Succeeds	1 min	3x 	CLI:Test Ping  10.0.10.3	5

*** Keywords ***
SUITE:Setup
	CLI:Open	session_alias=admin_left
	SUITE:Set Idle Timeout	0	#workaround because test takes long and sometimes connections go down
	CLI:Open	HOST_DIFF=${HOSTPEER}	session_alias=admin_right
	SUITE:Set Idle Timeout	0	#workaround because test takes long and sometimes connections go down
	CLI:Close Connection	#disconnecting to new idle timeout get affect
	CLI:Open	session_alias=admin_left
	CLI:Connect As Root	session_alias=root_left
	CLI:Open	HOST_DIFF=${HOSTPEER}	session_alias=admin_right
	CLI:Connect As Root	HOST_DIFF=${HOSTPEER}	session_alias=root_right
	SUITE:Delete Interfaces

SUITE:Teardown
	SUITE:Delete Interfaces
	CLI:Close Connection

SUITE:Cat Public Key
	[Arguments]	${INTERFACE}
	CLI:Enter Path	/settings/wireguard/${INTERFACE}
	${OUTPUT}	CLI:Show Settings
	${NUM_LINES}	Set Variable If	'${INTERFACE}' == 'ng1'	-4	-3
	${KEY_LINE}	Get Line	${OUTPUT}	${NUM_LINES}
	${PUBLIC_KEY}	Get Substring	${KEY_LINE}	46
	[Return]	${PUBLIC_KEY}

SUITE:Create Interfaces
	CLI:Switch Connection  	admin_left
	CLI:Enter Path	/settings/wireguard/
	CLI:Add
	CLI:Set	interface_name=ng1 interface_type=server status=enabled internal_address=10.0.10.1/24
	CLI:Set	listening_port=51820 external_address=${HOST} keypair=auto_generate
	CLI:Commit
	${NG1_PUBLIC_KEY}	SUITE:Cat Public Key	ng1
	Set Suite Variable	${NG1_PUBLIC_KEY}
	CLI:Commit
	CLI:Switch Connection	admin_right
	CLI:Enter Path	/settings/wireguard/
	CLI:Add
	CLI:Set	interface_name=ng2 interface_type=client status=enabled internal_address=10.0.10.3/32
	CLI:Set	keypair=auto_generate
	CLI:Commit
	${NG2_PUBLIC_KEY}	SUITE:Cat Public Key	ng2
	Set Suite Variable	${NG2_PUBLIC_KEY}

SUITE:Delete Interfaces
	CLI:Switch Connection 	 admin_left
	SUITE:Set Idle Timeout	300
	CLI:Enter Path  		/settings/wireguard/
	CLI:Delete If Exists  	ng1
	CLI:Commit
	${NG1_DELETED}	CLI:Show
	Should Not Contain	${NG1_DELETED}	ng1
	CLI:Switch Connection  	admin_right
	SUITE:Set Idle Timeout	300
	CLI:Enter Path  		/settings/wireguard/
	CLI:Delete If Exists  	ng2
	CLI:Commit
	${NG2_DELETED}	CLI:Show
	Should Not Contain	${NG2_DELETED}	ng2

SUITE:Set Idle Timeout
	[Arguments]	${TIMEOUT}
	CLI:Enter Path	/settings/system_preferences/
	CLI:Set	idle_timeout=${TIMEOUT}
	CLI:Commit

SUITE:Add Peers
	CLI:Switch Connection	root_left
	${PEER_NG2}				Replace String	${PEER_NG2}		<NG2 public key in ~/wireguard>		${NG2_PUBLIC_KEY}
	CLI:Write  				echo "${PEER_NG2}" > /var/sw/ng2.conf
	CLI:Switch Connection  admin_left
	CLI:Enter Path  	   /settings/wireguard/ng1/peers/
	CLI:Write  				import_peer
	CLI:Set  				name=ng2 configuration_file=ng2.conf
	CLI:Commit
	CLI:Switch Connection	root_right
	${PEER_NG1}				Replace String	${PEER_NG1}		<NG1 public key in ~/wireguard>		${NG1_PUBLIC_KEY}
	CLI:Write  				echo "${PEER_NG1}" > /var/sw/ng1.conf
	CLI:Switch Connection  admin_right
	CLI:Enter Path  	   /settings/wireguard/ng2/peers/
	CLI:Write  			   import_peer
	CLI:Set  			   name=ng1 configuration_file=ng1.conf
	CLI:Commit

SUITE:Send 1 Packet After Stablishing Connection
	CLI:Switch Connection	admin_right
	Run Keyword And Ignore Error	CLI:Test Ping	10.0.10.3	1	30
	CLI:Switch Connection	admin_left
	Run Keyword And Ignore Error	CLI:Test Ping	10.0.10.1	1	30

SUITE:Delete Device
	CLI:Enter Path	/settings/devices
	CLI:Delete If Exists Confirm	ng1
	${DEVICE_DELETED}	CLI:Show
	Should Not Contain	${DEVICE_DELETED}	ng1

SUITE:Import Wiregaurd Configuration back
	CLI:Switch Connection	admin_left
	CLI:Enter Path	/settings/wireguard/
	${OUTPUT}	CLI:Write	import_settings --file 123.cfg
	Should Contain	${OUTPUT}	Rejected paths: 0
	CLI:Switch Connection	admin_right
	CLI:Enter Path		/settings/wireguard/
	${OUTPUT}	CLI:Write	import_settings --file 123.cfg
	Should Contain	${OUTPUT}	Rejected paths: 0

SUITE:Export Wiregaurd Configuration
	CLI:Switch Connection	admin_left
	CLI:Enter Path	/settings/wireguard/
	CLI:Write	export_settings --file 123.cfg --plain-password
	CLI:Switch Connection	admin_right
	CLI:Enter Path		/settings/wireguard/
	CLI:Write	export_settings --file 123.cfg --plain-password

SUITE:Delete Wiregaurd Configuration
	SUITE:Delete Interfaces
