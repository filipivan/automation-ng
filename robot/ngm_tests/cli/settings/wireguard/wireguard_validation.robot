*** Settings ***
Resource	../../init.robot
Documentation	Tests for validation of wireguard fields testing both valid and invalid values
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI		EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
Default Tags	CLI
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SPECIAL_CHAR}		${ONE_POINTS}
${SPECIAL_CHARS}	${ONE_POINTS}
${STRING_SPACE}		"${WORDS_SPACES}"
${STRING_SPACES}	"${WORDS_TWO_SPACES}"
${FAIL_TYPE}		anything
${NAME}				test
*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/wireguard/
	CLI:Test Available commands   add               delete               factory_settings     revert               software_upgrade
							...   apply_settings    diagnostic_data      hostname             save_settings        start_tunnel
							...	  cd                event_system_audit   import_settings      set                  stop_tunnel
							...   change_password   event_system_clear   ls                   shell                system_certificate
							...   cloud_enrollment  exec                 pwd                  show                 system_config_check
							...   commit            exit                 quit                 show_settings        whoami
							...   create_csr        export_settings      reboot               shutdown

Test available set fields
	${fields} =	Create List	dns_server=        fwmark=            interface_type=    listening_port=    private_key=
	...	routing_rules=	external_address=  interface_name=    internal_address=  mtu=               public_key=        status=
	Run Keyword If	${NGVERSION} == 3.2	Append To List	${fields}	forward_delay_\(sec\)	max_age_\(sec\)	hello_time_\(sec\)
	...	ELSE	Append To List	${fields}	forward_delay=	hello_time=	max_age=
	CLI:Add
	CLI:Test Set Available Fields	@{fields}
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field interface_name
	CLI:Enter Path  	/settings/wireguard/
	CLI:Add
	CLI:Test Set Field Invalid Options  interface_name	${EMPTY}			Error: interface_name: Field must not be empty.	yes
	CLI:Test Set Field Invalid Options  interface_name	${SPECIAL_CHAR}		Error: interface_name: Validation error.		yes
	CLI:Test Set Field Invalid Options  interface_name	${SPECIAL_CHARS}	Error: interface_name: Validation error.		yes
	CLI:Test Set Field Invalid Options  interface_name	${STRING_SPACE}		Error: interface_name: Validation error.		yes
	CLI:Test Set Field Invalid Options  interface_name	${STRING_SPACES}	Error: interface_name: Validation error.		yes
	[Teardown]  CLI:Cancel  Raw

Test valid values for field interface_type
	CLI:Enter Path  			/settings/wireguard/
	CLI:Add
	CLI:Test Set Field Options	interface_type	server	client
	[Teardown]  CLI:Cancel  	Raw

Test invalid values for field interface_type
	CLI:Enter Path  					/settings/wireguard/
	CLI:Add
	CLI:Test Set Field Invalid Options  interface_type 	${FAIL_TYPE} 	Error: Invalid value: ${FAIL_TYPE} for parameter: interface_type
	[Teardown]  CLI:Cancel  	Raw

Test valid values for field status
	CLI:Enter Path  			/settings/wireguard/
	CLI:Add
	CLI:Test Set Field Options	status	enabled  disabled
	[Teardown]  CLI:Cancel  	Raw

Test invalid values for field status
	CLI:Enter Path  					/settings/wireguard/
	CLI:Add
	CLI:Test Set Field Invalid Options  status 	${FAIL_TYPE} 	Error: Invalid value: ${FAIL_TYPE} for parameter: status
	[Teardown]  CLI:Cancel  	Raw

Test valid values for field internal_address
	${VALID_ADDRESSES}=	Create List 	0.0.0.0/0 	255.255.255.255/32	8::8
	CLI:Enter Path  	/settings/wireguard/
	CLI:Add
	FOR	${VALID_ADDRESS}	IN	@{VALID_ADDRESSES}
		CLI:Test Set Validate Normal Fields  	internal_address	${VALID_ADDRESS}
	END
	[Teardown]	CLI:Cancel  Raw

Test invalid values for field internal_address
	${INVALID_ADDRESSES}=	Create List 	${EMPTY}	256.256.256.256/32	 8::8::8	a.b.c.d
	CLI:Enter Path  	/settings/wireguard/
	CLI:Add
	FOR	${INVALID_ADDRESS}	IN	@{INVALID_ADDRESSES}
		CLI:Set  			interface_name=${NAME} listening_port=51820		#fields must be filled, otherwise the error would be "field must not be empty"
		CLI:Test Set Field Invalid Options  internal_address	${INVALID_ADDRESS}		Error: internal_address: Invalid IP address.		yes
	END
	[Teardown]	CLI:Cancel  Raw

Test valid values for field listening_port
	${VALID_PORTS}=	Create List 	0 	65535
	CLI:Enter Path  	/settings/wireguard/
	CLI:Add
	FOR	${VALID_PORT}	IN	@{VALID_PORTS}
		CLI:Test Set Validate Normal Fields  	listening_port	${VALID_PORT}
	END
	[Teardown]	CLI:Cancel  Raw

Test invalid values for field listening_port
	CLI:Enter Path  	/settings/wireguard/
	CLI:Add
	CLI:Set  			interface_name=${NAME} internal_address=0.0.0.0/0		#fields must be filled, otherwise the error would be "field must not be empty"
	IF	${NGVERSION} <= 5.0
		CLI:Test Set Field Invalid Options  listening_port	${EMPTY}	Error: listening_port: Validation error.		yes
	ELSE
		CLI:Test Set Field Invalid Options  listening_port	${EMPTY}	Error: listening_port: Invalid port.		yes
	END
	[Teardown]	CLI:Cancel  Raw

Test valid values for field external_address
	${VALID_ADDRESSES}=	Create List 	0.0.0.0/0 	255.255.255.255/32	8::8	${EMPTY}	a.b.c.d
	CLI:Enter Path  	/settings/wireguard/
	CLI:Add
	FOR	${VALID_ADDRESS}	IN	@{VALID_ADDRESSES}
		CLI:Test Set Validate Normal Fields  	external_address	${VALID_ADDRESS}
	END
	[Teardown]	CLI:Cancel  Raw

Test invalid values for field external_address
	${INVALID_ADDRESSES}=	Create List 	256.256.256.256/32	 8::8::8
	CLI:Enter Path  	/settings/wireguard/
	CLI:Add
	FOR	${INVALID_ADDRESS}	IN	@{INVALID_ADDRESSES}
		CLI:Set  			interface_name=${NAME} listening_port=51820 internal_address=0.0.0.0/0	#fields must be filled, otherwise the error would be "field must not be empty"
		CLI:Test Set Field Invalid Options  external_address	${INVALID_ADDRESS}		Error: external_address: Invalid IP address.		yes
	END
	#tries to delete it for the case it creates any of the invalids, it won't fail the other tests because entry already exists
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	CLI:Delete If Exists	${NAME}

Test valid values for field dns_server
	${VALID_ADDRESSES}=	Create List 	0.0.0.0/0 	255.255.255.255/32	${EMPTY}
	CLI:Enter Path  	/settings/wireguard/
	CLI:Add
	FOR	${VALID_ADDRESS}	IN	@{VALID_ADDRESSES}
		CLI:Test Set Validate Normal Fields  	dns_server	${VALID_ADDRESS}
	END
	[Teardown]	CLI:Cancel  Raw

Test invalid values for field dns_server
	${INVALID_ADDRESSES}=	Create List 	256.256.256.256/32	a.b.c.d
	CLI:Enter Path  	/settings/wireguard/
	CLI:Add
	FOR	${INVALID_ADDRESS}	IN	@{INVALID_ADDRESSES}
		CLI:Set  			interface_name=${NAME} listening_port=51820 internal_address=0.0.0.0/0	#fields must be filled, otherwise the error would be "field must not be empty"
		CLI:Test Set Field Invalid Options  dns_server	${INVALID_ADDRESS}		Error: dns_server: Validation error.		yes
	END
	[Teardown]	CLI:Cancel  Raw

Test valid values for field mtu
	${VALID_MTUS}=	Create List 	1 	${NUMBER}		${EMPTY}
	CLI:Enter Path  	/settings/wireguard/
	CLI:Add
	FOR	${VALID_MTU}	IN	@{VALID_MTUS}
		CLI:Test Set Validate Normal Fields  	mtu		${VALID_MTU}
	END
	[Teardown]	CLI:Cancel  Raw

Test invalid values for field mtu
	${INVALID_MTUS}=	Create List 	0 	${ELEVEN_NUMBER}
	CLI:Enter Path  	/settings/wireguard/
	CLI:Add
	FOR	${INVALID_MTU}	IN	@{INVALID_MTUS}
		CLI:Set  			interface_name=${NAME} listening_port=51820 internal_address=0.0.0.0/0	#fields must be filled, otherwise the error would be "field must not be empty"
		IF	${NGVERSION} <= 5.0
			CLI:Test Set Field Invalid Options  mtu		${INVALID_MTU}		Error: mtu: Validation error.		yes
		ELSE
			CLI:Test Set Field Invalid Options  mtu		${INVALID_MTU}		Error: mtu: Invalid MTU		yes
		END
	END
	[Teardown]	CLI:Cancel  Raw

Test valid values for field keypair
	CLI:Enter Path  			/settings/wireguard/
	CLI:Add
	CLI:Test Set Field Options	keypair	auto_generate	input_manually
	[Teardown]  CLI:Cancel  	Raw

Test invalid values for field keypair
	CLI:Enter Path  					/settings/wireguard/
	CLI:Add
	CLI:Test Set Field Invalid Options  keypair 	${FAIL_TYPE} 	Error: Invalid value: ${FAIL_TYPE} for parameter: keypair
	[Teardown]  CLI:Cancel  	Raw
*** Keywords ***
SUITE:Setup
	CLI:Open  session_alias=HOST

SUITE:Teardown
	CLI:Close Connection