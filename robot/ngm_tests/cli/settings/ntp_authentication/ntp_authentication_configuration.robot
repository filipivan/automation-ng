*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > NTP Authentication Configuration ... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_0
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${KEY}	2

*** Test Cases ***
Test adding valid NTP
	CLI:Enter Path	/settings/ntp_authentication/
	CLI:Add
	CLI:Set	key_number=${KEY} hash_algorithm=sha1 password=password
	CLI:Commit
	CLI:Test Show Command Regexp  ${KEY}\\s+SHA1

Test drilldown on NTP
	CLI:Enter Path	/settings/ntp_authentication/
	CLI:Test Ls Command	${KEY}
	CLI:Enter Path	${KEY}
	#CLI:Test Show Command  key number: ${KEY}	hash_algorithm = sha1	password
	CLI:Test Show Command Regexp    key( |_)number: ${KEY}	hash_algorithm = sha1	password

Test editing NTP
	CLI:Enter Path	/settings/ntp_authentication/${KEY}
	CLI:Set Field	hash_algorithm	sha256
	CLI:Commit
	#	CLI:Test Show Command	key number: ${KEY}	hash_algorithm = sha256	password
	CLI:Test Show Command Regexp	key( |_)number: ${KEY}	hash_algorithm = sha256	password
	CLI:Enter Path	/settings/ntp_authentication/
	CLI:Test Show Command Regexp	${KEY}\\s+SHA256

Test adding duplicate entry
	CLI:Enter Path  /settings/ntp_authentication/
	CLI:Add
	CLI:Set	key_number=${KEY} hash_algorithm=sha1 password=password
	${OUTPUT}=	CLI:Commit  Raw
	Should Contain 	${OUTPUT}	Error: key_number: Entry already exists.
	[Teardown]  CLI:Cancel 	Raw

Test deleting NTP
	CLI:Enter Path	/settings/ntp_authentication/
	CLI:Delete	${KEY}
	${OUTPUT}=	CLI:Ls
	Should Not Contain	${OUTPUT}	${KEY}

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/ntp_authentication/
	CLI:Delete All

SUITE:Teardown
	CLI:Enter Path	/settings/ntp_authentication/
	CLI:Delete All
	CLI:Close Connection