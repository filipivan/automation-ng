*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > NTP Authentication Validating fields ... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_0
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/ntp_authentication/
	${list}=	Create List	exit	set	add	factory_settings	shell	apply_settings	hostname	show
	...	cd	ls	show_settings	change_password	pwd	shutdown	commit	quit	software_upgrade
	...	delete	reboot	system_certificate	event_system_audit	revert	system_config_check	event_system_clear
	...	save_settings	whoami
	CLI:Test Available Commands	@{list}

Test visible fields for show command
	CLI:Test Show Command	key number	hash algorithm

Test show_settings command
	CLI:Enter Path	/settings/ntp_authentication/
	${OUTPUT}=	CLI:Show Settings
	Should Not Contain	${OUTPUT}	/settings/ntp_authentication

Test available set fields for adding
	CLI:Enter Path	/settings/ntp_authentication/
	CLI:Add
	CLI:Test Set Available Fields	hash_algorithm	key_number	password
	[Teardown]	CLI:Cancel	Raw

Test available show fields for adding
	CLI:Enter Path	/settings/ntp_authentication/
	CLI:Add
	CLI:Test Show Command	key_number	hash_algorithm = sha256	password
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=key_number
	CLI:Enter Path	/settings/ntp_authentication/
	CLI:Add
	CLI:Test Set Field Invalid Options	key_number	${EMPTY}	Error: key_number: Field must not be empty.	yes
	@{LIST}=	Create List	${WORD}	${POINTS}	${ONE_WORD}	${ONE_POINTS}	${EXCEEDED}
	FOR	${VALUE}	IN	@{LIST}
		CLI:Test Set Field Invalid Options	key_number	${VALUE}	Error: key_number: This field only accepts integers. 	yes
	END
	[Teardown]	CLI:Cancel	Raw

Test available options for field=hash_algorithm
	CLI:Enter Path	/settings/ntp_authentication/
	CLI:Add
	CLI:Test Set Field Options	hash_algorithm	md5	sha1	sha3-224	sha3-384	sha384
	...	rmd160	sha256	sha3-256	sha3-512	sha512
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=hash_algorithm
	CLI:Enter Path	/settings/ntp_authentication/
	CLI:Add
	CLI:Test Set Validate Invalid Options	hash_algorithm	${EMPTY}	Error: Missing value for parameter: hash_algorithm
	CLI:Test Set Validate Invalid Options	hash_algorithm	${WORD}	Error: Invalid value: ${WORD} for parameter: hash_algorithm
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection