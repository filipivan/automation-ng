*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Auto Discovery Discovery Rules... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW
Suite Setup	CLI:Open
Suite Teardown	Close Connection

*** Variables ***
${CLIPATHROOT}=	/settings/auto_discovery/hostname_detection
${CLIPATHSTRINGS}=	${CLIPATHROOT}/string_settings
${CLIPATHGLOBAL}=	${CLIPATHROOT}/global_setting

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/auto_discovery/
	CLI:Test Available Commands	add	delete	hostname	revert	show_settings	whoami
	...	apply_settings	event_system_audit	ls	save_settings	shutdown
	...	cd	event_system_clear	pwd	set	software_upgrade
	...	change_password	exit	quit	shell	system_certificate
	...	commit	factory_settings	reboot	show	system_config_check

Test ls command
	CLI:Enter Path	/settings/auto_discovery/
	CLI:Test Ls Command	network_scan	vm_managers	discovery_rules	hostname_detection	discovery_logs	discover_now

Test show_settings command
	CLI:Enter Path	/settings/auto_discovery/

	Run Keyword If	${NGVERSION} <= 4.2	Write	show_settings
	...	ELSE	Write	export_settings

	${OUTPUT}=  CLI:Read Until Prompt
	Run Keyword If  '${NGVERSION}' >= '4.0'	Should Match Regexp	${OUTPUT}	(?i)${CLIPATHSTRINGS}/probe\.1 string( |_)type=probe
	Run Keyword If  '${NGVERSION}' >= '4.0'	Should Match Regexp	${OUTPUT}	(?i)${CLIPATHSTRINGS}/probe\.2 string( |_)type=probe
	Run Keyword If  '${NGVERSION}' >= '4.0'	Should Match Regexp	${OUTPUT}	(?i)${CLIPATHSTRINGS}/match\.1 string( |_)type=match
	Run Keyword If  '${NGVERSION}' >= '4.0'	Should Match Regexp	${OUTPUT}	(?i)${CLIPATHSTRINGS}/match\.2 string( |_)type=match
	Run Keyword If  '${NGVERSION}' >= '4.0'	Should Match Regexp	${OUTPUT}	(?i)${CLIPATHSTRINGS}/match\.3 string( |_)type=match
	Run Keyword If  '${NGVERSION}' >= '4.0'	Should Match Regexp	${OUTPUT}	(?i)${CLIPATHSTRINGS}/match\.4 string( |_)type=match
	Run Keyword If  '${NGVERSION}' >= '4.0'	Should Match Regexp	${OUTPUT}	(?i)${CLIPATHSTRINGS}/match\.5 string( |_)type=match

	Run keyword if	${NGVERSION} <= 4.2
	...	Should Match Regexp	${OUTPUT}	(?i)${CLIPATHSTRINGS}/probe\.1 string=\\\\r
	...	ELSE	Should Match Regexp	${OUTPUT}	(?i)${CLIPATHSTRINGS}/probe\.1 string=\\\\\\\\r

	Run keyword if	${NGVERSION} <= 4.2
	...	Should Match Regexp	${OUTPUT}	(?i)${CLIPATHSTRINGS}/probe\.2 string=\\\\n
	...	ELSE	Should Match Regexp	${OUTPUT}	(?i)${CLIPATHSTRINGS}/probe\.2 string=\\\\\\\\n

	Should Match Regexp	${OUTPUT}	(?i)${CLIPATHSTRINGS}/match\.1 string=%H
	Should Match Regexp	${OUTPUT}	(?i)${CLIPATHSTRINGS}/match\.2 string="\\[
	Should Match Regexp	${OUTPUT}	(?i)${CLIPATHSTRINGS}/match\.3 string=%H!login:
	Should Match Regexp	${OUTPUT}	(?i)${CLIPATHSTRINGS}/match\.4 string="ACS\\[v\\]\\?6000
	Should Match Regexp	${OUTPUT}	(?i)${CLIPATHSTRINGS}/match\.5 string="Avocent\\sPM

	Should Match Regexp	${OUTPUT}	(?i)${CLIPATHGLOBAL} probe_timeout=\\d+
	Should Match Regexp	${OUTPUT}	(?i)${CLIPATHGLOBAL} number_of_retries=\\d+
	Should Match Regexp	${OUTPUT}	(?i)${CLIPATHGLOBAL} update_device_name=yes|no
	Should Match Regexp	${OUTPUT}	(?i)${CLIPATHGLOBAL} new_discovered_device_receives_the_name_during_conflict=no|yes 
