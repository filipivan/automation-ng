*** Settings ***
Resource	../../init.robot
Documentation	Test Auto-Purge disabled/inactive ports
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2   NON-CRITICAL   NEED_REVIEW   BUG_NG_9910
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE_NAME}	perle-test
${DEVICE_TYPE}	${PERLE_SCS_TYPE}
${DEVICE_IP}	${PERLE_SCS_SERVER_IP}
${DEVICE_USERNAME}	${PERLE_SCS_SERVER_USERNAME}
${DEVICE_PASSWORD}	${PERLE_SCS_SERVER_PASSWORD}
${RULE_NAME}	perle-rule-test
${RULE_MODE}	console_server_ports
${RULE_ACTION}	clone_mode_on-demand
${NL}	${PERLE_SCS_NEW_LINE}
${PROMPT}	${PERLE_SCS_PROMPT}
${AVAILABLE_PORT}	${PERLE_SCS_AVAILABLE_PORTS[0]}

*** Test Cases ***
Test Disable Serial Port
	SUITE:Enable Auto-Purge On Device	disable_ports
	SUITE:Disable Device Serial Port
	SUITE:Wait For Discovery Logs To Match Regexp	Device Disabled

Test Delete Serial Port
	SUITE:Enable Auto-Purge On Device	remove_ports
	SUITE:Disable Device Serial Port
	SUITE:Wait For Discovery Logs To Match Regexp	Device Removed

*** Keywords ***
SUITE:Setup
	CLI:General Open	${DEVICE_IP}	${DEVICE_USERNAME}	${DEVICE_PASSWORD}	device_session	prompt=${PROMPT}
	SUITE:Enable Device Serial Port

	CLI:Open
	${TIME_SECONDS}=	CLI:Get Current System Time In Seconds
	Set Suite Variable	${TIME_SECONDS}

	CLI:Delete Rules	${RULE_NAME}
	CLI:Delete All Devices

	CLI:Add Device	${DEVICE_NAME}	${DEVICE_TYPE}	${DEVICE_IP}	${DEVICE_USERNAME}
	...	${DEVICE_PASSWORD}	on-demand
	CLI:Add Rule	${RULE_NAME}	${RULE_MODE}	${DEVICE_NAME}	enabled	${RULE_ACTION}
	SUITE:Wait For Discovery Logs To Match Regexp	Device Cloned

SUITE:Teardown
	SUITE:Enable Device Serial Port
	CLI:Switch Connection	default
	CLI:Delete Rules	${RULE_NAME}
	CLI:Delete All Devices
	SUITE:Reset Discovery Logs
	CLI:Close Connection

SUITE:Enable Auto-Purge On Device
	[Arguments]	${ACTION}
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/management
	CLI:Set	purge_disabled_end_point_ports=yes action=${ACTION}
	CLI:Commit

SUITE:Reset Discovery Logs
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/auto_discovery/discovery_logs
	CLI:Write	reset_logs

SUITE:Wait For Discovery Logs To Match Regexp
	[Arguments]	${REGEXP}
	CLI:Switch Connection	default
	Wait Until Keyword Succeeds	2x	1s
	...	SUITE:Discover Device And Wait For Discovery Logs To Match Regexp	${REGEXP}

SUITE:Discover Device And Wait For Discovery Logs To Match Regexp
	[Arguments]	${REGEXP}
	CLI:Discover Now	${DEVICE_NAME}	${DEVICE_TYPE}
	Wait Until Keyword Succeeds	45s	3s
	...	CLI:Discovery Logs Should Match Regexp And Time	${REGEXP}	${TIME_SECONDS}

SUITE:Disable Device Serial Port
	CLI:Switch Connection	device_session
	Write Bare	set line ${AVAILABLE_PORT} mode disabled${NL}
	${OUTPUT}=	Read Until Prompt
	Log To Console	\n${OUTPUT}

SUITE:Enable Device Serial Port
	CLI:Switch Connection	device_session
	Write Bare	set line ${AVAILABLE_PORT} mode enabled${NL}
	${OUTPUT}=	Read Until Prompt
	Log To Console	\n${OUTPUT}
	Write Bare	kill line ${PERLE_SCS_AVAILABLE_PORTS[0]}${PERLE_SCS_NEW_LINE}
	${OUTPUT}=	Read Until Prompt
	Log To Console	\n${OUTPUT}