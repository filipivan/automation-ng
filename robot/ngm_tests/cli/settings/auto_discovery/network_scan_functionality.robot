*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Auto Discovery Network Scan... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Test Setup	SUITE:Add a device and network_scan
Test Teardown	SUITE:Delete device and network_scan

*** Variables ***
${SCAN_ID}	dummy_network_scan
${IP_RANGE_START}	127.0.0.10
${IP_RANGE_END}	127.0.0.15
${DEVICE}	dummy_device_network

*** Test Cases ***
Test discovery_logs
	CLI:Enter Path	/settings/auto_discovery/discovery_logs/
	#CLI:Test Show Command Regexp	(\\S+\\s+){5}${IP_RANGE_START}\\s+${IP_RANGE_END}\\s+Network\\sScan\\s+Discovery\\sDisabled
	${start_num}=	Fetch From Right	${IP_RANGE_START}	.
	${length}=	Get Length	${start_num}
	${start_num}=	Convert To Integer	${start_num}
	${end_num}=	Fetch From Right	${IP_RANGE_END}	.
	${end_num}=	Convert To Integer	${end_num}

	${IP_PART}=	Get Substring	${IP_RANGE_START}	0	-${length}
	Sleep	5
	FOR		${index}	IN RANGE	${start_num}	${end_num}
		Run Keyword If	'${NGVERSION}' >= '4.0'	Wait Until Keyword Succeeds	1m	10s	CLI:Test Show Command Regexp	${IP_PART}${index}\\s+${IP_PART}${index}\\s+Network Scan\\s+None
		Run Keyword If	'${NGVERSION}' < '4.0'	Wait Until Keyword Succeeds	1m	10s	CLI:Test Show Command Regexp	${IP_PART}${index}\\s+${IP_PART}${index}\\s+network scan\\s+none
	END

*** Keyword ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection

SUITE:Set Invalid Values Error Messages
	[Arguments]	${COMMANDS}
	FOR		${COMMAND}	IN	@{COMMANDS}
		Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${COMMAND}	a	Error: Invalid value: a for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value: ${COMMAND}
		Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${COMMAND}	a	Error: Invalid value: a
	END

SUITE:Get Device Error
	CLI:Enter Path	/settings/devices
	@{DEVICELIST}=	CLI:Test Get Devices
	${LENGTH}=	Get Length	${DEVICELIST}
	${DEVICE}=	Run Keyword If	'${LENGTH}' == '1'	set variable	Error: device: Validation error.
	...	ELSE	set variable	${EMPTY}
	[Return]	${DEVICE}

SUITE:Add a device and network_scan
	CLI:Delete Devices	${DEVICE}
	CLI:Add Device	${DEVICE}	device_console	127.0.0.1
	CLI:Enter Path	/settings/auto_discovery/discovery_logs/
	CLI:Write	reset_logs
	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Delete If Exists Confirm	${SCAN_ID}
	CLI:Add
	CLI:Set	scan_id=${SCAN_ID} ip_range_start=${IP_RANGE_START} ip_range_end=${IP_RANGE_END} device=${DEVICE}
	CLI:Commit

SUITE:Delete device and network_scan
	CLI:Enter Path	/settings/auto_discovery/network_scan
	CLI:Delete If Exists Confirm	${SCAN_ID}
	CLI:Delete Devices	${DEVICE}
	CLI:Enter Path	/settings/auto_discovery/discovery_logs/
	CLI:Write	reset_logs