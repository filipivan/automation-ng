*** Settings ***
Resource	    ../../init.robot
Documentation	Test auto discovery KVM Ports
Metadata	    Version	1.0
Metadata	    Executed At	${HOST}
Force Tags	    CLI
Default Tags	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${TEMPLATE_DEVICE}	test.template-dev
${RARITAN_DEVICE}	test.raritan-kvm-dev
${RARITAN_KVM_RULE}	test-raritan-kvm-rule

*** Test Cases ***
Test Raritan KVM Ports are discovered
	Run Keyword If	'${NGVERSION}' <= '5.0'	Set Tags	NON-CRITICAL
	Skip If	not ${RARITAN_KVM_PRESENT}	Raritan KVM not available in automation environment
	Wait Until Keyword Succeeds	6x	10s	SUITE:Wait Until Raritan KVM Ports Are Discovered

*** Keywords ***
SUITE:Setup
	Skip If	not ${RARITAN_KVM_PRESENT}	Raritan KVM not available in automation environment
	CLI:Open
	${TIME_SECONDS}=	CLI:Get Current System Time In Seconds
	Set Suite Variable	${TIME_SECONDS}

	CLI:Delete Rules	${RARITAN_KVM_RULE}
	CLI:Delete All Devices
	SUITE:Add Devices For KVM Ports Discovery
	SUITE:Add KVM Ports Discovery Rule

SUITE:Teardown
	Skip If	not ${RARITAN_KVM_PRESENT}	Raritan KVM not available in automation environment
	CLI:Reset Discovery Logs
	CLI:Delete Rules	${RARITAN_KVM_RULE}
	CLI:Delete All Devices
	CLI:Close Connection

SUITE:Add Devices For KVM Ports Discovery
	CLI:Add Device	${TEMPLATE_DEVICE}	${RARITAN_KVM_TYPE}	127.0.0.1	MODE=disabled
	CLI:Add Device	${RARITAN_DEVICE}	${RARITAN_KVM_TYPE}	${RARITAN_KVM_IP}
	...	${RARITAN_KVM_USERNAME}	${RARITAN_KVM_PASSWORD}	on-demand

SUITE:Add KVM Ports Discovery Rule
	${RARITAN_KVM_PORT}=	Get From List	${RARITAN_KVM_AVAILABLE_PORTS}	0
	CLI:Add Rule	${RARITAN_KVM_RULE}	kvm_ports	${TEMPLATE_DEVICE}	enabled
	...	clone_mode_on-demand	port_list=${RARITAN_KVM_PORT}

SUITE:Wait Until Raritan KVM Ports Are Discovered
	CLI:Discover Now	${RARITAN_DEVICE}	kvm
	CLI:Enter Path	/settings/auto_discovery/discovery_logs/
	Wait Until Keyword Succeeds	45s	3s	CLI:Discovery Logs Should Match Regexp And Time
	...	${RARITAN_KVM_IP}.*(KVM|kvm) (P|p)orts.*(D|d)evice (C|c)loned	${TIME_SECONDS}