*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Auto Discovery Network Scan... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Test Available Commands	event_system_clear	set	exit	shell	add	hostname	show	cd	ls	show_settings
	...	change_password	pwd	shutdown	commit	quit	whoami	delete	reboot	event_system_audit	revert

Test visible fields for show commands
	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Test Show Command	scan id	ip range	status	similar devices	port scan	ping	interval

Test available value for field enable_scanning after add
	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Add
	CLI:Test Set Field Options	enable_scanning	yes	no
	[Teardown]	CLI:Cancel	Raw

Test set available fields after set ping, port_scan and similar_devices = yes
	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Add
	CLI:Set	ping=yes port_scan=yes similar_devices=yes
	CLI:Test Set Available Fields	device	enable_scanning	ip_range_end	ip_range_start	ping	port_list
	...	port_scan	scan_id	scan_interval	similar_devices
	[Teardown]	CLI:Cancel	Raw

Test available commands after invalid set operations
	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Add
	CLI:Commands After Set Error	enable_scanning	\\_\\)\\(\\*&¨%	cancel	commit	ls	save	set	show
	CLI:Commands After Set Error	ping	\\_\\)\\(\\*&¨%	cancel	commit	ls	save	set	show
	CLI:Commands After Set Error	port_scan	\\_\\)\\(\\*&¨%	cancel	commit	ls	save	set	show
	CLI:Commands After Set Error	similar_devices	\\_\\)\\(\\*&¨%	cancel	commit	ls	save	set	show
	[Teardown]	CLI:Cancel	Raw

Test available values for field ping
	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Add
	CLI:Test Set Field Options	ping	no	yes
	[Teardown]	CLI:Cancel	Raw

Test available values for field port_scan
	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Add
	CLI:Test Set Field Options	port_scan	no	yes
	[Teardown]	CLI:Cancel	Raw

Test set available fields after set port_scan = no
	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Add
	CLI:Set Field	port_scan	no
	CLI:Test Set Available Fields	device	enable_scanning	ip_range_end	ip_range_start	ping	port_scan	scan_id	scan_interval	similar_devices
	CLI:Test Set Unavailable Fields	port_list
	[Teardown]	CLI:Cancel	Raw

Test available value for field similar_devices
	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Add
	CLI:Test Set Field Options	similar_devices	no	yes
	[Teardown]	CLI:Cancel	Raw

Test set available fields after set similar_devices = no
	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Add
	CLI:Set Field	similar_devices	no
	CLI:Test Set Available Fields	enable_scanning	ip_range_end	ip_range_start	ping	port_scan
	...	scan_id	scan_interval	similar_devices	port_list
	CLI:Test Set Unavailable Fields	device
	CLI:Set Field	port_scan	no
	CLI:Test Set Available Fields	enable_scanning	ip_range_end	ip_range_start	ping	port_scan
	...	scan_id	scan_interval	similar_devices
	CLI:Test Set Unavailable Fields	port_list	device
	CLI:Set	port_scan=yes similar_devices=yes
	CLI:Test Set Available Fields	enable_scanning	ip_range_end	ip_range_start	ping	port_scan	scan_id
	...	scan_interval	similar_devices	port_list	device
	[Teardown]	CLI:Cancel	Raw

Test available value for field enable_scanning
	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Add
	CLI:Test Set Field Options	enable_scanning	no	yes
	[Teardown]	CLI:Cancel	Raw

Test validation for field scan_id
	${DEVICE}=	SUITE:Get Device Error
	Set Suite Variable	${DEVICE}

	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Add
	#Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	scan_id	${EMPTY}	Error: scan_id: Scan ID must not be empty	yes
	Run Keyword If	'${NGVERSION}' >= '4.2' and '${NGVERSION}' < '5.0'	CLI:Test Set Field Invalid Options	scan_id	${EMPTY}	Error: scan_id: Scan ID must not be empty	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	scan_id	${EMPTY}	Error: scan id: Field must not be empty.	yes
	CLI:Write	set ip_range_start=::1 ip_range_end=::2
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	scan_id	${EXCEEDED}	Error: scan_id: Validation error.${DEVICE}	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	scan_id	${EXCEEDED}	Error: scan id: Validation error.	yes

	CLI:Test Set Field Valid Options	scan_id	scan	no
	[Teardown]	CLI:Cancel	Raw

Test validation for field port_list
	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Add
	CLI:Write	set scan_id=1 ip_range_start=::1 ip_range_end=::2
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	port_list	${ONE_WORD}	${DEVICE}Error: port_list: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	port_list	${ONE_WORD}	Error: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	port_list	${POINTS}	${DEVICE}Error: port_list: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	port_list	${POINTS}	Error: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	port_list	${EXCEEDED}	${DEVICE}Error: port_list: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	port_list	${EXCEEDED}	Error: Validation error.	yes
	CLI:Test Set Field Valid Options	port_list	12	no

	[Teardown]	CLI:Cancel	Raw

Test validation for field scan_interval
	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Add
	CLI:Write	set scan_id=1 ip_range_start=::1 ip_range_end=::2
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	scan_interval	${EMPTY}	Error: scan_interval: Validation error.${DEVICE}	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	scan_interval	${EMPTY}	Error: interval: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	scan_interval	${ONE_WORD}	Error: scan_interval: Validation error.${DEVICE}	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	scan_interval	${ONE_WORD}	Error: interval: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	scan_interval	${POINTS}	Error: scan_interval: Validation error.${DEVICE}	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	scan_interval	${POINTS}	Error: interval: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	scan_interval	${EXCEEDED}	Error: scan_interval: Exceeded the maximum size for this field.${DEVICE}	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	scan_interval	${EXCEEDED}	Error: interval: Exceeded the maximum size for this field.	yes
	CLI:Test Set Field Valid Options	scan_interval	12	no
	[Teardown]	CLI:Cancel	Raw

Test validation for field ip_range_start/ip_range_end=Empty value
	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Add
	CLI:Set Field	scan_id	1
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ip_range_start	${EMPTY}	Error: ip_range_start: Validation error.Error: ip_range_end: Validation error	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_range_start	${EMPTY}	Error: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_range_end	${EMPTY}	Error: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

Test validation for field ip_range_start/ip_range_end=Invalid IPs
	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Add
	CLI:Set Field	scan_id	1
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ip_range_start	${ONE_WORD}	Error: ip_range_start: Validation error.Error: ip_range_end: Validation error	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ip_range_end	${ONE_WORD}	Error: ip_range_start: Validation error.Error: ip_range_end: Validation error	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ip_range_end	127.0.0.1	Error: ip_range_start: Validation error	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Set Field	ip_range_start	127.0.0.2
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_range_start	${ONE_WORD}	Error: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_range_end	${ONE_WORD}	Error: Validation error.	yes
	CLI:Test Set Field Valid Options	ip_range_start	::1	no
	CLI:Test Set Field Valid Options	ip_range_end	::2	no
	[Teardown]	CLI:Cancel	Raw

Test validation for field ip_range_start/ip_range_end=Exceeded value
	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Add
	CLI:Set Field	scan_id	1
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ip_range_start	${EXCEEDED}	Error: ip_range_start: Validation error.Error: ip_range_end: Validation error	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ip_range_end	${EXCEEDED}	Error: ip_range_start: Validation error.Error: ip_range_end: Validation error	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_range_start	${EXCEEDED}	Error: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_range_end	${EXCEEDED}	Error: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

Test set available fields after set enable_scanning = no
	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Add
	CLI:Set Field	enable_scanning	no
	CLI:Test Set Available Fields	enable_scanning	ip_range_end	ip_range_start	scan_id	scan_interval
	CLI:Test Set Unavailable Fields	port_list	device	ping	port_scan	similar_devices
	[Teardown]	CLI:Cancel	Raw

Test invalid values for fields for network_scan
	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Add
	${COMMANDS}=	Create List
	Append To List	${COMMANDS}	enable_scanning	similar_devices	port_scan	ping
	SUITE:SET INVALID VALUES ERROR MESSAGES	${COMMANDS}
	[Teardown]	CLI:Cancel	Raw

Test cancel command working after error
	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Add
	Write	enable_scanning = NOTVALID
	CLI:Read Until Prompt	Raw
	${OUTPUT}=	CLI:Cancel
	Should Not Contain	${OUTPUT}	Error
	[Teardown]	CLI:Cancel	Raw

Test if commit is raising error after add empty record
	CLI:Add
	${OUTPUT}=	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error
	[Teardown]	CLI:Cancel	Raw

Test cancel command working after error2
	CLI:Add
	${OUTPUT}=	CLI:Cancel
	Should Not Contain	${OUTPUT}	Error

*** Keyword ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection

SUITE:Set Invalid Values Error Messages
	[Arguments]	${COMMANDS}
	FOR		${COMMAND}	IN	@{COMMANDS}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value: ${COMMAND}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}
	END

SUITE:Get Device Error
	CLI:Enter Path	/settings/devices
	@{DEVICELIST}=	CLI:Test Get Devices
	${LENGTH}=	Get Length	${DEVICELIST}
	${DEVICE}=	Run Keyword If	'${LENGTH}' == '1'	Set Variable	Error: device: Validation error.
	...	ELSE	Set Variable	${EMPTY}
	[Return]	${DEVICE}