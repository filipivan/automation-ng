*** Settings ***
Resource	../../../init.robot
Documentation	Tests Settings > Auto Discovery > VM Managers > Install VMRC Bundle... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${FTP_VMRC}	${FTPSERVER2_URL}${FTPSERVER2_VMRC_PATH}
${FTP_USERNAME}	${FTPSERVER2_USER}
${FTP_PASSWORD}	${FTPSERVER2_PASSWORD}

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/auto_discovery/vm_managers/
	CLI:Test Available Commands	add	commit	exit	ls	revert	show	system_certificate
	...	apply_settings	delete	factory_settings	pwd	save_settings	show_settings	system_config_check
	...	cd	event_system_audit	hostname	quit	set	shutdown	whoami
	...	change_password	event_system_clear	install_vmrc	reboot	shell	software_upgrade

Test Install VMRC Bundle Remote
	SUITE:Remove VMRC Bundle If Exists
	CLI:Enter Path	/settings/auto_discovery/vm_managers/
	CLI:Write	install_vmrc
	${OUTPUT}=	CLI:Write	show	Raw	Yes
	Should Contain	${OUTPUT}	destination
	Should Contain	${OUTPUT}	filename

	CLI:Set	destination=remote_server url=${FTP_VMRC} username=${FTP_USERNAME} password=${FTP_PASSWORD} download_path_is_absolute_path_name=yes
	Write	save
	Set Client Configuration	timeout=5s
	${SAVE_OUTPUT}=	Wait Until Keyword Succeeds	4m	5s	CLI:Read Until Prompt
	Set Client Configuration	timeout=30s
	Should Not Contain	${SAVE_OUTPUT}	Error: File Transfer Failed
#	TODO: Installation: add success/failure message validation accordingly with the Bug 1157

	CLI:Test Not Available Commands	install_vmrc

	[Teardown]	CLI:Cancel	Raw

Test Remove VMRC Bundle
	SUITE:Remove VMRC Bundle If Exists
	CLI:Test Not Available Commands	remove_vmrc

#	TODO: Test install vmrc bundle file from automation local environment
#Test Install VMRC Bundle Local Computer
#	CLI:Enter Path	/settings/auto_discovery/vm_managers/

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Remove VMRC Bundle If Exists

SUITE:Teardown
	SUITE:Remove VMRC Bundle If Exists
	CLI:Close Connection

SUITE:Remove VMRC Bundle If Exists
	CLI:Enter Path	/settings/auto_discovery/vm_managers/
	${CHECK}=	Run Keyword and Return Status	CLI:Test Not Available Commands	install_vmrc
	Run Keyword If	${CHECK}	CLI:Write	remove_vmrc	Raw


