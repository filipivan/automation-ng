*** Settings ***
Resource	../../../init.robot
Documentation	Tests Validation > Auto Discovery > VM Managers > Install VMRC Bundle... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI	DEPENDENCE	DEPENDENCE_SERVER	FTP

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${FTP_VMRC10}	${FTPSERVER2_URL}${FTPSERVER2_VMRC10_PATH}
${FTP_USERNAME}	${FTPSERVER2_USER}

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/auto_discovery/vm_managers/
	CLI:Test Available Commands	add	commit	exit	ls	revert	show	system_certificate
	...	apply_settings	delete	factory_settings	pwd	save_settings	show_settings	system_config_check
	...	cd	event_system_audit	hostname	quit	set	shutdown	whoami
	...	change_password	event_system_clear	install_vmrc	reboot	shell	software_upgrade

Test Install VMRC Bundle destination=remote_server fields
	CLI:Enter Path	/settings/auto_discovery/vm_managers/
	CLI:Write  install_vmrc

	CLI:Test Set Validate Normal Fields	destination	remote_server	local_system
	CLI:Set Field	destination	remote_server

	CLI:Test Set Field Invalid Options	url	${EMPTY}	Error: Incorrect URL information.	yes
	CLI:Test Set Field Invalid Options	url	${WORD}	Error: Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>	yes
	CLI:Test Set Field Invalid Options	url	${POINTS}	Error: Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>	yes

	CLI:Set Field	url	${FTP_VMRC10}

	CLI:Test Set Field Invalid Options	username	${EMPTY}	Error: Protocol requires username/password.	yes
	CLI:Test Set Field Invalid Options	username	${POINTS}
	CLI:Test Set Field Invalid Options	username	${WORD}

	CLI:Set Field	username	${FTP_USERNAME}

	CLI:Test Set Field Invalid Options	password	${EMPTY}	Error: File Transfer Failed	yes
	CLI:Test Set Field Invalid Options	password	${WORD}
	CLI:Test Set Field Invalid Options	password	${POINTS}

	CLI:Test Set Validate Normal Fields	download_path_is_absolute_path_name	yes	no
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test Install VMRC Bundle destination=local_system fields
	CLI:Enter Path	/settings/auto_discovery/vm_managers/
	CLI:Write  install_vmrc

	CLI:Test Set Validate Normal Fields	destination	remote_server	local_system
	CLI:Set Field	destination	local_system

	CLI:Test Set Field Invalid Options	filename	${EMPTY}	Error: filename: Incorrect filename information.	yes
	CLI:Test Set Field Invalid Options	username	${POINTS}	Error: filename: Incorrect filename information.	yes
	CLI:Test Set Field Invalid Options	username	${WORD}	Error: filename: Incorrect filename information.	yes

	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Remove VMRC Bundle If Exists

SUITE:Teardown
	SUITE:Remove VMRC Bundle If Exists
	CLI:Close Connection

SUITE:Remove VMRC Bundle If Exists
	CLI:Enter Path	/settings/auto_discovery/vm_managers/
	${VMRC_INSTALLED}=	Run Keyword and Return Status	CLI:Test Not Available Commands	install_vmrc
	Run Keyword If	${VMRC_INSTALLED}	CLI:Write	remove_vmrc


