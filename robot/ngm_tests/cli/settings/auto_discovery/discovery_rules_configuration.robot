*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Auto Discovery Discovery Rules... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE_NAME}	dummy_rule_configuration_device
${TWO_RULES}	two_rules
${NEW_RULE}	new_rule
${SECOND_RULE}	second_rule

*** Test Cases ***
Test adding 2 rules with same name
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/
	CLI:Add
	CLI:Set Field	rule_name	${TWO_RULES}
	CLI:Commit
	CLI:Add
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	rule_name	${TWO_RULES}	Error: rule_name: Entry already exists.	yes
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	rule_name	${TWO_RULES}	Error: Entry already exists.	yes
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	SUITE:Delete Rule	${TWO_RULES}

Test add a proper discovery rule
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/
	CLI:Add
	CLI:Write	set rule_name=${NEW_RULE} status=enabled action=clone_mode_enabled method=kvm_ports clone_from=${DEVICE_NAME}
	CLI:Commit
	[Teardown]	CLI:Cancel	Raw

Test editing rule
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/${NEW_RULE}
	CLI:Test Show Command	action = clone_mode_enabled	rule_name: ${NEW_RULE}	status = enabled
	...	host_identifier	method	clone_from
	CLI:Set Field	status	disabled
	CLI:Commit
	CLI:Test Show Command	action = clone_mode_enabled	rule_name: ${NEW_RULE}	status = disabled
	...	host_identifier	method	clone_from
	[Teardown]	CLI:Cancel	Raw

Test add a second discovery rule
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/
	CLI:Add
	CLI:Write	set rule_name=${SECOND_RULE} status=enabled action=clone_mode_enabled method=kvm_ports clone_from=${DEVICE_NAME}
	CLI:Commit
	[Teardown]	CLI:Cancel	Raw

Test valid Up action
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/
	${BEFORE}=	CLI:Show
	${ret}=	CLI:Write	up_rule ${SECOND_RULE}
	${AFTER}=	CLI:Show
	Should Not Be Equal	${BEFORE}	${AFTER}

*** Keyword ***
SUITE:Setup
	CLI:Open
	CLI:Delete Devices	${DEVICE_NAME}
	CLI:Add Device	${DEVICE_NAME}
	SUITE:Delete Rule	${TWO_RULES}	${NEW_RULE}	${SECOND_RULE}

SUITE:Teardown
	CLI:Delete Devices	${DEVICE_NAME}
	SUITE:Delete Rule	${TWO_RULES}	${NEW_RULE}	${SECOND_RULE}
	CLI:Close Connection

SUITE:Delete Rule
	[Arguments]	@{RULES_NAME}
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/
	CLI:Delete If Exists Confirm	@{RULES_NAME}

