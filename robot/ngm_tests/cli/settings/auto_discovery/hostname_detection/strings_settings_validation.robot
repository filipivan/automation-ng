*** Settings ***
Resource	../../../init.robot
Documentation	Tests Settings > Auto Discovery > Hostname Detection > String Settings
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Test Cases ***
Test available settings
	CLI:Enter Path	/settings/auto_discovery/hostname_detection
	CLI:Test Ls Command	string_settings	global_setting

Test available commands after send tab-tab
	CLI:Enter Path	/settings/auto_discovery/hostname_detection/string_settings
	CLI:Test Available Commands	cd	change_password	commit	event_system_audit	event_system_clear	exit	add	delete
	...	set	hostname	ls	pwd	quit	reboot	revert	ls	shell	show	show_settings	shutdown	whoami
	Run Keyword If	'${NGVERSION}' >= '5.4'	CLI:Test Available Commands	up	down

Test available fields to set command
	CLI:Enter Path	/settings/auto_discovery/hostname_detection/string_settings
	CLI:Add
	CLI:Test Set Available Fields	string	string_type
	CLI:Cancel

Test validation for field string_type
	CLI:Enter Path	/settings/auto_discovery/hostname_detection/string_settings
	CLI:Add
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	string_type	${EMPTY}	Error: Missing value for parameter: string_type
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	string_type	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: string_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	string_type	${EMPTY}	Error: Missing value: string_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	string_type	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}
	CLI:Cancel
