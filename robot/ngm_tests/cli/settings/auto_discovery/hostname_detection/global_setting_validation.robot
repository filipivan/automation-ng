*** Settings ***
Resource	../../../init.robot
Documentation	Tests Settings > Auto Discovery > Hostname Detection > Global Setting
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Variables ***
${ONE_NUMBER_ZERO}	0

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/auto_discovery/hostname_detection/global_setting
	CLI:Test Available Commands	cd	change_password	commit	event_system_audit	event_system_clear	exit	set
	...	hostname	ls	pwd	quit	reboot	revert	ls	shell	show	show_settings	shutdown	whoami

Test show command
	CLI:Enter Path	/settings/auto_discovery/hostname_detection/global_setting
	CLI:Test Show Command	probe_timeout	number_of_retries	update_device_name
	...	new_discovered_device_receives_the_name_during_conflict

Test available fields to set command
	CLI:Enter Path	/settings/auto_discovery/hostname_detection/global_setting
	CLI:Test Set Available Fields	new_discovered_device_receives_the_name_during_conflict	number_of_retries
	...	probe_timeout	update_device_name
	CLI:Revert

Test validation for field=new_discovered_device_receives_the_name_during_conflict
	CLI:Enter Path	/settings/auto_discovery/hostname_detection/global_setting
	${FIELD}=	Set Variable	new_discovered_device_receives_the_name_during_conflict
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD}	${EMPTY}	Error: Missing value for parameter: ${FIELD}
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD}	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: ${FIELD}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD}	${EMPTY}	Error: Missing value: ${FIELD}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD}	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	CLI:Test Field Options	new_discovered_device_receives_the_name_during_conflict	no	yes
	CLI:Revert

Test validation for field=number_of_retries
	CLI:Enter Path	/settings/auto_discovery/hostname_detection/global_setting
	CLI:Test Set Field Invalid Options	number_of_retries	${EMPTY}	Error: number_of_retries: Validation error.
	CLI:Test Set Field Invalid Options	number_of_retries	${ONE_WORD}	Error: number_of_retries: Validation error.
	CLI:Test Set Field Invalid Options	number_of_retries	${ONE_NUMBER_ZERO}	Error: number_of_retries: Validation error.
	CLI:Test Set Field Invalid Options	number_of_retries	${NUMBER}	Error: number_of_retries: Exceeded the maximum size for this field.

	CLI:Test Set Field Valid Options	number_of_retries	4
	CLI:Revert

Test validation for field=probe_timeout
	CLI:Enter Path	/settings/auto_discovery/hostname_detection/global_setting
	CLI:Test Set Field Invalid Options	probe_timeout	${EMPTY}	Error: probe_timeout: Validation error.
	CLI:Test Set Field Invalid Options	probe_timeout	${ONE_WORD}	Error: probe_timeout: Validation error.
	CLI:Test Set Field Invalid Options	probe_timeout	${ONE_NUMBER_ZERO}	Error: probe_timeout: Validation error.
	CLI:Test Set Field Invalid Options	probe_timeout	${NUMBER}	Error: probe_timeout: Exceeded the maximum size for this field.

	CLI:Test Set Field Valid Options	probe_timeout	4
	CLI:Revert

Test validation for field=update_device_name
	CLI:Enter Path	/settings/auto_discovery/hostname_detection/global_setting
	${FIELD}=	Set Variable	update_device_name
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD}	${EMPTY}	Error: Missing value for parameter: ${FIELD}
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD}	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: ${FIELD}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD}	${EMPTY}	Error: Missing value: ${FIELD}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD}	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	CLI:Test Field Options	update_device_name	no	yes
	CLI:Revert
