*** Settings ***
Resource	../../../init.robot
Documentation	Tests the feature of changing the order of matches and probes in hostname detection matchlist
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2

Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Test Cases ***
Test up and down commands on probes
	[Documentation]
		...	Executes up and down commands in two default probes (.1 and .2) considering they can only
		...	change positions between probes and cannot go below a match.
	CLI:Enter Path	/settings/auto_discovery/hostname_detection/string_settings/
	${INITIAL_MATCHLIST}	CLI:Ls
	${INITIAL_MATCHLIST}	Split To Lines	${INITIAL_MATCHLIST}
	CLI:Write				down probe.1
	CLI:Commit
	${DOWN_MATCHLIST}		CLI:Ls
	${DOWN_MATCHLIST}		Split To Lines	${DOWN_MATCHLIST}
	Should Be Equal			${INITIAL_MATCHLIST}[0]	${DOWN_MATCHLIST}[1]
	CLI:Write				up probe.1
	CLI:Commit
	${UP_MATCHLIST}			CLI:Ls
	${UP_MATCHLIST}			Split To Lines	${UP_MATCHLIST}
	Should Be Equal			${INITIAL_MATCHLIST}[0]	${UP_MATCHLIST}[0]

	CLI:Write				up probe.2
	CLI:Commit
	${UP_MATCHLIST}			CLI:Ls
	${UP_MATCHLIST}			Split To Lines	${UP_MATCHLIST}
	Should Be Equal			${INITIAL_MATCHLIST}[1]	${UP_MATCHLIST}[0]
	CLI:Write				down probe.2
	CLI:Commit
	${DOWN_MATCHLIST}			CLI:Ls
	${DOWN_MATCHLIST}			Split To Lines	${DOWN_MATCHLIST}
	Should Be Equal			${INITIAL_MATCHLIST}[1]	${DOWN_MATCHLIST}[1]
	[Teardown]	CLI:Revert	Raw

Test up and down commands on matches
	[Documentation]
		...	Executes up and down commands in six default matches (.1 to .6) considering they can only switch
		...	positions between matches and cannot go above a probe.
	CLI:Enter Path			/settings/auto_discovery/hostname_detection/string_settings/
	${INITIAL_MATCHLIST}	CLI:Ls
	${INITIAL_MATCHLIST}	Split To Lines	${INITIAL_MATCHLIST}
	FOR	${I}	IN RANGE	5
		${MATCH_INDEX}			Evaluate	${I}+1		#iteration starts in 0 and matches starts in 1
		CLI:Write				down match.${MATCH_INDEX}
		CLI:Commit
		${DOWN_MATCHLIST}		CLI:Ls
		${DOWN_MATCHLIST}		Split To Lines	${DOWN_MATCHLIST}
		${INDEX}				Evaluate	${I}+2		#considering that two probes will always be above
		${INDEX_AFTER_DOWN}		Evaluate	${INDEX}+1
		Should Be Equal			${INITIAL_MATCHLIST}[${INDEX}]	${DOWN_MATCHLIST}[${INDEX_AFTER_DOWN}]
		CLI:Write				up match.${MATCH_INDEX}
		CLI:Commit
		${UP_MATCHLIST}			CLI:Ls
		${UP_MATCHLIST}			Split To Lines	${UP_MATCHLIST}
		${INDEX_AFTER_UP}		Evaluate	${INDEX}+1
		Should Be Equal			${INITIAL_MATCHLIST}[0]	${UP_MATCHLIST}[0]
	END
	[Teardown]	CLI:Revert	Raw

Test scenarios where up command does not change matchlist
	CLI:Enter Path			/settings/auto_discovery/hostname_detection/string_settings/
	${INITIAL_MATCHLIST}	CLI:Ls
	CLI:Write	up probe.1
	CLI:Commit
	${FINAL_MATCHLIST}		CLI:Ls
	Should Be Equal			${INITIAL_MATCHLIST}	${FINAL_MATCHLIST}
	CLI:Write	up match.1
	CLI:Commit
	${FINAL_MATCHLIST}		CLI:Ls
	Should Be Equal			${INITIAL_MATCHLIST}	${FINAL_MATCHLIST}

Test scenarios where down command does not change matchlist
	CLI:Enter Path			/settings/auto_discovery/hostname_detection/string_settings/
	${INITIAL_MATCHLIST}	CLI:Ls
	CLI:Write	down probe.2
	CLI:Commit
	${FINAL_MATCHLIST}		CLI:Ls
	Should Be Equal			${INITIAL_MATCHLIST}	${FINAL_MATCHLIST}
	CLI:Write	down match.6
	CLI:Commit
	${FINAL_MATCHLIST}		CLI:Ls
	Should Be Equal			${INITIAL_MATCHLIST}	${FINAL_MATCHLIST}