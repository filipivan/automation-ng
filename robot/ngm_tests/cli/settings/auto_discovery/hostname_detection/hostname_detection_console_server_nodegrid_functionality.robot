*** Settings ***
Resource	../../../init.robot
Documentation	Tests Settings > Auto Discovery > Hostname Detection in Console Server NG
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CASNG_NAME}	${CONSOLE_SERVER_NG_NAME}
${CASNG_IP}	${CONSOLE_SERVER_NG_IP}
${CASNG_USERNAME}	${CONSOLE_SERVER_NG_USERNAME}
${CASNG_PASSWORD}	${CONSOLE_SERVER_NG_PASSWORD}

${DEVICE_NAME}	console_NG_hostname_test
${HOSTNAME_1}	DetectMe
${HOSTNAME_2}	DetectAgain

*** Test Cases ***
Test hostname detection using discover now
	Skip If	'${HOST}' == '${CASNG_IP}'	${CASNG_IP} is the console server
	CLI:Switch Connection	console_server
	CLI:Change Hostname	${HOSTNAME_1}
	SUITE:Add Console Sever To Be Detected
	Wait Until Keyword Succeeds	5x	10s	SUITE:Check If Hostname Was Detected	${HOSTNAME_1}
	[Teardown]	CLI:Delete All Devices

Test hostname detection editing console server already added
	Skip If	'${HOST}' == '${CASNG_IP}'	${CASNG_IP} is the console server
	SUITE:Add Console Sever To Be Detected
	Wait Until Keyword Succeeds	5x	10s	SUITE:Check If Hostname Was Detected	${HOSTNAME_1}
	CLI:Switch Connection	console_server
	CLI:Change Hostname	${HOSTNAME_2}
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/devices
	CLI:Edit	${HOSTNAME_1}
	CLI:Set	mode=enabled	#once you edit the device, it triggers hostname detection again!
	CLI:Commit
	Wait Until Keyword Succeeds	5x	10s	SUITE:Check If Hostname Was Detected	${HOSTNAME_2}

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Open	HOST_DIFF=${CASNG_IP}	session_alias=console_server
	Skip If	'${HOST}' == '${CASNG_IP}'	${CASNG_IP} is the console server

SUITE:Teardown
	CLI:Switch Connection	console_server
	CLI:Change Hostname	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	default
	CLI:Delete All Devices
	CLI:Close Connection

SUITE:Add Console Sever To Be Detected
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Set	name=${DEVICE_NAME} type=console_server_nodegrid ip_address=${CASNG_IP}
	CLI:Set	username=${CASNG_USERNAME} password=${CASNG_PASSWORD} mode=on-demand
	CLI:Set	enable_hostname_detection=yes
	CLI:Commit

SUITE:Check If Hostname Was Detected
	[Arguments]	${HOSTNAME}
	CLI:Switch Connection	default
	#If ${DEVICE_NAME} is not a valid option, it means hostname was discovered. So this error should be ignored
	Run Keyword And Ignore Error	CLI:Discover Now	${DEVICE_NAME}
	${DEVICES}	CLI:Show
	Should Contain	${DEVICES}	${HOSTNAME}