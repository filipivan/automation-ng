*** Settings ***
Resource	../../../init.robot
Documentation	Tests Settings > Auto Discovery > Hostname Detection > String Settings
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Test Cases ***
Add temp match
	CLI:Enter Path	/settings/auto_discovery/hostname_detection/string_settings
	${OUTPUT}=	CLI:Show
	@{Details}=	Split to Lines	${OUTPUT}
	FOR		${LINE}	IN	@{Details}
		${CONTAINS_DDD}=	Evaluate	"%Hddd" in """${LINE}"""
		${Lines}=	Run Keyword if	${CONTAINS_DDD}	Replace String Using Regexp	${LINE}	\\s\\s*	--
		@{Details}=	Run Keyword if	${CONTAINS_DDD}	Split String	${Lines}	--
		Run Keyword if	${CONTAINS_DDD}	CLI:Delete If Exists Confirm	${Details}[1]
	END
	CLI:Add
	CLI:Set Field	string	%Hddd
	CLI:Commit
	[Teardown]	CLI:Cancel	Raw

Check new match
	CLI:Enter Path	/settings/auto_discovery/hostname_detection/string_settings
	CLI:Test Show Command	%Hddd

Edit new match
	CLI:Enter Path	/settings/auto_discovery/hostname_detection/string_settings
	${OUTPUT}=	CLI:Show
	@{Details}=	Split to Lines	${OUTPUT}
	FOR		${LINE}	IN	@{Details}
		${CONTAINS_DDD}=	Evaluate	"%Hddd" in """${LINE}"""
		${Lines}=	Run Keyword if	${CONTAINS_DDD}	Replace String Using Regexp	${LINE}	\\s\\s*	--
		@{Details}=	Run Keyword if	${CONTAINS_DDD}	Split String	${Lines}	--
		Run Keyword if	${CONTAINS_DDD}	CLI:Enter Path	${Details}[1]
	END
	#Run Keyword If	'${NGVERSION}' >='4.0' CLI:Test Show Command   string type: Match	string = ddd
	Run Keyword If	'${NGVERSION}' >= '4.0' and '${NGVERSION}' < '5.0'  CLI:Test Show Command   string type: Match
	CLI:Set Field	string	%Heee
	CLI:Commit
	#Run Keyword If	'${NGVERSION}' >= '4.0' CLI:Test Show Command	string type: Match	string = eee
	Run Keyword If	'${NGVERSION}' >= '4.0' and '${NGVERSION}' < '5.0'  CLI:Test Show Command	string type: Match	string = %Heee
	Run Keyword If	'${NGVERSION}' < '4.0'  CLI:Test Show Command	string type: match	string = %Heee
	[Teardown]	CLI:Cancel	Raw

Remove temp device
	CLI:Enter Path	/settings/auto_discovery/hostname_detection/string_settings
	${OUTPUT}=	CLI:Show
	@{Details}=	Split to Lines	${OUTPUT}
	FOR		${LINE}	IN	@{Details}
		${CONTAINS_EEE}=	Evaluate	"%Heee" in """${LINE}"""
		${Lines}=	Run Keyword if	${CONTAINS_EEE}	Replace String Using Regexp	${LINE}	\\s\\s*	--
		@{Details}=	Run Keyword if	${CONTAINS_EEE}	Split String	${Lines}	--
		Run Keyword if	${CONTAINS_EEE}	CLI:Delete If Exists Confirm	${Details}[1]
	END