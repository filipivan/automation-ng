*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Auto Discovery Discovery Rules validation.. through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE_NAME}	dummy_rule_validation_device
${RULE_NAME}	rule_validation
${RULE_NOEXISTS}	rule_notexists_validation
${RULE_NEW}	new_rule_validation

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/
	CLI:Test Available Commands	event_system_clear	set	exit	shell	add	hostname	show	cd	ls	show_settings
	...	change_password	pwd	shutdown	commit	quit	whoami	delete	reboot	event_system_audit	revert	up_rule	down_rule

Test visible fields for show command
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Show Command	rule_name	order	method	host identifier	lookup pattern	clone from	action	status
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Show Command	rule name	order	method	host identifier	lookup pattern	clone from	action	status

Test available fields to set command
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/
	CLI:Add
	CLI:Test Set Available Fields	action	clone_from	host_identifier	mac_address	method	rule_name	status

	CLI:Test Set Field Options	action	clone_mode_discovered	clone_mode_enabled	clone_mode_on-demand	discard_device
	CLI:Test Set Field Options	method	console_server_ports	dhcp	kvm_ports	network_scan	vm_manager	vm_serial
	CLI:Test Set Field Options	status	disabled	enabled

	CLI:Commands After Set Error	action	\\_\\)\\(\\*&¨%	cancel	commit	ls	save	set	show
	CLI:Commands After Set Error	method	\\_\\)\\(\\*&¨%	cancel	commit	ls	save	set	show
	CLI:Commands After Set Error	status	\\_\\)\\(\\*&¨%	cancel	commit	ls	save	set	show
	[Teardown]	CLI:Cancel	Raw

Test invalid values for fields for discovery_rules
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/
	CLI:Add
	${COMMANDS}=	Create List	action	clone_from	method	status
	CLI:Set Field	rule_name	${RULE_NAME}
	SUITE:Set Invalid Values Error Messages	${COMMANDS}
	[Teardown]	CLI:Cancel	Raw

Test validation for field=rule_name
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/
	CLI:Add
	CLI:Test Set Field Invalid Options	rule_name	${EMPTY}	Error: rule_name: Field must not be empty.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	rule_name	${POINTS}	Error: rule_name: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	rule_name	${EXCEEDED}	Error: rule_name: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	rule_name	${POINTS}	Error: rule name: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	rule_name	${EXCEEDED}	Error: rule name: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

Test validation for method=dhcp
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/
	CLI:Add
	CLI:Set Field	rule_name	${RULE_NAME}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	mac_address	${POINTS}	Error: mac_address: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	mac_address	${ONE_NUMBER}	Error: mac_address: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	mac_address	${EXCEEDED}	Error: mac_address: Validation error.	yes

	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	mac_address	${POINTS}	Error: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	mac_address	${ONE_NUMBER}	Error: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	mac_address	${EXCEEDED}	Error: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

Test validation for method=console_server_ports
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/
	CLI:Add
	CLI:Write	set rule_name=${RULE_NAME} method=console_server_ports
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	port_list	${POINTS}	Error: port_list: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	port_list	${EXCEEDED}	Error: port_list: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	port_list	${POINTS}	Error: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	port_list	${EXCEEDED}	Error: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

Test validation for method=kvm_ports
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/
	CLI:Add
	CLI:Write	set rule_name=${RULE_NAME} method=kvm_ports
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	port_list	${POINTS}	Error: port_list: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	port_list	${EXCEEDED}	Error: port_list: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	port_list	${POINTS}	Error: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	port_list	${EXCEEDED}	Error: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

Test validation for method=vm_manager
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/
	CLI:Add
	CLI:Write	set rule_name=${RULE_NAME} method=vm_manager
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	datacenter	${EXCEEDED}	Error: datacenter: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	datacenter	${EXCEEDED}	Error: Validation error.	yes
	CLI:Set Field	datacenter	${EMPTY}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	cluster	${EXCEEDED}	Error: cluster: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	cluster	${EXCEEDED}	Error: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

Test validation for method=vm_serial
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/
	CLI:Add
	CLI:Write	set rule_name=${RULE_NAME} method=vm_serial
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	port_uri	${EXCEEDED}	Error: port_uri: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	port_uri	${EXCEEDED}	Error: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

Test Up and Down actions with no arguments
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/
	${UP_OUTPUT}=	CLI:Write	up_rule	Raw
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${UP_OUTPUT}	Error: Not enough arguments
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${UP_OUTPUT}	Error: Missing arguments

	${DOWN_OUTPUT}=	CLI:Write	down_rule	Raw
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${DOWN_OUTPUT}	Error: Not enough arguments
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${DOWN_OUTPUT}	Error: Missing arguments

Test Up and Down actions with invalid argument
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/
	${UP_OUTPUT}=	CLI:Write	up_rule ${RULE_NOEXISTS}	Raw
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${UP_OUTPUT}	Error: Invalid Target name: ${RULE_NOEXISTS}. Please, use tab-tab to obtain available targets.
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${UP_OUTPUT}	Error: Invalid Target: ${RULE_NOEXISTS}. Press <tab> for a list of valid targets.

	${DOWN_OUTPUT}=	CLI:Write	down_rule ${RULE_NOEXISTS}	Raw
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${DOWN_OUTPUT}	Error: Invalid Target name: ${RULE_NOEXISTS}. Please, use tab-tab to obtain available targets.
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${DOWN_OUTPUT}	Error: Invalid Target: ${RULE_NOEXISTS}. Press <tab> for a list of valid targets.

Test Up and Down actions error
	CLI:Delete Devices	${DEVICE_NAME}
	CLI:Add Device	${DEVICE_NAME}
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/
	CLI:Add
	CLI:Write	set rule_name=${RULE_NEW} status=enabled action=clone_mode_enabled method=kvm_ports clone_from=${DEVICE_NAME}
	CLI:Commit
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/
	${ret}=	CLI:Write	up_rule ${RULE_NEW}	RAW
	Should Contain	${ret}	Error: Trying to move a rule outside the valid rule boundaries.

	${ret}=	CLI:Write	down_rule ${RULE_NEW}	RAW
	Should Contain	${ret}	Error: Trying to move a rule outside the valid rule boundaries.

*** Keyword ***
SUITE:Setup
	CLI:Open
	CLI:Delete Devices	${DEVICE_NAME}
	CLI:Add Device	${DEVICE_NAME}
	SUITE:Delete Rule	${RULE_NAME}	${RULE_NOEXISTS}	${RULE_NEW}

SUITE:Teardown
	CLI:Delete Devices	${DEVICE_NAME}
	SUITE:Delete Rule	${RULE_NAME}	${RULE_NOEXISTS}	${RULE_NEW}
	CLI:Close Connection

SUITE:Delete Rule
	[Arguments]	@{RULES_NAME}
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/
	CLI:Delete If Exists Confirm	@{RULES_NAME}

SUITE:Set Invalid Values Error Messages
	[Arguments]	${COMMANDS}
	FOR	${COMMAND}	IN	@{COMMANDS}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value: ${COMMAND}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}
	END