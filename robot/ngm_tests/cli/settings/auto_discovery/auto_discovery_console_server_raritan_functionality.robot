*** Settings ***
Resource	    ../../init.robot
Documentation	Test auto discovery Raritan Console Server
Metadata	    Version	1.0
Metadata	    Executed At	${HOST}
Force Tags	    CLI     EXCLUDEIN3_2
Default Tags	SSH

Suite Setup	    SUITE:Setup
Suite Teardown  SUITE:Teardown

*** Variables ***
${RARITAN_NAME}             test_console_server_raritan
${RARITAN_MODE}	            on-demand
${RARITAN_RULE_NAME}	    test_rule_console_server_raritan
${RARITAN_RULE_METHOD}	    console_server_ports
${RARITAN_RULE_CLONE_FROM}  test_console_server_raritan
${RARITAN_RULE_STATUS}      enabled
${RARITAN_RULE_ACTION}	    clone_mode_on-demand
${RARITAN_PROMPT}           admin >
${RARITAN_PROMPT_TOP}       admin > Maintenance > top

*** Test Cases ***
Test Discover Raritan serial port
	[Tags]	NON-CRITICAL
	Skip If	not ${RARITAN_SERVER_PRESENT}	Raritan not available in automation environment
	CLI:Discover Now	${RARITAN_NAME}     ${RARITAN_TYPE}
	SUITE:Wait For Discovery Logs To Match Regexp	Device Cloned	${RARITAN_NAME}

Test detect Raritan hostname
	[Tags]	NON-CRITICAL
	Skip If	not ${RARITAN_SERVER_PRESENT}	Raritan not available in automation environment
	SUITE:Enable Hostname Detection For Device	${RARITAN_NAME}
	CLI:Discover Now	${RARITAN_NAME}     ${RARITAN_TYPE}
	SUITE:Wait For Discovery Logs To Match Regexp
	...	${RARITAN_SERVER_IP}.*Hostname.*Device Updated	${RARITAN_NAME}

*** Keywords ***
SUITE:Setup
	Skip If	not ${RARITAN_SERVER_PRESENT}	Raritan not available in automation environment
	CLI:Open
	${TIME_SECONDS}=	CLI:Get Current System Time In Seconds
	Set Suite Variable	${TIME_SECONDS}

	@{DEVICES_LIST}=	CLI:Get Devices List	all	${PROTECTED_DEVICE_TYPES}
	CLI:Delete Rules	${RARITAN_RULE_NAME}
	CLI:Delete Devices	@{DEVICES_LIST}
	CLI:Add Device      ${RARITAN_NAME}     ${RARITAN_TYPE}     ${RARITAN_SERVER_IP}        ${RARITAN_SERVER_USERNAME}
	...                 ${RARITAN_SERVER_PASSWORD}      ${RARITAN_MODE}
	CLI:Add Rule        ${RARITAN_RULE_NAME}        ${RARITAN_RULE_METHOD}      ${RARITAN_RULE_CLONE_FROM}
	...                 ${RARITAN_RULE_STATUS}      ${RARITAN_RULE_ACTION}

SUITE:Teardown
	Skip If	not ${RARITAN_SERVER_PRESENT}	Raritan not available in automation environment
	CLI:Reset Discovery Logs
	@{DEVICES_LIST}=	CLI:Get Devices List	all	${PROTECTED_DEVICE_TYPES}
	CLI:Delete Rules	${RARITAN_RULE_NAME}
	CLI:Delete Devices	@{DEVICES_LIST}
	CLI:Close Connection

SUITE:Enable Hostname Detection For Device
	[Arguments]	${DEVICE_NAME}
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/access
	CLI:Set	enable_hostname_detection=yes
	CLI:Commit

SUITE:Wait For Discovery Logs To Match Regexp
	[Arguments]	${REGEXP}	${DEVICE}
	Wait Until Keyword Succeeds	2x	1s
	...	SUITE:Discover Device And Wait For Discovery Logs To Match Regexp
	...	${REGEXP}	${DEVICE}

SUITE:Discover Device And Wait For Discovery Logs To Match Regexp
	[Arguments]	${REGEXP}	${DEVICE}
	CLI:Discover Now	${DEVICE}
	Wait Until Keyword Succeeds	45s	3s
	...	CLI:Discovery Logs Should Match Regexp And Time	${REGEXP}	${TIME_SECONDS}