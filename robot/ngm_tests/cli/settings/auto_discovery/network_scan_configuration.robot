*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Auto Discovery Network Scan... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Test Setup	SUITE:Add a device and network_scan
Test Teardown	SUITE:Delete device and network_scan

*** Variables ***
${SCAN_ID}	dummy_network_scan
${IP_RANGE_START}	127.0.0.10
${IP_RANGE_END}	127.0.0.15
${DEVICE}	dummy_device_network

*** Test Cases ***
Test adding valid network_scan
	${RETURN}=	CLI:Enter Path	/settings/auto_discovery/network_scan
	CLI:Test Show Command Regexp	${SCAN_ID}\\s+${IP_RANGE_START}/${IP_RANGE_END}\\s+enabled\\s+${DEVICE}

Test editing valid network_scan
	CLI:Enter Path	/settings/auto_discovery/network_scan
	CLI:Test Ls Command	${SCAN_ID}
	CLI:Enter Path	${SCAN_ID}
	CLI:Test Show Command Regexp	scan( |_)id: ${SCAN_ID}	ip_range_start = ${IP_RANGE_START}	ip_range_end = ${IP_RANGE_END}	enable_scanning = yes
	...	similar_devices = yes	device = ${DEVICE}	port_scan = yes	port_list = 22-23,623	ping = yes	scan_interval = 60
	CLI:Set	ping=no scan_interval=80
	CLI:Commit
	CLI:Test Show Command Regexp	scan( |_)id: ${SCAN_ID}	ip_range_start = ${IP_RANGE_START}	ip_range_end = ${IP_RANGE_END}	enable_scanning = yes
	...	similar_devices = yes	device = ${DEVICE}	port_scan = yes	port_list = 22-23,623	ping = no	scan_interval = 80

*** Keyword ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection

SUITE:Add a device and network_scan
	CLI:Delete Devices	${DEVICE}
	CLI:Add Device	${DEVICE}	device_console	127.0.0.1
	CLI:Enter Path	/settings/auto_discovery/discovery_logs/
	CLI:Write	reset_logs
	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Delete If Exists Confirm	${SCAN_ID}
	CLI:Add
	CLI:Set	scan_id=${SCAN_ID} ip_range_start=${IP_RANGE_START} ip_range_end=${IP_RANGE_END} device=${DEVICE}
	CLI:Commit

SUITE:Delete device and network_scan
	CLI:Enter Path	/settings/auto_discovery/network_scan
	CLI:Delete If Exists Confirm	${SCAN_ID}
	CLI:Delete Devices	${DEVICE}
	CLI:Enter Path	/settings/auto_discovery/discovery_logs/
	CLI:Write	reset_logs