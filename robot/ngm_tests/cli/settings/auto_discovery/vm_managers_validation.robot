*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Auto Discovery VM Managers... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/auto_discovery/vm_managers/
	CLI:Test Available Commands	event_system_clear	set	exit	shell	add	hostname	show	cd	ls	show_settings
	...	change_password	pwd	shutdown	commit	quit	whoami	delete	reboot	event_system_audit	revert

Test visible fields for show commands
	CLI:Enter Path	/settings/auto_discovery/vm_managers/
	CLI:Test Show Command	vm server	type	discover virtual machines	interval in minutes

Test set available fields after add operation
	CLI:Enter Path	/settings/auto_discovery/vm_managers/
	CLI:Add
	CLI:Test Set Available Fields	html_console_port	password	type	username	vm_server
	[Teardown]	CLI:Cancel	Raw

Test available commands after invalid set operation
	CLI:Enter Path	/settings/auto_discovery/vm_managers/
	CLI:Add
	CLI:Commands After Set Error	type	\\_\\)\\(\\*&¨%	cancel	commit	ls	save	set	show
	[Teardown]	CLI:Cancel	Raw

Test if commit is raising error after add empty record
	CLI:Enter Path	/settings/auto_discovery/vm_managers/
	CLI:Add
	${OUTPUT}=	CLI:Commit	Raw
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	Error: vm_server: VM Server must be not empty
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${OUTPUT}	Error: Field must not be empty.
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=type
	CLI:Enter Path	/settings/auto_discovery/vm_managers/
	CLI:Add
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	type	${EMPTY}	Error: Missing value for parameter: type
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	type	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	type	${EMPTY}	Error: Missing value: type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	type	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=vm_server
	[Tags]	EXCLUDEIN3_2	# Validation not implemented on 3.2
	CLI:Enter Path	/settings/auto_discovery/vm_managers/
	CLI:Add
	CLI:Test Set Field Invalid Options	vm_server	${POINTS}	Error: vm_server: Validation error.	yes
	CLI:Test Set Field Invalid Options	vm_server	${EMPTY}	Error: vm_server: VM Server must be not empty	yes
	CLI:Test Set Field Invalid Options	vm_server	${EXCEEDED}	Error: vm_server: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=username
	[Tags]	EXCLUDEIN3_2	# Validation not implemented on 3.2
	CLI:Enter Path	/settings/auto_discovery/vm_managers/
	CLI:Delete If Exists Confirm	${VMMANAGER["ip"]}
	CLI:Add
	CLI:Set Field	vm_server	${VMMANAGER["ip"]}
	CLI:Test Set Field Invalid Options	username	${POINTS}	Error: username: This field should contain only numbers, letters, and the characters @ . \\ /.	yes
	CLI:Test Set Field Invalid Options	username	${EXCEEDED}	Error: username: Exceeded the maximum size for this field.	yes
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=html_console_port
	[Tags]	EXCLUDEIN3_2	# Validation not implemented on 3.2
	CLI:Enter Path	/settings/auto_discovery/vm_managers/
	CLI:Add
	CLI:Set Field	vm_server	${VMMANAGER["ip"]}
	CLI:Test Set Field Invalid Options	html_console_port	${POINTS}	Error: html_console_port: Validation error.	yes
	CLI:Test Set Field Invalid Options	html_console_port	${EXCEEDED}	Error: html_console_port: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Delete VM Manager And Hosts

SUITE:Teardown
	SUITE:Delete VM Manager And Hosts
	CLI:Close Connection

SUITE:Delete VM Manager And Hosts
	CLI:Enter Path	/settings/auto_discovery/vm_managers
	CLI:Delete If Exists Confirm	${VMMANAGER["ip"]}	@{VMMANAGER["hosts"]}