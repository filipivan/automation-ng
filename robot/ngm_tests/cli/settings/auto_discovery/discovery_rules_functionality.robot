*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Auto Discovery - Discovery Rules... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	NON-CRITICAL	NEED-REVIEW
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${FIFTY_DEVICES_ACCESS_LICENSE}	${FIFTY_DEVICES_ACCESS_LICENSE}
${IP_RANGE_START}	${ACS6000_IP_RANGE_START}
${IP_RANGE_END}	${ACS6000_IP_RANGE_END}
${ACS6000_IP}	${ACS6000_IP}
${ACS6000_USERNAME}	${ACS6000_USERNAME}
${ACS6000_PASSWORD}	${ACS6000_PASSWORD}
${SCAN_ID_APPLIANCE}	acs6000_network_scan_appliance
${SCAN_ID_PORTS}	acs6000_network_scan_ports
${ACS6000_APPLIANCE_DEVICE_NAME}	acs_appliance_device_test
${ACS6000_PORTS_DEVICE_NAME}	acs_ports_device_test
${ACS6000_APPLIANCE_RULE}	acs_appliance_rule
${ACS6000_PORTS_RULE}	acs_ports_rule
${ACS6000_PORTS}	10-bf-00-p-

*** Test Cases ***
Test appliance discovery rules with ACS
	CLI:Add License Key	${FIFTY_DEVICES_ACCESS_LICENSE}
	CLI:Add Device	${ACS6000_APPLIANCE_DEVICE_NAME}	console_server_acs6000	127.0.0.1	${ACS6000_USERNAME}	${ACS6000_PASSWORD}
	CLI:Add Network Scan	${SCAN_ID_APPLIANCE}	${IP_RANGE_START}	${IP_RANGE_END}	yes	${ACS6000_APPLIANCE_DEVICE_NAME}	no	no
	CLI:Add Rule	${ACS6000_APPLIANCE_RULE}	network_scan	${ACS6000_APPLIANCE_DEVICE_NAME}
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/${ACS6000_APPLIANCE_RULE}
	CLI:Set	scan_id=${SCAN_ID_APPLIANCE}
	CLI:Commit
	CLI:Enter Path	/settings/auto_discovery/discovery_logs/
	CLI:Write	reset_logs
	CLI:Enter Path	/settings/auto_discovery/discover_now/
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Write	discover_now net|${SCAN_ID_APPLIANCE},cas|${ACS6000_APPLIANCE_DEVICE_NAME}
	Run Keyword If	'${NGVERSION}' >='4.0'	CLI:Write	discover_now ${SCAN_ID_APPLIANCE},${ACS6000_APPLIANCE_DEVICE_NAME}
	Wait Until Keyword Succeeds	1m	1s	SUITE:Discovery Logs Appliance	${ACS6000_IP}	${ACS6000_IP}
	CLI:Enter Path	/settings/devices/
	CLI:Test Show Command Regexp	${ACS6000_IP}\\s+${ACS6000_IP}\\s+console_server_acs6000\\s+enabled
	[Teardown]	SUITE:Test Cases Teardown

Test console server ports discovery rules with ACS
	[Setup]	SUITE:Test Cases Setup
	CLI:Add License Key	${FIFTY_DEVICES_ACCESS_LICENSE}
	CLI:Add Device	${ACS6000_APPLIANCE_DEVICE_NAME}	console_server_acs6000	127.0.0.1	${ACS6000_USERNAME}	${ACS6000_PASSWORD}
	CLI:Add Device	${ACS6000_PORTS_DEVICE_NAME}	console_server_acs6000	127.0.0.1	${ACS6000_USERNAME}	${ACS6000_PASSWORD}
	CLI:Enter Path	/settings/devices/${ACS6000_PORTS_DEVICE_NAME}/access/
	CLI:Set	end_point=serial_port port_number=1
	CLI:Commit
	CLI:Add Network Scan	${SCAN_ID_PORTS}	${IP_RANGE_START}	${IP_RANGE_END}	yes	${ACS6000_PORTS_DEVICE_NAME}	no	no
	CLI:Add Network Scan	${SCAN_ID_APPLIANCE}	${IP_RANGE_START}	${IP_RANGE_END}	yes	${ACS6000_APPLIANCE_DEVICE_NAME}	no	no
	CLI:Add Rule	${ACS6000_APPLIANCE_RULE}	network_scan	${ACS6000_APPLIANCE_DEVICE_NAME}
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/${ACS6000_APPLIANCE_RULE}
	CLI:Set	scan_id=${SCAN_ID_APPLIANCE}
	CLI:Commit
	CLI:Add Rule	${ACS6000_PORTS_RULE}	console_server_ports	${ACS6000_PORTS_DEVICE_NAME}
	CLI:Enter Path	/settings/auto_discovery/discovery_logs/
	CLI:Write	reset_logs
	CLI:Enter Path	/settings/auto_discovery/discover_now/
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Write	discover_now net|${SCAN_ID_PORTS},net|${SCAN_ID_APPLIANCE},cas|${ACS6000_APPLIANCE_DEVICE_NAME}
	Run Keyword If	'${NGVERSION}' >='4.0'	CLI:Write	discover_now ${SCAN_ID_PORTS},${SCAN_ID_APPLIANCE},${ACS6000_APPLIANCE_DEVICE_NAME}
	Wait Until Keyword Succeedss	1m	1s	SUITE:Discovery Logs Console Server Ports	${ACS6000_IP}
	CLI:Enter Path	/settings/devices/
	CLI:Test Show Command Regexp	${ACS6000_IP}\\s+${ACS6000_IP}\\s+console_server_acs6000\\s+enabled
	[Teardown]	CLI:Cancel	Raw

*** Keyword ***
SUITE:Setup
	CLI:Open
	CLI:Delete All Devices
	CLI:Delete All License Keys Installed
	CLI:Delete Rules	${ACS6000_PORTS_RULE}	${ACS6000_APPLIANCE_RULE}
	CLI:Delete Network Scan	${SCAN_ID_APPLIANCE}	${SCAN_ID_PORTS}

SUITE:Teardown
	CLI:Delete All Devices
	CLI:Delete All License Keys Installed
	CLI:Delete Rules	${ACS6000_PORTS_RULE}	${ACS6000_APPLIANCE_RULE}
	CLI:Delete Network Scan	${SCAN_ID_APPLIANCE}	${SCAN_ID_PORTS}
	CLI:Close Connection

SUITE:Test Cases Setup
	CLI:Delete All Devices
	CLI:Delete Rules	${ACS6000_APPLIANCE_RULE}	${ACS6000_PORTS_RULE}
	CLI:Delete All License Keys Installed
	CLI:Delete Network Scan	${SCAN_ID_APPLIANCE}	${SCAN_ID_PORTS}

SUITE:Test Cases Teardown
	CLI:Cancel	Raw
	CLI:Delete All Devices
	CLI:Delete Rules	${ACS6000_APPLIANCE_RULE}	${ACS6000_PORTS_RULE}
	CLI:Delete All License Keys Installed
	CLI:Delete Network Scan	${SCAN_ID_APPLIANCE}	${SCAN_ID_PORTS}

SUITE:Discovery Logs Appliance
	[Arguments]	${IP_ADDRESS}	${DEVICE_NAME}
	CLI:Enter Path	/settings/auto_discovery/discovery_logs/
	${OUTPUT}=	CLI:Show
	CLI:Test Show Command Regexp  ((.|\\n)*)192.168.2.151\\s+192.168.2.151\\s+Network Scan\\s+Device Cloned((.|\\n)*)

SUITE:Discovery Logs Console Server Ports
	[Arguments]	${IP_ADDRESS}
	CLI:Enter Path	/settings/auto_discovery/discovery_logs/
	${OUTPUT}=	CLI:Show
	FOR		${INDEX}	IN RANGE	1	6
		CLI:Test Show Command Regexp  ((.|\\n)*)${IP_ADDRESS}\\s+${ACS6000_PORTS}${INDEX}\\s+Console Server Ports\\s+Device Cloned((.|\\n)*)
	END