*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Auto Discovery VM Managers... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	BUG_NG_7155
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${TEMPLATE_DEVICE}	test.template-dev
${VM_MANAGER_RULE}	test-vm-manager-rule
${ACCESS_LICENSE_KEY}	${FIVE_THOUSAND_DEVICES_ACCESS_LICENSE}

*** Test Cases ***
Test Hosts are discovered
	SUITE:Wait Until VM Manager Hosts Are Discovered

Test Virtual Machines are discovered
	SUITE:Wait Until VM Manager Virtual Machines Are Discovered

*** Keywords ***
SUITE:Setup
	CLI:Open
	${TIME_SECONDS}=	CLI:Get Current System Time In Seconds
	Set Suite Variable	${TIME_SECONDS}

	CLI:Delete All Devices
	SUITE:Delete VM Manager And Hosts
	CLI:Delete Rules	${VM_MANAGER_RULE}
	CLI:Delete All License Keys Installed
	CLI:Add License Key	${ACCESS_LICENSE_KEY}
	SUITE:Add VM Manager
	SUITE:Add Template Device For VM Manager Discovery
	SUITE:Add VM Manager Discovery Rule
	CLI:Discover Now	${VMMANAGER["ip"]}	vm

SUITE:Teardown
	CLI:Reset Discovery Logs
	CLI:Delete Rules	${VM_MANAGER_RULE}
	CLI:Delete All Devices
	SUITE:Delete VM Manager And Hosts
	CLI:Delete All License Keys Installed
	CLI:Close Connection

SUITE:Add Template Device For VM Manager Discovery
	CLI:Add Device	${TEMPLATE_DEVICE}	virtual_console_vmware	127.0.0.1	MODE=disabled
	...	ADDITIONAL_FIELDS=vm_manager=${VMMANAGER["ip"]}

SUITE:Add VM Manager
	CLI:Enter Path	/settings/auto_discovery/vm_managers/
	CLI:Add
	CLI:Set	vm_server=${VMMANAGER["ip"]} username=${VMMANAGER["username"]} password=${VMMANAGER["password"]}
	CLI:Set	type=VMware
	CLI:Commit
	CLI:Show

	CLI:Enter Path	/settings/auto_discovery/vm_managers/${VMMANAGER["ip"]}/
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Set  discover_virtual_machines=yes
	Run Keyword If	${NGVERSION} >= 4.2	Wait Until Keyword Succeeds	60s	3s	CLI:Test Set Field Options Raw	discovery_scope	${VMMANAGER["datacenter"]}:
	...	ELSE	Sleep	45s	# There is no validation

	CLI:Set	discover_virtual_machines=yes
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Set	discovery_scope=${VMMANAGER["datacenter"]}:
	...	ELSE	CLI:Set	discovery_scope=${VMMANAGER["datacenter"]}!;
	CLI:Commit
	CLI:Show

SUITE:Add VM Manager Discovery Rule
	CLI:Add Rule	${VM_MANAGER_RULE}	vm_manager	${TEMPLATE_DEVICE}	enabled	clone_mode_on-demand

SUITE:Wait Until VM Manager Hosts Are Discovered
	CLI:Enter Path	/settings/auto_discovery/vm_managers/
	Wait Until Keyword Succeeds	90s	5s	SUITE:VM Manager Hosts Should Be Discovered

SUITE:VM Manager Hosts Should Be Discovered
	CLI:Test Show Command	@{VMMANAGER["hosts"]}

SUITE:Wait Until VM Manager Virtual Machines Are Discovered
	CLI:Enter Path	/settings/auto_discovery/discovery_logs/
	Wait Until Keyword Succeeds	150s	5s	SUITE:VM Manager Virtual Machines Should Be Discovered

SUITE:VM Manager Virtual Machines Should Be Discovered
	${OUTPUT}=	CLI:Show
	FOR		${HOST}	IN	@{VMMANAGER["hosts"]}
		CLI:Discovery Logs Should Match Regexp And Time
		...	${HOST}.*(VM|vm) (M|m)anager.*(D|d)evice (C|c)loned	${TIME_SECONDS}
	END

SUITE:Delete VM Manager And Hosts
	CLI:Enter Path	/settings/auto_discovery/vm_managers/
	CLI:Delete All Confirm