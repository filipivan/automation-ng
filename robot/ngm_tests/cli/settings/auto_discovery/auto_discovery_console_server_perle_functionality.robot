*** Settings ***
Resource	../../init.robot
Documentation	Test auto discovery Perle Console Server SCS and SCR
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	NON-CRITICAL
Default Tags	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown


*** Variables ***
${PERLE_SCS_NAME}	perle-scs-test
${RULE_SCS_NAME}	perle-scs-rule-test
${PERLE_SCS_MODE}	on-demand
${PERLE_SCR_NAME}	perle-scr-test
${RULE_SCR_NAME}	perle-scr-rule-test
${PERLE_SCR_MODE}	on-demand
${RULE_MODE}	console_server_ports
${RULE_ACTION}	clone_mode_on-demand
${ACCESS_LICENSE_KEY}	${FIVE_THOUSAND_DEVICES_ACCESS_LICENSE}

*** Test Cases ***
Test discover Perle SCS serial port
	Skip If	not ${PERLE_SCS_PRESENT}	Perle SCS not available in automation environment
	SUITE:Wait For Discovery Logs To Match Regexp
	...	${PERLE_SCS_SERVER_IP}.*Console Server Ports.*Device Cloned
	...	${PERLE_SCS_NAME}

Test detect Perle SCS hostname
	Skip If	not ${PERLE_SCS_PRESENT}	Perle SCS not available in automation environment
	SUITE:Enable Hostname Detection For Device	${PERLE_SCS_NAME}
	SUITE:Wait For Discovery Logs To Match Regexp	${PERLE_SCS_SERVER_IP}.*Hostname.*Device Updated
	...	${PERLE_SCS_NAME}

Test discover Perle SCR serial ports
	Skip If	not ${PERLE_SCR_PRESENT}	Perle SCR not available in automation environment
	SUITE:Wait For Discovery Logs To Match Regexp
	...	${PERLE_SCR_SERVER_IP}.*Console Server Ports.*Device Cloned
	...	${PERLE_SCR_NAME}

Test detect Perle SCR hostname
	Skip If	not ${PERLE_SCR_PRESENT}	Perle SCR not available in automation environment
	SUITE:Enable Hostname Detection For Device	${PERLE_SCR_NAME}
	SUITE:Wait For Discovery Logs To Match Regexp	${PERLE_SCR_SERVER_IP}.*Hostname.*Device Updated
	...	${PERLE_SCR_NAME}

*** Keywords ***
SUITE:Setup
	# Workaround for not having the port enabled (necessary) when runnnig the tests
	# Not a problem for SCR as it is not used
	SUITE:Enable Perle SCS Serial Port

	CLI:Open
	${TIME_SECONDS}=	CLI:Get Current System Time In Seconds
	Set Suite Variable	${TIME_SECONDS}

	CLI:Delete All License Keys Installed
	CLI:Add License Key	${ACCESS_LICENSE_KEY}

	CLI:Delete Rules	${RULE_SCS_NAME}	${RULE_SCR_NAME}
	CLI:Delete All Devices

	Run Keyword If	${PERLE_SCS_PRESENT}
	...	CLI:Add Device  ${PERLE_SCS_NAME}	${PERLE_SCS_TYPE}	${PERLE_SCS_SERVER_IP}
	...	${PERLE_SCS_SERVER_USERNAME}	${PERLE_SCS_SERVER_PASSWORD}	${PERLE_SCS_MODE}
	Run Keyword If	${PERLE_SCR_PRESENT}
	...	CLI:Add Device  ${PERLE_SCR_NAME}	${PERLE_SCR_TYPE}	${PERLE_SCR_SERVER_IP}
	...	${PERLE_SCR_SERVER_USERNAME}	${PERLE_SCR_SERVER_PASSWORD}	${PERLE_SCR_MODE}

	${PERLE_SCS_PORT}=	Get From List	${PERLE_SCS_AVAILABLE_PORTS}	0
	${PERLE_SCR_PORT}=	Get From List	${PERLE_SCR_AVAILABLE_PORTS}	0
	Run Keyword If	${PERLE_SCS_PRESENT}
	...	CLI:Add Rule	${RULE_SCS_NAME}	${RULE_MODE}	${PERLE_SCS_NAME}	enabled	${RULE_ACTION}
	...	port_list=${PERLE_SCS_PORT}
	Run Keyword If	${PERLE_SCR_PRESENT}
	...	CLI:Add Rule	${RULE_SCR_NAME}	${RULE_MODE}	${PERLE_SCR_NAME}	enabled	${RULE_ACTION}
	...	port_list=${PERLE_SCR_PORT}

SUITE:Teardown
	CLI:Reset Discovery Logs
	CLI:Delete All License Keys Installed
	CLI:Delete Rules	${RULE_SCS_NAME}	${RULE_SCR_NAME}
	CLI:Delete All Devices
	CLI:Close Connection

SUITE:Wait For Discovery Logs To Match Regexp
	[Arguments]	${REGEXP}	${DEVICE}
	Wait Until Keyword Succeeds	3x	1s
	...	SUITE:Discover Device And Wait For Discovery Logs To Match Regexp
	...	${REGEXP}	${DEVICE}

SUITE:Discover Device And Wait For Discovery Logs To Match Regexp
	[Arguments]	${REGEXP}	${DEVICE}
	CLI:Discover Now	${DEVICE}
	Wait Until Keyword Succeeds	60s	3s
	...	CLI:Discovery Logs Should Match Regexp And Time	${REGEXP}	${TIME_SECONDS}

SUITE:Enable Hostname Detection For Device
	[Arguments]	${DEVICE_NAME}
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/access
	CLI:Set	enable_hostname_detection=yes
	CLI:Commit

SUITE:Enable Perle SCS Serial Port
	CLI:General Open	${PERLE_SCS_SERVER_IP}	${PERLE_SCS_SERVER_USERNAME}	${PERLE_SCS_SERVER_PASSWORD}	perle_session	prompt=${PERLE_SCS_PROMPT}
	Write Bare	set line ${PERLE_SCS_AVAILABLE_PORTS[0]} mode enabled${PERLE_SCS_NEW_LINE}
	${OUTPUT}=	Read Until Prompt
	Log To Console	\n${OUTPUT}
	Write Bare	kill line ${PERLE_SCS_AVAILABLE_PORTS[0]}${PERLE_SCS_NEW_LINE}
	${OUTPUT}=	Read Until Prompt
	Log To Console	\n${OUTPUT}
	CLI:Close Connection