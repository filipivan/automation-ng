*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Auto Discovery VM Managers... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${INTERVAL_IN_MINUTES}	15

*** Test Cases ***
Test adding valid VM Manager
	CLI:Enter Path	/settings/auto_discovery/vm_managers/
	CLI:Delete If Exists Confirm	${VMMANAGER["ip"]}
	CLI:Add
	CLI:Set	vm_server=${VMMANAGER["ip"]} username=${VMMANAGER["username"]} password=${VMMANAGER["password"]}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Show Command	vm_server = ${VMMANAGER["ip"]}	username = ${VMMANAGER["username"]}	type = VMware	html_console_port = 7331,7343
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Show Command	vm_server = ${VMMANAGER["ip"]}	username = ${VMMANAGER["username"]}	type	html_console_port = 7331,7343
	CLI:Commit
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Show Command Regexp	\\s+${VMMANAGER["ip"]}\\s+VMware\\s+No\\s+15
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Show Command Regexp	\\s+${VMMANAGER["ip"]}\\s+VMware\\s+no\\s+15
	[Teardown]	CLI:Cancel	Raw

Test editing valid VM Manager
	CLI:Enter Path	/settings/auto_discovery/vm_managers/${VMMANAGER["ip"]}/
	CLI:Test Show Command	vm_server: ${VMMANAGER["ip"]}	username = ${VMMANAGER["username"]}	password = ********	type = VMware	html_console_port = 7331,7343	discover_virtual_machines = no
	CLI:Set	discover_virtual_machines=yes
	CLI:Commit
	CLI:Test Show Command	vm_server: ${VMMANAGER["ip"]}	username = ${VMMANAGER["username"]}	password = ********	type = VMware	html_console_port = 7331,7343	discover_virtual_machines = yes	interval_in_minutes = 15	discovery_scope
	CLI:Set	interval_in_minutes=12
	CLI:Commit
	CLI:Enter Path	/settings/auto_discovery/vm_managers/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Show Command Regexp	\\s+${VMMANAGER["ip"]}\\s+VMware\\s+Yes\\s+12
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Show Command Regexp	\\s+${VMMANAGER["ip"]}\\s+VMware\\s+yes\\s+12
	CLI:Delete If Exists Confirm	${VMMANAGER["ip"]}
	[Teardown]	Run Keywords	CLI:Enter Path	/settings/auto_discovery/vm_managers/	AND	CLI:Delete If Exists Confirm	${VMMANAGER["ip"]}

Test invalid values for field=username
	[Tags]	EXCLUDEIN3_2	# Error messages not implemented on 3.2
	SUITE:Add VM Manager
	CLI:Enter Path	/settings/auto_discovery/vm_managers/${VMMANAGER["ip"]}
	CLI:Test Set Field Invalid Options	username	${POINTS}	Error: username: This field should contain only numbers, letters, and the characters @ . \\ /.
	CLI:Test Set Field Invalid Options	username	${EXCEEDED}	Error: username: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test invalid values for field=type
	SUITE:Add VM Manager
	CLI:Enter Path	/settings/auto_discovery/vm_managers/${VMMANAGER["ip"]}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	type	${EMPTY}	Error: Missing value for parameter: type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	type	${EMPTY}	Error: Missing value: type
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	type	${WORD}	Error: Invalid value: ${WORD} for parameter: type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	type	${WORD}	Error: Invalid value: ${WORD}
	[Teardown]	CLI:Revert

Test invalid values for field=html_console_port
	[Tags]	EXCLUDEIN3_2	# Error messages not implemented on 3.2
	SUITE:Add VM Manager
	CLI:Enter Path	/settings/auto_discovery/vm_managers/${VMMANAGER["ip"]}/
	CLI:Test Set Field Invalid Options	html_console_port	${POINTS}	Error: html_console_port: Validation error.
	CLI:Test Set Field Invalid Options	html_console_port	${EXCEEDED}	Error: html_console_port: Validation error.
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Revert

Test invalid values for field=discover_virtual_machines
	SUITE:Add VM Manager
	CLI:Enter Path	/settings/auto_discovery/vm_managers/${VMMANAGER["ip"]}/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	discover_virtual_machines	${EMPTY}	Error: Missing value for parameter: discover_virtual_machines
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	discover_virtual_machines	${EMPTY}	Error: Missing value: discover_virtual_machines
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	discover_virtual_machines	${WORD}	Error: Invalid value: ${WORD} for parameter: discover_virtual_machines
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	discover_virtual_machines	${WORD}	Error: Invalid value: ${WORD}
	CLI:Test Set Field Options	discover_virtual_machines	yes	no
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Revert

Test invalid values for field=interval_in_minutes
	[Tags]	EXCLUDEIN3_2	# Error messages not implemented on 3.2
	SUITE:Add VM Manager
	CLI:Enter Path	/settings/auto_discovery/vm_managers/${VMMANAGER["ip"]}/
	CLI:Set	discover_virtual_machines=yes
	CLI:Commit
	CLI:Test Set Field Invalid Options	interval_in_minutes	${EMPTY}	Error: interval_in_minutes: Validation error.
	CLI:Test Set Field Invalid Options	interval_in_minutes	${WORD}	Error: interval_in_minutes: Validation error.
	CLI:Test Set Field Invalid Options	interval_in_minutes	${EXCEEDED}	Error: interval_in_minutes: Exceeded the maximum size for this field.
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Revert

Test invalid values for field=discovery_scope
	[Tags]	EXCLUDEIN3_2	# Error messages not implemented on 3.2
	SUITE:Add VM Manager
	CLI:Enter Path	/settings/auto_discovery/vm_managers/${VMMANAGER["ip"]}/
	CLI:Set	discover_virtual_machines=yes
	CLI:Commit
	CLI:Set	discover_virtual_machines=no
	CLI:Commit
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Revert

Test delete VM Manager
	CLI:Enter Path	/settings/auto_discovery/vm_managers/
	CLI:Delete If Exists Confirm	${VMMANAGER["ip"]}
	[Teardown]	CLI:Revert

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Delete VM Manager And Hosts

SUITE:Teardown
	SUITE:Delete VM Manager And Hosts
	CLI:Close Connection

SUITE:Add VM Manager
	CLI:Enter Path	/settings/auto_discovery/vm_managers/
	CLI:Delete If Exists Confirm	${VMMANAGER["ip"]}
	CLI:Add
	CLI:Set	vm_server=${VMMANAGER["ip"]} username=${VMMANAGER["username"]} password=${VMMANAGER["password"]}
	CLI:Set	type=VMware
	CLI:Commit
	CLI:Show

SUITE:Delete VM Manager And Hosts
	CLI:Enter Path	/settings/auto_discovery/vm_managers
	CLI:Delete If Exists Confirm	${VMMANAGER["ip"]}	@{VMMANAGER["hosts"]}