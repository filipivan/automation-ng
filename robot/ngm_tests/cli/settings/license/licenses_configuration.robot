*** Settings ***
Resource	../../init.robot
Documentation	Test License Features
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW	LICENSES	ADD

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${LICENSE}	${FIFTY_DEVICES_ACCESS_LICENSE}
${EXPLICENSE}	${FIFTY_DEVICES_ACCESS_LICENSE_EXPIRED}
${LICENSE_SERIAL_NUMBER}	00000000043

*** Test Cases ***
Test Add Valid License Key
	CLI:Add
	CLI:Set	license_key=${LICENSE}
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	license_key = ${LICENSE}
	CLI:Commit
	${OUTPUT}=	CLI:Show
	Run Keyword If	'${NGVERSION}' == '3.2'	Set Suite Variable	${regex}	\[x]{5}\\-\[x]{5}\\-\[x]{5}\\-\[A-Z0-9]{5}\\s{2}\[0-9]{10,11}\\s+\[a-z]{3,10}\\s+\[0-9]{1,5}\\s+\[A-Za-z0-9- ]{10,100}
	Run Keyword If	'${NGVERSION}' >= '4.2'	Set Suite Variable	${regex}	\\d{11}\\s+[x]{5}\\-[x]{5}\\-[x]{5}\\-[A-Z0-9]{5}\\s{2}access\\s+\\d+\\s+\\d{4}-\\d{2}-\\d{2}\\s+local\\s+locally\\sinstalled
	${OUTPUT}=	Remove String Using Regexp	${OUTPUT}	['\r']
	${Details}=	Get Lines Containing String	${OUTPUT}	BTXIM
	Should Match Regexp	${Details}	${regex}
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	CLI:Delete All License Keys Installed

Test Add Expired License Key
	Run Keyword If	'${NGVERSION}' == '3.2'	Set Tags	NON-CRITICAL	#implementation is different in nightly/official
	CLI:Add
	CLI:Set	license_key=${EXPLICENSE}
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	license_key = ${EXPLICENSE}
	CLI:Commit
	${OUTPUT}=	CLI:Show
	${regex}=	Run Keyword If	'${NGVERSION}' == '3.2'	Set Variable	\\s*[x]{5}\-[x]{5}\-[x]{5}\-[A-Z0-9]{5}\\s{2}[0-9]{10,11}\\s+[a-z]{3,10}\\s+[0-9]{1,5}\\s+\\d{4}\-\\d{2}\-\\d{2}
	...	ELSE	Set Variable	\\d{11}\\s+x{5}-x{5}-x{5}-33MGM\\s+access\\s+50\\s+\\d{4}-\\d{2}-\\d{2}\\s+invalid\\sexpiration\\sor\\sclock\\sturned\\sback\\sdetected\\s+local\\s+locally\\sinstalled
	${OUTPUT}=	Remove String Using Regexp	${OUTPUT}	['\r']
	${Details}=	Get Lines Containing String	${OUTPUT}	33MGM
	Should Match Regexp	${Details}	${regex}
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	CLI:Delete All License Keys Installed

Test Add Duplicate License Key
	Skip If	'${NGVERSION}' == '3.2'	Message on 3.2 is weird but probably won't be fixed since it's only on 3.2
	CLI:Add License Key	${EXPLICENSE}
	CLI:Enter Path	/settings/license
	CLI:Add
	CLI:Set	license_key=${EXPLICENSE}
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	license_key = ${EXPLICENSE}
	${OUTPUT}=	CLI:Commit	Raw
	${ERRORMSG}=	Run Keyword If	'${NGVERSION}' >= '4.2'	Set Variable	Error: license_key: License Already Installed
	...	ELSE	Set Variable	Error: license_key: Cannot add duplicate license.
	Should Contain	${OUTPUT}	${ERRORMSG}	Raw
	[Teardown]	CLI:Cancel	Raw

Test Delete License Full Key
	CLI:Enter Path	/settings/license
	${OUTPUT}=	CLI:Write	delete ${EXPLICENSE}	Raw
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should contain	${OUTPUT}	Error: Invalid Target name: ${EXPLICENSE}. Please, use tab-tab to obtain available targets.
	Run Keyword If	'${NGVERSION}' == '3.2'	Should contain	${OUTPUT}	Error: Invalid Target: ${EXPLICENSE}. Press <tab> for a list of valid targets.

Test Delete License Key Manually Entered
	${LICENSEKEYPART}=	Fetch from Right	${EXPLICENSE}	-
	CLI:Enter Path	/settings/license
	${OUTPUT}=	CLI:Show
	${Key}=	Run Keyword And Ignore Error	Should Not Contain	${OUTPUT}	xxxxx-xxxxx-xxxxx-${LICENSEKEYPART}
	${KEYEXIST}=	Get From List	${Key}	0
	Run Keyword if	'${KEYEXIST}' != 'FAIL'	Write	add
	${OUTPUT}=	Run Keyword if	'${KEYEXIST}' != 'FAIL'	Read Until	{license}]#
	Run Keyword if	'${KEYEXIST}' != 'FAIL'	Write	set license_key=${EXPLICENSE}
	${OUTPUT}=	Run Keyword if	'${KEYEXIST}' != 'FAIL'	Read Until	{license}]#
	Run Keyword if	'${KEYEXIST}' != 'FAIL'	Write	commit
	${OUTPUT}=	Run Keyword if	'${KEYEXIST}' != 'FAIL'	Read Until Prompt
	${OUTPUT}=	CLI:Show
	CLI:Delete All License Keys Installed
	${OUTPUT}=	CLI:Show
	Should Not Contain	${OUTPUT}	xxxxx-xxxxx-xxxxx-${LICENSEKEYPART}

Test Delete License Key Tab
	${LICENSEKEYPART}=	Fetch from Right	${LICENSE}	-
	CLI:Enter Path	/settings/license
	${OUTPUT}=	CLI:Show
	${Key}=	Run Keyword And Ignore Error	Should Not Contain	${OUTPUT}	xxxxx-xxxxx-xxxxx-${LICENSEKEYPART}
	${KEYEXIST}=	Get From List	${Key}	0
	Run Keyword if	'${KEYEXIST}' != 'FAIL'	CLI:Add License Key	${LICENSE}
	CLI:Write Bare	delete \t\n
	${EXTRA_LICENSE}=	Run Keyword And Return Status	should match regexp	${OUTPUT}	Error: Invalid Target name: \\d+. Please, use tab-tab to obtain available targets.
	Run Keyword If	${EXTRA_LICENSE}	CLI:Delete	${LICENSE_SERIAL_NUMBER}
	Run Keyword If	${NGVERSION} != 3.2	Should Match Regexp	${OUTPUT}	serial( |_)number\\s+license( |_)key\\s+application\\s+number( |_)of( |_)licenses\\s+type\\s+peer( |_)address\\s+expiration( |_)date\\s+details
	...	ELSE	Should Match Regexp	${OUTPUT}	license( |_)key\\s+serial( |_)number\\s+type\\s+number( |_)of( |_)licenses\\s+details
	Should Not Contain	${OUTPUT}	xxxxx-xxxxx-xxxxx-${LICENSEKEYPART}

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Delete All License Keys Installed

SUITE:Teardown
	CLI:Delete All License Keys Installed
	CLI:Close Connection
