*** Settings ***
Resource	../../init.robot
Documentation	Test license renewal banner visibility
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	BUG-3769
Default Tags	CLI	SSH	SHOW	LICENSES	ADD

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EXPIRED_LICENSE}	${FIFTY_DEVICES_ACCESS_LICENSE_EXPIRED}
${LICENSE}	${FIFTY_DEVICES_ACCESS_LICENSE}
${YEAR}	2024
${MONTH}	12
${DAY}	30
${BANNER_REGEX}	${LICENSE_RENEWAL_REGEX}

*** Test Cases ***
Check License Banner With Almost Expiring License
	[Tags]	NON-CRITICAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6	EXCLUDEIN5_8	EXCLUDEIN5_10
	SUITE:Clear Event Log And Change Date To Next To Expiring Date
	CLI:Add License Key	${LICENSE}
	SUITE:Wait Until License Banner Is Visible
	[Teardown]	CLI:Delete All License Keys Installed

Check License Banner With Not Expiring License
	#SUITE:Clear Event Log And Chage Date To Network Time Protocol	#Commented while test Check License Banner With Almost Expiring License is excluded
	CLI:Add License Key	${LICENSE}
	SUITE:License Banner Should Not Be Visible After Waiting
	[Teardown]	CLI:Delete All License Keys Installed

Check License Banner With Expired License
	#SUITE:Clear Event Log And Chage Date To Network Time Protocol	#Commented while test Check License Banner With Almost Expiring License is excluded
	CLI:Add License Key	${EXPIRED_LICENSE}
	SUITE:License Banner Should Not Be Visible After Waiting
	[Teardown]	CLI:Delete All License Keys Installed

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Delete All License Keys Installed

SUITE:Teardown
	CLI:Switch Connection	default
	CLI:Delete All License Keys Installed
	#SUITE:Clear Event Log And Chage Date To Network Time Protocol	#Commented while test Check License Banner With Almost Expiring License is excluded
	CLI:Close Connection

SUITE:Clear Event Log And Change Date To Next To Expiring Date
	CLI:Write	event_system_clear
	CLI:Enter Path  /settings/date_and_time
	CLI:Set	date_and_time=manual
	CLI:Set	year=${YEAR} month=${MONTH} day=${DAY}
	CLI:Commit
	CLI:Show
	Sleep	10

SUITE:Clear Event Log And Chage Date To Network Time Protocol
	CLI:Write	event_system_clear
	CLI:Enter Path	/settings/date_and_time
	CLI:Set	date_and_time=network_time_protocol
	CLI:Commit
	CLI:Show
	Sleep	10

SUITE:Wait Until Event 134 Triggers
	[Arguments]	${TIMEOUT}
	Wait Until Keyword Succeeds	${TIMEOUT}	5s	SUITE:Check For Event 134

SUITE:Check For Event 134
	Write	event_system_audit
	${OUTPUT}=	Read Until Regexp
	...	\\(h->Help, q->Quit\\) [a-zA-Z0-9\\-_\\./\\\ ]+ (?:\\(End\\))?(?:[ ]*\\(Begin\\))?
	CLI:Write	q
	Should Contain	${OUTPUT}	Event ID 134

SUITE:Wait Until License Banner Is Visible
	[Arguments]	${TIMEOUT}=3m
	SUITE:Wait Until Event 134 Triggers	${TIMEOUT}
	SUITE:License Banner Should Be Visible After Opening Connection

SUITE:License Banner Should Not Be Visible After Waiting
	${STATUS}=	Run Keyword And Return Status	SUITE:Wait Until License Banner Is Visible	1m
	Run Keyword If	'${STATUS}' == 'True'	Fail	License Banner is present and should not be

SUITE:License Banner Should Be Visible After Opening Connection
	${BANNER}=	SUITE:Get Post Login Banner
	Should Match Regexp	${BANNER}	${BANNER_REGEX}

SUITE:Get Post Login Banner
	CLI:Open	HOST_DIFF=${HOSTPEER}	session_alias=peer_session
	Set Client Configuration	prompt=~$
	CLI:Write	shell
	Write	ssh ${DEFAULT_USERNAME}@${HOST}
	${OUTPUT}=	Read	delay=5s	loglevel=INFO
	Should Not Contain	${OUTPUT}	ssh: connect to host ${HOST} port 22: No route to host
	${CERTIFICATE_NOT_FOUND}=	Run Keyword And Return Status
	...	Should Contain	${OUTPUT}	ECDSA host key for ${HOST} has changed and you have requested strict checking.
	Run Keyword If	"${CERTIFICATE_NOT_FOUND}" == "True"
	...	Write	ssh-keygen -f "/home/${DEFAULT_USERNAME}/.ssh/known_hosts" -R "${HOST}"
	Run Keyword If	"${CERTIFICATE_NOT_FOUND}" == "True"	Read Until Prompt	loglevel=INFO
	Run Keyword If	'${CERTIFICATE_NOT_FOUND}' == "True"
	...	Write	ssh ${DEFAULT_USERNAME}@${HOST}
	${OUTPUT}=	Run Keyword If	'${CERTIFICATE_NOT_FOUND}' == "True"
	...	Read	delay=5s	loglevel=INFO
	...	ELSE	Set Variable	${OUTPUT}

	${CERTIFICATE}=	Run Keyword And Return Status
	...	Should Match Regexp	${OUTPUT}	Are you sure you want to continue connecting \\(.*\\)\\?
	Run Keyword If	'${CERTIFICATE}' == "True"	Write	yes
	${OUTPUT}=	Run Keyword If	'${CERTIFICATE}' == "True"
	...	Read Until	Password:	loglevel=INFO
	...	ELSE	Set Variable	${OUTPUT}

	Should Contain	${OUTPUT}	Password:
	Set Client Configuration	prompt=]#
	${OUTPUT}=	CLI:Write	${DEFAULT_PASSWORD}
	Set Client Configuration	prompt=~$
	CLI:Write	exit
	Set Client Configuration	prompt=]#
	CLI:Write	exit
	CLI:Close Connection	Yes
	CLI:Switch Connection	default

	[Return]	${OUTPUT}