*** Settings ***
Resource	../../init.robot
Documentation	Test License Features
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW	LICENSES	ADD

Suite Setup	SUITE:Open
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test Show Licenses Information
	# 4.0 shows more information about licenses
	${regex}=	Run Keyword If	'${NGVERSION}' < '4.0'	Set Variable	\[x]{5}\\-\[x]{5}\\-\[x]{5}\\-\[A-Z0-9]{5}\\s{2}\[0-9]{10,11}\\s+\[a-z]{3,10}\\s+\[0-9]{1,5}\\s+\[A-Za-z0-9- ]{10,100}
	...	ELSE	Set Variable	\\d{11}\\s+\[x]{5}\\-\[x]{5}\\-\[x]{5}\\-\[A-Z0-9]{5}\\s{2}\[0-9]{10,11}\\s+\[a-z]{3,10}\\s+\[0-9]{1,5}\\s+\[a-z0-9-]{10,15}\\s+\[a-z]+\\s+[a-z]+\\s\[a-z]+
	CLI:Enter Path	/settings/license
	${OUTPUT}=	CLI:Show
	${OUTPUT}=	Remove String Using Regexp	${OUTPUT}	['\r']
	@{Details}=	Split to Lines	${OUTPUT}	0	1
	FOR			${LINE}	IN	@{Details}
	#	Should Contain  ${LINE}	license( |_)key	serial number
	 	Should Match Regexp	${LINE}	license( |_)key	serial( |_)number
#		Should Contain	${LINE}	type	number of licenses	details
		Should Match Regexp	${LINE}	type	number( |_)of( |_)licenses	details
	END
	@{Details}=	Split to Lines	${OUTPUT}	1	2
	FOR			${LINE}	IN	@{Details}
		Should Contain	${LINE}	===========	=============
		Should Contain	${LINE}	====	==================	=======
	END
	@{Details}=	Split to Lines	${OUTPUT}	2	-3
# For Loop to test Child details is not implemented as no Child exist by default.
	FOR			${LINE}	IN	@{Details}
		${LINE}=	Strip String	${LINE}
		Should Match Regexp	${LINE}	${regex}
	END

Test Add Invalid License Key
	CLI:Enter Path	/settings/license
	CLI:ADD
	CLI:Set Field	license_key	werr23
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	license_key = werr23
	${OUTPUT}=	CLI:Commit	Raw
	Should contain	${OUTPUT}	Error: license_key: Not a Valid License Key
	[Teardown]	CLI:Cancel	Raw

Test Add Empty License Key
	CLI:Enter Path	/settings/license
	CLI:ADD
	CLI:Set Field	license_key	${EMPTY}
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	license_key =
	${OUTPUT}=	CLI:Commit	Raw
	Should contain	${OUTPUT}	Error: license_key: Field must not be empty.
	[Teardown]	CLI:Cancel	Raw

Test available commands after send tab-tab
	CLI:Enter Path	/settings/license/
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Available Commands	show_settings
	CLI:Test Available Commands	event_system_clear	exit	shell	add	hostname	show	cd	ls	change_password	pwd	shutdown	commit	quit	whoami	delete	reboot	event_system_audit	revert

Test available fields for set command
	Write	add
	${OUTPUT}=	Read Until	{license}]#
	CLI:Test Set Available Field	license_key

Test validation for commit invalid value
	Write	set license_key=*(&()))
	CLI:Read Until Prompt	Raw
	${OUTPUT}=	CLI:Commit	Raw
	Should Contain	${OUTPUT}	Error

Test cancel command works
	${OUTPUT}=	CLI:Cancel
	Should Not Contain	${OUTPUT}	Error

Test show_settings command
	CLI:Enter Path	/settings/license/
	Write	show_settings
	${OUTPUT}=	CLI:Read Until Prompt
	Should Not Contain	${OUTPUT}	/settings/license/

*** Keywords ***
SUITE:Open
	CLI:Open
	CLI:Enter Path	/settings/license
	${OUTPUT}=	CLI:Show
	${OUTPUT}=	Remove String Using Regexp	${OUTPUT}	['\r']
	@{Details}=	Split to Lines	${OUTPUT}	2	-3
	FOR			${LINE}	IN	@{Details}
		${LINE}=	Strip String	${LINE}
		@{LINE}=	Split String	${LINE}	${SPACE}
		CLI:Delete	${LINE}[0]
	END

SUITE:Teardown
	CLI:Enter Path	/settings/license
	${OUTPUT}=	CLI:Show
	${OUTPUT}=	Remove String Using Regexp	${OUTPUT}	['\r']
	@{Details}=	Split to Lines	${OUTPUT}	2	-3
	FOR			${LINE}	IN	@{Details}
		${LINE}=	Strip String	${LINE}
		@{LINE}=	Split String	${LINE}	${SPACE}
		CLI:Delete	${LINE}[0]
	END
	CLI:Close Connection
