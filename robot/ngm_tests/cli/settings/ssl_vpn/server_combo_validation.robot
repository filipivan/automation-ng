*** Settings ***
Resource	../../init.robot
Documentation	Tests Validate Fields at NETWORK :: SSL VPN :: SERVER
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SSL_VPN	SERVER	VALIDATE	MODIFY
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${LISTEN_IP}	1.1.1.1
${LISTEN_PORT}	1
${TUNNEL_MTU}	2
${NUMBER_TUNNELS}	2

${IPV4_TUNNEL}	192.168.16.1
${IPV6_TUNNEL}	f111::c111:1111:a111:c111

${LOCAL_ENDPOINT}	1.1.1.1
${REMOTE_ENDPOINT}	2.2.2.2

@{LOCAL_IPV6_ENDPOINT}	fe11::c111:1111:a111:c111	::1111	::f	f::
...	fe11::c111:1111:a111:f:f:f	a:b:c:d:e:f:1:1
@{REMOTE_IPV6_ENDPOINT}	fe22::c222:2222:a222:c222	::2222	::f	f::
...	fe22::c222:2222:a222:f:f:f	a:b:c:d:e:f:2:2

${MAX_NETMASK}	255.255.255.255
${MAX_BITMASK}	128

@{INVALID_NETMASK}	33	256.255.255.256	256.256.255.255
...	256.256.256.255	256.256.256.256	256.256.255.256	256.255.255.256
...	256.255.256.256	255.256.255.255	255.256.256.255	255.256.255.256
...	255.256.256.256	255.255.256.255	255.255.256.256	255.255.255.256
...	///	/2ff
@{INVALID_BITMASK}	129	/128	/129	/fffff${POINTS}	//2fff${POINTS}
...	/-----	//////

@{INVALID_IP}	a.168.16.1	a.a.16.1	a.a.a.1	a.a.a.a	a.a.168.a
...	a.168.16.a	a.168.a.1	a.168.a.a	192.a.16.1	192.a.a.1
...	192.a.16.a	192.a.a.a	192.168.a.1	192.168.a.a	192.168.16.a
...	256.255.255.256	256.256.255.255	256.256.256.255	256.256.256.256
...	256.256.255.256	256.255.255.256	256.255.256.256	255.256.255.255
...	255.256.256.255	255.256.255.256	255.256.256.256	255.255.256.255
...	255.255.256.256	255.255.255.256
@{INVALID_IPV6}	fa222::c222:2222:a222:c222	fa222::ca222:2222:a222:c222
...	fa222::ca222:2a222:a222:c222	fa222::ca222:2a222:aa222:c222
...	fa222::ca222:2a222:aa222:ca222	fa222::ca222:2a222:aa222:ca222
...	g222::c222:2222:a222:c222	g222::g222:2222:a222:c222	:2222:
...	g222::g222:g222:a222:c222	g222::g222:g222:g222:c222	:2222
...	g222::g222:g222:g222:g222	f.222::c222:2222:a222:c222	:2222:2

*** Test Cases ***
Test file events available commands after send tab-tab
	[Tags]	SHOW
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Test Available Commands	apply_settings	cd	change_password	commit	event_system_audit	event_system_clear	exit
	...	factory_settings	hostname	ls	pwd	quit	reboot	revert	save_settings	set	shell	show	shutdown
	...	software_upgrade	system_certificate	system_config_check	whoami

Test show command at vpn server
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Show

Test Enter ssl_vpn
	CLI:Enter Path	/settings/ssl_vpn
	CLI:Test Ls Command	 client	server	server_status

Test show_settings
	CLI:Enter Path	/settings/ssl_vpn/
	${OUTPUT}=	CLI:Show Settings
	Should Match Regexp	${OUTPUT}	/settings/ssl_vpn/server status=disabled
	Should Match Regexp	${OUTPUT}	/settings/ssl_vpn/server listen_port_number=1194
	Should Match Regexp	${OUTPUT}	/settings/ssl_vpn/server protocol=udp
	Should Match Regexp	${OUTPUT}	/settings/ssl_vpn/server tunnel_mtu=1500
	Should Match Regexp	${OUTPUT}	/settings/ssl_vpn/server number_of_concurrent_tunnels=256
	Should Match Regexp	${OUTPUT}	/settings/ssl_vpn/server authentication_method=tls
	Should Match Regexp	${OUTPUT}	/settings/ssl_vpn/server ip_addr=network
	Should Match Regexp	${OUTPUT}	/settings/ssl_vpn/server hmac|message_digest=SHA1
	Should Match Regexp	${OUTPUT}	/settings/ssl_vpn/server cipher=BF-CBC
	Should Match Regexp	${OUTPUT}	/settings/ssl_vpn/server min_tls_version=none
	Should Match Regexp	${OUTPUT}	/settings/ssl_vpn/server use_lzo_data_compress_algorithm=no
	Should Match Regexp	${OUTPUT}	/settings/ssl_vpn/server redirect_gateway=no

Test server available fields to set
	CLI:Enter Path	/settings/ssl_vpn/server
	CLI:Test Set Available Fields	status	listen_ip_address	authentication_method	min_tls_version	ca_certificate
	...	number_of_concurrent_tunnels	cipher	protocol	diffie_hellman	redirect_gateway	hmac|message_digest
	...	secret	ip_addr	server_certificate	server_key	tunnel_mtu	listen_port_number
	...	use_lzo_data_compress_algorithm

Test with field=status
	CLI:Enter Path	/settings/ssl_vpn/server/
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	status	${EMPTY}	Error: Missing value for parameter: status
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	status	${EMPTY}	Error: Missing value: status

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	status	${WORD}	Error: Invalid value: ${WORD} for parameter: status
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	status	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	status	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: status
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	status	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	status	${POINTS}	Error: Invalid value: ${POINTS} for parameter: status
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	status	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	status	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: status
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	status	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	status	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: status
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	status	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	status	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: status
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	status	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	status	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD} for parameter: status
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	status	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	status	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER} for parameter: status
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	status	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	status	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS} for parameter: status
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	status	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	status	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: status
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	status	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}

	${SETOUTPUT}=	CLI:Write	set . status=enabled	Raw	Yes
	${ERROR}=	Run Keyword And Return Status	Should Contain	${SETOUTPUT}	Error:
	Run Keyword If	${ERROR}	Should Contain	${SETOUTPUT}	Error: Authentication Method requires certificates in /etc/openvpn/CA
	CLI:Test Set Validate Normal Fields	status	disabled
	[Teardown]	CLI:Revert	Raw

Test with field=listen_ip_address
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Test Set Validate Invalid Options	listen_ip_address	${EMPTY}
	CLI:Test Set Validate Invalid Options	listen_ip_address	${WORD}	Error: listen_ip_address: Invalid IP address.
	CLI:Test Set Validate Invalid Options	listen_ip_address	${NUMBER}	Error: listen_ip_address: Invalid IP address.
	CLI:Test Set Validate Invalid Options	listen_ip_address	${POINTS}	Error: listen_ip_address: Invalid IP address.

	CLI:Test Set Validate Invalid Options	listen_ip_address	${ONE_WORD}	Error: listen_ip_address: Invalid IP address.
	CLI:Test Set Validate Invalid Options	listen_ip_address	${ONE_NUMBER}	Error: listen_ip_address: Invalid IP address.
	CLI:Test Set Validate Invalid Options	listen_ip_address	${ONE_POINTS}	Error: listen_ip_address: Invalid IP address.

	CLI:Test Set Validate Invalid Options	listen_ip_address	${ELEVEN_WORD}	Error: listen_ip_address: Invalid IP address.
	CLI:Test Set Validate Invalid Options	listen_ip_address	${ELEVEN_NUMBER}	Error: listen_ip_address: Invalid IP address.
	CLI:Test Set Validate Invalid Options	listen_ip_address	${ELEVEN_POINTS}	Error: listen_ip_address: Invalid IP address.
	CLI:Test Set Validate Invalid Options	listen_ip_address	${EXCEEDED}	Error: listen_ip_address: Invalid IP address.
	CLI:Test Set Validate Normal Fields	listen_ip_address	${LISTEN_IP}
	[Teardown]	CLI:Revert	Raw

Test with field=listen_port_number
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Test Set Validate Invalid Options	listen_port_number	${EMPTY}	Error: listen_port_number: Validation error.
	CLI:Test Set Validate Invalid Options	listen_port_number	${WORD}	Error: listen_port_number: Validation error.
	CLI:Test Set Validate Invalid Options	listen_port_number	${NUMBER}
	CLI:Test Set Validate Invalid Options	listen_port_number	${POINTS}	Error: listen_port_number: Validation error.

	CLI:Test Set Validate Invalid Options	listen_port_number	${ONE_WORD}	Error: listen_port_number: Validation error.
	CLI:Test Set Validate Invalid Options	listen_port_number	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	listen_port_number	${ONE_POINTS}	Error: listen_port_number: Validation error.

	CLI:Test Set Validate Invalid Options	listen_port_number	${ELEVEN_WORD}	Error: listen_port_number: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	listen_port_number	${ELEVEN_NUMBER}	Error: listen_port_number: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	listen_port_number	${ELEVEN_POINTS}	Error: listen_port_number: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	listen_port_number	${EXCEEDED}	Error: listen_port_number: Exceeded the maximum size for this field.
	CLI:Test Set Validate Normal Fields	listen_port_number	${LISTEN_PORT}
	[Teardown]	CLI:Revert	Raw

Test with field=protocol
	CLI:Enter Path	/settings/ssl_vpn/server/
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	protocol	${EMPTY}	Error: Missing value for parameter: protocol
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	protocol	${EMPTY}	Error: Missing value: protocol

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	protocol	${WORD}	Error: Invalid value: ${WORD} for parameter: protocol
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	protocol	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	protocol	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: protocol
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	protocol	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	protocol	${POINTS}	Error: Invalid value: ${POINTS} for parameter: protocol
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	protocol	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	protocol	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: protocol
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	protocol	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	protocol	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: protocol
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	protocol	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	protocol	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: protocol
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	protocol	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	protocol	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD} for parameter: protocol
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	protocol	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	protocol	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER} for parameter: protocol
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	protocol	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	protocol	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS} for parameter: protocol
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	protocol	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	protocol	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: protocol
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	protocol	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	protocol	udp	tcp	upd-ipv6	tcp-ipv6
	[Teardown]	CLI:Revert	Raw

Test with field=tunnel_mtu
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Test Set Validate Invalid Options	tunnel_mtu	${EMPTY}	Error: tunnel_mtu: Validation error.
	CLI:Test Set Validate Invalid Options	tunnel_mtu	${WORD}	Error: tunnel_mtu: Validation error.
	CLI:Test Set Validate Invalid Options	tunnel_mtu	${NUMBER}
	CLI:Test Set Validate Invalid Options	tunnel_mtu	${POINTS}	Error: tunnel_mtu: Validation error.
	CLI:Test Set Validate Invalid Options	tunnel_mtu	${ONE_WORD}	Error: tunnel_mtu: Validation error.
	CLI:Test Set Validate Invalid Options	tunnel_mtu	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	tunnel_mtu	${ONE_POINTS}	Error: tunnel_mtu: Validation error.
	CLI:Test Set Validate Invalid Options	tunnel_mtu	${ELEVEN_WORD}	Error: tunnel_mtu: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	tunnel_mtu	${ELEVEN_NUMBER}	Error: tunnel_mtu: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	tunnel_mtu	${ELEVEN_POINTS}	Error: tunnel_mtu: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	tunnel_mtu	${EXCEEDED}	Error: tunnel_mtu: Exceeded the maximum size for this field.
	CLI:Test Set Validate Normal Fields	tunnel_mtu	${TUNNEL_MTU}
	[Teardown]	CLI:Revert	Raw

Test with field=number_of_concurrent_tunnels
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Test Set Validate Invalid Options	number_of_concurrent_tunnels	${EMPTY}	Error: number_of_concurrent_tunnels: Validation error.
	CLI:Test Set Validate Invalid Options	number_of_concurrent_tunnels	${WORD}	Error: number_of_concurrent_tunnels: Validation error.
	CLI:Test Set Validate Invalid Options	number_of_concurrent_tunnels	${NUMBER}
	CLI:Test Set Validate Invalid Options	number_of_concurrent_tunnels	${POINTS}	Error: number_of_concurrent_tunnels: Validation error.
	CLI:Test Set Validate Invalid Options	number_of_concurrent_tunnels	${ONE_WORD}	Error: number_of_concurrent_tunnels: Validation error.
	CLI:Test Set Validate Invalid Options	number_of_concurrent_tunnels	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	number_of_concurrent_tunnels	${ONE_POINTS}	Error: number_of_concurrent_tunnels: Validation error.
	CLI:Test Set Validate Invalid Options	number_of_concurrent_tunnels	${ELEVEN_WORD}	Error: number_of_concurrent_tunnels: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	number_of_concurrent_tunnels	${ELEVEN_NUMBER}	Error: number_of_concurrent_tunnels: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	number_of_concurrent_tunnels	${ELEVEN_POINTS}	Error: number_of_concurrent_tunnels: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	number_of_concurrent_tunnels	${EXCEEDED}	Error: number_of_concurrent_tunnels: Exceeded the maximum size for this field.
	CLI:Test Set Validate Normal Fields	number_of_concurrent_tunnels	${NUMBER_TUNNELS}
	[Teardown]	CLI:Revert	Raw

Test with field=ip_addr
	CLI:Enter Path	/settings/ssl_vpn/server/
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ip_addr	${WORD}	Error: Invalid value: ${WORD} for parameter: ip_addr
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ip_addr	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ip_addr	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: ip_addr
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ip_addr	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ip_addr	${POINTS}	Error: Invalid value: ${POINTS} for parameter: ip_addr
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ip_addr	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ip_addr	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: ip_addr
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ip_addr	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ip_addr	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: ip_addr
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ip_addr	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ip_addr	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: ip_addr
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ip_addr	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ip_addr	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD} for parameter: ip_addr
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ip_addr	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ip_addr	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER} for parameter: ip_addr
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ip_addr	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ip_addr	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS} for parameter: ip_addr
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ip_addr	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ip_addr	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: ip_addr
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ip_addr	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	ip_addr	p2p	p2p-ipv6	network
	[Teardown]	CLI:Revert	Raw

#I don't know why, but need authentication_method equals tls
Test with field=ip_addr equals Network for ipv4_tunnel
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set Field	ip_addr	network
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${EMPTY}
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${WORD}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${NUMBER}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${POINTS}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${ONE_WORD}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${ONE_NUMBER}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${ONE_POINTS}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${ELEVEN_WORD}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${ELEVEN_NUMBER}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${ELEVEN_POINTS}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${EXCEEDED}	Error: ipv4_tunnel: Validation error.

	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${EMPTY}/${MAX_NETMASK}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${WORD}/${MAX_NETMASK}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${NUMBER}/${MAX_NETMASK}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${POINTS}/${MAX_NETMASK}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${ONE_WORD}/${MAX_NETMASK}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${ONE_NUMBER}/${MAX_NETMASK}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${ONE_POINTS}/${MAX_NETMASK}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${ELEVEN_WORD}/${MAX_NETMASK}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${ELEVEN_NUMBER}/${MAX_NETMASK}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${ELEVEN_POINTS}/${MAX_NETMASK}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${EXCEEDED}/${MAX_NETMASK}	Error: ipv4_tunnel: Validation error.

	FOR	${INVALID_NETMASK}	IN	@{INVALID_NETMASK}
		CLI:Test Set Validate Invalid Options	ipv4_tunnel	/${INVALID_NETMASK}	Error: ipv4_tunnel: Validation error.
		CLI:Test Set Validate Invalid Options	ipv4_tunnel	${IPV4_TUNNEL}/${INVALID_NETMASK}	Error: ipv4_tunnel: Validation error.

		CLI:Test Set Validate Invalid Options	ipv4_tunnel	${EMPTY}/${INVALID_NETMASK}	Error: ipv4_tunnel: Validation error.
		CLI:Test Set Validate Invalid Options	ipv4_tunnel	${WORD}/${INVALID_NETMASK}	Error: ipv4_tunnel: Validation error.
		CLI:Test Set Validate Invalid Options	ipv4_tunnel	${NUMBER}/${INVALID_NETMASK}	Error: ipv4_tunnel: Validation error.
		CLI:Test Set Validate Invalid Options	ipv4_tunnel	${POINTS}/${INVALID_NETMASK}	Error: ipv4_tunnel: Validation error.
		CLI:Test Set Validate Invalid Options	ipv4_tunnel	${ONE_WORD}/${INVALID_NETMASK}	Error: ipv4_tunnel: Validation error.
		CLI:Test Set Validate Invalid Options	ipv4_tunnel	${ONE_NUMBER}/${INVALID_NETMASK}	Error: ipv4_tunnel: Validation error.
		CLI:Test Set Validate Invalid Options	ipv4_tunnel	${ONE_POINTS}/${INVALID_NETMASK}	Error: ipv4_tunnel: Validation error.
		CLI:Test Set Validate Invalid Options	ipv4_tunnel	${ELEVEN_WORD}/${INVALID_NETMASK}	Error: ipv4_tunnel: Validation error.
		CLI:Test Set Validate Invalid Options	ipv4_tunnel	${ELEVEN_NUMBER}/${INVALID_NETMASK}	Error: ipv4_tunnel: Validation error.
		CLI:Test Set Validate Invalid Options	ipv4_tunnel	${ELEVEN_POINTS}/${INVALID_NETMASK}	Error: ipv4_tunnel: Validation error.
		CLI:Test Set Validate Invalid Options	ipv4_tunnel	${EXCEEDED}/${INVALID_NETMASK}	Error: ipv4_tunnel: Validation error.
	END

	FOR	${INVALID_IP}	IN	@{INVALID_IP}
		CLI:Test Set Validate Invalid Options	ipv4_tunnel	${INVALID_IP}	Error: ipv4_tunnel: Validation error.
		CLI:Test Set Validate Invalid Options	ipv4_tunnel	${INVALID_IP}/${MAX_NETMASK}	Error: ipv4_tunnel: Validation error.
	END
	CLI:Test Set Validate Normal Fields	ipv4_tunnel	${IPV4_TUNNEL}/${MAX_NETMASK}
	[Teardown]	CLI:Revert	Raw

Test with field=ip_addr equals Network for ipv6_tunnel
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set Field	ip_addr	network
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${EMPTY}
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${WORD}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${NUMBER}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${POINTS}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${ONE_WORD}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${ONE_NUMBER}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${ONE_POINTS}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${ELEVEN_WORD}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${ELEVEN_NUMBER}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${ELEVEN_POINTS}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${EXCEEDED}	Error: ipv6_tunnel: Validation error.

	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${EMPTY}/${MAX_BITMASK}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${WORD}/${MAX_BITMASK}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${NUMBER}/${MAX_BITMASK}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${POINTS}/${MAX_BITMASK}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${ONE_WORD}/${MAX_BITMASK}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${ONE_NUMBER}/${MAX_BITMASK}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${ONE_POINTS}/${MAX_BITMASK}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${ELEVEN_WORD}/${MAX_BITMASK}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${ELEVEN_NUMBER}/${MAX_BITMASK}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${ELEVEN_POINTS}/${MAX_BITMASK}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${EXCEEDED}/${MAX_BITMASK}	Error: ipv6_tunnel: Validation error.

	FOR	${INVALID_BITMASK}	IN	@{INVALID_BITMASK}
		CLI:Test Set Validate Invalid Options	ipv6_tunnel	/${INVALID_BITMASK}	Error: ipv6_tunnel: Validation error.
		CLI:Test Set Validate Invalid Options	ipv4_tunnel	${IPV6_TUNNEL}/${INVALID_BITMASK}	Error: ipv4_tunnel: Validation error.

		CLI:Test Set Validate Invalid Options	ipv6_tunnel	${EMPTY}/${INVALID_BITMASK}	Error: ipv6_tunnel: Validation error.
		CLI:Test Set Validate Invalid Options	ipv6_tunnel	${WORD}/${INVALID_BITMASK}	Error: ipv6_tunnel: Validation error.
		CLI:Test Set Validate Invalid Options	ipv6_tunnel	${NUMBER}/${INVALID_BITMASK}	Error: ipv6_tunnel: Validation error.
		CLI:Test Set Validate Invalid Options	ipv6_tunnel	${POINTS}/${INVALID_BITMASK}	Error: ipv6_tunnel: Validation error.
		CLI:Test Set Validate Invalid Options	ipv6_tunnel	${ONE_WORD}/${INVALID_BITMASK}	Error: ipv6_tunnel: Validation error.
		CLI:Test Set Validate Invalid Options	ipv6_tunnel	${ONE_NUMBER}/${INVALID_BITMASK}	Error: ipv6_tunnel: Validation error.
		CLI:Test Set Validate Invalid Options	ipv6_tunnel	${ONE_POINTS}/${INVALID_BITMASK}	Error: ipv6_tunnel: Validation error.
		CLI:Test Set Validate Invalid Options	ipv6_tunnel	${ELEVEN_WORD}/${INVALID_BITMASK}	Error: ipv6_tunnel: Validation error.
		CLI:Test Set Validate Invalid Options	ipv6_tunnel	${ELEVEN_NUMBER}/${INVALID_BITMASK}	Error: ipv6_tunnel: Validation error.
		CLI:Test Set Validate Invalid Options	ipv6_tunnel	${ELEVEN_POINTS}/${INVALID_BITMASK}	Error: ipv6_tunnel: Validation error.
		CLI:Test Set Validate Invalid Options	ipv6_tunnel	${EXCEEDED}/${INVALID_BITMASK}	Error: ipv6_tunnel: Validation error.
	END

	FOR	${INVALID_IPV6}	IN	@{INVALID_IPV6}
		CLI:Test Set Validate Invalid Options	ipv6_tunnel	${INVALID_IPV6}	Error: ipv6_tunnel: Validation error.
		CLI:Test Set Validate Invalid Options	ipv6_tunnel	${INVALID_IPV6}/${MAX_NETMASK}	Error: ipv6_tunnel: Validation error.
	END
	CLI:Test Set Validate Normal Fields	ipv6_tunnel	${IPV6_TUNNEL}/${MAX_BITMASK}
	[Teardown]	CLI:Revert	Raw

Test with field=ip_addr equals p2p for local_endpoint
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set Field	ip_addr	p2p
	CLI:Test Set Validate Invalid Options	local_endpoint	${EMPTY}
	CLI:Test Set Validate Invalid Options	local_endpoint	${WORD}	Error: local_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_endpoint	${NUMBER}	Error: local_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_endpoint	${POINTS}	Error: local_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_endpoint	${ONE_WORD}	Error: local_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_endpoint	${ONE_NUMBER}	Error: local_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_endpoint	${ONE_POINTS}	Error: local_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_endpoint	${ELEVEN_WORD}	Error: local_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_endpoint	${ELEVEN_NUMBER}	Error: local_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_endpoint	${ELEVEN_POINTS}	Error: local_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_endpoint	${EXCEEDED}	Error: local_endpoint: Invalid IP address.
	FOR	${INVALID_IP}	IN	@{INVALID_IP}
		CLI:Test Set Validate Invalid Options	local_endpoint	${INVALID_IP}	Error: local_endpoint: Invalid IP address.
	END
	CLI:Test Set Validate Normal Fields	local_endpoint	${LOCAL_ENDPOINT}
	[Teardown]	CLI:Revert	Raw

Test with field=ip_addr equals p2p for remote_endpoint
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set Field	ip_addr	p2p
	CLI:Test Set Validate Invalid Options	remote_endpoint	${EMPTY}
	CLI:Test Set Validate Invalid Options	remote_endpoint	${WORD}	Error: remote_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_endpoint	${NUMBER}	Error: remote_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_endpoint	${POINTS}	Error: remote_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_endpoint	${ONE_WORD}	Error: remote_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_endpoint	${ONE_NUMBER}	Error: remote_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_endpoint	${ONE_POINTS}	Error: remote_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_endpoint	${ELEVEN_WORD}	Error: remote_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_endpoint	${ELEVEN_NUMBER}	Error: remote_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_endpoint	${ELEVEN_POINTS}	Error: remote_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_endpoint	${EXCEEDED}	Error: remote_endpoint: Invalid IP address.
	FOR	${INVALID_IP}	IN	@{INVALID_IP}
		CLI:Test Set Validate Invalid Options	remote_endpoint	${INVALID_IP}	Error: remote_endpoint: Invalid IP address.
	END
	CLI:Test Set Validate Normal Fields	remote_endpoint	${REMOTE_ENDPOINT}
	[Teardown]	CLI:Revert	Raw

Test with field=ip_addr equals p2p_ipv6 for local_ipv6_endpoint
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set Field	ip_addr	p2p-ipv6
	CLI:Test Set Validate Invalid Options	local_ipv6_endpoint	${EMPTY}
	CLI:Test Set Validate Invalid Options	local_ipv6_endpoint	${WORD}	Error: local_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_ipv6_endpoint	${NUMBER}	Error: local_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_ipv6_endpoint	${POINTS}	Error: local_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_ipv6_endpoint	${ONE_WORD}	Error: local_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_ipv6_endpoint	${ONE_NUMBER}	Error: local_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_ipv6_endpoint	${ONE_POINTS}	Error: local_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_ipv6_endpoint	${ELEVEN_WORD}	Error: local_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_ipv6_endpoint	${ELEVEN_NUMBER}	Error: local_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_ipv6_endpoint	${ELEVEN_POINTS}	Error: local_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_ipv6_endpoint	${EXCEEDED}	Error: local_ipv6_endpoint: Invalid IP address.
	FOR	${INVALID_IPV6}	IN	@{INVALID_IPV6}
		CLI:Test Set Validate Invalid Options	local_ipv6_endpoint	${INVALID_IPV6}	Error: local_ipv6_endpoint: Invalid IP address.
	END
	CLI:Test Set Validate Normal Fields	local_ipv6_endpoint	@{LOCAL_IPV6_ENDPOINT}

Test with field=ip_addr equals p2p_ipv6 for remote_ipv6_endpoint
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set Field	ip_addr	p2p-ipv6
	CLI:Test Set Validate Invalid Options	remote_ipv6_endpoint	${EMPTY}
	CLI:Test Set Validate Invalid Options	remote_ipv6_endpoint	${WORD}	Error: remote_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_ipv6_endpoint	${NUMBER}	Error: remote_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_ipv6_endpoint	${POINTS}	Error: remote_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_ipv6_endpoint	${ONE_WORD}	Error: remote_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_ipv6_endpoint	${ONE_NUMBER}	Error: remote_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_ipv6_endpoint	${ONE_POINTS}	Error: remote_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_ipv6_endpoint	${ELEVEN_WORD}	Error: remote_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_ipv6_endpoint	${ELEVEN_NUMBER}	Error: remote_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_ipv6_endpoint	${ELEVEN_POINTS}	Error: remote_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_ipv6_endpoint	${EXCEEDED}	Error: remote_ipv6_endpoint: Invalid IP address.
	FOR	${INVALID_IPV6}	IN	@{INVALID_IPV6}
		CLI:Test Set Validate Invalid Options	remote_ipv6_endpoint	${INVALID_IPV6}	Error: remote_ipv6_endpoint: Invalid IP address.
	END
	CLI:Test Set Validate Normal Fields	remote_ipv6_endpoint	@{REMOTE_IPV6_ENDPOINT}
	[Teardown]	CLI:Revert	Raw

Test with field=authentication_method
	CLI:Enter Path	/settings/ssl_vpn/server/
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	authentication_method	${WORD}	Error: Invalid value: ${WORD} for parameter: authentication_method
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	authentication_method	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	authentication_method	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: authentication_method
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	authentication_method	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	authentication_method	${POINTS}	Error: Invalid value: ${POINTS} for parameter: authentication_method
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	authentication_method	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	authentication_method	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: authentication_method
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	authentication_method	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	authentication_method	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: authentication_method
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	authentication_method	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	authentication_method	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: authentication_method
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	authentication_method	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	authentication_method	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD} for parameter: authentication_method
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	authentication_method	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	authentication_method	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER} for parameter: authentication_method
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	authentication_method	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	authentication_method	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS} for parameter: authentication_method
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	authentication_method	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	authentication_method	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: authentication_method
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	authentication_method	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	#Gets Error: authentication_method: Network Server requires TLS authentication when trying to change it if ip_addr = net
	CLI:Set Field  ip_addr  p2p
	CLI:Test Set Validate Normal Fields	authentication_method	tls	static_key	password	password_plus_tls
	[Teardown]	CLI:Revert	Raw

################ IP_ADDR NEED TLS AUTHENTICATION ################

Test message about ip_addr need TLS authentication for authentication=tls
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set Field	ip_addr	network
	CLI:Test Set Validate Invalid Options	authentication_method	tls	${EMPTY}
	[Teardown]	CLI:Revert	Raw

Test message about ip_addr need TLS authentication for authentication=tls and changing ip_addr=network after ip_addr=p2p
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set Field	ip_addr	p2p
	CLI:Test Set Validate Invalid Options	authentication_method	tls	${EMPTY}
	[Teardown]	CLI:Revert	Raw

Test message about ip_addr need TLS authentication for authentication=tls and changing ip_addr=network after ip_addr=p2p-ipv6
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set Field	ip_addr	p2p-ipv6
	CLI:Test Set Validate Invalid Options	authentication_method	tls	${EMPTY}
	[Teardown]	CLI:Revert	Raw

################ STATIC_KEY ################

Test message about ip_addr need TLS authentication for authentication=static_key
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set Field	ip_addr	network
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	authentication_method	static_key	Error: authentication_method: Network Server requires TLS authentication
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	authentication_method	static_key	${EMPTY}
	[Teardown]	CLI:Revert	Raw

Test message about ip_addr need TLS authentication for authentication=static_key and changing ip_addr=network after ip_addr=p2p
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	ip_addr=p2p authentication_method=static_key
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ip_addr	network	Error: authentication_method: Network Server requires TLS authentication
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ip_addr	network	${EMPTY}
	[Teardown]	CLI:Revert	Raw

Test message about ip_addr need TLS authentication for authentication=static_key and changing ip_addr=network after ip_addr=p2p-ipv6
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	ip_addr=p2p-ipv6 authentication_method=static_key
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ip_addr	network	Error: authentication_method: Network Server requires TLS authentication
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ip_addr	network	${EMPTY}
	[Teardown]	CLI:Revert	Raw

################ PASSWORD ################

Test message about ip_addr need TLS authentication for authentication=password
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set Field	ip_addr	network
	CLI:Test Set Validate Invalid Options	authentication_method	password	${EMPTY}
	[Teardown]	CLI:Revert	Raw

Test message about ip_addr need TLS authentication for authentication=password and changing ip_addr=network after ip_addr=p2p
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	ip_addr=p2p authentication_method=password
	CLI:Test Set Validate Invalid Options	ip_addr	network	${EMPTY}
	[Teardown]	CLI:Revert	Raw

Test message about ip_addr need TLS authentication for authentication=password and changing ip_addr=network after ip_addr=p2p-ipv6
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	ip_addr=p2p-ipv6 authentication_method=password
	CLI:Test Set Validate Invalid Options	ip_addr	network	${EMPTY}
	[Teardown]	CLI:Revert	Raw

################ PASSWORD_PLUS_TLS ################

Test message about ip_addr need TLS authentication for authentication=password_plus_tls
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set Field	ip_addr	network
	CLI:Test Set Validate Invalid Options	authentication_method	password_plus_tls	${EMPTY}
	[Teardown]	CLI:Revert	Raw

Test message about ip_addr need TLS authentication for authentication=password_plus_tls and changing ip_addr=network after ip_addr=p2p
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	ip_addr=p2p authentication_method=password_plus_tls
	CLI:Test Set Validate Invalid Options	ip_addr	network	${EMPTY}
	[Teardown]	CLI:Revert	Raw

Test message about ip_addr need TLS authentication for authentication=password_plus_tls and changing ip_addr=network after ip_addr=p2p-ipv6
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	ip_addr=p2p-ipv6 authentication_method=password_plus_tls
	CLI:Test Set Validate Invalid Options	ip_addr	network	${EMPTY}
	[Teardown]	CLI:Revert	Raw

################ TLS ################

Test with authentication_method=tls ca_certificate
	Run Keyword If	${NGVERSION} >= 4.2	Set Tags	NON-CRITICAL	BUG_NG_7146
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set Field	authentication_method	tls
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${EMPTY}
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${EMPTY}	Error: Missing value: ca_certificate

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${WORD}	Error: Invalid value: ${WORD} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${POINTS}	Error: Invalid value: ${POINTS} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	ca_certificate
	[Teardown]	CLI:Revert	Raw

Test with authentication_method=tls server_certificate
	Run Keyword If	${NGVERSION} >= 4.2	Set Tags	NON-CRITICAL	BUG_NG_7146
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set Field	authentication_method	tls
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${EMPTY}
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${EMPTY}	Error: Missing value: server_certificate

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${WORD}	Error: Invalid value: ${WORD} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${POINTS}	Error: Invalid value: ${POINTS} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	server_certificate
	[Teardown]	CLI:Revert	Raw

Test with authentication_method=tls server_key
	Run Keyword If	${NGVERSION} >= 4.2	Set Tags	NON-CRITICAL	BUG_NG_7146
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set Field	authentication_method	tls
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${EMPTY}
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${EMPTY}	Error: Missing value: server_key

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${WORD}	Error: Invalid value: ${WORD} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${POINTS}	Error: Invalid value: ${POINTS} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	server_key
	[Teardown]	CLI:Revert	Raw

Test with authentication_method=tls diffie_hellman
	Run Keyword If	${NGVERSION} >= 4.2	Set Tags	NON-CRITICAL	BUG_NG_7146
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set Field	authentication_method	tls
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${EMPTY}
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${EMPTY}	Error: Missing value: diffie_hellman

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${WORD}	Error: Invalid value: ${WORD} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${POINTS}	Error: Invalid value: ${POINTS} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	diffie_hellman
	[Teardown]	CLI:Revert	Raw

################ STATIC_KEY ################

Test with authentication_method=static_key secret
	Run Keyword If	${NGVERSION} >= 4.2	Set Tags	NON-CRITICAL	BUG_NG_7146
	CLI:Enter Path	/settings/ssl_vpn/server/
	#This is because ip_addr=network need authentication=tls
	CLI:Set	ip_addr=p2p authentication_method=static_key
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	secret	${EMPTY}
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	secret	${EMPTY}	Error: Missing value: secret

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	secret	${WORD}	Error: Invalid value: ${WORD} for parameter: secret
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	secret	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	secret	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: secret
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	secret	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	secret	${POINTS}	Error: Invalid value: ${POINTS} for parameter: secret
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	secret	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	secret	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: secret
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	secret	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	secret	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: secret
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	secret	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	secret	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: secret
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	secret	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	secret	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD} for parameter: secret
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	secret	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	secret	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER} for parameter: secret
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	secret	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	secret	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS} for parameter: secret
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	secret	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	secret	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: secret
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	secret	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	secret
	[Teardown]	CLI:Revert	Raw

Test with authentication_method=static_key diffie_hellman
	Run Keyword If	${NGVERSION} >= 4.2	Set Tags	NON-CRITICAL	BUG_NG_7146
	CLI:Enter Path	/settings/ssl_vpn/server/
	#This is because ip_addr=network need authentication=tls
	CLI:Set	ip_addr=p2p authentication_method=static_key
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${EMPTY}
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${EMPTY}	Error: Missing value: diffie_hellman

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${WORD}	Error: Invalid value: ${WORD} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${POINTS}	Error: Invalid value: ${POINTS} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	diffie_hellman
	[Teardown]	CLI:Revert	Raw

################ PASSWORD ################

Test with authentication_method=password ca_certificate
	Run Keyword If	${NGVERSION} >= 4.2	Set Tags	NON-CRITICAL	BUG_NG_7146
	CLI:Enter Path	/settings/ssl_vpn/server/
	#This is because ip_addr=network need authentication=tls
	CLI:Set	ip_addr=p2p authentication_method=password
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${EMPTY}
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${EMPTY}	Error: Missing value: ca_certificate

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${WORD}	Error: Invalid value: ${WORD} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${POINTS}	Error: Invalid value: ${POINTS} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	ca_certificate
	[Teardown]	CLI:Revert	Raw

Test with authentication_method=password server_certificate
	Run Keyword If	${NGVERSION} >= 4.2	Set Tags	NON-CRITICAL	BUG_NG_7146
	CLI:Enter Path	/settings/ssl_vpn/server/
	#This is because ip_addr=network need authentication=tls
	CLI:Set	ip_addr=p2p authentication_method=password
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${EMPTY}
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${EMPTY}	Error: Missing value: server_certificate

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${WORD}	Error: Invalid value: ${WORD} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${POINTS}	Error: Invalid value: ${POINTS} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	server_certificate
	[Teardown]	CLI:Revert	Raw

Test with authentication_method=password server_key
	Run Keyword If	${NGVERSION} >= 4.2	Set Tags	NON-CRITICAL	BUG_NG_7146
	CLI:Enter Path	/settings/ssl_vpn/server/
	#This is because ip_addr=network need authentication=tls
	CLI:Set	ip_addr=p2p authentication_method=password
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${EMPTY}
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${EMPTY}	Error: Missing value: server_key

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${WORD}	Error: Invalid value: ${WORD} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${POINTS}	Error: Invalid value: ${POINTS} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	server_key
	[Teardown]	CLI:Revert	Raw

Test with authentication_method=password diffie_hellman
	Run Keyword If	${NGVERSION} >= 4.2	Set Tags	NON-CRITICAL	BUG_NG_7146
	CLI:Enter Path	/settings/ssl_vpn/server/
	#This is because ip_addr=network need authentication=tls
	CLI:Set	ip_addr=p2p authentication_method=password
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${EMPTY}
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${EMPTY}	Error: Missing value: diffie_hellman

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${WORD}	Error: Invalid value: ${WORD} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${POINTS}	Error: Invalid value: ${POINTS} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	diffie_hellman
	[Teardown]	CLI:Revert	Raw

################ PASSWORD ################

Test with authentication_method=password_plus_tls ca_certificate
	Run Keyword If	${NGVERSION} >= 4.2	Set Tags	NON-CRITICAL	BUG_NG_7146
	CLI:Enter Path	/settings/ssl_vpn/server/
	#This is because ip_addr=network need authentication=tls
	CLI:Set	ip_addr=p2p authentication_method=password_plus_tls
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${EMPTY}
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${EMPTY}	Error: Missing value: ca_certificate

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${WORD}	Error: Invalid value: ${WORD} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${POINTS}	Error: Invalid value: ${POINTS} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	ca_certificate	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: ca_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	ca_certificate	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	ca_certificate
	[Teardown]	CLI:Revert	Raw

Test with authentication_method=password_plus_tls server_certificate
	Run Keyword If	${NGVERSION} >= 4.2	Set Tags	NON-CRITICAL	BUG_NG_7146
	CLI:Enter Path	/settings/ssl_vpn/server/
	#This is because ip_addr=network need authentication=tls
	CLI:Set	ip_addr=p2p authentication_method=password_plus_tls
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${EMPTY}
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${EMPTY}	Error: Missing value: server_certificate

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${WORD}	Error: Invalid value: ${WORD} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${POINTS}	Error: Invalid value: ${POINTS} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_certificate	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: server_certificate
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_certificate	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	server_certificate
	[Teardown]	CLI:Revert	Raw

Test with authentication_method=password_plus_tls server_key
	Run Keyword If	${NGVERSION} >= 4.2	Set Tags	NON-CRITICAL	BUG_NG_7146
	CLI:Enter Path	/settings/ssl_vpn/server/
	#This is because ip_addr=network need authentication=tls
	CLI:Set	ip_addr=p2p authentication_method=password_plus_tls
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${EMPTY}
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${EMPTY}	Error: Missing value: server_key

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${WORD}	Error: Invalid value: ${WORD} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${POINTS}	Error: Invalid value: ${POINTS} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	server_key	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: server_key
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	server_key	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	server_key
	[Teardown]	CLI:Revert	Raw

Test with authentication_method=password_plus_tls diffie_hellman
	Run Keyword If	${NGVERSION} >= 4.2	Set Tags	NON-CRITICAL	BUG_NG_7146
	CLI:Enter Path	/settings/ssl_vpn/server/
	#This is because ip_addr=network need authentication=tls
	CLI:Set	ip_addr=p2p authentication_method=password_plus_tls
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${EMPTY}
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${EMPTY}	Error: Missing value: diffie_hellman

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${WORD}	Error: Invalid value: ${WORD} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${POINTS}	Error: Invalid value: ${POINTS} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: diffie_hellman
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	diffie_hellman	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	diffie_hellman
	[Teardown]	CLI:Revert	Raw

################################

Test with field=hmac|message_digest
	CLI:Enter Path	/settings/ssl_vpn/server/
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	hmac|message_digest	${EMPTY}	Error: Missing value for parameter: hmac|message_digest
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	hmac|message_digest	${EMPTY}	Error: Missing value: hmac|message_digest

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	hmac|message_digest	${WORD}	Error: Invalid value: ${WORD} for parameter: hmac|message_digest
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	hmac|message_digest	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	hmac|message_digest	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: hmac|message_digest
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	hmac|message_digest	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	hmac|message_digest	${POINTS}	Error: Invalid value: ${POINTS} for parameter: hmac|message_digest
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	hmac|message_digest	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	hmac|message_digest	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: hmac|message_digest
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	hmac|message_digest	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	hmac|message_digest	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: hmac|message_digest
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	hmac|message_digest	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	hmac|message_digest	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: hmac|message_digest
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	hmac|message_digest	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	hmac|message_digest	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD} for parameter: hmac|message_digest
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	hmac|message_digest	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	hmac|message_digest	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER} for parameter: hmac|message_digest
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	hmac|message_digest	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	hmac|message_digest	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS} for parameter: hmac|message_digest
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	hmac|message_digest	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	hmac|message_digest	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: hmac|message_digest
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	hmac|message_digest	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}

	Run Keyword If	${NGVERSION} <= 4.0	CLI:Test Set Validate Normal Fields	hmac|message_digest	DSA	RIPEMD160	RSA-SHA1-2	SHA224
	...	DSA-SHA	RSA-MD4	RSA-SHA224	SHA256	DSA-SHA1	RSA-MD5	RSA-SHA256	SHA384
	...	DSA-SHA1-old	RSA-MDC2	RSA-SHA384	SHA512	MD4	RSA-RIPEMD160	RSA-SHA512
	...	ecdsa-with-SHA1	MD5	RSA-SHA	SHA	whirlpool	MDC2	RSA-SHA1	 SHA1
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Normal Fields	hmac|message_digest	BLAKE2b512	RSA-MD5	RSA-SHA512	SHA3-256
	...	SHAKE256	BLAKE2s256	RSA-SHA512|224	SHA3-384	MD4	RSA-RIPEMD160
	...	RSA-SHA512|256	SHA3-512	id-rsassa-pkcs1-v1_5-with-sha3-224	MD5	RSA-SHA1
	...	SHA384	id-rsassa-pkcs1-v1_5-with-sha3-256	MD5-SHA1	RSA-SHA1-2	SHA1	SHA512
	...	id-rsassa-pkcs1-v1_5-with-sha3-384	RSA-SHA224	SHA224	SHA512-224	id-rsassa-pkcs1-v1_5-with-sha3-512
	...	RIPEMD160	RSA-SHA256	SHA256	SHA512-256	whirlpool	RSA-MD4	RSA-SHA384	SHA3-224	SHAKE128
	[Teardown]	CLI:Revert	Raw

Test with field=cipher
	CLI:Enter Path	/settings/ssl_vpn/server/
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	cipher	${EMPTY}	Error: Missing value for parameter: cipher
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	cipher	${EMPTY}	Error: Missing value: cipher

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	cipher	${WORD}	Error: Invalid value: ${WORD} for parameter: cipher
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	cipher	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	cipher	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: cipher
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	cipher	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	cipher	${POINTS}	Error: Invalid value: ${POINTS} for parameter: cipher
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	cipher	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	cipher	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: cipher
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	cipher	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	cipher	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: cipher
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	cipher	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	cipher	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: cipher
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	cipher	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	cipher	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD} for parameter: cipher
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	cipher	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	cipher	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER} for parameter: cipher
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	cipher	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	cipher	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS} for parameter: cipher
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	cipher	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	cipher	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: cipher
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	cipher	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}

	Run Keyword If	${NGVERSION} <= 4.0	CLI:Test Set Validate Normal Fields	cipher	AES-128-CBC	AES-192-CFB1	AES-256-OFB
	...	CAMELLIA-128-CFB8  CAMELLIA-256-CBC   CAST5-OFB	  DES-EDE-OFB	DESX-CBC	RC2-CFB
	...	AES-128-CFB	AES-192-CFB8	BF-CBC	CAMELLIA-128-OFB	CAMELLIA-256-CFB	DES-CBC	DES-EDE3-CBC	IDEA-CBC	RC2-OFB
	...	AES-128-CFB1	AES-192-OFB	BF-CFB	CAMELLIA-192-CBC	CAMELLIA-256-CFB1	DES-CFB	DES-EDE3-CFB	IDEA-CFB	SEED-CBC
	...	AES-128-CFB8	AES-256-CBC	BF-OFB	CAMELLIA-192-CFB	CAMELLIA-256-CFB8	DES-CFB1	DES-EDE3-CFB1	IDEA-OFB	SEED-CFB
	...	AES-128-OFB	AES-256-CFB1	CAMELLIA-128-CBC	CAMELLIA-192-CFB1	CAMELLIA-256-OFB	DES-CFB8	DES-EDE3-CFB8	RC2-40-CBC	SEED-OFB
	...	AES-192-CBC	AES-256-CFB1	CAMELLIA-128-CFB	CAMELLIA-192-CFB8	CAST5-CBC	DES-EDE-CBC	DES-EDE3-OFB	RC2-64-CBC
	...	AES-192-CFB	AES-256-CFB8	CAMELLIA-128-CFB1	CAMELLIA-192-OFB	CAST5-CFB	DES-EDE-CFB	DES-OFB	 RC2-CBC
	Run Keyword If	${NGVERSION} >= 4.2 and ${NGVERSION} <= 5.2	CLI:Test Set Validate Normal Fields	cipher	AES-128-CBC	AES-192-CFB8	ARIA-128-CBC
	...	ARIA-192-CFB8	BF-CBC   CAST5-CBC	  DES-EDE-OFB
	...	AES-128-CFB	AES-192-GCM	ARIA-128-CFB	ARIA-192-GCM	BF-CFB  CAST5-CFB	  DES-EDE3-CBC
	...	AES-128-CFB1	AES-192-OFB	ARIA-128-CFB1	ARIA-192-OFB	BF-OFB	CAST5-OFB	  DES-EDE3-CFB	RC2-40-CBC
	...	AES-128-CFB8	AES-256-CBC	ARIA-128-CFB8	ARIA-256-CBC   DES-CBC	 DES-EDE3-CFB1	RC2-64-CBC
	...	AES-128-GCM	AES-256-CFB	ARIA-128-GCM	ARIA-256-CFB   DES-CFB	 DES-EDE3-CFB8	RC2-CBC	 The
	...	AES-128-OFB	AES-256-CFB1	ARIA-128-OFB	ARIA-256-CFB1   DES-CFB1	DES-EDE3-OFB	RC2-CFB	 and
	...	AES-192-CBC	AES-256-CFB8	ARIA-192-CBC	ARIA-256-CFB8  DES-CFB8	DES-OFB	 RC2-OFB
	...	AES-192-CFB	AES-256-GCM	ARIA-192-CFB	ARIA-256-GCM	DES-EDE-CBC	DESX-CBC
	...	AES-192-CFB1	AES-256-OFB	ARIA-192-CFB1	ARIA-256-OFB	DES-EDE-CFB
	Run Keyword If	${NGVERSION} >= 5.4	CLI:Test Set Validate Normal Fields	cipher	AES-128-CBC        AES-256-CFB8       ARIA-256-CFB1      CAMELLIA-192-OFB   DES-EDE-CFB        RC2-CFB
	...	AES-128-CFB        AES-256-GCM        ARIA-256-CFB8      CAMELLIA-256-CBC   DES-EDE-OFB        RC2-OFB
	...	AES-128-CFB1       AES-256-OFB        ARIA-256-OFB       CAMELLIA-256-CFB   DES-EDE3-CBC       SEED-CBC
	...	AES-128-CFB8       ARIA-128-CBC       BF-CBC             CAMELLIA-256-CFB1  DES-EDE3-CFB       SEED-CFB
	...	AES-128-GCM        ARIA-128-CFB       BF-CFB             CAMELLIA-256-CFB8  DES-EDE3-CFB1      SEED-OFB
	...	AES-128-OFB        ARIA-128-CFB1      BF-OFB             CAMELLIA-256-OFB   DES-EDE3-CFB8      SM4-CBC
	...	AES-192-CBC        ARIA-128-CFB8      CAMELLIA-128-CBC   CAST5-CBC          DES-EDE3-OFB       SM4-CFB
	...	AES-192-CFB        ARIA-128-OFB       CAMELLIA-128-CFB   CAST5-CFB          DES-OFB            SM4-OFB
	...	AES-192-CFB1       ARIA-192-CBC       CAMELLIA-128-CFB1  CAST5-OFB          DESX-CBC           The
	...	AES-192-CFB8       ARIA-192-CFB       CAMELLIA-128-CFB8  CHACHA20-POLY1305  IDEA-CBC           and
	...	AES-192-GCM        ARIA-192-CFB1      CAMELLIA-128-OFB   DES-CBC            IDEA-CFB
	...	AES-192-OFB        ARIA-192-CFB8      CAMELLIA-192-CBC   DES-CFB            IDEA-OFB
	...	AES-256-CBC        ARIA-192-OFB       CAMELLIA-192-CFB   DES-CFB1           RC2-40-CBC
	...	AES-256-CFB        ARIA-256-CBC       CAMELLIA-192-CFB1  DES-CFB8           RC2-64-CBC
	...	AES-256-CFB1       ARIA-256-CFB       CAMELLIA-192-CFB8  DES-EDE-CBC        RC2-CBC
	[Teardown]	CLI:Revert	Raw

Test with field=min_tls_version
	CLI:Enter Path	/settings/ssl_vpn/server/
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	min_tls_version	${EMPTY}	Error: Missing value for parameter: min_tls_version
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	min_tls_version	${EMPTY}	Error: Missing value: min_tls_version

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	min_tls_version	${WORD}	Error: Invalid value: ${WORD} for parameter: min_tls_version
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	min_tls_version	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	min_tls_version	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: min_tls_version
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	min_tls_version	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	min_tls_version	${POINTS}	Error: Invalid value: ${POINTS} for parameter: min_tls_version
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	min_tls_version	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	min_tls_version	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: min_tls_version
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	min_tls_version	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	min_tls_version	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: min_tls_version
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	min_tls_version	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	min_tls_version	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: min_tls_version
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	min_tls_version	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	min_tls_version	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD} for parameter: min_tls_version
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	min_tls_version	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	min_tls_version	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER} for parameter: min_tls_version
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	min_tls_version	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	min_tls_version	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS} for parameter: min_tls_version
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	min_tls_version	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	min_tls_version	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: min_tls_version
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	min_tls_version	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	min_tls_version	1.0	1.1	1.2	none
	[Teardown]	CLI:Revert	Raw

Test with field=use_lzo_data_compress_algorithm
	CLI:Enter Path	/settings/ssl_vpn/server/
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${EMPTY}	Error: Missing value for parameter: use_lzo_data_compress_algorithm
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${EMPTY}	Error: Missing value: use_lzo_data_compress_algorithm

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${WORD}	Error: Invalid value: ${WORD} for parameter: use_lzo_data_compress_algorithm
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: use_lzo_data_compress_algorithm
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${POINTS}	Error: Invalid value: ${POINTS} for parameter: use_lzo_data_compress_algorithm
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: use_lzo_data_compress_algorithm
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: use_lzo_data_compress_algorithm
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: use_lzo_data_compress_algorithm
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD} for parameter: use_lzo_data_compress_algorithm
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER} for parameter: use_lzo_data_compress_algorithm
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS} for parameter: use_lzo_data_compress_algorithm
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: use_lzo_data_compress_algorithm
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	use_lzo_data_compress_algorithm
	[Teardown]	CLI:Revert	Raw

Test with field=redirect_gateway
	CLI:Enter Path	/settings/ssl_vpn/server/
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	redirect_gateway	${EMPTY}	Error: Missing value for parameter: redirect_gateway
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	redirect_gateway	${EMPTY}	Error: Missing value: redirect_gateway

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	redirect_gateway	${WORD}	Error: Invalid value: ${WORD} for parameter: redirect_gateway
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	redirect_gateway	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	redirect_gateway	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: redirect_gateway
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	redirect_gateway	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	redirect_gateway	${POINTS}	Error: Invalid value: ${POINTS} for parameter: redirect_gateway
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	redirect_gateway	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	redirect_gateway	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: redirect_gateway
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	redirect_gateway	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	redirect_gateway	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: redirect_gateway
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	redirect_gateway	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	redirect_gateway	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: redirect_gateway
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	redirect_gateway	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	redirect_gateway	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD} for parameter: redirect_gateway
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	redirect_gateway	${ELEVEN_WORD}	Error: Invalid value: ${ELEVEN_WORD}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	redirect_gateway	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER} for parameter: redirect_gateway
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	redirect_gateway	${ELEVEN_NUMBER}	Error: Invalid value: ${ELEVEN_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	redirect_gateway	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS} for parameter: redirect_gateway
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	redirect_gateway	${ELEVEN_POINTS}	Error: Invalid value: ${ELEVEN_POINTS}

	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Set Validate Invalid Options	redirect_gateway	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: redirect_gateway
	Run Keyword If	${NGVERSION} == 3.2	CLI:Test Set Validate Invalid Options	redirect_gateway	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Validate Normal Fields	redirect_gateway
	[Teardown]	CLI:Revert	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection
