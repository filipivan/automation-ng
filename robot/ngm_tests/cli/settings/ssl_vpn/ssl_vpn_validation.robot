*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > SSL VPN Settings... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	NON-CRITICAL	NEED-REVIEW
Default Tags	CLI	SSH
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${IP}	1.1.1.1

*** Test Cases ***
Test Enter ssl_vpn
	CLI:Enter Path	/settings/ssl_vpn
	CLI:Test Ls Command	client	server	server_status

Test show_settings
	Set Tags	SHOW_SETTINGS
	CLI:Enter Path	/settings/ssl_vpn/
	${OUTPUT}=	CLI:Show Settings
	Should Match Regexp	${OUTPUT}	/settings/ssl_vpn/server status=disabled
	Should Match Regexp	${OUTPUT}	/settings/ssl_vpn/server listen_port_number=1194
	Should Match Regexp	${OUTPUT}	/settings/ssl_vpn/server protocol=udp
	Should Match Regexp	${OUTPUT}	/settings/ssl_vpn/server tunnel_mtu=1500
	Should Match Regexp	${OUTPUT}	/settings/ssl_vpn/server number_of_concurrent_tunnels=256
	Should Match Regexp	${OUTPUT}	/settings/ssl_vpn/server authentication_method=tls
	Should Match Regexp	${OUTPUT}	/settings/ssl_vpn/server ip_addr=network
	Should Match Regexp	${OUTPUT}	/settings/ssl_vpn/server hmac|message_digest=SHA1
	Should Match Regexp	${OUTPUT}	/settings/ssl_vpn/server cipher=BF-CBC
	Should Match Regexp	${OUTPUT}	/settings/ssl_vpn/server min_tls_version=none
	Should Match Regexp	${OUTPUT}	/settings/ssl_vpn/server use_lzo_data_compress_algorithm=no
	Should Match Regexp	${OUTPUT}	/settings/ssl_vpn/server redirect_gateway=no

Test server available fields to set
	CLI:Enter Path	/settings/ssl_vpn/server
	CLI:Test Set Available Fields	status	listen_ip_address	authentication_method	min_tls_version	ca_certificate
	...	number_of_concurrent_tunnels	cipher	protocol	diffie_hellman	redirect_gateway	hmac|message_digest
	...	secret	ip_addr	server_certificate	server_key	tunnel_mtu	listen_port_number
	...	use_lzo_data_compress_algorithm

Test set command options for server page
	CLI:Test Set Field Options	status	enabled	disabled
	CLI:Test Field Options	authentication_method	password	password_plus_tls	static_key	tls
	CLI:Test Set Field Options	min_tls_version	1.0	1.1	1.2	none
	Run Keyword If	${NGVERSION} <= 4.0	CLI:Test Set Field Options	cipher	AES-128-CBC	AES-192-CFB1	AES-256-OFB
	...	CAMELLIA-128-CFB8	CAMELLIA-256-CBC	CAST5-OFB	DES-EDE-OFB	DESX-CBC	RC2-CFB
	...	AES-128-CFB	AES-192-CFB8	BF-CBC	CAMELLIA-128-OFB	CAMELLIA-256-CFB	DES-CBC	DES-EDE3-CBC	IDEA-CBC	RC2-OFB
	...	AES-128-CFB1	AES-192-OFB	BF-CFB	CAMELLIA-192-CBC	CAMELLIA-256-CFB1	DES-CFB	DES-EDE3-CFB	IDEA-CFB	SEED-CBC
	...	AES-128-CFB8	AES-256-CBC	BF-OFB	CAMELLIA-192-CFB	CAMELLIA-256-CFB8	DES-CFB1	DES-EDE3-CFB1	IDEA-OFB	SEED-CFB
	...	AES-128-OFB	AES-256-CFB1	CAMELLIA-128-CBC	CAMELLIA-192-CFB1	CAMELLIA-256-OFB	DES-CFB8	DES-EDE3-CFB8	RC2-40-CBC	SEED-OFB
	...	AES-192-CBC	AES-256-CFB1	CAMELLIA-128-CFB	CAMELLIA-192-CFB8	CAST5-CBC	DES-EDE-CBC	DES-EDE3-OFB	RC2-64-CBC
	...	AES-192-CFB	AES-256-CFB8	CAMELLIA-128-CFB1	CAMELLIA-192-OFB	CAST5-CFB	DES-EDE-CFB	DES-OFB	RC2-CBC

	Run Keyword If	${NGVERSION} > 4.0	CLI:Test Set Field Options	cipher	AES-128-CBC	AES-192-CFB8	ARIA-128-CBC
	...	ARIA-192-CFB8	BF-CBC	CAMELLIA-192-CFB	CAST5-CBC	DES-EDE-OFB	IDEA-CFB	SEED-OFB
	...	AES-128-CFB	AES-192-GCM	ARIA-128-CFB	ARIA-192-GCM	BF-CFB	CAMELLIA-192-CFB1	CAST5-CFB	DES-EDE3-CBC	IDEA-OFB	SM4-CBC
	...	AES-128-CFB1	AES-192-OFB	ARIA-128-CFB1	ARIA-192-OFB	BF-OFB	CAMELLIA-192-CFB8	CAST5-OFB	DES-EDE3-CFB	RC2-40-CBC	SM4-CFB
	...	AES-128-CFB8	AES-256-CBC	ARIA-128-CFB8	ARIA-256-CBC	CAMELLIA-128-CBC	CAMELLIA-192-OFB	DES-CBC	DES-EDE3-CFB1	RC2-64-CBC	SM4-OFB
	...	AES-128-GCM	AES-256-CFB	ARIA-128-GCM	ARIA-256-CFB	CAMELLIA-128-CFB	CAMELLIA-256-CBC	DES-CFB	DES-EDE3-CFB8	RC2-CBC	The
	...	AES-128-OFB	AES-256-CFB1	ARIA-128-OFB	ARIA-256-CFB1	CAMELLIA-128-CFB1	CAMELLIA-256-CFB	DES-CFB1	DES-EDE3-OFB	RC2-CFB	and
	...	AES-192-CBC	AES-256-CFB8	ARIA-192-CBC	ARIA-256-CFB8	CAMELLIA-128-CFB8	CAMELLIA-256-CFB1	DES-CFB8	DES-OFB	RC2-OFB
	...	AES-192-CFB	AES-256-GCM	ARIA-192-CFB	ARIA-256-GCM	CAMELLIA-128-OFB	CAMELLIA-256-CFB8	DES-EDE-CBC	DESX-CBC	SEED-CBC
	...	AES-192-CFB1	AES-256-OFB	ARIA-192-CFB1	ARIA-256-OFB	CAMELLIA-192-CBC	CAMELLIA-256-OFB	DES-EDE-CFB	IDEA-CBC	SEED-CFB

	CLI:Test Set Field Options	protocol	tcp	tcp-ipv6	udp	upd-ipv6
	CLI:Test Set Field Options	redirect_gateway	no	yes
	Run Keyword If	${NGVERSION} <= 4.0	CLI:Test Set Field Options	hmac|message_digest	DSA	RIPEMD160	RSA-SHA1-2	SHA224
	...	DSA-SHA	RSA-MD4	RSA-SHA224	SHA256	DSA-SHA1	RSA-MD5	RSA-SHA256	SHA384
	...	DSA-SHA1-old	RSA-MDC2	RSA-SHA384	SHA512	MD4	RSA-RIPEMD160	RSA-SHA512
	...	ecdsa-with-SHA1	MD5	RSA-SHA	SHA	whirlpool	MDC2	RSA-SHA1	SHA1
	Run Keyword If	${NGVERSION} > 4.0	CLI:Test Set Field Options	hmac|message_digest	BLAKE2b512	RSA-MD5	RSA-SHA512	SHA3-256
	...	SHAKE256	BLAKE2s256	RSA-MDC2	RSA-SHA512|224	SHA3-384	SM3	MD4	RSA-RIPEMD160
	...	RSA-SHA512|256	SHA3-512	id-rsassa-pkcs1-v1_5-with-sha3-224	MD5	RSA-SHA1	RSA-SM3
	...	SHA384	id-rsassa-pkcs1-v1_5-with-sha3-256	MD5-SHA1	RSA-SHA1-2	SHA1	SHA512
	...	id-rsassa-pkcs1-v1_5-with-sha3-384	MDC2	RSA-SHA224	SHA224	SHA512-224	id-rsassa-pkcs1-v1_5-with-sha3-512
	...	RIPEMD160	RSA-SHA256	SHA256	SHA512-256	whirlpool	RSA-MD4	RSA-SHA384	SHA3-224	SHAKE128
	CLI:Test Set Field Options	ip_addr	network	p2p	p2p-ipv6
	CLI:Test Set Field Options	use_lzo_data_compress_algorithm	no	yes

Test status field
	CLI:Enter Path	/settings/ssl_vpn/server/
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Normal Fields	status	enabled	disabled
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Normal Fields	status	enabled	disabled

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	status	${EMPTY}	Error: Missing value for parameter: status
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	status	${EMPTY}	Error: Missing value: status

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	status	${WORD}	Error: Invalid value: ${WORD} for parameter: status
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	status	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	status	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: status
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	status	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	status	${POINTS}	Error: Invalid value: ${POINTS} for parameter: status
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	status	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	status	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: status
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	status	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	status	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: status
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	status	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	status	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: status
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	status	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	status	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: status
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	status	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test listen_ip_address field
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Test Set Validate Invalid Options	listen_ip_address	${EMPTY}
	CLI:Test Set Validate Invalid Options	listen_ip_address	${WORD}	Error: listen_ip_address: Invalid IP address.
	CLI:Test Set Validate Invalid Options	listen_ip_address	${NUMBER}	Error: listen_ip_address: Invalid IP address.
	CLI:Test Set Validate Invalid Options	listen_ip_address	${POINTS}	Error: listen_ip_address: Invalid IP address.
	CLI:Test Set Validate Invalid Options	listen_ip_address	${ONE_WORD}	Error: listen_ip_address: Invalid IP address.
	CLI:Test Set Validate Invalid Options	listen_ip_address	${ONE_NUMBER}	Error: listen_ip_address: Invalid IP address.
	CLI:Test Set Validate Invalid Options	listen_ip_address	${ONE_POINTS}	Error: listen_ip_address: Invalid IP address.
	CLI:Test Set Validate Invalid Options	listen_ip_address	${EXCEEDED}	Error: listen_ip_address: Invalid IP address.
	CLI:Test Set Validate Invalid Options	listen_ip_address	${IP}
	[Teardown]	CLI:Revert

Test listen_port_number field
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Test Set Validate Invalid Options	listen_port_number	${EMPTY}	Error: listen_port_number: Validation error.
	CLI:Test Set Validate Invalid Options	listen_port_number	${WORD}	Error: listen_port_number: Validation error.
	CLI:Test Set Validate Invalid Options	listen_port_number	${NUMBER}
	CLI:Test Set Validate Invalid Options	listen_port_number	${POINTS}	Error: listen_port_number: Validation error.
	CLI:Test Set Validate Invalid Options	listen_port_number	${ONE_WORD}	Error: listen_port_number: Validation error.
	CLI:Test Set Validate Invalid Options	listen_port_number	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	listen_port_number	${ONE_POINTS}	Error: listen_port_number: Validation error.
	CLI:Test Set Validate Invalid Options	listen_port_number	${EXCEEDED}	Error: listen_port_number: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test protocol field
	CLI:Enter Path	/settings/ssl_vpn/server/
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Normal Fields	protocol	tcp	tcp-ipv6	udp	upd-ipv6
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Normal Fields	protocol	tcp	tcp-ipv6	udp	upd-ipv6

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	protocol	${EMPTY}	Error: Missing value for parameter: protocol
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	protocol	${EMPTY}	Error: Missing value: protocol

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	protocol	${WORD}	Error: Invalid value: ${WORD} for parameter: protocol
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	protocol	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	protocol	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: protocol
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	protocol	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	protocol	${POINTS}	Error: Invalid value: ${POINTS} for parameter: protocol
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	protocol	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	protocol	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: protocol
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	protocol	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	protocol	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: protocol
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	protocol	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	protocol	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: protocol
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	protocol	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	protocol	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: protocol
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	protocol	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test tunnel_mtu field
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Test Set Validate Invalid Options	tunnel_mtu	${EMPTY}	Error: tunnel_mtu: Validation error.
	CLI:Test Set Validate Invalid Options	tunnel_mtu	${WORD}	Error: tunnel_mtu: Validation error.
	CLI:Test Set Validate Invalid Options	tunnel_mtu	${NUMBER}
	CLI:Test Set Validate Invalid Options	tunnel_mtu	${POINTS}	Error: tunnel_mtu: Validation error.
	CLI:Test Set Validate Invalid Options	tunnel_mtu	${ONE_WORD}	Error: tunnel_mtu: Validation error.
	CLI:Test Set Validate Invalid Options	tunnel_mtu	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	tunnel_mtu	${ONE_POINTS}	Error: tunnel_mtu: Validation error.
	CLI:Test Set Validate Invalid Options	tunnel_mtu	${EXCEEDED}	Error: tunnel_mtu: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test number_of_concurrent_tunnels: field
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Test Set Validate Invalid Options	number_of_concurrent_tunnels	${EMPTY}	Error: number_of_concurrent_tunnels: Validation error.
	CLI:Test Set Validate Invalid Options	number_of_concurrent_tunnels	${WORD}	Error: number_of_concurrent_tunnels: Validation error.
	CLI:Test Set Validate Invalid Options	number_of_concurrent_tunnels	${NUMBER}
	CLI:Test Set Validate Invalid Options	number_of_concurrent_tunnels	${POINTS}	Error: number_of_concurrent_tunnels: Validation error.
	CLI:Test Set Validate Invalid Options	number_of_concurrent_tunnels	${ONE_WORD}	Error: number_of_concurrent_tunnels: Validation error.
	CLI:Test Set Validate Invalid Options	number_of_concurrent_tunnels	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	number_of_concurrent_tunnels	${ONE_POINTS}	Error: number_of_concurrent_tunnels: Validation error.
	CLI:Test Set Validate Invalid Options	number_of_concurrent_tunnels	${EXCEEDED}	Error: number_of_concurrent_tunnels: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test ip_addr field
	CLI:Enter Path	/settings/ssl_vpn/server/
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Normal Fields	ip_addr	network	p2p	p2p-ipv6
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Normal Fields	ip_addr	network	p2p	p2p-ipv6

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ip_addr	${EMPTY}	Error: Missing value for parameter: ip_addr
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ip_addr	${EMPTY}	Error: Missing value: ip_addr

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ip_addr	${WORD}	Error: Invalid value: ${WORD} for parameter: ip_addr
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ip_addr	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ip_addr	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: ip_addr
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ip_addr	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ip_addr	${POINTS}	Error: Invalid value: ${POINTS} for parameter: ip_addr
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ip_addr	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ip_addr	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: ip_addr
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ip_addr	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ip_addr	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: ip_addr
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ip_addr	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ip_addr	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: ip_addr
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ip_addr	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ip_addr	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: ip_addr
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ip_addr	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test ipv4_tunnel field
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	ip_addr=network
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${EMPTY}
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${WORD}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${NUMBER}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${POINTS}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${ONE_WORD}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${ONE_NUMBER}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${ONE_POINTS}	Error: ipv4_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv4_tunnel	${EXCEEDED}	Error: ipv4_tunnel: Validation error.
	[Teardown]	CLI:Revert

Test ipv6_tunnel: field
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	ip_addr=network
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${EMPTY}
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${WORD}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${NUMBER}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${POINTS}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${ONE_WORD}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${ONE_NUMBER}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${ONE_POINTS}	Error: ipv6_tunnel: Validation error.
	CLI:Test Set Validate Invalid Options	ipv6_tunnel	${EXCEEDED}	Error: ipv6_tunnel: Validation error.
	[Teardown]	CLI:Revert

Test local_endpoint field
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	ip_addr=p2p
	CLI:Test Set Validate Invalid Options	local_endpoint	${EMPTY}
	CLI:Test Set Validate Invalid Options	local_endpoint	${WORD}	Error: local_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_endpoint	${NUMBER}	Error: local_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_endpoint	${POINTS}	Error: local_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_endpoint	${ONE_WORD}	Error: local_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_endpoint	${ONE_NUMBER}	Error: local_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_endpoint	${ONE_POINTS}	Error: local_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_endpoint	${EXCEEDED}	Error: local_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_endpoint	${IP}
	[Teardown]	CLI:Revert

Test remote_endpoint field
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	ip_addr=p2p
	CLI:Test Set Validate Invalid Options	remote_endpoint	${EMPTY}
	CLI:Test Set Validate Invalid Options	remote_endpoint	${WORD}	Error: remote_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_endpoint	${NUMBER}	Error: remote_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_endpoint	${POINTS}	Error: remote_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_endpoint	${ONE_WORD}	Error: remote_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_endpoint	${ONE_NUMBER}	Error: remote_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_endpoint	${ONE_POINTS}	Error: remote_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_endpoint	${EXCEEDED}	Error: remote_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_endpoint	${IP}
	[Teardown]	CLI:Revert

Test local_ipv6_endpoint field
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	ip_addr=p2p-ipv6
	CLI:Test Set Validate Invalid Options	local_ipv6_endpoint	${EMPTY}
	CLI:Test Set Validate Invalid Options	local_ipv6_endpoint	${WORD}	Error: local_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_ipv6_endpoint	${NUMBER}	Error: local_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_ipv6_endpoint	${POINTS}	Error: local_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_ipv6_endpoint	${ONE_WORD}	Error: local_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_ipv6_endpoint	${ONE_NUMBER}	Error: local_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_ipv6_endpoint	${ONE_POINTS}	Error: local_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	local_ipv6_endpoint	${EXCEEDED}	Error: local_ipv6_endpoint: Invalid IP address.
	[Teardown]	CLI:Revert

Test remote_ipv6_endpoint field
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	ip_addr=p2p-ipv6
	CLI:Test Set Validate Invalid Options	remote_ipv6_endpoint	${EMPTY}
	CLI:Test Set Validate Invalid Options	remote_ipv6_endpoint	${WORD}	Error: remote_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_ipv6_endpoint	${NUMBER}	Error: remote_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_ipv6_endpoint	${POINTS}	Error: remote_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_ipv6_endpoint	${ONE_WORD}	Error: remote_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_ipv6_endpoint	${ONE_NUMBER}	Error: remote_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_ipv6_endpoint	${ONE_POINTS}	Error: remote_ipv6_endpoint: Invalid IP address.
	CLI:Test Set Validate Invalid Options	remote_ipv6_endpoint	${EXCEEDED}	Error: remote_ipv6_endpoint: Invalid IP address.
	[Teardown]	CLI:Revert

Test authentication_method field with ip_addr=network
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	ip_addr=network
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Normal Fields	authentication_method	tls	password	password_plus_tls
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Normal Fields	authentication_method	tls	password	password_plus_tls

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	authentication_method	static_key	Error: authentication_method: Network Server requires TLS authentication
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	authentication_method	static_key

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	authentication_method	${EMPTY}	Error: Missing value for parameter: authentication_method
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	authentication_method	${EMPTY}	Error: Missing value: authentication_method

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	authentication_method	${WORD}	Error: Invalid value: ${WORD} for parameter: authentication_method
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	authentication_method	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	authentication_method	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: authentication_method
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	authentication_method	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	authentication_method	${POINTS}	Error: Invalid value: ${POINTS} for parameter: authentication_method
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	authentication_method	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	authentication_method	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: authentication_method
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	authentication_method	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	authentication_method	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: authentication_method
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	authentication_method	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	authentication_method	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: authentication_method
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	authentication_method	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	authentication_method	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: authentication_method
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	authentication_method	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test authentication_method field with ip_addr=p2p
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	ip_addr=p2p
	CLI:Test Set Validate Normal Fields	authentication_method	tls	static_key	password	password_plus_tls
	[Teardown]	CLI:Revert

Test authentication_method field with ip_addr=p2p-ipv6
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	ip_addr=p2p-ipv6
	CLI:Test Set Validate Normal Fields	authentication_method	tls	static_key	password	password_plus_tls
	[Teardown]	CLI:Revert

Test ca_certificate field to TLS authentication method
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	authentication_method=tls

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${EMPTY}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${EMPTY}	Error: Missing value: ca_certificate

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${EXCEEDED}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test server_certificate field to TLS authentication method
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	authentication_method=tls
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${EMPTY}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${EMPTY}	Error: Missing value: server_certificate

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${EXCEEDED}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test server_key field to TLS authentication method
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	authentication_method=tls
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_key	${EMPTY}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_key	${EMPTY}	Error: Missing value: server_key

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_key	${WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_key	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_key	${NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_key	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_key	${POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_key	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_key	${ONE_WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_key	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_key	${ONE_NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_key	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_key	${ONE_POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_key	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_key	${EXCEEDED}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_key	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test diffie_hellman field to TLS authentication method
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	authentication_method=tls
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${EMPTY}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${EMPTY}	Error: Missing value: diffie_hellman

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${EXCEEDED}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test ca_certificate field to Password authentication method
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	authentication_method=password
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${EMPTY}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${EMPTY}	Error: Missing value: ca_certificate

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${EXCEEDED}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test server_certificate field to Password authentication method
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	authentication_method=password
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${EMPTY}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${EMPTY}	Error: Missing value: server_certificate

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${EXCEEDED}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test server_key field to Password authentication method
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	authentication_method=password
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_key	${EMPTY}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_key	${EMPTY}	Error: Missing value: server_key

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_key	${WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_key	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_key	${NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_key	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_key	${POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_key	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_key	${ONE_WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_key	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_key	${ONE_NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_key	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_key	${ONE_POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_key	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_key	${EXCEEDED}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_key	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test diffie_hellman field to Password authentication method
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set Field	authentication_method	password
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${EMPTY}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${EMPTY}	Error: Missing value: diffie_hellman

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${EXCEEDED}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test error message to Static Key authentication method
	CLI:Enter Path	/settings/ssl_vpn/server/
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	authentication_method	static_key	Error: authentication_method: Network Server requires TLS authentication
	[Teardown]	CLI:Revert

Test secret field to Static Key authentication method
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	ip_addr=p2p-ipv6 authentication_method=static_key
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	secret	${EMPTY}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	secret	${EMPTY}	Error: Missing value: secret

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	secret	${WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	secret	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	secret	${NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	secret	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	secret	${POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	secret	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	secret	${ONE_WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	secret	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	secret	${ONE_NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	secret	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	secret	${ONE_POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	secret	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	secret	${EXCEEDED}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	secret	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test diffie_hellman field to Static Key authentication method
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	ip_addr=p2p-ipv6 authentication_method=static_key
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${EMPTY}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${EMPTY}	Error: Missing value: diffie_hellman

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${EXCEEDED}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test ca_certificate field to Password plus TLS authentication method
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	authentication_method=password_plus_tls
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${EMPTY}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${EMPTY}	Error: Missing value: ca_certificate

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${EXCEEDED}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	ca_certificate	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test server_certificate field to Password plus TLS authentication method
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	authentication_method=password_plus_tls
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${EMPTY}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${EMPTY}	Error: Missing value: server_certificate

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${EXCEEDED}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_certificate	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test server_key field to Password plus TLS authentication method
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	authentication_method=password_plus_tls
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_key	${EMPTY}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_key	${EMPTY}	Error: Missing value: server_key

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_key	${WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_key	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_key	${NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_key	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_key	${POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_key	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_key	${ONE_WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_key	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_key	${ONE_NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_key	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_key	${ONE_POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_key	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	server_key	${EXCEEDED}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	server_key	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test diffie_hellman field to Password plus TLS authentication method
	CLI:Enter Path	/settings/ssl_vpn/server/
	CLI:Set	authentication_method=password_plus_tls
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${EMPTY}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${EMPTY}	Error: Missing value: diffie_hellman

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_WORD}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_NUMBER}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_POINTS}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${EXCEEDED}
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	diffie_hellman	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test hmac|message_digest field
	CLI:Enter Path	/settings/ssl_vpn/server/
	Run Keyword If	${NGVERSION} <= 4.0	CLI:Test Set Validate Normal Fields	hmac|message_digest	DSA	RIPEMD160	RSA-SHA1-2	SHA224
	...	DSA-SHA	RSA-MD4	RSA-SHA224	SHA256	DSA-SHA1	RSA-MD5	RSA-SHA256	SHA384
	...	DSA-SHA1-old	RSA-MDC2	RSA-SHA384	SHA512	MD4	RSA-RIPEMD160	RSA-SHA512
	...	ecdsa-with-SHA1	MD5	RSA-SHA	SHA	whirlpool	MDC2	RSA-SHA1	SHA1
	Run Keyword If	${NGVERSION} >= 4.1	CLI:Test Set Validate Normal Fields	hmac|message_digest	BLAKE2b512	RSA-MD5	RSA-SHA512	SHA3-256
	...	SHAKE256	BLAKE2s256	RSA-MDC2	RSA-SHA512|224	SHA3-384	SM3	MD4	RSA-RIPEMD160
	...	RSA-SHA512|256	SHA3-512	id-rsassa-pkcs1-v1_5-with-sha3-224	MD5	RSA-SHA1	RSA-SM3
	...	SHA384	id-rsassa-pkcs1-v1_5-with-sha3-256	MD5-SHA1	RSA-SHA1-2	SHA1	SHA512
	...	id-rsassa-pkcs1-v1_5-with-sha3-384	MDC2	RSA-SHA224	SHA224	SHA512-224	id-rsassa-pkcs1-v1_5-with-sha3-512
	...	RIPEMD160	RSA-SHA256	SHA256	SHA512-256	whirlpool	RSA-MD4	RSA-SHA384	SHA3-224	SHAKE128

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	hmac|message_digest	${EMPTY}	Error: Missing value for parameter: hmac|message_digest
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	hmac|message_digest	${EMPTY}	Error: Missing value: hmac|message_digest

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	hmac|message_digest	${WORD}	Error: Invalid value: ${WORD} for parameter: hmac|message_digest
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	hmac|message_digest	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	hmac|message_digest	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: hmac|message_digest
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	hmac|message_digest	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	hmac|message_digest	${POINTS}	Error: Invalid value: ${POINTS} for parameter: hmac|message_digest
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	hmac|message_digest	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	hmac|message_digest	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: hmac|message_digest
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	hmac|message_digest	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	hmac|message_digest	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: hmac|message_digest
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	hmac|message_digest	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	hmac|message_digest	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: hmac|message_digest
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	hmac|message_digest	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	hmac|message_digest	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: hmac|message_digest
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	hmac|message_digest	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test cipher field
	CLI:Enter Path	/settings/ssl_vpn/server/
	Run Keyword If	${NGVERSION} <= 4.0	CLI:Test Set Validate Normal Fields	cipher	AES-128-CBC	AES-192-CFB1	AES-256-OFB
	...	CAMELLIA-128-CFB8	CAMELLIA-256-CBC	CAST5-OFB	DES-EDE-OFB	DESX-CBC	RC2-CFB
	...	AES-128-CFB	AES-192-CFB8	BF-CBC	CAMELLIA-128-OFB	CAMELLIA-256-CFB	DES-CBC	DES-EDE3-CBC	IDEA-CBC	RC2-OFB
	...	AES-128-CFB1	AES-192-OFB	BF-CFB	CAMELLIA-192-CBC	CAMELLIA-256-CFB1	DES-CFB	DES-EDE3-CFB	IDEA-CFB	SEED-CBC
	...	AES-128-CFB8	AES-256-CBC	BF-OFB	CAMELLIA-192-CFB	CAMELLIA-256-CFB8	DES-CFB1	DES-EDE3-CFB1	IDEA-OFB	SEED-CFB
	...	AES-128-OFB	AES-256-CFB1	CAMELLIA-128-CBC	CAMELLIA-192-CFB1	CAMELLIA-256-OFB	DES-CFB8	DES-EDE3-CFB8	RC2-40-CBC	SEED-OFB
	...	AES-192-CBC	AES-256-CFB1	CAMELLIA-128-CFB	CAMELLIA-192-CFB8	CAST5-CBC	DES-EDE-CBC	DES-EDE3-OFB	RC2-64-CBC
	...	AES-192-CFB	AES-256-CFB8	CAMELLIA-128-CFB1	CAMELLIA-192-OFB	CAST5-CFB	DES-EDE-CFB	DES-OFB	RC2-CBC

	Run Keyword If	${NGVERSION} > 4.0	CLI:Test Set Validate Normal Fields	cipher	AES-128-CBC	AES-192-CFB8	ARIA-128-CBC
	...	ARIA-192-CFB8	BF-CBC	CAMELLIA-192-CFB	CAST5-CBC	DES-EDE-OFB	IDEA-CFB	SEED-OFB
	...	AES-128-CFB	AES-192-GCM	ARIA-128-CFB	ARIA-192-GCM	BF-CFB	CAMELLIA-192-CFB1	CAST5-CFB	DES-EDE3-CBC	IDEA-OFB	SM4-CBC
	...	AES-128-CFB1	AES-192-OFB	ARIA-128-CFB1	ARIA-192-OFB	BF-OFB	CAMELLIA-192-CFB8	CAST5-OFB	DES-EDE3-CFB	RC2-40-CBC	SM4-CFB
	...	AES-128-CFB8	AES-256-CBC	ARIA-128-CFB8	ARIA-256-CBC	CAMELLIA-128-CBC	CAMELLIA-192-OFB	DES-CBC	DES-EDE3-CFB1	RC2-64-CBC	SM4-OFB
	...	AES-128-GCM	AES-256-CFB	ARIA-128-GCM	ARIA-256-CFB	CAMELLIA-128-CFB	CAMELLIA-256-CBC	DES-CFB	DES-EDE3-CFB8	RC2-CBC	The
	...	AES-128-OFB	AES-256-CFB1	ARIA-128-OFB	ARIA-256-CFB1	CAMELLIA-128-CFB1	CAMELLIA-256-CFB	DES-CFB1	DES-EDE3-OFB	RC2-CFB	and
	...	AES-192-CBC	AES-256-CFB8	ARIA-192-CBC	ARIA-256-CFB8	CAMELLIA-128-CFB8	CAMELLIA-256-CFB1	DES-CFB8	DES-OFB	RC2-OFB
	...	AES-192-CFB	AES-256-GCM	ARIA-192-CFB	ARIA-256-GCM	CAMELLIA-128-OFB	CAMELLIA-256-CFB8	DES-EDE-CBC	DESX-CBC	SEED-CBC
	...	AES-192-CFB1	AES-256-OFB	ARIA-192-CFB1	ARIA-256-OFB	CAMELLIA-192-CBC	CAMELLIA-256-OFB	DES-EDE-CFB	IDEA-CBC	SEED-CFB

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	cipher	${EMPTY}	Error: Missing value for parameter: cipher
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	cipher	${EMPTY}	Error: Missing value: cipher

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	cipher	${WORD}	Error: Invalid value: ${WORD} for parameter: cipher
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	cipher	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	cipher	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: cipher
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	cipher	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	cipher	${POINTS}	Error: Invalid value: ${POINTS} for parameter: cipher
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	cipher	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	cipher	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: cipher
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	cipher	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	cipher	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: cipher
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	cipher	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	cipher	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: cipher
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	cipher	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	cipher	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: cipher
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	cipher	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test min_tls_version field
	CLI:Test Set Validate Normal Fields	min_tls_version	none	1.0	1.1	1.2

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	min_tls_version	${EMPTY}	Error: Missing value for parameter: min_tls_version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	min_tls_version	${EMPTY}	Error: Missing value: min_tls_version

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	min_tls_version	${WORD}	Error: Invalid value: ${WORD} for parameter: min_tls_version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	min_tls_version	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	min_tls_version	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: min_tls_version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	min_tls_version	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	min_tls_version	${POINTS}	Error: Invalid value: ${POINTS} for parameter: min_tls_version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	min_tls_version	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	min_tls_version	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: min_tls_version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	min_tls_version	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	min_tls_version	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: min_tls_version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	min_tls_version	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	min_tls_version	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: min_tls_version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	min_tls_version	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	min_tls_version	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: min_tls_version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	min_tls_version	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test use_lzo_data_compress_algorithm field
	CLI:Test Set Validate Normal Fields	use_lzo_data_compress_algorithm	yes	no

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${EMPTY}	Error: Missing value for parameter: use_lzo_data_compress_algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${EMPTY}	Error: Missing value: use_lzo_data_compress_algorithm

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${WORD}	Error: Invalid value: ${WORD} for parameter: use_lzo_data_compress_algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: use_lzo_data_compress_algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${POINTS}	Error: Invalid value: ${POINTS} for parameter: use_lzo_data_compress_algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: use_lzo_data_compress_algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: use_lzo_data_compress_algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: use_lzo_data_compress_algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: use_lzo_data_compress_algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	use_lzo_data_compress_algorithm	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

Test redirect_gateway field
	CLI:Test Set Validate Normal Fields	redirect_gateway	yes	no

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	redirect_gateway	${EMPTY}	Error: Missing value for parameter: redirect_gateway
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	redirect_gateway	${EMPTY}	Error: Missing value: redirect_gateway

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	redirect_gateway	${WORD}	Error: Invalid value: ${WORD} for parameter: redirect_gateway
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	redirect_gateway	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	redirect_gateway	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: redirect_gateway
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	redirect_gateway	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	redirect_gateway	${POINTS}	Error: Invalid value: ${POINTS} for parameter: redirect_gateway
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	redirect_gateway	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	redirect_gateway	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: redirect_gateway
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	redirect_gateway	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	redirect_gateway	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: redirect_gateway
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	redirect_gateway	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	redirect_gateway	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: redirect_gateway
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	redirect_gateway	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Validate Invalid Options	redirect_gateway	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: redirect_gateway
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Validate Invalid Options	redirect_gateway	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	[Teardown]	CLI:Revert

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection

SUITE:SET INVALID VALUES ERROR MESSAGES
	[Arguments]	${COMMANDS}
	FOR	${COMMAND}	IN	@{COMMANDS}
		Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${COMMAND}	a	Error: Invalid value: a for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value: ${COMMAND}
		Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${COMMAND}	a	Error: Invalid value: a
	END

SUITE:CHECK FOR STATIC KEY
	Set Default Configuration	prompt=#
	Log To Console	Opening connection to root
	CLI:Connect As Root
	CLI:Enter Path	/etc/openvpn/CA
	Write	ls
	${OUTPUT}=	Read Until Prompt
	${STRING}=	Split To Lines	${OUTPUT}	0	-1
	${STATUS}=	Run Keyword and Return Status	Should Be Empty	${STRING}
	CLose Connection
	CLI:Switch Connection	default
	[Return]	${STATUS}