*** Settings ***
Resource	../../init.robot
Documentation	Validation tests related to auditing event_list section
Metadata	Version 1.0
Metadata	Executed At ${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
Default Tags	CLI	SSH	AUDITING

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CUSTOM_SCRIPT}	test-custom-script.py
@{ALL_VALUES}	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}

*** Test Cases ***
Test Ls Command
	CLI:Enter Path	/settings/auditing/
	CLI:Test Ls Command	settings	events	destinations	event_list

Test Ls Command To Event List
	CLI:Enter Path	/settings/auditing/event_list/
	CLI:Test Ls Command	100	101	102	103	104	105	106	107	108	109	110	111	112	113	114	115	116	118	119	120	121	122	123	124
	...	126	127	128	129	130	131	132	133	134	135	136	137	138	139	140	141	142	143	144	145	150	151	152	153	154	155	156	157
	...	158	159	160	161	162	163	164	165	200	201	202	203	300	301	302	303	304	305	306	307	308	310	311	312	313	314	315	400
	...	401	402	410	411	450	451	452	453	454	460	461	462	463	464	465	466	467	468	469	470	471	472	500	501	502	503	504	505
	...	506	507	508	509	510	511	512	513	514	515	516	517	518	519	520	521	522	523	524	525

Test Available Commands
	CLI:Enter Path	/settings/auditing/event_list/
	CLI:Test Available Commands	enable	disable

Test Show Event list
	CLI:Enter Path	/settings/auditing/event_list/
	CLI:Test Show Command	100	101	102	103	104	105	106	107	108	109	110	111	112	113
	...	114	115	116	118	119	120	121	122	123	124	126	127	128	129	130	131	132	133	134	135	136	137	138	139	140	141	142	143
	...	144	145	150	151	152	153	154	155	156	157	158	159	160	161	162	163	164	165	200	201	202	203	300	301	302	303	304	305
	...	306	307	308	310	311	312	313	314	315	400	401	402	410	411	450	451	452	453	454	460	461	462	463	464	465	466	467	468
	...	469	470	471	472	500	501	502	503	504	505	506	507	508	509	510	511	512	513	514	515	516	517	518	519	520	521	522	523
	...	524	525

Test show available fields for event on list
	CLI:Enter Path	/settings/auditing/event_list/108/
	CLI:Test Show Command	enable	description	category	action_script
	CLI:Test Set Unavailable Fields	description	category

Test set valid values for field=enable
	CLI:Enter Path	/settings/auditing/event_list/108/
	CLI:Test Set Field Options	enable	no	yes
	[Teardown]	CLI:Revert	Raw

Test set invalid values for field=enable
	CLI:Enter Path	/settings/auditing/event_list/108/
	CLI:Test Set Field Invalid Options	enable	${EMPTY}
	...	Error: Missing value for parameter: enable

	FOR	${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	enable	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: enable
	END
	[Teardown]	CLI:Revert	Raw

Test set valid values for field=action_script
	CLI:Enter Path	/settings/auditing/event_list/108/
	CLI:Test Set Field Options Raw	action_script	actionscript_sample.sh	${EMPTY}
	[Teardown]	CLI:Revert	Raw

Test set invalid values for field=action_script
	CLI:Enter Path	/settings/auditing/event_list/108/
	IF	${NGVERSION} >= 5.6
		FOR	${INVALID_VALUE}	IN	@{ALL_VALUES}
			CLI:Test Set Field Invalid Options	action_script	${INVALID_VALUE}
			...	Error: Invalid value: ${INVALID_VALUE} for parameter: action_script
		END
	END
	#FIELD HAS NO VALIDATION FOR VERSION 5.4-, BUG 6595 was fixed for 5.6+
	[Teardown]	CLI:Revert	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Add Custom Action Script

SUITE:Teardown
	SUITE:Remove Settings From Event On List
	SUITE:Remove Custom Script
	CLI:Close Connection

SUITE:Add Custom Action Script
	CLI:Write	shell sudo touch /etc/scripts/auditing/${CUSTOM_SCRIPT}

SUITE:Remove Settings From Event On List
	CLI:Enter Path	/settings/auditing/event_list/108/
	CLI:Set	action_script=
	CLI:Commit

SUITE:Remove Custom Script
	CLI:Write	shell sudo rm /etc/scripts/auditing/${CUSTOM_SCRIPT}