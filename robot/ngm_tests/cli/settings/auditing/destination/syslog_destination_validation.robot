*** Settings ***
Resource	../../../init.robot
Documentation	Tests Auditing Destination section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	AUDITING	DESTINATION
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test Syslog Destination available commands after send tab-tab
	CLI:Enter Path	/settings/auditing/destinations/syslog
	CLI:Test Available Commands	cd	change_password	commit	event_system_audit	event_system_clear	exit	hostname
	...	ls	pwd	quit	reboot	revert	set	shell	show	show_settings	shutdown	whoami

Test Syslog Destination visible fields for show command
	CLI:Enter Path	/settings/auditing/destinations/syslog
	CLI:Test Show Command	system_console	admin_session	ipv4_remote_server	ipv6_remote_server	event_facility
	...	datalog_facility

Test Syslog Destination available fields to set command
	CLI:Enter Path	/settings/auditing/destinations/syslog
	CLI:Test Set Available Fields	admin_session	datalog_facility	event_facility	ipv4_remote_server
	...	ipv6_remote_server	system_console

#datalog_facility
Test available options for field=datalog_facility
	CLI:Enter Path	/settings/auditing/destinations/syslog
	CLI:Test Set Field Options	datalog_facility	log_local_0	log_local_1	log_local_2	log_local_3	log_local_4
	...	log_local_5
	[Teardown]	CLI:Revert

Test incorrect values for field=datalog_facility
	CLI:Enter Path	/settings/auditing/destinations/syslog
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	datalog_facility	${EMPTY}	Error: Missing value for parameter: datalog_facility
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	datalog_facility	${EMPTY}	Error: Missing value: datalog_facility
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	datalog_facility	a	Error: Invalid value: a for parameter: datalog_facility
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	datalog_facility	a	Error: Invalid value: a

Test for commands after Error for field=datalog_facility
	CLI:Commands After Set Error	datalog_facility	${EMPTY}	cd	revert	show	ls	set
	[Teardown]	CLI:Revert

#event_facility
Test available options for field=event_facility
	CLI:Enter Path	/settings/auditing/destinations/syslog
	CLI:Test Set Field Options	event_facility	log_local_0	log_local_1	log_local_2	log_local_3	log_local_4	log_local_5
	[Teardown]	CLI:Revert

Test incorrect values for field=event_facility
	CLI:Enter Path	/settings/auditing/destinations/syslog
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	event_facility	${EMPTY}	Error: Missing value for parameter: event_facility
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	event_facility	${EMPTY}	Error: Missing value: event_facility
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	event_facility	a	Error: Invalid value: a for parameter: event_facility
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	event_facility	a	Error: Invalid value: a

Test for commands after Error for field=event_facility
	CLI:Commands After Set Error	event_facility	${EMPTY}	cd	revert	show	ls	set
	[Teardown]	CLI:Revert

#admin_session
Test available options for field=admin_session
	CLI:Enter Path	/settings/auditing/destinations/syslog
	CLI:Test Set Field Options	admin_session	yes	no
	[Teardown]	CLI:Revert

Test incorrect values for field=admin_session
	CLI:Enter Path	/settings/auditing/destinations/syslog
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	admin_session	${EMPTY}	Error: Missing value for parameter: admin_session
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	admin_session	${EMPTY}	Error: Missing value: admin_session
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	admin_session	a	Error: Invalid value: a for parameter: admin_session
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	admin_session	a	Error: Invalid value: a

Test for commands after Error for field=admin_session
	CLI:Commands After Set Error	admin_session	${EMPTY}	cd	revert	show	ls	set
	[Teardown]	CLI:Revert

#system_console
Test available options for field=system_console
	CLI:Enter Path	/settings/auditing/destinations/syslog
	CLI:Test Set Field Options	system_console	yes	no
	[Teardown]	CLI:Revert

Test incorrect values for field=system_console
	CLI:Enter Path	/settings/auditing/destinations/syslog
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	system_console	${EMPTY}	Error: Missing value for parameter: system_console
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	system_console	${EMPTY}	Error: Missing value: system_console
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	system_console	a	Error: Invalid value: a for parameter: system_console
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	system_console	a	Error: Invalid value: a

Test for commands after Error for field=system_console
	CLI:Commands After Set Error	system_console	${EMPTY}	cd	revert	show	ls	set
	[Teardown]	CLI:Revert

#ipv4_remote_server
Test correct values for field=ipv4_remote_server
	CLI:Enter Path	/settings/auditing/destinations/syslog
	CLI:Write	set ipv4_remote_server=yes ipv4_address=127.0.0.1
	CLI:Test Set Field Options	ipv4_remote_server	yes	no
	[Teardown]	CLI:Revert

Test incorrect values for field=ipv4_remote_server
	CLI:Enter Path	/settings/auditing/destinations/syslog
	CLI:Write	set ipv4_remote_server=yes ipv4_address=127.0.0.1
	CLI:Test Set Field Options	ipv4_remote_server	yes	no
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ipv4_remote_server	${EMPTY}	Error: Missing value for parameter: ipv4_remote_server
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ipv4_remote_server	${EMPTY}	Error: Missing value: ipv4_remote_server
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ipv4_remote_server	a	Error: Invalid value: a for parameter: ipv4_remote_server
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ipv4_remote_server	a	Error: Invalid value: a
	[Teardown]	CLI:Revert

Test for commands after Error for field=ipv4_remote_server
	CLI:Commands After Set Error	ipv4_remote_server	${EMPTY}	cd	revert	show	ls	set
	[Teardown]	CLI:Revert

#ipv4_address
Test correct values for field=ipv4_address
	CLI:Enter Path	/settings/auditing/destinations/syslog
	CLI:Write	set ipv4_remote_server=yes ipv4_address=127.0.0.1
	CLI:Test Set Field Valid Options	ipv4_address	0.0.0.0
	CLI:Write	set ipv4_remote_server=yes ipv4_address=127.0.0.1
	CLI:Test Set Field Valid Options	ipv4_address	255.255.255.255
	CLI:Write	set ipv4_remote_server=yes ipv4_address=127.0.0.1
	CLI:Test Set Field Valid Options	ipv4_address	1.1.1.1
	CLI:Write	set ipv4_remote_server=yes ipv4_address=127.0.0.1
	CLI:Test Set Field Valid Options	ipv4_address	001.001.001.001
	[Teardown]	CLI:Revert

Test incorrect values for field=ipv4_address
	CLI:Enter Path	/settings/auditing/destinations/syslog
	CLI:Write	set ipv4_remote_server=yes ipv4_address=127.0.0.1
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ipv4_address	${EMPTY}	Error: ipv4_address: Validation error.
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ipv4_address	${EMPTY}	Error: ipv4_remote_server: At least one syslog server must be specified.
	CLI:Write	set ipv4_remote_server=yes ipv4_address=127.0.0.1
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ipv4_address	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa	Error: ipv4_remote_server: At least one syslog server must be specified.Error: ipv4_address: Exceeded the maximum size for this field.
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ipv4_address	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa	Error: ipv4_remote_server: At least one syslog server must be specified.
	CLI:Write	set ipv4_remote_server=yes ipv4_address=127.0.0.1
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ipv4_address	255.255.255.256	Error: ipv4_remote_server: At least one syslog server must be specified.Error: ipv4_address: Invalid IP address or host name.
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ipv4_address	255.255.255.256	Error: ipv4_remote_server: At least one syslog server must be specified.
	CLI:Write	set ipv4_remote_server=yes ipv4_address=127.0.0.1
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ipv4_address	-1	Error: ipv4_remote_server: At least one syslog server must be specified.Error: ipv4_address: Invalid IP address or host name.
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ipv4_address	-1	Error: ipv4_remote_server: At least one syslog server must be specified.
	CLI:Write	set ipv4_remote_server=yes ipv4_address=127.0.0.1
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ipv4_address	256.256.256.256	Error: ipv4_remote_server: At least one syslog server must be specified.Error: ipv4_address: Invalid IP address or host name.
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ipv4_address	256.256.256.256	Error: ipv4_remote_server: At least one syslog server must be specified.

Test for commands after Error for field=ipv4_address
	CLI:Commands After Set Error	ipv4_address	${EMPTY}	cd	revert	show	ls	set
	[Teardown]	CLI:Revert

#ipv6_remote_server
Test correct values for field=ipv6_remote_server
	CLI:Enter Path	/settings/auditing/destinations/syslog
	CLI:Write	set ipv6_remote_server=yes ipv6_address=fd38:24b8:1110:7777::5
	CLI:Test Set Field Options	ipv6_remote_server	yes	no
	[Teardown]	CLI:Revert

Test incorrect values for field=ipv6_remote_server
	CLI:Enter Path	/settings/auditing/destinations/syslog
	CLI:Write	set ipv6_remote_server=yes ipv6_address=fd38:24b8:1110:7777::5
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ipv6_remote_server	${EMPTY}	Error: Missing value for parameter: ipv6_remote_server
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ipv6_remote_server	${EMPTY}	Error: Missing value: ipv6_remote_server
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ipv6_remote_server	a	Error: Invalid value: a for parameter: ipv6_remote_server
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ipv6_remote_server	a	Error: Invalid value: a
	[Teardown]	CLI:Revert

Test for commands after Error for field=ipv6_remote_server
	CLI:Commands After Set Error	ipv6_remote_server	${EMPTY}	cd	revert	show	ls	set
	[Teardown]	CLI:Revert

#ipv6_address
Test correct values for field=ipv6_address
	Run Keyword If	'${NGVERSION}' == '3.2'	Set Tags	NON-CRITICAL
	#a bug is seen in v3.2 and will not be fixed
	CLI:Enter Path	/settings/auditing/destinations/syslog
	CLI:Write	set ipv6_remote_server=yes ipv6_address=fd38:24b8:1110:7777::5
	CLI:Test Set Field Valid Options	ipv6_address	::1
	CLI:Write	set ipv6_remote_server=yes ipv6_address=fd38:24b8:1110:7777::5
	CLI:Test Set Field Valid Options	ipv6_address	2001:0db8:0000:0000:0000:ff00:0042:8329
	CLI:Write	set ipv6_remote_server=yes ipv6_address=fd38:24b8:1110:7777::5
	CLI:Test Set Field Valid Options	ipv6_address	2001:db8:0:0:0:ff00:42:8329
	CLI:Write	set ipv6_remote_server=yes ipv6_address=fd38:24b8:1110:7777::5
	CLI:Test Set Field Valid Options	ipv6_address	2001:db8::ff00:42:8329
	[Teardown]	CLI:Revert

Test incorrect values for field=ipv6_address
	CLI:Enter Path	/settings/auditing/destinations/syslog
	CLI:Write	set ipv6_remote_server=yes ipv6_address=fd38:24b8:1110:7777::5
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ipv6_address	${EMPTY}	Error: ipv6_address: Validation error.
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ipv6_address	${EMPTY}	Error: ipv6_remote_server: At least one syslog server must be specified.
	CLI:Write	set ipv6_remote_server=yes ipv6_address=fd38:24b8:1110:7777::5
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ipv6_address	a	Error: ipv6_remote_server: At least one syslog server must be specified.Error: ipv6_address: Invalid IP address or host name.
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ipv6_address	a	Error: ipv6_remote_server: At least one syslog server must be specified.
	CLI:Write	set ipv6_remote_server=yes ipv6_address=fd38:24b8:1110:7777::5
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ipv6_address	gd38:24b8:1110:7777::5	Error: ipv6_remote_server: At least one syslog server must be specified.Error: ipv6_address: Invalid IP address or host name.
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ipv6_address	gd38:24b8:1110:7777::5	Error: ipv6_remote_server: At least one syslog server must be specified.
	[Teardown]	CLI:Revert

Test for commands after Error for field=ipv6_address
	CLI:Enter Path	/settings/auditing/destinations/syslog
	CLI:Write	set ipv6_remote_server=yes ipv6_address=fd38:24b8:1110:7777::5
	CLI:Commands After Set Error	ipv6_address	${EMPTY}	cd	revert	show	ls	set
	[Teardown]	CLI:Revert

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/auditing/destinations/syslog
	CLI:Set Field	admin_session	no
	CLI:Set Field	ipv4_remote_server	no
	CLI:Set Field	system_console	yes
	CLI:Commit
	CLI:Close Connection