*** Settings ***
Resource	../../../init.robot
Documentation	Tests Auditing Destination section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	NON-CRITICAL	NEED-REVIEW
Default Tags	CLI	AUDITING	DESTINATION	SNMPTRAP	EMAIL
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SERVERPRESENT}	${SNMPSERVERPRESENT}
${SERVER}	${SNMPSERVER}
${SERVERUSER}	${SNMPSERVERUSER}
${SERVERPASSWORD}	${SNMPSERVERPASSWORD}
${COMMUNITY}	${SNMPCOMMUNITY}
${SERVER_PATH}	${SNMPPATH}
${SERVER_FILE}	${SNMPFILE}

${MD5DESUSER}	${SNMPMD5DESUSER}
${MD5AESUSER}	${SNMPMD5AESUSER}
${SHADESUSER}	${SNMPSHADESUSER}
${SHAAESUSER}	${SNMPSHAAESUSER}

${MD5PASSWORD}	${SNMPMD5PASSWORD}
${SHAPASSWORD}	${SNMPSHAPASSWORD}

${DESPASSWORD}	${SNMPDESPASSWORD}
${AESPASSWORD}	${SNMPAESPASSWORD}

${ALIAS}	snmp_alias
${NGHOSTNAME_TEST}	snmptrap

${EMAIL_SERVER}	${EMAIL_SERVER}
${EMAIL_ACCOUNT}	${EMAIL_ACCOUNT}
${EMAIL_PASSWD}	${EMAIL_PASSWD}
${EMAIL_PORT}	${EMAIL_PORT}
${EMAIL_DESTINATION}	${EMAIL_DESTINATION}

*** Test Cases ***
Test SNMPTrap Server Version 2c tcp-ipv4 with Email destination
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:Set SNMPtrap v2	tcp-ipv4	161
	SUITE:Check Multiple Trap Servers
	SUITE:Setup Valid Email
	SUITE:Check Email Server
	SUITE:Check Multiple Trap Servers
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Revert	Raw

Test SNMPTrap Server Version 2c udp-ipv4 with Email destination
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:Set SNMPtrap v2	udp-ipv4	162
	SUITE:Check Multiple Trap Servers
	SUITE:Setup Valid Email
	SUITE:Check Email Server
	SUITE:Check Multiple Trap Servers
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Revert	Raw

Test SNMPTrap Server Version 3 MD5 DES udp-ipv4 with Email destination
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:Set SNMPtrap v3	udp-ipv4	162	md5	des	authPriv	${MD5DESUSER}	${MD5PASSWORD}	${DESPASSWORD}
	SUITE:Check Multiple Trap Servers
	SUITE:Setup Valid Email
	SUITE:Check Email Server
	SUITE:Check Multiple Trap Servers
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Revert	Raw

Test SNMPTrap Server Version 3 MD5 AES udp-ipv4 with Email destination
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:Set SNMPtrap v3	udp-ipv4	162	md5	aes	authPriv	${MD5AESUSER}	${MD5PASSWORD}	${AESPASSWORD}
	SUITE:Check Multiple Trap Servers
	SUITE:Setup Valid Email
	SUITE:Check Email Server
	SUITE:Check Multiple Trap Servers
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Revert	Raw

Test SNMPTrap Server Version 3 SHA DES udp-ipv4 with Email destination
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:Set SNMPtrap v3	udp-ipv4	162	sha	des	authPriv	${SHADESUSER}	${SHAPASSWORD}	${DESPASSWORD}
	SUITE:Check Multiple Trap Servers
	SUITE:Setup Valid Email
	SUITE:Check Email Server
	SUITE:Check Multiple Trap Servers
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Revert	Raw

Test SNMPTrap Server Version 3 SHA AES udp-ipv4 with Email destination
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:Set SNMPtrap v3	udp-ipv4	162	sha	aes	authPriv	${SHAAESUSER}	${SHAPASSWORD}	${AESPASSWORD}
	SUITE:Check Multiple Trap Servers
	SUITE:Setup Valid Email
	SUITE:Check Email Server
	SUITE:Check Multiple Trap Servers
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Revert	Raw

Test SNMPTrap Server Version 3 AUTHNOPRIV with Email destination
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:Set SNMPtrap v3	udp-ipv4	162	sha	aes	authNoPriv	${SHAAESUSER}	${SHAPASSWORD}	${AESPASSWORD}
	SUITE:Check Multiple Trap Servers
	SUITE:Setup Valid Email
	SUITE:Check Email Server
	SUITE:Check Multiple Trap Servers
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Revert	Raw

Test SNMPTrap Server Version 3 NOAUTHNOPRIV with Email destination
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:Set SNMPtrap v3	udp-ipv4	162	sha	aes	noAuthNoPriv	${SHAAESUSER}	${SHAPASSWORD}	${AESPASSWORD}
	SUITE:Check Multiple Trap Servers
	SUITE:Setup Valid Email
	SUITE:Check Email Server
	SUITE:Check Multiple Trap Servers
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Revert	Raw

Test SNMPTrap Server Version 3 AUTHNOPRIV with Email destination tcp-ipv4
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:Set SNMPtrap v3	tcpp-ipv4	162	sha	aes	authNoPriv	${SHAAESUSER}	${SHAPASSWORD}	${AESPASSWORD}
	SUITE:Check Multiple Trap Servers
	SUITE:Setup Valid Email
	SUITE:Check Email Server
	SUITE:Check Multiple Trap Servers
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Revert	Raw

Test SNMPTrap Server Version 3 NOAUTHNOPRIV with Email destination tcp-ipv4
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:Set SNMPtrap v3	tcp-ipv4	161	sha	aes	noAuthNoPriv	${SHAAESUSER}	${SHAPASSWORD}	${AESPASSWORD}
	SUITE:Check Multiple Trap Servers
	SUITE:Setup Valid Email
	SUITE:Check Email Server
	SUITE:Check Multiple Trap Servers
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Revert	Raw

*** Keywords ***
SUITE:Setup
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	CLI:Open
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	hostname=${NGHOSTNAME_TEST}
	CLI:Commit
	CLI:Enable All Events	yes	snmp_trap
	CLI:Enable All Events	yes	email
	${USER}=	CLI:Get User Whoami
	${USER}=	Set Suite Variable	${USER}

SUITE:Teardown
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set	snmptrap_server=127.0.0.1 snmptrap_transport_protocol=udp-ipv4 snmptrap_port=161 snmptrap_version=version_2c snmptrap_community=public
	CLI:Commit
	CLI:Enter Path	/settings/auditing/destinations/email
	CLI:Write	set start_tls=yes email_port=25 email_server=${EMPTY} username=${EMPTY} password=${EMPTY} confirm_password=${EMPTY} destination_email=${EMPTY}
	CLI:Commit
	CLI:Enable All Events	no	snmp_trap
	CLI:Enable All Events	no	email
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	hostname=nodegrid
	CLI:Commit
	CLI:Close Connection

SUITE:Set SNMPtrap v2
	[Arguments]	${PROTOCOL}	${PORT}
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set	snmptrap_version=version_2c snmptrap_server=${SERVER} snmptrap_community=${COMMUNITY} snmptrap_transport_protocol=${PROTOCOL} snmptrap_port=${PORT}
	CLI:Commit

SUITE:Set SNMPtrap v3
	[Arguments]	${protocol}	${port}	${auth_proto}	${priv_algo}	${sec_level}	${user}	${auth_pass}	${priv_pass}
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set	snmptrap_version=version_3 snmptrap_security_level=${sec_level} snmptrap_server=${SERVER} snmptrap_transport_protocol=${protocol} snmptrap_port=${port} snmptrap_user=${user} snmptrap_authentication=${auth_proto} snmptrap_authentication_password=${auth_pass} snmptrap_privacy_algo=${priv_algo} snmptrap_privacy_passphrase=${priv_pass}
	CLI:Commit

SUITE:Check Multiple Trap Servers
	Run Keyword If	'${NGVERSION}' < '4.0'	Set Tags	NON-CRITICAL
	Set Tags	4.0_OFFICIAL
	# Add support for multiple servers specified separated by comma
	@{servers}=	Split String	${SERVER}	,
	FOR		${ip}	IN	@{servers}
		CLI:Open	${SERVERUSER}	${SERVERPASSWORD}	session_alias=${ALIAS}	TYPE=root	HOST_DIFF=${ip}
		SUITE:Truncate Log File	${SERVER_PATH}	${SERVER_FILE}
		###
		CLI:Open	session_alias=eventsession	startping=no
		CLI:Write	ls
		Close Connection
		###
		CLI:Switch Connection	${ALIAS}
		Wait Until Keyword Succeeds	5x	2s	SUITE:Test Log	${SERVER_PATH}	${SERVER_FILE}
		Close Connection
		###
		CLI:Switch Connection	default
	END

SUITE:Test Log
	[Arguments]	${PATH}	${FILE}
	${OUTPUT}=	CLI:Write	cat ${PATH}/${FILE}
	Run Keyword If	'${NGVERSION}' >= '4.0'	Should Contain	${OUTPUT}	42518.2.1.1.120.0.200
	Run Keyword If	'${NGVERSION}' < '4.0'	Should Contain	${OUTPUT}	42518.100.120.0.200

SUITE:Truncate Log File
	[Arguments]	${PATH}	${FILE}
	${OUTPUT}=	CLI:Write	echo -n > ${PATH}/${FILE}

SUITE:Setup Valid Email
	CLI:Enter Path	/settings/auditing/destinations/email
	CLI:Set	email_server=${EMAIL_SERVER} email_port=${EMAIL_PORT} username=${EMAIL_ACCOUNT} password=${EMAIL_PASSWD} confirm_password=${EMAIL_PASSWD} destination_email=${EMAIL_DESTINATION} start_tls=yes
	CLI:Commit

SUITE:Reverting Email Configuration
	CLI:Enter Path	/settings/auditing/destinations/email
	CLI:Write	set start_tls=yes email_port=${EMAIL_PORT} email_server=${EMPTY} username=${EMPTY} password=${EMPTY} confirm_password=${EMPTY} destination_email=${EMPTY}
	CLI:Commit

SUITE:Check Email Server
	CLI:Open	USERNAME=${EMAIL_ACCOUNT}	PASSWORD=${EMAIL_PASSWD}	session_alias=mail_server	TYPE=shell	HOST_DIFF=${EMAIL_SERVER}
	Write	echo 1 | mail
	${OUTPUT}=	CLI:Read Until Prompt
	Should Not Contain	${OUTPUT}	No mail for jamie
	@{LINES}=	Split To Lines	${OUTPUT}
	${LINE}=	Set Variable	${LINES}[1]
	@{NUMBERS}=	Split String	${LINE}	${SPACE}
	${NUM}=	Set Variable	${NUMBERS}[1]
	${NUM}=	Convert To Integer	${NUM}
	${NUMBER}=	Evaluate	${NUM}-1
	Write	echo ${NUMBER} | mail
	${OUTPUT}=	CLI:Read Until Prompt
	@{LIST}=	Create List	Event ID 201:	From: ${USER}@${NGHOSTNAME_TEST}
	Run Keyword If	${NGVERSION} >= 4.1	Append To List	${LIST}	SUBJECT:Nodegrid Event Notification
	Run Keyword If	${NGVERSION} == 4.0	Append To List	${LIST}	SUBJECT:NodeGrid Manager Event Notification
	FOR		${INDEX}	IN	@{LIST}
		Should Contain	${OUTPUT}	${INDEX}
	END