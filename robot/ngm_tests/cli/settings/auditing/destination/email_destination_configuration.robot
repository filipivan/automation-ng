*** Settings ***
Resource	../../../init.robot
Documentation	Tests Auditing Destination section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	DEPENDENCE_EMAIL
Default Tags	CLI	SSH	AUDITING	DESTINATION	EMAIL
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EMAIL_SERVER}	smtp.gmail.com
${EMAIL_ACCOUNT}	zpe.brazil@gmail.com
${EMAIL_PORT}	587
${EMAIL_DESTINATION}	zpe.brazil@gmail.com
${EMAIL_PASSWD}	ZpeSystems

${PORT}	25
${LIMIT_PORT}	65536
${UNNVAILABLE_PORT}	1023

*** Test Cases ***
Test for test_mail with email_port=65536
	[Setup]	SUITE:Setup Valid Email
	CLI:Enter Path	/settings/auditing/destinations/email
	CLI:Set Field	email_port	${LIMIT_PORT}
	CLI:Commit
	${OUTPUT}=	CLI:Write	test_email	Raw	yes
	Should Contain	${OUTPUT}	Error: ssmtp:
	[Teardown]	SUITE:Reverting Test

Test for test_mail with email_port=1023
	[Setup]	SUITE:Setup Valid Email
	CLI:Enter Path	/settings/auditing/destinations/email
	CLI:Set Field	email_port	${UNNVAILABLE_PORT}
	CLI:Commit
	${OUTPUT}=	CLI:Write	test_email	Raw	yes
	Should Contain	${OUTPUT}	Error: ssmtp:
	[Teardown]	SUITE:Reverting Test

Test For Email With Sender
    [Tags]              EXCLUDEIN3_2
    [Setup]             SUITE:Setup Valid Email
    CLI:Enter Path      /settings/auditing/destinations/email
    CLI:Write           test_email
    [Teardown]          SUITE:Reverting Test

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	SUITE:Reverting Test
	CLI:Close Connection

SUITE:Setup Valid Email
	CLI:Enter Path	/settings/auditing/destinations/email
	CLI:Set	email_server=${EMAIL_SERVER2} email_port=${EMAIL_PORT} username=${EMAIL_ACCOUNT2} password=${EMAIL_PASSWD2} confirm_password=${EMAIL_PASSWD2} destination_email=${EMAIL_DESTINATION2} start_tls=yes
	CLI:Commit

SUITE:Reverting Test
	CLI:Enter Path	/settings/auditing/destinations/email
	CLI:Write	set start_tls=yes email_port=25 email_server=${EMPTY} username=${EMPTY} password=${EMPTY} confirm_password=${EMPTY} destination_email=${EMPTY}
    Run Keyword If  '${NGVERSION}' >= '4.2'  CLI:Set  sender=${EMPTY}
    CLI:Commit

SUITE:Setup Valid Email2 With Sender
    CLI:Enter Path  /settings/auditing/destinations/email
    CLI:Set         email_server=${EMAIL_SERVER2} email_port=${EMAIL_PORT2} username=${EMAIL_ACCOUNT2} start_tls=yes
    CLI:Set         password=${EMAIL_PASSWD2} confirm_password=${EMAIL_PASSWD2} destination_email=${EMAIL_DESTINATION2}
    CLI:Set         sender="te$t w!th sp@ce"
    CLI:Commit
