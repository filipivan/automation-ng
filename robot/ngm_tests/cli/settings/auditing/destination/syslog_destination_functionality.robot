*** Settings ***
Resource	../../../init.robot
Documentation	Tests Auditing Destination section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	AUDITING	DESTINATION
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${ALIAS}	syslog_connection
${SERVERPRESENT}	${SYSLOGSERVER2PRESENT}
${SERVER}	${SYSLOGSERVER2}
${SERVERUSER}	${SYSLOGSERVER2USER}
${SERVERPASSWORD}	${SYSLOGSERVER2PASSWORD2}

${SERVER_PATH}	${SYSLOGPATH}
${SERVER_FILE}	${SYSLOGEVENTSFILE2}
${EVENT_FACILITY}	${EVENTFACILITY}

${USER}	${DEFAULT_USERNAME}
${EVENT_NUMBER}	200
${EVENT200}	A user logged into the system. User: ${USER}@${CLIENTIP}. Session type: SSH. Authentication Method: Local.
${EVENT_DESCRIPTION}	${EVENT200}

*** Test Cases ***
Test Remote Syslog Server Event Notification
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical Syslog Server available. Test will be skipped.
	CLI:Open	${SERVERUSER}	${SERVERPASSWORD}	${ALIAS}	TYPE=root	HOST_DIFF=${SERVER}
	SUITE:Truncate Log File	${SERVER_PATH}	${SERVER_FILE}
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/auditing/destinations/syslog/
	CLI:Set Field	event_facility	${EVENT_FACILITY}
	CLI:Write	set ipv4_remote_server=yes ipv4_address=${SERVER}
	CLI:Commit
	CLI:Open	session_alias=eventsession	startping=no
	CLI:Write	ls
	Close Connection
	CLI:Switch Connection	${ALIAS}
	Wait Until Keyword Succeeds	5x	2s	SUITE:Test Log	${SERVER_PATH}	${SERVER_FILE}	${EVENT_NUMBER}	${EVENT_DESCRIPTION}

Test admin/root session Event Notification
	CLI:Switch Connection	default
	SUITE:Change admin_session
	CLI:Open	session_alias=eventsession	startping=no
	Close Connection
	CLI:Switch Connection	default
	Wait Until Keyword Succeeds	5x	1s	SUITE:Test admin_session	${EVENT_NUMBER}	${EVENT_DESCRIPTION}
	CLI:Write	\n
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	5x	1s	SUITE:Test admin_session	${EVENT_NUMBER}	${EVENT_DESCRIPTION}
	CLI:Write	\n
	[Teardown]	SUITE:Change admin_session	no

*** Keywords ***
SUITE:Setup
	CLI:Connect As Root
	CLI:Open
	CLI:Enable All Events	yes	syslog
	SUITE:Change admin_session	no

SUITE:Teardown
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/auditing/destinations/syslog
	CLI:Set	admin_session=no ipv4_remote_server=no system_console=yes
	CLI:Commit
	CLI:Close Connection

SUITE:Truncate Log File
	[Arguments]	${PATH}	${FILE}
	${OUTPUT}=	CLI:Write	echo -n > ${PATH}/${FILE}

SUITE:Test Log
	[Arguments]	${PATH}	${FILE}	${EVENTID}	${EVENTDESCRIPTION}
	${OUTPUT}=	CLI:Write	cat ${PATH}/${FILE}
	Should Contain	${OUTPUT}	Event ID ${EVENTID}: ${EVENTDESCRIPTION}

SUITE:Test admin_session
	[Arguments]	${EVENTID}	${EVENTDESCRIPTION}
#	${OUTPUT}=	CLI:Write	\n
#	${OUTPUT}=	CLI:Read Until Prompt
	${OUTPUT}=	Read	delay=1s
	Log	${OUTPUT}	console=yes
	Should Contain	${OUTPUT}	Event ID ${EVENTID}: ${EVENTDESCRIPTION}

SUITE:Change admin_session
	[Arguments]	${VALUE}=yes
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/auditing/destinations/syslog/
	CLI:Set Field	admin_session	${VALUE}
	CLI:Commit