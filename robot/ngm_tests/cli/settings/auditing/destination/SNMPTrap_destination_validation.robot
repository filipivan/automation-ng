*** Settings ***
Resource	../../../init.robot
Documentation	Tests Auditing Destination section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	AUDITING	DESTINATION
Suite Setup	SUITE:SNMP Start
Suite Teardown	SUITE:SNMP Teardown

*** Test Cases ***
Test SNMPTrap Destination available commands after send tab-tab
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Test Available Commands	cd	change_password	commit	event_system_audit	event_system_clear	exit	hostname	ls	pwd	quit	reboot	revert	set	shell	show	show_settings	shutdown	whoami

Test SNMPTrap Destination visible fields for show command
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set Field	snmptrap_server	127.0.0.1
	CLI:Set Field	snmptrap_version	version_2c
	CLI:Set Field	snmptrap_community	RESET
	CLI:Test Show Command	snmptrap_server	snmptrap_transport_protocol	snmptrap_port	snmptrap_version	snmptrap_community
	[Teardown]	CLI:Revert

Test SNMPTrap Destination available fields to set command
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set Field	snmptrap_server	127.0.0.1
	CLI:Set Field	snmptrap_version	version_2c
	CLI:Set Field	snmptrap_community	RESET
	CLI:Test Set Available Fields	snmptrap_community	snmptrap_port	snmptrap_server	snmptrap_transport_protocol	snmptrap_version
	[Teardown]	CLI:Revert

#snmptrap_community
Test correct values for field=snmptrap_community
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set Field	snmptrap_server	127.0.0.1
	CLI:Set Field	snmptrap_version	version_2c
	CLI:Test Set Field Valid Options	snmptrap_community	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa	revert=no
	CLI:Test Set Field Valid Options	snmptrap_community	public	revert=no
	CLI:Test Set Field Valid Options	snmptrap_community	private	revert=no
	CLI:Test Set Field Valid Options	snmptrap_community	1.1.1.1	revert=no
	CLI:Test Set Field Valid Options	snmptrap_community	abcdefghijklmnopqrstuvwxyz	revert=no
	CLI:Test Set Field Valid Options	snmptrap_community	-0123456789	revert=no
	CLI:Test Set Field Valid Options	snmptrap_community	/?:@=&$-_.+!*(),{}[]|#%^/\	revert=no
	CLI:Test Set Field Valid Options	snmptrap_community	${EMPTY}	revert=no
	[Teardown]	CLI:Revert

Test incorrect values for field=snmptrap_community
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
#	CLI:Test Set Field Invalid Options	snmptrap_community	${EMPTY}	Error: snmptrap_server: Validation error.
	CLI:Set Field	snmptrap_server	127.0.0.1
	CLI:Set Field	snmptrap_version	version_2c
	CLI:Test Set Field Invalid Options	snmptrap_community	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa	Error: snmptrap_community: Exceeded the maximum size for this field.
#	CLI:Test Set Field Invalid Options	snmptrap_community	private	Error: snmptrap_server: Validation error.
	[Teardown]	CLI:Revert

Test for commands after Error for field=snmptrap_community
	CLI:Set Field	snmptrap_server	127.0.0.1
	CLI:Set Field	snmptrap_version	version_2c
	CLI:Commands After Set Error	snmptrap_community	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa	cd	revert	show	ls	set
	[Teardown]	CLI:Revert

#snmptrap_server
Test correct values for field=snmptrap_server
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Test Set Field Valid Options	snmptrap_server	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
	CLI:Test Set Field Valid Options	snmptrap_server	www.sample-server.com
	CLI:Test Set Field Valid Options	snmptrap_server	1.1.1.1
	CLI:Test Set Field Valid Options	snmptrap_server	fd38:24b8:1110:7777::5
	CLI:Test Set Field Valid Options	snmptrap_server	abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-0123456789
	[Teardown]	CLI:Revert

Test incorrect values for field=snmptrap_server
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Test Set Field Invalid Options	snmptrap_server	${EMPTY}	Error: snmptrap_server: Validation error.
	CLI:Test Set Field Invalid Options	snmptrap_server	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa	Error: snmptrap_server: Validation error.
	CLI:Test Set Field Invalid Options	snmptrap_server	_	Error: snmptrap_server: Validation error.
	CLI:Test Set Field Invalid Options	snmptrap_server	@	Error: snmptrap_server: Validation error.

Test for commands after Error for field=snmptrap_server
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Commands After Set Error	snmptrap_server	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa	cd	revert	show	ls	set
	[Teardown]	CLI:Revert

#snmptrap_version
Test available options for field=snmptrap_version
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set Field	snmptrap_server	127.0.0.1
	CLI:Test Set Field Options	snmptrap_version	version_2c
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Write	set snmptrap_version=version_3 snmptrap_server=1
	Run Keyword If	'${NGVERSION}' == '3.2'	Set Tags	NON-CRITICAL
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Write	set snmptrap_version=version_3 snmptrap_security_level=authNoPriv
	[Teardown]	CLI:Revert

Test incorrect values for field=snmptrap_version
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	snmptrap_version	${EMPTY}	Error: Missing value for parameter: snmptrap_version
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	snmptrap_version	${EMPTY}	Error: Missing value: snmptrap_version
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	snmptrap_version	a	Error: Invalid value: a for parameter: snmptrap_version
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	snmptrap_version	a	Error: Invalid value: a

Test for commands after Error for field=snmptrap_version
	CLI:Commands After Set Error	snmptrap_server	${EMPTY}	cd	revert	show	ls	set
	[Teardown]	CLI:Revert

#snmptrap_security_level
Test available options for field=snmptrap_security_level
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Write	set snmptrap_server=127.0.0.1 snmptrap_version=version_3 snmptrap_security_level=authNoPriv
	CLI:Test Set Field Options	snmptrap_security_level	authNoPriv	authPriv	noAuthNoPriv
	[Teardown]	CLI:Revert

Test incorrect values for field=snmptrap_security_level
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Write	set snmptrap_server=127.0.0.1 snmptrap_version=version_3 snmptrap_security_level=authNoPriv
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	snmptrap_security_level	${EMPTY}	Error: Missing value for parameter: snmptrap_security_level
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	snmptrap_security_level	${EMPTY}	Error: Missing value: snmptrap_security_level
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	snmptrap_security_level	a	Error: Invalid value: a for parameter: snmptrap_security_level
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	snmptrap_security_level	a	Error: Invalid value: a
	[Teardown]	CLI:Revert

Test for commands after Error for field=snmptrap_security_level
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Write	set snmptrap_server=127.0.0.1 snmptrap_version=version_3 snmptrap_security_level=authNoPriv
	CLI:Commands After Set Error	snmptrap_security_level	${EMPTY}	cd	revert	show	ls	set
	[Teardown]	CLI:Revert

Test available fields to set command after snmptrap_security_level is set
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set Field	snmptrap_server	127.0.0.1
	CLI:Write	set snmptrap_version=version_3 snmptrap_security_level=authNoPriv
	CLI:Test Set Available Fields	snmptrap_authentication	snmptrap_privacy_algo	snmptrap_server	snmptrap_version	snmptrap_authentication_password	snmptrap_privacy_passphrase	snmptrap_transport_protocol	snmptrap_port	snmptrap_security_level	snmptrap_user
	[Teardown]	CLI:Revert

##snmptrap_authentication
Test correct values for field=snmptrap_authentication
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set Field	snmptrap_server	127.0.0.1
	CLI:Write	set snmptrap_version=version_3 snmptrap_security_level=authNoPriv
	CLI:Test Set Field Options	snmptrap_authentication	md5	sha
	[Teardown]	CLI:Revert

Test incorrect values for field=snmptrap_authentication
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set Field	snmptrap_server	127.0.0.1
	CLI:Write	set snmptrap_version=version_3 snmptrap_security_level=authNoPriv
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	snmptrap_authentication	${EMPTY}	Error: Missing value for parameter: snmptrap_authentication
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	snmptrap_authentication	${EMPTY}	Error: Missing value: snmptrap_authentication
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	snmptrap_authentication	a	Error: Invalid value: a for parameter: snmptrap_authentication
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	snmptrap_authentication	a	Error: Invalid value: a
	[Teardown]	CLI:Revert

Test for commands after Error for field=snmptrap_authentication
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Write	set snmptrap_server=127.0.0.1 snmptrap_version=version_3 snmptrap_security_level=authNoPriv
	CLI:Commands After Set Error	snmptrap_authentication	${EMPTY}	cd	revert	show	ls	set
	[Teardown]	CLI:Revert

##snmptrap_privacy_algo
Test correct values for field=snmptrap_privacy_algo
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set Field	snmptrap_server	127.0.0.1
	CLI:Write	set snmptrap_version=version_3 snmptrap_security_level=authNoPriv
	CLI:Test Set Field Options	snmptrap_privacy_algo	aes	des
	[Teardown]	CLI:Revert

Test incorrect values for field=snmptrap_privacy_algo
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set Field	snmptrap_server	127.0.0.1
	CLI:Write	set snmptrap_version=version_3 snmptrap_security_level=authNoPriv
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	snmptrap_privacy_algo	${EMPTY}	Error: Missing value for parameter: snmptrap_privacy_algo
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	snmptrap_privacy_algo	${EMPTY}	Error: Missing value: snmptrap_privacy_algo
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	snmptrap_privacy_algo	a	Error: Invalid value: a for parameter: snmptrap_privacy_algo
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	snmptrap_privacy_algo	a	Error: Invalid value: a
	[Teardown]	CLI:Revert

Test for commands after Error for field=snmptrap_privacy_algo
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Write	set snmptrap_server=127.0.0.1 snmptrap_version=version_3 snmptrap_security_level=authNoPriv
	CLI:Commands After Set Error	snmptrap_privacy_algo	${EMPTY}	cd	revert	show	ls	set
	[Teardown]	CLI:Revert

##snmptrap_authentication_password
Test correct values for field=snmptrap_authentication_password
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set Field	snmptrap_server	127.0.0.1
	CLI:Write	set snmptrap_version=version_3 snmptrap_security_level=authNoPriv
	CLI:Test Set Field Valid Options	snmptrap_authentication_password	${EMPTY}	revert=no
	CLI:Test Set Field Valid Options	snmptrap_authentication_password	password	revert=no	passwordfield=yes
	CLI:Test Set Field Valid Options	snmptrap_authentication_password	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa	revert=no	passwordfield=yes
	CLI:Test Set Field Valid Options	snmptrap_authentication_password	abcdefghijklmnopqrstuvwxyz	revert=no	passwordfield=yes
	CLI:Test Set Field Valid Options	snmptrap_authentication_password	-0123456789	revert=no	passwordfield=yes
	CLI:Test Set Field Valid Options	snmptrap_authentication_password	?@$-_.+!*(),%^\:/={}&[]#	revert=no	passwordfield=yes
	[Teardown]	CLI:Revert

Test incorrect values for field=snmptrap_authentication_password
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set Field	snmptrap_server	127.0.0.1
	CLI:Write	set snmptrap_version=version_3 snmptrap_security_level=authNoPriv
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	snmptrap_authentication_password	a	Error: snmptrap_authentication_password: This field should have at least 8 characters and less than 64 and should not begin with #.	revert=no
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	snmptrap_authentication_password	aaaaaaa	Error: snmptrap_authentication_password: This field should have at least 8 characters and less than 64 and should not begin with #.	revert=no
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	snmptrap_authentication_password	\#password	Error: snmptrap_authentication_password: This field should have at least 8 characters and less than 64 and should not begin with #.	revert=no
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	snmptrap_authentication_password	a	Error: snmptrap_authentication_password: This field should have at least 8 characters and less than 64 and should not begin with #.	revert=no
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	snmptrap_authentication_password	aaaaaaa	Error: snmptrap_authentication_password: This field should have at least 8 characters and less than 64 and should not begin with #.	revert=no
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	snmptrap_authentication_password	\#password	Error: snmptrap_authentication_password: This field should have at least 8 characters and less than 64 and should not begin with #.	revert=no
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	snmptrap_authentication_password	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa	Error: snmptrap_authentication_password: This field should have at least 8 characters and less than 64 and should not begin with #.	revert=no
	[Teardown]	CLI:Revert

Test for commands after Error for field=snmptrap_authentication_password
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Write	set snmptrap_server=127.0.0.1 snmptrap_version=version_3 snmptrap_security_level=authNoPriv
	CLI:Commands After Set Error	snmptrap_authentication_password	a	cd	revert	show	ls	set
	[Teardown]	CLI:Revert

##snmptrap_privacy_passphrase
Test correct values for field=snmptrap_privacy_passphrase
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set Field	snmptrap_server	127.0.0.1
	CLI:Write	set snmptrap_version=version_3 snmptrap_security_level=authNoPriv
	CLI:Test Set Field Valid Options	snmptrap_privacy_passphrase	${EMPTY}	revert=no
	CLI:Test Set Field Valid Options	snmptrap_privacy_passphrase	password	revert=no	passwordfield=yes
	CLI:Test Set Field Valid Options	snmptrap_privacy_passphrase	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa	passwordfield=yes	revert=no
	CLI:Test Set Field Valid Options	snmptrap_privacy_passphrase	abcdefghijklmnopqrstuvwxyz	revert=no	passwordfield=yes
	CLI:Test Set Field Valid Options	snmptrap_privacy_passphrase	-0123456789	revert=no	passwordfield=yes
	CLI:Test Set Field Valid Options	snmptrap_privacy_passphrase	?@$-_.+!*(),%^\:/={}&[]#	revert=no	passwordfield=yes
	[Teardown]	CLI:Revert

Test incorrect values for field=snmptrap_privacy_passphrase
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set Field	snmptrap_server	127.0.0.1
	CLI:Write	set snmptrap_version=version_3 snmptrap_security_level=authNoPriv
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	snmptrap_privacy_passphrase	a	Error: snmptrap_privacy_passphrase: This field should have at least 8 characters and less than 64 and should not begin with #.	revert=no
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	snmptrap_privacy_passphrase	aaaaaaa	Error: snmptrap_privacy_passphrase: This field should have at least 8 characters and less than 64 and should not begin with #.	revert=no
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	snmptrap_privacy_passphrase	\#password	Error: snmptrap_privacy_passphrase: This field should have at least 8 characters and less than 64 and should not begin with #.	revert=no
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	snmptrap_privacy_passphrase	a	Error: snmptrap_privacy_passphrase: This field should have at least 8 characters and less than 64 and should not begin with #.	revert=no
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	snmptrap_privacy_passphrase	aaaaaaa	Error: snmptrap_privacy_passphrase: This field should have at least 8 characters and less than 64 and should not begin with #.	revert=no
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	snmptrap_privacy_passphrase	\#password	Error: snmptrap_privacy_passphrase: This field should have at least 8 characters and less than 64 and should not begin with #.	revert=no
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	snmptrap_privacy_passphrase	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa	Error: snmptrap_privacy_passphrase: This field should have at least 8 characters and less than 64 and should not begin with #.	revert=no
	[Teardown]	CLI:Revert

Test for commands after Error for field=snmptrap_privacy_passphrase
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Write	set snmptrap_server=127.0.0.1 snmptrap_version=version_3 snmptrap_security_level=authNoPriv
	CLI:Commands After Set Error	snmptrap_privacy_passphrase	a	cd	revert	show	ls	set
	[Teardown]	CLI:Revert

##snmptrap_user
Test correct values for field=snmptrap_user
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set Field	snmptrap_server	127.0.0.1
	CLI:Write	set snmptrap_version=version_3 snmptrap_security_level=authNoPriv
	CLI:Test Set Field Valid Options	snmptrap_user	username	revert=no
	CLI:Test Set Field Valid Options	snmptrap_user	a	revert=no
	CLI:Test Set Field Valid Options	snmptrap_user	1.1.1.1	revert=no
	CLI:Test Set Field Valid Options	snmptrap_user	abcdefghijklmnopqrstuvwxyz	revert=no
	CLI:Test Set Field Valid Options	snmptrap_user	-0123456789	revert=no
	CLI:Test Set Field Valid Options	snmptrap_user	?@$-_.+!*(),%^\	revert=no
	CLI:Test Set Field Valid Options	snmptrap_user	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa	revert=no
	[Teardown]	CLI:Revert

Test incorrect values for field=snmptrap_user
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set Field	snmptrap_server	127.0.0.1
	CLI:Write	set snmptrap_version=version_3 snmptrap_security_level=authNoPriv
	CLI:Test Set Field Invalid Options	snmptrap_user	${EMPTY}	Error: snmptrap_user: Validation error.	revert=no
	CLI:Test Set Field Invalid Options	snmptrap_user	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa	Error: snmptrap_user: Exceeded the maximum size for this field.	revert=no
	CLI:Test Set Field Invalid Options	snmptrap_user	:	Error: snmptrap_user: Validation error.	revert=no
	CLI:Test Set Field Invalid Options	snmptrap_user	/	Error: snmptrap_user: Validation error.	revert=no
	CLI:Test Set Field Invalid Options	snmptrap_user	=	Error: snmptrap_user: Validation error.	revert=no
	CLI:Test Set Field Invalid Options	snmptrap_user	&	Error: snmptrap_user: Validation error.	revert=no
	CLI:Test Set Field Invalid Options	snmptrap_user	{	Error: snmptrap_user: Validation error.	revert=no
	CLI:Test Set Field Invalid Options	snmptrap_user	}	Error: snmptrap_user: Validation error.	revert=no
	CLI:Test Set Field Invalid Options	snmptrap_user	[	Error: snmptrap_user: Validation error.	revert=no
	CLI:Test Set Field Invalid Options	snmptrap_user	]	Error: snmptrap_user: Validation error.	revert=no
	CLI:Test Set Field Invalid Options	snmptrap_user	a#	Error: snmptrap_user: Validation error.	revert=no
	[Teardown]	CLI:Revert

Test for commands after Error for field=snmptrap_user
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Write	set snmptrap_server=127.0.0.1 snmptrap_version=version_3 snmptrap_security_level=authNoPriv
	CLI:Commands After Set Error	snmptrap_user	${EMPTY}	cd	revert	show	ls	set
	[Teardown]	CLI:Revert

#snmptrap_port
Test correct values for field=snmptrap_port
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set Field	snmptrap_server	127.0.0.1
	CLI:Test Set Field Valid Options	snmptrap_port	161	revert=no
	CLI:Test Set Field Valid Options	snmptrap_port	0	revert=no
	CLI:Test Set Field Valid Options	snmptrap_port	99999	revert=no
	[Teardown]	CLI:Revert

Test incorrect values for field=snmptrap_port
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set Field	snmptrap_server	127.0.0.1
	CLI:Test Set Field Invalid Options	snmptrap_port	${EMPTY}	Error: snmptrap_port: Validation error.	revert=no
	CLI:Test Set Field Invalid Options	snmptrap_port	-1	Error: snmptrap_port: Validation error.	revert=no
	CLI:Test Set Field Invalid Options	snmptrap_port	abc	Error: snmptrap_port: Validation error.	revert=no
	CLI:Test Set Field Invalid Options	snmptrap_port	1.0	Error: snmptrap_port: Validation error.	revert=no
	CLI:Test Set Field Invalid Options	snmptrap_port	1.	Error: snmptrap_port: Validation error.	revert=no
	CLI:Test Set Field Invalid Options	snmptrap_port	100000	Error: snmptrap_port: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test for commands after Error for field=snmptrap_port
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Write	set snmptrap_server=127.0.0.1 snmptrap_version=version_3 snmptrap_security_level=authNoPriv
	CLI:Commands After Set Error	snmptrap_port	${EMPTY}	cd	revert	show	ls	set
	[Teardown]	CLI:Revert

#snmptrap_transport_protocol
Test available options for field=snmptrap_transport_protocol
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set Field	snmptrap_server	127.0.0.1
	CLI:Test Set Field Options	snmptrap_transport_protocol	tcp-ipv4	tcp-ipv6	udp-ipv4	udp-ipv6
	[Teardown]	CLI:Revert

Test incorrect values for field=snmptrap_transport_protocol
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	snmptrap_transport_protocol	${EMPTY}	Error: Missing value for parameter: snmptrap_transport_protocol
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	snmptrap_transport_protocol	${EMPTY}	Error: Missing value: snmptrap_transport_protocol
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	snmptrap_transport_protocol	a	Error: Invalid value: a for parameter: snmptrap_transport_protocol
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	snmptrap_transport_protocol	a	Error: Invalid value: a
	[Teardown]	CLI:Revert

Test for commands after Error for field=snmptrap_transport_protocol
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Write	set snmptrap_server=127.0.0.1 snmptrap_version=version_3 snmptrap_security_level=authNoPriv
	CLI:Commands After Set Error	snmptrap_transport_protocol	${EMPTY}	cd	revert	show	ls	set
	[Teardown]	CLI:Revert

*** Keywords ***
SUITE:SNMP Start
	CLI:Open
	SUITE:Enable SNMP Events

SUITE:SNMP Teardown
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set Field	snmptrap_server	127.0.0.1
	CLI:Set Field	snmptrap_transport_protocol	udp-ipv4
	CLI:Set Field	snmptrap_port	161
	CLI:Set Field	snmptrap_version	version_2c
	CLI:Set Field	snmptrap_community	public
	CLI:Commit
	CLI:Close Connection

SUITE:Enable SNMP Events
	CLI:Enter Path	/settings/auditing/events/snmp_trap/
	CLI:Set Field	aaa_events	yes
	CLI:Set Field	device_events	yes
	CLI:Set Field	logging_events	yes
	CLI:Set Field	system_events	yes
	CLI:Commit