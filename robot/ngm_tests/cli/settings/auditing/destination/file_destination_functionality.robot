*** Settings ***
Resource	../../../../init.robot
Documentation	Test System Events section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	DEPENDENCE_NFS	DEPENDENCE_SNMP	SERVER
Default Tags	AUDITING	DESTINATION	EVENT	NFSSERVER	SNMPSERVER

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${ALIAS}	nfs_connection
${NFSSERVER_PRESENT}	${FALSE}
${NFSSERVER_HOST}	${NFSSERVER2}
${NFSSERVER_USER}	${NFSSERVER2USER}
${NFSSERVER_PASSWORD}	${NFSSERVER2PASSWORD}

${START_NFS}	${FALSE}
${NFSSERVER_PATH}	${NFSPATH}

${SNMPSERVER_HOST}	${SNMPSERVER}
${SNMPSERVER_COMMUNITY}	${SNMPCOMMUNITY}
${SNMPSERVER_PORT}	${SNMPTCPPORT}

${NG_HOSTNAME}	${HOSTNAME_NODEGRID}
${USER}	${DEFAULT_USERNAME}
${EVENT_NUMBER}	200
${EVENT200}	A user logged into the system. User: ${USER}@${CLIENTIP}. Session type: SSH. Authentication Method: Local.
${EVENT_DESCRIPTION}	${EVENT200}

*** Test Cases ***
Test local file destination
	CLI:Switch Connection	default
	CLI:Open	session_alias=event_session	startping=no
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	1m	1s	CLI:Test Event ID Logged	${NG_HOSTNAME}	${EVENT_NUMBER}	${EVENT_DESCRIPTION}
	[Teardown]	Run Keyword If Test Passed	Run Keywords	CLI:Switch Connection	event_session
	...	AND	CLI:Close Current Connection

Test nfs file destination
	Skip If	'${NFSSERVER_PRESENT}' == 'No'	No Physical NFS Server available. Test will be skipped.
	CLI:Open	${NFSSERVER_USER}	${NFSSERVER_PASSWORD}	session_alias=${ALIAS}	HOST_DIFF=${NFSSERVER_HOST}
	CLI:Write	sudo rm -R ${NFSSERVER_PATH}/*	#${NFSSERVER_PATH}
	${OUTPUT}=	CLI:Write	ls ${NFSSERVER_PATH}
	Should Not Contain Any	${OUTPUT}	DB	EVT	SNR

	Wait Until Keyword Succeeds	10x	1s	CLI:Clear Event Logfile	${NG_HOSTNAME}
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/services/
	CLI:Set Field	enable_rpc	yes
	CLI:Commit
	CLI:Enable Auditing Destination As NFS	${NFSSERVER_HOST}	${NFSSERVER_PATH}

	CLI:Switch Connection	default
	CLI:Open	session_alias=event_session	startping=no
	CLI:Switch Connection	${ALIAS}
	Wait Until Keyword Succeeds	1m	1s	CLI:Test NFS Event ID Logged	${NFSSERVER_PATH}	${NG_HOSTNAME}	${EVENT_NUMBER}	${EVENT_DESCRIPTION}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	1m	1s	CLI:Test Event ID Not Logged	${NG_HOSTNAME}	${EVENT_NUMBER}
	[Teardown]	Run Keyword If Test Passed	Run Keywords	CLI:Switch Connection	event_session
	...	AND	CLI:Close Current Connection

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Connect As Root
	CLI:Switch Connection	default
	CLI:Enable Auditing Destination To Events
	CLI:Enable All Events
	Wait Until Keyword Succeeds	10x	1s	CLI:Clear Event Logfile	${NG_HOSTNAME}

SUITE:Teardown
	CLI:Switch Connection	default
	CLI:Enable All Events
	CLI:Enable Auditing Destination To Events
	CLI:Close Connection
