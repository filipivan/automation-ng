*** Settings ***
Resource	../../../init.robot
Documentation	Tests Auditing Destination section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	AUDITING	DESTINATION	EMAIL
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
@{ALL_VALUES}=          ${WORD}     ${ONE_WORD}     ${TWO_WORD}     ${THREE_WORD}
                        ...     ${SEVEN_WORD}   ${EIGHT_WORD}   ${ELEVEN_WORD}  ${NUMBER}   ${ONE_NUMBER}
                        ...     ${TWO_NUMBER}   ${THREE_NUMBER}     ${SEVEN_NUMBER}     ${EIGHT_NUMBER}
                        ...     ${ELEVEN_NUMBER}    ${WORD_AND_NUMBER}  ${NUMBER_AND_WORD}  ${POINTS}
                        ...     ${ONE_POINTS}   ${TWO_POINTS}   ${THREE_POINTS}	${SEVEN_POINTS}
                        ...     ${EIGHT_POINTS}     ${ELEVEN_POINTS}    ${WORD_AND_POINTS}  ${NUMBER_AND_POINTS}
                        ...     ${POINTS_AND_NUMBER}    ${POINTS_AND_WORD}  ${EMPTY}   te$t w!th sp@ce

*** Test Cases ***
Test Email Destination available commands after send tab-tab
	CLI:Enter Path	/settings/auditing/destinations/email
	CLI:Test Available Commands	cd	change_password	commit	event_system_audit	event_system_clear	exit	hostname
	...	ls	pwd	quit	reboot	revert	set	shell	show	show_settings	shutdown	whoami	test_email

Test email Destination visible fields for show command
	CLI:Enter Path	/settings/auditing/destinations/email
	CLI:Test Show Command	email_server	email_port	username	password	confirm_password	destination_email
	...	start_tls
    Run Keyword If  '${NGVERSION}' >= '4.2'  CLI:Test Show Command  sender

Test Email Destination available fields to set command
	CLI:Enter Path	/settings/auditing/destinations/email
	CLI:Test Set Available Fields	email_server	email_port	username	password	confirm_password
	...	destination_email	start_tls
    Run Keyword If  '${NGVERSION}' >= '4.2'  CLI:Test Show Command  sender

Test with field=email_server
	CLI:Enter Path	/settings/auditing/destinations/email
	CLI:Test Set Validate Invalid Options	email_server	${EMPTY}
	CLI:Test Set Validate Invalid Options	email_server	${WORD}
	CLI:Test Set Validate Invalid Options	email_server	${NUMBER}
	CLI:Test Set Validate Invalid Options	email_server	${POINTS}	Error: email_server: Validation error.
	CLI:Test Set Validate Invalid Options	email_server	${ONE_WORD}
	CLI:Test Set Validate Invalid Options	email_server	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	email_server	${ONE_POINTS}	Error: email_server: Validation error.
	CLI:Test Set Validate Invalid Options	email_server	${TWO_WORD}
	CLI:Test Set Validate Invalid Options	email_server	${TWO_NUMBER}
	CLI:Test Set Validate Invalid Options	email_server	${TWO_POINTS}	Error: email_server: Validation error.
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	email_server	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument for command: set
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	email_server	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument: set
	CLI:Test Set Validate Invalid Options	email_server	${EXCEEDED}	Error: email_server: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert	Raw

Test with field=email_port
	CLI:Enter Path	/settings/auditing/destinations/email
	CLI:Test Set Validate Invalid Options	email_port	${EMPTY}	Error: email_port: Validation error.
	CLI:Test Set Validate Invalid Options	email_port	${WORD}	  Error: email_port: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	email_port	${NUMBER}	Error: email_port: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	email_port	${POINTS}	Error: email_port: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	email_port	${ONE_WORD}	Error: email_port: Validation error.
	CLI:Test Set Validate Invalid Options	email_port	${ONE_NUMBER}
	CLI:Test Set Validate Invalid Options	email_port	${ONE_POINTS}	Error: email_port: Validation error.
	CLI:Test Set Validate Invalid Options	email_port	${TWO_WORD}	Error: email_port: Validation error.
	CLI:Test Set Validate Invalid Options	email_port	${TWO_NUMBER}
	CLI:Test Set Validate Invalid Options	email_port	${TWO_POINTS}	Error: email_port: Validation error.

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	email_port	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument for command: set
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	email_port	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument: set

	CLI:Test Set Validate Invalid Options	email_port	${EXCEEDED}	Error: email_port: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	email_port	-1	Error: email_port: Validation error.
	CLI:Test Set Validate Invalid Options	email_port	abc	Error: email_port: Validation error.
	CLI:Test Set Validate Invalid Options	email_port	1.0	Error: email_port: Validation error.
	CLI:Test Set Validate Invalid Options	email_port	1.	Error: email_port: Validation error.
	CLI:Test Set Validate Invalid Options	email_port	100000	Error: email_port: Exceeded the maximum size for this field.

	CLI:Test Set Field Valid Options	email_port	25
	CLI:Test Set Field Valid Options	email_port	587
	CLI:Test Set Field Valid Options	email_port	465
	CLI:Test Set Field Valid Options	email_port	110
	CLI:Test Set Field Valid Options	email_port	0
	CLI:Test Set Field Valid Options	email_port	99999

Test with field=username
	CLI:Enter Path	/settings/auditing/destinations/email
	@{VALUES}=	Create List	${EMPTY}	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${TWO_WORD}	${TWO_NUMBER}	${TWO_POINTS}
	FOR		${VALUE}	IN	@{VALUES}
		CLI:Test Set Validate Invalid Options	username	${VALUE}
	END
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	username	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument for command: set
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	username	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument: set

	CLI:Test Set Validate Invalid Options	username	${EXCEEDED}	Error: username: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert	Raw

Test with field=password & confirm_password
	CLI:Enter Path	/settings/auditing/destinations/email
	${OUTPUT}=	CLI:Write	set password=${EMPTY} confirm_password=${EMPTY}	Raw	Yes
	Should Be Equal	${OUTPUT}	${EMPTY}
	${OUTPUT}=	CLI:Write	set password=${WORD} confirm_password=${WORD}	Raw	Yes
	Should Be Equal	${OUTPUT}	${EMPTY}
	${OUTPUT}=	CLI:Write	set password=${NUMBER} confirm_password=${NUMBER}	Raw	Yes
	Should Be Equal	${OUTPUT}	${EMPTY}
	${OUTPUT}=	CLI:Write	set password=${POINTS} confirm_password=${POINTS}	Raw	Yes
	Should Be Equal	${OUTPUT}	${EMPTY}
	${OUTPUT}=	CLI:Write	set password=${ONE_WORD} confirm_password=${ONE_WORD}	Raw	Yes
	Should Be Equal	${OUTPUT}	${EMPTY}
	${OUTPUT}=	CLI:Write	set password=${ONE_NUMBER} confirm_password=${ONE_NUMBER}	Raw	Yes
	Should Be Equal	${OUTPUT}	${EMPTY}
	${OUTPUT}=	CLI:Write	set password=${ONE_POINTS} confirm_password=${ONE_POINTS}	Raw	Yes
	Should Be Equal	${OUTPUT}	${EMPTY}
	${OUTPUT}=	CLI:Write	set password=${TWO_WORD} confirm_password=${TWO_WORD}	Raw	Yes
	Should Be Equal	${OUTPUT}	${EMPTY}
	${OUTPUT}=	CLI:Write	set password=${TWO_NUMBER} confirm_password=${TWO_NUMBER}	Raw	Yes
	Should Be Equal	${OUTPUT}	${EMPTY}
	${OUTPUT}=	CLI:Write	set password=${TWO_POINTS} confirm_password=${TWO_POINTS}	Raw	Yes
	Should Be Equal	${OUTPUT}	${EMPTY}
	${OUTPUT}=	CLI:Write	set password=${EXCEEDED} confirm_password=${EXCEEDED}	Raw	Yes
	Should Be Equal	${OUTPUT}	Error: password: Exceeded the maximum size for this field.

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	password	${WORD}	Error: password: Password mismatch.Error: confirm_password: Password mismatch.
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	password	${WORD}	Error: password: Password mismatch.
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	confirm_password	${WORD}	Error: password: Password mismatch.Error: confirm_password: Password mismatch.
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	confirm_password	${WORD}	Error: password: Password mismatch.
	${ERROR}=	CLI:Write	set password=${ONE_WORD} confirm_password=${ONE_WORD}${ONE_WORD}	Raw	Yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Be Equal	${ERROR}	Error: password: Password mismatch.Error: confirm_password: Password mismatch.
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Be Equal	${ERROR}	Error: password: Password mismatch.
	[Teardown]	CLI:Revert	Raw

Test with field=destination_email
	CLI:Enter Path	/settings/auditing/destinations/email
	@{VALUES}=	Create List	${EMPTY}	${WORD}	${NUMBER}	${POINTS}	${ONE_WORD}	${ONE_NUMBER}	${ONE_POINTS}	${TWO_WORD}	${TWO_NUMBER}	${TWO_POINTS}
	FOR		${VALUE}	IN	@{VALUES}
		CLI:Test Set Validate Invalid Options	destination_email	${VALUE}
	END
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	destination_email	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument for command: set
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	destination_email	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument: set

	CLI:Test Set Validate Invalid Options	destination_email	${EXCEEDED}	Error: destination_email: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert	Raw

Test correct values for field=start_tls
	CLI:Enter Path	/settings/auditing/destinations/email
	CLI:Test Set Field Options		start_tls	yes	no
	CLI:Commands After Set Error	email_server	\\_\\)\\(\\*&¨%	cd	revert	show	ls	test_email
	CLI:Commands After Set Error	email_port	\\_\\)\\(\\*&¨%	cd	revert	show	ls	test_email
	CLI:Commands After Set Error	username	${EXCEEDED}	cd	revert	show	ls	test_email
	CLI:Commands After Set Error	destination_email	${EXCEEDED}	cd	revert	show	ls	test_email
	[Teardown]	CLI:Revert	Raw

Test Valid Values For Field=sender
    [Tags]    EXCLUDEIN3_2
    CLI:Enter Path	/settings/auditing/destinations/email
    CLI:Set      email_server=${EMAIL_SERVER2} email_port=${EMAIL_PORT2} username=${EMAIL_ACCOUNT2} start_tls=yes
    CLI:Set      password=${EMAIL_PASSWD2} confirm_password=${EMAIL_PASSWD2} destination_email=${EMAIL_DESTINATION2}
    FOR	    ${VALID_VALUE}   IN  @{ALL_VALUES}
		CLI:Test Set Field Valid Options   sender   ${VALID_VALUE}   QUOTATION_MARKS=Yes
	END
    [Teardown]    CLI:Revert   Raw

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	SUITE:Reverting Test
	CLI:Close Connection

SUITE:Reverting Test
	CLI:Enter Path	/settings/auditing/destinations/email
	CLI:Write	set start_tls=yes email_port=25 email_server=${EMPTY} username=${EMPTY} password=${EMPTY} confirm_password=${EMPTY} destination_email=${EMPTY}
    Run Keyword If  '${NGVERSION}' >= '4.2'  CLI:Set  sender=${EMPTY}
    CLI:Commit