*** Settings ***
Resource	../../../init.robot
Documentation	Tests Auditing Destination section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	AUDITING	DESTINATION
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${ALIAS}	snmp_alias
${SERVERPRESENT}	${SNMPSERVERPRESENT}
${SERVER}	${SNMPSERVER2}
${SERVERUSER}	${SNMPSERVER2USER}
${SERVERPASSWORD}	${SNMPSERVER2PASSWORD2}
${COMMUNITY}	${SNMPCOMMUNITY2}

${SERVER_PATH}	${SNMPPATH2}
${SERVER_FILE}	${SNMPFILE2}

${MD5DESUSER}	${SNMPMD5DESUSER2}
${MD5AESUSER}	${SNMPMD5AESUSER2}
${SHADESUSER}	${SNMPSHADESUSER2}
${SHAAESUSER}	${SNMPSHAAESUSER2}

${MD5PASSWORD}	${SNMPMD5PASSWORD2}
${SHAPASSWORD}	${SNMPSHAPASSWORD2}

${DESPASSWORD}	${SNMPDESPASSWORD2}
${AESPASSWORD}	${SNMPAESPASSWORD2}

*** Test Cases ***

#
# TCP
#

Test SNMPTrap Server Version 2c tcp-ipv4
	[Tags]	NON-CRITICAL
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	CLI:Switch Connection	default
	SUITE:Set SNMPtrap v2	tcp-ipv4	161
	SUITE:Check Multiple Trap Servers
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Revert	Raw

Test SNMPTrap Server Version 3 MD5 DES tcp-ipv4
	[Tags]	NON-CRITICAL
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:Set SNMPtrap v3	tcp-ipv4	161	md5	des	authPriv	${MD5DESUSER}	${MD5PASSWORD}	${DESPASSWORD}
	SUITE:Check Multiple Trap Servers
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Revert	Raw

Test SNMPTrap Server Version 3 MD5 AES tcp-ipv4
	[Tags]	NON-CRITICAL
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:Set SNMPtrap v3	tcp-ipv4	161	md5	aes	authPriv	${MD5AESUSER}	${MD5PASSWORD}	${AESPASSWORD}
	SUITE:Check Multiple Trap Servers
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Revert	Raw

Test SNMPTrap Server Version 3 SHA DES tcp-ipv4
	[Tags]	NON-CRITICAL
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:Set SNMPtrap v3	tcp-ipv4	161	sha	des	authPriv	${SHADESUSER}	${SHAPASSWORD}	${DESPASSWORD}
	SUITE:Check Multiple Trap Servers
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Revert	Raw

Test SNMPTrap Server Version 3 SHA AES tcp-ipv4
	[Tags]	NON-CRITICAL
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:Set SNMPtrap v3	tcp-ipv4	161	sha	aes	authPriv	${SHAAESUSER}	${SHAPASSWORD}	${AESPASSWORD}
	SUITE:Check Multiple Trap Servers
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Revert	Raw

Test SNMPTrap Server Version 3 AUTHNOPRIV tcp-ipv4
	[Tags]	NON-CRITICAL
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:Set SNMPtrap v3	tcp-ipv4	161	sha	aes	authNoPriv	${SHAAESUSER}	${SHAPASSWORD}	${AESPASSWORD}
	SUITE:Check Multiple Trap Servers
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Revert	Raw

Test SNMPTrap Server Version 3 NOAUTHNOPRIV tcp-ipv4
	[Tags]	NON-CRITICAL
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:Set SNMPtrap v3	tcp-ipv4	161	sha	aes	noAuthNoPriv	${SHAAESUSER}	${SHAPASSWORD}	${AESPASSWORD}
	SUITE:Check Multiple Trap Servers
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Revert	Raw

#
# UDP
#

Test SNMPTrap Server Version 2c udp-ipv4
	[Tags]	NON-CRITICAL
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:Set SNMPtrap v2	udp-ipv4	162
	SUITE:Check Multiple Trap Servers
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Revert	Raw

Test SNMPTrap Server Version 3 MD5 DES udp-ipv4
	[Tags]	NON-CRITICAL
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:Set SNMPtrap v3	udp-ipv4	162	md5	des	authPriv	${MD5DESUSER}	${MD5PASSWORD}	${DESPASSWORD}
	SUITE:Check Multiple Trap Servers
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Revert	Raw

Test SNMPTrap Server Version 3 MD5 AES udp-ipv4
	[Tags]	NON-CRITICAL
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:Set SNMPtrap v3	udp-ipv4	162	md5	aes	authPriv	${MD5AESUSER}	${MD5PASSWORD}	${AESPASSWORD}
	SUITE:Check Multiple Trap Servers
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Revert	Raw

Test SNMPTrap Server Version 3 SHA DES udp-ipv4
	[Tags]	NON-CRITICAL
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:Set SNMPtrap v3	udp-ipv4	162	sha	des	authPriv	${SHADESUSER}	${SHAPASSWORD}	${DESPASSWORD}
	SUITE:Check Multiple Trap Servers
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Revert	Raw

Test SNMPTrap Server Version 3 SHA AES udp-ipv4
	[Tags]	NON-CRITICAL
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:Set SNMPtrap v3	udp-ipv4	162	sha	aes	authPriv	${SHAAESUSER}	${SHAPASSWORD}	${AESPASSWORD}
	SUITE:Check Multiple Trap Servers
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Revert	Raw

Test SNMPTrap Server Version 3 AUTHNOPRIV udp-ipv4
	[Tags]	NON-CRITICAL
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:Set SNMPtrap v3	udp-ipv4	162	sha	aes	authNoPriv	${SHAAESUSER}	${SHAPASSWORD}	${AESPASSWORD}
	SUITE:Check Multiple Trap Servers
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Revert	Raw

Test SNMPTrap Server Version 3 NOAUTHNOPRIV udp-ipv4
	[Tags]	NON-CRITICAL
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	SUITE:Set SNMPtrap v3	udp-ipv4	162	sha	aes	noAuthNoPriv	${SHAAESUSER}	${SHAPASSWORD}	${AESPASSWORD}
	SUITE:Check Multiple Trap Servers
	[Teardown]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Revert	Raw

*** Keywords ***
SUITE:Setup
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	CLI:Open
	CLI:Enable All Events	yes	snmp_trap

SUITE:Teardown
	Skip If	${SERVERPRESENT} == ${FALSE}	No Physical SNMP Server available. Test will be skipped.
	CLI:Open
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set	snmptrap_server=127.0.0.1 snmptrap_transport_protocol=udp-ipv4 snmptrap_port=161 snmptrap_version=version_2c snmptrap_community=public
	CLI:Commit
	CLI:Close Connection

SUITE:Set SNMPtrap v2
	[Arguments]	${protocol}	${port}
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set	snmptrap_version=version_2c snmptrap_server=${SERVER} snmptrap_community=${COMMUNITY} snmptrap_transport_protocol=${protocol} snmptrap_port=${port}
	CLI:Commit

SUITE:Set SNMPtrap v3
	[Arguments]	${protocol}	${port}	${auth_proto}	${priv_algo}	${sec_level}	${user}	${auth_pass}	${priv_pass}
	CLI:Enter Path	/settings/auditing/destinations/snmptrap
	CLI:Set	snmptrap_version=version_3 snmptrap_security_level=${sec_level} snmptrap_server=${SERVER}
	CLI:Set	snmptrap_transport_protocol=${protocol} snmptrap_port=${port} snmptrap_user=${user}
	CLI:Set	snmptrap_authentication=${auth_proto} snmptrap_authentication_password=${auth_pass}
	CLI:Set	snmptrap_privacy_algo=${priv_algo} snmptrap_privacy_passphrase=${priv_pass}
	CLI:Commit

SUITE:Check Multiple Trap Servers
	Run Keyword If	'${NGVERSION}' < '4.0'	Set Tags	NON-CRITICAL
	Set Tags	4.0_OFFICIAL
	# Add support for multiple servers specified separated by comma
	@{servers}=	Split String	${SERVER}	,
	FOR	${ip}	IN	@{servers}
		CLI:Open	${SERVERUSER}	${SERVERPASSWORD}	session_alias=${ALIAS}	TYPE=root	HOST_DIFF=${ip}
		SUITE:Truncate Log File	${SERVER_PATH}	${SERVER_FILE}
		###
		CLI:Open	session_alias=eventsession	startping=no
		CLI:Write	ls
		CLI:Close Current Connection
		###
		CLI:Switch Connection	${ALIAS}
		Wait Until Keyword Succeeds	10x	2s	SUITE:Test Log	${SERVER_PATH}	${SERVER_FILE}
		Close Connection
		###
		CLI:Switch Connection	default
	END

SUITE:Test Log
	[Arguments]	${PATH}	${FILE}
	${OUTPUT}=	CLI:Write	timeout 5 tail -n 10 ${PATH}/${FILE}
	Should Contain	${OUTPUT}	${DEFAULT_USERNAME}@${CLIENTIP}

SUITE:Truncate Log File
	[Arguments]	${PATH}	${FILE}
	${OUTPUT}=	CLI:Write	echo -n > ${PATH}/${FILE}
#	${OUTPUT}=	CLI:Write	ls -l ${PATH}/${FILE}
#	Should Not Contain	${OUTPUT}	ls: cannot access

