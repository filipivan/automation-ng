*** Settings ***
Resource        ../../../init.robot
Documentation   Functionality tests about auditing Destination email section
Metadata        Version         1.0
Metadata        Executed At     ${HOST}
Force Tags      CLI    DEPENDENCE_EMAIL
Default Tags    CLI     SSH     AUDITING    DESTINATION     EMAIL

Suite Setup         SUITE:Setup
Suite Teardown      SUITE:Teardown

*** Variables ***
${SENDER_TEST}        Au7omat1on te$t w!th sp@ce
${SENDER_TEST_v4_2}   Au7omat1on

*** Test Cases ***
Test For Email Without Sender
    [Setup]             SUITE:Setup Valid Email
    CLI:Enter Path      /settings/auditing/destinations/email
    CLI:Write           test_email
    SUITE:Check If Test Email Was Received
    [Teardown]          SUITE:Reverting Test

Test For Email With Sender
    [Tags]              EXCLUDEIN3_2
    [Setup]             SUITE:Setup Valid Email  SENDER=yes
    CLI:Enter Path      /settings/auditing/destinations/email
    CLI:Write           test_email
    SUITE:Check If Test Email Was Received  SENDER=yes
    [Teardown]          SUITE:Reverting Test

*** Keywords ***
SUITE:Setup
    CLI:Open
    CLI:Open            USERNAME=${EMAIL_ACCOUNT}  PASSWORD=${EMAIL_PASSWD}   session_alias=mail_server
                        ...  TYPE=shell  HOST_DIFF=${EMAIL_SERVER}

SUITE:Teardown
    SUITE:Reverting Test
    SUITE:Remove Old Emails From Mailbox
    CLI:Close Connection

SUITE:Setup Valid Email
    [Arguments]     ${SENDER}=no
    CLI:Switch Connection   default
    CLI:Enter Path      /settings/auditing/destinations/email
    CLI:Set             email_server=${EMAIL_SERVER} email_port=${EMAIL_PORT} username=${EMAIL_ACCOUNT} start_tls=yes
    CLI:Set             password=${EMAIL_PASSWD} confirm_password=${EMAIL_PASSWD} destination_email=${EMAIL_DESTINATION}
    Run Keyword If      '${SENDER}' == 'yes'   CLI:Set  sender="${SENDER_TEST}"
    CLI:Commit

SUITE:Reverting Test
    CLI:Switch Connection   default
    CLI:Enter Path      /settings/auditing/destinations/email
    CLI:Set             start_tls=yes email_port=25 email_server=${EMPTY} username=${EMPTY}
    CLI:Set             password=${EMPTY} confirm_password=${EMPTY} destination_email=${EMPTY}
    Run Keyword If      '${NGVERSION}' >= '4.2'  CLI:Set  sender=${EMPTY}
    CLI:Commit

SUITE:Check If Test Email Was Received
    [Arguments]         ${SENDER}=no
    CLI:Switch Connection            mail_server
    Write                        tail -44 /var/mail/${EMAIL_ACCOUNT}
    Set Client Configuration     prompt=:~$
    ${OUTPUT}=                   CLI:Read Until Prompt
    Should Not Contain           ${OUTPUT}     No mail for ${EMAIL_ACCOUNT}
    Run Keyword If               '${NGVERSION}' < '4.2'   Should Contain  ${OUTPUT}  Email test from NodeGrid
    Run Keyword If               '${NGVERSION}' >= '4.2'   Should Contain  ${OUTPUT}  Email test from Nodegrid
    Run Keyword If               '${NGVERSION}' >= '4.2' and '${SENDER}' == 'yes'
    ...                          Should Contain   ${OUTPUT}   From: "${SENDER_TEST}"

SUITE:Remove Old Emails From Mailbox
	CLI:Switch Connection	mail_server
	CLI:Write	echo -n > /var/mail/${EMAIL_ACCOUNT}
	${ERASED_MAILBOX}	CLI:Write	cat /var/mail/${EMAIL_ACCOUNT}
	Should Not Contain	${ERASED_MAILBOX}	${SENDER_TEST}