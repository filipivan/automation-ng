*** Settings ***
Resource	../../../init.robot
Documentation	Tests Auditing Destination section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	AUDITING	DESTINATION
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
# file destination tests
Test File Destination available commands after send tab-tab
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Test Available Commands	cd	change_password	commit	event_system_audit	event_system_clear	exit	hostname
	...	ls	pwd	quit	reboot	revert	set	shell	show	show_settings	shutdown	whoami

Test File Destination visible fields for show command
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Test Show Command	destination	file_size	number_of_archives	archive_by_time	nfs_server	nfs_path
	...	nfs_file_size	number_of_archives_in_nfs	nfs_archive_by_time

Test File Destination available fields to set command
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Test Set Available Fields	archive_by_time	file_size	nfs_file_size	nfs_server	number_of_archives_in_nfs
	...	destination	nfs_archive_by_time	nfs_path	number_of_archives

### destination
Test correct values for field=destination
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Test Set Field Options	destination	local	nfs

Test incorrect value for field=destination
	CLI:Enter Path	/settings/auditing/destinations/file
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	destination	${EMPTY}	Error: Missing value for parameter: destination
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	destination	${EMPTY}	Error: Missing value: destination
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	destination	remote	Error: Invalid value: remote for parameter: destination
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	destination	remote	Error: Invalid value: remote

Test for commands after Error for field=destination
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Commands After Set Error	destination	${EMPTY}	cd	revert	show	ls
	[Teardown]	CLI:Revert

Test correct values for field=archive_by_time
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Test Set Field Valid Options	archive_by_time	00:00
	CLI:Test Set Field Valid Options	archive_by_time	00:01
	CLI:Test Set Field Valid Options	archive_by_time	12:01
	CLI:Test Set Field Valid Options	archive_by_time	23:59
	CLI:Test Set Field Valid Options	archive_by_time	${EMPTY}
	[Teardown]	CLI:Revert

Test incorrect values for field=archive_by_time
	CLI:Enter Path	/settings/auditing/destinations/file
#	CLI:Test Set Field Invalid Options	archive_by_time	${EMPTY}	Error: file_size: Valid values are between 0 (disabled) and 2048 Kb. #Not used as it is a vaild value
	CLI:Test Set Field Invalid Options	archive_by_time	abc	Error: archive_by_time: Configure the archiving time [HH:MM], or leave it blank.
	CLI:Test Set Field Invalid Options	archive_by_time	25:00	Error: archive_by_time: Configure the archiving time [HH:MM], or leave it blank.
	CLI:Test Set Field Invalid Options	archive_by_time	24:00	Error: archive_by_time: Configure the archiving time [HH:MM], or leave it blank.
	CLI:Test Set Field Invalid Options	archive_by_time	0000	Error: archive_by_time: Configure the archiving time [HH:MM], or leave it blank.
	CLI:Test Set Field Invalid Options	archive_by_time	0:00	Error: archive_by_time: Configure the archiving time [HH:MM], or leave it blank.
	CLI:Test Set Field Invalid Options	archive_by_time	00:0	Error: archive_by_time: Configure the archiving time [HH:MM], or leave it blank.
	CLI:Test Set Field Invalid Options	archive_by_time	00:	Error: archive_by_time: Configure the archiving time [HH:MM], or leave it blank.
	CLI:Test Set Field Invalid Options	archive_by_time	:00	Error: archive_by_time: Configure the archiving time [HH:MM], or leave it blank.

Test for commands after Error for field=archive_by_time
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Commands After Set Error	archive_by_time	25:00	cd	revert	show	ls
	[Teardown]	CLI:Revert

### file_size
Test correct values for field=file_size
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Test Set Field Valid Options	file_size	0
	CLI:Test Set Field Valid Options	file_size	1
	CLI:Test Set Field Valid Options	file_size	2048
	CLI:Test Set Field Valid Options	file_size	1024
	[Teardown]	CLI:Revert

Test incorrect values for field=file_size
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Test Set Field Invalid Options	file_size	${EMPTY}	Error: file_size: Valid values are between 0 (disabled) and 2048 Kb.
	CLI:Test Set Field Invalid Options	file_size	-1	Error: file_size: Valid values are between 0 (disabled) and 2048 Kb.
	CLI:Test Set Field Invalid Options	file_size	2049	Error: file_size: Valid values are between 0 (disabled) and 2048 Kb.
	CLI:Test Set Field Invalid Options	file_size	abs	Error: file_size: Valid values are between 0 (disabled) and 2048 Kb.
	CLI:Test Set Field Invalid Options	file_size	1024.55	Error: file_size: Exceeded the maximum size for this field.
	CLI:Test Set Field Invalid Options	file_size	1024MB	Error: file_size: Exceeded the maximum size for this field.

Test for commands after Error for field=file_size
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Commands After Set Error	file_size	-1	cd	revert	show	ls
	[Teardown]	CLI:Revert

### nfs_file_size

Test correct values for field=nfs_file_size
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Test Set Field Valid Options	nfs_file_size	0
	CLI:Test Set Field Valid Options	nfs_file_size	99999
	CLI:Test Set Field Valid Options	nfs_file_size	1
	CLI:Test Set Field Valid Options	nfs_file_size	1024
	[Teardown]	CLI:Revert

Test incorrect values for field=nfs_file_size
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Test Set Field Invalid Options	nfs_file_size	${EMPTY}	Error: nfs_file_size: Validation error.
	CLI:Test Set Field Invalid Options	nfs_file_size	-1	Error: nfs_file_size: Validation error.
	CLI:Test Set Field Invalid Options	nfs_file_size	100000	Error: nfs_file_size: Exceeded the maximum size for this field.
	CLI:Test Set Field Invalid Options	nfs_file_size	abs	Error: nfs_file_size: Validation error.
	CLI:Test Set Field Invalid Options	nfs_file_size	1024.55	Error: nfs_file_size: Exceeded the maximum size for this field.
	CLI:Test Set Field Invalid Options	nfs_file_size	1024MB	Error: nfs_file_size: Exceeded the maximum size for this field.

Test for commands after Error for field=nfs_file_size
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Commands After Set Error	file_size	-1	cd	revert	show	ls
	[Teardown]	CLI:Revert

### nfs_archive_by_time

Test correct values for field=nfs_archive_by_time
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Test Set Field Valid Options	nfs_archive_by_time	00:00
	CLI:Test Set Field Valid Options	nfs_archive_by_time	00:01
	CLI:Test Set Field Valid Options	nfs_archive_by_time	12:01
	CLI:Test Set Field Valid Options	nfs_archive_by_time	23:59
	CLI:Test Set Field Valid Options	nfs_archive_by_time	${EMPTY}
	[Teardown]	CLI:Revert

Test incorrect values for field=nfs_archive_by_time
	CLI:Enter Path	/settings/auditing/destinations/file
#	CLI:Test Set Field Invalid Options	archive_by_time	${EMPTY}	Error: file_size: Valid values are between 0 (disabled) and 2048 Kb. #Not used as it is a vaild value
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	nfs_archive_by_time	abc	Error: nfs_archive_by_time: Configure the archiving time [HH:MM], or leave it blank.
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	nfs_archive_by_time	25:00	Error: nfs_archive_by_time: Configure the archiving time [HH:MM], or leave it blank.
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	nfs_archive_by_time	24:00	Error: nfs_archive_by_time: Configure the archiving time [HH:MM], or leave it blank.
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	nfs_archive_by_time	0000	Error: nfs_archive_by_time: Configure the archiving time [HH:MM], or leave it blank.
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	nfs_archive_by_time	0:00	Error: nfs_archive_by_time: Configure the archiving time [HH:MM], or leave it blank.
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	nfs_archive_by_time	00:0	Error: nfs_archive_by_time: Configure the archiving time [HH:MM], or leave it blank.
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	nfs_archive_by_time	00:	Error: nfs_archive_by_time: Configure the archiving time [HH:MM], or leave it blank.
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	nfs_archive_by_time	:00	Error: nfs_archive_by_time: Configure the archiving time [HH:MM], or leave it blank.
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	nfs_archive_by_time	abc	Error: nfs_archive_by_time: error.validation.loggingnfs_time
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	nfs_archive_by_time	25:00	Error: nfs_archive_by_time: error.validation.loggingnfs_time
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	nfs_archive_by_time	24:00	Error: nfs_archive_by_time: error.validation.loggingnfs_time
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	nfs_archive_by_time	0000	Error: nfs_archive_by_time: error.validation.loggingnfs_time
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	nfs_archive_by_time	0:00	Error: nfs_archive_by_time: error.validation.loggingnfs_time
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	nfs_archive_by_time	00:0	Error: nfs_archive_by_time: error.validation.loggingnfs_time
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	nfs_archive_by_time	00:	Error: nfs_archive_by_time: error.validation.loggingnfs_time
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	nfs_archive_by_time	:00	Error: nfs_archive_by_time: error.validation.loggingnfs_time

Test for commands after Error for field=nfs_archive_by_time
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Commands After Set Error	nfs_archive_by_time	:00	cd	revert	show	ls
	[Teardown]	CLI:Revert

### nfs_path

Test correct values for field=nfs_path
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Test Set Field Valid Options	nfs_path	${EMPTY}
	CLI:Test Set Field Valid Options	nfs_path	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
	CLI:Test Set Field Valid Options	nfs_path	//<user>:<password>@<host>:<port>/<url-path>
	CLI:Test Set Field Valid Options	nfs_path	www.samplenfserver.com/folder_a/folder_b/folder_c
	CLI:Test Set Field Valid Options	nfs_path	abcdefghijklmnopqrstuvwxyz/?:@=&$-_.+!*(),{}[]|#%^/\
	CLI:Test Set Field Valid Options	nfs_path	0123456789
	[Teardown]	CLI:Revert

Test incorrect values for field=nfs_path
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Test Set Field Invalid Options	nfs_path	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa	Error: nfs_path: Exceeded the maximum size for this field.
	CLI:Test Set Field Invalid Options	nfs_path	'	Error: Invalid command line${space}
	#CLI:Test Set Field Invalid Options	nfs_path	"	Error: Invalid command line${space}
	## 5.0 allows multiple line input with double quote (NG-3342)
	Run Keyword If	'${NGVERSION}' < '5.0' 	CLI:Test Set Field Invalid Options	nfs_path	"	Error: Invalid command line${space}

Test for commands after Error for field=nfs_path
	CLI:Enter Path	/settings/auditing/destinations/file
	#CLI:Commands After Set Error	nfs_path	"	cd	revert	show	ls
	Run Keyword If	'${NGVERSION}' < '5.0' 	CLI:Commands After Set Error	nfs_path	"	cd	revert	show	ls
	Run Keyword If	'${NGVERSION}' == '5.0'	CLI:Commands After Set Error	nfs_path	'	cd	revert	show	ls
	[Teardown]	CLI:Revert

### nfs_server
Test correct values for field=nfs_server
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Test Set Field Valid Options	nfs_server	${EMPTY}
	CLI:Test Set Field Valid Options	nfs_server	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
	CLI:Test Set Field Valid Options	nfs_server	www.sample-server.com
	CLI:Test Set Field Valid Options	nfs_server	1.1.1.1
	#3.2 does not support IPv6 addresses
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Valid Options	nfs_server	fd38:24b8:1110:7777::5
	CLI:Test Set Field Valid Options	nfs_server	abcdefghijklmnopqrstuvwxyz-0123456789
	[Teardown]	CLI:Revert

Test incorrect values for field=nfs_server
	CLI:Enter Path	/settings/auditing/destinations/file
#	CLI:Test Set Field Invalid Options	nfs_server	${EMPTY}	Error: nfs_file_size: Validation error.
	CLI:Test Set Field Invalid Options	nfs_server	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa	Error: nfs_server: Exceeded the maximum size for this field.
	${msg}=	Set Variable If	'${NGVERSION}' < '4.0'	Error: nfs_server: Accepted values are IPv4 Address or Domain Name (accepts alphanumeric characters, - and .).	Error: nfs_server: Accepted values are IPv4/IPv6 Addresses or Domain Name.
	CLI:Test Set Field Invalid Options	nfs_server	_	${msg}
	CLI:Test Set Field Invalid Options	nfs_server	@	${msg}

Test for commands after Error for field=nfs_server
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Commands After Set Error	nfs_server	@	cd	revert	show	ls
	[Teardown]	CLI:Revert

### number_of_archives
Test correct values for field=number_of_archives
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Test Set Field Valid Options	number_of_archives	1
	CLI:Test Set Field Valid Options	number_of_archives	2
	CLI:Test Set Field Valid Options	number_of_archives	3
	CLI:Test Set Field Valid Options	number_of_archives	4
	CLI:Test Set Field Valid Options	number_of_archives	5
	CLI:Test Set Field Valid Options	number_of_archives	6
	CLI:Test Set Field Valid Options	number_of_archives	7
	CLI:Test Set Field Valid Options	number_of_archives	8
	CLI:Test Set Field Valid Options	number_of_archives	9
	[Teardown]	CLI:Revert

Test incorrect values for field=number_of_archives
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Test Set Field Invalid Options	number_of_archives	0	Error: number_of_archives: Valid values are between 1 and 9.
	CLI:Test Set Field Invalid Options	number_of_archives	${EMPTY}	Error: number_of_archives: Valid values are between 1 and 9.
	CLI:Test Set Field Invalid Options	number_of_archives	-1	Error: number_of_archives: Valid values are between 1 and 9.
	CLI:Test Set Field Invalid Options	number_of_archives	10	Error: number_of_archives: Valid values are between 1 and 9.
	CLI:Test Set Field Invalid Options	number_of_archives	abs	Error: number_of_archives: Exceeded the maximum size for this field.
	CLI:Test Set Field Invalid Options	number_of_archives	1a	Error: number_of_archives: Valid values are between 1 and 9.
	CLI:Test Set Field Invalid Options	number_of_archives	1.0	Error: number_of_archives: Exceeded the maximum size for this field.
	CLI:Test Set Field Invalid Options	number_of_archives	1.	Error: number_of_archives: Valid values are between 1 and 9.

Test for commands after Error for field=number_of_archives
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Commands After Set Error	number_of_archives	1.	cd	revert	show	ls
	[Teardown]	CLI:Revert

### number_of_archives_in_nfs
Test correct values for field=number_of_archives_in_nfs
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Test Set Field Valid Options	number_of_archives_in_nfs	0
	CLI:Test Set Field Valid Options	number_of_archives_in_nfs	1
	CLI:Test Set Field Valid Options	number_of_archives_in_nfs	2
	CLI:Test Set Field Valid Options	number_of_archives_in_nfs	5
	CLI:Test Set Field Valid Options	number_of_archives_in_nfs	10
	CLI:Test Set Field Valid Options	number_of_archives_in_nfs	25
	CLI:Test Set Field Valid Options	number_of_archives_in_nfs	50
	CLI:Test Set Field Valid Options	number_of_archives_in_nfs	75
	CLI:Test Set Field Valid Options	number_of_archives_in_nfs	98
	CLI:Test Set Field Valid Options	number_of_archives_in_nfs	99
	[Teardown]	CLI:Revert

Test incorrect values for field=number_of_archives_in_nfs
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Test Set Field Invalid Options	number_of_archives_in_nfs	${EMPTY}	Error: number_of_archives_in_nfs: Valid values are between 0 and 99.
	CLI:Test Set Field Invalid Options	number_of_archives_in_nfs	-1	Error: number_of_archives_in_nfs: Valid values are between 0 and 99.
	CLI:Test Set Field Invalid Options	number_of_archives_in_nfs	100	Error: number_of_archives_in_nfs: Exceeded the maximum size for this field.
	CLI:Test Set Field Invalid Options	number_of_archives_in_nfs	abs	Error: number_of_archives_in_nfs: Exceeded the maximum size for this field.
	CLI:Test Set Field Invalid Options	number_of_archives_in_nfs	1a	Error: number_of_archives_in_nfs: Valid values are between 0 and 99.
	CLI:Test Set Field Invalid Options	number_of_archives_in_nfs	1.0	Error: number_of_archives_in_nfs: Exceeded the maximum size for this field.
	CLI:Test Set Field Invalid Options	number_of_archives_in_nfs	1.	Error: number_of_archives_in_nfs: Valid values are between 0 and 99.

Test for commands after Error for field=number_of_archives_in_nfs
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Commands After Set Error	number_of_archives_in_nfs	1.	cd	revert	show	ls
	[Teardown]	CLI:Revert

*** Keywords ***
SUITE:Setup
	CLI:Open
	${VAR}=	 CLI:Get Hostname
	Set Suite Variable	${HOSTNAME}	${VAR}

SUITE:Teardown
	CLI:Close Connection