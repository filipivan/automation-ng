*** Settings ***
Resource	../../../init.robot
Documentation	Tests Settings > Authentication Console... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SYSLOG_SERVER}
${SYSLOG_PORT}	2.2.2.2:5555
${SYSLOG_SERVER_IPV6}	[2001:db8::1]
${SYSLOG_IPV6_Port}	[2001:db8::1]:1233

*** Test Cases ***
Test enable IPV4 syslog access
	CLI:Enter Path	/settings/auditing/destinations/syslog/
	CLI:Write	set ipv4_remote_server=yes ipv4_address=${SYSLOGSERVER}
	CLI:Show
	CLI:Commit
	[Teardown]	SUITE:Disable method	ipv4

Test IPV4 syslog t with remote_serverport with remote_server
	CLI:Enter Path	/settings/auditing/destinations/syslog/
	CLI:Write	set ipv4_remote_server=yes ipv4_address=${SYSLOG_PORT}
	CLI:Write	show
	CLI:Write	commit
	[Teardown]	SUITE:Disable method	ipv4

Test enable IPV6 syslog access
	CLI:Enter Path	/settings/auditing/destinations/syslog/
	CLI:Write	set ipv6_remote_server=yes ipv6_address=${SYSLOG_SERVER_IPV6}
	CLI:Show
	CLI:Commit
	[Teardown]	SUITE:Disable method	ipv6

Test IPV6 syslog port
	CLI:Enter Path	/settings/auditing/destinations/syslog/
	CLI:Write	set ipv6_remote_server=yes ipv6_address=${SYSLOG_IPV6_Port}
	CLI:Write	show
	CLI:Write	commit
	[Teardown]	SUITE:Disable method	ipv6

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection

SUITE:Disable method
	[Arguments]	${TYPE}=ipv4
	CLI:Write	set ${TYPE}_address= ${TYPE}_remote_server=no
	CLI:Write	commit
