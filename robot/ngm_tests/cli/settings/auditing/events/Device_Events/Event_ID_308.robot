*** Settings ***
Resource	../../../../init.robot
Documentation	Test System Events section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	AUDITING	EVENTS	LOGGING	EVENT${EVENT_NUMBER}

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USER}	${DEFAULT_USERNAME}
${DEVICE_NAME}	event${EVENT_NUMBER}
${DEVICE_IP}	127.0.0.1
${EVENT_NUMBER}	308
${EVENT308}	Device session terminated. Device: ${DEVICE_NAME}. Command issued by user: ${USER}@${CLIENTIP}. User disconnected from the device: root@${CLIENTIP}
${EVENT_DESCRIPTION}	${EVENT308}

*** Test Cases ***
Test Event ID 308 with devices_events enabled
	[Setup]	Run Keywords
	...	Run Keyword If	'${NGVERSION}' == '3.2'	Set Tags	NON-CRITICAL	NEED-REVIEW	AND
	...	CLI:Enable Device Events	yes
	...	AND	Wait Until Keyword Succeeds	5x	1s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	Skip If	'${NGVERSION}' == '4.2' or '${NGVERSION}' == '5.0'	It's not working on these versions, but won't be fixed as it's a low prio on version previous to 5.2
	CLI:Switch Connection	default
	CLI:Open Device	${DEVICE_NAME}	USERNAME=root	PASSWORD=${ROOT_PASSWORD}	TYPE=root
	CLI:Write	ls
	CLI:Switch Connection	default
	CLI:Terminate Session	${DEVICE_NAME}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	10x	1s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}	${EVENT_DESCRIPTION}
#	[Teardown]	CLI:Switch Connection	default
	[Teardown]	Run Keywords	CLI:Switch Connection	default
#	...	AND	CLI:Delete All Devices
	...	AND	CLI:Terminate Session	${DEVICE_NAME}	Raw

Test Event ID 308 with devices_events disabled
	[Setup]	Run Keywords	CLI:Enable Device Events	no
#	...	AND	CLI:Add Device	${DEVICE_NAME}	device_console	127.0.0.1	root	${ROOT_PASSWORD}
	...	AND	Wait Until Keyword Succeeds	5x	1s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	default
	CLI:Open Device	${DEVICE_NAME}	TYPE=root
	CLI:Write	ls
	CLI:Switch Connection	default
	CLI:Terminate Session	${DEVICE_NAME}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	10x	1s	CLI:Test Event ID Not Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Connect As Root
	CLI:Switch Connection	default
	CLI:Enable Auditing Destination To Events
	CLI:Delete All Devices
	CLI:Add Device	${DEVICE_NAME}	device_console	${DEVICE_IP}	root	${ROOT_PASSWORD}
	Wait Until Keyword Succeeds	30s	1s	CLI:Test Device Connection	${DEVICE_NAME}	Connected

SUITE:Teardown
	CLI:Switch Connection	default
	CLI:Delete All Devices
	CLI:Enable Device Events	yes
	CLI:Close Connection
