*** Settings ***
Resource	../../../../init.robot
Documentation	Test System Events section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	DEPENDENCE_DEVICE	DEPENDENCE_PDU	EXCLUDEIN3_2
Default Tags	AUDITING	EVENTS	LOGGING	EVENT${EVENT_NUMBER}	REVIEWED

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USER}	${DEFAULT_USERNAME}
${DEVICE_NAME}	event${EVENT_NUMBER}
${DEVICE}	${PDU_SERVERTECH}
${EVENT_NUMBER}	312
${EVENT312}	Power Cycle command executed on a device. Device: ${DEVICE_NAME}. User: ${USER}.
${EVENT_DESCRIPTION}	${EVENT312}

*** Test Cases ***
Test Event ID 312 with devices_events enabled
	Run Keyword If	${NGVERSION} <= 4.2	Set Tags	NON-CRITICAL
	[Setup]	Run Keywords	CLI:Enable Device Events	yes
	...	AND	Wait Until Keyword Succeeds	150s	5s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	default
	${OUTLET}=	SUITE:Get Outlet
	CLI:Outlet Cycle	${DEVICE_NAME}	${OUTLET}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	60s	5s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}	${EVENT_DESCRIPTION}

Test Event ID 312 with devices_events disabled
	Run Keyword If	${NGVERSION} <= 4.2	Set Tags	NON-CRITICAL
	[Setup]	Run Keywords	CLI:Switch Connection	default
	...	AND	CLI:Enable Device Events	no
	...	AND	Wait Until Keyword Succeeds	150s	5s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	default
	${OUTLET}=	SUITE:Get Outlet
	CLI:Outlet Cycle	${DEVICE_NAME}	${OUTLET}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	60s	5s	CLI:Test Event ID Not Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Connect As Root
	CLI:Switch Connection	default
	CLI:Enable Auditing Destination To Events
	CLI:Delete All Devices
	CLI:Add Device	${DEVICE_NAME}	${DEVICE}[type]	${DEVICE}[ip]	${DEVICE}[username]	${DEVICE}[password]	enabled
	...	enable_device_state_detection_based_on_network_traffic=yes
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/management/
	CLI:Set	snmp=yes snmp_version=v2 snmp_commmunity=private
	CLI:Commit
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/commands/outlet/
	CLI:Set	protocol=snmp
	CLI:Commit
	CLI:Discover Now	${DEVICE_NAME}	pdu

SUITE:Teardown
	CLI:Switch Connection	default
	CLI:Delete All Devices
	CLI:Enable Device Events	yes
	CLI:Close Connection

SUITE:Get Outlet
	${OUTPUT}=	CLI:Outlet Status	${DEVICE_NAME}
	${OUTLETS}=	Split To Lines	${OUTPUT}	0	-1
	${QT_OUTLETS}=	Get Length	${OUTLETS}
	${RANDOM_OUTLET}=	Evaluate	random.randint(0, ${QT_OUTLETS} - 1)	modules=random, sys
	${FULL_OUTLET1}=	Set Variable	${OUTLETS}[${RANDOM_OUTLET}]
	${OUTLET}=	Split String	${FULL_OUTLET1}	:
	${OUTLET1}=	Set Variable	${OUTLET}[2]
	[Return]	${OUTLET1}
