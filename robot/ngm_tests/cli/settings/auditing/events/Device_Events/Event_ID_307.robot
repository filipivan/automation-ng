*** Settings ***
Resource	../../../../init.robot
Documentation	Test System Events section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2
Default Tags	AUDITING	EVENTS	LOGGING	EVENT${EVENT_NUMBER}

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USER}	${DEFAULT_USERNAME}
${DEVICE_NAME}	event${EVENT_NUMBER}
${DEVICE_IP}	127.0.0.1
${EVENT_NUMBER_306}	306
${EVENT_DESC306}	Device Up. Device: ${DEVICE_NAME}.
${EVENT_NUMBER}	307
${EVENT307}	Device Down. Device: ${DEVICE_NAME}.
${EVENT_DESCRIPTION}	${EVENT307}

*** Test Cases ***
Test Event ID 307 with devices_events enabled
	[Setup]	Run Keywords	CLI:Enable Device Events	yes
	...	AND	Wait Until Keyword Succeeds	10x	1s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	#Bug where this event is raised sometimes in v5.0, wont be fixed.
	Run Keyword If	${NGVERSION} == 5.0	Set Tags	NON-CRITICAL
	CLI:Switch Connection	default
	CLI:Add Device	${DEVICE_NAME}	device_console	${DEVICE_IP}	root	${ROOT_PASSWORD}
	...	enabled	enable_device_state_detection_based_on_network_traffic=yes
	Wait Until Keyword Succeeds	30s	1s	CLI:Test Device Connection	${DEVICE_NAME}	Connected
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	10x	1s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER_306}	${EVENT_DESC306}
	CLI:Switch Connection	default
	SUITE:Edit Device To Be Detected As Down
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	10x	1s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}	${EVENT_DESCRIPTION}
	[Teardown]	Run Keywords	CLI:Switch Connection	default
	...	AND	CLI:Delete All Devices

Test Event ID 307 with devices_events disabled
	[Setup]	Run Keywords	CLI:Enable Device Events	no
	...	AND	CLI:Add Device	${DEVICE_NAME}	device_console	127.0.0.1	root	${ROOT_PASSWORD}
	...	AND	Wait Until Keyword Succeeds	30s	1s	CLI:Test Device Connection	${DEVICE_NAME}	Connected
	...	AND	CLI:Switch Connection	root_session
	...	AND	Wait Until Keyword Succeeds	10x	1s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER_306}	${EVENT_DESC306}
	...	AND	Wait Until Keyword Succeeds	5x	1s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	#Bug where this event is raised sometimes in v5.0, wont be fixed.
	Run Keyword If	${NGVERSION} == 5.0	Set Tags	NON-CRITICAL
	CLI:Switch Connection	default
	CLI:Delete All Devices
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	10x	1s	CLI:Test Event ID Not Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Connect As Root
	CLI:Switch Connection	default
	CLI:Enable Auditing Destination To Events
	CLI:Delete All Devices

SUITE:Teardown
	CLI:Switch Connection	default
	CLI:Delete All Devices
	CLI:Enable Device Events	yes
	CLI:Close Connection

SUITE:Edit Device To Be Detected As Down
	CLI:Enter Path	/settings/devices/
	CLI:Edit	${DEVICE_NAME}
	CLI:Set	enable_device_state_detection_based_on_network_traffic=no mode=disabled
	CLI:Commit
	CLI:Edit	${DEVICE_NAME}
	CLI:Set	enable_device_state_detection_based_on_network_traffic=yes
	CLI:Commit