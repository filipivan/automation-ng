*** Settings ***
Resource	../../../../init.robot
Documentation	Test System Events section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	AUDITING	EVENTS	LOGGING	EVENT${EVENT_NUMBER}

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USER}	${DEFAULT_USERNAME}
${DEVICE_NAME}	event${EVENT_NUMBER}
${DEVICE_NAME2}	event${EVENT_NUMBER}_2
${DEVICE_IP}	127.0.0.1
${EVENT_NUMBER}	304
${EVENT304}	Device renamed. User: ${USER}. Device: ${DEVICE_NAME2}. Previous Name: ${DEVICE_NAME}.
${EVENT_DESCRIPTION}	${EVENT304}

*** Test Cases ***
Test Event ID 304 with devices_events enabled
	[Setup]	Run Keywords	CLI:Enable Device Events	yes
	...	AND	Wait Until Keyword Succeeds	5x	1s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	default
	CLI:Rename Device	${DEVICE_NAME}	${DEVICE_NAME2}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	5x	1s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}	${EVENT_DESCRIPTION}
	[Teardown]	Run Keywords	CLI:Switch Connection	default
	...	AND	CLI:Delete All Devices

Test Event ID 304 with devices_events disabled
	[Setup]	Run Keywords	CLI:Enable Device Events	no
	...	AND	CLI:Add Device	${DEVICE_NAME}	device_console	${DEVICE_IP}	root	${ROOT_PASSWORD}
	...	AND	Wait Until Keyword Succeeds	5x	1s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	default
	CLI:Rename Device	${DEVICE_NAME}	${DEVICE_NAME2}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	5x	1s	CLI:Test Event ID Not Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Connect As Root
	CLI:Switch Connection	default
	CLI:Enable Auditing Destination To Events
	CLI:Delete All Devices
	CLI:Add Device	${DEVICE_NAME}	device_console	${DEVICE_IP}	root	${ROOT_PASSWORD}

SUITE:Teardown
	CLI:Switch Connection	default
	CLI:Delete All Devices
	CLI:Enable Device Events	yes
	CLI:Close Connection

