*** Settings ***
Resource	../../../../init.robot
Documentation	Test System Events section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	AUDITING	EVENTS	LOGGING	EVENT${EVENT_NUMBER}

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USER}	${DEFAULT_USERNAME}
${EVENT_NUMBER}	200
${EVENT200}	A user logged into the system. User: ${USER}@${CLIENTIP}. Session type: SSH. Authentication Method: Local.
${EVENT_DESCRIPTION}	${EVENT200}

*** Test Cases ***
Test Event ID 200 with aaa_events enabled
	[Setup]	Run Keywords	CLI:Enable AAA Events	yes
	...	AND	Wait Until Keyword Succeeds	10x	1s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	default
	CLI:Open	session_alias=event_session	startping=no
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	1m	1s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}	${EVENT_DESCRIPTION}
	[Teardown]	Run Keyword If Test Passed	Run Keywords	CLI:Switch Connection	event_session
	...	AND	CLI:Close Current Connection

Test Event ID 200 with aaa_events disabled
	[Setup]	Run Keywords	CLI:Switch Connection	default
	...	AND	CLI:Enable AAA Events	no
	...	AND	Wait Until Keyword Succeeds	10x	1s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	default
	CLI:Open	session_alias=event_session	startping=no
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	1m	1s	CLI:Test Event ID Not Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}
	[Teardown]	Run Keyword If Test Passed	Run Keywords	CLI:Switch Connection	event_session
	...	AND	CLI:Close Current Connection

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Connect As Root
	CLI:Switch Connection	default
	CLI:Enable Auditing Destination To Events

SUITE:Teardown
	CLI:Switch Connection	default
	CLI:Enable AAA Events	yes
	CLI:Close Connection
