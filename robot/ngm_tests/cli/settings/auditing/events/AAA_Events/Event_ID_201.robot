*** Settings ***
Resource	../../../../init.robot
Documentation	Test System Events section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	AUDITING	EVENTS	LOGGING	EVENT${EVENT_NUMBER}

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USER}	${DEFAULT_USERNAME}
${EVENT_NUMBER}	201
${EVENT201}	A user logged out of the system. User: ${USER}@${CLIENTIP}. Session type: SSH.
${EVENT_DESCRIPTION}	${EVENT201}

*** Test Cases ***
Test Event ID 201 with aaa_events enabled
	[Setup]	Run Keywords	CLI:Enable AAA Events	yes
	...	AND	Wait Until Keyword Succeeds	10x	1s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	default
	CLI:Open	session_alias=event_session	startping=no
	CLI:Close Current Connection
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	5x	1s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}	${EVENT_DESCRIPTION}

Test Event ID 201 with aaa_events disabled
	[Setup]	Run Keywords	CLI:Switch Connection	default
	...	AND	CLI:Enable AAA Events	no
	...	AND	Wait Until Keyword Succeeds	10x	1s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	default
	CLI:Open	session_alias=event_session	startping=no
	CLI:Close Current Connection
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	5x	1s	CLI:Test Event ID Not Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Connect As Root
	CLI:Check Nodegrid Resources Status
	CLI:Switch Connection	default
	CLI:Enable Auditing Destination To Events

SUITE:Teardown
	CLI:Switch Connection	default
	CLI:Enable AAA Events	yes
	CLI:Close Connection
