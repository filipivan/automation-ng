*** Settings ***
Resource	../../../../init.robot
Documentation	Test System Events section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	AUDITING	EVENTS	LOGGING	EVENT${EVENT_NUMBER}

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USER}	event${EVENT_NUMBER}
${PASSWD}	password
${EVENT_NUMBER}	111
${EVENT111}	A user was deleted from the system datastore. Command issued by user: ${DEFAULT_USERNAME}. Deleted user: ${USER}.
${EVENT_DESCRIPTION}	${EVENT111}

*** Test Cases ***
Test Event ID 111 with system_events enabled
	[Setup]	Wait Until Keyword Succeeds	30s	3s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	Wait Until Keyword Succeeds	30s	3s	SUITE:Delete User And Wait Event Enabled

Test Event ID 111 with system_events disabled
	[Setup]	Wait Until Keyword Succeeds	30s	3s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	Wait Until Keyword Succeeds	30s	3s	SUITE:Delete User And Wait Event Disabled

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Connect As Root
	CLI:Switch Connection	default
	CLI:Enable Auditing Destination To Events
	CLI:Add User	${USER}	${PASSWD}

SUITE:Teardown
	CLI:Switch Connection	default
	CLI:Enable Logging Events	yes
	Run Keyword If Any Tests Failed	CLI:Delete Users	${USER}
	CLI:Close Connection

SUITE:Delete User And Wait Event Enabled
	CLI:Switch Connection	default
	CLI:Enable System Events	yes
	CLI:Add User	${USER}	${PASSWD}
	CLI:Delete Users	${USER}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	3x	5s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}	${EVENT_DESCRIPTION}

SUITE:Delete User And Wait Event Disabled
	CLI:Switch Connection	default
	CLI:Enable System Events	no
	CLI:Add User	${USER}	${PASSWD}
	CLI:Delete Users	${USER}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	3x	5s	CLI:Test Event ID Not Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}