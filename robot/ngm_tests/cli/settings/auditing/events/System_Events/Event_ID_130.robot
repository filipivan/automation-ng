*** Settings ***
Resource	../../../../init.robot
Documentation	Test System Events section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	AUDITING	EVENTS	LOGGING	EVENT${EVENT_NUMBER}

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${LICENSE_KEY}	${FIFTY_DEVICES_ACCESS_LICENSE}
${SERIAL_NUMBER}	578
${NUMBER_LICENSE}	50
${EXPIRATION}	2025-01-01
${EVENT_NUMBER}	130
${EVENT130}	A new license key was added to the system. Type: 0. Serial Number: ${SERIAL_NUMBER}. Number of Licenses: ${NUMBER_LICENSE}. Expiration: ${EXPIRATION}.
${EVENT_DESCRIPTION}	${EVENT130}

*** Test Cases ***
Test Event ID 130 with system_events enabled
	[Setup]	Run Keywords	CLI:Enable System Events	yes
	...	AND	Wait Until Keyword Succeeds	5x	1s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	default
	CLI:Add License Key	${LICENSE_KEY}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	5x	1s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}	${EVENT_DESCRIPTION}
	[Teardown]	Run Keywords	CLI:Switch Connection	default
	...	AND	CLI:Delete All License Keys Installed

Test Event ID 130 with system_events disabled
	[Setup]	Run Keywords	CLI:Enable System Events	no
	...	AND	Wait Until Keyword Succeeds	5x	1s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	default
	CLI:Add License Key	${LICENSE_KEY}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	5x	1s	CLI:Test Event ID Not Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}
	[Teardown]	Run Keywords	CLI:Switch Connection	default
	...	AND	CLI:Delete All License Keys Installed

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Connect As Root
	CLI:Switch Connection	default
	CLI:Enable Auditing Destination To Events

SUITE:Teardown
	CLI:Switch Connection	default
	CLI:Enable Logging Events	yes
	Run Keyword If Any Tests Failed	CLI:Delete All License Keys Installed
	CLI:Close Connection

