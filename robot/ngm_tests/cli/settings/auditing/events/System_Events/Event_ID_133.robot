*** Settings ***
Resource	../../../../init.robot
Documentation	Test System Events section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	AUDITING	EVENTS	LOGGING	EVENT${EVENT_NUMBER}

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EXPECTED4_0}	Error: No license available.
${EXPECTED3_2}	Cannot add more devices. There are not enough licenses available or the system reached the maximum number of supported devices.
${DEVICE_NAME}	event${EVENT_NUMBER}_
${EVENT_NUMBER}	133
${EVENT133}	The number of available licenses is scarce. Type: 0.
${EVENT_DESCRIPTION}	${EVENT133}

*** Test Cases ***
Test Event ID 133 with system_events enabled
	[Setup]	Run Keywords	CLI:Enable System Events	yes
	...	AND	Wait Until Keyword Succeeds	5x	1s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	default
	${OUTPUT}	SUITE:Scarce Devices License
	${EXPECTED}=	Set Variable If	'${NGVERSION}' >= '4.0'	${EXPECTED4_0}	${EXPECTED3_2}
	Should Be Equal As Strings	${OUTPUT}	${EXPECTED}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	5x	5s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}	${EVENT_DESCRIPTION}
	[Teardown]	Run Keywords	CLI:Switch Connection	default
	...	AND	CLI:Cancel	Raw
	...	AND	CLI:Delete All Devices

Test Event ID 133 with system_events disabled
	[Setup]	Run Keywords	CLI:Enable System Events	no
	...	AND	Wait Until Keyword Succeeds	5x	1s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	default
	${OUTPUT}	SUITE:Scarce Devices License
	${EXPECTED}=	Set Variable If	'${NGVERSION}' >= '4.0'	${EXPECTED4_0}	${EXPECTED3_2}
	Should Be Equal As Strings	${OUTPUT}	${EXPECTED}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	5x	5s	CLI:Test Event ID Not Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Connect As Root
	CLI:Switch Connection	default
	CLI:Enable Auditing Destination To Events
	CLI:Delete All Devices

SUITE:Teardown
	CLI:Switch Connection	default
	CLI:Delete All Devices
	CLI:Enable Logging Events	yes
	CLI:Close Connection

SUITE:Scarce Devices License
	CLI:Add Device	${DEVICE_NAME}
	CLI:Enter Path	/settings/devices
	FOR	${I}	IN RANGE	40	1	-1
		CLI:Write	clone ${DEVICE_NAME}
		CLI:Set	name=${DEVICE_NAME} ip_address=127.0.0.1 number_of_clones=${I}
		${OUTPUT}=	Run Keyword And Return Status	CLI:Commit
		Exit For Loop If	${OUTPUT}
		CLI:Cancel
	END
	${OUTPUT}=	CLI:Write	add	Raw	user=Yes
	Return From Keyword If	'${NGVERSION}' == '3.2'	${OUTPUT}
	CLI:Set Field	name	${DEVICE_NAME}_fail
	${OUTPUT}=	CLI:Write	commit	Raw	user=Yes
	CLI:Cancel
	[Return]	${OUTPUT}



