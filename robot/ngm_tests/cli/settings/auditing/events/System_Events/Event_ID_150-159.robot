*** Settings ***
Resource	../../../../init.robot
Documentation	Test System Events 150-159 about Cluster through the CLI.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	NON-CRITICAL	EVENT150-159
Default Tags	AUDITING	EVENTS

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${HOSTNAME_HOST}	coordinator
${HOSTNAME_PEER}	peer
${PSK}	automationtest
${DUMMY_DEVICE_NAME_CONNECTED_IN_COORDINATOR}	dummy_device_connected_in_coordinator
${DUMMY_DEVICE_NAME_CONNECTED_IN_PEER}	dummy_device_connected_in_peer
${DUMMY_DEVICES_TYPE}	device_console
${DUMMY_DEVICES_IP_ADDRESS}	127.0.0.1
${DUMMY_DEVICES_MODE}	enabled
${EVENT150_NUMBER}	150
${EVENT151_NUMBER}	151
${EVENT152_NUMBER}	152
${EVENT153_NUMBER}	153
${EVENT154_NUMBER}	154
${EVENT155_NUMBER}	155
${EVENT156_NUMBER}	156
${EVENT157_NUMBER}	157
${EVENT158_NUMBER}	158
${EVENT159_NUMBER}	159
${EVENT150_DESCRIPTION}	A cluster peer is online. Cluster: ${HOSTNAME_NODEGRID}.
${EVENT151_DESCRIPTION}	A cluster peer is offline. Cluster: ${HOSTNAME_NODEGRID}.
${EVENT152_DESCRIPTION}	A peer signed on the cluster. Cluster: ${HOSTNAME_NODEGRID}.
${EVENT153_DESCRIPTION}	A peer signed off the cluster. Cluster: ${HOSTNAME_NODEGRID}.
${EVENT154_DESCRIPTION}	A cluster peer was removed. Cluster: ${HOSTNAME_NODEGRID}.
${EVENT155_DESCRIPTION}	A cluster peer became coordinator. Cluster: ${HOSTNAME_NODEGRID}.
${EVENT156_DESCRIPTION}	A cluster coordinator became peer. Cluster: ${HOSTNAME_NODEGRID}.
${EVENT157_DESCRIPTION}	A cluster coordinator was deleted. Cluster: ${HOSTNAME_NODEGRID}.
${EVENT158_DESCRIPTION}	A cluster coordinator was created. Cluster: ${HOSTNAME_NODEGRID}.
${EVENT159_DESCRIPTION}	A cluster peer was configured. Cluster: ${HOSTNAME_NODEGRID}.

*** Test Cases ***
Test Events IDs 158, 159 And 152 With System_Events Enabled
	Wait Until Keyword Succeeds	15s	3s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}	CLI_ALIAS=default	ROOT_ALIAS=root_session
	CLI:Switch Connection	default
	CLI:Enable System Events	yes
	SUITE:Setup Cluster Coordinator
	CLI:Switch Connection	peer_session
	CLI:Enable System Events	yes
	SUITE:Setup Cluster Peer
	CLI:Switch Connection	default
	SUITE:Wait Until Peer Included In Host Access
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	10s	2s	CLI:Test Event ID Logged	${HOSTNAME_HOST}	${EVENT158_NUMBER}	${EVENT158_DESCRIPTION}
	Wait Until Keyword Succeeds	10s	2s	CLI:Test Event ID Logged	${HOSTNAME_HOST}	${EVENT159_NUMBER}	${EVENT159_DESCRIPTION}
	Wait Until Keyword Succeeds	15s	2s	CLI:Test Event ID Logged	${HOSTNAME_HOST}	${EVENT152_NUMBER}	${EVENT152_DESCRIPTION}

Test Event IDs 155 And 156 With System_Events Enabled
	Wait Until Keyword Succeeds	15s	3s	CLI:Clear Event Logfile	${HOSTNAME_HOST}	CLI_ALIAS=default	ROOT_ALIAS=root_session
	CLI:Switch Connection	default
	SUITE:Edit Coordinator To Became Peer
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	10s	2s	CLI:Test Event ID Logged	${HOSTNAME_HOST}	${EVENT156_NUMBER}	${EVENT156_DESCRIPTION}
	CLI:Switch Connection	default
	SUITE:Edit Peer To Became Coordinator
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	10s	2s	CLI:Test Event ID Logged	${HOSTNAME_HOST}	${EVENT155_NUMBER}	${EVENT155_DESCRIPTION}

Test Event IDs 150 With System_Events Enabled
	Wait Until Keyword Succeeds	15s	3s	CLI:Clear Event Logfile	${HOSTNAME_HOST}	CLI_ALIAS=default	ROOT_ALIAS=root_session
	CLI:Switch Connection	peer_session
	Write	reboot
	SUITE:Read Until Regexp	do you want to proceed with system reboot\\? \\(yes, no\\)
	Write	yes
	SUITE:Read Until Regexp	The system is going down for reboot NOW!
	CLI:Close Connection
	CLI:Open	session_alias=default
	CLI:Connect As Root	session_alias=root_session
	Sleep	160s
	CLI:Open	session_alias=peer_session	HOST_DIFF=${HOSTPEER}
	CLI:Connect As Root	session_alias=peer_root_session	HOST_DIFF=${HOSTPEER}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	10s	2s	CLI:Test Event ID Logged	${HOSTNAME_HOST}	${EVENT150_NUMBER}	${EVENT150_DESCRIPTION}

Test Event IDs 154 With System_Events Enabled
	Wait Until Keyword Succeeds	15s	3s	CLI:Clear Event Logfile	${HOSTNAME_HOST}	CLI_ALIAS=default	ROOT_ALIAS=root_session
	CLI:Switch Connection	default
	SUITE:Remove Peer From Coordinator
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	15s	2s	CLI:Test Event ID Logged	${HOSTNAME_HOST}	${EVENT154_NUMBER}	${EVENT154_DESCRIPTION}

Test Event ID 157 With System_Events Enabled
	Wait Until Keyword Succeeds	15s	3s	CLI:Clear Event Logfile	${HOSTNAME_HOST}	CLI_ALIAS=peer_session	ROOT_ALIAS=peer_root_session
	CLI:Switch Connection	default
	SUITE:Enable Cluster Coordinator
	SUITE:Disable Cluster
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	15s	2s	CLI:Test Event ID Logged	${HOSTNAME_HOST}	${EVENT157_NUMBER}	${EVENT157_DESCRIPTION}
##################################################################################################################################
Test Events IDs 158 And 159 With System_Events Disabled
	Wait Until Keyword Succeeds	15s	3s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}	CLI_ALIAS=default	ROOT_ALIAS=root_session
	CLI:Switch Connection	default
	CLI:Enable System Events	no
	SUITE:Setup Cluster Coordinator
	CLI:Switch Connection	peer_session
	CLI:Enable System Events	no
	SUITE:Setup Cluster Peer
	CLI:Switch Connection	default
	SUITE:Wait Until Peer Included In Host Access
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	10s	2s	CLI:Test Event ID Not Logged	${HOSTNAME_HOST}	${EVENT158_NUMBER}
	Wait Until Keyword Succeeds	10s	2s	CLI:Test Event ID Not Logged	${HOSTNAME_HOST}	${EVENT159_NUMBER}

Test Event IDs 155 And 156 With System_Events Disabled
	Wait Until Keyword Succeeds	15s	3s	CLI:Clear Event Logfile	${HOSTNAME_HOST}	CLI_ALIAS=default	ROOT_ALIAS=root_session
	CLI:Switch Connection	default
	SUITE:Edit Coordinator To Became Peer
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	10s	2s	CLI:Test Event ID Not Logged	${HOSTNAME_HOST}	${EVENT156_NUMBER}
	CLI:Switch Connection	default
	SUITE:Edit Peer To Became Coordinator
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	10s	2s	CLI:Test Event ID Not Logged	${HOSTNAME_HOST}	${EVENT155_NUMBER}

Test Event IDs 150 And 151 With System_Events Disabled
	Wait Until Keyword Succeeds	15s	3s	CLI:Clear Event Logfile	${HOSTNAME_HOST}	CLI_ALIAS=default	ROOT_ALIAS=root_session
	CLI:Switch Connection	peer_session
	Write	reboot
	SUITE:Read Until Regexp	do you want to proceed with system reboot\\? \\(yes, no\\)
	Write	yes
	SUITE:Read Until Regexp	The system is going down for reboot NOW!
	CLI:Close Connection
	CLI:Open	session_alias=default
	CLI:Connect As Root	session_alias=root_session
	Sleep	160s
	CLI:Open	session_alias=peer_session	HOST_DIFF=${HOSTPEER}
	CLI:Connect As Root	session_alias=peer_root_session	HOST_DIFF=${HOSTPEER}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	10s	2s	CLI:Test Event ID Not Logged	${HOSTNAME_HOST}	${EVENT151_NUMBER}
	Wait Until Keyword Succeeds	10s	2s	CLI:Test Event ID Not Logged	${HOSTNAME_HOST}	${EVENT150_NUMBER}

Test Event IDs 154 With System_Events Disabled
	Wait Until Keyword Succeeds	15s	3s	CLI:Clear Event Logfile	${HOSTNAME_HOST}	CLI_ALIAS=default	ROOT_ALIAS=root_session
	CLI:Switch Connection	peer_session
	SUITE:Remove Peer From Coordinator
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	15s	2s	CLI:Test Event ID Not Logged	${HOSTNAME_HOST}	${EVENT154_NUMBER}

Test Event ID 157 With System_Events Disabled
	Wait Until Keyword Succeeds	15s	3s	CLI:Clear Event Logfile	${HOSTNAME_HOST}	CLI_ALIAS=peer_session	ROOT_ALIAS=peer_root_session
	CLI:Switch Connection	default
	SUITE:Enable Cluster Coordinator
	SUITE:Disable Cluster
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	15s	2s	CLI:Test Event ID Not Logged	${HOSTNAME_HOST}	${EVENT157_NUMBER}

*** Keywords ***
SUITE:Setup
	CLI:Open
	Set Client Configuration	timeout=60s
	CLI:Change Hostname	${HOSTNAME_HOST}
	CLI:Connect As Root
	CLI:Open	session_alias=peer_session	HOST_DIFF=${HOSTPEER}
	Set Client Configuration	timeout=60s
	CLI:Change Hostname	${HOSTNAME_PEER}
	CLI:Connect As Root	session_alias=peer_root_session	HOST_DIFF=${HOSTPEER}

SUITE:Teardown
	CLI:Open	session_alias=default	HOST_DIFF=${HOST}	#workaround added because sometimes admin session was somehow already closed
	CLI:Connect As Root	#workaround added because sometimes root session was somehow already closed
	Wait Until Keyword Succeeds	30s	3s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}	CLI_ALIAS=default	ROOT_ALIAS=root_session
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Change Hostname	${HOSTNAME_NODEGRID}
	CLI:Enable System Events	yes
	SUITE:Disable Cluster
	CLI:Delete All License Keys Installed
	CLI:Delete All Devices
	CLI:Open	session_alias=peer_session	HOST_DIFF=${HOSTPEER}
	CLI:Connect As Root	session_alias=peer_root_session_bkp	HOST_DIFF=${HOSTPEER}	#workaround added because sometimes first root session was somehow already closed
	Wait Until Keyword Succeeds	5x	3s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}	CLI_ALIAS=peer_session	ROOT_ALIAS=peer_root_session_bkp
	CLI:Switch Connection	peer_session
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Change Hostname	${HOSTNAME_NODEGRID}
	Wait Until Keyword Succeeds	3x	1s	CLI:Enable System Events	yes
	CLI:Delete All License Keys Installed
	CLI:Delete All Devices
	CLI:Close Connection

SUITE:Setup Cluster Coordinator
	SUITE:Disable Cluster
	CLI:Delete All License Keys Installed
	CLI:Delete All Devices
	CLI:Add License Key	${TEN_DEVICES_CLUSTERING_LICENSE}
	CLI:Add Device	DEVICE_NAME=${DUMMY_DEVICE_NAME_CONNECTED_IN_COORDINATOR}	DEVICE_TYPE=${DUMMY_DEVICES_TYPE}	MODE=${DUMMY_DEVICES_MODE}
	...	IP_ADDRESS=${DUMMY_DEVICES_IP_ADDRESS}	USERNAME=${USERNAME}	PASSWORD=${DEFAULT_PASSWORD}
	SUITE:Enable Cluster Coordinator

SUITE:Setup Cluster Peer
	CLI:Delete All License Keys Installed
	CLI:Delete All Devices
	CLI:Add Device	DEVICE_NAME=${DUMMY_DEVICE_NAME_CONNECTED_IN_PEER}	DEVICE_TYPE=${DUMMY_DEVICES_TYPE}	MODE=${DUMMY_DEVICES_MODE}
	...	IP_ADDRESS=${DUMMY_DEVICES_IP_ADDRESS}	USERNAME=${USERNAME}	PASSWORD=${DEFAULT_PASSWORD}
	SUITE:Enable Cluster Peer

SUITE:Remove Peer From Coordinator
	Switch Connection	default
	CLI:Enter Path	/settings/cluster/cluster_peers/
	CLI:Remove If Exists	${HOSTNAME_PEER}.${DOMAINAME_NODEGRID}
	${OUTPUT}=	CLI:Show
	Should Not Contain	${OUTPUT}	${HOSTNAME_PEER}.${DOMAINAME_NODEGRID}

SUITE:Disable Cluster
	SUITE:Remove Peer From Coordinator	#if cluster is disabled in the peer itself, Peer is already in the cluster error is raised
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	Set Client Configuration	timeout=120s
	CLI:Set	enable_cluster=no auto_enroll=yes auto_psk=nodegrid-key enable_peer_management=no
	CLI:Set	enable_license_pool=yes lps_type=client
	Set Client Configuration	timeout=45s
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	${DEFAULT_SETUP}	Create List
	...	enable_cluster = no
	...	auto_enroll = yes
	...	auto_psk = nodegrid-key
	...	enable_peer_management = no
	...	enable_license_pool = yes
	...	lps_type = client
	CLI:Test Show Command Regexp	@{DEFAULT_SETUP}
	[Teardown]	CLI:Cancel	Raw

SUITE:Enable Cluster Coordinator
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	CLI:Set	enable_cluster=yes type=coordinator allow_enrollment=yes psk=${PSK}
	Set Client Configuration	timeout=120s
	CLI:Commit
	CLI:Edit
	CLI:Set	cluster_mode=mesh polling_rate=30 auto_enroll=yes auto_psk=nodegrid-key psk=${PSK}
	CLI:Commit
	CLI:Edit
	CLI:Set	enable_clustering_access=yes enable_peer_management=yes enable_license_pool=yes lps_type=server psk=${PSK}
	CLI:Commit
	CLI:Edit
	CLI:Set	renew_time=1 lease_time=7
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	${EXPECTED_SETUP}	Create List
	...	enable_cluster = yes
	...	cluster( |_)name: nodegrid
	...	type = coordinator
	...	allow_enrollment = yes
	...	polling_rate = 30
	...	psk = automationtest
	...	enable_clustering_access = yes
	...	auto_enroll = yes
	...	auto_psk = nodegrid-key
	...	auto_interval = 30
	...	enable_peer_management = yes
	CLI:Test Show Command Regexp	@{EXPECTED_SETUP}
	[Teardown]	CLI:Cancel	Raw

SUITE:Enable Cluster Peer
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	CLI:Set	enable_cluster=yes type=peer coordinator_address=${HOST} psk=${PSK}
	Set Client Configuration	timeout=120s
	CLI:Commit
	CLI:Edit
	CLI:Set	enable_clustering_access=yes enable_peer_management=yes enable_license_pool=yes lps_type=client psk=${PSK}
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	${EXPECTED_SETUP}	Create List
	...	enable_cluster = yes
	...	cluster( |_)name: nodegrid
	...	type = peer
	...	coordinator_address = ${HOST}
	...	psk =
	...	enable_clustering_access = yes
	...	auto_enroll = yes
	...	auto_psk = nodegrid-key
	...	enable_peer_management = yes
	...	enable_license_pool = yes
	...	lps_type = client
	CLI:Test Show Command Regexp	@{EXPECTED_SETUP}
	[Teardown]	CLI:Cancel	Raw

SUITE:Edit Coordinator To Became Peer
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	CLI:Set	enable_cluster=yes type=peer coordinator_address=${HOSTPEER} psk=${PSK}
	Set Client Configuration	timeout=120s
	CLI:Commit
	CLI:Edit
	CLI:Set	enable_clustering_access=yes enable_peer_management=yes enable_license_pool=yes lps_type=client coordinator_address=${HOSTPEER} psk=${PSK}
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	${EXPECTED_SETUP}	Create List
	...	enable_cluster = yes
	...	cluster( |_)name: nodegrid
	...	type = peer
	...	coordinator_address = ()|(${HOST})
	...	psk =
	...	enable_clustering_access = yes
	...	auto_enroll = yes
	...	auto_psk = nodegrid-key
	...	enable_peer_management = yes
	...	enable_license_pool = yes
	...	lps_type = client
	CLI:Test Show Command Regexp	@{EXPECTED_SETUP}
	[Teardown]	CLI:Cancel	Raw

SUITE:Wait Until Peer Included In Host Access
	CLI:Switch Connection	default
	Wait Until Keyword Succeeds	90s	10s	SUITE:Check If Peer Exist In Host Access

SUITE:Check If Peer Exist In Host Access
	CLI:Enter Path	/access/
	${OUTPUT}=	CLI:Write	show
	Should Contain	${OUTPUT}	${HOSTNAME_PEER}.${DOMAINAME_NODEGRID}

SUITE:Read Until Regexp
	[Arguments]	${PROMPT}
	Set Client Configuration	timeout=30s
	${OUTPUT}=	Read Until Regexp	${PROMPT}	loglevel=INFO
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Log To Console	${OUTPUT}
	[Return]	${OUTPUT}

SUITE:Edit Peer To Became Coordinator
	CLI:Enter Path	/settings/cluster/settings
	CLI:Edit
	CLI:Set	type=coordinator psk=${PSK}
	Set Client Configuration	timeout=120s
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	[Teardown]	CLI:Cancel	Raw