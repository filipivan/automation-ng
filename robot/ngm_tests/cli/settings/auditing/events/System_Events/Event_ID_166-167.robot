*** Settings ***
Resource	../../../../init.robot
Documentation	Test System Events 166 and 167 about WireGuard Tunnel on CLI. And testing default trigger scripts on this events.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI  NON-CRITICAL
Default Tags	AUDITING	EVENTS

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
 
*** Variables ***
${WIREGUARD_NAME}  wireguard	
${WIREGUARD_INTERNAL_IP}	10.0.10.1/24
${WIREGUARD_LISTENING_PORT}  6000
${EVENT_NUMBER_UP}  166
${EVENT_NUMBER_DOWN}  167
${EVENT_DESCRIPTION_UP}  Wireguard Tunnel Up. Name: ${WIREGUARD_NAME}.
${EVENT_DESCRIPTION_DOWN}  Wireguard Tunnel Down. Name: ${WIREGUARD_NAME}.
${SCRIPT_LOG_COMPARE_UP}  Event Number: 166, Args Count: 2, Arg2: wireguard, Arg3: .
${SCRIPT_LOG_COMPARE_DOWN}  Event Number: 167, Args Count: 2, Arg2: wireguard, Arg3: .

*** Test Cases ***
Nodegrid Wireguard Tunnel Down
    CLI:Enter Path	/settings/wireguard/
	CLI:Write  stop_tunnel  ${WIREGUARD_NAME}
	CLI:Switch Connection	root_session
    Wait Until Keyword Succeeds  3x  10s  CLI:Test Event ID Logged  nodegrid  ${EVENT_NUMBER_DOWN}  ${EVENT_DESCRIPTION_DOWN}
	CLI:Enter Path	/tmp/
	${SCRIPT_LOG}=  CLI:Write  cat action_script_sample.log
	Should Contain	${SCRIPT_LOG}  ${SCRIPT_LOG_COMPARE_DOWN}

Nodegrid Wireguard Tunnel Up
    CLI:Switch Connection	default
	CLI:Write  start_tunnel  ${WIREGUARD_NAME}
	CLI:Switch Connection	root_session
    Wait Until Keyword Succeeds  3x  10s  CLI:Test Event ID Logged  nodegrid  ${EVENT_NUMBER_UP}  ${EVENT_DESCRIPTION_UP}
    CLI:Enter Path	/tmp/
	${SCRIPT_LOG}=  CLI:Write    cat action_script_sample.log
	Should Contain  ${SCRIPT_LOG}  ${SCRIPT_LOG_COMPARE_UP}

*** Keywords ***
SUITE:Setup
    CLI:Open
	CLI:Connect As Root
	SUITE:Delete log if exist

	CLI:Switch Connection	default
	SUITE:Configure Wireguard Tunnel
	SUITE:Put a script into events
	
SUITE:Teardown
    CLI:Switch Connection	default
    CLI:Delete All Wireguards
	CLI:Close Connection

SUITE:Configure Wireguard Tunnel
    CLI:Delete All Wireguards
	CLI:Add Wireguard  ${WIREGUARD_NAME}  ${WIREGUARD_INTERNAL_IP}  ${WIREGUARD_LISTENING_PORT}

SUITE:Put a script into events
    CLI:Enter Path	/settings/auditing/event_list/166
	CLI:Write  set action_script=actionscript_sample.sh
	CLI:Commit

	CLI:Enter Path	/settings/auditing/event_list/167
	CLI:Write  set action_script=actionscript_sample.sh
	CLI:Commit

SUITE:Delete log if exist
	CLI:Enter Path	/tmp/
    CLI:Write    rm action_script_sample.log