*** Settings ***
Resource	../../../../init.robot
Documentation	Test System Events section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	AUDITING	EVENTS	LOGGING	EVENT${EVENT_NUMBER}

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USER}	event${EVENT_NUMBER}
${PASSWD}	password
${EVENT_NUMBER}	108
${EVENT108}	The configuration has changed. Change made by user: ${DEFAULT_USERNAME}.
${EVENT_DESCRIPTION}	${EVENT108}

*** Test Cases ***
Test Event ID 108 with system_events enabled
	CLI:Enable System Events	yes
	Wait Until Keyword Succeeds	30s	3s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	default
	CLI:Add User	${USER}	${PASSWD}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	30s	3s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}	${EVENT_DESCRIPTION}
	[Teardown]	Run Keywords	CLI:Switch Connection	default
	...	AND	CLI:Delete Users	${USER}

Test Event ID 108 with system_events disabled
	CLI:Enable System Events	no
	Wait Until Keyword Succeeds	30s	3s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	default
	CLI:Add User	${USER}	${PASSWD}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	30s	3s	CLI:Test Event ID Not Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}
	[Teardown]	Run Keywords	CLI:Switch Connection	default
	...	AND	CLI:Delete Users	${USER}

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Connect As Root
	CLI:Switch Connection	default
	CLI:Enable Auditing Destination To Events
	CLI:Delete Users	${USER}

SUITE:Teardown
	CLI:Switch Connection	default
	CLI:Enable Logging Events	yes
	Run Keyword If Any Tests Failed	CLI:Delete Users	${USER}
	CLI:Close Connection