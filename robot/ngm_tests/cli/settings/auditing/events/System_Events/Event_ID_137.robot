*** Settings ***
Resource	../../../../init.robot
Documentation	Test System Events section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	AUDITING	EVENTS	LOGGING	EVENT${EVENT_NUMBER}

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USER}	${DEFAULT_USERNAME}
${COMMAND}	/bin/su -
${EVENT_NUMBER}	137
${EVENT137}	Sudo command executed. User: ${USER}. Client IP: ${CLIENTIP}. Command: ${COMMAND}
${EVENT_DESCRIPTION}	${EVENT137}

*** Test Cases ***
Test Event ID 137 with system_events enabled
	[Setup]	Run Keywords	CLI:Enable System Events	yes
	...	AND	Wait Until Keyword Succeeds	5x	1s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	default
	SUITE:Shell
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	5x	1s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}	${EVENT_DESCRIPTION}
	[Teardown]	CLI:Switch Connection	default

Test Event ID 137 with system_events disabled
	[Setup]	Run Keywords	CLI:Enable System Events	no
	...	AND	Wait Until Keyword Succeeds	5x	1s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	default
	SUITE:Shell
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	5x	1s	CLI:Test Event ID Not Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Connect As Root
	CLI:Switch Connection	default
	CLI:Enable Auditing Destination To Events

SUITE:Teardown
	CLI:Switch Connection	default
	CLI:Enable Logging Events	yes
	CLI:Close Connection

SUITE:Shell
	Write	shell
	${RETURN}=	Read Until	:~$
	Write	sudo su -
	${RETURN}=	Read Until	:~#
	Write	exit
	${RETURN}=	Read Until	:~$
	CLI:Write	exit
