*** Settings ***
Resource	../../../../init.robot
Documentation	Test System Events section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	AUDITING	EVENTS	LOGGING	EVENT${EVENT_NUMBER}

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USER}	event${EVENT_NUMBER}
${PASSWD}	password
${PASSWD2}	12345678
${EVENT_NUMBER}	112
${EVENT112}	A user was changed in the system datastore. Command issued by user: ${DEFAULT_USERNAME}. Modified user: ${USER}.
${EVENT_DESCRIPTION}	${EVENT112}

*** Test Cases ***
Test Event ID 112 with system_events enabled
	[Setup]	Run Keywords	CLI:Enable System Events	VALUE=yes
	...	AND	Wait Until Keyword Succeeds	30s	3s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	Wait Until Keyword Succeeds	30s	3s	SUITE:Change User And Wait Event Enabled

Test Event ID 112 with system_events disabled
	[Setup]	Run Keywords	CLI:Switch Connection	default	AND	CLI:Enable System Events	VALUE=no
	...	AND	Wait Until Keyword Succeeds	30s	3s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	Wait Until Keyword Succeeds	30s	3s	SUITE:Change User And Wait Event Disabled

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Connect As Root
	CLI:Switch Connection	default
	CLI:Enable Auditing Destination To Events
	CLI:Add User	${USER}	${PASSWD}

SUITE:Teardown
	CLI:Switch Connection	default
	CLI:Delete Users	${USER}
	CLI:Enable Logging Events	VALUE=yes
	CLI:Close Connection

SUITE:Change User
	[Arguments]	${USER}	${VALUE}
	CLI:Enter Path	/settings/local_accounts/${USER}
	CLI:Set	password=${VALUE}
	CLI:Commit

SUITE:Change User And Wait Event Enabled
	CLI:Switch Connection	default
	SUITE:Change User	${USER}	${PASSWD2}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	5x	2s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}	${EVENT_DESCRIPTION}

SUITE:Change User And Wait Event Disabled
	CLI:Switch Connection	default
	SUITE:Change User	${USER}	${PASSWD2}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	5x	2s	CLI:Test Event ID Not Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}

