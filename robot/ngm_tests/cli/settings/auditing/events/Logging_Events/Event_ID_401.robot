*** Settings ***
Resource	../../../../init.robot
Documentation	Test System Events section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	AUDITING	EVENTS	LOGGING	EVENT${EVENT_NUMBER}	NON-CRITICAL	BUG_NG_9920

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE_NAME}	event${EVENT_NUMBER}
${COMMAND}	whoami
${EVENT_NUMBER}	401
${EVENT401}	An alert string was detected on a device session. Device: ${DEVICE_NAME}. Alert string index: 1. Alert string: ${COMMAND}. Line text:
${EVENT_DESCRIPTION}	${EVENT401}

*** Test Cases ***
Test Event ID 401 with logging_events enabled
	[Setup]	Run Keywords	CLI:Enable Logging Events	yes
	...	AND	CLI:Enable Data Logging	${DEVICE_NAME}	yes	yes	${COMMAND}
	...	AND	Wait Until Keyword Succeeds	5x	1s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	default
	CLI:Open Device	${DEVICE_NAME}	session_alias=event_session	TYPE=root
	Wait Until Keyword Succeeds	5x	1s	CLI:Write	${COMMAND}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	5x	1s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}	${EVENT_DESCRIPTION}
	[Teardown]	Run Keyword If Test Passed	Run Keywords	CLI:Switch Connection	event_session
	...	AND	CLI:Close Current Connection

Test Event ID 401 with logging_events disabled
	[Setup]	Run Keywords	CLI:Switch Connection	default
	...	AND	CLI:Enable Logging Events	no
	...	AND	Wait Until Keyword Succeeds	5x	1s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	default
	CLI:Open Device	${DEVICE_NAME}	session_alias=event_session	TYPE=root
	Wait Until Keyword Succeeds	5x	1s	CLI:Write	${COMMAND}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	5x	1s	CLI:Test Event ID Not Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}
	[Teardown]	Run Keyword If Test Passed	Run Keywords	CLI:Switch Connection	event_session
	...	AND	CLI:Close Current Connection

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Connect As Root
	CLI:Switch Connection	default
	CLI:Enable Auditing Destination To Events
	CLI:Delete All Devices
	CLI:Add Device	${DEVICE_NAME}	device_console	127.0.0.1	root	${ROOT_PASSWORD}
	Wait Until Keyword Succeeds	1m	1s	CLI:Test Device Connection	${DEVICE_NAME}	Connected

SUITE:Teardown
	CLI:Switch Connection	default
	CLI:Delete Devices	${DEVICE_NAME}
	CLI:Enable Logging Events	yes
	Run Keyword If	'${HOST}' == '192.168.3.59'	CLI:Enable Session Logging	no
	CLI:Close Connection