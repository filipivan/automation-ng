*** Settings ***
Resource	../../../../init.robot
Documentation	Test System Events section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	DEPENDENCE_SERVER	DEPENDENCE_NFS	DEPENDENCE_SNMP	EVENT411	NON-CRITICAL
Default Tags	AUDITING	EVENTS	LOGGING	EVENT${EVENT_NUMBER}	NON-CRITICAL	NEED-REVIEW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NFSSERVER_HOST}	${NFSSERVER}
${NFSSERVER_USER}	${NFSSERVERUSER}
${NFSSERVER_PASSWORD}	${NFSSERVERPASSWORD}
${NFSSERVER_PATH}	${NFSPATH}
${SNMPSERVER_HOST}	${SNMPSERVER}
${SNMPSERVER_COMMUNITY}	${SNMPCOMMUNITY}
${SNMPSERVER_PORT}	${SNMPTCPPORT}

${COMMAND_STATUS}=	sudo service nfs-server status
${ACTIVED}	active (exited)
${INACTIVED}	inactive (dead)

${EVENT_DESCRIPTION}	Event ID 411: Logging messages to NFS server recovered.

*** Test Cases ***
Test Event ID 411 with logging_events enabled
	CLI:Enable Logging Events	yes
	CLI:Switch Connection	default
	SUITE:Stop NFS Server
	SUITE:Generate Events While NFS Server Is Down
	SUITE:Start Service
	Wait Until Keyword Succeeds	5x	20s	SUITE:Check If File Was Created Remotely
	Wait Until Keyword Succeeds	5x	20s	SUITE:Check NFS Logs For Event	${EVENT_DESCRIPTION}

Test Event ID 411 with logging_events disabled
	SUITE:Delete Files From NFS
	CLI:Enable Logging Events	no
	CLI:Switch Connection	default
	SUITE:Stop NFS Server
	SUITE:Generate Events While NFS Server Is Down
	SUITE:Start Service
	CLI:Switch Connection	nfs_connection
	${OUTPUT}	CLI:Write	cat ${NFSSERVER_PATH}/EVT/${HOSTNAME_NODEGRID}
	Should Not Contain	${OUTPUT}	${EVENT_DESCRIPTION}

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Open	${NFSSERVER_USER}	${NFSSERVER_PASSWORD}	nfs_connection	HOST_DIFF=${NFSSERVER_HOST}
	SUITE:Delete Files From NFS
	CLI:Switch Connection	default
	SUITE:Enable RPC	yes
	CLI:Enable Auditing Destination As NFS	${NFSSERVER_HOST}	${NFSSERVER_PATH}

SUITE:Teardown
	CLI:Switch Connection	default
	CLI:Enable Logging Events	yes
	SUITE:Enable RPC	no
	CLI:Enable Auditing Destination To Events
	SUITE:Start Service
	Sleep	120s	#workarond added once events continue to be generated and next test Event_ID_108 was not passing because of that
	CLI:Close Connection

SUITE:Enable RPC
	[Arguments]	${VALUE}
	CLI:Enter Path	/settings/services/
	CLI:Set Field	enable_rpc	${VALUE}
	CLI:Commit

SUITE:Delete Files From NFS
	[Arguments]	${ALIAS}=nfs_connection
	CLI:Switch Connection	${ALIAS}
	CLI:Write	cd ${NFSSERVER_PATH}
	CLI:Write	ls -l
	CLI:Write	sudo rm -R *
	${OUTPUT}=	CLI:Write	ls -l
	Should Not Contain Any	${OUTPUT}	DB	EVT	SNR

SUITE:Start Service
	CLI:Switch Connection	nfs_connection
	CLI:Write	sudo service nfs-server restart
	Wait Until Keyword Succeeds	5x	1s	SUITE:Service Status	${COMMAND_STATUS}	${ACTIVED}

SUITE:Stop NFS Server
	CLI:Switch Connection	nfs_connection
	${STATUS_ACT}=	Run Keyword And Return Status	SUITE:Service Status	${COMMAND_STATUS}	${ACTIVED}
	Run Keyword If	${STATUS_ACT} == ${FALSE}	CLI:Write	sudo service nfs-server start
	Wait Until Keyword Succeeds	5x	1s	SUITE:Service Status	${COMMAND_STATUS}	${ACTIVED}
	CLI:Write	sudo service nfs-server stop
	Wait Until Keyword Succeeds	5x	1s	SUITE:Service Status	${COMMAND_STATUS}	${INACTIVED}

SUITE:Service Status
	[Arguments]	${COMMAND}	${EXPECTED}
	${OUTPUT}=	CLI:Write	${COMMAND}
	Should Contain	${OUTPUT}	${EXPECTED}

SUITE:Generate Events While NFS Server Is Down
	CLI:Open	session_alias=event_session
	CLI:Enter Path	/settings/system_preferences
	CLI:Set	idle_timeout=0
	CLI:Commit
	CLI:Set	idle_timeout=300
	CLI:Commit
	CLI:Close Current Connection

SUITE:Check If File Was Created Remotely
	CLI:Switch Connection	nfs_connection
	${OUTPUT}	CLI:Write	cat ${NFSSERVER_PATH}/EVT/${HOSTNAME_NODEGRID}
	Should Not Contain	${OUTPUT}	No such file or directory

SUITE:Check NFS Logs For Event
	[Arguments]	${EXPECTED}
	CLI:Switch Connection	nfs_connection
	${OUTPUT}	CLI:Write	cat ${NFSSERVER_PATH}/EVT/${HOSTNAME_NODEGRID}
	Should Contain	${OUTPUT}	${EXPECTED}