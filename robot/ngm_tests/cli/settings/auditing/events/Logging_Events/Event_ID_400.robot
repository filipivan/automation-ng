*** Settings ***
Resource	../../../../init.robot
Documentation	Test System Events section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	AUDITING	EVENTS	LOGGING	EVENT${EVENT_NUMBER}

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${COMMAND}	whoami
${EVENT_NUMBER}	400
${EVENT400}	A system alert was detected. User: nodegrid. Alert index: 1. Alert string: ${COMMAND}. Line text:
${EVENT_DESCRIPTION}	${EVENT400}

*** Test Cases ***
Test Event ID 400 with logging_events enabled
	[Setup]	Run Keywords	CLI:Enable Logging Events	yes
	...	AND	CLI:Enable Session Logging	yes	yes	${COMMAND}
	...	AND	Wait Until Keyword Succeeds	5x	1s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	default
	# special case here user must login again to occurs event
	CLI:Open	session_alias=event_session	startping=no
	CLI:Write	${COMMAND}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	5x	1s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}	${EVENT_DESCRIPTION}
	[Teardown]	Run Keyword If Test Passed	Run Keywords	CLI:Switch Connection	event_session
	...	AND	CLI:Close Current Connection

Test Event ID 400 with logging_events disabled
	[Setup]	Run Keywords	CLI:Switch Connection	default
	...	AND	CLI:Enable Logging Events	no
	...	AND	Wait Until Keyword Succeeds	5x	1s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	default
	# special case here user must login again to occurs event
	CLI:Open	session_alias=event_session	startping=no
	CLI:Write	${COMMAND}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	5x	1s	CLI:Test Event ID Not Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}
	[Teardown]	Run Keyword If Test Passed	Run Keywords	CLI:Switch Connection	event_session
	...	AND	CLI:Close Current Connection

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Connect As Root
	CLI:Switch Connection	default
	CLI:Enable Auditing Destination To Events

SUITE:Teardown
	CLI:Switch Connection	default
	CLI:Enable Logging Events	yes
	#IP 192.168.2.34 is no longer valid, 192.168.3.59 is NGM4.2.8
	Run Keyword If	'${HOST}' == '192.168.3.59'	CLI:Enable Session Logging	no
	CLI:Close Connection
