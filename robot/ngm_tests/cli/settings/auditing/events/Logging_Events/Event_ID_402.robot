*** Settings ***
Resource	../../../../init.robot
Documentation	Test System Events section
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	DEPENDENCE_DEVICE	DEPENDENCE_ILO
Default Tags	AUDITING	EVENTS	LOGGING	EVENT${EVENT_NUMBER}

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EXECUTE_ILO_TESTS}	${FALSE}
${COMMAND}	IML Cleared
${DEVICE_NAME}	event${EVENT_NUMBER}
${DEVICE_TYPE}	ilo
${DEVICE_IP}	${ILO_IP}
${DEVICE_USERNAME}	${ILO_USERNAME}
${DEVICE_PASSWORD}	${ILO_PASSWORD}
${EVENT_NUMBER}	402
${EVENT402}	An event log alert string was detected on a device event log. Device: ${DEVICE_NAME}. Alert string index: 1. Alert string: ${COMMAND}.
${EVENT_DESCRIPTION}	${EVENT402}

*** Test Cases ***
Test Event ID 402 with logging_events enabled
	[Setup]	Run Keyword If	${EXECUTE_ILO_TESTS} == ${FALSE}	Skip	ILO not available to use in test.
	...	ELSE	Run Keywords	CLI:Enable Logging Events	yes
	...	AND	CLI:Enable Event Logging	${DEVICE_NAME}	yes	yes	1	minutes	${COMMAND}
	...	AND	Wait Until Keyword Succeeds	5x	1s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	Skip If	${EXECUTE_ILO_TESTS} == ${FALSE}	ILO not available to use in test.
	CLI:Switch Connection	default
	CLI:Open Device	${DEVICE_NAME}	session_alias=event_session	TYPE=root
	CLI:Write	${COMMAND}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	5x	1s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}	${EVENT_DESCRIPTION}
	[Teardown]	Run Keyword If	${EXECUTE_ILO_TESTS} == ${FALSE}	Skip	ILO not available to use in test.
	...	ELSE	Run Keywords	CLI:Switch Connection	event_session
	...	AND	CLI:Close Current Connection

Test Event ID 402 with logging_events disabled
	[Setup]	Run Keyword If	${EXECUTE_ILO_TESTS} == ${FALSE}	Skip	ILO not available to use in test.
	...	ELSE	Run Keywords	CLI:Switch Connection	default
	...	AND	CLI:Enable Logging Events	no
	...	AND	Wait Until Keyword Succeeds	5x	1s	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	Skip If	${EXECUTE_ILO_TESTS} == ${FALSE}	ILO not available to use in test.
	CLI:Switch Connection	default
	CLI:Open Device	${DEVICE_NAME}	session_alias=event_session	TYPE=root
	CLI:Write	${COMMAND}
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	5x	1s	CLI:Test Event ID Not Logged	${HOSTNAME_NODEGRID}	${EVENT_NUMBER}
	[Teardown]	Run Keyword If	${EXECUTE_ILO_TESTS} == ${FALSE}	Skip	ILO not available to use in test.
	...	ELSE	Run Keywords	CLI:Switch Connection	event_session
	...	AND	CLI:Close Current Connection

*** Keywords ***
SUITE:Setup
	Skip If	${EXECUTE_ILO_TESTS} == ${FALSE}	ILO not available to use in test.
	CLI:Open
	CLI:Connect As Root
	CLI:Switch Connection	default
	CLI:Enable Auditing Destination To Events
	CLI:Delete All Devices
#	CLI:Delete Devices	${DEVICE_NAME}
	CLI:Add Device	${DEVICE_NAME}	${DEVICE_TYPE}	${DEVICE_IP}	${DEVICE_USERNAME}	${DEVICE_PASSWORD}
	Wait Until Keyword Succeeds	1m	1s	CLI:Test Device Connection	${DEVICE_NAME}	Connected

SUITE:Teardown
	Skip If	${EXECUTE_ILO_TESTS} == ${FALSE}	ILO not available to use in test.
	CLI:Switch Connection	default
	CLI:Delete Devices	${DEVICE_NAME}
	CLI:Enable Logging Events	yes
	CLI:Enable Event Logging	no
	CLI:Close Connection