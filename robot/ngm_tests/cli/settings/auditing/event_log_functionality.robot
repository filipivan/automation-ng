*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Event Log Feature through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EVENT_LOG
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${AUTOMATION_DEVICE}	automation_device
*** Test Cases ***
Test Event Log System Event Category
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Event Log Command
	CLI:Write	event_system_clear
	SUITE:Set Idle Timeout	301
	SUITE:Set Idle Timeout	300
	Write	event_system_audit
	${OUTPUT}=	Read Until	(h->Help, q->Quit)
	Should Contain	${OUTPUT}	Change made by user: ${USER}.
	CLI:Write	q
	Should Contain	${OUTPUT}	108
	[Teardown]	SUITE:Set Idle Timeout	300

Test Event Log AAA Event Category
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Event Log Command
	CLI:Write	event_system_clear
	CLI:Open	session_alias=another_session
	${OUTPUT}=	CLI:Ls
	Should Contain	${OUTPUT}	access/	system/	settings/
	CLI:Close Connection	Yes
	Sleep	3s
	CLI:Switch Connection	admin_session
	@{EVENT_LOG_LIST}=	Create List	 Event ID 200	 Event ID 201
	Write	event_system_audit
	${OUTPUT}=	Read Until	(h->Help, q->Quit)
	Should Contain	${OUTPUT}	A user logged out of the system.
	CLI:Write	q
	FOR	${ITEM}	IN	@{EVENT_LOG_LIST}
		Should Contain	${OUTPUT}	${ITEM}
	END

Test Event Log Device Event Category
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Event Log Command
	CLI:Write	event_system_clear
	SUITE:Add Device
	CLI:Delete Device	${AUTOMATION_DEVICE}
	@{EVENT_LOG_LIST}=	Create List	 Event ID 302	 Event ID 108	Event ID 303
	Write	event_system_audit
	${OUTPUT}=	Read Until	(h->Help, q->Quit)
	Should Contain	${OUTPUT}	Device deleted. User: ${USER}. Device: ${AUTOMATION_DEVICE}
	CLI:Write	q
	FOR	${ITEM}	IN	@{EVENT_LOG_LIST}
		Should Contain	${OUTPUT}	${ITEM}
	END

Test Event Log Empty
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Event Log Command
	CLI:Write	event_system_clear
	@{EVENT_LOG_LIST}=	Create List	 Event ID 302	Event ID 303	Event ID 108	Event ID 200	Event ID 201
	${OUTPUT}=	Write	event_system_audit
	CLI:Write	q
	FOR	${ITEM}	IN	@{EVENT_LOG_LIST}
		Should Not Contain	${OUTPUT}	${ITEM}
	END

*** Keywords ***
SUITE:Setup
	CLI:Open	session_alias=admin_session
	SUITE:Delete Device
	${USER}	CLI:Get User Whoami
	Set Suite Variable	${USER}
	SUITE:Enable Event Log

SUITE:Teardown
	SUITE:Delete Device
	CLI:Close Connection

SUITE:Enable Event Log
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Set	destination=local
	CLI:Commit
	CLI:Enter Path	/settings/auditing/settings/
	CLI:Set	datalog_destination=file
	CLI:Commit
	CLI:Enter Path	/settings/auditing/events/file/
	CLI:Set	aaa_events=yes device_events=yes logging_events=yes system_events=yes
	CLI:Commit

SUITE:Add Device
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Set	name=${AUTOMATION_DEVICE} ip_address=127.0.0.1 type=device_console
	CLI:Commit

SUITE:Check Device
	CLI:Enter Path	/settings/devices/
	${OUTPUT}=	CLI:Ls
	Should Contain	${OUTPUT}	${AUTOMATION_DEVICE}

SUITE:Delete Device
	CLI:Enter Path	/settings/devices/
	CLI:Delete If Exists Confirm	${AUTOMATION_DEVICE}
	CLI:Commit

SUITE:Set Idle Timeout
	[Arguments]	${IDLE_TIMEOUT}
	CLI:Enter Path	/settings/system_preferences/
	CLI:Set	idle_timeout=${IDLE_TIMEOUT}
	CLI:Commit

SUITE:Check Event Log Command
	CLI:Enter Path	/
	${OUTPUT}=	CLI:Write	\t\t
	Should Contain	${OUTPUT}	event_system_audit	event_system_clear