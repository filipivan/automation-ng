*** Settings ***
Resource	../../../init.robot
Documentation	Tests allow to make log files persistence through reboot
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2    NON-CRITICAL
Default Tags	SSH	AUDITING	SETTINGS
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${file_content}     Testing log files persistnce through reboot
${nfs_path}     /nfs

*** Test Cases ***
Test case to Enable persistence logs
    CLI:Switch Connection	default
    CLI:Enter Path	/settings/auditing/settings
    CLI:Set  enable_persistent_logs=yes
    CLI:Commit
    ${OUTPUT}=  CLI:Show
    Should Contain   ${OUTPUT}      enable_persistent_logs = yes

Test case to set destination to nfs server
    CLI:Switch Connection	default
    SUITE:Add configuration for destination as nfs server
    CLI:Close Connection
    CLI:Open
	CLI:Connect As Root
	CLI:Switch Connection	default

check the logs on nfs server before reboot
    SUITE:LOGIN to NFS server and check the logs

Test case to set destination to local
    CLI:Switch Connection	default
    SUITE:Add local destination configuration

Test case to see the log files and write into file
    CLI:Switch Connection   root_session
    CLI:Enter Path  /var/local/LOGS
    ${check_log_files}=  CLI:Write  ls
    Should Contain  ${check_log_files}   dlog.log  messages  wtmp
    CLI:Write   echo "${file_content}" > /var/local/LOGS/dlog.log
    ${check_file_content}=  CLI:Write  cat dlog.log
    Should Contain  ${check_file_content}   ${file_content}

Test case to check file content persistence for local destination on reboot
    CLI:Enter Path	/system/toolkit/
    CLI:Write  reboot
    Sleep    240s
    CLI:Connect As Root
    CLI:Enter Path  /var/local/LOGS
    ${check_file_content}=  CLI:Write   cat dlog.log
    Should Contain  ${check_file_content}   ${file_content}
    SUITE:LOGIN to NFS server and check the logs

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Connect As Root

SUITE:Disable Persistence Log
    CLI:Enter Path	/settings/auditing/settings
    CLI:Set  enable_persistent_logs=no
    CLI:Commit

SUITE:Teardown
	CLI:Close Connection
	CLI:Open
    SUITE:Disable Persistence Log
    CLI:Close Connection

SUITE:LOGIN to NFS server and check the logs
    CLI:General Open    ${NFSSERVER}    ${NFSSERVERUSER}    ${NFSSERVERPASSWORD}    nfs_session
    CLI:Enter Path     /nfs/LOGS
    ${OUTPUT}   CLI:Write  ls
    Should Contain  ${OUTPUT}   dlog.log  messages  wtmp
    ${OUTPUT}=    CLI:Write   cat /messages
    Should Not Contain    ${OUTPUT}     Event ID 200:   Event ID 201:   Event ID 135:

SUITE:Add configuration for destination as nfs server
    CLI:Enter Path	/settings/auditing/destinations/file/
    CLI:Set  destination=nfs nfs_server=192.168.2.115 nfs_path=/nfs
    CLI:Commit
    ${check_destination}=  CLI:Show
    Should Contain  ${check_destination}   destination = nfs

SUITE:Add local destination configuration
    CLI:Enter Path	/settings/auditing/destinations/file/
    CLI:set    destination=local file_size=256 number_of_archives=1
    CLI:Commit
    ${check_dest}=  CLI:Show
    Should Contain  ${check_dest}   destination = local