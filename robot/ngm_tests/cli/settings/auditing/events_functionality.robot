*** Settings ***
Resource	../../init.robot
Documentation	Tests Auditing Events section
Metadata	Version 1.0
Metadata	Executed At ${HOST}
Force Tags	CLI
Default Tags	CLI SSH AUDITING	DESTINATION

Suite Setup	Test suite setup
Suite Teardown	Test suite teardown

*** Test Cases ***
Test successfull system events
	Skip If	"${NFSSERVERPRESENT}" == "No"	No Physical NFS Server available. Test will be skipped.
	${HOSTNAME}=	CLI:Get Hostname
	Log	\nhostname: ${HOSTNAME}	INFO	console=yes
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Set Field	destination	local
	CLI:Commit

	Wait Until Keyword Succeeds	3x	200ms	CLI:TRUNCATE LOGFILE

	CLI:Open	session_alias=eventsession	startping=no
	Close Connection

	Wait Until Keyword Succeeds	3x	200ms	CLI:TEST EVENT ID	200	A user logged into the system. User: ${DEFAULT_USERNAME}@${CLIENTIP}. Session type: SSH. Authentication Method: Local.
	Close Connection

*** Keywords ***
CLI:TEST EVENT ID
	[Arguments]	${EVENTID}	${EVENTDESCRIPTION}
	CLI:Switch Connection	root_session
	Write	tail -5 /var/local/EVT/${HOSTNAME}
	${OUTPUT}=	Read Until Prompt
	Should Contain	${OUTPUT}	Event ID ${EVENTID}: ${EVENTDESCRIPTION}
	Log	\nEvent File Output: ${OUTPUT}	INFO	console=yes

CLI:TRUNCATE LOGFILE
	CLI:Switch Connection	default
	CLI:Enter Path	/
	CLI:Write	event_system_clear
	CLI:Switch Connection	root_session
	Write	tail -5 /var/local/EVT/nodegrid
	${OUTPUT}=	Read Until Prompt
	Should not Contain	${OUTPUT}	Event ID

Test suite setup
	CLI:Open
	${VAR}=	CLI:Get Hostname
	Set Suite Variable	${HOSTNAME}	${VAR}
	Log	\nhostname: ${HOSTNAME}	INFO	console=yes
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Set Field	destination	local
	CLI:Commit
	CLI:Enter Path	/settings/auditing/settings/
	CLI:Set Field	datalog_destination	file
	CLI:Commit
	CLI:Enter Path	/settings/auditing/events/file/
	CLI:SET FIELD	aaa_events	yes
	CLI:SET FIELD	device_events	yes
	CLI:SET FIELD	logging_events	yes
	CLI:SET FIELD	system_events	yes
	CLI:Commit

	Set Default Configuration	loglevel=INFO
	Set Default Configuration	prompt=#
	Set Default Configuration	newline=\n
	Set Default Configuration	width=400
	Set Default Configuration	height=600
	Set Default Configuration	timeout=300

	CLI:Connect As Root
	CLI:Switch Connection	default

Test suite teardown
	CLI:Close Connection