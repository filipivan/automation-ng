*** Settings ***
Resource	../../init.robot
Documentation	Tests Auditing Events section
Metadata	Version 1.0
Metadata	Executed At ${HOST}
Force Tags	CLI
Default Tags	CLI SSH AUDITING	DESTINATION

Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/auditing/settings
	CLI:Test Available Commands	commit	exit	ls	reboot	set	show_settings	system_certificate	cd	event_system_audit	factory_settings	pwd	revert	shell	shutdown	system_config_check	change_password	event_system_clear	hostname	quit	save_settings	show	software_upgrade	whoami

Check Auditing settings fields
	CLI:Enter Path	/settings/auditing/settings
	CLI:Test Show Command	datalog_add_timestamp	datalog_destination	datalog_timestamp_format	event_timestamp_format

Test available fields to set command
	CLI:Enter Path	/settings/auditing/settings
	CLI:Test Set Available Fields	datalog_add_timestamp	datalog_destination	datalog_timestamp_format	event_timestamp_format

Test correct values for field=datalog_add_timestamp
	CLI:Enter Path	/settings/auditing/settings
	CLI:Test Set Field Options	datalog_add_timestamp	no	yes

Test incorrect values for field=datalog_add_timestamp
	${FIELD_VALUE}=	Set Variable	datalog_add_timestamp
	CLI:Enter Path	/settings/auditing/settings/
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a

Test correct values for field=datalog_timestamp_format
	CLI:Enter Path	/settings/auditing/settings
	CLI:Test Set Field Options	datalog_timestamp_format	local_time	utc

Test incorrect values for field=datalog_timestamp_format
	${FIELD_VALUE}=	Set Variable	datalog_timestamp_format
	CLI:Enter Path	/settings/auditing/settings/
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a

Test correct values for field=event_timestamp_format
	CLI:Enter Path	/settings/auditing/settings
	CLI:Test Set Field Options	event_timestamp_format	local_time	utc

Test incorrect values for field=event_timestamp_format
	${FIELD_VALUE}=	Set Variable	event_timestamp_format
	CLI:Enter Path	/settings/auditing/settings/
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a

Test correct values for field=datalog_destination
	CLI:Enter Path	/settings/auditing/settings
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Options	datalog_destination	file	syslog
	${OUTPUT}=	CLI:Write	set datalog_destination=file
	Should Not Contain	${OUTPUT}	Error
	${OUTPUT}=	CLI:Write	set datalog_destination=syslog
	Should Not Contain	${OUTPUT}	Error
	${OUTPUT}=	CLI:Write	set datalog_destination=file,syslog
	Should Not Contain	${OUTPUT}	Error
	${OUTPUT}=	CLI:Write	set datalog_destination=syslog,file
	Should Not Contain	${OUTPUT}	Error
	CLI:Revert

Test incorrect values for field=datalog_destination
	${FIELD_VALUE}=	Set Variable	datalog_destination
	CLI:Enter Path	/settings/auditing/settings/
#	Run Keyword If '${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: datalog_destination: Validation error.
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: datalog_destination: Validation error.
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: datalog_destination: Validation error.
	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	syslog,file,syslog	Error: ${FIELD_VALUE}: Validation error.

Test show_settings command
	CLI:Enter Path	/settings/auditing/
	Write	show_settings
	${OUTPUT}=	CLI:Read Until Prompt
	Should Match Regexp	${OUTPUT}	/settings/auditing/settings\\sevent_timestamp_format=utc|local_time
	Should Match Regexp	${OUTPUT}	/settings/auditing/settings\\sdatalog_destination=file|syslog
	Should Match Regexp	${OUTPUT}	/settings/auditing/settings\\sdatalog_add_timestamp=no|yes
	Should Match Regexp	${OUTPUT}	/settings/auditing/settings\\sdatalog_timestamp_format=utc|local_time

	Should Match Regexp	${OUTPUT}	/settings/auditing/events/email\\ssystem_events=no|yes
	Should Match Regexp	${OUTPUT}	/settings/auditing/events/email\\saaa_events=no|yes
	Should Match Regexp	${OUTPUT}	/settings/auditing/events/email\\sdevice_events=no|yes
	Should Match Regexp	${OUTPUT}	/settings/auditing/events/email\\slogging_events=no|yes

	Should Match Regexp	${OUTPUT}	/settings/auditing/events/file\\ssystem_events=yes|no
	Should Match Regexp	${OUTPUT}	/settings/auditing/events/file\\saaa_events=yes|no
	Should Match Regexp	${OUTPUT}	/settings/auditing/events/file\\sdevice_events=yes|no
	Should Match Regexp	${OUTPUT}	/settings/auditing/events/file\\slogging_events=yes|no

	Should Match Regexp	${OUTPUT}	/settings/auditing/events/snmp_trap\\ssystem_events=no|yes
	Should Match Regexp	${OUTPUT}	/settings/auditing/events/snmp_trap\\saaa_events=no|yes
	Should Match Regexp	${OUTPUT}	/settings/auditing/events/snmp_trap\\sdevice_events=no|yes
	Should Match Regexp	${OUTPUT}	/settings/auditing/events/snmp_trap\\slogging_events=no|yes

	Should Match Regexp	${OUTPUT}	/settings/auditing/events/syslog\\ssystem_events=yes|no
	Should Match Regexp	${OUTPUT}	/settings/auditing/events/syslog\\saaa_events=yes|no
	Should Match Regexp	${OUTPUT}	/settings/auditing/events/syslog\\sdevice_events=yes|no
	Should Match Regexp	${OUTPUT}	/settings/auditing/events/syslog\\slogging_events=yes|no

	Should Match Regexp	${OUTPUT}	/settings/auditing/destinations/file\\sdestination=local
	Should Match Regexp	${OUTPUT}	/settings/auditing/destinations/file\\sfile_size=\\d+
	Should Match Regexp	${OUTPUT}	/settings/auditing/destinations/file\\snumber_of_archives=\\d+
	Should Match Regexp	${OUTPUT}	/settings/auditing/destinations/file\\snfs_file_size=\\d+
	Should Match Regexp	${OUTPUT}	/settings/auditing/destinations/file\\snumber_of_archives_in_nfs=\\d+

	Should Match Regexp	${OUTPUT}	/settings/auditing/destinations/syslog\\ssystem_console=yes|no
	Should Match Regexp	${OUTPUT}	/settings/auditing/destinations/syslog\\sadmin_session=no|yes
	Should Match Regexp	${OUTPUT}	/settings/auditing/destinations/syslog\\sipv4_remote_server=no|yes
	Should Match Regexp	${OUTPUT}	/settings/auditing/destinations/syslog\\sipv6_remote_server=no|yes
	Should Match Regexp	${OUTPUT}	/settings/auditing/destinations/syslog\\sevent_facility=log_local_\\d+
	Should Match Regexp	${OUTPUT}	/settings/auditing/destinations/syslog\\sdatalog_facility=log_local_\\d+

	#Run Keyword If	'${NGVERSION}' >= '4.0' /settings/auditing/destinations/snmptrap\\ssnmp_engine_id=.+
	Run Keyword If	'${NGVERSION}' >= '4.0' and '${NGVERSION}' < '5.0'  Should Match Regexp	${OUTPUT}	/settings/auditing/destinations/snmptrap\\ssnmp_engine_id=.+
	Should Match Regexp	${OUTPUT}	/settings/auditing/destinations/snmptrap\\ssnmptrap_server="*\\d+\\.\\d+\\.\\d+\\.\\d+\\s*"*
	Should Match Regexp	${OUTPUT}	/settings/auditing/destinations/snmptrap\\ssnmptrap_transport_protocol=udp-ipv4|tcp-ipv4|tcp-ipv6|udp-ipv6
	Should Match Regexp	${OUTPUT}	/settings/auditing/destinations/snmptrap\\ssnmptrap_port=\\d+
	Should Match Regexp	${OUTPUT}	/settings/auditing/destinations/snmptrap\\ssnmptrap_version=version_.+

	Should Match Regexp	${OUTPUT}	/settings/auditing/destinations/email\\semail_port=\\d+
	Should Match Regexp	${OUTPUT}	/settings/auditing/destinations/email\\spassword=\\*+
	Should Match Regexp	${OUTPUT}	/settings/auditing/destinations/email\\sconfirm_password=\\*+
	Should Match Regexp	${OUTPUT}	/settings/auditing/destinations/email\\sstart_tls=yes|no