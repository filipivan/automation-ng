*** Settings ***
Resource	../../init.robot
Documentation	Functionality tests related to auditing event_list section
Metadata	Version 1.0
Metadata	Executed At ${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
Default Tags	CLI	SSH	AUDITING

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE}	test-event-list-device

*** Test Cases ***
Test Disable Events 100,101,102,301
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/auditing/event_list/
	CLI:Write	disable 100,101,102,301
	CLI:Commit
	CLI:Test Show Command Regexp	100\\s+no	101\\s+no	102\\s+no	301\\s+no
	[Teardown]	CLI:Revert	Raw

Test Enable Events 100,101,102,301
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/auditing/event_list/
	CLI:Write	enable 100,101,102,301
	CLI:Commit
	CLI:Test Show Command Regexp	100\\s+yes	101\\s+yes	102\\s+yes	301\\s+yes
	[Teardown]	CLI:Revert	Raw

Test Disable Event 135 and check logging
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/auditing/event_list/
	CLI:Write	disable 135
	CLI:Commit
	CLI:Test Show Command Regexp	135\\s+no

	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	root_session
	CLI:Test Event ID Not Logged	${HOSTNAME_NODEGRID}	135
	[Teardown]	CLI:Revert	Raw

Test Disable Event 200 and check logging
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/auditing/event_list/
	CLI:Write	disable 200
	CLI:Commit
	CLI:Test Show Command Regexp	200\\s+no

	SUITE:Clear Event Logfile	${HOSTNAME_NODEGRID}	root_session
	CLI:Close Connection
	CLI:Open
	CLI:Connect As Root
	CLI:Test Event ID Not Logged	${HOSTNAME_NODEGRID}	200
	[Teardown]	CLI:Revert	Raw

Test Enable Event 135 and check logging
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/auditing/event_list/
	CLI:Write	enable 135
	CLI:Commit
	CLI:Test Show Command Regexp	135\\s+yes

	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	CLI:Switch Connection	default
	CLI:Write	shell ls /home/admin/
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	60s	3s	CLI:Test Event ID Logged
	...	${HOSTNAME_NODEGRID}	135	Shell session started.
	[Teardown]	CLI:Revert	Raw

Test Enable Event 200 and check logging
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/auditing/event_list/
	CLI:Write	enable 200
	CLI:Commit
	CLI:Test Show Command Regexp	200\\s+yes

	CLI:Clear Event Logfile	${HOSTNAME_NODEGRID}
	CLI:Close Connection
	CLI:Open
	CLI:Connect As Root
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	60s	3s	CLI:Test Event ID Logged	${HOSTNAME_NODEGRID}	200
	...	A user logged into the system. User: admin@${CLIENTIP}. Session type: SSH. Authentication Method: Local.
	[Teardown]	CLI:Revert	Raw

Test trigger Event with action script configured
	CLI:Switch Connection	default
	SUITE:Configure Script For Events
	CLI:Add Device	${DEVICE}
	CLI:Delete Devices	${DEVICE}
	SUITE:Script Should Have Been Run

*** Keywords ***
SUITE:Setup
	CLI:Connect As Root
	CLI:Open
	Set Client Configuration	timeout=100s
	SUITE:Remove Script Log File
	CLI:Delete All Devices

SUITE:Teardown
	SUITE:Enable All Used Events And Check
	SUITE:Remove Settings From Event On List
	CLI:Delete Devices	${DEVICE}
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Close Connection

SUITE:Enable All Used Events And Check
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/auditing/event_list/
	CLI:Write	enable 100,101,102,135,200,301
	CLI:Commit
	CLI:Test Show Command Regexp	100\\s+yes	101\\s+yes	102\\s+yes	135\\s+yes	200\\s+yes	301\\s+yes

SUITE:Remove Script Log File
	CLI:Write	shell sudo rm /tmp/action_script_sample.log

SUITE:Configure Script For Events
	CLI:Enter Path	/settings/auditing/event_list/302/
	CLI:Set	enable=yes action_script=actionscript_sample.sh
	CLI:Commit
	CLI:Enter Path	/settings/auditing/event_list/303/
	CLI:Set	enable=yes action_script=actionscript_sample.sh
	CLI:Commit

SUITE:Script Should Have Been Run
	${OUTPUT}=	CLI:Write	shell cat /tmp/action_script_sample.log
	Should Contain	${OUTPUT}	Event Number: 302
	Should Contain	${OUTPUT}	Event Number: 303

SUITE:Remove Settings From Event On List
	CLI:Enter Path	/settings/auditing/event_list/302/
	CLI:Set	action_script=
	CLI:Commit
	CLI:Enter Path	/settings/auditing/event_list/303/
	CLI:Set	action_script=
	CLI:Commit

SUITE:Clear Event Logfile
	[Documentation]	Keyword makes the event file empty using echo -n and then checks is really empty with cat command
	[Arguments]	${HOSTNAME}	${ROOT_ALIAS}=root_session
	CLI:Switch Connection	${ROOT_ALIAS}
	CLI:Write	echo -n > /var/local/EVT/${HOSTNAME}
	${OUTPUT}=	CLI:Write	cat /var/local/EVT/${HOSTNAME}
	Should Not Contain	${OUTPUT}	Event ID