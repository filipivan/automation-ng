*** Settings ***
Resource	../../init.robot
Documentation	Tests Auditing Events section
Metadata	Version 1.0
Metadata	Executed At ${HOST}
Force Tags	CLI
Default Tags	CLI SSH AUDITING	DESTINATION

Suite Setup	Test suite setup
Suite Teardown	Test suite teardown

*** Test Cases ***
#Event Settings Tests
#Interface Tests

## Test TAB-TAB
Test events available commands after send tab-tab
	CLI:Enter Path	/settings/auditing/events
	CLI:Test Available Commands	apply_settings	commit	exit	ls	reboot	set	show_settings	system_certificate	cd	event_system_audit	factory_settings	pwd	revert	shell	shutdown	system_config_check	change_password	event_system_clear	hostname	quit	save_settings	show	software_upgrade	whoami

## Test show
Check events show command fields
	CLI:Enter Path	/settings/auditing/events
	CLI:Test Show Command	email	file	snmp trap	syslog

# file destination tests
Test file events available commands after send tab-tab
	CLI:Enter Path	/settings/auditing/events/file/
	CLI:Test Available Commands	apply_settings	commit	factory_settings	quit	set	shutdown	whoami	cancel	event_system_audit	hostname	reboot	shell	software_upgrade	cd	event_system_clear	ls	revert	show	system_certificate	change_password	exit	pwd	save_settings	show_settings	system_config_check

Test file events show command fields
	CLI:Enter Path	/settings/auditing/events/file/
	CLI:Test Show Command	system_events	aaa_events	device_events	logging_events

Test file events available fields to set command
	CLI:Enter Path	/settings/auditing/events/file
	CLI:Test Set Available Fields	system_events	aaa_events	device_events	logging_events

Test file correct values for field=system_events
	CLI:Enter Path	/settings/auditing/events/file
	CLI:Test Set Field Options	system_events	no	yes

Test file incorrect values for field=system_events
	${FIELD_VALUE}=	Set Variable	system_events
	CLI:Enter Path	/settings/auditing/events/file
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a


Test file correct values for field=aaa_events
	CLI:Enter Path	/settings/auditing/events/file
	CLI:Test Set Field Options	aaa_events	no	yes

Test file incorrect values for field=aaa_events
	${FIELD_VALUE}=	Set Variable	aaa_events
	CLI:Enter Path	/settings/auditing/events/file
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a

Test file correct values for field=device_events
	CLI:Enter Path	/settings/auditing/events/file
	CLI:Test Set Field Options	device_events	no	yes

Test file incorrect values for field=device_events
	${FIELD_VALUE}=	Set Variable	device_events
	CLI:Enter Path	/settings/auditing/events/file
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a

Test file correct values for field=logging_events
	CLI:Enter Path	/settings/auditing/events/file
	CLI:Test Set Field Options	logging_events	no	yes

Test file incorrect values for field=logging_events
	${FIELD_VALUE}=	Set Variable	logging_events
	CLI:Enter Path	/settings/auditing/events/file
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a

# syslog destination tests
Test syslog events available commands after send tab-tab
	CLI:Enter Path	/settings/auditing/events/syslog/
	CLI:Test Available Commands	apply_settings	commit	factory_settings	quit	set	shutdown	whoami	cancel	event_system_audit	hostname	reboot	shell	software_upgrade	cd	event_system_clear	ls	revert	show	system_certificate	change_password	exit	pwd	save_settings	show_settings	system_config_check

Test syslog events show command fields
	CLI:Enter Path	/settings/auditing/events/syslog/
	CLI:Test Show Command	system_events	aaa_events	device_events	logging_events

Test syslog events available fields to set command
	CLI:Enter Path	/settings/auditing/events/syslog
	CLI:Test Set Available Fields	system_events	aaa_events	device_events	logging_events

Test syslog correct values for field=system_events
	CLI:Enter Path	/settings/auditing/events/syslog
	CLI:Test Set Field Options	system_events	no	yes

Test syslog incorrect values for field=system_events
	${FIELD_VALUE}=	Set Variable	system_events
	CLI:Enter Path	/settings/auditing/events/syslog
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a

Test syslog correct values for field=aaa_events
	CLI:Enter Path	/settings/auditing/events/syslog
	CLI:Test Set Field Options	aaa_events	no	yes

Test syslog incorrect values for field=aaa_events
	${FIELD_VALUE}=	Set Variable	aaa_events
	CLI:Enter Path	/settings/auditing/events/syslog
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a

Test syslog correct values for field=device_events
	CLI:Enter Path	/settings/auditing/events/syslog
	CLI:Test Set Field Options	device_events	no	yes

Test syslog incorrect values for field=device_events
	${FIELD_VALUE}=	Set Variable	device_events
	CLI:Enter Path	/settings/auditing/events/syslog
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a

Test syslog correct values for field=logging_events
	CLI:Enter Path	/settings/auditing/events/syslog
	CLI:Test Set Field Options	logging_events	no	yes

Test syslog incorrect values for field=logging_events
	${FIELD_VALUE}=	Set Variable	logging_events
	CLI:Enter Path	/settings/auditing/events/syslog
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a

# snmptrap destination tests
Test snmptrap events available commands after send tab-tab
	CLI:Enter Path	/settings/auditing/events/snmp_trap/
	CLI:Test Available Commands	apply_settings	commit	factory_settings	quit	set	shutdown	whoami	cancel	event_system_audit	hostname	reboot	shell	software_upgrade	cd	event_system_clear	ls	revert	show	system_certificate	change_password	exit	pwd	save_settings	show_settings	system_config_check

Test snmptrap events show command fields
	CLI:Enter Path	/settings/auditing/events/snmp_trap/
	CLI:Test Show Command	system_events	aaa_events	device_events	logging_events

Test snmptrap events available fields to set command
	CLI:Enter Path	/settings/auditing/events/snmp_trap/
	CLI:Test Set Available Fields	system_events	aaa_events	device_events	logging_events

Test snmptrap correct values for field=system_events
	CLI:Enter Path	/settings/auditing/events/snmp_trap/
	CLI:Test Set Field Options	system_events	no	yes

Test snmptrap incorrect values for field=system_events
	${FIELD_VALUE}=	Set Variable	system_events
	CLI:Enter Path	/settings/auditing/events/snmp_trap
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a

Test snmptrap correct values for field=aaa_events
	CLI:Enter Path	/settings/auditing/events/snmp_trap/
	CLI:Test Set Field Options	aaa_events	no	yes

Test snmptrap incorrect values for field=aaa_events
	${FIELD_VALUE}=	Set Variable	aaa_events
	CLI:Enter Path	/settings/auditing/events/snmp_trap
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a

Test snmptrap correct values for field=device_events
	CLI:Enter Path	/settings/auditing/events/snmp_trap/
	CLI:Test Set Field Options	device_events	no	yes

Test snmptrap incorrect values for field=device_events
	${FIELD_VALUE}=	Set Variable	device_events
	CLI:Enter Path	/settings/auditing/events/snmp_trap
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a

Test snmptrap correct values for field=logging_events
	CLI:Enter Path	/settings/auditing/events/snmp_trap
	CLI:Test Set Field Options	logging_events	no	yes

Test snmptrap incorrect values for field=logging_events
	${FIELD_VALUE}=	Set Variable	logging_events
	CLI:Enter Path	/settings/auditing/events/snmp_trap
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a

# email destination tests
Test email events available commands after send tab-tab
	CLI:Enter Path	/settings/auditing/events/email/
	CLI:Test Available Commands	apply_settings	commit	factory_settings	quit	set	shutdown	whoami	cancel	event_system_audit	hostname	reboot	shell	software_upgrade	cd	event_system_clear	ls	revert	show	system_certificate	change_password	exit	pwd	save_settings	show_settings	system_config_check

Test email events show command fields
	CLI:Enter Path	/settings/auditing/events/email/
	CLI:Test Show Command	system_events	aaa_events	device_events	logging_events

Test email events available fields to set command
	CLI:Enter Path	/settings/auditing/events/email/
	CLI:Test Set Available Fields	system_events	aaa_events	device_events	logging_events

Test email event correct values for field=system_events
	CLI:Enter Path	/settings/auditing/events/email/
	CLI:Test Set Field Options	system_events	no	yes

Test email event incorrect values for field=system_events
	${FIELD_VALUE}=	Set Variable	system_events
	CLI:Enter Path	/settings/auditing/events/email
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a

Test email event correct values for field=aaa_events
	CLI:Enter Path	/settings/auditing/events/email/
	CLI:Test Set Field Options	aaa_events	no	yes

Test email event incorrect values for field=aaa_events
	${FIELD_VALUE}=	Set Variable	aaa_events
	CLI:Enter Path	/settings/auditing/events/email
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a

Test email event correct values for field=device_events
	CLI:Enter Path	/settings/auditing/events/email/
	CLI:Test Set Field Options	device_events	no	yes

Test email event incorrect values for field=device_events
	${FIELD_VALUE}=	Set Variable	device_events
	CLI:Enter Path	/settings/auditing/events/email
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a

Test email event correct values for field=logging_events
	CLI:Enter Path	/settings/auditing/events/email/
	CLI:Test Set Field Options	logging_events	no	yes

Test email event incorrect values for field=logging_events
	${FIELD_VALUE}=	Set Variable	logging_events
	CLI:Enter Path	/settings/auditing/events/email
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	${EMPTY}	Error: Missing value: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a for parameter: ${FIELD_VALUE}
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Field Invalid Options	${FIELD_VALUE}	a	Error: Invalid value: a

*** Keywords ***
Test suite setup
	CLI:Open
	${VAR}=	CLI:Get Hostname
	Set Suite Variable	${HOSTNAME}	${VAR}
	Log	\nhostname: ${HOSTNAME}	INFO	console=yes
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Set Field	destination	local
	CLI:Commit
	CLI:Enter Path	/settings/auditing/settings/
	CLI:Set Field	datalog_destination	file
	CLI:Commit
	CLI:Enter Path	/settings/auditing/events/file/
	CLI:SET FIELD	aaa_events	yes
	CLI:SET FIELD	device_events	yes
	CLI:SET FIELD	logging_events	yes
	CLI:SET FIELD	system_events	yes
	CLI:Commit

	Set Default Configuration	loglevel=INFO
	Set Default Configuration	prompt=#
	Set Default Configuration	newline=\n
	Set Default Configuration	width=400
	Set Default Configuration	height=600
	Set Default Configuration	timeout=300

	CLI:Connect As Root

	CLI:Switch Connection	default

Test suite teardown
	CLI:Close Connection