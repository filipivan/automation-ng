*** Settings ***
Resource	../../init.robot
Documentation	Tests devices session preferences validation
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	VALIDATE	SESSION_PREFERENCES
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${INVALID_TWO_SAME_WORDS}	aa
${RIGHT}	ab
${CONTROL_CHARACTER}	^z
@{ALL_VALUES}	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}

*** Test Cases ***
Test with field=disconnect_hotkey
	# Observation
	#Error: disconnect_hotkey: This field must have at least two characters. If it starts with ^ (control) the next must be a valid control character.
	CLI:Enter Path	/settings/devices_session_preferences
	CLI:Test Set Validate Invalid Options	disconnect_hotkey	${EMPTY}
	CLI:Test Set Validate Invalid Options	disconnect_hotkey	${WORD}	Error: disconnect_hotkey: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	disconnect_hotkey	${NUMBER}	Error: disconnect_hotkey: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	disconnect_hotkey	${POINTS}	Error: disconnect_hotkey: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	disconnect_hotkey	${ONE_WORD}	Error: disconnect_hotkey: This field must have at least two characters. If it starts with ^ (control) the next must be a valid control character.
	CLI:Test Set Validate Invalid Options	disconnect_hotkey	${ONE_NUMBER}	Error: disconnect_hotkey: This field must have at least two characters. If it starts with ^ (control) the next must be a valid control character.
	CLI:Test Set Validate Invalid Options	disconnect_hotkey	${ONE_POINTS}	Error: disconnect_hotkey: This field must have at least two characters. If it starts with ^ (control) the next must be a valid control character.
	CLI:Test Set Validate Invalid Options	disconnect_hotkey	${TWO_WORD}
	CLI:Test Set Validate Invalid Options	disconnect_hotkey	${TWO_NUMBER}
	CLI:Test Set Validate Invalid Options	disconnect_hotkey	${TWO_POINTS}
	CLI:Test Set Validate Invalid Options	disconnect_hotkey	${CONTROL_CHARACTER}
	CLI:Test Set Validate Invalid Options	disconnect_hotkey	${EXCEEDED}	Error: disconnect_hotkey: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	disconnect_hotkey	${INVALID_TWO_SAME_WORDS}	Error: disconnect_hotkey: This field must have at least two characters. If it starts with ^ (control) the next must be a valid control character.
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Set Validate Invalid Options	disconnect_hotkey	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument for command: set
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Test Set Validate Invalid Options	disconnect_hotkey	${ONE_WORD} ${ONE_WORD}	Error: Invalid argument: set
	CLI:Test Set Validate Normal Fields	disconnect_hotkey	${RIGHT}
	[Teardown]	CLI:Revert	Raw

Test Valid Values For Field=terminate_session
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/devices_session_preferences
	CLI:Test Set Field Options	terminate_session	no	yes
	[Teardown]	CLI:Revert	Raw

Test Invalid Values For Field=enabled
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/devices_session_preferences
	CLI:Test Set Field Invalid Options	terminate_session	${EMPTY}
	...	Error: Missing value for parameter: terminate_session

	FOR	${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	terminate_session	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: terminate_session
	END
	[Teardown]	CLI:Revert	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection