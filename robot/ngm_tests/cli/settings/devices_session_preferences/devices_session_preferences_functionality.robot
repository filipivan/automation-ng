*** Settings ***
Resource	../../init.robot
Documentation	Tests devices session preferences functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	VALIDATE	SESSION_PREFERENCES
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DISCONNECT_STRING}	^Qe
${DEVICE_NAME}	test-device-peer

*** Test Cases ***
Test Disconnect From Device With terminate_session Field Disabled
	[Setup]	Run Keyword If	'${NGVERSION}' >= '5.0'	SUITE:Disable terminate_session field
	Run Keyword If	'${NGVERSION}' == '3.2'	SUITE:Open	${DEFAULT_USERNAME}:${DEVICE_NAME}	session_alias=device_session	#workaround added because sometimes on 3.2 a fingerprint confirmation is asked
	...	ELSE	CLI:Open	${DEFAULT_USERNAME}:${DEVICE_NAME}	session_alias=device_session
	SUITE:Send Disconnect Command
	SUITE:Session should be still opened
	[Teardown]	SUITE:Teardown Device Session

Test Disconnect From Device With terminate_session Field Enabled
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	[Setup]	SUITE:Enable terminate_session field
	CLI:Open	${DEFAULT_USERNAME}:${DEVICE_NAME}	session_alias=device_session
	SUITE:Send Disconnect Command
	SUITE:Session Should Be Closed
	[Teardown]	SUITE:Teardown Device Session

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Delete Devices	${DEVICE_NAME}
	CLI:Add Device	${DEVICE_NAME}	device_console	${HOSTPEER}
	...	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}	on-demand
	SUITE:Set Disconnect Hotkey	${DISCONNECT_STRING}

SUITE:Teardown
	CLI:Delete Devices	${DEVICE_NAME}
	SUITE:Set Disconnect Hotkey	${EMPTY}
	CLI:Close Connection

SUITE:Open
	[Arguments]	${USERNAME}	${PASSWORD}=${DEFAULT_PASSWORD}	${session_alias}=default
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nOpen Connection ${HOST} - alias ${session_alias}	INFO	console=yes
	Open Connection    ${HOST}    alias=${session_alias}
	Log	Login ${USERNAME} ${PASSWORD}	INFO	console=yes
	${OUTPUT}=	Run Keyword And Ignore Error	Login    ${USERNAME}    ${PASSWORD}    loglevel=INFO
	${CONTAINS}=	Evaluate	"Are you sure you want to continue connecting (yes/no)?" in """${OUTPUT}"""
	Run Keyword If	${CONTAINS}	Write	yes
	...	ELSE	Should Contain	${OUTPUT}	PASS

SUITE:Session should be still opened
	${OUTPUT}=	CLI:Write	info
	Should Match Regexp	${OUTPUT}	(N|n)ame: ${DEVICE_NAME}

SUITE:Session Should Be Closed
	Wait Until Keyword Succeeds	5x	1s	SUITE:Check Session Is Closed

SUITE:Check Session Is Closed
	Run Keyword And Expect Error	OSError: Socket is closed	CLI:Write Bare	\n
	Log To Console	\nConnection is closed\n

SUITE:Enable terminate_session field
	CLI:Enter Path	/settings/devices_session_preferences
	CLI:Set	terminate_session=yes
	CLI:Commit

SUITE:Set Disconnect Hotkey
	[Arguments]	${HOTKEY}
	CLI:Enter Path	/settings/devices_session_preferences
	CLI:Set	disconnect_hotkey=${HOTKEY}
	CLI:Commit

SUITE:Teardown Device Session
	${YET_CONNECTED}=	Run Keyword And Return Status	CLI:Switch Connection	device_session
	Run Keyword If	${YET_CONNECTED} == True	CLI:Close Connection	Yes
	CLI:Switch Connection	default

SUITE:Disable terminate_session field
	CLI:Enter Path	/settings/devices_session_preferences
	CLI:Set	terminate_session=no
	CLI:Commit

SUITE:Send Disconnect Command
	Set Client Configuration	timeout=10s
	Run Keyword And Ignore Error	CLI:Write	\x11e	Yes
	Run Keyword And Ignore Error	CLI:Write	\x11e	Yes