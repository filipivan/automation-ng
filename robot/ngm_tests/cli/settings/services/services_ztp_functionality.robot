*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Services ZTP... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	NON-CRITICAL	EXCLUDEIN3_2
Default Tags	CLI	SSH	SHOW
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test ZTP functionality with python script to show system about
	Wait Until Keyword Succeeds	4m	5s	CLI:Connect As Root
	Wait Until Keyword Succeeds	1m	1s	SUITE:Get ZTP process
	Wait Until Keyword Succeeds	1m	1s	SUITE:Get Result of ZTP	${VERSION}
	Wait Until Keyword Succeeds	1m	5s	SUITE:Check about command	${VERSION}

*** Keywords ***
SUITE:Setup
	CLI:Connect As Root
	CLI:Write	rm -rf /var/ztp/ztp.log
	CLI:Open	timeout=1m

SUITE:Get Result of ZTP
	[Arguments]	${VERSION}
	${OUTPUT}=	CLI:Write	cat /var/ztp/ztp.log	Raw
	Run Keyword If	'${NGVERSION}' > '3.2'	Should Contain	${OUTPUT}	${VERSION}
	Should Contain	${OUTPUT}	Successfully complete Zero Touch Provisioning.

SUITE:Get ZTP process
	${OUTPUT}=	CLI:Write	ps -ef | grep ztp
	Should Contain	${OUTPUT}	/usr/sbin/ztpd

SUITE:Check about command
	[Arguments]	${VERSION}
	${OUTPUTS}=	CLI:Write	ls -l /tmp/aboutcommand_*	user=yes
	@{LAST_OUTPUT}=	Split String	${OUTPUTS}	${SPACE}
	${LAST_OUTPUT}=	Set Variable	@{LAST_OUTPUT}[-1]
	${OUTPUT}=	CLI:Write	cat ${LAST_OUTPUT}
	Should Contain	${OUTPUT}	${VERSION}

SUITE:Teardown
	CLI:Close Connection