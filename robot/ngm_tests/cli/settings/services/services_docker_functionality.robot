*** Settings ***
Resource	../../init.robot
Documentation	Functionality test cases through the CLI and root mode to check the docker support
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	NON-CRITICAL	NEED-REVIEW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${LICENSE}	${LICENSE_VM_AND_DOCKER}
${CONTAINER_NAME}	test_container

*** Test Cases ***
Test Run Docker Command To 'hello-world' Containner
	${LIST}=	Create List
	...	WARNING: IPv4 forwarding is disabled. Networking will not work.
	...	Hello from Docker!
	CLI:Enter Path	/
	Set Default Configuration	timeout=120s
	${OUTPUT}=	CLI:Write	shell sudo docker run hello-world
	Set Default Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Should Contain All	${OUTPUT}	${LIST}
	[Teardown]	SUITE:Remove All Docker Containners

Test Pull Docker Command To Alpine Linux Containner
	${LIST}=	Create List
	...	CONTAINER ID     IMAGE     COMMAND     CREATED     STATUS     PORTS     NAMES
	CLI:Enter Path	/
	${OUTPUT}=	CLI:Write	shell sudo docker pull alpine
	${STATUS}=	Run Keyword And Return Status	should contain	${OUTPUT}
		...	Pull complete	Downloaded newer image for alpine
	Run Keyword If	not '${STATUS}'		Should Contain	${OUTPUT}	Image is up to date for alpine:latest
	${OUTPUT}=	CLI:Write	shell sudo docker ps -a
	CLI:Should Contain All	${OUTPUT}	${LIST}
	[Teardown]	SUITE:Remove All Docker Containners

Test Ps Docker Command To Check All Containners
	${LIST}=	Create List
	...	CONTAINER ID     IMAGE     COMMAND     CREATED     STATUS     PORTS     NAMES
	CLI:Enter Path	/
	${OUTPUT}=	CLI:Write	shell sudo docker ps -a
	CLI:Should Contain All	${OUTPUT}	${LIST}
	[Teardown]	SUITE:Remove All Docker Containners

Test Run Docker Command With "Ls" To Alpine Linux Containner
	${LIST}=	Create List
	...	WARNING: IPv4 forwarding is disabled. Networking will not work.
	...	bin    etc    lib    mnt    proc   run    srv    tmp    var
	...	dev    home   media  opt    root   sbin   sys    usr
	CLI:Enter Path	/
	${OUTPUT}=	CLI:Write	shell sudo docker run alpine ls
	CLI:Should Contain All	${OUTPUT}	${LIST}
	[Teardown]	SUITE:Remove All Docker Containners

Test Ping To External Data Network From Alpine Docker Containner
	[Setup]	SUITE:Enable Ipv4 Forward	yes
	${LIST}=	Create List
	...	5 packets transmitted	5 packets received
	CLI:Enter Path	/
	${OUTPUT}=	CLI:Write	shell sudo docker run alpine ping 8.8.8.8 -W 60 -c 5
	CLI:Should Contain All	${OUTPUT}	${LIST}
	Should Not Contain	${OUTPUT}
	...	WARNING: IPv4 forwarding is disabled. Networking will not work.
	[Teardown]	Run Keywords	SUITE:Remove All Docker Containners	AND	SUITE:Enable Ipv4 Forward	no

Test Run Static Website In A Containner And Check If Without License Will Be Droped After 10 Min
	CLI:Enter Path	/
	Set Client Configuration	timeout=60s
	CLI:Write	shell sudo docker run -d dockersamples/static-site
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Wait Until Keyword Succeeds	60s	5s	SUITE:Should Contain Log Messages	Command: /usr/bin/docker run -d dockersamples/static-site.
	Sleep	2s
	Wait Until Keyword Succeeds	10m	5s	SUITE:Should Contain Log Messages	Licensed stop VM: docker stop
	[Teardown]	SUITE:Remove All Docker Containners

Test Run Static Website In A Containner And Check If Using License Won't Be Droped After 8 Min
	${LIST}=	Create List
	...	CONTAINER ID     IMAGE     COMMAND     CREATED     STATUS     PORTS     NAMES
	...	Up
	[Setup]	Run Keywords	SUITE:Insert Docker License
	CLI:Enter Path	/
	${OUTPUT}=	CLI:Write	shell sudo docker run -d dockersamples/static-site
	Wait Until Keyword Succeeds	30s	5s	SUITE:Should Contain Log Messages	Command: /usr/bin/docker run -d dockersamples/static-site.
	${OUTPUT}=	CLI:Write	shell sudo docker ps -a
	CLI:Should Contain All	${OUTPUT}	${LIST}
	SUITE:Should Not Contain Log Messages	Licensed stop VM: docker stop
	[Teardown]	Run Keywords	SUITE:Remove All Docker Containners

Test Create Docker Container Without Seccomp
	[Documentation]	Create a docker container without Seccomp and check if its PID appears on AppArmor but nor on Seccomp logs
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	NON-CRITICAL
	SUITE:Create Container Without Seccomp	${CONTAINER_NAME}
	[Teardown]	SUITE:Remove Docker Container	${CONTAINER_NAME}

Test Create Docker Container Without AppArmor
	[Documentation]	Create a docker container without AppArmor and check if its PID appears on Seccomp but nor on AppArmor logs
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	NON-CRITICAL
	SUITE:Create Container Without AppArmor	${CONTAINER_NAME}
	[Teardown]	SUITE:Remove Docker Container	${CONTAINER_NAME}

Test Create Docker Container With Seccomp and AppArmor
	[Documentation]	Create a docker container and check if its PID appears on Seccomp and AppArmor logs
	...	After it, disable docker and also removes its license and check if it doesn't appear anymore on Seccomp and AppArmor logs
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	NON-CRITICAL
	${OUTPUT}=	SUITE:Create Container	${CONTAINER_NAME}
	SUITE:Disable Docker
	SUITE:Delete Docker License
	SUITE:Grep Seccomp	${OUTPUT}	False
	SUITE:AppArmor Status	${OUTPUT}	False
	[Teardown]	SUITE:Remove Docker Container	${CONTAINER_NAME}

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Enable Ipv4 Forward	no
	SUITE:Disable Docker
	SUITE:Delete Docker License
	SUITE:Enable Docker
	SUITE:Remove All Docker Containners

SUITE:Teardown
	SUITE:Enable Ipv4 Forward	no
	SUITE:Remove All Docker Containners
	SUITE:Disable Docker
	SUITE:Delete Docker License
	CLI:Close Connection

SUITE:Disable Docker
	CLI:Enter Path	/settings/services/
	CLI:Set	enable_docker=no
	CLI:Commit
	SUITE:Logout And Relogin
	[Teardown]	CLI:Cancel	Raw

SUITE:Logout And Relogin
	CLI:Close Connection
	CLI:Open

SUITE:Delete Docker License
	CLI:Delete All License Keys Installed
	[Teardown]	CLI:Cancel	Raw

SUITE:Enable Docker
	CLI:Enter Path	/settings/services/
	CLI:Set	enable_docker=yes
	CLI:Commit
	SUITE:Logout And Relogin
	[Teardown]	CLI:Cancel	Raw

SUITE:Insert Docker License
	CLI:Add License Key	${LICENSE}
	[Teardown]	CLI:Cancel	Raw

SUITE:Enable Ipv4 Forward
	[Arguments]	${STATUS}
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	enable_ipv4_ip_forward=${STATUS}
	CLI:Commit

SUITE:Remove All Docker Containners
	CLI:Enter Path	/
	${OUTPUT}=	CLI:Write	shell sudo docker ps -a
	${END_NUM}=	Get Line Count	${OUTPUT}
	FOR		${INDEX}	IN RANGE	0	${END_NUM}
		${CONTAINNER_ID}=	SUITE:Get Containner Id
		${EMPTY_LIST}=	Run Keyword And Return Status	Should Be Empty	${CONTAINNER_ID}
		Return From Keyword If	${EMPTY_LIST}
		CLI:Write	shell sudo docker rm ${CONTAINNER_ID}
	END

SUITE:Remove Docker Container
	[Arguments]	${NAME}
	CLI:Write	shell sudo docker stop ${NAME}
	CLI:Write	shell sudo docker rm ${NAME}

SUITE:Get Containner Id
	CLI:Enter Path	/
	${OUTPUT}=	CLI:Write	shell sudo docker ps -a
	@{GET_LINES}=	Split To Lines	${OUTPUT}
	${LINE}=	Get From List	${GET_LINES}	1
	${CONTAINNER_ID}=	Get Substring	${LINE}  0	12
	[Return]	${CONTAINNER_ID}

SUITE:Should Contain Log Messages
	[Arguments]	${MESSAGE}
	CLI:Enter Path	/
	${MESSAGES}=	CLI:Write	shell sudo tail -5 /var/log/messages
	Should Contain	${MESSAGES}	${MESSAGE}

SUITE:Should Not Contain Log Messages
	[Arguments]	${MESSAGE}
	CLI:Enter Path	/
	${MESSAGES}=	CLI:Write	shell sudo tail -5 /var/log/messages
	Should Not Contain	${MESSAGES}	${MESSAGE}

SUITE:Create Container
	[Arguments]	${NAME}
	CLI:Write	shell sudo docker run --name ${NAME} -d httpd
	${OUTPUT}=	SUITE:Get Containner PID	${NAME}
	SUITE:Grep Seccomp	${OUTPUT}
	SUITE:AppArmor Status	${OUTPUT}
	[Return]	${OUTPUT}

SUITE:Create Container Without Seccomp
	[Arguments]	${NAME}
	CLI:Write	shell sudo docker run --name ${NAME} --security-opt seccomp=unconfined -d httpd
	${OUTPUT}=	SUITE:Get Containner PID	${NAME}
	SUITE:Grep Seccomp	${OUTPUT}	False
	SUITE:AppArmor Status	${OUTPUT}

SUITE:Create Container Without AppArmor
	[Arguments]	${NAME}
	CLI:Write	shell sudo docker run --name ${NAME} --security-opt apparmor=unconfined -d httpd
	${OUTPUT}=	SUITE:Get Containner PID	${CONTAINER_NAME}
	SUITE:Grep Seccomp	${OUTPUT}
	SUITE:AppArmor Status	${OUTPUT}	False

SUITE:Grep Seccomp
	[Arguments]	${PID}	${SHOULD_CONTAIN}=True
	${OUTPUT}=	CLI:Write	shell sudo grep Seccomp /proc/*/status | grep -v 0$
	IF	${SHOULD_CONTAIN}
		Should Contain	${OUTPUT}	/proc/${PID}/status:Seccomp:
	ELSE
		Should Not Contain	${OUTPUT}	${PID}
	END

SUITE:AppArmor Status
	[Arguments]	${PID}	${SHOULD_CONTAIN}=True
	${OUTPUT}=	CLI:Write	shell sudo aa-status
	IF	${SHOULD_CONTAIN}
		Should Contain	${OUTPUT}	/usr/local/apache2/bin/httpd (${PID}) docker-default
	ELSE
		Should Not Contain	${OUTPUT}	${PID}
	END

SUITE:Get Containner PID
	[Arguments]	${NAME}
	CLI:Enter Path	/
	${OUTPUT}=	CLI:Write	shell sudo docker inspect -f '{{.State.Pid}}' ${NAME}	user=Yes
	[Return]	${OUTPUT}