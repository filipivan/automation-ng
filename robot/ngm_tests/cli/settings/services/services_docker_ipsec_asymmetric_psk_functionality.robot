*** Settings ***
Resource	../../init.robot
Documentation	Tests through the CLI and root mode, using Docker to support IPsec with asymmetric PSK auth
	...	support for IKEv2 tunnel
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
@{ADMINSIDES}	admin_left	admin_right
${PSK_TEST}	./#$PSKtest
@{STRONGSWAN_FILES}	strongswan_install.sh	strongswan_remove.sh
${SP3}	${SPACE}${SPACE}${SPACE}
${IP_TUN_SUBNET_LEFT}	192.168.105.0
${IP_TUN_SUBNET_RIGHT}	192.168.106.0
${IP_TUN_LEFT}	192.168.105.20
${IP_TUN_RIGHT}	192.168.106.21

${IPSEC_CONF}	SEPARATOR=
...	conn link\n
...	${SP3}auto=route\n
...	${SP3}type=tunnel\n
...	${SP3}fragmentation=yes\n
...	${SP3}keyexchange=ikev2\n
...	${SP3}ike=aes128-sha1-modp1536\n
...	${SP3}esp=aes128-sha1\n
...	${SP3}left=${HOST}\n
...	${SP3}leftid=${HOST}\n
...	${SP3}leftsubnet=${IP_TUN_SUBNET_LEFT}/24\n
...	${SP3}leftauth=psk\n
...	${SP3}right=${HOSTPEER}\n
...	${SP3}rightid=${HOSTPEER}\n
...	${SP3}rightsubnet=${IP_TUN_SUBNET_RIGHT}/24\n
...	${SP3}rightauth=psk
${IPSEC_SECRETS_LEFT}	SEPARATOR=
...	${SP3}${HOST} ${HOSTPEER} : PSK "strongswan123"\n
...	${SP3}${HOSTPEER} : PSK "cisco123"
${IPSEC_SECRETS_RIGHT}	SEPARATOR=
...	${SP3}${HOSTPEER} ${HOST} : PSK "cisco123"\n
...	${SP3}${HOST} : PSK "strongswan123"

*** Test Cases ***
Test Ping Between Right And Left
	SUITE:Install All Strongswan Configurations
	CLI:Switch Connection	admin_left
	${PING_STATUS}=	Run Keyword And Return Status	Run Keyword And Continue On Failure
	...	CLI:Test Ping	${IP_TUN_RIGHT}	NUM_PACKETS=1	TIMEOUT=15
	Run Keyword If	'${PING_STATUS}' != 'False'	Run Keyword And Continue On Failure
	...	Ping should not work, 'cause first packet is to open the tunnel
	FOR		${SIDE}	IN	@{ADMINSIDES}
		CLI:Switch Connection	${SIDE}
		Run Keyword If	'${SIDE}' == 'admin_left'	Run Keyword And Continue On Failure	Wait Until Keyword Succeeds	3x	3s	CLI:Test Ping	${IP_TUN_RIGHT}	NUM_PACKETS=5	TIMEOUT=30
		Run Keyword If	'${SIDE}' == 'admin_right'	Run Keyword And Continue On Failure	Wait Until Keyword Succeeds	3x	3s	CLI:Test Ping	${IP_TUN_LEFT}	NUM_PACKETS=5	TIMEOUT=30
	END
	[Teardown]	SUITE:Delete All Strongswan Configurations

Test Ping Between Right And Left Without Strongswan Configurations
	CLI:Switch Connection	admin_left
	${STATUS}=	Run Keyword And Return Status	Wait Until Keyword Succeeds	3x	3s	CLI:Test Ping	${IP_TUN_RIGHT}	NUM_PACKETS=5	TIMEOUT=30
	Run Keyword If	${STATUS}	Ping Should Not Work
	CLI:Switch Connection	admin_right
	${STATUS}=	Run Keyword And Return Status	Wait Until Keyword Succeeds	3x	3s	CLI:Test Ping	${IP_TUN_LEFT}	NUM_PACKETS=5	TIMEOUT=30
	Run Keyword If	${STATUS}	Ping Should Not Work

*** Keywords ***
SUITE:Setup
	${STATUS1}=	CLI:Check If FTP Server Is Alive And If Exists File	strongswan_install.sh
	Set Suite Variable	${STATUS1}
	${STATUS2}=	CLI:Check If FTP Server Is Alive And If Exists File	strongswan_remove.sh
	Set Suite Variable	${STATUS2}
	Run Keyword If	not ${STATUS1} or not ${STATUS2}	Fatal Error	Files: "strongswan_install.sh" and "strongswan_remove.sh" should exists in FTP server. Please, check the FTP Server ${FTPSERVER2_IP}.
	CLI:Open	session_alias=admin_left	HOST_DIFF=${HOST}
	CLI:Open	session_alias=admin_right	HOST_DIFF=${HOSTPEER}
	SUITE:Disable Docker
	SUITE:Delete Docker License Left
	SUITE:Remove Peer From Coordinator
	SUITE:Disable Cluster Both
	SUITE:Enable Docker
	SUITE:Insert Docker License Left
	SUITE:Change Domain Name
	SUITE:Create Cluster Cordinator
	SUITE:Create Cluster Peer

SUITE:Teardown
	Run Keyword If	not ${STATUS1} or not ${STATUS2}	CLI:Close Connection
	SUITE:Disable Docker
	SUITE:Delete Docker License Left
	SUITE:Remove Peer From Coordinator
	SUITE:Change To Default Domain Name
	SUITE:Disable Cluster Both
	CLI:Close Connection

SUITE:Enable Docker
	FOR		${SIDE}	IN	@{ADMINSIDES}
		CLI:Switch Connection	${SIDE}
		CLI:Enter Path	/settings/services/
		CLI:Set	enable_docker=yes
		CLI:Commit
	END
	SUITE:Logout And Relogin
	[Teardown]	SUITE:Revert Both Sides	/settings/services/

SUITE:Logout And Relogin
	CLI:Close Connection
	CLI:Open	session_alias=admin_left	HOST_DIFF=${HOST}
	CLI:Open	session_alias=admin_right	HOST_DIFF=${HOSTPEER}

SUITE:Revert Both Sides
	[Arguments]	${PATH}
	FOR		${SIDE}	IN	@{ADMINSIDES}
		CLI:Switch Connection	${SIDE}
		CLI:Enter Path	${PATH}
		CLI:Revert	Raw
	END

SUITE:Disable Docker
	Set Client Configuration	timeout=60s
	FOR		${SIDE}	IN	@{ADMINSIDES}
		CLI:Switch Connection	${SIDE}
		CLI:Enter Path	/settings/services/
		CLI:Set	enable_docker=no
		CLI:Commit
	END
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	SUITE:Logout And Relogin
	[Teardown]	SUITE:Revert Both Sides	/settings/services/

SUITE:Insert Docker License Left
	CLI:Switch Connection	admin_left
	${OUTPUT}=	CLI:Add License Key	${LICENSE_VM_AND_DOCKER}
	Should Not Contain	${OUTPUT}	Error
	[Teardown]	CLI:Cancel	Raw

SUITE:Delete Docker License Left
	CLI:Switch Connection	admin_left
	CLI:Delete All License Keys Installed
	[Teardown]	CLI:Cancel	Raw

SUITE:Create Cluster Cordinator
	CLI:Switch Connection	admin_left
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	CLI:Set	enable_cluster=yes type=coordinator enable_peer_management=yes psk=${PSK_TEST}
	CLI:Commit
	[Teardown]	CLI:Cancel	Raw

SUITE:Create Cluster Peer
	CLI:Switch Connection	admin_right
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	CLI:Set	enable_cluster=yes type=peer coordinator_address=${HOST} psk=${PSK_TEST} enable_peer_management=yes
	CLI:Commit
	[Teardown]	CLI:Cancel	Raw

SUITE:Change Domain Name
	CLI:Switch Connection	admin_left
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	domain_name=coordinator
	CLI:Commit
	CLI:Switch Connection	admin_right
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	domain_name=peer
	CLI:Commit

SUITE:Change To Default Domain Name
	CLI:Switch Connection	admin_left
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	domain_name=localdomain
	CLI:Commit
	CLI:Switch Connection	admin_right
	CLI:Enter Path	/settings/network_settings/
	CLI:Set	domain_name=localdomain
	CLI:Commit

SUITE:Remove Peer From Coordinator
	CLI:Switch Connection	admin_left
	CLI:Enter Path	/settings/cluster/cluster_peers/
	CLI:Remove If Exists	nodegrid.peer
	CLI:Remove If Exists	nodegrid.localdomain
	${OUTPUT}=	CLI:Show
	Should Not Contain	${OUTPUT}	nodegrid.peer

SUITE:Disable Cluster Both
	CLI:Switch Connection	admin_right
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	CLI:Set	enable_cluster=no
	CLI:Commit
	CLI:Switch Connection	admin_left
	CLI:Enter Path	/settings/cluster/settings/
	CLI:Edit
	CLI:Set	enable_cluster=no
	CLI:Commit
	[Teardown]	SUITE:Revert Both Sides	/settings/cluster/settings/

SUITE:Cancel Both Sides
	[Arguments]	${PATH}
	FOR		${SIDE}	IN	@{ADMINSIDES}
		CLI:Switch Connection	${SIDE}
		CLI:Enter Path	${PATH}
		CLI:Cancel	Raw
	END

SUITE:Install All Strongswan Configurations
	SUITE:Wget Strongswan Files From FTP Automation Server To Both Sides
	SUITE:Check If There are Arquives On /var/sw To Both Sides
	SUITE:Install Strongswan Both
	SUITE:Check If Strongswan Was Installed Both
	SUITE:Create Files ipsec And Configure subnet Both
	SUITE:Check If Files ipsec And subnet Were Created Both

SUITE:Delete All Strongswan Configurations
	SUITE:Delete Files ipsec.conf And ipsec.secrets On Left and Right
	SUITE:Check If ipsec.conf And ipsec.secrets Were Deleted Both
	SUITE:Remove Strongswan On Left And Right
	SUITE:Check If Strongswan Was Removed Both
	SUITE:Delete Files On /var/sw Both
	SUITE:Check If Strongswan Files Was Deleted Both

SUITE:Wget Strongswan Files From FTP Automation Server To Both Sides
	FOR		${SIDE}	IN	@{ADMINSIDES}
		CLI:Switch Connection	${SIDE}
		Set Client Configuration	prompt=#
		CLI:Write	shell sudo su -
		Set Client Configuration	timeout=120s
		CLI:Write	cd /var/sw/
		${STATUS}=	Run Keyword And Return Status	SUITE:Wget Strongswan Install File
		Run Keyword If	'${STATUS}' == '${FALSE}'	Fail	Should get Strongswan Install File.
		${STATUS}=	Run Keyword And Return Status	SUITE:Wget Strongswan Remove File
		Run Keyword If	'${STATUS}' == '${FALSE}'	Fail	Should get Strongswan Remove File.
		${OUTPUT}=	CLI:Write	ls
		Run Keyword If	'${SIDE}' == 'admin_left'	SUITE:Check Arquives Left Side On /var/sw	${OUTPUT}
		Run Keyword If	'${SIDE}' == 'admin_right'	SUITE:Check Arquives Right Side On /var/sw	${OUTPUT}
		Run Keyword If	'${SIDE}' == 'admin_left'	SUITE:Client Configurations And Cancel
	END
	[Teardown]	SUITE:Client Configurations And Cancel

SUITE:Wget Strongswan Install File
	${OUTPUT}=	CLI:Write	wget --user=${FTPSERVER2_USER} --password=${FTPSERVER2_PASSWORD} ftp://${FTPSERVER2_USER}@${FTPSERVER2_IP}/files/strongswan_install.sh
	${LIST}=	Create List	strongswan_install.sh	100%	saved
	CLI:Should Contain All	${OUTPUT}	${LIST}

SUITE:Wget Strongswan Remove File
	${OUTPUT}=	CLI:Write	wget --user=${FTPSERVER2_USER} --password=${FTPSERVER2_PASSWORD} ftp://${FTPSERVER2_USER}@${FTPSERVER2_IP}/files/strongswan_remove.sh
	${LIST}=	Create List	strongswan_remove.sh	100%	saved
	CLI:Should Contain All	${OUTPUT}	${LIST}

SUITE:Client Configurations And Cancel
	Set Client Configuration	prompt=]#
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Write	exit
	[Teardown]	CLI:Cancel	Raw

SUITE:Check Arquives Left Side On /var/sw
	[Arguments]	${OUTPUT}
	${HAS_FILES_LEFT}=	Run Keyword And Return Status	Should Contain	${OUTPUT}	@{STRONGSWAN_FILES}
	Set Suite Variable	${HAS_FILES_LEFT}

SUITE:Check Arquives Right Side On /var/sw
	[Arguments]	${OUTPUT}
	${HAS_FILES_RIGHT}=	Run Keyword And Return Status	Should Contain	${OUTPUT}	@{STRONGSWAN_FILES}
	Set Suite Variable	${HAS_FILES_RIGHT}

SUITE:Check If There are Arquives On /var/sw To Both Sides
	Run Keyword And Continue On Failure	Should Be True	'${HAS_FILES_LEFT}'
	Run Keyword And Continue On Failure	Should Be True	'${HAS_FILES_RIGHT}'

SUITE:Delete Files On /var/sw Both
	FOR		${SIDE}	IN	@{ADMINSIDES}
		CLI:Switch Connection	${SIDE}
		Set Client Configuration	prompt=#
		CLI:Write	shell sudo su -
		Set Client Configuration	timeout=120s
		CLI:Write	rm /var/sw/strongswan*
		CLI:Write	cd /var/sw/
		${OUTPUT}=	CLI:Write	ls
		Run Keyword If	'${SIDE}' == 'admin_left'	SUITE:Check If Arquives Was Deleted Left	${OUTPUT}
		Run Keyword If	'${SIDE}' == 'admin_right'	SUITE:Check If Arquives Was Deleted Right	${OUTPUT}
		Run Keyword If	'${SIDE}' == 'admin_left'	SUITE:Client Configurations And Cancel
	END
	[Teardown]	SUITE:Client Configurations And Cancel

SUITE:Check If Arquives Was Deleted Left
	[Arguments]	${OUTPUT}
	${NO_INSTALL_FILE_LEFT}=	Run Keyword And Return Status
	...	Should Not Contain	${OUTPUT}	strongswan_install.sh
	${NO_REMOVE_FILE_LEFT}=	Run Keyword And Return Status
	...	Should Not Contain	${OUTPUT}	strongswan_remove.sh
	Set Suite Variable	${NO_INSTALL_FILE_LEFT}
	Set Suite Variable	${NO_REMOVE_FILE_LEFT}

SUITE:Check If Arquives Was Deleted Right
	[Arguments]	${OUTPUT}
	${NO_INSTALL_FILE_RIGHT}=	Run Keyword And Return Status
	...	Should Not Contain	${OUTPUT}	strongswan_install.sh
	${NO_REMOVE_FILE_RIGHT}=	Run Keyword And Return Status
	...	Should Not Contain	${OUTPUT}	strongswan_remove.sh
	Set Suite Variable	${NO_INSTALL_FILE_RIGHT}
	Set Suite Variable	${NO_REMOVE_FILE_RIGHT}

SUITE:Check If Strongswan Files Was Deleted Both
	Run Keyword And Continue On Failure	Should Be True	'${NO_INSTALL_FILE_LEFT}'
	Run Keyword And Continue On Failure	Should Be True	'${NO_INSTALL_FILE_RIGHT}'
	Run Keyword And Continue On Failure	Should Be True	'${NO_REMOVE_FILE_LEFT}'
	Run Keyword And Continue On Failure	Should Be True	'${NO_REMOVE_FILE_RIGHT}'

SUITE:Install Strongswan Both
	FOR		${SIDE}	IN	@{ADMINSIDES}
		CLI:Switch Connection	${SIDE}
		Set Client Configuration	prompt=#
		CLI:Write	shell sudo su -
		Set Client Configuration	timeout=600s
		CLI:Write	/etc/init.d/ipsec stop
		CLI:Write	chmod +x /var/sw/strongswan_install.sh
		Sleep	10s
		CLI:Write	cd /var/sw/
		${OUTPUT}=	CLI:Write	./strongswan_install.sh
		Run Keyword If	'${SIDE}' == 'admin_left'	SUITE:Check If Strongswan Was Installed Left	${OUTPUT}
		Run Keyword If	'${SIDE}' == 'admin_right'	SUITE:Check If Strongswan Was Installed Right	${OUTPUT}
		Run Keyword If	'${SIDE}' == 'admin_left'	SUITE:Client Configurations And Cancel
	END
	[Teardown]	SUITE:Client Configurations And Cancel

SUITE:Check If Strongswan Was Installed Left
	[Arguments]	${OUTPUT}
	${IS_INSTALLED_LEFT}=	Run Keyword And Return Status	Should Contain	${OUTPUT}
	...	Container strongswan created successfully!
	Set Suite Variable	${IS_INSTALLED_LEFT}

SUITE:Check If Strongswan Was Installed Right
	[Arguments]	${OUTPUT}
	${IS_INSTALLED_RIGHT}=	Run Keyword And Return Status	Should Contain	${OUTPUT}
	...	Container strongswan created successfully!
	Set Suite Variable	${IS_INSTALLED_RIGHT}

SUITE:Check If Strongswan Was Installed Both
	Run Keyword And Continue On Failure	Should Be True	'${IS_INSTALLED_LEFT}'
	Run Keyword And Continue On Failure	Should Be True	'${IS_INSTALLED_RIGHT}'

SUITE:Remove Strongswan On Left And Right
	FOR		${SIDE}	IN	@{ADMINSIDES}
		CLI:Switch Connection	${SIDE}
		Set Client Configuration	prompt=#
		CLI:Write	shell sudo su -
		Set Client Configuration	timeout=600s
		CLI:Write	chmod +x /var/sw/strongswan_remove.sh
		CLI:Write	cd /var/sw/
		${OUTPUT}=	CLI:Write	./strongswan_remove.sh
		Run Keyword If	'${SIDE}' == 'admin_left'	SUITE:Check If Strongswan Was Removed Left	${OUTPUT}
		Run Keyword If	'${SIDE}' == 'admin_right'	SUITE:Check If Strongswan Was Removed Right	${OUTPUT}
		Run Keyword If	'${SIDE}' == 'admin_left'	SUITE:Client Configurations And Cancel
	END
	[Teardown]	SUITE:Client Configurations And Cancel

SUITE:Check If Strongswan Was Removed Left
	[Arguments]	${OUTPUT}
	${WAS_REMOVED_LEFT}=	Run Keyword And Return Status	Should Contain	${OUTPUT}
	...	Strongswan files were removed successfully!
	Set Suite Variable	${WAS_REMOVED_LEFT}

SUITE:Check If Strongswan Was Removed Right
	[Arguments]	${OUTPUT}
	${WAS_REMOVED_RIGHT}=	Run Keyword And Return Status	Should Contain	${OUTPUT}
	...	Strongswan files were removed successfully!
	Set Suite Variable	${WAS_REMOVED_RIGHT}

SUITE:Check If Strongswan Was Removed Both
	Run Keyword And Continue On Failure	Should Be True	'${WAS_REMOVED_LEFT}'
	Run Keyword And Continue On Failure	Should Be True	'${WAS_REMOVED_RIGHT}'

SUITE:Create Files ipsec And Configure subnet Both
	FOR		${SIDE}	IN	@{ADMINSIDES}
		CLI:Switch Connection	${SIDE}
		Set Client Configuration	prompt=#
		CLI:Write	shell sudo su -
		Set Client Configuration	timeout=120s
		CLI:Write	echo "${IPSEC_CONF}" > /var/opt/strongswan/etc/ipsec.d/conf/ipsec.conf
		Run Keyword If	'${SIDE}' == 'admin_left'	CLI:Write	echo "${IPSEC_SECRETS_LEFT}" > /var/opt/strongswan/etc/ipsec.d/ipsec.secrets
		Run Keyword If	'${SIDE}' == 'admin_right'	CLI:Write	echo "${IPSEC_SECRETS_RIGHT}" > /var/opt/strongswan/etc/ipsec.d/ipsec.secrets
		Run Keyword If	'${SIDE}' == 'admin_left'	CLI:Write	ifconfig lo:0 ${IP_TUN_LEFT} netmask 255.255.255.0 up
		Run Keyword If	'${SIDE}' == 'admin_right'	CLI:Write	ifconfig lo:0 ${IP_TUN_RIGHT} netmask 255.255.255.0 up
		CLI:Write	docker restart strongswan
		${LOGS_STRONGSWAN}=	CLI:Write	docker logs strongswan
		${CAT_CONF}=	CLI:Write	cat /var/opt/strongswan/etc/ipsec.d/conf/ipsec.conf
		${CAT_SECRETS}=	CLI:Write	cat /var/opt/strongswan/etc/ipsec.d/ipsec.secrets
		${OUTPUT}	CLI:Write	ip a
		Run Keyword If	'${SIDE}' == 'admin_left'
		...	SUITE:Check If Files ipsec And Subnet Were Created Left	${CAT_CONF}	${CAT_SECRETS}	${OUTPUT}	${LOGS_STRONGSWAN}
		Run Keyword If	'${SIDE}' == 'admin_right'
		...	SUITE:Check If Files ipsec And Subnet Were Created Right	${CAT_CONF}	${CAT_SECRETS}	${OUTPUT}	${LOGS_STRONGSWAN}
		Sleep	10s
		Run Keyword If	'${SIDE}' == 'admin_left'	SUITE:Client Configurations And Cancel
	END
	[Teardown]	SUITE:Client Configurations And Cancel

SUITE:Check If Files ipsec And Subnet Were Created Left
	[Arguments]	${CAT_CONF}	${CAT_SECRETS}	${OUTPUT}	${LOGS_STRONGSWAN}
	${CONF_CREATED_LEFT}=	Run Keyword And Return Status	Should Contain	${CAT_CONF}	${IPSEC_CONF}
	Set Suite Variable	${CONF_CREATED_LEFT}
	${IPSEC_SECRETS_LEFT_CAT}=	Remove String Using Regexp	${IPSEC_SECRETS_LEFT}	['"']
	${SECRETS_CREATED_LEFT}=	Run Keyword And Return Status	Should Contain	${CAT_SECRETS}	${IPSEC_SECRETS_LEFT_CAT}
	Set Suite Variable	${SECRETS_CREATED_LEFT}
	${SUBNET_CREATED_LEFT}=	Run Keyword And Return Status	Should Contain	${OUTPUT}	inet ${IP_TUN_LEFT}/24 scope global lo:0
	Set Suite Variable	${SUBNET_CREATED_LEFT}
	${LOGS_STATUS_LEFT}=	Run Keyword And Return Status	Should Not Contain	${LOGS_STRONGSWAN}	Error
	Set Suite Variable	${LOGS_STATUS_LEFT}

SUITE:Check If Files ipsec And Subnet Were Created Right
	[Arguments]	${CAT_CONF}	${CAT_SECRETS}	${OUTPUT}	${LOGS_STRONGSWAN}
	${CONF_CREATED_RIGHT}=	Run Keyword And Return Status	Should Contain	${CAT_CONF}	${IPSEC_CONF}
	Set Suite Variable	${CONF_CREATED_RIGHT}
	${IPSEC_SECRETS_RIGHT_CAT}=	Remove String Using Regexp	${IPSEC_SECRETS_RIGHT}	['"']
	${SECRETS_CREATED_RIGHT}=	Run Keyword And Return Status	Should Contain	${CAT_SECRETS}	${IPSEC_SECRETS_RIGHT_CAT}
	Set Suite Variable	${SECRETS_CREATED_RIGHT}
	${SUBNET_CREATED_RIGHT}=	Run Keyword And Return Status	Should Contain	${OUTPUT}	inet ${IP_TUN_RIGHT}/24 scope global lo:0
	Set Suite Variable	${SUBNET_CREATED_RIGHT}
	${LOGS_STATUS_RIGHT}=	Run Keyword And Return Status	Should Not Contain	${LOGS_STRONGSWAN}	Error
	Set Suite Variable	${LOGS_STATUS_RIGHT}

SUITE:Check If Files ipsec And subnet Were Created Both
	Run Keyword And Continue On Failure	Should Be True	'${CONF_CREATED_LEFT}'
	Run Keyword And Continue On Failure	Should Be True	'${SECRETS_CREATED_LEFT}'
	Run Keyword And Continue On Failure	Should Be True	'${CONF_CREATED_RIGHT}'
	Run Keyword And Continue On Failure	Should Be True	'${SECRETS_CREATED_RIGHT}'
	Run Keyword And Continue On Failure	Should Be True	'${SUBNET_CREATED_LEFT}'
	Run Keyword And Continue On Failure	Should Be True	'${SUBNET_CREATED_RIGHT}'
	Run Keyword And Continue On Failure	Should Be True	'${LOGS_STATUS_LEFT}'
	Run Keyword And Continue On Failure	Should Be True	'${LOGS_STATUS_RIGHT}'

SUITE:Delete Files ipsec.conf And ipsec.secrets On Left and Right
	FOR		${SIDE}	IN	@{ADMINSIDES}
		CLI:Switch Connection	${SIDE}
		Set Client Configuration	prompt=#
		CLI:Write	shell sudo su -
		Set Client Configuration	timeout=120s
		CLI:Write	rm /var/opt/strongswan/etc/ipsec.d/conf/ipsec.conf
		CLI:Write	rm /var/opt/strongswan/etc/ipsec.d/ipsec.secrets
		CLI:Write	cd /var/opt/strongswan/etc/ipsec.d/conf/
		${OUTPUT_CONF}=	CLI:Write	ls
		CLI:Write	cd /var/opt/strongswan/etc/ipsec.d/
		${OUTPUT_SECRETS}=	CLI:Write	ls
		Run Keyword If	'${SIDE}' == 'admin_left'
		...	SUITE:Check If ipsec.conf And ipsec.secrets Were Deleted Left	${OUTPUT_CONF}	${OUTPUT_SECRETS}
		Run Keyword If	'${SIDE}' == 'admin_right'
		...	SUITE:Check If ipsec.conf And ipsec.secrets Were Deleted Right	${OUTPUT_CONF}	${OUTPUT_SECRETS}
		Run Keyword If	'${SIDE}' == 'admin_left'	CLI:Write	ifconfig lo:0 ${IP_TUN_LEFT} netmask 255.255.255.0 Down
		Run Keyword If	'${SIDE}' == 'admin_right'	CLI:Write	ifconfig lo:0 ${IP_TUN_RIGHT} netmask 255.255.255.0 Down
		Run Keyword If	'${SIDE}' == 'admin_left'	SUITE:Client Configurations And Cancel
	END
	[Teardown]	SUITE:Client Configurations And Cancel

SUITE:Check If ipsec.conf And ipsec.secrets Were Deleted Left
	[Arguments]	${OUTPUT_CONF}	${OUTPUT_SECRETS}
	${NO_CONF_FILE_LEFT}=	Run Keyword And Return Status	Should Not Contain	${OUTPUT_CONF}	ipsec.conf
	Set Suite Variable	${NO_CONF_FILE_LEFT}
	${NO_SECRETS_FILE_LEFT}=	Run Keyword And Return Status	Should Not Contain	${OUTPUT_SECRETS}	ipsec.secrets
	Set Suite Variable	${NO_SECRETS_FILE_LEFT}

SUITE:Check If ipsec.conf And ipsec.secrets Were Deleted Right
	[Arguments]	${OUTPUT_CONF}	${OUTPUT_SECRETS}
	${NO_CONF_FILE_RIGHT}=	Run Keyword And Return Status	Should Not Contain	${OUTPUT_CONF}	ipsec.conf
	Set Suite Variable	${NO_CONF_FILE_RIGHT}
	${NO_SECRETS_FILE_RIGHT}=	Run Keyword And Return Status	Should Not Contain	${OUTPUT_SECRETS}	ipsec.secrets
	Set Suite Variable	${NO_SECRETS_FILE_RIGHT}

SUITE:Check If ipsec.conf And ipsec.secrets Were Deleted Both
	Run Keyword And Continue On Failure	Should Be True	'${NO_CONF_FILE_LEFT}'
	Run Keyword And Continue On Failure	Should Be True	'${NO_SECRETS_FILE_LEFT}'
	Run Keyword And Continue On Failure	Should Be True	'${NO_CONF_FILE_RIGHT}'
	Run Keyword And Continue On Failure	Should Be True	'${NO_SECRETS_FILE_RIGHT}'