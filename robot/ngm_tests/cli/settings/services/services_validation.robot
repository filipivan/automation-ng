*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Services Settings... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
@{ALL_VALUES}	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}	${EXCEEDED}

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/services/
	${list}=	Create List	cd	hostname	set	change_password	ls	shell	commit	pwd	show	event_system_audit	quit
	...	event_system_clear	reboot	shutdown	exit	revert	whoami	system_certificate	system_config_check	factory_settings
	CLI:Test Available Commands	@{list}

Test show_settings command
	Set Tags	SHOW_SETTINGS	#Not in 4.2 Official yet
	CLI:Enter Path	/settings/services/
	Write	show_settings
	${OUTPUT}=	CLI:Read Until Prompt
	Should Match Regexp	${OUTPUT}	/settings/services enable_detection_of_usb_devices=yes|no
	Should Match Regexp	${OUTPUT}	/settings/services enable_rpc=no|yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Match Regexp	${OUTPUT}	/settings/services enable_grpc=no|yes
	Should Match Regexp	${OUTPUT}	/settings/services enable_ftp_service=no|yes
	Should Match Regexp	${OUTPUT}	/settings/services enable_snmp_service=yes|no
	Should Match Regexp	${OUTPUT}	/settings/services enable_telnet_service_to_nodegrid=no|yes
	Should Match Regexp	${OUTPUT}	/settings/services enable_telnet_service_to_managed_devices=no|yes
	Should Match Regexp	${OUTPUT}	/settings/services enable_icmp_echo_reply=yes
	Should Match Regexp	${OUTPUT}	/settings/services enable_usb_over_ip=no|yes
	Run Keyword If	'${NGVERSION}' < '4.2'	Should Match Regexp	${OUTPUT}	/settings/services enable_virtualization_services=no|yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Match Regexp	${OUTPUT}	/settings/services enable_docker=no|yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Match Regexp	${OUTPUT}	/settings/services enable_qemu\|kvm=no|yes
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Match Regexp	${OUTPUT}	/settings/services cloud_tcp_port=9966
	...	ELSE	Should Match Regexp	${OUTPUT}	/settings/services cluster_tcp_port=9966
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Match Regexp	${OUTPUT}	/settings/services auto_cloud_enroll=yes|no
	...	ELSE	Should Match Regexp	${OUTPUT}	/settings/services auto_cluster_enroll=yes|no
	Should Match Regexp	${OUTPUT}	/settings/services search_engine_tcp_port=9300
	Should Match Regexp	${OUTPUT}	/settings/services enable_search_engine_high_level_cipher_suite=no|yes
	Should Match Regexp	${OUTPUT}	/settings/services enable_vm_serial_access=yes|no
	Should Match Regexp	${OUTPUT}	/settings/services vm_serial_port=9977
	Should Match Regexp	${OUTPUT}	/settings/services vmotion_timeout=300
	Should Match Regexp	${OUTPUT}	/settings/services enable_zero_touch_provisioning=yes|no
	Should Match Regexp	${OUTPUT}	/settings/services enable_pxe=yes|no
	Should Match Regexp	${OUTPUT}	/settings/services device_access_per_user_group_authorization=no|yes
	Should Match Regexp	${OUTPUT}	/settings/services enable_autodiscovery=yes|no
	Should Match Regexp	${OUTPUT}	/settings/services dhcp_lease_per_autodiscovery_rules=no|yes
	Should Match Regexp	${OUTPUT}	/settings/services block_host_with_multiple_authentication_fails=no|yes
	Should Match Regexp	${OUTPUT}	/settings/services rescue_mode_require_authentication=no|yes
	Should Match Regexp	${OUTPUT}	/settings/services ssh_allow_root_access=yes|no
	Should Match Regexp	${OUTPUT}	/settings/services ssh_tcp_port=22
	Should Match Regexp	${OUTPUT}	/settings/services enable_http_access=yes|no
	Should Match Regexp	${OUTPUT}	/settings/services http_port=80
	Should Match Regexp	${OUTPUT}	/settings/services enable_https_access=yes|no
	Should Match Regexp	${OUTPUT}	/settings/services https_port=443
	Should Match Regexp	${OUTPUT}	/settings/services redirect_http_to_https=yes|no
	Should Match Regexp	${OUTPUT}	/settings/services tlsv1.2=yes|no
	Should Match Regexp	${OUTPUT}	/settings/services tlsv1.1=yes|no
	Should Match Regexp	${OUTPUT}	/settings/services tlsv1=no|yes
	Should Match Regexp	${OUTPUT}	/settings/services cipher_suite_level=medium

Test available fields to set
	CLI:Enter Path	/settings/services
	${AUTODISCOVERY_ENABLED}=	Run Keyword And Return Status	CLI:Test Show Command	enable_autodiscovery = yes
	Run Keyword If	${AUTODISCOVERY_ENABLED} == False	Run Keywords	CLI:Set Field	enable_autodiscovery	yes	AND	CLI:Commit

	CLI:Test Set Available Fields	enable_detection_of_usb_devices	enable_rpc	enable_ftp_service
	...	enable_snmp_service	enable_telnet_service_to_nodegrid	enable_telnet_service_to_managed_devices
	...	enable_icmp_echo_reply	enable_usb_over_ip
	...	search_engine_tcp_port	enable_search_engine_high_level_cipher_suite
	...	enable_vm_serial_access	vm_serial_port	vmotion_timeout	enable_zero_touch_provisioning
	...	enable_pxe	device_access_per_user_group_authorization	enable_autodiscovery
	...	dhcp_lease_per_autodiscovery_rules	block_host_with_multiple_authentication_fails
	...	rescue_mode_require_authentication	ssh_allow_root_access	ssh_tcp_port
	...	ssh_ciphers	ssh_macs	ssh_kexalgorithms	enable_http_access	http_port
	...	enable_https_access	https_port	redirect_http_to_https	tlsv1.2
	...	tlsv1.1	tlsv1	cipher_suite_level
	Run Keyword If	'${NGVERSION}' >= '4.2'	Run Keywords	CLI:Set Field	enable_grpc	yes	AND	CLI:Commit
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Available Fields	enable_grpc	grpc_port
	Run Keyword If	'${NGVERSION}' < '4.2'	CLI:Test Set Available Fields	cloud_tcp_port	auto_cloud_enroll
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Available Fields	cluster_tcp_port	auto_cluster_enroll	enable_docker	enable_qemu|kvm
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Set Available Fields	enable_bluetooth

Test validation for field=cipher_suite_level
	CLI:Enter Path	/settings/services/
	CLI:Test Set Field Options	cipher_suite_level	custom	high	low	medium
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	cipher_suite_level	${EMPTY}	Error: Missing value for parameter: cipher_suite_level
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	cipher_suite_level	a	Error: Invalid value: a for parameter: cipher_suite_level
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	cipher_suite_level	${EMPTY}	Error: Missing value: cipher_suite_level
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	cipher_suite_level	a	Error: Invalid value: a

Test invalid values for fields in services
	CLI:Enter Path	/settings/services/
	${COMMANDS}=	Create List
	Append To List	${COMMANDS}	enable_detection_of_usb_devices	enable_rpc	enable_ftp_service
	...	enable_snmp_service	enable_telnet_service_to_nodegrid	enable_telnet_service_to_managed_devices
	...	enable_icmp_echo_reply	enable_usb_over_ip	enable_search_engine_high_level_cipher_suite
	...	enable_vm_serial_access	enable_zero_touch_provisioning
	...	enable_pxe	device_access_per_user_group_authorization	enable_autodiscovery
	...	dhcp_lease_per_autodiscovery_rules	block_host_with_multiple_authentication_fails
	...	rescue_mode_require_authentication	ssh_allow_root_access	enable_http_access
	...	enable_https_access	redirect_http_to_https	tlsv1.2
	...	tlsv1.1	tlsv1
	Run Keyword If	'${NGVERSION}' >= '4.2'	Append To List	${COMMANDS}	enable_grpc
	Run Keyword If	'${NGVERSION}' < '4.2'	Append To List	${COMMANDS}	auto_cloud_enroll
	Run Keyword If	'${NGVERSION}' >= '4.2'	Append To List	${COMMANDS}	auto_cluster_enroll	enable_qemu|kvm	enable_docker
	Run Keyword If	'${NGVERSION}' >= '5.0'	Append To List	${COMMANDS}	enable_bluetooth

	SUITE:Set Invalid Values Error Messages	${COMMANDS}

Test invalid values for field=grpc_port
	Skip If	'${NGVERSION}' == '3.2'	Feature not in 3.2
	CLI:Enter Path	/settings/services/
	CLI:Set Field	enable_grpc	yes
	CLI:Test Set Field Invalid Options	grpc_port	${EMPTY}	Error: grpc_port: Validation error.
	CLI:Test Set Field Invalid Options	grpc_port	a	Error: grpc_port: Validation error.
	CLI:Test Set Field Invalid Options	grpc_port	~!@	Error: grpc_port: Validation error.
	CLI:Test Set Field Invalid Options	grpc_port	${EXCEEDED}	Error: grpc_port: Validation error.
	CLI:Revert

Test invalid values for field=vm_serial_port
	CLI:Enter Path	/settings/services/
	CLI:Test Set Field Invalid Options	vm_serial_port	${EMPTY}	Error: vm_serial_port: Validation error.
	CLI:Test Set Field Invalid Options	vm_serial_port	a	Error: vm_serial_port: Validation error.
	CLI:Test Set Field Invalid Options	vm_serial_port	~!@	Error: vm_serial_port: Validation error.
	CLI:Test Set Field Invalid Options	vm_serial_port	${EXCEEDED}	Error: vm_serial_port: Validation error.

Test invalid values for field=vmotion_timeout
	CLI:Enter Path	/settings/services/
	CLI:Test Set Field Invalid Options	vmotion_timeout	${EMPTY}	Error: vmotion_timeout: Value must be between 0 and 3600 seconds.
	CLI:Test Set Field Invalid Options	vmotion_timeout	a	Error: vmotion_timeout: Value must be between 0 and 3600 seconds.
	CLI:Test Set Field Invalid Options	vmotion_timeout	~!@	Error: vmotion_timeout: Value must be between 0 and 3600 seconds.
	CLI:Test Set Field Invalid Options	vmotion_timeout	3601	Error: vmotion_timeout: Value must be between 0 and 3600 seconds.
	CLI:Test Set Field Invalid Options	vmotion_timeout	${EXCEEDED}	Error: vmotion_timeout: Exceeded the maximum size for this field.

Test invalid values for field=ssh_tcp_port
	CLI:Enter Path	/settings/services/
	CLI:Test Set Field Invalid Options	ssh_tcp_port	${EMPTY}	Error: ssh_tcp_port: Validation error.
	CLI:Test Set Field Invalid Options	ssh_tcp_port	a	Error: ssh_tcp_port: Validation error.
	CLI:Test Set Field Invalid Options	ssh_tcp_port	~!@	Error: ssh_tcp_port: Validation error.
	CLI:Test Set Field Invalid Options	ssh_tcp_port	${EXCEEDED}	Error: ssh_tcp_port: Validation error.

Test invalid values for field=ssh_ciphers/ssh_macs/ssh_kexalgorithms
	CLI:Enter Path	/settings/services/
	CLI:Test Set Field Invalid Options	ssh_ciphers	a	Error: ssh_ciphers: Invalid Ciphers: a
	CLI:Test Set Field Invalid Options	ssh_macs	a	Error: ssh_macs: Invalid MACs: a
	CLI:Test Set Field Invalid Options	ssh_kexalgorithms	a	Error: ssh_kexalgorithms: Invalid KexAlgorithms: a

Test valid values for field=ssh_ciphers
	CLI:Enter Path	/settings/services/
	${OUTPUT}=	CLI:Set Field	ssh_ciphers	aes128-ctr
	Should Not Contain	${OUTPUT}	Error
	CLI:Test Show Command	ssh_ciphers = aes128-ctr
	CLI:Revert

Test valid values for field=ssh_macs
	CLI:Enter Path	/settings/services/
	${OUTPUT}=	CLI:Set Field	ssh_macs	hmac-sha1
	Should Not Contain	${OUTPUT}	Error
	CLI:Test Show Command	ssh_macs = hmac-sha1
	CLI:Revert

Test valid values for field=ssh_kexalgorithms
	CLI:Enter Path	/settings/services/
	${OUTPUT}=	CLI:Set Field	ssh_kexalgorithms	diffie-hellman-group1-sha1
	Should Not Contain	${OUTPUT}	Error
	CLI:Test Show Command	ssh_kexalgorithms = diffie-hellman-group1-sha1
	CLI:Revert

Test invalid values for field=http_port
	CLI:Enter Path	/settings/services/
	CLI:Test Set Field Invalid Options	http_port	${EMPTY}	Error: http_port: Validation error.
	CLI:Test Set Field Invalid Options	http_port	a	Error: http_port: Validation error.
	CLI:Test Set Field Invalid Options	http_port	~!@	Error: http_port: Validation error.
	CLI:Test Set Field Invalid Options	http_port	${EXCEEDED}	Error: http_port: Validation error.

Test invalid values for field=https_port
	CLI:Enter Path	/settings/services/
	CLI:Test Set Field Invalid Options	https_port	${EMPTY}	Error: https_port: Validation error.
	CLI:Test Set Field Invalid Options	https_port	a	Error: https_port: Validation error.
	CLI:Test Set Field Invalid Options	https_port	~!@	Error: https_port: Validation error.
	CLI:Test Set Field Invalid Options	https_port	${EXCEEDED}	Error: https_port: Validation error.

Test invalid values for field=cloud_tcp_port
	CLI:Enter Path	/settings/services/
	Run Keyword If	'${NGVERSION}'== '3.2'	CLI:Test Set Field Invalid Options	cloud_tcp_port	${EMPTY}	Error: cloud_tcp_port: Validation error.
	...	ELSE	CLI:Test Set Field Invalid Options	cluster_tcp_port	${EMPTY}	Error: cluster_tcp_port: Validation error.
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	cloud_tcp_port	a	Error: cloud_tcp_port: Validation error.
	...	ELSE	CLI:Test Set Field Invalid Options	cluster_tcp_port	a	Error: cluster_tcp_port: Validation error.
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	cloud_tcp_port	~!@	Error: cloud_tcp_port: Validation error.
	...	ELSE	CLI:Test Set Field Invalid Options	cluster_tcp_port	~!@	Error: cluster_tcp_port: Validation error.
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	cloud_tcp_port	${EXCEEDED}	Error: cloud_tcp_port: Exceeded the maximum size for this field.
	...	ELSE	CLI:Test Set Field Invalid Options	cluster_tcp_port	${EXCEEDED}	Error: cluster_tcp_port: Exceeded the maximum size for this field.

Test invalid values for field=period_host_will_stay_blocked
	CLI:Enter Path	/settings/services/
	CLI:Set Field	block_host_with_multiple_authentication_fails	yes
	CLI:Test Set Field Invalid Options	period_host_will_stay_blocked	${EMPTY}	Error: period_host_will_stay_blocked: Validation error.
	CLI:Test Set Field Invalid Options	period_host_will_stay_blocked	a	Error: period_host_will_stay_blocked: Validation error.
	CLI:Test Set Field Invalid Options	period_host_will_stay_blocked	~!@	Error: period_host_will_stay_blocked: Validation error.
	CLI:Test Set Field Invalid Options	period_host_will_stay_blocked	${EXCEEDED}	Error: period_host_will_stay_blocked: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test invalid values for field=timeframe_to_monitor_authentication_fails
	CLI:Enter Path	/settings/services/
	CLI:Set Field	block_host_with_multiple_authentication_fails	yes
	CLI:Test Set Field Invalid Options	timeframe_to_monitor_authentication_fails	${EMPTY}	Error: timeframe_to_monitor_authentication_fails: Validation error.
	CLI:Test Set Field Invalid Options	timeframe_to_monitor_authentication_fails	a	Error: timeframe_to_monitor_authentication_fails: Validation error.
	CLI:Test Set Field Invalid Options	timeframe_to_monitor_authentication_fails	~!@	Error: timeframe_to_monitor_authentication_fails: Validation error.
	CLI:Test Set Field Invalid Options	timeframe_to_monitor_authentication_fails	${EXCEEDED}	Error: timeframe_to_monitor_authentication_fails: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test invalid values for field=number_of_authentication_fails_to_block_host
	CLI:Enter Path	/settings/services/
	CLI:Set Field	block_host_with_multiple_authentication_fails	yes
	CLI:Test Set Field Invalid Options	number_of_authentication_fails_to_block_host	${EMPTY}	Error: number_of_authentication_fails_to_block_host: Validation error.
	CLI:Test Set Field Invalid Options	number_of_authentication_fails_to_block_host	a	Error: number_of_authentication_fails_to_block_host: Validation error.
	CLI:Test Set Field Invalid Options	number_of_authentication_fails_to_block_host	~!@	Error: number_of_authentication_fails_to_block_host: Validation error.
	CLI:Test Set Field Invalid Options	number_of_authentication_fails_to_block_host	${EXCEEDED}	Error: number_of_authentication_fails_to_block_host: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

Test Valid Values For Field=enable_bluetooth
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/services/
	CLI:Test Set Field Options	enable_bluetooth	no	yes
	[Teardown]	CLI:Revert	Raw

Test Invalid Values For Field=enable_bluetooth
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/services/
	CLI:Test Set Field Invalid Options	enable_bluetooth	${EMPTY}
	...	Error: Missing value for parameter: enable_bluetooth

	FOR	${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	enable_bluetooth	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: enable_bluetooth
	END
	[Teardown]	CLI:Revert	Raw

Test Available Fields In Services After Enabling Bluetooth
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/qos/classes
	CLI:Test Set Available Fields	bluetooth_discoverable_mode	bluetooth_display_name
	[Teardown]	CLI:Revert	Raw


Test Valid Values For Field=bluetooth_discoverable_mode
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/services
	CLI:Test Set Validate Normal Fields	enable_bluetooth	yes
	CLI:Test Set Field Options	bluetooth_discoverable_mode	no	yes
	[Teardown]	CLI:Revert	Raw

Test Invalid Values For Field=bluetooth_discoverable_mode
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Enter Path	/settings/services/
	CLI:Test Set Validate Normal Fields	enable_bluetooth	yes
	CLI:Test Set Field Invalid Options	bluetooth_discoverable_mode	${EMPTY}
	...	Error: Missing value for parameter: bluetooth_discoverable_mode

	FOR	${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	bluetooth_discoverable_mode	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: bluetooth_discoverable_mode
	END
	[Teardown]	CLI:Revert	Raw

Test Valid Values For Field=bluetooth_display_name
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	FOR	${VALID_NAME}	IN	@{ALL_VALUES}
		CLI:Test Set Validate Normal Fields	bluetooth_display_name	${VALID_NAME}
	END
	[Teardown]	CLI:Revert	Raw

Test Valid Values For Field=enable_elasticsearch/enable_search_engine
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	CLI:Enter Path	/settings/services
	Run Keyword If	${NGVERSION} <= 5.6	CLI:Test Set Field Options	enable_elasticsearch	yes	no
	Run Keyword If	${NGVERSION} > 5.8	CLI:Test Set Field Options	enable_search_engine	yes	no
	[Teardown]	CLI:Revert	Raw

Test Invalid Values For Field=enable_elasticsearch/enable_search_engine
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	CLI:Enter Path	/settings/services
	IF	${NGVERSION} <= 5.6
		CLI:Test Set Field Invalid Options	enable_elasticsearch	${EMPTY}
		...	Error: Missing value for parameter: enable_elasticsearch
		FOR	${INVALID_VALUE}	IN	@{ALL_VALUES}
			CLI:Test Set Field Invalid Options	enable_elasticsearch	${INVALID_VALUE}
			...	Error: Invalid value: ${INVALID_VALUE} for parameter: enable_elasticsearch
		END
	ELSE
		CLI:Test Set Field Invalid Options	enable_search_engine	${EMPTY}
		...	Error: Missing value for parameter: enable_search_engine
		FOR	${INVALID_VALUE}	IN	@{ALL_VALUES}
			CLI:Test Set Field Invalid Options	enable_search_engine	${INVALID_VALUE}
			...	Error: Invalid value: ${INVALID_VALUE} for parameter: enable_search_engine
		END
	END
	[Teardown]	CLI:Revert	Raw

Test Valid Values For Field=enable_kibana/enable_dashboards
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	CLI:Enter Path	/settings/services
	IF	${NGVERSION} <= 5.6
		CLI:Set	enable_elasticsearch=yes
		CLI:Test Set Field Options	enable_kibana	yes	no
	ELSE
		CLI:Set	enable_search_engine=yes
		CLI:Test Set Field Options	enable_dashboards	yes	no
	END
	[Teardown]	CLI:Revert	Raw

Test Invalid Values For Field=enable_kibana/enable_dashboards
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	CLI:Enter Path	/settings/services
	IF	${NGVERSION} <= 5.6
		CLI:Set	enable_elasticsearch=yes
		CLI:Test Set Field Invalid Options	enable_kibana	${EMPTY}
		...	Error: Missing value for parameter: enable_kibana
		FOR	${INVALID_VALUE}	IN	@{ALL_VALUES}
			CLI:Test Set Field Invalid Options	enable_kibana	${INVALID_VALUE}
			...	Error: Invalid value: ${INVALID_VALUE} for parameter: enable_kibana
		END
	ELSE
		CLI:Set	enable_search_engine=yes
		CLI:Test Set Field Invalid Options	enable_dashboards	${EMPTY}
		...	Error: Missing value for parameter: enable_dashboards
		FOR	${INVALID_VALUE}	IN	@{ALL_VALUES}
			CLI:Test Set Field Invalid Options	enable_dashboards	${INVALID_VALUE}
			...	Error: Invalid value: ${INVALID_VALUE} for parameter: enable_dashboards
		END
	END
	[Teardown]	CLI:Revert	Raw

Test Valid Values For Field=enable_vmware_manager
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	CLI:Enter Path	/settings/services
	CLI:Test Set Field Options	enable_vmware_manager	yes	no
	[Teardown]	CLI:Revert	Raw

Test Invalid Values For Field=enable_vmware_manager
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	CLI:Enter Path	/settings/services
	CLI:Test Set Field Invalid Options	enable_vmware_manager	${EMPTY}
	...	Error: Missing value for parameter: enable_vmware_manager
	FOR	${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	enable_vmware_manager	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: enable_vmware_manager
	END
	[Teardown]	CLI:Revert	Raw

Test Valid Values For Field=enable_telegraf
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	CLI:Enter Path	/settings/services
	CLI:Test Set Field Options	enable_telegraf	yes	no
	[Teardown]	CLI:Revert	Raw

Test Invalid Values For Field=enable_telegraf
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	CLI:Enter Path	/settings/services
	CLI:Test Set Field Invalid Options	enable_telegraf	${EMPTY}
	...	Error: Missing value for parameter: enable_telegraf
	FOR	${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	enable_telegraf	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: enable_telegraf
	END
	[Teardown]	CLI:Revert	Raw

Test if kibana can be enabled if elasticsearch is disabled
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	CLI:Enter Path	/settings/services
	${ERROR}	CLI:Write	set enable_elasticsearch=no enable_kibana=yes	Raw
	Should Contain	${ERROR}	Error: Invalid parameter name: enable_kibana
	[Teardown]	CLI:Revert	Raw
*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection

SUITE:Set Invalid Values Error Messages
	[Arguments]	${COMMANDS}
	FOR	${COMMAND}	IN	@{COMMANDS}
		Run Keyword If	'${NGVERSION}' <= '5.6' or '${COMMAND}' != 'enable_search_engine_high_level_cipher_suite'	CLI:Test Set Field Options	${COMMAND}	no	yes
		...	ELSE	Run Keywords	CLI:Set	enable_search_engine_high_level_cipher_suite=yes	AND	CLI:Commit	AND	CLI:Set	enable_search_engine_high_level_cipher_suite=no	AND	CLI:Commit
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	a	Error: Invalid value: a for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value: ${COMMAND}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${COMMAND}	a	Error: Invalid value: a
	END