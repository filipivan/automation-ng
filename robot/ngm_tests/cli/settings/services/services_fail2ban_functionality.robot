*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Services, functionality test cases to block host with multiple authentication
...	fails with ignorip option through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	NON-CRITICAL

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${WRONG_PASSWORD}	WRONG_PASSWORD
${INTERFACE}	eth0

*** Test Cases ***
Test Fail2ban Without Whitelisted Ipv4 Addresses
	[Setup]	SUITE:Enable Fail2ban	PERIOD=1	NUMBER_FAILS=3	WHITELISTED_IP=${EMPTY}	TIMEFRAME=1
	Skip If	not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	SUITE:Switch Connection To Root	HOST
	SUITE:Switch Connection To Root	PEER
	SUITE:Force Ban For Authentication Failure Multiple Times
	SUITE:Try To Login	BLOCKED=Yes
	Sleep	60s
	SUITE:Try To Login	BLOCKED=No
	[Teardown]	Run Keywords	Skip If	not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	...	AND	SUITE:Set Client Configurations And Exit On Buth Sides	AND	SUITE:Disable Fail2ban

Test Fail2ban With Whitelisted Ipv4 Addresses
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	[Setup]	SUITE:Enable Fail2ban	PERIOD=1	NUMBER_FAILS=3	WHITELISTED_IP=${HOST_DIFF}	TIMEFRAME=1
	Skip If	not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	SUITE:Switch Connection To Root	HOST
	SUITE:Switch Connection To Root	PEER
	SUITE:Force Ban For Authentication Failure Multiple Times	WHITELISTED_IP=Yes
	SUITE:Try To Login	BLOCKED=No
	[Teardown]	Run Keywords	Skip If	not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	...	AND	SUITE:Set Client Configurations And Exit On Buth Sides	AND	SUITE:Disable Fail2ban

Test Fail2ban Without Whitelisted Ipv6 Addresses
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	WHITHOUT_DHCPV6
	Skip If	not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	CLI:Switch Connection	HOST
	${HOST_IP6}=	CLI:Get Interface Ipv6 Address	${INTERFACE}
	CLI:Switch Connection	PEER
	${HOSTSHARED_IP6}=	CLI:Get Interface Ipv6 Address	${INTERFACE}
	SUITE:Enable Fail2ban	PERIOD=1	NUMBER_FAILS=3	WHITELISTED_IP=${EMPTY}	TIMEFRAME=1
	SUITE:Switch Connection To Root	HOST
	SUITE:Switch Connection To Root	PEER
	SUITE:Force Ban For Authentication Failure Multiple Times	WHITELISTED_IP=No	IP_HOST=${HOST_IP6}
	...	IP_HOSTSHARED=${HOSTSHARED_IP6}	IPV6=Yes	INTERFACE=${INTERFACE}
	SUITE:Try To Login	BLOCKED=Yes	WHITELISTED_IP=No	IP_HOST=${HOST_IP6}	IPV6=Yes	INTERFACE=${INTERFACE}
	...	IP_HOSTSHARED=${HOSTSHARED_IP6}
	Sleep	60s
	SUITE:Try To Login	BLOCKED=No	WHITELISTED_IP=No	IP_HOST=${HOST_IP6}	IPV6=Yes	INTERFACE=${INTERFACE}
	...	IP_HOSTSHARED=${HOSTSHARED_IP6}
	[Teardown]	Run Keywords	Skip If	not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	...	AND	SUITE:Set Client Configurations And Exit On Buth Sides	AND	SUITE:Disable Fail2ban

Test Fail2ban With Whitelisted Ipv6 Addresses
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	Skip If	not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	CLI:Switch Connection	HOST
	${HOST_IP6}=	CLI:Get Interface Ipv6 Address	${INTERFACE}
	CLI:Switch Connection	PEER
	${HOSTSHARED_IP6}=	CLI:Get Interface Ipv6 Address	${INTERFACE}
	SUITE:Enable Fail2ban	PERIOD=1	NUMBER_FAILS=3	WHITELISTED_IP=${HOSTSHARED_IP6}	TIMEFRAME=1
	SUITE:Switch Connection To Root	HOST
	SUITE:Switch Connection To Root	PEER
	SUITE:Force Ban For Authentication Failure Multiple Times	WHITELISTED_IP=Yes	IP_HOST=${HOST_IP6}
	...	IP_HOSTSHARED=${HOSTSHARED_IP6}	IPV6=Yes	INTERFACE=${INTERFACE}
	SUITE:Try To Login	BLOCKED=No	WHITELISTED_IP=Yes	IP_HOST=${HOST_IP6}	IPV6=Yes	INTERFACE=${INTERFACE}
	...	IP_HOSTSHARED=${HOSTSHARED_IP6}
	[Teardown]	Run Keywords	Skip If	not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	...	AND	SUITE:Set Client Configurations And Exit On Buth Sides	AND	SUITE:Disable Fail2ban

*** Keywords ***
SUITE:Setup
	Skip If	not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	CLI:Open	session_alias=HOST
	CLI:Edit To Default Interface Configuration	ETH0
	${HOST_DIFF}=	Run Keyword If	'${NGVERSION}' <= '4.2'	Set Variable	${HOSTPEER}
	...	ELSE	Set Variable	${HOSTSHARED}
	Log To Console	\n${HOST_DIFF}\n
	Set Suite Variable	${HOST_DIFF}
	CLI:Open	session_alias=PEER	HOST_DIFF=${HOST_DIFF}
	CLI:Edit To Default Interface Configuration	ETH0
	SUITE:Disable Fail2ban

SUITE:Teardown
	Skip If	not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	SUITE:Disable Fail2ban
	CLI:Close Connection

SUITE:Enable Fail2ban
	[Arguments]		${PERIOD}=10	${NUMBER_FAILS}=5	${WHITELISTED_IP}=${EMPTY}	${TIMEFRAME}=10
	Skip If	not ${HAS_HOSTSHARED}	HOSTSHARED is not configured/available for this testing build
	CLI:Switch Connection	HOST
	CLI:Enter Path	/settings/services/
	CLI:Set	block_host_with_multiple_authentication_fails=yes
	CLI:Set	period_host_will_stay_blocked=${PERIOD} number_of_authentication_fails_to_block_host=${NUMBER_FAILS}
	CLI:Set	timeframe_to_monitor_authentication_fails=${TIMEFRAME}
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Set	whitelisted_ip_addresses=${WHITELISTED_IP}
	CLI:Commit
	${OUTPUT}=	CLI:Test Show Command	block_host_with_multiple_authentication_fails = yes	period_host_will_stay_blocked = ${PERIOD}
	...	number_of_authentication_fails_to_block_host = ${NUMBER_FAILS}	timeframe_to_monitor_authentication_fails = ${TIMEFRAME}
	Log To Console	\n${OUTPUT}\n
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Show Command	whitelisted_ip_addresses = ${WHITELISTED_IP}

SUITE:Disable Fail2ban
	[Arguments]	${PERIOD}=10	${NUMBER_FAILS}=5	${WHITELISTED_IP}=${EMPTY}	${TIMEFRAME}=10
	CLI:Switch Connection	HOST
	CLI:Enter Path	/settings/services/
	CLI:Set	block_host_with_multiple_authentication_fails=yes
	CLI:Set	period_host_will_stay_blocked=${PERIOD} number_of_authentication_fails_to_block_host=${NUMBER_FAILS}
	CLI:Set	timeframe_to_monitor_authentication_fails=${TIMEFRAME}
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Set	whitelisted_ip_addresses=${WHITELISTED_IP}
	CLI:Commit
	CLI:Set	block_host_with_multiple_authentication_fails=no
	CLI:Commit
	CLI:Test Show Command	block_host_with_multiple_authentication_fails = no
	${OUTPUT}=	CLI:Test Not Show Command	block_host_with_multiple_authentication_fails = yes	period_host_will_stay_blocked = ${PERIOD}
	...	number_of_authentication_fails_to_block_host = ${NUMBER_FAILS}	timeframe_to_monitor_authentication_fails = ${TIMEFRAME}
	Log To Console	\n${OUTPUT}\n
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Not Show Command	whitelisted_ip_addresses = ${WHITELISTED_IP}

SUITE:Switch Connection To Root
	[Arguments]	${SESSION_ALIAS}
	CLI:Switch Connection	${SESSION_ALIAS}
	Set Client Configuration	prompt=#
	CLI:Write	shell sudo su -

SUITE:Force Ban For Authentication Failure Multiple Times
	[Arguments]	${WHITELISTED_IP}=No	${IP_HOST}=${HOST}	${IP_HOSTSHARED}=${HOST_DIFF}	${IPV6}=No	${INTERFACE}=${INTERFACE}
	...	${BLOCKED}=No

	Set Client Configuration	prompt=~#
	Run Keyword If	"${IPV6}" == "No"	CLI:Write	ssh-keygen -f "/home/root/.ssh/known_hosts" -R "${IP_HOST}"
	Run Keyword If	"${IPV6}" == "Yes"	CLI:Write	ssh-keygen -f "/home/root/.ssh/known_hosts" -R "${IP_HOST}%${INTERFACE}"

	${OUTPUT}=	Wait Until Keyword Succeeds	60s	3s
	...	SUITE:Open SSH Connection From Peer	IP_HOST=${IP_HOST}	INTERFACE=${INTERFACE}	IPV6=${IPV6}

	${HAS_TO_CONFIRM}=	Run Keyword And Return Status
	...	Should Match Regexp	${OUTPUT}	Are you sure you want to continue connecting \\(.*\\)\\?
	Run Keyword If	${HAS_TO_CONFIRM}	Write	yes
	${OUTPUT}=	Run Keyword If	${HAS_TO_CONFIRM}
	...	Read Until	Password:
	Run Keyword If	${HAS_TO_CONFIRM}	Log To Console	\n${OUTPUT}\n

	FOR		${INDEX}	IN RANGE	3
		Write	${WRONG_PASSWORD}	loglevel=TRACE
		${OUTPUT}=	Run Keyword If	'${INDEX}' < "2"	SUITE:Read Until	(P|p)assword:
		...	ELSE	SUITE:Read Until	~#
		Log To Console	\n${OUTPUT}\n
	END

	CLI:Switch Connection	HOST
	Run Keyword If	'${NGVERSION}' >= '5.0' and '${WHITELISTED_IP}' == 'No' and '${IPV6}' == 'No'	Wait Until Keyword Succeeds	15s	3s	SUITE:Check IF Event Was Generated
	...	Event ID 203: IP banned for failure to authenticate multiple times. IP Address: ${IP_HOSTSHARED}.
	Run Keyword If	'${NGVERSION}' >= '5.0' and '${WHITELISTED_IP}' == 'No' and '${IPV6}' == 'Yes'	Wait Until Keyword Succeeds	15s	3s	SUITE:Check IF Event Was Generated
	...	Event ID 203: IP banned for failure to authenticate multiple times. IP Address: ${IP_HOSTSHARED}
	Run Keyword If	'${NGVERSION}' >= '5.0' and '${WHITELISTED_IP}' == 'Yes' and '${IPV6}' == 'Yes'	Wait Until Keyword Succeeds	15s	3s	SUITE:Check IF Event Was Generated
	...	Event ID 202: User authentication failed. User: admin@${IP_HOSTSHARED}
	Run Keyword If	'${NGVERSION}' < '5.0' and '${WHITELISTED_IP}' == 'No' and '${IPV6}' == 'No'	Wait Until Keyword Succeeds	15s	3s	SUITE:Check IF Event Was Generated
	...	Event ID 202: User authentication failed. User: admin@${IP_HOSTSHARED}.

SUITE:Open SSH Connection From Peer
	[Arguments]	${IP_HOST}	${INTERFACE}	${IPV6}
	Run Keyword If	'${IPV6}' == 'No'	Write	ssh ${USERNAME}@${IP_HOST}
	Run Keyword If	'${IPV6}' == 'Yes'	Write	ssh -6 ${USERNAME}@${IP_HOST}%${INTERFACE}
	Set Client Configuration	timeout=60s
	${OUTPUT}=	Read Until Regexp	(~#|(P|p)assword:|Are you sure you want to continue connecting \\(.*\\)\\?)	loglevel=INFO
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Log To Console	\n${OUTPUT}\n
	[Return]	${OUTPUT}

SUITE:Read Until
	[Arguments]	${PROMPT}
	Set Client Configuration	timeout=60s
	${OUTPUT}=	Read Until Regexp	${PROMPT}	loglevel=INFO
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	[Return]	${OUTPUT}

SUITE:Check IF Event Was Generated
	[Arguments]	${EVENT}
	${OUTPUT}=	CLI:Write	tail -1 /var/local/EVT/${HOSTNAME_NODEGRID}
	Should Contain	${OUTPUT}	${EVENT}
	Log To Console	\n${OUTPUT}\n

SUITE:Set Client Configurations And Exit On Buth Sides
	CLI:Switch Connection	HOST
	Set Client Configuration	prompt=]#
	CLI:Write	exit
	CLI:Switch Connection	PEER
	Set Client Configuration	prompt=]#
	CLI:Write	exit

SUITE:Try To Login
	[Arguments]	${WHITELISTED_IP}=No	${IP_HOST}=${HOST}	${IP_HOSTSHARED}=${HOST_DIFF}	${IPV6}=No	${INTERFACE}=${INTERFACE}
	...	${BLOCKED}=No
	CLI:Switch Connection	PEER
	${OUTPUT}=	Wait Until Keyword Succeeds	60s	3s
	...	SUITE:Open SSH Connection From Peer	IP_HOST=${IP_HOST}	INTERFACE=${INTERFACE}	IPV6=${IPV6}

	Run Keyword If	'${BLOCKED}' == 'No'	Should Not Contain	${OUTPUT}	ssh: connect to host ${IP_HOST} port 22: No route to host
	Run Keyword If	'${NGVERSION}' >= '5.0' and '${BLOCKED}' == 'Yes' and '${IPV6}' == 'No'	Should Contain	${OUTPUT}	ssh: connect to host ${IP_HOST} port 22: Connection refused
	Run Keyword If	'${NGVERSION}' >= '5.0' and '${BLOCKED}' == 'Yes' and '${IPV6}' == 'Yes'	Should Contain	${OUTPUT}	ssh: connect to host ${IP_HOST}%${INTERFACE} port 22: Connection refused
	Log To Console	\n${OUTPUT}\n

	Run Keyword If	'${BLOCKED}' == 'No'	Write	${DEFAULT_PASSWORD}	loglevel=INFO
	${OUTPUT}=	Run Keyword If	'${BLOCKED}' == 'No'	SUITE:Read Until	]#
	Run Keyword If	'${BLOCKED}' == 'No'	Log To Console	\n${OUTPUT}\n

	CLI:Switch Connection	HOST
	Run Keyword If	'${BLOCKED}' == 'No' and '${IPV6}' == 'No'	Wait Until Keyword Succeeds	30s	3s	SUITE:Check IF Event Was Generated
	...	Event ID 200: A user logged into the system. User: admin@${IP_HOSTSHARED}. Session type: SSH. Authentication Method: Local.
	Run Keyword If	'${BLOCKED}' == 'No' and '${IPV6}' == 'Yes'	Wait Until Keyword Succeeds	30s	3s	SUITE:Check IF Event Was Generated
	...	Event ID 200: A user logged into the system. User: admin@${IP_HOSTSHARED}%${INTERFACE}. Session type: SSH. Authentication Method: Local.

	CLI:Switch Connection	PEER
	Run Keyword If	'${BLOCKED}' == 'No'	Write	exit	loglevel=INFO
	${OUTPUT}=	Run Keyword If	'${BLOCKED}' == 'No'	SUITE:Read Until	~#
	Run Keyword If	'${BLOCKED}' == 'No'	Log To Console	\n${OUTPUT}\n