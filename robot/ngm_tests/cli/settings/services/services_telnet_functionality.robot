*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Telnet Services Commands... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI    NON-CRITICAL   NEED-REVIEW
Default Tags	CLI	SSH	SHOW
Library	Telnet	timeout=30s
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test functionality of field=enable_telnet_service_to_nodegrid
	SSHLibrary.Write	cd /settings/services/
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	set enable_telnet_service_to_nodegrid=yes
	${OUTPUT}=	SSHLibrary.Read Until Prompt
	Log To Console 	Console: ${OUTPUT}
	SSHLibrary.Write	show
	${OUTPUT}=	SSHLibrary.Read Until Prompt
	Log To Console	Output: ${OUTPUT}
	SSHLibrary.Write	commit
	${OUTPUT}=	SSHLibrary.Read Until Prompt
	Log To Console 	Console: ${OUTPUT}
	Should Not Contain 	${OUTPUT} 	Error
	SSHLibrary.Write	show
	${OUTPUT}=	SSHLibrary.Read Until Prompt
	Log To Console	Output: ${OUTPUT}
	Should Contain	${OUTPUT}	enable_telnet_service_to_nodegrid = yes

	Telnet.Open Connection	${HOST}	alias=device_session	prompt=]#
	Telnet.Login	admin	${DEFAULT_PASSWORD}
	Log	\nhost: ${HOST}	INFO	console=yes
	Telnet.Write	whoami
	${OUTPUT}=	Telnet.Read Until Prompt
	Should Contain	${OUTPUT}	${DEFAULT_USERNAME}
	Telnet.Close Connection
	CLI:Switch Connection	default

	SSHLibrary.Write	cd /settings/services/
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	set enable_telnet_service_to_nodegrid=no
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	commit
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	show
	${OUTPUT}=	SSHLibrary.Read Until Prompt
	Log To Console	Output: ${OUTPUT}
	Should Contain	${OUTPUT}	enable_telnet_service_to_nodegrid = no

	Run Keyword And Expect Error	error: [Errno 111] Connection refused	Telnet.Open Connection	${HOST}	alias=device_session

Test functionality of field=enable_telnet_service_to_managed_devices
	SSHLibrary.Write	cd /settings/services/
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	set enable_telnet_service_to_nodegrid=yes enable_telnet_service_to_managed_devices=yes
	${OUTPUT}=	SSHLibrary.Read Until Prompt
	Log To Console 	Console: ${OUTPUT}
	SSHLibrary.Write	show
	${OUTPUT}=	SSHLibrary.Read Until Prompt
	Log To Console	Output: ${OUTPUT}
	SSHLibrary.Write	commit
	${OUTPUT}=	SSHLibrary.Read Until Prompt
	Log To Console 	Console: ${OUTPUT}
	Should Not Contain 	${OUTPUT} 	Error
	Wait Until Keyword Succeeds	1m	1s	SUITE:Should Contain

	SSHLibrary.Write	cd /settings/devices/
	SSHLibrary.Read Until Prompt
	${EXISTS}=	SUITE:Check If Device Already Exists
	Run Keyword If	${EXISTS}	SUITE:Delete Device
	SSHLibrary.Write	add
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	set name=telnet_device type=device_console ip_address=127.0.0.1 username=root password=${ROOT_PASSWORD} mode=enabled
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	commit
	${OUTPUT}=	SSHLibrary.Read Until Prompt
	${STATUS}=	Run Keyword and Return Status	Should Not Contain	${OUTPUT}	Error
	Run Keyword If	${STATUS} == False	Run Keywords	SSHLibrary.Write	cancel	AND	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	show
	${OUTPUT}=	SSHLibrary.Read Until Prompt
	Log To Console	Output: ${OUTPUT}
	Should Contain	${OUTPUT}	telnet_device
	SSHLibrary.Write	cd /settings/devices/telnet_device/access/
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	set allow_telnet_protocol=yes
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	commit
	${OUTPUT}=	SSHLibrary.Read Until Prompt
	${STATUS}=	Run Keyword and Return Status	Should Not Contain	${OUTPUT}	Error

	Telnet.Open Connection	${HOST}	alias=device_session	prompt=cli ]
	${OUTPUT}=	Telnet.Login	${DEFAULT_USERNAME}:telnet_device	${DEFAULT_PASSWORD}
	Log to console	\nOpenen Session: ${OUTPUT}
	Telnet.Write Bare	\n\n\
	Telnet.Read Until	\#
	Telnet.Close Connection

	CLI:Switch Connection	default
	SSHLibrary.Write	cd /settings/services/
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	set enable_telnet_service_to_managed_devices=no
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	commit
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	show
	${OUTPUT}=	SSHLibrary.Read Until Prompt
	Log To Console	Output: ${OUTPUT}
	Should Contain	${OUTPUT}	enable_telnet_service_to_managed_devices = no

	Telnet.Open Connection	${HOST}	alias=device_session	prompt=]
	${STATUS}=	Run Keyword And Return Status	Run Keyword And Expect Error	Login incorrect	Telnet.Login	${DEFAULT_USERNAME}:telnet_device	${DEFAULT_PASSWORD}
	Run Keyword If	${STATUS} == False	Telnet.Open Connection	${HOST}	alias=device_session	prompt=]
	Run Keyword If	${STATUS} == False	Run Keyword And Expect Error	Connection closed by foreign host.	Telnet.Login	${DEFAULT_USERNAME}:telnet_device	${DEFAULT_PASSWORD}
	SSHLibrary.Write	cd /settings/services/
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	set enable_telnet_service_to_nodegrid=no
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	commit
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	show
	${OUTPUT}=	SSHLibrary.Read Until Prompt
	Log To Console	Output: ${OUTPUT}
	Should Contain	${OUTPUT}	enable_telnet_service_to_nodegrid = no

	SUITE:Delete Device

Test functionality of field=telnet_tcp_port
	Run Keyword If	'${NGVERSION}' < '4.0'	Set Tags	NON-CRITICAL
	SSHLibrary.Write	cd /settings/services/
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	set enable_telnet_service_to_nodegrid=yes telnet_tcp_port=36
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	commit
	${OUTPUT}=	SSHLibrary.Read Until Prompt
	Log To Console 	Console: ${OUTPUT}
	Should Not Contain 	${OUTPUT} 	Error
	SSHLibrary.Write	show
	${OUTPUT}=	SSHLibrary.Read Until Prompt
	Log To Console	Output: ${OUTPUT}
	Should Contain	${OUTPUT}	telnet_tcp_port = 36

	#default port for Telnet is 23
	Run Keyword And Expect Error	error: [Errno 111] Connection refused	Telnet.Open Connection	${HOST}	alias=device_session

	Telnet.Open Connection	${HOST}	alias=device_session	port=36	prompt=]#
	Telnet.Login	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}
	Log	\nhost: ${HOST}	INFO	console=yes
	Telnet.Write	whoami
	${OUTPUT}=	Telnet.Read Until Prompt
	Should Contain	${OUTPUT}	${DEFAULT_USERNAME}
	Telnet.Close Connection
	CLI:Switch Connection	default

	SSHLibrary.Write	cd /settings/services/
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	set telnet_tcp_port=23
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	set enable_telnet_service_to_nodegrid=no
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	commit
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	show
	${OUTPUT}=	SSHLibrary.Read Until Prompt
	Log To Console	Output: ${OUTPUT}
	Should Contain	${OUTPUT}	enable_telnet_service_to_nodegrid = no

Test shell access through telnet
	Skip If	'${NGVERSION}' <= '4.0'	Feature only in 4.1
	SSHLibrary.Write	cd /settings/services/
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	set enable_telnet_service_to_nodegrid=yes
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	commit
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	show
	${OUTPUT}=	SSHLibrary.Read Until Prompt
	Log To Console	Output: ${OUTPUT}
	Should Contain	${OUTPUT}	enable_telnet_service_to_nodegrid = yes

	SUITE:Add Group and User

	SSHLibrary.Write	cd /settings/authorization/another_group/profile
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	set startup_application=shell
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	commit
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	show
	${OUTPUT}=	SSHLibrary.Read Until Prompt
	Log To Console	Output: ${OUTPUT}
	Should Contain	${OUTPUT}	startup_application = shell

	Telnet.Open Connection	${HOST}	alias=telnet_session	prompt=$
	Telnet.Login	grumpy	grumpy
	Log	\nhost: ${HOST}	INFO	console=yes
	Telnet.Write	ls /
	${OUTPUT}=	Telnet.Read Until Prompt
	Log To Console	Output: ${OUTPUT}
	Should Contain	${OUTPUT}	backup	boot	etc	lib
	Telnet.Write	cli
	Telnet.Read Until	]#
	Telnet.Write	ls /
	${OUTPUT}=	Telnet.Read Until	]#
	Log To Console	Output: ${OUTPUT}
	Should Contain	${OUTPUT}	access	system
	Telnet.Close Connection
	CLI:Switch Connection	default
	SUITE:Delete Group and User

	SSHLibrary.Write	cd /settings/services/
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	set enable_telnet_service_to_nodegrid=no
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	commit
	SSHLibrary.Read Until Prompt

*** Keywords ***
SUITE:Setup
	SSHLibrary.Enable Ssh Logging	about.log
	SSHLibrary.Set Default Configuration	loglevel=INFO
	SSHLibrary.Set Default Configuration	prompt=]#
	SSHLibrary.Set Default Configuration	newline=\n
	SSHLibrary.Set Default Configuration	width=400
	SSHLibrary.Set Default Configuration	height=600
	SSHLibrary.Set Default Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	SSHLibrary.Open Connection	${HOST}	alias=default
	SSHLibrary.Login	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}	loglevel=INFO

SUITE:Teardown
	Run Keyword If Any Tests Failed	SUITE:Reset Telnet
	SSHLibrary.Close All Connections

SUITE:Delete Group and User
	SSHLibrary.Write	cd /settings/local_accounts/
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	delete grumpy
	SSHLibrary.Read Until	:
	SSHLibrary.Write	yes
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	commit
	SSHLibrary.Read Until Prompt

	SSHLibrary.Write	cd /settings/authorization/
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	delete another_group
	SSHLibrary.Read Until	:
	SSHLibrary.Write	yes
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	commit
	SSHLibrary.Read Until Prompt

SUITE:Add Group and User
	SSHLibrary.Write	cd /settings/authorization/
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	add
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	set name=another_group
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	commit
	SSHLibrary.Read Until Prompt

	SSHLibrary.Write	cd /settings/local_accounts/
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	add
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	set username=grumpy password=grumpy user_group=another_group
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	commit
	SSHLibrary.Read Until Prompt

SUITE:Check If Device Already Exists
	SSHLibrary.Write	ls /settings/devices/
	${LSOUTPUT}=	Catenate	${SPACE}
	FOR		${INDEX}	IN RANGE	1	6
		${OUTPUT}=	SSHLibrary.Read Until Regexp	[:#]
		${LSOUTPUT}=	Catenate	${LSOUTPUT}	${OUTPUT}
		${LASTCHAR}=	Get Substring	${OUTPUT}	-1
		Run Keyword If	'${LASTCHAR}' == '#'	Exit For Loop
		SSHLibrary.Write Bare	\n
	END
	${OUTPUT}=	Replace String	${LSOUTPUT}	\n	${SPACE}
	${RESULT}=	Run Keyword And Return Status	Should Contain	${OUTPUT}	${SPACE}telnet_device/
	[Return]	${RESULT}

SUITE:Delete Device
	SSHLibrary.Write	cd /settings/devices/
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	delete telnet_device
	SSHLibrary.Read Until	:
	SSHLibrary.Write	yes
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	commit
	SSHLibrary.Read Until Prompt

SUITE:Reset Telnet
	SSHLibrary.Write	cd /settings/services/
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	set enable_telnet_service_to_managed_devices=no enable_telnet_service_to_nodegrid=no
	SSHLibrary.Read Until Prompt
	SSHLibrary.Write	commit
	SSHLibrary.Read Until Prompt
	${DEVICE}=	SUITE:Check If Device Already Exists
	Run Keyword If	${DEVICE}	SUITE:Delete Device

SUITE:Should Contain
	SSHLibrary.Write	show
	${OUTPUT}=	SSHLibrary.Read Until Prompt
	Log To Console	Output: ${OUTPUT}
	Should Contain	${OUTPUT}	enable_telnet_service_to_managed_devices = yes