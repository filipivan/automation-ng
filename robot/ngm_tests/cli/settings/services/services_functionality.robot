*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Services Commands... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW
Suite Setup	CLI:Open
Suite Teardown	Close All Connections

*** Variables ***
${PS_GRPC}	ps -aux | grep /usr/bin/node | grep grpc
${EXPECTED_PGRPC}	grpc_server
${NETSTAT_GRPC}	netstat -napt | grep node | grep

${DEFAULT_GRPC_PORT}	4830
${CHANGED_GRPC_PORT}	4895
${EXPECTED_NGRPC_4830}	:::${DEFAULT_GRPC_PORT}
${EXPECTED_NGRPC_4860}	:::${CHANGED_GRPC_PORT}

*** Test Cases ***
Test functionality of field=enable_rpc
	CLI:Enter Path	/settings/services/
	CLI:Set Field	enable_rpc	yes
	CLI:Commit
	CLI:Test Show Command	enable_rpc = yes

	CLI:Connect As Root
	${OUTPUT}=	CLI:Write	netstat -natp |grep rpc
	Should Contain	${OUTPUT}	rpcbind
	Close Connection
	CLI:Switch Connection	default

	CLI:Enter Path	/settings/services/
	CLI:Set Field	enable_rpc	no
	CLI:Commit
	CLI:Test Show Command	enable_rpc = no

	CLI:Connect As Root
	${OUTPUT}=	CLI:Write	netstat -natp |grep rpc
	Should Not Contain	${OUTPUT}	rpcbind
	Close Connection
	CLI:Switch Connection	default

Test functionality of field=enable_autodiscovery
	CLI:Enter Path	/settings/services/
	CLI:Set Field	enable_autodiscovery	no
	CLI:Commit
	CLI:Test Show Command	enable_autodiscovery = no

	CLI:Enter Path	/settings/devices
	@{DEVICELIST}=	CLI:Test Get Devices
	${LENGTH}=	Get Length	${DEVICELIST}
	Run Keyword If	'${LENGTH}' == '1'	CLI:Add Device	dummy_device_network	device_console	127.0.0.1
	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Delete If Exists Confirm	dummy_network_scan
	CLI:Add
	CLI:Set Field	scan_id	dummy_network_scan
	CLI:Set Field	ip_range_start	${HOST}
	CLI:Set Field	ip_range_end	${HOST}
	CLI:Set Field	scan_interval	10
	CLI:Commit
	CLI:Test Show Command Regexp	dummy_network_scan\\s+${HOST}/${HOST}\\s+enabled

	CLI:Enter Path	/settings/auto_discovery/discovery_logs
	Run Keyword If	'${NGVERSION}' < '4.0'	Wait Until Keyword Succeeds	5x	5s	CLI:Test Show Command Regexp	(\\S+\\s+){5}${HOST}\\s+${HOST}\\s+network\\sscan\\s+discovery\\sdisabled
	Run Keyword If	'${NGVERSION}' >= '4.0' and '${HOST}' == '192.168.2.155'	Wait Until Keyword Succeeds	5x	5s	CLI:Test Show Command Regexp	(\\S+\\s+){5}${HOST}\\s+ad.zpesystems.com\\s+Network\\sScan\\s+Discovery\\sDisabled
	Run Keyword If	'${NGVERSION}' >= '4.0' and '${HOST}' != '192.168.2.155'	Wait Until Keyword Succeeds	5x	5s	CLI:Test Show Command Regexp	(\\S+\\s+){5}${HOST}\\s+${HOST}\\s+Network\\sScan\\s+Discovery\\sDisabled
	CLI:Write	reset_logs
	${STATUS}=	Run Keyword And Return Status	CLI:Test Not Show Command	Discovery Disabled
	Run Keyword If	${STATUS} == False	CLI:Write	reset_logs

	CLI:Enter Path	/settings/services/
	CLI:Set Field	enable_autodiscovery	yes
	CLI:Commit
	CLI:Test Show Command	enable_autodiscovery = yes

	CLI:Enter Path	/settings/auto_discovery/discover_now/
	Run Keyword If	'${NGVERSION}' < '4.0'	CLI:Write	discover_now net|dummy_network_scan
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Write	discover_now dummy_network_scan

	CLI:Enter Path	/settings/auto_discovery/discovery_logs
	Run Keyword If	'${NGVERSION}' < '4.0'	Wait Until Keyword Succeeds	5x	5s	CLI:Test Show Command Regexp	(\\S+\\s+){5}${HOST}\\s+${HOST}\\s+network\\sscan\\s+none
	Run Keyword If	'${NGVERSION}' >= '4.0' and '${HOST}' != '192.168.2.155'	Wait Until Keyword Succeeds	5x	5s	CLI:Test Show Command Regexp	(\\S+\\s+){5}${HOST}\\s+${HOST}\\s+Network\\sScan\\s+None
	Run Keyword If	'${NGVERSION}' >= '4.0' and '${HOST}' == '192.168.2.155'	Wait Until Keyword Succeeds	5x	5s	CLI:Test Show Command Regexp	(\\S+\\s+){5}${HOST}\\s+ad.zpesystems.com\\s+Network\\sScan\\s+None
	CLI:Write	reset_logs

	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Delete If Exists Confirm	dummy_network_scan
	CLI:Delete Devices	dummy_device_network
	[Teardown]	Run Keywords	CLI:Enter Path	/settings/auto_discovery/network_scan/	AND	CLI:Delete If Exists Confirm	dummy_network_scan	AND	CLI:Delete Devices	dummy_device_network

Check if field is set in different places=auto_cloud_enroll
	CLI:Enter Path	/settings/services/
	Run Keyword If	'${NGVERSION}' < '4.1'	CLI:Set Field	auto_cloud_enroll	no
	...	ELSE	CLI:Set Field	auto_cluster_enroll	no
	CLI:Commit
	Run Keyword If	'${NGVERSION}' < '4.1'	CLI:Test Show Command	auto_cloud_enroll = no
	...	ELSE	CLI:Test Show Command	auto_cluster_enroll = no

	CLI:Enter Cloud Settings Path
	CLI:Test Show Command	auto_enroll = no
	Run Keyword If	'${NGVERSION}' > '3.2'	CLI:Edit
	CLI:Set Field	auto_enroll	yes
	CLI:Commit
	CLI:Test Show Command	auto_enroll = yes

	CLI:Enter Path	/settings/services/
	Run Keyword If	'${NGVERSION}' < '4.1'	CLI:Test Show Command	auto_cloud_enroll = yes
	...	ELSE	CLI:Test Show Command	auto_cluster_enroll = yes

Test functionality of field=enable_grpc
	Skip If	'${NGVERSION}' < '4.1'	Feature not in 4.0 or 3.2
	CLI:Enter Path	/settings/services/
	CLI:Set Field	enable_grpc	no
	CLI:Commit
	CLI:Test Show Command	enable_grpc = no
	CLI:Test Not Show Command	grpc_port	${DEFAULT_GRPC_PORT}

	CLI:Connect As Root
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Command and Expected Result	${PS_GRPC}	${EXPECTED_PGRPC}	${FALSE}
	CLI:Switch Connection	default

	CLI:Enter Path	/settings/services/
	CLI:Set Field	enable_grpc	yes
	CLI:Commit
	CLI:Test Show Command	enable_grpc = yes

	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Command and Expected Result	${PS_GRPC}	${EXPECTED_PGRPC}
	CLI:Switch Connection	default

	CLI:Enter Path	/settings/services/
	CLI:Set Field	enable_grpc	no
	CLI:Commit
	CLI:Test Show Command	enable_grpc = no
	CLI:Test Not Show Command	grpc_port	${DEFAULT_GRPC_PORT}

	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Command and Expected Result	${PS_GRPC}	${EXPECTED_PGRPC}	${FALSE}
	Close Connection
	[Teardown]	Run Keywords	Skip If	'${NGVERSION}' < '4.1'	Feature not in 4.0 or 3.2
	...	AND	CLI:Switch Connection	default
	...	AND	SUITE:Disable gRPC

Test functionality of field=grpc_port
	Skip If	'${NGVERSION}' < '4.1'	Feature not in 4.0 or 3.2
	CLI:Enter Path	/settings/services/
	CLI:Set Field	enable_grpc	yes
	CLI:Set Field	grpc_port	${DEFAULT_GRPC_PORT}
	CLI:Set Field	enable_grpc	no
	CLI:Commit
	CLI:Test Show Command	enable_grpc = no
	CLI:Test Not Show Command	grpc_port	${DEFAULT_GRPC_PORT}

	CLI:Connect As Root
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Command and Expected Result	${PS_GRPC}	${EXPECTED_PGRPC}	${FALSE}
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Command and Expected Result	${NETSTAT_GRPC} ${EXPECTED_NGRPC_4830}	${EXPECTED_NGRPC_4830}	${FALSE}

	CLI:Switch Connection	default
	CLI:Enter Path	/settings/services/
	CLI:Set Field	enable_grpc	yes
	CLI:Set Field	grpc_port	${DEFAULT_GRPC_PORT}
	CLI:Commit
	CLI:Test Show Command	enable_grpc = yes
	CLI:Test Show Command	grpc_port = ${DEFAULT_GRPC_PORT}

	Sleep	4
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Command and Expected Result	${PS_GRPC}	${EXPECTED_PGRPC}
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Command and Expected Result	${NETSTAT_GRPC} ${EXPECTED_NGRPC_4830}	${EXPECTED_NGRPC_4830}

	CLI:Switch Connection	default
	CLI:Enter Path	/settings/services/
	CLI:Set Field	enable_grpc	yes
	CLI:Set Field	grpc_port	${CHANGED_GRPC_PORT}
	CLI:Commit
	CLI:Test Show Command	enable_grpc = yes
	CLI:Test Show Command	grpc_port = ${CHANGED_GRPC_PORT}

	Sleep	4
	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Command and Expected Result	${PS_GRPC}	${EXPECTED_PGRPC}
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Command and Expected Result	${NETSTAT_GRPC} ${EXPECTED_NGRPC_4860}	${EXPECTED_NGRPC_4860}
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Command and Expected Result	${NETSTAT_GRPC} ${EXPECTED_NGRPC_4830}	${EXPECTED_NGRPC_4830}	${FALSE}

	CLI:Switch Connection	default
	CLI:Set Field	enable_grpc	yes
	CLI:Set Field	grpc_port	${DEFAULT_GRPC_PORT}
	CLI:Set Field	enable_grpc	no
	CLI:Commit
	CLI:Test Show Command	enable_grpc = no
	CLI:Test Not Show Command	grpc_port	${DEFAULT_GRPC_PORT}

	CLI:Switch Connection	root_session
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Command and Expected Result	${PS_GRPC}	${EXPECTED_PGRPC}	${FALSE}
	Wait Until Keyword Succeeds	1m	1s	SUITE:Check Command and Expected Result	${NETSTAT_GRPC} ${EXPECTED_NGRPC_4830}	${EXPECTED_NGRPC_4830}	${FALSE}
	Close Connection
	CLI:Switch Connection	default
	[Teardown]	Run Keywords	Skip If	'${NGVERSION}' < '4.1'	Feature not in 4.0 or 3.2
	...	AND	CLI:Switch Connection	default
	...	AND	SUITE:Disable gRPC

*** Keywords ***
SUITE:Disable gRPC
	CLI:Enter Path	/settings/services/
	CLI:Set Field	enable_grpc	yes
	CLI:Set Field	grpc_port	${DEFAULT_GRPC_PORT}
	CLI:Set Field	enable_grpc	no
	CLI:Commit
	CLI:Test Show Command	enable_grpc = no
	CLI:Test Not Show Command	grpc_port	${DEFAULT_GRPC_PORT}

SUITE:Check Command and Expected Result
	[Arguments]	${COMMAND}	${EXPECTED}	${SHOULD}=${TRUE}
	${OUTPUT}=	CLI:Write	${COMMAND}
	Run Keyword If	${SHOULD}	Should Contain	${OUTPUT}	${EXPECTED}
	Run Keyword If	${SHOULD} == ${FALSE}	Should Not Contain	${OUTPUT}	${EXPECTED}
