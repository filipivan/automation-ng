*** Settings ***
Resource	../../init.robot
Documentation	Functionality test cases through the CLI and root mode for an Alpine virtual machine
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	NON-CRITICAL

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${LICENSE}	${LICENSE_VM_AND_DOCKER}
${VM_NAME}	test_vm_alpine
${VM_NAME_IMAGE}	test_vm_alpine.img
${ALPINE_FILE_NAME}	alpine-virt-3.14.2-x86_64.iso
${NEW_VM_NAME}	alpine_renamed
*** Test Cases ***
Test add configuration before VM
	[Documentation]	Adds configuration to map VM and checks the port was not mapped, since the VM was not yet created
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL
	Skip If	${USB_DEVICES_EMPTY}	No USB devices to map on this unit
	SUITE:Map USB Device To A VM	${USB_PORTS}
	SUITE:Check USB Devices Mapped To VM In system/usb_devices	${USB_NAMES}[0]	EXPECTED=${FALSE}

Test Create Directory And Wget Alpine ISO
	[Documentation]	Downloads VM image .iso image from FTP server
	[Setup]	CLI:Connect As Root
	CLI:Write	mkdir /var/lib/libvirt/images/${VM_NAME}	Raw
	CLI:Enter Path	/var/lib/libvirt/images/${VM_NAME}
	Log To Console	Executing wget now
	Set Client Configuration	timeout=60s
	${OUTPUT}=	CLI:Write	wget --user=${FTPSERVER2_USER} --password=${FTPSERVER2_PASSWORD} ftp://${FTPSERVER2_USER}@${FTPSERVER2_IP}/files/${ALPINE_FILE_NAME}	Raw
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	${LIST}=	Create List	${ALPINE_FILE_NAME}	100%	saved
	${STATUS}=	Run Keyword And Return Status	CLI:Should Contain All	${OUTPUT}	${LIST}
	Run Keyword If	not ${STATUS}	SUITE:Go Back To CLI And Fail	Something wrong happened when transferring the file from ftp server to NG

Test Execute "virt-install" Command To Alpine VM
	[Documentation]	Install Alpine VM image using virsh commands in root shell
	CLI:Switch Connection	root_session
	CLI:Enter Path	/var/lib/libvirt/images/${VM_NAME}
	Set Client Configuration	prompt=login:
	Set Client Configuration	timeout=120s
	${OUTPUT}=	CLI:Write	virt-install --name test_vm_alpine --ram 512 --disk path=/var/lib/libvirt/images/${VM_NAME}/${VM_NAME_IMAGE},size=3 --vcpus 1 --os-type linux --os-variant generic --network bridge:virbr0,model=virtio --graphics none --console pty,target_type=serial --cdrom ./${ALPINE_FILE_NAME}	Raw
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	${STATUS}=	Run Keyword And Return Status	Should Contain	${OUTPUT}	Welcome to Alpine
	Run Keyword If	not ${STATUS}	SUITE:Should Fail	Something wrong happened during the VM instalation

Test Login To Alpine VM
	[Documentation]	Logins in Alpine VM to check installation was successfull
	Set Client Configuration	prompt=#
	${OUTPUT}=	CLI:Write	${ROOT_PASSWORD}	Raw
	${LIST}=	Create List	Welcome to Alpine!	You can setup the system with the command: setup-alpine
	${STATUS}=	Run Keyword And Return Status	CLI:Should Contain All	${OUTPUT}	${LIST}
	Run Keyword If	not ${STATUS}	SUITE:Should Fail	Login to Alpine VM Failed

Test Setup alpine VM
	[Documentation]	Setup Alpine VM with desired configurations
	Write	setup-alpine
	SUITE:Read Until	Select keyboard layout: [none]
	Write Bare	\n
	SUITE:Read Until	Enter system hostname (short form, e.g. 'foo') [localhost]
	Write Bare	\n
	SUITE:Read Until	Which one do you want to initialize? (or '?' or 'done') [eth0]
	Write Bare	\n
	SUITE:Read Until	Ip address for eth0? (or 'dhcp', 'none', '?') [dhcp]
	Write Bare	\n
	SUITE:Read Until	Do you want to do any manual network configuration? (y/n) [n]
	Write Bare	\n
	SUITE:Read Until	New password:
	Write Bare	\n
	SUITE:Read Until	Retype password:
	Write Bare	\n
	SUITE:Read Until	Which timezone are you in? ('?' for list) [UTC]
	Write Bare	\n
	SUITE:Read Until	HTTP/FTP proxy URL? (e.g. 'http://proxy:8080', or 'none') [none]
	Write Bare	\n
	SUITE:Read Until	URL to add (or r/f/e/done) [1]
	Write Bare	\n
	SUITE:Read Until	Which SSH server? ('openssh', 'dropbear' or 'none') [openssh]
	Write Bare	\n
	SUITE:Read Until	Which disk(s) would you like to use? (or '?' for help or 'none') [none]
	Write	sda
	SUITE:Read Until	How would you like to use it? ('sys', 'data', 'lvm' or '?' for help) [?]
	Write	sys
	SUITE:Read Until	WARNING: Erase the above disk(s) and continue? (y/n) [n]
	Write	y
	SUITE:Read Until	~#	Installation is complete. Please reboot.

Test Reboot Alpine VM
	[Documentation]	Reboots the machine to apply configuration
	Set Client Configuration	prompt=localhost login:
	Set Client Configuration	timeout=120s
	${OUTPUT}=	CLI:Write	reboot	Raw
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	${STATUS}=	Run Keyword And Return Status	Should Contain	${OUTPUT}	Welcome to Alpine
	Run Keyword If	not ${STATUS}	SUITE:Should Fail	Alpine VM Reboot problems

Test Login To Alpine VM After Installed To Check VM Persistence On NG
	[Documentation]	Tries login again and tests ping inside Alpine VM to check external connectivity
	Set Client Configuration	prompt=Password:
	CLI:Write	root
	Set Client Configuration	prompt=#
	CLI:Write Bare	\n
	${OUTPUT}=	CLI:Write	cat /etc/network/interfaces
	${LIST}=	Create List	auto eth0	iface eth0 inet dhcp	hostname localhost
	${STATUS1}=	Run Keyword And Return Status	CLI:Should Contain All	${OUTPUT}	${LIST}
	Run Keyword If	not ${STATUS1}	SUITE:Should Fail	Missing dhcp configurations inside alpine VM
	${STATUS2}=	Run Keyword And Return Status	Wait Until Keyword Succeeds	2x	3s	SUITE:Test Ping For Alpine	google.com	NUM_PACKETS=3	TIMEOUT=30
	Run Keyword If	not ${STATUS2}	SUITE:Should Fail	VM Cannot access the external network after reboot
	CLI:Write	\x1D
	[Teardown]	CLI:Switch Connection	default

Test USB device was mapped successfully
	[Documentation]	Checks USB port was mapped to Alpine VM
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL
	Skip If	${USB_DEVICES_EMPTY}	No USB devices to map on this unit
	SUITE:Check USB Devices Mapped To VM Via lsusb Command	${USB_PORTS}
	SUITE:Check USB Devices Mapped To VM In system/usb_devices	${USB_NAMES}[0]
	SUITE:Check USB Devices Mapped To VM Using Virsh Commands	${USB_PORTS}

Test enable/disable service and check USB was mapped/unmapped
	[Documentation]	Disables virtualization service and checks the USB port was unmapped, then enables the service
	...	again and checks USB port was remapped
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL
	[Setup]	CLI:Switch Connection	default
	Skip If	${USB_DEVICES_EMPTY}	No USB devices to map on this unit
	SUITE:Disable Qemu And Kvm
	SUITE:Check USB Devices Mapped To VM In system/usb_devices	${USB_NAMES}[0]	EXPECTED=${FALSE}
	SUITE:Enable Qemu And Kvm
	SUITE:Check USB Devices Mapped To VM Via lsusb Command	${USB_PORTS}
	SUITE:Check USB Devices Mapped To VM In system/usb_devices	${USB_NAMES}[0]
	SUITE:Check USB Devices Mapped To VM Using Virsh Commands	${USB_PORTS}

Test enable/disable config and check USB was mapped/unmapped
	[Documentation]	Checks if, after the configuration was disabled, the USB port was unmapped.  Enables configuration
	...	again and checks USB port was remapped.
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL
	[Setup]	CLI:Switch Connection	default
	Skip If	${USB_DEVICES_EMPTY}	No USB devices to map on this unit
	SUITE:Dettach USB Device From VM	${USB_PORTS}
	SUITE:Check USB Devices Mapped To VM In system/usb_devices	${USB_NAMES}[0]	EXPECTED=${FALSE}
	SUITE:Map USB Device To A VM	${USB_PORTS}
	SUITE:Check USB Devices Mapped To VM Via lsusb Command	${USB_PORTS}
	SUITE:Check USB Devices Mapped To VM In system/usb_devices	${USB_NAMES}[0]
	SUITE:Check USB Devices Mapped To VM Using Virsh Commands	${USB_PORTS}

Test Power off alpine VM and Execute "virsh list --all" command
	[Documentation]	Logins in VM console, powers off the VM and checks if it is off with virsh commands
	CLI:Switch Connection	default
	CLI:Connect As Root
	Set Client Configuration	prompt=#
	Set Client Configuration	timeout=120s
	Write	virsh console ${VM_NAME}
	Read Until	Escape character is ^]
	CLI:Write Bare	\n
	${OUTPUT}=	CLI:Write	poweroff	Raw
	CLI:Write	exit	Raw	#should fallback to NG root
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	${OUTPUT}=	CLI:Write	cat /software
	${STATUS1}=	Run Keyword And Return Status	should match regexp	${OUTPUT}	(nodegrid|Nodegrid|NodeGrid)
	Run Keyword If	not ${STATUS1}	SUITE:Should Fail	VM Doesn't powered off
	${OUTPUT}=	CLI:Write	virsh list --all
	${LIST}=	Create List	test_vm_alpine	shut off
	${STATUS2}=	Run Keyword And Return Status	CLI:Should Contain All	${OUTPUT}	${LIST}
	Run Keyword If	not ${STATUS2}	SUITE:Should Fail	VM Doesn't powered off

Test check USB device is still mapped after VM was shutoff
	[Documentation]	Checks USB port remains mapped even if VM is off
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL
	Skip If	${USB_DEVICES_EMPTY}	No USB devices to map on this unit
	SUITE:Check USB Devices Mapped To VM In system/usb_devices	${USB_NAMES}[0]

Test USB device is unmapped after VM is renamed
	[Documentation]	Checks USB port is not mapped, since VM changed its name
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL
	Skip If	${USB_DEVICES_EMPTY}	No USB devices to map on this unit
	CLI:Switch Connection	root_session
	${OUTPUT}	CLI:Write	virsh domrename ${VM_NAME} ${NEW_VM_NAME}
	Should Contain	${OUTPUT}	Domain successfully renamed
	CLI:Write	virsh start ${NEW_VM_NAME}
	SUITE:Check USB Devices Mapped To VM Via lsusb Command	${USB_PORTS}	VM=${NEW_VM_NAME}	EXPECTED=${FALSE}
	SUITE:Check USB Devices Mapped To VM In system/usb_devices	${USB_NAMES}[0]	VM=${NEW_VM_NAME}	EXPECTED=${FALSE}
	SUITE:Check USB Devices Mapped To VM Using Virsh Commands	${USB_PORTS}	VM=${NEW_VM_NAME}	EXPECTED=${FALSE}

Test USB is mapped after VM is renamed back to original name
	[Documentation]	Renames the VM back to its original name and checks USB port was automatically remapped
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL
	Skip If	${USB_DEVICES_EMPTY}	No USB devices to map on this unit
	CLI:Switch Connection	root_session
	Write	virsh console ${NEW_VM_NAME}
	Read Until	Escape character is ^] (Ctrl + ])
	CLI:Write Bare	\n
	${OUTPUT}=	CLI:Write	poweroff	Raw
	CLI:Write	exit	Raw	#should fallback to NG root
	${OUTPUT}	CLI:Write	virsh domrename ${NEW_VM_NAME} ${VM_NAME}
	Should Contain	${OUTPUT}	Domain successfully renamed
	CLI:Write	virsh start ${VM_NAME}
	SUITE:Check USB Devices Mapped To VM Via lsusb Command	${USB_PORTS}
	SUITE:Check USB Devices Mapped To VM In system/usb_devices	${USB_NAMES}[0]
	SUITE:Check USB Devices Mapped To VM Using Virsh Commands	${USB_PORTS}

Test removing using udev command and check USB was unmapped from VM
	[Documentation]	Tests remove USB devices using udev commands and checks USB port is unmapped
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL
	Skip If	${USB_DEVICES_EMPTY}	No USB devices to map on this unit
	CLI:Connect As Root
	CLI:Write	/usr/bin/udevadm trigger --action=remove
	Wait Until Keyword Succeeds	10x	5s	CLI:Test Event ID Logged	nodegrid	331	USB device disconnected. USB Port: ${USB_PORTS}.
	CLI:Switch Connection	default
	SUITE:Check USB Devices Mapped To VM Via lsusb Command	${USB_PORTS}	EXPECTED=${FALSE}
	SUITE:Check USB Devices Mapped To VM In system/usb_devices	${USB_NAMES}[0]	EXPECTED=${FALSE}
	SUITE:Check USB Devices Mapped To VM Using Virsh Commands	${USB_PORTS}	EXPECTED=${FALSE}

Test adding using udev command and check USB was mapped to VM
	[Documentation]	Tests add USB devices using udev commands and checks USB port is remapped
	[Tags]	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL
	Skip If	${USB_DEVICES_EMPTY}	No USB devices to map on this unit
	CLI:Switch Connection	root_session
	CLI:Write	/usr/bin/udevadm trigger --action=add
	Wait Until Keyword Succeeds	10x	5s	CLI:Test Event ID Logged	nodegrid	330	USB device connected. USB Port: ${USB_PORTS}.
	CLI:Switch Connection	default
	SUITE:Check USB Devices Mapped To VM In system/usb_devices	${USB_NAMES}[0]
	SUITE:Check USB Devices Mapped To VM Using Virsh Commands	${USB_PORTS}

Test Start Alpine VM And Remove License To Check License Persistence
	[Documentation]	Removes the virtualization license and waits to check if after some minutes the VM is powered off automatically
	CLI:Switch Connection	root_session
	CLI:Write	virsh start ${VM_NAME}	Raw
	${OUTPUT}=	CLI:Write	virsh list --all
	${LIST}=	Create List	${VM_NAME}	running
	${STATUS1}=	Run Keyword And Return Status	CLI:Should Contain All	${OUTPUT}	${LIST}
	Run Keyword If	not ${STATUS1}	SUITE:Go Back To CLI And Fail	VM should be running
	CLI:Switch Connection	default
	SUITE:Delete License For VM And Docker
	CLI:Switch Connection	root_session
	${STATUS2}=	Run Keyword And Return Status	Wait Until Keyword Succeeds	8min	5s	SUITE:Check If VM Shut Off
	Run Keyword If	not ${STATUS2}	SUITE:Go Back To CLI And Fail	After 8min VM doens't shut off
	CLI:Switch Connection	default

*** Keywords ***
SUITE:Setup
	${STATUS}=	CLI:Check If FTP Server Is Alive And If Exists File	${ALPINE_FILE_NAME}
	Set Suite Variable	${STATUS}
	Run Keyword If	not ${STATUS}	Fatal Error	File: "${ALPINE_FILE_NAME}" should exists in FTP server. Please, check the FTP Server ${FTPSERVER2_IP}.
	CLI:Open
	CLI:Connect As Root
	${USB_PORTS}	SUITE:Get USB Devices
	${USB_NAMES}	SUITE:Get USB Entries
	Set Suite Variable	${USB_PORTS}
	Set Suite Variable	${USB_NAMES}
	SUITE:Destroy,Undefine And Remove Files Regarding Alpine VM
	SUITE:Disable Qemu And Kvm
	SUITE:Delete License For VM And Docker
	SUITE:Insert License For VM And Docker
	SUITE:Enable Qemu And Kvm

SUITE:Teardown
	Run Keyword If	not ${STATUS}	CLI:Close Connection
	Run Keyword And Ignore Error	CLI:Write	\x1D	#skip alpine VM in case it's still open
	SUITE:Destroy,Undefine And Remove Files Regarding Alpine VM
	CLI:Open
	Run Keyword If	'${NGVERSION}' >= '5.6' and not ${USB_DEVICES_EMPTY}	SUITE:Dettach USB Device From VM	${USB_PORTS}
	SUITE:Disable Qemu And Kvm
	SUITE:Delete License For VM And Docker
	CLI:Close Connection

SUITE:Disable Qemu And Kvm
	[Documentation]	Disables virtualization service in NG CLI
	CLI:Enter Path	/settings/services/
	CLI:Set	enable_qemu|kvm=no
	CLI:Commit
	SUITE:Logout And Relogin
	[Teardown]	CLI:Cancel	Raw

SUITE:Logout And Relogin
	CLI:Close Connection
	CLI:Open

SUITE:Enable Qemu And Kvm
	[Documentation]	Enables virtualization service in NG CLI
	CLI:Enter Path	/settings/services/
	CLI:Set	enable_qemu|kvm=yes
	CLI:Commit
	SUITE:Logout And Relogin
	[Teardown]	CLI:Cancel	Raw

SUITE:Delete License For VM And Docker 
	CLI:Delete All License Keys Installed
	[Teardown]	CLI:Cancel	Raw

SUITE:Insert License For VM And Docker
	CLI:Add License Key	${LICENSE}
	[Teardown]	CLI:Cancel	Raw

SUITE:Check If VM Shut Off
	[Documentation]	Checks if VM is off with virsh command
	${OUTPUT}=	CLI:Write	virsh list --all
	${LIST}=	Create List	${VM_NAME}	shut off
	CLI:Should Contain All	${OUTPUT}	${LIST}

SUITE:Destroy,Undefine And Remove Files Regarding Alpine VM
	[Documentation]	Removes all files and their dependencies from /var/lib/libvirt/images
	CLI:Connect As Root
	CLI:Write	virsh destroy ${VM_NAME}	Raw
	${OUTPUT}=	CLI:Write	virsh undefine ${VM_NAME}	Raw
	Set Client Configuration	prompt=#
	CLI:Enter Path	/var/lib/libvirt/images
	CLI:Write	rm -r ${VM_NAME}	Raw
	${OUTPUT}=	CLI:Ls
	${STATUS1}=	Run Keyword And Return Status	Should Not Contain	${OUTPUT}	${VM_NAME}
	Run Keyword If	not ${STATUS1}	SUITE:Go Back To CLI And Fail	VM files should be removed
	CLI:Switch Connection	default

SUITE:Go Back To CLI And Fail
	[Arguments]	${MESSAGE}
	CLI:Switch Connection	default
	SUITE:Disable Qemu And Kvm
	SUITE:Delete License For VM And Docker
	Log To Console	${FAIL_MESSAGE}
	Fatal Error	${FAIL_MESSAGE}

SUITE:Should Fail
	[Arguments]	${FAIL_MESSAGE}
	Set Client Configuration	prompt=#
	CLI:Write Bare	\x1b	Raw
	CLI:Switch Connection	default
	Log To Console	${FAIL_MESSAGE}
	Fatal Error	${FAIL_MESSAGE}

SUITE:Test Ping For Alpine
	[Documentation]	Executes a ping inside the VM created to check connectivity
	[Arguments]	${IP_ADDRESS}	${NUM_PACKETS}=10	${TIMEOUT}=60
	${PING_COMMAND}=	Set Variable	ping ${IP_ADDRESS} -W ${TIMEOUT} -c ${NUM_PACKETS}
	${OUTPUT}=	CLI:Write	${PING_COMMAND}
	Should Contain	${OUTPUT}	${NUM_PACKETS} packets received

SUITE:Read Until
	[Arguments]	${PROMPT}	${LAST_COMMAND}=${EMPTY}
	Set Client Configuration	timeout=120s
	${OUTPUT}=	Read Until	${PROMPT}	loglevel=INFO
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Log To Console	${OUTPUT}
	${LIST}=	Create List	${PROMPT}	${LAST_COMMAND}
	${STATUS}=	Run Keyword And Return Status	CLI:Should Contain All	${OUTPUT}	${LIST}
	Run Keyword If	not ${STATUS}	SUITE:Should Fail	Some step during the setup-alpine failed
	[Return]	${OUTPUT}

SUITE:Get USB Devices
	[Documentation]	Checks the USBs present in the device and, if the device is valid, gets the correspondent USB port.
	...	If the kernel device is detected as "sd" it is replaced by "usb". If there are no valid devices it sets a flag
	...	to skip related testcases.
	CLI:Switch Connection	default
	CLI:Enter Path	/system/usb_devices
	${OUTPUT}	CLI:Show
	@{LINES}	Split To Lines	${OUTPUT}	2	-1
	@{VALID_DEVICES}	Create List
	${HAS_USB}	Set variable	${FALSE}
	FOR	${LINE}	IN	@{LINES}
		${IS_VALID_TYPE}	Run Keyword And Return Status	Should Contain Any	${LINE}	Storage	USB Sensor Device
		${IS_HD}	Run Keyword And Return Status	Should Contain	${LINE}	Mass Storage
		${IS_VALID}	Set Variable If	${IS_VALID_TYPE} and not ${IS_HD}	${TRUE}	${FALSE}
		${USB_PORT}	Get Regexp Matches	${LINE}	(usbS\\d-(\\w|\\d+)|usbS\\d|sdS\\d)
		${IS_SD}	Run Keyword And Return Status	Should Contain	${USB_PORT}[0]	sd
		IF	${IS_SD}
			${USB_PORT}	Replace String	${USB_PORT}[0]	sd	usb
		END
		Run Keyword If	${IS_VALID}	Append To List	${VALID_DEVICES}	${USB_PORT}
		IF	${IS_VALID}
			${HAS_USB}	Set Variable	${TRUE}
		END
	END
	Run Keyword if	${HAS_USB}	SUITE:Set USB	${VALID_DEVICES}
	IF	${HAS_USB}
		[Return]	${USB_PORT}
	ELSE
		${USB_DEVICES_EMPTY}	Set Variable	${TRUE}
		Set Suite Variable	${USB_DEVICES_EMPTY}
	END

SUITE:Set USB
	[Arguments]	${VALID_DEVICES}
	${USB_PORT}	Get From List	@{VALID_DEVICES}	0
	${USB_DEVICES_EMPTY}	Run Keyword And Return Status	Should Be Empty	${USB_PORT}
	Set Suite Variable	${USB_DEVICES_EMPTY}

SUITE:Get USB Entries
	[Documentation]	Checks the USBs present in the device and, if the device is valid, gets the correspondent USB entry.
	CLI:Switch Connection	default
	CLI:Enter Path	/system/usb_devices
	${OUTPUT}	CLI:Show
	@{LINES}	Split To Lines	${OUTPUT}	2	-1
	@{VALID_ENTRIES}	Create List
	FOR	${LINE}	IN	@{LINES}
		${IS_VALID_TYPE}	Run Keyword And Return Status	Should Contain Any	${LINE}	Storage	USB Sensor Device
		${IS_HD}	Run Keyword And Return Status	Should Contain	${LINE}	Mass Storage
		${IS_VALID}	Set Variable If	${IS_VALID_TYPE} and not ${IS_HD}	${TRUE}	${FALSE}
		@{USB_ENTRY}	Split String	${LINE}	separator=${SPACE}${SPACE}${SPACE}	max_split=1
		Run Keyword If	${IS_VALID}	Append To List	${VALID_ENTRIES}	${USB_ENTRY}[0]
	END
	[Return]	@{VALID_ENTRIES}

SUITE:Check USB Devices Mapped To VM Via lsusb Command
	[Documentation]	Checks using lsusb command inside the created VM to check if the USB device is mapped
	[Arguments]	${USB_PORT}	${VM}=${VM_NAME}	${EXPECTED}=${TRUE}
	CLI:Connect As Root	session_alias=lsusb_session
	Write	virsh console ${VM}
	Read Until	Escape character is ^] (Ctrl + ])
	Write Bare	\n
	${OUTPUT}	Read Until Regexp	(#)|(login:)
	${HAS_TO_LOGIN}	Run Keyword And Return Status	Should Contain	${OUTPUT}	login:
	IF	${HAS_TO_LOGIN}
		Write	root
		Read Until	assword:
		CLI:Write Bare	\n
	END
	${VM_USBS}	CLI:Write	lsusb
	CLI:Write	\x1D
	CLI:Close Current Connection
	${USABLE_USB}	Run Keyword And Return Status	Should Contain	${VM_USBS}	Bus 004 Device
	IF	${EXPECTED} and not ${USABLE_USB}
		Fail	USB port ${USB_PORT} was not mapped to Virtual Machine ${VM} when it should
	ELIF	not ${EXPECTED} and ${USABLE_USB}
		Fail	USB port ${USB_PORT} was mapped to Virtual Machine ${VM} when it should not
	END

SUITE:Check USB Devices Mapped To VM In system/usb_devices
	[Documentation]	Checks if the usb port is showing as mapped in tracking page inside NG
	[Arguments]	${USB_ENTRY}	${VM}=${VM_NAME}	${EXPECTED}=${TRUE}
	CLI:Switch Connection	default
	CLI:Enter Path	/system/usb_devices/
	CLI:Enter Path	${USB_ENTRY}
	${OUTPUT}	CLI:Show
	${MAPPED_TRACKING}	Run Keyword And Return Status	Should Contain	${OUTPUT}	mapped to virtual machine: ${VM_NAME}
	IF	${EXPECTED} and not ${MAPPED_TRACKING}
		Fail	USB port was not mapped to ${VM} when it should
	ELSE IF	not ${EXPECTED} and ${MAPPED_TRACKING}
		Fail	USB port was mapped to ${VM} when it should not
	END

SUITE:Check USB Devices Mapped To VM Using Virsh Commands
	[Documentation]	Checks if the usb port is showing as mapped using virsh commands
	[Arguments]	${USB_PORT}	${VM}=${VM_NAME}	${EXPECTED}=${TRUE}
	CLI:Switch Connection	default
	CLI:Connect As Root	session_alias=virsh_session
	${MAPPED_IN_VM}	CLI:Write	virsh qemu-monitor-command ${VM} --hmp 'info usb'
	Run Keyword If	${EXPECTED}	Should Contain	${MAPPED_IN_VM}	${USB_PORT}_autoattach
	CLI:Close Current Connection

SUITE:Map USB Device To A VM
	[Documentation]	Adds configuration to map VM inside USB port
	[Arguments]	${USB_PORT}	${VM}=${VM_NAME}
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/devices/${USB_PORT}/access
	CLI:Set	map_to_virtual_machine=yes virtual_machine_name=${VM_NAME}
	CLI:Commit
	${OUTPUT}	CLI:Show
	Should Contain	${OUTPUT}	virtual_machine_name = ${VM_NAME}

SUITE:Dettach USB Device From VM
	[Documentation]	Deletes configuration to map VM inside USB port
	[Arguments]	${USB_PORT}	${VM}=${VM_NAME}
	CLI:Enter Path	/settings/devices/${USB_PORT}/access
	CLI:Set	map_to_virtual_machine=no
	CLI:Commit
	${OUTPUT}	CLI:Show
	Should Not Contain	${OUTPUT}	${VM_NAME}