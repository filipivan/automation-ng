*** Settings ***
Resource	../../init.robot
Documentation	Tests about profiling nodegrid for mid/low hardware ends by desabling cpu/memory consuming services
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test if memory consuming services are in default value
	CLI:Switch Connection	default
	${ELASTICSEARCH}	CLI:Show	/settings/services enable_elasticsearch
	IF	'${NGVERSION}' <= '5.6'
		Should Contain	${ELASTICSEARCH}	enable_elasticsearch = yes
		Should Contain	${ELASTICSEARCH}	enable_kibana = yes
	ELSE
		Should Contain	${ELASTICSEARCH}	enable_search_engine = yes
		Should Contain	${ELASTICSEARCH}	enable_dashboards = yes
	END
	${TELEGRAF}	CLI:Show	/settings/services enable_telegraf
	Should Contain	${TELEGRAF}	enable_telegraf = no
	${VMWARE_MANAGER}	CLI:Show	/settings/services enable_vmware_manager
	Should Contain	${VMWARE_MANAGER}	enable_vmware_manager = yes

Test if Memory and CPU consumption is lower with services disabled
	CLI:Enter Path	/settings/services/
	CLI:Set	enable_elasticsearch=yes enable_kibana=yes enable_telegraf=yes enable_vmware_manager=yes
	CLI:Commit
	${USED_MEMORY_BEFORE}	${FREE_MEMORY_BEFORE}	SUITE:Get Memory Info
	${CPU_USAGE_BEFORE}	SUITE:Get Process That Uses Most CPU In Percentage
	Set Suite Variable	${USED_MEMORY_BEFORE}
	Set Suite Variable	${FREE_MEMORY_BEFORE}
	Set Suite Variable	${CPU_USAGE_BEFORE}
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/services/
	CLI:Set	enable_elasticsearch=no enable_telegraf=no enable_vmware_manager=no
	CLI:Commit
	Wait Until Keyword Succeeds	3x	30s	SUITE:Check If NG Is Consuming Less Memory And CPU

Test If auto_discovery/vm_managers path is hidden when service is disabled
	[Tags]	NON-CRITICAL	NEED-REVIEW
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/services
	CLI:Set	enable_vmware_manager=yes
	CLI:Commit
	CLI:Close Current Connection
	CLI:Open	#need to relogin for this changes to take effect - confirmed w/ Thais
	Set Client Configuration	timeout=60s
	CLI:Enter Path	/settings/auto_discovery/
	${VM_MANAGERS_PATH}	CLI:Ls
	Should Contain	${VM_MANAGERS_PATH}	vm_managers
	CLI:Enter Path	/settings/services
	CLI:Set	enable_vmware_manager=no
	CLI:Commit
	CLI:Close Current Connection
	CLI:Open	#need to relogin for this changes to take effect - confirmed w/ Thais
	Set Client Configuration	timeout=60s
	CLI:Enter Path	/settings/auto_discovery/
	${VM_MANAGERS_HIDDEN}	CLI:Ls
	Should Not Contain	${VM_MANAGERS_HIDDEN}	vm_managers
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT }

Test Processes Cannot Be Mannually Restarted Thru Shell/Root
	[Tags]	NON-CRITICAL	NEED-REVIEW
	CLI:Enter Path	/settings/services/
	CLI:Set	enable_elasticsearch=no enable_telegraf=no enable_vmware_manager=no
	CLI:Commit
	CLI:Connect As Root
	@{ELASTICSEARCH_PROCESSES}	Create List	elasticsearch	es_mngmt	es_update	es_restart.sh	fluentd
	FOR	${PROCESS}	IN	@{ELASTICSEARCH_PROCESSES}
		${ELASTICSEARCH_WARNING}	CLI:Write	/etc/init.d/${PROCESS} start
		Should Contain	${ELASTICSEARCH_WARNING}	Enable Elasticsearch checkbox first (security::services > enable_elasticsearch)
	END

	@{KIBANA_PROCESSES}	Create List	kibana	kibana_proxy
	FOR	${PROCESS}	IN	@{KIBANA_PROCESSES}
		${KIBANA_WARNING}	CLI:Write	/etc/init.d/${PROCESS} start
		Should Contain	${KIBANA_WARNING}	Enable Kibana checkbox first (security::services > enable_kibana)
	END

	${TELEGRAF_WARNING}	CLI:Write	/etc/init.d/telegraf start
	Should Contain	${TELEGRAF_WARNING}	Enable Telegraf checkbox first (security::services > enable_telegraf)

	${VCENTERD_WARNING}	CLI:Write	/etc/init.d/vCenterd start
	Should Contain	${VCENTERD_WARNING}	Enable VMware checkbox first (security::services > enable_vmware)

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Connect As Root

SUITE:Teardown
	Wait Until Keyword Succeeds	30s	10s	SUITE:Set Services Back To Default
	CLI:Close Connection

SUITE:Set Services Back To Default
	CLI:Open
	Set Client Configuration	timeout=60s
	CLI:Enter Path	/settings/services
	CLI:Set	enable_elasticsearch=yes enable_kibana=yes enable_vmware_manager=yes enable_telegraf=no
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}

SUITE:Get Memory Info
	CLI:Switch Connection	root_session
	${OUTPUT}	CLI:Write	free -m
	${OUTPUT}	Split To Lines	${OUTPUT}	1	-2
	${OUTPUT}	Remove String Using Regexp	${OUTPUT}[0]	Mem:\\s+\\d+\\s+
	${USED}	Get Substring	${OUTPUT}	0	4
	${OUTPUT}	Remove String Using Regexp	${OUTPUT}	${USED}\\s+
	${FREE}	Get Substring	${OUTPUT}	0	4
	Log To Console	Used memory: ${USED}
	Log To Console	Free memory: ${FREE}
	${USED}	Convert To Integer	${USED}
	${FREE}	Convert To Integer	${FREE}
	[Return]	${USED}	${FREE}

SUITE:Get Process That Uses Most CPU In Percentage
	CLI:Switch Connection	root_session
	${OUTPUT}	CLI:Write	ps -aux --sort=-%cpu
	${OUTPUT}	Split To Lines	${OUTPUT}	1	3
	${OUTPUT}	Remove String Using Regexp	${OUTPUT}[0]	(telegraf|admin|daemon|root)\\s+\\w+\\s+  #bug NG-7403
	${MOST_CPU}	Get Substring	${OUTPUT}	0	4
	Log To Console	The most CPU consuming process uses: ${MOST_CPU} %
	${MOST_CPU}	Convert To Number	${MOST_CPU}
	[Return]	${MOST_CPU}

SUITE:Check If NG Is Consuming Less Memory And CPU
	${USED_MEMORY_AFTER}	${FREE_MEMORY_AFTER}	SUITE:Get Memory Info
	${CPU_USAGE_AFTER}	SUITE:Get Process That Uses Most CPU In Percentage

	${DECREASE_USED_MEMORY}	Run Keyword And Return Status	Evaluate	${USED_MEMORY_BEFORE} > ${USED_MEMORY_AFTER}
	Should Be True	${DECREASE_USED_MEMORY}
	${INCREASE_FREE_MEMORY}	Run Keyword And Return Status	Evaluate	${FREE_MEMORY_BEFORE} < ${FREE_MEMORY_AFTER}
	Should Be True	${INCREASE_FREE_MEMORY}
	${DECREASE_CPU_USAGE}	Run Keyword And Return Status	Evaluate	${CPU_USAGE_BEFORE} > ${CPU_USAGE_AFTER}
	Should Be True	${DECREASE_CPU_USAGE}
