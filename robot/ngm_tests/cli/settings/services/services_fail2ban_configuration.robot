*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Services, configuration test cases to block host with multiple authentication
...	fails with ignorip option through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${WHITELISTED_IP_ADDRESSES_TEST}	10.0.0.1/16,255.255.255.0,fe80::1/84,2001:0db8:0000:0000:0000:8a2e:0370:7334,2001:db8::8a2e:370:7334

*** Test Cases ***
Test Enable/Disable Fail2ban With Default Configurations
	SUITE:Enable Fail2ban
	[Teardown]	SUITE:Disable Fail2ban

Test Enable/Disable Fail2ban With Other Configurations
	SUITE:Enable Fail2ban	PERIOD=100	NUMBER_FAILS=25	WHITELISTED_IP=${WHITELISTED_IP_ADDRESSES_TEST}	TIMEFRAME=3
	[Teardown]	SUITE:Disable Fail2ban	PERIOD=100	NUMBER_FAILS=25	WHITELISTED_IP=${WHITELISTED_IP_ADDRESSES_TEST}	TIMEFRAME=3

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	SUITE:Disable Fail2ban
	CLI:Close Connection

SUITE:Enable Fail2ban
	[Arguments]	${PERIOD}=10	${NUMBER_FAILS}=5	${WHITELISTED_IP}=${EMPTY}	${TIMEFRAME}=10
	CLI:Enter Path	/settings/services/
	CLI:Set	block_host_with_multiple_authentication_fails=yes
	CLI:Set	period_host_will_stay_blocked=${PERIOD} number_of_authentication_fails_to_block_host=${NUMBER_FAILS}
	CLI:Set	timeframe_to_monitor_authentication_fails=${TIMEFRAME}
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Set	whitelisted_ip_addresses=${WHITELISTED_IP}
	CLI:Commit
	CLI:Test Show Command	block_host_with_multiple_authentication_fails = yes	period_host_will_stay_blocked = ${PERIOD}
	...	number_of_authentication_fails_to_block_host = ${NUMBER_FAILS}	timeframe_to_monitor_authentication_fails = ${TIMEFRAME}
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Show Command	whitelisted_ip_addresses = ${WHITELISTED_IP}

SUITE:Disable Fail2ban
	[Arguments]	${PERIOD}=10	${NUMBER_FAILS}=5	${WHITELISTED_IP}=${EMPTY}	${TIMEFRAME}=10
	CLI:Enter Path	/settings/services/
	CLI:Set	block_host_with_multiple_authentication_fails=yes
	CLI:Set	period_host_will_stay_blocked=${PERIOD} number_of_authentication_fails_to_block_host=${NUMBER_FAILS}
	CLI:Set	timeframe_to_monitor_authentication_fails=${TIMEFRAME}
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Set	whitelisted_ip_addresses=${WHITELISTED_IP}
	CLI:Commit
	CLI:Set	block_host_with_multiple_authentication_fails=no
	CLI:Commit
	CLI:Test Show Command	block_host_with_multiple_authentication_fails = no
	CLI:Test Not Show Command	block_host_with_multiple_authentication_fails = yes	period_host_will_stay_blocked = ${PERIOD}
	...	number_of_authentication_fails_to_block_host = ${NUMBER_FAILS}	timeframe_to_monitor_authentication_fails = ${TIMEFRAME}
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Not Show Command	whitelisted_ip_addresses = ${WHITELISTED_IP}