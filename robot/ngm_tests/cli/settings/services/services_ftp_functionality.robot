*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Services FTP Commands... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test connecting with ftp with enable_ftp_service = no
	Run Keyword If	'${NGVERSION}' >= '4.2'	SUITE:Check Network Settings
	CLI:Enter Path	/settings/services/
	CLI:Set Field	enable_ftp_service	no
	CLI:Commit
	CLI:Test Show Command	enable_ftp_service = no

	CLI:Connect As Root
	Write	ftp ${HOST} 21
	${OUTPUT}=	Read Until	ftp>
	Should Contain	${OUTPUT}	ftp: connect to address ${HOST}: Connection refused
	Write	quit

Test connecting with ftp as admin
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/services/
	CLI:Set Field	enable_ftp_service	yes
	CLI:Commit
	CLI:Test Show Command	enable_ftp_service = yes

	#Login as admin
	CLI:Switch Connection	root_session
	Write	ftp ${HOST} 21
	${OUTPUT}=	Read Until	Name (${HOST}:root):
	#Should Contain	${OUTPUT}	220 nodegrid.localdomain FTP server (GNU inetutils 1.9.2) ready.
	Should Match Regexp	${OUTPUT}	FTP server \\(GNU inetutils \\d.\\d.\\d\\) ready\\.

	Write	${DEFAULT_USERNAME}
	Read Until	Password:
	Write	${DEFAULT_PASSWORD}
	${OUTPUT}=	Read Until	ftp>
	Should Not Contain	${OUTPUT}	ftp: Login failed.
	Should Contain	${OUTPUT}	User ${DEFAULT_USERNAME} logged in.
	Write	quit

	CLI:Switch Connection	default
	SUITE:Disable ftp service
	[Teardown]	Run Keywords	CLI:Switch Connection	root_session
	...	AND	CLI:Write	quit	Raw
	...	AND	SUITE:Disable ftp service

Test functionality of ftp
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/services/
	CLI:Set Field	enable_ftp_service	yes
	CLI:Commit
	CLI:Test Show Command	enable_ftp_service = yes

	#Login as admin
	CLI:Switch Connection	root_session
	Write	ftp ${HOST} 21
	${OUTPUT}=	Read Until	Name (${HOST}:root):
	#Should Contain	${OUTPUT}	220 nodegrid.localdomain FTP server (GNU inetutils 1.9.2) ready.
	Should Match Regexp	${OUTPUT}	FTP server \\(GNU inetutils \\d.\\d.\\d\\) ready\\.

	Write	${DEFAULT_USERNAME}
	Read Until	Password:
	Write	${DEFAULT_PASSWORD}
	${OUTPUT}=	Read Until	ftp>
	Should Not Contain	${OUTPUT}	ftp: Login failed.
	Write	cd /
	${OUTPUT}=	Read Until	ftp>
	Should Contain	${OUTPUT}	250 CWD command successful.
	Write	get software
	${OUTPUT}=	Read Until	ftp>
	Should Contain	${OUTPUT}	150 Opening BINARY mode data connection for 'software'
	Write	quit
	${OUTPUT}=	Read Until	~#
	Write	cat software
	${OUTPUT}=	Read Until	~#
	Should Contain	${OUTPUT}	FAMILY=ZPESystems NodeGrid
	Write	rm software
	${OUTPUT}=	Read Until	~#
	CLI:Switch Connection	default
	SUITE:Disable ftp service
	[Teardown]	Run Keywords	CLI:Switch Connection	root_session
	...	AND	CLI:Write	quit	Raw
	...	AND	SUITE:Disable ftp service

Test connecting with ftp as root
	#Should get login failure
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/services/
	CLI:Set Field	enable_ftp_service	yes
	CLI:Commit
	CLI:Test Show Command	enable_ftp_service = yes

	#Login as root
	CLI:Switch Connection	root_session
	Write	ftp ${HOST} 21
	${OUTPUT}=	Read Until	Name (${HOST}:root):
	#Should Contain	${OUTPUT}	220 nodegrid.localdomain FTP server (GNU inetutils 1.9.2) ready.
	Should Match Regexp	${OUTPUT}	FTP server \\(GNU inetutils \\d.\\d.\\d\\) ready\\.
	Write	root
	Read Until	Password:
	Write	root
	${OUTPUT}=	Read Until	ftp>
	Should Contain	${OUTPUT}	ftp: Login failed.
	${CHECK}=	Run Keyword And Return Status	Write	quit

	CLI:Switch Connection	default
	CLI:Enter Path	/settings/services/
	CLI:Set Field	enable_ftp_service	no
	CLI:Commit
	CLI:Test Show Command	enable_ftp_service = no
	[Teardown]	Run Keywords	CLI:Switch Connection	root_session
	...	AND	CLI:Write	quit	Raw
	...	AND	SUITE:Disable ftp service

Test connecting with ftp when authentication server is on
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/services/
	CLI:Set Field	enable_ftp_service	yes
	CLI:Commit
	CLI:Test Show Command	enable_ftp_service = yes

	#Adding authentication server
	CLI:Enter Path	/settings/authentication/servers
	CLI:Add
	CLI:Write	set method=tacacs+ status=enabled remote_server=${TACACS_SERVER} tacacs+_accounting_server=${TACACS_SERVER} fallback_if_denied_access=yes tacacs+_secret=secret tacacs+_service=raccess tacacs+_version=v0_v1
	CLI:Commit

	#Login as admin
	CLI:Switch Connection	root_session
	Write	ftp ${HOST} 21
	${OUTPUT}=	Read Until	Name (${HOST}:root):
	#Should Contain	${OUTPUT}	220 nodegrid.localdomain FTP server (GNU inetutils 1.9.2) ready.
	Should Match Regexp	${OUTPUT}	FTP server \\(GNU inetutils \\d.\\d.\\d\\) ready\\.
	
	Write	${DEFAULT_USERNAME}
	Read Until	Password:
	Write	${DEFAULT_PASSWORD}
	${OUTPUT}=	Read Until	ftp>
	Should Not Contain	${OUTPUT}	ftp: Login failed.
	Write	quit

	SUITE:Disable ftp service
	SUITE:Delete Authentication Server
	[Teardown]	Run Keywords	CLI:Switch Connection	root_session
	...	AND	CLI:Write	quit	Raw
	...	AND	SUITE:Disable ftp service
	...	AND	SUITE:Delete Authentication Server

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Disable ftp service
	SUITE:Delete Authentication Server

SUITE:Teardown
	CLI:Open
	SUITE:Delete Authentication Server
	SUITE:Disable ftp service
	CLI:Close Connection

SUITE:Disable ftp service
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/services/
	CLI:Set Field	enable_ftp_service	no
	CLI:Commit
	CLI:Test Show Command	enable_ftp_service = no

SUITE:Delete Authentication Server
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/authentication/servers
	${Output}=	CLI:Show
	${TACACS}=	Get Lines Containing String	${Output}	TACACS+	case_insensitive=True
	@{LINES}=	Split to Lines	${TACACS}
	FOR	${LINE}	IN	@{Lines}
		${OutputText}=	CLI:Show
		${TACACSLINES}=	Get Lines Containing String	${OutputText}	TACACS+	case_insensitive=True
		@{Liness}=	Split to Lines	${TACACSLINES}
		${Lines}=	Replace String Using Regexp	${Liness}[0]	\\s\\s*	--
		@{Details}=	Split String	${Lines}	--
		CLI:Delete	${Details}[1]
	END

SUITE:Check Network Settings
	#Precaution to check if hostname is nodegrid, and domain_name is localdomain
	CLI:Enter Path	/settings/network_settings
	${STATUS}=	Run Keyword And Return Status	CLI:Test Show Command	hostname = nodegrid	domain_name = localdomain
	Run Keyword If	${STATUS} == False	Run Keywords	CLI:Write	set hostname=nodegrid domain_name=localdomain	AND	CLI:Commit

