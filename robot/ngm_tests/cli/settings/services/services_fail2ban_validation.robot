*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Services, validation test cases to block host with multiple authentication
...	fails with ignorip option through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
@{ALL_VALUES}	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
	...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
	...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
	...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
	...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
	...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}

*** Test Cases ***
Test Show Command
	CLI:Enter Path	/settings/services/
	CLI:Test Show Command	block_host_with_multiple_authentication_fails = no

Test Set Field Options
	CLI:Enter Path	/settings/services/
	CLI:Test Set Field Options Raw	block_host_with_multiple_authentication_fails	yes	no

Test Allow Fail2ban To Show Fields
	CLI:Enter Path	/settings/services/
	CLI:Set	block_host_with_multiple_authentication_fails=yes
	CLI:Test Show Command	block_host_with_multiple_authentication_fails = yes	period_host_will_stay_blocked = 10
	...	number_of_authentication_fails_to_block_host = 5	timeframe_to_monitor_authentication_fails = 10
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Show Command	whitelisted_ip_addresses =
	[Teardown]	CLI:Revert	Raw

Test Valid Values For Field=block_host_with_multiple_authentication_fails
	CLI:Enter Path	/settings/services/
	CLI:Test Set Field Options	block_host_with_multiple_authentication_fails	yes	no
	[Teardown]	CLI:Revert	Raw

Test Invalid Values For Field=block_host_with_multiple_authentication_fails
	CLI:Enter Path	/settings/services/

	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	block_host_with_multiple_authentication_fails	${EMPTY}
	...	Error: Missing value: block_host_with_multiple_authentication_fails
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	block_host_with_multiple_authentication_fails	${EMPTY}
	...	Error: Missing value for parameter: block_host_with_multiple_authentication_fails

	FOR		${INVALID_VALUE}	IN	@{ALL_VALUES}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	block_host_with_multiple_authentication_fails	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	block_host_with_multiple_authentication_fails	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: block_host_with_multiple_authentication_fails
	END
	[Teardown]	CLI:Revert	Raw

Test Valid Values For Field=period_host_will_stay_blocked
	${VALID_PERIODS}=	Create List	${ONE_NUMBER}	${TWO_NUMBER}	${THREE_NUMBER}
	CLI:Enter Path	/settings/services/
	CLI:Set	block_host_with_multiple_authentication_fails=yes

	FOR		${VALID_PERIOD}	IN	@{VALID_PERIODS}
		CLI:Test Set Validate Normal Fields	period_host_will_stay_blocked	${VALID_PERIOD}
	END
	[Teardown]	CLI:Revert	Raw

Test Invalid Values For Field=period_host_will_stay_blocked
	${INVALID_INTERVALS}=	Create List	${POINTS}	${ONE_POINTS}	${TWO_POINTS}
	...	${THREE_POINTS}	${SEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}	${POINTS_AND_NUMBER}
	...	${POINTS_AND_WORD}	${EIGHT_POINTS}	${ELEVEN_NUMBER}
	CLI:Enter Path	/settings/services/
	CLI:Set	block_host_with_multiple_authentication_fails=yes

	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	block_host_with_multiple_authentication_fails	${EMPTY}
	...	Error: Missing value: block_host_with_multiple_authentication_fails
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	block_host_with_multiple_authentication_fails	${EMPTY}
	...	Error: Missing value for parameter: block_host_with_multiple_authentication_fails

	FOR		${INVALID_INTERVAL}	IN	@{INVALID_INTERVALS}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	block_host_with_multiple_authentication_fails	${INVALID_INTERVAL}
		...	Error: Invalid value: ${INVALID_INTERVAL}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	block_host_with_multiple_authentication_fails	${INVALID_INTERVAL}
		...	Error: Invalid value: ${INVALID_INTERVAL} for parameter: block_host_with_multiple_authentication_fails
	END
	[Teardown]	CLI:Revert	Raw

Test Valid Values For Field=number_of_authentication_fails_to_block_host
	${VALID_NUMBERS}=	Create List	${ONE_NUMBER}	${TWO_NUMBER}	${THREE_NUMBER}
	CLI:Enter Path	/settings/services/
	CLI:Set	block_host_with_multiple_authentication_fails=yes

	FOR		${VALID_NUMBER}	IN	@{VALID_NUMBERS}
		CLI:Test Set Validate Normal Fields	number_of_authentication_fails_to_block_host	${VALID_NUMBER}
	END
	[Teardown]	CLI:Revert	Raw

Test Invalid Values For Field=number_of_authentication_fails_to_block_host
	${INVALID_NUMBERS}=	Create List	${POINTS}	${ONE_POINTS}	${TWO_POINTS}
	...	${THREE_POINTS}	${SEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}	${POINTS_AND_NUMBER}
	...	${POINTS_AND_WORD}	${EIGHT_POINTS}	${EMPTY}
	CLI:Enter Path	/settings/services/
	CLI:Set	block_host_with_multiple_authentication_fails=yes

	CLI:Test Set Field Invalid Options	number_of_authentication_fails_to_block_host	${ELEVEN_NUMBER}
	...	Error: number_of_authentication_fails_to_block_host: Exceeded the maximum size for this field.

	FOR		${INVALID_NUMBER}	IN	@{INVALID_NUMBERS}
		CLI:Test Set Field Invalid Options	number_of_authentication_fails_to_block_host	${INVALID_NUMBER}
		...	Error: number_of_authentication_fails_to_block_host: Validation error.
	END
	[Teardown]	CLI:Revert	Raw

Test Valid Values For Field=whitelisted_ip_addresses
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	@{VALID_IPS}=	Create List	192.168.15.60	192.168.16.85	192.168.17.55	0.0.0.0	255.255.255.255
	CLI:Enter Path	/settings/services/
	CLI:Set	block_host_with_multiple_authentication_fails=yes

	FOR		${VALID_IP}	IN	@{VALID_IPS}
		CLI:Test Set Validate Normal Fields	whitelisted_ip_addresses	${VALID_IP}
	END
	[Teardown]	CLI:Revert	Raw

Test Invalid Values For Field=whitelisted_ip_addresses
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	@{INVALID_IPS}=	Create List	20.20	10.10.10	300.300.300.300	-1.-1.-1.-1	0,5.0,5.0,5.0,5	0.00.00.00
	...	@{ALL_VALUES}
	CLI:Enter Path	/settings/services/
	CLI:Set	block_host_with_multiple_authentication_fails=yes

	FOR		${INVALID_IP}	IN	@{INVALID_IPS}
		CLI:Test Set Field Invalid Options	whitelisted_ip_addresses	${INVALID_IP}
		...		Error: whitelisted_ip_addresses: Invalid IP address.
	END
	[Teardown]	CLI:Revert	Raw

Test Valid Values For Field=timeframe_to_monitor_authentication_fails
	${VALID_PERIODS}=	Create List	${ONE_NUMBER}	${TWO_NUMBER}	${THREE_NUMBER}
	CLI:Enter Path	/settings/services/
	CLI:Set	block_host_with_multiple_authentication_fails=yes

	FOR		${VALID_PERIOD}	IN	@{VALID_PERIODS}
		CLI:Test Set Validate Normal Fields	timeframe_to_monitor_authentication_fails	${VALID_PERIOD}
	END
	[Teardown]	CLI:Revert	Raw

Test Invalid Values For Field=timeframe_to_monitor_authentication_fails
	${INVALID_INTERVALS}=	Create List	${POINTS}	${ONE_POINTS}	${TWO_POINTS}
	...	${THREE_POINTS}	${SEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}	${POINTS_AND_NUMBER}
	...	${POINTS_AND_WORD}	${EIGHT_POINTS}	${EMPTY}
	CLI:Enter Path	/settings/services/
	CLI:Set	block_host_with_multiple_authentication_fails=yes

	FOR		${INVALID_INTERVAL}	IN	@{INVALID_INTERVALS}
		Run Keyword If	'${NGVERSION}' >= '3.2'	CLI:Test Set Field Invalid Options	timeframe_to_monitor_authentication_fails	${INVALID_INTERVAL}
		...	Error: timeframe_to_monitor_authentication_fails: Validation error.
	END
	[Teardown]	CLI:Revert	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	SUITE:Disable Fail2ban
	CLI:Close Connection

SUITE:Disable Fail2ban
	[Arguments]	${PERIOD}=10	${NUMBER_FAILS}=5	${WHITELISTED_IP}=${EMPTY}	${TIMEFRAME}=10
	CLI:Enter Path	/settings/services/
	CLI:Set	block_host_with_multiple_authentication_fails=yes
	CLI:Set	period_host_will_stay_blocked=${PERIOD} number_of_authentication_fails_to_block_host=${NUMBER_FAILS}
	CLI:Set	timeframe_to_monitor_authentication_fails=${TIMEFRAME}
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Set	whitelisted_ip_addresses=${WHITELISTED_IP}
	CLI:Commit
	CLI:Set	block_host_with_multiple_authentication_fails=no
	CLI:Commit
	CLI:Test Show Command	block_host_with_multiple_authentication_fails = no
	CLI:Test Not Show Command	block_host_with_multiple_authentication_fails = yes	period_host_will_stay_blocked = ${PERIOD}
	...	number_of_authentication_fails_to_block_host = ${NUMBER_FAILS}	timeframe_to_monitor_authentication_fails = ${TIMEFRAME}
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Not Show Command	whitelisted_ip_addresses = ${WHITELISTED_IP}