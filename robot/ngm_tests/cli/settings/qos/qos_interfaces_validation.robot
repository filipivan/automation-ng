*** Settings ***
Resource	../../init.robot
Documentation	Tests QoS Interfaces validation
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${INTERFACE}	eth0
${INPUT_BWIDTH}	10
${OUTPUT_BWIDTH}	10
${DUMMY_CLASS1}	test-class-dummy1
${DUMMY_CLASS2}	test-class-dummy2
${VALID_INTERFACE1}	1m22madea()-_-__.4391k3.4a
@{ALL_VALUES}	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}

*** Test Cases ***
Test Available Commands After Sending Tab-Tab
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Test Available Commands	enable	reboot	add	event_system_audit	revert
	CLI:Test Available Commands	add	commit	edit	exit	ls	save_settings
	...	software_upgrade	apply_settings	create_csr	enable	export_settings
	...	pwd	shell	system_certificate	cd	delete	event_system_audit
	...	factory_settings	quit	show	system_config_check	change_password
	...	diagnostic_data	event_system_clear	hostname	reboot	show_settings	whoami
	...	cloud_enrollment	disable	exec	import_settings	revert	shutdown

Test Visible Fields For Show Command
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Test Show Command Regexp	name\\s+status\\s+direction\\s+classes

Test Available Commands After Add Command
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Add
	CLI:Test Available Commands	cancel	commit	ls	save	set	show
	[Teardown]	CLI:Cancel	Raw

Test Available Fields After Add Command
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Add
	CLI:Test Set Available Fields	interface	qos_direction	enabled
	...	custom_parameters	classes	input_bandwidth	input_bandwidth_unit
	...	output_bandwidth	output_bandwidth_unit
	[Teardown]	CLI:Cancel	Raw

Test Valid Values For Field=interface
	${VALID_INTERFACES}=	Create List	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
	...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
	...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${VALID_INTERFACE1}

	FOR	${VALID_INTERFACE}	IN	@{VALID_INTERFACES}
		SUITE:Test Set Valid Value For Field	interface	${VALID_INTERFACE}
	END
	[Teardown]	SUITE:Cancel And Remove All Interfaces

Test Invalid Values For Field=interface
	CLI:Enter Path	/settings/qos/interfaces
	${INVALID_INTERFACES}=	Create List	${POINTS}	${ONE_POINTS}	${TWO_POINTS}
	...	${THREE_POINTS}	${SEVEN_POINTS}	${EIGHT_POINTS}	${ELEVEN_POINTS}
	...	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}	${POINTS_AND_NUMBER}
	...	${POINTS_AND_WORD}	${EMPTY}

	FOR		${INVALID_INTERFACE}	IN	@{INVALID_INTERFACES}
		SUITE:Test Set Invalid Value For Field	interface	${INVALID_INTERFACE}
		...	Error: interface: This field contains invalid characters.
	END
	[Teardown]	SUITE:Cancel And Remove All Interfaces

Test Valid Values For Field=qos_direction
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Add
	CLI:Test Set Field Options	qos_direction	bidirectional	input	output
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=qos_direction
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Add
	CLI:Set	interface=${INTERFACE}
	CLI:Set	input_bandwidth=${INPUT_BWIDTH} output_bandwidth=${OUTPUT_BWIDTH}
	CLI:Test Set Field Invalid Options	qos_direction	${EMPTY}
	...	Error: Missing value for parameter: qos_direction

	FOR		${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	qos_direction	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: qos_direction
	END
	[Teardown]	SUITE:Cancel And Remove All Interfaces

Test Valid Values For Field=enabled
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Add
	CLI:Test Set Field Options	enabled	no	yes
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=enabled
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Add
	CLI:Test Set Field Invalid Options	enabled	${EMPTY}
	...	Error: Missing value for parameter: enabled

	FOR		${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	enabled	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: enabled
	END
	[Teardown]	SUITE:Cancel And Remove All Interfaces

Test Valid Values For Field=custom_parameters
	${VALUES}=	Copy List	${ALL_VALUES}
	Append To List	${VALUES}	${EMPTY}

	FOR		${VALID_VALUE}	IN	@{VALUES}
		SUITE:Test Set Valid Value For Field	custom_parameters	${VALID_VALUE}
	END
	[Teardown]	SUITE:Cancel And Remove All Interfaces

Test Valid Values For Field=classes
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Add
	CLI:Test Set Validate Normal Fields	classes	${DUMMY_CLASS1}	${DUMMY_CLASS2}	${EMPTY}
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=classes
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Add
	FOR		${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	classes	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: classes
	END
	[Teardown]	SUITE:Cancel And Remove All Interfaces

Test Valid Values For Field=input_bandwidth
	${VALID_INPUT_BWIDTHS}=	Create List	${NUMBER}	${ONE_NUMBER}
	...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}

	FOR		${VALID_INPUT_BWIDTH}	IN	@{VALID_INPUT_BWIDTHS}
		SUITE:Test Set Valid Value For Field	input_bandwidth	${VALID_INPUT_BWIDTH}
	END
	[Teardown]	SUITE:Cancel And Remove All Interfaces

Test Invalid Values For Field=input_bandwidth
	${INVALID_BWIDTHS}=	Create List	${POINTS}	${ONE_POINTS}	${TWO_POINTS}
	...	${THREE_POINTS}	${SEVEN_POINTS}	${EIGHT_POINTS}
	...	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}	${POINTS_AND_NUMBER}
	...	${POINTS_AND_WORD}	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}
	${EXCEEDED_VALUES}=	Create List	${ELEVEN_NUMBER}	${ELEVEN_WORD}	${ELEVEN_POINTS}

	SUITE:Test Set Invalid Value For Field	input_bandwidth	${EMPTY}
	...	 Error: input_bandwidth: Field must not be empty.

	FOR		${INVALID_BWIDTH}	IN	@{INVALID_BWIDTHS}
		SUITE:Test Set Invalid Value For Field	input_bandwidth	${INVALID_BWIDTH}
		...	Error: input_bandwidth: Validation error.
	END
	FOR		${EXCEEDED_VALUE}	IN	@{EXCEEDED_VALUES}
		SUITE:Test Set Invalid Value For Field	input_bandwidth	${EXCEEDED_VALUE}
		...	Error: input_bandwidth: Exceeded the maximum size for this field.
	END
	[Teardown]	SUITE:Cancel And Remove All Interfaces

Test Valid Values For Field=input_bandwidth_unit
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Add
	CLI:Test Set Field Options	input_bandwidth_unit	bitps	bps	gbitps	gbps
	...	kbitps	kbps	mbitps	mbps
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=input_bandwidth_unit
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Add
	CLI:Set	interface=${INTERFACE}
	CLI:Set	input_bandwidth=${INPUT_BWIDTH} output_bandwidth=${OUTPUT_BWIDTH}
	CLI:Test Set Field Invalid Options	input_bandwidth_unit	${EMPTY}
	...	Error: Missing value for parameter: input_bandwidth_unit

	FOR		${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	input_bandwidth_unit	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: input_bandwidth_unit
	END
	[Teardown]	SUITE:Cancel And Remove All Interfaces

Test Valid Values For Field=output_bandwidth
	${VALID_OUTPUT_BWIDTHS}=	Create List	${NUMBER}	${ONE_NUMBER}
	...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}

	FOR		${VALID_OUTPUT_BWIDTH}	IN	@{VALID_OUTPUT_BWIDTHS}
		SUITE:Test Set Valid Value For Field	output_bandwidth	${VALID_OUTPUT_BWIDTH}
	END
	[Teardown]	SUITE:Cancel And Remove All Interfaces

Test Invalid Values For Field=output_bandwidth
	${INVALID_BWIDTHS}=	Create List	${POINTS}	${ONE_POINTS}	${TWO_POINTS}
	...	${THREE_POINTS}	${SEVEN_POINTS}	${EIGHT_POINTS}
	...	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}	${POINTS_AND_NUMBER}
	...	${POINTS_AND_WORD}	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}
	${EXCEEDED_VALUES}=	Create List	${ELEVEN_NUMBER}	${ELEVEN_WORD}	${ELEVEN_POINTS}

	SUITE:Test Set Invalid Value For Field	output_bandwidth	${EMPTY}
	...	Error: output_bandwidth: Field must not be empty.

	FOR		${INVALID_BWIDTH}	IN	@{INVALID_BWIDTHS}
		SUITE:Test Set Invalid Value For Field	output_bandwidth	${INVALID_BWIDTH}
		...	Error: output_bandwidth: Validation error.
	END
	FOR		${EXCEEDED_VALUE}	IN	@{EXCEEDED_VALUES}
		SUITE:Test Set Invalid Value For Field	output_bandwidth	${EXCEEDED_VALUE}
		...	Error: output_bandwidth: Exceeded the maximum size for this field.
	END
	[Teardown]	SUITE:Cancel And Remove All Interfaces

Test Valid Values For Field=output_bandwidth_unit
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Add
	CLI:Test Set Field Options	output_bandwidth_unit	bitps	bps	gbitps	gbps
	...	kbitps	kbps	mbitps	mbps
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=output_bandwidth_unit
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Add
	CLI:Set	interface=${INTERFACE}
	CLI:Set	input_bandwidth=${INPUT_BWIDTH} output_bandwidth=${OUTPUT_BWIDTH}
	CLI:Test Set Field Invalid Options	output_bandwidth_unit	${EMPTY}
	...	Error: Missing value for parameter: output_bandwidth_unit

	FOR		${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	output_bandwidth_unit	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: output_bandwidth_unit
	END
	[Teardown]	SUITE:Cancel And Remove All Interfaces

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Set Interfaces
	SUITE:Remove All Interfaces
	SUITE:Add Dummy Classes

SUITE:Teardown
	SUITE:Delete Dummy Classes
	CLI:Close Connection

SUITE:Set Interfaces
	${CONNECTIONS}=	CLI:Get Builtin Network Connections	Yes
	Set Suite Variable	${CONNECTIONS}

SUITE:Remove All Interfaces
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Delete All

SUITE:Cancel And Remove All Interfaces
	CLI:Cancel	Raw
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Delete All

SUITE:Test Set Valid Value For Field
	[Arguments]	${NAME}	${VALUE}
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Add
	Run Keyword If	"${NAME}" == "input_bandwidth"	Run Keywords
	...		CLI:Test Set Validate Normal Fields	interface	${INTERFACE}	AND
	...		CLI:Test Set Validate Normal Fields	input_bandwidth	${VALUE}	AND
	...		CLI:Test Set Validate Normal Fields	output_bandwidth	${OUTPUT_BWIDTH}
	...	ELSE IF	"${NAME}" == "output_bandwidth"	Run Keywords
	...		CLI:Test Set Validate Normal Fields	interface	${INTERFACE}	AND
	...		CLI:Test Set Validate Normal Fields	input_bandwidth	${INPUT_BWIDTH}	AND
	...		CLI:Test Set Validate Normal Fields	output_bandwidth	${VALUE}
	...	ELSE IF	"${NAME}" == "interface"	Run Keywords
	...		CLI:Test Set Validate Normal Fields	interface	${VALUE}	AND
	...		CLI:Test Set Validate Normal Fields	input_bandwidth	${INPUT_BWIDTH}	AND
	...		CLI:Test Set Validate Normal Fields	output_bandwidth	${OUTPUT_BWIDTH}
	...	ELSE	Run Keywords
	...		CLI:Test Set Validate Normal Fields	${NAME}	${VALUE}	AND
	...		CLI:Test Set Validate Normal Fields	interface	${INTERFACE}	AND
	...		CLI:Test Set Validate Normal Fields	input_bandwidth	${INPUT_BWIDTH}	AND
	...		CLI:Test Set Validate Normal Fields	output_bandwidth	${OUTPUT_BWIDTH}
	CLI:Save

	SUITE:Cancel And Remove All Interfaces

SUITE:Test Set Invalid Value For Field
	[Arguments]	${NAME}	${VALUE}	${ERROR}=${EMPTY}
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Add
	Run Keyword If	"${NAME}" == "input_bandwidth"	Run Keywords
	...		CLI:Test Set Validate Normal Fields	interface	${INTERFACE}	AND
	...		CLI:Test Set Validate Normal Fields	output_bandwidth	${OUTPUT_BWIDTH}	AND
	...		CLI:Test Set Field Invalid Options	input_bandwidth	${VALUE}
	...		${ERROR}	save=yes
	...	ELSE IF	"${NAME}" == "output_bandwidth"	Run Keywords
	...		CLI:Test Set Validate Normal Fields	interface	${INTERFACE}	AND
	...		CLI:Test Set Validate Normal Fields	input_bandwidth	${INPUT_BWIDTH}	AND
	...		CLI:Test Set Field Invalid Options	output_bandwidth	${VALUE}
	...		${ERROR}	save=yes
	...	ELSE IF	"${NAME}" == "interface"	Run Keywords
	...		CLI:Test Set Validate Normal Fields	input_bandwidth	${INPUT_BWIDTH}	AND
	...		CLI:Test Set Validate Normal Fields	output_bandwidth	${OUTPUT_BWIDTH}	AND
	...		CLI:Test Set Field Invalid Options	interface	${VALUE}
	...		${ERROR}	save=yes
	...	ELSE	Run Keywords
	...		CLI:Test Set Validate Normal Fields	interface	${INTERFACE}	AND
	...		CLI:Test Set Validate Normal Fields	input_bandwidth	${INPUT_BWIDTH}	AND
	...		CLI:Test Set Validate Normal Fields	output_bandwidth	${OUTPUT_BWIDTH}	AND
	...		CLI:Test Set Field Invalid Options	${NAME}	${VALUE}
	...		${ERROR}	save=yes

	CLI:Cancel	Raw

SUITE:Add Dummy Classes
	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	CLI:Set	name=${DUMMY_CLASS1}
	CLI:Save
	CLI:Add
	CLI:Set	name=${DUMMY_CLASS2}
	CLI:Save

SUITE:Delete Dummy Classes
	CLI:Enter Path	/settings/qos/classes
	CLI:Delete All