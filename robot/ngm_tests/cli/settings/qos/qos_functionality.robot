*** Settings ***
Resource	    ../../init.robot
Documentation	Tests QoS functionality and qos_settings.py script for reducing GSM traffic
Metadata	    Version	1.0
Metadata	    Executed At	${HOST}
Force Tags	    CLI    EXCLUDEIN3_2    EXCLUDEIN4_2	NON-CRITICAL	NEED-REVIEW
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${INTERFACE}	eth0
${RULE_NAME}	test-rule
${CLASS_NAME}	test-class
${INTERFACE_HIGH_VALUE}	10
${INTERFACE_HIGH_UNIT}	mbitps
${CLASS_HIGH_VALUE_MIN}	90
${CLASS_HIGH_VALUE_MAX}	100
${CLASS_HIGH_UNIT}	gbitps
${IPERF_SERVER_PORT}	5201
${IPERF_TIME_SEC}	20
${ACCEPTANCE_MARGIN}	0.15

${CELLULAR_CONNECTION}	${GSM_CELLULAR_CONNECTION}
${TYPE}	${GSM_TYPE}
${STRONG_PASSWORD}	${QA_PASSWORD}
${GSM_QOS_FILE}	qos_settings.py
${SPEED_TEST_SCRIPT}	speedtest.py
${SPEED_TEST_SCRIPT_URL}	https://raw.githubusercontent.com/sivel/speedtest-cli/master/${SPEED_TEST_SCRIPT}
${GSM_SPEED_THRESHOLD}	0.4

*** Test Cases ***
Test Class input bandwidth Kbitps Magnitude
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Open	${DEFAULT_USERNAME}	${STRONG_PASSWORD}	session_alias=default
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	${HAS_WMODEM}	${MODEM_INFO}=	CLI:Get Wireless Modems SIM Card Info	0
	Set Suite Variable	${MODEM_INFO}
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	CLI:Switch Connection	default
	SUITE:Add GSM Connection
	SUITE:Wait For GSM To Be Up
	SUITE:Get GSM Ipv4 On Host

	CLI:Connect As Root	PASSWORD=${STRONG_PASSWORD}
	SUITE:Download And Edit QoS Settings File

	SUITE:Edit Rule Source And Destination	${HOSTPEER}	${HOST}
	SUITE:Edit Interface Bandwidth
	...	${INTERFACE_HIGH_VALUE}	${INTERFACE_HIGH_VALUE}	${INTERFACE_HIGH_UNIT}
	SUITE:Edit Class Input Bandwidth	100	200	kbitps
	SUITE:Start Iperf Server	default
	${IPERF_OUTPUT}=	SUITE:Get Client Iperf Output	peer_session
	SUITE:Bandwidth Should Match Class QoS Settings	${IPERF_OUTPUT}
	...	kbitps	100	200
	[Teardown]	SUITE:Stop Iperf Server	root_host

Test Class input bandwidth Mbitps magnitude
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	SUITE:Edit Rule Source And Destination	${HOSTPEER}	${HOST}
	SUITE:Edit Interface Bandwidth
	...	${INTERFACE_HIGH_VALUE}	${INTERFACE_HIGH_VALUE}	${INTERFACE_HIGH_UNIT}
	SUITE:Edit Class Input Bandwidth	1	2	mbitps
	SUITE:Start Iperf Server	default
	${IPERF_OUTPUT}=	SUITE:Get Client Iperf Output	peer_session
	SUITE:Bandwidth Should Match Class QoS Settings	${IPERF_OUTPUT}
	...	mbitps	1	2
	[Teardown]	SUITE:Stop Iperf Server	root_host

Test Class output bandwidth Kbitps magnitude
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	SUITE:Edit Rule Source And Destination	${HOST}	${HOSTPEER}
	SUITE:Edit Interface Bandwidth
	...	${INTERFACE_HIGH_VALUE}	${INTERFACE_HIGH_VALUE}	${INTERFACE_HIGH_UNIT}
	SUITE:Edit Class Output Bandwidth	400	500	kbitps
	SUITE:Start Iperf Server	peer_session
	${IPERF_OUTPUT}=	SUITE:Get Client Iperf Output	default
	SUITE:Bandwidth Should Match Class QoS Settings	${IPERF_OUTPUT}
	...	kbitps	400	500
	[Teardown]	SUITE:Stop Iperf Server	root_peer

Test Class output bandwidth Mbitps magnitude
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	SUITE:Edit Rule Source And Destination	${HOST}	${HOSTPEER}
	SUITE:Edit Interface Bandwidth
	...	${INTERFACE_HIGH_VALUE}	${INTERFACE_HIGH_VALUE}	${INTERFACE_HIGH_UNIT}
	SUITE:Edit Class Output Bandwidth	1	2	mbitps
	SUITE:Start Iperf Server	peer_session
	${IPERF_OUTPUT}=	SUITE:Get Client Iperf Output	default
	SUITE:Bandwidth Should Match Class QoS Settings	${IPERF_OUTPUT}
	...	mbitps	1	2
	[Teardown]	SUITE:Stop Iperf Server	root_peer

Test Interface input bandwidth Kbitps
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	SUITE:Edit Rule Source And Destination	${HOSTPEER}	${HOST}
	SUITE:Edit Interface Bandwidth	350	950	kbitps
	SUITE:Edit Class Input Bandwidth
	...	${CLASS_HIGH_VALUE_MIN}	${CLASS_HIGH_VALUE_MAX}	${CLASS_HIGH_UNIT}
	SUITE:Start Iperf Server	default
	${IPERF_OUTPUT}=	SUITE:Get Client Iperf Output	peer_session
	SUITE:Bandwidth Should Match Interface QoS Settings	${IPERF_OUTPUT}
	...	kbitps	350
	[Teardown]	SUITE:Stop Iperf Server	root_host

Test Interface input bandwidth Mbitps
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	SUITE:Edit Rule Source And Destination	${HOSTPEER}	${HOST}
	SUITE:Edit Interface Bandwidth	2	10	mbitps
	SUITE:Edit Class Input Bandwidth
	...	${CLASS_HIGH_VALUE_MIN}	${CLASS_HIGH_VALUE_MAX}	${CLASS_HIGH_UNIT}
	SUITE:Start Iperf Server	default
	${IPERF_OUTPUT}=	SUITE:Get Client Iperf Output	peer_session
	SUITE:Bandwidth Should Match Interface QoS Settings	${IPERF_OUTPUT}
	...	mbitps	2
	[Teardown]	SUITE:Stop Iperf Server	root_host

Test Interface output bandwidth Kbitps
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	SUITE:Edit Rule Source And Destination	${HOST}	${HOSTPEER}
	SUITE:Edit Interface Bandwidth	900	250	kbitps
	SUITE:Edit Class Output Bandwidth
	...	${CLASS_HIGH_VALUE_MIN}	${CLASS_HIGH_VALUE_MAX}	${CLASS_HIGH_UNIT}
	SUITE:Start Iperf Server	peer_session
	${IPERF_OUTPUT}=	SUITE:Get Client Iperf Output	default
	SUITE:Bandwidth Should Match Interface QoS Settings	${IPERF_OUTPUT}
	...	kbitps	250
	[Teardown]	SUITE:Stop Iperf Server	root_peer

Test Interface output bandwidth Mbitps
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	SUITE:Edit Rule Source And Destination	${HOST}	${HOSTPEER}
	SUITE:Edit Interface Bandwidth	10	5	mbitps
	SUITE:Edit Class Output Bandwidth
	...	${CLASS_HIGH_VALUE_MIN}	${CLASS_HIGH_VALUE_MAX}	${CLASS_HIGH_UNIT}
	SUITE:Start Iperf Server	peer_session
	${IPERF_OUTPUT}=	SUITE:Get Client Iperf Output	default
	SUITE:Bandwidth Should Match Interface QoS Settings	${IPERF_OUTPUT}
	...	mbitps	5
	[Teardown]	SUITE:Stop Iperf Server	root_peer

Test add GSM QoS settings
	[Tags]	NON-CRITICAL	EXCLUDEIN3_2	EXCLUDEIN4_2
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	${DOWNLOAD_SPEED_BEFORE}=	SUITE:Get Download Speed	${GSM_IP_ADDRESS}
	Set Suite Variable	${DOWNLOAD_SPEED_BEFORE}
	SUITE:Delete All QoS Configuration
	SUITE:Add GSM QoS Settings
	SUITE:GSM QoS Settings Should Be Successfully Added

Test transfer file with GSM QoS settings applied
	[Tags]	NON-CRITICAL	EXCLUDEIN3_2	EXCLUDEIN4_2
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	${DOWNLOAD_SPEED_AFTER}=	SUITE:Get Download Speed	${GSM_IP_ADDRESS}
	SUITE:GSM Speed Should Be Reduced	${DOWNLOAD_SPEED_AFTER}	${DOWNLOAD_SPEED_BEFORE}

Test remove GSM QoS settings
	[Tags]	NON-CRITICAL	EXCLUDEIN3_2	EXCLUDEIN4_2
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	SUITE:Remove GSM QoS Settings
	SUITE:GSM QoS Settings Should Be Successfully Removed
	CLI:Close Connection

Test Show FireQoS Status in Tracking - eth0 Interface
	[Tags]	NON-CRITICAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	CLI:Open	PASSWORD=${STRONG_PASSWORD}	session_alias=eth0_session
	SUITE:Iperf3 On Source
	CLI:Open	root	root	HOST_DIFF=${HOSTPEER}
	SUITE:Iperf3 On Destination	${HOST}
	SUITE:Check Tracking	eth0
	CLI:Write	shell sudo killall iperf3
	CLI:Close Connection

Test Show FireQoS Status in Tracking - eth1 Interface
	[Tags]	NON-CRITICAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
	Skip If	not ${HAS_WMODEM_SUPPORT}	Wireless modem is not supported by this system
	Skip If	not ${HAS_WMODEM}	No wireless modem with SIM card available
	CLI:Open	PASSWORD=${STRONG_PASSWORD}	session_alias=eth0_session
	${SOURCE_IP}	${DESTINATION_IP}	SUITE:Check Eth1
	SUITE:Add Interface	eth1
	SUITE:Edit Class	${CLASS_NAME}	eth1
	SUITE:Iperf3 On Source
	CLI:Open	root	root	HOST_DIFF=${HOSTPEER}
	Run Keyword And Warn On Failure	SUITE:Iperf3 On Destination	${SOURCE_IP}	${DESTINATION_IP}
	Run Keyword And Warn On Failure	SUITE:Check Tracking	eth1
	CLI:Write	shell sudo killall iperf3
	CLI:Close Connection

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Connect As Root	session_alias=root_host
	CLI:Open	HOST_DIFF=${HOSTPEER}	session_alias=peer_session
	CLI:Connect As Root	HOST_DIFF=${HOSTPEER}	session_alias=root_peer
	SUITE:Delete All QoS Configuration
	SUITE:Setup QoS Pre Configurations

	${HAS_WMODEM_SUPPORT}=	CLI:Has Wireless Modem Support
	Set Suite Variable	${HAS_WMODEM_SUPPORT}
	${HAS_WMODEM}	Set Variable	${FALSE}
	Set Suite Variable	${HAS_WMODEM}

	CLI:Change User Password From Shell	root	${STRONG_PASSWORD}	${ROOT_PASSWORD}
	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${STRONG_PASSWORD}	${STRONG_PASSWORD}

SUITE:Teardown
	Run Keyword If	${HAS_WMODEM_SUPPORT} and ${HAS_WMODEM}	CLI:Open	PASSWORD=${STRONG_PASSWORD}
	Run Keyword If	${HAS_WMODEM_SUPPORT} and ${HAS_WMODEM}	CLI:Delete Network Connections	${CELLULAR_CONNECTION}
	Run Keyword If	${HAS_WMODEM_SUPPORT} and ${HAS_WMODEM}	SUITE:Delete All QoS Configuration
	Run Keyword If	${HAS_WMODEM_SUPPORT} and ${HAS_WMODEM}	CLI:Close Connection

	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}	${STRONG_PASSWORD}
	CLI:Change User Password From Shell	root	${ROOT_PASSWORD}	${STRONG_PASSWORD}
	CLI:Open
	Write	shell sudo killall iperf3
	Read Until Prompt
	CLI:Close Connection

SUITE:Start Iperf Server
	[ARGUMENTS]	${SESSION_ALIAS}
	${SERVER_ADDRESS}=	Set Variable If
	...	"${SESSION_ALIAS}" == "peer_session"	${HOSTPEER}
	...	"${SESSION_ALIAS}" == "default"	${HOST}
	CLI:Switch Connection	${SESSION_ALIAS}
	Set Client Configuration	prompt=#
	CLI:Write	shell sudo su -
	CLI:Write	iperf3 -s -B ${SERVER_ADDRESS} -p ${IPERF_SERVER_PORT}&
	CLI:Write Bare	\n

SUITE:Get Client Iperf Output
	[Arguments]	${SESSION_ALIAS}
	${SERVER_ADDRESS}=	Set Variable If
	...	"${SESSION_ALIAS}" == "peer_session"	${HOST}
	...	"${SESSION_ALIAS}" == "default"	${HOSTPEER}
	${CLIENT_ADDRESS}=	Set Variable If
	...	"${SESSION_ALIAS}" == "peer_session"	${HOSTPEER}
	...	"${SESSION_ALIAS}" == "default"	${HOST}

	CLI:Switch Connection	${SESSION_ALIAS}
	Set Client Configuration	prompt=#
	CLI:Write	shell sudo su -
	Set Client Configuration	timeout=60s
	${IPERF_OUTPUT}=	CLI:Write	iperf3 -c ${SERVER_ADDRESS} -p ${IPERF_SERVER_PORT} -t ${IPERF_TIME_SEC} -B ${CLIENT_ADDRESS}
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Should Contain	${IPERF_OUTPUT}	iperf Done.
	Set Client Configuration	prompt=]#
	CLI:Write	exit
	[Return]	${IPERF_OUTPUT}

SUITE:Stop Iperf Server
	[Arguments]	${SESSION_ALIAS}
	CLI:Switch Connection	${SESSION_ALIAS}
	CLI:Write	killall iperf3
	CLI:Switch Connection	default

SUITE:Bandwidth Should Match Class QoS Settings
	[Arguments]	${IPERF_OUTPUT}	${QOS_UNIT}	${MIN_BWIDTH}	${MAX_BWIDTH}
	${UNIT_MULTIPLIER}=	SUITE:Get Bandwidth Multiplier	${QOS_UNIT}
	${MIN_BWIDTH_VALUE}=	Evaluate	${MIN_BWIDTH} * ${UNIT_MULTIPLIER} * (1.0 - ${ACCEPTANCE_MARGIN})
	${MAX_BWIDTH_VALUE}=	Evaluate	${MAX_BWIDTH} * ${UNIT_MULTIPLIER} * (1.0 + ${ACCEPTANCE_MARGIN})

	${MATCH}	${MATCH_BWIDTH_VALUE}	${MATCH_UNIT}=	Should Match Regexp	${IPERF_OUTPUT}
	...	(\\d+\\.*\\d*) (\\w+)\\/sec\\s+receiver
	${MATCH_UNIT_MULTIPLIER}=	SUITE:Get Iperf Bandwidth Multiplier	${MATCH_UNIT}
	${BWIDTH_VALUE}=	Evaluate	${MATCH_BWIDTH_VALUE} * ${MATCH_UNIT_MULTIPLIER}

	Should Be True	${MIN_BWIDTH_VALUE} <= ${BWIDTH_VALUE} and ${BWIDTH_VALUE} <= ${MAX_BWIDTH_VALUE}

SUITE:Bandwidth Should Match Interface QoS Settings
	[Arguments]	${IPERF_OUTPUT}	${QOS_UNIT}	${MAX_BWIDTH}
	${UNIT_MULTIPLIER}=	SUITE:Get Bandwidth Multiplier	${QOS_UNIT}
	${MAX_BWIDTH_VALUE}=	Evaluate	${MAX_BWIDTH} * ${UNIT_MULTIPLIER} * (1.0 + ${ACCEPTANCE_MARGIN})

	${MATCH}	${MATCH_BWIDTH_VALUE}	${MATCH_UNIT}=	Should Match Regexp	${IPERF_OUTPUT}
	...	(\\d+\\.*\\d*) (\\w+)\\/sec\\s+receiver
	${MATCH_UNIT_MULTIPLIER}=	SUITE:Get Iperf Bandwidth Multiplier	${MATCH_UNIT}
	${BWIDTH_VALUE}=	Evaluate	${MATCH_BWIDTH_VALUE} * ${MATCH_UNIT_MULTIPLIER}

	Should Be True	${BWIDTH_VALUE} <= ${MAX_BWIDTH_VALUE}

SUITE:Get Bandwidth Multiplier
	[Arguments]	${UNIT}
	${UNIT_MULTIPLIER}=	Run Keyword If	"${UNIT}" == "kbitps"	Set Variable	1024
	...	ELSE IF	"${UNIT}" == "mbitps"	Set Variable	1048576	# 1024^2
	...	ELSE IF	"${UNIT}" == "gbitps"	Set Variable	1073741824	# 1024^3
	...	ELSE IF	"${UNIT}" == "kbps"	Set Variable	8192	# 1024 * 8
	...	ELSE IF	"${UNIT}" == "mbps"	Set Variable	8388608	# 1024^2 * 8
	...	ELSE IF	"${UNIT}" == "gbps"	Set Variable	8589934592	# 1024^3 * 8
	[Return]	${UNIT_MULTIPLIER}

SUITE:Get Iperf Bandwidth Multiplier
	[Arguments]	${UNIT}
	${UNIT_MULTIPLIER}=	Run Keyword If	"${UNIT}" == "Kbits"	Set Variable	1024
	...	ELSE IF	"${UNIT}" == "Mbits"	Set Variable	1048576	# 1024^2
	...	ELSE IF	"${UNIT}" == "Gbits"	Set Variable	1073741824	# 1024^3
	[Return]	${UNIT_MULTIPLIER}

SUITE:Edit Interface Bandwidth
	[Arguments]	${INPUT_BWIDTH}	${OUTPUT_BWIDTH}	${UNIT}
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Edit	${INTERFACE}
	CLI:Set	input_bandwidth=${INPUT_BWIDTH} output_bandwidth=${OUTPUT_BWIDTH}
	CLI:Set	input_bandwidth_unit=${UNIT} output_bandwidth_unit=${UNIT}
	CLI:Show
	CLI:Commit

SUITE:Edit Class Input Bandwidth
	[Arguments]	${INPUT_BWIDTH_MIN}	${INPUT_BWIDTH_MAX}	${UNIT}
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/qos/classes
	CLI:Edit	${CLASS_NAME}
	CLI:Set	input_reserved_bandwidth=${INPUT_BWIDTH_MIN} input_reserved_unit=${UNIT}
	CLI:Set	input_max_bandwidth=${INPUT_BWIDTH_MAX} input_max_unit=${UNIT}
	CLI:Show
	CLI:Commit

SUITE:Edit Class Output Bandwidth
	[Arguments]	${OUTPUT_BWIDTH_MIN}	${OUTPUT_BWIDTH_MAX}	${UNIT}
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/qos/classes
	CLI:Edit	${CLASS_NAME}
	CLI:Set	output_reserved_bandwidth=${OUTPUT_BWIDTH_MIN} output_reserved_unit=${UNIT}
	CLI:Set	output_max_bandwidth=${OUTPUT_BWIDTH_MAX} output_max_unit=${UNIT}
	CLI:Show
	CLI:Commit

SUITE:Edit Rule Source And Destination
	[Arguments]	${SOURCE_IP}	${DESTINATION_IP}
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/qos/rules
	CLI:Edit	${RULE_NAME}
	CLI:Set	source_ip=${SOURCE_IP} destination_ip=${DESTINATION_IP}
	CLI:Show
	CLI:Commit

SUITE:Setup QoS Pre Configurations
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/qos/rules
	CLI:Add
	CLI:Set	name=${RULE_NAME} ipv4=yes ipv6=no
	CLI:Commit

	CLI:Enter Path	/settings/qos/interfaces
	CLI:Add
	CLI:Set	interface=${INTERFACE} enabled=yes qos_direction=bidirectional
	CLI:Set	input_bandwidth=1 output_bandwidth=1
	CLI:Commit

	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	CLI:Set	name=${CLASS_NAME} enabled=yes interfaces=${INTERFACE} rules=${RULE_NAME}
	CLI:Set	priority=7
	CLI:Commit

SUITE:Delete All QoS Configuration
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/qos/classes
	CLI:Delete All
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Delete All
	CLI:Enter Path	/settings/qos/rules
	CLI:Delete All

SUITE:Add GSM Connection
	CLI:Enter Path	/settings/network_connections/
	CLI:Add
	CLI:Set	name=${CELLULAR_CONNECTION} type=${TYPE}
	CLI:Set	ethernet_interface=${MODEM_INFO["interface"]} ipv4_mode=dhcp
	CLI:Set	ipv6_mode=no_ipv6_address sim-1_access_point_name=${MODEM_INFO["apn"]}
	CLI:Commit

SUITE:Wait For GSM To Be Up
	CLI:Enter Path	/settings/network_connections/
	Wait Until Keyword Succeeds	30s	3s	CLI:Test Show Command Regexp	${CELLULAR_CONNECTION}.*(\\d+\\.*){4}

SUITE:Get GSM Ipv4 On Host
	${IP_ADDRESS}=	CLI:Get Interface IP Address	${MODEM_INFO["shell interface"]}
	Set Suite Variable	${GSM_IP_ADDRESS}	${IP_ADDRESS}

SUITE:Get Download Speed
	[Arguments]	${SOURCE_IP_ADDRESS}
	CLI:Switch Connection	root_session
	CLI:Write	rm /home/root/${SPEED_TEST_SCRIPT}
	CLI:Write	cd /home/root/
	${OUTPUT}=	CLI:Write	wget ${SPEED_TEST_SCRIPT_URL}
	Should Contain	${OUTPUT}	${SPEED_TEST_SCRIPT} saved
	${OUTPUT}=	CLI:Write	python3 /home/root/${SPEED_TEST_SCRIPT} --source ${SOURCE_IP_ADDRESS}
	${FULL_MATCH}	${MBITS_SPEED}=	Should Match Regexp	${OUTPUT}	Download: (.*) Mbit\\/s
	[Return]	${MBITS_SPEED}

SUITE:Download And Edit QoS Settings File
	CLI:Switch Connection	root_session
	CLI:Write	rm /home/root/${GSM_QOS_FILE}
	CLI:Write	cd /home/root/
	${OUTPUT}=	CLI:Write	wget --user=${FTPSERVER3_USER} --password=${FTPSERVER3_PASSWORD} ${FTPSERVER3_URL}/files/${GSM_QOS_FILE}
	Should Contain	${OUTPUT}	${GSM_QOS_FILE} saved
	CLI:Write	sed -Ei 's/^INTERFACE = ".*"/INTERFACE = "${MODEM_INFO["shell interface"]}"/g' /home/root/${GSM_QOS_FILE}
	CLI:Write	sed -Ei 's/^PASS = ".*"/PASS = "${STRONG_PASSWORD}"/g' /home/root/${GSM_QOS_FILE}
	CLI:Write	chmod +x /home/root/${GSM_QOS_FILE}

SUITE:Add GSM QoS Settings
	CLI:Switch Connection	root_session
	Set Client Configuration	timeout=60s
	${OUTPUT}=	CLI:Write	/home/root/${GSM_QOS_FILE} add
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Should Contain	${OUTPUT}	Finished successfully

SUITE:GSM QoS Settings Should Be Successfully Added
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/qos/rules
	CLI:Test Show Command	qos_settings_
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Test Show Command	${MODEM_INFO["shell interface"]}
	CLI:Enter Path	/settings/qos/classes
	CLI:Test Show Command	qos_settings_

SUITE:Remove GSM QoS Settings
	CLI:Switch Connection	root_session
	${OUTPUT}=	CLI:Write	/home/root/${GSM_QOS_FILE} rm
	Should Contain	${OUTPUT}	Finished successfully

SUITE:GSM QoS Settings Should Be Successfully Removed
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/qos/rules
	CLI:Test Not Show Command	qos_settings_
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Test Not Show Command	${MODEM_INFO["shell interface"]}
	CLI:Enter Path	/settings/qos/classes
	CLI:Test Not Show Command	qos_settings_

SUITE:GSM Speed Should Be Reduced
	[Arguments]	${SPEED_AFTER}	${SPEED_BEFORE}
	${SPEED_AFTER_FLOAT}=	Convert To Number	${SPEED_AFTER}
	${SPEED_BEFORE_FLOAT}=	Convert To Number	${SPEED_BEFORE}
	Should Be True	${SPEED_AFTER_FLOAT} < ${SPEED_BEFORE_FLOAT} * 0.4

SUITE:Add Interface
	[Arguments]	${INTERFACE}
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Add
	CLI:Set	interface=${INTERFACE} enabled=yes qos_direction=bidirectional
	CLI:Set	input_bandwidth=10 output_bandwidth=10 input_bandwidth_unit=mbitps output_bandwidth_unit=mbitps
	CLI:Commit

SUITE:Edit Class
	[Arguments]	${CLASSES}	${INTERFACE}
	CLI:Enter Path	/settings/qos/classes
	CLI:Edit	${CLASSES}
	CLI:Set	interfaces=${INTERFACE}
	CLI:Commit

SUITE:Iperf3 On Source
	Set Client Configuration	prompt=~#
	CLI:Write	shell sudo su -
	Write	iperf3 -s -p 5201
	Read Until	Server listening on 5201

SUITE:Iperf3 On Destination
	[Arguments]	${IP_SOURCE}	${IP_DESTINATION}=${EMPTY}
	Set Client Configuration	timeout=300
	${ETH}	Run Keyword And Return Status	Should Be Empty	${IP_DESTINATION}
	${OUTPUT}	Run Keyword If	${ETH}	CLI:Write	iperf3 -c ${IP_SOURCE} -p 5201 -t 20
	...	ELSE	CLI:Write	iperf3 -c ${IP_SOURCE} -4 -p 5201 -t 5 -i 1 --bind ${IP_DESTINATION}
	Should Contain	${OUTPUT}	iperf Done.
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Close Connection

SUITE:Check Tracking
	[Arguments]	${INTERFACE}	${HOST_DIFF}=${HOST}
	CLI:Open	HOST_DIFF=${HOST_DIFF}	PASSWORD=${STRONG_PASSWORD}
	CLI:Enter Path	/system/qos/
	${OUTPUT}	CLI:Show	user=Yes
	${TRASH}	${OUTPUT}	Split String From Right	${OUTPUT}	=	1	#removes the header
	Run Keyword If	'${INTERFACE}'=='eth0'	Should Match Regexp	${OUTPUT}	(\\s)*${INTERFACE}(\\s)+input(\\s)+${CLASS_NAME}(\\s)+(0*[1-9][0-9]*(\\.[0-9]+)?|0+\\.[0-9]*[1-9][0-9]*)(\\s)+(MB|KB)(\\s)+(([0-9]*)(\\s)*)+${INTERFACE}(\\s)+output(\\s)+${CLASS_NAME}(\\s)+(0*[1-9][0-9]*(\\.[0-9]+)?|0+\\.[0-9]*[1-9][0-9]*)(\\s)+(MB|KB)(\\s)+(.)*
	...	ELSE	Should Match Regexp	${OUTPUT}	(.)*${INTERFACE}(\\s)+input(\\s)+${CLASS_NAME}(\\s)+(0*[1-9][0-9]*(\\.[0-9]+)?|0+\\.[0-9]*[1-9][0-9]*)(\\s)+(MB|KB)(\\s)+(([0-9]*)(\\s)*)+${INTERFACE}(\\s)+output(\\s)+${CLASS_NAME}(\\s)+(0*[1-9][0-9]*(\\.[0-9]+)?|0+\\.[0-9]*[1-9][0-9]*)(\\s)+(MB|KB)(\\s)+(.)*

SUITE:Check Eth1
	${HAS_ETH1}	CLI:Has Interface	eth1
	Skip If	not ${HAS_ETH1}	Host Device doesn't have eth1
	${HOST_ETH1_IP}	CLI:Get Interface IP Address	eth1
	${HAS_ETH1_IP}	Run Keyword And Return Status	Should Match Regexp	${HOST_ETH1_IP}	192.168.[0-9][0-9]?[0-9]?.[0-9][0-9]?[0-9]?
	Skip If	not ${HAS_ETH1_IP}	Host's Eth1 doesn't have IP address
	CLI:Open	HOST_DIFF=${HOSTPEER}
	${HAS_ETH1}	CLI:Has Interface	eth1
	Skip If	not ${HAS_ETH1}	Peer Device doesn't have eth1
	${PEER_ETH1_IP}	CLI:Get Interface IP Address	eth1
	${HAS_ETH1_IP}	Run Keyword And Return Status	Should Match Regexp	${PEER_ETH1_IP}	192.168.[0-9][0-9]?[0-9]?.[0-9][0-9]?[0-9]?
	Skip If	not ${HAS_ETH1_IP}	Peer's Eth1 doesn't have IP address
	CLI:Switch Connection	eth0_session
	[Return]	${HOST_ETH1_IP}	${PEER_ETH1_IP}