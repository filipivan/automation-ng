*** Settings ***
Resource	../../init.robot
Documentation	Tests QoS configuration
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${RULE_NAME}	test-rule

${INTERFACE}	eth0
${INTERFACE_DIRECTION1}	bidirectional
${INTERFACE_DIRECTION2}	output
${INTERFACE_INPUT_BWIDTH}	10
${INTERFACE_OUTPUT_BWIDTH}	10

${CLASS_NAME}	test-class
${CLASS_PRIORITY1}	0
${CLASS_PRIORITY2}	7
${CLASS_UNIT1}	%
${CLASS_UNIT2}	mbps
${CLASS_INPUT_RESERVED1}	30
${CLASS_INPUT_MAX1}	40
${CLASS_OUTPUT_RESERVED1}	38
${CLASS_OUTPUT_MAX1}	69
${CLASS_INPUT_RESERVED2}	100
${CLASS_INPUT_MAX2}	500
${CLASS_OUTPUT_RESERVED2}	80
${CLASS_OUTPUT_MAX2}	250


*** Test Cases ***
Test Add QoS Rule
	CLI:Enter Path	/settings/qos/rules
	CLI:Add
	CLI:Set	name=${RULE_NAME} enabled=no
	CLI:Commit
	CLI:Test Show Command Regexp	${RULE_NAME}\\s{2,}no
	[Teardown]  CLI:Cancel	Raw

Test Add QoS Interface
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Add
	CLI:Set	interface=${INTERFACE} enabled=no qos_direction=${INTERFACE_DIRECTION1}
	CLI:Set	input_bandwidth=${INTERFACE_INPUT_BWIDTH} output_bandwidth=${INTERFACE_OUTPUT_BWIDTH}
	CLI:Commit
	CLI:Test Show Command Regexp
	...	${INTERFACE}\\s{2,}Disabled\\s{2,}${INTERFACE_DIRECTION1}
	[Teardown]  CLI:Cancel	Raw

Test Add QoS Class
	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	CLI:Set	name=${CLASS_NAME} enabled=no input_reserved_unit=${CLASS_UNIT1}
	CLI:Set	input_max_unit=${CLASS_UNIT1} output_reserved_unit=${CLASS_UNIT1}
	CLI:Set	output_max_unit=${CLASS_UNIT1} input_reserved_bandwidth=${CLASS_INPUT_RESERVED1}
	CLI:Set	input_max_bandwidth=${CLASS_INPUT_MAX1} output_reserved_bandwidth=${CLASS_OUTPUT_RESERVED1}
	CLI:Set	output_max_bandwidth=${CLASS_OUTPUT_MAX1} priority=${CLASS_PRIORITY1}
	CLI:Commit
	SUITE:Match QoS Classes Table	${CLASS_NAME}	no	${CLASS_PRIORITY1}
	...	${CLASS_UNIT1}	${CLASS_INPUT_RESERVED1}	${CLASS_INPUT_MAX1}
	...	${CLASS_OUTPUT_RESERVED1}	${CLASS_OUTPUT_MAX1}
	[Teardown]  CLI:Cancel	Raw

Test Edit QoS Rule
	CLI:Enter Path	/settings/qos/rules
	CLI:Edit	${RULE_NAME}
	CLI:Set	enabled=yes syn=yes ack=no classes=${CLASS_NAME}
	CLI:Commit
	CLI:Test Show Command Regexp	${RULE_NAME}\\s{2,}yes
	[Teardown]  CLI:Cancel	Raw

Test Edit QoS Interface
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Edit	${INTERFACE}
	CLI:Set	enabled=yes qos_direction=${INTERFACE_DIRECTION2}
	CLI:Set	input_bandwidth=${INTERFACE_INPUT_BWIDTH} output_bandwidth=${INTERFACE_OUTPUT_BWIDTH}
	CLI:Set	classes=${CLASS_NAME}
	CLI:Commit
	CLI:Test Show Command Regexp
	...	${INTERFACE}\\s{2,}Running\\s{2,}${INTERFACE_DIRECTION2}
	[Teardown]  CLI:Cancel	Raw

Test Edit QoS Class
	CLI:Enter Path	/settings/qos/classes
	CLI:Edit	${CLASS_NAME}
	CLI:Set	enabled=yes input_reserved_unit=${CLASS_UNIT2}
	CLI:Set	input_max_unit=${CLASS_UNIT2} output_reserved_unit=${CLASS_UNIT2}
	CLI:Set	output_max_unit=${CLASS_UNIT2} input_reserved_bandwidth=${CLASS_INPUT_RESERVED2}
	CLI:Set	input_max_bandwidth=${CLASS_INPUT_MAX2} output_reserved_bandwidth=${CLASS_OUTPUT_RESERVED2}
	CLI:Set	output_max_bandwidth=${CLASS_OUTPUT_MAX2} priority=${CLASS_PRIORITY2}
	CLI:Set	rules=${RULE_NAME} interfaces=${INTERFACE}
	CLI:Commit
	SUITE:Match QoS Classes Table	${CLASS_NAME}	yes	${CLASS_PRIORITY2}
	...	(${CLASS_UNIT2}|MB\\/s)	${CLASS_INPUT_RESERVED2}	${CLASS_INPUT_MAX2}
	...	${CLASS_OUTPUT_RESERVED2}	${CLASS_OUTPUT_MAX2}
	[Teardown]  CLI:Cancel	Raw

Test Disable QoS Rule
	CLI:Enter Path	/settings/qos/rules
	CLI:Write	disable ${RULE_NAME}
	CLI:Test Show Command Regexp	${RULE_NAME}\\s{2,}no
	[Teardown]  CLI:Revert	Raw

Test Disable QoS Interface
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Write	disable ${INTERFACE}
	CLI:Test Show Command Regexp
	...	${INTERFACE}\\s{2,}Disabled\\s{2,}${INTERFACE_DIRECTION2}
	[Teardown]  CLI:Revert	Raw

Test Disable QoS Class
	CLI:Enter Path	/settings/qos/classes
	CLI:Write	disable ${CLASS_NAME}
	SUITE:Match QoS Classes Table	${CLASS_NAME}	no	${CLASS_PRIORITY2}
	...	(${CLASS_UNIT2}|MB\\/s)	${CLASS_INPUT_RESERVED2}	${CLASS_INPUT_MAX2}
	...	${CLASS_OUTPUT_RESERVED2}	${CLASS_OUTPUT_MAX2}
	[Teardown]  CLI:Revert	Raw

Test Enable QoS Rule
	CLI:Enter Path	/settings/qos/rules
	CLI:Write	enable ${RULE_NAME}
	CLI:Test Show Command Regexp	${RULE_NAME}\\s{2,}yes
	[Teardown]  CLI:Revert	Raw

Test Enable QoS Interface
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Write	disable ${INTERFACE}
	CLI:Test Show Command Regexp
	...	${INTERFACE}\\s{2,}Disabled\\s{2,}${INTERFACE_DIRECTION2}
	[Teardown]  CLI:Revert	Raw

Test Enable QoS Class
	CLI:Enter Path	/settings/qos/classes
	CLI:Write	enable ${CLASS_NAME}
	SUITE:Match QoS Classes Table	${CLASS_NAME}	yes	${CLASS_PRIORITY2}
	...	(${CLASS_UNIT2}|MB\\/s)	${CLASS_INPUT_RESERVED2}	${CLASS_INPUT_MAX2}
	...	${CLASS_OUTPUT_RESERVED2}	${CLASS_OUTPUT_MAX2}
	[Teardown]  CLI:Revert	Raw

Test Add Duplicated Name QoS Rule
	CLI:Enter Path	/settings/qos/rules
	CLI:Add
	CLI:Test Set Field Invalid Options	name	${RULE_NAME}
	...	Error: name: A rule with that name already exists.	save=yes
	[Teardown]  CLI:Cancel	Raw

Test Add Duplicated Name QoS Interface
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Add
	CLI:Set	input_bandwidth=${INTERFACE_INPUT_BWIDTH} output_bandwidth=${INTERFACE_OUTPUT_BWIDTH}
	CLI:Test Set Field Invalid Options	interface	${INTERFACE}
	...	Error: interface: An interface with that name already exists.	save=yes
	[Teardown]  CLI:Cancel	Raw

Test Add Duplicated Name QoS Class
	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	CLI:Test Set Field Invalid Options	name	${CLASS_NAME}
	...	Error: name: A class with that name already exists.	save=yes
	[Teardown]  CLI:Cancel	Raw

Test Delete QoS Rule
	CLI:Enter Path	/settings/qos/rules
	CLI:Delete	${RULE_NAME}
	[Teardown]  CLI:Revert	Raw

Test Delete QoS Interface
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Delete	${INTERFACE}
	[Teardown]  CLI:Revert	Raw

Test Delete QoS Class
	CLI:Enter Path	/settings/qos/classes
	CLI:Delete	${CLASS_NAME}
	[Teardown]  CLI:Revert	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Delete All QoS Configuration

SUITE:Teardown
	SUITE:Delete All QoS Configuration
	CLI:Close Connection

SUITE:Match QoS Classes Table
	[Arguments]	${NAME}	${ENABLED}	${PRIORITY}	${UNIT}	${INPUT_RESERVED}
	...	${INPUT_MAX}	${OUTPUT_RESERVED}	${OUTPUT_MAX}
	CLI:Test Show Command Regexp
	...	${NAME}\\s{2,}${ENABLED}\\s{2,}${PRIORITY}\\s{2,}${INPUT_RESERVED}${UNIT}\\s{2,}${INPUT_MAX}${UNIT}\\s{2,}${OUTPUT_RESERVED}${UNIT}\\s{2,}${OUTPUT_MAX}${UNIT}

SUITE:Delete All QoS Configuration
	CLI:Enter Path	/settings/qos/classes
	CLI:Delete All
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Delete All
	CLI:Enter Path	/settings/qos/rules
	CLI:Delete All