*** Settings ***
Resource	../../init.robot
Documentation	Tests QoS Classes validation
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CLASS_NAME}	test-class
${VALID_CLASSE_NAME1}	-._cl4((SS_n4)M3-1S.Val1d)
${VALID_CLASSE_NAME2}	1Cl4SS_n4)M3-1S.Val1(d_.-
${DUMMY_RULE1}	test-rule-dummy1
${DUMMY_RULE2}	test-rule-dummy2
${DUMMY_INTERFACE1}	test-interface-dummy1
${DUMMY_INTERFACE2}	test-interface-dummy2
${DUMMY_INTERFACE_BWIDTH}	10
@{ALL_VALUES}	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}


*** Test Cases ***
Test Available Commands After Sending Tab-Tab
	CLI:Enter Path	/settings/qos/classes
	CLI:Test Available Commands	add	event_system_audit	revert	apply_settings
	...	event_system_clear	save_settings	cd	exec	shell	change_password
	...	exit	show	cloud_enrollment	export_settings	show_settings	commit
	...	factory_settings	shutdown	create_csr	hostname	software_upgrade
	...	delete	import_settings	system_certificate	diagnostic_data	ls
	...	system_config_check	disable	pwd	whoami	edit	quit	enable	reboot

Test Visible Fields For Show Command
	CLI:Enter Path	/settings/qos/classes
	CLI:Test Show Command Regexp
	...	name\\s+enabled\\s+priority\\s+input reserved\\s+input max\\s+output reserved\\s+output max

Test Available Commands After Add Command
	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	CLI:Test Available Commands	cancel	commit	ls	save	set	show
	[Teardown]	CLI:Cancel	Raw

Test Available Fields After Add Command
	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	CLI:Test Set Available Fields	name	enabled	priority	custom_parameters
	...	rules	interfaces	input_reserved_bandwidth	input_reserved_unit
	...	input_max_bandwidth	input_max_unit	output_reserved_bandwidth
	...	output_reserved_unit	output_max_bandwidth	output_max_unit
	[Teardown]	CLI:Cancel	Raw

Test Valid Values For Field=name
	${VALID_NAMES}=	Create List	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
	...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
	...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${VALID_CLASSE_NAME1}
	...	${VALID_CLASSE_NAME2}

	FOR		${VALID_NAME}	IN	@{VALID_NAMES}
		SUITE:Test Set Valid Value For Field	name	${VALID_NAME}
	END
	[Teardown]	SUITE:Cancel And Remove All Classes

Test Invalid Values For Field=name
	${INVALID_NAMES}=	Create List	${POINTS}	${ONE_POINTS}	${TWO_POINTS}
	...	${THREE_POINTS}	${SEVEN_POINTS}	${EIGHT_POINTS}	${ELEVEN_POINTS}
	...	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}	${POINTS_AND_NUMBER}
	...	${POINTS_AND_WORD}	${EMPTY}

	FOR		${INVALID_NAME}	IN	@{INVALID_NAMES}
		SUITE:Test Set Invalid Value For Field	name	${INVALID_NAME}
		...	Error: name: This field contains invalid characters.
	END
	[Teardown]	SUITE:Cancel And Remove All Classes

Test Valid Values For Field=enabled
	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	CLI:Test Set Field Options	enabled	no	yes
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=enabled
	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	CLI:Test Set Field Invalid Options	enabled	${EMPTY}
	...	Error: Missing value for parameter: enabled

	FOR		${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	enabled	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: enabled
	END
	[Teardown]	SUITE:Cancel And Remove All Classes

Test Valid Values For Field=priority
	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	CLI:Test Set Field Options	priority	0	1	2	3	4	5	6	7
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=priority
	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	CLI:Test Set Field Invalid Options	enabled	${EMPTY}
	...	Error: Missing value for parameter: enabled

	FOR		${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	enabled	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: enabled
	END
	[Teardown]	SUITE:Cancel And Remove All Classes

Test Valid Values For Field=custom_parameters
	${VALUES}=	Copy List	${ALL_VALUES}
	Append To List	${VALUES}	${EMPTY}

	FOR		${VALID_VALUE}	IN	@{VALUES}
		SUITE:Test Set Valid Value For Field	custom_parameters	${VALID_VALUE}
	END
	[Teardown]	SUITE:Cancel And Remove All Classes

Test Valid Values For Field=rules
	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	CLI:Test Set Field Options	rules	${DUMMY_RULE1}	${DUMMY_RULE2}
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=rules
	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	FOR		${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	rules	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: rules
	END
	[Teardown]	SUITE:Cancel And Remove All Classes

Test Valid Values For Field=interfaces
	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	CLI:Test Set Field Options	interfaces	${DUMMY_INTERFACE1}	${DUMMY_INTERFACE2}
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=interfaces
	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	FOR		${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	interfaces	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: interfaces
	END
	[Teardown]	SUITE:Cancel And Remove All Classes

Test Valid Values For Field=input_reserved_bandwidth
	${VALID_BWIDTHS}=	Create List	${NUMBER}	${ONE_NUMBER}	${TWO_NUMBER}
	...	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}	${EMPTY}

	FOR		${VALID_BWIDTH}	IN	@{VALID_BWIDTHS}
		SUITE:Test Set Valid Value For Field	input_reserved_bandwidth	${VALID_BWIDTH}
	END
	[Teardown]	SUITE:Cancel And Remove All Classes

Test Invalid Values For Field=input_reserved_bandwidth
	${INVALID_BWIDTHS}=	Create List	${POINTS}	${ONE_POINTS}	${TWO_POINTS}
	...	${THREE_POINTS}	${SEVEN_POINTS}	${EIGHT_POINTS}
	...	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}	${POINTS_AND_NUMBER}
	...	${POINTS_AND_WORD}	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}
	${EXCEEDED_VALUES}=	Create List	${ELEVEN_WORD}	${ELEVEN_POINTS}	${ELEVEN_NUMBER}

	FOR		${INVALID_BWIDTH}	IN	@{INVALID_BWIDTHS}
		SUITE:Test Set Invalid Value For Field	input_reserved_bandwidth	${INVALID_BWIDTH}
		...	Error: input_reserved_bandwidth: Validation error.
	END
	FOR		${EXCEEDED_VALUE}	IN	@{EXCEEDED_VALUES}
		SUITE:Test Set Invalid Value For Field	input_reserved_bandwidth	${EXCEEDED_VALUE}
		...	Error: input_reserved_bandwidth: Exceeded the maximum size for this field.
	END
	[Teardown]	SUITE:Cancel And Remove All Classes

Test Valid Values For Field=input_reserved_unit
	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	CLI:Test Set Field Options	input_reserved_unit
	...	%	bitps	bps	gbitps	gbps	kbitps	kbps	mbitps	mbps
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=input_reserved_unit
	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	CLI:Test Set Field Invalid Options	input_reserved_unit	${EMPTY}
	...	Error: Missing value for parameter: input_reserved_unit

	FOR		${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	input_reserved_unit	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: input_reserved_unit
	END
	[Teardown]	SUITE:Cancel And Remove All Classes

Test Valid Values For Field=input_max_bandwidth
	${VALID_BWIDTHS}=	Create List	${NUMBER}	${ONE_NUMBER}	${TWO_NUMBER}
	...	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}	${EMPTY}

	FOR		${VALID_BWIDTH}	IN	@{VALID_BWIDTHS}
		SUITE:Test Set Valid Value For Field	input_max_bandwidth	${VALID_BWIDTH}
	END
	[Teardown]	SUITE:Cancel And Remove All Classes

Test Invalid Values For Field=input_max_bandwidth
	${INVALID_BWIDTHS}=	Create List	${POINTS}	${ONE_POINTS}	${TWO_POINTS}
	...	${THREE_POINTS}	${SEVEN_POINTS}	${EIGHT_POINTS}
	...	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}	${POINTS_AND_NUMBER}
	...	${POINTS_AND_WORD}	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}
	${EXCEEDED_VALUES}=	Create List	${ELEVEN_WORD}	${ELEVEN_POINTS}	${ELEVEN_NUMBER}

	FOR		${INVALID_BWIDTH}	IN	@{INVALID_BWIDTHS}
		SUITE:Test Set Invalid Value For Field	input_max_bandwidth	${INVALID_BWIDTH}
		...	Error: input_max_bandwidth: Validation error.
	END
	FOR		${EXCEEDED_VALUE}	IN	@{EXCEEDED_VALUES}
		SUITE:Test Set Invalid Value For Field	input_max_bandwidth	${EXCEEDED_VALUE}
		...	Error: input_max_bandwidth: Exceeded the maximum size for this field.
	END
	[Teardown]	SUITE:Cancel And Remove All Classes

Test Valid Values For Field=input_max_unit
	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	CLI:Test Set Field Options	input_max_unit
	...	%	bitps	bps	gbitps	gbps	kbitps	kbps	mbitps	mbps
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=input_max_unit
	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	CLI:Test Set Field Invalid Options	input_max_unit	${EMPTY}
	...	Error: Missing value for parameter: input_max_unit

	FOR		${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	input_max_unit	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: input_max_unit
	END
	[Teardown]	SUITE:Cancel And Remove All Classes

Test Valid Values For Field=output_reserved_bandwidth
	${VALID_BWIDTHS}=	Create List	${NUMBER}	${ONE_NUMBER}	${TWO_NUMBER}
	...	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}	${EMPTY}

	FOR		${VALID_BWIDTH}	IN	@{VALID_BWIDTHS}
		SUITE:Test Set Valid Value For Field	output_reserved_bandwidth	${VALID_BWIDTH}
	END
	[Teardown]	SUITE:Cancel And Remove All Classes

Test Invalid Values For Field=output_reserved_bandwidth
	${INVALID_BWIDTHS}=	Create List	${POINTS}	${ONE_POINTS}	${TWO_POINTS}
	...	${THREE_POINTS}	${SEVEN_POINTS}	${EIGHT_POINTS}
	...	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}	${POINTS_AND_NUMBER}
	...	${POINTS_AND_WORD}	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}
	${EXCEEDED_VALUES}=	Create List	${ELEVEN_WORD}	${ELEVEN_POINTS}	${ELEVEN_NUMBER}

	FOR		${INVALID_BWIDTH}	IN	@{INVALID_BWIDTHS}
		SUITE:Test Set Invalid Value For Field	output_reserved_bandwidth	${INVALID_BWIDTH}
		...	Error: output_reserved_bandwidth: Validation error.
	END
	FOR		${EXCEEDED_VALUE}	IN	@{EXCEEDED_VALUES}
		SUITE:Test Set Invalid Value For Field	output_reserved_bandwidth	${EXCEEDED_VALUE}
		...	Error: output_reserved_bandwidth: Exceeded the maximum size for this field.
	END
	[Teardown]	SUITE:Cancel And Remove All Classes

Test Valid Values For Field=output_reserved_unit
	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	CLI:Test Set Field Options	output_reserved_unit
	...	%	bitps	bps	gbitps	gbps	kbitps	kbps	mbitps	mbps
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=output_reserved_unit
	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	CLI:Test Set Field Invalid Options	output_reserved_unit	${EMPTY}
	...	Error: Missing value for parameter: output_reserved_unit

	FOR		${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	output_reserved_unit	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: output_reserved_unit
	END
	[Teardown]	SUITE:Cancel And Remove All Classes

Test Valid Values For Field=output_max_bandwidth
	${VALID_BWIDTHS}=	Create List	${NUMBER}	${ONE_NUMBER}	${TWO_NUMBER}
	...	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}	${EMPTY}

	FOR		${VALID_BWIDTH}	IN	@{VALID_BWIDTHS}
		SUITE:Test Set Valid Value For Field	output_max_bandwidth	${VALID_BWIDTH}
	END
	[Teardown]	SUITE:Cancel And Remove All Classes

Test Invalid Values For Field=output_max_bandwidth
	${INVALID_BWIDTHS}=	Create List	${POINTS}	${ONE_POINTS}	${TWO_POINTS}
	...	${THREE_POINTS}	${SEVEN_POINTS}	${EIGHT_POINTS}
	...	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}	${POINTS_AND_NUMBER}
	...	${POINTS_AND_WORD}	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}
	${EXCEEDED_VALUES}=	Create List	${ELEVEN_WORD}	${ELEVEN_POINTS}	${ELEVEN_NUMBER}

	FOR		${INVALID_BWIDTH}	IN	@{INVALID_BWIDTHS}
		SUITE:Test Set Invalid Value For Field	output_max_bandwidth	${INVALID_BWIDTH}
		...	Error: output_max_bandwidth: Validation error.
	END
	FOR		${EXCEEDED_VALUE}	IN	@{EXCEEDED_VALUES}
		SUITE:Test Set Invalid Value For Field	output_max_bandwidth	${EXCEEDED_VALUE}
		...	Error: output_max_bandwidth: Exceeded the maximum size for this field.
	END
	[Teardown]	SUITE:Cancel And Remove All Classes

Test Valid Values For Field3=output_max_unit
	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	CLI:Test Set Field Options	output_max_unit
	...	%	bitps	bps	gbitps	gbps	kbitps	kbps	mbitps	mbps
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=output_max_unit
	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	CLI:Test Set Field Invalid Options	output_max_unit	${EMPTY}
	...	Error: Missing value for parameter: output_max_unit

	FOR		${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	output_max_unit	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: output_max_unit
	END
	[Teardown]	SUITE:Cancel And Remove All Classes

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Remove All Classes
	SUITE:Delete Dummy Interfaces And Classses
	SUITE:Add Dummy Rules And Interfaces

SUITE:Teardown
	SUITE:Delete Dummy Interfaces And Classses
	CLI:Close Connection

SUITE:Remove All Classes
	CLI:Enter Path	/settings/qos/classes
	CLI:Delete All

SUITE:Cancel And Remove All Classes
	CLI:Cancel	Raw
	SUITE:Remove All Classes

SUITE:Test Set Valid Value For Field
	[Arguments]	${FIELD}	${VALUE}
	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	Run Keyword If	"${FIELD}" == "name"
	...		CLI:Test Set Validate Normal Fields	name	${VALUE}
	...	ELSE	Run Keywords
	...		CLI:Test Set Validate Normal Fields	name	${CLASS_NAME}	AND
	...		CLI:Test Set Validate Normal Fields	${FIELD}	${VALUE}
	CLI:Save

	SUITE:Cancel And Remove All Classes

SUITE:Test Set Invalid Value For Field
	[Arguments]	${FIELD}	${VALUE}	${ERROR}=${EMPTY}
	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	Run Keyword If	"${FIELD}" == "name"
	...		CLI:Test Set Field Invalid Options	name	${VALUE}
	...		${ERROR}	save=yes
	...	ELSE	Run Keywords
	...		CLI:Test Set Validate Normal Fields	name	${CLASS_NAME}	AND
	...		CLI:Test Set Field Invalid Options	${FIELD}	${VALUE}
	...		${ERROR}	save=yes

	CLI:Cancel	Raw

SUITE:Add Dummy Rules And Interfaces
	CLI:Enter Path	/settings/qos/rules
	CLI:Add
	CLI:Set	name=${DUMMY_RULE1}
	CLI:Save
	CLI:Add
	CLI:Set	name=${DUMMY_RULE2}
	CLI:Save

	CLI:Enter Path	/settings/qos/interfaces
	CLI:Add
	CLI:Set	interface=${DUMMY_INTERFACE1}
	CLI:Set	input_bandwidth=${DUMMY_INTERFACE_BWIDTH} output_bandwidth=${DUMMY_INTERFACE_BWIDTH}
	CLI:Save
	CLI:Add
	CLI:Set	interface=${DUMMY_INTERFACE2}
	CLI:Set	input_bandwidth=${DUMMY_INTERFACE_BWIDTH} output_bandwidth=${DUMMY_INTERFACE_BWIDTH}
	CLI:Save

SUITE:Delete Dummy Interfaces And Classses
	CLI:Enter Path	/settings/qos/rules
	CLI:Delete All
	CLI:Enter Path	/settings/qos/interfaces
	CLI:Delete All