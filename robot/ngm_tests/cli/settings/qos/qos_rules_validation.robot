*** Settings ***
Resource	../../init.robot
Documentation	Tests QoS Rules validation
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL	#TODO: remove non-critical tag. It was added just to make it pass for 5.6 release once feature works but test is failing
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown


*** Variables ***
${RULE_NAME}	test-rule
${DUMMY_CLASS1}	test-class-dummy1
${DUMMY_CLASS2}	test-class-dummy2
${VALID_RULE_NAME1}	_((0--0-rule-is-valid-02_)03..04)
@{ALL_VALUES}	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}

*** Test Cases ***
Test Available Commands After Sending Tab-Tab
	CLI:Enter Path	/settings/qos/rules
	CLI:Test Available Commands	enable	reboot	add	event_system_audit	revert
	...	apply_settings	event_system_clear	save_settings	cd	exec	shell
	...	change_password	exit	show	cloud_enrollment	export_settings
	...	show_settings	commit	factory_settings	shutdown	create_csr
	...	hostname	software_upgrade	delete	import_settings	system_certificate
	...	diagnostic_data	ls	system_config_check	disable	pwd	whoami	edit	quit

Test Visible Fields For Show Command
	CLI:Enter Path	/settings/qos/rules
	CLI:Test Show Command Regexp	name\\s+enabled

Test Available Commands After Add Command
	CLI:Enter Path	/settings/qos/rules
	CLI:Add
	CLI:Test Available Commands	cancel	commit	ls	save	set	show
	[Teardown]	CLI:Cancel	Raw

Test Available Fields After Add Command
	CLI:Enter Path	/settings/qos/rules
	CLI:Add
	CLI:Test Set Available Fields	name	enabled	ipv4	ipv6	protocol
	...	source_ip	destination_ip	source_port	destination_port	source_mac
	...	destination_mac	custom_parameters	tcp_flags	syn	ack	classes
	[Teardown]	CLI:Cancel	Raw

Test Valid Values For Field=name
	${VALID_NAMES}=	Create List	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
	...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
	...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}
	...	${VALID_RULE_NAME1}

	FOR		${VALID_NAME}	IN	@{VALID_NAMES}
		SUITE:Test Set Valid Value For Field	name	${VALID_NAME}
	END
	[Teardown]	SUITE:Cancel And Remove All Rules

Test Invalid Values For Field=name
	${INVALID_NAMES}=	Create List	${POINTS}	${ONE_POINTS}	${TWO_POINTS}
	...	${THREE_POINTS}	${SEVEN_POINTS}	${EIGHT_POINTS}	${ELEVEN_POINTS}
	...	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}	${POINTS_AND_NUMBER}
	...	${POINTS_AND_WORD}	${EMPTY}

	FOR		${INVALID_NAME}	IN	@{INVALID_NAMES}
		SUITE:Test Set Invalid Value For Field	name	${INVALID_NAME}
		...	Error: name: This field contains invalid characters.
	END
	[Teardown]	SUITE:Cancel And Remove All Rules

Test Valid Values For Field=enabled
	CLI:Enter Path	/settings/qos/rules
	CLI:Add
	CLI:Test Set Field Options	enabled	no	yes
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=enabled
	CLI:Enter Path	/settings/qos/rules
	CLI:Add
	CLI:Test Set Field Invalid Options	enabled	${EMPTY}
	...	Error: Missing value for parameter: enabled

	FOR		${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	enabled	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: enabled
	END
	[Teardown]	SUITE:Cancel And Remove All Rules

Test Valid Values For Field=ipv4
	CLI:Enter Path	/settings/qos/rules
	CLI:Add
	CLI:Test Set Field Options	ipv4	no	yes
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=ipv4
	CLI:Enter Path	/settings/qos/rules
	CLI:Add
	CLI:Test Set Field Invalid Options	ipv4	${EMPTY}
	...	Error: Missing value for parameter: ipv4

	FOR		${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	ipv4	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: ipv4
	END
	[Teardown]	SUITE:Cancel And Remove All Rules

Test Valid Values For Field=ipv6
	CLI:Enter Path	/settings/qos/rules
	CLI:Add
	CLI:Test Set Field Options	ipv6	no	yes
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=ipv6
	CLI:Enter Path	/settings/qos/rules
	CLI:Add
	CLI:Test Set Field Invalid Options	ipv6	${EMPTY}
	...	Error: Missing value for parameter: ipv6

	FOR		${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	ipv6	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: ipv6
	END
	[Teardown]	SUITE:Cancel And Remove All Rules

Test Valid Values For Field=protocol
	${VALID_VALUES}=	Create List	ip	hopopt	icmp	igmp	ggp	ipencap	st
	...	tcp	egp	igp	pup	udp	hmp	xns-idp	rdp	iso-tp4	dccp	xtp	ddp	idpr-cmtp
	...	ipv6	ipv6-route	ipv6-frag	idrp	rsvp	gre	esp	ah	skip
	...	ipv6-icmp	ipv6-nonxt	ipv6-opts	rspf	vmtp	eigrp	ospf
	...	ax.25	ipip	etherip	encap	pim	ipcomp	vrrp	l2tp	isis
	...	sctp	fc	mobility-header	udplite	mpls-in-ip	manet	hip	shim6
	...	wesp	rohc	${ONE_NUMBER}	${TWO_NUMBER}
	...	${THREE_NUMBER}	${EMPTY}
	Run Keyword If	${NGVERSION} == '5.0'	Append To List	${VALID_VALUES}	${NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}	${ELEVEN_NUMBER}
	...	ELSE	Append To List	${VALID_VALUES}	255	0
	CLI:Enter Path	/settings/qos/rules

	FOR		${VALID_VALUE}	IN	@{VALID_VALUES}
		CLI:Add
		CLI:Set	name=${RULE_NAME}
		CLI:Test Set Validate Normal Fields	protocol	${VALID_VALUE}
		CLI:Save
		SUITE:Cancel And Remove All Rules
	END
	[Teardown]	SUITE:Cancel And Remove All Rules

Test Invalid Values For Field=protocol
	${INVALID_VALUES}=	Create List	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${WORD_AND_NUMBER}
	...	${NUMBER_AND_WORD}	${POINTS}
	...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
	...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
	...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}
	Run Keyword If	${NGVERSION} >= 5.2	Append To List	${INVALID_VALUES}	256	-1
	CLI:Enter Path	/settings/qos/rules

	FOR		${INVALID_VALUE}	IN	@{INVALID_VALUES}
		CLI:Add
		CLI:Set	name=${RULE_NAME}
		CLI:Test Set Field Invalid Options	protocol	${INVALID_VALUE}
		...	Error: protocol: Invalid protocol. Valid values: a protocol name, "all" or a number [0-255] (comma separated)	save=yes
		CLI:Cancel	Raw
	END
	[Teardown]	SUITE:Cancel And Remove All Rules

Test Valid Values For Field=source_ip
	${VALID_VALUES}=	Create List	192.168.30.40	10.10.10.20	255.255.255.255
	...	0.0.0.0	127.0.0.123	${EMPTY}
	CLI:Enter Path	/settings/qos/rules

	FOR		${VALID_VALUE}	IN	@{VALID_VALUES}
		CLI:Add
		CLI:Set	name=${RULE_NAME}
		CLI:Test Set Validate Normal Fields	source_ip	${VALID_VALUE}
		CLI:Save
		SUITE:Cancel And Remove All Rules
	END
	[Teardown]	SUITE:Cancel And Remove All Rules

Test Invalid Values For Field=source_ip
	${INVALID_VALUES}=	Copy List	${ALL_VALUES}
	Append To List	${INVALID_VALUES}	255.255.255.255.255	255.255.255.256	0.0.0.0.0
	...	123.123.123	10.20
	CLI:Enter Path	/settings/qos/rules

	FOR		${INVALID_VALUE}	IN	@{INVALID_VALUES}
		CLI:Add
		CLI:Set	name=${RULE_NAME}
		CLI:Test Set Field Invalid Options	source_ip	${INVALID_VALUE}
		...	Error: source_ip: Validation error.	save=yes
		CLI:Cancel	Raw
	END
	[Teardown]	SUITE:Cancel And Remove All Rules

Test Valid Values For Field=destination_ip
	${VALID_VALUES}=	Create List	192.168.30.40	10.10.10.20	255.255.255.255
	...	0.0.0.0	127.0.0.123	${EMPTY}
	CLI:Enter Path	/settings/qos/rules

	FOR		${VALID_VALUE}	IN	@{VALID_VALUES}
		CLI:Add
		CLI:Set	name=${RULE_NAME}
		CLI:Test Set Validate Normal Fields	destination_ip	${VALID_VALUE}
		CLI:Save
		SUITE:Cancel And Remove All Rules
	END
	[Teardown]	SUITE:Cancel And Remove All Rules

Test Invalid Values For Field=destination_ip
	${INVALID_VALUES}=	Copy List	${ALL_VALUES}
	Append To List	${INVALID_VALUES}	255.255.255.255.255	255.255.255.256	0.0.0.0.0
	...	123.123.123	10.20
	CLI:Enter Path	/settings/qos/rules

	FOR		${INVALID_VALUE}	IN	@{INVALID_VALUES}
		CLI:Add
		CLI:Set	name=${RULE_NAME}
		CLI:Test Set Field Invalid Options	destination_ip	${INVALID_VALUE}
		...	Error: destination_ip: Validation error.	save=yes
		CLI:Cancel	Raw
	END
	[Teardown]	SUITE:Cancel And Remove All Rules

Test Valid Values For Field=source_port
	${VALID_VALUES}=	Create List	1	65535	2,65535	500:1000	1000:2000,1002,1004:1005
	CLI:Enter Path	/settings/qos/rules

	FOR		${VALID_VALUE}	IN	@{VALID_VALUES}
		CLI:Add
		CLI:Set	name=${RULE_NAME}
		CLI:Test Set Validate Normal Fields	source_port	${VALID_VALUE}
		CLI:Save
		SUITE:Cancel And Remove All Rules
	END
	[Teardown]	SUITE:Cancel And Remove All Rules

Test Invalid Values For Field=source_port
	${INVALID_VALUES}=	Create List	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	65536	-13	0	0:65535
	...	1,65536	1,2,65536	1,-2,12345	12345,-14
	...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
	...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
	...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
	...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}
	CLI:Enter Path	/settings/qos/rules

	FOR		${INVALID_VALUE}	IN	@{INVALID_VALUES}
		CLI:Add
		CLI:Set	name=${RULE_NAME}
		CLI:Test Set Field Invalid Options	source_port	${INVALID_VALUE}
		...	Error: source_port: Validation error.	save=yes
		CLI:Cancel	Raw
	END
	[Teardown]	SUITE:Cancel And Remove All Rules

Test Valid Values For Field=destination_port
	${VALID_VALUES}=	Create List	1	65535	2,65535	500:1000	1000:2000,1002,1004:1005
	CLI:Enter Path	/settings/qos/rules

	FOR		${VALID_VALUE}	IN	@{VALID_VALUES}
		CLI:Add
		CLI:Set	name=${RULE_NAME}
		CLI:Test Set Validate Normal Fields	destination_port	${VALID_VALUE}
		CLI:Save
		SUITE:Cancel And Remove All Rules
	END
	[Teardown]	SUITE:Cancel And Remove All Rules

Test Invalid Values For Field=destination_port
	${INVALID_VALUES}=	Create List	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
	...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	65536	-13	0	0-65535
	...	1,65536	1,2,65536	1,-2,12345	12345,-14
	...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
	...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
	...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
	...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}
	CLI:Enter Path	/settings/qos/rules

	FOR		${INVALID_VALUE}	IN	@{INVALID_VALUES}
		CLI:Add
		CLI:Set	name=${RULE_NAME}
		CLI:Test Set Field Invalid Options	destination_port	${INVALID_VALUE}
		...	Error: destination_port: Validation error.	save=yes
		CLI:Cancel	Raw
	END
	[Teardown]	SUITE:Cancel And Remove All Rules

Test Valid Values For Field=source_mac
	${VALID_VALUES}=	Create List	aa:aa:aA:Aa:AA:aa	00:00:00:00:00:00	ff:fF:FF:Ff:fF:FF

	CLI:Enter Path	/settings/qos/rules
	FOR		${VALID_VALUE}	IN	@{VALID_VALUES}
		CLI:Add
		CLI:Set	name=${RULE_NAME}
		CLI:Test Set Validate Normal Fields	source_mac	${VALID_VALUE}
		CLI:Save
		SUITE:Cancel And Remove All Rules
	END
	[Teardown]	SUITE:Cancel And Remove All Rules

Test Invalid Values For Field=source_mac
	${INVALID_VALUES}=	Copy List	${ALL_VALUES}
	Append To List	${INVALID_VALUES}	GG:gg:GG:gg:gg:GG	XX:xx:XX:XX:XX:XX	00::00:00:00:00
	CLI:Enter Path	/settings/qos/rules

	FOR		${INVALID_VALUE}	IN	@{INVALID_VALUES}
		CLI:Add
		CLI:Set	name=${RULE_NAME}
		CLI:Test Set Field Invalid Options	source_mac	${INVALID_VALUE}
		...	Error: source_mac: Validation error.	save=yes
		CLI:Cancel	Raw
	END
	[Teardown]	SUITE:Cancel And Remove All Rules

Test Valid Values For Field=destination_mac
	${VALID_VALUES}=	Create List	aa:aa:aA:Aa:AA:aa	00:00:00:00:00:00	ff:fF:FF:Ff:fF:FF

	CLI:Enter Path	/settings/qos/rules
	FOR		${VALID_VALUE}	IN	@{VALID_VALUES}
		CLI:Add
		CLI:Set	name=${RULE_NAME}
		CLI:Test Set Validate Normal Fields	destination_mac	${VALID_VALUE}
		CLI:Save
		SUITE:Cancel And Remove All Rules
	END
	[Teardown]	SUITE:Cancel And Remove All Rules

Test Invalid Values For Field=destination_mac
	${INVALID_VALUES}=	Copy List	${ALL_VALUES}
	Append To List	${INVALID_VALUES}	GG:gg:GG:gg:gg:GG	XX:xx:XX:XX:XX:XX	00::00:00:00:00
	CLI:Enter Path	/settings/qos/rules

	FOR		${INVALID_VALUE}	IN	@{INVALID_VALUES}
		CLI:Add
		CLI:Set	name=${RULE_NAME}
		CLI:Test Set Field Invalid Options	destination_mac	${INVALID_VALUE}
		...	Error: destination_mac: Validation error.	save=yes
		CLI:Cancel	Raw
	END
	[Teardown]	SUITE:Cancel And Remove All Rules

Test Valid Values For Field=custom_parameters
	${VALUES}=	Copy List	${ALL_VALUES}
	Append To List	${VALUES}	${EMPTY}

	FOR		${VALID_VALUE}	IN	@{VALUES}
		SUITE:Test Set Valid Value For Field	custom_parameters	${VALID_VALUE}
	END
	[Teardown]	SUITE:Cancel And Remove All Rules

Test Valid Values For Field=syn
	CLI:Enter Path	/settings/qos/rules
	CLI:Add
	CLI:Test Set Field Options	syn	no	yes
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=syn
	CLI:Enter Path	/settings/qos/rules
	CLI:Add
	CLI:Test Set Field Invalid Options	syn	${EMPTY}
	...	Error: Missing value for parameter: syn

	FOR		${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	syn	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: syn
	END
	[Teardown]	SUITE:Cancel And Remove All Rules

Test Valid Values For Field=ack
	CLI:Enter Path	/settings/qos/rules
	CLI:Add
	CLI:Test Set Field Options	ack	no	yes
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=ack
	CLI:Enter Path	/settings/qos/rules
	CLI:Add
	CLI:Test Set Field Invalid Options	ack	${EMPTY}
	...	Error: Missing value for parameter: ack

	FOR		${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	ack	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: ack
	END
	[Teardown]	SUITE:Cancel And Remove All Rules

Test Valid Values For Field=classes
	CLI:Enter Path	/settings/qos/rules
	CLI:Add
	CLI:Test Set Field Options	classes	${DUMMY_CLASS1}	${DUMMY_CLASS2}	${EMPTY}
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=classes
	CLI:Enter Path	/settings/qos/rules
	CLI:Add
	FOR		${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	classes	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: classes
	END
	[Teardown]	SUITE:Cancel And Remove All Rules

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Remove All Rules
	SUITE:Delete Dummy Classes
	SUITE:Add Dummy Classes

SUITE:Teardown
	SUITE:Delete Dummy Classes
	CLI:Close Connection

SUITE:Remove All Rules
	CLI:Enter Path	/settings/qos/rules
	Run Keyword If	${NGVERSION} == '3.2'	CLI:Delete All
	...	ELSE	Run Keywords	write	delete -	AND	CLI:Commit

SUITE:Cancel And Remove All Rules
	CLI:Cancel	Raw
	SUITE:Remove All Rules

SUITE:Add Dummy Classes
	CLI:Enter Path	/settings/qos/classes
	CLI:Add
	CLI:Set	name=${DUMMY_CLASS1}
	CLI:Save
	CLI:Add
	CLI:Set	name=${DUMMY_CLASS2}
	CLI:Save

SUITE:Delete Dummy Classes
	CLI:Enter Path	/settings/qos/classes
	Run Keyword If	${NGVERSION} == '3.2'	CLI:Delete All
	...	ELSE	Run Keywords	write	delete -	AND	CLI:Commit

SUITE:Test Set Valid Value For Field
	[Arguments]	${FIELD}	${VALUE}
	CLI:Enter Path	/settings/qos/rules
	CLI:Add
	Run Keyword If	"${FIELD}" == "name"
	...		CLI:Test Set Validate Normal Fields	name	${VALUE}
	...	ELSE	Run Keywords
	...		CLI:Test Set Validate Normal Fields	name	${RULE_NAME}	AND
	...		CLI:Test Set Validate Normal Fields	${FIELD}	${VALUE}
	CLI:Save

	SUITE:Cancel And Remove All Rules

SUITE:Test Set Invalid Value For Field
	[Arguments]	${FIELD}	${VALUE}	${ERROR}=${EMPTY}
	CLI:Enter Path	/settings/qos/rules
	CLI:Add
	Run Keyword If	"${FIELD}" == "name"
	...		CLI:Test Set Field Invalid Options	name	${VALUE}
	...		${ERROR}	save=yes
	...	ELSE	Run Keywords
	...		CLI:Test Set Validate Normal Fields	name	${RULE_NAME}	AND
	...		CLI:Test Set Field Invalid Options	${FIELD}	${VALUE}
	...		${ERROR}	save=yes

	CLI:Cancel	Raw