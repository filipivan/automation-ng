*** Settings ***
Resource	../../init.robot
Documentation	Functionality test cases about DHCP server through the CLI on Hosts path.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL
Default Tags	CLI	SSH	NETWORK	DHCP SERVER	DHCP HOSTS
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SWITCH_CLIENT_INTERFACE}	${SWITCH_NETPORT_SHARED_INTERFACE1}
${SWITCH_SERVER_INTERFACE}	${SWITCH_NETPORT_HOST_INTERFACE1}
${BCKP_INTERFACE}	backplane0
${BCKP_CONNECTION}	BACKPLANE0
${VLAN_NAME}	vlan100
${SERVER_SUBNET}	10.20.30.0
${SERVER_NETMASK}	255.255.255.0
${HOST_NAME}	test
${CHANGEABLE_HOSTNAME}	localhost
${ASSIGNED_NAME}	test-assigned
${SERVER_STATIC_IP}	10.20.30.1
${CLIENT_STATIC_IP}	10.20.30.2
${HW_ADDRESS_IP}	10.20.30.40
${VLAN_INTERFACE_IP}	10.20.30.50
${HOSTNAME_VLAN_INTERFACE_IP}	10.20.30.60
${HOSTNAME_INTERFACE_IP}	10.20.30.70
${ASSIGNED_HOSTNAME_IP}	10.20.30.80

*** Test Cases ***
Test interface gets IP address from hardware address
	[Documentation]	Test adds a host under DHCP server that assigned ${HW_ADDRESS_IP} to VLAN connection that has
	...	${BCKP_INTERFACE} as tagged port and then checks the desired IP was really assigned to the connection.
	CLI:Switch Connection	server
	SUITE:Add Host	MAC_ADDRESS=${BCKP_MAC}	DESIRED_IP=${HW_ADDRESS_IP}
	CLI:Switch Connection	client
	CLI:Enter Path	/settings/network_connections/${VLAN_NAME}
	CLI:Set	ipv4_mode=dhcp
	CLI:Commit
	Wait Until Keyword Succeeds	30s	5s	SUITE:Check If Client Got Desired IP	${HW_ADDRESS_IP}
	[Teardown]	SUITE:Delete Host

Test interface gets IP address from agent circuit ID vlan:interface
	[Documentation]	Test adds a host under DHCP server that assigned ${VLAN_INTERFACE_IP} to VLAN connection that has
	...	circuit ID format as vlan:interface and then checks the desired IP was really assigned to the connection.
	CLI:Switch Connection	server
	SUITE:Add Host	DESIRED_IP=${VLAN_INTERFACE_IP}	CIRCUIT_ID=VLAN0100:${SWITCH_SERVER_INTERFACE}
	CLI:Switch Connection	client
	SUITE:Change VLAN Connection IPv4 Mode	static
	SUITE:Change VLAN Connection IPv4 Mode	dhcp
	Wait Until Keyword Succeeds	45s	10s	SUITE:Check If Client Got Desired IP	${VLAN_INTERFACE_IP}
	[Teardown]	SUITE:Delete Host

Test interface gets IP address from agent circuit ID hostname:vlan:interface
	[Documentation]	Test adds a host under DHCP server that assigned ${HOSTNAME_VLAN_INTERFACE_IP} to VLAN connection that has
	...	circuit ID format as hostname:vlan:interface and then checks the desired IP was really assigned to the connection.
	[Tags]	EXCLUDEIN5_6
	CLI:Switch Connection	server
	SUITE:Configure DHCP Snooping	hostname:vlan:interface
	SUITE:Add Host	DESIRED_IP=${HOSTNAME_VLAN_INTERFACE_IP}	CIRCUIT_ID=${HOSTNAME_NODEGRID}:VLAN0100:${SWITCH_SERVER_INTERFACE}
	CLI:Switch Connection	client
	SUITE:Change VLAN Connection IPv4 Mode	static
	SUITE:Change VLAN Connection IPv4 Mode	dhcp
	Wait Until Keyword Succeeds	45s	10s	SUITE:Check If Client Got Desired IP	${HOSTNAME_VLAN_INTERFACE_IP}
	[Teardown]	SUITE:Delete Host

Test interface gets IP address from agent circuit ID hostname:interface
	[Documentation]	Test adds a host under DHCP server that assigned ${HOSTNAME_INTERFACE_IP} to VLAN connection that has
	...	circuit ID format as hostname:interface and then checks the desired IP was really assigned to the connection.
	[Tags]	EXCLUDEIN5_6
	CLI:Switch Connection	server
	SUITE:Configure DHCP Snooping	hostname:interface
	SUITE:Add Host	DESIRED_IP=${HOSTNAME_INTERFACE_IP}	CIRCUIT_ID=${HOSTNAME_NODEGRID}:${SWITCH_SERVER_INTERFACE}
	CLI:Switch Connection	client
	SUITE:Change VLAN Connection IPv4 Mode	static
	SUITE:Change VLAN Connection IPv4 Mode	dhcp
	Wait Until Keyword Succeeds	45s	10s	SUITE:Check If Client Got Desired IP	${HOSTNAME_INTERFACE_IP}
	[Teardown]	SUITE:Delete Host

Test interface gets hostname from assigned hostname
	[Documentation]	Test adds a host to change hostname from client to ${ASSIGNED_NAME}. First the test changes the client
	...	hostname to a ${CHANGEABLE_HOSTNAME} and then sends the DHCP packet and verify the hostname changed in the kernel.
	CLI:Switch Connection	server
	SUITE:Add Host	MAC_ADDRESS=${BCKP_MAC}	DESIRED_IP=${ASSIGNED_HOSTNAME_IP}	ASSIGNED_HOSTNAME=${ASSIGNED_NAME}
	CLI:Switch Connection	client
	CLI:Change Hostname	${CHANGEABLE_HOSTNAME}
	SUITE:Change VLAN Connection IPv4 Mode	static
	SUITE:Change VLAN Connection IPv4 Mode	dhcp
	Wait Until Keyword Succeeds	30s	5s	SUITE:Check If Client Got Desired IP	${ASSIGNED_HOSTNAME_IP}
	Wait Until Keyword Succeeds	60s	10s	SUITE:Check Hostname Changed	${ASSIGNED_NAME}
	[Teardown]	Run Keywords	SUITE:Delete Host	AND	CLI:Change Hostname	${HOSTNAME_NODEGRID}

*** Keywords ***
SUITE:Setup
	Skip If	not ${HAS_HOSTSHARED}	Build does not have hostshared

	CLI:Open	session_alias=server	HOST_DIFF=${HOST}
	SUITE:Enable And Disable Used Switch Interfaces	DISABLE=${SWITCH_SERVER_INTERFACE}	ENABLE=sfp0,sfp1
	SUITE:Delete VLAN
	SUITE:Desconfigure DHCP Snooping
	SUITE:Delete VLAN Connection
	SUITE:Delete DHCP Server
	SUITE:Enable And Disable Used Switch Interfaces	DISABLE=sfp0,sfp1	ENABLE=${SWITCH_SERVER_INTERFACE}
	SUITE:Add VLAN	100	${BCKP_INTERFACE},${SWITCH_SERVER_INTERFACE}
	SUITE:Configure DHCP Snooping	vlan:interface
	SUITE:Create VLAN Connection	${SERVER_STATIC_IP}
	SUITE:Create DHCP Server

	CLI:Open	session_alias=client	HOST_DIFF=${HOSTSHARED}
	SUITE:Enable And Disable Used Switch Interfaces	DISABLE=${SWITCH_CLIENT_INTERFACE}	ENABLE=sfp0,sfp1
	SUITE:Delete VLAN
	SUITE:Delete VLAN Connection
	${BCKP_MAC}	CLI:Get Interface MAC Address	${BCKP_CONNECTION}
	Set Suite Variable	${BCKP_MAC}
	SUITE:Enable And Disable Used Switch Interfaces	DISABLE=sfp0,sfp1	ENABLE=${SWITCH_CLIENT_INTERFACE}
	SUITE:Add VLAN	100	${BCKP_INTERFACE},${SWITCH_CLIENT_INTERFACE}
	SUITE:Create VLAN Connection	${CLIENT_STATIC_IP}
	CLI:Test Ping	${SERVER_STATIC_IP}	5	30	${BCKP_INTERFACE}.100

SUITE:Teardown
	Skip If	not ${HAS_HOSTSHARED}	Build does not have hostshared

	CLI:Switch Connection	server
	SUITE:Delete DHCP Server
	SUITE:Delete VLAN Connection
	SUITE:Delete VLAN
	SUITE:Desconfigure DHCP Snooping
	SUITE:Enable And Disable Used Switch Interfaces	DISABLE=${SWITCH_SERVER_INTERFACE}	ENABLE=sfp0,sfp1

	CLI:Switch Connection	client
	SUITE:Delete VLAN Connection
	SUITE:Delete VLAN
	SUITE:Enable And Disable Used Switch Interfaces	DISABLE=${SWITCH_CLIENT_INTERFACE}	ENABLE=sfp0,sfp1

	CLI:Close Connection

SUITE:Enable And Disable Used Switch Interfaces
	[Documentation]	Keyword is used to enable and disable only a list of specific switch interfaces.
	[Arguments]	${ENABLE}=${EMPTY}	${DISABLE}=${EMPTY}
	CLI:Enter Path	/settings/switch_interfaces/
	CLI:Edit	${DISABLE}
	CLI:Set	status=disabled
	CLI:Commit
	CLI:Edit	${ENABLE}
	CLI:Set	status=enabled
	CLI:Commit

SUITE:Add VLAN
	[Documentation]	Keyword adds a VLAN with a given ${ID}. First it sets the interfaces as untaged, so they will be
	...	removed from any other VLAN already created and they set ${INTERFACES} as tagged to assure they belong to the
	...	created VLAN
	[Arguments]	${ID}	${INTERFACES}
	CLI:Enter Path	/settings/switch_vlan/
	CLI:Add
	CLI:Set	tagged_ports= untagged_ports=${INTERFACES} vlan=${ID}
	CLI:Commit
	CLI:Edit	${ID}
	CLI:Set	untagged_ports= tagged_ports=${INTERFACES}
	CLI:Commit

SUITE:Delete VLAN
	[Documentation]	Keyword deletes VLAN 100 if it exists
	CLI:Enter Path	/settings/switch_vlan/
	CLI:Delete If Exists	100
	CLI:Commit

SUITE:Configure DHCP Snooping
	[Documentation]	Keyword first sets the ${BCKP_INTERFACE} as trusted then enables DHCP snooping option globally in NG.
	...	After that, it enables DHCP snooping and option 82 in VLAN 100, setting the given ${CIRCUIT_ID} format.
	[Arguments]	${CIRCUIT_ID}=vlan:interface
	CLI:Enter Path	/settings/switch_backplane/
	CLI:Set	${BCKP_INTERFACE}_dhcp_snooping=trusted
	CLI:Commit
	CLI:Enter Path	/settings/switch_global/
	CLI:Set	dhcp_snooping_status=enabled
	CLI:Commit
	CLI:Enter Path	/settings/switch_dhcp_snooping/100
	CLI:Set	enable_dhcp_snooping=yes enable_dhcp_option_82=yes
	CLI:Set	circuit_id_format=${CIRCUIT_ID}
	CLI:Commit

SUITE:Desconfigure DHCP Snooping
	[Documentation]	Keyword sets ${BCKP_INTERFACE} as untrusted and sets DHCP snooping as disabled globally.
	CLI:Enter Path	/settings/switch_backplane/
	CLI:Set	${BCKP_INTERFACE}_dhcp_snooping=untrusted
	CLI:Commit
	CLI:Enter Path	/settings/switch_global/
	CLI:Set	dhcp_snooping_status=disabled
	CLI:Commit

SUITE:Create VLAN Connection
	[Documentation]	Keyword creates a VLAN 100 connection with a given ${IP_ADDRESS} and ${BCKP_INTERFACE} as interface.
	[Arguments]	${IP_ADDRESS}
	CLI:Enter Path	/settings/network_connections
	CLI:Add
	CLI:Set	name=${VLAN_NAME} type=vlan vlan_id=100 ethernet_interface=${BCKP_INTERFACE} ipv4_mode=static
	CLI:Set	ipv4_address=${IP_ADDRESS} ipv4_bitmask=24
	CLI:Commit

SUITE:Delete VLAN Connection
	[Documentation]	Keyword deletes ${VLAN_NAME} connection if it exists.
	CLI:Enter Path	/settings/network_connections
	CLI:Delete If Exists	${VLAN_NAME}

SUITE:Create DHCP Server
	[Documentation]	Keyword creates a DHCP server in range ${SERVER_SUBNET}|${SERVER_NETMASK}.
	CLI:Enter Path	/settings/dhcp_server
	CLI:Add
	CLI:Set	protocol=dhcp4 subnet=${SERVER_SUBNET} netmask=${SERVER_NETMASK}
	CLI:Commit

SUITE:Add Host
	[Documentation]	Keyword adds a host to already created DHCP server setting hostname as ${HOST_NAME}. At least one
	...	of two options ${MAC_ADDRESS} or ${CIRCUIT_ID} must be specified. The ${DESIRED_IP} is the IP Address wanted for
	...	client interface and ${ASSIGNED_HOSTNAME} is the hostname wanted for client device.
	[Arguments]	${MAC_ADDRESS}=${EMPTY}	${DESIRED_IP}=${EMPTY}	${CIRCUIT_ID}=${EMPTY}	${ASSIGNED_HOSTNAME}=${EMPTY}
	CLI:Enter Path	/settings/dhcp_server/${SERVER_SUBNET}|${SERVER_NETMASK}/hosts
	CLI:Add
	CLI:Set	hostname=${HOST_NAME} ip_address=${DESIRED_IP}
	CLI:Set	agent_circuit_id=${CIRCUIT_ID} hw_address=${MAC_ADDRESS}
	CLI:Set	assigned_hostname=${ASSIGNED_HOSTNAME}
	CLI:Commit

SUITE:Delete Host
	[Documentation]	Keyword deletes the first host added to an already created DHCP server.
	CLI:Switch Connection	server
	CLI:Enter Path	/settings/dhcp_server/${SERVER_SUBNET}|${SERVER_NETMASK}/hosts
	CLI:Delete If Exists	1
	CLI:Commit

SUITE:Check If Client Got Desired IP
	[Documentation]	Keyword forces VLAN connection in client side to be up and then checks that the interface got the
	...	${DESIRED_IP}.
	[Arguments]	${DESIRED_IP}
	CLI:Enter Path	/settings/network_connections/
	Set Client Configuration	timeout=60s
	CLI:Write	up_connection ${VLAN_NAME}
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	${INTERFACE_IP}	CLI:Get Interface IP Address	${BCKP_INTERFACE}.100
	Should Be Equal	${INTERFACE_IP}	${DESIRED_IP}

SUITE:Check Hostname Changed
	[Documentation]	Keyword enters root shell in client side and hits enter to show the ${ASSIGNED_NAME} is shown in prompt.
	[Arguments]	${ASSIGNED_NAME}
	CLI:Switch Connection	client
	CLI:Connect As Root	session_alias=test_hostname	HOST_DIFF=${HOSTSHARED}
	${OUTPUT}	CLI:Write Bare	\n\n\n
	Should Contain	${OUTPUT}	${ASSIGNED_NAME}
	[Teardown]	Run Keywords	CLI:Close Current Connection

SUITE:Delete DHCP Server
	[Documentation]	Keyword deletes ${SERVER_SUBNET}|${SERVER_NETMASK} DHCP server if it exists.
	CLI:Enter Path	/settings/dhcp_server
	CLI:Delete If Exists	${SERVER_SUBNET}|${SERVER_NETMASK}

SUITE:Change VLAN Connection IPv4 Mode
	[Documentation]	Keyword changes the IPv4 mode for VLAN connection. If ${MODE} is set to static, the client connection
	...	gets ${CLIENT_STATIC_IP} as IP address. If ${MODE} is set to DHCP, connection will get IP from DHCP server.
	[Arguments]	${MODE}
	CLI:Enter Path	/settings/network_connections/${VLAN_NAME}
	IF	'${MODE}' == 'static'
		CLI:Set	ipv4_mode=${MODE} ipv4_address=${CLIENT_STATIC_IP}
	ELSE
		CLI:Set	ipv4_mode=${MODE}
	END
	CLI:Commit