*** Settings ***
Resource	../../init.robot
Documentation	Configuration test cases about DHCP server through the CLI on Setings path.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	NETWORK	DHCP SERVER	SERVER
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SUBNET}	192.168.16.0
${NETMASK}	255.255.255.0	#Netmask only accept values in mask format, nothing of numbers
${DOMAIN}	zpesystems.com
${DNS}	192.168.16.13
${ROUTER_IP}	192.168.16.1
${NAME_DHCP}	${SUBNET}|${NETMASK}
${DHCPV6_PREFIX}	2001:db8:cafe::
${IPV6_LENGTH}	64
${IPV6_DNS}	2001:4860:4860::8888, 2001:4860:4860::8844
${IPV6_ROUTER_IP}	2001:db8:cafe::1
${IPV6_LEASE_TIME}	15
${IPV6_NAME_DHCP}	${DHCPV6_PREFIX}|${IPV6_LENGTH}
${IPV6_RANGE_START}	2001:db8:cafe::100
${IPV6_RANGE_END}	2001:db8:cafe::110

*** Test Cases ***
Test show_settings for valid dhcp
	[Setup]	SUITE:Setup Modify Test
	CLI:Enter Path	/settings/dhcp_server/
	Write	show_settings
	${OUTPUT}=	CLI:Read Until Prompt
	Should Match Regexp	${OUTPUT}	/settings/dhcp_server/${NAME_DHCP}/settings subnet|netmask=${SUBNET}/${NETMASK}
	Should Match Regexp	${OUTPUT}	/settings/dhcp_server/${NAME_DHCP}/settings domain=${DOMAIN}
	Should Match Regexp	${OUTPUT}	/settings/dhcp_server/${NAME_DHCP}/settings domain_name_servers=${DNS}
	Should Match Regexp	${OUTPUT}	/settings/dhcp_server/${NAME_DHCP}/settings router_ip=${ROUTER_IP}
	[Teardown]	SUITE:Teardown Modify Test

#Test TAB-TAB
Test file events available commands after send tab-tab
	[Setup]	SUITE:Setup Modify Test
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/settings
	CLI:Test Available Commands
	...	apply_settings       exit                 reboot               show_settings
	...	cd                   factory_settings     revert               shutdown
	...	change_password      hostname             save_settings        software_upgrade
	...	commit               ls                   set                  system_certificate
	...	event_system_audit   pwd                  shell                system_config_check
	...	event_system_clear   quit                 show                 whoami
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Available Commands
	...	cloud_enrollment	create_csr	diagnostic_data
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Available Commands
	...	exec	export_settings
	[Teardown]	SUITE:Teardown Modify Test

Test validate dhcp settings=domain
	[Setup]	SUITE:Setup Modify Test
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/settings/
	CLI:Test Set Validate Invalid Options	domain	${EMPTY}
	CLI:Test Set Validate Invalid Options	domain	${WORD}
	CLI:Test Set Validate Invalid Options	domain	${NUMBER}
	CLI:Test Set Validate Invalid Options	domain	${POINTS}	Error: domain: Validation error.
	CLI:Test Set Validate Invalid Options	domain	${EXCEEDED}	Error: domain: Exceeded the maximum size for this field.
	CLI:Test Set Validate Normal Fields	domain	${DOMAIN}
	[Teardown]	SUITE:Teardown Modify Test

Test validate dhcp settings=domain_name_servers
	[Setup]	SUITE:Setup Modify Test
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/settings/
	CLI:Test Set Validate Invalid Options	domain_name_servers	${EMPTY}
	CLI:Test Set Validate Invalid Options	domain_name_servers	${WORD}	Error: domain_name_servers: Validation error.
	CLI:Test Set Validate Invalid Options	domain_name_servers	${NUMBER}	Error: domain_name_servers: Validation error.
	CLI:Test Set Validate Invalid Options	domain_name_servers	${POINTS}	Error: domain_name_servers: Validation error.
	CLI:Test Set Validate Invalid Options	domain_name_servers	${EXCEEDED}	Error: domain_name_servers: Validation error.
	CLI:Test Set Validate Normal Fields	domain_name_servers	${DNS}
	[Teardown]	SUITE:Teardown Modify Test

Test validate dhcp settings=router_ip
	[Setup]	SUITE:Setup Modify Test
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/settings/
	CLI:Test Set Validate Invalid Options	router_ip	${EMPTY}
	CLI:Test Set Validate Invalid Options	router_ip	${WORD}	Error: router_ip: Validation error.
	CLI:Test Set Validate Invalid Options	router_ip	${NUMBER}	Error: router_ip: Validation error.
	CLI:Test Set Validate Invalid Options	router_ip	${POINTS}	Error: router_ip: Validation error.
	CLI:Test Set Validate Invalid Options	router_ip	${EXCEEDED}	Error: router_ip: Validation error.
	CLI:Test Set Validate Normal Fields	router_ip	${ROUTER_IP}
	[Teardown]	SUITE:Teardown Modify Test

Test delete dhcp
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Delete If Exists	${NAME_DHCP}
	${OUTPUT}=	CLI:Check If Exists	${NAME_DHCP}
	Should Not Be True	${OUTPUT}
	[Teardown]	CLI:Cancel	Raw

Test To Configure DHCP Server To Ipv6
	Skip If	'${NGVERSION}' < '5.4'
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Set	protocol=dhcp6
	#set prefix=2001:0db8:cafe:: length=64 domain=zpesystems.com domain_name_servers=2001:4860:4860::8888, 2001:4860:4860::8844 router_ip=2001:db8:cafe::1 lease_time=15
	CLI:Set	prefix=${DHCPV6_PREFIX} length=${IPV6_LENGTH} domain=${DOMAIN} router_ip=${IPV6_ROUTER_IP} domain_name_servers="${IPV6_DNS}" lease_time=${IPV6_LEASE_TIME}
	CLI:Commit
	CLI:Test Show Command	${DHCPV6_PREFIX}/${IPV6_LENGTH}
	CLI:Enter Path	/settings/dhcp_server/${IPV6_NAME_DHCP}/network_range
	CLI:Add
	#set ip_address_start=2001:db8:cafe::100 ip_address_end=2001:db8:cafe::110
	CLI:Set	ip_address_start=${IPV6_RANGE_START} ip_address_end=${IPV6_RANGE_END}
	CLI:Commit
	CLI:Test Show Command	${IPV6_RANGE_START}/${IPV6_RANGE_END}
	[Teardown]	CLI:Cancel	Raw

Test Add Duplicate IPV6 Server
	[Documentation]	Test adding a duplicate DHCP server. It's expected that it gives you an error
	Skip If	'${NGVERSION}' < '5.4'
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Set	protocol=dhcp6
	#set prefix=2001:0db8:cafe:: length=64 domain=zpesystems.com domain_name_servers=2001:4860:4860::8888, 2001:4860:4860::8844 router_ip=2001:db8:cafe::1 lease_time=15
	CLI:Set	prefix=${DHCPV6_PREFIX} length=${IPV6_LENGTH} domain=${DOMAIN} router_ip=${IPV6_ROUTER_IP} domain_name_servers="${IPV6_DNS}" lease_time=${IPV6_LEASE_TIME}
	Write	commit
	${OUTPUT}	Read Until Prompt
	Should Contain	${OUTPUT}	Error: Entry already exists
	[Teardown]	CLI:Cancel	Raw

Test To Delete DHCP Configuration To Ipv6
	Skip If	'${NGVERSION}' < '5.4'
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Delete	${IPV6_NAME_DHCP}

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Delete If Exists	${NAME_DHCP}

SUITE:Teardown
	CLI:Delete If Exists	${NAME_DHCP}
	CLI:Close Connection

SUITE:Setup Modify Test
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Test Set Field Invalid Options	subnet	${SUBNET}
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	CLI:Test Set Field Invalid Options	domain	${DOMAIN}
	CLI:Test Set Field Invalid Options	domain_name_servers	${DNS}
	CLI:Test Set Field Invalid Options	router_ip	${ROUTER_IP}
	CLI:Commit	yes

SUITE:Teardown Modify Test
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Delete If Exists	${NAME_DHCP}