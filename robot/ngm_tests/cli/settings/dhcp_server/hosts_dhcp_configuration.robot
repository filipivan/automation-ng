*** Settings ***
Resource	../../init.robot
Documentation	Configuration test cases about DHCP server through the CLI on Hosts path.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	NETWORK	DHCP SERVER	DHCP HOSTS
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SUBNET}	192.168.16.0
${NETMASK}	255.255.255.0
${NAME_DHCP}	${SUBNET}|${NETMASK}
${HOSTNAME}	hostname
${HW_ADDRESS}	e3:e3:e3:e3:e3:e3
${HW_INVALID}	e3:e3:a:e3:e3:e3
${IP_ADDRESS}	192.168.15.1
${IP_INVALID}	192.a.a.1
${DHCPV6_PREFIX}	2001:db8:cafe::
${IPV6_LENGTH}	64
${IPV6_DNS}	2001:4860:4860::8888, 2001:4860:4860::8844
${IPV6_ROUTER_IP}	2001:db8:cafe::1
${IPV6_LEASE_TIME}	15
${IPV6_NAME_DHCP}	${DHCPV6_PREFIX}|${IPV6_LENGTH}
${IPV6_RANGE_START}	2001:db8:cafe::100
${DOMAIN}	zpesystems.com

*** Test Cases ***
#Test TAB-TAB
Test file events available commands after send tab-tab
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/hosts
	CLI:Test Available Commands
	...	add                  event_system_clear   reboot               shutdown
	...	apply_settings       exit                 revert               software_upgrade
	...	cd                   factory_settings     save_settings        system_certificate
	...	change_password      hostname             set                  system_config_check
	...	commit               ls                   shell                whoami
	...	delete               pwd                  show
	...	event_system_audit   quit                 show_settings
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Available Commands
	...	cloud_enrollment	create_csr	diagnostic_data
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Available Commands
	...	exec	export_settings

Test show command
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/hosts/
	${OUTPUT}	CLI:Show
	IF	'${NGVERSION}' <= '4.2'
		${LIST}	Create List	hostname	hw address	ip address
	ELSE
		${LIST}	Create List	hostname	hw_address	ip_address
	END
	CLI:Should Contain All	${OUTPUT}	${LIST}

Test defining Empty values dhcp host
	Skip If	'${NGVERSION}' == '3.2'	Error messages not implemented on 3.2
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/hosts/
	CLI:Add
	Run Keyword If	'${NGVERSION}' <= '5.2'	CLI:Test Set Field Invalid Options	hostname	${EMPTY}	Error: hostname: Validation error.Error: hw_address: Validation error.Error: ip_address: Validation error.	yes
	Run Keyword If	'${NGVERSION}' <= '5.2'	CLI:Test Set Field Invalid Options	hw_address	${HW_ADDRESS}	Error: hostname: Validation error.Error: hw_address: Validation error.Error: ip_address: Validation error.	yes
	Run Keyword If	'${NGVERSION}' <= '5.2'	CLI:Test Set Field Invalid Options	ip_address	${IP_ADDRESS}	Error: hostname: Validation error.Error: hw_address: Validation error.Error: ip_address: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '5.4'	CLI:Test Set Field Invalid Options	hostname	${EMPTY}	Error: hostname: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '5.4'	CLI:Test Set Field Invalid Options	hw_address	${HW_ADDRESS}	Error: hostname: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '5.4'	CLI:Test Set Field Invalid Options	ip_address	${IP_ADDRESS}	Error: hostname: Validation error.	yes
	CLI:Test Set Field Invalid Options	hostname	${EMPTY}
	CLI:Test Set Field Invalid Options	hw_address	${EMPTY}
	CLI:Test Set Field Invalid Options	ip_address	${IP_ADDRESS}
	CLI:Test Set Field Invalid Options	hw_address	${HW_ADDRESS}
	CLI:Test Set Field Invalid Options	hostname	${HOSTNAME}
	Run Keyword If	'${NGVERSION}' <= '5.4'	CLI:Test Set Field Invalid Options	hw_address	${EMPTY}	Error: hw_address: Validation error.	yes
	...	ELSE	CLI:Test Set Field Invalid Options	hw_address	${EMPTY}	Error: hw_address: At least one of the fields (HW Address or Agent Circuit ID) must be present.Error: agent_circuit_id: At least one of the fields (HW Address or Agent Circuit ID) must be present.	yes
	CLI:Test Set Field Invalid Options	hw_address	${HW_ADDRESS}
	Run Keyword If	'${NGVERSION}' <= '5.4'	CLI:Test Set Field Invalid Options	ip_address	${EMPTY}	Error: ip_address: Validation error.	yes
	...	ELSE	CLI:Test Set Field Invalid Options	hw_address	${EMPTY}	Error: hw_address: At least one of the fields (HW Address or Agent Circuit ID) must be present.Error: agent_circuit_id: At least one of the fields (HW Address or Agent Circuit ID) must be present.	yes
	CLI:Test Set Field Invalid Options	ip_address	${IP_ADDRESS}	save=yes
	CLI:Delete If Exists	1
	[Teardown]	CLI:Cancel	Raw

Test defining Word values dhcp host
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/hosts/
	CLI:Add
	CLI:Test Set Field Invalid Options	hostname	${WORD}
	CLI:Test Set Field Invalid Options	hw_address	${WORD}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ip_address	${WORD}	Error: ip_address: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ip_address	${WORD}	Error: hw_address: Validation error	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_address	${WORD}	Error: hw address: Validation error.Error: ip address: Validation error.	yes
	CLI:Test Set Field Invalid Options	hostname	${HOSTNAME}
	CLI:Test Set Field Invalid Options	ip_address	${IP_ADDRESS}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	hw_address	${WORD}	Error: hw_address: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	hw_address	${WORD}	Error: hw address: Validation error.	yes
	CLI:Test Set Field Invalid Options	hw_address	${HW_ADDRESS}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ip_address	${WORD}	Error: ip_address: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_address	${WORD}	Error: ip address: Validation error.	yes
	CLI:Test Set Field Invalid Options	ip_address	${IP_ADDRESS}	save=yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Delete If Exists	${WORD}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Delete If Exists	1
	[Teardown]	CLI:Cancel	Raw

Test defining Number values dhcp host
	Skip If	'${NGVERSION}' == '3.2'	Error messages not implemented on 3.2
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/hosts/
	CLI:Add
	CLI:Test Set Field Invalid Options	hostname	${NUMBER}
	CLI:Test Set Field Invalid Options	hw_address	${NUMBER}
	CLI:Test Set Field Invalid Options	ip_address	${NUMBER}	Error: hw_address: Validation error.Error: ip_address: Validation error.	yes
	CLI:Test Set Field Invalid Options	hostname	${NUMBER}
	CLI:Test Set Field Invalid Options	hw_address	${NUMBER}	Error: hw_address: Validation error.Error: ip_address: Validation error.	yes
	CLI:Test Set Field Invalid Options	hw_address	${HW_ADDRESS}
	CLI:Test Set Field Invalid Options	ip_address	${NUMBER}	Error: ip_address: Validation error.	yes
	CLI:Test Set Field Invalid Options	ip_address	${IP_ADDRESS}	save=yes
	CLI:Delete If Exists	1
	[Teardown]	CLI:Cancel	Raw

Test defining Points values dhcp host
	Skip If	'${NGVERSION}' == '3.2'	Error messages not implemented on 3.2
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/hosts/
	CLI:Add
	CLI:Set	hw_address=${HW_ADDRESS} ip_address=${IP_ADDRESS}
	Run Keyword If	'${NGVERSION}' >= '5.4'	CLI:Test Set Field Invalid Options	hostname	${POINTS}	Error: hostname: Validation error.	commit
	CLI:Set	hostname=${HOSTNAME}
	CLI:Test Set Field Invalid Options	hw_address	${POINTS}	Error: hw_address: Validation error.	commit
	CLI:Set	hw_address=${HW_ADDRESS}
	CLI:Test Set Field Invalid Options	ip_address	${POINTS}	Error: ip_address: Validation error.	commit
	CLI:Delete If Exists	1
	[Teardown]	CLI:Cancel	Raw

Test defining IP wrong dhcp host
	Skip If	'${NGVERSION}' == '3.2'	Error messages not implemented on 3.2
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/hosts/
	CLI:Add
	CLI:Set	hostname=${HOSTNAME} hw_address=${HW_ADDRESS}
	CLI:Test Set Field Invalid Options	ip_address	${IP_INVALID}	Error: ip_address: Validation error.	yes
	CLI:Set Field	ip_address	${IP_ADDRESS}
	CLI:Test Set Field Invalid Options	hw_address	${IP_INVALID}	Error: hw_address: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

Test defining Right values dhcp host
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/hosts/
	CLI:Add
	CLI:Set	hostname=${HOSTNAME} hw_address=${HW_ADDRESS} ip_address=${IP_ADDRESS}
	CLI:Commit
	CLI:Show
	[Teardown]	CLI:Cancel	Raw

Test Adding Duplicate DHCP host on same server
	[Documentation]	It tries to add a second host with same name the DHCP Server. It was created after BUG_NG_9086
	Skip If	'${NGVERSION}' <= '5.0'	Fix only implemented on 5.2+
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/hosts/
	CLI:Add
	CLI:Set	hostname=${HOSTNAME} hw_address=${HW_ADDRESS} ip_address=${IP_ADDRESS}
	Write	commit
	${OUTPUT}	Read Until Prompt
	Should Contain	${OUTPUT}	Error: hostname: Entry already exists.
	[Teardown]	CLI:Cancel	Raw

Test Adding Duplicate DHCP host on another server
	[Documentation]	It tries to add a host with same name to a second DHCP Server. It was created after BUG_NG_9086
	Skip If	'${NGVERSION}' <= '5.0'	Fix only implemented on 5.2+
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Write	set subnet=192.168.14.0 netmask=255.255.254.0
	CLI:Save
	CLI:Enter Path	192.168.14.0|255.255.254.0/hosts/
	CLI:Add
	CLI:Set	hostname=${HOSTNAME} hw_address=${HW_ADDRESS} ip_address=${IP_ADDRESS}
	Write	commit
	${OUTPUT}	Read Until Prompt
	Should Contain	${OUTPUT}	Error: hostname: Entry already exists.
	[Teardown]	CLI:Cancel	Raw

Test validate values dhcp host
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/hosts/1
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/hosts/${HOSTNAME}
	CLI:Test Validate Unmodifiable Field	hostname	${HOSTNAME}
	#CLI:Test Show Command Regexp	hw\\s+address:\\s+${HW_ADDRESS}
	CLI:Test Show Command Regexp	hw( |_)address:\\s+${HW_ADDRESS}
	#CLI:Test Show Command Regexp	ip\\s+address:\\s+${IP_ADDRESS}
	CLI:Test Show Command Regexp	ip( |_)address:\\s+${IP_ADDRESS}

Test Configure Hosts To DHCP Server To Ipv6
	Skip If	'${NGVERSION}' < '5.4'
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Set	protocol=dhcp6
	#set prefix=2001:0db8:cafe:: length=64 domain=zpesystems.com domain_name_servers=2001:4860:4860::8888, 2001:4860:4860::8844 router_ip=2001:db8:cafe::1 lease_time=15
	CLI:Set	prefix=${DHCPV6_PREFIX} length=${IPV6_LENGTH} domain=${DOMAIN} router_ip=${IPV6_ROUTER_IP} domain_name_servers="${IPV6_DNS}" lease_time=${IPV6_LEASE_TIME}
	CLI:Commit
	CLI:Test Show Command	${DHCPV6_PREFIX}/${IPV6_LENGTH}
	CLI:Enter Path	/settings/dhcp_server/${IPV6_NAME_DHCP}/hosts
	CLI:Add
	CLI:Set	hostname=${HOSTNAME} duid=${HW_ADDRESS} ip_address=${IPV6_RANGE_START}
	CLI:Commit
	CLI:Test Show Command	${IPV6_RANGE_START}
	[Teardown]	CLI:Cancel	Raw

Test Add Duplicate HOST on Same IPV6 Server
	Skip If	'${NGVERSION}' < '5.4'
	CLI:Enter Path	/settings/dhcp_server/${IPV6_NAME_DHCP}/hosts
	CLI:Add
	CLI:Set	hostname=${HOSTNAME} duid=${HW_ADDRESS} ip_address=${IPV6_RANGE_START}
	Write	commit
	${OUTPUT}	Read Until Prompt
	Should Contain	${OUTPUT}	Error: hostname: Entry already exists.
	[Teardown]	CLI:Cancel	Raw

Test Add Duplicate HOST to Another IPV6 Server
	Skip If	'${NGVERSION}' < '5.4'
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Set	protocol=dhcp6
	#set prefix=2001:0db8:cafe:: length=64 domain=zpesystems.com domain_name_servers=2001:4860:4860::8888, 2001:4860:4860::8844 router_ip=2001:db8:cafe::1 lease_time=15
	CLI:Set	prefix=2001:db8:caff:: length=${IPV6_LENGTH} domain=${DOMAIN} router_ip=${IPV6_ROUTER_IP} domain_name_servers="${IPV6_DNS}" lease_time=${IPV6_LEASE_TIME}
	CLI:Commit
	CLI:Enter Path	/settings/dhcp_server/2001:db8:caff::|${IPV6_LENGTH}/hosts
	CLI:Add
	CLI:Set	hostname=${HOSTNAME} duid=${HW_ADDRESS} ip_address=${IPV6_RANGE_START}
	Write	commit
	${OUTPUT}	Read Until Prompt
	Should Contain	${OUTPUT}	Error: hostname: Entry already exists.
	[Teardown]	CLI:Cancel	Raw

Test To Delete DHCP Configuration To Ipv6
	Skip If	'${NGVERSION}' < '5.4'
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Delete If Exists	${IPV6_NAME_DHCP}

Test leaving hw_address and circuit_id as blank
	[Documentation]	Test set both hw_address and agent_circuit_id as blank and checks that is not a valid configuration
	...	and the proper error message should be raised.
	[Tags]	NON-CRITICAL	EXCLUDEIN3_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4
	CLI:Enter Path	/settings/dhcp_server/${SUBNET}|${NETMASK}/hosts
	CLI:Add
	CLI:Set	hostname=${HOSTNAME} ip_address=${IP_ADDRESS}
	CLI:Set	hw_address= agent_circuit_id=
	${EXPECTED_ERROR}	CLI:Commit	Raw
	Should Contain	${EXPECTED_ERROR}	At least one of the fields (HW Address or Agent Circuit ID) must be present.
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Delete If Exists	${NAME_DHCP}
	CLI:Add
	CLI:Write	set subnet=${SUBNET} netmask=${NETMASK}
	CLI:Save

SUITE:Teardown
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Delete If Exists	${NAME_DHCP}	2001:db8:caff::|${IPV6_LENGTH}	192.168.14.0|255.255.254.0
	CLI:Close Connection

