*** Settings ***
Resource	../../init.robot
Documentation	Functionality test cases about DHCP server through the CLI.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	NETWORK	DHCP SERVER	SERVER	EXCLUDEIN3_2	NEED-REVIEW	NON-CRITICAL	#added tag since device's switch(6.47) seem not to work. Tried on another device(6.46) and it worked
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${VLAN_ID}	150
${VLAN_INTERFACE_NAME}	test_automated_dhcp_vlan
${VLAN_IPV4_HOST}	10.10.80.1
${VLAN_IPV4_SHARED}	10.10.80.22
${IPV4_SUBNET}	10.10.80.0
${IPV4_NETMASK}	255.255.255.0	#Netmask only accept values in mask format, nothing of numbers
${DOMAIN}	zpesystems.com
${IPV4_DNS}	8.8.8.8
${IPV4_ROUTER_IP}	10.10.80.1
${IPV4_NAME_DHCP}	${IPV4_SUBNET}|${IPV4_NETMASK}
${IPV4_RANGE_START}	10.10.80.40
${IPV4_RANGE_END}	10.10.80.80
${DHCPV6_PREFIX}	2001:db8:cafe::
${IPV6_LENGTH}	64
${IPV6_DNS}	2001:4860:4860::8888, 2001:4860:4860::8844
${IPV6_ROUTER_IP}	2001:db8:cafe::1
${IPV6_LEASE_TIME}	15
${IPV6_NAME_DHCP}	${DHCPV6_PREFIX}|${IPV6_LENGTH}
${IPV6_RANGE_START}	2001:db8:cafe::100
${IPV6_RANGE_END}	2001:db8:cafe::110

*** Test Cases ***
Test Check If DHCP Client Get The Ipv4 Address From Server
	Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	SUITE:Setup DHCP Server To Ipv4
	SUITE:Setup DHCP Client To Ipv4
	CLI:Switch Connection	hostshared_session
	CLI:Enter Path	/settings/network_connections/
	${STATUS}=	CLI:Show
	Should Contain	${STATUS}	${IPV4_RANGE_START}
	[Teardown]	CLI:Cancel	Raw

Test Check If DHCP Client Is Shown As Leased in Table
	[Tags]	NON-CRITICAL
	Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	Skip If	'${NGVERSION}' < '5.4'
	Wait Until Keyword Succeeds	30s	3s	SUITE:Check If DHCP Client Is Shown As Leased In Table	${VLAN_INTERFACE_NAME}	dhcpv4

Test Ping External Address Through The VLAN Interface Using DHCP Ipv4 Address
	Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	CLI:Switch Connection	hostshared_session
	Wait Until Keyword Succeeds	3x	3s
	...	CLI:Test Ping	${VLAN_IPV4_HOST}	NUM_PACKETS=5	TIMEOUT=30	SOURCE_INTERFACE=backplane0.${VLAN_ID}
	[Teardown]	SUITE:Delete DHCP Setup To Ipv4

Test Check If DHCP Client Get The Ipv6 Address From Server
	Skip If	'${NGVERSION}' < '5.4'
	Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	SUITE:Setup DHCP Server To Ipv6
	SUITE:Setup DHCP Client To Ipv6
	#DHCPv6 Should work without restart it by root
#	CLI:Switch Connection	hostdefault_session
#	${ROOT_OUTPUT}=	CLI:Write	shell sudo /etc/init.d/dhcp6-server restart
#	Should Contain	${ROOT_OUTPUT}	Starting DHCP6 server
	CLI:Switch Connection	hostshared_session
	Wait Until Keyword Succeeds	30s	3s	SUITE:Check If DHCP Client Get The Ipv6 Address From Server
	[Teardown]	SUITE:Delete DHCP Setup To Ipv6

Test Check If DHCPv6 Client Is Shown As Leased in Table
	[Tags]	NON-CRITICAL
	Skip If	'${NGVERSION}' < '5.4'
	Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	SUITE:Setup DHCP Server To Ipv6
	SUITE:Setup DHCP Client To Ipv6
	Wait Until Keyword Succeeds	30s	3s	SUITE:Check If DHCP Client Get The Ipv6 Address From Server
	Wait Until Keyword Succeeds	30s	3s	SUITE:Check If DHCP Client Is Shown As Leased In Table	${VLAN_INTERFACE_NAME}	dhcpv6
	[Teardown]	SUITE:Delete DHCP Setup To Ipv6

*** Keywords ***
SUITE:Setup
	Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	Set Suite Variable	${NETPORT_HOST_INTERFACE}	${SWITCH_NETPORT_HOST_INTERFACE2}
	Set Suite Variable	${NETPORT_SHARED_INTERFACE}	${SWITCH_NETPORT_SHARED_INTERFACE2}
	CLI:Open	session_alias=hostdefault_session
	CLI:Open	session_alias=hostshared_session	HOST_DIFF=${HOSTSHARED}
	SUITE:Delete VLAN
	SUITE:Delete DHCP Setup To Ipv4
	SUITE:Delete DHCP Setup To Ipv6
	SUITE:Setup VLAN

SUITE:Teardown
	Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	SUITE:Delete VLAN
	SUITE:Delete DHCP Setup To Ipv4
	SUITE:Delete DHCP Setup To Ipv6
	CLI:Close Connection

SUITE:Enable Interfaces On Switch Interfaces
	CLI:Switch Connection	hostdefault_session
	CLI:Enter Path	/settings/switch_interfaces/${NETPORT_HOST_INTERFACE}
	CLI:Set	status=enabled
	CLI:Commit
	CLI:Switch Connection	hostshared_session
	CLI:Enter Path	/settings/switch_interfaces/${NETPORT_SHARED_INTERFACE}
	CLI:Set	status=enabled
	CLI:Commit
	[Teardown]	CLI:Cancel	Raw

SUITE:Delete VLAN
	Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	CLI:Switch Connection	hostdefault_session
	CLI:Delete VLAN	${VLAN_ID}
	CLI:Delete Network Connections	${VLAN_INTERFACE_NAME}
	CLI:Disable Switch Interface	${NETPORT_HOST_INTERFACE}
	CLI:Switch Connection	hostshared_session
	CLI:Delete VLAN	${VLAN_ID}
	CLI:Delete Network Connections	${VLAN_INTERFACE_NAME}
	CLI:Disable Switch Interface	${NETPORT_SHARED_INTERFACE}

SUITE:Setup VLAN
	Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	CLI:Switch Connection	hostdefault_session
	CLI:Disable All Switch Interfaces
	CLI:Add VLAN	${VLAN_ID}	backplane0,${NETPORT_HOST_INTERFACE}
	CLI:Add Static IPv4 Network Connection	${VLAN_INTERFACE_NAME}	${VLAN_IPV4_HOST}	vlan	backplane0	24	${VLAN_ID}
#	SUITE:Change To BackPlane1 And Then Come Back To BackPlane0
	CLI:Switch Connection	hostshared_session
	CLI:Disable All Switch Interfaces
	CLI:Add VLAN	${VLAN_ID}	backplane0,${NETPORT_SHARED_INTERFACE}
	CLI:Add Static IPv4 Network Connection	${VLAN_INTERFACE_NAME}	${VLAN_IPV4_SHARED}	vlan	backplane0	24	${VLAN_ID}
#	SUITE:Change To BackPlane1 And Then Come Back To BackPlane0
	SUITE:Enable Interfaces On Switch Interfaces
	CLI:Switch Connection	hostdefault_session
	Wait Until Keyword Succeeds	3x	5s	CLI:Test Ping	${VLAN_IPV4_SHARED}	NUM_PACKETS=5	TIMEOUT=30
	CLI:Switch Connection	hostshared_session
	Wait Until Keyword Succeeds	3x	5s	CLI:Test Ping	${VLAN_IPV4_HOST}	NUM_PACKETS=5	TIMEOUT=30

SUITE:Delete DHCP Setup To Ipv4
	CLI:Switch Connection	hostdefault_session
	CLI:Enter Path	/settings/dhcp_server
	CLI:Delete If Exists	${IPV4_NAME_DHCP}

SUITE:Setup DHCP Server To Ipv4
	CLI:Switch Connection	hostdefault_session
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	#set subnet=10.10.80.0 netmask=255.255.255.0 domain=zpesystems.com domain_name_servers=8.8.8.8 router_ip=10.10.80.1
	CLI:Set	subnet=${IPV4_SUBNET} netmask=${IPV4_NETMASK} domain=${DOMAIN} domain_name_servers=${IPV4_DNS} router_ip=${IPV4_ROUTER_IP}
	CLI:Save
	CLI:Test Show Command	${IPV4_SUBNET}/${IPV4_NETMASK}
	CLI:Enter Path	/settings/dhcp_server/${IPV4_NAME_DHCP}/network_range
	CLI:Add
	CLI:Set	ip_address_start=${IPV4_RANGE_START} ip_address_end=${IPV4_RANGE_END}
	CLI:Commit
	CLI:Test Show Command	${IPV4_RANGE_START}/${IPV4_RANGE_END}
	[Teardown]	CLI:Cancel	Raw

SUITE:Setup DHCP Client To Ipv4
	CLI:Switch Connection	hostshared_session
	CLI:Enter Path	/settings/network_connections/${VLAN_INTERFACE_NAME}
	CLI:Set	ipv4_mode=dhcp
	CLI:Commit

SUITE:Change To BackPlane1 And Then Come Back To BackPlane0
	[Tags]	BUG_NG_7027
	CLI:Enter Path	/settings/network_connections/${VLAN_INTERFACE_NAME}
	CLI:Set	ethernet_interface=backplane1
	CLI:Commit
	CLI:Set	ethernet_interface=backplane0
	CLI:Commit

SUITE:Setup DHCP Server To Ipv6
	CLI:Switch Connection	hostdefault_session
	CLI:Enter Path	/settings/network_connections/${VLAN_INTERFACE_NAME}
	CLI:Set	ipv6_mode=static
	CLI:Set	ipv6_address=2001:db8:cafe::1 ipv6_prefix_length=64
	CLI:Commit
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Set	protocol=dhcp6
	#set prefix=2001:db8:cafe:: length=64 domain=zpesystems.com domain_name_servers=2001:4860:4860::8888, 2001:4860:4860::8844 lease_time=15
	CLI:Set	prefix=${DHCPV6_PREFIX} length=${IPV6_LENGTH} domain=${DOMAIN} domain_name_servers="${IPV6_DNS}" lease_time=${IPV6_LEASE_TIME}
	CLI:Commit
	CLI:Test Show Command	${DHCPV6_PREFIX}/${IPV6_LENGTH}
	${OUTPUT}=	CLI:Ls
	Should Contain	${OUTPUT}	${IPV6_NAME_DHCP}
	CLI:Enter Path	/settings/dhcp_server/${IPV6_NAME_DHCP}/network_range
	CLI:Add
	#set ip_address_start=2001:db8:cafe::100 ip_address_end=2001:db8:cafe::110
	CLI:Set	ip_address_start=${IPV6_RANGE_START} ip_address_end=${IPV6_RANGE_END}
	CLI:Commit
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	${IPV6_RANGE_START}/${IPV6_RANGE_END}
	[Teardown]	CLI:Cancel	Raw

SUITE:Setup DHCP Client To Ipv6
	CLI:Switch Connection	hostshared_session
	CLI:Enter Path	/settings/network_connections/${VLAN_INTERFACE_NAME}
	CLI:Set	ipv6_mode=stateful_dhcpv6
	CLI:Commit

SUITE:Delete DHCP Setup To Ipv6
	CLI:Switch Connection	hostdefault_session
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Delete If Exists	${IPV6_NAME_DHCP}

SUITE:Check If DHCP Client Get The Ipv6 Address From Server
	CLI:Enter Path	/settings/network_connections/
	${STATUS}=	CLI:Show
	Should Contain	${STATUS}	${DHCPV6_PREFIX}

SUITE:Check If DHCP Client Is Shown As Leased In Table
	[Arguments]	${INTERFACE}	${PROTOCOL}
	CLI:Switch Connection	hostshared_session
	CLI:Enter Path	/settings/network_connections/
	${TABLE}	CLI:Show
	${DHCP_CONNECTION}	Get Lines Containing String	${TABLE}	${INTERFACE}
	# Gets the 17 digits from MAC address column (e.g. e4:1a:2c:01:47:64)
	${MAC_ADDRESS}	Get Substring	${DHCP_CONNECTION}	-30	-13
	# For DUID the 0s on the lefts are not displayed (e.g. e4:1a:2c:1:47:64)
	${DUID}	Remove String	${MAC_ADDRESS}	0
	# For DUID the last digit is variable and depends on other metrics (e.g. e4:1a:2c:1:47:6_)
	${DUID}	Get Substring	${DUID}	0	-1
	CLI:Switch Connection	hostdefault_session
	CLI:Enter Path  /system/dhcp
	${LEASES}	CLI:Show

	Run Keyword If	'${PROTOCOL}' == 'dhcpv4'	Should Match Regexp	${LEASES}	${IPV4_RANGE_START}\\s+${MAC_ADDRESS}\\s+${HOSTNAME_NODEGRID}

	Run Keyword If	'${PROTOCOL}' == 'dhcpv6'	Should Contain	${LEASES}	${DHCPV6_PREFIX}
	#The DUID is the part that comes from the MAC address + fixed prefix (e.g. 0:3:0:1:e4:1a:2c:1:47:6_)
	Run Keyword If	'${PROTOCOL}' == 'dhcpv6'	Should Contain	${LEASES}	0:3:0:1:${DUID}