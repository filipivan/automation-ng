*** Settings ***
Resource	../../init.robot
Documentation	Validation test cases about DHCP server through the CLI.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	NETWORK	DHCP SERVER	SERVER
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SUBNET}	192.168.16.0
${NETMASK}	255.255.255.0	#Netmask only accept values in mask format, nothing of numbers
${DOMAIN}	zpesystems.com
${DNS}	192.168.16.13
${ROUTER_IP}	192.168.16.1
${NAME_DHCP}	${SUBNET}|${NETMASK}
${DHCPV6_PREFIX}	2001:db8:cafe::
${IPV6_LENGTH}	64
${IPV6_DNS}	2001:4860:4860::8888, 2001:4860:4860::8844
${IPV6_ROUTER_IP}	2001:db8:cafe::1
${IPV6_LEASE_TIME}	15
${IPV6_NAME_DHCP}	${DHCPV6_PREFIX}|${IPV6_LENGTH}
${IPV6_RANGE_START}	2001:db8:cafe::100
${IPV6_RANGE_END}	2001:db8:cafe::110
@{VALID_PROTOCOLS}	dhcp4	dhcp6
@{INVALID_PROTOCOLS}	20.20	${WORD}	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
...	${ELEVEN_WORD}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}	${EXCEEDED}	test.2021
@{VALID_IPV6}	2001:db8:cafe::	2001:ffff:cafe::	2001:4860:4860::	2001:4860:4860::	fe80::
@{INVALID_IPV6}	20.20   10.10.10	300.300.300.300	-1.-1.-1.-1	0,5.0,5.0,5.0,5	0.00.00.00	${WORD}
...	${ELEVEN_WORD}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}	${EXCEEDED}	test.2021
@{VALID_LENGTH}	64
@{INVALID_LENGTH}	20.20   10.10.10	300.300.300.300	-1.-1.-1.-1	0,5.0,5.0,5.0,5	0.00.00.00	${WORD}
...	${ELEVEN_WORD}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}	${EXCEEDED}	test.2021
${HOSTNAME}	test
${DESIRED_IP}	1.1.1.1
${MAC_ADDRESS}	00:11:22:33:44:55
${INVALID_NAME}	!@#$%¨&*()

*** Test Cases ***
Test file events available commands after send tab-tab
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Test Available Commands
	...	add                  event_system_clear   reboot               shutdown
	...	apply_settings       exit                 revert               software_upgrade
	...	cd                   factory_settings     save_settings        system_certificate
	...	change_password      hostname             set                  system_config_check
	...	commit               ls                   shell                whoami
	...	delete               pwd                  show
	...	event_system_audit   quit                 show_settings
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Available Commands
	...	cloud_enrollment	create_csr	diagnostic_data
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Available Commands
	...	exec	export_settings

Test show command
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Test Show Command
	...	subnet/netmask  domain  domain name servers  router ip
	...	==============  ======  ===================  =========
	Run Keyword If	'${NGVERSION}' >= '5.4'	CLI:Test Show Command
	...	subnet/netmask or prefix/length  domain  domain name servers  router ip  lease time
	...	===============================  ======  ===================  =========  ==========

Test show_settings command if no dhcp
	CLI:Enter Path	/settings/dhcp_server/
	${OUTPUT}=	CLI:Ls
	${OUTPUT}=	Replace String	${OUTPUT}	\n	${SPACE}
	${RESULT}=	Run Keyword And Return Status	Should Contain	${OUTPUT}	/
	Log To Console	${RESULT}
	Write	show_settings
	${OUTPUT}=	CLI:Read Until Prompt
	Run Keyword If	${RESULT} == False	Should Not Contain	${OUTPUT}	/settings/dhcp_server/

Test defining Empty values dhcp
	CLI:Enter Path	/settings/dhcp_server/
	@{LIST}=	Create List	subnet	netmask	domain	domain_name_server	router_ip
	CLI:Add
	FOR		${VALUE}	IN	@{LIST}
		Run Keyword If	'${NGVERSION}' > '4.2'	CLI:Test Set Field Invalid Options	${VALUE}	${EMPTY}	Error: subnet: Field must not be empty.Error: netmask: Field must not be empty.	yes
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${VALUE}	${EMPTY}	Error: subnet/netmask: Validation error.	yes
	END
	CLI:Delete If Exists	${NAME_DHCP}
	[Teardown]	CLI:Cancel	Raw

Test defining Empty values dhcp | subnet
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	CLI:Test Set Field Invalid Options	domain	${DOMAIN}
	CLI:Test Set Field Invalid Options	domain_name_servers	${DNS}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	subnet	${EMPTY}	Error: subnet: Field must not be empty.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	subnet	${EMPTY}	Error: subnet/netmask: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

Test defining Empty values dhcp | netmask
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Test Set Field Invalid Options	subnet	${SUBNET}
	CLI:Test Set Field Invalid Options	domain	${DOMAIN}
	CLI:Test Set Field Invalid Options	domain_name_servers	${DNS}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	netmask	${EMPTY}	Error: netmask: Field must not be empty.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	netmask	${EMPTY}	Error: subnet/netmask: Validation error.	yes
	[Teardown]	CLI:Cancel	Raw

Test defining Empty values dhcp | router_ip
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Test Set Field Invalid Options	subnet	${SUBNET}
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	CLI:Test Set Field Invalid Options	domain	${DOMAIN}
	CLI:Test Set Field Invalid Options	domain_name_servers	${DNS}
	CLI:Test Set Field Invalid Options	router_ip	${EMPTY}	save=yes
	CLI:Delete If Exists	${NAME_DHCP}
	[Teardown]	CLI:Cancel	Raw

Test defining Empty values dhcp | domain_name_servers
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Test Set Field Invalid Options	subnet	${SUBNET}
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	CLI:Test Set Field Invalid Options	domain	${DOMAIN}
	CLI:Test Set Field Invalid Options	router_ip	${ROUTER_IP}
	CLI:Test Set Field Invalid Options	domain_name_servers	${EMPTY}	save=yes
	CLI:Delete If Exists	${NAME_DHCP}
	[Teardown]	CLI:Cancel	Raw

Test defining Empty values dhcp | domain
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Test Set Field Invalid Options	subnet	${SUBNET}
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	CLI:Test Set Field Invalid Options	router_ip	${ROUTER_IP}
	CLI:Test Set Field Invalid Options	domain_name_servers	${DNS}
	CLI:Test Set Field Invalid Options	domain	${EMPTY}	save=yes
	CLI:Delete If Exists	${NAME_DHCP}
	[Teardown]	CLI:Cancel	Raw

Test defining Empty values dhcp | subnet | netmask
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Test Set Field Invalid Options	subnet	${SUBNET}
	CLI:Test Set Field Invalid Options	router_ip	${ROUTER_IP}
	CLI:Test Set Field Invalid Options	domain_name_servers	${DNS}
	CLI:Test Set Field Invalid Options	domain	${DOMAIN}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	netmask	${EMPTY}	Error: netmask: Field must not be empty.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	netmask	${EMPTY}	Error: subnet/netmask: Validation error.	yes
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	subnet	${EMPTY}	Error: subnet: Field must not be empty.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	subnet	${EMPTY}	Error: subnet/netmask: Validation error.	yes
	CLI:Test Set Field Invalid Options	subnet	${SUBNET}	save=yes
	CLI:Delete If Exists	${NAME_DHCP}
	[Teardown]	CLI:Cancel	Raw

Test defining Word values dhcp | subnet | netmask | domain
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	subnet	${WORD}	Error: subnet: Invalid SubNet	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	subnet	${WORD}	Error: subnet/netmask: Validation error.	yes
	CLI:Test Set Field Invalid Options	subnet	${SUBNET}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	netmask	${WORD}	Error: netmask: Invalid Netmask	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	netmask	${WORD}	Error: subnet/netmask: Validation error.	yes
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	CLI:Test Set Field Invalid Options	domain	${WORD}	save=yes
	CLI:Delete If Exists	${NAME_DHCP}
	[Teardown]	CLI:Cancel	Raw

Test defining Word values dhcp | dns | router_ip
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Test Set Field Invalid Options	subnet	${SUBNET}
	CLI:Test Set Field Invalid Options	domain	${DOMAIN}
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	domain_name_servers	${WORD}	Error: domain_name_servers: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	domain_name_servers	${WORD}	${EMPTY}
	CLI:Test Set Field Invalid Options	domain_name_servers	${DNS}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	router_ip	${WORD}	Error: router_ip: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	router_ip	${WORD} ${EMPTY}
	CLI:Test Set Field Invalid Options	router_ip	${ROUTER_IP}	save=yes
	CLI:Delete If Exists	${NAME_DHCP}
	[Teardown]	CLI:Cancel	Raw

Test defining Number values dhcp | subnet | netmask
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	subnet	${NUMBER}	Error: subnet: Invalid SubNet	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	subnet	${NUMBER}	${EMPTY}
	CLI:Test Set Field Invalid Options	subnet	${SUBNET}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	netmask	${NUMBER}	Error: netmask: Invalid Netmask	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	netmask	${NUMBER}	${EMPTY}
	CLI:Test Set Field Invalid Options	netmask	255.255.255.0	save=yes
	CLI:Delete If Exists	${NAME_DHCP}
	[Teardown]	CLI:Cancel	Raw

Test defining Number values dhcp | netmask=33
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Test Set Field Invalid Options	subnet	${SUBNET}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	netmask	33	Error: netmask: Invalid Netmask	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	netmask	33	${EMPTY}
	CLI:Delete If Exists	${NAME_DHCP}
	[Teardown]	CLI:Cancel	Raw

Test defining Number values dhcp | dns | router_ip
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	CLI:Test Set Field Invalid Options	subnet	${SUBNET}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	domain_name_servers	${NUMBER}	Error: domain_name_servers: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	domain_name_servers	${NUMBER}	${EMPTY}
	CLI:Test Set Field Invalid Options	domain_name_servers	${DNS}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	router_ip	${NUMBER}	Error: router_ip: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	router_ip	${NUMBER}	${EMPTY}
	CLI:Test Set Field Invalid Options	router_ip	${ROUTER_IP}	save=yes
	CLI:Delete If Exists	${NAME_DHCP}
	[Teardown]	CLI:Cancel	Raw

Test defining Points values dhcp
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	subnet	${POINTS}	Error: subnet: Invalid SubNet	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	subnet	${POINTS}	${EMPTY}
	CLI:Test Set Field Invalid Options	subnet	${SUBNET}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	netmask	${POINTS}	Error: netmask: Invalid Netmask	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	netmask	${POINTS}	${EMPTY}
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	CLI:Test Set Field Invalid Options	domain	${POINTS}	Error: domain: Validation error.	yes
	CLI:Test Set Field Invalid Options	domain	${DOMAIN}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	domain_name_servers	${POINTS}	Error: domain_name_servers: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	domain_name_servers	${POINTS}	Error: domain name servers: Validation error.	yes
	CLI:Test Set Field Invalid Options	domain_name_servers	${DNS}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	router_ip	${POINTS}	Error: router_ip: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	router_ip	${POINTS}	Error: router ip: Validation error.	yes
	CLI:Test Set Field Invalid Options	router_ip	${ROUTER_IP}	save=yes
	CLI:Delete If Exists	${NAME_DHCP}
	[Teardown]	CLI:Cancel	Raw

Test defining ONE_WORD values for dhcp | subnet | netmask | domain
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	subnet	${ONE_WORD}	Error: subnet: Invalid SubNet	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	subnet	${ONE_WORD}	${EMPTY}
	CLI:Test Set Field Invalid Options	subnet	${SUBNET}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	netmask	${ONE_WORD}	Error: netmask: Invalid Netmask	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	netmask	${ONE_WORD}	${EMPTY}
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	CLI:Test Set Field Invalid Options	domain	${ONE_WORD}	save=yes
	CLI:Delete If Exists	${NAME_DHCP}
	[Teardown]	CLI:Cancel	Raw

Test defining ONE_WORD values for dhcp | dns | router_ip
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Test Set Field Invalid Options	subnet	${SUBNET}
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	domain_name_servers	${ONE_WORD}	Error: domain_name_servers: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	domain_name_servers	${ONE_WORD} ${EMPTY}
	CLI:Test Set Field Invalid Options	domain_name_servers	${DNS}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	router_ip	${ONE_WORD}	Error: router_ip: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	router_ip	${ONE_WORD}	Error: router ip: Validation error.	yes
	CLI:Test Set Field Invalid Options	router_ip	${ROUTER_IP}	save=yes
	[Teardown]	SUITE:Teardown Add Test

Test defining ONE_NUMBER values for dhcp | subnet | netmask | domain
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	subnet	${ONE_NUMBER}	Error: subnet: Invalid SubNet	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	subnet	${ONE_NUMBER}	Error: subnet/netmask: Validation error.	yes
	CLI:Test Set Field Invalid Options	subnet	${SUBNET}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	netmask	${ONE_NUMBER}	Error: netmask: Invalid Netmask	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	netmask	${ONE_NUMBER}	Error: subnet/netmask: Validation error.	yes
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	CLI:Test Set Field Invalid Options	domain	${ONE_NUMBER}	save=yes
	[Teardown]	SUITE:Teardown Add Test

Test defining ONE_NUMBER values for dhcp | domain_name_servers | router_ip
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	CLI:Test Set Field Invalid Options	subnet	${SUBNET}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	domain_name_servers	${ONE_NUMBER}	Error: domain_name_servers: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	domain_name_servers	${ONE_NUMBER}	${EMPTY}
	CLI:Test Set Field Invalid Options	domain_name_servers	${DNS}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	router_ip	${ONE_NUMBER}	Error: router_ip: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	router_ip	${ONE_NUMBER}	${EMPTY}
	CLI:Test Set Field Invalid Options	domain_name_servers	${DNS}
	CLI:Test Set Field Invalid Options	router_ip	${ROUTER_IP}	save=yes
	[Teardown]	SUITE:Teardown Add Test

Test defining ONE_POINTS values for dhcp | subnet | netmask | domain
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	subnet	${ONE_POINTS}	Error: subnet: Invalid SubNet	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	subnet	${ONE_POINTS}	Error: subnet/netmask: Validation error.	yes
	CLI:Test Set Field Invalid Options	subnet	${SUBNET}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	netmask	${ONE_POINTS}	Error: netmask: Invalid Netmask	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	netmask	${ONE_POINTS}	Error: subnet/netmask: Validation error.	yes
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	CLI:Test Set Field Invalid Options	domain	${ONE_POINTS}	Error: domain: Validation error.	${EMPTY}
	[Teardown]	SUITE:Teardown Add Test

Test defining ONE_POINTS values for dhcp | domain_name_servers | router_ip
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	CLI:Test Set Field Invalid Options	subnet	${SUBNET}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	domain_name_servers	${ONE_POINTS}	Error: domain_name_servers: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	domain_name_servers	${ONE_POINTS}	${EMPTY}
	CLI:Test Set Field Invalid Options	domain_name_servers	${DNS}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	router_ip	${ONE_POINTS}	Error: router_ip: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	router_ip	${ONE_POINTS}	${EMPTY}
	CLI:Test Set Field Invalid Options	domain_name_servers	${DNS}
	CLI:Test Set Field Invalid Options	router_ip	${ROUTER_IP}	save=yes
	[Teardown]	SUITE:Teardown Add Test

Test defining EXCEEDED values dhcp
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	subnet	${EXCEEDED}	Error: subnet: Invalid SubNet	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	subnet	${EXCEEDED}	Error: subnet/netmask: Validation error.	yes
	CLI:Test Set Field Invalid Options	subnet	${SUBNET}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	netmask	${EXCEEDED}	Error: netmask: Invalid Netmask	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	netmask	${EXCEEDED}	Error: subnet/netmask: Validation error.	yes
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	CLI:Test Set Field Invalid Options	subnet	${SUBNET}
	CLI:Test Set Field Invalid Options	domain	${EXCEEDED}	Error: domain: Exceeded the maximum size for this field.	yes
	CLI:Test Set Field Invalid Options	domain	${DOMAIN}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	domain_name_servers	${EXCEEDED}	Error: domain_name_servers: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	domain_name_servers	${EXCEEDED}	Error: domain name servers: Validation error.	yes
	CLI:Test Set Field Invalid Options	domain_name_servers	${DNS}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	router_ip	${EXCEEDED}	Error: router_ip: Validation error.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	router_ip	${EXCEEDED}	Error: router ip: Validation error.	yes
	CLI:Test Set Field Invalid Options	router_ip	${ROUTER_IP}	save=yes
	[Teardown]	SUITE:Teardown Add Test

Test Valid Values For Field=protocol
	Skip If	'${NGVERSION}' < '5.4'
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Test Set Validate Normal Fields	protocol	@{VALID_PROTOCOLS}
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=protocol
	Skip If	'${NGVERSION}' < '5.4'
	FOR	${INVALID_PROTOCOLS}	IN	@{INVALID_PROTOCOLS}
		CLI:Enter Path	/settings/dhcp_server/
		CLI:Add
		CLI:Test Set Field Invalid Options	protocol	${INVALID_PROTOCOLS}
		...	Error: Invalid value: ${INVALID_PROTOCOLS} for parameter: protocol
		CLI:Cancel	Raw
	END
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Test Set Field Invalid Options	protocol	${EMPTY}
	...	Error: Missing value for parameter: protocol
	CLI:Cancel	Raw
	[Teardown]	CLI:Cancel	Raw

Test Valid Values For Field=prefix
	Skip If	'${NGVERSION}' < '5.4'
	FOR	${VALID_IPV6}	IN	@{VALID_IPV6}
		CLI:Enter Path	/settings/dhcp_server/
		CLI:Add
		CLI:Set	protocol=dhcp6 length=${IPV6_LENGTH}
		CLI:Test Set Field Valid Options	prefix	${VALID_IPV6}	revert=no
		CLI:Commit
		CLI:Delete	${VALID_IPV6}|${IPV6_LENGTH}
	END
	CLI:Delete If Exists	${VALID_IPV6}|${IPV6_LENGTH}
	[Teardown]    CLI:Cancel	Raw

Test Invalid Values For Field=prefix
	Skip If	'${NGVERSION}' < '5.4'
	FOR	${INVALID_IPV6}	IN	@{INVALID_IPV6}
		CLI:Enter Path	/settings/dhcp_server/
		CLI:Add
		CLI:Set	protocol=dhcp6 length=${IPV6_LENGTH}
		CLI:Test Set Field Invalid Options	prefix	${INVALID_IPV6}
		...	Error: prefix: Invalid Prefix	save=commit	#Wrong error see BUG_NG_7037
		CLI:Cancel	Raw
	END
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Set	protocol=dhcp6 length=${IPV6_LENGTH}
	CLI:Test Set Field Invalid Options	prefix	${EMPTY}
	...	Error: prefix: Field must not be empty.	save=commit
	[Teardown]	CLI:Cancel	Raw

Test Valid Values For Field=length
	Skip If	'${NGVERSION}' < '5.4'
	FOR	${VALID_LENGTH}	IN	@{VALID_LENGTH}
		CLI:Enter Path	/settings/dhcp_server/
		CLI:Add
		CLI:Set	protocol=dhcp6 prefix=${DHCPV6_PREFIX}
		CLI:Test Set Field Valid Options	length	${VALID_LENGTH}	revert=no
		Sleep	1s
		CLI:Commit
		CLI:Delete	${DHCPV6_PREFIX}|${VALID_LENGTH}
	END
	CLI:Delete If Exists	${DHCPV6_PREFIX}|${VALID_LENGTH}
	[Teardown]    CLI:Cancel	Raw

Test Invalid Values For Field=length
	Skip If	'${NGVERSION}' < '5.4'
	FOR	${INVALID_LENGTH}	IN	@{INVALID_LENGTH}
		CLI:Enter Path	/settings/dhcp_server/
		CLI:Add
		CLI:Set	protocol=dhcp6 prefix=${DHCPV6_PREFIX}
		CLI:Test Set Field Invalid Options	length	${INVALID_LENGTH}
		...	Error: length: Invalid Length	save=commit
		CLI:Cancel	Raw
	END
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Set	protocol=dhcp6 prefix=${DHCPV6_PREFIX}
	CLI:Test Set Field Invalid Options	length	${INVALID_LENGTH}
	...	Error: length: Invalid Length	save=commit
	[Teardown]	CLI:Cancel	Raw

Test valid values for field agent_circuit_id
	[Documentation]	Test some values for field agent_circuit_id and checks with show command
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Test Set Field Invalid Options	netmask	${NETMASK}
	CLI:Test Set Field Invalid Options	subnet	${SUBNET}
	CLI:Commit
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/hosts/
	CLI:Add
	CLI:Set	hostname=${HOSTNAME} ip_address=${DESIRED_IP}
	@{IDS}	Create List	vlan:interface	hostname:vlan:interface	hostname:interface	${WORD}	${NUMBER}	${POINTS}
	FOR	${ID}	IN	@{IDS}
		CLI:Test Set Field Valid Options	agent_circuit_id	${ID}	revert=no
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field agent_circuit_id
	[Documentation]	Test setting agent_circuit_id as empty and verifies the expected error message
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/hosts/
	CLI:Add
	CLI:Set	hostname=${HOSTNAME} ip_address=${DESIRED_IP}
	CLI:Set	agent_circuit_id=${EMPTY}
	${EXPECT_ERROR}	CLI:Commit	Raw
	Should Contain	${EXPECT_ERROR}	Error: agent_circuit_id: At least one of the fields (HW Address or Agent Circuit ID) must be present.
	[Teardown]	CLI:Cancel	Raw

Test valid values for field assigned_hostname
	[Documentation]	Test some values for field assigned_hostname and checks with show command
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/hosts/
	CLI:Add
	CLI:Set	hostname=${HOSTNAME} ip_address=${DESIRED_IP} hw_address=${MAC_ADDRESS}
	@{NAMES}	Create List	${WORD}	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}	${ELEVEN_WORD}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}	${EXCEEDED}
	FOR	${NAME}	IN	@{NAMES}
		CLI:Test Set Field Valid Options	assigned_hostname	${NAME}	revert=no
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field assigned_hostname
	[Documentation]	Test setting assigned_hostname as a series of invalid characters and verifies the expected error message
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/hosts/
	CLI:Add
	CLI:Set	hostname=${HOSTNAME} ip_address=${DESIRED_IP} hw_address=${MAC_ADDRESS}
	CLI:Set	assigned_hostname=${INVALID_NAME}
	${EXPECTED_ERROR}	CLI:Commit	Raw
	Should Contain	${EXPECTED_ERROR}	Error: assigned_hostname: Invalid Assigned Hostname
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Delete If Exists	${NAME_DHCP}

SUITE:Teardown
	CLI:Delete If Exists	${NAME_DHCP}
	CLI:Close Connection

SUITE:Teardown Add Test
	CLI:Cancel	Raw
	CLI:Delete If Exists	${NAME_DHCP}