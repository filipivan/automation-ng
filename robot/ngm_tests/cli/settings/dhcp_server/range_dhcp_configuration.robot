*** Settings ***
Resource	../../init.robot
Documentation	Configuration test cases about DHCP server through the CLI on Network Range path.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	NETWORK	DHCP SERVER	DHCP RANGE
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SUBNET}	192.168.16.0
${NETMASK}	255.255.255.0
${NAME_DHCP}	${SUBNET}|${NETMASK}
${IP_INVALID}	192.100.10.1
${IP_START}	192.168.16.1
${IP_END}	192.168.16.10
${RANGE}	${IP_START}|${IP_END}
${DHCPV6_PREFIX}	2001:db8:cafe::
${IPV6_LENGTH}	64
${IPV6_DNS}	2001:4860:4860::8888, 2001:4860:4860::8844
${IPV6_ROUTER_IP}	2001:db8:cafe::1
${IPV6_LEASE_TIME}	15
${IPV6_NAME_DHCP}	${DHCPV6_PREFIX}|${IPV6_LENGTH}
${IPV6_RANGE_START}	2001:db8:cafe::100
${IPV6_RANGE_END}	2001:db8:cafe::110
${DOMAIN}	zpesystems.com

*** Test Cases ***
#Test TAB-TAB
Test file events available commands after send tab-tab
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/network_range
	CLI:Test Available Commands
	...	add                  event_system_clear   reboot               shutdown
	...	apply_settings       exit                 revert               software_upgrade
	...	cd                   factory_settings     save_settings        system_certificate
	...	change_password      hostname             set                  system_config_check
	...	commit               ls                   shell                whoami
	...	delete               pwd                  show
	...	event_system_audit   quit                 show_settings
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Available Commands
	...	cloud_enrollment	create_csr	diagnostic_data
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Available Commands
	...	exec	export_settings

Test show command network_range
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/network_range/
	CLI:Test Show Command	ip range

Test set fields in network_range
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/network_range/
	CLI:Add
	CLI:Test Set Available Fields	 ip_address_start
	CLI:Test Set Available Fields	 ip_address_end
	CLI:Cancel

Test defining Empty values dhcp host
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/network_range/
	CLI:Add
	Run Keyword If	'${NGVERSION}' >= '4.2'	 CLI:Test Set Field Invalid Options	ip_address_start	${EMPTY}	Error: Invalid IP range.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_address_start	${EMPTY}	Error: ip range: Validation error.	yes
	CLI:Test Set Field Invalid Options	ip_address_start	${IP_START}
	Run Keyword If	'${NGVERSION}' >= '4.2'	 CLI:Test Set Field Invalid Options	ip_address_end	${EMPTY}	Error: Invalid IP range.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_address_end	${EMPTY}	Error: ip range: Validation error.	yes
	CLI:Test Set Field Invalid Options	ip_address_end	${IP_END}
	Run Keyword If	'${NGVERSION}' >= '4.2'	 CLI:Test Set Field Invalid Options	ip_address_start	${EMPTY}	Error: Invalid IP range.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_address_start	${EMPTY}	Error: ip range: Validation error.	yes
	CLI:Test Set Field Invalid Options	ip_address_start	${IP_START}	save=yes
	CLI:Delete If Exists	${RANGE}
	[Teardown]	CLI:Cancel	Raw

Test defining Word values dhcp host
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/network_range/
	CLI:Add
	CLI:Test Set Field Invalid Options	ip_address_start	${IP_START}
	Run Keyword If	'${NGVERSION}' >= '4.2'	 CLI:Test Set Field Invalid Options	ip_address_end	${WORD}	Error: Invalid IP range.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_address_end	${WORD}	Error: ip range: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	 CLI:Test Set Field Invalid Options	ip_address_start	${WORD}	Error: Invalid IP range.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_address_start	${WORD}	Error: ip range: Validation error.	yes
	CLI:Test Set Field Invalid Options	ip_address_end	${IP_END}
	Run Keyword If	'${NGVERSION}' >= '4.2'	 CLI:Test Set Field Invalid Options	ip_address_start	${WORD}	Error: Invalid IP range.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_address_start	${WORD}	Error: ip range: Validation error.	yes
	CLI:Test Set Field Invalid Options	ip_address_start	${IP_START}	save=yes
	CLI:Delete If Exists	${RANGE}
	[Teardown]	CLI:Cancel	Raw

Test defining Number values dhcp host
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/network_range/
	CLI:Add
	CLI:Test Set Field Invalid Options	ip_address_start	${IP_START}
	Run Keyword If	'${NGVERSION}' >= '4.2'	 CLI:Test Set Field Invalid Options	ip_address_end	${NUMBER}	Error: Invalid IP range.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_address_end	${NUMBER}	Error: ip range: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	 CLI:Test Set Field Invalid Options	ip_address_start	${NUMBER}	Error: Invalid IP range.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_address_start	${NUMBER}	Error: ip range: Validation error.	yes
	CLI:Test Set Field Invalid Options	ip_address_end	${IP_END}
	Run Keyword If	'${NGVERSION}' >= '4.2'	 CLI:Test Set Field Invalid Options	ip_address_start	${NUMBER}	Error: Invalid IP range.	 yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_address_start	${NUMBER}	Error: ip range: Validation error.	yes
	CLI:Test Set Field Invalid Options	ip_address_start	${IP_START}	save=yes
	CLI:Delete If Exists	${RANGE}
	[Teardown]	CLI:Cancel	Raw

Test defining Points values dhcp host
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/network_range/
	CLI:Add
	CLI:Test Set Field Invalid Options	ip_address_start	${IP_START}
	Run Keyword If	'${NGVERSION}' >= '4.2'	 CLI:Test Set Field Invalid Options	ip_address_end	${POINTS}	Error: Invalid IP range.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_address_end	${POINTS}	Error: ip range: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	 CLI:Test Set Field Invalid Options	ip_address_start	${POINTS}	Error: Invalid IP range.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_address_start	${POINTS}	Error: ip range: Validation error.	yes
	CLI:Test Set Field Invalid Options	ip_address_end	${IP_END}
	Run Keyword If	'${NGVERSION}' >= '4.2'	 CLI:Test Set Field Invalid Options	ip_address_start	${POINTS}	Error: Invalid IP range.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_address_start	${POINTS}	Error: ip range: Validation error.	yes
	CLI:Test Set Field Invalid Options	ip_address_start	${IP_START}	save=yes
	CLI:Delete If Exists	${RANGE}
	[Teardown]	CLI:Cancel	Raw

Test defining IP wrong dhcp host
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/network_range/
	CLI:Add
	CLI:Test Set Field Invalid Options	ip_address_start	${IP_START}
	Run Keyword If	'${NGVERSION}' >= '4.2'	 CLI:Test Set Field Invalid Options	ip_address_end	${POINTS}	Error: Invalid IP range.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_address_end	${POINTS}	Error: ip range: Validation error.	yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	 CLI:Test Set Field Invalid Options	ip_address_start	${POINTS}	Error: Invalid IP range.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_address_start	${POINTS}	Error: ip range: Validation error.	yes
	CLI:Test Set Field Invalid Options	ip_address_end	${IP_END}
	Run Keyword If	'${NGVERSION}' >= '4.2'	 CLI:Test Set Field Invalid Options	ip_address_start	${POINTS}	Error: Invalid IP range.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	ip_address_start	${POINTS}	Error: ip range: Validation error.	yes
	CLI:Test Set Field Invalid Options	ip_address_start	${IP_START}	save=yes
	CLI:Delete If Exists	${RANGE}
	[Teardown]	CLI:Cancel	Raw

Test defining Right values dhcp network_range
	CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/network_range/
	CLI:Add
	CLI:Set	ip_address_start=${IP_START} ip_address_end=${IP_END}
	CLI:Commit
	CLI:Test Show Command	 ${IP_START}/${IP_END}
	[Teardown]	CLI:Cancel	Raw

Test entering network_range
	${STATUS}=	Run Keyword And Return Status	 CLI:Enter Path	/settings/dhcp_server/${NAME_DHCP}/network_range/${RANGE}
	Run Keyword If	${STATUS}	 CLI:Test Show Command	 ip range: ${IP_START}/${IP_END}

Test Configure Range To DHCP Server To Ipv6
	Skip If	'${NGVERSION}' < '5.4'
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Add
	CLI:Set	protocol=dhcp6
	#set prefix=2001:0db8:cafe:: length=64 domain=zpesystems.com domain_name_servers=2001:4860:4860::8888, 2001:4860:4860::8844 router_ip=2001:db8:cafe::1 lease_time=15
	CLI:Set	prefix=${DHCPV6_PREFIX} length=${IPV6_LENGTH} domain=${DOMAIN} router_ip=${IPV6_ROUTER_IP} domain_name_servers="${IPV6_DNS}" lease_time=${IPV6_LEASE_TIME}
	CLI:Commit
	CLI:Test Show Command	${DHCPV6_PREFIX}/${IPV6_LENGTH}
	CLI:Enter Path	/settings/dhcp_server/${IPV6_NAME_DHCP}/network_range
	CLI:Add
	#set ip_address_start=2001:db8:cafe::100 ip_address_end=2001:db8:cafe::110
	CLI:Set	ip_address_start=${IPV6_RANGE_START} ip_address_end=${IPV6_RANGE_END}
	CLI:Commit
	CLI:Test Show Command	${IPV6_RANGE_START}/${IPV6_RANGE_END}
	[Teardown]	CLI:Cancel	Raw

Test To Delete DHCP Configuration To Ipv6
	Skip If	'${NGVERSION}' < '5.4'
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Delete If Exists	${IPV6_NAME_DHCP}

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Delete If Exists	${NAME_DHCP}
	CLI:Add
	CLI:Write	set subnet=${SUBNET} netmask=${NETMASK}
	CLI:Save

SUITE:Teardown
	CLI:Enter Path	/settings/dhcp_server/
	CLI:Delete If Exists	${NAME_DHCP}
	CLI:Close Connection