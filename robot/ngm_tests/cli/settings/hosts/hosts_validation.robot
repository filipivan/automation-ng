*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Hosts... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${IP_ADDRESS}	192.1.1.1
${HOSTNAME}	hostname
${ALIAS}	session

${NEWHOSTNAME}	newhostname
${NEWALIAS}	newsession

${INVALID_IP_WORD}	a.a.a.a
${INVALID_IP_WORD_1}	a.2.3.4
${INVALID_IP_WORD_2}	1.a.3.4
${INVALID_IP_WORD_3}	1.2.a.4
${INVALID_IP_WORD_4}	1.2.3.a

${INVALID_IP_NUMBER_1}	1.a.a.a
${INVALID_IP_NUMBER_2}	a.2.a.a
${INVALID_IP_NUMBER_3}	a.a.3.a
${INVALID_IP_NUMBER_4}	a.a.a.4

${INVALID_IP_MAX}	256.256.256.256
${INVALID_IP_MAX_1}	256.255.255.255
${INVALID_IP_MAX_2}	255.256.255.255
${INVALID_IP_MAX_3}	255.255.256.255
${INVALID_IP_MAX_4}	255.255.255.256

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/hosts/
	${list}=	Create List	cd	hostname	set	change_password	ls	shell	commit	pwd	show	event_system_audit	quit
	...	event_system_clear	reboot	shutdown	exit	revert	whoami	show_settings	add	delete
	CLI:Test Available Commands	@{list}

Test visible fields for show command
	CLI:Enter Path	/settings/hosts/
	CLI:Test Show Command	ip address	hostname	alias

Test show_settings command
	CLI:Enter Path	/settings/hosts/
	${OUTPUT}=	CLI:Write	show_settings
	#Run Keyword If	'${NGVERSION}' > '4.2'	Should Match Regexp	${OUTPUT}	/settings/hosts/::1 ip address=::1
	Run Keyword If	'${NGVERSION}' > '4.2'	Should Match Regexp	${OUTPUT}	/settings/hosts/::1 ip( |_)address=::1
	#Run Keyword If	'${NGVERSION}' > '4.2'	Should Match Regexp	${OUTPUT}	/settings/hosts/::1 hostname=localhost
	Run Keyword If	'${NGVERSION}' > '4.2'	Should Match Regexp	${OUTPUT}	/settings/hosts/::1 hostname=nodegrid

	Run Keyword If	'${NGVERSION}' > '4.2'	Should Match Regexp	${OUTPUT}	/settings/hosts/::1 alias="ip6-localhost ip6-loopback"
	#Run Keyword If	'${NGVERSION}' > '4.2'	Should Match Regexp	${OUTPUT}	/settings/hosts/fe00::0 ip address=fe00::0
	Run Keyword If	'${NGVERSION}' > '4.2'	Should Match Regexp	${OUTPUT}	/settings/hosts/fe00::0 ip( |_)address=fe00::0
	Run Keyword If	'${NGVERSION}' > '4.2'	Should Match Regexp	${OUTPUT}	/settings/hosts/fe00::0 hostname=ip6-localnet
	#Run Keyword If	'${NGVERSION}' > '4.2'	Should Match Regexp	${OUTPUT}	/settings/hosts/ff00::0 ip address=ff00::0
	Run Keyword If	'${NGVERSION}' > '4.2'	Should Match Regexp	${OUTPUT}	/settings/hosts/ff00::0 ip( |_)address=ff00::0
	Run Keyword If	'${NGVERSION}' > '4.2'	Should Match Regexp	${OUTPUT}	/settings/hosts/ff00::0 hostname=ip6-mcastprefix
	#Run Keyword If	'${NGVERSION}' > '4.2'	Should Match Regexp	${OUTPUT}	/settings/hosts/ff02::1 ip address=ff02::1
	Run Keyword If	'${NGVERSION}' > '4.2'	Should Match Regexp	${OUTPUT}	/settings/hosts/ff02::1 ip( |_)address=ff02::1
	Run Keyword If	'${NGVERSION}' > '4.2'	Should Match Regexp	${OUTPUT}	/settings/hosts/ff02::1 hostname=ip6-allnodes
	Run Keyword If	'${NGVERSION}' > '4.2'	Should Match Regexp	${OUTPUT}	/settings/hosts/ff02::2 ip( |_)address=ff02::2
	Run Keyword If	'${NGVERSION}' > '4.2'	Should Match Regexp	${OUTPUT}	/settings/hosts/ff02::2 hostname=ip6-allrouters
# in v5.0.x show_settting don't list local (127.0.0.1) in hosts page
#	Run Keyword If	'${NGVERSION}' > '4.2'	Should Match Regexp	${OUTPUT}	/settings/hosts/127.0.1.1 ip_address=127.0.1.1
#	Run Keyword If	'${NGVERSION}' > '4.2'	Should Match Regexp	${OUTPUT}	/settings/hosts/127.0.1.1 hostname=nodegrid
	Run Keyword If	'${NGVERSION}' <= '4.2'	Should Not Contain	${OUTPUT}	/settings/hosts/

Test available set fields for adding host
	CLI:Enter Path	/settings/hosts/
	CLI:Add
	CLI:Test Set Available Fields	alias	hostname	ip_address
	[Teardown]	CLI:Cancel

Test available show fields for adding host
	CLI:Enter Path	/settings/hosts/
	CLI:Add
	CLI:Test Show Command	ip_address =	hostname =	alias =
	[Teardown]	CLI:Cancel

Test defining EMPTY values host
	CLI:Enter Path	/settings/hosts/
	CLI:Add
	CLI:Test Set Field Invalid Options	alias	${ALIAS}	Error: ip_address: Field must not be empty.	yes
	CLI:Test Set Field Invalid Options	alias	${EMPTY}
	CLI:Test Set Field Invalid Options	hostname	${HOSTNAME}	Error: ip_address: Field must not be empty.	yes
	CLI:Test Set Field Invalid Options	hostname	${EMPTY}
	CLI:Test Set Field Invalid Options	ip_address	${IP_ADDRESS}	Error: hostname: Validation error.	yes

	CLI:Test Set Field Invalid Options	hostname	${EMPTY}	Error: hostname: Validation error.	yes
	CLI:Test Set Field Invalid Options	hostname	${HOSTNAME}
	CLI:Test Set Field Invalid Options	ip_address	${EMPTY}	Error: ip_address: Field must not be empty.	yes
	CLI:Test Set Field Invalid Options	ip_address	${IP_ADDRESS}
	CLI:Test Set Field Invalid Options	alias	${EMPTY}	save=yes
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete If Exists	${IP_ADDRESS}

Test defining WORD values host
	CLI:Enter Path	/settings/hosts/
	CLI:Add
	CLI:Test Set Field Invalid Options	hostname	${WORD}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ip_address	${WORD}	Error: ip_address: Invalid IP address.	yes
	Run Keyword If	'${NGVERSION}' < '4.2'	CLI:Test Set Field Invalid Options	ip_address	${WORD}	Error: ip address: Invalid IP address.	yes
	CLI:Test Set Field Invalid Options	ip_address	${IP_ADDRESS}
	CLI:Test Set Field Invalid Options	alias	${WORD}	save=yes
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete If Exists	${IP_ADDRESS}

Test defining NUMBER values host
	CLI:Enter Path	/settings/hosts/
	CLI:Add
	CLI:Test Set Field Invalid Options	hostname	${NUMBER}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ip_address	${NUMBER}	Error: ip_address: Invalid IP address.	yes
	Run Keyword If	'${NGVERSION}' < '4.2'	CLI:Test Set Field Invalid Options	ip_address	${NUMBER}	Error: ip address: Invalid IP address.	yes
	CLI:Test Set Field Invalid Options	ip_address	${IP_ADDRESS}
	CLI:Test Set Field Invalid Options	alias	${NUMBER}	save=yes
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete If Exists	${IP_ADDRESS}

Test defining POINTS values host
	CLI:Enter Path	/settings/hosts/
	CLI:Add
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ip_address	${POINTS}	Error: ip_address: Invalid IP address.Error: hostname: Validation error.	yes
	Run Keyword If	'${NGVERSION}' < '4.2'	CLI:Test Set Field Invalid Options	ip_address	${POINTS}	Error: ip address: Invalid IP address.Error: hostname: Validation error.	yes

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	alias	${POINTS}	Error: ip_address: Invalid IP address.Error: hostname: Validation error.Error: alias: Validation error.	yes
	Run Keyword If	'${NGVERSION}' < '4.2'	CLI:Test Set Field Invalid Options	alias	${POINTS}	Error: ip address: Invalid IP address.Error: hostname: Validation error.Error: alias: Validation error.	yes
	CLI:Test Set Field Invalid Options	alias	${EMPTY}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	hostname	${POINTS}	Error: ip_address: Invalid IP address.Error: hostname: Validation error.	yes
	Run Keyword If	'${NGVERSION}' < '4.2'	CLI:Test Set Field Invalid Options	hostname	${POINTS}	Error: ip address: Invalid IP address.Error: hostname: Validation error.	yes
	CLI:Test Set Field Invalid Options	hostname	${EMPTY}

	CLI:Test Set Field Invalid Options	hostname	${HOSTNAME}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ip_address	${POINTS}	Error: ip_address: Invalid IP address.	yes
	Run Keyword If	'${NGVERSION}' < '4.2'	CLI:Test Set Field Invalid Options	ip_address	${POINTS}	Error: ip address: Invalid IP address.	yes
	CLI:Test Set Field Invalid Options	ip_address	${IP_ADDRESS}
	CLI:Test Set Field Invalid Options	hostname	${POINTS}	Error: hostname: Validation error.	yes
	CLI:Test Set Field Invalid Options	hostname	${HOSTNAME}
	CLI:Test Set Field Invalid Options	alias	${POINTS}	Error: alias: Validation error.	yes
	CLI:Test Set Field Invalid Options	alias	${ALIAS}	save=yes
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete If Exists	${IP_ADDRESS}

Test defining EXCEEDED values host
	CLI:Enter Path	/settings/hosts/
	CLI:Add
	CLI:Test Set Field Invalid Options	hostname	${HOSTNAME}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ip_address	${EXCEEDED}	Error: ip_address: Invalid IP address.	yes
	Run Keyword If	'${NGVERSION}' < '4.2'	CLI:Test Set Field Invalid Options	ip_address	${EXCEEDED}	Error: ip address: Invalid IP address.	yes

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	alias	${EXCEEDED}	Error: ip_address: Invalid IP address.Error: alias: Exceeded the maximum size for this field.	yes
	Run Keyword If	'${NGVERSION}' < '4.2'	CLI:Test Set Field Invalid Options	alias	${EXCEEDED}	Error: ip address: Invalid IP address.Error: hostname: Invalid IP address.Error: alias: Validation error.	yes
	CLI:Test Set Field Invalid Options	alias	${EMPTY}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	hostname	${EXCEEDED}	Error: ip_address: Invalid IP address.Error: hostname: Exceeded the maximum size for this field.	yes
	Run Keyword If	'${NGVERSION}' < '4.2'	CLI:Test Set Field Invalid Options	hostname	${EXCEEDED}	Error: ip address: Invalid IP address.Error: hostname: Invalid IP address.Error: alias: Validation error.	yes
	CLI:Test Set Field Invalid Options	hostname	${EMPTY}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ip_address	${EXCEEDED}	Error: ip_address: Invalid IP address.Error: hostname: Validation error.	yes
	Run Keyword If	'${NGVERSION}' < '4.2'	CLI:Test Set Field Invalid Options	ip_address	${EXCEEDED}	Error: ip address: Invalid IP address.Error: hostname: Invalid IP address.Error: alias: Validation error.	yes


	CLI:Test Set Field Invalid Options	ip_address	${IP_ADDRESS}
	CLI:Test Set Field Invalid Options	hostname	${EXCEEDED}	Error: hostname: Exceeded the maximum size for this field.	yes
	CLI:Test Set Field Invalid Options	hostname	${HOSTNAME}
	CLI:Test Set Field Invalid Options	alias	${EXCEEDED}	Error: alias: Exceeded the maximum size for this field.	yes
	CLI:Test Set Field Invalid Options	alias	${ALIAS}	save=yes
	[Teardown]	Run Keywords	CLI:Cancel	Raw
	...	AND	CLI:Delete If Exists	${IP_ADDRESS}

Test defining Invalid IP value host
	CLI:Enter Path	/settings/hosts/
	CLI:Add
	CLI:Test Set Field Invalid Options	hostname	${HOSTNAME}
	CLI:Test Set Field Invalid Options	alias	${ALIAS}
	${COMMANDS}=	Create List	${INVALID_IP_WORD}	${INVALID_IP_WORD_1}	${INVALID_IP_WORD_2}	${INVALID_IP_WORD_3}
	...	${INVALID_IP_WORD_4}	${INVALID_IP_NUMBER_1}	${INVALID_IP_NUMBER_2}	${INVALID_IP_NUMBER_3}
	...	${INVALID_IP_NUMBER_4}	${INVALID_IP_MAX}	${INVALID_IP_MAX_1}	${INVALID_IP_MAX_2}
	...	${INVALID_IP_MAX_3}	${INVALID_IP_MAX_4}
	FOR	${COMMAND}	IN	@{COMMANDS}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	ip_address	${COMMAND}	Error: ip_address: Invalid IP address.	yes
		Run Keyword If	'${NGVERSION}' < '4.2'	CLI:Test Set Field Invalid Options	ip_address	${COMMAND}	Error: ip address: Invalid IP address.	yes
	END
	[Teardown]	CLI:Cancel

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/hosts/
	CLI:Delete If Exists	${IP_ADDRESS}

SUITE:Teardown
	CLI:Enter Path	/settings/hosts/
	CLI:Delete If Exists	${IP_ADDRESS}
	CLI:Close Connection