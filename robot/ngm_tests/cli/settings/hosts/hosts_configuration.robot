*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Hosts... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${IP_ADDRESS}	192.1.1.1
${HOSTNAME}	hostname
${ALIAS}	session

${NEWHOSTNAME}	newhostname
${NEWALIAS}	newsession

${INVALID_IP_WORD}	a.a.a.a
${INVALID_IP_WORD_1}	a.2.3.4
${INVALID_IP_WORD_2}	1.a.3.4
${INVALID_IP_WORD_3}	1.2.a.4
${INVALID_IP_WORD_4}	1.2.3.a

${INVALID_IP_NUMBER_1}	1.a.a.a
${INVALID_IP_NUMBER_2}	a.2.a.a
${INVALID_IP_NUMBER_3}	a.a.3.a
${INVALID_IP_NUMBER_4}	a.a.a.4

${INVALID_IP_MAX}	256.256.256.256
${INVALID_IP_MAX_1}	256.255.255.255
${INVALID_IP_MAX_2}	255.256.255.255
${INVALID_IP_MAX_3}	255.255.256.255
${INVALID_IP_MAX_4}	255.255.255.256

*** Test Cases ***
Test adding TWO HOSTS with same ip address
	Skip If	'${NGVERSION}' < '4.2'	Error message not implemented in 3.2
	CLI:Enter Path	/settings/hosts/
	CLI:Delete If Exists	${IP_ADDRESS}
	CLI:Add
	CLI:Write	set hostname=${HOSTNAME} ip_address=${IP_ADDRESS} alias=${ALIAS}
	CLI:Save
	CLI:Add
	CLI:Write	set hostname=${HOSTNAME}1 alias=${ALIAS}1
	CLI:Test Set Field Invalid Options	ip_address	${IP_ADDRESS}	Error: ip_address: Entry already exists.	yes
	CLI:Cancel
	CLI:Delete If Exists	${IP_ADDRESS}
	[Teardown]	CLI:Cancel	Raw

Test DEFINING valid values when adding host
	CLI:Enter Path	/settings/hosts/
	CLI:Add
	CLI:Write	set hostname=${HOSTNAME} ip_address=${IP_ADDRESS} alias=${ALIAS}
	CLI:Save
	[Teardown]	CLI:Cancel	Raw

Test show_settings for valid host
	CLI:Enter Path	/settings/hosts/
	Write	show_settings
	${OUTPUT}=	CLI:Read Until Prompt
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Match Regexp	${OUTPUT}	/settings/hosts/192.1.1.1 ip( |_)address=${IP_ADDRESS}
	Run Keyword If	'${NGVERSION}' < '4.2'	Should Match Regexp	${OUTPUT}	/settings/hosts/192.1.1.1 ip_address=${IP_ADDRESS}
	Should Match Regexp	${OUTPUT}	/settings/hosts/192.1.1.1 hostname=${HOSTNAME}
	Should Match Regexp	${OUTPUT}	/settings/hosts/192.1.1.1 alias=${ALIAS}

Test valid values added in host
	CLI:Enter Path	/settings/hosts/${IP_ADDRESS}
	Run Keyword If	'${NGVERSION}' < '4.2'	CLI:Test Show Command	ip_address: ${IP_ADDRESS}	hostname = ${HOSTNAME}	alias = ${ALIAS}

Test validate RIGHT values host
	CLI:Enter Path	/settings/hosts/${IP_ADDRESS}/
#	CLI:Test Validate Unmodifiable Field	ip_address	${IP_ADDRESS}
	CLI:Test Set Validate Normal Fields	hostname	${HOSTNAME}
	CLI:Test Set Validate Normal Fields	alias	${ALIAS}
	[Teardown]	CLI:Revert

Test validate EMPTY values host
	Skip If	'${NGVERSION}' < '4.2'	Error message not implemented in 3.2
	CLI:Enter Path	/settings/hosts/${IP_ADDRESS}/
	CLI:Test Set Validate Invalid Options	hostname	${EMPTY}	Error: hostname: Validation error.
	CLI:Test Set Validate Invalid Options	alias	${EMPTY}
	[Teardown]	CLI:Revert

Test validate WORD values host
	CLI:Enter Path	/settings/hosts/${IP_ADDRESS}/
	CLI:Test Set Validate Invalid Options	hostname	${WORD}
	CLI:Test Set Validate Invalid Options	alias	${WORD}
	[Teardown]	CLI:Revert

Test validate NUMBER values host
	CLI:Enter Path	/settings/hosts/${IP_ADDRESS}/
	CLI:Test Set Validate Invalid Options	hostname	${NUMBER}
	CLI:Test Set Validate Invalid Options	alias	${NUMBER}
	[Teardown]	CLI:Revert

Test validate POINTS values host
	Skip If	'${NGVERSION}' < '4.2'	Error message not implemented in 3.2
	CLI:Enter Path	/settings/hosts/${IP_ADDRESS}/
	CLI:Test Set Validate Invalid Options	hostname	${POINTS}	Error: hostname: Validation error.
	CLI:Test Set Validate Invalid Options	alias	${POINTS}	Error: alias: Validation error.
	[Teardown]	CLI:Revert

Test validate EXCEEDED values host
	Skip If	'${NGVERSION}' < '4.2'	Error message not implemented in 3.2
	CLI:Enter Path	/settings/hosts/${IP_ADDRESS}/
	CLI:Test Set Validate Invalid Options	hostname	${EXCEEDED}	Error: hostname: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	alias	${EXCEEDED}	Error: alias: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/hosts/
	CLI:Delete If Exists	${IP_ADDRESS}

SUITE:Teardown
	CLI:Enter Path	/settings/hosts/
	CLI:Delete If Exists	${IP_ADDRESS}
	CLI:Close Connection
