*** Settings ***
Resource	../../init.robot
Documentation	Functionality test cases about snmp with encryptation through the CLI.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	NON-CRITICAL

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${AUTOMATION_USERNAME}	automationtest
${AUTOMATION_PASSWORD}	12345678
${CONNECTION}	ETH0
${TREE_STATIC}	.1.3.6.1.4.1.42518.4.2.1.2.1.1.0

*** Test Cases ***

#######################################################################################################################
############################################ SHA-512 And AES-256 ######################################################
#######################################################################################################################

Test Check If System Has SNMP Protocol Support On Both Sides
	${LIST}=	Create List
	...	-a PROTOCOL	set authentication protocol (MD5|SHA|SHA-224|SHA-256|SHA-384|SHA-512)
	...	-x PROTOCOL	set privacy protocol (DES|AES|AES-192|AES-256)
	CLI:Switch Connection	default
	${OUTPUT}=	CLI:Write	shell sudo snmpget 2>&1 | grep PROTOCOL
	CLI:Should Contain All	${OUTPUT}	${LIST}
	CLI:Switch Connection	peer_session
	${OUTPUT}=	CLI:Write	shell sudo snmpget 2>&1 | grep PROTOCOL
	CLI:Should Contain All	${OUTPUT}	${LIST}

Test Enable SNMP On Both Sides
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/services/
	CLI:Set	enable_snmp_service=yes
	CLI:Commit
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	enable_snmp_service = yes
	CLI:Switch Connection	peer_session
	CLI:Enter Path	/settings/services/
	CLI:Set	enable_snmp_service=yes
	CLI:Commit
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	enable_snmp_service = yes

Test Configure SNMP With SHA-512 Authentication Protocol And AES-256 Privacy Protocol On Host Side
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Set	version=version_3
	CLI:Set	username=${AUTOMATION_USERNAME} access_type=read_only security_level=authpriv authentication_algorithm=sha-512
	CLI:Set	authentication_password=${AUTOMATION_PASSWORD} privacy_algorithm=aes-256 privacy_password=${AUTOMATION_PASSWORD}
	CLI:Commit
	${OUTPUT}=	CLI:Show
	Should Match Regexp	${OUTPUT}	${AUTOMATION_USERNAME}\\s+Version 3

Test Check On Peer If Can Establish An Encrypted SNMP Connection With Host Using SHA-512 Authentication Protocol And AES-256 Privacy Protocol
	CLI:Switch Connection	peer_session
	Set Client Configuration	prompt=$
	CLI:Write	shell
	CLI:Write	OID=${TREE_STATIC}
	${OUTPUT}=	CLI:Write	snmpget -v 3 -u ${AUTOMATION_USERNAME} -a SHA-512 -A ${AUTOMATION_PASSWORD} -x AES-256 -X ${AUTOMATION_PASSWORD} -l AuthPriv ${HOST}:161 $OID
	Set Client Configuration	prompt=]#
	CLI:Write	exit
	Should Contain	${OUTPUT}	${TREE_STATIC} = INTEGER:

Test Get Ipv6 Of The Host
	CLI:Switch Connection	default
	${HOST_IPV6}=	CLI:Get Interface Ipv6 Address	${CONNECTION}
	Set Suite Variable	${HOST_IPV6}

Test Check On Peer If Can Establish An Encrypted SNMP Connection With Host Using SHA-512 Authentication Protocol And AES-256 Privacy Protocol And Ipv6
	CLI:Switch Connection	peer_session
	Set Client Configuration	prompt=$
	CLI:Write	shell
	CLI:Write	OID=${TREE_STATIC}
	${OUTPUT}=	CLI:Write	snmpget -v 3 -u ${AUTOMATION_USERNAME} -a SHA-512 -A ${AUTOMATION_PASSWORD} -x AES-256 -X ${AUTOMATION_PASSWORD} -l AuthPriv [${HOST_IPV6}]:161 $OID
	Set Client Configuration	prompt=]#
	CLI:Write	exit
	Should Contain	${OUTPUT}	${TREE_STATIC} = INTEGER:

Test Delete SNMP Configuration With SHA-512 Authentication Protocol And AES-256 Privacy Protocol On Host Side
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Delete If Exists	${AUTOMATION_USERNAME}

#######################################################################################################################
############################################ SHA-384 And AES-192 ######################################################
#######################################################################################################################

Test Configure SNMP With SHA-384 Authentication Protocol And AES-192 Privacy Protocol On Host Side
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Set	version=version_3
	CLI:Set	username=${AUTOMATION_USERNAME} access_type=read_only security_level=authpriv authentication_algorithm=sha-384
	CLI:Set	authentication_password=${AUTOMATION_PASSWORD} privacy_algorithm=aes-192 privacy_password=${AUTOMATION_PASSWORD}
	CLI:Commit
	${OUTPUT}=	CLI:Show
	Should Match Regexp	${OUTPUT}	${AUTOMATION_USERNAME}\\s+Version 3

Test Check On Peer If Can Establish An Encrypted SNMP Connection With Host Using SHA-384 Authentication Protocol And AES-192 Privacy Protocol
	CLI:Switch Connection	peer_session
	Set Client Configuration	prompt=$
	CLI:Write	shell
	CLI:Write	OID=${TREE_STATIC}
	${OUTPUT}=	CLI:Write	snmpget -v 3 -u ${AUTOMATION_USERNAME} -a SHA-384 -A ${AUTOMATION_PASSWORD} -x AES-192 -X ${AUTOMATION_PASSWORD} -l AuthPriv ${HOST}:161 $OID
	Set Client Configuration	prompt=]#
	CLI:Write	exit
	Should Contain	${OUTPUT}	${TREE_STATIC} = INTEGER:

Test Check On Peer If Can Establish An Encrypted SNMP Connection With Host Using SHA-384 Authentication Protocol And AES-192 Privacy Protocol And Ipv6
	CLI:Switch Connection	peer_session
	Set Client Configuration	prompt=$
	CLI:Write	shell
	CLI:Write	OID=${TREE_STATIC}
	${OUTPUT}=	CLI:Write	snmpget -v 3 -u ${AUTOMATION_USERNAME} -a SHA-384 -A ${AUTOMATION_PASSWORD} -x AES-192 -X ${AUTOMATION_PASSWORD} -l AuthPriv [${HOST_IPV6}]:161 $OID
	Set Client Configuration	prompt=]#
	CLI:Write	exit
	Should Contain	${OUTPUT}	${TREE_STATIC} = INTEGER:

Test Delete SNMP Configuration With SHA-384 Authentication Protocol And AES-192 Privacy Protocol On Host Side
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Delete If Exists	${AUTOMATION_USERNAME}

#######################################################################################################################
############################################ SHA-256 And AES ##########################################################
#######################################################################################################################

Test Configure SNMP With SHA-256 Authentication Protocol And AES Privacy Protocol On Host Side
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Set	version=version_3
	CLI:Set	username=${AUTOMATION_USERNAME} access_type=read_only security_level=authpriv authentication_algorithm=sha-256
	CLI:Set	authentication_password=${AUTOMATION_PASSWORD} privacy_algorithm=aes privacy_password=${AUTOMATION_PASSWORD}
	CLI:Commit
	${OUTPUT}=	CLI:Show
	Should Match Regexp	${OUTPUT}	${AUTOMATION_USERNAME}\\s+Version 3

Test Check On Peer If Can Establish An Encrypted SNMP Connection With Host Using SHA-256 Authentication Protocol And AES Privacy Protocol
	CLI:Switch Connection	peer_session
	Set Client Configuration	prompt=$
	CLI:Write	shell
	CLI:Write	OID=${TREE_STATIC}
	${OUTPUT}=	CLI:Write	snmpget -v 3 -u ${AUTOMATION_USERNAME} -a SHA-256 -A ${AUTOMATION_PASSWORD} -x AES -X ${AUTOMATION_PASSWORD} -l AuthPriv ${HOST}:161 $OID
	Set Client Configuration	prompt=]#
	CLI:Write	exit
	Should Contain	${OUTPUT}	${TREE_STATIC} = INTEGER:

Test Check On Peer If Can Establish An Encrypted SNMP Connection With Host Using SHA-256 Authentication Protocol And AES Privacy Protocol And Ipv6
	CLI:Switch Connection	peer_session
	Set Client Configuration	prompt=$
	CLI:Write	shell
	CLI:Write	OID=${TREE_STATIC}
	${OUTPUT}=	CLI:Write	snmpget -v 3 -u ${AUTOMATION_USERNAME} -a SHA-256 -A ${AUTOMATION_PASSWORD} -x AES -X ${AUTOMATION_PASSWORD} -l AuthPriv [${HOST_IPV6}]:161 $OID
	Set Client Configuration	prompt=]#
	CLI:Write	exit
	Should Contain	${OUTPUT}	${TREE_STATIC} = INTEGER:

Test Delete SNMP Configuration With SHA-256 Authentication Protocol And AES Privacy Protocol On Host Side
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Delete If Exists	${AUTOMATION_USERNAME}

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Open	HOST_DIFF=${HOSTPEER}	session_alias=peer_session
	SUITE:Delete SNMP Configuration

SUITE:Teardown
	SUITE:Delete SNMP Configuration
	CLI:Close Connection

SUITE:Delete SNMP Configuration
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Delete If Exists	${AUTOMATION_USERNAME}