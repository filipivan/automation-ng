*** Settings ***
Resource	../../init.robot
Documentation	Test the functionality of oids related to SSD life left
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SNMP	V1_V2_V3	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SSD_LIFE_LEFT_OID}	.1.3.6.1.4.1.42518.4.2.1.1.1.19.0
${TEST_COMMUNITY}	snmpv1v2-test

*** Test Cases ***
Test If SNMP protocol is enabled
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/services/
	CLI:Set	enable_snmp_service=yes
	CLI:Commit
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	enable_snmp_service = yes

Test check if SSD life left returned by smartctl command is the same as snmpget
	${SMARTCTL_RESULT}	SUITE:Get SSD Life Left Using Smartctl
	${SNMPGET_RESULT}	SUITE:Get SSD Life Left Using Snmpget
	Skip If	${SNMPGET_RESULT} == -1	The SSD model present on this unit is not supported
	Should Be Equal		${SMARTCTL_RESULT}	${SNMPGET_RESULT}

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Connect As Root
	CLI:Switch Connection	default
	SUITE:Add SNMP Configuration

SUITE:Teardown
	SUITE:Delete SNMP Configuration
	CLI:Close Connection

SUITE:Add SNMP Configuration
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Set		community=${TEST_COMMUNITY} oid=.1 access_type=read_only
	CLI:Commit
	${COMMUNITIES}	CLI:Show
	Should Contain	${COMMUNITIES}	${TEST_COMMUNITY}

SUITE:Delete SNMP Configuration
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Delete	${TEST_COMMUNITY}_default
	CLI:Commit
	${COMMUNITIES}	CLI:Show
	Should Not Contain	${COMMUNITIES}	${TEST_COMMUNITY}

SUITE:Get SSD Life Left Using Smartctl
	CLI:Switch Connection	root_session
	${SMARTCTL_SSD_LEFT}	CLI:Write	/usr/sbin/smartctl -f hex,id -A /dev/sda|grep -E "^(0xa9|0xe7)"
	${SMARTCTL_SSD_LEFT}	Split To Lines	${SMARTCTL_SSD_LEFT}
	#gets only the last three digits of first line (SSD Life Left in percentage)
	${SMARTCTL_SSD_LEFT}	Get Substring	${SMARTCTL_SSD_LEFT}[0]	-3
	[Return]	${SMARTCTL_SSD_LEFT}

SUITE:Get SSD Life Left Using Snmpget
	CLI:Switch Connection	root_session
	${SNMPGET_SSD_LEFT}	CLI:Write	snmpget -v 2c -c ${TEST_COMMUNITY} ${HOST} ${SSD_LIFE_LEFT_OID}
	${SNMPGET_SSD_LEFT}	Split To Lines	${SNMPGET_SSD_LEFT}
	#gets only the last three digits of first line (SSD Life Left in percentage)
	${SNMPGET_SSD_LEFT}	Get Substring	${SNMPGET_SSD_LEFT}[0]	-3
	[Return]	${SNMPGET_SSD_LEFT}