*** Settings ***
Resource	../../init.robot
Documentation	Tests Validate Monitoring of LTE using SNMP
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SNMP	V1_V2_V3	VALIDATE	NON-CRITICAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SNMPV1V2_NAME}	snmpv1v2-test
${ACCESS_TYPE}	read_and_write
${OID}	.1.3.6.1.4.1.42518.4.2.1.1.7.2

*** Test Cases ***
Test LTE Monitoring via SNMP
	[Tags]	NON-CRITICAL	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
	${SNMP_OUTPUT}	SUITE:SNMPwalk Entity	${OID}
	${M2CLIENT_OUTPUT}	SUITE:Get M2CLIENT
	${CLI_OUTPUT}	SUITE:Get Wireless Info On CLI
	SUITE:Compare Outputs	${SNMP_OUTPUT}	${M2CLIENT_OUTPUT}	${CLI_OUTPUT}

*** Keywords ***
SUITE:Setup
	CLI:Open	session_alias=default
	${HAS_M2}	SUITE:Has M2 Modem
	Set Suite Variable	${HAS_M2}
	Skip If	not ${HAS_M2}	Device doesn't have M2 Modem
	SUITE:Delete All SNMP
	SUITE:Add SNMP

SUITE:Teardown
	Run Keyword If	${HAS_M2}	CLI:Switch Connection	default
	Run Keyword If	${HAS_M2}	SUITE:Delete All SNMP
	CLI:Close Connection

SUITE:SNMPwalk Entity
	[Documentation]	Gets the snmpwalk of lte monitoring
	[Arguments]	${TREE}
	${OUTPUT}=	CLI:Write	shell sudo snmpwalk -v 2c -c ${SNMPV1V2_NAME} 127.0.0.1 ${TREE}
	Should Not Contain	${OUTPUT}	No Such Object available on this agent at this OID
	[Return]	${OUTPUT}

SUITE:Get M2CLIENT
	[Documentation]	Gets the M2Client information of the M2 Modems
	${OUTPUT}=	CLI:Write	shell sudo /usr/sbin/m2client --tracklist	user=Yes
	Should Not Contain	${OUTPUT}	not supported
	[Return]	${OUTPUT}

SUITE:Add SNMP
	[Documentation]	Add a SNMP community
	[Arguments]	${COMMUNITY}=${SNMPV1V2_NAME}	${ACCESS_TYPE}=${ACCESS_TYPE}
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/snmp/v1_v2_v3
	CLI:Add
	CLI:Set	community=${COMMUNITY}
	CLI:Set	access_type=${ACCESS_TYPE}
	CLI:Commit

SUITE:Delete All SNMP
	CLI:Enter Path	/settings/snmp/v1_v2_v3
	${OUTPUT}=	CLI:Show
	${SNMP_LIST}=	Create List
	@{SNMPs}=	Split To Lines	${OUTPUT}	2	-1
	${LENGTH}=	Get Length	${SNMPs}
	Return From Keyword If	'${LENGTH}' == '0'
	FOR	${SNMP}	IN	@{SNMPs}
		Log	Start Loop for ${SNMP}	INFO
		${SNMP_SPLIT}=	Split String	${SNMP}
		SUITE:Delete SNMP	${SNMP_SPLIT}[0]
	END

SUITE:Delete SNMP
	[Arguments]	${SNMPV1V2_NAME}=${SNMPV1V2_NAME}
	CLI:Enter Path	/settings/snmp/v1_v2_v3
	CLI:Delete If Exists	${SNMPV1V2_NAME}_default

SUITE:Compare Outputs
	[Documentation]	Checks if there is one or more modems and then calls the right keyword to compare the infos
	[Arguments]	${SNMP_OUTPUT}	${M2CLIENT_OUTPUT}	${CLI_OUTPUT}
	@{MODEMS_M2CLIENT}=	Split String	${M2CLIENT_OUTPUT}	;	max_split=2	#splits in order to check it there is  more than 1 modem
	@{CLI_OUTPUT_LINE}	Split to Lines	${CLI_OUTPUT}	2
	${M2MODEM_QUANTITY}	Get Length	${MODEMS_M2CLIENT}
	${RANGE_UNIQUE_MODEM}	Create List	0	8	1	#workaround to iterate each 2 in case there is more than one modem, in this case it's only for one modem value
	Run Keyword If	'${M2MODEM_QUANTITY}'<='2'	SUITE:Compare Modem Info	${SNMP_OUTPUT}	${M2CLIENT_OUTPUT}	${CLI_OUTPUT_LINE}[0]	${RANGE_UNIQUE_MODEM}	1	-3
	...	ELSE	SUITE:Compare Multiple Modem Info	${SNMP_OUTPUT}	${MODEMS_M2CLIENT}	${CLI_OUTPUT_LINE}[0]	${CLI_OUTPUT_LINE}[1]

SUITE:Compare Modem Info
	[Documentation]	Compares the informations from M2 modem obtained from M2Client, CLI and SNMP
	[Arguments]	${SNMP_OUTPUT}	${M2CLIENT_OUTPUT}	${CLI_OUTPUT}	${RANGE}	${START_SPLIT}	${STOP_SPLIT}
	@{SNMP_LINES}	Split to Lines	${SNMP_OUTPUT}	${START_SPLIT}	${STOP_SPLIT}	#split SNMP into lines ignoring header and also signal strengh at the end once signal is variable
	@{M2CLIENT}=	Split String	${M2CLIENT_OUTPUT}	|	max_split=9	#splits the M2 client info into each of the value
	${DATA_CONSUMPTION}	Set Variable	0	#auxiliar variable to save data consumption from SNMP and use it to compare with CLI output
	${J}	Set Variable	0	#set auxiliar variable due to 2 modems cases
	FOR	${INDEX}	IN RANGE	${RANGE}[0]	${RANGE}[1]	${RANGE}[2]
		${TRASH}	${SNMP_VALUE}	Split String	${SNMP_LINES}[${INDEX}]	:${SPACE}	max_split=1
		${SNMP_VALUE}	Remove String	${SNMP_VALUE}	"
		IF	${J}==5	#workaround to compare the 5th element which is the data consumption using regexp once it on bytes on M2Client and on kB on CLI and SNMP
			Should Match Regexp	${M2CLIENT}[${J}]	${SNMP_VALUE}(?:\\d{3})?	#compares the data comsumption from M2CLIENT with the one from SNMP+3digits
			${DATA_CONSUMPTION}	Set Variable	${SNMP_VALUE}
		ELSE
			Should be equal	${M2CLIENT}[${J}]	${SNMP_VALUE}	#compares the value obtained from SNMP with the one obtained from M2Client
		END
		${J}	Evaluate	${J}+1
	END
	Should Match Regexp	${CLI_OUTPUT}	\\s*${M2CLIENT}[0]\\s*${M2CLIENT}[1]\\s*Disconnected\\s*Registered\\s*SIM\\s${M2CLIENT}[4]\\s*${DATA_CONSUMPTION}\\sk?B\\s/\\s--\\sGB\\s*${M2CLIENT}[6]\\s*${M2CLIENT}[7](\\s*.*%*)+	#Compares the value obtained from CLI with the one obtained from M2Client

SUITE:Compare Multiple Modem Info
	[Documentation]	Splits each of the modems and calls the comparation keyword for each of them
	[Arguments]	${SNMP_OUTPUT}	${MODEMS_M2CLIENT}	@{CLI_OUTPUT_LINE}
	@{RANGE_MODEM1}	Create List	0	15	2	#workaround to iterate only over 1st modem info on SNMP results
	@{RANGE_MODEM2}	Create List	1	16	2	#workaround to iterate only over 2nd modem info on SNMP results
	SUITE:Compare Modem Info	${SNMP_OUTPUT}	${MODEMS_M2CLIENT}[0]	${CLI_OUTPUT_LINE}[0]	${RANGE_MODEM1}	2	-5
	SUITE:Compare Modem Info	${SNMP_OUTPUT}	${MODEMS_M2CLIENT}[1]	${CLI_OUTPUT_LINE}[1]	${RANGE_MODEM2}	2	-5

SUITE:Get Wireless Info On CLI
	CLI:Enter Path	/system/wireless_modem/
	${OUTPUT}	CLI:Show
	[Return]	${OUTPUT}

SUITE:Has M2 Modem
	[Documentation]	Check if device has M2 modem connected
	CLI:Enter Path	/system/usb_devices
	${OUTPUT}	CLI:Show
	${HAS_M2}	Run Keyword And Return Status	Should Contain	${OUTPUT}	M2 Wireless Modem
	[Return]	${HAS_M2}