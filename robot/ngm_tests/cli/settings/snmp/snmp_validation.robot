*** Settings ***
Resource	../../init.robot
Documentation	Tests Validate fields when adding at NETWORK :: SNMP
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SNMP	V1_V2_V3	VALIDATE	ADD
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NAME_V1_V2}	snmpv1v2
${PATH_V1_V2}	${NAME_V1_V2}_${NAME_V1_V2}
${VERSION}	version v1/v2

${NAME_V1_V2_IPV6}	snmpv1v2ipv6
${PATH_V1_V2_IPV6}	${NAME_V1_V2_IPV6}_${NAME_V1_V2_IPV6}
${VERSION_IPV6}	version v1/v2 ipv6

${NAME_V3}	snmpv3
${VERSION_3}	Version v3

${OID}	.1
${OID_1}	.1.3.6.1.4.1.318.1.1.8.5.3.3.1.3.1.1.1
${OID_2}	.1.3.6.1.4.1.311

${PASSWD}	12345678

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/snmp/
	CLI:Test Available Commands
	...	add                  factory_settings     shell
	...	apply_settings       hostname             show
	...	cd                   ls                   show_settings
	...	change_password      pwd                  shutdown
	...	commit               quit                 software_upgrade
	...	delete               reboot               system_certificate
	...	event_system_audit   revert               system_config_check
	...	event_system_clear   save_settings        whoami
	...	exit                 set
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Available Commands
	...	cloud_enrollment	create_csr	diagnostic_data
	Run Keyword If	${NGVERSION} >= 5.0	CLI:Test Available Commands
	...	exec	export_settings

Test ls command
	CLI:Enter Path	/settings/snmp/
	CLI:Test Ls Command	v1_v2_v3	system

Test show_settings
	CLI:Enter Path	/settings/snmp/
	${OUTPUT}=	CLI:Show_settings
	Should Match Regexp	${OUTPUT}	/settings/snmp/system syscontact=support@zpesystems.com
	Should Match Regexp	${OUTPUT}	/settings/snmp/system syslocation="Nodegrid "
	#Run Keyword If	'${NGVERSION}' >= '4.0'	Should Match Regexp	${OUTPUT}	/settings/snmp/system snmp_engine_id=(\\d+)+(\\w+)+
	Run Keyword If	'${NGVERSION}' >= '4.0' and '${NGVERSION}' < '5.0'  Should Match Regexp	${OUTPUT}	/settings/snmp/system snmp_engine_id=(\\d+)+(\\w+)+
    #Run Keyword If	'${NGVERSION}' >= '4.0'	Should Match Regexp	${OUTPUT}	/settings/snmp/system sysname=nodegrid
    Run Keyword If	'${NGVERSION}' >= '4.0' and '${NGVERSION}' < '5.0'  Should Match Regexp	${OUTPUT}	/settings/snmp/system sysname=nodegrid

Test show command with SNMPs createds
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Show

Test file events available commands after send tab-tab path v1_v2_v3
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Test Available Commands
	...	add                  factory_settings     shell
	...	apply_settings       hostname             show
	...	cd                   ls                   show_settings
	...	change_password      pwd                  shutdown
	...	commit               quit                 software_upgrade
	...	delete               reboot               system_certificate
	...	event_system_audit   revert               system_config_check
	...	event_system_clear   save_settings        whoami
	...	exit                 set
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Available Commands
	...	cloud_enrollment	create_csr	diagnostic_data
	Run Keyword If	${NGVERSION} >= 5.0	CLI:Test Available Commands
	...	exec	export_settings

Test defining Empty values for snmp version=v1|v2 | community
	Run Keyword If	'${NGVERSION}' == '3.2'	Set Tags	NON-CRITICAL
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	community	${EMPTY}	Error: community: Field must not be empty.	yes
	CLI:Test Set Field Invalid Options	community	${NAME_V1_V2}
	CLI:Test Set Field Invalid Options	source	${EMPTY}	save=yes
	CLI:Delete If Exists	${NAME_V1_V2}_default
	[Teardown]	SUITE:Test Teardown

Test defining Empty values for snmp version=v1|v2 | source
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	community	${NAME_V1_V2}
	CLI:Test Set Field Invalid Options	source	${NAME_V1_V2}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining Empty values for snmp version=v1|v2 | oid
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	community	${NAME_V1_V2}
	CLI:Test Set Field Invalid Options	source	${NAME_V1_V2}
	CLI:Test Set Field Invalid Options	oid	${EMPTY}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining Empty values for snmp version=v1|v2
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	version	${EMPTY}	Error: Missing value for parameter: version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	version	${EMPTY}	Error: Missing value: version

	CLI:Test Set Field Invalid Options	community	${NAME_V1_V2}
	CLI:Test Set Field Invalid Options	source	${NAME_V1_V2}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	snmp_for_ipv6	${EMPTY}	Error: Missing value for parameter: snmp_for_ipv6
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	snmp_for_ipv6	${EMPTY}	Error: Missing value: snmp_for_ipv6

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	access_type	${EMPTY}	Error: Missing value for parameter: access_type
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	access_type	${EMPTY}	Error: Missing value: access_type

	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining Empty values for snmp version=3 | username
	Run Keyword If	'${NGVERSION}' == '3.2'	Set Tags	NON-CRITICAL
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	CLI:Test Set Field Invalid Options	username	${EMPTY}	Error: username: Field must not be empty.	yes
	CLI:Test Set Field Invalid Options	username	${NAME_V3}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining Empty values for snmp version=3 | authentication_password
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${NAME_V3}
	### In v3.2, authentication_password is not setted by default
	${OUTPUT}=	Run Keyword If	${NGVERSION} < 4.0	CLI:Save	Raw
	Run Keyword If	${NGVERSION} < 4.0	Should Contain	${OUTPUT}	Error: authentication_password: This field should have at least 8 characters and less than 64 and should not begin with \#.
	CLI:Test Set Field Invalid Options	authentication_password	${EMPTY}	Error: authentication_password: This field should have at least 8 characters and less than 64 and should not begin with \#.	yes
	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining Empty values for snmp version=3 | privacy_password
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${NAME_V3}
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	CLI:Test Set Field Invalid Options	privacy_password	${EMPTY}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining Empty values for snmp version=3 | oid
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${NAME_V3}
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	CLI:Test Set Field Invalid Options	oid	${EMPTY}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining Empty values for snmp version=3
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	version	${EMPTY}	Error: Missing value for parameter: version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	version	${EMPTY}	Error: Missing value: version

	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${NAME_V3}
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	security_level	${EMPTY}	Error: Missing value for parameter: security_level
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	security_level	${EMPTY}	Error: Missing value: security_level

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	authentication_algorithm	${EMPTY}	Error: Missing value for parameter: authentication_algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_algorithm	${EMPTY}	Error: Missing value: authentication_algorithm
	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	privacy_algorithm	${EMPTY}	Error: Missing value for parameter: privacy_algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	privacy_algorithm	${EMPTY}	Error: Missing value: privacy_algorithm
	CLI:Test Set Field Invalid Options	privacy_password	${PASSWD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	access_type	${EMPTY}	Error: Missing value for parameter: access_type
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	access_type	${EMPTY}	Error: Missing value: access_type

	CLI:Test Set Field Invalid Options	oid	${EMPTY}	save=yes
	[Teardown]	SUITE:Test Teardown

############ WORD ############

Test defining WORD values for snmp version=v1|v2 | community
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	community	${WORD}
	CLI:Test Set Field Invalid Options	source	${EMPTY}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining WORD values for snmp version=v1|v2 | source
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	community	${WORD}
	CLI:Test Set Field Invalid Options	source	${WORD}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining WORD values for snmp version=v1|v2 | oid
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	community	${WORD}
	CLI:Test Set Field Invalid Options	source	${WORD}
	CLI:Test Set Field Invalid Options	oid	${WORD}	Error: oid: Validation error.	yes
	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining WORD values for snmp version=v1|v2
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	version	${WORD}	Error: Invalid value: ${WORD} for parameter: version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	version	${WORD}	Error: Invalid value: ${WORD}

	CLI:Test Set Field Invalid Options	community	${WORD}
	CLI:Test Set Field Invalid Options	source	${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	snmp_for_ipv6	${WORD}	Error: Invalid value: ${WORD} for parameter: snmp_for_ipv6
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	snmp_for_ipv6	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	access_type	${WORD}	Error: Invalid value: ${WORD} for parameter: access_type
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	access_type	${WORD}	Error: Invalid value: ${WORD}

	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining WORD values for snmp version=3 | username
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	CLI:Test Set Field Invalid Options	username	${WORD}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining WORD values for snmp version=3 | authentication_password
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${WORD}
	### In v3.2, authentication_password is not setted by default
	${OUTPUT}=	Run Keyword If	${NGVERSION} < 4.0	CLI:Save	Raw
	Run Keyword If	${NGVERSION} < 4.0	Should Contain	${OUTPUT}	Error: authentication_password: This field should have at least 8 characters and less than 64 and should not begin with \#.
	CLI:Test Set Field Invalid Options	authentication_password	${WORD}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining WORD values for snmp version=3 | privacy_password
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${WORD}
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	CLI:Test Set Field Invalid Options	privacy_password	${WORD}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining WORD values for snmp version=3 | oid
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${WORD}
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	CLI:Test Set Field Invalid Options	privacy_password	${WORD}
	CLI:Test Set Field Invalid Options	oid	${WORD}	Error: oid: Validation error.	yes
	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining WORD values for snmp version=3
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	version	${WORD}	Error: Invalid value: ${WORD} for parameter: version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	version	${WORD}	Error: Invalid value: ${WORD}

	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${WORD}
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	security_level	${WORD}	Error: Invalid value: ${WORD} for parameter: security_level
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	security_level	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	authentication_algorithm	${WORD}	Error: Invalid value: ${WORD} for parameter: authentication_algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_algorithm	${WORD}	Error: Invalid value: ${WORD}
	CLI:Test Set Field Invalid Options	authentication_password	${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	privacy_algorithm	${WORD}	Error: Invalid value: ${WORD} for parameter: privacy_algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	privacy_algorithm	${WORD}	Error: Invalid value: ${WORD}
	CLI:Test Set Field Invalid Options	privacy_password	${WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	access_type	${WORD}	Error: Invalid value: ${WORD} for parameter: access_type
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	access_type	${WORD}	Error: Invalid value: ${WORD}

	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

############ NUMBER ############

Test defining NUMBER values for snmp version=v1|v2 | community
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	community	${NUMBER}
	CLI:Test Set Field Invalid Options	source	${EMPTY}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining NUMBER values for snmp version=v1|v2 | source
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	community	${NUMBER}
	CLI:Test Set Field Invalid Options	source	${NUMBER}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining NUMBER values for snmp version=v1|v2 | oid
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	community	${NUMBER}
	CLI:Test Set Field Invalid Options	source	${NUMBER}
	CLI:Test Set Field Invalid Options	oid	${NUMBER}	Error: oid: Validation error.	yes
	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining NUMBER values for snmp version=v1|v2
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	version	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	version	${NUMBER}	Error: Invalid value: ${NUMBER}

	CLI:Test Set Field Invalid Options	community	${NUMBER}
	CLI:Test Set Field Invalid Options	source	${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	snmp_for_ipv6	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: snmp_for_ipv6
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	snmp_for_ipv6	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	access_type	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: access_type
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	access_type	${NUMBER}	Error: Invalid value: ${NUMBER}

	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining NUMBER values for snmp version=3 | username
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	CLI:Test Set Field Invalid Options	username	${NUMBER}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining NUMBER values for snmp version=3 | authentication_password
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${NUMBER}
	### In v3.2, authentication_password is not setted by default
	${OUTPUT}=	Run Keyword If	${NGVERSION} < 4.0	CLI:Save	Raw
	Run Keyword If	${NGVERSION} < 4.0	Should Contain	${OUTPUT}	Error: authentication_password: This field should have at least 8 characters and less than 64 and should not begin with \#.
	CLI:Test Set Field Invalid Options	authentication_password	${NUMBER}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining NUMBER values for snmp version=3 | privacy_password
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${NUMBER}
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	CLI:Test Set Field Invalid Options	privacy_password	${NUMBER}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining NUMBER values for snmp version=3 | oid
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${NUMBER}
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	CLI:Test Set Field Invalid Options	oid	${NUMBER}	Error: oid: Validation error.	yes
	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining NUMBER values for snmp version=3
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	version	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	version	${NUMBER}	Error: Invalid value: ${NUMBER}

	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${NUMBER}
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	security_level	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: security_level
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	security_level	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	authentication_algorithm	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: authentication_algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_algorithm	${NUMBER}	Error: Invalid value: ${NUMBER}
	CLI:Test Set Field Invalid Options	authentication_password	${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	privacy_algorithm	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: privacy_algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	privacy_algorithm	${NUMBER}	Error: Invalid value: ${NUMBER}
	CLI:Test Set Field Invalid Options	privacy_password	${NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	access_type	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: access_type
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	access_type	${NUMBER}	Error: Invalid value: ${NUMBER}

	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

############ POINTS ############

Test defining POINTS values for snmp version=v1|v2 | community
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	community	/~=:	Error: community: This field contains invalid characters.	yes
	Run Keyword If	${NGVERSION} == 4.0	CLI:Test Set Field Invalid Options	community	${POINTS}
	Run Keyword If	${NGVERSION} < 4.0 or ${NGVERSION} > 4.0	CLI:Test Set Field Invalid Options	community	${POINTS}	Error: community: This field contains invalid characters.	yes
	Run Keyword If	${NGVERSION} < 4.0 or ${NGVERSION} > 4.0	CLI:Test Set Field Invalid Options	community	${NAME_V1_V2}
	Run Keyword If	${NGVERSION} == 4.0	CLI:Test Set Field Invalid Options	source	${EMPTY}	Error: source: This field contains invalid characters.	save=yes
	CLI:Cancel
	[Teardown]	SUITE:Test Teardown

Test defining POINTS values for snmp version=v1|v2 | source
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	community	${NAME_V1_V2}
	CLI:Test Set Field Invalid Options	source	${POINTS}	Error: source: Validation error.	yes
	CLI:Test Set Field Invalid Options	source	${NAME_V1_V2}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining POINTS values for snmp version=v1|v2 | oid
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	community	${NAME_V1_V2}
	CLI:Test Set Field Invalid Options	source	${NAME_V1_V2}
	CLI:Test Set Field Invalid Options	oid	${POINTS}	Error: oid: Validation error.	yes
	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining POINTS values for snmp version=v1|v2
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	version	${POINTS}	Error: Invalid value: ${POINTS} for parameter: version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	version	${POINTS}	Error: Invalid value: ${POINTS}

	CLI:Test Set Field Invalid Options	community	${NAME_V1_V2}
	CLI:Test Set Field Invalid Options	source	${NAME_V1_V2}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	snmp_for_ipv6	${POINTS}	Error: Invalid value: ${POINTS} for parameter: snmp_for_ipv6
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	snmp_for_ipv6	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	access_type	${POINTS}	Error: Invalid value: ${POINTS} for parameter: access_type
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	access_type	${POINTS}	Error: Invalid value: ${POINTS}

	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining POINTS values for snmp version=3 | username
	Run Keyword If	'${NGVERSION}' == '3.2'	Set Tags	NON-CRITICAL
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	CLI:Test Set Field Invalid Options	username	/~=:	Error: username: This field contains invalid characters.	yes

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	username	${POINTS}	Error: username: This field contains invalid characters.	save=yes
	CLI:Cancel
	[Teardown]	SUITE:Test Teardown

Test defining POINTS values for snmp version=3 | authentication_password
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${NAME_V3}
	### In v3.2, authentication_password is not setted by default
	${OUTPUT}=	Run Keyword If	${NGVERSION} < 4.0	CLI:Save	Raw
	Run Keyword If	${NGVERSION} < 4.0	Should Contain	${OUTPUT}	Error: authentication_password: This field should have at least 8 characters and less than 64 and should not begin with \#.
	CLI:Test Set Field Invalid Options	authentication_password	${POINTS}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining POINTS values for snmp version=3 | privacy_password
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${NAME_V3}
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	CLI:Test Set Field Invalid Options	privacy_password	${POINTS}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining POINTS values for snmp version=3 | oid
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${NAME_V3}
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	CLI:Test Set Field Invalid Options	privacy_password	${POINTS}
	CLI:Test Set Field Invalid Options	oid	${POINTS}	Error: oid: Validation error.	yes
	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining POINTS values for snmp version=3
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	version	${POINTS}	Error: Invalid value: ${POINTS} for parameter: version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	version	${POINTS}	Error: Invalid value: ${POINTS}

	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${NAME_V3}
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	security_level	${POINTS}	Error: Invalid value: ${POINTS} for parameter: security_level
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	security_level	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	authentication_algorithm	${POINTS}	Error: Invalid value: ${POINTS} for parameter: authentication_algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_algorithm	${POINTS}	Error: Invalid value: ${POINTS}
	CLI:Test Set Field Invalid Options	authentication_password	${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	privacy_algorithm	${POINTS}	Error: Invalid value: ${POINTS} for parameter: privacy_algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	privacy_algorithm	${POINTS}	Error: Invalid value: ${POINTS}
	CLI:Test Set Field Invalid Options	privacy_password	${POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	access_type	${POINTS}	Error: Invalid value: ${POINTS} for parameter: access_type
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	access_type	${POINTS}	Error: Invalid value: ${POINTS}

	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

############ FEW_WORD ############

Test defining FEW_WORD values for snmp version=v1|v2 | community
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	community	${ONE_WORD}
	CLI:Test Set Field Invalid Options	source	${EMPTY}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining FEW_WORD values for snmp version=v1|v2 | source
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	community	${ONE_WORD}
	CLI:Test Set Field Invalid Options	source	${ONE_WORD}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining FEW_WORD values for snmp version=v1|v2 | oid
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	community	${ONE_WORD}
	CLI:Test Set Field Invalid Options	source	${ONE_WORD}
	CLI:Test Set Field Invalid Options	oid	${ONE_WORD}	Error: oid: Validation error.	yes
	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining FEW_WORD values for snmp version=v1|v2
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	version	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	version	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	CLI:Test Set Field Invalid Options	community	${ONE_WORD}
	CLI:Test Set Field Invalid Options	source	${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	snmp_for_ipv6	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: snmp_for_ipv6
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	snmp_for_ipv6	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	access_type	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: access_type
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	access_type	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining FEW_WORD values for snmp version=3 | username
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	CLI:Test Set Field Invalid Options	username	${ONE_WORD}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining FEW_WORD values for snmp version=3 | authentication_password
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${ONE_WORD}
	### In v3.2, authentication_password is not setted by default
	${OUTPUT}=	Run Keyword If	${NGVERSION} < 4.0	CLI:Save	Raw
	Run Keyword If	${NGVERSION} < 4.0	Should Contain	${OUTPUT}	Error: authentication_password: This field should have at least 8 characters and less than 64 and should not begin with \#.
	CLI:Test Set Field Invalid Options	authentication_password	${ONE_WORD}	Error: authentication_password: This field should have at least 8 characters and less than 64 and should not begin with #.	yes
	CLI:Test Set Field Invalid Options	authentication_password	${SEVEN_WORD}	Error: authentication_password: This field should have at least 8 characters and less than 64 and should not begin with \#.	yes
	CLI:Test Set Field Invalid Options	authentication_password	${EIGHT_WORD}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining FEW_WORD values for snmp version=3 | privacy_password
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${ONE_WORD}
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	CLI:Test Set Field Invalid Options	privacy_password	${ONE_WORD}	Error: privacy_password: This field should have at least 8 characters and less than 64 and should not begin with #.	yes
	CLI:Test Set Field Invalid Options	privacy_password	${SEVEN_WORD}	Error: privacy_password: This field should have at least 8 characters and less than 64 and should not begin with \#.	yes
	CLI:Test Set Field Invalid Options	privacy_password	${EIGHT_WORD}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining FEW_WORD values for snmp version=3 | oid
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${ONE_WORD}
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	CLI:Test Set Field Invalid Options	oid	${ONE_WORD}	Error: oid: Validation error.	yes
	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining FEW_WORD values for snmp version=3
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	version	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	version	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${ONE_WORD}
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	security_level	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: security_level
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	security_level	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	authentication_algorithm	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: authentication_algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_algorithm	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}
	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	privacy_algorithm	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: privacy_algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	privacy_algorithm	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}
	CLI:Test Set Field Invalid Options	privacy_password	${PASSWD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	access_type	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: access_type
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	access_type	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

############ FEW_NUMBER ############

Test defining FEW_NUMBER values for snmp version=v1|v2 | community
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	community	${ONE_NUMBER}
	CLI:Test Set Field Invalid Options	source	${EMPTY}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining FEW_NUMBER values for snmp version=v1|v2 | source
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	community	${ONE_NUMBER}
	CLI:Test Set Field Invalid Options	source	${ONE_NUMBER}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining FEW_NUMBER values for snmp version=v1|v2 | oid
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	community	${ONE_NUMBER}
	CLI:Test Set Field Invalid Options	source	${ONE_NUMBER}
	CLI:Test Set Field Invalid Options	oid	${ONE_NUMBER}	Error: oid: Validation error.	yes
	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining FEW_NUMBER values for snmp version=v1|v2
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	version	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	version	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	CLI:Test Set Field Invalid Options	community	${ONE_NUMBER}
	CLI:Test Set Field Invalid Options	source	${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	snmp_for_ipv6	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: snmp_for_ipv6
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	snmp_for_ipv6	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	access_type	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: access_type
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	access_type	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining FEW_NUMBER values for snmp version=3 | username
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	CLI:Test Set Field Invalid Options	username	${ONE_NUMBER}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining FEW_NUMBER values for snmp version=3 | authentication_password
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${ONE_NUMBER}
	### In v3.2, authentication_password is not setted by default
	${OUTPUT}=	Run Keyword If	${NGVERSION} < 4.0	CLI:Save	Raw
	Run Keyword If	${NGVERSION} < 4.0	Should Contain	${OUTPUT}	Error: authentication_password: This field should have at least 8 characters and less than 64 and should not begin with \#.
	CLI:Test Set Field Invalid Options	authentication_password	${ONE_NUMBER}	Error: authentication_password: This field should have at least 8 characters and less than 64 and should not begin with #.	yes
	CLI:Test Set Field Invalid Options	authentication_password	${SEVEN_NUMBER}	Error: authentication_password: This field should have at least 8 characters and less than 64 and should not begin with \#.	yes
	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining FEW_NUMBER values for snmp version=3 | privacy_password
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${ONE_NUMBER}
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	CLI:Test Set Field Invalid Options	privacy_password	${ONE_NUMBER}	Error: privacy_password: This field should have at least 8 characters and less than 64 and should not begin with #.	yes
	CLI:Test Set Field Invalid Options	privacy_password	${SEVEN_NUMBER}	Error: privacy_password: This field should have at least 8 characters and less than 64 and should not begin with \#.	yes
	CLI:Test Set Field Invalid Options	privacy_password	${EIGHT_NUMBER}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining FEW_NUMBER values for snmp version=3 | oid
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${ONE_NUMBER}
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	CLI:Test Set Field Invalid Options	oid	${ONE_NUMBER}	Error: oid: Validation error.	yes
	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining FEW_NUMBER values for snmp version=3
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	version	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	version	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${ONE_NUMBER}
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	security_level	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: security_level
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	security_level	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	authentication_algorithm	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: authentication_algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_algorithm	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}
	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	privacy_algorithm	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: privacy_algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	privacy_algorithm	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}
	CLI:Test Set Field Invalid Options	privacy_password	${PASSWD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	access_type	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: access_type
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	access_type	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

############ FEW_POINTS ############

Test defining FEW_POINTS values for snmp version=v1|v2 | community
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	community	/~=:	Error: community: This field contains invalid characters.	yes
	CLI:Test Set Field Invalid Options	community	${ONE_POINTS}
	CLI:Test Set Field Invalid Options	source	${EMPTY}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining FEW_POINTS values for snmp version=v1|v2 | source
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	community	${NAME_V1_V2}
	CLI:Test Set Field Invalid Options	source	${ONE_POINTS}	Error: source: Validation error.	yes
	CLI:Test Set Field Invalid Options	source	${NAME_V1_V2}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining FEW_POINTS values for snmp version=v1|v2 | oid
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	community	${NAME_V1_V2}
	CLI:Test Set Field Invalid Options	source	${NAME_V1_V2}
	CLI:Test Set Field Invalid Options	oid	${ONE_POINTS}	Error: oid: Validation error.	yes
	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining FEW_POINTS values for snmp version=v1|v2
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	version	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	version	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	CLI:Test Set Field Invalid Options	community	${NAME_V1_V2}
	CLI:Test Set Field Invalid Options	source	${NAME_V1_V2}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	snmp_for_ipv6	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: snmp_for_ipv6
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	snmp_for_ipv6	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	access_type	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: access_type
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	access_type	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining FEW_POINTS values for snmp version=3 | username
	Run Keyword If	'${NGVERSION}' == '3.2'	Set Tags	NON-CRITICAL	NEED-REVIEW
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	CLI:Test Set Field Invalid Options	username	/~=:	Error: username: This field contains invalid characters.	yes
	CLI:Test Set Field Invalid Options	username	${ONE_POINTS}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining FEW_POINTS values for snmp version=3 | authentication_password
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${NAME_V3}
	### In v3.2, authentication_password is not setted by default
	${OUTPUT}=	Run Keyword If	${NGVERSION} < 4.0	CLI:Save	Raw
	Run Keyword If	${NGVERSION} < 4.0	Should Contain	${OUTPUT}	Error: authentication_password: This field should have at least 8 characters and less than 64 and should not begin with \#.
	CLI:Test Set Field Invalid Options	authentication_password	${ONE_POINTS}	Error: authentication_password: This field should have at least 8 characters and less than 64 and should not begin with \#.	yes
	CLI:Test Set Field Invalid Options	authentication_password	${SEVEN_POINTS}	Error: authentication_password: This field should have at least 8 characters and less than 64 and should not begin with \#.	yes
	CLI:Test Set Field Invalid Options	authentication_password	\#	Error: authentication_password: This field should have at least 8 characters and less than 64 and should not begin with \#.	yes
	CLI:Test Set Field Invalid Options	authentication_password	${EIGHT_POINTS}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining FEW_POINTS values for snmp version=3 | privacy_password
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${NAME_V3}
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	CLI:Test Set Field Invalid Options	privacy_password	${ONE_POINTS}	Error: privacy_password: This field should have at least 8 characters and less than 64 and should not begin with \#.	yes
	CLI:Test Set Field Invalid Options	privacy_password	${SEVEN_POINTS}	Error: privacy_password: This field should have at least 8 characters and less than 64 and should not begin with \#.	yes
	CLI:Test Set Field Invalid Options	privacy_password	\#	Error: privacy_password: This field should have at least 8 characters and less than 64 and should not begin with \#.	yes
	CLI:Test Set Field Invalid Options	privacy_password	${EIGHT_POINTS}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining FEW_POINTS values for snmp version=3 | oid
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${NAME_V3}
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	CLI:Test Set Field Invalid Options	oid	${ONE_POINTS}	Error: oid: Validation error.	yes
	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining FEW_POINTS values for snmp version=3
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	version	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	version	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${NAME_V3}
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	security_level	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: security_level
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	security_level	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	authentication_algorithm	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: authentication_algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_algorithm	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}
	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	privacy_algorithm	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: privacy_algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	privacy_algorithm	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}
	CLI:Test Set Field Invalid Options	privacy_password	${PASSWD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	access_type	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: access_type
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	access_type	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

############ EXCEEDED ############

Test defining EXCEEDED values for snmp version=v1|v2 | community
	Run Keyword If	'${NGVERSION}' == '3.2'	Set Tags	NON-CRITICAL
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	source	${EMPTY}
	#CLI:Test Set Field Invalid Options	community	${EXCEEDED}	Error: community: Exceeded the maximum size for this field.	yes
	CLI:Test Set Field Invalid Options	community	${EXCEEDED}	Error: Exceeded the maximum size for this field.	yes
	CLI:Cancel
	[Teardown]	SUITE:Test Teardown

Test defining EXCEEDED values for snmp version=v1|v2 | source
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	community	${NAME_V1_V2}
	CLI:Test Set Field Invalid Options	source	${EXCEEDED}	Error: source: Validation error.	yes
	CLI:Test Set Field Invalid Options	source	${NAME_V1_V2}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining EXCEEDED values for snmp version=v1|v2 | oid
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	community	${NAME_V1_V2}
	CLI:Test Set Field Invalid Options	source	${NAME_V1_V2}
	CLI:Test Set Field Invalid Options	oid	${EXCEEDED}	Error: oid: Exceeded the maximum size for this field.	yes
	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining EXCEEDED values for snmp version=v1|v2
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	version	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	version	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}

	CLI:Test Set Field Invalid Options	community	${NAME_V1_V2}
	CLI:Test Set Field Invalid Options	source	${NAME_V1_V2}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	snmp_for_ipv6	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: snmp_for_ipv6
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	snmp_for_ipv6	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	access_type	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: access_type
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	access_type	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}

	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining EXCEEDED values for snmp version=3 | username
	Run Keyword If	'${NGVERSION}' == '3.2'	Set Tags	NON-CRITICAL
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	#CLI:Test Set Field Invalid Options	username	${EXCEEDED}	Error: username: Exceeded the maximum size for this field.	yes
	CLI:Test Set Field Invalid Options	username	${EXCEEDED}	Error: Exceeded the maximum size for this field.    yes

	CLI:Cancel
	[Teardown]	SUITE:Test Teardown

Test defining EXCEEDED values for snmp version=3 | authentication_password
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${NAME_V3}
	### In v3.2, authentication_password is not setted by default
	${OUTPUT}=	Run Keyword If	${NGVERSION} < 4.0	CLI:Save	Raw
	Run Keyword If	${NGVERSION} < 4.0	Should Contain	${OUTPUT}	Error: authentication_password: This field should have at least 8 characters and less than 64 and should not begin with \#.
##############	CLI:Test Set Field Invalid Options	authentication_password	${EXCEEDED}	Error: authentication_password: This field should have at least 8 characters and less than 64 and should not begin with \#.	yes
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${EXCEEDED}	Error: authentication_password: Exceeded the maximum size for this field.	yes
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	authentication_password	${EXCEEDED}	Error: authentication_password: This field should have at least 8 characters and less than 64 and should not begin with #.	yes
	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining EXCEEDED values for snmp version=3 | privacy_password
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${NAME_V3}
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
##############	CLI:Test Set Field Invalid Options	privacy_password	${EXCEEDED}	Error: privacy_password: This field should have at least 8 characters and less than 64 and should not begin with \#.	yes
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	privacy_password	${EXCEEDED}	Error: privacy_password: Exceeded the maximum size for this field.	yes
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	privacy_password	${EXCEEDED}	Error: privacy_password: This field should have at least 8 characters and less than 64 and should not begin with #.	yes
	CLI:Test Set Field Invalid Options	privacy_password	${PASSWD}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining EXCEEDED values for snmp version=3 | oid
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${NAME_V3}
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	CLI:Test Set Field Invalid Options	oid	${EXCEEDED}	Error: oid: Exceeded the maximum size for this field.	yes
	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

Test defining EXCEEDED values for snmp version=3
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	version	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: version
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	version	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}

	CLI:Test Set Field Invalid Options	version	version_3
	CLI:Test Set Field Invalid Options	username	${NAME_V3}
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	security_level	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: security_level
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	security_level	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	authentication_algorithm	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: authentication_algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	authentication_algorithm	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	privacy_algorithm	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: privacy_algorithm
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	privacy_algorithm	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}
	CLI:Test Set Field Invalid Options	privacy_password	${PASSWD}

	Run Keyword If	${NGVERSION} >= 4.0	CLI:Test Set Field Invalid Options	access_type	${EXCEEDED}	Error: Invalid value: ${EXCEEDED} for parameter: access_type
	Run Keyword If	${NGVERSION} < 4.0	CLI:Test Set Field Invalid Options	access_type	${EXCEEDED}	Error: Invalid value: ${EXCEEDED}

	CLI:Test Set Field Invalid Options	oid	${OID}	save=yes
	[Teardown]	SUITE:Test Teardown

*** Keywords ***
SUITE:Setup
	Wait Until Keyword Succeeds	5x	5s	CLI:Open
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	SUITE:Delete SNMPs

SUITE:Teardown
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	SUITE:Delete SNMPs
	CLI:Close Connection

SUITE:Test Teardown
	CLI:Cancel	Raw
	SUITE:Delete SNMPs

SUITE:Delete SNMPs
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Delete If Exists	${PATH_V1_V2}	${NAME_V1_V2}_default
	...	${PATH_V1_V2_IPV6}	${NAME_V1_V2_IPV6}_default6
	...	${NAME_V3}	${WORD}_${WORD}	${WORD}_default	${WORD}
	...	${NUMBER}_${NUMBER}	${NUMBER}_default	${NUMBER}
	...	${POINTS}_${NAME_V1_V2}	${POINTS}_default	${POINTS}
	...	${ONE_NUMBER}_${ONE_NUMBER}	${ONE_NUMBER}_default	${ONE_NUMBER}
	...	${ONE_WORD}_${ONE_WORD}	${ONE_WORD}_default	${ONE_WORD}
	...	${ONE_POINTS}_${ONE_POINTS}	${ONE_POINTS}_default	${ONE_POINTS}
	...	${EXCEEDED}_${EXCEEDED}	${EXCEEDED}_default	${EXCEEDED}
	#Delete snmp v3 in Nodegrid v3.2
	CLI:Delete If Exists	${NAME_V3}_	${WORD}_	${NUMBER}_	${POINTS}_
	...	${ONE_NUMBER}_	${ONE_WORD}_	${ONE_POINTS}_	${EXCEEDED}_

	#Delete SNMP of BUG_716 in Trello
	CLI:Delete If Exists	!@-$%^|_()_default	!@-$%^|_()_!@-$%^|_()
	...	!@-$%^|_()_default6