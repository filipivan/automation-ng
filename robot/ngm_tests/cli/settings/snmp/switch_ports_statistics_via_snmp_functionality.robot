*** Settings ***
Resource	../../init.robot
Documentation	Functionality test cases about snmp statistics using switch interfaces through the CLI.
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
Default Tags	CLI	SSH	SNMP	V1_V2_V3

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SNMP_COMMUNITY}	private
${TREE_STATIC}	.1.3.6.1.4.1.42518.4.2.1.3.1.1.2.1

*** Test Cases ***
Test Switch Interfaces statistics for descriptions
	Skip If		not ${SWITCH_SNMP_SFP_AVAILABLE}	SNMP tests for SFP ports are not configured for this build
	Skip If		not ${SWITCH_SNMP_NETPORT_AVAILABLE}	SNMP tests for NET ports are not configured for this build
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/switch_interfaces/
	@{INTERFACES}=	CLI:Get Switch Interfaces
	${LENGTH}=	Get Length	${INTERFACES}
	${END}=	Set Variable	${LENGTH - 2}
	FOR	${INDEX}	${INTERFACE}	IN ENUMERATE	@{INTERFACES}
		${OID}=	SUITE:Convert Interface Name Into OID	${INTERFACE}	description
		${ORIGINAL_DESCRIPTION}=	SUITE:Run SNMPget	${OID}	description
		SUITE:Change Switch Interface Configuration	${INTERFACE}	description	"test ${INTERFACE} description"
		${CHECK_DESCRIPTION}=	SUITE:Run SNMPget	${OID}	description
		Should Be Equal	${CHECK_DESCRIPTION}	"test ${INTERFACE} description"
		SUITE:Change Switch Interface Configuration	${INTERFACE}	description	${ORIGINAL_DESCRIPTION}
		${CHECK_DESCRIPTION}=	SUITE:Run SNMPget	${OID}	description
		Should Be Equal	${CHECK_DESCRIPTION}	${ORIGINAL_DESCRIPTION}
		Run Keyword If	'${INDEX}' == '${END}'	Exit For Loop
	END
Test Switch Interfaces statistics for status
	Skip If		not ${SWITCH_SNMP_SFP_AVAILABLE}	SNMP tests for SFP ports are not configured for this build
	Skip If		not ${SWITCH_SNMP_NETPORT_AVAILABLE}	SNMP tests for NET ports are not configured for this build
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/switch_interfaces/
	@{INTERFACES}=	CLI:Get Switch Interfaces
	${LENGTH}=	Get Length	${INTERFACES}
	${END}=	Set Variable	${LENGTH - 2}
	FOR	${INDEX}	${INTERFACE}	IN ENUMERATE	@{INTERFACES}
		${OID}=	SUITE:Convert Interface Name Into OID	${INTERFACE}	status
		${ORIGINAL_STATUS}=	SUITE:Run SNMPget	${OID}	status
		SUITE:Change Switch Interface Configuration	${INTERFACE}	status	disabled
		${CHECK_STATUS}=	SUITE:Run SNMPget	${OID}	status
		Should Be Equal	${CHECK_STATUS}	disabled
		SUITE:Change Switch Interface Configuration	${INTERFACE}	status	${ORIGINAL_STATUS}
		${CHECK_STATUS}=	SUITE:Run SNMPget	${OID}	status
		Should Be Equal	${CHECK_STATUS}	${ORIGINAL_STATUS}
		Run Keyword If	'${INDEX}' == '${END}'	Exit For Loop
	END
	CLI:Enable Switch Interface	sfp0
	CLI:Enable Switch Interface	sfp1

Test Switch Interfaces statistics for speed
	Skip If		not ${SWITCH_SNMP_SFP_AVAILABLE}	SNMP tests for SFP ports are not configured for this build
	Skip If		not ${SWITCH_SNMP_NETPORT_AVAILABLE}	SNMP tests for NET ports are not configured for this build
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/switch_interfaces/
	@{INTERFACES}=	Create List	${SFP_HOST}	${NETPORT_HOST}
	${LENGTH}=	Get Length	${INTERFACES}
	${END}=	Set Variable	${LENGTH - 2}
	FOR	${INDEX}	IN RANGE	${LENGTH}
		${INTERFACE}=	Get From List	${INTERFACES}	${INDEX}
		CLI:Enable Switch Interface	${INTERFACE}
		${OID}=	SUITE:Convert Interface Name Into OID	${INTERFACE}	speed
		${ORIGINAL_SPEED}=	SUITE:Run SNMPget	${OID}	speed
		SUITE:Change Switch Interface Configuration	${INTERFACE}	speed	10m
		Sleep	3s
		${CHECK_SPEED}=	SUITE:Run SNMPget	${OID}	speed
		Should Be Equal	${CHECK_SPEED}	10m
		SUITE:Change Switch Interface Configuration	${INTERFACE}	speed	${ORIGINAL_SPEED}
		Sleep	3s
		${CHECK_SPEED}=	SUITE:Run SNMPget	${OID}	speed
		Should Be Equal	${CHECK_SPEED}	${ORIGINAL_SPEED}
		CLI:Disable Switch Interface	${INTERFACE}
		Run Keyword If	'${INDEX}' == '${END}'	Exit For Loop
	END
	CLI:Enable Switch Interface	sfp0
	CLI:Enable Switch Interface	sfp1

Test Switch Interfaces statistics for packets received
	Skip If		not ${SWITCH_SNMP_SFP_AVAILABLE}	SNMP tests for SFP ports are not configured for this build
	Skip If		not ${SWITCH_SNMP_NETPORT_AVAILABLE}	SNMP tests for NET ports are not configured for this build
	SUITE:Create VLAN
	${OID}=	SUITE:Convert Interface Name Into OID	${SFP_HOST}	Rx
	${RX_BEFORE_PING}=	SUITE:Run SNMPget	${OID}	packets
	CLI:Switch Connection	shared_session
	CLI:Test Ping	10.0.0.80	NUM_PACKETS=5	SOURCE_INTERFACE=backplane1.80	TIMEOUT=20
	Sleep	5s	#Sometimes it takes some time to update the counters
	${RX_AFTER_PING}=	SUITE:Run SNMPget	${OID}	packets
	${RECEIVING}=	Evaluate	${RX_AFTER_PING}>${RX_BEFORE_PING}
	Should Be True	${RECEIVING}

Test Switch Interfaces statistics for packets transmitted
	Skip If		not ${SWITCH_SNMP_SFP_AVAILABLE}	SNMP tests for SFP ports are not configured for this build
	Skip If		not ${SWITCH_SNMP_NETPORT_AVAILABLE}	SNMP tests for NET ports are not configured for this build
	${OID}=	SUITE:Convert Interface Name Into OID	${SFP_HOST}	Tx
	${TX_BEFORE_PING}=	SUITE:Run SNMPget	${OID}	packets
	CLI:Switch Connection	default
	CLI:Test Ping	10.0.0.82	NUM_PACKETS=5	SOURCE_INTERFACE=backplane1.80	TIMEOUT=20
	Sleep	5s	#Sometimes it takes some time to update the counters
	${TX_AFTER_PING}=	SUITE:Run SNMPget	${OID}	packets
	${TRANSMITTING}=	Evaluate	${TX_AFTER_PING}>${TX_BEFORE_PING}
	Should Be True	${TRANSMITTING}

*** Keywords ***
SUITE:Setup
	Skip If		not ${SWITCH_SNMP_SFP_AVAILABLE}	SNMP tests for SFP ports are not configured for this build
	Skip If		not ${SWITCH_SNMP_NETPORT_AVAILABLE}	SNMP tests for NET ports are not configured for this build

	Set Suite Variable	${SFP_HOST}	${SWITCH_SFP_HOST_INTERFACE1}
	Set Suite Variable	${SFP_SHARED}	${SWITCH_SFP_SHARED_INTERFACE1}
	Set Suite Variable	${NETPORT_HOST}	${SWITCH_NETPORT_HOST_INTERFACE1}
	Set Suite Variable	${NETPORT_SHARED}	${SWITCH_NETPORT_SHARED_INTERFACE1}

	CLI:Open
	CLI:Connect As Root
	CLI:Open	HOST_DIFF=${HOSTSHARED}	session_alias=shared_session
	CLI:Connect As Root	HOST_DIFF=${HOSTSHARED}	session_alias=shared_root_session
	SUITE:Delete SNMP Configuration
	SUITE:Add SNMP Configuration
	SUITE:Delete VLAN

SUITE:Teardown
	Skip If		not ${SWITCH_SNMP_SFP_AVAILABLE}	SNMP tests for SFP ports are not configured for this build
	Skip If		not ${SWITCH_SNMP_NETPORT_AVAILABLE}	SNMP tests for NET ports are not configured for this build

	SUITE:Delete VLAN
	SUITE:Delete SNMP Configuration
	CLI:Close Connection

SUITE:Add SNMP Configuration
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Set	community=${SNMP_COMMUNITY} oid=.1
	CLI:Commit
	CLI:Switch Connection	shared_session
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Set	community=${SNMP_COMMUNITY} oid=.1
	CLI:Commit

SUITE:Delete SNMP Configuration
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Delete If Exists	${SNMP_COMMUNITY}_default
	CLI:Commit
	CLI:Switch Connection	shared_session
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Delete If Exists	${SNMP_COMMUNITY}_default
	CLI:Commit

SUITE:Run SNMPget
	[Arguments]	${OID}	${TYPE}
	${IGNORE}=	Get Length	${OID}
	${EQUALS}=	Run Keyword If	'${TYPE}' == 'description'	Set Variable	7
	...	ELSE	Run Keyword If	'${TYPE}' == 'packets'	Set Variable	9
	...	ELSE	Set Variable	8
	${CONST}=	Set Variable	4
	${IGNORE}=	Evaluate	${IGNORE}+${EQUALS}+${CONST}
	CLI:Switch Connection	 root_session
	${STATISTIC}=	CLI:Write	snmpget -v 2c -c ${SNMP_COMMUNITY} ${HOST} ${OID}
	${STATISTIC}=	Get Lines Containing String	${STATISTIC}	${OID}
	${STATISTIC}=	Get Substring	${STATISTIC}	${IGNORE}
	Log To Console	${STATISTIC}
	${STATISTIC}=	Run Keyword If	'${TYPE}' == 'description'	Set Variable	${STATISTIC}
	...	 ELSE	Run Keyword If	'${TYPE}' == 'status' and '${STATISTIC}' == '3'	Set Variable	enabled
	...	 ELSE	Run Keyword If	'${TYPE}' == 'status' and '${STATISTIC}' == '0'	Set Variable	disabled
	...	ELSE	Run Keyword If	'${TYPE}' == 'speed' and '${STATISTIC}' == '0'	Set Variable	10m
	...	ELSE	Run Keyword If	'${TYPE}' == 'speed' and '${STATISTIC}' == '10'	Set Variable	10m
	...	ELSE	Run Keyword If	'${TYPE}' == 'speed' and '${STATISTIC}' == '100'	Set Variable	100m
	...	ELSE	Run Keyword If	'${TYPE}' == 'speed' and '${STATISTIC}' == '1000'	Set Variable	1g
	...	ELSE	Run Keyword If	'${TYPE}' == 'speed' and '${STATISTIC}' == '10000'	Set Variable	10g
	...	ELSE	Run Keyword If	'${TYPE}' == 'packets'	Convert To Integer	${STATISTIC}
	Set Suite Variable	${STATISTIC}
	[Return]	${STATISTIC}

SUITE:Change Switch Interface Configuration
	[Arguments]	${INTERFACE}	${FIELD}	${VALUE}
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/switch_interfaces/${INTERFACE}
	CLI:Set	${FIELD}=${VALUE}
	CLI:Commit

SUITE:Create VLAN
	CLI:Switch Connection	default
	CLI:Add VLAN	80	TAGGED_PORTS=${SFP_HOST},backplane1
	CLI:Add Static IPv4 Network Connection	vlan80	10.0.0.80	TYPE=vlan	INTERFACE=backplane1	BITMASK_SIZE=24	VLAN_ID=80
	CLI:Enable Switch Interface	sfp0	VLAN_ID=80
	CLI:Enable Switch Interface	sfp1	VLAN_ID=80
	CLI:Switch Connection	shared_session
	CLI:Add VLAN	80	TAGGED_PORTS=${SFP_SHARED},backplane1
	CLI:Add Static IPv4 Network Connection	vlan80	10.0.0.82	TYPE=vlan	INTERFACE=backplane1	BITMASK_SIZE=24	VLAN_ID=80
	CLI:Enable Switch Interface	sfp0	VLAN_ID=80
	CLI:Enable Switch Interface	sfp1	VLAN_ID=80

SUITE:Delete VLAN
	CLI:Switch Connection	default
	CLI:Disable All Switch Interfaces
	CLI:Delete VLAN	80
	CLI:Delete Network Connections	vlan80
	CLI:Enable Switch Interface	sfp0
	CLI:Enable Switch Interface	sfp1
	CLI:Switch Connection	shared_session
	CLI:Disable All Switch Interfaces
	CLI:Delete VLAN	80
	CLI:Delete Network Connections	vlan80
	CLI:Enable Switch Interface	sfp0
	CLI:Enable Switch Interface	sfp1

SUITE:Convert Interface Name Into OID
	[Arguments]	${NAME}	${STAT}
	${OID}=	 Set Variable	${TREE_STATIC}
	#First (and second in Tx case) Digit(s) is(are) related to the statistic
	${OID}=	Run Keyword If	'${STAT}' == 'description'	Set Variable	${OID}.4
	...	ELSE	Run Keyword If	'${STAT}' == 'status'	Set Variable	${OID}.5
	...	ELSE	Run Keyword If	'${STAT}' == 'speed'	Set Variable	${OID}.7
	...	ELSE	Run Keyword If	'${STAT}' == 'Rx'	Set Variable	${OID}.8
	...	ELSE	Run Keyword If	'${STAT}' == 'Tx'	Set Variable	${OID}.12
	#Second digit is related to port type (0 if SFP and nothing if NET)
	${IS_SFP}	Run Keyword And Return Status	Should Contain	${NAME}	sfp
	${OID}	Set Variable If	'${IS_SFP}' == 'True'	${OID}.0	${OID}
	#Second and/or Third digits are related to the name
	${OID_END}=	Run Keyword If	'${IS_SFP}' == 'True'	SUITE:Get SFP OID Based On The Name	${NAME}
	...	ELSE	SUITE:Get NET OID Based On The Name	${NAME}
	${OID}=	Set Variable	${OID}.${OID_END}
	Log To Console	${OID}
	Set Suite Variable	${OID}
	Run Keyword If	'${STAT}' == 'speed'	Set Suite Variable	${IS_SFP}
	[Return]	${OID}

SUITE:Get SFP OID Based On The Name
	[Arguments]	${NAME}
	${OID_END}	Get Substring	${NAME}	3	4
	[Return]	${OID_END}

SUITE:Get NET OID Based On The Name
	[Arguments]	${NAME}
	${OID_END}	Get Substring	${NAME}	4	5
	${NAME}	Get Substring	${NAME}	6	8
	${OID_END}	Set Variable	${OID_END}.${NAME}
	[Return]	${OID_END}