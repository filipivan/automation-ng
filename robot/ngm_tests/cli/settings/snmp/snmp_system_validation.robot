*** Settings ***
Resource	../../init.robot
Documentation	Tests at NETWORK :: SNMP :: SYSTEM
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	NETWORK	SNMP	SYSTEM
Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Variables ***
${NAME}	nodegrid
${SYSCONTACT}	support@zpesystems.com
${SYSLOCATION}	Nodegrid

*** Test Cases ***
Test file events available commands after send tab-tab
	CLI:Enter Path	/settings/snmp/system
	CLI:Test Available Commands
	...	apply_settings       hostname             shell
	...	cd                   ls                   show
	...	change_password      pwd                  show_settings
	...	commit               quit                 shutdown
	...	event_system_audit   reboot               software_upgrade
	...	event_system_clear   revert               system_certificate
	...	exit                 save_settings        system_config_check
	...	factory_settings     set                  whoami
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Available Commands
	...	cloud_enrollment	create_csr	diagnostic_data
	Run Keyword If	${NGVERSION} >= 5.0	CLI:Test Available Commands
	...	exec	export_settings

Test show command
	CLI:Enter Path	/settings/snmp/system/
	CLI:Show

Test unmodifiable fields
	CLI:Enter Path	/settings/snmp/system/
	Run Keyword If	${NGVERSION} >= 4.2	CLI:Test Validate Unmodifiable Field	sysname	${NAME}
	[Teardown]	CLI:Revert

Test correct values for system
	CLI:Enter Path	/settings/snmp/system/
	CLI:Test Set Validate Normal Fields	syscontact	${SYSCONTACT}
	CLI:Test Set Validate Normal Fields	syslocation	${SYSLOCATION}
	[Teardown]	CLI:Revert

Test Validate Empty values for system
	CLI:Enter Path	/settings/snmp/system/
	CLI:Test Set Validate Invalid Options	syscontact	${EMPTY}	Error: syscontact: Validation error.
	CLI:Test Set Validate Invalid Options	syslocation	${EMPTY}	Error: syslocation: Validation error.
	[Teardown]	CLI:Revert

Test Validate WORD values for system
	CLI:Enter Path	/settings/snmp/system/
	CLI:Test Set Validate Invalid Options	syscontact	${WORD}
	CLI:Test Set Validate Invalid Options	syslocation	${WORD}
	[Teardown]	CLI:Revert

Test Validate NUMBER values for system
	CLI:Enter Path	/settings/snmp/system/
	CLI:Test Set Validate Invalid Options	syscontact	${NUMBER}
	CLI:Test Set Validate Invalid Options	syslocation	${NUMBER}
	[Teardown]	CLI:Revert

Test Validate POINTS values for system
	CLI:Enter Path	/settings/snmp/system/
	CLI:Test Set Validate Invalid Options	syscontact	${POINTS}	Error: syscontact: Validation error.
	CLI:Test Set Validate Invalid Options	syslocation	${POINTS}	Error: syslocation: Validation error.
	CLI:Test Set Validate Invalid Options	syscontact	\#	Error: syscontact: Validation error.
	CLI:Test Set Validate Invalid Options	syslocation	\#	Error: syslocation: Validation error.
	[Teardown]	CLI:Revert

Test Validate FEW_CHARACTERES values for system
	CLI:Enter Path	/settings/snmp/system/
	CLI:Test Set Validate Invalid Options	syscontact	${ONE_WORD}
	CLI:Test Set Validate Invalid Options	syslocation	${ONE_WORD}
	[Teardown]	CLI:Revert

Test Validate EXCEEDED values for system
	CLI:Enter Path	/settings/snmp/system/
	CLI:Test Set Validate Invalid Options	syscontact	${EXCEEDED}	Error: syscontact: Exceeded the maximum size for this field.
	CLI:Test Set Validate Invalid Options	syslocation	${EXCEEDED}	Error: syslocation: Exceeded the maximum size for this field.
	[Teardown]	CLI:Revert