*** Settings ***
Resource	../../init.robot
Documentation	Test validate systems values through snmpwalk
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	NON-CRITICAL
Default Tags	CLI	SSH	SNMP	V1_V2_V3	VALIDATE	ADD
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SNMPV1V2_NAME}	snmpv1v2-test
${ACCESS_TYPE}	read_and_write
${ENTITY_COMMUNITY}	qatest
${ENTITY_OID}	.1.3
${ENTITY_TREE}	.1.3.6.1.2.1.47.1.1.1.1
${SSD_LIFE_LEFT_OID}	.1.3.6.1.4.1.42518.4.2.1.1.1.19.0
${SSD_OID}	.1.3.6.1.4.1.42518.4.2.1.1.1
@{ENTITY_OIDS_INFORMATION}
...	.1.3.6.1.2.1.47.1.1.1.1.1.1 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.1.2 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.1.3 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.1.4 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.2.1 = STRING:
...	.1.3.6.1.2.1.47.1.1.1.1.2.2 = STRING:
...	.1.3.6.1.2.1.47.1.1.1.1.2.3 = STRING:
...	.1.3.6.1.2.1.47.1.1.1.1.2.4 = STRING:
...	.1.3.6.1.2.1.47.1.1.1.1.3.1 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.3.2 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.3.3 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.3.4 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.4.1 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.4.2 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.4.3 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.4.4 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.5.1 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.5.2 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.5.3 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.5.4 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.6.1 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.6.2 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.6.3 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.6.4 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.7.1 = STRING:
...	.1.3.6.1.2.1.47.1.1.1.1.7.2 = ""
...	.1.3.6.1.2.1.47.1.1.1.1.7.3 = ""
...	.1.3.6.1.2.1.47.1.1.1.1.7.4 = ""
...	.1.3.6.1.2.1.47.1.1.1.1.8.1 = STRING:
...	.1.3.6.1.2.1.47.1.1.1.1.8.2 = ""
...	.1.3.6.1.2.1.47.1.1.1.1.8.3 = ""
...	.1.3.6.1.2.1.47.1.1.1.1.8.4 = ""
...	.1.3.6.1.2.1.47.1.1.1.1.9.1 = ""
...	.1.3.6.1.2.1.47.1.1.1.1.9.2 = ""
...	.1.3.6.1.2.1.47.1.1.1.1.9.3 = ""
...	.1.3.6.1.2.1.47.1.1.1.1.9.4 = ""
...	.1.3.6.1.2.1.47.1.1.1.1.10.1 = STRING:
...	.1.3.6.1.2.1.47.1.1.1.1.10.2 = ""
...	.1.3.6.1.2.1.47.1.1.1.1.10.3 = ""
...	.1.3.6.1.2.1.47.1.1.1.1.10.4 = ""
...	.1.3.6.1.2.1.47.1.1.1.1.11.1 = STRING:
...	.1.3.6.1.2.1.47.1.1.1.1.11.2 = ""
...	.1.3.6.1.2.1.47.1.1.1.1.11.3 = ""
...	.1.3.6.1.2.1.47.1.1.1.1.11.4 = ""
...	.1.3.6.1.2.1.47.1.1.1.1.12.1 = STRING:
...	.1.3.6.1.2.1.47.1.1.1.1.12.2 = STRING:
...	.1.3.6.1.2.1.47.1.1.1.1.12.3 = STRING:
...	.1.3.6.1.2.1.47.1.1.1.1.12.4 = STRING:
...	.1.3.6.1.2.1.47.1.1.1.1.13.1 = STRING:
...	.1.3.6.1.2.1.47.1.1.1.1.13.2 = ""
...	.1.3.6.1.2.1.47.1.1.1.1.13.3 = ""
...	.1.3.6.1.2.1.47.1.1.1.1.13.4 = ""
...	.1.3.6.1.2.1.47.1.1.1.1.16.1 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.16.2 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.16.3 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.16.4 = INTEGER:

@{ENTITY_OIDS}
...	.1.3.6.1.2.1.47.1.1.1.1.1.1
...	.1.3.6.1.2.1.47.1.1.1.1.1.2
...	.1.3.6.1.2.1.47.1.1.1.1.1.3
...	.1.3.6.1.2.1.47.1.1.1.1.1.4
...	.1.3.6.1.2.1.47.1.1.1.1.2.1
...	.1.3.6.1.2.1.47.1.1.1.1.2.2
...	.1.3.6.1.2.1.47.1.1.1.1.2.3
...	.1.3.6.1.2.1.47.1.1.1.1.2.4
...	.1.3.6.1.2.1.47.1.1.1.1.3.1
...	.1.3.6.1.2.1.47.1.1.1.1.3.2
...	.1.3.6.1.2.1.47.1.1.1.1.3.3
...	.1.3.6.1.2.1.47.1.1.1.1.3.4
...	.1.3.6.1.2.1.47.1.1.1.1.4.1
...	.1.3.6.1.2.1.47.1.1.1.1.4.2
...	.1.3.6.1.2.1.47.1.1.1.1.4.3
...	.1.3.6.1.2.1.47.1.1.1.1.4.4
...	.1.3.6.1.2.1.47.1.1.1.1.5.1
...	.1.3.6.1.2.1.47.1.1.1.1.5.2
...	.1.3.6.1.2.1.47.1.1.1.1.5.3
...	.1.3.6.1.2.1.47.1.1.1.1.5.4
...	.1.3.6.1.2.1.47.1.1.1.1.6.1
...	.1.3.6.1.2.1.47.1.1.1.1.6.2
...	.1.3.6.1.2.1.47.1.1.1.1.6.3
...	.1.3.6.1.2.1.47.1.1.1.1.6.4
...	.1.3.6.1.2.1.47.1.1.1.1.7.1
...	.1.3.6.1.2.1.47.1.1.1.1.7.2
...	.1.3.6.1.2.1.47.1.1.1.1.7.3
...	.1.3.6.1.2.1.47.1.1.1.1.7.4
...	.1.3.6.1.2.1.47.1.1.1.1.8.1
...	.1.3.6.1.2.1.47.1.1.1.1.8.2
...	.1.3.6.1.2.1.47.1.1.1.1.8.3
...	.1.3.6.1.2.1.47.1.1.1.1.8.4
...	.1.3.6.1.2.1.47.1.1.1.1.9.1
...	.1.3.6.1.2.1.47.1.1.1.1.9.2
...	.1.3.6.1.2.1.47.1.1.1.1.9.3
...	.1.3.6.1.2.1.47.1.1.1.1.9.4
...	.1.3.6.1.2.1.47.1.1.1.1.10.1
...	.1.3.6.1.2.1.47.1.1.1.1.10.2
...	.1.3.6.1.2.1.47.1.1.1.1.10.3
...	.1.3.6.1.2.1.47.1.1.1.1.10.4
...	.1.3.6.1.2.1.47.1.1.1.1.11.1
...	.1.3.6.1.2.1.47.1.1.1.1.11.2
...	.1.3.6.1.2.1.47.1.1.1.1.11.3
...	.1.3.6.1.2.1.47.1.1.1.1.11.4
...	.1.3.6.1.2.1.47.1.1.1.1.12.1
...	.1.3.6.1.2.1.47.1.1.1.1.12.2
...	.1.3.6.1.2.1.47.1.1.1.1.12.3
...	.1.3.6.1.2.1.47.1.1.1.1.12.4
...	.1.3.6.1.2.1.47.1.1.1.1.13.1
...	.1.3.6.1.2.1.47.1.1.1.1.13.2
...	.1.3.6.1.2.1.47.1.1.1.1.13.3
...	.1.3.6.1.2.1.47.1.1.1.1.13.4
...	.1.3.6.1.2.1.47.1.1.1.1.16.1
...	.1.3.6.1.2.1.47.1.1.1.1.16.2
...	.1.3.6.1.2.1.47.1.1.1.1.16.3
...	.1.3.6.1.2.1.47.1.1.1.1.16.4

@{ENTITY_OIDS_INFORMATION_VM}
...	.1.3.6.1.2.1.47.1.1.1.1.1.1 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.2.1 = STRING:
...	.1.3.6.1.2.1.47.1.1.1.1.3.1 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.4.1 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.5.1 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.6.1 = INTEGER:
...	.1.3.6.1.2.1.47.1.1.1.1.7.1 = STRING:
...	.1.3.6.1.2.1.47.1.1.1.1.8.1 = STRING:
...	.1.3.6.1.2.1.47.1.1.1.1.9.1 = ""
...	.1.3.6.1.2.1.47.1.1.1.1.10.1 = STRING:
...	.1.3.6.1.2.1.47.1.1.1.1.11.1 = STRING:
...	.1.3.6.1.2.1.47.1.1.1.1.12.1 = STRING:
...	.1.3.6.1.2.1.47.1.1.1.1.13.1 = STRING:
...	.1.3.6.1.2.1.47.1.1.1.1.16.1 = INTEGER:

@{ENTITY_OIDS_VM}
...	.1.3.6.1.2.1.47.1.1.1.1.1.1
...	.1.3.6.1.2.1.47.1.1.1.1.2.1
...	.1.3.6.1.2.1.47.1.1.1.1.3.1
...	.1.3.6.1.2.1.47.1.1.1.1.4.1
...	.1.3.6.1.2.1.47.1.1.1.1.5.1
...	.1.3.6.1.2.1.47.1.1.1.1.6.1
...	.1.3.6.1.2.1.47.1.1.1.1.7.1
...	.1.3.6.1.2.1.47.1.1.1.1.8.1
...	.1.3.6.1.2.1.47.1.1.1.1.9.1
...	.1.3.6.1.2.1.47.1.1.1.1.10.1
...	.1.3.6.1.2.1.47.1.1.1.1.11.1
...	.1.3.6.1.2.1.47.1.1.1.1.12.1
...	.1.3.6.1.2.1.47.1.1.1.1.13.1
...	.1.3.6.1.2.1.47.1.1.1.1.16.1

@{SYSTEM_VALUES_OIDS_CHECK}
...	.1.3.6.1.4.1.42518.4.2.1.1.1.1.0 = STRING:
...	.1.3.6.1.4.1.42518.4.2.1.1.1.2.0 = STRING:
...	.1.3.6.1.4.1.42518.4.2.1.1.1.3.0 = INTEGER:
...	.1.3.6.1.4.1.42518.4.2.1.1.1.4.0 = STRING:
...	.1.3.6.1.4.1.42518.4.2.1.1.1.5.0 = INTEGER:
...	.1.3.6.1.4.1.42518.4.2.1.1.1.6.0 = STRING:
...	.1.3.6.1.4.1.42518.4.2.1.1.1.7.0 = STRING:
...	.1.3.6.1.4.1.42518.4.2.1.1.1.8.0 = INTEGER:
...	.1.3.6.1.4.1.42518.4.2.1.1.1.9.0 = INTEGER:
...	.1.3.6.1.4.1.42518.4.2.1.1.1.10.0 = INTEGER:
...	.1.3.6.1.4.1.42518.4.2.1.1.1.11.0 = INTEGER:
...	.1.3.6.1.4.1.42518.4.2.1.1.1.12.0 = INTEGER:
...	.1.3.6.1.4.1.42518.4.2.1.1.1.13.0 = INTEGER:
...	.1.3.6.1.4.1.42518.4.2.1.1.1.14.0 = INTEGER:
...	.1.3.6.1.4.1.42518.4.2.1.1.1.15.0 = INTEGER:
...	.1.3.6.1.4.1.42518.4.2.1.1.1.16.0 = INTEGER:
...	.1.3.6.1.4.1.42518.4.2.1.1.1.17.0 = INTEGER:
...	.1.3.6.1.4.1.42518.4.2.1.1.1.18.0 = STRING:
...	.1.3.6.1.4.1.42518.4.2.1.1.1.19.0 = INTEGER:

@{SSD_INFORMATION_OIDS}
...	.1.3.6.1.4.1.42518.4.2.1.1.1.1.0
...	.1.3.6.1.4.1.42518.4.2.1.1.1.2.0
...	.1.3.6.1.4.1.42518.4.2.1.1.1.3.0
...	.1.3.6.1.4.1.42518.4.2.1.1.1.4.0
...	.1.3.6.1.4.1.42518.4.2.1.1.1.5.0
...	.1.3.6.1.4.1.42518.4.2.1.1.1.6.0
...	.1.3.6.1.4.1.42518.4.2.1.1.1.7.0
...	.1.3.6.1.4.1.42518.4.2.1.1.1.8.0
...	.1.3.6.1.4.1.42518.4.2.1.1.1.9.0
...	.1.3.6.1.4.1.42518.4.2.1.1.1.10.0
...	.1.3.6.1.4.1.42518.4.2.1.1.1.11.0
...	.1.3.6.1.4.1.42518.4.2.1.1.1.12.0
...	.1.3.6.1.4.1.42518.4.2.1.1.1.13.0
...	.1.3.6.1.4.1.42518.4.2.1.1.1.14.0
...	.1.3.6.1.4.1.42518.4.2.1.1.1.15.0
...	.1.3.6.1.4.1.42518.4.2.1.1.1.16.0
...	.1.3.6.1.4.1.42518.4.2.1.1.1.17.0
...	.1.3.6.1.4.1.42518.4.2.1.1.1.18.0
...	.1.3.6.1.4.1.42518.4.2.1.1.1.19.0

${USB_SENSORS_TREE}	.1.3.6.1.4.1.42518.4.2.1.1.5

@{USB_SENSORS_OIDS}
...	.1.3.6.1.4.1.42518.4.2.1.1.5.1.0
...	.1.3.6.1.4.1.42518.4.2.1.1.5.2.1.2.0
...	.1.3.6.1.4.1.42518.4.2.1.1.5.2.1.3.0
...	.1.3.6.1.4.1.42518.4.2.1.1.5.2.1.4.0
...	.1.3.6.1.4.1.42518.4.2.1.1.5.2.1.5.0

@{SYSTEM_VALUES_USB_SENSORS}
...	.1.3.6.1.4.1.42518.4.2.1.1.5.1.0 = INTEGER:
...	.1.3.6.1.4.1.42518.4.2.1.1.5.2.1.2.0 = STRING:
...	.1.3.6.1.4.1.42518.4.2.1.1.5.2.1.3.0 = INTEGER:
...	.1.3.6.1.4.1.42518.4.2.1.1.5.2.1.4.0 = INTEGER:
...	.1.3.6.1.4.1.42518.4.2.1.1.5.2.1.5.0 = Gauge32:

*** Test Cases ***
Test Request For Entity Information Via SNMPwalk
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	Run Keyword If	not ${IS_NGM}	Log To Console	\n\n${ENTITY_OIDS_INFORMATION}\n\n
	...	ELSE	Log To Console	\n\n${ENTITY_OIDS_INFORMATION_VM}\n\n
	${SYSTEM_INFOS}=	Wait Until Keyword Succeeds	10x	5s	SUITE:SNMPwalk Entity	${ENTITY_TREE}
	IF	${IS_NGM}
		@{ENTITY_OIDS_INFORMATION}=	Set Variable	@{ENTITY_OIDS_INFORMATION_VM}
	END
	FOR	${OID_CHECK}	IN	@{ENTITY_OIDS_INFORMATION}
		Should Contain	${SYSTEM_INFOS}	${OID_CHECK}
	END

Test Request For Entity Information Via SNMPget
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	IF	${IS_NGM}
		@{ENTITY_OIDS_INFORMATION}=	Set Variable	@{ENTITY_OIDS_INFORMATION_VM}
		${ENTITY_OIDS}=	Set Variable	${ENTITY_OIDS_VM}
	END
	${LENGTH}=	Get Length	${ENTITY_OIDS_INFORMATION}
	FOR	${I}	IN RANGE	${LENGTH}
		${OID}=	Get From List	${ENTITY_OIDS}	${I}
		${OID_CHECK}=	Get From List	${ENTITY_OIDS_INFORMATION}	${I}
		${SYSTEM_INFO}=	Wait Until Keyword Succeeds	10x	5s	SUITE:SNMPget Entity	${OID}
		Should Contain	${SYSTEM_INFO}	${OID_CHECK}
	END

Test Request Device Name Via SNMPwalk
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	SUITE:SNMPwalk For Name

Test Request Slot-0 Via SNMPget
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	Skip If	${IS_NGM}
	CLI:Switch Connection	default
	SUITE:SNMPget For Slot

Test Request Through Snmpwalk
	[Tags]	NON-CRITICAL
	Log To Console	\n\n${SYSTEM_VALUES_OIDS_CHECK}\n\n
	${SYSTEM_INFOS}=	SUITE:Get System Values Through SNMP Walk On Tree	${SYSTEM_VALUES_OID_TREE}

	FOR	${OID_CHECK}	IN	@{SYSTEM_VALUES_OIDS_CHECK}
		Should Contain	${SYSTEM_INFOS}	${OID_CHECK}
	END

Test Request Through Snmpget
	[Tags]	NON-CRITICAL
	${LENGTH}=	Get Length	${SYSTEM_VALUES_OIDS_CHECK}

	FOR		${I}	IN RANGE	${LENGTH}
		${OID}=	Get From List	${SYSTEM_VALUES_OIDS}	${I}
		${OID_CHECK}=	Get From List	${SYSTEM_VALUES_OIDS_CHECK}	${I}
		${SYSTEM_INFO}=	SUITE:Get Single System Value Through SNMP Get	${OID}
		Should Contain	${SYSTEM_INFO}	${OID_CHECK}
	END

Test Request For USB Sensors Statistics Via SNMPwalk
	[Tags]  NON-CRITICAL
	Skip If		not ${HAS_USB_SENSOR}	No USB sensors present in this suite
	Log To Console	\n\n${SYSTEM_VALUES_USB_SENSORS}\n\n
	${SYSTEM_INFOS}=	SUITE:Get System Values Through SNMP Walk On Tree	${USB_SENSORS_TREE}
	FOR		${OID_CHECK}	IN	@{SYSTEM_VALUES_USB_SENSORS}
		Should Contain	${SYSTEM_INFOS}	${OID_CHECK}
	END

Test Request For USB Sensors Statistics Via SNMPget
	[Tags]  NON-CRITICAL
	Skip If		not ${HAS_USB_SENSOR}	No USB sensors present in this suite
	${LENGTH}=	Get Length	${SYSTEM_VALUES_USB_SENSORS}
	FOR	${I}	IN RANGE	${LENGTH}
		${OID}=	Get From List	${USB_SENSORS_OIDS}	${I}
		${OID_CHECK}=	Get From List	${SYSTEM_VALUES_USB_SENSORS}	${I}
		${SYSTEM_INFO}=	SUITE:Get Single System Value Through SNMP Get	${OID}
		Should Contain	${SYSTEM_INFO}	${OID_CHECK}
	END

Test Request For SSD Information Via SNMPwalk
	[Tags]	NON-CRITICAL
	Skip If	${SYSTEM_VALUES_SSD_INFORMATION} == -1	The SSD model present on this unit is not supported
	Log To Console	\n\n${SYSTEM_VALUES_SSD_INFORMATION}\n\n
	${SYSTEM_INFOS}=	SUITE:Get System Values Through SNMP Walk On Tree	${SSD_OID}
	FOR	${OID_CHECK}	IN	@{SYSTEM_VALUES_OIDS_CHECK}
		Should Contain	${SYSTEM_INFOS}	${OID_CHECK}
	END

Test Request For SSD Information Via SNMPget
	[Tags]	NON-CRITICAL
	Skip If	${SYSTEM_VALUES_SSD_INFORMATION} == -1	The SSD model present on this unit is not supported
	${LENGTH}=	Get Length	${SYSTEM_VALUES_OIDS_CHECK}
	FOR	${I}	IN RANGE	${LENGTH}
		${OID}=	Get From List	${SSD_INFORMATION_OIDS}	${I}
		${OID_CHECK}=	Get From List	${SYSTEM_VALUES_OIDS_CHECK}	${I}
		${SYSTEM_INFO}=	SUITE:Get Single System Value Through SNMP Get	${OID}
		Should Contain	${SYSTEM_INFO}	${OID_CHECK}
	END

*** Keywords ***
SUITE:Setup
	CLI:Open	session_alias=default
	SUITE:Delete All SNMP
	SUITE:Add SNMP	${ENTITY_COMMUNITY}	read_only	${ENTITY_OID}
	SUITE:Add SNMP
	${IS_BSR}=	CLI:Is Bold SR
	${IS_NGM}=	CLI:Is VM System
	${IS_NSR}=	CLI:Is Net SR
	Set Suite Variable	${IS_BSR}
	Set Suite Variable	${IS_NGM}
	Set Suite Variable	${IS_NSR}
	${HAS_USB_SENSOR}	CLI:Has USB Sensor
	Set Suite Variable	${HAS_USB_SENSOR}
	${SYSTEM_VALUES_SSD_INFORMATION}	SUITE:Get SSD Life Left Using Snmpget
	Set Suite Variable	${SYSTEM_VALUES_SSD_INFORMATION}

SUITE:Teardown
	CLI:Switch Connection	default
	SUITE:Delete All SNMP
	CLI:Close Connection

SUITE:SNMPwalk For Name
	${OUTPUT}=	Run Keyword If	${IS_BSR}	SUITE:SNMPwalk Entity	.1.3.6.1.2.1.47.1.1.1.1.2.1
	Run Keyword If	${IS_BSR}	Should Contain	${OUTPUT}	BSR-
	${OUTPUT}=	Run Keyword If	${IS_NGM}	SUITE:SNMPwalk Entity	.1.3.6.1.2.1.47.1.1.1.1.2.1
	Run Keyword If	${IS_NGM}	Should Contain	${OUTPUT}	Nodegrid Manager
	${OUTPUT}=	Run Keyword If	${IS_NSR}	SUITE:SNMPwalk Entity	.1.3.6.1.2.1.47.1.1.1.1.2.1
	Run Keyword If	${IS_NSR}	Should Contain	${OUTPUT}	NSR-

SUITE:SNMPwalk Entity
	[Arguments]	${TREE}
	CLI:Connect As Root
	${OUTPUT}=	CLI:Write	snmpwalk -v 2c -c ${ENTITY_COMMUNITY} 127.0.0.1 ${TREE}
	Should Not Contain	${OUTPUT}	No Such Object available on this agent at this OID
	[Return]	${OUTPUT}

SUITE:SNMPget For Slot
	CLI:Connect As Root
	${OUTPUT}=	SUITE:SNMPget Entity	.1.3.6.1.2.1.47.1.1.1.1.2.2
	Run Keyword If	${IS_BSR}	Should Contain	${OUTPUT}	slot-0
	Run Keyword If	not ${IS_BSR}	Should Contain	${OUTPUT}	slot-1

SUITE:SNMPget Entity
	[Arguments]	${TREE}
	CLI:Connect As Root
	${OUTPUT}=	CLI:Write	snmpget -v 2c -c ${ENTITY_COMMUNITY} 127.0.0.1 ${TREE}
	Should Not Contain	${OUTPUT}	No Such Instance currently exists at this OID
	[Return]	${OUTPUT}

SUITE:Add SNMP
	[Arguments]	${COMMUNITY}=${SNMPV1V2_NAME}	${ACCESS_TYPE}=${ACCESS_TYPE}	${OID}=${EMPTY}
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/snmp/v1_v2_v3
	CLI:Add
	CLI:Set	community=${COMMUNITY}
	CLI:Set	access_type=${ACCESS_TYPE}
	CLI:Set	oid=${OID}
	CLI:Commit

SUITE:Delete All SNMP
	CLI:Enter Path	/settings/snmp/v1_v2_v3
	${OUTPUT}=	CLI:Show
	${SNMP_LIST}=	Create List
	@{SNMPs}=	Split To Lines	${OUTPUT}	2	-1
	${LENGTH}=	Get Length	${SNMPs}
	Return From Keyword If	'${LENGTH}' == '0'
	FOR	${SNMP}	IN	@{SNMPs}
		Log	Start Loop for ${SNMP}	INFO
		${SNMP_SPLIT}=	Split String	${SNMP}
		SUITE:Delete SNMP	${SNMP_SPLIT}[0]
	END

SUITE:Delete SNMP
	[Arguments]	${SNMPV1V2_NAME}=${SNMPV1V2_NAME}
	CLI:Enter Path	/settings/snmp/v1_v2_v3
	CLI:Delete If Exists	${SNMPV1V2_NAME}_default

SUITE:Get Single System Value Through SNMP Get
	[Arguments]	${OID}
	CLI:Switch Connection	root_session
	${SYSTEM_VALUE}=	CLI:Write	snmpget -v2c -c ${SNMPV1V2_NAME} 127.0.0.1 ${OID}
	Should Not Contain	${SYSTEM_VALUE}	Timeout: No Response from 127.0.0.1
	[Return]	${SYSTEM_VALUE}

SUITE:Get System Values Through SNMP Walk On Tree
	[Arguments]  ${OID_TREE}
	CLI:Switch Connection	root_session
	${SYSTEM_VALUES}=	CLI:Write	snmpwalk -v2c -c ${SNMPV1V2_NAME} 127.0.0.1 ${OID_TREE}
	Should Not Contain	${SYSTEM_VALUES}	Timeout: No Response from 127.0.0.1
	[Return]	${SYSTEM_VALUES}

SUITE:Get SSD Life Left Using Snmpget
	CLI:Connect As Root	session_alias=root_session
	${SNMPGET_SSD_LEFT}	CLI:Write	snmpget -v 2c -c ${SNMPV1V2_NAME} ${HOST} ${SSD_LIFE_LEFT_OID}
	${SNMPGET_SSD_LEFT}	Split To Lines	${SNMPGET_SSD_LEFT}
	#gets only the last three digits of first line (SSD Life Left in percentage)
	${SNMPGET_SSD_LEFT}	Get Substring	${SNMPGET_SSD_LEFT}[0]	-3
	CLI:Switch Connection	default
	[Return]	${SNMPGET_SSD_LEFT}