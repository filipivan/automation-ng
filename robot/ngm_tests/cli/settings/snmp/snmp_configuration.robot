*** Settings ***
Resource	../../init.robot
Documentation	Tests Validate fields when adding at NETWORK :: SNMP
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SNMP	V1_V2_V3	VALIDATE	ADD
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NAME_V1_V2}	snmpv1v2
${PATH_V1_V2}	${NAME_V1_V2}_${NAME_V1_V2}
${VERSION}	version v1/v2

${NAME_V1_V2_IPV6}	snmpv1v2ipv6
${PATH_V1_V2_IPV6}	${NAME_V1_V2_IPV6}_${NAME_V1_V2_IPV6}
${VERSION_IPV6}	version v1/v2 ipv6

${NAME_V3}	snmpv3

${OID}	.1
${OID_1}	.1.3.6.1.4.1.318.1.1.8.5.3.3.1.3.1.1.1
${OID_2}	.1.3.6.1.4.1.311

${PASSWD}	12345678

*** Test Cases ***
Test defining Right values for snmp version=v1|v2 without ipv6
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Set	version=version_v1|v2
	CLI:Set	community=${NAME_V1_V2}
	CLI:Set	source=${NAME_V1_V2}
	CLI:Test Set Field Options	snmp_for_ipv6	yes	no
	CLI:Test Set Field Options	access_type	read_only	read_and_write
	CLI:Set	oid=${OID}
	CLI:Save
	[Teardown]	CLI:Cancel	Raw

Test defining Right values for snmp version=v1|v2 with ipv6
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Set	version=version_v1|v2
	CLI:Set	community=${NAME_V1_V2_IPV6}
	CLI:Set	source=${NAME_V1_V2_IPV6}
	CLI:Test Set Field Options	snmp_for_ipv6	no	yes
	CLI:Test Set Field Options	access_type	read_and_write	read_only
	CLI:Set	oid=${OID}
	CLI:Save
	[Teardown]	CLI:Cancel	Raw

Test defining Right values for snmp version=3
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Set	version=version_3
	CLI:Set	username=${NAME_V3}
	CLI:Test Set Field Options	security_level	authpriv	authnopriv	noauthnopriv
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Options	authentication_algorithm	md5	sha
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Options	authentication_algorithm	md5	sha	sha-224	sha-256	sha-384	sha-512
	CLI:Set	authentication_password=${PASSWD}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Options	privacy_algorithm	aes	des
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Options	privacy_algorithm	aes	aes-192	aes-256	des
	CLI:Set	privacy_password=${PASSWD}
	CLI:Test Set Field Options	access_type	read_only	read_and_write
	CLI:Set	oid=${OID}
	CLI:Save
	[Teardown]	CLI:Cancel	Raw

Test validate modify values for version v1|v2 without ipv6
	CLI:Enter Path	/settings/snmp/v1_v2_v3/${PATH_V1_V2}/
	CLI:Test Validate Unmodifiable Field	community	${NAME_V1_V2}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Validate Unmodifiable Field	version	${VERSION}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Validate Unmodifiable Field	version	version_v1|v2
	CLI:Test Set Validate Normal Fields	access_type	read_only	read_and_write
	CLI:Test Set Validate Normal Fields	oid	${OID}	${OID_1}	${OID_2}
	[Teardown]	CLI:Revert	Raw

Test validate modify values for version v1|v2 with ipv6
	CLI:Enter Path	/settings/snmp/v1_v2_v3/${PATH_V1_V2_IPV6}/
	CLI:Test Validate Unmodifiable Field	community	${NAME_V1_V2_IPV6}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Validate Unmodifiable Field	version	${VERSION_IPV6}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Validate Unmodifiable Field	version	version_v1|v2_ipv6_
	CLI:Test Set Validate Normal Fields	access_type	read_only	read_and_write
	CLI:Test Set Validate Normal Fields	oid	${OID}	${OID_1}	${OID_2}
	[Teardown]	CLI:Revert	Raw

Test validate modify values for version v3
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Enter Path	/settings/snmp/v1_v2_v3/${NAME_V3}/
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Enter Path	/settings/snmp/v1_v2_v3/${NAME_V3}_/
	CLI:Test Validate Unmodifiable Field	username	${NAME_V3}
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Validate Unmodifiable Field	version	version: Version 3
	Run Keyword If	'${NGVERSION}' == '4.2'	CLI:Test Validate Unmodifiable Field	version	Version v3
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Validate Unmodifiable Field	version	version_v3
	CLI:Test Set Validate Normal Fields	access_type	read_only	read_and_write
	CLI:Test Set Validate Normal Fields	oid	${OID}	${OID_1}	${OID_2}
	CLI:Test Set Validate Normal Fields	security_level	authpriv	authnopriv	noauthnopriv
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields		authentication_algorithm	md5	sha
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields		authentication_algorithm	md5	sha	sha-224	sha-256	sha-384	sha-512
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Normal Fields	privacy_algorithm	aes	des
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Normal Fields	privacy_algorithm	aes	aes-192	aes-256	des
	[Teardown]	CLI:Revert	Raw

Test validate EMPTY values for snmp version=v1|v2 without ipv6
	CLI:Enter Path	/settings/snmp/v1_v2_v3/${PATH_V1_V2}/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Validate Unmodifiable Field	version	${VERSION}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Validate Unmodifiable Field	version	version_v1|v2
	CLI:Test Validate Unmodifiable Field	community	${NAME_V1_V2}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	access_type	${EMPTY}	Error: Missing value for parameter: access_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	access_type	${EMPTY}	Error: Missing value: access_type
	CLI:Test Set Validate Invalid Options	oid	${EMPTY}
	[Teardown]	CLI:Revert	Raw

Test validate EMPTY values for snmp version=v1|v2 with ipv6
	CLI:Enter Path	/settings/snmp/v1_v2_v3/${PATH_V1_V2_IPV6}/
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Validate Unmodifiable Field	version	${VERSION_IPV6}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Validate Unmodifiable Field	version	version_v1|v2_ipv6_
	CLI:Test Validate Unmodifiable Field	community	${NAME_V1_V2_IPV6}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	access_type	${EMPTY}	Error: Missing value for parameter: access_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	access_type	${EMPTY}	Error: Missing value: access_type
	CLI:Test Set Validate Invalid Options	oid	${EMPTY}
	[Teardown]	CLI:Revert	Raw

Test validate EMPTY values for snmp version=3
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Enter Path	/settings/snmp/v1_v2_v3/${NAME_V3}/
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Enter Path	/settings/snmp/v1_v2_v3/${NAME_V3}_/

	CLI:Test Validate Unmodifiable Field	username	${NAME_V3}
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Validate Unmodifiable Field	version	version: Version 3
	Run Keyword If	'${NGVERSION}' == '4.2'	CLI:Test Validate Unmodifiable Field	version	Version v3
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Validate Unmodifiable Field	version	version_v3

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	security_level	${EMPTY}	Error: Missing value for parameter: security_level
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	security_level	${EMPTY}	Error: Missing value: security_level

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	authentication_algorithm	${EMPTY}	Error: Missing value for parameter: authentication_algorithm
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	authentication_algorithm	${EMPTY}	Error: Missing value: authentication_algorithm

	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	authentication_password	${EMPTY}	Error: authentication_password: This field should have at least 8 characters and less than 64 and should not begin with \#.

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	privacy_algorithm	${EMPTY}	Error: Missing value for parameter: privacy_algorithm
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	privacy_algorithm	${EMPTY}	Error: Missing value: privacy_algorithm
	CLI:Test Set Validate Invalid Options	privacy_password	${EMPTY}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	access_type	${EMPTY}	Error: Missing value for parameter: access_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	access_type	${EMPTY}	Error: Missing value: access_type
	CLI:Test Set Validate Invalid Options	oid	${EMPTY}
	[Teardown]	CLI:Revert	Raw

Test validate WORD values for snmp version=v1|v2 without ipv6
	CLI:Enter Path	/settings/snmp/v1_v2_v3/${PATH_V1_V2}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Validate Unmodifiable Field	version	${VERSION}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Validate Unmodifiable Field	version	version_v1|v2
	CLI:Test Validate Unmodifiable Field	community	${NAME_V1_V2}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	access_type	${WORD}	Error: Invalid value: ${WORD} for parameter: access_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	access_type	${WORD}	Error: Invalid value: ${WORD}
	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	oid	${WORD}	Error: oid: Validation error.
	[Teardown]	CLI:Revert	Raw

Test validate WORD values for snmp version=v1|v2 with ipv6
	CLI:Enter Path	/settings/snmp/v1_v2_v3/${PATH_V1_V2_IPV6}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Validate Unmodifiable Field	version	${VERSION_IPV6}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Validate Unmodifiable Field	version	version_v1|v2_ipv6_
	CLI:Test Validate Unmodifiable Field	community	${NAME_V1_V2_IPV6}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	access_type	${WORD}	Error: Invalid value: ${WORD} for parameter: access_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	access_type	${WORD}	Error: Invalid value: ${WORD}
	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	oid	${WORD}	Error: oid: Validation error.
	[Teardown]	CLI:Revert	Raw

Test validate WORD values for snmp version=3
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Enter Path	/settings/snmp/v1_v2_v3/${NAME_V3}/
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Enter Path	/settings/snmp/v1_v2_v3/${NAME_V3}_/

	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Validate Unmodifiable Field	version	version: Version 3
	Run Keyword If	'${NGVERSION}' == '4.2'	CLI:Test Validate Unmodifiable Field	version	Version v3
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Validate Unmodifiable Field	version	version_v3
	CLI:Test Validate Unmodifiable Field	username	${NAME_V3}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	security_level	${WORD}	Error: Invalid value: ${WORD} for parameter: security_level
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	security_level	${WORD}	Error: Invalid value: ${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	authentication_algorithm	${WORD}	Error: Invalid value: ${WORD} for parameter: authentication_algorithm
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	authentication_algorithm	${WORD}	Error: Invalid value: ${WORD}
	CLI:Test Set Validate Invalid Options	authentication_password	${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	privacy_algorithm	${WORD}	Error: Invalid value: ${WORD} for parameter: privacy_algorithm
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	privacy_algorithm	${WORD}	Error: Invalid value: ${WORD}
	CLI:Test Set Validate Invalid Options	privacy_password	${WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	access_type	${WORD}	Error: Invalid value: ${WORD} for parameter: access_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	access_type	${WORD}	Error: Invalid value: ${WORD}

	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	oid	${WORD}	Error: oid: Validation error.


Test validate NUMBER values for snmp version=v1|v2 without ipv6
	CLI:Enter Path	/settings/snmp/v1_v2_v3/${PATH_V1_V2}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Validate Unmodifiable Field	version	${VERSION}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Validate Unmodifiable Field	version	version_v1|v2
	CLI:Test Validate Unmodifiable Field	community	${NAME_V1_V2}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	access_type	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: access_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	access_type	${NUMBER}	Error: Invalid value: ${NUMBER}
	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	oid	${NUMBER}	Error: oid: Validation error.
	[Teardown]	CLI:Revert	Raw

Test validate NUMBER values for snmp version=v1|v2 with ipv6
	Remove Tags	ADD
	CLI:Enter Path	/settings/snmp/v1_v2_v3/${PATH_V1_V2_IPV6}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Validate Unmodifiable Field	version	${VERSION_IPV6}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Validate Unmodifiable Field	version	version_v1|v2_ipv6_
	CLI:Test Validate Unmodifiable Field	community	${NAME_V1_V2_IPV6}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	access_type	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: access_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	access_type	${NUMBER}	Error: Invalid value: ${NUMBER}
	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	oid	${NUMBER}	Error: oid: Validation error.
	[Teardown]	CLI:Revert	Raw

Test validate NUMBER values for snmp version=3
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Enter Path	/settings/snmp/v1_v2_v3/${NAME_V3}/
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Enter Path	/settings/snmp/v1_v2_v3/${NAME_V3}_/
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Validate Unmodifiable Field	version	version: Version 3
	Run Keyword If	'${NGVERSION}' == '4.2'	CLI:Test Validate Unmodifiable Field	version	Version v3
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Validate Unmodifiable Field	version	version_v3
	CLI:Test Validate Unmodifiable Field	username	${NAME_V3}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	security_level	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: security_level
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	security_level	${NUMBER}	Error: Invalid value: ${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	authentication_algorithm	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: authentication_algorithm
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	authentication_algorithm	${NUMBER}	Error: Invalid value: ${NUMBER}

	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	authentication_password	${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	privacy_algorithm	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: privacy_algorithm
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	privacy_algorithm	${NUMBER}	Error: Invalid value: ${NUMBER}

	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	privacy_password	${NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	access_type	${NUMBER}	Error: Invalid value: ${NUMBER} for parameter: access_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	access_type	${NUMBER}	Error: Invalid value: ${NUMBER}

	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	oid	${NUMBER}	Error: oid: Validation error.
	[Teardown]	CLI:Revert	Raw

Test validate POINTS values for snmp version=v1|v2 without ipv6
	CLI:Enter Path	/settings/snmp/v1_v2_v3/${PATH_V1_V2_IPV6}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Validate Unmodifiable Field	version	${VERSION}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Validate Unmodifiable Field	version	version_v1|v2
	CLI:Test Validate Unmodifiable Field	community	${NAME_V1_V2}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	access_type	${POINTS}	Error: Invalid value: ${POINTS} for parameter: access_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	access_type	${POINTS}	Error: Invalid value: ${POINTS}
	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	oid	${POINTS}	Error: oid: Validation error.
	[Teardown]	CLI:Revert	Raw

Test validate POINTS values for snmp version=v1|v2 with ipv6
	CLI:Enter Path	/settings/snmp/v1_v2_v3/${PATH_V1_V2_IPV6}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Validate Unmodifiable Field	version	${VERSION_IPV6}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Validate Unmodifiable Field	version	version_v1|v2_ipv6_
	CLI:Test Validate Unmodifiable Field	community	${NAME_V1_V2_IPV6}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	access_type	${POINTS}	Error: Invalid value: ${POINTS} for parameter: access_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	access_type	${POINTS}	Error: Invalid value: ${POINTS}
	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	oid	${POINTS}	Error: oid: Validation error.
	[Teardown]	CLI:Revert	Raw

Test validate POINTS values for snmp version=3
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Enter Path	/settings/snmp/v1_v2_v3/${NAME_V3}/
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Enter Path	/settings/snmp/v1_v2_v3/${NAME_V3}_/
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Validate Unmodifiable Field	version	version: Version 3
	Run Keyword If	'${NGVERSION}' == '4.2'	CLI:Test Validate Unmodifiable Field	version	Version v3
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Validate Unmodifiable Field	version	version_v3
	CLI:Test Validate Unmodifiable Field	username	${NAME_V3}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	security_level	${POINTS}	Error: Invalid value: ${POINTS} for parameter: security_level
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	security_level	${POINTS}	Error: Invalid value: ${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	authentication_algorithm	${POINTS}	Error: Invalid value: ${POINTS} for parameter: authentication_algorithm
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	authentication_algorithm	${POINTS}	Error: Invalid value: ${POINTS}
	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	authentication_password	${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	privacy_algorithm	${POINTS}	Error: Invalid value: ${POINTS} for parameter: privacy_algorithm
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	privacy_algorithm	${POINTS}	Error: Invalid value: ${POINTS}

	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	privacy_password	${POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	access_type	${POINTS}	Error: Invalid value: ${POINTS} for parameter: access_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	access_type	${POINTS}	Error: Invalid value: ${POINTS}

	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	oid	${POINTS}	Error: oid: Validation error.
	[Teardown]	CLI:Revert	Raw

Test validate FEW_WORD values for snmp version=v1|v2 without ipv6
	CLI:Enter Path	/settings/snmp/v1_v2_v3/${PATH_V1_V2}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Validate Unmodifiable Field	version	${VERSION}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Validate Unmodifiable Field	version	version_v1|v2
	CLI:Test Validate Unmodifiable Field	community	${NAME_V1_V2}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	access_type	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: access_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	access_type	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}
	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	oid	${ONE_WORD}	Error: oid: Validation error.
	[Teardown]	CLI:Revert	Raw

Test validate FEW_WORD values for snmp version=v1|v2 with ipv6
	CLI:Enter Path	/settings/snmp/v1_v2_v3/${PATH_V1_V2_IPV6}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Validate Unmodifiable Field	version	${VERSION_IPV6}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Validate Unmodifiable Field	version	version_v1|v2_ipv6_
	CLI:Test Validate Unmodifiable Field	community	${NAME_V1_V2_IPV6}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	access_type	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: access_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	access_type	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}
	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	oid	${ONE_WORD}	Error: oid: Validation error.
	[Teardown]	CLI:Revert	Raw

Test validate FEW_WORD values for snmp version=3
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Enter Path	/settings/snmp/v1_v2_v3/${NAME_V3}/
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Enter Path	/settings/snmp/v1_v2_v3/${NAME_V3}_/
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Validate Unmodifiable Field	version	version: Version 3
	Run Keyword If	'${NGVERSION}' == '4.2'	CLI:Test Validate Unmodifiable Field	version	Version v3
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Validate Unmodifiable Field	version	version_v3
	CLI:Test Validate Unmodifiable Field	username	${NAME_V3}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	security_level	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: security_level
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	security_level	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	authentication_algorithm	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: authentication_algorithm
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	authentication_algorithm	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	authentication_password	${ONE_WORD}	Error: authentication_password: This field should have at least 8 characters and less than 64 and should not begin with \#.

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	privacy_algorithm	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: privacy_algorithm
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	privacy_algorithm	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	privacy_password	${ONE_WORD}	Error: privacy_password: This field should have at least 8 characters and less than 64 and should not begin with \#.

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	access_type	${ONE_WORD}	Error: Invalid value: ${ONE_WORD} for parameter: access_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	access_type	${ONE_WORD}	Error: Invalid value: ${ONE_WORD}

	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	oid	${ONE_WORD}	Error: oid: Validation error.
	[Teardown]	CLI:Revert	Raw

Test validate FEW_NUMBER values for snmp version=v1|v2 without ipv6
	CLI:Enter Path	/settings/snmp/v1_v2_v3/${PATH_V1_V2}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Validate Unmodifiable Field	version	${VERSION}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Validate Unmodifiable Field	version	version_v1|v2
	CLI:Test Validate Unmodifiable Field	community	${NAME_V1_V2}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	access_type	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: access_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	access_type	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}
	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	oid	${ONE_NUMBER}	Error: oid: Validation error.
	[Teardown]	CLI:Revert	Raw

Test validate FEW_NUMBER values for snmp version=v1|v2 with ipv6
	CLI:Enter Path	/settings/snmp/v1_v2_v3/${PATH_V1_V2_IPV6}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Validate Unmodifiable Field	version	${VERSION_IPV6}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Validate Unmodifiable Field	version	version_v1|v2_ipv6_
	CLI:Test Validate Unmodifiable Field	community	${NAME_V1_V2_IPV6}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	access_type	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: access_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	access_type	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}
	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	oid	${ONE_NUMBER}	Error: oid: Validation error.
	[Teardown]	CLI:Revert	Raw

Test validate FEW_NUMBER values for snmp version=3
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Enter Path	/settings/snmp/v1_v2_v3/${NAME_V3}/
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Enter Path	/settings/snmp/v1_v2_v3/${NAME_V3}_/
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Validate Unmodifiable Field	version	version: Version 3
	Run Keyword If	'${NGVERSION}' == '4.2'	CLI:Test Validate Unmodifiable Field	version	Version v3
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Validate Unmodifiable Field	version	version_v3
	CLI:Test Validate Unmodifiable Field	username	${NAME_V3}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	security_level	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: security_level
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	security_level	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	authentication_algorithm	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: authentication_algorithm
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	authentication_algorithm	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}
	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	authentication_password	${ONE_NUMBER}	Error: authentication_password: This field should have at least 8 characters and less than 64 and should not begin with \#.

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	privacy_algorithm	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: privacy_algorithm
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	privacy_algorithm	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}
	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	privacy_password	${ONE_NUMBER}	Error: privacy_password: This field should have at least 8 characters and less than 64 and should not begin with \#.

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	access_type	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER} for parameter: access_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	access_type	${ONE_NUMBER}	Error: Invalid value: ${ONE_NUMBER}

	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	oid	${ONE_NUMBER}	Error: oid: Validation error.
	[Teardown]	CLI:Revert	Raw

Test validate FEW_POINTS values for snmp version=v1|v2 without ipv6
	CLI:Enter Path	/settings/snmp/v1_v2_v3/${PATH_V1_V2_IPV6}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Validate Unmodifiable Field	version	${VERSION}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Validate Unmodifiable Field	version	version_v1|v2
	CLI:Test Validate Unmodifiable Field	community	${NAME_V1_V2}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	access_type	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: access_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	access_type	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}
	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	oid	${ONE_POINTS}	Error: oid: Validation error.
	[Teardown]	CLI:Revert	Raw

Test validate FEW_POINTS values for snmp version=v1|v2 with ipv6
	CLI:Enter Path	/settings/snmp/v1_v2_v3/${PATH_V1_V2_IPV6}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Validate Unmodifiable Field	version	${VERSION_IPV6}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Validate Unmodifiable Field	version	version_v1|v2_ipv6_
	CLI:Test Validate Unmodifiable Field	community	${NAME_V1_V2_IPV6}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	access_type	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: access_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	access_type	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}
	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	oid	${ONE_POINTS}	Error: oid: Validation error.
	[Teardown]	CLI:Revert	Raw

Test validate FEW_POINTS values for snmp version=3
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Enter Path	/settings/snmp/v1_v2_v3/${NAME_V3}/
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Enter Path	/settings/snmp/v1_v2_v3/${NAME_V3}_/
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Validate Unmodifiable Field	version	version: Version 3
	Run Keyword If	'${NGVERSION}' == '4.2'	CLI:Test Validate Unmodifiable Field	version	Version v3
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Validate Unmodifiable Field	version	version_v3
	CLI:Test Validate Unmodifiable Field	username	${NAME_V3}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	security_level	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: security_level
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	security_level	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	authentication_algorithm	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: authentication_algorithm
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	authentication_algorithm	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}
	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	authentication_password	${ONE_POINTS}	Error: authentication_password: This field should have at least 8 characters and less than 64 and should not begin with \#.

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	privacy_algorithm	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: privacy_algorithm
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	privacy_algorithm	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}
	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	privacy_password	${ONE_POINTS}	Error: privacy_password: This field should have at least 8 characters and less than 64 and should not begin with \#.

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	access_type	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS} for parameter: access_type
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Validate Invalid Options	access_type	${ONE_POINTS}	Error: Invalid value: ${ONE_POINTS}

	#3.2 Doesn't have error message but doesn't change value
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Validate Invalid Options	oid	${ONE_POINTS}	Error: oid: Validation error.
	[Teardown]	CLI:Revert	Raw

Test ALREADY EXISTS values for snmp version=v1v2 | source=empty | no ipv6
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Set	community=${NAME_V1_V2}
	CLI:Set	source=${EMPTY}
	CLI:Save
	CLI:Add
	CLI:Set	community=${NAME_V1_V2}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	source	${EMPTY}	Error: community: Entry already exists.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	source	${EMPTY}	Error: community: Entry already exists.Error: source: Entry already exists.	yes
	CLI:Cancel	Raw
	[Teardown]	SUITE:Test Teardown

Test ALREADY EXISTS values for snmp version=v1v2 | source | no ipv6
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Set	community=${NAME_V1_V2}
	CLI:Set	source=${NAME_V1_V2}
	CLI:Save
	CLI:Add
	CLI:Set	community=${NAME_V1_V2}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	source	${NAME_V1_V2}	Error: community: Entry already exists.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	source	${NAME_V1_V2}	Error: community: Entry already exists.Error: source: Entry already exists.	yes
	CLI:Cancel	Raw
	[Teardown]	SUITE:Test Teardown

Test ALREADY EXISTS values for snmp version=v1v2 | source=empty | ipv6
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Set	community=${NAME_V1_V2_IPV6}
	CLI:Set	snmp_for_ipv6=yes
	CLI:Set	source=${EMPTY}
	CLI:Save
	CLI:Add
	CLI:Set	community=${NAME_V1_V2_IPV6}
	CLI:Set	snmp_for_ipv6=yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	source	${EMPTY}	Error: community: Entry already exists.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	source	${EMPTY}	Error: community: Entry already exists.	yes
	CLI:Cancel	Raw
	[Teardown]	SUITE:Test Teardown

Test ALREADY EXISTS values for snmp version=v1v2 | source | ipv6
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Add
	CLI:Set	community=${NAME_V1_V2_IPV6}
	CLI:Set	snmp_for_ipv6=yes
	CLI:Set	source=${NAME_V1_V2_IPV6}
	CLI:Save
	CLI:Add
	CLI:Set	community=${NAME_V1_V2_IPV6}
	CLI:Set	snmp_for_ipv6=yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	source	${NAME_V1_V2_IPV6}	Error: community: Entry already exists.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	source	${NAME_V1_V2_IPV6}	Error: community: Entry already exists.	yes
	CLI:Cancel	Raw
	[Teardown]	SUITE:Test Teardown

Test ALREADY EXISTS values for snmp version=3
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Write	add
	CLI:Set	version=version_3
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	CLI:Set	username=${NAME_V3}
	CLI:Save
	CLI:Write	add
	CLI:Set	version=version_3
	### In v3.2, authentication_password is not setted by default
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	username	${NAME_V3}	Error: username: Entry already exists.	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	username	${NAME_V3}	Error: community: Entry already exists.	yes
	CLI:Cancel	Raw
	[Teardown]	SUITE:Test Teardown

Test defining Right OID for snmp version=v1|v2
	@{LIST}=	Create List	${OID}	${OID_1}	${OID_2}
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	FOR		${ITEM}	IN	@{LIST}
		CLI:Add
		CLI:Test Set Field Invalid Options	community	${NAME_V1_V2}
		CLI:Test Set Field Invalid Options	source	${NAME_V1_V2}
		CLI:Test Set Field Invalid Options	oid	${ITEM}	save=yes
		CLI:Delete If Exists	${PATH_V1_V2}
	END
	[Teardown]	SUITE:Test Teardown

Test defining Right OID for snmp version=3
	@{LIST}=	Create List	${OID}	${OID_1}	${OID_2}
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	FOR		${ITEM}	IN	@{LIST}
		CLI:Add
		CLI:Test Set Field Invalid Options	version	version_3
		CLI:Test Set Field Invalid Options	username	${NAME_V3}
		### In v3.2, authentication_password is not setted by default
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	authentication_password	${PASSWD}
		CLI:Test Set Field Invalid Options	oid	${ITEM}	save=yes
		CLI:Delete If Exists	${NAME_V3}	${NAME_V3}_
	END
	[Teardown]	SUITE:Test Teardown
*** Keywords ***
SUITE:Setup
	Wait Until Keyword Succeeds	5x	5s	CLI:Open
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	SUITE:Delete SNMPs

SUITE:Teardown
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	SUITE:Delete SNMPs
	CLI:Close Connection

SUITE:Test Teardown
	CLI:Cancel	Raw
	SUITE:Delete SNMPs

SUITE:Delete SNMPs
	CLI:Enter Path	/settings/snmp/v1_v2_v3/
	CLI:Delete If Exists	${PATH_V1_V2}	${NAME_V1_V2}_default
	...	${PATH_V1_V2_IPV6}	${NAME_V1_V2_IPV6}_default6
	...	${NAME_V3}	${WORD}_${WORD}	${WORD}_default	${WORD}
	...	${NUMBER}_${NUMBER}	${NUMBER}_default	${NUMBER}
	...	${POINTS}_${NAME_V1_V2}	${POINTS}_default	${POINTS}
	...	${ONE_NUMBER}_${ONE_NUMBER}	${ONE_NUMBER}_default	${ONE_NUMBER}
	...	${ONE_WORD}_${ONE_WORD}	${ONE_WORD}_default	${ONE_WORD}
	...	${ONE_POINTS}_${ONE_POINTS}	${ONE_POINTS}_default	${ONE_POINTS}
	...	${EXCEEDED}_${EXCEEDED}	${EXCEEDED}_default	${EXCEEDED}
	#Delete snmp v3 in Nodegrid v3.2
	CLI:Delete If Exists	${NAME_V3}_	${WORD}_	${NUMBER}_	${POINTS}_
	...	${ONE_NUMBER}_	${ONE_WORD}_	${ONE_POINTS}_	${EXCEEDED}_

	#Delete SNMP of BUG_716 in Trello
	CLI:Delete If Exists	!@-$%^|_()_default	!@-$%^|_()_!@-$%^|_()
	...	!@-$%^|_()_default6