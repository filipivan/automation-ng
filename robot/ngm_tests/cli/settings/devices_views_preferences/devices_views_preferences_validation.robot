*** Settings ***
Resource	../../init.robot
Documentation	Tests devices view preferences validation
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
@{ALL_VALUES}	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}	${EXCEEDED}

*** Test Cases ***
Test Visible Fields For Show Command
	CLI:Enter Path	/settings/devices_views_preferences/
	CLI:Test Set Available Fields	predefined_columns	custom_columns
	[Teardown]	CLI:Cancel	Raw

Test Valid Values For Field=predefined_columns
	CLI:Enter Path	/settings/devices_views_preferences/
	CLI:Test Set Field Options Raw	predefined_columns	end_point	ip_alias
	...	nodegrid_host	serial_port	groups	kvm_port	port_alias	type
	...	ip_address	mode	second_ip_alias
	Run Keyword If	${NGVERSION} >= 5.0	CLI:Test Set Field Options Raw	predefined_columns	alias
	[Teardown]	CLI:Cancel	Raw

Test Invalid Values For Field=predefined_columns
	CLI:Enter Path	/settings/devices_views_preferences/

	FOR	${INVALID_VALUE}	IN	@{ALL_VALUES}
		CLI:Test Set Field Invalid Options	predefined_columns	${INVALID_VALUE}
		...	Error: Invalid value: ${INVALID_VALUE} for parameter: predefined_columns
	END
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection