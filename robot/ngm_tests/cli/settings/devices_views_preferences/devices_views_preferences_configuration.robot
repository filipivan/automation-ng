*** Settings ***
Resource	../../init.robot
Documentation	Tests devices view preferences configuration
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE}	test-dev.ice-columns

*** Test Cases ***
Test configure all Predefined Fields
	CLI:Enter Path	/settings/devices_views_preferences/
	${COLUMNS}=	Set Variable If	${NGVERSION} <= 4.2
	...	type,end_point,ip_address,ip_alias,kvm_port,nodegrid_host,second_ip_alias,serial_port,mode,groups,port_alias
	...	type,end_point,ip_address,ip_alias,kvm_port,nodegrid_host,second_ip_alias,serial_port,mode,groups,port_alias,alias
	CLI:Set	predefined_columns=${COLUMNS}
	CLI:Commit

	CLI:Enter Path	/access/
	CLI:Test Show Command	type	end point	ip address
	...	ip alias	kvm port	nodegrid host	second ip alias	serial port
	...	mode	groups	port alias
	Run Keyword If	${NGVERSION} >= 5.0	CLI:Test Show Command	${SPACE}${SPACE}alias
	[Teardown]	CLI:Cancel	Raw

Test configure no Predefined Fields
	CLI:Enter Path	/settings/devices_views_preferences/
	CLI:Set	predefined_columns=
	CLI:Commit

	CLI:Enter Path	/access/
	CLI:Test Not Show Command	type	end point	ip address
	...	ip alias	kvm port	nodegrid host	second ip alias	serial port
	...	mode	groups	port alias
	Run Keyword If	${NGVERSION} >= 5.0	CLI:Test Not Show Command	${SPACE}${SPACE}alias
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection