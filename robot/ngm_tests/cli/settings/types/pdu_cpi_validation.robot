*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Types > Pdu CPI... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI	SSH	SHOW

Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Test Cases ***

Test available commands after send tab-tab
	CLI:Enter Path	/settings/types/pdu_cpi
	CLI:Test Available Commands	apply_settings	commit	factory_settings	quit	set	shutdown	whoami	cancel
	...	event_system_audit	hostname	reboot	shell	software_upgrade	cd	event_system_clear	ls	revert
	...	show	system_certificate	change_password	exit	pwd	save_settings	show_settings	system_config_check

Test visible fields for show command
	CLI:Enter Path	/settings/types/pdu_cpi
	CLI:Test Show Command	device_type_name: pdu_cpi
	...	family: pdu
	...	protocol = snmp
#	...	ssh_options =
	...	login_prompt = ogin:
	...	password_prompt = sword:
	...	command_prompt = pdu.*>
	...	console_escape_sequence =

Test available fields after set protocol=ssh
	CLI:Enter Path	/settings/types/pdu_cpi
	Write	set protocol=ssh
	CLI:Read Until Prompt
	CLI:Test Show Command	ssh_options =

	CLI:Test Set Available Fields	command_prompt	console_escape_sequence	login_prompt	password_prompt	protocol	ssh_options
	[Teardown]	CLI:Revert

Test available fields after set protocol=none
	CLI:Enter Path	/settings/types/pdu_cpi
	CLI:Test Set Field Options	protocol	ipmi	none	snmp	ssh	telnet
	CLI:Set Field	protocol	none
	CLI:Test Not Show Command	ssh_options =
	CLI:Test Set Available Fields	command_prompt	login_prompt	password_prompt	protocol	console_escape_sequence
	CLI:Test Set Unavailable Fields	ssh_options
	[Teardown]	CLI:Revert