*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Types > Pdu Geist... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI	SSH	SHOW

Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Test Cases ***

Test available commands after send tab-tab
	CLI:Enter Path	/settings/types/pdu_geist
	CLI:Test Available Commands	cd	event_system_audit	hostname	quit	set	show_settings
	...	change_password	event_system_clear	ls	reboot	shell	shutdown	cancel	commit	exit	pwd	revert	show	whoami

Test visible fields for show command
	CLI:Enter Path	/settings/types/pdu_geist
	CLI:Test Show Command	device_type_name: pdu_geist	family: pdu	protocol = ssh	ssh_options =
	...	login_prompt = sername:	password_prompt = sword:	command_prompt = >	console_escape_sequence =

Test available fields after set protocol=ssh
	CLI:Enter Path	/settings/types/pdu_geist
	Write	set protocol=ssh
	CLI:Read Until Prompt
	CLI:Test Show Command	ssh_options =

	CLI:Test Set Available Fields	command_prompt	login_prompt	password_prompt	protocol	console_escape_sequence	ssh_options
	[Teardown]	CLI:Revert

Test available fields after set protocol=none
	CLI:Enter Path	/settings/types/pdu_geist
	CLI:Test Set Field Options	protocol	none	ssh	ipmi	telnet	snmp
	CLI:Set Field	protocol	telnet
	CLI:Test Not Show Command	ssh_options =
	CLI:Test Set Available Fields	command_prompt	login_prompt	password_prompt	protocol	console_escape_sequence
	CLI:Test Set Unavailable Fields	ssh_options
	[Teardown]	CLI:Revert