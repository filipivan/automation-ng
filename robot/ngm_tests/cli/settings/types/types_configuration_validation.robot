*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Types... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	CLI:Close Connection

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/types/
	CLI:Delete If Exists Confirm	clone_device_console	clone1_device_console

SUITE:Show Settings
	[Arguments]	${SHOW}	${TYPE}	${FAM}	${PROTOCOL}	${QUOTATION}
	Should Match Regexp	${SHOW}	/settings/types/${TYPE} device_type_name=${TYPE}
	Run Keyword If	'${TYPE}' == 'usb_sensor' and '${NGVERSION}' < '4.1'	Set Suite Variable	${SP}	${SPACE}	ELSE	Set Suite Variable	${SP}	${EMPTY}
	Run Keyword If	${QUOTATION} and '${NGVERSION}' >= '4.0'	Should Match Regexp	${SHOW}	/settings/types/${TYPE} family="${FAM}${SP}"
	Run Keyword If	${QUOTATION} == False and '${NGVERSION}' >= '4.0'	Should Match Regexp	${SHOW}	/settings/types/${TYPE} family=${FAM}
	Should Match Regexp	${SHOW}	/settings/types/${TYPE} protocol=${PROTOCOL}
	Run Keyword If	'${PROTOCOL}' != 'none'	Should Match Regexp	${SHOW}	/settings/types/${TYPE} login_prompt=
	Run Keyword If	'${PROTOCOL}' != 'none'	Should Match Regexp	${SHOW}	/settings/types/${TYPE} password_prompt=
	Run Keyword If	'${PROTOCOL}' != 'none'	Should Match Regexp	${SHOW}	/settings/types/${TYPE} command_prompt=

Test valid value for clone_type field
	CLI:Enter Path	/settings/types/
	CLI:Write	clone_type device_console
	CLI:Set Field	device_type_name	clone_device_console
	CLI:Test Show Command	device_type_name = clone_device_console	device type source: device_console
	CLI:Commit
	CLI:Test Show Command Regexp	clone_device_console\\s+device\\sconsole

Test deleting type
	CLI:Enter Path	/settings/types/
	CLI:Test Show Command	clone_device_console
	CLI:Delete Confirm	clone_device_console
	CLI:Test Not Show Command	clone_device_console

Test deleting protected type
	Skip If	'${NGVERSION}' < '4.1'	Error message not implemented in 3.2,4.0
	CLI:Enter Path	/settings/types/
	CLI:Test Show Command	device_console
	Write	delete device_console
	${DELETE_QUESTION}=	Read Until	:
	Write	yes
	${OUTPUT}=	CLI:Read Until Prompt	Raw
	Should Contain	${OUTPUT}	Error: Protected types cannot be deleted

Test cloning with dot in name
	Skip If	'${NGVERSION}' < '4.1'	Bug#1133 is not going to be fixed in 3.2 or 4.0
	CLI:Enter Path	/settings/types/
	CLI:Write	clone_type device_console
	CLI:Set Field	device_type_name	device.console
	CLI:Commit
	CLI:Test Show Command Regexp	device.console\\s+device\\sconsole\\s+ssh
	CLI:Delete Confirm	device.console

Test cloning duplicate entries
	Skip If	'${NGVERSION}' < '4.0'	Error message not implemented in 3.2
	CLI:Enter Path	/settings/types/
	CLI:Write	clone_type device_console
	CLI:Set Field	device_type_name	clone_device_console
	CLI:Test Show Command	device_type_name = clone_device_console	device type source: device_console
	CLI:Commit
	CLI:Test Show Command Regexp	clone_device_console\\s+device\\sconsole
	CLI:Write	clone_type device_console
	CLI:Test Set Field Invalid Options	device_type_name	clone_device_console	Error: clone: Cannot clone template.Error: device_type_name: Cannot clone type. Either name already exists or name is incorrect.	yes
	CLI:Cancel
	CLI:Delete Confirm	clone_device_console
	[Teardown]	Run Keywords	CLI:Cancel	Raw	AND	CLI:Delete All Devices

Test cloning type from cloned type
	CLI:Enter Path	/settings/types/
	CLI:Write	clone_type device_console
	CLI:Set Field	device_type_name	clone_device_console
	CLI:Test Show Command	device_type_name = clone_device_console	device type source: device_console
	CLI:Commit
	CLI:Test Show Command Regexp	clone_device_console\\s+device\\sconsole\\s+ssh
	CLI:Enter Path	/settings/types/clone_device_console/
	CLI:Set Field	protocol	none
	CLI:Commit
	CLI:Enter Path	/settings/types/
	CLI:Write	clone_type clone_device_console
	CLI:Set Field	device_type_name	clone1_device_console
	CLI:Test Show Command	device_type_name = clone1_device_console	device type source: clone_device_console
	CLI:Commit
	CLI:Test Show Command Regexp	clone1_device_console\\s+device\\sconsole\\s+none
	CLI:Delete Confirm	clone_device_console	clone1_device_console
	[Teardown]	CLI:Cancel	Raw	AND	CLI:Delete All Device	clone_device_console

#If you do TAB-TAB after first clone, then it will give error, If you type whole thing, it does not give error, but only gets first value
Test cloning more than one type at same time
	CLI:Enter Path	/settings/types/
	write bare	clone_type device_console,\t\t
	${OUTPUT}=	CLI:Read Until Prompt	Yes
	Run Keyword If	'${NGVERSION}' >= '4.0'	Should Contain	${OUTPUT}	Error: Command clone_type accepts maximum of 1 argument(s)
	Run Keyword If	'${NGVERSION}' < '4.0'	Should Contain	${OUTPUT}	Error: Invalid arguments