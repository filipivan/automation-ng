*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Types > Usb Kvm... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Test Cases ***

Test available commands after send tab-tab
	Skip If	'${NGVERSION}' < '4.0'	Not implemented in 3.2
	CLI:Enter Path	/settings/types/usb_kvm
	CLI:Test Available Commands	cd	event_system_audit	hostname	quit	set	show_settings	change_password	event_system_clear	ls	reboot	shell	shutdown	cancel	commit	exit	pwd	revert	show	whoami

Test visible fields for show command
	Skip If	'${NGVERSION}' < '4.0'	Not implemented in 3.2
	CLI:Enter Path	/settings/types/usb_kvm
	CLI:Test Show Command	device_type_name: usb_kvm	family: usb kvm	protocol =	login_prompt =	password_prompt =	command_prompt =	console_escape_sequence =

Test available fields after set protocol=ssh
	Skip If	'${NGVERSION}' < '4.0'	Not implemented in 3.2
	CLI:Enter Path	/settings/types/usb_kvm
	Write	set protocol=ssh
	CLI:Read Until Prompt
	CLI:Test Show Command	ssh_options =
	CLI:Test Set Available Fields	command_prompt	login_prompt	password_prompt	protocol	console_escape_sequence	ssh_options
	[Teardown]	CLI:Revert

Test available fields after set protocol=telnet
	Skip If	'${NGVERSION}' < '4.0'	Not implemented in 3.2
	CLI:Enter Path	/settings/types/usb_kvm
	CLI:Test Set Field Options	protocol	none	ssh	ipmi	telnet
	CLI:Set Field	protocol	telnet
	CLI:Test Not Show Command	ssh_options =
	CLI:Test Set Available Fields	command_prompt	login_prompt	password_prompt	protocol	console_escape_sequence
	CLI:Test Set Unavailable Fields	ssh_options

	[Teardown]	CLI:Revert