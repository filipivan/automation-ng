*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Types > Console Server Nodegrid... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	CLI	SSH	SHOW

Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/types/console_server_nodegrid
	CLI:Test Available Commands	cd	event_system_audit	hostname	quit	set	show_settings	change_password	event_system_clear	ls	reboot	shell	shutdown	cancel	commit	exit	pwd	revert	show	whoami

Test visible fields for show command
	CLI:Enter Path	/settings/types/console_server_nodegrid
	CLI:Test Show Command	device_type_name: console_server_nodegrid	family: console server	protocol = ssh
	...	ssh_options =	login_prompt = ogin:	password_prompt = sword:	command_prompt =
	CLI:Read Until Prompt

Test available fields after set protocol=ssh
	CLI:Enter Path	/settings/types/console_server_nodegrid
	Write	set protocol=ssh
	CLI:Read Until Prompt
	CLI:Test Set Available Fields	command_prompt	login_prompt	password_prompt	protocol	console_escape_sequence	ssh_options
	[Teardown]	CLI:Revert

Test available fields after set protocol=none
	CLI:Enter Path	/settings/types/console_server_nodegrid
	CLI:Set Field	protocol	telnet
	CLI:Test Set Available Fields	command_prompt	login_prompt	password_prompt	protocol	console_escape_sequence
	CLI:Test Set Unavailable Fields	ssh_options
	[Teardown]	CLI:Revert