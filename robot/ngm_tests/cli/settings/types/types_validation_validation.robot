*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > Types... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	CLI:Close Connection

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/types/
	CLI:Test Available Commands	event_system_clear	set	exit	shell	cd	hostname	show	change_password	ls
	...	show_settings	clone_type	pwd	shutdown	commit	quit	whoami	delete	reboot	event_system_audit	revert

Test visible fields for ls command
	CLI:Enter Path	/settings/types/
	CLI:Test Ls Command	ilo/	imm/	drac/	idrac6/	ipmi_1.5/	ipmi_2.0/	ilom/	cimc_ucs/	netapp/	device_console/
	...	infrabox/	virtual_console_vmware/	virtual_console_kvm/	console_server_acs/	console_server_opengear/
	...	console_server_acs6000/	console_server_digicp/	console_server_lantronix/	pdu_apc/	pdu_baytech/
	...	pdu_eaton/	pdu_mph2/	pdu_pm3000/	pdu_raritan/	pdu_servertech/	pdu_enconnex/	kvm_dsr/	kvm_mpu/	kvm_raritan/	usb/
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Ls Command	usb_sensor/	usb_kvm/

Test visible fields for show command
	CLI:Enter Path	/settings/types/
	CLI:Test Show Command Regexp	device\\stype\\sname\\s+family\\s+protocol	ilo\\s+ilo	imm\\s+imm	drac\\s+drac	idrac6\\s+drac
	...	ipmi_1.5\\s+ipmi\\s1.5	ipmi_2.0\\s+ipmi 2.0	ilom\\s+ilom	cimc_ucs\\s+cimc\\sucs	netapp\\s+netapp
	...	device_console\\s+device\\sconsole	infrabox\\s+infrabox	virtual_console_vmware\\s+virtual\\sconsole\\svmware
	...	virtual_console_kvm\\s+virtual\\sconsole\\skvm	console_server_acs\\s+console\\sserver	console_server_acs6000\\s+console\\s+server
	...	console_server_digicp\\s+console\\sserver	console_server_lantronix\\s+console\\sserver	pdu_apc\\s+pdu	pdu_baytech\\s+pdu	pdu_eaton\\s+pdu
	...	pdu_mph2\\s+pdu	pdu_pm3000\\s+pdu	pdu_raritan\\s+pdu	pdu_servertech\\s+pdu	pdu_enconnex\\s+pdu	kvm_dsr\\s+avocent\\s+dsr
	...	kvm_mpu\\s+avocent\\smpu	kvm_raritan\\s+raritan\\skvm	usb\\s+usb
	Run Keyword If	'${NGVERSION}' >= '4.0'	CLI:Test Show Command Regexp	usb_sensor\\s+usb\\ssensor	usb_kvm\\s+usb\\skvm

Test show_settings
	CLI:Enter Path	/settings/types/
	Write	show_settings
	${SHOW}=	Read Until	types]#
	${OUTPUT}=	CLI:Show
	@{LINES}=	Run Keyword If	'${NGVERSION}' >= '4.0'	Split To Lines	${OUTPUT}	2	-1
	...	ELSE	Split To Lines	${OUTPUT}	2	-3
	FOR		${LINE}	IN	@{LINES}
		${LINE}=	Strip String	${LINE}
		${TYPE}=	Fetch From Left	${LINE}	${SPACE}
		${L_T}=	Get Length	${TYPE}
		${PROTOCOL}=	Fetch From Right	${LINE}	${SPACE}
		${L_P}=	Get Length	${PROTOCOL}
		${LINE}=	Get Substring	${LINE}	${L_T}	-${L_P}
		${FAM}=	Strip String	${LINE}
		${QUOTATION}=	Run Keyword And Return Status	Should Match Regexp	${FAM}	\\w\\s\\w
		SUITE:Show Settings	${SHOW}	${TYPE}	${FAM}	${PROTOCOL}	${QUOTATION}
	END

Test invalid value for clone_type field
	Skip If	'${NGVERSION}' < '4.0'	Error message not implemented in 3.2
	CLI:Enter Path	/settings/types/
	CLI:Write	clone_type device_console
	CLI:Test Show Command	device_type_name	device type source: device_console
	CLI:Test Set Field Invalid Options	device_type_name	${EMPTY}	Error: device_type_name: Field must not be empty.	yes
	CLI:Test Set Field Invalid Options	device_type_name	pdu_apc	Error: clone: Cannot clone template.Error: device_type_name: Cannot clone type. Either name already exists or name is incorrect.	yes
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enter Path	/settings/types/
	CLI:Delete If Exists Confirm	clone_device_console	clone1_device_console

SUITE:Show Settings
	[Arguments]	${SHOW}	${TYPE}	${FAM}	${PROTOCOL}	${QUOTATION}
	Should Match Regexp	${SHOW}	/settings/types/${TYPE} device_type_name=${TYPE}
	Run Keyword If	'${TYPE}' == 'usb_sensor' and '${NGVERSION}' < '4.1'	Set Suite Variable	${SP}	${SPACE}	ELSE	Set Suite Variable	${SP}	${EMPTY}
	Run Keyword If	${QUOTATION} and '${NGVERSION}' == '4.2'	Should Match Regexp	${SHOW}	/settings/types/${TYPE} family="${FAM}${SP}"
	Run Keyword If	${QUOTATION} == False and '${NGVERSION}' == '4.2'	Should Match Regexp	${SHOW}	/settings/types/${TYPE} family=${FAM}
	Run Keyword If	${QUOTATION} and '${NGVERSION}' >= '5.0'	Should Match Regexp	${SHOW}	/settings/types/${TYPE} \#clone_key=
	Run Keyword If	${QUOTATION} == False and '${NGVERSION}' >= '5.0'	Should Match Regexp	${SHOW}	/settings/types/${TYPE} \#clone_key=
	Run Keyword If	'${NGVERSION}' <= '4.2'	Should Match Regexp	${SHOW}	/settings/types/${TYPE} protocol=${PROTOCOL}
	Run Keyword If	'${NGVERSION}' <= '4.2'	Run Keyword If	'${PROTOCOL}' != 'none'	Should Match Regexp	${SHOW}	/settings/types/${TYPE} login_prompt=
	Run Keyword If	'${NGVERSION}' <= '4.2'	Run Keyword If	'${PROTOCOL}' != 'none'	Should Match Regexp	${SHOW}	/settings/types/${TYPE} password_prompt=
	Run Keyword If	'${NGVERSION}' <= '4.2'	Run Keyword If	'${PROTOCOL}' != 'none'	Should Match Regexp	${SHOW}	/settings/types/${TYPE} command_prompt=