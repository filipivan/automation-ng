*** Settings ***
Resource	../../init.robot
Documentation	Tests Validation > ZPE Cloud Settings... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_0
Default Tags	CLI	SSH	SHOW

Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Variables ***
${PASSCODE}	test_passcode

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/settings/zpe_cloud/
	${list}=	Create List	cd	hostname	set	change_password	ls	shell	commit	pwd	show	event_system_audit	quit
	...	event_system_clear	reboot	shutdown	exit	revert	whoami	system_certificate	system_config_check	factory_settings
	CLI:Test Available Commands	@{list}

Test show_settings command
	Set Tags	SHOW_SETTINGS	#Not in 4.0 Official yet
	CLI:Enter Path	/settings/zpe_cloud/
	${OUTPUT}=	CLI:Write	show_settings
	Should Match Regexp	${OUTPUT}	/settings/zpe_cloud enable_zpe_cloud=yes|no

	CLI:Write	set enable_zpe_cloud=yes
	${OUTPUT}=	CLI:Write	show_settings
	Should Match Regexp	${OUTPUT}	/settings/zpe_cloud enable_file_protection=no|yes
	CLI:Write	set enable_zpe_cloud=yes enable_file_protection=yes passcode=${PASSCODE}
	${OUTPUT}=	CLI:Write	show_settings
	#Should Match Regexp	${OUTPUT}	/settings/zpe_cloud passcode=${PASSCODE}

	Run Keyword If	'${NGVERSION}' < '4.0'  Should Match Regexp	${OUTPUT}   /settings/zpe_cloud passcode=${PASSCODE}
	Run Keyword If  '${NGVERSION}' >='4.0'  Should Match Regexp  ${OUTPUT}  /settings/zpe_cloud passcode=\*/*\/*\/*\/*\/*\/*\/*/*\/*\/*\/*\/*\
	Should Match Regexp	${OUTPUT}	/settings/zpe_cloud enable_file_encryption=no|yes
	[Teardown]	CLI:Revert

Test available fields to set
	[Tags]	BUG_NG_10341	NON-CRITICAL
	CLI:Enter Path	/settings/zpe_cloud
	${ENABLE_ZPECLOUD}=	Run Keyword And Return Status	CLI:Test Show Command	enable_zpe_cloud = yes
	Run Keyword If	${ENABLE_ZPECLOUD}	CLI:Write	set enable_zpe_cloud=no
	CLI:Test Set Available Fields	enable_zpe_cloud
	CLI:Test Set Unavailable Fields	enable_file_protection	passcode	enable_file_encryption

	CLI:Write	set enable_zpe_cloud=yes enable_file_protection=no
	CLI:Test Set Available Fields	enable_zpe_cloud	enable_file_protection
	CLI:Test Set Unavailable Fields	passcode	enable_file_encryption

	CLI:Write	set enable_zpe_cloud=yes enable_file_protection=yes passcode=${PASSCODE}
	CLI:Test Set Available Fields	enable_zpe_cloud	enable_file_protection	passcode	enable_file_encryption
	[Teardown]	CLI:Revert

Test invalid values for fields
	[Tags]	BUG_NG_10341	NON-CRITICAL
	CLI:Enter Path	/settings/zpe_cloud/
	${COMMANDS}=	Create List	enable_zpe_cloud	enable_file_protection	enable_file_encryption
	SUITE:Error Messages:Invalid Values:Options Fields	${COMMANDS}
	[Teardown]	CLI:Revert

Test valid values for field=enable_zpe_cloud
	CLI:Enter Path	/settings/zpe_cloud/
	CLI:Test Set Field Options	enable_zpe_cloud	no	yes
	[Teardown]	CLI:Revert

Test valid values for field=enable_file_protection
	CLI:Enter Path	/settings/zpe_cloud/
	CLI:Write	set enable_zpe_cloud=yes
	CLI:Write	set enable_file_protection=yes passcode=${PASSCODE}
	CLI:Test Set Field Options	enable_file_protection	no	yes
	[Teardown]	CLI:Revert

Test valid values for field=passcode
	CLI:Enter Path	/settings/zpe_cloud/
	CLI:Write	set enable_zpe_cloud=yes
	CLI:Write	set enable_file_protection=yes passcode=${PASSCODE}
	[Teardown]	CLI:Revert

Test valid values for field=enable_file_encryption
	[Tags]	BUG_NG_10341	NON-CRITICAL
	CLI:Enter Path	/settings/zpe_cloud/
	CLI:Write	set enable_zpe_cloud=yes
	CLI:Write	set enable_file_protection=yes passcode=${PASSCODE}
	CLI:Test Set Field Options	enable_file_encryption	no	yes
	[Teardown]	CLI:Revert

*** Keywords ***
SUITE:Error Messages:Invalid Values:Options Fields
	[Arguments]	${COMMANDS}
	FOR	${COMMAND}	IN	@{COMMANDS}
		CLI:Test Set Field Options	${COMMAND}	no	yes
		CLI:Write	set enable_zpe_cloud=yes enable_file_protection=yes passcode=${PASSCODE}
		CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value for parameter: ${COMMAND}
		CLI:Test Set Field Invalid Options	${COMMAND}	a	Error: Invalid value: a for parameter: ${COMMAND}
	END