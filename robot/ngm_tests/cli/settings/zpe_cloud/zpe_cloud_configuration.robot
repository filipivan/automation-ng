*** Settings ***
Resource	../../init.robot
Documentation	Tests Settings > ZPE Cloud Settings... through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_0
Default Tags	CLI	SSH	SHOW

Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Variables ***
${PASSCODE}	test_passcode

*** Test Cases ***
Test valid values for field=enable_zpe_cloud
	CLI:Enter Path	/settings/zpe_cloud/
	CLI:Test Set Field Options	enable_zpe_cloud	no	yes
	[Teardown]	CLI:Revert

Test valid values for field=enable_file_protection
	CLI:Enter Path	/settings/zpe_cloud/
	CLI:Write	set enable_zpe_cloud=yes
	CLI:Write	set enable_file_protection=yes passcode=${PASSCODE}
	CLI:Test Set Field Options	enable_file_protection	no	yes
	[Teardown]	CLI:Revert

Test valid values for field=passcode
	CLI:Enter Path	/settings/zpe_cloud/
	CLI:Write	set enable_zpe_cloud=yes
	CLI:Write	set enable_file_protection=yes passcode=${PASSCODE}
	[Teardown]	CLI:Revert

Test valid values for field=enable_file_encryption
	[Tags]	NON-CRITICAL	BUG_NG_10341
	CLI:Enter Path	/settings/zpe_cloud/
	CLI:Write	set enable_zpe_cloud=yes
	CLI:Write	set enable_file_protection=yes passcode=${PASSCODE}
	CLI:Test Set Field Options	enable_file_encryption	no	yes
	[Teardown]	CLI:Revert