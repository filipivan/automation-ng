*** Settings ***
Resource	../../init.robot
Documentation	Tests bluetooth validation
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2
Default Tags	CLI	SSH	SHOW
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
@{ALL_VALUES}	${WORD}	${ONE_WORD}	${TWO_WORD}	${THREE_WORD}
...	${SEVEN_WORD}	${EIGHT_WORD}	${ELEVEN_WORD}	${NUMBER}	${ONE_NUMBER}
...	${TWO_NUMBER}	${THREE_NUMBER}	${SEVEN_NUMBER}	${EIGHT_NUMBER}
...	${ELEVEN_NUMBER}	${WORD_AND_NUMBER}	${NUMBER_AND_WORD}	${POINTS}
...	${ONE_POINTS}	${TWO_POINTS}	${THREE_POINTS}	${SEVEN_POINTS}
...	${EIGHT_POINTS}	${ELEVEN_POINTS}	${WORD_AND_POINTS}	${NUMBER_AND_POINTS}
...	${POINTS_AND_NUMBER}	${POINTS_AND_WORD}	${EXCEEDED}

*** Test Cases ***
Test Available Commands After Sending Tab-Tab
	CLI:Enter Path	/system/bluetooth
	CLI:Test Available Commands	event_system_clear
	...	save_settings	exec	shell	apply_settings
	...	exit	show	cd	export_settings	show_settings	change_password
	...	factory_settings	shutdown	cloud_enrollment	hostname
	...	software_upgrade	commit	import_settings	system_certificate	connect
	...	ls	system_config_check	create_csr	pwd	unpair	diagnostic_data	quit
	...	whoami	disconnect	reboot	event_system_audit	revert

Test Visible Fields For Show Command In Bluetooth Path
	CLI:Enter Path	/system/bluetooth
	CLI:Test Show Command Regexp	bluetooth_display_name\\s+connected\\s+network

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection