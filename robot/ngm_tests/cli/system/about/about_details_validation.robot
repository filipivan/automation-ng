*** Settings ***
Resource	../../init.robot
Documentation	Get Details from Test System
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH

Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Test Cases ***
Get Device Details
	[Documentation]	Navigates to About information on a CLI prompt
	...	== REQUIREMENTS ==
	...	SSH session is open and a CLI prompt is available
	...	== ARGUMENTS ==
	...	None
	...	== EXPECTED RESULT ==
	...	The About information are displayed and contain the following Text Elements
	...	- System:
	...	- Version
	...	- Licenses:
	...	- CPU:
	...	- CPU Cores:
	...	- Bogomips per core:
	...	- Serial Number:
	...	- Uptime:
	CLI:Enter Path	/system/about
	${ABOUT_OUTPUT}=	CLI:Show
	@{lines_list}=	Split To Lines	${ABOUT_OUTPUT}	0	-1
	&{dict}=	Create Dictionary	a	a
	FOR	${ELEMENT}	IN	@{lines_list}
		Log	Start Loop for ${ELEMENT}	INFO
		${len}=	Get Length	${ELEMENT}
		Log	Lenghth of element ${len}	INFO
		${v1}	${v2}=	Run Keyword If	${len} > 0	Split String	${ELEMENT}	:	1
		Run Keyword If	${len} > 0	Log	Length of values 2	INFO
		Run Keyword If	${len} > 0	Set To Dictionary	${dict}	'${v1}'	'${v2}'
	END
	Log Dictionary	${dict}	INFO
	${SYSTEM}=	Get From Dictionary	${dict}	'system'
	Set Global Variable	${SYSTEM}
	${AVAILABLE_LICENSES}=	Get From Dictionary	${dict}	'licenses'
	Should be Integer	${AVAILABLE_LICENSES}
	${VERSION}=	Get From Dictionary	${dict}	'software'
	Set Global Variable	${VERSION}
	${CPU}=	Get From Dictionary	${dict}	'cpu'
	${CPU_CORES}=	Get From Dictionary	${dict}	'cpu_cores'
	Should be Integer	${CPU_CORES}
	${BOGOMIPS_PER_CORE}=	Get From Dictionary	${dict}	'bogomips_per_core'
	Should be Real	${BOGOMIPS_PER_CORE}
	${SERIAL_NUMBER}=	Get From Dictionary	${dict}	'serial_number'
	Should be Integer	${SERIAL_NUMBER}

	${VERSION_NUMBER}	${REST}=	Split String	${VERSION}	(	max_split=1
	${VERSION_NUMBER}=	Fetch From Right	${VERSION_NUMBER}	v
	${VERSION_NUMBER}=	Strip String	${VERSION_NUMBER}	mode=both

	Run Keyword if	"${VERSION_NUMBER}" > "3.2.44"	SUITE:Test Uptime	${dict}

	${IS_NODEGRID_MANAGER}=	Run Keyword If	'${NGVERSION}' < '4.1'	Evaluate	"${SYSTEM}" == "\' NodeGrid Manager\'"
	...	ELSE	Evaluate	"${SYSTEM}" == "\' Nodegrid Manager\'"

	${SYSTEM}=	Get Substring	${SYSTEM}	2
	${LENGTH_SYSTEM}=	Get Length	${SYSTEM}
	${LENGTH_SYSTEM}	Evaluate	${LENGTH_SYSTEM} - 1
	${SYSTEM}=	Get Substring	${SYSTEM}	0	${LENGTH_SYSTEM}

	${VERSION}=	Get Substring	${VERSION}	2
	${LENGTH_VERSION}=	Get Length	${VERSION}
	${LENGTH_VERSION}	Evaluate	${LENGTH_VERSION} - 1
	${VERSION}=	Get Substring	${VERSION}	0	${LENGTH_VERSION}

	${MODEL}=	Set Variable	${EMPTY}
	${MODEL}=	Run Keyword Unless	${IS_NODEGRID_MANAGER}	Get From Dictionary	${dict}	'model'
	${MODEL}=	Run Keyword Unless	${IS_NODEGRID_MANAGER}	Get Substring	${MODEL}	2
	${LENGTH_MODEL}=	Run Keyword Unless	${IS_NODEGRID_MANAGER}	Get Length	${MODEL}
	${LENGTH_MODEL}	Run Keyword Unless	${IS_NODEGRID_MANAGER}	Evaluate	${LENGTH_MODEL} - 1
	${MODEL}=	Run Keyword Unless	${IS_NODEGRID_MANAGER}	Get Substring	${MODEL}	0	${LENGTH_MODEL}

	Set Global Variable	${SYSTEM}
	Set Global Variable	${VERSION}
	Set Global Variable	${MODEL}

	Run Keyword Unless	${IS_NODEGRID_MANAGER}
	...	Run Keywords
	...	Get From Dictionary	${dict}	'part_number'
	...	AND	Get From Dictionary	${dict}	'bios_version'
	...	AND	Get From Dictionary	${dict}	'psu'

Test available commands after send tab-tab
	CLI:Enter Path	/system/about/
	CLI:Test Available Commands	cd	change_password	commit	event_system_audit	event_system_clear	exit	hostname	ls	pwd	quit	reboot	revert	shell	show	show_settings	shutdown	whoami

Test ls command
	CLI:Enter Path	/system/about/
	CLI:Test Ls Command	Try show command instead...

*** Keywords ***
SUITE:Test Uptime
	[Arguments]	${dict}
	${UPTIME}=	Get From Dictionary	${dict}	'uptime'
	Should Contain	${UPTIME}	days
	Should Contain	${UPTIME}	hours
	Should Contain	${UPTIME}	minutes