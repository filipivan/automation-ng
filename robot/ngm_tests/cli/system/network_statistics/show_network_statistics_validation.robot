*** Settings ***
Resource	../../init.robot
Documentation	Tests NG Network Statistics display details through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	SSH	SHOW	NETWORK

Suite Setup	SUITE:Setup
Suite Teardown	CLI:Close Connection

*** Test Cases ***
Show Network Statistics Overview
	CLI:Enter Path	/system/network_statistics/
	${OUTPUT}=	CLI:Write	show
	${OUTPUT}=	Remove String Using Regexp	${OUTPUT}	['\r']
	Log to Console	\n${OUTPUT}
	@{Details}=	Split to Lines	${OUTPUT}	0	1
	FOR	${LINE}	IN	@{Details}
		Should Contain	${LINE}	ifname	ifindex	state	rx packets
		Should Contain	${LINE}	tx packets	collisions	errors
	END
	@{Details}=	Split to Lines	${OUTPUT}	1	2
	FOR	${LINE}	IN	@{Details}
		Should Contain	${LINE}	======	=======	=====	==========
		Should Contain	${LINE}	==========	==========	======
	END
	@{Details}=	Split to Lines	${OUTPUT}	2	-3
	FOR	${LINE}	IN	@{Details}
		${CONTAINS_VTI}	Run Keyword and Return Status	Should Contain	${LINE}	ip_vti0
		${CONTAINS_VNET3}	Run Keyword and Return Status	Should Contain	${LINE}	vnet3
		Run keyword if	not ${CONTAINS_VTI} and not ${CONTAINS_VNET3}	Should Match Regexp	${LINE}	\[a-z0-9]{1,10}\\s+\\d{1,3}\\s+\[up|down]{2,4}\\s+\\d{1,10}\\s+\\d{1,10}\\s+\\d{1,10}\\s+\\d{1,10}
	END
	[Teardown]	CLI:Revert	Raw

Show Network Statistics Details
	CLI:Enter Path	/system/network_statistics/
	${OUTPUT}=	CLI:Write	ls
	${OUTPUT}=	Remove String Using Regexp	${OUTPUT}	['\r']
	@{Folders}=	Split to Lines	${OUTPUT}	0	-3
	FOR	${ELEMENT}	IN	@{Folders}
		Set Client Configuration	timeout=45s
		${DETAILS}=	CLI:Show	${ELEMENT}
		Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
		Should Contain	${DETAILS}	ifname:
		${IS_DOCKER}=	Run Keyword And Return Status	Should Contain	${DETAILS}	ifname: docker
		${IS_WIRELESS}=	Run Keyword And Return Status	Should Contain Any	${DETAILS}	ifname: wlan	ifname: wwan
		Run Keyword If	not ${IS_DOCKER} and not ${IS_WIRELESS}	Should Contain	${DETAILS}	speed(mb/s):
		Should Contain	${DETAILS}	collisions:
		Should Contain	${DETAILS}	rx packets:
		Should Contain	${DETAILS}	rx bytes:
		Should Contain	${DETAILS}	rx errors:
		Should Contain	${DETAILS}	rx crc errors:
		Should Contain	${DETAILS}	rx dropped:
		Should Contain	${DETAILS}	rx fifo errors:
		Should Contain	${DETAILS}	rx compressed:
		Should Contain	${DETAILS}	rx frame errors:
		Should Contain	${DETAILS}	rx length errors:
		Should Contain	${DETAILS}	rx missed errors:
		Should Contain	${DETAILS}	rx over errors:
		Should Contain	${DETAILS}	tx packets:
		Should Contain	${DETAILS}	tx bytes:
		Should Contain	${DETAILS}	tx errors:
		Should Contain	${DETAILS}	tx carrier errors:
		Should Contain	${DETAILS}	tx dropped:
		Should Contain	${DETAILS}	tx fifo errors:
		Should Contain	${DETAILS}	tx compressed:
		Should Contain	${DETAILS}	tx aborted errors:
		Should Contain	${DETAILS}	tx heartbeat errors:
		Should Contain	${DETAILS}	tx window errors:
		Log to Console	\nDetails for Session ${ELEMENT} are:\n ${DETAILS}
	END
	[Teardown]	CLI:Revert	Raw

Test available commands after send tab-tab
	CLI:Enter Path	/system/network_statistics/
	CLI:Test Available Commands
	...	apply_settings       hostname             show
	...	cd                   ls                   show_settings
	...	change_password      pwd                  shutdown
	...	commit               quit                 software_upgrade
	...	event_system_audit   reboot               system_certificate
	...	event_system_clear   revert               system_config_check
	...	exit                 save_settings        whoami
	...	factory_settings     shell
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Available Commands
	...	cloud_enrollment     create_csr           diagnostic_data
	Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Available Commands
	...	export_settings      import_settings      exec
	[Teardown]	CLI:Revert	Raw

Test ls command
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/system/network_statistics/
	${SHOULD_CONTAIN_INTERFACES}	CLI:Get Builtin Network Connections	YES
	CLI:Test Ls Command	@{SHOULD_CONTAIN_INTERFACES}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Ls Command	Try show command instead...
	[Teardown]	CLI:Revert	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open
	${HAS_ETH1}=	CLI:Has ETH1
	Set Suite Variable	${HAS_ETH1}	${HAS_ETH1}


