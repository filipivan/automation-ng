*** Settings ***
Resource	../../init.robot
Documentation	Tests NG Discovery Logs display details through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	DISCOYERY	SHOW

Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Test Cases ***
Show Discovery Logs
	CLI:Write	cd /system/discovery_logs/
	${OUTPUT}=	CLI:Write	show	user=Yes
	${OUTPUT}=	Remove String Using Regexp	${OUTPUT}	['\r']
	Should Match Regexp	${OUTPUT}	date\\s+ip address\\s+device name\\s+discovery method\\s+action
	Should Match Regexp	${OUTPUT}	\\s+=+\\s+=+\\s+=+\\s+=+\\s+=+
	@{Details}=	Split to Lines	${OUTPUT}	2
	FOR	${LINE}	IN	@{Details}
		${STRIP}=	Strip String	${LINE}
		Should Match Regexp	${STRIP}	${DISCOVERY_LOG_FORMAT}
	END

Test available commands after send tab-tab
	CLI:Enter Path	/system/discovery_logs/
	CLI:Test Available Commands	cd	change_password	commit	event_system_audit	event_system_clear	exit	hostname
	...	ls	pwd	quit	reboot	revert	shell	show	show_settings	shutdown	whoami	reset_logs

Test ls command
	CLI:Enter Path	/system/discovery_logs/
	${OUTPUT}=	CLI:Write	ls
	Should Not contain	${OUTPUT}	try

#Test reset logs
## Ideally add first a Discovery Loog and then reset it after
#	CLI:Enter Path	/system/discovery_logs/
#	Write	reset_logs
#	${OUTPUT}=	CLI:Read Until Prompt
#	Should Contain	${OUTPUT}	discovery_logs
#	Write	show
#	${OUTPUT}=	Read	delay=1s
#	${OUTPUT}=	Remove String Using Regexp	${OUTPUT}	['\r']
#	${LINES}=	Get Line Count	${OUTPUT}
#	Should Be Equal	${LINES}	${3}