*** Settings ***
Resource	../../init.robot
Documentation	Tests NG Open Sessions display details through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW	SESSION

Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/system/open_sessions/
	CLI:Test Available Commands	cd	change_password	commit	event_system_audit	event_system_clear	exit	hostname	ls	pwd	quit	reboot	revert	shell	show	show_settings	shutdown	whoami	terminate_session

Test ls command
	CLI:Enter Path	/system/open_sessions/
	CLI:Test Ls Command	Try show command instead...

Show Open Sessions Overview
	@{localnetwork}=	CLIENT:Get IPv4 Address
	CLI:Enter Path	/system/open_sessions/
	CLI:Test Show Command	user	mode	source ip	type	device name	ref	session start
	CLI:Test Show Command Any	@{localnetwork}
	CLI:Test Show Command Any	@{SESSION_MODE}
	CLI:Test Show Command Any	@{SESSION_TYPES}

Show Open Session Details
	CLI:Enter Path	/system/open_sessions/
	${SESSIONS}=	CLI:Get Available Paths
	FOR	${SESSION}	IN	@{SESSIONS}
		CLI:Enter Path	${SESSION}
		CLI:Test Show Command	user:	mode:	source ip:	type:	ref:	session start:
		CLI:Test Show Command Any	${CLIENTIP}
		CLI:Test Show Command Any	@{SESSION_MODE}
		CLI:Test Show Command Any	@{SESSION_TYPES}
		CLI:Enter Path	..
	END

Test terminate_session with invalid target
	Write	cd /system/open_sessions/
	CLI:Read Until Prompt
	Write	terminate_session sometarget
	${Output}=	CLI:Read Until Prompt	Raw
	Run Keyword If	'${NGVERSION}' >= '4.0'	Should Contain	${Output}	Error: Invalid Target name: sometarget. Please, use tab-tab to obtain available targets.
	Run Keyword If	'${NGVERSION}' < '4.0'	Should Contain	${Output}	Error: Invalid Target: sometarget. Press <tab> for a list of valid targets.

Test terminate_session, tab-tab and No more target error
	Write	cd /system/open_sessions/
	${OUTPUT}=	CLI:Read Until Prompt
	Log to console	\n${OUTPUT}
	Write	ls
	${OUTPUT}=	CLI:Read Until Prompt
	Log to console	${OUTPUT}
	${SESSIONS}=	Get Lines Containing String	${OUTPUT}	/
	${SESSIONS}=	Split to Lines	${SESSIONS}
	${COMMAND}=	Catenate	terminate_session	${SPACE}
	FOR	${SESSION}	IN	@{SESSIONS}
		${SESSIONID}=	Remove String	${SESSION}	/
		${COMMAND}=	Catenate	SEPARATOR=	${COMMAND}	${SESSIONID},
	END
	${COMMAND}=	Catenate	${COMMAND}	\t\n
	Write Bare	${COMMAND}
	${OUTPUT}=	CLI:Read Until Prompt	Raw
	Log to Console	${OUTPUT}
	Run Keyword If	'${NGVERSION}' >= '4.0'	Should Contain	${Output}	Error: No more targets available for command terminate_session
	Run Keyword If	'${NGVERSION}' < '4.0'	Should Contain	${Output}	Error: Target not available

Test Terminate all sessions
	CLI:Open	startping=no
	Write	cd /system/open_sessions/
	CLI:Read Until Prompt
	Write	ls
	${OUTPUT}=	CLI:Read Until Prompt
#	Log to console	\nOpenen Sessions: \n${OUTPUT}
	${SESSIONS}=	Get Lines Containing String	${OUTPUT}	/
	@{SESSIONS}=	Split to Lines	${SESSIONS}
	${OPENSESSIONS}=	Get Length	${SESSIONS}
#	Log to Console	\nAmount of Open Sessions: ${OPENSESSIONS}
#	Log to Console	\nTerminate all sessions\n
	Terminate Session
	${OUTPUT}=	CLI:Read Until Prompt	Raw
#	Log to Console	${OUTPUT}
	Should Contain	${Output}	Error: Warning: Cannot self terminate.
	Write	ls
	${OUTPUT}=	CLI:Read Until Prompt
#	Log to console	${OUTPUT}
	${SESSIONS}=	Get Lines Containing String	${OUTPUT}	/
	@{SESSIONS}=	Split to Lines	${SESSIONS}
	${OPENSESSIONSAFTER}=	Get Length	${SESSIONS}
#	Log to Console	Amount of Open Sessions: ${OPENSESSIONSAFTER}
	Should Be True	${OPENSESSIONS}>${OPENSESSIONSAFTER}

Test terminate_session
	CLI:Open	startping=no
	Write	cd /system/open_sessions/
	CLI:Read Until Prompt
	Write	show
	${OUTPUT}=	CLI:Read Until Prompt
	Log to Console	${OUTPUT}
	${LINE}=	Get Lines Containing String	${OUTPUT}	${CLIENTIP}
	@{LINE}=	Split String	${LINE}
	${SESSIONID}=	Get From List	${LINE}	4
	Log to Console	Session ID to be terminated: ${SESSIONID}
	Should Contain	${OUTPUT}	${CLIENTIP}
	Terminate Session	${SESSIONID}
	${OUTPUT}=	CLI:Read Until Prompt	Raw
	Should Not Contain	${Output}	Error: Warning: Cannot self terminate.
	Write	show
	${OUTPUT}=	CLI:Read Until Prompt
	Log to Console	${OUTPUT}

Test terminate_session, self termination error
	#CLI:Switch Connection	test
	Write	cd /system/open_sessions/
	CLI:Read Until Prompt
	Write	show
	${OUTPUT}=	CLI:Read Until Prompt
	Log to Console	${OUTPUT}
	${LINE}=	Get Lines Containing String	${OUTPUT}	${CLIENTIP}
	@{LINE}=	Split String	${LINE}
	${SESSIONID}=	Get From List	${LINE}	4
	Log to Console	Session ID: ${SESSIONID}
	Should Contain	${OUTPUT}	${CLIENTIP}
	Terminate Session	${SESSIONID}
	${OUTPUT}=	CLI:Read Until Prompt	Raw
	Should Contain	${Output}	Error: Warning: Cannot self terminate.

*** Keywords ***
Terminate Session
	[Arguments]	${SESSIONID}=-
	Write	terminate_session ${SESSIONID}
#	The backend takes some time to update the open session table
	Sleep	3 seconds