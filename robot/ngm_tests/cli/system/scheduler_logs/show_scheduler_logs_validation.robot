*** Settings ***
Resource	../../init.robot
Documentation	Test show scheduler log
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	SSH	SHOW	EXCLUDEIN3_2

Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Test Cases ***
Test available commands after send tab-tab
	Skip If	'${NGVERSION}' < '4.0'	Feature not available in 3.2
	CLI:Enter Path	/system/scheduler_logs/
	CLI:Test Available Commands	apply_settings	hostname	shell	cd	ls	show	change_password	pwd
	...	commit	quit	software_upgrade	event_system_audit	reboot	system_certificate	shutdown	save_settings
	...	event_system_clear	reset_task_log	system_config_check	exit	revert	whoami	factory_settings

Test Show Command
	Skip If	'${NGVERSION}' < '4.0'	Feature not available in 3.2
	CLI:Enter Path	/system/scheduler_logs/
	${OUTPUT}=	CLI:Show	RAW_Mode=Yes
	Should Match Regexp	${OUTPUT}	\\s+task\\sname\\s+user\\s+date\\s+pid\\s+event\\s+error

Test Show Command On Task Directory
	Skip If	'${NGVERSION}' < '4.0'	Feature not available in 3.2
	SUITE:Add Scheduler Task
	CLI:Enter Path	/system/scheduler_logs
	Wait Until Keyword Succeeds	14x	5000ms	CLI:Test Ls Command	system_task
	${OUTPUT}=	CLI:Show	RAW_Mode=Yes
	Should Match Regexp	${OUTPUT}	\\s+system_task\\s+${DEFAULT_USERNAME}\\s+\\d+/\\d+-\\d+:\\d+:\\d+\\s+\\d+\\s+Task\\s+
	CLI:Enter Path	system_task
	CLI:Test Show Command	task name: system_task	user: ${DEFAULT_USERNAME}	date:	pid:	event:
	SUITE:Delete Scheduler Task

Test Reset Log
	Skip If	'${NGVERSION}' < '4.0'	Feature not available in 3.2
	CLI:Enter Path	/system/scheduler_logs
	${OUTPUT}=	CLI:Show	RAW_Mode=Yes
	Should Match Regexp	${OUTPUT}	\\s+system_task\\s+${DEFAULT_USERNAME}\\s+\\d+/\\d+-\\d+:\\d+:\\d+\\s+\\d+\\s+Task\\s+
	CLI:Write	reset_task_log
	${OUTPUT}=	CLI:Show	RAW_Mode=Yes
	Should Not Match Regexp	${OUTPUT}	\\s+system_task\\s+${DEFAULT_USERNAME}\\s+\\d+/\\d+-\\d+:\\d+:\\d+\\s+\\d+\\s+Task\\s+

*** Keywords ***
SUITE:Add Scheduler Task
	CLI:Enter Path	/settings/scheduler/
	CLI:Delete If Exists	system_task
	CLI:Add
	CLI:Write	set task_name=system_task command_to_execute=whoami user=${DEFAULT_USERNAME}
	${OUTPUT}=	CLI:Commit	Raw
	Should Not Contain	${OUTPUT}	Error:
	CLI:Test Ls Command	system_task

SUITE:Delete Scheduler Task
	CLI:Enter Path	/settings/scheduler/
	CLI:Delete	system_task
	CLI:Test Not Show Command	system_task