*** Settings ***
Resource	../../init.robot
Documentation	Tests NG LLDP display details through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	LLDP	SHOW

Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Test Cases ***
Show LLDP Overview
	${IPV4}	CLI:Get Interface IP Address	eth0
	CLI:Enter Path	/settings/network_connections/ETH0
	CLI:Set	ipv6_mode=address_auto_configuration
	CLI:Commit
	${IPV6}	CLI:Get Interface Ipv6 Address	ETH0
	${MAC_ADDRESS}	CLI:Get Interface MAC Address	ETH0
	${DEVICE_TYPE}	CLI:Get System Name
	${VERSION}	CLI:Get System Version
	${HOSTNAME}	CLI:Get Hostname
	@{SYSTEM_ESPECIFICATIONS}	Create List	${IPV4}	${MAC_ADDRESS}	${VERSION}	${HOSTNAME}
	Run Keyword If	${NGVERSION} >= 4.2	Append To List	${SYSTEM_ESPECIFICATIONS}	${IPV6}	${DEVICE_TYPE}
	${OUTPUT}	CLI:Show	/system/lldp
	CLI:Should Contain All	${OUTPUT}	${SYSTEM_ESPECIFICATIONS}

Test available commands after send tab-tab
	CLI:Enter Path	/system/lldp/
	CLI:Test Available Commands	cd	change_password	commit	event_system_audit	event_system_clear	exit	hostname	ls	pwd	quit	reboot	revert	shell	show	show_settings	shutdown	whoami

Test ls command
	CLI:Enter Path	/system/lldp/
	CLI:Test Ls Command	Local_Chassis	Try show command instead...