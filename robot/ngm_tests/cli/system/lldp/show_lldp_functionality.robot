*** Settings ***
Resource	../../init.robot
Documentation	Tests NG LLDP functionality between two devices through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2
Default Tags	CLI	SSH	LLDP	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CONNECTION}	ETH0
${INTERFACE}	eth0

*** Test Cases ***
Test port_id=interface_index and port_description=interface_description in host device
	[Setup]	SUITE:Enable LLDP	hostshared_session	interface_name	interface_description
	SUITE:Enable LLDP	default	interface_index	interface_description
	CLI:Switch Connection	hostshared_session
	Wait Until Keyword Succeeds	10s	3x	SUITE:Check LLDP entry	${HOST}	interface_index	interface_description
	[Teardown]	SUITE:Disable LLDP	default

Test port_id=interface_name and port_description=interface_description in host device
	SUITE:Enable LLDP	default	interface_name	interface_description
	CLI:Switch Connection	hostshared_session
	Wait Until Keyword Succeeds	10s	3x	SUITE:Check LLDP entry	${HOST}	interface_name	interface_description
	[Teardown]	SUITE:Disable LLDP	default

Test port_id=interface_index and port_description=interface_name in host device
	SUITE:Enable LLDP	default	interface_index	interface_name
	CLI:Switch Connection	hostshared_session
	Wait Until Keyword Succeeds	10s	3x	SUITE:Check LLDP entry	${HOST}	interface_index	interface_name
	[Teardown]	SUITE:Disable LLDP	default

Test port_id=interface_name and port_description=interface_name in host device
	SUITE:Enable LLDP	default	interface_name	interface_name
	CLI:Switch Connection	hostshared_session
	Wait Until Keyword Succeeds	10s	3x	SUITE:Check LLDP entry	${HOST}	interface_name	interface_name
	[Teardown]	SUITE:Disable LLDP	default
########################################################################################################################
Test port_id=interface_index and port_description=interface_description in shared device
	[Setup]	SUITE:Enable LLDP	default	interface_name	interface_description
	SUITE:Enable LLDP	hostshared_session	interface_index	interface_description
	CLI:Switch Connection	default
	Wait Until Keyword Succeeds	30s	6x	SUITE:Check LLDP entry	${HOSTSHARED}	interface_index	interface_description
	[Teardown]	SUITE:Disable LLDP	hostshared_session

Test port_id=interface_name and port_description=interface_description in shared device
	SUITE:Enable LLDP	hostshared_session	interface_name	interface_description
	CLI:Switch Connection	default
	Wait Until Keyword Succeeds	30s	6x	SUITE:Check LLDP entry	${HOSTSHARED}	interface_name	interface_description
	[Teardown]	SUITE:Disable LLDP	hostshared_session

Test port_id=interface_index and port_description=interface_name in shared device
	SUITE:Enable LLDP	hostshared_session	interface_index	interface_name
	CLI:Switch Connection	default
	Wait Until Keyword Succeeds	30s	6x	SUITE:Check LLDP entry	${HOSTSHARED}	interface_index	interface_name
	[Teardown]	SUITE:Disable LLDP	hostshared_session

Test port_id=interface_name and port_description=interface_name in shared device
	SUITE:Enable LLDP	hostshared_session	interface_name	interface_name
	CLI:Switch Connection	default
	Wait Until Keyword Succeeds	30s	6x	SUITE:Check LLDP entry	${HOSTSHARED}	interface_name	interface_name
	[Teardown]	SUITE:Disable LLDP	hostshared_session

*** Keywords ***
SUITE:Setup
	Skip If	not ${HAS_HOSTSHARED}	LLDP functionality only works for devices in same subnet
	CLI:Open
	${HOST_SYSTEM}	CLI:Get System Name
	Set Suite Variable	${HOST_SYSTEM}
	${HOST_VERSION}	CLI:Get System Version
	Set Suite Variable	${HOST_VERSION}
	${HOST_IPV6}	CLI:Get Interface Ipv6 Address	${CONNECTION}
	Set Suite Variable	${HOST_IPV6}
	${HOST_MAC}	CLI:Get Interface MAC Address	${CONNECTION}
	Set Suite Variable	${HOST_MAC}
	CLI:Open	session_alias=hostshared_session	HOST_DIFF=${HOSTSHARED}
	${SHARED_SYSTEM}	CLI:Get System Name
	Set Suite Variable	${SHARED_SYSTEM}
	${SHARED_VERSION}	CLI:Get System Version
	Set Suite Variable	${SHARED_VERSION}
	${SHARED_IPV6}	CLI:Get Interface Ipv6 Address	${CONNECTION}
	Set Suite Variable	${SHARED_IPV6}
	${SHARED_MAC}	CLI:Get Interface MAC Address	${CONNECTION}
	Set Suite Variable	${SHARED_MAC}

SUITE:Teardown
	Skip If	not ${HAS_HOSTSHARED}	LLDP functionality only works for devices in same subnet
	SUITE:Disable LLDP	default
	CLI:Close Connection

SUITE:Enable LLDP
	[Arguments]	${SESSION}	${PORT_ID}	${PORT_DESCRIPTION}
	CLI:Switch Connection	${SESSION}
	CLI:Enter Path	/settings/network_connections/${CONNECTION}
	CLI:Set	enable_lldp=yes port_id=${PORT_ID} port_description=${PORT_DESCRIPTION}
	CLI:Commit

SUITE:Disable LLDP
	[Arguments]	${SESSION}
	CLI:Switch Connection	${SESSION}
	CLI:Enter Path	/settings/network_connections/${CONNECTION}
	CLI:Set	enable_lldp=no
	CLI:Commit

SUITE:Check LLDP entry
	[Arguments]	${IP_ADDRESS}	${PORT_ID}	${PORT_DESCRIPTION}
	${LLDP_ENTRIES}	CLI:Show	/system/lldp
	${LLDP_INFO}	Run Keyword If	'${IP_ADDRESS}' == '${HOST}'	Get Lines Containing String	${LLDP_ENTRIES}	${HOST}
	...	ELSE	Get Lines Containing String	${LLDP_ENTRIES}	${HOSTSHARED}
	Should Contain	${LLDP_INFO}	${CONNECTION}
	IF	'${IP_ADDRESS}' == '${HOST}'
		Should Contain	${LLDP_INFO}	${HOST_SYSTEM}
		Should Contain	${LLDP_INFO}	${HOST_VERSION}
		Should Contain	${LLDP_INFO}	${HOST_IPV6}
		Should Contain	${LLDP_INFO}	${HOST_MAC}
	ELSE
		Should Contain	${LLDP_INFO}	${SHARED_SYSTEM}
		Should Contain	${LLDP_INFO}	${SHARED_VERSION}
		Should Contain	${LLDP_INFO}	${SHARED_IPV6}
		Should Contain	${LLDP_INFO}	${SHARED_MAC}
	END
	IF	'${PORT_ID}' == 'interface_index' and '${PORT_DESCRIPTION}' == 'interface_description'
		Should Match Regexp	${LLDP_INFO}	(.*)local\\s\\d\\s+Interface\\s\\d\\sas\\s${INTERFACE}(.*)
	ELSE IF	'${PORT_ID}' == 'interface_name' and '${PORT_DESCRIPTION}' == 'interface_description'
		Should Match Regexp	${LLDP_INFO}	(.*)local\\s${INTERFACE}\\s+Interface\\s\\d\\sas\\s${INTERFACE}(.*)
	ELSE IF	'${PORT_ID}' == 'interface_index' and '${PORT_DESCRIPTION}' == 'interface_name'
		Should Match Regexp	${LLDP_INFO}	(.*)local\\s\\d\\s+${INTERFACE}(.*)
	ELSE IF	'${PORT_ID}' == 'interface_name' and '${PORT_DESCRIPTION}' == 'interface_name'
		Should Match Regexp	${LLDP_INFO}	(.*)local\\s${INTERFACE}\\s+${INTERFACE}(.*)
	END