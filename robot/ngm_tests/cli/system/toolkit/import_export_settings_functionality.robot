*** Settings ***
Resource	../../init.robot
Documentation	Test toolkit import/export and apply/save settings functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	SSH	EXCLUDEIN3_2	EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${BACKUP_FILENAME}	test-backup-settings
${EXPORT_FILENAME}	test-export-settings.config
${EXPORT_FILENAME_AFTER}	test-export-settings2.config
${EXPORT_DIFF_FILE}	test-export-settings.diff

# Configuration for persistency
${CONFIG_CUSTOMFIELD_NAME}	customfield_testing_import_export
${CONFIG_CUSTOMFIELD_VALUE}	customvalue_testing_import_export
${CONFIG_DEVICE_NAME}	myself
${CONFIG_DEVICE_TYPE}	device_console
${CONFIG_DEVICE_IP}	127.0.0.1
${CONFIG_DEVICE_USER}	${DEFAULT_USERNAME}
${CONFIG_DEVICE_PASSWD}	${DEFAULT_PASSWORD}
${CONFIG_HOST_IP}	1.1.1.1
${CONFIG_HOST_NAME}	aliasTestingImportExport
${CONFIG_HOST_ALIAS}	aliasTestingImportExport
${CONFIG_LOCALACCOUNT_NAME}	user_testing_import_export
${CONFIG_LOCALACCOUNT_PASSWD}	user_testing_import_export
${CONFIG_VLAN}	100

${NIGHTLY}	False	#workaroung because fix is still not on 5.2 and 5.0 official images

*** Test Cases ***
Test backup current settings
	CLI:Switch Connection	default
	CLI:Write	save_settings
	CLI:Set	destination=local_system filename=${BACKUP_FILENAME}
	CLI:Commit
	CLI:Write	finish

	CLI:Switch Connection	root_session
	${OUTPUT}=	CLI:Write	ls /backup/
	Should Contain	${OUTPUT}	${BACKUP_FILENAME}

Test Export current settings
	CLI:Switch Connection	default
	Set Client Configuration	timeout=240s
	SUITE:Add Configuration To Check Persistency When Importing

	CLI:Write	export_settings /settings --plain-password --file /home/${DEFAULT_USERNAME}/${EXPORT_FILENAME}
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}

	CLI:Switch Connection	root_session
	${OUTPUT}=	CLI:Write	ls /home/${DEFAULT_USERNAME}/
	Should Contain	${OUTPUT}	${EXPORT_FILENAME}
	CLI:Write	mv /home/${DEFAULT_USERNAME}/${EXPORT_FILENAME} /backup/
	[Teardown]	CLI:Close Connection

Test delete configuration to Clear settings os host
	CLI:Open
	SUITE:Delete Configuration From Testing Persistency
	${CONF_PERSISTED}	Run Keyword And Return Status	SUITE:Check Persistency
	Should Not Be True	${CONF_PERSISTED}

Test Import the previously exported settings
	[Tags]	BUG_NG_13141	NON-CRITICAL
	[Setup]	CLI:Open
	Set Client Configuration	timeout=350s
	${OUTPUT}=	CLI:Write	import_settings --file /backup/${EXPORT_FILENAME}
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Should Match Regexp	${OUTPUT}	Processed paths: \\d+ Rejected paths: 0
	Sleep	2s
	SUITE:Check Persistency

Test Export current settings and compare with the previous one
	[Setup]	CLI:Open
	Set Client Configuration	timeout=360s
	CLI:Write	export_settings /settings --plain-password --file /home/${DEFAULT_USERNAME}/${EXPORT_FILENAME_AFTER}
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}

	CLI:Connect As Root
	${OUTPUT}=	CLI:Write	ls /home/${DEFAULT_USERNAME}/
	Should Contain	${OUTPUT}	${EXPORT_FILENAME_AFTER}
	CLI:Write	mv /home/${DEFAULT_USERNAME}/${EXPORT_FILENAME_AFTER} /backup/
	CLI:Write	diff /backup/${EXPORT_FILENAME} /backup/${EXPORT_FILENAME_AFTER} > /backup/${EXPORT_DIFF_FILE}
	CLI:Write	cat /backup/${EXPORT_DIFF_FILE}

Test Restore Backup Settings
	CLI:Switch Connection	default
	Set Client Configuration	timeout=120s
	SUITE:Restore Backup
	[Teardown]	CLI:Close Connection

Test Export and Import Settings For USB Port
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	Skip If	not ${HAS_USB}
	CLI:Open
	${NIGHTLY}	Set Variable	True	#workaroung because fix is still not on 5.2 and 5.0 official images
	SUITE:Create USB Export Settings
	IF	'${NGVERSION}' >= '5.6'
		SUITE:Import USB Setting	test-usb-kvm	usb_kvm
		SUITE:Import USB Setting	test-usb-sensor	usb_sensor
		SUITE:Import USB Setting	test-usb-serial	usb_serialB
		SUITE:Import USB Setting	test-usb-device	usb_device
	ELSE
		SUITE:Import USB Setting	test-usb-kvm	usb_serialB
		SUITE:Import USB Setting	test-usb-sensor	usb_serialB
		SUITE:Import USB Setting	test-usb-serial	usb_serialB
		SUITE:Import USB Setting	test-usb-device	usb_serialB
	END

*** Keywords ***
SUITE:Setup
	CLI:Open
	Set Client Configuration	timeout=60s
	CLI:Connect As Root
	CLI:Switch Connection	default
	${IS_SR}=	CLI:Is SR System
	Set Suite Variable	${IS_SR}
	${HAS_USB}	CLI:Has USB Ports
	Set Suite Variable	${HAS_USB}
	SUITE:Delete Configuration From Testing Persistency

SUITE:Teardown
	CLI:Open
	Set Client Configuration	timeout=120s
	SUITE:Delete Configuration From Testing Persistency
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Run Keyword If	'${NGVERSION}' > '5.0' and ${HAS_USB} and ${NIGHTLY}	SUITE:Import USB Setting	${USB_DEVICES}[0]	usb_serialB	#BUG_NG_8118	BUG_NG_7287
	CLI:Close Connection

SUITE:Restore Backup
	CLI:Write	apply_settings
	CLI:Set	from=local_system filename=${BACKUP_FILENAME}
	Write	commit
	Log To Console	\nApplying settings\n
	Read Until	:
	Set Client Configuration	timeout=120s
	CLI:Write	yes
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Write	finish

SUITE:Delete Configuration From Testing Persistency
	CLI:Switch Connection	default
	CLI:Delete Devices	${CONFIG_DEVICE_NAME}
	CLI:Enter Path	/settings/custom_fields/
	Run Keyword If	'${NGVERSION}' == '4.2'	CLI:Delete If Exists	1
	...	ELSE	CLI:Delete If Exists	${CONFIG_CUSTOMFIELD_NAME}
	CLI:Enter Path	/settings/hosts/
	CLI:Delete If Exists	${CONFIG_HOST_IP}
	CLI:Delete Users	${CONFIG_LOCALACCOUNT_NAME}
	Run Keyword If	${IS_SR}	CLI:Delete VLAN	${CONFIG_VLAN}

SUITE:Add Configuration To Check Persistency When Importing
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/custom_fields/
	CLI:Add
	CLI:Set	field_name=${CONFIG_CUSTOMFIELD_NAME} field_value=${CONFIG_CUSTOMFIELD_VALUE}
	CLI:Commit
	Wait Until Keyword Succeeds	3x	1m	SUITE:Make Sure Device Was Added

	CLI:Enter Path	/settings/hosts
	CLI:Add
	CLI:Set	ip_address=${CONFIG_HOST_IP} hostname=${CONFIG_HOST_NAME} alias=${CONFIG_HOST_ALIAS}
	CLI:Commit

	CLI:Enter Path	/settings/local_accounts
	CLI:Add
	CLI:Set	username=${CONFIG_LOCALACCOUNT_NAME} password=${CONFIG_LOCALACCOUNT_PASSWD}
	CLI:Commit

	Return From Keyword If	not ${IS_SR}

	${SWITCH_INTERFACES}=	CLI:Get Switch Interfaces
	${INTERFACE1}=	Get From List	${SWITCH_INTERFACES}	0
	${INTERFACE2}=	Get From List	${SWITCH_INTERFACES}	1
	Set Suite Variable	${INTERFACE1}
	Set Suite Variable	${INTERFACE2}
	CLI:Add VLAN	${CONFIG_VLAN}	${INTERFACE1}	${INTERFACE2}

SUITE:Check Persistency
	CLI:Switch Connection	default
	CLI:Show	/settings/custom_fields/
	${OUTPUT}=	Run Keyword If	'${NGVERSION}' == '4.2'
	...	CLI:Show	/settings/custom_fields/1
	...	ELSE	CLI:Show	/settings/custom_fields/${CONFIG_CUSTOMFIELD_NAME}
	Should Match Regexp	${OUTPUT}	field( |_)name: ${CONFIG_CUSTOMFIELD_NAME}
	Should Match Regexp	${OUTPUT}	field( |_)value( =|:) ${CONFIG_CUSTOMFIELD_VALUE}

	${OUTPUT}=	CLI:Show	/settings/devices/${CONFIG_DEVICE_NAME}/access
	Should Contain	${OUTPUT}	name: ${CONFIG_DEVICE_NAME}
	Should Contain	${OUTPUT}	type = ${CONFIG_DEVICE_TYPE}
	Should Contain	${OUTPUT}	ip_address = ${CONFIG_DEVICE_IP}
	Should Contain	${OUTPUT}	username = ${CONFIG_DEVICE_USER}
	Should Contain	${OUTPUT}	password =

	${OUTPUT}=	CLI:Show	/settings/hosts/${CONFIG_HOST_IP}
	Should Match Regexp	${OUTPUT}	ip( |_)address: ${CONFIG_HOST_IP}
	Should Contain	${OUTPUT}	hostname = ${CONFIG_HOST_NAME}
	Should Contain	${OUTPUT}	alias = ${CONFIG_HOST_ALIAS}

	${OUTPUT}=	CLI:Show	/settings/local_accounts/${CONFIG_LOCALACCOUNT_NAME}
	Should Contain	${OUTPUT}	username: ${CONFIG_LOCALACCOUNT_NAME}
	Should Contain	${OUTPUT}	password =

	Return From Keyword If	not ${IS_SR}

	${OUTPUT}=	CLI:Show	/settings/switch_vlan/${CONFIG_VLAN}
	Should Contain	${OUTPUT}	tagged_ports = ${INTERFACE1}
	Should Contain	${OUTPUT}	untagged_ports = ${INTERFACE2}

SUITE:Make Sure Device Was Added
	CLI:Add Device	${CONFIG_DEVICE_NAME}	${CONFIG_DEVICE_TYPE}	${CONFIG_DEVICE_IP}	${CONFIG_DEVICE_USER}	${CONFIG_DEVICE_PASSWD}
	[Teardown]	CLI:Cancel	Raw

SUITE:Create USB Export Settings
	@{USB_DEVICES}	CLI:Get USB Devices List
	Set Suite Variable	${USB_DEVICES}
	CLI:Write	export_settings /settings/devices/${USB_DEVICES}[0] --plain-password --file /home/admin/${USB_DEVICES}[0]
	${TEST-USB}	Create Dictionary	name=${USB_DEVICES}[0]
	${TEST-USB-KVM}	Create Dictionary	name=test-usb-kvm	other_configs=mode=on-demand icon=kvm.png	type=type=usb_kvm
	${TEST-USB-SENSOR}	Create Dictionary	name=test-usb-sensor	other_configs=mode=on-demand icon=usb.png	type=type=usb_sensor
	${TEST-USB-SERIAL}	Create Dictionary	name=test-usb-serial	other_configs=mode=on-demand icon=serial_console.png	type=type=usb_serialb
	${TEST-USB-DEVICE}	Create Dictionary	name=test-usb-device	other_configs=mode=on-demand icon=server.png	type=type=usb_device
	${NEW_DEVICES}	Create List	${TEST-USB}	${TEST-USB-KVM}	${TEST-USB-SENSOR}	${TEST-USB-SERIAL}	${TEST-USB-DEVICE}
	FOR	${i}	IN RANGE	1	5
		CLI:Enter Path	/settings/devices/
		${j}	set variable	${i-1}
		${CURRENT_NAME}	Get From Dictionary	${NEW_DEVICES}[${j}]	name
		${NEW_NAME}	Get From Dictionary	${NEW_DEVICES}[${i}]	name
		CLI:Write	rename ${CURRENT_NAME}
		CLI:Set	new_name=${NEW_NAME}
		CLI:Commit
		CLI:Cancel	raw
		CLI:Enter Path	${NEW_NAME}/access
		${OTHER_CONFIGS}	Get From Dictionary	${NEW_DEVICES}[${i}]	other_configs
		CLI:Set	${OTHER_CONFIGS}
		${TYPE}	Run Keyword If	'${NGVERSION}' >= '5.6'	Get From Dictionary	${NEW_DEVICES}[${i}]	type
		Run Keyword If	'${NGVERSION}' >= '5.6'	CLI:Set	${TYPE}
		CLI:Commit
		CLI:Write	export_settings /settings/devices/${NEW_NAME} --plain-password --file /home/admin/${NEW_NAME}
	END
	SUITE:Import USB Setting	${USB_DEVICES}[0]	usb_serialB

SUITE:Import USB Setting
	[Arguments]	${CONFIG_FILE}	${TYPE}
	Write	import_settings --file /home/admin/${CONFIG_FILE}
	${OUTPUT}	Wait Until Keyword Succeeds	12x	5s	Read Until Prompt
	${FOLDERS}	Create List	access
	Run Keyword If	'${TYPE}'!='usb_device'	Append To List	${FOLDERS}	management
	Run Keyword If	'${TYPE}'!='usb_device' and '${TYPE}'!='usb_sensor' and '${TYPE}'!='usb_kvm'	Append To List	${FOLDERS}	commands/console	commands/data_logging
	Run Keyword If	'${TYPE}'!='usb_device' and '${TYPE}'!='usb_sensor' and '${TYPE}'!='usb_kvm'	Append To List	${FOLDERS}	logging
	FOR	${FOLDER}	IN	@{FOLDERS}
		Should Contain	${OUTPUT}	/settings/devices/${CONFIG_FILE}/${FOLDER} Result: succeeded
	END
	Should Contain	${OUTPUT}	Rejected paths: 0
	${OUTPUT}	CLI:Show	/settings/devices/
	${LINE}	Get Lines Matching Regexp	${OUTPUT}	(\\s)*${CONFIG_FILE}(.)+
	Should Match Regexp	${LINE}	(\\s)*${CONFIG_FILE}(\\s)+usbS([0-9]+(-)?[0-9]*)(\\s)+${TYPE}(\\s)+(enabled|on-demand|disabled|discovered)(\\s)+(.)+

