*** Settings ***
Resource	../../init.robot
Documentation	Test diagnostic data command thru CLI and shell
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	SSH	SHOW	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
${SUCCESS_MSG}	diagnostic data: Diagnostic data collection is in progress. This operation may take some minutes to complete.
${ERROR_MSG}	diagnostic data: Not enough space to collect diagnostic data.
${LOG_DIR}	/var/log
${BIG_FILE_PATH}	${LOG_DIR}/mytest.log
*** Test Cases ***
Test diagnostic_data directory tree
	[Documentation]	Tests the directory tree of a normal diagnostic data file
	SUITE:Check Space In /tmp Directory
	SUITE:Run Diagnostic Data
	Sleep	5s
	SUITE:Decompress Diagnostic Data File
	SUITE:Check Directory Tree
	[Teardown]	SUITE:Delete All Files Related To Diagnostic Data

Test diagnostic_data logs masks IP addresses and users
	[Documentation]	Tests if diagnostic data logs masks sensible information (IP address and users).
	SUITE:Check Space In /tmp Directory
	SUITE:Run Diagnostic Data
	Sleep	5s
	SUITE:Decompress Diagnostic Data File
	CLI:Enter Path	log
	${MASKED_IFCONFIG}	CLI:Write	cat status/ifconfig.txt | grep inet	user=Yes
	Should Contain	${MASKED_IFCONFIG}	MASKED_IP
	Should Not Contain	${MASKED_IFCONFIG}	${HOST}
	${MASKED_PS}	CLI:Write	cat status/ps.txt | grep inet	user=Yes
	Should Contain	${MASKED_PS}	MASKED_USER
	Should Not Contain	${MASKED_PS}	${HOST}
	Should Not Contain	${MASKED_PS}	${DEFAULT_USERNAME}
	[Teardown]	SUITE:Delete All Files Related To Diagnostic Data

Test diagnostic_data directory tree with a big file
	[Documentation]	Tests if diagnostic data runs and logs an error in the directory tree saying the file is too big
	...	to collect and only this file will not be logged.
	CLI:Switch Connection	root_session
	CLI:Write	fallocate -x -l 1.5GB ${BIG_FILE_PATH}
	SUITE:Check Space In /tmp Directory	ENOUGH=${FALSE}
#test diagnostic_data command directly in root shell
	CLI:Write	diagnostic_data
	Sleep	5s
	SUITE:Decompress Diagnostic Data File	AS_ROOT=${TRUE}
	SUITE:Check Directory Tree	ERROR=${TRUE}	AS_ROOT=${TRUE}
	[Teardown]	SUITE:Delete All Files Related To Diagnostic Data

Test diagnostic_data with no space in /tmp
	[Documentation]	Tests if diagnostic data does not run and shows an error when trying to collect a file
	...	bigger than avaliable /tmp space
	Skip If	${NGVERSION} < 5.0	Error message not implemented in v4.2
	CLI:Switch Connection	root_session
	CLI:Write	fallocate -x -l 5GB ${BIG_FILE_PATH}
	SUITE:Check Space In /tmp Directory	ENOUGH=${FALSE}
	SUITE:Run Diagnostic Data	ERROR=${TRUE}
	[Teardown]	SUITE:Delete All Files Related To Diagnostic Data

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Connect As Root

SUITE:Teardown
	CLI:Close Connection

SUITE:Check Space In /tmp Directory
	[Arguments]	${ENOUGH}=${TRUE}
	CLI:Switch Connection	root_session
	${SPACE_IN_TMP}	CLI:Write	df -h /tmp	user=Yes
	Run Keyword If	${ENOUGH}	Should Not Contain	${SPACE_IN_TMP}	100\%

SUITE:Run Diagnostic Data
	[Documentation]	Runs diagnostic data (only on CLI) and checks the error/success messages
	[Arguments]	${ERROR}=${FALSE}
	CLI:Switch Connection	default
	CLI:Enter Path	/system/toolkit
	${OUTPUT}	CLI:Write	diagnostic_data
	IF	${ERROR}
		Should Not Contain	${OUTPUT}	${SUCCESS_MSG}
		Should Contain	${OUTPUT}	${ERROR_MSG}
	ELSE
		Should Contain	${OUTPUT}	${SUCCESS_MSG}
		Should Not Contain	${OUTPUT}	${ERROR_MSG}
	END
	CLI:Write	finish

SUITE:Decompress Diagnostic Data File
	[Documentation]	Depending if the command was executed in CLI or shell, gets the name of the file,
	...	decompresses it and check if the base directory is shown
	[Arguments]	${AS_ROOT}=${FALSE}
	CLI:Switch Connection	root_session
	IF	${AS_ROOT}
		CLI:Enter Path	/tmp
		${DIAGNOSTIC_DATA_FILE}	CLI:Write	ls /tmp | grep logs_	user=Yes
	ELSE
		CLI:Enter Path	/home/admin/logs
		${DIAGNOSTIC_DATA_FILE}	Wait Until Keyword Succeeds	30s	5s	SUITE:Check Diagnostic Data File Is Created
	END
	CLI:Write	tar -xf ${DIAGNOSTIC_DATA_FILE}
	${PATHS}	CLI:Ls	user=Yes
	Should Contain	${PATHS}	log
	[Return]	${DIAGNOSTIC_DATA_FILE}

SUITE:Check Directory Tree
	[Documentation]	Checks Directory Tree and check the errors file if it exists
	[Arguments]	${ERROR}=${FALSE}	${AS_ROOT}=${FALSE}
	IF	${AS_ROOT}
		CLI:Enter Path	/tmp
	ELSE
		CLI:Enter Path	/home/admin/logs
	END
	CLI:Enter Path	log
	${SUBDIRECTORIES}	CLI:Ls
	Should Contain	${SUBDIRECTORIES}	configs
	Should Contain	${SUBDIRECTORIES}	logs
	Should Contain	${SUBDIRECTORIES}	status
	IF	${ERROR}
		Should Contain	${SUBDIRECTORIES}	diagnostic_data_errors.txt
		${ERRORS}	CLI:Write	cat diagnostic_data_errors.txt	user=Yes
		Should Contain	${ERRORS}	Not enough space in /tmp to collect ${LOG_DIR}
	END
	${CONFIGS}	CLI:Ls	configs/	user=Yes
	Should Not Be Empty	${CONFIGS}
	${LOGS}	CLI:Ls	logs/	user=Yes
	Should Not Be Empty	${LOGS}
	${STATUS}	CLI:Ls	status/	user=Yes
	Should Not Be Empty	${STATUS}

SUITE:Delete All Files Related To Diagnostic Data
	[Documentation]	Checks for the files created and, if they exist, deletes them
	CLI:Switch Connection	root_session
	CLI:Enter Path	/home/${DEFAULT_USERNAME}
	${OUTPUT}	CLI:Ls
	${LOGS_PATH}	Run Keyword And Return Status	Should Contain	${OUTPUT}	logs
	Run keyword If	${LOGS_PATH}	CLI:Write	rm -r logs
	CLI:Enter Path	/tmp
	${OUTPUT}	CLI:Ls
	${LOGS_FILE}	Run Keyword And Return Status	Should Contain	${OUTPUT}	logs_
	Run keyword If	${LOGS_FILE}	CLI:Write	rm logs_*
	${LOG_PATH}	Run Keyword And Return Status	Should Contain	${OUTPUT}	log${SPACE}
	Run keyword If	${LOG_PATH}	CLI:Write	rm -r log
	CLI:Enter Path	${LOG_DIR}
	${OUTPUT}	CLI:Ls
	${MYTEST_FILE}	Run Keyword And Return Status	Should Contain	${OUTPUT}	mytest.log
	Run keyword If	${MYTEST_FILE}	CLI:Write	rm mytest.log

SUITE:Check Diagnostic Data File Is Created
	${DIAGNOSTIC_DATA_FILE}	CLI:Ls	user=Yes
	Should Not Be Empty	${DIAGNOSTIC_DATA_FILE}
	[Return]	${DIAGNOSTIC_DATA_FILE}