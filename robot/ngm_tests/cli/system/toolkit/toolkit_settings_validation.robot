*** Settings ***
Resource	../../init.robot
Documentation	Test toolkit settings
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
@{VALID_COUNTRY_CODES}	  US  BR  GB  IN
@{VALID_STATES}           CA  SC  IRE  KA		California	${TWO_WORD}	${ELEVEN_WORD}
@{VALID_LOCALITIES}       Fremont  Dublin  Blumenau  Bangalore
@{VALID_COMMOM_NAMES}	  www.zpesystems.com	www.zpebr.com.br	zpesystems.net	zpe.co.uk
@{VALID_EMAIL_ADDRESSES}  testeUS@zpesystems.com  testeBR@zpesystems.com  testeGB@zpesystems.com  testeIN@zpesystems.com
@{CERTIFICATE_INFOR}	  BEGIN CERTIFICATE  END CERTIFICATE
@{ALL_VALUES}=            ${WORD}     ${ONE_WORD}     ${TWO_WORD}     ${THREE_WORD}
                          ...     ${SEVEN_WORD}   ${EIGHT_WORD}   ${ELEVEN_WORD}  ${NUMBER}   ${ONE_NUMBER}
                          ...     ${TWO_NUMBER}   ${THREE_NUMBER}     ${SEVEN_NUMBER}     ${EIGHT_NUMBER}
                          ...     ${ELEVEN_NUMBER}    ${WORD_AND_NUMBER}  ${NUMBER_AND_WORD}
                          ...     ${ONE_POINTS}   ${TWO_POINTS}   ${THREE_POINTS}
                          ...     ${NUMBER_AND_POINTS}	"${WORDS_SPACES}"	"${WORDS_TWO_SPACES}"
*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/system/toolkit/
	CLI:Test Available Commands	hostname	apply_settings
	...	show	exit	save_settings
	...	cd	ls	shutdown	factory_settings	shell	event_system_clear
	...	change_password	pwd	software_upgrade	commit	quit	system_certificate
	...	event_system_audit	reboot	system_config_check	revert	whoami
	Run Keyword If	${NGVERSION} >= 5.0
	...	CLI:Test Available Commands	import_settings	export_settings

Test show and ls command
	CLI:Enter Path	/system/toolkit/
	CLI:Test Show Command	${EMPTY}
	CLI:Test Ls Command	${EMPTY}

Test available commands for command=save_settings
	CLI:Enter Path	/system/toolkit/
	CLI:Write	save_settings
	CLI:Test Available Commands	cancel	commit	ls	save	set	show
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field destination for command=save_settings
	CLI:Enter Path	/system/toolkit/
	CLI:Write	save_settings

	SUITE:Set Invalid Values Error Messages	destination
	CLI:Test Set Field Options	destination	local_system	remote_server
	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field destination=local_system for command=save_settings
	CLI:Enter Path	/system/toolkit/
	CLI:Write	save_settings

	CLI:Set Field	destination	local_system
	CLI:Test Show Command	destination = local_system	filename
	...	Current configuration settings will be saved to one of the destinations.
	${OUTPUT}=	CLI:Commit	Yes
	Should Contain	${OUTPUT}	Error: filename: Incorrect filename information.
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field destination=remote_server for command=save_settings
	CLI:Enter Path	/system/toolkit/
	CLI:Write	save_settings

	CLI:Set Field	destination	remote_server
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Show Command	destination = remote_server	url	username	password
	...	the_path_in_url_to_be_used_as_absolute_path_name = no	Current configuration settings will be saved to one of the destinations.
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Show Command	destination = remote_server	url	username	password
	...	Current configuration settings will be saved to one of the destinations.
	${OUTPUT}=	CLI:Commit	Yes


	Should Contain	${OUTPUT}	Error: url: Incorrect URL information.

	CLI:Test Set Field Invalid Options	url	invalid_url	Error: url: Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>	yes

	CLI:Test Set Field Invalid Options	url	https://invalid/invalid	Error: url: Invalid protocol. Supported: ftp, tftp, sftp and scp.	yes

	CLI:Test Set Field Invalid Options	url	ftp://1.1.1.1:21/	Error: url: Protocol requires username/password.	yes

	Run Keyword If	'${NGVERSION}' >= '4.2'	SUITE:Set Invalid Values Error Messages	the_path_in_url_to_be_used_as_absolute_path_name
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Options	the_path_in_url_to_be_used_as_absolute_path_name	no	yes
	[Teardown]	CLI:Cancel	Raw

Test available commands for command=apply_settings
	CLI:Enter Path	/system/toolkit/
	CLI:Write	apply_settings
	CLI:Test Available Commands	apply	cancel	commit	ls	set	show
	[Teardown]	CLI:Cancel	Raw

Test same behavior of apply and commit command for command=apply_settings
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/system/toolkit/
	CLI:Write	apply_settings
	Write	apply
	${OUTPUT}=	Read Until	:
	Write	no
	read until prompt
	Should Contain	${OUTPUT}	applying configuration settings may disconnect active sessions.

	Write	commit
	${OUTPUT}=	Read Until	:
	Write	no
	read until prompt
	Should Contain	${OUTPUT}	applying configuration settings may disconnect active sessions.
	[Teardown]	CLI:Cancel	Raw

Test invalid values for command=apply_settings
	CLI:Enter Path	/system/toolkit/
	CLI:Write	apply_settings
	SUITE:Set Invalid Values Error Messages	from
	CLI:Test Set Field Options	from	local_system	remote_server
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field from=local_system for command=apply_settings
	CLI:Enter Path	/system/toolkit/
	CLI:Write	apply_settings

	CLI:Set Field	from	local_system

	CLI:Test Show Command	from = local_system	filename	Apply configuration from one of the locations. This procedure may disconnect active sessions.

	Run Keyword If	'${NGVERSION}' >= '4.2'	Run Keywords	Write	commit	AND	read until	:	AND	Write	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	Write	commit
	${OUTPUT}=	Read Until Prompt
	Should Contain	${OUTPUT}	Error: filename: Incorrect filename information.

	CLI:Set Field	filename	not_found
	Run Keyword If	'${NGVERSION}' >= '4.2'	Run Keywords	Write	commit	AND	read until	:	AND	Write	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	Write	commit
	${OUTPUT}=	Read Until Prompt
	Should Contain	${OUTPUT}	Error: Apply configuration settings failed. File not found.
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field from=remote_server for command=apply_settings
	CLI:Enter Path	/system/toolkit/
	CLI:Write	apply_settings

	CLI:Set Field	from	remote_server
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Show Command	from = remote_server	url	username	password	the_path_in_url_to_be_used_as_absolute_path_name = no	Apply configuration from one of the locations. This procedure may disconnect active sessions.
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Show Command	from = remote_server	url	username	password	Apply configuration from one of the locations. This procedure may disconnect active sessions.

	Run Keyword If	'${NGVERSION}' >= '4.2'	SUITE:Set Invalid Values Error Messages	the_path_in_url_to_be_used_as_absolute_path_name
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Options	the_path_in_url_to_be_used_as_absolute_path_name	no	yes

	SUITE:Invalid URL
	[Teardown]	CLI:Cancel	Raw

Test command=system_certificate
	CLI:Enter Path	/system/toolkit/
	CLI:Write	system_certificate
	CLI:Test Available Commands	apply	cancel	commit	ls	set	show
	CLI:Test Set Available Fields	from	password	url	username
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Available Fields	the_path_in_url_to_be_used_as_absolute_path_name
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=url for command=system_certificate
	CLI:Enter Path	/system/toolkit/
	CLI:Write	system_certificate
	SUITE:Invalid URL
	[Teardown]	CLI:Cancel	Raw

Test invalid values for fields for command=system_certificate
	CLI:Enter Path	/system/toolkit/
	CLI:Write	system_certificate

	Run Keyword If	'${NGVERSION}' == '3.2'	SUITE:Set Invalid Values Error Messages	from
	Run Keyword If	'${NGVERSION}' >= '4.2'	SUITE:Set Invalid Values Error Messages	from	the_path_in_url_to_be_used_as_absolute_path_name
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Options	the_path_in_url_to_be_used_as_absolute_path_name	no	yes
	[Teardown]	CLI:Cancel	Raw

Test command=system_config_check
	CLI:Enter Path	/system/toolkit/
	CLI:Write	system_config_check
	CLI:Test Available Commands	apply	cancel	commit	ls	set	show
	CLI:Test Set Available Fields	action	checksum
	[Teardown]	CLI:Cancel	Raw

Test invalid values for command=system_config_check
	CLI:Enter Path	/system/toolkit/
	CLI:Write	system_config_check

	Run Keyword If	'${NGVERSION}' == '3.2'	SUITE:Set Invalid Values Error Messages	checksum
	Run Keyword If	'${NGVERSION}' >= '4.2'	SUITE:Set Invalid Values Error Messages	checksum	action

	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Options	action	baseline	compare	view
	CLI:Test Set Field Options	checksum	md5sum	sha256sum
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field=action for command=system_config_check
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path	/system/toolkit/
	CLI:Write	system_config_check
	CLI:Set Field	checksum	md5sum
	CLI:Set Field	action	compare
	CLI:Test Show Command	checksum = md5sum	action = compare	reference
	${OUTPUT}=	CLI:Commit	Yes
	${NO_BASELINE}=	Run Keyword And Return Status	Should Contain	${OUTPUT}	Error: Baseline filename must be provided
	Run Keyword If	'${NO_BASELINE}' == 'False'	CLI:Write	finish	Yes
	Run Keyword If	${NO_BASELINE}	CLI:Cancel
	[Teardown]	CLI:Cancel	Raw

Test command=factory_settings
	CLI:Enter Path	/system/toolkit/
	CLI:Write	factory_settings
	CLI:Test Available Commands	cancel	ls	restore	set	show
	CLI:Test Show Command	clear_all_log_files
	[Teardown]	CLI:Cancel	Raw

Test invalid values for command=factory_settings
	CLI:Enter Path	/system/toolkit/
	CLI:Write	factory_settings
	SUITE:Set Invalid Values Error Messages	clear_all_log_files
	CLI:Test Set Field Options	clear_all_log_files	yes	no
	[Teardown]	CLI:Cancel	Raw

Test command=software_upgrade
	CLI:Enter Path	/system/toolkit/
	CLI:Write	software_upgrade
	CLI:Test Available Commands	cancel	commit	ls	set	show	upgrade
	CLI:Test Set Available Fields	filename	if_downgrading	format_partitions_before_upgrade	image_location
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Show Command	image_location = local_system	filename	Image files must be previously copied to /var/sw directory.
	...	format_partitions_before_upgrade = no	if_downgrading = restore_configuration_saved_on_version_upgrade	The system will reboot automatically to complete upgrade process.
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Show Command	format_partitions_before_upgrade = no
	...	if_downgrading = restore_configuration_saved_on_version_upgrade	The system will reboot automatically to complete upgrade process.
	[Teardown]	CLI:Cancel	Raw

Test invalid values for command=software_upgrade
	CLI:Enter Path	/system/toolkit/
	CLI:Write	software_upgrade
	SUITE:Set Invalid Values Error Messages	if_downgrading	format_partitions_before_upgrade	image_location
	CLI:Test Set Field Options	if_downgrading	apply_factory_default_configuration	restore_configuration_saved_on_version_upgrade
	CLI:Test Set Field Options	format_partitions_before_upgrade	no	yes
	CLI:Test Set Field Options	image_location	local_system	remote_server
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field image_location=local_system for command=software_upgrade
	Set Tags	NON-CRITICAL	#3.2 Official doesn't show error
	CLI:Enter Path	/system/toolkit/
	CLI:Write	software_upgrade

	CLI:Set Field	image_location	local_system
	CLI:Test Show Command	Image files must be previously copied to /var/sw directory.
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Set Field	filename	${EMPTY}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Show Command Regexp	\\s+filename\\s=\\s+Image
	${OUTPUT}=	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Commit	Yes
	#Should Contain	${OUTPUT}	Error: filename: Incorrect filename information.	#This won't show on web

	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	Error: filename: Incorrect filename information.
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field image_location=remote_server for command=software_upgrade
	CLI:Enter Path	/system/toolkit/
	CLI:Write	software_upgrade
	CLI:Set Field	image_location	remote_server
	CLI:Test Set Available Fields	password	the_path_in_url_to_be_used_as_absolute_path_name	url	username

	CLI:Set Field	url	${EMPTY}
	${OUTPUT}=	CLI:Commit	Yes

	Should Contain	${OUTPUT}	Error: url: Incorrect URL information.

	CLI:Set Field	url	invalid_url
	${OUTPUT}=	CLI:Commit	Yes

	Should Contain	${OUTPUT}	Error: url: Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>

	CLI:Set Field	url	https://invalid/invalid
	${OUTPUT}=	CLI:Commit	Yes
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	Error: Failed to download image.
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${OUTPUT}	Error: Error: Failed to download image.

	CLI:Set Field	url	ftp://1.1.1.1:21/
	${OUTPUT}=	CLI:Commit	Yes

	Should Contain	${OUTPUT}	Error: url: Protocol requires username/password.

	Run Keyword If	'${NGVERSION}' >= '4.2'	SUITE:Set Invalid Values Error Messages	the_path_in_url_to_be_used_as_absolute_path_name
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Options	the_path_in_url_to_be_used_as_absolute_path_name	no	yes
	[Teardown]	CLI:Cancel	Raw

Test warning message for command=reboot
	Run Keyword If	'${NGVERSION}' >= '4.2'	Set Tags	BUG_1242
	CLI:Enter Path	/system/toolkit/
	Write	reboot
	${OUTPUT}=	Read Until	:
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	all active sessions will be dropped.\r\ndo you want to proceed with system reboot? (yes, no)
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${OUTPUT}	All active sessions will be dropped.\r\nDo you want to proceed with system reboot? (yes, no)
	Write	no
	${OUTPUT}=	CLI:Read Until Prompt
	Should Not Contain	${OUTPUT}	Error:
	[Teardown]	CLI:Write	no	Raw

Test warning message for command=shutdown
	CLI:Enter Path	/system/toolkit/
	Write	shutdown
	${OUTPUT}=	Read Until	:
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	all active sessions will be dropped and this system will be turned off.\r\ndo you want to proceed with system shutdown? (yes, no)
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${OUTPUT}	All active sessions will be dropped and this system will be turned off.\r\nDo you want to proceed with system shutdown? (yes, no)
	Write	no
	${OUTPUT}=	CLI:Read Until Prompt
	Should Not Contain	${OUTPUT}	Error:
	[Teardown]	CLI:Write	no	Raw

Test command=create_csr
	[Tags]  EXCLUDEIN3_2
	CLI:Enter Path	/system/toolkit/
	CLI:Write	create_csr
	CLI:Test Available Commands		cancel        commit        finish        generate_csr  ls            set           show
	CLI:Test Set Available Fields	common_name  locality  self_sign  validity  country_code  organization  state  email_address  organization_unit  subject_alternative_names
	CLI:Test Show Command			common_name  locality  self_sign  validity  country_code  organization  state  email_address  organization_unit  subject_alternative_names
	[Teardown]	CLI:Cancel	Raw

Test valid values for field country_code in command=create_csr
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path          /system/toolkit
    FOR    ${VALID_COUNTRY_CODE}   IN  @{VALID_COUNTRY_CODES}
       CLI:Write    create_csr
       CLI:Set      state=California locality=Fremont organization="ZPE Systems" organization_unit=Engineering
    	CLI:Set 	 common_name=www.zpesystems.com email_address=zpe@zpesystems.com
       CLI:Set      country_code=${VALID_COUNTRY_CODE}
       ${OUTPUT}=   CLI:Write    generate_csr
       CLI:Should Contain All    ${OUTPUT}  ${CERTIFICATE_INFOR}
    	CLI:Write  	finish
	END

Test invalid values for field country_code in command=create_csr
	[Tags]	EXCLUDEIN3_2
	${INVALID_COUNTRY_CODE}=   Create List     ${POINTS}   ${ONE_POINTS}   ${TWO_POINTS}
    ...     ${THREE_POINTS}     ${SEVEN_POINTS}   ${WORD_AND_POINTS}    ${NUMBER_AND_POINTS}    ${POINTS_AND_NUMBER}
    ...     ${POINTS_AND_WORD}   ${EIGHT_POINTS}
	CLI:Enter Path          /system/toolkit
    FOR    ${INVALID_COUNTRY_CODE}   IN  @{INVALID_COUNTRY_CODE}
       CLI:Write    create_csr
       CLI:Set      state=California locality=Fremont organization="ZPE Systems" organization_unit=Engineering
    	CLI:Set 	 common_name=www.zpesystems.com email_address=zpe@zpesystems.com
  	   CLI:Test Set Field Invalid Options     country_code    ${INVALID_COUNTRY_CODE}
       ...   Error: country_code: Illegal Characters. Use i.e. US   save=commit
       CLI:Cancel  Raw
	END
	${INVALID_COUNTRY_CODE_LENGTH}=   Create List     ${THREE_NUMBER}   ${THREE_WORD}   ${EXCEEDED}
	CLI:Enter Path          /system/toolkit
    FOR    ${INVALID_COUNTRY_CODE_LENGTH}   IN  @{INVALID_COUNTRY_CODE_LENGTH}
       CLI:Write    create_csr
       CLI:Set      state=California locality=Fremont organization="ZPE Systems" organization_unit=Engineering
    	CLI:Set 	 common_name=www.zpesystems.com email_address=zpe@zpesystems.com
  	   CLI:Test Set Field Invalid Options     country_code    ${INVALID_COUNTRY_CODE_LENGTH}
       ...   Error: country_code: Illegal Characters. Use i.e. US   save=commit
       CLI:Cancel  Raw
	END
	CLI:Write    create_csr
    CLI:Set      state=California locality=Fremont organization="ZPE Systems" organization_unit=Engineering
    CLI:Set 	 common_name=www.zpesystems.com email_address=zpe@zpesystems.com
	CLI:Test Set Field Invalid Options     country_code    ${EMPTY}
      ...   Error: country_code: Field must not be empty.   save=commit
    CLI:Cancel  Raw

Test valid values for field state in command=create_csr
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path          /system/toolkit
    FOR    ${VALID_STATE}   IN  @{VALID_STATES}
       CLI:Write    create_csr
       CLI:Set      country_code=US locality=Fremont organization="ZPE Systems" organization_unit=Engineering
    	CLI:Set 	 common_name=www.zpesystems.com email_address=zpe@zpesystems.com
       CLI:Set      state=${VALID_STATE}
       ${OUTPUT}=   CLI:Write    generate_csr
       CLI:Should Contain All    ${OUTPUT}  ${CERTIFICATE_INFOR}
    	CLI:Write  	finish
	END

Test invalid values for field state in command=create_csr
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	${INVALID_STATES}=   Create List     ${POINTS}  ${SEVEN_POINTS}   ${WORD_AND_POINTS}   ${POINTS_AND_NUMBER}
    ...     ${POINTS_AND_WORD}   ${EIGHT_POINTS}
	CLI:Enter Path          /system/toolkit
    FOR    ${INVALID_STATE}   IN  @{INVALID_STATES}
       CLI:Write    create_csr
       CLI:Set      country_code=US locality=Fremont organization="ZPE Systems" organization_unit=Engineering
    	CLI:Set 	 common_name=www.zpesystems.com email_address=zpe@zpesystems.com
  	   Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Set Field Invalid Options     state    ${INVALID_STATE}
       ...   Error: state: Illegal Characters. Use i.e. California   save=commit
       Run Keyword If	'${NGVERSION}' == '4.2'	CLI:Test Set Field Invalid Options     state    ${INVALID_STATE}
       ...   Error: Generated an invalid Certicate Signing Request   save=commit
       CLI:Cancel  Raw
	END
    CLI:Write    create_csr
    CLI:Set      country_code=US locality=Fremont organization="ZPE Systems" organization_unit=Engineering
    CLI:Set 	 common_name=www.zpesystems.com email_address=zpe@zpesystems.com
	CLI:Test Set Field Invalid Options     state    ${EMPTY}
      ...   Error: state: Field must not be empty.   save=commit
    CLI:Cancel  Raw

Test valid values for field locality in command=create_csr
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path          /system/toolkit
    FOR    ${VALID_LOCALITY}   IN  @{VALID_LOCALITIES}
       CLI:Write    create_csr
       CLI:Set      country_code=US state=CA organization="ZPE Systems" organization_unit=Engineering
    	CLI:Set 	 common_name=www.zpesystems.com email_address=zpe@zpesystems.com
       CLI:Set      locality=${VALID_LOCALITY}
       ${OUTPUT}=   CLI:Write    generate_csr
       CLI:Should Contain All    ${OUTPUT}  ${CERTIFICATE_INFOR}
    	CLI:Write  	finish
	END

Test invalid values for field locality in command=create_csr
	[Tags]	EXCLUDEIN3_2
	${INVALID_LOCALITIES}=   Create List     ${POINTS}   ${ONE_POINTS}   ${TWO_POINTS}
    ...     ${THREE_POINTS}     ${SEVEN_POINTS}   ${WORD_AND_POINTS}    ${NUMBER_AND_POINTS}    ${POINTS_AND_NUMBER}
    ...     ${POINTS_AND_WORD}   ${EIGHT_POINTS}
	CLI:Enter Path          /system/toolkit
    FOR    ${INVALID_LOCALITY}   IN  @{INVALID_LOCALITIES}
       CLI:Write    create_csr
       CLI:Set      country_code=US state=CA organization="ZPE Systems" organization_unit=Engineering
    	CLI:Set 	 common_name=www.zpesystems.com email_address=zpe@zpesystems.com
  	   Run Keyword If	'${NGVERSION}' >= '5.0'	CLI:Test Set Field Invalid Options     locality    ${INVALID_LOCALITY}
       ...   Error: locality: Illegal Characters. Use i.e Los Angeles   save=commit
       CLI:Cancel  Raw
	END
    CLI:Write    create_csr
    CLI:Set      country_code=US state=CA organization="ZPE Systems" organization_unit=Engineering
    CLI:Set 	 common_name=www.zpesystems.com email_address=zpe@zpesystems.com
	CLI:Test Set Field Invalid Options     locality    ${EMPTY}
      ...   Error: locality: Field must not be empty.   save=commit
    CLI:Cancel  Raw

Test valid values for field organization in command=create_csr
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path          /system/toolkit
    FOR    ${VALID_ORGANIZATION}   IN  @{ALL_VALUES}
       CLI:Write    create_csr
       CLI:Set      country_code=US state=CA locality=Fremont organization_unit=Engineering
    	CLI:Set 	 common_name=www.zpesystems.com email_address=zpe@zpesystems.com
       CLI:Set      organization=${VALID_ORGANIZATION}
       ${OUTPUT}=   CLI:Write    generate_csr
       CLI:Should Contain All    ${OUTPUT}  ${CERTIFICATE_INFOR}
    	CLI:Write  	finish
	END

Test invalid values for field organization in command=create_csr
	[Tags]	EXCLUDEIN3_2
	CLI:Write    create_csr
    CLI:Set      country_code=US state=CA locality=Fremont organization_unit=Engineering
    CLI:Set 	 common_name=www.zpesystems.com email_address=zpe@zpesystems.com
	CLI:Test Set Field Invalid Options     organization    ${EMPTY}
      ...   Error: organization: Field must not be empty.   save=commit
    CLI:Cancel  Raw

Test valid values for field organization_unit in command=create_csr
	[Tags]	EXCLUDEIN3_2
	@{VALID_ORG_UNITS}	Create List	${WORD}     ${ONE_WORD}     ${TWO_WORD}     ${THREE_WORD}
                        ...     ${SEVEN_WORD}   ${EIGHT_WORD}   ${ELEVEN_WORD}  ${NUMBER}   ${ONE_NUMBER}
                        ...     ${TWO_NUMBER}   ${THREE_NUMBER}     ${SEVEN_NUMBER}     ${EIGHT_NUMBER}
                        ...     ${ELEVEN_NUMBER}    ${WORD_AND_NUMBER}  ${NUMBER_AND_WORD}
	CLI:Enter Path          /system/toolkit
    FOR    ${VALID_ORG_UNIT}   IN  @{VALID_ORG_UNITS}
       CLI:Write    create_csr
       CLI:Set      country_code=US state=California locality=Fremont organization="ZPE Systems"
    	CLI:Set 	 common_name=www.zpesystems.com email_address=zpe@zpesystems.com
       CLI:Set      organization_unit=${VALID_ORG_UNIT}
       ${OUTPUT}=   CLI:Write    generate_csr
       CLI:Should Contain All    ${OUTPUT}  ${CERTIFICATE_INFOR}
    	CLI:Write  	finish
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field organization_unit in command=create_csr
	[Tags]	EXCLUDEIN3_2
	${INVALID_ORG_UNITS}=   Create List     ${POINTS}  ${SEVEN_POINTS}   ${WORD_AND_POINTS}   ${POINTS_AND_NUMBER}
    ...     ${POINTS_AND_WORD}   ${EIGHT_POINTS}
	CLI:Enter Path          /system/toolkit
    FOR    ${INVALID_ORG_UNIT}   IN  @{INVALID_ORG_UNITS}
       CLI:Write    create_csr
       CLI:Set      country_code=US state=CA locality=Fremont organization="ZPE Systems"
    	CLI:Set 	 common_name=www.zpesystems.com email_address=zpe@zpesystems.com
		CLI:Test Set Field Invalid Options     organization_unit    ${INVALID_ORG_UNIT}
       ...   Error: Generated an invalid Certicate Signing Request   save=commit
       CLI:Cancel  Raw
	END

    CLI:Write    create_csr
    CLI:Set      country_code=US state=CA locality=Fremont organization="ZPE Systems"
    CLI:Set 	 common_name=www.zpesystems.com email_address=zpe@zpesystems.com
	CLI:Test Set Field Invalid Options     organization_unit    ${EMPTY}
      ...   Error: organization_unit: Field must not be empty.   save=commit
    CLI:Cancel  Raw
	[Teardown]	CLI:Cancel	Raw

Test valid values for field common_name in command=create_csr
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path          /system/toolkit
    FOR    ${VALID_COMMOM_NAME}   IN  @{VALID_COMMOM_NAMES}
       CLI:Write    create_csr
       CLI:Set      country_code=US state=CA locality=CA organization="ZPE Systems"
    	CLI:Set 	 email_address=zpe@zpesystems.com organization_unit=Engineering
       CLI:Set      common_name=${VALID_COMMOM_NAME}
       ${OUTPUT}=   CLI:Write    generate_csr
       CLI:Should Contain All    ${OUTPUT}  ${CERTIFICATE_INFOR}
    	CLI:Write  	finish
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field common_name in command=create_csr
	[Tags]	EXCLUDEIN3_2
	${INVALID_COMMON_NAMES}=   Create List     ${POINTS}   ${ONE_POINTS}   ${TWO_POINTS}
    ...     ${THREE_POINTS}     ${SEVEN_POINTS}   ${WORD_AND_POINTS}    ${NUMBER_AND_POINTS}    ${POINTS_AND_NUMBER}
    ...     ${POINTS_AND_WORD}   ${EIGHT_POINTS}	"${WORDS_SPACES}"
	CLI:Enter Path          /system/toolkit
    FOR    ${INVALID_COMMON_NAME}   IN  @{INVALID_COMMON_NAMES}
       CLI:Write    create_csr
       CLI:Set      country_code=US state=CA locality=Fremont organization="ZPE Systems"
    	CLI:Set 	 organization_unit=Engineering email_address=zpe@zpesystems.com
  	   CLI:Test Set Field Invalid Options     common_name    ${INVALID_COMMON_NAME}
       ...   Error: common_name: Invalid Common Name address   save=commit
       CLI:Cancel  Raw
	END

    CLI:Write    create_csr
    CLI:Set      country_code=US state=CA locality=Fremont organization="ZPE Systems"
    CLI:Set 	 organization_unit=Engineering email_address=zpe@zpesystems.com
	CLI:Test Set Field Invalid Options     common_name    ${EMPTY}
      ...   Error: common_name: Field must not be empty.   save=commit
    CLI:Cancel  Raw
	[Teardown]	CLI:Cancel	Raw

Test valid values for field email_address in command=create_csr
	[Tags]	EXCLUDEIN3_2
	CLI:Enter Path          /system/toolkit
    FOR    ${VALID_EMAIL_ADDRESS}   IN  @{VALID_EMAIL_ADDRESSES}
       CLI:Write    create_csr
       CLI:Set      country_code=US state=CA locality=CA organization="ZPE Systems"
    	CLI:Set 	 common_name=www.zpesystems.com organization_unit=Engineering
       CLI:Set      email_address=${VALID_EMAIL_ADDRESS}
       ${OUTPUT}=   CLI:Write    generate_csr
       CLI:Should Contain All    ${OUTPUT}  ${CERTIFICATE_INFOR}
    	CLI:Write  	finish
	END
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field email_address in command=create_csr
	[Tags]	EXCLUDEIN3_2
	${INVALID_EMAIL_ADDRESSES}=   Create List     ${POINTS}   ${ONE_POINTS}   ${TWO_POINTS}
    ...     ${THREE_POINTS}     ${SEVEN_POINTS}   ${WORD_AND_POINTS}    ${NUMBER_AND_POINTS}    ${POINTS_AND_NUMBER}
    ...     ${POINTS_AND_WORD}   ${EIGHT_POINTS}	"${WORDS_SPACES}"
	CLI:Enter Path          /system/toolkit
    FOR    ${INVALID_EMAIL_ADDRESS}   IN  @{INVALID_EMAIL_ADDRESSES}
       CLI:Write    create_csr
       CLI:Set      country_code=US state=CA locality=Fremont organization="ZPE Systems"
    	CLI:Set 	 organization_unit=Engineering common_name=www.zpesystems.com
  	   CLI:Test Set Field Invalid Options     email_address    ${INVALID_EMAIL_ADDRESS}
       ...   Error: email_address: Invalid e-mail address   save=commit
       CLI:Cancel  Raw
	END

    CLI:Write    create_csr
    CLI:Set      country_code=US state=CA locality=Fremont organization="ZPE Systems"
    CLI:Set 	 organization_unit=Engineering common_name=www.zpesystems.com
	CLI:Test Set Field Invalid Options     email_address    ${EMPTY}
      ...   Error: email_address: Field must not be empty.   save=commit
    CLI:Cancel  Raw
	[Teardown]	CLI:Cancel	Raw

Test valid values for field system_profile
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	CLI:Write	factory_settings
	CLI:Test Set Validate Normal Fields	system_profile	out_of_band	gateway
	[Teardown]	CLI:Cancel	Raw

Test invalid values for field system_profile
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2
	CLI:Write	factory_settings
	FOR	${VALUE}	IN	${WORD}	${NUMBER}	${POINTS}
		CLI:Test Set Field Invalid Options	system_profile	${VALUE}	Error: Invalid value: ${VALUE} for parameter: system_profile
	END
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection

SUITE:Set Invalid Values Error Messages
	[Arguments]	@{COMMANDS}
	FOR	${COMMAND}	IN	@{COMMANDS}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Test Set Field Invalid Options	${COMMAND}	a	Error: Invalid value: a for parameter: ${COMMAND}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${COMMAND}	${EMPTY}	Error: Missing value: ${COMMAND}
		Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Test Set Field Invalid Options	${COMMAND}	a	Error: Invalid value: a
	END

SUITE:Invalid URL
	CLI:Set Field	url	${EMPTY}
	Run Keyword If	'${NGVERSION}' >= '4.2'	Run Keywords	Write	commit	AND	read until	:	AND	Write	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	Write	commit
	${OUTPUT}=	Read Until Prompt
	Run Keyword If	'${NGVERSION}' == '3.2' or '${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	Error: url: Incorrect URL information.
	...	ELSE	Should Contain	${OUTPUT}	Error: Incorrect URL information.

	CLI:Set Field	url	invalid_url
	Run Keyword If	'${NGVERSION}' >= '4.2'	Run Keywords	Write	commit	AND	read until	:	AND	Write	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	Write	commit
	${OUTPUT}=	Read Until Prompt
	Run Keyword If	'${NGVERSION}' == '3.2' or '${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	Error: url: Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>
	...	ELSE	Should Contain	${OUTPUT}	Error: Invalid URL format. Format: <PROTOCOL>://<ServerAddress>/<Remote File>

	CLI:Set Field	url	https://invalid/invalid
	Run Keyword If	'${NGVERSION}' >= '4.2'	Run Keywords	Write	commit	AND	read until	:	AND	Write	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	Write	commit
	${OUTPUT}=	Read Until Prompt
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${OUTPUT}	Error: File Transfer Failed

	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	Error: url: Invalid protocol. Supported: ftp, tftp, sftp and scp.

	CLI:Set Field	url	ftp://invalid/invalid
	Run Keyword If	'${NGVERSION}' >= '4.2'	Run Keywords	Write	commit	AND	read until	:	AND	Write	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	Write	commit
	${OUTPUT}=	Read Until Prompt
	Run Keyword If	'${NGVERSION}' == '3.2' or '${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	Error: url: Protocol requires username/password.
	...	ELSE	Should Contain	${OUTPUT}	Error: Protocol requires username/password.

	CLI:Set Field	username	invalid
	Run Keyword If	'${NGVERSION}' >= '4.2'	Run Keywords	Write	commit	AND	read until	:	AND	Write	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	Write	commit
	${OUTPUT}=	Read Until Prompt
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	Error: File Transfer Failed
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${OUTPUT}	Error: url: Protocol requires username/password.

	CLI:Set Field	username	${EMPTY}
	CLI:Set Field	password	invalid
	Run Keyword If	'${NGVERSION}' >= '4.2'	Run Keywords	Write	commit	AND	read until	:	AND	Write	yes
	Run Keyword If	'${NGVERSION}' == '3.2'	Write	commit
	${OUTPUT}=	Read Until Prompt
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	Error: File Transfer Failed
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${OUTPUT}	Error: url: Protocol requires username/password.