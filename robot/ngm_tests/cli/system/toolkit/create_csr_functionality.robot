*** Settings ***
Resource	../../init.robot
Documentation	Test toolkit command create_csr functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	SSH	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
${COUNTRY}		US
${STATE}		California
${CITY}			Fremont
${ZPE}			"ZPE Systems"
${UNIT}			Engineering
${WEBSITE}		www.zpesystems.com
${EMAIL}		zpe@zpesystems.com
${PASSWD}		abcd1234
${CMP_NAME}		ZPE
${PEM_CERT}		nodegrid.pem
${CSR_ROOT}		rootcert.csr
${CRT_ROOT}		rootcert.crt
${SRL_ROOT}		rootcert.srl
${PRIVATE_KEY}	ca_privkey.key
${FTP_IP}		${FTPSERVER2_IP}
${FTP_PORT}		${FTPSERVER2_PORT}
${FTP_URL}		${FTPSERVER2_URL}
${FTP_USER}		${FTPSERVER2_USER}
${FTP_PASSWD}	${FTPSERVER2_PASSWORD}

*** Test Cases ***
Test Create Unsigned CSR
	CLI:Write 				create_csr
	CLI:Set  				country_code=${COUNTRY} state=${STATE} locality=${CITY} organization=${ZPE}
	CLI:Set 				organization_unit=${UNIT} common_name=${WEBSITE} email_address=${EMAIL}
	CLI:Set 				subject_alternative_names=test.zpesystems.com self_sign=no
	${REQUEST}=				CLI:Write  	generate_csr
	Should Contain			${REQUEST}	-----BEGIN CERTIFICATE REQUEST-----
	Should Contain			${REQUEST}	-----END CERTIFICATE REQUEST-----
	CLI:Write 				finish
	CLI:Switch Connection   root_session
	CLI:Enter Path  		/tmp/
	${CSR_PEM}=				CLI:Write  				cat csr.pem
	CLI:Should Be Equal  	${CSR_PEM}	${REQUEST}

Test emulating certification authority signing process
	CLI:Switch Connection 	root_session
	CLI:Enter Path  	  	/tmp/
	CLI:Write   		 	openssl ecparam -out ${PRIVATE_KEY} -name prime256v1 -genkey
	SUITE:Fill Root CSR
	CLI:Write  		 		openssl x509 -req -sha256 -days 365 -in ${CSR_ROOT} -signkey ${PRIVATE_KEY} -out ${CRT_ROOT}
	CLI:Write 				openssl x509 -req -in /tmp/csr.pem -CA ${CRT_ROOT} -CAkey ${PRIVATE_KEY} -CAcreateserial -out ${PEM_CERT} -days 60 -sha256
	CLI:Reconnect As Root
	@{FILES}= 				Create List 	${CSR_ROOT}	${CRT_ROOT}	${PEM_CERT}	${PRIVATE_KEY}
	Run Keyword If	${NGVERSION} < 5.4	Append To List	${FILES}	${SRL_ROOT}	#in newer versions of OpenSSL this file is not generated anymore.
	${ROOT_FILES}			CLI:Ls			/tmp/
	CLI:Should Contain All	${ROOT_FILES}	${FILES}

Test upload system certificate from remote repository
	Skip If		not ${FTPSERVER2_PRESENT}			No FTP server avaliable for this build
	SUITE:Transfer Certificate To Remote Server
	CLI:Switch Connection  	default
	CLI:Write  				system_certificate
	CLI:Set 				from=remote_server url=ftp://${FTP_IP}/files/${PEM_CERT}
	CLI:Set  				username=${FTP_USER} password=${FTP_PASSWD}
	CLI:Set  				the_path_in_url_to_be_used_as_absolute_path_name=yes
	Write					apply
	Read Until				you want to proceed? (yes, no)
	CLI:Write				yes

Test Create Signed CSR
	CLI:Write 				create_csr
	CLI:Set  				country_code=${COUNTRY} state=${STATE} locality=${CITY} organization=${ZPE}
	CLI:Set 				organization_unit=${UNIT} common_name=${WEBSITE} email_address=${EMAIL}
	CLI:Set 				subject_alternative_names=test.zpesystems.com
	CLI:Set 				self_sign=yes validity=730
	CLI:Write  				generate_csr
	CLI:Test Show Command	Certificate applied. This may disconnect your session.
	CLI:Write 				finish

*** Keywords ***
SUITE:Setup
	Skip If		not ${FTPSERVER2_PRESENT}	No FTP server avaliable for this build
	CLI:Open
	CLI:Connect As Root
	CLI:Switch Connection	default

SUITE:Teardown
	SUITE:Remove Files
	SUITE:Remove Certificate From Remote Server
	CLI:Close Connection

SUITE:Fill Root CSR
	CLI:Switch Connection  	root_session
	Write  					openssl req -new -sha256 -key ${PRIVATE_KEY} -out ${CSR_ROOT}
	@{FIELDS}=	Create List	${COUNTRY}	${STATE}	${CITY}	 ${ZPE}	${UNIT}	${WEBSITE}	${EMAIL}	${PASSWD}
	FOR	${FIELD}	IN 	@{FIELDS}
		Write 	${FIELD}
		Read Until Regexp 	(\\]:)
	END
	CLI:Write	${CMP_NAME}

SUITE:Remove Files
	@{FILES}=	Create List	${CSR_ROOT}	${CRT_ROOT}	${PEM_CERT}	${PRIVATE_KEY}
	Run Keyword If	${NGVERSION} < 5.4	Append To List	${FILES}	${SRL_ROOT}
	FOR	${FILE}	IN 	@{FILES}
		CLI:Remove File Remotely	/tmp/${FILE}
	END

SUITE:Transfer Certificate To Remote Server
	[Documentation]  creates a backup of /home/root/.ssh/known_hosts and then removes the original file
				...	so a new fingerprint is always generated and the prompt is predictable. after the file
				...	is transfered, the backup file becomes the standard know_hosts file again.

	CLI:Write				cp /home/root/.ssh/known_hosts /home/root/.ssh/known_hosts.backup
	CLI:Write  				rm /home/root/.ssh/known_hosts
	Write					scp /tmp/${PEM_CERT} ${FTP_USER}@${FTP_IP}:/home/${FTP_USER}/ftp/files
	Read Until 				(yes/no/[fingerprint])?
	Write					yes
	Read Until				password:
	Write					${FTP_PASSWD}
	CLI:Write  				mv /home/root/.ssh/known_hosts.backup /home/root/.ssh/known_hosts

SUITE:Remove Certificate From Remote Server
	CLI:Open  		${FTP_USER}	${FTP_PASSWD}	session alias=ftp	TYPE=shell	HOST_DIFF=${FTP_IP}
	CLI:Enter Path  ftp/files
	${OUT}			CLI:Ls
	${PEM}			Run Keyword And Return Status	Should Contain 	${OUT}	${PEM_CERT}
	Run Keyword If 	'${PEM}' == 'True'	CLI:Write  rm ${PEM_CERT}