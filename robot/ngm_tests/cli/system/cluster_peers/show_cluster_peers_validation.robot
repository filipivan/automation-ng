*** Settings ***
Resource	../../init.robot
Documentation	Test show clould peers 
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	SSH	SHOW	CLOUD_PEERS	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	CLI:Close Connection

*** Test Cases ***
Test Show Command
	CLI:Enter Path	/system/cluster_peers
	${OUTPUT}=	CLI:Show	RAW_Mode=Yes
	Should Contain	${OUTPUT}	notice: Cluster must be enabled. Cluster Peers is only available for Coordinator nodes in star mode.

*** Keywords ***
SUITE:Setup
	CLI:Open
	SUITE:Disable Cluster

SUITE:Disable Cluster
	CLI:Enter Path    /settings/cluster/settings
	CLI:Edit
	CLI:Set	enable_cluster=no enable_peer_management=no enable_license_pool=no
	CLI:Commit
	CLI:Test Show Command	enable_cluster = no	enable_peer_management = no	enable_license_pool = no

