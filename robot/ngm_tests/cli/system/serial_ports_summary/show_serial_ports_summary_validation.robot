*** Settings ***
#Documentation	Suite description
Resource	../../init.robot
Documentation	Tests NG Serial Ports Summary through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW	Serial Ports Summary

Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Variables ***
${ret}=	CLI:Is Serial Console

*** Test Cases ***
Test available commands after send tab-tab
	Run Keyword If	'${ret}' == '1'
	...	Run Keywords
	...	CLI:Enter Path	/system/serial_ports_summary
	...	AND	CLI:Test Available Commands	cd	change_password	commit	event_system_audit	event_system_clear	exit	hostname	ls	pwd	quit	reboot	revert	shell	show	show_settings	shutdown	whoami
	Run Keyword If	'${ret}' == '0'	Log To Console	"Disabled on NodeGrid Manager"

Test ls command
	Run Keyword If	'${ret}' == '1'
	...	Run Keywords
	...	CLI:Enter Path	/system/serial_ports_summary
	...	AND	CLI:Test Ls Command	Try show command instead...
	Run Keyword If	'${ret}' == '0'	Log To Console	"Disabled on NodeGrid Manager"

Show Serial_Ports_Summary
	#${ret}=	CLI:Is Serial Console

	Run Keyword If	'${ret}' == '1'
	...	Run Keywords
	...	CLI:Enter Path	/system/serial_ports_summary
	...	AND	CLI:Test Show Command	enabled_ports:	disabled_ports:	connected_ports:	disconnected_ports:
	Run Keyword If	'${ret}' == '0'	Log To Console	"Disabled on NodeGrid Manager"