*** Settings ***
Resource	../../init.robot
Documentation	Tests NG Device Sessions Table Details display details through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	DEVICE_SESSION	SESSION	TRACKING
Default Tags	CLI	SSH	EVENTS

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}
${USER_REMOTE}	${USERNAME}@${CLIENTIP}

${DEVICE_NAME}	${DUMMY_DEVICE_CONSOLE_NAME}

*** Test Cases ***
Test terminate_device_sessions with invalid target
	CLI:Enter Path	/system/device_sessions/
	${OUTPUT}=	CLI:Write	terminate_device_sessions sometarget	Raw
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	Error: Invalid Target name: sometarget. Please, use tab-tab to obtain available targets.
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${OUTPUT}	Error: Invalid Target: sometarget. Press <tab> for a list of valid targets.

Test terminate_device_sessions with no target and tab tab
	CLI:Enter Path	/system/device_sessions/
	${OUTPUT}=	CLI:Write	terminate_device_sessions \t	Raw
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	Error: No more targets available for command terminate_device_sessions
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${OUTPUT}	Error: Target not available
	${OUTPUT}=	CLI:Write	\n	Raw
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${OUTPUT}	Error: Not enough arguments
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${OUTPUT}	Error: Missing arguments

Show Device Sessions for local connection
	CLI:Open	session_alias=test
	CLI:Connect	${DEVICE_NAME}
	CLI:Write	pwd
	CLI:Switch Connection	default
	Wait Until Keyword Succeeds	5x	25s	SUITE:Validate Created Device Session	${DEVICE_NAME}	${USERNAME}
	[Teardown]	SUITE:Close Device Connect Connection

Show Device Sessions for remote connection
	CLI:Open Device	${DEVICE_NAME}	TYPE=connect_device
	CLI:Switch Connection	default
	Wait Until Keyword Succeeds	5x	25s	SUITE:Validate Created Device Session	${DEVICE_NAME}	${USER_REMOTE}
	[Teardown]	SUITE:Close Device Remote Connection

Test terminate_device_sessions
	CLI:Open Device	${DEVICE_NAME}	TYPE=connect_device
	CLI:Switch Connection	default
	Wait Until Keyword Succeeds	5x	25s	SUITE:Validate Created Device Session	${DEVICE_NAME}	${USER_REMOTE}
	CLI:Write	terminate_device_sessions ${DEVICE_NAME}\n	Raw
	Wait Until Keyword Succeeds	5x	25s	SUITE:Validate Deleted Device Session	${DEVICE_NAME}
	[Teardown]	SUITE:Close Device Remote Connection

*** Keywords ***
SUITE:Setup
	CLI:Open
	Set Client Configuration	timeout=120s
	CLI:Delete Devices	${DEVICE_NAME}
	Wait Until Keyword Succeeds	3x	1m	SUITE:Make Sure Device Was Added
	@{CONNECTIONLIST}=	CLI:Get Active Sessions
	CLI:Terminate Active Sessions	@{CONNECTIONLIST}

SUITE:Teardown
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/devices/
	CLI:Delete Device	${DEVICE_NAME}
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Close Connection

SUITE:Close Device Connect Connection
	[Arguments]	${SESSION_ALIAS}=test
	CLI:Switch Connection	${SESSION_ALIAS}
	CLI:Disconnect Device
	CLI:Close Current Connection	Yes

SUITE:Close Device Remote Connection
	[Arguments]	${SESSION_ALIAS}=device_session
	CLI:Switch Connection	${SESSION_ALIAS}
	CLI:Close Current Connection	Yes

SUITE:Validate Created Device Session
	[Arguments]	${DEVICE}	${USER}=${USERNAME}
	CLI:Enter Path	/system/device_sessions/
	${SESSION_TABLE}=	CLI:Show
	@{Details}=	Split to Lines	${SESSION_TABLE}
	Should Contain	${Details}[0]	device name
	Should Contain	${Details}[0]	number of sessions
	Should Contain	${Details}[0]	users
	Should Contain	${Details}[2]	${DEVICE}
	Should Contain	${Details}[2]	${USER}

SUITE:Validate Deleted Device Session
	[Arguments]	${DEVICE}
	${SESSION_TABLE}=	CLI:Show
	@{Details}=	Split to Lines	${SESSION_TABLE}
	Should Contain	${Details}[0]	device name
	Should Contain	${Details}[0]	number of sessions
	Should Contain	${Details}[0]	users
	Should Not Contain	${Details}[2]	${DEVICE}

SUITE:Make Sure Device Was Added
	CLI:Add Device	${DEVICE_NAME}	device_console	127.0.0.1	${USERNAME}	${PASSWORD}	MODE=on-demand
	[Teardown]	CLI:Cancel	Raw