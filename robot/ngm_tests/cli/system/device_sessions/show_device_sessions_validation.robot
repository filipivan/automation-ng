*** Settings ***
Resource	../../init.robot
Documentation	Tests NG Device Sessions Table Details display details through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	DEVICE_SESSION	SESSION	TRACKING
Default Tags	CLI	SSH	EVENTS

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/system/device_sessions/
	CLI:Test Available Commands	cd	change_password	commit	event_system_audit	event_system_clear	exit	hostname
	...	ls	pwd	quit	reboot	revert	shell	show	show_settings	shutdown	whoami	terminate_device_sessions

Test ls command
	CLI:Enter Path	/system/device_sessions/
	${OUTPUT}=	CLI:Write	ls
	Should Not Contain	${OUTPUT}	try

Test cd command
	CLI:Enter Path	/system/device_sessions/
	CLI:Enter Path	/

Test pwd command
	CLI:Enter Path	/system/device_sessions/
	${OUTPUT}=	CLI:Write	pwd	user=Yes
	Should Be Equal	${OUTPUT}	/system/device_sessions

Test show command
	CLI:Enter Path	/system/device_sessions/
	${OUTPUT}=	CLI:Show
	@{Details}=	Split to Lines	${OUTPUT}
	Should Contain	${Details}[0]	device name	number of sessions	users
	Should Contain	${Details}[1]	===========	==================	=====

Test show_settings command
	CLI:Enter Path	/system/device_sessions/
	${OUTPUT}=	CLI:Show Settings
	Should Not Contain	${OUTPUT}	try
	Should Not Contain	${OUTPUT}	device name	number of sessions	users
	Should Not Contain	${OUTPUT}	===========	==================	=====

*** Keywords ***
SUITE:Setup
	CLI:Open
	Set Client Configuration	timeout=60s

SUITE:Teardown
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Close Connection