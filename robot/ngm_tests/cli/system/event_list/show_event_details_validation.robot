*** Settings ***
Resource	../../init.robot
Documentation	Tests NG Event Details display details through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	EVENTS
Test Template	Event Detail Information

Suite Setup	SUITE:Setup
Suite Teardown	CLI:Close Connection

*** Test Cases ***
EventDetails::100	100	NodeGrid System Rebooting	System Event
EventDetails::101	101	NodeGrid System Started	System Event
EventDetails::102	102	NodeGrid Software Upgrade Started	System Event
EventDetails::103	103	NodeGrid Software Upgrade Completed	System Event
EventDetails::104	104	NodeGrid Configuration Settings Saved to File	System Event
EventDetails::105	105	NodeGrid Configuration Settings Applied	System Event
EventDetails::106	106	NodeGrid ZTP Started	System Event
EventDetails::107	107	NodeGrid ZTP Completed	System Event
EventDetails::108	108	NodeGrid Configuration Changed	System Event
EventDetails::110	110	NodeGrid Local User Added to System Datastore	System Event
EventDetails::111	111	NodeGrid Local User Deleted from System Datastore	System Event
EventDetails::112	112	NodeGrid Local User Modified in System Datastore	System Event
EventDetails::115	115	NodeGrid Session Terminated	System Event
EventDetails::116	116	NodeGrid Session Timed Out	System Event
EventDetails::118	118	NodeGrid Power Supply State Changed	System Event
EventDetails::119	119	NodeGrid Power Supply Sound Alarm Stopped by User	System Event
EventDetails::120	120	NodeGrid Utilization Rate Exceeded	System Event
EventDetails::121	121	NodeGrid Thermal Temperature ThrottleUp	System Event
EventDetails::122	122	NodeGrid Thermal Temperature Dropping	System Event
EventDetails::123	123	NodeGrid Thermal Temperature Warning	System Event
EventDetails::124	124	NodeGrid Thermal Temperature Critical	System Event
EventDetails::126	126	NodeGrid Fan Status Changed	System Event
EventDetails::127	127	NodeGrid Fan Sound Alarm Stopped by User	System Event
EventDetails::130	130	NodeGrid License Added	System Event
EventDetails::131	131	NodeGrid License Removed	System Event
EventDetails::132	132	NodeGrid License Conflict	System Event
EventDetails::133	133	NodeGrid License Scarce	System Event
EventDetails::134	134	NodeGrid License Expiring	System Event
EventDetails::151	151	NodeGrid ${CLOUD/CLUSTER} Peer Offline	System Event
EventDetails::152	152	NodeGrid ${CLOUD/CLUSTER} Peer Signed On	System Event
EventDetails::153	153	NodeGrid ${CLOUD/CLUSTER} Peer Signed Off	System Event
EventDetails::154	154	NodeGrid ${CLOUD/CLUSTER} Peer Removed	System Event
EventDetails::155	155	NodeGrid ${CLOUD/CLUSTER} Peer Became Coordinator	System Event
EventDetails::156	156	NodeGrid ${CLOUD/CLUSTER} Coordinator Became Peer	System Event
EventDetails::157	157	NodeGrid ${CLOUD/CLUSTER} Coordinator Deleted	System Event
EventDetails::158	158	NodeGrid ${CLOUD/CLUSTER} Coordinator Created	System Event
EventDetails::159	159	NodeGrid ${CLOUD/CLUSTER} Peer Configured	System Event
EventDetails::200	200	NodeGrid User Logged In	AAA Event
EventDetails::201	201	NodeGrid User Logged Out	AAA Event
EventDetails::202	202	NodeGrid System Authentication Failure	AAA Event
EventDetails::300	300	NodeGrid Device Session Started	Device Event
EventDetails::301	301	NodeGrid Device Session Stopped	Device Event
EventDetails::302	302	NodeGrid Device Created	Device Event
EventDetails::303	303	NodeGrid Device Deleted	Device Event
EventDetails::304	304	NodeGrid Device Renamed	Device Event
EventDetails::305	305	NodeGrid Device Cloned	Device Event
EventDetails::306	306	NodeGrid Device Up	Device Event
EventDetails::307	307	NodeGrid Device Down	Device Event
EventDetails::308	308	NodeGrid Device Session Terminated	Device Event
EventDetails::310	310	NodeGrid Power On Command Executed on a Device	Device Event
EventDetails::311	311	NodeGrid Power Off Command Executed on a Device	Device Event
EventDetails::312	312	NodeGrid Power Cycle Command Executed on a Device	Device Event
EventDetails::313	313	NodeGrid Suspend Command Executed on a Device	Device Event
EventDetails::314	314	NodeGrid Reset Command Executed on a Device	Device Event
EventDetails::315	315	NodeGrid Shutdown Command Executed on a Device	Device Event
EventDetails::400	400	NodeGrid System Alert Detected	Logging Event
EventDetails::401	401	NodeGrid Alert String Detected on a Device Session	Logging Event
EventDetails::402	402	NodeGrid Event Log String Detected on a Device Event Log	Logging Event
EventDetails::410	410	NodeGrid System NFS Failure	Logging Event
EventDetails::411	411	NodeGrid System NFS Recovered	Logging Event
EventDetails::450	450	NodeGrid Datapoint State High Critical	Logging Event
EventDetails::451	451	NodeGrid Datapoint State High Warning	Logging Event
EventDetails::452	452	NodeGrid Datapoint State Normal	Logging Event
EventDetails::453	453	NodeGrid Datapoint State Low Warning	Logging Event
EventDetails::454	454	NodeGrid Datapoint State Low Critical	Logging Event
EventDetails::460	460	NodeGrid Door Unlocked	Logging Event
EventDetails::461	461	NodeGrid Door Locked	Logging Event
EventDetails::462	462	NodeGrid Door Open	Logging Event
EventDetails::463	463	NodeGrid Door Close	Logging Event
EventDetails::464	464	NodeGrid Door Access Denied	Logging Event
EventDetails::465	465	NodeGrid Door Alarm Active	Logging Event
EventDetails::466	466	NodeGrid Door Alarm Inactive	Logging Event

*** Keywords ***
SUITE:Setup
	CLI:Open
	## 'Cloud' is now 'Cluster' in trunk
	Run Keyword If	'${NGVERSION}' < '4.1'	Set Suite Variable	${CLOUD/CLUSTER}	Cloud
	...	ELSE	Set Suite Variable	${CLOUD/CLUSTER}	Cluster
	Run Keyword If	'${NGVERSION}' < '4.1'	Set Suite Variable	${CLUSTER_FIELD}	enable_clustering
	...	ELSE	Set Suite Variable	${CLUSTER_FIELD}	enable_clustering_access

Event Detail Information
	[Arguments]	${EVENT_NUMBER}	${DESCRIPTION}	${CATEGORY}
	${DESCRIPTION}=	Convert To Lowercase	${DESCRIPTION}
	${CATEGORY}=	Convert To Lowercase	${CATEGORY}
	Write	cd /system/event_list/${EVENT_NUMBER}
	Read Until Prompt
	Write	show
	${EVENT_OUTPUT}=	Read Until Prompt
	Should Contain	${EVENT_OUTPUT}	event number: ${EVENT_NUMBER}
	Should Contain	${EVENT_OUTPUT}	description: ${DESCRIPTION}
	Should Contain	${EVENT_OUTPUT}	occurrences:
	Should Contain	${EVENT_OUTPUT}	category: ${CATEGORY}
	Log to Console	${EVENT_OUTPUT}