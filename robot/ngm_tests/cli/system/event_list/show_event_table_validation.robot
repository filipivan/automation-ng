*** Settings ***
Resource	../../init.robot
Documentation	Tests NG Event Table Details display details through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	EVENTS

Suite Setup	SUITE:Setup
Suite Teardown	CLI:Close Connection

*** Variables ***
${WRONG}	wrong_credential

*** Test Cases ***
Show Event Table
	CLI:Enter Path	/system/event_list/
	${OUTPUT}=	CLI:Show
	Should Match Regexp	${OUTPUT}	event\\snumber\\s+description\\s+occurrences\\s+category\\s+
	Should Match Regexp	${OUTPUT}	\\s+=+\\s+=+\\s+=+\\s+=+\\s+
	@{DETAILS}=	Split to Lines	${OUTPUT}	2	-3
	FOR	${LINE}	IN	@{DETAILS}
		${LINE}=	Strip String	${LINE}
		Should Match Regexp	${LINE}	\\d{3}\\s+\.{57}\\s+\\d{1,11}\\s+\[a-z]{3,7}\\s\[a-z]{5}
	END

Test available commands after send tab-tab
	CLI:Enter Path	/system/event_list/
	CLI:Test Available Commands	cd	change_password	commit	event_system_audit	event_system_clear	exit	hostname
	...	ls	pwd	quit	reboot	revert	shell	show	show_settings	shutdown	whoami	reset_counters

Test ls command
	CLI:Enter Path	/system/event_list/
	CLI:Test Ls Command	100	101	102	103	104	105	106	107	108	110	111	112	115	116	118	119	120	121	122	123	124	126	127	130
	...	131	132	133	134	150	151	152	153	154	155	156	157	158	159	200	201	202	300	301	302	303	304	305	306	307	308	310	311
	...	312	313	314	315	400	401	402	410	411	450	451	452	453	454	460	461	462	463	464	465	466

Show Event Details
	CLI:Enter Path	/system/event_list/
	${SESSIONS}=	CLI:Get Available Paths
	FOR	${SESSION}	IN	@{SESSIONS}
		CLI:Enter Path	${SESSION}
		CLI:Test Show Command	event number:	description:	occurrences:	category:
		CLI:Test Show Command Any	@{EVENTCATEGOREY}
		CLI:Test Show Command Regexp	[0-9]{1,4}
		CLI:Enter Path	..
	END

Test Reset Counters
	CLI:Open	${WRONG}	${WRONG}	session_alias=eventsession	RAW_MODE=Raw
	CLI:Switch Connection	default
	${COUNTER_BEFORE}=	Get Login Counter	202
	Should Not Be Equal	${COUNTER_BEFORE}	0
	CLI:Write	reset_counters 202
	${COUNTER_AFTER}=	Get Login Counter	202
	Should Be Equal	${COUNTER_AFTER}	0

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Enable All Events

SUITE:Teardown
	CLI:Close Connection

Get Login Counter
	[Arguments]	${EVENTID}
	CLI:Enter Path	/system/event_list/
	${EVENT200}=	CLI:Write	show ${EVENTID}
	@{EVENT200}=	Split to Lines	${EVENT200}
	${COUNTERLINE}=	Get From List	${EVENT200}	2
	@{COUNTERLINE}=	Split String	${COUNTERLINE}	:
	${COUNTER}=	Get From List	${COUNTERLINE}	1
	${COUNTER}=	Strip String	${COUNTER}
	[Return]	${COUNTER}