*** Settings ***
Resource	../../init.robot
Documentation	Tests NG Routing Table display details through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW	ROUTING
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEFAULT_DESTINATION}	0.0.0.0/0
${DEFAULT_INTERFACE}	eth0
${DEFAULT_METRIC}	0

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/system/routing_table/
	CLI:Test Available Commands	cd	change_password	commit	event_system_audit	event_system_clear	exit	hostname
	...	ls	pwd	quit	reboot	revert	shell	show	show_settings	shutdown	whoami

Test ls command
	CLI:Enter Path	/system/routing_table/
	CLI:Test Ls Command	Try show command instead...

Show Routing Table Overview
	CLI:Enter Path	/system/routing_table/
	${OUTPUT}	CLI:Test Show Command	destination	gateway	metric	interface	from	table
	${IPv4}=	CLI:Get Interface IP Address	eth0
	Should Contain	${OUTPUT}	${IPv4}

Show Routing Table Details
	CLI:Enter Path	/system/routing_table/
	${OUTPUT}	CLI:Show
	@{ROUTES}	Split To Lines	${OUTPUT}
	${IPV4}	CLI:Get Interface IP Address	eth0
	${IS_NETWORK_6}	Run Keyword And Return Status	Should Contain	${IPV4}	192.168.6.
	${DEFAULT_GATEWAY}	Set Variable If	${IS_NETWORK_6}	192.168.6.1	192.168.2.1
	#workaround because in v3.2 the bridge interface funcitonality creates and extra table which is not deleted
	${TABLE}	Set Variable If	${NGVERSION} == 3.2	br\\d	${DEFAULT_INTERFACE}
	FOR	${ROUTE}	IN	@{ROUTES}
		${DEFAULT_ROUTE}	Run Keyword And Return Status	Should Match Regexp	${ROUTE}
		...	${DEFAULT_DESTINATION}\\s+${DEFAULT_GATEWAY}\\s+${DEFAULT_METRIC}\\s+${DEFAULT_INTERFACE}\\s+${IPV4}\\s+${TABLE}
		Run Keyword If	${DEFAULT_ROUTE}	Pass Execution	Found default route	ELSE	Continue For Loop
	END
	Fail	Did not find default route

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection
