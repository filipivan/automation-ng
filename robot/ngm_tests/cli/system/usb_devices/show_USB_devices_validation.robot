*** Settings ***
Resource	../../init.robot
Documentation	Tests NG USB Devices display details through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW	USB

Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Test Cases ***
Show USB Devices Overview
	Write	cd /system/usb_devices/
	${OUTPUT}=	Read Until Prompt
	Write	show
	${OUTPUT}=	Read Until Prompt

	${RET}=	CLI:Is Serial Console
	Skip If	'${RET}' == '0'	Skip this test on NodeGrid Manager

	${OUTPUT}=	Remove String Using Regexp	${OUTPUT}	['\r']
	Log to Console	\n${OUTPUT}
	@{Details}=	Split to Lines	${OUTPUT}	0	1
	FOR	${LINE}	IN	@{Details}
		Should Contain	${LINE}	usb path	usb id	detected type	kernel device
		Should Contain	${LINE}	description
	END
	@{Details}=	Split to Lines	${OUTPUT}	1	2
	FOR	${LINE}	IN	@{Details}
		Should Contain	${LINE}	========	=========	=============
		Should Contain	${LINE}	=============	===============
	END
	@{Details}=	Split to Lines	${OUTPUT}	2	-3
	FOR	${LINE}	IN	@{Details}
		${LINE}=	Strip String	${LINE}
		Should Match Regexp	${LINE}	\\d\\-\[0-9.]{1,3}\\s+\[a-f0-9]{4}\\:\[a-f0-9]{4}\\s+\[a-zA-Z]{1,20}
	END
#	1-2	0403:6001	serial device	usbS1-2	FT232R USB UART

Show USB Devices Details
	Write	cd /system/usb_devices/
	${OUTPUT}=	Read Until Prompt
	Write	ls
	${OUTPUT}=	Read Until Prompt

	${RET}=	CLI:Is Serial Console
	Skip If	'${RET}' == '0'	Skip this test on NodeGrid Manager

	${OUTPUT}=	Remove String Using Regexp	${OUTPUT}	['\r']
	@{Folders}=	Split to Lines	${OUTPUT}	0	-3
	FOR	${ELEMENT}	IN	@{Folders}
		Write	show ${ELEMENT}
		${Details}=	Read Until Prompt
		Should Contain	${Details}	bus:dev
		${USBDevice}=	Fetch From Left	${ELEMENT}	/
		Should Contain	${Details}	usb path:
		Should Contain	${Details}	vendorid:productid:
		Should Contain	${Details}	detected type:
		Should Contain Any	${Details}	kernel device: sdS	kernel device: usbS	kernel device: cdc-wdm	kernel device: hci0
		${IS_BLUETOOTH_ADAPTER}	Run Keyword And Return Status	Should Contain	${Details}	kernel device: hci0
		${USBDevice}=	Catenate	usbS${USBDevice}
		${USBDevice}=	Fetch From Right	${USBDevice}	:
		${USBDevice}=	Strip String	${USBDevice}
		Should Contain	${Details}	manufacturer:
		Run Keyword If	not ${IS_BLUETOOTH_ADAPTER}	Should Contain	${Details}	description:
		Should Contain	${Details}	number of interfaces:
		${USB_SENSOR}	Run Keyword And Return Status	Should Contain	${Details}	USB Sensor
		Run keyword If	not ${USB_SENSOR}	Should Contain	${Details}	driver(s):
		#Should Contain	${Details}	serial statistics:
		Log to Console	\nDetails for Session ${ELEMENT} are:\n ${Details}
	END