*** Settings ***
Resource	../../../init.robot
Documentation	Tests NG System Usage display details through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	MEMORY	SHOW

Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Test Cases ***
Test available commands after send tab-tab
	CLI:Enter Path	/system/system_usage/
	CLI:Test Available Commands	cd	change_password	commit	event_system_audit	event_system_clear	exit	hostname	ls	pwd	quit	reboot	revert	shell	show	show_settings	shutdown	whoami

Test ls command
	CLI:Enter Path	/system/system_usage/
	CLI:Test Ls Command	memory_usage/	cpu_usage/	disk_usage/

Show Memory Usage
	CLI:Enter Path	/system/system_usage/memory_usage/
	Write	show
	${OUTPUT}=	Read Until Prompt
	${OUTPUT}=	Remove String Using Regexp	${OUTPUT}	['\r']
	Log to Console	\n${OUTPUT}
	@{Details}=	Split to Lines	${OUTPUT}	0	1
	FOR	${LINE}	IN	@{Details}
		Should Contain	${LINE}	memory type	total (kb)	used (kb)	free (kb)
	END
	@{Details}=	Split to Lines	${OUTPUT}	1	2
	FOR	${LINE}	IN	@{Details}
		Should Contain	${LINE}	===========	==========	=========	=========
	END
	@{Details}=	Split to Lines	${OUTPUT}	2	-3
	@{Values}=	Create List	Mem	Swap
	FOR	${LINE}	IN	@{Details}
		Should Contain Any	${LINE}	@{Values}
		${LINE}=	Strip String	${LINE}
		Should Match Regexp	${LINE}	\\w{3}\\s+\\d+\\s+\\d+\\s+\\d+
	END

Show Memory Details
	CLI:Enter Path	/system/system_usage/memory_usage/
	Write	ls
	${OUTPUT}=	Read Until Prompt
	${OUTPUT}=	Remove String Using Regexp	${OUTPUT}	['\r']
	@{Folders}=	Split to Lines	${OUTPUT}	0	-3
	@{Values}=	Create List	Mem	Swap
	FOR	${ELEMENT}	IN	@{Folders}
		${ELEMENT}=	Remove String Using Regexp	${ELEMENT}	['/']
		Convert To Integer	${ELEMENT}
		Write	show ${ELEMENT}
		${Details}=	Read Until Prompt
		${Details}=	Remove String Using Regexp	${Details}	['\r']
		@{LINES}=	Split to Lines	${Details}	0	-1
		${Line1}=	Get From List	${LINES}	0
		Should Match Regexp	${Line1}	\\bmemory\\b\\s\\btype\\b\\:\\s
		Should Contain Any	${Line1}	@{Values}
		${Line2}=	Get From List	${LINES}	1
		Should Match Regexp	${Line2}	\\btotal\\b\\s\\(\\bkb\\b\\)\\:\\s\\d+
		${Line3}=	Get From List	${LINES}	2
		Should Match Regexp	${Line3}	\\bused\\b\\s\\(\\bkb\\b\\)\\:\\s\\d+
		${Line4}=	Get From List	${LINES}	3
		Should Match Regexp	${Line4}	\\bfree\\b\\s\\(\\bkb\\b\\)\\:\\s\\d+
		Log to Console	\nDetails for Memory Details are:\n ${Details}
	END

Show CPU Usage
	CLI:Enter Path	/system/system_usage/cpu_usage/
	Write	show
	${OUTPUT}=	Read Until Prompt
	${OUTPUT}=	Remove String Using Regexp	${OUTPUT}	['\r']
	Log to Console	\n${OUTPUT}
#	Log to Console	Test Header Lines\n
	@{Details}=	Split to Lines	${OUTPUT}	0	1
	FOR	${LINE}	IN	@{Details}
		Should Contain	${LINE}	user %	system %	idle %	waiting i/o %
	END
	@{Details}=	Split to Lines	${OUTPUT}	1	2
	FOR	${LINE}	IN	@{Details}
		Should Contain	${LINE}	======	========	======	=============
	END
#	Log to Console	Test Value Lines\n
	@{Details}=	Split to Lines	${OUTPUT}	2	-3
	FOR	${LINE}	IN	@{Details}
		${LINE}=	Strip String	${LINE}
		Should Match Regexp	${LINE}	\\d+\\s+\\d+\\s+\\d+\\s+\\d+
#		Log to Console	${LINE}
	END

Show CPU Details
	CLI:Enter Path	/system/system_usage/cpu_usage/
	Write	ls
	${OUTPUT}=	Read Until Prompt
	${OUTPUT}=	Remove String Using Regexp	${OUTPUT}	['\r']
	@{Folders}=	Split to Lines	${OUTPUT}	0	-3
	FOR	${ELEMENT}	IN	@{Folders}
		${ELEMENT}=	Remove String Using Regexp	${ELEMENT}	['/']
		Convert To Integer	${ELEMENT}
		Write	show ${ELEMENT}
		${Details}=	Read Until Prompt
		${Details}=	Remove String Using Regexp	${Details}	['\r']
		@{LINES}=	Split to Lines	${Details}	0	-1
		${Line1}=	Get From List	${LINES}	0
		Should Match Regexp	${Line1}	\\buser\\b\\s\\%\\:\\s\\d+
		${Line2}=	Get From List	${LINES}	1
		Should Match Regexp	${Line2}	\\bsystem\\b\\s\\%\\:\\s\\d+
		${Line3}=	Get From List	${LINES}	2
		Should Match Regexp	${Line3}	\\bidle\\b\\s\\%\\:\\s\\d+
		${Line4}=	Get From List	${LINES}	3
		Should Match Regexp	${Line4}	\\bwaiting\\b\\s\i\\/\o\\s\\%\\:\\s\\d+
		Log to Console	\nDetails for CPU Details are:\n ${Details}
	END

Show Disk Usage
	CLI:Enter Path	/system/system_usage/disk_usage/
	Write	show
	${OUTPUT}=	Read Until Prompt
	${OUTPUT}=	Remove String Using Regexp	${OUTPUT}	['\r']
	Log to Console	\n${OUTPUT}
##	Log to Console	Test Header Lines\n
	@{Details}=	Split to Lines	${OUTPUT}	0	1
	FOR	${LINE}	IN	@{Details}
		Should Contain	${LINE}	partition	size (kb)	used (kb)	available (kb)
		Should Contain	${LINE}	use %	description
	END
	@{Details}=	Split to Lines	${OUTPUT}	1	2
	FOR	${LINE}	IN	@{Details}
		Should Contain	${LINE}	=========	=========	=========	==============
		Should Contain	${LINE}	=====	=============
	END
#	Log to Console	Test Value Lines\n
	@{Details}=	Split to Lines	${OUTPUT}	2	-3
	FOR	${LINE}	IN	@{Details}
		${LINE}=	Strip String	${LINE}
		Should Match Regexp	${LINE}	\\w\\s+\\d+\\s+\\d+\\s+\\d+\\s+\\w
#		Log to Console	${LINE}
	END

Show Disk Details
	CLI:Enter Path	/system/system_usage/disk_usage/
	Write	ls
	${OUTPUT}=	Read Until Prompt
	${OUTPUT}=	Remove String Using Regexp	${OUTPUT}	['\r']
	@{Folders}=	Split to Lines	${OUTPUT}	0	-3
	FOR	${ELEMENT}	IN	@{Folders}
		${ELEMENT}=	Remove String Using Regexp	${ELEMENT}	['/']
		Convert To Integer	${ELEMENT}
		Write	show ${ELEMENT}
		${Details}=	Read Until Prompt
		${Details}=	Remove String Using Regexp	${Details}	['\r']
		@{LINES}=	Split to Lines	${Details}	0	-1
		${Line1}=	Get From List	${LINES}	0
		Should Match Regexp	${Line1}	\\bpartition\\b\\:\\s+\[\-devsabc0-20\/]
		${Line2}=	Get From List	${LINES}	1
		Should Match Regexp	${Line2}	\\bsize\\b\\s\[\(\)kb\:]{5}\\s+\\d+
		${Line3}=	Get From List	${LINES}	2
		Should Match Regexp	${Line3}	\\bused\\b\\s\[\(\)kb\:]{5}\\s+\\d+
		${Line4}=	Get From List	${LINES}	3
		Should Match Regexp	${Line4}	\\bavailable\\b\\s\[\(\)kb\:]{5}\\s+\\d+
		${Line5}=	Get From List	${LINES}	4
		Should Match Regexp	${Line5}	\\buse\\b\\s\[\:\%]{2}\\s+\\d+
		${Line6}=	Get From List	${LINES}	5
		Should Match Regexp	${Line6}	\\bdescription\\b\\:\\s+\\w
		Log to Console	\nDetails for ${Line1} are:\n ${Details}
	END