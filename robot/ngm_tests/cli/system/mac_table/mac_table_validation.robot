*** Settings ***
Resource	../../init.robot
Documentation	Validation test cases to Mac Table through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	NSR	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_1
Default Tags	CLI	SSH	NSR

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test Available Commands After Sending Tab-Tab
	Skip If	not ${HOST_IS_NSR}	MAC Table only in NSR
	CLI:Enter Path	/system/mac_table/
	CLI:Test Available Commands	acknowledge_alarm_state	event_system_audit	ls	shell
	...	apply_settings	event_system_clear	page	show
	...	cd	exec	pwd	show_settings
	...	change_password	exit	quit	shutdown
	...	cloud_enrollment	export_settings	reboot	software_upgrade
	...	commit	factory_settings	refresh	system_certificate
	...	create_csr	hostname	revert	system_config_check
	...	diagnostic_data	import_settings	save_settings	whoami

Test Visible Title Colums For Show Command
	Skip If	not ${HOST_IS_NSR}	MAC Table only in NSR
	CLI:Enter Path	/system/mac_table/
	CLI:Test Show Command Regexp	(interface\\s+vlan\\s+mac address)|(entry\\s+interface\\s+mac address\\s+vlan)

*** Keywords ***
SUITE:Setup
	CLI:Open
	${HOST_IS_NSR}=	CLI:Is Net SR
	Set Suite Variable	${HOST_IS_NSR}
	Skip If	not ${HOST_IS_NSR}	MAC Table only in NSR

SUITE:Teardown
	CLI:Close Connection