*** Settings ***
Resource	../../init.robot
Documentation	Functionality test cases to Mac Table through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	NON-CRITICAL	NSR	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_1
Default Tags	CLI	SSH	NSR

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${TEST_VLAN_ID}	80
${TEST_VLAN_IP_HOST}	10.0.0.80
${TEST_VLAN_IP_PEER}	10.0.0.82
${TEST_NAME_SFP}	test_vlan_sfp_connection
${TEST_NAME_NETS}	test_vlan_nets_connection
${INTERFACE}	BACKPLANE0

*** Test Cases ***
Test Show MAC related to PEER Interface With SFP Port
	[Setup]	SUITE:VLAN Setup	${SFP_HOST_PORT}	${SFP_PEER_PORT}
	Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	MAC Table only in NSR
	Skip If	not ${PEER_IS_NSR}	MAC Table only in NSR
	CLI:Switch Connection	PEER
	${STATUS}=	Run Keyword And Return Status	CLI:Test Ping	IP_ADDRESS=${TEST_VLAN_IP_HOST}	NUM_PACKETS=1	TIMEOUT=15
	Run Keyword If	'${STATUS}' == '${FALSE}'	Log To Console	First packet used to establish connection
	CLI:Test Ping	IP_ADDRESS=${TEST_VLAN_IP_HOST}	NUM_PACKETS=5	TIMEOUT=30
	CLI:Switch Connection	HOST
	CLI:Enter Path	/system/mac_table/
	CLI:Write	refresh
	CLI:Write	page 0
	CLI:Test Show Command	${EMPTY}${TEST_VLAN_ID}	${MAC}	${SFP_HOST_PORT}
	[Teardown]	SUITE:VLAN Teardown

Test Show MAC related to PEER Interface With NetS Port
	[Setup]	SUITE:VLAN Setup	${NETS_HOST_PORT}	${NETS_PEER_PORT}
	Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	MAC Table only in NSR
	Skip If	not ${PEER_IS_NSR}	MAC Table only in NSR
	CLI:Switch Connection	PEER
	${STATUS}=	Run Keyword And Return Status	CLI:Test Ping	IP_ADDRESS=${TEST_VLAN_IP_HOST}	NUM_PACKETS=1	TIMEOUT=15
	Run Keyword If	'${STATUS}' == '${FALSE}'	Log To Console	First packet used to establish connection
	CLI:Test Ping	IP_ADDRESS=${TEST_VLAN_IP_HOST}	NUM_PACKETS=5	TIMEOUT=30
	CLI:Switch Connection	HOST
	CLI:Enter Path	/system/mac_table/
	CLI:Write	refresh
	CLI:Write	page 0
	CLI:Test Show Command	${EMPTY}${TEST_VLAN_ID}	${MAC}	${NETS_HOST_PORT}
	[Teardown]	SUITE:VLAN Teardown

*** Keywords ***
SUITE:Setup
	Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	Set Suite Variable	${SFP_HOST_PORT}	${SWITCH_SFP_HOST_INTERFACE1}
	Set Suite Variable	${SFP_PEER_PORT}	${SWITCH_SFP_SHARED_INTERFACE1}
	Set Suite Variable	${NETS_HOST_PORT}	${SWITCH_NETPORT_HOST_INTERFACE1}
	Set Suite Variable	${NETS_PEER_PORT}	${SWITCH_NETPORT_SHARED_INTERFACE1}

	CLI:Open	session_alias=HOST
	${HOST_IS_NSR}=	CLI:Is Net SR
	Set Suite Variable	${HOST_IS_NSR}
	CLI:Open	session_alias=PEER	HOST_DIFF=${HOSTSHARED}
	${PEER_IS_NSR}=	CLI:Is Net SR
	Set Suite Variable	${PEER_IS_NSR}
	Skip If	not ${HOST_IS_NSR}	MAC Table only in NSR
	Skip If	not ${PEER_IS_NSR}	MAC Table only in NSR
	CLI:Switch Connection	HOST
	SUITE:Disable All Switch Interfaces
	CLI:Switch Connection	PEER
	${MAC}=	CLI:Get Interface MAC Address	${INTERFACE}
	Set Suite Variable	${MAC}
	SUITE:Disable All Switch Interfaces
	SUITE:VLAN Teardown

SUITE:Teardown
	Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	SUITE:Close connection If Host Or Peer Is Not NSR
	Skip If	not ${HOST_IS_NSR}	MAC Table only in NSR
	Skip If	not ${PEER_IS_NSR}	MAC Table only in NSR
	SUITE:VLAN Teardown
	CLI:Switch Connection	HOST
	SUITE:Disable All Switch Interfaces
	CLI:Enable Switch Interface	sfp0	SPEED=auto	AUTO_NEGOTIATION=disabled
	CLI:Enable Switch Interface	sfp1	SPEED=auto	AUTO_NEGOTIATION=disabled
	CLI:Switch Connection	PEER
	SUITE:Disable All Switch Interfaces
	CLI:Enable Switch Interface	sfp0	SPEED=auto	AUTO_NEGOTIATION=disabled
	CLI:Enable Switch Interface	sfp1	SPEED=auto	AUTO_NEGOTIATION=disabled
	CLI:Close Connection

SUITE:Disable All Switch Interfaces
	CLI:Enter Path	/settings/switch_interfaces/
	@{INTERFACES}=	SUITE:Get Switch Interfaces
	${LENGTH}=	Get Length	${INTERFACES}
	${END}=	Set Variable	${LENGTH - 2}
	FOR	${INDEX}	${INTERFACE}	IN ENUMERATE	@{INTERFACES}
		Wait Until Keyword Succeeds	3x	500ms	CLI:Disable Switch Interface	${INTERFACE}
		Run Keyword If	'${INDEX}' == '${END}'	Exit For Loop
	END
SUITE:Get Switch Interfaces
	${LS}=	CLI:Write	ls	user=Yes
	${LS}=	CLI:Ls
	${LS}=	Remove String	${LS}	${SPACE}	\t	\r	\n
	${INTERFACES}=	Split String	${LS}	/
	[Return]	${INTERFACES}

SUITE:VLAN Setup
	[Arguments]	${HOST_PORT}	${PEER_PORT}
	Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	MAC Table only in NSR
	Skip If	not ${PEER_IS_NSR}	MAC Table only in NSR
	CLI:Switch Connection	HOST
	CLI:Add VLAN	${TEST_VLAN_ID}	TAGGED_PORTS=backplane0,${HOST_PORT}
	CLI:Enable Switch Interface	${HOST_PORT}	VLAN_ID=${TEST_VLAN_ID}	SPEED=10g	AUTO_NEGOTIATION=enabled
	CLI:Add Static IPv4 Network Connection	${TEST_NAME_SFP}	${TEST_VLAN_IP_HOST}	TYPE=vlan	VLAN_ID=${TEST_VLAN_ID}
	CLI:Switch Connection	PEER
	CLI:Add VLAN	${TEST_VLAN_ID}	TAGGED_PORTS=backplane0,${PEER_PORT}
	CLI:Enable Switch Interface	${PEER_PORT}	VLAN_ID=${TEST_VLAN_ID}	SPEED=10g	AUTO_NEGOTIATION=enabled
	CLI:Add Static IPv4 Network Connection	${TEST_NAME_SFP}	${TEST_VLAN_IP_PEER}	TYPE=vlan	VLAN_ID=${TEST_VLAN_ID}

SUITE:VLAN Teardown
	Skip If	not ${VLAN_AVAILABLE}	VLAN tests not configured/available for this testing build
	Skip If	not ${HOST_IS_NSR}	MAC Table only in NSR
	Skip If	not ${PEER_IS_NSR}	MAC Table only in NSR
	CLI:Switch Connection	HOST
	CLI:Delete VLAN	${TEST_VLAN_ID}
	CLI:Delete Network Connections	${TEST_NAME_SFP}
	CLI:Disable Switch Interface	${SFP_HOST_PORT}
	CLI:Disable Switch Interface	${NETS_HOST_PORT}
	CLI:Switch Connection	PEER
	CLI:Delete VLAN	${TEST_VLAN_ID}
	CLI:Delete Network Connections	${TEST_NAME_SFP}
	CLI:Disable Switch Interface	${SFP_PEER_PORT}
	CLI:Disable Switch Interface	${NETS_PEER_PORT}

SUITE:Close connection If Host Or Peer Is Not NSR
	Run Keyword If	'${HOST_IS_NSR}' == 'False' or '${PEER_IS_NSR}' == 'False'	CLI:Close Connection