*** Settings ***
Resource        ../../init.robot
Documentation   Tests System > Tests of validation on switch statistics through the CLI
Metadata        Version	1.0
Metadata        Executed At	${HOST}
Force Tags      CLI  EXCLUDEIN3_2  EXCLUDEIN4_2    DEPENDENCE_SWITCH
Default Tags    CLI	SSH	SHOW

Suite Setup     SUITE:Setup
Suite Teardown  SUITE:Teardown

*** Variables ***

*** Test Cases ***
Test available commands after send tab-tab
    Skip If       ${IS_NSR} == ${FALSE}    System is not NSR
    CLI:Enter Path      /system/switch_statistics/
    CLI:Test Available Commands  acknowledge_alarm_state  exit  shell  apply_settings  export_settings  show
    ...  cd  factory_settings  show_settings  change_password  hostname  shutdown  cloud_enrollment  import_settings
    ...  software_upgrade  commit  ls  system_certificate  create_csr  pwd  system_config_check  diagnostic_data
    ...  quit  unauthorize_802.1x_session  event_system_audit  reboot  whoami  event_system_clear  revert  exec
    ...  save_settings

Test Show Column "802.1x state" On Switch Statistics Table
    Skip If       ${IS_NSR} == ${FALSE}    System is not NSR
    CLI:Enter Path          /system/switch_statistics/
    ${OUTPUT}=              CLI:Show
    Should Contain          ${OUTPUT}   802.1x state

*** Keywords ***
SUITE:Setup
    CLI:Open
    SUITE:Check If Is NSR
    Skip If    ${IS_NSR} == ${FALSE}      System is not NSR
    SUITE:Check If System Has Switch And Interfaces
    SUITE:Get Interface Net's Name

SUITE:Teardown
    Run Keyword If      ${IS_NSR} == ${FALSE}  CLI:Close Connection
    Skip If   ${IS_NSR} == ${FALSE}     System is not NSR
    CLI:Close Connection

SUITE:Check If System Has Switch And Interfaces
    ${OUTPUT}=              CLI:Enter Path  /system/switch_statistics/    RAW_Mode=Yes
    ${HAS_SWITCH}=          Run Keyword And Return Status   Should Not Contain  ${OUTPUT}
    ...     Error: Invalid path: switch_statistics
    Set Suite Variable      ${HAS_SWITCH}
    ${OUTPUT}=              CLI:Ls
    ${INTERFACES}=          Remove String Using Regexp       ${OUTPUT}    ['/']
    @{GET_LINES}=           Split To Lines   ${INTERFACES}
    ${INT_NAME}=            Get From List    ${GET_LINES}   2
    ${HAS_SFP}=             Run Keyword And Return Status   Should Contain  ${INT_NAME}   sfp
    Set Suite Variable      ${HAS_SFP}
    ${NETS_NAME}=           Get From List    ${GET_LINES}   4
    ${HAS_NETS}=            Run Keyword And Return Status   Should Contain  ${NETS_NAME}   netS
    Set Suite Variable      ${HAS_NETS}
    Set Suite Variable      ${NETS_NAME}

SUITE:Check If Is NSR
    ${IS_NSR}=      CLI:Is Net SR
    Set Suite Variable   ${IS_NSR}
    [Teardown]   Skip If    ${IS_NSR} == ${FALSE}      System is not NSR

SUITE:Get Interface Net's Name
    CLI:Enter Path          /system/switch_statistics/
    ${OUTPUT}=              CLI:Ls
    ${INTERFACES}=          Remove String Using Regexp    ${OUTPUT}    ['/']
    @{GET_LINES}=           Split To Lines   ${INTERFACES}
    ${NETS_NAME}=           Get From List    ${GET_LINES}    4
    Set Suite Variable      ${NETS_NAME}