*** Settings ***
Resource	    ../init.robot
Documentation	This is the functionality tests for add by CLI Console-Like Command SSH/Telnet
Metadata	    Version	1.0
Metadata	    Executed At	${HOST}
Force Tags	    CLI  EXCLUDEIN3_2  EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE_NAME}      test-device
${DEVICE_TYPE}      device_console
@{DEVICE}           ${DEVICE_NAME}

*** Test Cases ***
Test SSH Funtionality
    SUITE:Connect And Using SSH Funtionality
    SUITE:Run Some Commands On Terminal

Test Telnet Funtionality
    SUITE:Connect And Using Telnet Funtionality
    SUITE:Run Some Commands On Terminal

*** Keywords ***
SUITE:Setup
    CLI:Open    session_alias=admin_left    HOST_DIFF=${HOST}
    SUITE:Enable Or Disable Telnet Service To Managed Devices And To Nodegrid   yes
    CLI:Delete Devices  @{DEVICE}
    CLI:Add Device  ${DEVICE_NAME}  ${DEVICE_TYPE}  ${HOSTPEER}
    ...   ${DEFAULT_USERNAME}  ${DEFAULT_PASSWORD}
    SUITE:Add SSH Command
    SUITE:Add Telnet Command
    CLI:Open    session_alias=admin_right   HOST_DIFF=${HOSTPEER}
    SUITE:Enable Or Disable Telnet Service To Managed Devices And To Nodegrid   yes
    CLI:Open    session_alias=admin_left    HOST_DIFF=${HOST}

SUITE:Teardown
    CLI:Open    session_alias=admin_left    HOST_DIFF=${HOST}
    SUITE:Enable Or Disable Telnet Service To Managed Devices And To Nodegrid   no
    CLI:Delete Devices      @{DEVICE}
    CLI:Open    session_alias=admin_right   HOST_DIFF=${HOSTPEER}
    SUITE:Enable Or Disable Telnet Service To Managed Devices And To Nodegrid   no
    CLI:Close Connection

SUITE:Add SSH Command
    CLI:Enter Path  /settings/devices/${DEVICE_NAME}/commands/
    CLI:Add
    CLI:Set         command=ssh
    CLI:Set         port_number=22
    CLI:Commit

SUITE:Add Telnet Command
    CLI:Enter Path  /settings/devices/${DEVICE_NAME}/commands/
    CLI:Add
    CLI:Set         command=telnet
    CLI:Set         port_number=23
    CLI:Commit

SUITE:Connect And Using SSH Funtionality
    CLI:Enter Path              /access/${DEVICE_NAME}/
    Write	                    ssh
	${OUTPUT}=	Read Until	    Password:
	Write                   ${DEFAULT_PASSWORD}
	Read Until Regexp	(WARNING:)|(]#)
	CLI:Write Bare	\n

SUITE:Connect And Using Telnet Funtionality
    CLI:Enter Path              /access/${DEVICE_NAME}/
    Write	                    telnet
	${OUTPUT}=	Read Until	    Password:
	CLI:Write                   ${DEFAULT_PASSWORD}

SUITE:Run Some Commands On Terminal
    ${OUTPUT}=  CLI:Write       whoami
    Should Contain              ${OUTPUT}   ${DEFAULT_USERNAME}
    ${OUTPUT}=  CLI:Write       hostname
    Should Contain              ${OUTPUT}   ${HOSTNAME_NODEGRID}
    CLI:Write                   exit

SUITE:Enable Or Disable Telnet Service To Managed Devices And To Nodegrid
    [Arguments]                 ${ENABLED}
    CLI:Enter Path              /settings/services/
    CLI:Test Show Command       enable_telnet_service_to_nodegrid   enable_telnet_service_to_managed_devices
    CLI:Set                     enable_telnet_service_to_nodegrid=${ENABLED}
    CLI:Set                     enable_telnet_service_to_managed_devices=${ENABLED}
    CLI:Commit