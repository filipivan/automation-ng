*** Settings ***
Resource	../../init.robot
Documentation	validate power control menu
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI		DEPENDENCE_PDU	NON-CRITICAL
Default Tags	ACCESS	PDU

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE_MERGED}	device_merged
${DEVICE_TYPE}    pdu_servertech
${MERGED_DEVICE_TYPE}    device_console
${DEVICE_IP}     192.168.7.233
${DEVICE_NAME}    Servertech_pdu_new
${MODE}    on-demand
${USR}     admn
${PASSWD}   admn
${FULLNAME_OUTLET}   outlet=Servertech_pdu_new:A:AA10-Master_Outlet_10

*** Test Cases ***
Test case to add managed device and outlet command
    CLI:Enter Path	/settings/devices
    CLI:Add Device	${DEVICE_NAME}	${DEVICE_TYPE}	${DEVICE_IP}    ${USR}  ${PASSWD}   ${MODE}
    CLI:Enter Path  /settings/devices/${DEVICE_NAME}/commands/outlet/
    CLI:Set  protocol=snmp
    CLI:Commit
    CLI:Enter Path  /settings/devices/${DEVICE_NAME}/management/
    CLI:Set  snmp_version=v2
    CLI:Set    snmp_commmunity=private
    CLI:Commit
	[Teardown]	CLI:Cancel	Raw

Test case to check outlets discovered
    CLI:Enter Path	/settings/auto_discovery/discover_now/
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Write	discover_now pdu|${DEVICE_NAME}
	Run Keyword If	'${NGVERSION}' >='4.2'	CLI:Write	discover_now ${DEVICE_NAME}
    CLI:Enter Path  /settings/devices/${DEVICE_NAME}/outlets/
    ${OUTPUT}=	CLI:Show
    Should Contain	${OUTPUT}    AA10
    Should Contain	${OUTPUT}    AA2
	[Teardown]	CLI:Cancel	Raw

Test case to merge one outlet and validate power control menu
    CLI:Add Device	${DEVICE_MERGED}	device_console	127.0.0.1	root	${ROOT_PASSWORD}	enabled
    CLI:Enter Path  /settings/devices/${DEVICE_MERGED}/commands
    CLI:Add
    CLI:Set	command=outlet
    Run Keyword If	'${NGVERSION}' == '3.2'  CLI:Set  outlet=Servertech_pdu_new:A:AA1
    Run Keyword If	'${NGVERSION}' >='4.2'  CLI:Set  outlet=Servertech_pdu_new:A:AA10-Master_Outlet_10
    CLI:Commit
    CLI:Enter Path   /access/${DEVICE_MERGED}
    CLI:Write  power_status
    Wait Until Keyword Succeeds	10x  3s	SUITE:Power status on
    CLI:Write  power_off
    Wait Until Keyword Succeeds	10x  3s	SUITE:Power status off
    CLI:Write  power_cycle
    CLI:Commit
    Wait Until Keyword Succeeds	10x  3s	SUITE:Power status on
	[Teardown]	CLI:Cancel	Raw

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Enter Path	/settings/devices/
	CLI:Delete If Exists Confirm	${DEVICE_NAME}
	CLI:Delete If Exists Confirm	${DEVICE_MERGED}
	CLI:Close Connection

SUITE:Power status on
    CLI:Enter Path   /access/${DEVICE_MERGED}
    ${OUTPUT}=  CLI:Write  power_status
    Should Contain	${OUTPUT}   Powerstatus: on

SUITE:Power status off
    CLI:Enter Path   /access/${DEVICE_MERGED}
    ${OUTPUT}=  CLI:Write  power_status
    Should Contain	${OUTPUT}   Powerstatus: off