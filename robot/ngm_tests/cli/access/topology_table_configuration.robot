*** Settings ***
Resource	../init.robot
Documentation	Tests Access through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	Go To Access
Suite Teardown	Delete Device And Close Connection

*** Test Cases ***
Check topology table headers names
	CLI:Table Should Have Headers	name	status

Test local devices in topology table
	@{DEVICES_MANAGED_DEVICES}=	CLI:Get Local Devices Identifiers
	@{TOPOLOGY_DEVICES}=	CLI:Get Topology Table Identifiers
	${HOSTNAME}=	CLI:Get Hostname
	Remove Values From List	${TOPOLOGY_DEVICES}	${HOSTNAME}	#This keyword directly removes the hostname from the TOPOLOGY_DEVICES List
	Log List	${TOPOLOGY_DEVICES}	INFO
	List Should Not Contain Duplicates	${TOPOLOGY_DEVICES}
	List Should Contain Sub List	${DEVICES_MANAGED_DEVICES}	${TOPOLOGY_DEVICES}

Check right device name in /access menu
	CLI:Delete Devices	${DUMMY_DEVICE_CONSOLE_NAME_ACCESS}
	CLI:Add Device	${DUMMY_DEVICE_CONSOLE_NAME_ACCESS}	device_console

	CLI:Enter Path	/access
	${ret}=	CLI:Ls
	Should Contain	${ret}	${DUMMY_DEVICE_CONSOLE_NAME_ACCESS}

	CLI:Delete Device	${DUMMY_DEVICE_CONSOLE_NAME_ACCESS}

*** Keywords ***
Delete Device And Close Connection
	CLI:Delete Devices	${DUMMY_DEVICE_CONSOLE_NAME_ACCESS}
	CLI:Close Connection

Go To Access
	CLI:Open
	CLI:Enter Path	/access