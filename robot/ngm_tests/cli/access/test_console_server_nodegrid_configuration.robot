*** Settings ***
Resource	../init.robot
Documentation	Tests console_server_nodegrid device type: add console_server_nodegrid, add discovery rule, discover console server ports
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	NON-CRITICAL	NEED-REVIEW	DEPENDENCE_DEVICE	DEPENDENCE_NSC

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CASNG_NAME}	${CONSOLE_SERVER_NG_NAME}
${CASNG_IP}	${CONSOLE_SERVER_NG_IP}
${CASNG_USERNAME}	${CONSOLE_SERVER_NG_USERNAME}
${CASNG_PASSWORD}	${CONSOLE_SERVER_NG_PASSWORD}
${DISCOVERY_RULE}	NSC

*** Test Cases ***
Test adding console_server_nodegrid device
	Skip If	'${HOST}' == '${CASNG_IP}'	${CASNG_IP} is the console server
	CLI:Add Device	${CASNG_NAME}	console_server_nodegrid	${CASNG_IP}	${CASNG_USERNAME}	${CASNG_PASSWORD}	enabled
	CLI:Test Show Command	${CASNG_NAME}
	[Teardown]	CLI:Cancel	Raw

Test adding discovery rule
	Skip If	'${HOST}' == '${CASNG_IP}'	${CASNG_IP} is the console server
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/
	CLI:Delete If Exists Confirm	${DISCOVERY_RULE}
	CLI:Add
	CLI:Set	rule_name=${DISCOVERY_RULE} method=console_server_ports clone_from=${CASNG_NAME}
	CLI:Commit
	CLI:Test Show Command	${DISCOVERY_RULE}
	[Teardown]	CLI:Cancel	Raw

Test discovering console_server_nodegrid
	Skip If	'${HOST}' == '${CASNG_IP}'	${CASNG_IP} is the console server
	CLI:Open	session_alias=console_server	HOST_DIFF=${CASNG_IP}
	SUITE:Enable console server ports	enabled
	CLI:Switch Connection	default
	Wait Until Keyword Succeeds	1m	1s	SUITE:Discover console server ports
	[Teardown]	SUITE:Enable console server ports	disabled

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Delete All Devices
	CLI:Delete All License Keys Installed

SUITE:Teardown
	Skip If	'${HOST}' == '${CASNG_IP}'	${CASNG_IP} is the console server
	CLI:Switch Connection	default
	CLI:Delete All Devices
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/
	CLI:Delete Confirm	NSC
	CLI:Delete Devices	${CASNG_NAME}
	CLI:Enter Path	/settings/auto_discovery/discovery_logs/
	CLI:Write	reset_logs
	CLI:Test Not Show Command	${CASNG_IP}
	Run Keyword If Any Tests Failed	SUITE:Enable console server ports	disabled
	CLI:Close Connection

SUITE:Enable console server ports
	[Arguments]	${MODE}
	CLI:Switch Connection	console_server
	CLI:Enter Path	/settings/devices/
	CLI:Edit	ttyS4,ttyS5,ttyS6,ttyS7,ttyS8
	CLI:Set Field	mode	${MODE}
	CLI:Commit
	CLI:Test Show Command Regexp	ttyS4\\s+ttyS4\\s+local_serial\\s+${MODE}\\s+not supported

SUITE:Discover console server ports
	${IS_VM}	CLI:Is VM System
	CLI:Enter Path	/settings/auto_discovery/discover_now/
	CLI:Write	discover_now ${CASNG_NAME}
	CLI:Enter Path	/settings/auto_discovery/discovery_logs/
	CLI:Test Show Command Regexp	ttyS4\\s+Console Server Ports\\s+Device Cloned
	CLI:Test Show Command Regexp	ttyS5\\s+Console Server Ports\\s+Device Cloned
	CLI:Test Show Command Regexp	ttyS6\\s+Console Server Ports\\s+Device Cloned
	CLI:Test Show Command Regexp	ttyS7\\s+Console Server Ports\\s+Device Cloned
	IF	${IS_VM}
		CLI:Test Show Command Regexp	ttyS8\\s+Console Server Ports\\s+No License Available
	ELSE
		CLI:Test Show Command Regexp	ttyS8\\s+Console Server Ports\\s+Device Cloned
		CLI:Enter Path	/access/
		Wait Until Keyword Succeeds	1m	5s	CLI:Test Show Command	${CASNG_NAME}	ttyS4	ttyS5	ttyS6	ttyS7
	END