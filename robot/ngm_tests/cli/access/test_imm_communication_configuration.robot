*** Settings ***
Resource	../init.robot
Documentation	Tests Nodegrid communication with IMM service processor, like power commands and connect
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	DEPENDENCE_DEVICE

Suite Setup	Delete And Create Imm Device
Suite Teardown	Delete Device And Close Connection

*** Test Cases ***
Test Commands With SSH
	Set Tags	SSH
	Run Keyword Unless	${EXECUTE_IMM_TESTS}	Skip	IMM tests are disabled
	CLI:Enter Path	/settings/devices/${IMM_NAME}/access
	CLI:Write	set type=imm
	CLI:Commit
	CLI:Test power_status command	${IMM_NAME}	"No"
	CLI:Test connect command	${IMM_NAME}	${IMM_AWAITED_STRING_ON_CONNECT_CMD}

Test Commands With IPMI_2.0
	Set Tags	IPMI_2.0
	Run Keyword Unless	${EXECUTE_IMM_TESTS}	Skip	IMM tests are disabled
	CLI:Enter Path	/settings/devices/${IMM_NAME}/access
	CLI:Write	set type=ipmi_2.0
	CLI:Commit
	CLI:Test power_status command	${IMM_NAME}	"No"
	CLI:Test connect command	${IMM_NAME}	${IMM_AWAITED_STRING_ON_CONNECT_CMD}

*** Keywords ***
Delete And Create Imm Device
	Run Keyword Unless	${EXECUTE_IMM_TESTS}	Skip	IMM tests are disabled
	CLI:Open	timeout=240s
	CLI:Delete Devices	${IMM_NAME}
	CLI:Add Device	${IMM_NAME}	imm	${IMM_IP}	${IMM_USERNAME}	${IMM_PASSWORD}	on-demand
	Sleep	2s

Delete Device And Close Connection
	Run Keyword Unless	${EXECUTE_IMM_TESTS}	Skip	IMM tests are disabled
	CLI:Delete Devices	${IMM_NAME}
	CLI:Close Connection
	#To guarantees that the IMM device deletion works in case of the disconnect
	#from IMM fails we disconnect the ssh session to nodegrid and connect again
	#this way the device will certainly be deleted
	CLI:Open
	CLI:Delete Devices	${IMM_NAME}
	CLI:Close Connection
