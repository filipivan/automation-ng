*** Settings ***
Resource	../init.robot
Documentation	This is the validation tests for add by CLI Console-Like Command SSH/Telnet
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	 CLI  EXCLUDEIN3_2  EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE_NAME}      test-device
${DEVICE_TYPE}      device_console

*** Test Cases ***
Test Command Field Should Contain SSH and Telnet Options
    CLI:Enter Path  /settings/devices/${DEVICE_NAME}/commands/
    CLI:Add
    CLI:Test Field Options  command    ssh   telnet
    CLI:Cancel

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Delete Devices  ${DEVICE_NAME}
    CLI:Add Device  ${DEVICE_NAME}  ${DEVICE_TYPE}  ${HOSTPEER}
    ...   ${DEFAULT_USERNAME}  ${DEFAULT_PASSWORD}
SUITE:Teardown
	CLI:Delete Devices  ${DEVICE_NAME}
	CLI:Close Connection
