*** Settings ***
Resource	../init.robot
Documentation	IDRAC9 configuration test
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	NON-CRITICAL	EXCLUDEIN3_2
Default Tags	SSH
Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${IDRAC9_IP}	${IDRAC9_IP}
${IDRAC9_NAME}	${IDRAC9_NAME}
${IDRAC9_AWAITED_STRING_ON_CONNECT_CMD}	${IDRAC9_AWAITED_STRING_ON_CONNECT_CMD}
${IDRAC9_USERNAME}	${IDRAC9_USERNAME}
${IDRAC9_PASSWORD}	${IDRAC9_PASSWORD}

*** Test Cases ***
Test Check Commands
	[Tags]	NON-CRITICAL	NEED-REVIEW
	[Setup]	CLI:Add Device	${IDRAC9_NAME}	idrac6	${IDRAC9_IP}	${IDRAC9_USERNAME}	${IDRAC9_PASSWORD}	on-demand
	CLI:Enter Path	/settings/devices/${IDRAC9_NAME}/access
	CLI:Write	set type=idrac6
	CLI:Commit
	CLI:Enter Path	/access/${IDRAC9_NAME}
	Write	connect
	Read Until	^\\
	Write Bare	\x1c
	${OUTPUT}	Read Until	${IDRAC9_AWAITED_STRING_ON_CONNECT_CMD}
	SUITE:Check Idrac9 Info
	Write	get BIOS.SysInformation.SystemModelName
	${OUTPUT}	Read Until	${IDRAC9_AWAITED_STRING_ON_CONNECT_CMD}
	Write	get BIOS.SysInformation.SystemModelName
	${MODEL_NAME}	Read Until	${IDRAC9_AWAITED_STRING_ON_CONNECT_CMD}
	Should Contain	${MODEL_NAME}	SystemModelName=PowerEdge R240
	Write	quit
	Read Until	^\
	[Teardown]	Run Keywords	CLI:Disconnect Device
	...	AND	CLI:Delete Devices	${IDRAC9_NAME}

*** Keywords ***
SUITE:Setup
	CLI:Open	session_alias=admin_session	#timeout=240s
	CLI:Delete Devices	${IDRAC9_NAME}

SUITE:Teardown
	CLI:Close Connection
	CLI:Open
	CLI:Delete Devices	${IDRAC9_NAME}
	CLI:Close Connection

SUITE:Check Idrac9 Info
	Sleep	5s
	Write Bare	\t\t
	${OUTPUT}=	Read Until	${IDRAC9_AWAITED_STRING_ON_CONNECT_CMD}
	@{OPTIONS}=	Create List	arp	autoupdatescheduler	bioscert	biosscan	cd	clearasrscreen	clearpending	closessn
	...	clrsel	config	coredump	coredumpdelete	debug	diagnostics	driverpack	eventfilters	exit
	...	exposeisminstallertohost	fcstatistics	FrontPanelError	fwupdate	get	getconfig	gethostnetworkinterfaces
	...	getled	getniccfg	getraclog	getractime	getremoteservicesstatus	getsel	getsensorinfo	getssninfo	getsvctag
	...	getsysinfo	gettracelog	getuscversion	getversion	GroupManager	help	httpsbootcert	hwinventory	idmconfig
	...	ifconfig	inlettemphistory	jobqueue	lclog	license	netstat	networktransceiverstatistics	nicstatistics
	...	ping	ping6	quit	racdump	racreset	racresetcfg	recover	remoteimage	rollback	sekm	sensorsettings
	...	serialcapture	serveraction	set	setled	setniccfg	sshpkauth	sslcertdelete	sslcertview	sslcsrgen
	...	sslencryptionstrength	sslresetcfg	storage	supportassist	swinventory	switchconnection	systemconfig
	...	systemerase	systemperfstatistics	techsupreport	testalert	testemail	testrsyslogconnection	testtrap
	...	traceroute	traceroute6	update	usercertview	vflashpartition	vflashsd	vmdisconnect
	FOR	${OPTION}	IN	@{OPTIONS}
		Should Contain	${OUTPUT}	${OPTION}
	END