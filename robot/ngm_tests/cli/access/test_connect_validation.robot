*** Settings ***
Resource	../init.robot
Documentation	Tests Access through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${USERNAME}	${DEFAULT_USERNAME}
${PASSWORD}	${DEFAULT_PASSWORD}

*** Test Cases ***
Test connect command with credentials ask during login
	[Tags]	EXCLUDEIN3_2	#v3.2 only works with password set
	[Setup]	Run Keywords	CLI:Delete Devices	${DUMMY_DEVICE_CONSOLE_NAME}
	...	AND	CLI:Add Device	${DUMMY_DEVICE_CONSOLE_NAME}	device_console	127.0.0.1	${EMPTY}	${EMPTY}
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Set	credential=ask_during_login
	CLI:Commit
	Sleep	2s
	CLI:Enter Path	/access/${DUMMY_DEVICE_CONSOLE_NAME}
	Write	connect
	Read Until	[Enter '^Ec.' to cli ]
	Write Bare	\n
	Read Until	Login:
	Write	${USERNAME}
	Read Until	Password:
	CLI:Write	${PASSWORD}
	${OUTPUT}=	CLI:Write	pwd
	Should Contain	${OUTPUT}	/
	[Teardown]	Run Keywords	CLI:Disconnect Device
	...	AND	CLI:Delete Devices	${DUMMY_DEVICE_CONSOLE_NAME}

Test connect command with credentials ask during login changing port number
	[Tags]	EXCLUDEIN3_2	#v3.2 only works with password set
	[Setup]	Run Keywords	CLI:Delete Devices	${DUMMY_DEVICE_CONSOLE_NAME}
	...	AND	CLI:Add Device	${DUMMY_DEVICE_CONSOLE_NAME}	device_console	127.0.0.1	${EMPTY}	${EMPTY}
	CLI:Enter Path	/settings/devices/${DUMMY_DEVICE_CONSOLE_NAME}/access/
	CLI:Set	credential=ask_during_login port=22
	CLI:Commit
	Sleep	2s
	CLI:Enter Path	/access/${DUMMY_DEVICE_CONSOLE_NAME}
	Write	connect
	Read Until	[Enter '^Ec.' to cli ]
	Write Bare	\n
	Read Until	Login:
	Write	${USERNAME}
	Read Until	Password:
	CLI:Write	${PASSWORD}
	${OUTPUT}=	CLI:Write	pwd
	Should Contain	${OUTPUT}	/
	[Teardown]	Run Keywords	CLI:Disconnect Device
	...	AND	CLI:Delete Devices	${DUMMY_DEVICE_CONSOLE_NAME}

Test connect command with credentials set now
	[Setup]	Run Keywords	CLI:Delete Devices	${DUMMY_DEVICE_CONSOLE_NAME}
	...	AND	CLI:Add Device	${DUMMY_DEVICE_CONSOLE_NAME}	device_console	127.0.0.1	${USERNAME}	${PASSWORD}	on-demand
	Sleep	2s
	CLI:Connect	${DUMMY_DEVICE_CONSOLE_NAME}
	Write	pwd
	${OUTPUT}=	CLI:Read Until Prompt
	Should Contain	${OUTPUT}	/
	[Teardown]	CLI:Delete Devices	${DUMMY_DEVICE_CONSOLE_NAME}

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Delete Devices	${DUMMY_DEVICE_CONSOLE_NAME}
	CLI:Close Connection
