*** Settings ***
Resource	../../init.robot
Documentation	Test tabular view configuration
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2
Default Tags	SSH

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
@{PREDEFINED_COLUMN_OPTIONS}     alias    end_point  groups   ip_address   ip_alias    kvm_port    mode    nodegrid_host  port_alias    second_ip_alias   serial_port    type
@{PREDEFINED_COLUMN_OPTIONS_4_2}      end_point  groups   ip_address   ip_alias    kvm_port    mode    nodegrid_host  port_alias

*** Test Cases ***
Test case to check the available options to set for tabular view
    CLI:Enter Path  /settings/devices_views_preferences/
    ${available_options}    CLI:Show
    Should Contain  ${available_options}    predefined_columns =
    Should Contain  ${available_options}    custom_columns =
    Run Keyword If	${NGVERSION} == 4.2	CLI:Test Set Field Options Raw  predefined_columns  @{PREDEFINED_COLUMN_OPTIONS_4_2}
    Run Keyword If	${NGVERSION} > 4.2	CLI:Test Set Field Options Raw  predefined_columns  @{PREDEFINED_COLUMN_OPTIONS}

Test case to set predefined column and custom_column
    CLI:Enter Path  /settings/devices_views_preferences/
    FOR		    ${PREDEFINED_COLUMN_OPTION}  IN  @{PREDEFINED_COLUMN_OPTIONS}
        Run Keyword If	${NGVERSION} > 4.2	CLI:Set  predefined_columns=${PREDEFINED_COLUMN_OPTION}
        CLI:Set  custom_columns=1
        CLI:Commit
    END
    FOR		    ${PREDEFINED_COLUMN_OPTION}  IN  @{PREDEFINED_COLUMN_OPTIONS_4_2}
        Run Keyword If	${NGVERSION} == 4.2	CLI:Set  predefined_columns=${PREDEFINED_COLUMN_OPTION}
        CLI:Set  custom_columns=1
        CLI:Commit
    END
	[Teardown]	SUITE:Set Back To Default

*** Keyword ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection

SUITE:Set Back To Default
	CLI:Enter Path	/settings/devices_views_preferences
	CLI:Set	custom_columns=
	CLI:Set	predefined_columns=
	CLI:Commit

