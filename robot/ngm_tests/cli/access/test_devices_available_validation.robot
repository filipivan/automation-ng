*** Settings ***
Resource	../init.robot
Documentation	Tests Access through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Test Template	CLI:Create Device Check Available Commands On Access And Delete

Suite Setup	Go To Access
Suite Teardown	Delete Devices And Close Connection

*** Test Cases ***
cimc_ucs_test	cimc_ucs_test	cimc_ucs
console_server_acs_test	console_server_acs_test	console_server_acs
console_server_acs6000_test	console_server_acs6000_test	console_server_acs6000
console_server_digicp_test	console_server_digicp_test	console_server_digicp
console_server_lantronix_test	console_server_lantronix_test	console_server_lantronix
console_server_opengear_test	console_server_opengear_test	console_server_opengear
console_server_nodegrid_test	console_server_nodegrid_test	console_server_nodegrid
device_console_test	device_console_test	device_console
drac_test	drac_test	drac
idrac6_test	idrac6_test	idrac6
ilo_test	ilo_test	ilo
ilom_test	ilom_test	ilom
imm_test	imm_test	imm
infrabox_test	infrabox_test	infrabox
ipmi_1.5_test	ipmi_1.5_test	ipmi_1.5
ipmi_2.0_test	ipmi_2.0_test	ipmi_2.0
kvm_aten_test	kvm_aten_test	kvm_aten
kvm_dsr_test	kvm_dsr_test	kvm_dsr
kvm_mpu_test	kvm_mpu_test	kvm_mpu
kvm_raritan_test	kvm_raritan_test	kvm_raritan
netapp_test	netapp_test	netapp
pdu_apc_test	pdu_apc_test	pdu_apc
pdu_baytech_test	pdu_baytech_test	pdu_baytech
pdu_eaton_test	pdu_eaton_test	pdu_eaton
pdu_enconnex_test	pdu_enconnex_test	pdu_enconnex
pdu_mph2_test	pdu_mph2_test	pdu_mph2
pdu_pm3000_test	pdu_pm3000_test	pdu_pm3000
pdu_raritan_test	pdu_raritan_test	pdu_raritan
pdu_servertech_test	pdu_servertech_test	pdu_servertech
pdu_geist_test	pdu_geist_test	pdu_geist
#usb_test	usb_test	usb
virtual_console_kvm_test	virtual_console_kvm_test	virtual_console_kvm
virtual_console_vmware_test	virtual_console_vmware_test	virtual_console_vmware

*** Keywords ***
Go To Access
	CLI:Open
	CLI:Enter Path	/access
	CLI:Delete Devices	cimc_ucs_test	console_server_acs_test	console_server_acs6000_test	console_server_digicp_test
	...	console_server_lantronix_test	console_server_opengear_test	device_console_test	drac_test	idrac6_test	ilo_test
	...	ilom_test	imm_test	infrabox_test	ipmi_1.5_test	ipmi_2.0_test	kvm_aten_test	kvm_dsr_test	kvm_mpu_test
	...	kvm_raritan_test	netapp_test	pdu_apc_test	pdu_baytech_test	pdu_eaton_test	pdu_enconnex_test	pdu_mph2_test
	...	pdu_pm3000_test	pdu_raritan_test	pdu_servertech_test	usb_test	virtual_console_kvm_test	virtual_console_vmware_test
	...	console_server_nodegrid_test	pdu_geist_test

Delete Devices And Close Connection
	CLI:Delete Devices	cimc_ucs_test	console_server_acs_test	console_server_acs6000_test	console_server_digicp_test
	...	console_server_lantronix_test	console_server_opengear_test	device_console_test	drac_test	idrac6_test	ilo_test
	...	ilom_test	imm_test	infrabox_test	ipmi_1.5_test	ipmi_2.0_test	kvm_aten_test	kvm_dsr_test	kvm_mpu_test
	...	kvm_raritan_test	netapp_test	pdu_apc_test	pdu_baytech_test	pdu_eaton_test	pdu_enconnex_test	pdu_mph2_test
	...	pdu_pm3000_test	pdu_raritan_test	pdu_servertech_test	usb_test	virtual_console_kvm_test	virtual_console_vmware_test
	...	console_server_nodegrid_test	pdu_geist_test
	CLI:Close Connection

CLI:Create Device Check Available Commands On Access And Delete
	[Arguments]	${DEVICE_NAME}	${DEVICE_TYPE}
	Skip If	'${DEVICE_TYPE}'=='pdu_geist' and '${NGVERSION}' < '4.0' or '${DEVICE_TYPE}'=='console_server_nodegrid' and '${NGVERSION}' < '4.0'	Types not in 3.2
	CLI:Add Device	${DEVICE_NAME}	${DEVICE_TYPE}
	CLI:Enter Path	/access/
	CLI:Show
	CLI:Enter Path	/access/${DEVICE_NAME}

	CLI:Test Available Commands	cd	exit	pwd	reboot	show	shell	show_settings	shutdown	whoami	quit	ls	commit	event_system_clear	event_system_audit	hostname	change_password	revert

	Run Keyword If	'${DEVICE_TYPE}'=='virtual_console_kvm'
	...	Run Keywords
	...	CLI:Test Available Commands	power_suspend	connect	reset_device	power_cycle	power_on	power_off	power_status
	...	AND	CLI:Test Unavailable Commands	outlet_cycle	outlet_status	outlet_off	outlet_on	sp_console	sensors_data	reset_sp	cert_download	cert_request	cert_upload	debug

	Run Keyword If	'${DEVICE_TYPE}'=='usb'
	...	Run Keywords
	...	CLI:Test Available Commands	debug	reset_device	connect	power_cycle	power_on	power_off	power_status
	...	AND	CLI:Test Unavailable Commands	outlet_cycle	outlet_status	outlet_off	outlet_on	sp_console	sensors_data	reset_sp	shutdown_device	cert_download	cert_request	cert_upload	power_suspend

	Run Keyword If	'${DEVICE_TYPE}'=='imm'
	...	Run Keywords
	...	CLI:Test Available Commands	sensors_data	reset_sp	shutdown_device	cert_download	cert_request	cert_upload	connect	reset_device	sp_console	power_cycle	power_on	power_off	power_status
	...	AND	CLI:Test Unavailable Commands	outlet_cycle	outlet_status	outlet_off	outlet_on	debug	power_suspend

	Run Keyword If	'${DEVICE_TYPE}'=='idrac6'
	...	Run Keywords
	...	CLI:Test Available Commands	reset_sp	shutdown_device	cert_download	cert_request	cert_upload	connect	reset_device	sp_console	power_cycle	power_on	power_off	power_status
	...	AND	Run Keyword If	'${NGVERSION}' > '3.2'	CLI:Test Available Commands	sensors_data
	...	AND	CLI:Test Unavailable Commands	outlet_cycle	outlet_status	outlet_off	outlet_on	debug	power_suspend

	Run Keyword If	'${DEVICE_TYPE}'=='ilo'
	...	Run Keywords
	...	CLI:Test Available Commands	cert_download	cert_request	cert_upload	shutdown_device	connect	reset_device	sp_console	power_cycle	power_on	power_off	power_status
	...	AND	CLI:Test Unavailable Commands	outlet_cycle	outlet_status	outlet_off	outlet_on	reset_sp	sensors_data	debug	power_suspend

	Run Keyword If	'${DEVICE_TYPE}'=='cimc_ucs'
	...	Run Keywords
	...	CLI:Test Available Commands	sensors_data	shutdown_device	connect	reset_device	sp_console	power_cycle	power_on	power_off	power_status
	...	AND	CLI:Test Unavailable Commands	outlet_cycle	outlet_status	outlet_off	outlet_on	cert_download	cert_request	cert_upload	reset_sp	debug	power_suspend

	Run Keyword If	'${DEVICE_TYPE}'=='ipmi_2.0'
	...	Run Keywords
	...	CLI:Test Available Commands	shutdown_device	sensors_data	reset_device	sp_console	power_cycle	power_on	power_off	power_status	connect
	...	AND	CLI:Test Unavailable Commands	outlet_cycle	outlet_status	outlet_off	outlet_on	cert_download	cert_request	cert_upload	reset_sp	debug	power_suspend

	Run Keyword If	'${DEVICE_TYPE}'=='ilom'
	...	Run Keywords
	...	CLI:Test Available Commands	sensors_data	connect	reset_device	sp_console	power_cycle	power_on	power_off	power_status
	...	AND	CLI:Test Unavailable Commands	outlet_cycle	outlet_status	outlet_off	outlet_on	power_suspend	debug	reset_sp	shutdown_device	cert_download	cert_request	cert_upload

	Run Keyword If	'${DEVICE_TYPE}'=='drac'
	...	Run Keywords
	...	CLI:Test Available Commands	connect	reset_device	sp_console	power_cycle	power_on	power_off	power_status
	...	AND	CLI:Test Unavailable Commands	outlet_cycle	outlet_status	outlet_off	outlet_on	cert_download	cert_request	cert_upload	shutdown_device	reset_sp	sensors_data	power_suspend	debug

	Run Keyword If	'${DEVICE_TYPE}'=='netapp'
	...	Run Keywords
	...	CLI:Test Available Commands	sensors_data	connect	sp_console	power_cycle	power_on	power_off	power_status
	...	AND	CLI:Test Unavailable Commands	outlet_cycle	outlet_status	outlet_off	outlet_on	debug	power_suspend	reset_sp	shutdown_device	cert_download	cert_request	cert_upload	reset_device

	Run Keyword If	'${DEVICE_TYPE}'=='pdu_apc'
	...	Run Keywords
	...	CLI:Test Available Commands	connect	outlet_cycle	outlet_status	outlet_off	outlet_on
	...	AND	CLI:Test Unavailable Commands	power_suspend	debug	sensors_data	reset_sp	shutdown_device	cert_download	cert_request	cert_upload	reset_device	sp_console	power_cycle	power_on	power_off	power_status

	Run Keyword If	'${DEVICE_TYPE}'=='pdu_baytech'
	...	Run Keywords
	...	CLI:Test Available Commands	connect	outlet_cycle	outlet_status	outlet_off	outlet_on
	...	AND	CLI:Test Unavailable Commands	power_suspend	debug	sensors_data	reset_sp	shutdown_device	cert_download	cert_request	cert_upload	reset_device	sp_console	power_cycle	power_on	power_off	power_status

	Run Keyword If	'${DEVICE_TYPE}'=='pdu_eaton'
	...	Run Keywords
	...	CLI:Test Available Commands	connect	outlet_cycle	outlet_status	outlet_off	outlet_on
	...	AND	CLI:Test Unavailable Commands	power_suspend	debug	sensors_data	reset_sp	shutdown_device	cert_download	cert_request	cert_upload	reset_device	sp_console	power_cycle	power_on	power_off	power_status

	Run Keyword If	'${DEVICE_TYPE}'=='pdu_enconnex'
	...	Run Keywords
	...	CLI:Test Available Commands	connect	outlet_cycle	outlet_status	outlet_off	outlet_on
	...	AND	CLI:Test Unavailable Commands	power_suspend	debug	sensors_data	reset_sp	shutdown_device	cert_download	cert_request	cert_upload	reset_device	sp_console	power_cycle	power_on	power_off	power_status

	Run Keyword If	'${DEVICE_TYPE}'=='pdu_mph2'
	...	Run Keywords
	...	CLI:Test Available Commands	connect	outlet_cycle	outlet_status	outlet_off	outlet_on
	...	AND	CLI:Test Unavailable Commands	power_suspend	debug	sensors_data	reset_sp	shutdown_device	cert_download	cert_request	cert_upload	reset_device	sp_console	power_cycle	power_on	power_off	power_status

	Run Keyword If	'${DEVICE_TYPE}'=='pdu_pm3000'
	...	Run Keywords
	...	CLI:Test Available Commands	connect	outlet_cycle	outlet_status	outlet_off	outlet_on
	...	AND	CLI:Test Unavailable Commands	power_suspend	debug	sensors_data	reset_sp	shutdown_device	cert_download	cert_request	cert_upload	reset_device	sp_console	power_cycle	power_on	power_off	power_status

	Run Keyword If	'${DEVICE_TYPE}'=='pdu_raritan'
	...	Run Keywords
	...	CLI:Test Available Commands	connect	outlet_cycle	outlet_status	outlet_off	outlet_on
	...	AND	CLI:Test Unavailable Commands	power_suspend	debug	sensors_data	reset_sp	shutdown_device	cert_download	cert_request	cert_upload	reset_device	sp_console	power_cycle	power_on	power_off	power_status

	Run Keyword If	'${DEVICE_TYPE}'=='pdu_servertech'
	...	Run Keywords
	...	CLI:Test Available Commands	connect	outlet_cycle	outlet_status	outlet_off	outlet_on
	...	AND	CLI:Test Unavailable Commands	power_suspend	debug	sensors_data	reset_sp	shutdown_device	cert_download	cert_request	cert_upload	reset_device	sp_console	power_cycle	power_on	power_off	power_status

	Run Keyword If	'${DEVICE_TYPE}'=='pdu_geist'
	...	Run Keywords
	...	CLI:Test Available Commands	connect	outlet_cycle	outlet_status	outlet_off	outlet_on
	...	AND	CLI:Test Unavailable Commands	power_suspend	debug	sensors_data	reset_sp	shutdown_device	cert_download	cert_request	cert_upload	reset_device	sp_console	power_cycle	power_on	power_off	power_status

	Run Keyword If	'${DEVICE_TYPE}'=='console_server_acs'
	...	Run Keywords
	...	CLI:Test Available Commands	connect
	...	AND	CLI:Test Unavailable Commands	outlet_cycle	outlet_status	outlet_off	outlet_on	sp_console	sensors_data	reset_sp	shutdown_device	cert_download	cert_request	cert_upload	debug	power_suspend	reset_device	power_cycle	power_on	power_off	power_status

	Run Keyword If	'${DEVICE_TYPE}'=='console_server_acs6000'
	...	Run Keywords
	...	CLI:Test Available Commands	connect
	...	AND	CLI:Test Unavailable Commands	outlet_cycle	outlet_status	outlet_off	outlet_on	sp_console	sensors_data	reset_sp	shutdown_device	cert_download	cert_request	cert_upload	debug	power_suspend	reset_device	power_cycle	power_on	power_off	power_status

	Run Keyword If	'${DEVICE_TYPE}'=='console_server_digicp'
	...	Run Keywords
	...	CLI:Test Available Commands	connect
	...	AND	CLI:Test Unavailable Commands	outlet_cycle	outlet_status	outlet_off	outlet_on	sp_console	sensors_data	reset_sp	shutdown_device	cert_download	cert_request	cert_upload	debug	power_suspend	reset_device	power_cycle	power_on	power_off	power_status

	Run Keyword If	'${DEVICE_TYPE}'=='console_server_lantronix'
	...	Run Keywords
	...	CLI:Test Available Commands	connect
	...	AND	CLI:Test Unavailable Commands	outlet_cycle	outlet_status	outlet_off	outlet_on	sp_console	sensors_data	reset_sp	shutdown_device	cert_download	cert_request	cert_upload	debug	power_suspend	reset_device	power_cycle	power_on	power_off	power_status

	Run Keyword If	'${DEVICE_TYPE}'=='console_server_opengear'
	...	Run Keywords
	...	CLI:Test Available Commands	connect
	...	AND	CLI:Test Unavailable Commands	outlet_cycle	outlet_status	outlet_off	outlet_on	sp_console	sensors_data	reset_sp	shutdown_device	cert_download	cert_request	cert_upload	debug	power_suspend	reset_device	power_cycle	power_on	power_off	power_status

	Run Keyword If	'${DEVICE_TYPE}'=='console_server_nodegrid'
	...	Run Keywords
	...	CLI:Test Available Commands	connect
	...	AND	CLI:Test Unavailable Commands	outlet_cycle	outlet_status	outlet_off	outlet_on	sp_console	sensors_data	reset_sp	shutdown_device	cert_download	cert_request	cert_upload	debug	power_suspend	reset_device	power_cycle	power_on	power_off	power_status

	Run Keyword If	'${DEVICE_TYPE}'=='device_console'
	...	Run Keywords
	...	CLI:Test Available Commands	connect
	...	AND	CLI:Test Unavailable Commands	outlet_cycle	outlet_status	outlet_off	outlet_on	sp_console	sensors_data	reset_sp	shutdown_device	cert_download	cert_request	cert_upload	debug	power_suspend	reset_device	power_cycle	power_on	power_off	power_status

	[TEARDOWN]	CLI:Delete Devices	${DEVICE_NAME}
