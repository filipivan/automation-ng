*** Settings ***
Resource	../init.robot
Documentation	Tests Access through the CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW

Suite Setup	Go To Access
Suite Teardown	CLI:Close Connection

*** Test Cases ***
Test tab-tab command
	CLI:Test Available Commands	cd	commit	event_system_clear	hostname	pwd	reboot	search	show	shutdown	change_password	event_system_audit	exit	ls	quit	revert	shell	show_settings	whoami

Test load accessible devices
	${DEVICES}=	CLI:Get Available Paths
	Set Suite Variable	${DEVICES}

Test show command
	CLI:Test Show Command	@{DEVICES}

*** Keywords ***
Go To Access
	CLI:Open
	CLI:Enter Path	/access
