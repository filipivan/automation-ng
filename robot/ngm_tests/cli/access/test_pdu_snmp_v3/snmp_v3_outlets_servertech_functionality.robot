*** Settings ***
Resource	../../../init.robot
Documentation	Functionality tests to access servertech PDU using snmp v3 through CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	NON-CRITICAL	DEPENDENCE_DEVICE	DEPENDENCE_PDU	REVIEWED
Default Tags	ACCESS	PDU

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SSH_OPTIONS}	${SSH_OPTIONS_PDU_SERVERTECH}
${DEVICE_NAME}	${DUMMY_PDU_SERVERTECH_NAME}
${DEVICE}	${PDU_SERVERTECH}
${CLONE_PDU}	${PDU_SERVERTECH_CLONE}
${CLONE_NAME}	pdu_s

*** Test Cases ***
Test Single Outlet Off To Prepare Environment
	CLI:Switch Connection	default
	CLI:Outlet Off	DEVICE=${DEVICE_NAME}	OUTLET=AB9
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=off	OUTLET=AB9

Test Single Outlet On
	CLI:Outlet On	DEVICE=${DEVICE_NAME}	OUTLET=AB9
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=on	OUTLET=AB9

Test Single Outlet Off
	CLI:Outlet Off	DEVICE=${DEVICE_NAME}	OUTLET=AB9
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=off	OUTLET=AB9

Test Single Outlet Cycle
	CLI:Outlet Cycle	DEVICE=${DEVICE_NAME}	OUTLET=AB9
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=on	OUTLET=AB9

############ MULTIPLE OUTLETS ############
Test Multiple Outlets Off To Prepare Environment
	CLI:Switch Connection	default
	CLI:Outlet Off	DEVICE=${DEVICE_NAME}	OUTLET=AA9,AB9,AC9
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=off	OUTLET=AA9,AB9,AC9

Test Multiple Outlets On
	CLI:Outlet On	DEVICE=${DEVICE_NAME}	OUTLET=AA9,AB9,AC9
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=on	OUTLET=AA9,AB9,AC9

Test Multiple Outlets Off
	CLI:Outlet Off	DEVICE=${DEVICE_NAME}	OUTLET=AA9,AB9,AC9
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=off	OUTLET=AA9,AB9,AC9

Test Multiple Outlets Cycle
	CLI:Outlet Cycle	DEVICE=${DEVICE_NAME}	OUTLET=AA9,AB9,AC9
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=on	OUTLET=AA9,AB9,AC9

############ ALL OUTLETS ############
Test All Outlets Off To Prepare Environment
	CLI:Switch Connection	default
	CLI:Outlet Off	DEVICE=${DEVICE_NAME}
	Wait Until Keyword Succeeds	3m	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=off

Test All Outlets On
	CLI:Outlet On	DEVICE=${DEVICE_NAME}
	Wait Until Keyword Succeeds	2m	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=on

Test All Outlets Off
	CLI:Outlet Off	DEVICE=${DEVICE_NAME}
	Wait Until Keyword Succeeds	2m	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=off

Test All Outlets Cycle
	CLI:Outlet Cycle	DEVICE=${DEVICE_NAME}
	Wait Until Keyword Succeeds	2m	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=on

*** Keywords ***
SUITE:Setup
	CLI:Open
	${HAVE_PDU_INFO}=	Run Keyword And Return Status	Variable Should Exist	${PDU_SERVERTECH}
	Run Keyword If	${HAVE_PDU_INFO} == ${FALSE}	Fail	Missing PDU data required to create the PDU device
	${HAS_RUN_TEST}=	Run Keyword And Return Status	Variable Should Exist	${RUN_TEST}
	Run Keyword If	${HAS_RUN_TEST}	Run Keyword If	${RUN_TEST} == ${FALSE}	Fail	Could not get outlets
	CLI:Delete All Devices
	CLI:Clone Type	pdu_servertech	${CLONE_NAME}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Enter Path	/settings/types/
	${STATUS}=	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Check If Exists	${CLONE_PDU}
	Run Keyword If	'${NGVERSION}' >= '4.2' and ${STATUS} == False	CLI:Clone Type	pdu_servertech	${CLONE_PDU}	ssh_options=${SSH_OPTIONS}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Add Device	${DEVICE_NAME}	${DEVICE}[type]	${DEVICE}[ip]	${DEVICE}[username]	${DEVICE}[password]	enabled
	...	ELSE	CLI:Add Device	${DEVICE_NAME}	${CLONE_PDU}	${DEVICE}[ip]	${DEVICE}[username]	${DEVICE}[password]	enabled
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/management/
	CLI:Set	snmp=yes snmp_version=v3 snmpv3_username=zpeautomation snmpv3_security_level=authpriv snmpv3_authentication_algorithm=md5 snmpv3_authentication_password=zpeautomation snmpv3_privacy_algorithm=des snmpv3_privacy_password=zpeautomation
	CLI:Commit
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/commands/outlet/
	CLI:Set	protocol=snmp
	CLI:Commit
	CLI:Discover Now	${DEVICE_NAME}	pdu
	${RUN_TEST}=	Run Keyword And Return Status	Wait Until Keyword Succeeds	150s	5s	CLI:Outlet Status	DEVICE=${DEVICE_NAME}	OUTLET=AA9,AB9,AC9,AA10,AB10,AC10
	Run Keyword If	${RUN_TEST} == ${FALSE}	Fail	Could not get outlets status

SUITE:Teardown
	CLI:Delete All Devices
	CLI:Enter Path  /settings/types
	CLI:Delete If Exists Confirm	${CLONE_NAME}
	CLI:Close Connection