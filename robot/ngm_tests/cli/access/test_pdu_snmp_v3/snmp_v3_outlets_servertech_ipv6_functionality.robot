*** Settings ***
Resource	../../../init.robot
Documentation	Functionality tests to access servertech PDU using snmp v3 and ipv6 through CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	DEPENDENCE_DEVICE	DEPENDENCE_PDU	NON-CRITICAL	BUG_NG_6134
Default Tags	ACCESS	PDU

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SSH_OPTIONS}	${SSH_OPTIONS_PDU_SERVERTECH}
${DEVICE_NAME}	${DUMMY_PDU_SERVERTECH_NAME}
${DEVICE}	${PDU_SERVERTECH}
${CLONE_PDU}	${PDU_SERVERTECH_CLONE}
${CLONE_NAME}	pdu_s
*** Test Cases ***
Test Outlet Off To Prepare Environment
	CLI:Switch Connection	default
	CLI:Outlet Off	DEVICE=${DEVICE_NAME}	OUTLET=AA11,AB11,AC11
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=off	OUTLET=AA11,AB11,AC11

Test Outlet On
	CLI:Outlet On	DEVICE=${DEVICE_NAME}	OUTLET=AA11,AB11,AC11
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=on	OUTLET=AA11,AB11,AC11

Test Outlet Off
	CLI:Outlet Off	DEVICE=${DEVICE_NAME}	OUTLET=AA11,AB11,AC11
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=off	OUTLET=AA11,AB11,AC11

Test Outlet Cycle
	CLI:Outlet Cycle	DEVICE=${DEVICE_NAME}	OUTLET=AA11,AB11,AC11
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=on	OUTLET=AA11,AB11,AC11

*** Keywords ***
SUITE:Setup
	CLI:Open
	${HAVE_PDU_INFO}=	Run Keyword And Return Status	Variable Should Exist	${PDU_SERVERTECH}
	Run Keyword If	${HAVE_PDU_INFO} == ${FALSE}	Skip	Missing PDU data required to create the PDU device
	CLI:Delete All Devices
	CLI:Clone Type	pdu_servertech	${CLONE_NAME}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Enter Path	/settings/types/
	${STATUS}=	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Check If Exists	${CLONE_PDU}
	Run Keyword If	'${NGVERSION}' >= '4.2' and ${STATUS} == False	CLI:Clone Type	pdu_servertech	${CLONE_PDU}	ssh_options=${SSH_OPTIONS}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Add Device	${DEVICE_NAME}	${CLONE_NAME}	${DEVICE}[ip6]	${DEVICE}[username]	${DEVICE}[password]	enabled
	...	ELSE	CLI:Add Device	${DEVICE_NAME}	${CLONE_PDU}	${DEVICE}[ip6]	${DEVICE}[username]	${DEVICE}[password]	enabled
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/management/
	CLI:Set	snmp=yes snmp_version=v3 snmpv3_username=zpeautomation snmpv3_security_level=authpriv snmpv3_authentication_algorithm=md5 snmpv3_authentication_password=zpeautomation snmpv3_privacy_algorithm=des snmpv3_privacy_password=zpeautomation
	CLI:Commit
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/commands/outlet/
	CLI:Set	protocol=snmp
	CLI:Commit
	CLI:Discover Now	${DEVICE_NAME}	pdu
	${RUN_TEST}=	Run Keyword And Return Status	Wait Until Keyword Succeeds	150s	5s	CLI:Outlet Status	DEVICE=${DEVICE_NAME}	OUTLET=AA11,AB11,AC11
	Run Keyword If	${RUN_TEST} == ${FALSE}	Skip	Could not get outlets status

SUITE:Teardown
	CLI:Delete All Devices
	CLI:Enter Path  /settings/types
	CLI:Delete If Exists Confirm	${CLONE_NAME}
	CLI:Close Connection