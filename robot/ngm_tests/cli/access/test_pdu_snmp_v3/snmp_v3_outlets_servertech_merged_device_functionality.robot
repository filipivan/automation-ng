*** Settings ***
Resource	../../../init.robot
Documentation	Functionality tests to access servertech PDU merged device using snmp v3 through CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	DEPENDENCE_DEVICE	DEPENDENCE_PDU	EXCLUDEIN3_2
Default Tags	ACCESS	PDU

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SSH_OPTIONS}	${SSH_OPTIONS_PDU_SERVERTECH}
${DEVICE_NAME}	${DUMMY_PDU_SERVERTECH_NAME}
${DEVICE}	${PDU_SERVERTECH}
${CLONE_PDU}	${PDU_SERVERTECH_CLONE}
${DEVICE_MERGED}	device_merged
${CLONE_NAME}	pdu_s

*** Test Cases ***
############ SINGLE OUTLET TO MERGED DEVICE WITH OUTLET COMMAND ############
Test Single Outlet Off Merged Device To Prepare Environment
	CLI:Switch Connection	default
	CLI:Outlet Off	DEVICE=${DEVICE_NAME}	OUTLET=AA12
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=off	OUTLET=AA12

Test Single Outlet On Merged Device
	CLI:Outlet On	DEVICE=${DEVICE_NAME}	OUTLET=AA12
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=on	OUTLET=AA12

Test Single Outlet Off Merged Device
	CLI:Outlet Off	DEVICE=${DEVICE_NAME}	OUTLET=AA12
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=off	OUTLET=AA12

Test Single Outlet Cycle Merged Device
	CLI:Outlet Cycle	DEVICE=${DEVICE_NAME}	OUTLET=AA12
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=on	OUTLET=AA12

############ MULTIPLE OUTLETS TO MERGED DEVICE WITH OUTLET COMMAND ############
Test Multiple Outlets Off Merged Device To Prepare Environment
	CLI:Enter Path	/settings/devices/${DEVICE_MERGED}/commands
	CLI:Delete If Exists	outlet
	SUITE:Merge Outlets To Device

	CLI:Outlet Off	DEVICE=${DEVICE_NAME}	OUTLET=AA12,AB12,AC12
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=off	OUTLET=AA12,AB12,AC12

Test Multiple Outlets On Merged Device
	CLI:Outlet On	DEVICE=${DEVICE_NAME}	OUTLET=AA12,AB12,AC12
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=on	OUTLET=AA12,AB12,AC12

Test Multiple Outlets Off Merged Device
	CLI:Outlet Off	DEVICE=${DEVICE_NAME}	OUTLET=AA12,AB12,AC12
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=off	OUTLET=AA12,AB12,AC12

Test Multiple Outlets Cycle Merged Device
	CLI:Outlet Cycle	DEVICE=${DEVICE_NAME}	OUTLET=AA12,AB12,AC12
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=on	OUTLET=AA12,AB12,AC12

############ MULTIPLE OUTLETS TO MERGED DEVICE WITH POWER COMMAND ############
Test Power Off Multiple Outlets Merged Device To Prepare Environment
	CLI:Enter Path	/access/${DEVICE_MERGED}
	CLI:Device Power Off
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=off	OUTLET=AA12,AB12,AC12

Test Power On Multiple Outlets Merged Device
	CLI:Enter Path	/access/${DEVICE_MERGED}
	CLI:Device Power On
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=on	OUTLET=AA12,AB12,AC12

Test Power Off Multiple Outlets Merged Device
	CLI:Enter Path	/access/${DEVICE_MERGED}
	CLI:Device Power Off
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=off	OUTLET=AA12,AB12,AC12

Test Power Cycle Multiple Outlets Merged Device
	CLI:Enter Path	/access/${DEVICE_MERGED}
	CLI:Device Power Cycle
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=on	OUTLET=AA12,AB12,AC12

*** Keywords ***
SUITE:Setup
	CLI:Open
	${HAVE_PDU_INFO}=	Run Keyword And Return Status	Variable Should Exist	${PDU_SERVERTECH}
	Run Keyword If	${HAVE_PDU_INFO} == ${FALSE}	Skip	Missing PDU data required to create the PDU device
	CLI:Delete All Devices
	CLI:Clone Type	pdu_servertech	${CLONE_NAME}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Enter Path	/settings/types/
	${STATUS}=	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Check If Exists	${CLONE_PDU}
	Run Keyword If	'${NGVERSION}' >= '4.2' and ${STATUS} == False	CLI:Clone Type	pdu_servertech	${CLONE_PDU}	ssh_options=${SSH_OPTIONS}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Add Device	${DEVICE_NAME}	${DEVICE}[type]	${DEVICE}[ip]	${DEVICE}[username]	${DEVICE}[password]	enabled
	...	ELSE	CLI:Add Device	${DEVICE_NAME}	${CLONE_PDU}	${DEVICE}[ip]	${DEVICE}[username]	${DEVICE}[password]	enabled
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/management/
	CLI:Set	snmp=yes snmp_version=v3 snmpv3_username=zpeautomation snmpv3_security_level=authpriv snmpv3_authentication_algorithm=md5 snmpv3_authentication_password=zpeautomation snmpv3_privacy_algorithm=des snmpv3_privacy_password=zpeautomation
	CLI:Commit
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/commands/outlet/
	CLI:Set	protocol=snmp
	CLI:Commit
	CLI:Discover Now	${DEVICE_NAME}	pdu
	${RUN_TEST}=	Run Keyword And Return Status	Wait Until Keyword Succeeds	150s	5s	CLI:Outlet Status	DEVICE=${DEVICE_NAME}	OUTLET=AA12,AB12,AC12
	Run Keyword If	${RUN_TEST} == ${FALSE}	Skip	Could not get outlets status
	CLI:Add Device	${DEVICE_MERGED}	device_console	127.0.0.1	root	${ROOT_PASSWORD}	enabled
	SUITE:Merge One Outlet To Device

SUITE:Teardown
	CLI:Delete All Devices
	CLI:Enter Path  /settings/types
	CLI:Delete If Exists Confirm	${CLONE_NAME}
	CLI:Close Connection

SUITE:Merge One Outlet To Device
	Wait Until Keyword Succeeds	60s	3s	SUITE:Show Outlets
	${FULLNAME_OUTLET}=	Set Variable	DUMMY_SERVERTECH:A:AA12-TowerA_InfeedA_Outlet12
	CLI:Enter Path	/settings/devices/${DEVICE_MERGED}/commands
	CLI:Add
	CLI:Set	command=outlet outlet=${FULLNAME_OUTLET} enabled=yes
	CLI:Commit
	${OUTPUT}=	CLI:Show
	Should Match Regexp	${OUTPUT}	\\s+outlet\\s+enabled

SUITE:Merge Outlets To Device
	${OUTPUT}=	Wait Until Keyword Succeeds	60s	3s	SUITE:Show Outlets
	${LIST}=	Create List	AA12	AB12	AC12	TowerA_InfeedA_Outlet12	TowerA_InfeedB_Outlet12	TowerA_InfeedB_Outlet12
	CLI:Should Contain All	${OUTPUT}	${LIST}
	${FULLNAME_OUTLETS}=	Set Variable	DUMMY_SERVERTECH:A:AA12-TowerA_InfeedA_Outlet12,DUMMY_SERVERTECH:A:AB12-TowerA_InfeedB_Outlet12,DUMMY_SERVERTECH:A:AC12-TowerA_InfeedC_Outlet12
	CLI:Enter Path	/settings/devices/${DEVICE_MERGED}/commands
	CLI:Add
	CLI:Set	command=outlet outlet=${FULLNAME_OUTLETS} enabled=yes
	CLI:Commit
	${OUTPUT}=	CLI:Show
	Should Match Regexp	${OUTPUT}	\\s+outlet\\s+enabled

SUITE:Show Outlets
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/outlets/
	${LIST}=	Create List	AA12	TowerA_InfeedA_Outlet12
	${OUTPUT}=	CLI:Write	show	user=Yes	lines=No
	CLI:Should Contain All	${OUTPUT}	${LIST}
	[Return]	${OUTPUT}