*** Settings ***
Resource	../../init.robot
Documentation	Test switched pdu geist commands like outlet_on, outlet_off, outlet_cycle and outlet_status
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	DEPENDENCE_DEVICE	DEPENDENCE_PDU	NON-CRITICAL	EXCLUDEIN3_2	REVIEWED
Default Tags	ACCESS	PDU

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE_NAME}	dummy_pdu_geist
${DEVICE}	${PDU_GEIST}
${CLONE_NAME}	pdu_g

*** Test Cases ***
############ MULTIPLE OUTLETS ############
Test Multiple Outlets Off To Prepare Environment
	CLI:Switch Connection	default
	CLI:Outlet Off	DEVICE=${DEVICE_NAME}	OUTLET=1-4
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=off	OUTLET=1-4

Test Multiple Outlets On
	CLI:Outlet On	DEVICE=${DEVICE_NAME}	OUTLET=1-4
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=on	OUTLET=1-4

Test Multiple Outlets Off
	CLI:Outlet Off	DEVICE=${DEVICE_NAME}	OUTLET=1-4
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=off	OUTLET=1-4

Test Multiple Outlets Cycle
	CLI:Outlet Cycle	DEVICE=${DEVICE_NAME}	OUTLET=1-4
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=on	OUTLET=1-4

############ ALL OUTLETS ############
Test All Outlets Off To Prepare Environment
	CLI:Switch Connection	default
	CLI:Outlet Off	DEVICE=${DEVICE_NAME}	OUTLET=1-48
	Wait Until Keyword Succeeds	2m	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=off	OUTLET=1-48

Test All Outlets On
	CLI:Outlet On	DEVICE=${DEVICE_NAME}	OUTLET=1-48
	Wait Until Keyword Succeeds	2m	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=on	OUTLET=1-48

Test All Outlets Off
	CLI:Outlet Off	DEVICE=${DEVICE_NAME}	OUTLET=1-48
	Wait Until Keyword Succeeds	2m	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=off	OUTLET=1-48

Test All Outlets Cycle
	CLI:Outlet Cycle	DEVICE=${DEVICE_NAME}	OUTLET=1-48
	Wait Until Keyword Succeeds	2m	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=on	OUTLET=1-48

*** Keywords ***
SUITE:Setup
	CLI:Open
	${HAVE_PDU_INFO}=	Run Keyword And Return Status	Variable Should Exist	${PDU_GEIST}
	Run Keyword If	${HAVE_PDU_INFO} == ${FALSE}	Fail	Missing PDU data required to create the PDU device
	CLI:Delete All Devices
	CLI:Clone Type	pdu_geist	${CLONE_NAME}
	CLI:Enter Path	/settings/types/
	${STATUS}=	CLI:Check If Exists	${CLONE_NAME}
	Run Keyword If	${STATUS} == False	CLI:Clone Type	pdu_geist	${CLONE_NAME}
	CLI:Add Device	${DEVICE_NAME}	${CLONE_NAME}	${DEVICE}[ip]	${DEVICE}[username]	${DEVICE}[password]	enabled
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/management/
	CLI:Set	snmp=yes snmp_version=v2 snmp_commmunity=private
	CLI:Commit
	CLI:Discover Now	${DEVICE_NAME}	pdu
	${RUN_TEST}=	Run Keyword And Return Status	Wait Until Keyword Succeeds	120s	5s	CLI:Outlet Status	DEVICE=${DEVICE_NAME}	OUTLET=1-4
	Run Keyword If	${RUN_TEST} == ${FALSE}	Fail	Could not get outlets status

SUITE:Teardown
	CLI:Open
	CLI:Delete All Devices
	CLI:Enter Path  /settings/types
	CLI:Delete If Exists Confirm	${CLONE_NAME}
	CLI:Close Connection

