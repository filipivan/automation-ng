*** Settings ***
Resource	../../../init.robot
Documentation	Functionality tests to access servertech PDU merged device using snmp v2 through CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	DEPENDENCE_DEVICE	DEPENDENCE_PDU
Default Tags	ACCESS	PDU

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SSH_OPTIONS}	${SSH_OPTIONS_PDU_SERVERTECH}
${DEVICE_NAME}	${DUMMY_PDU_SERVERTECH_NAME}
${DEVICE}	${PDU_SERVERTECH}
${CLONE_PDU}	${PDU_SERVERTECH_CLONE}
${DEVICE_MERGED}	device_merged
${CLONE_NAME}	pdu_s

*** Test Cases ***
############ SINGLE OUTLET TO MERGED DEVICE WITH OUTLET COMMAND ############
Test Single Outlet Off Merged Device To Prepare Environment
	CLI:Switch Connection	default
	CLI:Outlet Off	DEVICE=${DEVICE_NAME}	OUTLET=AA6
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=off	OUTLET=AA6

Test Single Outlet On Merged Device
	CLI:Outlet On	DEVICE=${DEVICE_NAME}	OUTLET=AA6
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=on	OUTLET=AA6

Test Single Outlet Off Merged Device
	CLI:Outlet Off	DEVICE=${DEVICE_NAME}	OUTLET=AA6
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=off	OUTLET=AA6

Test Single Outlet Cycle Merged Device
	CLI:Outlet Cycle	DEVICE=${DEVICE_NAME}	OUTLET=AA6
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=on	OUTLET=AA6

############ MULTIPLE OUTLETS TO MERGED DEVICE WITH OUTLET COMMAND ############
Test Multiple Outlets Off Merged Device To Prepare Environment
	CLI:Enter Path	/settings/devices/${DEVICE_MERGED}/commands
	CLI:Delete If Exists	outlet
	SUITE:Merge Outlets To Device

	CLI:Outlet Off	DEVICE=${DEVICE_NAME}	OUTLET=AA6,AB6,AC6
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=off	OUTLET=AA6,AB6,AC6

Test Multiple Outlets On Merged Device
	CLI:Outlet On	DEVICE=${DEVICE_NAME}	OUTLET=AA6,AB6,AC6
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=on	OUTLET=AA6,AB6,AC6

Test Multiple Outlets Off Merged Device
	CLI:Outlet Off	DEVICE=${DEVICE_NAME}	OUTLET=AA6,AB6,AC6
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=off	OUTLET=AA6,AB6,AC6

Test Multiple Outlets Cycle Merged Device
	CLI:Outlet Cycle	DEVICE=${DEVICE_NAME}	OUTLET=AA6,AB6,AC6
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=on	OUTLET=AA6,AB6,AC6

############ MULTIPLE OUTLETS TO MERGED DEVICE WITH POWER COMMAND ############
Test Power Off Multiple Outlets Merged Device To Prepare Environment
	CLI:Enter Path	/access/${DEVICE_MERGED}
	CLI:Device Power Off
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=off	OUTLET=AA6,AB6,AC6

Test Power On Multiple Outlets Merged Device
	CLI:Enter Path	/access/${DEVICE_MERGED}
	CLI:Device Power On
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=on	OUTLET=AA6,AB6,AC6

Test Power Off Multiple Outlets Merged Device
	CLI:Enter Path	/access/${DEVICE_MERGED}
	CLI:Device Power Off
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=off	OUTLET=AA6,AB6,AC6

Test Power Cycle Multiple Outlets Merged Device
	CLI:Enter Path	/access/${DEVICE_MERGED}
	CLI:Device Power Cycle
	Wait Until Keyword Succeeds	60s	5s	CLI:Check Command All Outlets	DEVICE_NAME=${DEVICE_NAME}	VALUE=on	OUTLET=AA6,AB6,AC6

*** Keywords ***
SUITE:Setup
	Run Keyword If	'${NGVERSION}' == '3.2'	Set Tags	NON-CRITICAL	#implementation is different in nightly/official
	CLI:Open
	${HAVE_PDU_INFO}=	Run Keyword And Return Status	Variable Should Exist	${PDU_SERVERTECH}
	Run Keyword If	${HAVE_PDU_INFO} == ${FALSE}	Skip	Missing PDU data required to create the PDU device
	CLI:Delete All Devices
	CLI:Clone Type	pdu_servertech	${CLONE_NAME}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Enter Path	/settings/types/
	${STATUS}=	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Check If Exists	${CLONE_PDU}
	Run Keyword If	'${NGVERSION}' >= '4.2' and ${STATUS} == False	CLI:Clone Type	pdu_servertech	${CLONE_PDU}	ssh_options=${SSH_OPTIONS}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Add Device	${DEVICE_NAME}	${DEVICE}[type]	${DEVICE}[ip]	${DEVICE}[username]	${DEVICE}[password]	enabled
	...	ELSE	CLI:Add Device	${DEVICE_NAME}	${CLONE_PDU}	${DEVICE}[ip]	${DEVICE}[username]	${DEVICE}[password]	enabled
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/management/
	CLI:Set	snmp=yes snmp_version=v2 snmp_commmunity=private
	CLI:Commit
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/commands/outlet/
	CLI:Set	protocol=snmp
	CLI:Commit
	CLI:Discover Now	${DEVICE_NAME}	pdu
	${RUN_TEST}=	Run Keyword And Return Status	Wait Until Keyword Succeeds	150s	5s	CLI:Outlet Status	DEVICE=${DEVICE_NAME}	OUTLET=AA6,AB6,AC6
	Run Keyword If	${RUN_TEST} == ${FALSE}	Skip	Could not get outlets status
	CLI:Add Device	${DEVICE_MERGED}	device_console	127.0.0.1	root	${ROOT_PASSWORD}	enabled
	SUITE:Merge One Outlet To Device

SUITE:Teardown
	CLI:Delete All Devices
	CLI:Enter Path  /settings/types
	CLI:Delete If Exists Confirm	${CLONE_NAME}
	CLI:Close Connection

SUITE:Merge One Outlet To Device
	${OUTPUT}=	Wait Until Keyword Succeeds	60s	3s	SUITE:Show Outlets
	${LIST}=	Create List	AA6	TowerA_InfeedA_Outlet6
	CLI:Should Contain All	${OUTPUT}	${LIST}
	IF	${NGVERSION} >= 4.2
		${FULLNAME_OUTLET}=	Set Variable	DUMMY_SERVERTECH:A:AA6-TowerA_InfeedA_Outlet6
	ELSE
		${FULLNAME_OUTLET}=	Set Variable	DUMMY_SERVERTECH:A:AA6
	END
	CLI:Enter Path	/settings/devices/${DEVICE_MERGED}/commands
	CLI:Add
	CLI:Set	command=outlet outlet=${FULLNAME_OUTLET} enabled=yes
	CLI:Commit
	${OUTPUT}=	CLI:Show
	Should Match Regexp	${OUTPUT}	\\s+outlet\\s+enabled
	[Teardown]	CLI:Cancel	Raw

SUITE:Merge Outlets To Device
	${OUTPUT}=	Wait Until Keyword Succeeds	60s	3s	SUITE:Show Outlets
	${LIST}=	Create List	AA6	AB6	AC6	TowerA_InfeedA_Outlet6	TowerA_InfeedB_Outlet6	TowerA_InfeedB_Outlet6
	CLI:Should Contain All	${OUTPUT}	${LIST}
	IF	${NGVERSION} >= 4.2
		${FULLNAME_OUTLETS}=	Set Variable	DUMMY_SERVERTECH:A:AA6-TowerA_InfeedA_Outlet6,DUMMY_SERVERTECH:A:AB6-TowerA_InfeedB_Outlet6,DUMMY_SERVERTECH:A:AC6-TowerA_InfeedC_Outlet6
	ELSE
		${FULLNAME_OUTLETS}=	Set Variable	DUMMY_SERVERTECH:A:AA6,DUMMY_SERVERTECH:A:AB6,DUMMY_SERVERTECH:A:AC6
	END
	CLI:Enter Path	/settings/devices/${DEVICE_MERGED}/commands
	CLI:Add
	CLI:Set	command=outlet outlet=${FULLNAME_OUTLETS} enabled=yes
	CLI:Commit
	${OUTPUT}=	CLI:Show
	Should Match Regexp	${OUTPUT}	\\s+outlet\\s+enabled
	[Teardown]	CLI:Cancel	Raw

SUITE:Show Outlets
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/outlets/
	${OUTPUT}=	CLI:Write	show	user=Yes	lines=No
	[Return]	${OUTPUT}