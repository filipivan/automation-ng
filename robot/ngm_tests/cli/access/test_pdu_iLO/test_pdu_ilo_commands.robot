*** Settings ***
Resource	../../init.robot
Documentation	Tests Nodegrid iLO service commands
Metadata	Version	1.0
Force Tags	CLI	DEPENDENCE_DEVICE

Suite Setup  SUITE:Setup
Suite Teardown  SUITE:Teardown

*** Test Cases ***
Test Command iLO sp_console
    Set Tags	SSH
	CLI:Enter Path  /access/${ILO_NAME}
	Write  sp_console
    Read Until	</>hpiLO->
	Write	exit
	
*** Keywords ***
SUITE:Setup
	Run Keyword Unless	${EXECUTE_ILO_TESTS}	Skip	Ilo tests are disabled
	CLI:Open  timeout=240s
	CLI:Delete Devices	${ILO_NAME}
	CLI:Add Device  ${ILO_NAME}  ilo  ${ILO_IP}  ${ILO_USERNAME}  ${ILO_PASSWORD}  on-demand
	Sleep	2s

SUITE:Teardown
	Run Keyword Unless	${EXECUTE_ILO_TESTS}	Skip	Ilo tests are disabled
	CLI:Delete Devices	${ILO_NAME}
	CLI:Close Connection