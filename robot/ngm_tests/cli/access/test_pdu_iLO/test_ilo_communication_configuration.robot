*** Settings ***
Resource	../../init.robot
Documentation	Tests Nodegrid communication with ILO service processor, like power commands and connect
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	DEPENDENCE_DEVICE

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test Commands With SSH
	Set Tags	SSH
	Run Keyword Unless	${EXECUTE_ILO_TESTS}	Skip	Ilo tests are disabled
	CLI:Enter Path	/settings/devices/${ILO_NAME}/access
	CLI:Write	set type=ilo
	CLI:Commit
	CLI:Test power_status command	${ILO_NAME}
	CLI:Test connect command	${ILO_NAME}	${ILO_AWAITED_STRING_ON_CONNECT_CMD}

Test Commands With IPMI_2.0
	Set Tags	IPMI_2.0
	Run Keyword Unless	${EXECUTE_ILO_TESTS}	Skip	Ilo tests are disabled
	CLI:Enter Path	/settings/devices/${ILO_NAME}/access
	CLI:Write	set type=ipmi_2.0
	CLI:Commit
	CLI:Test power_status command	${ILO_NAME}
	CLI:Test connect command	${ILO_NAME}	${ILO_AWAITED_STRING_ON_CONNECT_CMD}

*** Keywords ***
SUITE:Setup
	Run Keyword Unless	${EXECUTE_ILO_TESTS}	Skip	Ilo tests are disabled
	CLI:Open	timeout=240s
	CLI:Delete Devices	${ILO_NAME}
	CLI:Add Device	${ILO_NAME}	ilo	${ILO_IP}	${ILO_USERNAME}	${ILO_PASSWORD}	on-demand
	Sleep	2s

SUITE:Teardown
	Run Keyword Unless	${EXECUTE_ILO_TESTS}	Skip	Ilo tests are disabled
	CLI:Delete Devices	${ILO_NAME}
	CLI:Close Connection
	#To guarantees that the ilo device deletion works in case of the disconnect
	#from ilo fails we disconnect the ssh session to nodegrid and connect again
	#this way the device will certainly be deleted
	CLI:Open
	CLI:Delete Devices	${ILO_NAME}
	CLI:Close Connection