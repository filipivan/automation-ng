*** Settings ***
Resource	../init.robot
Documentation	Tests Nodegrid communication with IDRAC service processor, like power commands and connect
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	DEPENDENCE_DEVICE

Suite Setup	Delete And Create Idrac Device
Suite Teardown	Delete Device And Close Connection

*** Test Cases ***
Test Connect With SSH
	Set Tags	SSH
	Run Keyword Unless	${EXECUTE_IDRAC_TESTS}	Skip	Idrac tests are disabled
	CLI:Enter Path	/settings/devices/${IDRAC_NAME}/access
	CLI:Write	set type=idrac6
	CLI:Commit
	CLI:Test connect command	${IDRAC_NAME}	${IDRAC_AWAITED_STRING_ON_CONNECT_CMD}

Test Commands With SSH
	Set Tags	SSH
	Run Keyword Unless	${EXECUTE_IDRAC_TESTS}	Skip	Idrac tests are disabled
	CLI:Enter Path	/settings/devices/${IDRAC_NAME}/access
	CLI:Write	set type=idrac6
	CLI:Commit
	CLI:Test power_status command	${IDRAC_NAME}
	CLI:Test connect command	${IDRAC_NAME}	${IDRAC_AWAITED_STRING_ON_CONNECT_CMD}

Test Commands With IPMI_2.0
	Set Tags	IPMI_2.0
	Run Keyword Unless	${EXECUTE_IDRAC_TESTS}	Skip	Idrac tests are disabled
	CLI:Enter Path	/settings/devices/${IDRAC_NAME}/access
	CLI:Write	set type=ipmi_2.0
	CLI:Commit
	CLI:Test power_status command	${IDRAC_NAME}
	CLI:Test connect command	${IDRAC_NAME}	${IDRAC_AWAITED_STRING_ON_CONNECT_CMD}

*** Keywords ***
Delete And Create Idrac Device
	Run Keyword Unless	${EXECUTE_IDRAC_TESTS}	Skip	Idrac tests are disabled
	CLI:Open	timeout=240s
	CLI:Delete Devices	${IDRAC_NAME}
	CLI:Add Device	${IDRAC_NAME}	idrac6	${IDRAC_IP}	${IDRAC_USERNAME}	${IDRAC_PASSWORD}	on-demand
	Sleep	2s

Delete Device And Close Connection
	Run Keyword Unless	${EXECUTE_IDRAC_TESTS}	Skip	Idrac tests are disabled
	CLI:Delete Devices	${IDRAC_NAME}
	CLI:Close Connection
	#To guarantees that the idrac device deletion works in case of the disconnect
	#from idrac fails we disconnect the ssh session to nodegrid and connect again
	#this way the device will certainly be deleted
	CLI:Open
	CLI:Delete Devices	${IDRAC_NAME}
	CLI:Close Connection
