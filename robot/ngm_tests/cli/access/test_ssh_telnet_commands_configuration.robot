*** Settings ***
Resource	    ../init.robot
Documentation	This is the configuration tests for add by CLI Console-Like Command SSH/Telnet
Metadata	    Version	1.0
Metadata	    Executed At	${HOST}
Force Tags	    CLI   EXCLUDEIN3_2   EXCLUDEIN4_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DEVICE_NAME}      test-device
${DEVICE_TYPE}      device_console
@{DEVICE}           ${DEVICE_NAME}

*** Test Cases ***
Test SSH Configuration
    SUITE:Add SSH Command
    SUITE:SSH Command Should Be Available

Test Telnet Configuration
    SUITE:Add Telnet Command
    SUITE:SSH Telnet Should Be Available

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Delete Devices  @{DEVICE}
    CLI:Add Device  ${DEVICE_NAME}  ${DEVICE_TYPE}  ${HOSTPEER}
    ...   ${DEFAULT_USERNAME}  ${DEFAULT_PASSWORD}

SUITE:Teardown
	CLI:Delete Devices  @{DEVICE}
	CLI:Close Connection

SUITE:Add SSH Command
    CLI:Enter Path  /settings/devices/${DEVICE_NAME}/commands/
    CLI:Add
    CLI:Set         port_number=22 command=ssh
    CLI:Commit
    
SUITE:SSH Command Should Be Available
    CLI:Enter Path  /settings/devices/${DEVICE_NAME}/commands/
    CLI:Test Show Command Regexp  ssh\\s+enabled
    CLI:Enter Path  /access/${DEVICE_NAME}
    CLI:Test Available Commands  ssh

SUITE:Add Telnet Command
    CLI:Enter Path  /settings/devices/${DEVICE_NAME}/commands/
    CLI:Add
    CLI:Set         port_number=23 command=telnet
    CLI:Commit

SUITE:SSH Telnet Should Be Available
    CLI:Enter Path  /settings/devices/${DEVICE_NAME}/commands/
    CLI:Test Show Command Regexp  telnet\\s+enabled
    CLI:Enter Path  /access/${DEVICE_NAME}
    CLI:Test Available Commands  telnet


