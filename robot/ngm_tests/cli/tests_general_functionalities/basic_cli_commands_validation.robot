*** Settings ***
Resource	../init.robot
Documentation	Tests NG CLI basic commands
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	SHOW	SESSION

Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Test Cases ***
Test Tab Tab Command
	${OUTPUT}=	CLI:Write Bare	\t\t
	${ret}=	CLI:Has Dual Power or Fans
	@{opts}=	Create List	apply_settings	change_password	delete	event_system_audit	exit
	...	hostname	pwd	add	cd	commit	event_system_clear	factory_settings
	...	ls	quit	whoami	reboot	shutdown	show	set	shell	revert	software_upgrade
	...	system_config_check	system_certificate	save_settings	test_email
	Run Keyword If	'${NGVERSION}' >= '4.1'	Remove Values From List	${opts}	test_email
	Run Keyword If	'${NGVERSION}' == '3.2'	Append to List	${opts}	show_settings
	Run Keyword If	'${NGVERSION}' == '3.2'	Append to List	${opts}	drop_session
	Run Keyword If	${ret}	Append To List	${opts}	acknowledge_alarm_state
	CLI:Should Contain All	${output}	${opts}

Test add global settings
	CLI:Enter Path	/
	${output}=	CLI:Write Bare	add /settings/\t\t
	# remove add \settings by sending backspaces
	CLI:Write	\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03
	@{dir_list}=	Create List	authentication	auto_discovery	devices	dial_up	ipv4_firewall	license
	...	network_connections	ssl_vpn	authorization	dhcp_server	hosts	ipv6_firewall	local_accounts	snmp
	...	static_routes
	Run Keyword If	'${NGVERSION}'>='4.2'	Append To List	${dir_list}	custom_fields
	CLI:Should Contain All	${output}	${dir_list}

Test pwd and cd Command
	CLI:Enter Path	/
	${OUTPUT}=	CLI:Write	pwd
	Should Contain	${OUTPUT}	/

	CLI:Enter Path	/system/
	${OUTPUT}=	CLI:Write	pwd
	Should Contain	${OUTPUT}	/system

	CLI:Enter Path	/settings/system_preferences/
	${OUTPUT}=	CLI:Write	pwd
	Should Contain	${OUTPUT}	/settings/system_preferences

	CLI:Enter Path	/settings/authentication/
	${OUTPUT}=	CLI:Write	pwd
	Should Contain	${OUTPUT}	/settings/authentication

	CLI:Enter Path	/system/network_statistics/
	${OUTPUT}=	CLI:Write	pwd
	Should Contain	${OUTPUT}	/system/network_statistics

	CLI:Enter Path	/system/discovery_logs/
	${OUTPUT}=	CLI:Write	pwd
	Should Contain	${OUTPUT}	/system/discovery_logs

	CLI:Enter Path	/system/system_usage
	${OUTPUT}=	CLI:Write	pwd
	Should Contain	${OUTPUT}	/system/system_usage

	CLI:Enter Path	/access/
	${OUTPUT}=	CLI:Write	pwd
	Should Contain	${OUTPUT}	/access

Test if '/' is added with autocomplete after '.' or '..' using cd command
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
	${SLASH_PRESENT}		CLI:Write Bare 		cd .\t\n
	Should Contain  	${SLASH_PRESENT}	cd .\x07	#\x07 is the "/" in unicode
	${SLASH_PRESENT}		CLI:Write Bare 		cd ..\t\n
	Should Contain  	${SLASH_PRESENT}	cd ..\x07	##\x07 is the "/" in unicode

Test if command 'cd' with no arguments brings the user back to '/'
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
	CLI:Enter Path  		/settings/ipv4_firewall/chains/INPUT/
	${BACK_HOME}			CLI:Enter Path  				 		${EMPTY}
	Log To Console 			${BACK_HOME}
	Should Be Equal  		${BACK_HOME}			 				[admin@nodegrid /]#

Test returning to the previous path using cd command
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
	CLI:Enter Path 			/settings/ipv4_firewall/chains/
	CLI:Enter Path  		/system/toolkit
	${PREVIOUS_PATH}		CLI:Write  								cd -
	Should Be Equal  	${PREVIOUS_PATH}						[admin@nodegrid chains]#

Check if warning is displayed if the user press <TAB> after 'cd -' command
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
	${WARNING}				CLI:Write Bare  			cd -\t
	Should Contain			${WARNING}					Warning: Press ENTER to process command
	CLI:Write  				${SPACE}					#just to escape the warning

Test ls Command
	CLI:Enter Path	/
	CLI:Test Ls Command	access/	system/	settings/\n

	CLI:Enter Path	/system
	${OUTPUT}=	CLI:Ls
	@{list}=	Create List	toolkit/	open_sessions/	device_sessions/	event_list/	routing_table/	system_usage/
	...	discovery_logs/	lldp/	network_statistics	usb_devices	about

	${HAS_SERIAL}=	CLI:Has Serial Ports Support
	${DEVICES}	CLI:Show	/settings/devices
	${HAS_TTY}	Run Keyword And Return Status	Should Contain	${DEVICES}	ttyS
	Run Keyword If	${HAS_SERIAL} and ${HAS_TTY}	Append To List	${list}	serial_statistics	serial_ports_summary
	CLI:Should Contain All	${output}	${list}
	Run Keyword If	'${HAS_SERIAL}' == '${FALSE}'	Should Not Contain	${OUTPUT}	serial_statistics	serial_ports_summary

	CLI:Enter Path	/settings/authentication/
	CLI:Test Ls Command	servers/	console/

	CLI:Enter Path	/system/network_statistics/
	Run Keyword If	'${NGVERSION}' > '3.2'	CLI:Test Ls Command	eth0/

	CLI:Enter Path	/system/system_usage
	CLI:Test Ls Command	memory_usage/	cpu_usage/	disk_usage/

Test whoami Command
	${USER_WHOAMI}=	CLI:Get User Whoami
	${USER_PROMPT}=	CLI:Get User Prompt
	Should Be Equal	${USER_WHOAMI}	${USER_PROMPT}

Test show and hostname command
	${RETURN}=	CLI:Write	show /settings/network_settings/
	${RETURN}=	Remove String Using Regexp	${RETURN}	['\r']
	@{LINES}=	Split To Lines	${RETURN}	0	1
	${SYSTEMNAME}=	Set Variable	${EMPTY}
	FOR	${LINE}	IN	@{LINES}
		@{Values}=	Split String	${LINE}	=
		${Value}=	Get From List	${Values}	1
		${SYSTEMNAME}=	Strip String	${Value}
	END
	${OUTPUT}=	CLI:Write	hostname
	Should Contain	${OUTPUT}	${SYSTEMNAME}

Test shell Command
	Write	shell
	${RETURN}=	Read Until	:~$
	${Banner1}=	Set Variable	WARNING: Improper use of shell commands could lead to data loss,
	${Banner2}=	Set Variable	the deletion of important system files or other unexpected result.
	${Banner3}=	Set Variable	Please double-check your syntax when typing shell commands
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${RETURN}	${Banner1}
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${RETURN}	${Banner2}
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${RETURN}	${Banner3}
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${RETURN}	WARNING: Use of shell commands implies advanced knowledge of the product and operating system.
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${RETURN}	Ensure you know how the commands and scripts affect the system before executing them.
	CLI:Write	exit

Test shell exit Command
	Write	shell
	${RETURN}=	Read Until	:~$
	${Banner1}=	Set Variable	WARNING: Improper use of shell commands could lead to data loss,
	${Banner2}=	Set Variable	the deletion of important system files or other unexpected result.
	${Banner3}=	Set Variable	Please double-check your syntax when typing shell commands
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${RETURN}	${Banner1}
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${RETURN}	${Banner2}
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain	${RETURN}	${Banner3}
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${RETURN}	WARNING: Use of shell commands implies advanced knowledge of the product and operating system.
	Run Keyword If	'${NGVERSION}' == '3.2'	Should Contain	${RETURN}	Ensure you know how the commands and scripts affect the system before executing them.
	${RETURN}=	CLI:Write	exit
	Should Contain	${RETURN}	logout

Test exit Command
	Write	exit
	${output}=	Read	delay=1s
	${error}=	Set Variable	OSError: Socket is closed
	Run Keyword And Expect Error	${error}	Write	pwd

Test quit Command
	CLI:Open	startping=No
	Write	quit
	${output}=	Read	delay=1s
	${error}=	Set Variable	OSError: Socket is closed
	Run Keyword And Expect Error	${error}	Write	pwd

Test change password
	CLI:Open	startping=No
	CLI:Change Password	${DEFAULT_PASSWORD}	a_long_password
	[Teardown]	CLI:Change User Password From Shell	${DEFAULT_USERNAME}	${DEFAULT_PASSWORD}

Test event_system_clear/event_system_audit command
	CLI:Open	startping=No

	#To have at least one event logged
	CLI:Open	session_alias=event
	Close Connection
	CLI:Switch Connection	default

	Write	event_system_audit
	${OUTPUT}=	Read Until	Quit)
	Write	q
	Read Until Prompt
	Write	event_system_clear
	Read Until Prompt
	Write	event_system_audit
	${OUTPUT_CLEAR}=	Read Until	Quit)
	Write	q
	Read Until Prompt

	${OUTPUT}=	Replace String	${OUTPUT}	\r\n	${empty}
	${OUTPUT_CLEAR}=	Replace String	${OUTPUT_CLEAR}	\r\n	${empty}
	Should Not Be Equal	${OUTPUT}	${OUTPUT_CLEAR}

Check if autocompletion works in fields with dots
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0
	CLI:Enter Path	/settings/authentication/sso/
	CLI:Add
	Write Bare	set x.50\t\n
	${AUTOCOMPLETED}	Read Until	set x.509_certificate
	Should Contain	${AUTOCOMPLETED}	set x.509_certificate
	CLI:Cancel	Raw
	CLI:Enter Path	/settings/devices/
	CLI:Add
	Write Bare	set type=ipmi_1.\t\n
	${AUTOCOMPLETED}	Read Until	set type=ipmi_1.5
	Should Contain	${AUTOCOMPLETED}	set type=ipmi_1.5
	CLI:Cancel	Raw
	[Teardown]	CLI:Cancel	Raw
#Can be implemented
#system_config_check
#shutdown
#save_settings
#show_settings
#reboot

#Need to be later implemented
#acknowledge_alarm_state
#commit
#system_certificate
#factory_settings
#apply_settings
#drop_session
#revert
#event_system_audit
#event_system_clear
#software_upgrade
#test_email