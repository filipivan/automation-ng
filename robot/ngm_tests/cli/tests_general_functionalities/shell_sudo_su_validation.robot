*** Settings ***
Resource	../init.robot
Documentation	Test shell sudo su commands thru CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	SHELL

Suite Setup	CLI:Open
Suite Teardown	CLI:Close Connection

*** Variables ***

*** Test Cases ***
#shell and sudo su - command - commad privileges within Linux where you can type linux commands line user interface

#[Documentation] Test shell with sudo su - commands thru CLI
 #	...	== REQUIREMENTS ==
 #	...	SSH session is open and a CLI prompt is available and shell command is vailable
 #	...	== ARGUMENTS ==
 #	...	None
 #	...	==Steps==
 #	...	CLI: shell sudo su -	and exit or
 #	...	CLI: shell, type sudo su - and exit at the prompt
 #	...	== EXPECTED RESULT ==
 #	...	shell prompt with sudo privileges that allows access to other linux commands as a different user.
 #	...

Test shell with sudo su - command
	Write	shell
	${RETURN}=	Read Until	:~$
	Run Keyword If	'${NGVERSION}' >= '4.0'	Should Contain	${RETURN}	WARNING: Improper use of shell commands could lead to data loss,
	Run Keyword If	'${NGVERSION}' >= '4.0'	Should Contain	${RETURN}	the deletion of important system files or other unexpected result.
	Run Keyword If	'${NGVERSION}' >= '4.0'	Should Contain	${RETURN}	Please double-check your syntax when typing shell commands
	Run Keyword If	'${NGVERSION}' < '4.0'	Should Contain	${RETURN}	WARNING: Use of shell commands implies advanced knowledge of the product and operating system.
	Run Keyword If	'${NGVERSION}' < '4.0'	Should Contain	${RETURN}	Ensure you know how the commands and scripts affect the system before executing them.
	write	sudo su -
	${RETURN}=	Read Until	:~#
	Write Bare	ps -ef|grep elast*\n
	${RETURN}=	Read Until	:~#
	Log To Console	'${RETURN}'
	Write	exit
	${RETURN}=	Read Until	:~$
	Should Contain	${RETURN}	logout
	Write	exit
	${RETURN}=	Read Until Prompt
	Should Contain	${RETURN}	logout
	Write	exit

Test shell sudo su -
	[Timeout]	1 minute
	CLI:Open	startping=no
	Write	shell sudo su -
	${RETURN}=	Read Until	:~#
	Write Bare	ps -ef|grep sshd\n
	${OUTPUT}=	Read Until	:~#
	Log To Console	'${OUTPUT}'
	write	exit
	${OUTPUT}=	Read Until Prompt
	Should Contain	${OUTPUT}	logout
	Write Bare	ls\n
	${OUTPUT}=	Read Until Prompt
	Should Not Contain	${OUTPUT}	?????????????????
