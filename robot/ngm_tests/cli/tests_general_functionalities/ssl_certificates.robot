*** Settings ***
Resource	../init.robot
Documentation	Tests SSL certificates through CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4
Default Tags	CLI	SSH	SHOW	SSL

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
*** Variables ***
${CUSTOM_NAME}	dummy_cert
${VALID_CERT}	dag
${FTP_USER}	${FTPSERVER2_USER}
${FTP_PASS}	${FTPSERVER2_PASSWORD}
${FTP_IP}	${FTPSERVER2_IP}

*** Test Cases ***
Test check default config for certificates
	CLI:Switch Connection	root_session
	${DEFAULT_DIRS}	CLI:Write	ls /usr/share/ca-certificates/
	Should contain	${DEFAULT_DIRS}	mozilla
	${DEFAULT_CERTS}	CLI:Write	ls -l --show-control-chars /usr/share/ca-certificates/mozilla
	Should Not Be Empty	${DEFAULT_CERTS}
	Should Not Contain	${DEFAULT_CERTS}	total 0
	@{CERTS}	Split To Lines	${DEFAULT_CERTS}
	@{SYSTEM_CERTS}	Create List
	FOR	${CERT}	IN	@{CERTS}
		${IS_CERT}	Run Keyword And Return Status	Should Contain	${CERT}	.crt
		IF	${IS_CERT}
			IF	'${NGVERSION}' > '5.8'
				${ONLY_CERT}	Remove String Using Regexp	${CERT}	.*root\\sroot\\s+\\d{0,4}\\s+Apr\\s+5\\s+2011\\s
				Append To List	${SYSTEM_CERTS}	${ONLY_CERT}
			ELSE
				${ONLY_CERT}	Remove String Using Regexp	${CERT}	.*root\\sroot\\s+\\d{0,4}\\s+Mar\\s+9\\s+2018\\s
				Append To List	${SYSTEM_CERTS}	${ONLY_CERT}
			END
		END
	END
	Set Suite Variable	@{SYSTEM_CERTS}

Test check default certificates configured in ca-certificates.conf
	CLI:Switch Connection	root_session
	${DEFAULT_CONFIG}	CLI:Write	cat /etc/ca-certificates.conf | grep mozilla
	FOR	${CERT}	IN	@{SYSTEM_CERTS}
		Should Contain	${DEFAULT_CONFIG}	${CERT}
	END

Test check SSL certificates
	CLI:Switch Connection	root_session
	${SSL_CERTS}	CLI:Write	ls -l --show-control-chars /etc/ssl/certs
	FOR	${CERT}	IN	@{SYSTEM_CERTS}
		Should Contain	${SSL_CERTS}	${CERT}
	END

Test update-ca-certificates does not change default certs
	CLI:Switch Connection	root_session
	${OUTPUT}	CLI:Write	update-ca-certificates
	Should Contain	${OUTPUT}	0 added, 0 removed; done.
	${SSL_CERTS}	CLI:Write	ls -l --show-control-chars /etc/ssl/certs
	FOR	${CERT}	IN	@{SYSTEM_CERTS}
		Should Contain	${SSL_CERTS}	${CERT}
	END

Test adding invalid certificate does not update /etc/ssl/certs
	CLI:Switch Connection	root_session
	CLI:Write	touch /usr/local/share/ca-certificates/${CUSTOM_NAME}.pem
	${OUTPUT}	CLI:Write	update-ca-certificates
	Should Contain	${OUTPUT}	0 added, 0 removed; done.
	[Teardown]	CLI:Write	rm /usr/local/share/ca-certificates/${CUSTOM_NAME}.pem	Raw

Test adding dummy certificate only update symbolic link in /etc/ssl/certs
	CLI:Switch Connection	root_session
	CLI:Write	touch /usr/local/share/ca-certificates/${CUSTOM_NAME}.crt
	${OUTPUT}	CLI:Write	update-ca-certificates
	Should Contain	${OUTPUT}	1 added, 0 removed; done.
	Should Contain	${OUTPUT}	rehash: warning: skipping ${CUSTOM_NAME}.pem,it does not contain exactly one certificate or CRL
	${SSL_CERTS}	CLI:Write	ls -l /etc/ssl/certs | grep ${CUSTOM_NAME}
	Should Contain	${SSL_CERTS}	${CUSTOM_NAME}.pem -> ../../../usr/local/share/ca-certificates/${CUSTOM_NAME}.crt
	CLI:Write	rm /etc/ssl/certs/${CUSTOM_NAME}.pem
	[Teardown]	CLI:Write	rm /usr/local/share/ca-certificates/${CUSTOM_NAME}.crt	Raw

Test adding valid certificate updates /etc/ssl/certs
	CLI:Switch Connection	root_session
	Write	scp ${FTP_USER}@${FTP_IP}:/home/ftpuser/ftp/files/${VALID_CERT}.crt /usr/local/share/ca-certificates/
	${RESPONSE}	Read Until Regexp	(fingerprint)|(assword:)
	${ASKED_FINGERPRINT}	Run Keyword And Return Status	Should Contain	${RESPONSE}	fingerprint
	IF	${ASKED_FINGERPRINT}
		Write	yes
		Read Until	assword:
	END
	CLI:Write	${FTP_PASS}
	${OUTPUT}	CLI:Write	update-ca-certificates
	Should Contain	${OUTPUT}	1 added, 0 removed; done.
	Should Contain	${OUTPUT}	rehash: warning: skipping ca-certificates.crt,it does not contain exactly one certificate or CRL
	${SSL_CERTS}	CLI:Write	ls -l /etc/ssl/certs | grep ${VALID_CERT}
	Should Contain	${SSL_CERTS}	${VALID_CERT}.pem -> ../../../usr/local/share/ca-certificates/${VALID_CERT}.crt
	Should Contain	${SSL_CERTS}	.0 -> ${VALID_CERT}.pem
	CLI:Write	rm /etc/ssl/certs/${VALID_CERT}.pem
	[Teardown]	CLI:Write	rm /usr/local/share/ca-certificates/${VALID_CERT}.crt	Raw

Test SSL certs after enabling FIPS
	[Tags]	EXCLUDEIN5_6
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/fips_140
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	enable_fips_140-3
	CLI:Switch Connection	root_session
	${SSL_CERTS}	CLI:Write	ls -l --show-control-chars /etc/ssl/certs
	FOR	${CERT}	IN	@{SYSTEM_CERTS}
		Should Contain	${SSL_CERTS}	${CERT}
	END
	[Teardown]	SUITE:Disable fips

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Connect As Root

SUITE:Teardown
	CLI:Close Connection

SUITE:Disable fips
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/fips_140
	Write	set enable_fips_140-3=no
	${PROMPT}	Read Until Regexp	:|]#
	${ASK_CONFIRMATION}	Run Keyword And Return Status	Should Contain	${PROMPT}	:
	IF	${ASK_CONFIRMATION}
		CLI:Write	yes
	END
	CLI:Commit