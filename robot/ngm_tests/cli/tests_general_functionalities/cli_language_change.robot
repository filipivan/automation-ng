*** Settings ***
Resource	../init.robot
Documentation	Test sending language variable through ssh and telnet
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI		EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN4_1 	EXCLUDEIN4_0   NON-CRITICAL   NEED-REVIEW

Suite Teardown	CLI:Close Connection

*** Variables ***
${JAPANESE} 	ja_JP
${PORTUGUESE} 	pt_BR
${DEFAULT} 	en_US

*** Keywords ***
SUITE:Enable Telnet
	CLI:Open
	CLI:Enter Path 	/settings/services
	CLI:Set Field 	enable_telnet_service_to_nodegrid	yes
	CLI:Commit
	CLI:Test Show Command 	enable_telnet_service_to_nodegrid = yes
	Close Connection

SUITE:Disable Telnet
	CLI:Open
	CLI:Enter Path 	/settings/services
	CLI:Set Field 	enable_telnet_service_to_nodegrid	no
	CLI:Commit
	CLI:Test Show Command 	enable_telnet_service_to_nodegrid = no
	Close Connection

*** Test Cases ***
Test LANG=ja_JP via SSH
	CLI:Connect as Root
	#Gets rid of WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED! msg
	Write	ssh-keygen -f "/home/root/.ssh/known_hosts" -R ${HOST}
	Read Until Prompt

	Write 	LANG=${JAPANESE} ssh ${DEFAULT_USERNAME}@${HOST}

	# Are you sure you want to continue connecting (yes/no)?
	${ST}=	Run Keyword And Return Status	Read Until	(yes/no)?
	Run Keyword If	${ST}	Write	yes
	Run Keyword If	${ST}	Read Until	Password:
	Write 	${DEFAULT_PASSWORD}

	Set Client Configuration	prompt=]#
	${OUTPUT}= 	Read Until Prompt
	Should Contain 	${OUTPUT} 	${DEFAULT_USERNAME}@

	#Checking an error message to see if it is localized
	CLI:Enter Path 	/settings/license
	CLI:Add
	${OUTPUT}= 	CLI:Write 	commit 	Yes
	Should Contain 	${OUTPUT} 	あ_ Error: _蒼ｱlicense_key: あ_Field must not be empty._蒼ｱ
	CLI:Cancel

	# Checking that the commands are still in English
	CLI:Test Available Commands	add	commit	event_system_clear	ls	reboot	shell	shutdown	cd	delete	exit
	...	pwd	revert	show	whoami	change_password	event_system_audit	hostname	quit	set	show_settings

	#Checking that admin locale is in ja_JP
	Write	shell
	${SHELL_OUTPUT}=	Read Until	~$
	Should Contain	${SHELL_OUTPUT}	${DEFAULT_USERNAME}@nodegrid:
	Write 	locale
	${LOCALE}=	Read Until 	~$
	Should Contain 	${LOCALE} 	${JAPANESE}
	Write 	exit
	Read Until  ]#

	Write 	exit
	Set Client Configuration    prompt=#
	Read Until Prompt

	#Checking that the root locale hasn't changed
	Write 	locale
	${OUTPUT}=	Read Until Prompt
	Should Not Contain 	${OUTPUT} 	${JAPANESE}
	Close Connection

Test LANG=ja_JP via Telnet
	SUITE:Enable Telnet

	CLI:Connect as Root
	Write 	/bin/echo DEFAULT > .telnetrc
	Read Until Prompt
	Write 	/bin/echo " environ define LANG ${JAPANESE}" >> .telnetrc

	Write 	 telnet -l ${DEFAULT_USERNAME} ${HOST}
	Read Until 	Password:
	Write 	${DEFAULT_PASSWORD}
	Set Client Configuration	prompt=]#
	${OUTPUT}= 	Read Until Prompt
	Should Contain 	${OUTPUT} 	${DEFAULT_USERNAME}@

	#Checking an error message to see if it is localized
	CLI:Enter Path 	/settings/license
	CLI:Add
	${OUTPUT}= 	CLI:Write 	commit 	Yes
	Should Contain 	${OUTPUT} 	あ_ Error: _蒼ｱlicense_key: あ_Field must not be empty._蒼ｱ
	CLI:Cancel

	# Checking that the commands are still in English
	CLI:Test Available Commands	add	commit	event_system_clear	ls	reboot	shell	shutdown	cd	delete	exit
	...	pwd	revert	show	whoami	change_password	event_system_audit	hostname	quit	set	show_settings


	#Checking that admin locale is in ja_JP
	Write	shell
	${SHELL_OUTPUT}=	Read Until	~$
	Should Contain	${SHELL_OUTPUT}	${DEFAULT_USERNAME}@nodegrid:
	Write 	locale
	${LOCALE}=	Read Until 	~$
	Should Contain 	${LOCALE} 	${JAPANESE}
	Write 	exit
	Read Until  ]#

	Write 	exit
	Set Client Configuration    prompt=#
	Read Until Prompt

	#Checking that the root locale hasn't changed
	Write 	locale
	${OUTPUT}=	Read Until Prompt
	Should Not Contain 	${OUTPUT} 	${JAPANESE}

	Write 	rm .telnetrc
	Read Until Prompt
	Close Connection
	[Teardown]  SUITE:Disable Telnet

#Should default to en_US
Test LANG=pt_BR via SSH
	CLI:Connect as Root
	#Gets rid of WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED! msg
	Write	ssh-keygen -f "/home/root/.ssh/known_hosts" -R ${HOST}
	Read Until Prompt

	Write 	LANG=${PORTUGUESE} ssh ${DEFAULT_USERNAME}@${HOST}

	# Are you sure you want to continue connecting (yes/no)?
	${ST}=	Run Keyword And Return Status	Read Until	(yes/no)?
	Run Keyword If	${ST}	Write	yes
	Run Keyword If	${ST}	Read Until	Password:
	Write 	${DEFAULT_PASSWORD}

	Set Client Configuration	prompt=]#
	${OUTPUT}= 	Read Until Prompt
	Should Contain 	${OUTPUT} 	${DEFAULT_USERNAME}@

	#Checking an error message to see if it is localized
	CLI:Enter Path 	/settings/license
	CLI:Add
	${OUTPUT}= 	CLI:Write 	commit 	Yes
	Should Contain 	${OUTPUT} 	Error: license_key: Field must not be empty.
	CLI:Cancel

	# Checking that the commands are still in English
	CLI:Test Available Commands	add	commit	event_system_clear	ls	reboot	shell	shutdown	cd	delete	exit
	...	pwd	revert	show	whoami	change_password	event_system_audit	hostname	quit	set	show_settings

	#Checking that admin locale is in en_US
	Write	shell
	${SHELL_OUTPUT}=	Read Until	~$
	Should Contain	${SHELL_OUTPUT}	${DEFAULT_USERNAME}@nodegrid:
	Write 	locale
	${LOCALE}=	Read Until 	~$
	Should Contain 	${LOCALE} 	${DEFAULT}
	Write 	exit
	Read Until  ]#

	Write 	exit
	Set Client Configuration    prompt=#
	Read Until Prompt

	#Checking that the root locale hasn't changed
	Write 	locale
	${OUTPUT}=	Read Until Prompt
	Should Not Contain 	${OUTPUT} 	${PORTUGUESE}
	Close Connection

Test LANG=pt_BR via Telnet
	SUITE:Enable Telnet

	CLI:Connect as Root
	Write 	/bin/echo DEFAULT > .telnetrc
	Read Until Prompt
	Write 	/bin/echo " environ define LANG ${PORTUGUESE}" >> .telnetrc

	Write 	 telnet -l ${DEFAULT_USERNAME} ${HOST}
	Read Until 	Password:
	Write 	${DEFAULT_PASSWORD}
	Set Client Configuration	prompt=]#
	${OUTPUT}= 	Read Until Prompt
	Should Contain 	${OUTPUT} 	${DEFAULT_USERNAME}@

	#Checking an error message to see if it is localized
	CLI:Enter Path 	/settings/license
	CLI:Add
	${OUTPUT}= 	CLI:Write 	commit 	Yes
	Should Contain 	${OUTPUT} 	Error: license_key: Field must not be empty.
	CLI:Cancel

	# Checking that the commands are still in English
	CLI:Test Available Commands	add	commit	event_system_clear	ls	reboot	shell	shutdown	cd	delete	exit
	...	pwd	revert	show	whoami	change_password	event_system_audit	hostname	quit	set	show_settings


	#Checking that admin locale is in en_US
	Write	shell
	${SHELL_OUTPUT}=	Read Until	~$
	Should Contain	${SHELL_OUTPUT}		${DEFAULT_USERNAME}@nodegrid:
	Write 	locale
	${LOCALE}=	Read Until 	~$
	Should Contain 	${LOCALE} 	${DEFAULT}
	Write 	exit
	Read Until  ]#

	Write 	exit
	Set Client Configuration    prompt=#
	Read Until Prompt

	#Checking that the root locale hasn't changed
	Write 	locale
	${OUTPUT}=	Read Until Prompt
	Should Not Contain 	${OUTPUT} 	${PORTUGUESE}

	Write 	rm .telnetrc
	Read Until Prompt
	Close Connection
	[Teardown]  SUITE:Disable Telnet