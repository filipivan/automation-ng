*** Settings ***
Resource	../init.robot
Documentation	Tests support commit confirmation with automatic rollback validation ---> CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI 	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN3_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Test Tab Tab Command
#navigate any path and tab tab to check The analogous commands
    CLI:Enter Path	/settings/
	${OUTPUT}=	CLI:Write Bare	\t\t
    Should Contain   ${OUTPUT}   config_confirm    config_revert   config_start
    CLI:Enter Path	/access/
	${OUTPUT}=	CLI:Write Bare	\t\t
    Should Contain   ${OUTPUT}   config_confirm    config_revert   config_start
    CLI:Enter Path	/system/
	${OUTPUT}=	CLI:Write Bare	\t\t
    Should Contain   ${OUTPUT}   config_confirm    config_revert   config_start

Test to validate error message when execute config_confirm before config_start
    CLI:Enter Path  /settings/
    Write    config_confirm
    ${OUTPUT}=     Read Until Prompt
    Should Contain    ${OUTPUT}    Error: There is no active configuration transaction.

Test to validate error message when execute config_revert before config_start
    CLI:Enter Path  /settings/
    Write    config_revert
    ${OUTPUT}=     Read Until Prompt
    Should Contain    ${OUTPUT}    Error: There is no active configuration transaction.

Test validation error messages when execute config_start twice
    CLI:Enter Path  /settings/
    CLI:Start Configuration transaction
    Write    config_start
    ${OUTPUT}=     Read Until Prompt
    Should Contain    ${OUTPUT}  Error: Another configuration transaction is underway.
	[Teardown]	CLI:Confirm Configuration transaction

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	Close Connection