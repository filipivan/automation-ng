*** Settings ***
Resource	../init.robot
Documentation	Tests NG CLI basic commands
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI
Default Tags	CLI	SSH	FOLDER	LS

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
Root Folder Structure
	CLI:Test Ls Command	access/	system/	settings/

Access Folder Structure
	CLI:Enter Path	/access/
	${OUTPUT}=	CLI:Write	ls
	Should Match Regexp	${OUTPUT}	\[A-Za-z0-9-]\\/

Access Device Folder Structure
	CLI:Enter Path	/access/
	Write	ls
	${OUTPUT}=	Read Until Prompt
	${OUTPUT}=	Remove String Using Regexp	${OUTPUT}	['\r']
	@{Folders}=	Split to Lines	${OUTPUT}	0	-3
	FOR	${ELEMENT}	IN	@{Folders}
		Write	ls ${ELEMENT}
		${DEVICE}=	Read Until Prompt
		${DEVICENAME}=	Fetch from Left	${ELEMENT}	/
		Should Contain	${DEVICE}	${DEVICENAME}
		Run Keyword if	'${NGVERSION}' < '4.0'	Should Contain	${DEVICE}	Try show command instead...
	END

System Folder Structure
	CLI:Enter Path	/system/
	${systemdescription}=	CLI:Get System Description
	${systemdescription}=	Get Substring	${systemdescription}	9	16
	${OUTPUT}=	CLI:Write	ls
	${opts}=	Create List	toolkit/	open_sessions/	device_sessions/	event_list/	routing_table/	system_usage/	discovery_logs/
	...	lldp/	network_statistics/	usb_devices/	about/
	${SERIAL_PORT}=	Run Keyword And Return Status	SUITE:Check Serial Ports Card
	Run Keyword If	'${systemdescription}' == 'Nodegrid Services Router' and ${SERIAL_PORT} == ${TRUE}	Append To List	${opts}	serial_statistics/	serial_ports_summary/
	...	Else If	'${systemdescription}' != 'Manager'	Append To List	${opts}	serial_statistics/	serial_ports_summary/
	CLI:Should Contain All	${OUTPUT}	${opts}

Settings Folder Structure
	CLI:Enter Path	/settings
	${OUTPUT}=	CLI:Write	ls
	@{opts}=	Create List	license/	system_preferences/	custom_fields/	system_logging/	date_and_time/
	...	dial_up/	devices/	types/	auto_discovery/	power_menu/	devices_session_preferences/
	...	network_settings/	network_connections/	static_routes/	hosts/	snmp/	dhcp_server/	local_accounts/
	...	password_rules/	authorization/	authentication/	ipv4_firewall/	ipv6_firewall/	ssl_vpn/	services/	auditing/
	Run Keyword If	'${NGVERSION}' < '4.0'	Append To List	${opts}	cloud_management/	cloud/
	Run Keyword If	'${NGVERSION}' == '4.0'	Append To List	${opts}	cloud/
	Run Keyword If	'${NGVERSION}' > '4.0'	Append To List	${opts}	cluster/

	CLI:Should Contain All	${OUTPUT}	${opts}

*** Keywords ***
SUITE:Setup
	CLI:Open

SUITE:Teardown
	CLI:Close Connection

SUITE:Check Serial Ports Card
	CLI:Enter Path	/settings/slots/
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	Serial Rolled Expansion Card