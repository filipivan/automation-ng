*** Settings ***
Resource	../init.robot
Documentation	Tests fips through CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN5_6
Default Tags	CLI	SSH	SHOW	FIPS

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${LICENSE_KEY}	${FIFTY_DEVICES_ACCESS_LICENSE}
${DEVICE_NAME}	test_device
${DEVICE_TYPE}	device_console
${MODE}		on-demand
${USR}	admin
${PASSWD}	admin

*** Test Cases ***
Test case to add device and check check able to connect test and target device
	CLI:Add Device	${DEVICE_NAME}	${DEVICE_TYPE}	${HOSTPEER}    ${USR}  ${PASSWD}   ${MODE}
	SUITE:check target and test devices able to connect

Test case to check open ssl providers before and after fips enabled
	SUITE:Navigate path for FIPS
	SUITE:show open ssl providers
	SUITE:Enable FIPS
	SUITE:show open ssl providers after enabling fips

Test case to check able to connect to test and target devices on enabling fips
	SUITE:check target and test devices able to connect
	CLI:Switch Connection	default

Test case to check add/remove license key on enabling fips
	SUITE:Check fips enabled
	SUITE:Add License key
	SUITE:Remove License key

Test case to disable fips and check ssh for devices
	SUITE:Disable fips
	CLI:Open
	CLI:Connect As Root
	CLI:Switch Connection	default
	CLI:Open	session_alias=PEER	HOST_DIFF=${HOSTPEER}

Test case to delete Added configurations
	CLI:Enter Path	/settings/devices/
	CLI:Delete If Exists Confirm	${DEVICE_NAME}

*** Keywords ***
SUITE:Setup
	CLI:Open
	CLI:Connect As Root
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/devices/
	CLI:Delete If Exists Confirm	${DEVICE_NAME}

SUITE:check target and test devices able to connect
	CLI:Open	session_alias=PEER	HOST_DIFF=${HOSTPEER}
	CLI:Open

SUITE:Navigate path for FIPS
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/fips_140
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	enable_fips_140-3
	Should Contain	${OUTPUT}	Enabling or disabling FIPS 140-3 requires the system to be rebooted for all changes to take effect.


SUITE:show open ssl providers
	CLI:Switch Connection	root_session
	${show_providers}=  CLI:Write	openssl list -providers
	Should Contain	${show_providers}	OpenSSL Default Provider
	Should Contain	${show_providers}	OpenSSL Legacy Provider

SUITE:Enable FIPS
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/fips_140
	CLI:Write	set enable_fips_140-3=yes
	CLI:Commit
	${OUTPUT}	CLI:Show
	Should Contain	${OUTPUT}	enable_fips_140-3 = yes
	Should Contain	${OUTPUT}	Enabling or disabling FIPS 140-3 requires the system to be rebooted for all changes to take effect.
	Write	reboot
	Read until	 (yes, no)
	write	yes
	Sleep	300s
	CLI:Open
	CLI:Connect As Root

SUITE:Disable fips
	CLI:Enter Path	/settings/fips_140
	CLI:Write	set enable_fips_140-3=no
	CLI:Commit
	${OUTPUT}	CLI:Show
	Should Contain	${OUTPUT}	enable_fips_140-3 = no
	Should Contain	${OUTPUT}	Enabling or disabling FIPS 140-3 requires the system to be rebooted for all changes to take effect.
	Write	reboot
	Read until	 (yes, no)
	write	yes
	Sleep	300s

SUITE:Teardown
	CLI:Close Connection

SUITE:show open ssl providers after enabling fips
	CLI:Open
	CLI:Connect As Root
	CLI:Switch Connection	root_session
	${show_providers}=  CLI:Write	openssl list -providers
	Should Contain	${show_providers}	name: OpenSSL Base Provider
	Should Contain	${show_providers}	name: KeyPair FIPS Provider for OpenSSL 3
	CLI:Close Connection

SUITE:Check fips enabled
	CLI:Switch Connection	default
	CLI:Enter Path	/settings/fips_140
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	enable_fips_140-3 = yes
	Should Contain	${OUTPUT}	Enabling or disabling FIPS 140-3 requires the system to be rebooted for all changes to take effect.


SUITE:Add License key
	CLI:Enter Path	/settings/license
	CLI:Add
	CLI:Set	license_key=${LICENSE_KEY}
	CLI:Commit
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}	xxxxx-xxxxx-xxxxx-BTXIM

SUITE:Remove License key
	CLI:Delete All License Keys Installed