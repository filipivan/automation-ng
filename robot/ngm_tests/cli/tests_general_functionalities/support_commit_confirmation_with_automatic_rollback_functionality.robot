*** Settings ***
Resource	../init.robot
Documentation	Tests support commit confirmation with automatic rollback functionality ----> CLI
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	    EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	EXCLUDEIN3_2
Default Tags	CLI	SSH	SHOW

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${IDLE_TIMEOUT}    300
@{NEW_TIMEOUTS}  500    1000    2000    3000    5000
${LICENSE_KEY}   7X8GD-WDKHF-9ZT9A-7POUR
${NEW_CONN}     new_connection

*** Test Cases ***
Test Configuration transaction operations under system::preference
#check after commit comformation and on revert the configuration is retained
    SUITE:Change idle timeout under system::preference and check config_revert

Test Configuration transaction operations under settings::license
    SUITE:Add license key perform start transaction
    SUITE:check configuration Reverted on config_revert for added license

Test Configuration transaction operations under network connections
    SUITE:Add network connection and start transaction
    SUITE:check configuration Reverted on config_revert for added network connection

Test unconfirmed configurations are automatically reverted on timeout and confirmed remained
    SUITE:Check transaction that is not confirmed automatically reverted on timeout
    SUITE:check transactions confirmed on config_confirm still there on timeout
    SUITE:Delete added configuration
*** Keywords ***
SUITE:Setup
	CLI:Open
	Set Client Configuration	timeout=60s

SUITE:Teardown
	CLI:Open
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Close Connection

SUITE:Change idle timeout under system::preference and check config_revert
    CLI:Enter Path  /settings/system_preferences/
    ${OUTPUT}=  CLI:Show
    Should Contain   ${OUTPUT}  idle_timeout = ${IDLE_TIMEOUT}
    FOR		    ${NEW_TIMEOUT}  IN  @{NEW_TIMEOUTS}
        CLI:Set  idle_timeout=${NEW_TIMEOUT}
        CLI:Start Configuration transaction
        CLI:Commit
        ${OUTPUT}=  CLI:Show
        Should Contain    ${OUTPUT}   ${NEW_TIMEOUT}
        CLI:Revert Configuration transaction
        Wait Until Keyword Succeeds	3x	5s	SUITE:Check config is reverted
    END

SUITE:Add license key perform start transaction
    CLI:Enter Path  /settings/license/
    ${OUTPUT}=  CLI:Show
    Should Not Contain    ${OUTPUT}   ${LICENSE_KEY}
    CLI:Start Configuration transaction
    CLI:Write  add
    CLI:Set  license_key=${LICENSE_KEY}
    CLI:Commit
    ${OUTPUT}=  CLI:Show
    Should Contain    ${OUTPUT}   xxxxx-xxxxx-xxxxx-7POUR

SUITE:check configuration Reverted on config_revert for added license
    Write    config_revert
    ${OUTPUT}=     Read Until Prompt
    ${OUTPUT}=  CLI:Show
    Should Not Contain    ${OUTPUT}   xxxxx-xxxxx-xxxxx-7POUR

SUITE:Add network connection and start transaction
    CLI:Enter Path  /settings/network_connections/
    ${OUTPUT}=  CLI:Show
    Should Not Contain    ${OUTPUT}   ${NEW_CONN}
    CLI:Start Configuration transaction
    CLI:Write  add
    CLI:Set  name=${NEW_CONN}
    CLI:Commit
    ${OUTPUT}=  CLI:Show
    Should Contain    ${OUTPUT}   ${NEW_CONN}

SUITE:check configuration Reverted on config_revert for added network connection
    CLI:Revert Configuration transaction
    ${OUTPUT}=  CLI:Show
    Should Not Contain    ${OUTPUT}   ${NEW_CONN}

SUITE:check transactions confirmed on config_confirm still there on timeout
    CLI:Enter Path  /settings/network_connections/
    ${OUTPUT}=  CLI:Show
    Should Not Contain    ${OUTPUT}   ${NEW_CONN}
    CLI:Start Configuration transaction
    CLI:Write  add
    CLI:Set  name=${NEW_CONN}
    CLI:Commit
    CLI:Confirm Configuration transaction
    Wait Until Keyword Succeeds	30x	3s  SUITE:Check output for network connection after timeout

SUITE:Check transaction that is not confirmed automatically reverted on timeout
    CLI:Enter Path  /settings/system_preferences/
    CLI:Start Configuration transaction
    CLI:Set  idle_timeout=7000
    CLI:Commit
    ${OUTPUT}=  CLI:Show
    Wait Until Keyword Succeeds	5x	3s	SUITE:Check output before timeout
    Wait Until Keyword Succeeds	40x	3s	SUITE:Check output after timeout

SUITE:Delete added configuration
    CLI:Enter Path  /settings/network_connections/
    CLI:Delete    ${NEW_CONN}
    CLI:Commit
    ${OUTPUT}=  CLI:Show
    Should Not Contain    ${OUTPUT}    ${NEW_CONN}

SUITE:Check output before timeout
    ${OUTPUT}=  CLI:Show
    Should Contain    ${OUTPUT}    7000

SUITE:Check output after timeout
    ${OUTPUT}=  CLI:Show
    Should Contain    ${OUTPUT}    ${IDLE_TIMEOUT}

SUITE:Check output for network connection after timeout
    ${OUTPUT}=  CLI:Show
    Should Contain    ${OUTPUT}   ${NEW_CONN}

SUITE:Check config is reverted
	CLI:Enter Path	/settings/system_preferences/
	${OUTPUT}=	CLI:Show
	Should Contain	${OUTPUT}  idle_timeout = ${idle_timeout}
