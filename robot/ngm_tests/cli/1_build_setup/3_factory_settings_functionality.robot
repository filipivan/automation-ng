*** Settings ***
Resource	../init.robot
Documentation	Test factory settings toolkit functionality
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Force Tags	CLI	FACTORY_SETTINGS
Default Tags	FACTORY_SETTINGS

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
# Configuration for persistency
${CONFIG_CUSTOMFIELD_NAME}	customfield_testing_Factory
${CONFIG_CUSTOMFIELD_VALUE}	customvalue_testing_Factory
${CONFIG_DEVICE_NAME}	myself
${CONFIG_DEVICE_TYPE}	device_console
${CONFIG_DEVICE_IP}	127.0.0.1
${CONFIG_DEVICE_USER}	${DEFAULT_USERNAME}
${CONFIG_DEVICE_PASSWD}	${DEFAULT_PASSWORD}
${CONFIG_HOST_IP}	1.1.1.1
${CONFIG_HOST_NAME}	aliasTestingFactory
${CONFIG_HOST_ALIAS}	aliasTestingFactory
${CONFIG_LOCALACCOUNT_NAME}	user_testing_Factory
${CONFIG_LOCALACCOUNT_PASSWD}	user_testing_Factory
@{ALLOWED_OPEN_PORTS}	22	68	80	161	199	443	546	9966	4870	9977	12345	9200	9300

*** Test Cases ***
Test DD Command Right After Upgrade
	[Tags]	NON-CRITICAL
	Repeat Keyword	3 times	SUITE:Test DD
	[Teardown]	CLI:Switch Connection	default

Check M2MGMT Service
	[Tags]	EXCLUDEIN3_2
	Skip If	${IS_VM}
	CLI:Connect As Root
	SUITE:Check M2MGMT
	[Teardown]	CLI:Switch Connection	default

Test apply Factory Settings
	CLI:Apply Factory Settings	Yes
	[Teardown]	Run Keyword If	${NGVERSION} <= 4.2	CLI:Wait Nodegrid Webserver To Be Up	LOGIN_TIMEOUT=5m

Test admin password should be changed at login time
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	CLI:Change Password At Login Time
	CLI:Wait Nodegrid Webserver To Be Up	${HOST}	${QA_PASSWORD}	2m
	[Teardown]	CLI:Close Connection

Test SSH and Serial Console access as root should be disabled
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	[Setup]	CLI:Open	${DEFAULT_USERNAME}	${QA_PASSWORD}
	SUITE:Access Options For Root Should Be Disabled
	SUITE:Connecting As Root Should Fail
	[Teardown]	CLI:Close Connection

Test root password should be changed at login time
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2
	[Setup]	CLI:Open	${DEFAULT_USERNAME}	${QA_PASSWORD}
	CLI:Enable Ssh And Console Access As Root
	CLI:Change Password At Login Time	root	root	prompt=~#
	[Teardown]	Run Keywords	CLI:Change Root And Admin Password To QA Default
	...	AND	CLI:Close Connection

Test configuration should not persist
	[Setup]	Run Keywords	CLI:Open	AND	CLI:Connect As Root
	SUITE:Configuration Should Not Persist
	Run Keyword If	'${NGVERSION}' >= '4.2'	SUITE:Users Home Folders Should Be Empty
	SUITE:Previous Logs Should Be Clean
	[Teardown]	CLI:Close Connection

Test save diff between current /etc and factory /etc
	[Setup]	CLI:Connect As Root
	CLI:Write	tar -C /tmp/ -xzf /media/hdCnf/backup/factory_default_files.tgz
	CLI:Write	diff -r /etc/ /tmp/etc/ > /backup/test-factory-etc.diff
	[Teardown]	CLI:Close Connection

Test Open Ports
	[Documentation]	Checks the open ports after factory setting in order to be sure only non-crit ports are remain open
	[Tags]	OPEN_PORTS	NON-CRITICAL	BUG_NG_10672
	[Setup]	CLI:Connect As Root
	Run Keyword If	'${NGVERSION}' == '4.2'	Set Tags	NON-CRITICAL
	@{PORTS}	SUITE:Get Open Ports
	SUITE:Check Open Ports	@{PORTS}

Test Users Permissions
	[Documentation]	Checks the permissions of users admin, root and ansible
	[Tags]	BUG_NG_8469	EXCLUDEIN_OFFICIAL	#tag can be removed as soon as all official version have fix BUG_NG_8188
	[Setup]	CLI:Connect As Root
	SUITE:Delete Test Persistency User
	${PERMISSIONS}	SUITE:Get Users Permissions
	SUITE:Check Users Permissions	${PERMISSIONS}

Test change default VLANs to avoid network loops
	[Documentation]	If 2 switch interfaces belonging to the same VLAN are set as untagged and up, it can cause a
	...	network loop between HOST and HOSTSHARED and some broadcast packets can be flooded causing a lot delay
	...	and consuming devices resources during tests.
	CLI:Open
	${IS_SR}	CLI:Is SR System
	Skip If	not ${IS_SR}	VLAN only in SR family
	IF	${NGVERSION} <= 4.2
		CLI:Enter Path	/settings/switch_vlan/1
		CLI:Set	tagged_ports= untagged_ports=
		CLI:Commit
	ELSE
		CLI:Edit VLAN	1	TAGGED_PORTS=	UNTAGGED_PORTS=
		CLI:Edit VLAN	2	TAGGED_PORTS=	UNTAGGED_PORTS=
	END

Test if USB event was raised
	[Tags]	EXCLUDEIN3_2	EXCLUDEIN4_2	EXCLUDEIN5_0	EXCLUDEIN5_2	EXCLUDEIN5_4	NON-CRITICAL
	[Documentation]	Checks if after the factory settings the events for external USB devices inserted are raised
	CLI:Switch Connection	default
	CLI:Enter Path	/system/usb_devices
	${OUTPUT}	CLI:Ls
	@{LINES}	Split To Lines	${OUTPUT}	0	-1
	FOR	${LINE}	IN	@{LINES}
		CLI:Switch Connection	default
		CLI:Enter Path	/system/usb_devices/${LINE}
		${USB_DETAILS}	CLI:Show
		${IS_EXTERNAL}	Run Keyword And Return Status	Should Contain Any	${USB_DETAILS}	Storage	USB Sensor Device
		IF	${IS_EXTERNAL}
			${USB_PORT}	SUITE:Get USB Port	${LINE}
			${VENDOR ID}	${PRODUCT_ID}	SUITE:Get Vendor And Product IDs	${LINE}
			${MANUFACTURER}	SUITE:Get USB Manufacturer	${LINE}
			${DESCRIPTION}	SUITE:Get USB Device Description	${LINE}
			CLI:Connect As Root
			CLI:Test Event ID Logged	nodegrid	330
			...	USB device connected. USB Port: ${USB_PORT}. Vendor ID: ${VENDOR_ID}. Product ID: ${PRODUCT_ID}. Manufacturer: ${MANUFACTURER}. Description: ${DESCRIPTION}
			CLI:Close Current Connection
		END
	END

*** Keywords ***
SUITE:Setup
	CLI:Open
	${IS_VM}	CLI:Is VM System
	Set Suite variable	${IS_VM}
	SUITE:Delete Persistency Check Configuration
	SUITE:Add Configuration To Check Persistency
	Run Keyword If	'${NGVERSION}' >= '4.2'	SUITE:Add Files To Users Folders
	${TIME_SECONDS_BEFORE_FACTORY}=	CLI:Get Current System Time In Seconds
	Set Suite Variable	${TIME_SECONDS_BEFORE_FACTORY}

SUITE:Teardown
	Run Keyword If Any Tests Failed
	...	Fatal Error	Configuration was kept after applying factory settings to the system
	CLI:Close Connection

SUITE:Test DD
	CLI:Connect As Root
	Write	time dd if=/dev/zero of=/var/sw/xyz bs=1024 count=10000000
	Sleep	60s
	${OUTPUT}	CLI:Read Until Prompt
	${OUTPUT}	Split To Lines	${OUTPUT}	4	-3
	${OUTPUT}	Convert To String	${OUTPUT[0]}
	Should Match Regexp	${OUTPUT}	real\\t0m[0-4]\\d.\\d{3}s
	${SYS_VERSION}	CLI:Write	cli show /system/about software	user=yes
	${TIME}	CLI:Write	date	user=yes
	Write	mkdir /var/DD
	Read Until Prompt
	Write	echo 'Current Date: ${TIME}${SPACE}${SPACE}${SPACE}${SYS_VERSION}${SPACE}${SPACE}${SPACE}time: ${OUTPUT}' >> /var/DD/DD_TIME.log
	CLI:Read Until Prompt
	CLI:Write	rm /var/sw/xyz
	CLI:Close Current Connection

SUITE:Check M2MGMT
	${OUTPUT}	CLI:Write	ps -ef | grep m2m
	Should Contain	${OUTPUT}	/usr/sbin/m2mgmtd

SUITE:Add Configuration To Check Persistency
	CLI:Connect as Root
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Write	> /etc/scripts/custom_commands/config_test.py
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Write	/bin/echo "def CustomTest(dev):" > /etc/scripts/custom_commands/config_test.py
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Write	/bin/echo " print('hello')" >> /etc/scripts/custom_commands/config_test.py

	Switch Connection	default
	CLI:Enter Path	/settings/custom_fields/
	CLI:Add
	CLI:Set	field_name=${CONFIG_CUSTOMFIELD_NAME} field_value=${CONFIG_CUSTOMFIELD_VALUE}
	CLI:Commit
	CLI:Add Device	${CONFIG_DEVICE_NAME}	${CONFIG_DEVICE_TYPE}	${CONFIG_DEVICE_IP}	${CONFIG_DEVICE_USER}	${CONFIG_DEVICE_PASSWD}

	CLI:Enter Path	/settings/hosts
	CLI:Add
	CLI:Set	ip_address=${CONFIG_HOST_IP} hostname=${CONFIG_HOST_NAME} alias=${CONFIG_HOST_ALIAS}
	CLI:Commit

	CLI:Enter Path	/settings/local_accounts
	CLI:Add
	CLI:Set	username=${CONFIG_LOCALACCOUNT_NAME} password=${CONFIG_LOCALACCOUNT_PASSWD}
	CLI:Commit

	${CHECKSUM_BEFORE}=	CLI:Generate System Checksum
	Set Suite Variable	${CHECKSUM_BEFORE}

SUITE:Configuration Should Not Persist
	CLI:Switch Connection	root_session
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Write	cd /etc/scripts/custom_commands/
	${OUTPUT}=	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Write	ls
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Not Contain	${OUTPUT}	config_test.py

	CLI:Switch Connection	default
	${OUTPUT}=	CLI:Show	/settings/custom_fields/
	Should Not Contain	${OUTPUT}	${CONFIG_CUSTOMFIELD_NAME}

	${OUTPUT}=	CLI:Show	/settings/devices/
	Should Not Contain	${OUTPUT}	${CONFIG_DEVICE_NAME}

	${OUTPUT}=	CLI:Show	/settings/hosts/
	Should Not Contain	${OUTPUT}	${CONFIG_HOST_IP}

	${OUTPUT}=	CLI:Show	/settings/local_accounts/
	Should Not Contain	${OUTPUT}	${CONFIG_LOCALACCOUNT_NAME}

	${CHECKSUM_AFTER}=	CLI:Generate System Checksum

	Should Not Be Equal	${CHECKSUM_BEFORE}	${CHECKSUM_AFTER}

SUITE:Add Files To Users Folders
	CLI:Write	shell sudo touch /home/admin/test-file-factory-admin.txt
	CLI:Write	shell sudo mkdir /home/admin/test-folder-factory-admin
	CLI:Write	shell sudo touch /home/root/test-file-factory-root.txt
	CLI:Write	shell sudo mkdir /home/root/test-folder-factory-root

SUITE:Access Options For Root Should Be Disabled
	CLI:Enter Path	/settings/services
	${SHOW_OUTPUT}=    CLI:Show
    Should Contain	${SHOW_OUTPUT}	ssh_allow_root_access = no
    Should Contain	${SHOW_OUTPUT}	allow_root_console_access = no

SUITE:Connecting As Root Should Fail
	${STATUS}=	Run Keyword And Return Status	CLI:Connect As Root
	Run Keyword If 	${STATUS} == ${TRUE}
	...	Fail	Should not be able to login as root on 5.0+ using default configuration
	CLI:Close Connection

SUITE:Users Home Folders Should Be Empty
	${OUTPUT}=	CLI:Write	shell sudo ls /home/root/	user=Yes
	Should Be Empty	${OUTPUT}
	${OUTPUT}=	CLI:Write	shell sudo ls /home/admin/	user=Yes
	Should Be Empty	${OUTPUT}

SUITE:Previous Logs Should Be Clean
	SUITE:Previous Data Logs Should Be Clean
	SUITE:Previous SNR Should Be Clean
	SUITE:Previous Event Logs Should Be Clean

SUITE:Previous Data Logs Should Be Clean
	${OUTPUT}=	CLI:Write	shell sudo ls /var/local/DB	user=Yes
	Should Be Empty	${OUTPUT}
	${OUTPUT}=	CLI:Write	shell sudo ls /var/unsent/DB	user=Yes
	Should Be Empty	${OUTPUT}

SUITE:Previous SNR Should Be Clean
	${OUTPUT}=	CLI:Write	shell sudo ls /var/local/SNR	user=Yes
	Should Be Empty	${OUTPUT}
	${OUTPUT}=	CLI:Write	shell sudo ls /var/unsent/SNR	user=Yes
	Should Be Empty	${OUTPUT}

SUITE:Previous Event Logs Should Be Clean
	${OUTPUT}=	CLI:Write	shell sudo cat /var/local/EVT/*
	${TIMESTAMPS}=	Get Regexp Matches	${OUTPUT}	<(.*)[A-Z]>	1
	FOR		${TIMESTAMP}	IN	@{TIMESTAMPS}
		${TIME_SECONDS}=	Convert Date	${TIMESTAMP}	date_format=%Y-%m-%dT%H:%M:%S	result_format=epoch
		Should Be True	'${TIME_SECONDS_BEFORE_FACTORY}' < '${TIME_SECONDS}'
	END

	${OUTPUT}=	CLI:Write	shell sudo cat /var/unsent/EVT/*
	${TIMESTAMPS}=	Get Regexp Matches	${OUTPUT}	<(.*)[A-Z]>	1
	FOR		${TIMESTAMP}	IN	@{TIMESTAMPS}
		${TIME_SECONDS}=	Convert Date	${TIMESTAMP}	date_format=%Y-%m-%dT%H:%M:%S	result_format=epoch
		Should Be True	'${TIME_SECONDS_BEFORE_FACTORY}' < '${TIME_SECONDS}'
	END

SUITE:Delete Persistency Check Configuration
	CLI:Delete Devices	${CONFIG_DEVICE_NAME}
	CLI:Enter Path	/settings/custom_fields/
	Run Keyword If	'${NGVERSION}' == '4.2'	CLI:Delete If Exists	1
	...	ELSE	CLI:Delete If Exists	${CONFIG_CUSTOMFIELD_NAME}
	CLI:Enter Path	/settings/hosts/
	CLI:Delete If Exists	${CONFIG_HOST_IP}
	CLI:Delete Users	${CONFIG_LOCALACCOUNT_NAME}

SUITE:Get Open Ports
	${OUTPUT}=	CLI:Write	netstat -nltpu | grep -E 'udp|tcp' | grep -vE '127.0.0.1:|::1:'	# gets the open ports | filters for only the udp and tcp to remove header | removes the ones that have the IPV4 or IPV6 loopback respectively
	${OUTPUT}=	Remove String	${OUTPUT}	root@nodegrid:~#	#removes user once if used $2_{user}=yes on CLI:Write, Split to Lines didn't work
	@{PORTS}=	Split to Lines	${OUTPUT}
	[Return]	@{PORTS}

SUITE:Check Open Ports
	[Arguments]	@{PORTS}
	FOR	${PORT}	IN	@{PORTS}
		${PORT}	Remove String Using Regexp	${PORT}	((tcp)|(udp))(6)?(\\s)*[0-9](\\s)*[0-9](\\s)*(([0-9]*[0-9]*(\.)[0-9][0-9]*[0-9]*(\.)[0-9][0-9]*[0-9]*(\.)[0-9][0-9]*[0-9]*)|(([0-9]|[a-f])*:([0-9]|[a-f])*:([0-9]|[a-f])*(:([0-9]|[a-f])*)?(:([0-9]|[a-f])*)?)):	#removes all before port number
		${PORT}	Remove String Using Regexp	${PORT}	(\\s)*((0.0.0.0:\\*)|(:::\\*))(\\s)*(LISTEN)??(\\s)*[0-9]*\/(.)*	#removes all after port number
		Should Contain Any	${PORT}	@{ALLOWED_OPEN_PORTS}	#checks if any item from @{ALLOWED_OPEN_PORTS} is on ${VALUE}
	END

SUITE:Get Users Permissions
	${OUTPUT}	CLI:Write	ls -l /home/
	@{USERS}	Split to Lines	${OUTPUT}	1	-1
	${USERS}	Get Length	${USERS}
	Run Keyword If	'${NGVERSION}' <= '5.0'	Should Be True	'${USERS}' == '2'
	...	ELSE	Run Keyword And Warn On Failure	Should Be True	'${USERS}' == '3'		#workaround added once ansible user doesn't appear on all device. It's under analysis and we'll raise a bug as soon as we have the analysis done
	[Return]	${OUTPUT}

SUITE:Check Users Permissions
	[Arguments]	${PERMISSIONS}
	@{USERS}	Create List	admin	root
	#Run Keyword If	'${NGVERSION}' >= '5.2'	Append To List	${USERS}	ansible
	FOR	${USER}	IN	@{USERS}
		Should Match Regexp	${PERMISSIONS}	((.)*\\n)*drwx------\\s[0-9](\\s)+${USER}(\\s)+${USER}(\\s)+[0-9]?[0-9]?[0-9]?[0-9]?\\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)(\\s)+([0-2]?[0-9]|3[0-1])(\\s)+(([0-1][0-9]|2[0-3])(:)?[0-5][0-9])\\s${USER}(\\n(.)*)*
	END
	IF	'${NGVERSION}' >= '5.2'	#workaround added once ansible user doesn't appear on all device. It's under analysis and we'll raise a bug as soon as we have the analysis done
		${USER}	Set Variable	ansible
		Run Keyword And Warn On Failure	Should Match Regexp	${PERMISSIONS}	((.)*\\n)*drwx------\\s[0-9](\\s)+${USER}(\\s)+${USER}(\\s)+[0-9]?[0-9]?[0-9]?[0-9]?\\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)(\\s)+([0-2]?[0-9]|3[0-1])(\\s)+(([0-1][0-9]|2[0-3])(:)?[0-5][0-9])\\s${USER}(\\n(.)*)*
	END

SUITE:Delete Test Persistency User
	CLI:Enter Path	/home
	Write	rm -rf ${CONFIG_LOCALACCOUNT_NAME}

SUITE:Get USB Port
	[Documentation]	Gets the USB slot which the external USB device is inserted. Some devices, like pendrives,
	...	are detected as sd kernel devices, so it is needed to change for the usb port correspondent on NG device.
	[Arguments]	${ENTRY}
	${OUTPUT}	CLI:Show	/system/usb_devices/${ENTRY}/ "kernel device"	user=Yes
	${USB_PORT}	Fetch From Right	${OUTPUT}	kernel device:${SPACE}
	${IS_SD}	Run Keyword And Return Status	Should Contain	${USB_PORT}	sdS
	IF	${IS_SD}
		${USB_PORT}	Replace String	${USB_PORT}	sd	usb
	END
	[Return]	${USB_PORT}

SUITE:Get Vendor And Product IDs
	[Documentation]	Gets the Vendor and Product IDs from an external USB device. This information is displayed in
	...	the format vendorid:productid:
	[Arguments]	${ENTRY}
	${OUTPUT}	CLI:Show	/system/usb_devices/${ENTRY}/ "vendorid:productid"	user=Yes
	${IDS}	Fetch From Right	${OUTPUT}	vendorid:productid:${SPACE}
	${VENDOR_ID}	Fetch From Left	${IDS}	:
	${PRODUCT_ID}	Fetch From Right	${IDS}	:
	[Return]	${VENDOR_ID}	${PRODUCT_ID}

SUITE:Get USB Manufacturer
	[Documentation]	Gets the manufacturer from an external USB device. Replaces spaces for underscores.
	[Arguments]	${ENTRY}
	${OUTPUT}	CLI:Show	/system/usb_devices/${ENTRY}/ "manufacturer"	user=Yes
	${MANUFACTURER}	Fetch From Right	${OUTPUT}	manufacturer:${SPACE}
	${HAS_SPACES}	Run Keyword And Return Status	Should Contain	${MANUFACTURER}	${SPACE}
	IF	${HAS_SPACES}
		${MANUFACTURER}	Replace String	${MANUFACTURER}	${SPACE}	_
	END
	[Return]	${MANUFACTURER}

SUITE:Get USB Device Description
	[Documentation]	Gets the description from an external USB device. Replaces spaces for underscores.
	[Arguments]	${ENTRY}
	${OUTPUT}	CLI:Show	/system/usb_devices/${ENTRY}/ "description"	user=Yes
	${DESCRIPTION}	Fetch From Right	${OUTPUT}	description:${SPACE}
	${HAS_SPACES}	Run Keyword And Return Status	Should Contain	${DESCRIPTION}	${SPACE}
	IF	${HAS_SPACES}
		${DESCRIPTION}	Replace String	${DESCRIPTION}	${SPACE}	_
	END
	[Return]	${DESCRIPTION}