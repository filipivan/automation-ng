*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	NEED-CREATION
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${PEERS}=	Create List

*** Test Cases ***

Delete cluster peers (status_code = 200)
	[Documentation]	Delete cluster peers. Status code 200 is expected. 
	...	Endpoint: /cluster/peers

	${PAYLOAD}=	Set Variable	{"peers":[${PEERS}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/cluster/peers
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session