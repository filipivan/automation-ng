*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${ENABLE_CLUSTER}=	true
${TYPE_PEER}=	peer
${TYPE_COORD}=	coordinator
${AUTO_PSK}=	nodegrid-key
${AUTO_INTERVAL}=	60
${PSK}=	nodegrid
${POLLING_RATE}=	30
${COORDINATOR_ADDRESS}=	192.168.7.25
${RENEW_TIME}=	1
${LEASE_TIME}=	7
${AUTO_ENROLL}=	true
${ENABLE_CLUSTER}	true
${ALLOW_ENROLLMENT}	true
${ENABLE_CLUSTERING}	false
${ENABLE_PEER_MANAGEMENT}	false
${ENABLE_LICENSE_POOL}	true
${TYPE}	master
${LPS_TYPE}=	client
${CLUSTER_NAME}=	nodegrid
${PRESHAREDKEY}=	TestPreSharedKey

*** Test Cases ***
#TODO:Check how to encrease the timeout. It works but takes some seconds
Update cluster settings (status_code = 200)
	[Documentation]	Update cluster settings. Status code 200 is expected. 
	...	Endpoint: /cluster/settings

	${PAYLOAD}=	Set Variable	{"auto_enroll":${AUTO_ENROLL}, "enable_cluster":${ENABLE_CLUSTER}, "allow_enrollment":${ALLOW_ENROLLMENT}, "enable_clustering":${ENABLE_CLUSTERING}, "enable_peer_management":${ENABLE_PEER_MANAGEMENT}, "enable_license_pool":${ENABLE_LICENSE_POOL}, "type":"${TYPE}", "auto_psk":"${AUTO_PSK}", "auto_interval":"${AUTO_INTERVAL}", "psk":"${PSK}", "polling_rate":"${POLLING_RATE}", "coordinator_address":"${COORDINATOR_ADDRESS}", "renew_time":"${RENEW_TIME}", "lease_time":"${LEASE_TIME}", "type":"${TYPE}", "lps_type":"${LPS_TYPE}", "cluster_name":"${CLUSTER_NAME}", "preSharedKey":"${PRESHAREDKEY}"}
	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/cluster/settings
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session