*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${IP_RANGE_START}=	1.1.1.2
${IP_RANGE_END}=	1.1.1.10

*** Test Cases ***

Add new IP range (status_code = 200)
	[Documentation]	Add new IP range . Status code 200 is expected. 
	...	Endpoint: /cluster/settings/range

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	ip_range_start=${IP_RANGE_START}
	Set To Dictionary	${payload}	ip_range_end=${IP_RANGE_END}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/cluster/settings/range
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session