*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${IP_RANGE_START}=	1.1.1.2
${IP_RANGE_END}=	1.1.1.10

*** Test Cases ***

Delete IP range (status_code = 200)
	[Documentation]	Delete IP range  . Status code 200 is expected. 
	...	Endpoint: /cluster/settings/range

	${PAYLOAD}=	Set Variable	{"ranges":["${IP_RANGE_START}-${IP_RANGE_END}"]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/cluster/settings/range
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session