*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${MULTI_PEERS}=	ea91d006-df4a-4834-a040-28f0ca45bb22
${URL}=	${FTPSERVER_URL}/nodegrid-genericx86-64-20230115233439_Branch-NG_5.6-20230115153001.signed.iso
${CLUSTER_USERNAME}=	${FTPSERVER_USER}
${CLUSTER_PASSWORD}=	${FTPSERVER_PASSWORD}
${THE_PATH_IN_URL_TO_BE_USED_AS_ABSOLUTE_PATH_NAME}=	false
${FORMAT_PARTITIONS_BEFORE_UPGRADE}=	false
${IMAGE_LOCATION}=	upgrade-server
${IF_DOWNGRADING}=	restore

*** Test Cases ***

Upgrade devices connected to the Cluster (status_code = 200)
	[Documentation]	Upgrade devices connected to the Cluster. Status code 200 is expected. 
	...	Endpoint: /cluster/management/upgrade

	${PAYLOAD}=	Set Variable	{"multi_peers":"${MULTI_PEERS}", "url":"${URL}", "username":"${CLUSTER_USERNAME}", "password":"${CLUSTER_PASSWORD}", "the_path_in_url_to_be_used_as_absolute_path_name":"${THE_PATH_IN_URL_TO_BE_USED_AS_ABSOLUTE_PATH_NAME}", "format_partitions_before_upgrade":"${FORMAT_PARTITIONS_BEFORE_UPGRADE}", "image_location":"${IMAGE_LOCATION}", "if_downgrading":"${IF_DOWNGRADING}"}
	
	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/cluster/management/upgrade
	Should Be Equal   ${response.status_code}   ${200}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session