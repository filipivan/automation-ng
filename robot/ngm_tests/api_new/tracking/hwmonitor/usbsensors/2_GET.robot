*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***

Get USB Sensor information (status_code = 200)
	[Documentation]	Get USB Sensor information. Status code 200 is expected. 
	...	Endpoint: /tracking/hwmonitor/usbsensors

	${response}=	API::Send Get Request	/tracking/hwmonitor/usbsensors
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get USB Sensor information Parameters
	IF    ${resp} == []
		SKIP	HWMonitor don't have usbsensors
	ELSE
		${PARAMETERS}	Create list	'id':	'name':	'value':	'unit':	'description':
		CLI:Should Contain All	${resp}	${PARAMETERS}
	END

*** Keywords ***
SUITE:Setup
	API::Post::Session
	${IS_NGM}	API::Is NGM
	Set Suite Variable	${IS_NGM}

SUITE:Teardown
	API::Delete::Session