*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get Tracking Device Wireless Modem information (status_code = 200)
	[Documentation]	Get Tracking Device Wireless Modem information. Status code 200 is expected. 
	...	Endpoint: /tracking/devices/wirelessmodem/{id}

	${response}=	API::Send Get Request	/tracking/devices/wirelessmodem/{id}
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get Tracking Device Wireless Modem information Parameters
	${PARAMETERS}	Create list	'data_usage':	'signal_strength':	'slot':	'modem_model':	'firmware_version':	'hardware_version':	'carrier_configuration':	'equipment_id':	'interface':	'status':	'current_operator':	 'temperature':	 'active_sim_card': 'No SIM Detected', 'ip_family':	 'ip_address':	 'ip_gateway':	 'ip_primary_dns':	 'carrier_mtu':	 'bytes_accumulated_sim_1':	 'bytes_accumulated_sim_2':	 'sim_1_last_update':	 'sim_1_status':	'sim_1_subscriber_id':	 'sim_1_circuit_card_id':	 'sim_1_operator':	 'sim_1_phone_number_configured':	 'sim_1_sim_state':	 'sim_1_connection':	 'sim_1_signal_strength':	 'sim_1_access_point_name':	 'sim_1_user_name':	'sim_1_password':	'sim_2_last_update':	'sim_2_status':	'sim_2_subscriber_id':	'sim_2_circuit_card_id':	'sim_2_operator':	'sim_2_phone_number_configured':	'sim_2_sim_state':	'sim_2_connection':	'sim_2_signal_strength':	'sim_2_access_point_name':	'sim_2_user_name':	'sim_2_password':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session