*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get Tracking Devices Wireless Modem information (status_code = 200)
	[Documentation]	Get Tracking Devices Wireless Modem information. Status code 200 is expected. 
	...	Endpoint: /tracking/devices/wirelessmodem

	${response}=	API::Send Get Request	/tracking/devices/wirelessmodem
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get Tracking Devices Wireless Modem information Parameters
	${PARAMETERS}	Create list	id':	'slot':	'interface':	'status':	'sim_state':	'active':	'data_consumption':	'operator':	'radio_mode':	'signal_strength':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session