*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get Serial Statistics (status_code = 200)
	[Documentation]	Retrieve the Serial Statistics related to serial devices. Return an entry for each port available.. Status code 200 is expected. 
	...	Endpoint: /tracking/devices/serialstats

	${response}=	API::Send Get Request	/tracking/devices/serialstats
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get enrollment information Parameters
	Skip If	${IS_VM}
	${PARAMETERS}	Create list	'id':	'speed':	'port':	'device_name':	'rx_bytes':	'tx_bytes':	'rs-232_signals':	'cts_shift':	'dcd_shift':	'frame_error':	'overrun':	'parity_error':	'break':	'buffer_overrun':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session
	${IS_VM}=	API::Is NGM
	Set Suite Variable	${IS_VM}

SUITE:Teardown
	API::Delete::Session