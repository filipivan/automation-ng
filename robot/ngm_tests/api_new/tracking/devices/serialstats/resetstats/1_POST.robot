*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${DEVICES}=	Create List

*** Test Cases ***

Reset Serial Statistics (status_code = 200)
	[Documentation]	Reset the Serial Statistics values for the specified devices. Status code 200 is expected. 
	...	Endpoint: /tracking/devices/serialstats/resetstats

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	devices=${DEVICES}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/tracking/devices/serialstats/resetstats
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session