*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${ENABLE_GEO_FENCE}=	true
${PERIMETER_TYPE}=	circle
${RADIUS}=	1000
${COORDINATES}=	37.50422035,-121.97403149822762
${INSIDE_PERIMETER_ACTION}=	template.sh

*** Test Cases ***

Get GEO Fence information (status_code = 200)
	[Documentation]	Get GEO Fence information. Status code 200 is expected. 
	...	Endpoint: /tracking/devices/geo_fence

	${response}=	API::Send Get Request	/tracking/devices/geo_fence
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

#Validate Get enrollment information Parameters
#	${PARAMETERS}	Create list	'options':	'label':	'url':	'customer_code':	'enrollment_key':	'status':
#	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session
	SUITE:Enable GeoFence

SUITE:Teardown
	SUITE:Disable GeoFence
	API::Delete::Session

SUITE:Enable GeoFence
	${PAYLOAD}=	Set Variable	{"enable_geo_fence":${ENABLE_GEO_FENCE}, "perimeter_type":"${PERIMETER_TYPE}", "radius":"${RADIUS}", "coordinates":"${COORDINATES}", "inside_perimeter_action":"${INSIDE_PERIMETER_ACTION}"}
	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/geo_fence
	Should Be Equal   ${response.status_code}   ${200}

SUITE:Disable GeoFence
	${PAYLOAD}=	Set Variable	{"enable_geo_fence":false}
	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/geo_fence
	Should Be Equal   ${response.status_code}   ${200}