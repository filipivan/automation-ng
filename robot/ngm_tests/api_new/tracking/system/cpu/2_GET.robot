*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get CPU usage information (status_code = 200)
	[Documentation]	Get CPU usage information. Status code 200 is expected. 
	...	Endpoint: /tracking/system/cpu

	${response}=	API::Send Get Request	/tracking/system/cpu
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get CPU usage information Parameters
	${PARAMETERS}	Create list	'id':	'user_%':	'system_%':	'idle_%':	'waiting_i/o_%':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session