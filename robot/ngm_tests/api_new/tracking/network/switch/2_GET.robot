*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get network switch interfaces information (status_code = 200)
	[Documentation]	Get network switch interfaces information. Status code 200 is expected. 
	...	Endpoint: /tracking/network/switch

	${response}=	API::Send Get Request	/tracking/network/switch
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get network switch interfaces information Parameters
	${PARAMETERS}	Create list	'id':	'status':	'state':	'speed':	'power':	'stp_state':	'stp_role':	'description':	'interface':	'rx_packets':	'tx_packets':	'802.1x_state':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session
	${IS_NSR}	API::Is NSR
	Skip If	not ${IS_NSR}	Switch Only Available on NSR

SUITE:Teardown
	API::Delete::Session