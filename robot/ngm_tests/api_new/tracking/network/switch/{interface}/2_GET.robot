*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***
#TODO: need to get the available intercafes first
Get network switch interface information (status_code = 200)
	[Documentation]	Get network switch interface information. Status code 200 is expected. 
	...	Endpoint: /tracking/network/switch/{interface}

	Skip If	not ${IS_NSR}	Switch Only Available on NSR

	${SWITCH_INTERFACES}	SUITE:Get Interfaces
	Set Suite Variable	${SWITCH_INTERFACES}

Validate Get network switch interface information Parameters
	${PARAMETERS}	Create List	'id':	'interface':	'description':	'status':	'state':	'speed(mb/s)':	'rx_packets':	'rx_bytes':	'tx_packets':	'tx_bytes':
	${SFP_EEPROM}	Create List	'value':	'sfp_eeprom_field':
	FOR	${INTERFACE}	IN	@{SWITCH_INTERFACES}
		${response}=	API::Send Get Request	/tracking/network/switch/${INTERFACE}
		Should Be Equal   ${response.status_code}   ${200}
		${resp}=    Evaluate   str(${response.json()})
		Log    ${resp}
		CLI:Should Contain All	${resp}	${PARAMETERS}
		${CONTAINS}	Run Keyword and Return Status	Should Contain	${resp}	'sfp_eeprom':
		IF	${CONTAINS}
			CLI:Should Contain All	${resp}	${SFP_EEPROM}
		END
	END

*** Keywords ***
SUITE:Setup
	API::Post::Session
	
	${IS_NSR}	API::Is NSR
	Skip If	not ${IS_NSR}	Switch Only Available on NSR
	Set Suite variable	${IS_NSR}

SUITE:Teardown
	API::Delete::Session

SUITE:Get Interfaces
	${response}=	API::Send Get Request	/tracking/network/switch
	Should Be Equal   ${response.status_code}   ${200}
	${INTERFACE}=	evaluate	str(${response.json()})
	${INTERFACE}=	Remove String	${INTERFACE}	[
	${INTERFACE}=	Remove String	${INTERFACE}	]
	${INTERFACE}	split string	${INTERFACE}	},
	@{INTERFACES}	Create List
	FOR	${I}	IN	@{INTERFACE}
		${COLUMNS}=	Split String	${I}	',
		FOR	${COLUMN}	IN	@{COLUMNS}
			${CONTAINS}	Run Keyword And Return Status	Should Contain	${COLUMN}	'id':
			IF	${CONTAINS}
				${ID}=	Replace String Using Regexp	${COLUMN}	\\s*\{\'id\'\:\\s|\'|\:	${EMPTY}
				Append to List	${INTERFACES}	${ID}
			END
			Exit For Loop If	${CONTAINS}
		END
	END
	[Return]	@{INTERFACES}