*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${PAGE}=	${1}
${PAGE_SIZE}=	${5}

*** Test Cases ***

Get paginated entries of switch MAC address table (status_code = 200)
	[Documentation]	Get paginated entries of switch MAC address table. Status code 200 is expected. 
	...	Endpoint: /tracking/network/mactable

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	page=${PAGE}
	Set To Dictionary	${payload}	page_size=${PAGE_SIZE}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/tracking/network/mactable
	Should Be Equal   ${response.status_code}   ${200}

Get paginated entries of switch MAC address table (status_code = 400)
	[Documentation]	Get paginated entries of switch MAC address table. Status code 400 is expected.
	...	Endpoint: /tracking/network/mactable

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	page=${0}
	Set To Dictionary	${payload}	page_size=${0}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/tracking/network/mactable	ERROR_CONTROL=${FALSE}	EXPECT_ERROR=HTTPError: 400 Client Error: Bad Request for url: https://${HOST}/api/v1/tracking/network/mactable
	Should Be equal   ${response}   HTTPError: 400 Client Error: Bad Request for url: https://${HOST}/api/v1/tracking/network/mactable

#Get paginated entries of switch MAC address table (status_code = 416)
#	[Documentation]	Get paginated entries of switch MAC address table. Status code 416 is expected. 
#	...	Endpoint: /tracking/network/mactable

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	page=${PAGE}
#	Set To Dictionary	${payload}	page_size=${PAGE_SIZE}
#
#	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/tracking/network/mactable
#	Should Be Equal   ${response.status_code}   ${416}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session