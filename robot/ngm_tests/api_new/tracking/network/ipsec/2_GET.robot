*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get network ipsec table information (status_code = 200)
	[Documentation]	Get network ipsec table information. Status code 200 is expected. 
	...	Endpoint: /tracking/network/ipsec

	${response}=	API::Send Get Request	/tracking/network/ipsec
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}
#TODO: need to add ipsec
#Validate Get enrollment information Parameters
#	${PARAMETERS}	Create list	'options':	'label':	'url':	'customer_code':	'enrollment_key':	'status':
#	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session