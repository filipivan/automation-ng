*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get network LLDP information (status_code = 200)
	[Documentation]	Get network LLDP information. Status code 200 is expected. 
	...	Endpoint: /tracking/network/lldp

	${response}=	API::Send Get Request	/tracking/network/lldp
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get network LLDP information Parameters
	${PARAMETERS}	Create list	'id':	'type':	'connection':	'chassis_id':	'port_id':	'port_description': 	'age':	'system_name':	'ipv4_mgmt_addr':	'ipv6_mgmt_addr':	'system_description':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session