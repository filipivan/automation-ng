*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***
#TODO: need to get the available intercafes first
Get network mstp instances interfaces information (status_code = 200)
	[Documentation]	Get network mstp instances interfaces information. Status code 200 is expected. 
	...	Endpoint: /tracking/network/mstp/{instance}/{interface}

	@{INTERFACES}	SUITE:Get Interfaces

	FOR	${INTERFACE}	IN	@{INTERFACES}
		@{INSTANCES}	SUITE:Get Instances	@{INTERFACES}
		${response}=	API::Send Get Request	/tracking/network/mstp/0/0
		Should Be Equal   ${response.status_code}   ${200}
	END


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session

SUITE:Get Interfaces
	${response}=	API::Send Get Request	/tracking/network/mstp
	Should Be Equal   ${response.status_code}   ${200}
	${INSTANCE}=	evaluate	str(${response.json()})
	LOG	${INSTANCE}
	${INSTANCE}=	Remove String	${INSTANCE}	[
	${INSTANCE}=	Remove String	${INSTANCE}	]
	${INSTANCE}	split string	${INSTANCE}	},
	@{INSTANCES}	Create List
	FOR	${I}	IN	@{INSTANCES}
		${COLUMNS}=	Split String	${I}	",
		FOR	${COLUMN}	IN	@{COLUMNS}
			${CONTAINS}	Run Keyword And Return Status	Should Contain	${COLUMN}	'id':
			IF	${CONTAINS}
				${ID}=	Replace String Using Regexp	${COLUMN}	\\s*\{\'id\'\:\\s\'	${EMPTY}
				Append to List	${INSTANCES}	${ID}
			END
			Exit For Loop If	${CONTAINS}
		END
	END
	[Return]	@{INSTANCES}

SUITE:Get Instances
	[Arguments]	${INTERFACES}
	${INSTANCES}	Create List
	FOR	${I}	IN	@{INTERFACES}
		${COLUMNS}=	Split String	${I}	",
		FOR	${COLUMN}	IN	@{COLUMNS}
			${CONTAINS}	Run Keyword And Return Status	Should Contain	${COLUMN}	'id':
			IF	${CONTAINS}
				${ID}=	Replace String Using Regexp	${COLUMN}	\\s*\{\'id\'\:\\s\'	${EMPTY}
				Append to List	${INSTANCES}	${ID}
			END
			Exit For Loop If	${CONTAINS}
		END
	END
	[Return]	@{INSTANCES}