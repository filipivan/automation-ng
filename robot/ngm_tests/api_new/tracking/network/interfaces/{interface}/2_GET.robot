*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get network interface information (status_code = 200)
	[Documentation]	Get network interface information. Status code 200 is expected. 
	...	Endpoint: /tracking/network/interfaces/{interface}

	${response}=	API::Send Get Request	/tracking/network/interfaces/{interface}
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get network interface information Parameters
	${PARAMETERS}	Create list	'ifname':	'speed(mb/s)':	'duplex':	'collisions':	'rx_packets':	'rx_bytes':	'rx_errors':	'rx_crc_errors':	'rx_dropped':	'rx_fifo_errors':	'rx_compressed':	'rx_frame_errors':	'rx_length_errors':	'rx_missed_errors':	'rx_over_errors':	'tx_packets':	'tx_bytes':	'tx_errors':	'tx_carrier_errors':	'tx_dropped':	'tx_fifo_errors':	'tx_compressed':	'tx_aborted_errors':	'tx_heartbeat_errors':	'tx_window_errors':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session