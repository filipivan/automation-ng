*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get network routing table information (status_code = 200)
	[Documentation]	Get network routing table information. Status code 200 is expected. 
	...	Endpoint: /tracking/network/routingtable

	${response}=	API::Send Get Request	/tracking/network/routingtable
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get network routing table information Parameters
	${PARAMETERS}	Create list	'id':	'destination':	'gateway':	'metric':	'from':	'table':	'interface':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session