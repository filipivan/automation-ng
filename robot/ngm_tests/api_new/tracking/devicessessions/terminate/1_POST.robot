*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${TYPE}=	device_console
${NAME}=	test_session_device
${IP_ADDRESS}=	192.168.16.123
${MODE}=	ondemand

*** Test Cases ***

Get devices sessions (status_code = 200)
	[Documentation]	Get devices sessions. Status code 200 is expected. 
	...	Endpoint: /tracking/devicessessions/terminate

	${ID}	SUITE:Get Device Sessions
	${ID}	Create List	${ID}
	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${PAYLOAD}	devices_sessions=${ID}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/tracking/devicessessions/terminate
	Should Be Equal   ${response.status_code}   ${200}
	SUITE:Get Device Sessions	SHOULD_CONTAIN=${FALSE}

*** Keywords ***
SUITE:Setup
	API::Post::Session
	SUITE:Add Device
	CLI:Open
	CLI:Connect Device	${NAME}

SUITE:Teardown
	SUITE:Delete Device
	CLI:Close Connection
	API::Delete::Session

SUITE:Add Device
	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	type=${TYPE}
	Set To Dictionary	${payload}	name=${NAME}
	Set To Dictionary	${payload}	ip_address=${IP_ADDRESS}
	Set To Dictionary	${payload}	mode=${MODE}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table
	Should Be Equal   ${response.status_code}   ${200}

SUITE:Get Device Sessions
	[Arguments]	${SHOULD_CONTAIN}=${TRUE}
	${response}=	API::Send Get Request	/tracking/devicessessions
	Should Be Equal   ${response.status_code}   ${200}
	${DEVICES}=	evaluate	str(${response.json()})
	LOG	${DEVICES}
	${DEVICES}=	Remove String	${DEVICES}	[
	${DEVICES}=	Remove String	${DEVICES}	]
	${DEVICES}	split string	${DEVICES}	},
	FOR	${DEVICE}	IN	@{DEVICES}
		${COLUMNS}=	Split String	${DEVICE}	',
		FOR	${COLUMN}	IN	@{COLUMNS}
			${CONTAINS}	Run Keyword And Return Status	Should Contain	${COLUMN}	'id':
			IF	${CONTAINS}
				${ID}=	Replace String Using Regexp	${COLUMN}	\\s*\{\'id\'\:\\s\'	${EMPTY}
				Continue For Loop If	${TRUE}
			END
			${CONTAINS}	Run Keyword And Return Status	Should Contain	${COLUMN}	"device_name": "${NAME}",
			Run Keyword If	not ${SHOULD_CONTAIN} and ${CONTAINS}	Fail	Device session should have terminated
			Exit For Loop If	${CONTAINS}
		END
	END
	Return From Keyword If	not ${SHOULD_CONTAIN}	${FALSE}
	[Return]	${ID}

SUITE:Delete Device
	${PAYLOAD}=	Set Variable	{"devices":["${NAME}"]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table
	Should Be Equal   ${response.status_code}   ${200}