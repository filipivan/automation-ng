*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SCHEDULES}=	"test_task_schedule"

*** Test Cases ***

Reset scheduler logs (status_code = 200)
	[Documentation]	Reset scheduler logs. Status code 200 is expected. 
	...	Endpoint: /tracking/schedule/resetlogs

	${PAYLOAD}=	Create Dictionary

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/tracking/schedule/resetlogs
	Should Be Equal   ${response.status_code}   ${200}

Test Post /tracking/schedule/resetlogs Functionality
	${response}=	API::Send Get Request	/tracking/schedule
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Be Equal	${resp}	[]

*** Keywords ***
SUITE:Setup
	API::Post::Session
	SUITE:Delete Schedule

SUITE:Teardown
	API::Delete::Session

SUITE:Delete Schedule
	[Documentation]	Remove scheduled task. Status code 200 is expected.
	...	Endpoint: /system/schedule

	${PAYLOAD}=	Set variable	{"schedules":[${SCHEDULES}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/system/schedule
	Should Be Equal   ${response.status_code}   ${200}
