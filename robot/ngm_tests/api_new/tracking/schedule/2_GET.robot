*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get scheduler logs (status_code = 200)
	[Documentation]	Get scheduler logs. Status code 200 is expected. 
	...	Endpoint: /tracking/schedule

	${response}=	API::Send Get Request	/tracking/schedule
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get scheduler logs Parameters
	${PARAMETERS}	Create list	'id':	'date':	'pid':	'event':	'task_name':	'user':	'error':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session