*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${PAGE}=	${1}
${TASK_NAME}=	test_task_schedule
${USER}=	daemon
${COMMAND_TO_EXECUTE}=	cd .

*** Test Cases ***

Get paginated entries of scheduler logs (status_code = 200)
	[Documentation]	Get paginated entries of scheduler logs. Status code 200 is expected. 
	...	Endpoint: /tracking/schedule

	${PAYLOAD}=		Create Dictionary
	Set To Dictionary	${payload}	page=${PAGE}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/tracking/schedule
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}

*** Keywords ***
SUITE:Setup
	API::Post::Session
	SUITE:Add Schedule

SUITE:Teardown
	API::Delete::Session

SUITE:Add Schedule
	[Documentation]	Add new scheduled task. Status code 200 is expected.
	...	Endpoint: /system/schedule

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	task_name=${TASK_NAME}
	Set To Dictionary	${payload}	user=${USER}
	Set To Dictionary	${payload}	command_to_execute=${COMMAND_TO_EXECUTE}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/schedule
	Should Be Equal   ${response.status_code}   ${200}
	Sleep	60s	#sleep until there are some logs generated