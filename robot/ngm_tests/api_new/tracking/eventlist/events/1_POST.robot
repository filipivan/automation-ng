*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${FILTER}=	200
${DATE_FROM}=	04/01/2021 00:00:00
${DATE_TO}=	04/31/2021 23:59:59

*** Test Cases ***
Search for system events occurrences (status_code = 200)
	[Documentation]	Search for system events occurrences. Status code 200 is expected. 
	...	Endpoint: /tracking/eventlist/events

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	filter=${FILTER}
	Set To Dictionary	${payload}	date_from=${DATE_FROM}
	Set To Dictionary	${payload}	date_to=${DATE_TO}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/tracking/eventlist/events
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session