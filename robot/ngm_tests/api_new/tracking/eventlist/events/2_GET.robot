*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***

Get system events in chronological order (status_code = 200)
	[Documentation]	Get system events in chronological order. Status code 200 is expected. 
	...	Endpoint: /tracking/eventlist/events

	${response}=	API::Send Get Request	/tracking/eventlist/events
	Should Be Equal   ${response.status_code}   ${200}
	
Check Get system events (status_code = 200)
	[Documentation]	Get system events and check if its appearing all title fields. Status code 200 is expected. 
	...	Endpoint: /tracking/eventlist/events
	
	${response}=	API::Send Get Request	/tracking/eventlist/events
	${resp}=    Evaluate   str(${response.json()})
	${PARAMETERS}	Create list	'id':	'event_id':	'event_name':	'hostname':	'date':	'description':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session