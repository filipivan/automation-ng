*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***


*** Test Cases ***

Reset discovery logs (status_code = 200)
	[Documentation]	Reset discovery logs. Status code 200 is expected. 
	...	Endpoint: /tracking/discoverylogs/resetlogs

	${PAYLOAD}=	Create Dictionary

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/tracking/discoverylogs/resetlogs
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session