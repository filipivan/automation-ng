*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${SESSIONS}=	"3086|admin|ssh|192.168.14.12|Tue Jul 19 13:26:31 2022|cli|nodegrid|||"

*** Test Cases ***
#TODO: need to undertand why it seems to get stuck and then don't work
Terminate open sessions (status_code = 200)
	[Documentation]	Terminate open sessions. Status code 200 is expected. 
	...	Endpoint: /tracking/opensessions/terminate

	${SESSION}	SUITE:Get Session
	${SESSION}	Create List	${SESSION}
	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	sessions=${SESSION}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/tracking/opensessions/terminate
	Should Be Equal   ${response.status_code}   ${200}

Test Terminated Session was Closed
	SUITE:Get Session	SHOULD_CONTAIN=${FALSE}

*** Keywords ***
SUITE:Setup
	API::Post::Session
	CLI:Open	session_alias=CLI_SESSION

SUITE:Teardown
	CLI:Close Connection
	API::Delete::Session

SUITE:Get Session
	[Arguments]	${SHOULD_CONTAIN}=${TRUE}
	${response}=	API::Send Get Request	/tracking/opensessions
	Should Be Equal   ${response.status_code}   ${200}
	${SESSIONS}=	evaluate	str(${response.json()})
	${SESSIONS}=	Remove String	${SESSIONS}	[
	${SESSIONS}=	Remove String	${SESSIONS}	]
	${SESSIONS}	split string	${SESSIONS}	},
	FOR	${S}	IN	@{SESSIONS}
		${SESSION}=	Split String	${S}	',
		FOR	${COLUMN}	IN	@{SESSION}
			${CONTAINS}	Run Keyword And Return Status	Should Contain	${COLUMN}	'id':
			IF	${CONTAINS}
				${SESSION}=	Replace String Using Regexp	${COLUMN}	\\s*\{\'id\'\:\\s\'	${EMPTY}
				${CLI_SESSION}	Run Keyword And Return Status	Should Match Regexp	${ID}	\\d+\|admin\|ssh\|192.168.14.12\|.*\|cli\|nodegrid\|+
				Run Keyword If	not ${SHOULD_CONTAIN} and ${CLI_SESSION}	Fail	Session should have terminated
				Exit For Loop If	${CLI_SESSION}
			END
		END
		Exit For Loop If	${CLI_SESSION}
	END
	[Return]	${SESSION}

