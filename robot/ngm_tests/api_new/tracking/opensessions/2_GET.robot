*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get open sessions (status_code = 200)
	[Documentation]	Get open sessions. Status code 200 is expected. 
	...	Endpoint: /tracking/opensessions

	${response}=	API::Send Get Request	/tracking/opensessions
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get open sessions Parameters
	${PARAMETERS}	Create list	'id':	'user':	'mode':	'source_ip':	'type':	'device_name':	'ref':	'session_start':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session