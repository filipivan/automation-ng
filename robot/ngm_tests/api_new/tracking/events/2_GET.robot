*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get system events statistics (status_code = 200)
	[Documentation]	Get system events statistics. Status code 200 is expected. 
	...	Endpoint: /tracking/events

	${response}=	API::Send Get Request	/tracking/events
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get system events statistics Parameters
	Should Match Regexp	${resp}	[(\{\'id\'\: \'\\D{1-3}\'\, \'category\'\: \'(\\W\\s*)+\'\, \'event_number\'\: \'\\D{1-3}\'\, \'description\'\: \'(\\W\\s*)+\'\, \'occurrences\'\: \'0\'\}\,\\s)+\{\'id\'\: \'\\D{1-3}\'\, \'category\'\: \'(\\W\\s*)+\'\, \'event_number\'\: \'\\D{1-3}\'\, \'description\'\: \'(\\W\\s*)+\'\, \'occurrences\'\: \'0\'\}]

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session