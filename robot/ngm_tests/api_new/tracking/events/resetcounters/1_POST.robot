*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${TYPE}=	device_console
${NAME}=	testdevice
${IP_ADDRESS}=	192.168.16.123
${MODE}=	ondemand

*** Test Cases ***
Reset event counters (status_code = 200)
	[Documentation]	Reset event counters. Status code 200 is expected. 
	...	Endpoint: /tracking/events/resetcounters

	${EVENTS}=	Create List	302
	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	events=${EVENTS}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/tracking/events/resetcounters
	Should Be Equal   ${response.status_code}   ${200}

Test Post /tracking/events/resetcounters Functionality
	${response}=	API::Send Get Request	/tracking/events
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Contain	${resp}	'id': '302', 'category': 'Device Event', 'event_number': '302', 'description': 'Nodegrid Device Created', 'occurrences': '0'

*** Keywords ***
SUITE:Setup
	API::Post::Session
	SUITE:Add Device and Increase Counter

SUITE:Teardown
	SUITE:Delete Device
	API::Delete::Session

SUITE:Add Device and Increase Counter
	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	type=${TYPE}
	Set To Dictionary	${payload}	name=${NAME}
	Set To Dictionary	${payload}	ip_address=${IP_ADDRESS}
	Set To Dictionary	${payload}	mode=${MODE}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table
	Should Be Equal   ${response.status_code}   ${200}
	SUITE:Check Counter Increased

SUITE:Check Counter Increased
	${response}=	API::Send Get Request	/tracking/events
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should not Contain	${resp}	'id': '302', 'category': 'Device Event', 'event_number': '302', 'description': 'Nodegrid Device Created', 'occurrences': '0'

SUITE:Delete Device
	${PAYLOAD}=	Set Variable	{"devices":["${NAME}"]}
	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table
	Should Be Equal   ${response.status_code}   ${200}