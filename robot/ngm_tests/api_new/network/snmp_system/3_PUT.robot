*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${SYSCONTACT}=	support@zpesystems.com
${SYSLOCATION}=	nodegrid

*** Test Cases ***

Update SNMP entry (status_code = 200)
	[Documentation]	Update SNMP entry. Status code 200 is expected. 
	...	Endpoint: /network/snmp_system

	${PAYLOAD}=	Set Variable	{"syscontact":"${SYSCONTACT}", "syslocation":"${SYSLOCATION}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/snmp_system
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session