*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${CONNECT_AUTOMATICALLY}=	true
${ETHERNET_INTERFACE}=	eth1
${NAME}=	test_connection
${TYPE}=	bridge
${BRIDGE_INTERFACES}=	eth1

*** Test Cases ***
#commented because it breaks the device
Add network connections (status_code = 200)
	[Documentation]	Add network connections. Status code 200 is expected.
	...	Endpoint: /network/connections

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	connect_automatically=${CONNECT_AUTOMATICALLY}
	Set To Dictionary	${payload}	ethernet_interface=${ETHERNET_INTERFACE}
	Set To Dictionary	${payload}	name=${NAME}
	Set To Dictionary	${payload}	type=${TYPE}
	Set To Dictionary	${payload}	bridge_interfaces=${BRIDGE_INTERFACES}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/connections
	Should Be Equal   ${response.status_code}   ${200}

Test Post /network/connections Functionality
	${response}=	API::Send Get Request	/network/connections
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Contain	${resp}	${NAME}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session