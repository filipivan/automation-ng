*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***


*** Test Cases ***

Down network connection (status_code = 200)
	[Documentation]	Down network connection. Status code 200 is expected. 
	...	Endpoint: /network/connections/{connection}/down

	${PAYLOAD}=	Create Dictionary

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/connections/test_connection/down
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}

Test Put /network/connections/{connection}/down Functionality
	${response}=	API::Send Get Request	/network/connections/
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Contain	${resp}	{'id': 'test_connection', 'description': '', 'autoConn': 'no', 'name': 'test_connection', 'status': 'not active', 'type': 'bridge', 'interface': 'br0', 'carrier_state': 'down', 'ipv4_address': ' ', 'ipv6_address': ' ', 'mac_address': ' '}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session