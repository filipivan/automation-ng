*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${CONNECT_AUTOMATICALLY}=	false

*** Test Cases ***

Update network connections (status_code = 200)
	[Documentation]	Update network connections. Status code 200 is expected. 
	...	Endpoint: /network/connections/{connection}

	${PAYLOAD}=	Set variable	{"connect_automatically":${CONNECT_AUTOMATICALLY}}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/connections/test_connection
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}

Test Put /network/connections/{connection} Functionality
	${response}=	API::Send Get Request	/network/connections
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Contain	${resp}	{'id': 'test_connection', 'description': '', 'autoConn': 'no'

*** Keywords ***
SUITE:Setup
	API::Post::Session
	SUITE:Check Connect Automatically Yes

SUITE:Teardown
	API::Delete::Session

SUITE:Check Connect Automatically Yes
	${response}=	API::Send Get Request	/network/connections
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Not Contain	${resp}	{'id': 'test_connection', 'description': '', 'autoConn': 'yes'