*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***


*** Test Cases ***

Up network connection (status_code = 200)
	[Documentation]	Up network connection. Status code 200 is expected. 
	...	Endpoint: /network/connections/{connection}/up

	${PAYLOAD}=	Create Dictionary

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/connections/test_connection/up
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}

Test Put /network/connections/{connection}/up Functionality
	Skip If	not ${ETH1_HAS_IP}	interface will never go up because it has no ip address
	Wait Until Keyword Succeeds	5 times	20s	SUITE:Check Connection Up

*** Keywords ***
SUITE:Setup
	API::Post::Session
	${ETH1_HAS_IP}	SUITE:Check if ETH1 has IP Address	#later we can use GSM to make sure it'll have GSM
	Set Suite Variable	${ETH1_HAS_IP}

SUITE:Teardown
	SUITE:Interface Down
	API::Delete::Session

SUITE:Interface Down
	${PAYLOAD}=	Create Dictionary
	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/connections/test_connection/down
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}

SUITE:Check Connection Up
	${response}=	API::Send Get Request	/network/connections/
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Contain	${resp}	{'id': 'test_connection', 'description': '', 'autoConn': 'no', 'name': 'test_connection', 'status': 'connected', 'type': 'bridge', 'interface': 'br0', 'carrier_state': 'up

SUITE:Check if ETH1 has IP Address
	${HAS_INTERFACE}	API::Has Interface	ETH1
	IF	${HAS_INTERFACE}
		${HAS_INTERFACE}	API::Interface Has IP_Address	ETH1
	END
	[Return]	${HAS_INTERFACE}