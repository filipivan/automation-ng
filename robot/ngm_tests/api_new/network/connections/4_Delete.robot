*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${CONNECTIONS}=	"test_connection"

*** Test Cases ***

Delete network connections (status_code = 200)
	[Documentation]	Delete network connections. Status code 200 is expected. 
	...	Endpoint: /network/connections

	${PAYLOAD}=	Set variable	{"connections":[${CONNECTIONS}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/network/connections
	Should Be Equal   ${response.status_code}   ${200}

Test Delete /network/connections Functionality
	${response}=	API::Send Get Request	/network/connections
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Not Contain	${resp}	${CONNECTIONS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session