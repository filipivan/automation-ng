*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${IP_RANGE_START}=	192.168.18.1
${IP_RANGE_END}=	192.168.18.192
${SUBNET}=	192.168.18.0
${NETMASK}=	255.255.255.0

*** Test Cases ***

Add DHCP network range information (status_code = 200)
	[Documentation]	Add DHCP network range information. Status code 200 is expected. 
	...	Endpoint: /network/dhcp/{dhcpdip}/networkrange

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	ip_address_start=${IP_RANGE_START}
	Set To Dictionary	${payload}	ip_address_end=${IP_RANGE_END}
	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/dhcp/${SUBNET}/${NETMASK}/networkrange
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session