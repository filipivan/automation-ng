*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SUBNET}=	192.168.18.0
${NETMASK}=	255.255.255.0

*** Test Cases ***

Get DHCP network range list (status_code = 200)
	[Documentation]	Get DHCP network range list. Status code 200 is expected. 
	...	Endpoint: /network/dhcp/{dhcpdip}/networkrange

	${BODY}=	Evaluate   str({'id': '192.168.18.1/192.168.18.192', 'ip_range': '192.168.18.1/192.168.18.192'})

	API::Check Body Request    PATH=/network/dhcp/${SUBNET}/${NETMASK}/networkrange	BODY_EXPECTED=${BODY}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session