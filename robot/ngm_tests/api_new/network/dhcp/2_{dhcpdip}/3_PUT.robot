*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${DOMAIN_NAME_SERVERS}=	8.8.4.4
${ROUTER_IP}=	192.168.1.1
${DOMAIN}=	www.zpesystems.com
${LEASETIME}=	20000

*** Test Cases ***
Update DHCP server (status_code = 200)
	[Documentation]	Update DHCP server. Status code 200 is expected. 
	...	Endpoint: /network/dhcp/{dhcpdip}

	${PAYLOAD}=	Set variable	{"domain_name_servers":"${DOMAIN_NAME_SERVERS}", "router_ip":"${ROUTER_IP}", "domain":"${DOMAIN}", "leasetime":"${LEASETIME}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/dhcp/192.168.18.0/255.255.255.0
	Should Be Equal   ${response.status_code}   ${200}


Test Update DHCP server (status_code = 200)
	[Documentation]	Test Update DHCP server. Status code 200 is expected. 
	...	Endpoint: /network/dhcp/{dhcpdip}
	${BODY_EXPECTED}=	Evaluate	str({'domain': '${DOMAIN}', 'domain_name_servers': '${DOMAIN_NAME_SERVERS}', 'router_ip': '${ROUTER_IP}', 'lease_time': '${LEASETIME}', 'protocol': 'DHCP4', 'subnet/netmask_or_prefix/length': '192.168.18.0/255.255.255.0'})
	API::Check Body Request    PATH=/network/dhcp/192.168.18.0/255.255.255.0    BODY_EXPECTED=${BODY_EXPECTED}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session