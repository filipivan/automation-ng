*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${HOSTNAME}=	myhostname
${HW_ADDRESS}=	00:0a:95:9d:68:16
${IP_ADDRESS}=	192.168.16.10
${SUBNET}=	192.168.18.0
${NETMASK}=	255.255.255.0

*** Test Cases ***

Add DHCP network range information (status_code = 200)
	[Documentation]	Add DHCP network range information. Status code 200 is expected. 
	...	Endpoint: /network/dhcp/{dhcpdip}/hosts

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	hostname=${HOSTNAME}
	Set To Dictionary	${payload}	hw_address=${HW_ADDRESS}
	Set To Dictionary	${payload}	ip_address=${IP_ADDRESS}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/dhcp/${SUBNET}/${NETMASK}/hosts
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session