*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SUBNET}=	192.168.18.0
${NETMASK}=	255.255.255.0

*** Test Cases ***

Get DHCP hosts list (status_code = 200)
	[Documentation]	Get DHCP hosts list. Status code 200 is expected. 
	...	Endpoint: /network/dhcp/{dhcpdip}/hosts

	${response}=	API::Send Get Request	/network/dhcp/${SUBNET}/${NETMASK}/hosts
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session