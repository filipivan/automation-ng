*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${SUBNET}=	192.168.18.0
${NETMASK}=	255.255.255.0
${PROTOCOL}=	ipv4
${DOMAIN}=	zpesystems.com
${DOMAIN_NAME_SERVERS}=	8.8.8.8
${ROUTER_IP}=	192.168.16.1
${LEASETIME}=	20000

*** Test Cases ***

Add DHCP server (status_code = 200)
	[Documentation]	Add DHCP server. Status code 200 is expected. 
	...	Endpoint: /network/dhcp

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	subnet=${SUBNET}
	Set To Dictionary	${payload}	netmask=${NETMASK}
	Set To Dictionary	${payload}	protocol=${PROTOCOL}
	Set To Dictionary	${payload}	domain=${DOMAIN}
	Set To Dictionary	${payload}	domain_name_servers=${DOMAIN_NAME_SERVERS}
	Set To Dictionary	${payload}	router_ip=${ROUTER_IP}
	Set To Dictionary	${payload}	leasetime=${LEASETIME}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/dhcp
	Should Be Equal   ${response.status_code}   ${200}

Test POST /network/dhcp Functionality
  [Documentation]    Check that POST returns the correct field and values using GET
  
	${BODY}=	Evaluate   str({'id': '${SUBNET}/${NETMASK}', 'leasetime': '${LEASETIME}', 'subnet/netmask': '${SUBNET}/${NETMASK}', 'domain': '${DOMAIN}', 'domain_name_servers': '${DOMAIN_NAME_SERVERS}', 'router_ip': '${ROUTER_IP}'})
	API::Check Body Request  PATH=/network/dhcp	BODY_EXPECTED=${BODY}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session