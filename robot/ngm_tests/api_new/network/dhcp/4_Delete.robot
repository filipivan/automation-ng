*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${SUBNET}=	192.168.18.0
${NETMASK}=	255.255.255.0

*** Test Cases ***

Delete DHCP servers (status_code = 200)
	[Documentation]	Delete DHCP servers. Status code 200 is expected. 
	...	Endpoint: /network/dhcp

	${PAYLOAD}=	Set variable	{"dhcps":["${SUBNET}/${NETMASK}"]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/network/dhcp
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session