*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${LINK_NAME}=	api_link
${INTERFACE_NAME}=	eth0
${PROBES_PER_SECOND}=	2
${IDLE_TIME}=	5

*** Test Cases ***

Add SD-WAN Link Profiles (status_code = 200)
	[Documentation]	Add SD-WAN Link Profiles. Status code 200 is expected. 
	...	Endpoint: /network/sdwan/link_profile

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	link_name=${LINK_NAME}
	Set To Dictionary	${payload}	interface_name=${INTERFACE_NAME}
	Set To Dictionary	${payload}	probes_per_second=${PROBES_PER_SECOND}
	Set To Dictionary	${payload}	idle_time=${IDLE_TIME}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/sdwan/link_profile
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session