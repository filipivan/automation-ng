*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${LINK_PROFILE}=	"api_link"

*** Test Cases ***

Delete SD-WAN Link Profiles (status_code = 200)
	[Documentation]	Delete SD-WAN Link Profiles. Status code 200 is expected. 
	...	Endpoint: /network/sdwan/link_profile

	${PAYLOAD}=	Set Variable	{"link_profile":[${LINK_PROFILE}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/network/sdwan/link_profile
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session