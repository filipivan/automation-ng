*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${NAME}=	api_quality
${METHOD}=	aggressive
${LATENCY_THRESHOLD}=	200

*** Test Cases ***

Add SD-WAN Path Quality (status_code = 200)
	[Documentation]	Add SD-WAN Path Quality. Status code 200 is expected. 
	...	Endpoint: /network/sdwan/path_quality

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${NAME}
	Set To Dictionary	${payload}	method=${METHOD}
	Set To Dictionary	${payload}	latency_threshold=${LATENCY_THRESHOLD}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/sdwan/path_quality
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session