*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${LATENCY_THRESHOLD}=	200

*** Test Cases ***

Edit SD-WAN Path Quality Profile (status_code = 200)
	[Documentation]	Edit SD-WAN Path Quality Profile. Status code 200 is expected. 
	...	Endpoint: /network/sdwan/path_quality/{path_quality}

	${PAYLOAD}=	Set Variable	{"latency_threshold":"${LATENCY_THRESHOLD}", "sdwanPathQualityName":"api_quality"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/sdwan/path_quality/api_quality
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session