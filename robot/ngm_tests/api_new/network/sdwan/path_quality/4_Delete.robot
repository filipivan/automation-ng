*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${PATH_QUALITY}=	"api_quality"

*** Test Cases ***

Delete SD-WAN Path Quality (status_code = 200)
	[Documentation]	Delete SD-WAN Path Quality. Status code 200 is expected. 
	...	Endpoint: /network/sdwan/path_quality

	${PAYLOAD}=	Set Variable	{"path_quality":[${PATH_QUALITY}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/network/sdwan/path_quality
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session