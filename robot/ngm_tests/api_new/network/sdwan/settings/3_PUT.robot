*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${ENABLE_SD-WAN}=	true
${DISABLE_SD-WAN}=	false

*** Test Cases ***

Update SD-WAN Settings Configuration (status_code = 200)
	[Documentation]	Update SD-WAN Settings Configuration. Status code 200 is expected. 
	...	Endpoint: /network/sdwan/settings

	${PAYLOAD}=	Set Variable	{"enable_sd-wan":"${ENABLE_SD-WAN}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/sdwan/settings
	Should Be Equal   ${response.status_code}   ${200}

Disable SD-WAN Settings Configuration (status_code = 200)
	[Documentation]	Update SD-WAN Settings Configuration. Status code 200 is expected. 
	...	Endpoint: /network/sdwan/settings

	${PAYLOAD}=	Set Variable	{"enable_sd-wan":"${DISABLE_SD-WAN}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/sdwan/settings
	Should Be Equal   ${response.status_code}   ${200}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session