*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${PATH_STEERING}=	"api_steering"

*** Test Cases ***

Delete SD-WAN Path Steering (status_code = 200)
	[Documentation]	Delete SD-WAN Path Steering. Status code 200 is expected. 
	...	Endpoint: /network/sdwan/path_steering

	${PAYLOAD}=	Set Variable	{"path_steering":[${PATH_STEERING}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/network/sdwan/path_steering
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session