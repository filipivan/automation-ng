*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${MEASUREMENT_TARGET}=	measurement.zpecloud.com

*** Test Cases ***

Edit SD-WAN Path Steering Profile (status_code = 200)
	[Documentation]	Edit SD-WAN Path Steering Profile. Status code 200 is expected. 
	...	Endpoint: /network/sdwan/path_steering/{path_steering}

		${PAYLOAD}	Set Variable	{"measurement_target":"${MEASUREMENT_TARGET}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/sdwan/path_steering/api_steering
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session