*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${NAME}=	api_steering
${MEASUREMENT_PROTOCOL}=	ping


*** Test Cases ***
#TODO: check why it works manually but doesn't work here
Add SD-WAN Path Steering (status_code = 200)
	[Documentation]	Add SD-WAN Path Steering. Status code 200 is expected. 
	...	Endpoint: /network/sdwan/path_steering

	${LINK_PRIORITY}	Create List	broadband_1

	${PAYLOAD}	Create Dictionary
	Set To Dictionary	${payload}	name=${NAME}
	Set To Dictionary	${payload}	measurement_protocol=${MEASUREMENT_PROTOCOL}
	Set To Dictionary	${payload}	link_priority=${LINK_PRIORITY}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/sdwan/path_steering
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session