*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${CONNECTION}=	ETH0
${TYPE}=	ipv4
${DESTINATION_IP}=	127.0.0.1
${DESTINATION_BITMASK}=	255.255.255.254
${GATEWAY_IP}=	127.0.0.1
${METRIC}=	100

*** Test Cases ***

Add static route (status_code = 200)
	[Documentation]	Add static route. Status code 200 is expected. 
	...	Endpoint: /network/staticroutes

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	connection=${CONNECTION}
	Set To Dictionary	${payload}	type=${TYPE}
	Set To Dictionary	${payload}	destination_ip=${DESTINATION_IP}
	Set To Dictionary	${payload}	destination_bitmask=${DESTINATION_BITMASK}
	Set To Dictionary	${payload}	gateway_ip=${GATEWAY_IP}
	Set To Dictionary	${payload}	metric=${METRIC}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/staticroutes
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session