*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${STATIC_ROUTES}=	ETH0
${TYPE}=	ipv4
${ROUTE}=	route1

*** Test Cases ***

Delete static route (status_code = 200)
	[Documentation]	Delete static route. Status code 200 is expected. 
	...	Endpoint: /network/staticroutes

	${PAYLOAD}=	Set Variable	{"static_routes":["${STATIC_ROUTES}_${TYPE}_${ROUTE}"]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/network/staticroutes
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session