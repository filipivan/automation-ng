*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${GATEWAY_IP}=	192.168.15.1
${DESTINATION_BITMASK}=	255.255.255.1
${STATIC_ROUTES}=	ETH0
${TYPE}=	ipv4
${ROUTE}=	route1

*** Test Cases ***

Update static route (status_code = 200)
	[Documentation]	Update static route. Status code 200 is expected. 
	...	Endpoint: /network/staticroutes/{route}

	${PAYLOAD}=	Set Variable	{"gateway_ip":"${GATEWAY_IP}", "destination_bitmask":"${DESTINATION_BITMASK}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/staticroutes/${STATIC_ROUTES}|${TYPE}|${ROUTE}
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session