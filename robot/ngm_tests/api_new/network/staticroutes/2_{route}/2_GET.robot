*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${STATIC_ROUTES}=	ETH0
${TYPE}=	ipv4
${ROUTE}=	route1

*** Test Cases ***

Get static route (status_code = 200)
	[Documentation]	Get static route. Status code 200 is expected. 
	...	Endpoint: /network/staticroutes/{route}

	${response}=	API::Send Get Request	/network/staticroutes/${STATIC_ROUTES}|${TYPE}|${ROUTE}
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session