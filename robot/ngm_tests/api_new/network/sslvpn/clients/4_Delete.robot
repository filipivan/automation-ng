*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${SSL_VPNS}=	"test_client_vpn"

*** Test Cases ***

Delete SSL VPN Client (status_code = 200)
	[Documentation]	Delete SSL VPN Client. Status code 200 is expected. 
	...	Endpoint: /network/sslvpn/clients

	${PAYLOAD}=	Set Variable	{"ssl_vpns":[${SSL_VPNS}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/network/sslvpn/clients
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session