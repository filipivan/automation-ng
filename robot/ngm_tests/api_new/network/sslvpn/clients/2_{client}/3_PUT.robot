*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${NAME}=	test_client_vpn
${GATEWAY_IP_ADDRESS}=	192.168.1.110
${GATEWAY_TCP_PORT}=	1194
${TUNNEL_MTU}=	1500
${USERNAME}=	authUsername
${PASSWORD}=	authPasswd

*** Test Cases ***

Add SSL VPN Client (status_code = 200)
	[Documentation]	Add SSL VPN Client. Status code 200 is expected. 
	...	Endpoint: /network/sslvpn/clients/{client}

	${PAYLOAD}	Set variable	{"gateway_ip_address":"${GATEWAY_IP_ADDRESS}","gateway_tcp_port":"${GATEWAY_TCP_PORT}","name":"${NAME}","password":"${PASSWORD}","tunnel_mtu":"${TUNNEL_MTU}","username":"${USERNAME}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/sslvpn/clients/test_client_vpn
	Should Be Equal   ${response.status_code}   ${200}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session