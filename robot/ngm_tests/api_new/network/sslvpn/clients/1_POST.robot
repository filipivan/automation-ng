*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${NAME}=	test_client_vpn
${GATEWAY_IP_ADDRESS}=	192.168.1.1
${NETWORK_CONNECTION}=	NONE

*** Test Cases ***

Add SSL VPN Client (status_code = 200)
	[Documentation]	Add SSL VPN Client. Status code 200 is expected. 
	...	Endpoint: /network/sslvpn/clients

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${NAME}
	Set To Dictionary	${payload}	gateway_ip_address=${GATEWAY_IP_ADDRESS}
	Set To Dictionary	${payload}	network_connection=${NETWORK_CONNECTION}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/sslvpn/clients
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session