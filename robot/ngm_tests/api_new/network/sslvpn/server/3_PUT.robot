*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${STATUS}=	no
${LISTEN_IP_ADDRESS}=	192.168.1.1

*** Test Cases ***

Update SSL VPN Server Configuration (status_code = 200)
	[Documentation]	Update SSL VPN Server Configuration. Status code 200 is expected. 
	...	Endpoint: /network/sslvpn/server

	${PAYLOAD}=	Set Variable	{"status":"${STATUS}", "listen_ip_address":"${LISTEN_IP_ADDRESS}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/sslvpn/server
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session