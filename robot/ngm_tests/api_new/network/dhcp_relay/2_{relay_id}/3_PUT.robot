*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SERVERS}=	192.168.15.1, 192.168.160.254
${ENABLE_OPTION_82}=	false

*** Test Cases ***

Update DHCP relay (status_code = 200)
	[Documentation]	Update DHCP relay. Status code 200 is expected. 
	...	Endpoint: /network/dhcp_relay/{relay_id}

	${PAYLOAD}=	Set Variable	{"servers":"${SERVERS}", "enable_option_82":"${ENABLE_OPTION_82}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/dhcp_relay/relay1
	Should Be Equal   ${response.status_code}   ${200}

Test Update DHCP relay (status_code = 200)
	[Documentation]	Test Update DHCP relay. Status code 200 is expected. 
	...	Endpoint: /network/dhcp_relay/{relay_id}
	...	
	${BODY_EXPECTED}=	Evaluate	str({'servers': '192.168.15.1,192.168.160.254', 'interfaces': '', 'incoming_option_82_policy': 'replace', 'enable_option_82': False, 'protocol': 'DHCPv4'})
	API::Check Body Request    PATH=/network/dhcp_relay/relay1	BODY_EXPECTED=${BODY_EXPECTED}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session