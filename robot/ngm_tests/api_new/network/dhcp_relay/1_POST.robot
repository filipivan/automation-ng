*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${PROTOCOL}=	DHCPv4
${SERVERS}=	10.20.30.1
${ENABLE_OPTION_82}=	yes
${INCOMING_OPTION_82_POLICY}=	replace_option_82

*** Test Cases ***

Add DHCP relay (status_code = 200)
	[Documentation]	Add DHCP relay. Status code 200 is expected. 
	...	Endpoint: /network/dhcp_relay

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	protocol=${PROTOCOL}
	Set To Dictionary	${payload}	servers=${SERVERS}
	Set To Dictionary	${payload}	enable_option_82=${ENABLE_OPTION_82}
	Set To Dictionary	${payload}	incoming_option_82_policy=${INCOMING_OPTION_82_POLICY}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/dhcp_relay
	Should Be Equal   ${response.status_code}   ${200}

Test POST /network/dhcp Functionality
  [Documentation]    Check that POST returns the correct field and values using GET
  
	${BODY}=	Evaluate   str({'protocol': '${PROTOCOL}', 'servers': '${SERVERS}', 'interfaces': '', 'server_interfaces': '-', 'client_interfaces': '-', 'index': 'relay1', 'option_82': 'enabled'})
	API::Check Body Request  PATH=/network/dhcp_relay	BODY_EXPECTED=${BODY}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session