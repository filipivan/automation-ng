*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${IP_ADDRESS}=	192.168.1.6
${HOSTNAME}=	changedhost

*** Test Cases ***

Update static route (status_code = 200)
	[Documentation]	Update static route. Status code 200 is expected. 
	...	Endpoint: /network/hosts/{host}

	${PAYLOAD}=	Set Variable	{"hostname":"${HOSTNAME}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/hosts/${IP_ADDRESS}
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session