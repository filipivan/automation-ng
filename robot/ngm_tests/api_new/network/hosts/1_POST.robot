*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${IP_ADDRESS}=	192.168.1.6
${HOSTNAME}=	localhost
${ALIAS}=	localhost

*** Test Cases ***

Add host (status_code = 200)
	[Documentation]	Add host. Status code 200 is expected. 
	...	Endpoint: /network/hosts

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	ip_address=${IP_ADDRESS}
	Set To Dictionary	${payload}	hostname=${HOSTNAME}
	Set To Dictionary	${payload}	alias=${ALIAS}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/hosts
	Should Be Equal   ${response.status_code}   ${200}

Test POST /network/hosts Functionality
  [Documentation]    Check that POST returns the correct field and values using GET

	${BODY}=	Evaluate   str({'id': '${IP_ADDRESS}', 'alias': '${ALIAS}', 'ip_address': '${IP_ADDRESS}', 'hostname': '${HOSTNAME}'})
	API::Check Body Request  PATH=/network/hosts	BODY_EXPECTED=${BODY}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session