*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${ACCESS_TYPE}=	read_only

*** Test Cases ***

Update SNMP entry (status_code = 200)
	[Documentation]	Update SNMP entry. Status code 200 is expected. 
	...	Endpoint: /network/snmp/{snmp}

	${PAYLOAD}=	Set Variable	{"access_type":"${ACCESS_TYPE}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/snmp/community|test_snmp~default
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session