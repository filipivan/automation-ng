*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${COMMUNITY}=	test_snmp
${ACCESS_TYPE}=	read_only
${VERSION}=	version v1/v2
${SOURCE}=	default
${OID}=	
${SNMP_FOR_IPV6}=	false

*** Test Cases ***

Add SNMP entry (status_code = 200)
	[Documentation]	Add SNMP entry. Status code 200 is expected. 
	...	Endpoint: /network/snmp

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	community=${COMMUNITY}
	Set To Dictionary	${payload}	access_type=${ACCESS_TYPE}
	Set To Dictionary	${payload}	version=${VERSION}
	Set To Dictionary	${payload}	source=${SOURCE}
	Set To Dictionary	${payload}	oid=${OID}
	Set To Dictionary	${payload}	snmp_for_ipv6=${SNMP_FOR_IPV6}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/snmp
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session