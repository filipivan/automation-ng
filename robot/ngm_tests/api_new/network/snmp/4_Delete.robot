*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${SNMPS}=	"community|test_snmp~default"

*** Test Cases ***

Delete SNMP entries (status_code = 200)
	[Documentation]	Delete SNMP entries. Status code 200 is expected. 
	...	Endpoint: /network/snmp

	${PAYLOAD}=	Set Variable	{"snmps":[${SNMPS}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/network/snmp
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session