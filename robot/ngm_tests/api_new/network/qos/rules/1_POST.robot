*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${NAME}=	SSH_dst
${PROTOCOL}=	tcp,udp
${SOURCE_IP}=	
${DESTINATION_IP}=	
${SOURCE_PORT}=	
${DESTINATION_PORT}=	22
${SOURCE_MAC}=	
${DESTINATION_MAC}=	
${EXTRA}=	
${ENABLED}=	:true,syn:false,ack:false,ipv4:true,ipv6:true
${CLASSES}=	Create List

*** Test Cases ***

Create a new Network QoS rule configuration (status_code = 200)
	[Documentation]	Create a new Network QoS rule configuration. Status code 200 is expected. 
	...	Endpoint: /network/qos/rules

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${NAME}
	Set To Dictionary	${payload}	protocol=${PROTOCOL}
	Set To Dictionary	${payload}	source_ip=${SOURCE_IP}
	Set To Dictionary	${payload}	destination_ip=${DESTINATION_IP}
	Set To Dictionary	${payload}	source_port=${SOURCE_PORT}
	Set To Dictionary	${payload}	destination_port=${DESTINATION_PORT}
	Set To Dictionary	${payload}	source_mac=${SOURCE_MAC}
	Set To Dictionary	${payload}	destination_mac=${DESTINATION_MAC}
	Set To Dictionary	${payload}	extra=${EXTRA}
	Set To Dictionary	${payload}	enabled=${ENABLED}
	Set To Dictionary	${payload}	classes=${CLASSES}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/qos/rules
	Should Be Equal   ${response.status_code}   ${200}

#Create a new Network QoS rule configuration (status_code = 400)
#	[Documentation]	Create a new Network QoS rule configuration. Status code 400 is expected. 
#	...	Endpoint: /network/qos/rules

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	name=${NAME}
#	Set To Dictionary	${payload}	protocol=${PROTOCOL}
#	Set To Dictionary	${payload}	source_ip=${SOURCE_IP}
#	Set To Dictionary	${payload}	destination_ip=${DESTINATION_IP}
#	Set To Dictionary	${payload}	source_port=${SOURCE_PORT}
#	Set To Dictionary	${payload}	destination_port=${DESTINATION_PORT}
#	Set To Dictionary	${payload}	source_mac=${SOURCE_MAC}
#	Set To Dictionary	${payload}	destination_mac=${DESTINATION_MAC}
#	Set To Dictionary	${payload}	extra=${EXTRA}
#	Set To Dictionary	${payload}	enabled=${ENABLED}
#	Set To Dictionary	${payload}	classes=${CLASSES}
#
#	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/qos/rules
#	Should Be Equal   ${response.status_code}   ${400}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session