*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${QOS_FORM_RULE_NAME}=	SSH_dst
${PROTOCOL}=	tcp,udp

${SOURCE_IP}=	127.0.0.1
${SOURCE_PORT}=	22
${SOURCE_MAC}=	${EMPTY}

${DESTINATION_IP}=	127.0.0.1
${DESTINATION_PORT}=	22
${DESTINATION_MAC}=	${EMPTY}

${extra}=	22
${SYN}=	True
${ACK}=	True
${IPV4}=	True
${IPV6}=	True
${CLASSES}=	SSH_Traffic
${ENABLED}=	True

*** Test Cases ***
Change an existing Network QoS rule configuration (status_code = 200)
	[Documentation]	Change an existing Network QoS rule configuration. Status code 200 is expected. 
	...	Endpoint: /network/qos/rules/{rule-id}

	${PAYLOAD}=	Set Variable	{"qos_form_rule_name": "${QOS_FORM_RULE_NAME}", "protocol": "${PROTOCOL}", "source_ip": "${SOURCE_IP}", "destination_ip": "${DESTINATION_IP}", "source_port": "${SOURCE_PORT}", "destination_port": "${DESTINATION_PORT}", "source_mac": "${SOURCE_MAC}", "destination_mac":"${DESTINATION_MAC}", "extra":"${extra}", "enabled":"${ENABLED}", "syn":"${SYN}", "ack":"${ACK}", "ipv4":"${IPV4}", "ipv6":"${IPV6}", "classes":"[${CLASSES}]"}
	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/qos/rules/SSH_dst
	Should Be Equal   ${response.status_code}   ${200}

Check change configuration an existing Network QoS rule
	[Documentation]	Verify change an existing network QoS rule configuration.
	...	Endpoint: /network/qos/rules/SSH_dst
	[Tags]	NON-CRITICAL	BUG_NG_12326
	
	${BODY_EXPECTED}=	Set Variable	{'protocol': '${PROTOCOL}', 'source_ip': '${SOURCE_IP}', 'destination_ip': '${DESTINATION_IP}', 'source_port': '${SOURCE_PORT}', 'destination_port': '${DESTINATION_PORT}', 'source_mac': '${SOURCE_MAC}', 'destination_mac': '${DESTINATION_MAC}', 'extra': '${extra}', 'enabled': ${ENABLED}, 'ipv4': ${IPV4}, 'ipv6': ${IPV6}, 'syn': ${SYN}, 'ack': ${ACK}, 'name': '${QOS_FORM_RULE_NAME}'}
	API::Check Body Request    BODY_EXPECTED=${BODY_EXPECTED}	PATH=/network/qos/rules/SSH_dst

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session