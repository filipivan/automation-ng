*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Show information about a Network QoS rule (status_code = 200)
	[Documentation]	Show information about a Network QoS rule. Status code 200 is expected. 
	...	Endpoint: /network/qos/rules/{rule-id}

	${response}=	API::Send Get Request	/network/qos/rules/SSH_dst
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session