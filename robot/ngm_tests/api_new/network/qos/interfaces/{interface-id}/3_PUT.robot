*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${INTERFACE}=	loopback
${QOS_FORM_NAME}=	loopback
${EXTRA}=	${EMPTY}
${INPUT_BANDWIDTH}=	700
${OUTPUT_BANDWIDTH}=	200
${INPUT_BANDWIDTH_UNIT}=	Mbps
${OUTPUT_BANDWIDTH_UNIT}=	Mbps
${QOS_DIRECTION}=	bidirectional
${ENABLED}=	True
${CLASSES}=	SSH_Traffic

*** Test Cases ***

Change existing Network QoS configuration of an interface (status_code = 200)
	[Documentation]	Change existing Network QoS configuration of an interface. Status code 200 is expected. 
	...	Endpoint: /network/qos/interfaces/{interface-id}

	${PAYLOAD}=	Set Variable	{"qos_form_name": "${QOS_FORM_NAME}", "extra": "${EXTRA}", "input_bandwidth": "${INPUT_BANDWIDTH}", "output_bandwidth": "${OUTPUT_BANDWIDTH}", "input_bandwidth_unit": "${INPUT_BANDWIDTH_UNIT}", "output_bandwidth_unit": "${OUTPUT_BANDWIDTH_UNIT}", "qos_direction": "${QOS_DIRECTION}", "enabled": "${ENABLED}"}
	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/qos/interfaces/${INTERFACE}
	Should Be Equal   ${response.status_code}   ${200}

Check change an existing Network QoS configuration of an interface
	[Documentation]	Check change existing Network QoS configuration of an interface.
	...	Endpoint: /network/qos/interfaces/loopback
	[Tags]	NON-CRITICAL	BUG_NG_12326
	
	${BODY_EXPECTED}=	Set Variable	{'custom_parameters': '${EXTRA}', 'input_bandwidth': '${INPUT_BANDWIDTH}', 'output_bandwidth': '${OUTPUT_BANDWIDTH}', 'qos_direction': '${QOS_DIRECTION}', 'input_bandwidth_unit': '${INPUT_BANDWIDTH_UNIT}', 'output_bandwidth_unit': '${OUTPUT_BANDWIDTH_UNIT}', 'enabled': ${ENABLED}, 'classes': ['${CLASSES}'], 'interface': '${INTERFACE}'}
	API::Check Body Request	BODY_EXPECTED=${BODY_EXPECTED}	PATH=/network/qos/interfaces/${INTERFACE}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session