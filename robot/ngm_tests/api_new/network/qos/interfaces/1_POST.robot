*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${INTERFACE}=	loopback
${EXTRA}=	${EMPTY}
${INPUT_BANDWIDTH}=	800
${OUTPUT_BANDWIDTH}=	800
${INPUT_BANDWIDTH_UNIT}=	Mbit
${OUTPUT_BANDWIDTH_UNIT}=	Mbit
${QOS_DIRECTION}=	bidirectional
${ENABLED}=	true

${NAME}=	SSH_Traffic
${INPUT_RESERVED}=	100
${INPUT_MAX}=	75
${OUTPUT_RESERVED}=	50
${OUTPUT_MAX}=	75
${INPUT_RESERVED_UNIT}=	Mbps
${INPUT_MAX_UNIT}=	Mbps
${OUTPUT_RESERVED_UNIT}=	Mbps
${OUTPUT_MAX_UNIT}=	Mbps
${PRIORITY}=	1

*** Test Cases ***
Create a new Network interface QoS configuration (status_code = 200)
	[Documentation]	Create a new Network interface QoS configuration. Status code 200 is expected. 
	...	Endpoint: /network/qos/interfaces

	${CLASSES}	Create List	${NAME}

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	interface=${INTERFACE}
	Set To Dictionary	${payload}	extra=${EXTRA}
	Set To Dictionary	${payload}	input_bandwidth=${INPUT_BANDWIDTH}
	Set To Dictionary	${payload}	output_bandwidth=${OUTPUT_BANDWIDTH}
	Set To Dictionary	${payload}	input_bandwidth_unit=${INPUT_BANDWIDTH_UNIT}
	Set To Dictionary	${payload}	output_bandwidth_unit=${OUTPUT_BANDWIDTH_UNIT}
	Set To Dictionary	${payload}	qos_direction=${QOS_DIRECTION}
	Set To Dictionary	${payload}	enabled=${ENABLED}
	Set To Dictionary	${payload}	classes=${CLASSES}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/qos/interfaces
	Should Be Equal   ${response.status_code}   ${200}

#Create a new Network interface QoS configuration (status_code = 400)
#	[Documentation]	Create a new Network interface QoS configuration. Status code 400 is expected. 
#	...	Endpoint: /network/qos/interfaces

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	interface=${INTERFACE}
#	Set To Dictionary	${payload}	extra=${EXTRA}
#	Set To Dictionary	${payload}	input_bandwidth=${INPUT_BANDWIDTH}
#	Set To Dictionary	${payload}	output_bandwidth=${OUTPUT_BANDWIDTH}
#	Set To Dictionary	${payload}	input_bandwidth_unit=${INPUT_BANDWIDTH_UNIT}
#	Set To Dictionary	${payload}	output_bandwidth_unit=${OUTPUT_BANDWIDTH_UNIT}
#	Set To Dictionary	${payload}	qos_direction=${QOS_DIRECTION}
#	Set To Dictionary	${payload}	enabled=${ENABLED}
#	Set To Dictionary	${payload}	classes=${CLASSES}
#
#	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/qos/interfaces
#	Should Be Equal   ${response.status_code}   ${400}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session