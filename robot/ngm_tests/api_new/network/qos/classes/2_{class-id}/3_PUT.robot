*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${QOS_FORM_CLASS_NAME}=	SSH_Traffic
${CUSTOM_PARAMETERS}=	${EMPTY}
${INPUT_RESERVED_BANDWIDTH}=	10
${INPUT_MAX_BANDWIDTH}=	75
${OUTPUT_RESERVED_BANDWIDTH}=	50
${OUTPUT_MAX_BANDWIDTH}=	75
${INPUT_RESERVED_UNIT}=	Mbps
${INPUT_MAX_UNIT}=	Mbps
${OUTPUT_RESERVED_UNIT}=	Mbps
${OUTPUT_MAX_UNIT}=	Mbps
${PRIORITY}=	1
${ENABLED}=	True

*** Test Cases ***
Change an existing Network QoS class configuration (status_code = 200)
	[Documentation]	Change an existing Network QoS class configuration. Status code 200 is expected. 
	...	Endpoint: /network/qos/classes/{class-id}

	${PAYLOAD}=	Set Variable	{"qos_form_class_name": "${QOS_FORM_CLASS_NAME}", "custom_parameters": "${CUSTOM_PARAMETERS}", "input_reserved_bandwidth": "${INPUT_RESERVED_BANDWIDTH}", "input_max_bandwidth": "${INPUT_MAX_BANDWIDTH}", "output_reserved_bandwidth": "${OUTPUT_RESERVED_BANDWIDTH}", "output_max_bandwidth": "${OUTPUT_MAX_BANDWIDTH}", "input_reserved_unit": "${INPUT_RESERVED_UNIT}", "input_max_unit": "${INPUT_MAX_UNIT}", "output_reserved_unit": "${OUTPUT_RESERVED_UNIT}", "output_max_unit": "${OUTPUT_MAX_UNIT}", "priority": "${PRIORITY}", "enabled": "${ENABLED}"}
	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/qos/classes/SSH_Traffic
	Should Be Equal   ${response.status_code}   ${200}

Check change an existing Network QoS class configuration (status_code = 200)
	[Documentation]	Check change an existing Network QoS class configuration.
	...	Endpoint: /network/qos/classes/SSH_Traffic
	[Tags]	NON-CRITICAL	BUG_NG_12326

	${BODY_EXPECTED}=	Set Variable	{'custom_parameters': '${CUSTOM_PARAMETERS}', 'input_reserved_bandwidth': '${INPUT_RESERVED_BANDWIDTH}', 'input_max_bandwidth': '${INPUT_MAX_BANDWIDTH}', 'output_reserved_bandwidth': '${OUTPUT_RESERVED_BANDWIDTH}', 'output_max_bandwidth': '${OUTPUT_MAX_BANDWIDTH}', 'priority': '${PRIORITY}', 'input_reserved_unit': '${INPUT_RESERVED_UNIT}', 'input_max_unit': '${INPUT_MAX_UNIT}', 'output_reserved_unit': '${OUTPUT_RESERVED_UNIT}', 'output_max_unit': '${OUTPUT_MAX_UNIT}', 'enabled': '${ENABLED}', 'name': '${QOS_FORM_CLASS_NAME}'}
	API::Check Body Request	BODY_EXPECTED=${BODY_EXPECTED}	PATH=/network/qos/classes/SSH_Traffic

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session