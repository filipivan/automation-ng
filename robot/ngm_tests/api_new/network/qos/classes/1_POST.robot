*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${NAME}=	SSH_Traffic
${EXTRA}=	"Teste123"
${INPUT_RESERVED}=	75
${INPUT_MAX}=	100
${OUTPUT_RESERVED}=	50
${OUTPUT_MAX}=	75
${INPUT_RESERVED_UNIT}=	Mbps
${INPUT_MAX_UNIT}=	Mbps
${OUTPUT_RESERVED_UNIT}=	Mbps
${OUTPUT_MAX_UNIT}=	Mbps
${PRIORITY}=	2
${ENABLED}=	false

*** Test Cases ***

Create a new Network QoS class configuration (status_code = 200)
	[Documentation]	Create a new Network QoS class configuration. Status code 200 is expected. 
	...	Endpoint: /network/qos/classes

	${PAYLOAD}=	Create Dictionary

	#DONT CHANGE THIS VARIABLE TO ${INTERFACES} BECAUSE HAVE A PUBLIC VARIABLE WITH THIS NAME
	${API_POST_TEST_INTERFACES}	Create List	loopback

	Set To Dictionary	${payload}	name=${NAME}
	Set To Dictionary	${payload}	extra=${EXTRA}
	Set To Dictionary	${payload}	input_reserved=${INPUT_RESERVED}
	Set To Dictionary	${payload}	input_max=${INPUT_MAX}
	Set To Dictionary	${payload}	output_reserved=${OUTPUT_RESERVED}
	Set To Dictionary	${payload}	output_max=${OUTPUT_MAX}
	Set To Dictionary	${payload}	input_reserved_unit=${INPUT_RESERVED_UNIT}
	Set To Dictionary	${payload}	input_max_unit=${INPUT_MAX_UNIT}
	Set To Dictionary	${payload}	output_reserved_unit=${OUTPUT_RESERVED_UNIT}
	Set To Dictionary	${payload}	output_max_unit=${OUTPUT_MAX_UNIT}
	Set To Dictionary	${payload}	interfaces=${API_POST_TEST_INTERFACES} 
	Set To Dictionary	${payload}	priority=${PRIORITY}
	Set To Dictionary	${payload}	enabled=${ENABLED}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/qos/classes
	Should Be Equal   ${response.status_code}   ${200}

#Create a new Network QoS class configuration (status_code = 400)
#	[Documentation]	Create a new Network QoS class configuration duplicated. Status code 400 is expected. 
#	...	Endpoint: /network/qos/classes
#	
#	${PAYLOAD}=	Create Dictionary
#	${ENABLED}	Create List	loopback
#
#	Set To Dictionary	${payload}	name=${NAME}
#	Set To Dictionary	${payload}	extra=${EXTRA}
#	Set To Dictionary	${payload}	input_reserved=${INPUT_RESERVED}
#	Set To Dictionary	${payload}	input_max=${INPUT_MAX}
#	Set To Dictionary	${payload}	output_reserved=${OUTPUT_RESERVED}
#	Set To Dictionary	${payload}	output_max=${OUTPUT_MAX}
#	Set To Dictionary	${payload}	input_reserved_unit=${INPUT_RESERVED_UNIT}
#	Set To Dictionary	${payload}	input_max_unit=${INPUT_MAX_UNIT}
#	Set To Dictionary	${payload}	output_reserved_unit=${OUTPUT_RESERVED_UNIT}
#	Set To Dictionary	${payload}	output_max_unit=${OUTPUT_MAX_UNIT}
#	Set To Dictionary	${payload}	priority=${PRIORITY}
#	Set To Dictionary	${payload}	enabled=${ENABLED}
#
#	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/qos/classes
#	Should Be Equal   ${response.status_code}   ${400}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session