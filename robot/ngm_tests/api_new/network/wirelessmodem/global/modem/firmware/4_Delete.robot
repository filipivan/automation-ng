*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../init.robot
Force Tags	API	NON-CRITICAL
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${FILES}=	Create List

*** Test Cases ***

Delete wireless modem file (status_code = 200)
	[Documentation]	Delete modem file. Status code 200 is expected. 
	...	Endpoint: /network/wirelessmodem/global/${API_JENKINS_WIRELESSMODEM}/firmware

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	files=${FILES}
	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/network/wirelessmodem/global/${API_JENKINS_WIRELESSMODEM}/firmware
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

	${IS_NSR}	API::Is NSR
	Skip If	not ${IS_NSR}	Wireless Modem only available on NSR
	
SUITE:Teardown
	API::Delete::Session