*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../../init.robot
Force Tags	API	NON-CRITICAL
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${FILE_LOCATION}=	upgrade-local
${FILENAME}=	testfiles.zip

*** Test Cases ***

Upgrade wireless modem (status_code = 200)
	[Documentation]	Upgrade modem. Status code 200 is expected. 
	...	Endpoint: /network/wirelessmodem/global/${API_JENKINS_WIRELESSMODEM}/firmware/upgrade

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	file_location=${FILE_LOCATION}
	Set To Dictionary	${payload}	filename=${FILENAME}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/wirelessmodem/global/${API_JENKINS_WIRELESSMODEM}/firmware/upgrade
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

	${IS_NSR}	API::Is NSR
	Skip If	not ${IS_NSR}	Wireless Modem only available on NSR
	
SUITE:Teardown
	API::Delete::Session