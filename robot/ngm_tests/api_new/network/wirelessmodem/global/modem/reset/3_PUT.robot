*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../init.robot
Force Tags	API	NEED-CREATION
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***


*** Test Cases ***

Reset wireless modem (status_code = 200)
	[Documentation]	Reset modem. Status code 200 is expected. 
	...	Endpoint: /network/wirelessmodem/global/${API_JENKINS_WIRELESSMODEM}/reset

	${PAYLOAD}=	Create Dictionary

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/wirelessmodem/global/${API_JENKINS_WIRELESSMODEM}/reset
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

	${IS_NSR}	API::Is NSR
	Skip If	not ${IS_NSR}	Wireless Modem only available on NSR

SUITE:Teardown
	API::Delete::Session