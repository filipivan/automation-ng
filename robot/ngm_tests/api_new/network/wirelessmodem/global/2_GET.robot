*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***

Get wireless modem information (status_code = 200)
	[Documentation]	Get wireless modem information. Status code 200 is expected. 
	...	Endpoint: /network/wirelessmodem/global

	${response}=	API::Send Get Request	/network/wirelessmodem/global
	Should Be Equal   ${response.status_code}   ${200}

Get wireless modem information slot1 and set as global
	[Documentation]	Get wireless modem information. Status code 200 is expected. 
	...	Endpoint: /network/wirelessmodem/global
	${response}=	API::Send Get Request	/network/wirelessmodem/global/
	Should Be Equal   ${response.status_code}   ${200}
	
	${BODY}=   Evaluate   str(${response.json()}[0])
	${BODYSTR}=	String.Split String    ${BODY}    ,
	${SLOT}=	String.Split String    ${BODYSTR}[0]    :
	${SLOT}=	String.Replace String	replace_with=${EMPTY}	search_for='  string=${SLOT}[1]
	${SLOT}=	Remove String    ${SLOT}	${SPACE}
	Set Global Variable		${API_JENKINS_WIRELESSMODEM}	${SLOT}
	
*** Keywords ***
SUITE:Setup
	API::Post::Session

	${IS_NSR}	API::Is NSR
	Skip If	not ${IS_NSR}	Wireless Modem only available on NSR
	
SUITE:Teardown
	API::Delete::Session