*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${802_USERNAME1}=	user_noconfirmpassword
${802_USERNAME2}=	user_confirmpassword

*** Test Cases ***

Delete 802.1x credential entry (status_code = 200)
	[Documentation]	Delete 802.1x credential entry. Status code 200 is expected. 
	...	Endpoint: /network/802.1x/credentials

	${PAYLOAD}=	Set Variable	{"credentials": ["${802_USERNAME1}", "${802_USERNAME2}"]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/network/802.1x/credentials
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session