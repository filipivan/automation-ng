*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${802_USERNAME}=	user_noconfirmpassword
${AUTHENTICATION_METHOD_TTLS}=	TTLS
${AUTHENTICATION_METHOD_PEAP}=	PEAP
${ANONYMOUS_IDENTITY}=	TSET
${AUTHENTICATION_INNER_MD5}=	MD5
${AUTHENTICATION_INNER_PAP}=	PAP
${AUTHENTICATION_INNER_CHAP}=	CHAP
${AUTHENTICATION_INNER_MSCHAP}=	MSCHAP
${AUTHENTICATION_INNER_MSCHAPV2}=	MSCHAPV2

*** Test Cases ***

Update 802.1x credential information (status_code = 200)
	[Documentation]	Update 802.1x credential information. Status code 200 is expected. 
	...	Endpoint: /network/802.1x/credentials/{credential}

	${PAYLOAD}=	Set Variable	{"authentication_method":"${AUTHENTICATION_METHOD_PEAP}","anonymous_identity":"${ANONYMOUS_IDENTITY}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/802.1x/credentials/${802_USERNAME}
	Should Be Equal   ${response.status_code}   ${200}

Update 802.1x credential information to TTLS and PAP (status_code = 200)
	[Documentation]	Update 802.1x credential information. Status code 200 is expected. 
	...	Endpoint: /network/802.1x/credentials/{credential}
	
	${PAYLOAD}=	Set Variable	{"authentication_method":"${AUTHENTICATION_METHOD_TTLS}","inner_authentication":"${AUTHENTICATION_INNER_PAP}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/802.1x/credentials/${802_USERNAME}
	Should Be Equal   ${response.status_code}   ${200}

Update 802.1x credential information to TTLS and CHAP (status_code = 200)
	[Documentation]	Update 802.1x credential information. Status code 200 is expected. 
	...	Endpoint: /network/802.1x/credentials/{credential}

	${PAYLOAD}=	Set Variable	{"authentication_method":"${AUTHENTICATION_METHOD_TTLS}","inner_authentication":"${AUTHENTICATION_INNER_CHAP}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/802.1x/credentials/${802_USERNAME}
	Should Be Equal   ${response.status_code}   ${200}

Update 802.1x credential information to TTLS and MSCHAP (status_code = 200)
	[Documentation]	Update 802.1x credential information. Status code 200 is expected. 
	...	Endpoint: /network/802.1x/credentials/{credential}

	${PAYLOAD}=	Set Variable	{"authentication_method":"${AUTHENTICATION_METHOD_TTLS}","inner_authentication":"${AUTHENTICATION_INNER_MSCHAP}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/802.1x/credentials/${802_USERNAME}
	Should Be Equal   ${response.status_code}   ${200}

Update 802.1x credential information to TTLS and MSCHAPV2 (status_code = 200)
	[Documentation]	Update 802.1x credential information. Status code 200 is expected. 
	...	Endpoint: /network/802.1x/credentials/{credential}

	${PAYLOAD}=	Set Variable	{"authentication_method":"${AUTHENTICATION_METHOD_TTLS}","inner_authentication":"${AUTHENTICATION_INNER_MSCHAPV2}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/802.1x/credentials/${802_USERNAME}
	Should Be Equal   ${response.status_code}   ${200}

Update 802.1x credential information to TTLS and MD5 (status_code = 400)
	[Documentation]	Update 802.1x credential information. Status code 400 is expected. 
	...	Endpoint: /network/802.1x/credentials/{credential}
	
	${PAYLOAD}=	Set Variable	{"authentication_method":"${AUTHENTICATION_METHOD_TTLS}","inner_authentication":"${AUTHENTICATION_INNER_MD5}"}

	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/802.1x/credentials/${802_USERNAME}	ERROR_CONTROL=${FALSE}	EXPECT_ERROR=HTTPError: 400 Client Error: Bad Request for url: https://${HOST}/api/v1/network/802.1x/credentials/${802_USERNAME}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session