*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${802_USERNAME1}=	user_noconfirmpassword
${802_USERNAME2}=	user_confirmpassword
${PASSWORD}=	${QA_PASSWORD}
${AUTHENTICATION_METHOD}=	MD5

*** Test Cases ***

Add 802.1x credentials without confirm_password (status_code = 200)
	[Documentation]	Add entry in 802.1x credentials without using optoinal parameter "confirm_password". Status code 200 is expected.
	...	Endpoint: /network/802.1x/credentials

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	username=${802_USERNAME1}
	Set To Dictionary	${payload}	password=${PASSWORD}
	Set To Dictionary	${payload}	authentication_method=${AUTHENTICATION_METHOD}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/802.1x/credentials
	Should Be Equal   ${response.status_code}   ${200}
	
Test POST /network/802.1x/credentials Functionality without confirm_password
  [Documentation]    Check that POST returns the correct field and values using GET

	${BODY}=	Evaluate   str({'id': '${802_USERNAME1}', 'username': '${802_USERNAME1}', 'authentication_method': '${AUTHENTICATION_METHOD}'})
	API::Check Body Request  PATH=/network/802.1x/credentials	BODY_EXPECTED=${BODY}

Add new entry in 802.1x credentials with confirm_password (status_code = 200)
	[Documentation]	Add entry in 802.1x credentials using optional parameter "confirm_password". Status code 200 is expected.
	...	Endpoint: /network/802.1x/credentials

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	username=${802_USERNAME2}
	Set To Dictionary	${payload}	password=${PASSWORD}
	Set To Dictionary	${payload}	confirm_password=${PASSWORD}
	Set To Dictionary	${payload}	authentication_method=${AUTHENTICATION_METHOD}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/802.1x/credentials
	Should Be Equal   ${response.status_code}   ${200}

Test POST /network/802.1x/credentials Functionality with confirm_password
  [Documentation]    Check that POST returns the correct field and values using GET

	${BODY}=	Evaluate   str({'id': '${802_USERNAME2}', 'username': '${802_USERNAME2}', 'authentication_method': '${AUTHENTICATION_METHOD}'})
	API::Check Body Request  PATH=/network/802.1x/credentials	BODY_EXPECTED=${BODY}

Add 802.1x credentials that already exist (status_code = 400)
	[Documentation]	Add entry in 802.1x credentials that already exist. Status code 400 is expected.
	...	Endpoint: /network/802.1x/credentials

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	username=${802_USERNAME1}
	Set To Dictionary	${payload}	password=${PASSWORD}
	Set To Dictionary	${payload}	authentication_method=${AUTHENTICATION_METHOD}

	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/802.1x/credentials	ERROR_CONTROL=${FALSE}	EXPECT_ERROR=HTTPError: 400 Client Error: Bad Request for url: https://${HOST}/api/v1/network/802.1x/credentials

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session