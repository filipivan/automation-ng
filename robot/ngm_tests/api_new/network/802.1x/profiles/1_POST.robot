*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${NAME}=	test_server
${TYPE}=	internal

*** Test Cases ***

Add new entry in 802.1x profiles (status_code = 200)
	[Documentation]	Add entry in 802.1x profiles. Status code 200 is expected. 
	...	Endpoint: /network/802.1x/profiles

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${NAME}
	Set To Dictionary	${payload}	type=${TYPE}
	Set To Dictionary	${payload}	select_users=["admin"]

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/802.1x/profiles
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session