*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SELECT_USERS}=	"user2","user3"

*** Test Cases ***

Update 802.1x profile information (status_code = 200)
	[Documentation]	Update 802.1x profile information. Status code 200 is expected. 
	...	Endpoint: /network/802.1x/profiles/{profile}

	${PAYLOAD}=	Set Variable	{"select_users":[${SELECT_USERS}]}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/802.1x/profiles/test_server
	Should Be Equal   ${response.status_code}   ${200}

Test Update 802.1x profile information (status_code = 200)
	[Documentation]	Test Update 802.1x profile information. Status code 200 is expected. 
	...	Endpoint: /network/802.1x/profiles/{profile}

	${BODY_EXPECTED}=	Evaluate	str({'retransmit_interval': '3600', 'ip_address': '', 'port_number': '', 'user': '', 'type': 'internal', 'shared_secret': '********', 'select_users': [${SELECT_USERS}], 'name': 'test_server'})
	API::Check Body Request	PATH=/network/802.1x/profiles/test_server	BODY_EXPECTED=${BODY_EXPECTED}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session