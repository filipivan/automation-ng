*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${PROFILES}=	"test_server"

*** Test Cases ***

Delete 802.1x profile entry (status_code = 200)
	[Documentation]	Delete 802.1x profile entry. Status code 200 is expected. 
	...	Endpoint: /network/802.1x/profiles

	${PAYLOAD}=	Set Variable	{"profiles":[${PROFILES}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/network/802.1x/profiles
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session