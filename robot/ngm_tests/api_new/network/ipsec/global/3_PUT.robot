*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${ENABLE_LOGGING}=	true

*** Test Cases ***

Update IPsec Global Configuration (status_code = 200)
	[Documentation]	Update IPsec Global Configuration. Status code 200 is expected. 
	...	Endpoint: /network/ipsec/global

	${PAYLOAD}=	Set Variable	{"enable_logging":"${ENABLE_LOGGING}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/ipsec/global
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session