*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${NAME}=	test_ipsec_tunnel
${LEFT_ID}=	@West
${RIGHT_ID}=	@East
${LEFT_ADDRESS}=	%defaultroute
${RIGHT_ADDRESS}=	1.1.1.1
${SECRET}=	secrettest

*** Test Cases ***

Add IPsec Tunnel (status_code = 200)
	[Documentation]	Add IPsec Tunnel. Status code 200 is expected. 
	...	Endpoint: /network/ipsec/tunnel

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${NAME}
	Set To Dictionary	${payload}	left_id=${LEFT_ID}
	Set To Dictionary	${payload}	right_id=${RIGHT_ID}
	Set To Dictionary	${payload}	left_address=${LEFT_ADDRESS}
	Set To Dictionary	${payload}	right_address=${RIGHT_ADDRESS}
	Set To Dictionary	${payload}	secret=${SECRET}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/ipsec/tunnel
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session