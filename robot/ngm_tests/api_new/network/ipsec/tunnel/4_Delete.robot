*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${TUNNEL}=	"test_ipsec_tunnel"

*** Test Cases ***

Delete IPsec Tunnel (status_code = 200)
	[Documentation]	Delete IPsec Tunnel. Status code 200 is expected. 
	...	Endpoint: /network/ipsec/tunnel

	${PAYLOAD}=	Set Variable	{"tunnel":[${TUNNEL}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/network/ipsec/tunnel
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session