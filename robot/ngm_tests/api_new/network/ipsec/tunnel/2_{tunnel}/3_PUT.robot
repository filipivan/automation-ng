*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	NEED-CREATION
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${RIGHT_ADDRESS}=	2.2.2.2

*** Test Cases ***

Edit IPsec Tunnel (status_code = 200)
	[Documentation]	Edit IPsec Tunnel. Status code 200 is expected. 
	...	Endpoint: /network/ipsec/tunnel/{tunnel}

	${PAYLOAD}=	Set Variable	{"right_address":"${RIGHT_ADDRESS}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/ipsec/tunnel/test_ipsec_tunnel
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session