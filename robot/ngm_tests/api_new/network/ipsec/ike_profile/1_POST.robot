*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${PROFILE_NAME}=	test_ipsec_ike
${IKE_VERSION}=	ikev2
${MTU}=	1500

*** Test Cases ***

Add IPsec IKE Profile (status_code = 200)
	[Documentation]	Add IPsec IKE Profile. Status code 200 is expected. 
	...	Endpoint: /network/ipsec/ike_profile

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	profile_name=${PROFILE_NAME}
	Set To Dictionary	${payload}	ike_version=${IKE_VERSION}
	Set To Dictionary	${payload}	mtu=${MTU}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/ipsec/ike_profile
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session