*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get IPsec IKE Profiles (status_code = 200)
	[Documentation]	Get IPsec IKE PRofiles. Status code 200 is expected. 
	...	Endpoint: /network/ipsec/ike_profile

	${response}=	API::Send Get Request	/network/ipsec/ike_profile
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session