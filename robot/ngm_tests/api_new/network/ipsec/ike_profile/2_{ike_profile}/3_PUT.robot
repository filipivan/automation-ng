*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	NEED-CREATION
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${MTU}=	1000
${PHASE_1_AUTHENTICATION}=	SHA1

*** Test Cases ***

Edit IKE Profile (status_code = 200)
	[Documentation]	Edit IKE Profile. Status code 200 is expected. 
	...	Endpoint: /network/ipsec/ike_profile/{ike_profile}

	${PAYLOAD}=	Set Variable	{"phase_1_authentication":"${PHASE_1_AUTHENTICATION}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/ipsec/ike_profile/test_ipsec_ike
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session