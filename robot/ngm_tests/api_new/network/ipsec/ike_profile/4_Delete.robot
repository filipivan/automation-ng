*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${IKE_PROFILE}=	test_ipsec_ike

*** Test Cases ***

Delete IPsec IKE Profile (status_code = 200)
	[Documentation]	Delete IPsec IKE Profile. Status code 200 is expected. 
	...	Endpoint: /network/ipsec/ike_profile

	${PAYLOAD}=	Set Variable	{"ike_profile":["${IKE_PROFILE}"]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/network/ipsec/ike_profile
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session