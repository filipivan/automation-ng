*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${ACL_IDS}=	"0"

*** Test Cases ***

Delete switch acl entry (status_code = 200)
	[Documentation]	Delete switch acl entry. Status code 200 is expected. 
	...	Endpoint: /network/switch/acl

	${PAYLOAD}=	Set Variable	{"acl_ids":[${ACL_IDS}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/acl
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session