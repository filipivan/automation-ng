*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../init.robot
Force Tags	API	NEED-CREATION
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${DIRECTION}=	egress

*** Test Cases ***

Update switch acl direction (status_code = 200)
	[Documentation]	Update switch acl direction. Status code 200 is expected. 
	...	Endpoint: /network/switch/acl/{acl_id}/direction

	${PAYLOAD}=	Set variable	{"direction":"${DIRECTION}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/acl/0/direction
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session