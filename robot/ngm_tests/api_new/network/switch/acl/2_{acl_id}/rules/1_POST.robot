*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${SOURCE_IP}=	1.1.1.1
${RULE_ID}=	1
${ACTION}=	permit

*** Test Cases ***

Add new rule in switch acl (status_code = 200)
	[Documentation]	Add new rule in switch acl. Status code 200 is expected. 
	...	Endpoint: /network/switch/acl/{acl_id}/rules

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	source_ip=${SOURCE_IP}
	Set To Dictionary	${payload}	rule_id=${RULE_ID}
	Set To Dictionary	${payload}	action=${ACTION}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/acl/0/rules
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session