*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${RULE_IDS}=	"1"

*** Test Cases ***

Delete switch acl rule (status_code = 200)
	[Documentation]	Delete switch acl rule. Status code 200 is expected. 
	...	Endpoint: /network/switch/acl/{acl_id}/rules

	${PAYLOAD}=	Set variable	{"rule_ids":[${RULE_IDS}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/acl/0/rules
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session