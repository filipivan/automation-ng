*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${SOURCE_IP}=	2.2.2.2
${ACTION}=	deny

*** Test Cases ***

Update switch acl rule information (status_code = 200)
	[Documentation]	Update switch acl rule information. Status code 200 is expected. 
	...	Endpoint: /network/switch/acl/{acl_id}/rules/{rule_id}

	${PAYLOAD}=	Set Variable	{"source_ip":"${SOURCE_IP}", "action":"${ACTION}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/acl/0/rules/1
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session