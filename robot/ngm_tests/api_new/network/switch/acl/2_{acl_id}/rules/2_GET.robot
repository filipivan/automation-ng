*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get switch acl rules list (status_code = 200)
	[Documentation]	Get switch acl rules list. Status code 200 is expected. 
	...	Endpoint: /network/switch/acl/{acl_id}/rules

	${response}=	API::Send Get Request	/network/switch/acl/0/rules
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session