*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${NAME}=	ACL1
${DIRECTION}=	ingress

*** Test Cases ***

Add new entry in switch acl (status_code = 200)
	[Documentation]	Add new entry in switch acl. Status code 200 is expected. 
	...	Endpoint: /network/switch/acl

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${NAME}
	Set To Dictionary	${payload}	direction=${DIRECTION}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/acl
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session