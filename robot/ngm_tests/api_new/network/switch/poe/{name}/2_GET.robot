*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	NEED-CREATION
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get switch PoE information (status_code = 200)
	[Documentation]	Get switch PoE information. Status code 200 is expected. 
	...	Endpoint: /network/switch/poe/{name}

	Skip If	not ${IS_GSR}	Only SR Family has POE ports, but NSR autoamtion devices have it not installed
	${response}=	API::Send Get Request	/network/switch/poe/{name}
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session
	${IS_GSR}	API::IS GSR
	Set Suite Variable	${IS_GSR}

SUITE:Teardown
	API::Delete::Session