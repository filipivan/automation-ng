*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	NEED-CREATION
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${ENABLE_POE}=	false
${POWER_LIMIT}=	auto
${POWER_PRIORITY}=	low

*** Test Cases ***

Update switch PoE information (status_code = 200)
	[Documentation]	Update switch PoE information. Status code 200 is expected. 
	...	Endpoint: /network/switch/poe/{name}

	Skip If	not ${IS_GSR}	Only SR Family has POE ports, but NSR autoamtion devices have it not installed
	${PAYLOAD}=	Set variable	{"enable_poe":"${ENABLE_POE}", "power_limit":"${POWER_LIMIT}", power_priority="${POWER_PRIORITY}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/poe/{name}
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session
	${IS_GSR}	API::IS GSR
	Set Suite Variable	${IS_GSR}

SUITE:Teardown
	API::Delete::Session