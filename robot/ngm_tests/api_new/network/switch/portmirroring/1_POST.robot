*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${SESSION_NAME}=	my_session_name
${DESTINATION}=	backplane0
${DIRECTION}=	both
${STATUS}=	enabled

*** Test Cases ***

Create new port mirroring session (status_code = 200)
	[Documentation]	Create new port mirroring session. Status code 200 is expected. 
	...	Endpoint: /network/switch/portmirroring

	${SOURCES}	Create List	sfp0	sfp1

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	session_name=${SESSION_NAME}
	Set To Dictionary	${payload}	destination=${DESTINATION}
	Set To Dictionary	${payload}	direction=${DIRECTION}
	Set To Dictionary	${payload}	status=${STATUS}
	Set To Dictionary	${payload}	sources=${SOURCES}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/portmirroring
	Should Be Equal   ${response.status_code}   ${200}

#Create new port mirroring session (status_code = 400)
#	[Documentation]	Create new port mirroring session. Status code 400 is expected. 
#	...	Endpoint: /network/switch/portmirroring

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	session_name=${SESSION_NAME}
#	Set To Dictionary	${payload}	destination=${DESTINATION}
#	Set To Dictionary	${payload}	direction=${DIRECTION}
#	Set To Dictionary	${payload}	status=${STATUS}
#	Set To Dictionary	${payload}	sources=${SOURCES}
#
#	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/portmirroring
#	Should Be Equal   ${response.status_code}   ${400}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session