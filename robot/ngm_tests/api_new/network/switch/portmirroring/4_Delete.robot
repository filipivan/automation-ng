*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${SESSIONS}=	["my_name"]

*** Test Cases ***

Delete port mirroring session (status_code = 200)
	[Documentation]	Delete port mirroring session. Status code 200 is expected. 
	...	Endpoint: /network/switch/portmirroring

	${PAYLOAD}=	Set Variable	{"sessions":${SESSIONS}}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/portmirroring
	Should Be Equal   ${response.status_code}   ${200}

#Delete port mirroring session (status_code = 400)
#	[Documentation]	Delete port mirroring session. Status code 400 is expected. 
#	...	Endpoint: /network/switch/portmirroring

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	sessions=${SESSIONS}
#
#	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/portmirroring
#	Should Be Equal   ${response.status_code}   ${400}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session