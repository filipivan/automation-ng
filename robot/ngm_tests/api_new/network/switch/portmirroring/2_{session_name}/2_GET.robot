*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get port mirroring session (status_code = 200)
	[Documentation]	Get port mirroring session. Status code 200 is expected. 
	...	Endpoint: /network/switch/portmirroring/{session_name}

	${response}=	API::Send Get Request	/network/switch/portmirroring/my_session_name
	Should Be Equal   ${response.status_code}   ${200}

#Get port mirroring session (status_code = 400)
#	[Documentation]	Get port mirroring session. Status code 400 is expected. 
#	...	Endpoint: /network/switch/portmirroring/2_{vlan}{session_name}
#
#	${response}=	API::Send Get Request	/network/switch/portmirroring/{session_name}
#	Should Be Equal   ${response.status_code}   ${400}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session