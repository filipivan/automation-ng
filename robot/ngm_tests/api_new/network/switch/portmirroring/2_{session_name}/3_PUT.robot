*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${DESTINATION}=	backplane1
${DIRECTION}=	egress
${STATUS}=	disabled
${SOURCES}=	["backplane0", "sfp0"]

*** Test Cases ***

Update port mirroring session (status_code = 200)
	[Documentation]	Update port mirroring session. Status code 200 is expected. 
	...	Endpoint: /network/switch/portmirroring/{session_name}

	${PAYLOAD}=	Set Variable	{"destination":"${DESTINATION}", "direction":"${DIRECTION}", "status":"${STATUS}", "sources":${SOURCES}}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/portmirroring/my_session_name
	Should Be Equal   ${response.status_code}   ${200}

#Update port mirroring session (status_code = 400)
#	[Documentation]	Update port mirroring session. Status code 400 is expected. 
#	...	Endpoint: /network/switch/portmirroring/2_{vlan}{session_name}

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	destination=${DESTINATION}
#	Set To Dictionary	${payload}	direction=${DIRECTION}
#	Set To Dictionary	${payload}	status=${STATUS}
#	Set To Dictionary	${payload}	sources=${SOURCES}
#
#	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/portmirroring/2_{vlan}{session_name}
#	Should Be Equal   ${response.status_code}   ${400}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session