*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${NEW_NAME}=	my_name

*** Test Cases ***

Rename a port mirroring session (status_code = 200)
	[Documentation]	Rename a port mirroring session. Status code 200 is expected. 
	...	Endpoint: /network/switch/portmirroring/{session_name}/rename

	${PAYLOAD}=	Set Variable	{"new_name":"${NEW_NAME}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/portmirroring/my_session_name/rename
	Should Be Equal   ${response.status_code}   ${200}

#Rename a port mirroring session (status_code = 400)
#	[Documentation]	Rename a port mirroring session. Status code 400 is expected. 
#	...	Endpoint: /network/switch/portmirroring/2_{vlan}{session_name}/rename

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	new_name=${NEW_NAME}
#
#	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/portmirroring/2_{vlan}{session_name}/rename
#	Should Be Equal   ${response.status_code}   ${400}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session