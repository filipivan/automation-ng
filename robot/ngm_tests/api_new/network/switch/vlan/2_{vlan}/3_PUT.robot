*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${TAGGED_PORTS}=	backplane0
${UNTAGGED_PORTS}=	backplane1

*** Test Cases ***
Update switch vlan information (status_code = 200)
	[Documentation]	Update switch vlan information. Status code 200 is expected. 
	...	Endpoint: /network/switch/vlan/{vlan}

	${PAYLOAD}=	Set Variable	{"tagged_ports":["${TAGGED_PORTS}"], "untagged_ports":["${UNTAGGED_PORTS}"]}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/vlan/10
	Should Be Equal   ${response.status_code}   ${200}

Test Update switch vlan information (status_code = 200)
	[Documentation]	Update switch vlan information. Status code 200 is expected. 
	...	Endpoint: /network/switch/vlan/{vlan}
	
	${BODY_EXPECTED}=	Evaluate   str({'tagged_ports': ['${TAGGED_PORTS}'], 'untagged_ports': ['${UNTAGGED_PORTS}'], 'vlan': '10'})
	API::Check Body Request    PATH=/network/switch/vlan/10   BODY_EXPECTED=${BODY_EXPECTED}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session