*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${VLANS}=	10

*** Test Cases ***

Delete switch vlan entry (status_code = 200)
	[Documentation]	Delete switch vlan entry. Status code 200 is expected. 
	...	Endpoint: /network/switch/vlan

	${PAYLOAD}=	Set variable	{"vlans":["${VLANS}"]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/vlan
	Should Be Equal   ${response.status_code}   ${200}


Test Delete switch vlan entry (status_code = 200)
	[Documentation]	Delete switch vlan entry. Status code 200 is expected. 
	...	Endpoint: /network/switch/vlan

	${BODY_EXPECTED}=	Evaluate	str([{'id': '1', 'vlan': '1', 'untagged_ports': ['backplane0', 'netS2-[1..16]', 'sfp1'], 'tagged_ports': ['']}, {'id': '2', 'vlan': '2', 'untagged_ports': [''], 'tagged_ports': ['']}])
	API::Check Body Request	PATH=/network/switch/vlan	BODY_EXPECTED=${BODY_EXPECTED}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session