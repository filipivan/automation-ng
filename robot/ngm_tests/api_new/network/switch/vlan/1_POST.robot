*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${VLAN}=	10

*** Test Cases ***

Add new entry in switch vlan (status_code = 200)
	[Documentation]	Add new entry in switch vlan. Status code 200 is expected. 
	...	Endpoint: /network/switch/vlan

	${UNTAGGED_PORTS}	Create List	sfp0

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	vlan=${VLAN}
	Set To Dictionary	${payload}	untagged_ports=${UNTAGGED_PORTS}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/vlan
	Should Be Equal   ${response.status_code}   ${200}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session