*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${VLANS}=	1, 2, 10
${DHCP_OPTION_82}=	enabled
${DHCP_SNOOPING}=	enabled

*** Test Cases ***

Update VLANs DHCP Snooping (status_code = 200)
	[Documentation]	Update DHCP snooping configuration for one or more switch VLANs. Status code 200 is expected. 
	...	Endpoint: /network/switch/vlan/dhcpsnooping

	${PAYLOAD}=	Set Variable	{"vlans": [${VLANS}], "dhcp_snooping": "${DHCP_SNOOPING}", "dhcp_option_82": "${DHCP_OPTION_82}", "circuit_id_format": "vlan:interface"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/vlan/dhcpsnooping
	Should Be Equal   ${response.status_code}   ${200}

Test Update VLANs DHCP Snooping (status_code = 200)
	[Documentation]	Test Update DHCP snooping configuration for one or more switch VLANs. Status code 200 is expected. 
	...	Endpoint: /network/switch/vlan/dhcpsnooping

	${BODY_EXPECTED}=	Evaluate   str([{'dhcp_snooping': '${DHCP_SNOOPING}', 'dhcp_option_82': '${DHCP_OPTION_82}', 'vlan': 1}, {'dhcp_snooping': '${DHCP_SNOOPING}', 'dhcp_option_82': '${DHCP_OPTION_82}', 'vlan': 2}, {'dhcp_snooping': '${DHCP_SNOOPING}', 'dhcp_option_82': '${DHCP_OPTION_82}', 'vlan': 10}])
	API::Check Body Request    PATH=/network/switch/vlan/dhcpsnooping    BODY_EXPECTED=${BODY_EXPECTED}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session