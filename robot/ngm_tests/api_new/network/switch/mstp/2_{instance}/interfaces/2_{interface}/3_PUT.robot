*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../../init.robot
Force Tags	API	NON-CRITICAL	EXCLUDEIN5_2	BUG_NG_9850
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${PRIORITY}=	4096

*** Test Cases ***

Update switch mstp interface information (status_code = 200)
	[Documentation]	Update switch mstp interface information. Status code 200 is expected. 
	...	Endpoint: /network/switch/mstp/{instance}/interfaces/{interface}

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	priority=${PRIORITY}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/mstp/1/interfaces/{interface}
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session