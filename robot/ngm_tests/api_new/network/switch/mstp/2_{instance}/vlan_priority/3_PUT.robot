*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../init.robot
Force Tags	API	NON-CRITICAL	EXCLUDEIN5_2	BUG_NG_9850
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${VLAN}=	1-5

*** Test Cases ***

Update switch mstp instance vlan and priority (status_code = 200)
	[Documentation]	Update switch mstp instance vlan and priority. Status code 200 is expected. 
	...	Endpoint: /network/switch/mstp/{instance}/vlan_priority

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	vlan=${VLAN}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/mstp/1/vlan_priority
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session