*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API		EXCLUDEIN5_2
Default Tags	EXCLUDEIN5_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${MST_INSTANCE_ID}=	1
${VLAN}=	1,2
${PRIORITY}=	4096

*** Test Cases ***

Add new instance in switch mstp (status_code = 200)
	[Documentation]	Add new instance in switch mstp. Status code 200 is expected. 
	...	Endpoint: /network/switch/mstp

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	mst_instance_id=${MST_INSTANCE_ID}
	Set To Dictionary	${payload}	vlan=${VLAN}
	Set To Dictionary	${payload}	priority=${PRIORITY}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/mstp
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session