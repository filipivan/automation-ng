*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API		EXCLUDEIN5_2
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${INSTANCES}=	"1"

*** Test Cases ***

Delete switch mstp entry (status_code = 200)
	[Documentation]	Delete switch mstp entry. Status code 200 is expected. 
	...	Endpoint: /network/switch/mstp

	${PAYLOAD}=	Set Variable	{"instances": [${INSTANCES}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/mstp
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session