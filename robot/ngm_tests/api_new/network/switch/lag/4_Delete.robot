*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${LAGS}=	"LAG1"

*** Test Cases ***

Delete switch lag entry (status_code = 200)
	[Documentation]	Delete switch lag entry. Status code 200 is expected. 
	...	Endpoint: /network/switch/lag

	${PAYLOAD}=	Set Variable	{"lags":[${LAGS}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/lag
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session