*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${NAME}=	LAG1
${ID}=	44
${TYPE}=	static
${PORTS}=	["netS1-6","netS1-7","netS1-8"]

*** Test Cases ***

Add new entry in switch lag (status_code = 200)
	[Documentation]	Add new entry in switch lag. Status code 200 is expected. 
	...	Endpoint: /network/switch/lag

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${NAME}
	Set To Dictionary	${payload}	id=${ID}
	Set To Dictionary	${payload}	type=${TYPE}
	Set To Dictionary	${payload}	ports=${PORTS}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/lag
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session