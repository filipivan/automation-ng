*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${PORTS}=	"netS1-6","netS1-7","netS1-8"

*** Test Cases ***

Update switch lag information (status_code = 200)
	[Documentation]	Update switch lag information. Status code 200 is expected. 
	...	Endpoint: /network/switch/lag/{lag}

	${PAYLOAD}=	Set Variable	{"ports":[${PORTS}]}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/lag/LAG1
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session