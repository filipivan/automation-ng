*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${BACKPLANE0_PORT_VLAN_ID}=	1
${BACKPLANE1_PORT_VLAN_ID}=	2
${BACKPLANE0_JUMBO_FRAME}=	disabled
${BACKPLANE1_JUMBO_FRAME}=	disabled

*** Test Cases ***

Update switch backplane information (status_code = 200)
	[Documentation]	Update switch backplane information. Status code 200 is expected. 
	...	Endpoint: /network/switch/backplane

	${PAYLOAD}=	Set variable	{"backplane0_port_vlan_id":"${BACKPLANE0_PORT_VLAN_ID}", "backplane1_port_vlan_id":"${BACKPLANE1_PORT_VLAN_ID}", "backplane0_jumbo_frame":"${BACKPLANE0_JUMBO_FRAME}", "backplane1_jumbo_frame":"${BACKPLANE1_JUMBO_FRAME}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/backplane
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session