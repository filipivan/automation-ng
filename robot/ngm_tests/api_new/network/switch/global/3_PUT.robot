*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${JUMBO_FRAME_SIZE}=	9002

*** Test Cases ***

Update switch global information (status_code = 200)
	[Documentation]	Update switch global information. Status code 200 is expected. 
	...	Endpoint: /network/switch/global

	${PAYLOAD}=	Set variable	{"jumbo_frame_size":"${JUMBO_FRAME_SIZE}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/global
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session