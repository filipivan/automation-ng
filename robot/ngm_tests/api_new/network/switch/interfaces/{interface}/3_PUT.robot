*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${PORT_VLAN_ID}=	1
${STATUS}=	enabled
${SPEED}=	auto
${JUMBO_FRAME}=	enabled
${DESCRIPTION}=	This is a API Automation Test Description
${ENABLE_LLDP_ADVERTISING_AND_RECEPTION_THROUGH_THIS_INTERFACE}=	true
${MSTP_STATUS}=	enabled
${802.1X_STATUS}=	enabled
${MSTP_BPDU_GUARD}=	on
${DHCP_SNOOPING}=	trusted
${SPEED}=	10000

*** Test Cases ***

Update switch interfaces information (status_code = 200)
	[Documentation]	This test will update all switch interfaces to only one state. 
	...	After that he will check if all interfaces have the same value. 
	...	Status code 200 is expected. 
	...	Endpoint: /network/switch/interfaces/{interface}
	[Tags]	NON-CRITICAL	BUG_NG_13005

	${PAYLOAD}=	Set Variable	{"port_vlan_id":"${PORT_VLAN_ID}", "status":"${STATUS}", "speed":"${SPEED}", "jumbo_frame":"${JUMBO_FRAME}", "description":"${DESCRIPTION}", "enable_lldp_advertising_and_reception_through_this_interface":"${ENABLE_LLDP_ADVERTISING_AND_RECEPTION_THROUGH_THIS_INTERFACE}", "802.1x_status":"${802.1X_STATUS}", "mstp_status":"${MSTP_STATUS}", "mstp_bpdu_guard":"${MSTP_BPDU_GUARD}", "dhcp_snooping":"${DHCP_SNOOPING}"}
	${SWITCH_INTERFACES}	API::Get Switch Interfaces
	Set Suite Variable	${SWITCH_INTERFACES}

	FOR	${INTERFACE}	IN	@{SWITCH_INTERFACES}
		${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/interfaces/${INTERFACE}
		Should Be Equal   ${response.status_code}   ${200}
	END

	FOR	${INTERFACE}	IN	@{SWITCH_INTERFACES}
		IF	"${INTERFACE}" == "sfp0" or "${INTERFACE}" == "sfp1"
			${PAYLOAD}=	Set Variable	{'description': '${DESCRIPTION}', 'port_vlan_id': '${PORT_VLAN_ID}', 'status': '${STATUS}', 'speed': '${SPEED}', 'auto-negotiation': 'disabled', 'jumbo_frame': '${JUMBO_FRAME}', 'acl_ingress': 'None', 'acl_egress': 'None', 'dhcp_snooping': '${DHCP_SNOOPING}', 'port_id': 'ifname', 'port_description': 'default', '802.1x_profile': '', 'mstp_status': '${MSTP_STATUS}', 'mstp_bpdu_guard': '${MSTP_BPDU_GUARD}', 'enable_lldp_advertising_and_reception_through_this_interface': True, 'enable_802.1x': False, 'interface': '${INTERFACE}', 'untagged_vlan': '1'}
		ELSE
			${PAYLOAD}=	Set Variable	{'description': '${DESCRIPTION}', 'port_vlan_id': '${PORT_VLAN_ID}', 'status': '${STATUS}', 'speed': '${SPEED}', 'jumbo_frame': '${JUMBO_FRAME}', 'acl_ingress': 'None', 'acl_egress': 'None', 'dhcp_snooping': '${DHCP_SNOOPING}', 'port_id': 'ifname', 'port_description': 'default', '802.1x_profile': '', 'mstp_status': '${MSTP_STATUS}', 'mstp_bpdu_guard': '${MSTP_BPDU_GUARD}', 'enable_lldp_advertising_and_reception_through_this_interface': True, 'enable_802.1x': False, 'interface': '${INTERFACE}', 'untagged_vlan': '1'}
		END

		API::Check Body Request    /network/switch/interfaces/${INTERFACE}    ${PAYLOAD}
	END

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	SUITE:Disable Interfaces
	API::Delete::Session

SUITE:Disable Interfaces
	${PAYLOAD}=	Set Variable	{"port_vlan_id":"${PORT_VLAN_ID}", "jumbo_frame":"disabled", "enable_lldp_advertising_and_reception_through_this_interface":"false", "802.1x_status":"disabled", "mstp_status":"disabled", "mstp_bpdu_guard":"off", "speed":"auto", "dhcp_snooping":"untrusted"}
	FOR	${INTERFACE}	IN	@{SWITCH_INTERFACES}
		${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/switch/interfaces/${INTERFACE}
		Should Be Equal   ${response.status_code}   ${200}
	END