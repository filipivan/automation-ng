*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${HOSTNAME}=	apitest
${HOSTNAME_ORIGINAL}=	nodegrid
*** Test Cases ***

Update network settings (status_code = 200)
	[Documentation]	Update network settings. Status code 200 is expected. 
	...	Endpoint: /network/settings

	${PAYLOAD}=	Set Variable	{"hostname":"${HOSTNAME}"}
	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/settings
	Should Be Equal   ${response.status_code}   ${200}

	[Teardown]	SUITE:Return Back network settings
*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session

SUITE:Return Back network settings
	${PAYLOAD}=	Set Variable	{"hostname":"${HOSTNAME_ORIGINAL}"}
	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/settings
	Should Be Equal   ${response.status_code}   ${200}