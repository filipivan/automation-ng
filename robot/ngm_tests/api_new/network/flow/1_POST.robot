*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${NAME}=	MyFlow
${COLLECTOR_ADDRESS}=	127.0.0.1
${COLLECTOR_PORT}=	1234
${SAMPLING_RATE}=	1
${ENABLED}=	yes
${AGGREGATION}=	src_host,dst_host
${INTERFACE}=	eth0

*** Test Cases ***

Create a new Network Flow to be exported (status_code = 200)
	[Documentation]	Create a new Network Flow to be exported. Status code 200 is expected. 
	...	Endpoint: /network/flow

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${NAME}
	Set To Dictionary	${payload}	collector_address=${COLLECTOR_ADDRESS}
	Set To Dictionary	${payload}	collector_port=${COLLECTOR_PORT}
	Set To Dictionary	${payload}	sampling_rate=${SAMPLING_RATE}
	Set To Dictionary	${payload}	enabled=${ENABLED}
	Set To Dictionary	${payload}	aggregation=${AGGREGATION}
	Set To Dictionary	${payload}	interface=${INTERFACE}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/flow
	Should Be Equal   ${response.status_code}   ${200}

#Create a new Network Flow to be exported (status_code = 400)
#	[Documentation]	Create a new Network Flow to be exported. Status code 400 is expected. 
#	...	Endpoint: /network/flow

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	name=${NAME}
#	Set To Dictionary	${payload}	collector_address=${COLLECTOR_ADDRESS}
#	Set To Dictionary	${payload}	collector_port=${COLLECTOR_PORT}
#	Set To Dictionary	${payload}	sampling_rate=${SAMPLING_RATE}
#	Set To Dictionary	${payload}	enabled=${ENABLED}
#	Set To Dictionary	${payload}	aggregation=${AGGREGATION}
#	Set To Dictionary	${payload}	interface=${INTERFACE}
#
#	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/flow
#	Should Be Equal   ${response.status_code}   ${400}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session