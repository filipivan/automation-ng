*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${NAME}=	MyFlow
${COLLECTOR_ADDRESS}=	127.0.0.2
${COLLECTOR_PORT}=	2345
${SAMPLING_RATE}=	1
${ENABLED}=	no
${AGGREGATION}=	src_host,dst_host
${INTERFACE}=	eth0

*** Test Cases ***

Change configuration of an existing Network Flow (status_code = 200)
	[Documentation]	Change configuration of an existing Network Flow. Status code 200 is expected. 
	...	Endpoint: /network/flow/{flow-id}

	${PAYLOAD}=	Set variable	{"name":"${NAME}", "collector_address":"${COLLECTOR_ADDRESS}", "collector_port":"${COLLECTOR_PORT}", "sampling_rate":"${SAMPLING_RATE}", "enabled":"${ENABLED}", "aggregation":"${AGGREGATION}", "interface":"${INTERFACE}"}
	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/flow/flow-MyFlow
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session