*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***


*** Test Cases ***

Delete a configured Network Flow (status_code = 200)
	[Documentation]	Delete a configured Network Flow. Status code 200 is expected. 
	...	Endpoint: /network/flow/{flow-id}

	${PAYLOAD}=	Set Variable	${EMPTY}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/network/flow/flow-MyFlow
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session