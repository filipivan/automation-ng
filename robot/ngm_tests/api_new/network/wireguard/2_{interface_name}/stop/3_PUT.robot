*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***

Stop Wireguard Tunnel (status_code = 200)
	[Documentation]	Stop Wireguard Tunnel. Status code 200 is expected. 
	...	Endpoint: /network/wireguard/{interface_name}/stop

	${PAYLOAD}=	Create Dictionary

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/wireguard/wg0/stop
	Should Be Equal   ${response.status_code}   ${200}

Test Stop Wireguard Tunnel (status_code = 200)
	[Documentation]	Check Stop Wireguard Tunnel. Status code 200 is expected. Status must to be Down
	...	Endpoint: /network/wireguard
	
	Sleep	10s
	${BODY_EXPECTED}=	Evaluate   str([{'id': 'wg0', 'address': '10.0.0.10/24', 'port': '1000', 'peers': '0', 'status': 'down', 'interface_name': 'wg0'}])
	API::Check Body Request    PATH=/network/wireguard   BODY_EXPECTED=${BODY_EXPECTED}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session