*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${PEER_NAME}=	peer1
${PUBLIC_KEY}=	HuI1c5zp/CcPgvyVwf3YonEAQaLtT0eXP9FWzNkLoiM=
${EXTERNAL_ADDRESS}=	192.168.2.1
${LISTENING_PORT}=	51820
${KEEPALIVE}=	20
${ALLOWED_IPS}=	10.0.0.1/24

*** Test Cases ***

Add new peer in wireguard interface (status_code = 200)
	[Documentation]	Add new peer in wireguard interface. Status code 200 is expected. 
	...	Endpoint: /network/wireguard/{interface_name}/peers

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	peer_name=${PEER_NAME}
	Set To Dictionary	${payload}	allowed_ips=${ALLOWED_IPS}
	Set To Dictionary	${payload}	public_key=${PUBLIC_KEY}
	Set To Dictionary	${payload}	external_address=${EXTERNAL_ADDRESS}
	Set To Dictionary	${payload}	listening_port=${LISTENING_PORT}
	Set To Dictionary	${payload}	keepalive=${KEEPALIVE}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/wireguard/wg0/peers
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session