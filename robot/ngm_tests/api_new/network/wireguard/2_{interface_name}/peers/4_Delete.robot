*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${PEERS}=	"peer1"

*** Test Cases ***

Delete peer in wireguard interface (status_code = 200)
	[Documentation]	Delete peer in wireguard interface. Status code 200 is expected. 
	...	Endpoint: /network/wireguard/{interface_name}/peers

	${PAYLOAD}=	Set Variable	{"peers":[${PEERS}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/network/wireguard/wg0/peers
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session