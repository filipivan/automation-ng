*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${KEEPALIVE}=	15
${PUBLIC_KEY}=	HuI1c5zp/CcPgvyVwf3YonEAQaLtT0eXP9FWzNkLoiM
${ALLOWED_IPS}	10.0.0.2/24

*** Test Cases ***

Update wireguard interface's peer information (status_code = 200)
	[Documentation]	Update wireguard interface's peer information. Status code 200 is expected. 
	...	Endpoint: /network/wireguard/{interface_name}/peers/{peer_name}

	${PAYLOAD}=	Set Variable	{"keepalive":"${KEEPALIVE}", "public_key":"${PUBLIC_KEY}", "allowed_ips": "${ALLOWED_IPS}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/wireguard/wg0/peers/peer1
	Should Be Equal   ${response.status_code}   ${200}

Test Update wireguard interface's peer information (status_code = 200)
	[Documentation]	Update wireguard interface's peer information. Status code 200 is expected. 
	...	Endpoint: /network/wireguard/{interface_name}/peers/{peer_name}
	
	${BODY_EXPECTED}=	Evaluate   str({'label': 'form', 'allowed_ips': '${ALLOWED_IPS}', 'public_key': '${PUBLIC_KEY}', 'keepalive': '${KEEPALIVE}', 'description': '', 'peer_name': 'peer1'})
	API::Check Body Request    PATH=/network/wireguard/wg0/peers/peer1   BODY_EXPECTED=${BODY_EXPECTED}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session