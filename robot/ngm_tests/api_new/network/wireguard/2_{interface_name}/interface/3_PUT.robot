*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${LISTENING_PORT}=	1000
${INTERFACE_NAME}=	wg0

*** Test Cases ***

Update wireguard interface (status_code = 200)
	[Documentation]	Update wireguard interface. Status code 200 is expected. 
	...	Endpoint: /network/wireguard/{interface_name}/interface

	${PAYLOAD}=	Set Variable	{"listening_port":"${LISTENING_PORT}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/network/wireguard/${INTERFACE_NAME}/interface
	Should Be Equal   ${response.status_code}   ${200}

Test Update wireguard interface (status_code = 200)
	[Documentation]	Test Update wireguard interface. Status code 200 is expected. 
	...	Endpoint: /network/wireguard/{interface_name}/interface
	
	${response}=	API::Send Get Request	/network/wireguard/${INTERFACE_NAME}/interface
	${resp}=	Evaluate	str(${response.json()})
	${VALIDATION}	Create list	'listening_port': '${LISTENING_PORT}'	'interface_name': '${INTERFACE_NAME}'
	CLI:Should Contain All    ${resp}	${VALIDATION}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session