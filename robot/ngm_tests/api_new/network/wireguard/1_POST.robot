*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${INTERFACE_NAME}=	wg0
${INTERFACE_TYPE}=	server
${STATUS}=	enabled
${INTERNAL_ADDRESS}=	10.0.0.10/24
${LISTENING_PORT}=	51820
${KEYPAIR}=	auto

*** Test Cases ***

Add new interface in wireguard (status_code = 200)
	[Documentation]	Add new interface in wireguard. Status code 200 is expected. 
	...	Endpoint: /network/wireguard

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	interface_name=${INTERFACE_NAME}
	Set To Dictionary	${payload}	interface_type=${INTERFACE_TYPE}
	Set To Dictionary	${payload}	status=${STATUS}
	Set To Dictionary	${payload}	internal_address=${INTERNAL_ADDRESS}
	Set To Dictionary	${payload}	listening_port=${LISTENING_PORT}
	Set To Dictionary	${payload}	keypair=${KEYPAIR}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/network/wireguard
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session