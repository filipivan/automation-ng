*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${INTERFACES_NAME}=	"wg0"

*** Test Cases ***

Delete wireguard interface (status_code = 200)
	[Documentation]	Delete wireguard interface. Status code 200 is expected. 
	...	Endpoint: /network/wireguard

	${PAYLOAD}=	Set Variable	{"interfaces":[${INTERFACES_NAME}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/network/wireguard
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session