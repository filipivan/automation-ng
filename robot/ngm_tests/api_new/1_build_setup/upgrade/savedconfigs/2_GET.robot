*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get available configuration backups for downgrade (status_code = 200)
	[Documentation]	Get available configuration backups for downgrade. Status code 200 is expected. 
	...	Endpoint: /system/toolkit/upgrade/savedconfigs

	LOG	test
#	${response}=	API::Send Get Request	/system/toolkit/upgrade/savedconfigs
#	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session