*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot
Force Tags	API	NEED-CREATION	FACTORY-RESET
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup

*** Variables ***
${CLEAR_ALL_LOG_FILES}=	true
${CLEAR_ALL_CLOUD_CONFIGURATION}=	true

*** Test Cases ***

Reset nodegrid to factory default (status_code = 200)
	[Documentation]	Reset nodegrid to factory default. Status code 200 is expected. 
	...	Endpoint: /system/toolkit/factory

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	clear_all_log_files=${CLEAR_ALL_LOG_FILES}
	Set To Dictionary	${payload}	clear_all_cloud_configuration=${CLEAR_ALL_CLOUD_CONFIGURATION}

	SUITE::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/factory
	Run Keyword And Ignore Error	API::Delete::Session
	Sleep	260s

Setup Device
	Wait Until Keyword Succeeds	2x	10s	SUITE:Set Password First Login

	Wait Until Keyword Succeeds	2x	10s	SUITE:Enable Root
	SUITE:Change Password Back to Admin and root

	Wait Until Keyword Succeeds	5x	10s	API::Post::Session

	[Teardown]	API::Delete::Session

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE::Set Password First Login
	[Documentation]	Change password when it is enforced (e.g. on first login) and return a valid ticket. Status code 200 is expected.
	...	Endpoint: /ChangePasswordFirstLogin

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	username=${DEFAULT_USERNAME}
	Set To Dictionary	${payload}	password=${FACTORY_PASSWORD}
	Set To Dictionary	${payload}	new_password=${QA_PASSWORD}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/ChangePasswordFirstLogin
	Should Be Equal   ${response.status_code}   ${200}

SUITE::Send Post Request
	[Arguments]	${PATH}	${PAYLOAD}=${EMPTY}	${INDEX_TICKET}=0	${RAW_MODE}=no	${MODE}=yes
	API::Setup HTTP Context
	${headers}=	Run Keyword If	'${PATH}' != '/Session'	API::Get Ticket Header	${INDEX_TICKET}
	...	ELSE	API::Get Headers
	Run Keyword If	'${MODE}' == 'yes'	Log	\n++++++++++++++++++++++++++++ POST - ${API_VERSION}/${PATH} ++++++++++++++++++++++++++++\n${headers}\n${PAYLOAD}\n	INFO	console=yes
	Run Keyword And Ignore Error	POST on Session	api	/api/${API_VERSION}${PATH}	json=${PAYLOAD}	headers=${headers}

SUITE:Change Password Back to Admin and root
	CLI:Open	${DEFAULT_USERNAME}	${QA_PASSWORD}
	Write	shell sudo passwd ${DEFAULT_USERNAME}
	Read Until	New password:
	Write	${FACTORY_PASSWORD}
	Read Until	Retype new password:
	Write	${FACTORY_PASSWORD}
	Read Until	password updated successfully
	CLI:Read Until Prompt

	Write	shell sudo passwd ${ROOT_DEFAULT_USERNAME}
	Read Until	New password:
	Write	${ROOT_PASSWORD}
	Read Until	Retype new password:
	Write	${ROOT_PASSWORD}
	Read Until	password updated successfully
	CLI:Read Until Prompt

	[Teardown]	CLI:Close Connection

SUITE:Enable Root
	API::Post::Session	PASSWORD=${QA_PASSWORD}
	${PAYLOAD}=	Set Variable	{"allow_root_console_access":"false", "ssh_allow_root_access":"false"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/services
	Should Be Equal   ${response.status_code}   ${200}

	SUITE:Set IDLE

	API::Delete::Session

SUITE:Set Password First Login
	[Documentation]	Change password when it is enforced (e.g. on first login) and return a valid ticket. Status code 200 is expected.
	...	Endpoint: /ChangePasswordFirstLogin

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	username=${DEFAULT_USERNAME}
	Set To Dictionary	${payload}	password=${FACTORY_PASSWORD}
	Set To Dictionary	${payload}	new_password=${QA_PASSWORD}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/ChangePasswordFirstLogin
	Should Be Equal   ${response.status_code}   ${200}

SUITE:Set IDLE
	${PAYLOAD}	Set variable	{"idle_timeout":"0"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/system/preferences
	Should Be Equal   ${response.status_code}   ${200}