*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../init.robot
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${IMAGE_LOCATION}=	upgrade-remote
${UPGRADE_USERNAME}=	anonymous
${UPGRADE_PASSWORD}=	anonymous
${IF_DOWNGRADING}=	factory

${FTP_PRESENT}	${FTPSERVER_PRESENT}
${FTP_URL}	${FTPSERVER_URL}
${FTP_IP}	${FTPSERVER_IP}
${FTP_PORT}	${FTPSERVER_PORT}
${FTP_USER}	${FTPSERVER_USER}
${FTP_PASSWD}	${FTPSERVER_PASSWORD}
${RELEASE_PATH}	/releases/

${ISO_REGEX_5.0}	\\s(nodegrid-genericx86-64-\\d+_Branch-NG_5.0-\\d+.iso)\\s
${ISO_REGEX_5.2}	\\s(nodegrid-genericx86-64-\\d+_Branch-NG_5.2-\\d+.signed.iso)\\s
${ISO_REGEX_5.4}	\\s(nodegrid-genericx86-64-\\d+_Branch-NG_5.4-\\d+.signed.iso)\\s
${ISO_REGEX_5.6}	\\s(nodegrid-genericx86-64-\\d+_Branch-NG_5.6-\\d+.signed.iso)\\s
${ISO_REGEX_VERSION}	\\s(nodegrid-genericx86-64-\\d+_Branch-NG_${NGVERSION}-\\d+.iso)\\s
${ISO_REGEX_VERSION_SVN}	\\s(nodegrid-genericx86-64-\\d+_Branch-NG_${NGVERSION}_\\d+_R\\d+-r\\d+.iso)\\s
${ISO_REGEX_MASTER}	\\s(nodegrid-genericx86-64-\\d+_Master-\\d+.signed.iso)\\s

*** Test Cases ***

Upgrade operating system to last Nightly Image
	[Tags]	EXCLUDEIN_OFFICIAL	SWUPGRADE
	[Documentation]	Upgrade operating system. Status code 200 is expected. 
	...	Endpoint: /system/toolkit/upgrade

	${URL_NIGHTLY}=	SUITE:Find The Latest Image	${NGVERSION}	${FALSE}
	${URL_OFFICIAL}=	SUITE:Find The Latest Image	${NGVERSION}	DOWNGRADE_IMAGE=${TRUE}
	LOG	${URL_NIGHTLY}
	LOG	${URL_OFFICIAL}

	SUITE:Upgrade	ftp://${FTPSERVER_IP}${URL_OFFICIAL}
	Run Keyword And Ignore Error	API::Delete::Session
	Wait Until Keyword Succeeds	10x	10s	SUITE:Set Password First Login

	API::Post::Session	PASSWORD=${QA_PASSWORD}
	SUITE:Upgrade	ftp://${FTPSERVER_IP}${URL_NIGHTLY}
	Run Keyword And Ignore Error	API::Delete::Session
	Wait Until Keyword Succeeds	10x	10s	SUITE:Enable Root
	SUITE:Change Password Back to Admin and root
	API::Post::Session

Upgrade operating system to last Official Image
	[Tags]	EXCLUDEIN_NIGHTLY	SWUPGRADE
	[Documentation]	Upgrade operating system. Status code 200 is expected.
	...	Endpoint: /system/toolkit/upgrade

	${URL_NIGHTLY}=	SUITE:Find The Latest Image	${NGVERSION}	${FALSE}	DOWNGRADE_IMAGE=${TRUE}
	${URL_OFFICIAL}=	SUITE:Find The Latest Image	${NGVERSION}
	LOG	${URL_NIGHTLY}
	LOG	${URL_OFFICIAL}

	SUITE:Upgrade	ftp://${FTPSERVER_IP}${URL_NIGHTLY}
	Run Keyword And Ignore Error	API::Delete::Session
	Wait Until Keyword Succeeds	2x	10s	SUITE:Set Password First Login

	API::Post::Session	PASSWORD=${QA_PASSWORD}
	SUITE:Upgrade	ftp://${FTPSERVER_IP}${URL_OFFICIAL}
	Run Keyword And Ignore Error	API::Delete::Session
	Wait Until Keyword Succeeds	2x	10s	SUITE:Enable Root
	SUITE:Change Password Back to Admin and root
	API::Post::Session

*** Keywords ***
SUITE:Setup
	API::Post::Session
	${DOWNGRADES}	Create Dictionary	5.2=5.4	5.4=5.2	5.6=5.4	5.8=5.6
	Set Suite Variable	${DOWNGRADES}

SUITE:Teardown
	API::Delete::Session
	Run Keyword If Any Tests Failed	Fatal Error	Upgrade didn't work

SUITE:Set Password First Login
	[Documentation]	Change password when it is enforced (e.g. on first login) and return a valid ticket. Status code 200 is expected.
	...	Endpoint: /ChangePasswordFirstLogin

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	username=${DEFAULT_USERNAME}
	Set To Dictionary	${payload}	password=${FACTORY_PASSWORD}
	Set To Dictionary	${payload}	new_password=${QA_PASSWORD}

	${response}=	Run Keyword And Continue On Failure	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/ChangePasswordFirstLogin
	LOG	str(${response})

SUITE:Change Password Back to Admin and root
	CLI:Open	${DEFAULT_USERNAME}	${QA_PASSWORD}	startping=no

	Write	shell sudo passwd ${DEFAULT_USERNAME}
	Read Until	New password:
	Write	${FACTORY_PASSWORD}
	Read Until	Retype new password:
	Write	${FACTORY_PASSWORD}
	Read Until	password updated successfully
	CLI:Read Until Prompt

	Write	shell sudo passwd ${ROOT_DEFAULT_USERNAME}
	Read Until	New password:
	Write	${ROOT_PASSWORD}
	Read Until	Retype new password:
	Write	${ROOT_PASSWORD}
	Read Until	password updated successfully
	CLI:Read Until Prompt

	[Teardown]	CLI:Close Connection

SUITE::Send Post Request
	[Arguments]	${PATH}	${PAYLOAD}=${EMPTY}	${INDEX_TICKET}=0	${RAW_MODE}=no	${MODE}=yes
	API::Setup HTTP Context
	${headers}=	Run Keyword If	'${PATH}' != '/Session'	API::Get Ticket Header	${INDEX_TICKET}
	...	ELSE	API::Get Headers
	Run Keyword If	'${MODE}' == 'yes'	Log	\n++++++++++++++++++++++++++++ POST - ${API_VERSION}/${PATH} ++++++++++++++++++++++++++++\n${headers}\n${PAYLOAD}\n	INFO	console=yes
	Run Keyword And Ignore Error	POST on Session	api	/api/${API_VERSION}${PATH}	json=${PAYLOAD}	headers=${headers}

SUITE:Upgrade
	[Arguments]	${URL}
	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	image_location=${IMAGE_LOCATION}
	Set To Dictionary	${payload}	username=${UPGRADE_USERNAME}
	Set To Dictionary	${payload}	password=${UPGRADE_PASSWORD}
	Set To Dictionary	${payload}	url=${URL}
	Set To Dictionary	${payload}	if_downgrading=${IF_DOWNGRADING}

	${response}=	SUITE::Send Upgrade Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/upgrade
	Sleep	240s

SUITE::Send Upgrade Request
	[Arguments]	${PATH}	${PAYLOAD}=${EMPTY}	${INDEX_TICKET}=0	${RAW_MODE}=no	${MODE}=yes	${ERROR_CONTROL}=${TRUE}	${EXPECT_ERROR}=False
	API::Setup HTTP Context
	${headers}=	Run Keyword If	'${PATH}' != '/Session'	API::Get Ticket Header	${INDEX_TICKET}
	...	ELSE	API::Get Headers
	Run Keyword If	'${MODE}' == 'yes'	Log	\n++++++++++++++++++++++++++++ POST - ${API_VERSION}/${PATH} ++++++++++++++++++++++++++++\n${headers}\n${PAYLOAD}\n	INFO	console=yes
	${response}	Run Keyword And Ignore Error	POST on Session	api	/api/${API_VERSION}${PATH}	json=${PAYLOAD}	headers=${headers}
	${MESSAGE}	Run Keyword and Return Status	Should Contain	str(${response})	('PASS', <Response [200]>)
	Run Keyword If	not ${MESSAGE}	Should Contain	str(${response.json()})	ConnectionError: ('Connection aborted.', RemoteDisconnected('Remote end closed connection without response'))

SUITE:Find The Latest Image
	[Arguments]	${VERSION}	${RELEASED}=${TRUE}	${DOWNGRADE_IMAGE}=${FALSE}
	IF	${DOWNGRADE_IMAGE}
		${VERSION}	Set Variable	${DOWNGRADES}[${VERSION}]
	END
	CLI:Connect As Root
	SUITE:Connect To FTP
	Run Keyword If	${RELEASED}	CLI:Write	cd ${RELEASE_PATH}
	${OUTPUT}=	CLI:Write	ls
	Run Keyword If	not ${RELEASED}	SUITE:Disconnect From FTP

	${DISCONNECTION}=	Run Keyword And Return Status	Should Contain	${OUTPUT}	Failed to establish connection.
	Run Keyword If	${DISCONNECTION}	Fatal Error	Could not connect to ftp ${FTP_IP} ${FTP_PORT}

	${PATH}=	Run Keyword If	${RELEASED}	SUITE:Get Official Image	${VERSION}	${OUTPUT}
	...	ELSE	SUITE:Get Daily Image	${VERSION}	${OUTPUT}

	[Return]	${PATH}

SUITE:Get Daily Image
	[Arguments]	${VERSION}	${OUTPUT}
	${DAILY_OUTPUT_IMAGES}=	Get Regexp Matches	${OUTPUT}	${ISO_REGEX_${VERSION}}	1
	${FOUND_IMAGES}=	Run Keyword And Return Status	Should Not Be Empty	${DAILY_OUTPUT_IMAGES}
	Run Keyword If	not ${FOUND_IMAGES}	Fatal Error	Could not find the daily image

	Reverse List	${DAILY_OUTPUT_IMAGES}
	${FOUND_IMAGE}=	Set Variable	${FALSE}
	${DAILY}=	Set Variable	${EMPTY}
	FOR	${IMAGE}	IN	@{DAILY_OUTPUT_IMAGES}
		${IMAGE_HAS_MD5}=	Run Keyword And Return Status	Should Contain	${OUTPUT}	${IMAGE}.md5
		${FOUND_IMAGE}=	Set Variable If	${IMAGE_HAS_MD5}	${TRUE}	${FALSE}
		${DAILY}=	Set Variable	${IMAGE}
		Exit For Loop If	${FOUND_IMAGE}
	END
	Run Keyword If	not ${FOUND_IMAGE}	Fatal Error	Could not find the daily image with md5 file

	${PATH_DAILY}=	Set Variable	/${DAILY}
	[Return]	${PATH_DAILY}

SUITE:Get Official Image
	[Arguments]	${VERSION}	${OUTPUT}
	${OUTPUT}=	Fetch From Left	${OUTPUT}	226 Directory send OK
	${OUTPUT}=	Fetch From Right	${OUTPUT}	Here comes the directory listing.
	${LINES}=	Split To Lines	${OUTPUT}

	# needs to get paths to navigate (i.e.: releases from 2019 and 2020 are different paths)
	${SUBPATHS}=	Create List
	FOR	${LINE}	IN	@{LINES}
		Continue For Loop If	'${LINE}' == '${EMPTY}'
		${YEAR}=	Fetch From Right	${LINE}	${SPACE}
		Append To List	${SUBPATHS}	${YEAR}
	END

	Reverse List	${SUBPATHS}
	${OUTPUT}=	Set Variable
	${FOUND_IMAGE}=	Set Variable	${FALSE}
	FOR	${SUBPATH}	IN	@{SUBPATHS}
		${OUTPUT}=	CLI:Write	ls ${RELEASE_PATH}${SUBPATH}
		${FOUND_IMAGE}=	Run Keyword And Return Status	Should Match Regexp	${OUTPUT}	${ISO_REGEX_${VERSION}}
		Continue For Loop If	not ${FOUND_IMAGE}
		Set Suite Variable	${YEAR}	${SUBPATH}
		Exit For Loop If	${FOUND_IMAGE}
	END
	SUITE:Disconnect From FTP

	Run Keyword If	not ${FOUND_IMAGE}	Fatal Error	Could not find the release image

	${RELEASES}=	Get Regexp Matches	${OUTPUT}	${ISO_REGEX_${VERSION}}	1
	${RELEASE_NAME}=	Set Variable	${RELEASES}[-1]
	${PATH_RELEASE}=	Set Variable	${RELEASE_PATH}${YEAR}/${RELEASE_NAME}
	[Return]	${PATH_RELEASE}

SUITE:Connect To FTP
	[Arguments]	${IP}=${FTP_IP}	${PORT}=${FTP_PORT}	${USER}=${FTP_USER}	${PASSWD}=${FTP_PASSWD}
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nftp ${IP} ${PORT}	INFO	console=yes
	Write	ftp ${IP} ${PORT}
	${OUTPUT1}=	Read Until	Name (${IP}:
	Write	${USER}
	${OUTPUT2}=	Read Until	Password:
	Write	${PASSWD}
	${OUTPUT3}=	Read Until	ftp>
	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${OUTPUT1}\n${OUTPUT2}\n${OUTPUT3}	INFO	console=yes

	${FAIL_AUTH}=	Run Keyword And Return Status	Should Not Contain	${OUTPUT3}	Login successful
	Run Keyword If	${FAIL_AUTH}	Fatal Error	Authentication failed to ftp ${IP} ${PORT}

	Set Client Configuration	prompt=ftp>

SUITE:Disconnect From FTP
	Set Client Configuration	prompt=#
	CLI:Write	quit

SUITE:Enable Root
	API::Post::Session	PASSWORD=${QA_PASSWORD}
	${PAYLOAD}=	Set Variable	{"allow_root_console_access":"true", "ssh_allow_root_access":"true"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/services
	Should Be Equal   ${response.status_code}   ${200}

	SUITE:Set IDLE

	API::Delete::Session

SUITE:Set IDLE
	${PAYLOAD}	Set variable	{"idle_timeout":"0"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/system/preferences
	Should Be Equal   ${response.status_code}   ${200}