*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${PATH}	/services/status/reboot
${USERNAME}=	admin
${PASSWORD}=	${DEFAULT_PASSWORD}
${LOGIN_PATH}=	/services/status/login

*** Test Cases ***

Reboot the machine using POST(status_code = 200)
	[Documentation]	Endpoint to reboot the machine when standard access is not available. Status code 200 is expected.
	...	Endpoint: /services/status/reboot
	[Tags]	reboot

	${headers}	Create Dictionary	token=${TOKEN}

	${response}=   POST on Session	api	${PATH}	headers=${headers}
	Should Be Equal   ${response.status_code}   ${200}
	Sleep	360s

Reboot the machine using GET(status_code = 200)
	[Documentation]	Endpoint to reboot the machine when standard access is not available. Status code 200 is expected.
	...	Endpoint: /services/status/reboot
	[Tags]	reboot

	SUITE::Get Token

	${headers}	Create Dictionary	token=${TOKEN}

	${response}=   GET on Session	api	${PATH}	headers=${headers}
	Should Be Equal   ${response.status_code}   ${200}
	Sleep	180s
	Wait Until Keyword Succeeds	5x	10s	API::Post::Session

Reboot the machine using POST(status_code = 401)
	[Documentation]	Endpoint to reboot the machine when standard access is not available. Status code 401 is expected.
	...	Endpoint: /services/status/reboot

	${headers}	Create Dictionary	token=${TOKEN}wrong

	${response}=	Run Keyword And Expect Error	HTTPError: 401 Client Error: Unauthorized for url: https://${HOST}/services/status/reboot	Post on Session	api	${PATH}	headers=${headers}
	Should Be Equal   ${response}   HTTPError: 401 Client Error: Unauthorized for url: https://${HOST}/services/status/reboot

Reboot the machine using GET(status_code = 401)
	[Documentation]	Endpoint to reboot the machine when standard access is not available. Status code 401 is expected.
	...	Endpoint: /services/status/reboot

	${headers}	Create Dictionary	token=${TOKEN}wrong

	${response}=	Run Keyword And Expect Error	HTTPError: 401 Client Error: Unauthorized for url: https://${HOST}/services/status/reboot	Get on Session	api	${PATH}	headers=${headers}
	Should Be Equal   ${response}   HTTPError: 401 Client Error: Unauthorized for url: https://${HOST}/services/status/reboot

*** Keywords ***
SUITE:Setup
	API::Post::Session
	SUITE::Get Token

SUITE:Teardown
	API::Delete::Session

SUITE::Get Token
	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	username=${USERNAME}
	Set To Dictionary	${payload}	password=${PASSWORD}

	${response}=	POST on Session	api	${LOGIN_PATH}	json=${PAYLOAD}	headers=${EMPTY}
	${TOKEN}	Get From Dictionary	${response.json()}	token
	Set Suite Variable	${TOKEN}