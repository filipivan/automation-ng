*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${PATH}	/services/status/table
${USERNAME}=	admin
${PASSWORD}=	${DEFAULT_PASSWORD}
${LOGIN_PATH}=	/services/status/login

*** Test Cases ***

Get basic information about the essential services of Nodegrid (status_code = 200)
	[Documentation]	Get basic information about the essential services of Nodegrid. Status code 200 is expected. 
	...	Endpoint: /services/status/table

	${headers}	Create Dictionary	token=${TOKEN}

	${response}=   GET on Session	api	${PATH}	headers=${headers}
	Should Be Equal   ${response.status_code}   ${200}

Get basic information about the essential status_services of Nodegrid (status_code = 401)
	[Documentation]	Get basic information about the essential status_services of Nodegrid. Status code 401 is expected.
	...	Endpoint: /services/status/table

	${headers}	Create Dictionary	token=${TOKEN}wrong

	${response}=	Run Keyword And Expect Error	HTTPError: 401 Client Error: Unauthorized for url: https://${HOST}/services/status/table	GET on Session	api	${PATH}	headers=${headers}
	Should Be Equal   ${response}   HTTPError: 401 Client Error: Unauthorized for url: https://${HOST}/services/status/table

*** Keywords ***
SUITE:Setup
	API::Post::Session
	SUITE::Get Token

SUITE:Teardown
	API::Delete::Session

SUITE::Get Token
	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	username=${USERNAME}
	Set To Dictionary	${payload}	password=${PASSWORD}

	${response}=	POST on Session	api	${LOGIN_PATH}	json=${PAYLOAD}	headers=${EMPTY}
	${TOKEN}	Get From Dictionary	${response.json()}	token
	Set Suite Variable	${TOKEN}