*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2
Suite Setup	SUITE:SETUP
Suite Teardown	SUITE:TEARDOWN

*** Variables ***
${USERNAME}=	admin
${PASSWORD}=	${DEFAULT_PASSWORD}
${PATH}=	/services/status/login

*** Test Cases ***
Authenticate user and password for services status page (status_code = 200)
	[Documentation]	Authenticate user in the admin group and return a valid token for services status page. Status code 200 is expected. 
	...	Endpoint: /services/status/login
	
	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	username=${USERNAME}
	Set To Dictionary	${payload}	password=${PASSWORD}

	${response}=	POST on Session	api	${PATH}	json=${PAYLOAD}	headers=${EMPTY}
	Should Be Equal   ${response.status_code}   ${200}

Authenticate user and password for status_services status page (status_code = 401)
	[Documentation]	Authenticate user in the admin group and return a valid token for status_services status page. Status code 401 is expected.
	...	Endpoint: /status_services/status/login

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	username=${USERNAME}
	Set To Dictionary	${payload}	password=${PASSWORD}wrong

	${response}=	Run Keyword And Expect Error	HTTPError: 401 Client Error: Unauthorized for url: https://${HOST}/services/status/login	POST on Session	api	${PATH}	json=${PAYLOAD}	headers=${EMPTY}
	Should Be Equal   ${response}   HTTPError: 401 Client Error: Unauthorized for url: https://${HOST}/services/status/login

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session