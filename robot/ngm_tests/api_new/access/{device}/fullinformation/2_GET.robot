*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get full information about a specific device (status_code = 200)
	[Documentation]	Get full information about a specific device (works with Cluster as well). Status code 200 is expected. 
	...	Endpoint: /access/{device}/fullinformation

	${response}=	API::Send Get Request	/access/test/fullinformation
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session
	API::Add Devices::Table	test	device_console	127.0.0.1	${USERNAME}	${FACTORY_PASSWORD}

SUITE:Teardown
	API::Delete Devices::Table	test
	API::Delete::Session