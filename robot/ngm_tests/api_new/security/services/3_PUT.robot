*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${SSH_TCP_PORT}=	22
${HTTP_PORT}=	80
${HTTPS_PORT}=	443
${ENABLE_ICMP_ECHO_REPLY}=	true
${ENABLE_ICMP_SECURE_REDIRECTS}=	true
${SSH_ALLOW_ROOT_ACCESS}=	true
${ENABLE_HTTP_ACCESS}=	true
${ENABLE_HTTPS_ACCESS}=	true
${ENABLE_HTTP_FILE_REPOSITORY}=	false
${REDIRECT_HTTP_TO_HTTPS}=	true
${STATUS_PAGE_ENABLE}=	true
${STATUS_PAGE_REBOOT}=	true

*** Test Cases ***

Update security services configuration (status_code = 200)
	[Documentation]	Update security services configuration. Status code 200 is expected. 
	...	Endpoint: /security/services

	${PAYLOAD}=	Set Variable	{"ssh_tcp_port":"${SSH_TCP_PORT}", "http_port":"${HTTP_PORT}", "https_port":"${HTTPS_PORT}", "enable_icmp_echo_reply":"${ENABLE_ICMP_ECHO_REPLY}", "enable_icmp_secure_redirects":"${ENABLE_ICMP_SECURE_REDIRECTS}", "ssh_allow_root_access":"${SSH_ALLOW_ROOT_ACCESS}", "enable_http_access":"${ENABLE_HTTP_ACCESS}", "enable_https_access":"${ENABLE_HTTPS_ACCESS}", "enable_http_file_repository":"${ENABLE_HTTP_FILE_REPOSITORY}", "redirect_http_to_https":"${REDIRECT_HTTP_TO_HTTPS}", "status_page_enable":"${STATUS_PAGE_ENABLE}", "status_page_reboot":"${STATUS_PAGE_REBOOT}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/services
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session