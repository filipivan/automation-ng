*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${ENABLE_GEO_FENCE}=	true
${PERIMETER_TYPE}=	circle
${RADIUS}=	1000
${COORDINATES}=	37.50422035,-121.97403149822762
${INSIDE_PERIMETER_ACTION}=	template.sh

*** Test Cases ***

Update security GEO Fence configuration (status_code = 200)
	[Documentation]	Update security GEO Fence configuration. Status code 200 is expected. 
	...	Endpoint: /security/geo_fence

	${PAYLOAD}=	Set Variable	{"enable_geo_fence":${ENABLE_GEO_FENCE}, "perimeter_type":"${PERIMETER_TYPE}", "radius":"${RADIUS}", "coordinates":"${COORDINATES}", "inside_perimeter_action":"${INSIDE_PERIMETER_ACTION}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/geo_fence
	Should Be Equal   ${response.status_code}   ${200}

Test Put /security/firewall_changepolicy Functionality
	${response}=	API::Send Get Request	/security/geo_fence
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}
	Should Contain	${resp}	'address_location': '', 'coordinates': '37.50422035,-121.97403149822762', 'radius': '1000', 'number_of_retries': '3', 'interval': '60', 'inside_perimeter_action': 'template.sh', 'outside_perimeter_action': '', 'enable_geo_fence': True, 'perimeter_type': 'circle'

Update security GEO Fence configuration Disable (status_code = 200)
	[Documentation]	Update security GEO Fence configuration. Status code 200 is expected.
	...	Endpoint: /security/geo_fence

	${PAYLOAD}=	Set Variable	{"enable_geo_fence":false}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/geo_fence
	Should Be Equal   ${response.status_code}   ${200}

Test Put /security/firewall_changepolicy Functionality
	${response}=	API::Send Get Request	/security/geo_fence
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}
	Should Contain	${resp}	'enable_geo_fence': False

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session