*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get security GEO Fence configuration (status_code = 200)
	[Documentation]	Get security GEO Fence configuration. Status code 200 is expected. 
	...	Endpoint: /security/geo_fence

	${response}=	API::Send Get Request	/security/geo_fence
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get security GEO Fence configuration Parameters
	${PARAMETERS}	Create list	'address_location':	'coordinates':	'radius':	'number_of_retries':	'interval':	'inside_perimeter_action':	'outside_perimeter_action':	'enable_geo_fence':	'perimeter_type':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session