*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${FIREWALL_POLICY}=	INPUT:IPv6

*** Test Cases ***
Get firewall policy for the provided chains IVP6 (status_code = 200)
	[Documentation]	Get firewall policy for the provided chains. Status code 200 is expected. 
	...	Endpoint: /security/firewall_changepolicy

	${LIST}	Create list	${FIREWALL_POLICY}
	${PAYLOAD}	Create Dictionary	chains=${LIST}

	${response}=	API::Send Get Request	PAYLOAD=${PAYLOAD}	PATH=/security/firewall_changepolicy
	Should Be Equal   ${response.status_code}   ${200}

Validate Get firewall policy for the provided chains IVP6

	${LIST}	Create list	${FIREWALL_POLICY}
	${PAYLOAD}	Create Dictionary	chains=${LIST}
	${PAYLOAD_RESPONSE}=	Set Variable	{'label': 'policy', 'policy': 'ACCEPT', 'chains': '${FIREWALL_POLICY}'}
	${response}=	API::Send Get Request	PAYLOAD=${PAYLOAD}	PATH=/security/firewall_changepolicy
	${response}=    Evaluate   str(${response.json()})

	Should Be Equal    ${PAYLOAD_RESPONSE}    ${response}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session