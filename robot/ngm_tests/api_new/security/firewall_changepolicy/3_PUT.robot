*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CHAINS_IPV6}=	INPUT:IPv6
${CHAINS_IPV4}=	INPUT:IPv4
${POLICY_ACCEPT}=	ACCEPT
${POLICY_REJECT}=	REJECT

*** Test Cases ***

Update firewall policy for the provided chains IPV6 ACCEPT (status_code = 200)
	[Documentation]	Update firewall policy for the provided chains IPV6. Status code 200 is expected. 
	...	Endpoint: /security/firewall_changepolicy

	${PAYLOAD}=	Set Variable	{"chains":"${CHAINS_IPV6}", "policy":"${POLICY_ACCEPT}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/firewall_changepolicy
	Should Be Equal   ${response.status_code}   ${200}

Test Put /security/firewall_changepolicy IPV6 Functionality ACCEPT
	[Documentation]	Test Update firewall policy for the provided chains IPV6. Status code 200 is expected. 
	...	Endpoint: /security/firewall_changepolicy
		
	${LIST}	Create list	${CHAINS_IPV6}
	${PAYLOAD}	Create Dictionary	chains=${LIST}
	${PAYLOAD_RESPONSE}=	Set Variable	{'label': 'policy', 'policy': '${POLICY_ACCEPT}', 'chains': '${CHAINS_IPV6}'}
	${response}=	API::Send Get Request	PAYLOAD=${PAYLOAD}	PATH=/security/firewall_changepolicy
	${response}=	Evaluate	str(${response.json()})

Update firewall policy for the provided chains IPV4 ACCEPT (status_code = 200)
	[Documentation]	Update firewall policy for the provided chains IPV4. Status code 200 is expected.
	...	Endpoint: /security/firewall_changepolicy

	${PAYLOAD}=	Set Variable	{"chains":"${CHAINS_IPV4}", "policy":"${POLICY_ACCEPT}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/firewall_changepolicy
	Should Be Equal   ${response.status_code}   ${200}

Test Put /security/firewall_changepolicy IPV4 Functionality ACCEPT
	[Documentation]	Test Update firewall policy for the provided chains IPV4. Status code 200 is expected.
	...	Endpoint: /security/firewall_changepolicy
	
	${LIST}	Create list	${CHAINS_IPV4}
	${PAYLOAD}	Create Dictionary	chains=${LIST}
	${PAYLOAD_RESPONSE}=	Set Variable	{'label': 'policy', 'policy': '${POLICY_ACCEPT}', 'chains': '${CHAINS_IPV4}'}
	${response}=	API::Send Get Request	PAYLOAD=${PAYLOAD}	PATH=/security/firewall_changepolicy
	${response}=	Evaluate	str(${response.json()})

Update firewall policy for the provided chains IPV6 REJECT (status_code = 200)
	[Documentation]	Update firewall policy for the provided chains IPV6. Status code 200 is expected. 
	...	Endpoint: /security/firewall_changepolicy

	${PAYLOAD}=	Set Variable	{"chains":"${CHAINS_IPV6}", "policy":"${POLICY_REJECT}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/firewall_changepolicy
	Should Be Equal   ${response.status_code}   ${200}

Test Put /security/firewall_changepolicy IPV6 Functionality REJECT
	[Documentation]	Test Update firewall policy for the provided chains IPV6. Status code 200 is expected. 
	...	Endpoint: /security/firewall_changepolicy
		
	${LIST}	Create list	${CHAINS_IPV6}
	${PAYLOAD}	Create Dictionary	chains=${LIST}
	${PAYLOAD_RESPONSE}=	Set Variable	{'label': 'policy', 'policy': '${POLICY_REJECT}', 'chains': '${CHAINS_IPV6}'}
	${response}=	API::Send Get Request	PAYLOAD=${PAYLOAD}	PATH=/security/firewall_changepolicy
	${response}=	Evaluate	str(${response.json()})

Update firewall policy for the provided chains IPV4 REJECT (status_code = 200)
	[Documentation]	Update firewall policy for the provided chains IPV4. Status code 200 is expected.
	...	Endpoint: /security/firewall_changepolicy

	${PAYLOAD}=	Set Variable	{"chains":"${CHAINS_IPV4}", "policy":"${POLICY_REJECT}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/firewall_changepolicy
	Should Be Equal   ${response.status_code}   ${200}

Test Put /security/firewall_changepolicy IPV4 Functionality REJECT
	[Documentation]	Test Update firewall policy for the provided chains IPV4. Status code 200 is expected.
	...	Endpoint: /security/firewall_changepolicy
	
	${LIST}	Create list	${CHAINS_IPV4}
	${PAYLOAD}	Create Dictionary	chains=${LIST}
	${PAYLOAD_RESPONSE}=	Set Variable	{'label': 'policy', 'policy': '${POLICY_REJECT}', 'chains': '${CHAINS_IPV4}'}
	${response}=	API::Send Get Request	PAYLOAD=${PAYLOAD}	PATH=/security/firewall_changepolicy
	${response}=	Evaluate	str(${response.json()})

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session