*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get authentication console configuration (status_code = 200)
	[Documentation]	Get authentication console configuration. Status code 200 is expected. 
	...	Endpoint: /security/authentication_console

	${response}=	API::Send Get Request	/security/authentication_console
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get authentication console configuration Parameters
	Should Contain	${resp}	admin_and_root_fallback_to_local_on_console

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session