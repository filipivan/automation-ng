*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${ADMIN_AND_ROOT_FALLBACK_TO_LOCAL_ON_CONSOLE}=	false

*** Test Cases ***

Update authentication console configuration false (status_code = 200)
	[Documentation]	Update authentication console configuration. Status code 200 is expected. 
	...	Endpoint: /security/authentication_console

	${PAYLOAD}=	Set Variable	{"admin_and_root_fallback_to_local_on_console":"${ADMIN_AND_ROOT_FALLBACK_TO_LOCAL_ON_CONSOLE}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/authentication_console
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}

Test Put /security/authentication_console false Functionality
	[Documentation]	Get authentication console configuration. Status code 200 is expected.
	...	Endpoint: /security/authentication_console

	${response}=	API::Send Get Request	/security/authentication_console
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}
	Should Contain	${resp}	'admin_and_root_fallback_to_local_on_console': False

Update authentication console configuration true (status_code = 200)
	[Documentation]	Update authentication console configuration. Status code 200 is expected.
	...	Endpoint: /security/authentication_console

	${PAYLOAD}=	Set Variable	{"admin_and_root_fallback_to_local_on_console":"true"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/authentication_console
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}

Test Put /security/authentication_console true Functionality
	[Documentation]	Get authentication console configuration. Status code 200 is expected.
	...	Endpoint: /security/authentication_console

	${response}=	API::Send Get Request	/security/authentication_console
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}
	Should Contain	${resp}	'admin_and_root_fallback_to_local_on_console': True

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session