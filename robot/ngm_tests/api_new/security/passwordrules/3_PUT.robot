*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${PWD_EXPIRATION_MAX_DAYS}=	1000
${PASSWORDS_IN_HISTORY}=	0

*** Test Cases ***

Update password rules (status_code = 200)
	[Documentation]	Update password rules. Status code 200 is expected. 
	...	Endpoint: /security/passwordrules

	${PAYLOAD}=	Set Variable	{"pwd_expiration_max_days":"${PWD_EXPIRATION_MAX_DAYS}", "passwords_in_history":"${PASSWORDS_IN_HISTORY}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/passwordrules
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session