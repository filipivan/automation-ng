*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${TYPE}=	IPv4
${CHAIN}=	test_chain

*** Test Cases ***

Add firewall chain (status_code = 200)
	[Documentation]	Add firewall chain. Status code 200 is expected. 
	...	Endpoint: /security/firewall

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	type=${TYPE}
	Set To Dictionary	${payload}	chain=${CHAIN}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/security/firewall
	Should Be Equal   ${response.status_code}   ${200}

Test Post /security/firewall Functionality
	${response}=	API::Send Get Request	/security/firewall
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Contain	${resp}	${CHAIN}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session