*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${RULES}=	"0"
${SOURCE_NET4}=	192.168.16.25

*** Test Cases ***

Delete firewall chain rules (status_code = 200)
	[Documentation]	Delete firewall chain rules. Status code 200 is expected. 
	...	Endpoint: /security/firewall/{chain}/rules

	${PAYLOAD}=	Set Variable	{ "rules":[${RULES}] }

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/security/firewall/test_chain:IPv4/rules
	Should Be Equal   ${response.status_code}   ${200}

Test Delete /security/firewall/{chain}/rules Functionality
	${response}=	API::Send Get Request	/security/firewall/test_chain:IPv4/rules
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should not Contain	${resp}	${SOURCE_NET4}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session