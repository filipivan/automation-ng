*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${TARGET}=	ACCEPT
${SOURCE_NET4}=	192.168.16.250
${INPUT_INTERFACE}=	eth0

*** Test Cases ***

Update firewall chain rule (status_code = 200)
	[Documentation]	Update firewall chain rule. Status code 200 is expected. 
	...	Endpoint: /security/firewall/{chain}/rules/{rule}

	${PAYLOAD}=	Set Variable	{"target":"${TARGET}", "source_net4":"${SOURCE_NET4}", "input_interface":"${INPUT_INTERFACE}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/firewall/test_chain:IPv4/rules/0
	Should Be Equal   ${response.status_code}   ${200}

Test Put /security/firewall/{chain}/rules/{rule} Functionality
	${response}=	API::Send Get Request	/security/firewall/test_chain:IPv4/rules/0
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Contain	${resp}	${SOURCE_NET4}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session