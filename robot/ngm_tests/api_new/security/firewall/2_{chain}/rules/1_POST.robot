*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${TARGET}=	ACCEPT
${SOURCE_NET4}=	192.168.16.255
${INPUT_INTERFACE}=	eth0

*** Test Cases ***

Add new firewall chain rules (status_code = 200)
	[Documentation]	Add new firewall chain rules. Status code 200 is expected. 
	...	Endpoint: /security/firewall/{chain}/rules

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	target=${TARGET}
	Set To Dictionary	${payload}	source_net4=${SOURCE_NET4}
	Set To Dictionary	${payload}	input_interface=${INPUT_INTERFACE}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/security/firewall/test_chain:IPv4/rules
	Should Be Equal   ${response.status_code}   ${200}

Test Post /security/firewall/{chain}/rules Functionality
	${response}=	API::Send Get Request	/security/firewall/test_chain:IPv4/rules
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Contain	${resp}	${SOURCE_NET4}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session