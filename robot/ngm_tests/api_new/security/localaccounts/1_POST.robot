*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${TEST_USERNAME}=	test_user
${ACCOUNT_TYPE}=	regular
${PASSWORD}=	test_user
${CONFIRM_PASSWORD}=	test_user
${ACCOUNT_EXPIRATION-DATE}=	${EMPTY}
${PASSWORD_CHANGE_AT_LOGIN}=	false
${HASH_FORMAT_PASSWORD}=	false


*** Test Cases ***

Add new local account (status_code = 200)
	[Documentation]	Add new local account. Status code 200 is expected. 
	...	Endpoint: /security/localaccounts
	${USER_GROUP}=	Create List	admin
	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	username=${TEST_USERNAME}
	Set To Dictionary	${payload}	account_type=${ACCOUNT_TYPE}
	Set To Dictionary	${payload}	password=${PASSWORD}
	Set To Dictionary	${payload}	confirm_password=${CONFIRM_PASSWORD}
	Set To Dictionary	${payload}	account_expiration-date=${ACCOUNT_EXPIRATION-DATE}
	Set To Dictionary	${payload}	password_change_at_login=${PASSWORD_CHANGE_AT_LOGIN}
	Set To Dictionary	${payload}	hash_format_password=${HASH_FORMAT_PASSWORD}
	Set To Dictionary	${payload}	user_group=${USER_GROUP}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/security/localaccounts
	Should Be Equal   ${response.status_code}   ${200}

Test Post /security/localaccounts Functionality
	${response}=	API::Send Get Request	/security/localaccounts
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Contain	${resp}	${TEST_USERNAME}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session