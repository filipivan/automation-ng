*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${TEST_USERNAME}=	test_user

*** Test Cases ***

Lock local account (status_code = 200)
	[Documentation]	Lock local account. Status code 200 is expected. 
	...	Endpoint: /security/localaccounts/{account}/lock

	${PAYLOAD}=	Set Variable	${EMPTY}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/localaccounts/test_user/lock	ERROR_CONTROL=${FALSE}
	Should Be Equal   ${response.status_code}   ${200}

Test Put /security/localaccounts/{account} Functionality
	${response}=	API::Send Get Request	/security/localaccounts
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Contain	${resp}	'username': 'test_user', 'state': 'locked'
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}

Test to Open Session with Locked User
	Run Keyword And Expect Error	HTTPError: 401 Client Error: Unauthorized for url: https://${HOST}/api/v1/Session	API::Post::Session	${TEST_USERNAME}	${TEST_USERNAME}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session