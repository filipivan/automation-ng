*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${ACCOUNT_TYPE}=	api
${PASSWORD}=	test123
${CONFIRM_PASSWORD}=	test123
${UGROUP}=	"admin"

*** Test Cases ***

Update local account (status_code = 200)
	[Documentation]	Update local account. Status code 200 is expected. 
	...	Endpoint: /security/localaccounts/{account}

	${PAYLOAD}=	Set Variable	{"account_type":"${ACCOUNT_TYPE}", "password":"${PASSWORD}", "confirm_password":"${CONFIRM_PASSWORD}", "uGroup":[${UGROUP}]}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/localaccounts/test_user
	Should Be Equal   ${response.status_code}   ${200}

Test Put /security/localaccounts/{account} Functionality
	${response}=	API::Send Get Request	/security/localaccounts/test_user
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
#	Should Contain	${resp}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session