*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NODEGRID_USER}=	test_user

*** Test Cases ***

Change API Key of API account (status_code = 200)
	[Documentation]	Change API Key of API account. Status code 200 is expected. 
	...	Endpoint: /security/localaccounts/{account}/reset_api_key

	${PAYLOAD}=	Set Variable	${EMPTY}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/localaccounts/${NODEGRID_USER}/reset_api_key
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Test Put /security/localaccounts/{account}/reset_api_key Functionality
	${response}=	API::Send Put Request	PATH=/security/localaccounts/${NODEGRID_USER}/reset_api_key
	Should Be Equal   ${response.status_code}   ${200}
	${resp2}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp2}
	Log    ${resp2}

	Should Not be Equal	${resp}	${resp2}

Change API Key of API account (status_code = 400)
	[Documentation]	Change API Key of API account. Status code 400 is expected.
	...	Endpoint: /security/localaccounts/test_wrong_user/4_reset_api_key
	
	Run Keyword And Expect Error	HTTPError: 404 Client Error: Not Found for url: https://${HOST}/api/v1/security/localaccounts/2_%7Bvlan%7D%7Baccount%7D/4_reset_api_key	API::Send Put Request	PATH=/security/localaccounts/2_{vlan}{account}/4_reset_api_key

Test Login Using Api_Key Functionality with wrong username (status_code = 401)
	[Documentation]	NG-11604 - Try to create a session with API Key of API account, using a wrong account name. Status code 401 is expected.
	API::Delete::Session

	${resp2}	String.Replace String	${resp2}	\{	${EMPTY}
	${resp2}	String.Replace String	${resp2}	\}	${EMPTY}
	${resp2}	String.Replace String	${resp2}	'	${EMPTY}
	${resp2}	String.Replace String	${resp2}	${SPACE}	${EMPTY}
	@{AUTH_STRING}	String.Split String	${resp2}	:

	Log	${AUTH_STRING[0]}	console=yes
	${AUTH_KEY}	Set Variable	${AUTH_STRING[1]}
	Set Suite Variable	${AUTH_KEY}

	Log	${AUTH_STRING[1]}	console=yes
	${AUTH_TYPE}	Set Variable	${AUTH_STRING[0]}
	Set Suite Variable	${AUTH_TYPE}

	${headers}=	API::Get Headers	AUTH_TYPE=${AUTH_TYPE}	AUTH_KEY=${AUTH_KEY}	AUTH_USERNAME=${NODEGRID_USER}XXX
	Get on Session	api	/api/${API_VERSION}/system/about	headers=${headers}	expected_status=401
	
	[Teardown]	API::Post::Session

Test Login Using Api_Key Functionality with wrong authentication key (status_code = 401)
	[Documentation]	NG-11604 - Try to create a session with API Key of API account, using a wrong authentication key. Status code 401 is expected.
	API::Delete::Session

	${headers}=	API::Get Headers	AUTH_TYPE=${AUTH_TYPE}	AUTH_KEY=${AUTH_KEY}XXX	AUTH_USERNAME=${NODEGRID_USER}
	Get on Session	api	/api/${API_VERSION}/system/about	headers=${headers}	expected_status=401
	
	[Teardown]	API::Post::Session

Test Login Using Api_Key Functionality (status_code = 200)
	[Documentation]	NG-11604 - Create a session with API Key of API account. Status code 200 is expected.
	API::Delete::Session

	${headers}=	API::Get Headers	AUTH_TYPE=${AUTH_TYPE}	AUTH_KEY=${AUTH_KEY}	AUTH_USERNAME=${NODEGRID_USER}
	Get on Session	api	/api/${API_VERSION}/system/about	headers=${headers}	expected_status=200
	
	[Teardown]	API::Post::Session

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session