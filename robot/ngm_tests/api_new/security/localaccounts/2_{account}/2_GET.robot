*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get local account (status_code = 200)
	[Documentation]	Get local account. Status code 200 is expected. 
	...	Endpoint: /security/localaccounts/{account}

	${response}=	API::Send Get Request	/security/localaccounts/test_user
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get local accounts Parameters
	${PARAMETERS}	Create list	'label':	'api_key':	'account_expiration-date':	'hash_format_password': 	'password_change_at_login': 	'account_type':	'password':	'confirm_password':	'user_group':	'username':
	CLI:Should Contain All	${resp}	${PARAMETERS}

Get local account (status_code = 400)
	[Documentation]	Get local account. Status code 400 is expected.
	...	Endpoint: /security/localaccounts/2_{vlan}{account}

	${response}=	API::Send Get Request	PATH=/security/localaccounts/test_wrong_user	ERROR_CONTROL=${FALSE}	EXPECT_ERROR=HTTPError: 400 Client Error: Bad Request for url: https://${HOST}/api/v1/security/localaccounts/test_wrong_user
	Should Be Equal   ${response}   HTTPError: 400 Client Error: Bad Request for url: https://${HOST}/api/v1/security/localaccounts/test_wrong_user



*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session