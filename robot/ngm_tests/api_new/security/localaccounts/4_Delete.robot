*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${LOCAL_ACCOUNTS}=	"test_user"

*** Test Cases ***

Delete local accounts (status_code = 200)
	[Documentation]	Delete local accounts. Status code 200 is expected. 
	...	Endpoint: /security/localaccounts

	${PAYLOAD}=	Set Variable	{"local_accounts":[${LOCAL_ACCOUNTS}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/security/localaccounts
	Should Be Equal   ${response.status_code}   ${200}

Test Post /security/localaccounts Functionality
	${response}=	API::Send Get Request	/security/localaccounts
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should not Contain	${resp}	${LOCAL_ACCOUNTS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session