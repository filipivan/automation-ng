*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${ENABLE_ZPE_CLOUD}=	true
${ENABLE_REMOTE_ACCESS}=	true
${ENABLE_FILE_PROTECTION}=	true
${ENABLE_FILE_ENCRYPTION}=	true
${FILE_ENCRYPTION_MODE}=	passcode
${PASSCODE}=	adminzpe2022
${CONFIRM_PASSCODE}=	adminzpe2022
${ENCRYPTION_PASSCODE}=	.my!P4ssc0d3
${CONFIRM_ENCRYPTION_PASSCODE}=	.my!P4ssc0d3

*** Test Cases ***
Update security zpecloud configuration (status_code = 200)
	[Documentation]	Update security zpecloud configuration. Status code 200 is expected. 
	...	Endpoint: /security/zpecloud

	${PAYLOAD}=	Set Variable	{"enable_zpe_cloud":"${ENABLE_ZPE_CLOUD}", "enable_remote_access":"${ENABLE_REMOTE_ACCESS}", "enable_file_protection":"${ENABLE_FILE_PROTECTION}", "enable_file_encryption":"${ENABLE_FILE_ENCRYPTION}", "file_encryption_mode":"${FILE_ENCRYPTION_MODE}", "encryption_passcode":"${ENCRYPTION_PASSCODE}", "confirm_encryption_passcode":"${CONFIRM_ENCRYPTION_PASSCODE}", "passcode":"${PASSCODE}", "confirm_passcode":"${CONFIRM_PASSCODE}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/zpecloud
	Should Be Equal   ${response.status_code}   ${200}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session