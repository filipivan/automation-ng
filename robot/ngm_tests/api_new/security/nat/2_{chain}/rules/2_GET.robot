*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get NAT chain rules (status_code = 200)
	[Documentation]	Get NAT chain rules. Status code 200 is expected. 
	...	Endpoint: /security/nat/{chain}/rules

	${response}=	API::Send Get Request	/security/nat/test_chain:IPv4/rules
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get NAT chain rules Parameters
	${PARAMETERS}	Create list	'id':	'rules':	'target':	'source_net4':	'destination_net4':	'protocol':	'input_interface':	'output_interface':	'source_port':	'destination_port':	'packets':	'bytes':	'description':
	CLI:Should Contain All	${resp}	${PARAMETERS}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session