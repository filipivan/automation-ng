*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${RULES}=	"0"
@{OUTPUT_INTERFACE}=	eth0	eth1

*** Test Cases ***

Delete NAT chain rules (status_code = 200)
	[Documentation]	Delete NAT chain rules. Status code 200 is expected. 
	...	Endpoint: /security/nat/{chain}/rules

	${PAYLOAD}=	Set variable	{"rules": [${RULES}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/security/nat/test_chain:IPv4/rules
	Should Be Equal   ${response.status_code}   ${200}

Test Delete /security/nat/{chain}/rules Functionality
	${response}=	API::Send Get Request	/security/nat/test_chain:IPv4/rules
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	CLI:Should Not Contain All	${resp}	${OUTPUT_INTERFACE}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session