*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${TARGET}=	MASQUERADE
${OUTPUT_INTERFACE}=	eth1

*** Test Cases ***

Update NAT chain rule (status_code = 200)
	[Documentation]	Update NAT chain rule. Status code 200 is expected. 
	...	Endpoint: /security/nat/{chain}/rules/{rule}

	${PAYLOAD}=	Set Variable	{"target":"${TARGET}", "output_interface":"${OUTPUT_INTERFACE}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/nat/test_chain:IPv4/rules/0
	Should Be Equal   ${response.status_code}   ${200}

Test Post /security/nat/{chain}/rules/{rule} Functionality
	${response}=	API::Send Get Request	/security/nat/test_chain:IPv4/rules/0
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Contain	${resp}	${OUTPUT_INTERFACE}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session