*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get NAT chain rule (status_code = 200)
	[Documentation]	Get NAT chain rule. Status code 200 is expected. 
	...	Endpoint: /security/nat/{chain}/rules/{rule}

	${response}=	API::Send Get Request	/security/nat/test_chain:IPv4/rules/0
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get NAT chain rule Parameters
	${PARAMETERS}	Create list	'label':	'rule_number':	'description':	'source_net4':	'destination_net4':	'to_source':	'to_destination':	'protocol_number':	'source_port':	'destination_port':	'to_ports':	'source_udp_port':	'destination_udp_port':	'log_prefix':	'target':	'input_interface':	'output_interface':	'fragments':	'tcp_flag_syn':	'tcp_flag_ack':	'tcp_flag_fin':	'tcp_flag_rst':	'tcp_flag_urg':	'tcp_flag_psh':	'icmp_type':	'log_level':	'reverse_match_for_source_ip/mask': 	'reverse_match_for_destination_ip/mask': 	'reverse_match_for_input_interface': 	'reverse_match_for_output_interface': 	'enable_state_match': 	'new': True, 'established': 	'related': 	'invalid': 	'snat': 	'dnat': 	'reverse_state_match': 	'reverse_match_for_tcp_flags': 	'reverse_match_for_icmp_type': 	'reverse_match_for_protocol': 	'reverse_match_for_source_port': 	'reverse_match_for_destination_port': 	'log_tcp_sequence_numbers': 	'log_options_from_the_tcp_packet_header': 	'log_options_from_the_ip_packet_header': 	'protocol':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session