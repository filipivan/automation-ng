*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${TARGET}=	MASQUERADE
${OUTPUT_INTERFACE}=	eth0

*** Test Cases ***

Add new NAT chain rules (status_code = 200)
	[Documentation]	Add new NAT chain rules. Status code 200 is expected. 
	...	Endpoint: /security/nat/{chain}/rules

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	target=${TARGET}
	Set To Dictionary	${payload}	output_interface=${OUTPUT_INTERFACE}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/security/nat/test_chain:IPv4/rules
	Should Be Equal   ${response.status_code}   ${200}

Test Post /security/nat/{chain}/rules Functionality
	${response}=	API::Send Get Request	/security/nat/test_chain:IPv4/rules
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Contain	${resp}	${OUTPUT_INTERFACE}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session