*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${TYPE}=	IPv4
${CHAINS}=	test_chain

*** Test Cases ***

Delete NAT chain (status_code = 200)
	[Documentation]	Delete NAT chain. Status code 200 is expected. 
	...	Endpoint: /security/nat

	${PAYLOAD}=	Set Variable	{"chains":["${CHAINS}:${TYPE}"]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/security/nat
	Should Be Equal   ${response.status_code}   ${200}

Test Delete /security/nat Functionality
	${response}=	API::Send Get Request	/security/nat
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Not Contain	${resp}	${CHAINS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session