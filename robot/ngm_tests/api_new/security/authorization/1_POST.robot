*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${NAME}=	testgroup

*** Test Cases ***

Add authorization groups (status_code = 200)
	[Documentation]	Add authorization groups. Status code 200 is expected. 
	...	Endpoint: /security/authorization

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${NAME}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/security/authorization
	Should Be Equal   ${response.status_code}   ${200}

Test Post /security/authentication Functionality
		[Documentation]	Get authorization groups. Status code 200 is expected.
	...	Endpoint: /security/authorization

	${response}=	API::Send Get Request	/security/authorization
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Contain	${resp}	{'id': 'testgroup', 'name': 'testgroup'}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session