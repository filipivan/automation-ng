*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${RESTRICT_CONFIGURE_SYSTEM_PERMISSION_TO_READ_ONLY}=	true
${CUSTOM_SESSION_TIMEOUT}=	true
${TIMEOUT}=	500
${MENU-DRIVEN_ACCESS_TO_DEVICES}=	yes
${PERMISSIONS}=	["configure_system"]

*** Test Cases ***

Update authorization group profile configuration (status_code = 200)
	[Documentation]	Update authorization group profile configuration. Status code 200 is expected. 
	...	Endpoint: /security/authorization/{group}/profile

	${PAYLOAD}=	Set variable	{"restrict_configure_system_permission_to_read_only":"${RESTRICT_CONFIGURE_SYSTEM_PERMISSION_TO_READ_ONLY}", "custom_session_timeout":"${CUSTOM_SESSION_TIMEOUT}", "timeout":"${TIMEOUT}", "menu-driven_access_to_devices":"${MENU-DRIVEN_ACCESS_TO_DEVICES}", "permissions":${PERMISSIONS}}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/authorization/testgroup/profile
	Should Be Equal   ${response.status_code}   ${200}

Test Put /security/authorization/{group}/profile Functionality
	${response}=	API::Send Get Request	/security/authorization/testgroup/profile
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}
	Should Contain	${resp}	{'timeout': '500', 'email_events_to': '', 'restrict_configure_system_permission_to_read_only': True, 'menu-driven_access_to_devices': True, 'sudo_permission': False, 'custom_session_timeout': True, 'startup_application': 'start_appl_cli', 'permissions': ['configure_system']}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session