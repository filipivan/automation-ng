*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get authorization group profile configuration (status_code = 200)
	[Documentation]	Get authorization group profile configuration. Status code 200 is expected. 
	...	Endpoint: /security/authorization/{group}/profile

	${response}=	API::Send Get Request	/security/authorization/testgroup/profile
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get authorization group profile configuration Parameters
	${PARAMETERS}	Create list	'timeout':	'email_events_to':	'restrict_configure_system_permission_to_read_only':	'menu-driven_access_to_devices':	'sudo_permission':	'custom_session_timeout':	'startup_application':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session