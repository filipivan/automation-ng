*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${POWER}=	PowerStatus
${OUTLETS}=	pdu_test:A:AA1

*** Test Cases ***

Delete authorization group outlet configuration (status_code = 200)
	[Documentation]	Delete authorization group outlet configuration. Status code 200 is expected. 
	...	Endpoint: /security/authorization/{group}/outlets

	${PAYLOAD}=	Set Variable	{"power":"${POWER}", "outlets":"${OUTLETS}"}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/security/authorization/testgroup/outlets
	Should Be Equal   ${response.status_code}   ${200}

Test Post /security/authorization/testgroup/outlets Functionality
	${response}=	API::Send Get Request	/security/authorization/testgroup/outlets
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should not Contain	${resp}	${OUTLETS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session