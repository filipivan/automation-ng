*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${POWER}=	PowerStatus
${OUTLETS}=	pdu_test:A:AA1

*** Test Cases ***

Add authorization group outlet configuration (status_code = 200)
	[Documentation]	Add authorization group outlet configuration. Status code 200 is expected. 
	...	Endpoint: /security/authorization/{group}/outlets

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	power=${POWER}
	Set To Dictionary	${payload}	outlets=${OUTLETS}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/security/authorization/testgroup/outlets
	Should Be Equal   ${response.status_code}   ${200}

Test Post /security/authorization/testgroup/outlets Functionality
	${response}=	API::Send Get Request	/security/authorization/testgroup/outlets
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Contain	${resp}	{'id': 'pdu_test:A:AA1', 'device': 'pdu_test', 'pdu_id': 'A', 'outlet_id': 'AA1', 'power_mode': 'status only'}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session