*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${POWER}=	NoAccess

*** Test Cases ***

Update authorization group outlet configuration (status_code = 200)
	[Documentation]	Update authorization group outlet configuration. Status code 200 is expected. 
	...	Endpoint: /security/authorization/{group}/outlets/{outlet}

	${PAYLOAD}=	Set variable	{"power":"${POWER}"}
	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/authorization/testgroup/outlets/{outlet}
	Should Be Equal   ${response.status_code}   ${200}

Test Put /security/authorization/testgroup/outlets Functionality
	
	${response}=	API::Send Get Request	/security/authorization/testgroup/outlets
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}

	Should Contain	${resp}	'id': 'pdu_test:A:AA1', 'device': 'pdu_test', 'pdu_id': 'A', 'outlet_id': 'AA1', 'power_mode': 'status only'
	
*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session