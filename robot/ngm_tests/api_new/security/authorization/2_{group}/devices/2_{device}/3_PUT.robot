*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${SESSION}=	ReadWrite
${ACCESS_LOG_AUDIT}=	true
${POWER}=	PowerControl
${DOOR}=	PowerControl

*** Test Cases ***

Update authorization group device configuration (status_code = 200)
	[Documentation]	Update authorization group device configuration. Status code 200 is expected. 
	...	Endpoint: /security/authorization/{group}/devices/{device}

	${PAYLOAD}=	Set Variable	{"session":"${SESSION}", "power":"${POWER}", "access_log_audit":"${ACCESS_LOG_AUDIT}", "door": "${DOOR}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/authorization/testgroup/devices/testdevice
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}

Test Put /security/authorization/{group}/devices/{device} Functionality
	[Tags]	NON-CRITICAL	BUG_NG_11883
	[Documentation]	Get authorization group device configuration. Status code 200 is expected.
	...	Endpoint: /security/authorization/{group}/devices/{device}

	${response}=	API::Send Get Request	/security/authorization/testgroup/devices/testdevice
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Contain	${resp}	{'label': 'add', 'mks': False, 'kvm': False, 'reset_device': False, 'sp_console': False, 'virtual_media': False, 'access_log_audit': True, 'access_log_clear': False, 'event_log_audit': False, 'event_log_clear': False, 'sensors_data': False, 'monitoring': False, 'custom_commands': False, 'session': '${SESSION}', 'power': '${POWER}', 'door': '${DOOR}', 'targets': 'testdevice'}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session