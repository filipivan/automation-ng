*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get authorization group device configuration (status_code = 200)
	[Documentation]	Get authorization group device configuration. Status code 200 is expected. 
	...	Endpoint: /security/authorization/{group}/devices/{device}

	${response}=	API::Send Get Request	/security/authorization/testgroup/devices/testdevice
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get authorization group device configuration Parameters
	${PARAMETERS}	Create list	'label':	'mks':	'kvm':	'reset_device':	'sp_console':	'virtual_media':	'access_log_audit':	'access_log_clear':	'event_log_audit':	'event_log_clear':	'sensors_data':	'monitoring':	'custom_commands':	'session':	'power':	'door':	'targets':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session