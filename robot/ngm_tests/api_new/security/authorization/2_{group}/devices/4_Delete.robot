*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${DEVICES}=	"testdevice"

*** Test Cases ***

Delete authorization group devices configuration (status_code = 200)
	[Documentation]	Delete authorization group devices configuration. Status code 200 is expected. 
	...	Endpoint: /security/authorization/{group}/devices

	${PAYLOAD}=	Set variable	{"devices":[${DEVICES}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/security/authorization/testgroup/devices
	Should Be Equal   ${response.status_code}   ${200}

Test Delete authorization group devices configuration Functionality
	[Documentation]	Get authorization group devices configuration. Status code 200 is expected.
	...	Endpoint: /security/authorization/{group}/devices

	${response}=	API::Send Get Request	/security/authorization/testgroup/devices
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Not Contain	${resp}	${DEVICES}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	SUITE:Delete devices
	API::Delete::Session

SUITE:Delete devices
	[Documentation]	Delete device. Status code 200 is expected.
	...	Endpoint: /devices/table

	${PAYLOAD}=	Set Variable	{"devices":[${DEVICES}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table
	Should Be Equal   ${response.status_code}   ${200}