*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get authorization group devices configuration (status_code = 200)
	[Documentation]	Get authorization group devices configuration. Status code 200 is expected. 
	...	Endpoint: /security/authorization/{group}/devices

	${response}=	API::Send Get Request	/security/authorization/testgroup/devices
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get authorization group device configuration Parameters
	${PARAMETERS}	Create list	'id':	'targets':	'session_mode':	'mks':	'kvm':	'power_mode':	'reset_device':	'door_mode':	'access_log':	'event_log':	'sp_console':	'sensors_data':	'monitoring':	'virtual_media':	'custom_commands':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session