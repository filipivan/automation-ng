*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${TYPE}=	device_console
${NAME}=	testdevice
${IP_ADDRESS}=	192.168.16.123
${MODE}=	ondemand

*** Test Cases ***

Add authorization group devices configuration (status_code = 200)
	[Documentation]	Add authorization group devices configuration. Status code 200 is expected. 
	...	Endpoint: /security/authorization/{group}/devices

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	devices=${NAME}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/security/authorization/testgroup/devices
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}

Test Post /security/authorization/{group}/devices Functionality
	[Documentation]	Get authorization group devices configuration. Status code 200 is expected.
	...	Endpoint: /security/authorization/{group}/devices

	${response}=	API::Send Get Request	/security/authorization/testgroup/devices
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Contain	${resp}	${NAME}

*** Keywords ***
SUITE:Setup
	API::Post::Session
	SUITE:Add new device

SUITE:Teardown
	API::Delete::Session

SUITE:Add new device
	[Documentation]	Add new device. Status code 200 is expected.
	...	Endpoint: /devices/table

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	type=${TYPE}
	Set To Dictionary	${payload}	name=${NAME}
	Set To Dictionary	${payload}	ip_address=${IP_ADDRESS}
	Set To Dictionary	${payload}	mode=${MODE}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table
	Should Be Equal   ${response.status_code}   ${200}