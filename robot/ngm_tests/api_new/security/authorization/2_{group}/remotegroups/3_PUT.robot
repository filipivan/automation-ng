*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${REMOTE_GROUPS}=	test123

*** Test Cases ***

Update authorization remote groups configuration (status_code = 200)
	[Documentation]	Update authorization remote groups configuration. Status code 200 is expected. 
	...	Endpoint: /security/authorization/{group}/remotegroups

	${PAYLOAD}=	Set Variable	{"remote_groups":"${REMOTE_GROUPS}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/authorization/testgroup/remotegroups
	Should Be Equal   ${response.status_code}   ${200}

Test Put /security/authorization/{group}/remotegroups Functionality
	${response}=	API::Send Get Request	/security/authorization/testgroup/remotegroups
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}
	Should Contain	${resp}	{'remote_groups': '${REMOTE_GROUPS}'}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session