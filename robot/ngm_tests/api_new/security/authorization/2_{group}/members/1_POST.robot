*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${GROUP_NAME}=	testgroup
${LOCAL_USER}=	test_user
${ACCOUNT_TYPE}=	regular
${PASSWORD}=	test_user
${CONFIRM_PASSWORD}=	test_user
${ACCOUNT_EXPIRATION-DATE}=	${EMPTY}
${PASSWORD_CHANGE_AT_LOGIN}=	false
${HASH_FORMAT_PASSWORD}=	false

*** Test Cases ***

Add authorization group members (status_code = 200)
	[Documentation]	Add authorization group members. Status code 200 is expected. 
	...	Endpoint: /security/authorization/{group}/members

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	local_users=${LOCAL_USER}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/security/authorization/${GROUP_NAME}/members
	Should Be Equal   ${response.status_code}   ${200}

Check Add authorization group members (status_code = 200)
	[Documentation]	Get authorization group members. Status code 200 is expected.
	...	Endpoint: /security/authorization/{group}/members

	${response}=	API::Send Get Request	/security/authorization/${GROUP_NAME}/members
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Contain	${resp}	{'id': '${LOCAL_USER}', 'members': '${LOCAL_USER}'}

Add a non existing user at /security/authorization/testgroup/members (status_code = 400)
	[Documentation]	Get authorization group members. Status code 200 is expected.
	...	Endpoint: /security/authorization/{group}/members
	
	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	local_users=${LOCAL_USER}XXX

	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/security/authorization/${GROUP_NAME}/members	EXPECT_ERROR=HTTPError: 400 Client Error: Bad Request for url: https://${HOST}/api/v1/security/authorization/testgroup/members  ERROR_CONTROL=${FALSE}
	
*** Keywords ***
SUITE:Setup
	API::Post::Session
	SUITE:Add new local account

SUITE:Teardown
	API::Delete::Session

SUITE:Add new local account
	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	username=${LOCAL_USER}
	Set To Dictionary	${payload}	account_type=${ACCOUNT_TYPE}
	Set To Dictionary	${payload}	password=${PASSWORD}
	Set To Dictionary	${payload}	confirm_password=${CONFIRM_PASSWORD}
	Set To Dictionary	${payload}	account_expiration-date=${ACCOUNT_EXPIRATION-DATE}
	Set To Dictionary	${payload}	password_change_at_login=${PASSWORD_CHANGE_AT_LOGIN}
	Set To Dictionary	${payload}	hash_format_password=${HASH_FORMAT_PASSWORD}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/security/localaccounts
	Should Be Equal   ${response.status_code}   ${200}