*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${MEMBERS}=	"test_user"

*** Test Cases ***

Delete authorization group members (status_code = 200)
	[Documentation]	Delete authorization group members. Status code 200 is expected. 
	...	Endpoint: /security/authorization/{group}/members

	${PAYLOAD}=	Set Variable	{"members":[${MEMBERS}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/security/authorization/testgroup/members
	Should Be Equal   ${response.status_code}   ${200}

Test Delete /security/authorization/{group}/members Functionality
	[Documentation]	Get authorization group members. Status code 200 is expected.
	...	Endpoint: /security/authorization/{group}/members

	${response}=	API::Send Get Request	/security/authorization/testgroup/members
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Not Contain	${resp}	${MEMBERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	SUITE:Delete local account
	API::Delete::Session

SUITE:Delete local account
	${PAYLOAD}=	Set Variable	{"local_accounts":[${MEMBERS}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/security/localaccounts
	Should Be Equal   ${response.status_code}   ${200}