*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${ENABLE_AUTHENTICATION_SERVER_SELECTION_BASED_ON_REALMS}=	false

*** Test Cases ***

Update authentication realms configuration False (status_code = 200)
	[Documentation]	Update authentication realms configuration. Status code 200 is expected. 
	...	Endpoint: /security/authentication_realms

	${PAYLOAD}=	Set variable	{"enable_authentication_server_selection_based_on_realms":"${ENABLE_AUTHENTICATION_SERVER_SELECTION_BASED_ON_REALMS}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/authentication_realms
	Should Be Equal   ${response.status_code}   ${200}

Test Put /security/authentication_realms false Functionality
	[Documentation]	Get authentication realms configuration. Status code 200 is expected.
	...	Endpoint: /security/authentication_realms

	${response}=	API::Send Get Request	/security/authentication_realms
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}
	Should Contain	${resp}	'enable_authentication_server_selection_based_on_realms': False

Update authentication realms configuration True (status_code = 200)
	[Documentation]	Update authentication realms configuration. Status code 200 is expected.
	...	Endpoint: /security/authentication_realms

	${PAYLOAD}=	Set variable	{"enable_authentication_server_selection_based_on_realms":"true"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/authentication_realms
	Should Be Equal   ${response.status_code}   ${200}

Test Put /security/authentication_realms true Functionality
	[Documentation]	Get authentication realms configuration. Status code 200 is expected.
	...	Endpoint: /security/authentication_realms

	${response}=	API::Send Get Request	/security/authentication_realms
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}
	Should Contain	${resp}	'enable_authentication_server_selection_based_on_realms': True

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session