*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get authentication realms configuration (status_code = 200)
	[Documentation]	Get authentication realms configuration. Status code 200 is expected. 
	...	Endpoint: /security/authentication_realms

	${response}=	API::Send Get Request	/security/authentication_realms
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get authentication realms configuration Parameters
	Should Contain	${resp}	enable_authentication_server_selection_based_on_realms

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session