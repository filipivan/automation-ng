*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	NON-CRITICAL
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***

Get NAT policy for the provided chains (status_code = 200)
	[Documentation]	Get NAT policy for the provided chains. Status code 200 is expected. 
	...	Endpoint: /security/nat_changepolicy

	${PAYLOAD}=		Set Variable	{"chains":["PREROUTING:IPv6"]}
	${response}=	API::Send Get Request	PATH=/security/nat_changepolicy	PAYLOAD=${PAYLOAD}
	Should Be Equal	${response.status_code}	${200}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session