*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${INDEXES}=	"1","2"

*** Test Cases ***

Delete authentication methods (status_code = 200)
	[Documentation]	Delete authentication methods. Status code 200 is expected. 
	...	Endpoint: /security/authentication

	${PAYLOAD}=	Set Variable	{"indexes":[${INDEXES}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/security/authentication
	Should Be Equal   ${response.status_code}   ${200}

Test Delete /security/authentication Functionality
	[Documentation]	Get authentication methods. Status code 200 is expected.
	...	Endpoint: /security/authentication

	${response}=	API::Send Get Request	/security/authentication
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Not Contain	${resp}	192.168.1.1	192.168.1.2

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session