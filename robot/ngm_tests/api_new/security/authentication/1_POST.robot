*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${METHOD}=	ldap
${METHOD2}=	radius
${STATUS}=	enabled
${FALLBACK}=	disabled
${REMOTE_SERVER}=	192.168.1.1

*** Test Cases ***

Add authentication methods (status_code = 200)
	[Documentation]	Add authentication methods. Status code 200 is expected. 
	...	Endpoint: /security/authentication

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	method=${METHOD}
	Set To Dictionary	${payload}	status=${STATUS}
	Set To Dictionary	${payload}	fallback=${FALLBACK}
	Set To Dictionary	${payload}	remote_server=${REMOTE_SERVER}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/security/authentication
	Should Be Equal   ${response.status_code}   ${200}

	${PAYLOAD2}=	Create Dictionary
	Set To Dictionary	${payload2}	method=${METHOD2}
	Set To Dictionary	${payload2}	status=${STATUS}
	Set To Dictionary	${payload2}	fallback=${FALLBACK}
	Set To Dictionary	${payload2}	remote_server=${REMOTE_SERVER}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD2}	PATH=/security/authentication
	Should Be Equal   ${response.status_code}   ${200}

Test Post /security/authentication Functionality
	[Documentation]	Get authentication methods. Status code 200 is expected.
	...	Endpoint: /security/authentication

	${response}=	API::Send Get Request	/security/authentication
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Contain	${resp}	{'id': '1', 'method': 'ldap or ad', 'status': 'enabled', 'fallback': 'disabled', 'index': '1', 'remote_server': '192.168.1.1'}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session