*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	NON-CRITICAL
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${NAME}=	DUOAPI
${STATUS}=	enabled
${ENTITY_ID}=	Nodegrid
${IMPORTMETASELECTION}=	importmeta-remote
${URL}=	${FTPSERVER2_URL}${SSOSERVER_XML_PATH}
${USERNAME}	${FTPSERVER2_USER}
${PASSWORD}	${FTPSERVER2_PASSWORD}

*** Test Cases ***

Add SSO Identity Provider with Metadata (status_code = 200)
	[Documentation]	Add SSO Identity Provider with Metadata. Status code 200 is expected. 
	...	Endpoint: /security/authentication/sso/import_metadata

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${NAME}_xml
	Set To Dictionary	${payload}	status=${STATUS}
	Set To Dictionary	${payload}	entity_id=${ENTITY_ID}
	Set To Dictionary	${payload}	importmetaselection=${IMPORTMETASELECTION}
	Set To Dictionary	${payload}	url=${URL}
	Set To Dictionary	${payload}	username=${USERNAME}
	Set To Dictionary	${payload}	password=${PASSWORD}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/security/authentication/sso/import_metadata
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session