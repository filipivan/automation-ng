*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${NAMES}=	idp_test

*** Test Cases ***

Delete SSO Identity Provider (status_code = 200)
	[Documentation]	Delete SSO Identity Provider. Status code 200 is expected. 
	...	Endpoint: /security/authentication/sso

	${PAYLOAD}=	Set Variable	{"names":["${NAMES}"]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/security/authentication/sso
	Should Be Equal   ${response.status_code}   ${200}

Test Delete /security/authentication/sso Functionality
	[Documentation]	Get SSO Identity Providers. Status code 200 is expected.
	...	Endpoint: /security/authentication/sso

	${response}=	API::Send Get Request	/security/authentication/sso
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should not Contain	${resp}	${NAMES}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session