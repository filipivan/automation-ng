*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get SSO Identity Provider (status_code = 200)
	[Documentation]	Get SSO Identity Provider. Status code 200 is expected. 
	...	Endpoint: /security/authentication/sso/{method}

	${response}=	API::Send Get Request	/security/authentication/sso/idp_test
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get authentication Parameters
	${PARAMETERS}	Create list	'id':	'type':	'label':	'options':	'visible':	'structure':	'cert':	BEGIN CERTIFICATE	END CERTIFICATE	'label':	'entity_id':	'sso_url':	'issuer':	'logout_url':	'status':	'icon':	'force_re-authentication':	'sign_request':	'enable_single_logout':	'name':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session