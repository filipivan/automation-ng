*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../init.robot
Force Tags	API	NON-CRITICAL
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${SYSTEMCERTSELECTION}=	systemcert-remote
${URL}=	${FTPSERVER2_URL}${SSOSERVER_CRT_PATH}
${FTP_USERNAME}	${FTPSERVER2_USER}
${FTP_PASSWORD}	${FTPSERVER2_PASSWORD}

*** Test Cases ***

Upload new certificate for SSO Identity Provider (status_code = 200)
	[Documentation]	Upload new certificate for SSO Identity Provider. Status code 200 is expected. 
	...	Endpoint: /security/authentication/sso/{method}/certificate

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	url=${URL}
	Set To Dictionary	${payload}	username=${FTP_USERNAME}
	Set To Dictionary	${payload}	password=${FTP_PASSWORD}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/security/authentication/sso/idp_test/certificate
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session