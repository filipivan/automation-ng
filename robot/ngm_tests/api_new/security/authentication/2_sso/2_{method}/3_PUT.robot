*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${STATUS}=	disabled

*** Test Cases ***

Edit SSO Identity Provider (status_code = 200)
	[Documentation]	Edit SSO Identity Provider. Status code 200 is expected. 
	...	Endpoint: /security/authentication/sso/{method}

	${PAYLOAD}=	Set Variable	{"status": "${STATUS}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/authentication/sso/idp_test
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}

Test Put /security/authentication/sso/{method} Functionality
	${response}=	API::Send Get Request	/security/authentication/sso/idp_test
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should contain	${resp}	'status': 'disabled', 'icon': 'sso.png', 'force_re-authentication': False, 'sign_request': False, 'enable_single_logout': False, 'name': 'idp_test'


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session