*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${NAME}=	idp_test
${STATUS}=	enabled
${ENTITY_ID}=	nodegrid_API_automation
${x.509}	remote_server
${SSO_URL}=	${SSOSERVER_URL}${SSOSERVER_SERVICE_PATH}
${ISSUER}=	${SSOSERVER_URL}${SSOSERVER_ISSUER_PATH}
${SYSTEMCERTSELECTION}=	systemcert-remote
${URL}=	${FTPSERVER2_URL}${SSOSERVER_CRT_PATH}
${FTP_USERNAME}	${FTPSERVER2_USER}
${FTP_PASSWORD}	${FTPSERVER2_PASSWORD}
${URL_XML}	${FTPSERVER2_URL}${SSOSERVER_XML_PATH}

*** Test Cases ***

Add SSO Identity Provider (status_code = 200)
	[Documentation]	Add SSO Identity Provider. Status code 200 is expected. 
	...	Endpoint: /security/authentication/sso

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${NAME}
	Set To Dictionary	${payload}	status=${STATUS}
	Set To Dictionary	${payload}	entity_id=${ENTITY_ID}
	Set To Dictionary	${payload}	x.509=${x.509}
	Set To Dictionary	${payload}	sso_url=${SSO_URL}
	Set To Dictionary	${payload}	issuer=${ISSUER}
	Set To Dictionary	${payload}	systemcertselection=${SYSTEMCERTSELECTION}
	Set To Dictionary	${payload}	url_xml=${URL_XML}
	Set To Dictionary	${payload}	url=${URL}
	Set To Dictionary	${payload}	username=${FTP_USERNAME}
	Set To Dictionary	${payload}	password=${FTP_PASSWORD}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/security/authentication/sso
	Should Be Equal   ${response.status_code}   ${200}

Test Post /security/authentication/sso Functionality
	[Documentation]	Get SSO Identity Providers. Status code 200 is expected.
	...	Endpoint: /security/authentication/sso

	${response}=	API::Send Get Request	/security/authentication/sso
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Contain	${resp}	'id': 'idp_test', 'name': 'idp_test', 'status': 'enabled', 'entity_id': 'nodegrid_API_automation', 'acs_url': 'https://${HOST}/saml/2-0/idp_test', 'logout_url':

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session