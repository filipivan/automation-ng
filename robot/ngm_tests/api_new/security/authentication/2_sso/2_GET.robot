*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get SSO Identity Providers (status_code = 200)
	[Documentation]	Get SSO Identity Providers. Status code 200 is expected. 
	...	Endpoint: /security/authentication/sso

	${response}=	API::Send Get Request	/security/authentication/sso
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get SSO Identity Providers Parameters
	${PARAMETERS}	Create list	'id':	'name':	'status':	'entity_id':	'acs_url':	'logout_url':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session