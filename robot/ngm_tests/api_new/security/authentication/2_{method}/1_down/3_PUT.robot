*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***


*** Test Cases ***

Move authentication method down (status_code = 200)
	[Documentation]	Move authentication method down. Status code 200 is expected. 
	...	Endpoint: /security/authentication/{method}/down

	${PAYLOAD}=	Create Dictionary

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/authentication/1/down
	Should Be Equal   ${response.status_code}   ${200}

Test Put /security/authentication/{method}/down Functionality
	[Documentation]	Get authentication methods. Status code 200 is expected.
	...	Endpoint: /security/authentication/{method}/down

	${response}=	API::Send Get Request	/security/authentication
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Contain	${resp}	{'id': '2', 'method': 'ldap or ad', 'status': 'enabled', 'fallback': 'disabled', 'index': '2', 'remote_server': '192.168.1.1'}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session