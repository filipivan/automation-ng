*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***

Get authentication method (status_code = 200)
	[Documentation]	Get authentication method. Status code 200 is expected. 
	...	Endpoint: /security/authentication/{method}

	${response}=	API::Send Get Request	/security/authentication/2
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get authentication Parameters
	${PARAMETERS}	Create list	'label':	'remote_server':	'radius_accounting_server':	'radius_port':	'radius_accounting_port':	'radius_timeout':	'radius_retries':	'radius_service_type_login':	'radius_service_type_framed':	'radius_service_type_callback_login':	'radius_service_type_callback_framed':	'radius_service_type_outbound':	 'radius_service_type_administrative':	 'tacacs+_accounting_server':	 'tacacs_port':	'tacacs+_timeout':	'tacacs+_retries':	'tacacs+_enforce_source_ip':	 'tacacs+_user_level_1':	 'tacacs+_user_level_2':	 'tacacs+_user_level_3':	 'tacacs+_user_level_4':	 'tacacs+_user_level_5':	 'tacacs+_user_level_6':	 'tacacs+_user_level_7':	 'tacacs+_user_level_8':	 'tacacs+_user_level_9':	 'tacacs+_user_level_10':	 'tacacs+_user_level_11':	 'tacacs+_user_level_12':	 'tacacs+_user_level_13':	 'tacacs+_user_level_14':	 'tacacs+_user_level_15':	 'ldap_ad_base':	 'ldap_port':	'ldap_ad_database_username':	 'ldap_ad_login_attribute':	 'ldap_ad_group_attribute':	 'ldap_ad_search_filter':	 'group_base':	 'kerberos_realm_domain_name':	 'kerberos_domain_name':	 'method':	'2-factor_authentication':	'status':	'tacacs+_service':	'tacacs+_version':	'ldap_ad_secure':	'fallback_if_denied_access':	'radius_enable_servicetype':	'authorize_ssh_pkey_users':	'tacacs+_enable_user-level':	'global_catalog_server':	'search_nested_groups':	'enable_ad_referrals':	'radius_secret':	'radius_confirm_secret':	'tacacs+_secret':	'tacacs+_confirm_secret':	'ldap_ad_database_password':	'ldap_ad_confirm_password':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session