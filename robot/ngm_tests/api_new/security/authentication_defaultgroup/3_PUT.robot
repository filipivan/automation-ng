*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${DEFAULT_GROUP_FOR_REMOTE_USERS}=	admin

*** Test Cases ***

Update authentication default group configuration (status_code = 200)
	[Documentation]	Update authentication default group configuration. Status code 200 is expected. 
	...	Endpoint: /security/authentication_defaultgroup

	${PAYLOAD}=	Set Variable	{"default_group_for_remote_users":"${DEFAULT_GROUP_FOR_REMOTE_USERS}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/authentication_defaultgroup
	Should Be Equal   ${response.status_code}   ${200}

Test Put security/authentication_defaultgroup Functionality
	[Documentation]	Get authentication default group configuration. Status code 200 is expected.
	...	Endpoint: /security/authentication_defaultgroup

	${response}=	API::Send Get Request	/security/authentication_defaultgroup
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}
	Should Contain	${resp}	'default_group_for_remote_users': 'admin'

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	${PAYLOAD}=	Set Variable	{"default_group_for_remote_users":"user"}
	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/security/authentication_defaultgroup
	Should Be Equal   ${response.status_code}   ${200}
	API::Delete::Session