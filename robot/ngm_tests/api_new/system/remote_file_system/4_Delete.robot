*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${MOUNTPOINT_NFS}=	mp2_nfs
${MOUNTPOINT_WINDOWSSHARING}=	mp2_windowssharing
${MOUNTPOINT_SSHFS_PASSWORD}=	mp2_sshfs_password
${MOUNTPOINT_SSHFS_SSHKEY}=	mp2_sshfs_sshkey

*** Test Cases ***
Delete file system in remote file system NFS (status_code = 200)
	[Documentation]	Delete file system in remote file system. Status code 200 is expected. 
	...	Endpoint: /system/remote_file_system

	${PAYLOAD}=	Set Variable	{"mountpoint":"${MOUNTPOINT_NFS}"}
	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/system/remote_file_system
	Should Be Equal   ${response.status_code}   ${200}

Delete file system in remote file system windows_sharing (status_code = 200)
	[Documentation]	Delete file system in remote file system. Status code 200 is expected. 
	...	Endpoint: /system/remote_file_system

	${PAYLOAD}=	Set Variable	{"mountpoint":"${MOUNTPOINT_WINDOWSSHARING}"}
	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/system/remote_file_system
	Should Be Equal   ${response.status_code}   ${200}

Delete file system in remote file system ssfhs password (status_code = 200)
	[Documentation]	Delete file system in remote file system. Status code 200 is expected. 
	...	Endpoint: /system/remote_file_system

	${PAYLOAD}=	Set Variable	{"mountpoint":"${MOUNTPOINT_SSHFS_PASSWORD}"}
	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/system/remote_file_system
	Should Be Equal   ${response.status_code}   ${200}

Delete file system in remote file system ssfhs sshkey (status_code = 200)
	[Documentation]	Delete file system in remote file system. Status code 200 is expected. 
	...	Endpoint: /system/remote_file_system

	${PAYLOAD}=	Set Variable	{"mountpoint":"${MOUNTPOINT_SSHFS_SSHKEY}"}
	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/system/remote_file_system
	Should Be Equal   ${response.status_code}   ${200}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session