*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${REMOTE_SERVER}=	192.168.16.156
${REMOTE_DIRECTORY}=	/mnt/zpe_sharedfolder
${INCLUDE_IN_THE_FILE_MANAGER}=	true
${MOUNT_ON-DEMAND}=	true
${DEFAULT_PASSWORD}=	admin

*** Test Cases ***

Add new file system in remote file system (status_code = 200) for nfs
	[Documentation]	Add new file system in remote file system. Status code 200 is expected. 
	...	Endpoint: /system/remote_file_system

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${PAYLOAD}	mount_point=mp2_nfs
	Set To Dictionary	${PAYLOAD}	file_system_type=nfs
	Set To Dictionary	${PAYLOAD}	remote_server=${REMOTE_SERVER}
	Set To Dictionary	${PAYLOAD}	remote_directory=${REMOTE_DIRECTORY}
	Set To Dictionary	${PAYLOAD}	include_in_the_file_manager=${INCLUDE_IN_THE_FILE_MANAGER}
	Set To Dictionary	${PAYLOAD}	mount_on-demand=${MOUNT_ON-DEMAND}

	Set To Dictionary	${PAYLOAD}	username=${USERNAME}
	Set To Dictionary	${PAYLOAD}	password=${DEFAULT_PASSWORD}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/remote_file_system
	Should Be Equal   ${response.status_code}   ${200}

Add new file system in remote file system (status_code = 200) for windowssharing
	[Documentation]	Add new file system in remote file system. Status code 200 is expected. 
	...	Endpoint: /system/remote_file_system

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${PAYLOAD}	mount_point=mp2_windowssharing
	Set To Dictionary	${PAYLOAD}	file_system_type=windowssharing
	Set To Dictionary	${PAYLOAD}	remote_server=${REMOTE_SERVER}
	Set To Dictionary	${PAYLOAD}	remote_directory=${REMOTE_DIRECTORY}
	Set To Dictionary	${PAYLOAD}	include_in_the_file_manager=${INCLUDE_IN_THE_FILE_MANAGER}
	Set To Dictionary	${PAYLOAD}	mount_on-demand=${MOUNT_ON-DEMAND}

	Set To Dictionary	${PAYLOAD}	username=${USERNAME}
	Set To Dictionary	${PAYLOAD}	password=${DEFAULT_PASSWORD}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/remote_file_system
	Should Be Equal   ${response.status_code}   ${200}


Add new file system in remote file system (status_code = 200) for sshfs with sshfs_password
	[Documentation]	Add new file system in remote file system. Status code 200 is expected. 
	...	Endpoint: /system/remote_file_system

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${PAYLOAD}	mount_point=mp2_sshfs_password
	Set To Dictionary	${PAYLOAD}	file_system_type=sshfs
	Set To Dictionary	${PAYLOAD}	remote_server=${REMOTE_SERVER}
	Set To Dictionary	${PAYLOAD}	remote_directory=${REMOTE_DIRECTORY}
	Set To Dictionary	${PAYLOAD}	include_in_the_file_manager=${INCLUDE_IN_THE_FILE_MANAGER}
	Set To Dictionary	${PAYLOAD}	mount_on-demand=${MOUNT_ON-DEMAND}

	Set To Dictionary	${PAYLOAD}	username=${USERNAME}
	Set To Dictionary	${PAYLOAD}	password=${DEFAULT_PASSWORD}

	Set To Dictionary	${PAYLOAD}	sshfs_authentication_method=sshfs_password

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/remote_file_system
	Should Be Equal   ${response.status_code}   ${200}

Add new file system in remote file system (status_code = 200) for sshfs with sshfs_sshkey
	[Documentation]	Add new file system in remote file system. Status code 200 is expected. 
	...	Endpoint: /system/remote_file_system

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${PAYLOAD}	mount_point=mp2_sshfs_sshkey
	Set To Dictionary	${PAYLOAD}	file_system_type=sshfs
	Set To Dictionary	${PAYLOAD}	remote_server=${REMOTE_SERVER}
	Set To Dictionary	${PAYLOAD}	remote_directory=${REMOTE_DIRECTORY}
	Set To Dictionary	${PAYLOAD}	include_in_the_file_manager=${INCLUDE_IN_THE_FILE_MANAGER}
	Set To Dictionary	${PAYLOAD}	mount_on-demand=${MOUNT_ON-DEMAND}

	Set To Dictionary	${PAYLOAD}	username=${USERNAME}
	Set To Dictionary	${PAYLOAD}	password=${DEFAULT_PASSWORD}

	Set To Dictionary	${PAYLOAD}	sshfs_authentication_method=sshfs_sshkey
	Set To Dictionary	${PAYLOAD}	ssh_key_file_path=/test

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/remote_file_system
	Should Be Equal   ${response.status_code}   ${200}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session