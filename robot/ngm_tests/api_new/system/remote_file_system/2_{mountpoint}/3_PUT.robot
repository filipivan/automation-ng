*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${INCLUDE_IN_THE_FILE_MANAGER}=	true

*** Test Cases ***
Update remote file system information (status_code = 200)
	[Documentation]	Update remote file system information. Status code 200 is expected. 
	...	Endpoint: /system/remote_file_system/{mountpoint}

	${PAYLOAD}=	Set Variable	{"include_in_the_file_manager":"${INCLUDE_IN_THE_FILE_MANAGER}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/system/remote_file_system/mp2_nfs
	Should Be Equal   ${response.status_code}   ${200}


Check update remote file system information (status_code = 200)
	[Documentation]	Verify if the last update make some change. Status code 200 is expected. 
	...	Endpoint: /system/remote_file_system/{mountpoint}
	
	${response}=	API::Send Get Request	/system/remote_file_system/mp2_nfs
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Should Contain	${resp}	{'remote_server': '192.168.16.156', 'remote_directory': '/mnt/zpe_sharedfolder', 'username': 'admin', 'ssh_key_file_path': '', 'file_system_type': 'nfs', 'mount_on-demand': True, 'include_in_the_file_manager': True, 'sshfs_authentication_method': 'sshfs_password', 'password': '********', 'mount_point': 'mp2_nfs'}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session