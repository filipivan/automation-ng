*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get list of slots (status_code = 200)
	[Documentation]	Get list of slots. Status code 200 is expected. 
	...	Endpoint: /system/slots

	${response}=	API::Send Get Request	/system/slots
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get list of slots Parameters
	${PARAMETERS}	Create list	'id':	'slot_number':	'card_sku':	'card_type':	'add-ons':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

	${IS_NSR}	API::Is NSR
	Skip If	not ${IS_NSR}	Slots only available on NSR

SUITE:Teardown
	API::Delete::Session