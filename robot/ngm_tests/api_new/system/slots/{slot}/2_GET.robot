*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***

Get slot information (status_code = 200)
	[Documentation]	Get slot information. Status code 200 is expected. 
	...	Endpoint: /system/slots/{slot}

	@{SLOTS}	SUITE:Get Slots
	Set Suite Variable	@{SLOTS}
	FOR	${SLOT}	IN	@{SLOTS}
		${response}=	API::Send Get Request	/system/slots/${SLOT}
		Should Be Equal   ${response.status_code}   ${200}
	END

Validate Get slot information Parameters
	${response}=	API::Send Get Request	/system/slots/${SLOTS[0]}
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	${PARAMETERS}	Create list	'slot_number':	'card_sku':	'card_type':
	CLI:Should Contain All	${resp}	${PARAMETERS}

Get wrong slot information (status_code = 400)
	[Documentation]	Get slot information. Status code 400 is expected. Because take a timeout error 
	...	Endpoint: /system/slots/{slot}
	${response}=	API::Send Get Request    PAYLOAD=${EMPTY}	PATH=/system/slots/10|full	ERROR_CONTROL=${FALSE}	EXPECT_ERROR=HTTPError: 400 Client Error: Bad Request for url: https://${HOST}/api/v1/system/slots/10%7Cfull
	Should Be Equal   ${response}   HTTPError: 400 Client Error: Bad Request for url: https://${HOST}/api/v1/system/slots/10%7Cfull

*** Keywords ***
SUITE:Setup
	API::Post::Session

	${IS_NSR}	API::Is NSR
	Skip If	not ${IS_NSR}	Slots only available on NSR

SUITE:Teardown
	API::Delete::Session

SUITE:Get Slots
	${response}=	API::Send Get Request	/system/slots
	Should Be Equal   ${response.status_code}   ${200}
	${SLOT}=	evaluate	str(${response.json()})
	${SLOT}=	Remove String	${SLOT}	[
	${SLOT}=	Remove String	${SLOT}	]
	${SLOT}	split string	${SLOT}	},
	@{SLOTS}	Create List
	FOR	${S}	IN	@{SLOT}
		${COLUMNS}=	Split String	${S}	',
		FOR	${COLUMN}	IN	@{COLUMNS}
			${CONTAINS}	Run Keyword And Return Status	Should Contain	${COLUMN}	'id':
			IF	${CONTAINS}
				${ID}=	Replace String Using Regexp	${COLUMN}	\\s*\{\'id\'\:\\s\'	${EMPTY}
				Append to List	${SLOTS}	${ID}
				Exit For Loop If	${CONTAINS}
			END
		END
	END
	[Return]	@{SLOTS}