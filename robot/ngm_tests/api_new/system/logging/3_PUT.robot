*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${SESSION_STRING_1}=	test_string
${SESSION_STRING_2}=	test_string2
${SESSION_STRING_3}=	test_string3
${SESSION_STRING_4}=	test_string4
${SESSION_STRING_5}=	${EMPTY}
${ENABLE_SESSION_LOGGING}=	False
${ENABLE_SESSION_LOGGING_ALERTS}=	False

*** Test Cases ***

Update system logging configuration (status_code = 200)
	[Documentation]	Update system logging configuration. Status code 200 is expected. 
	...	Endpoint: /system/logging

	${PAYLOAD}=	Set Variable	{"session_string_1":"${SESSION_STRING_1}", "session_string_2":"${SESSION_STRING_2}", "session_string_3":"${SESSION_STRING_3}", "session_string_4":"${SESSION_STRING_4}", "session_string_5":"${SESSION_STRING_5}", "enable_session_logging":"${ENABLE_SESSION_LOGGING}", "enable_session_logging_alerts":"${ENABLE_SESSION_LOGGING_ALERTS}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/system/logging
	Should Be Equal   ${response.status_code}   ${200}

Check update logging preferences (status_code = 200)
	[Documentation]	Verify if the last update make some change. Status code 200 is expected. 
	...	Endpoint:	/system/logging

	${response}=	API::Send Get Request	/system/logging
	${resp}=    Evaluate   str(${response.json()})

	${PAYLOAD}=	Set Variable	{'session_string_1': '${SESSION_STRING_1}', 'session_string_2': '${SESSION_STRING_2}', 'session_string_3': '${SESSION_STRING_3}', 'session_string_4': '${SESSION_STRING_4}', 'session_string_5': '${SESSION_STRING_5}', 'enable_session_logging': ${ENABLE_SESSION_LOGGING}, 'enable_session_logging_alerts': ${ENABLE_SESSION_LOGGING_ALERTS}}
	Should Be Equal	${resp}	${PAYLOAD}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session