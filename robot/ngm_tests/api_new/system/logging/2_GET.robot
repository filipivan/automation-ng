*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get system logging configuration (status_code = 200)
	[Documentation]	Get system logging configuration. Status code 200 is expected. 
	...	Endpoint: /system/logging

	${response}=	API::Send Get Request	/system/logging
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get system logging configuration Parameters
	${PARAMETERS}	Create list	'session_string_1':	'session_string_2':	'session_string_3':	'session_string_4':	'session_string_5':	'enable_session_logging':	'enable_session_logging_alerts':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session