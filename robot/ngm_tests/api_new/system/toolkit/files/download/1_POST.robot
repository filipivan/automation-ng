*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${FILENAME}=	strongswan_remove.sh
${URL}=	ftp://192.168.2.88:21//files/strongswan_remove.sh
${ACCESS_USERNAME}=	ftpuser
${PASSWORD}=	ftpuser

*** Test Cases ***

Download a file to Nodegrid (status_code = 200)
	[Documentation]	"Download a file into the Nodegrid using the following protocols: http, https, tftp, ftp, sftp or scp. Permission 600 will be given to the downloaded if the pemission parameters empty. Also the ownership will be set to the user and primary group in the ticket's session. The user must be in the admin group to call this endpoint". Status code 200 is expected. 
	...	Endpoint: /system/toolkit/files/download

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	filename=${FILENAME}
	Set To Dictionary	${payload}	url=${URL}
	Set To Dictionary	${payload}	username=${ACCESS_USERNAME}
	Set To Dictionary	${payload}	password=${PASSWORD}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/files/download
	Should Be Equal   ${response.status_code}   ${200}

#Download a file to Nodegrid (status_code = 400)
#	[Documentation]	"Download a file into the Nodegrid using the following protocols: http, https, tftp, ftp, sftp or scp. Permission 600 will be given to the downloaded if the pemission parameters empty. Also the ownership will be set to the user and primary group in the ticket's session. The user must be in the admin group to call this endpoint". Status code 400 is expected. 
#	...	Endpoint: /system/toolkit/files/download

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	filename=${FILENAME}
#	Set To Dictionary	${payload}	url=${URL}
#	Set To Dictionary	${payload}	username=${USERNAME}
#	Set To Dictionary	${payload}	password=${PASSWORD}
#
#	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/files/download
#	Should Be Equal   ${response.status_code}   ${400}

#Download a file to Nodegrid (status_code = 403)
#	[Documentation]	"Download a file into the Nodegrid using the following protocols: http, https, tftp, ftp, sftp or scp. Permission 600 will be given to the downloaded if the pemission parameters empty. Also the ownership will be set to the user and primary group in the ticket's session. The user must be in the admin group to call this endpoint". Status code 403 is expected. 
#	...	Endpoint: /system/toolkit/files/download

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	filename=${FILENAME}
#	Set To Dictionary	${payload}	url=${URL}
#	Set To Dictionary	${payload}	username=${USERNAME}
#	Set To Dictionary	${payload}	password=${PASSWORD}
#
#	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/files/download
#	Should Be Equal   ${response.status_code}   ${403}

#Download a file to Nodegrid (status_code = 404)
#	[Documentation]	"Download a file into the Nodegrid using the following protocols: http, https, tftp, ftp, sftp or scp. Permission 600 will be given to the downloaded if the pemission parameters empty. Also the ownership will be set to the user and primary group in the ticket's session. The user must be in the admin group to call this endpoint". Status code 404 is expected.
#	...	Endpoint: /system/toolkit/files/download
#
#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	filename=test_unvailable_file.test
#	Set To Dictionary	${payload}	url=${URL}
#	Set To Dictionary	${payload}	username=${USERNAME}
#	Set To Dictionary	${payload}	password=${PASSWORD}
#
#	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/files/download
#	Should Be Equal   ${response.status_code}   ${404}

#Download a file to Nodegrid (status_code = 405)
#	[Documentation]	"Download a file into the Nodegrid using the following protocols: http, https, tftp, ftp, sftp or scp. Permission 600 will be given to the downloaded if the pemission parameters empty. Also the ownership will be set to the user and primary group in the ticket's session. The user must be in the admin group to call this endpoint". Status code 405 is expected. 
#	...	Endpoint: /system/toolkit/files/download

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	filename=${FILENAME}
#	Set To Dictionary	${payload}	url=${URL}
#	Set To Dictionary	${payload}	username=${USERNAME}
#	Set To Dictionary	${payload}	password=${PASSWORD}
#
#	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/files/download
#	Should Be Equal   ${response.status_code}   ${405}

#Download a file to Nodegrid (status_code = 500)
#	[Documentation]	"Download a file into the Nodegrid using the following protocols: http, https, tftp, ftp, sftp or scp. Permission 600 will be given to the downloaded if the pemission parameters empty. Also the ownership will be set to the user and primary group in the ticket's session. The user must be in the admin group to call this endpoint". Status code 500 is expected.
#	...	Endpoint: /system/toolkit/files/download
#
#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	filename=${FILENAME}
#	Set To Dictionary	${payload}	url=${URL}
#	Set To Dictionary	${payload}	username=${USERNAME}
#	Set To Dictionary	${payload}	password=testuser
#
#	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/files/download
#	Should Be Equal   ${response.status_code}   ${500}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session