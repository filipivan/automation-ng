*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${FILENAME}=	/bin/date

*** Test Cases ***

Execute a script or binary in Nodegrid (status_code = 200)
	[Documentation]	"Execute a script or binary in Nodegrid. The user must be in the admin group to call this endpoint". Status code 200 is expected. 
	...	Endpoint: /system/toolkit/files/execute

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	filename=${FILENAME}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/files/execute
	Should Be Equal   ${response.status_code}   ${200}

#Execute a script or binary in Nodegrid (status_code = 400)
#	[Documentation]	"Execute a script or binary in Nodegrid. The user must be in the admin group to call this endpoint". Status code 400 is expected. 
#	...	Endpoint: /system/toolkit/files/execute

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	filename=${FILENAME}
#
#	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/files/execute
#	Should Be Equal   ${response.status_code}   ${400}

#Execute a script or binary in Nodegrid (status_code = 403)
#	[Documentation]	"Execute a script or binary in Nodegrid. The user must be in the admin group to call this endpoint". Status code 403 is expected. 
#	...	Endpoint: /system/toolkit/files/execute

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	filename=${FILENAME}
#
#	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/files/execute
#	Should Be Equal   ${response.status_code}   ${403}

#Execute a script or binary in Nodegrid (status_code = 405)
#	[Documentation]	"Execute a script or binary in Nodegrid. The user must be in the admin group to call this endpoint". Status code 405 is expected. 
#	...	Endpoint: /system/toolkit/files/execute

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	filename=${FILENAME}
#
#	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/files/execute
#	Should Be Equal   ${response.status_code}   ${405}

#Execute a script or binary in Nodegrid (status_code = 500)
#	[Documentation]	"Execute a script or binary in Nodegrid. The user must be in the admin group to call this endpoint". Status code 500 is expected. 
#	...	Endpoint: /system/toolkit/files/execute

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	filename=${FILENAME}
#
#	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/files/execute
#	Should Be Equal   ${response.status_code}   ${500}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session