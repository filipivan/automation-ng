*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	NEED-CREATION
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${FILENAME}=	/software
${URL}=	scp://localhost:22//tmp/software_new  #Just need to fix the path to take the local file to upload to the system.
${USERNAME}=	root
${PASSWORD}=	root

*** Test Cases ***

Upload a file from the Nodegrid (status_code = 200)
	[Documentation]	"Upload a file from the Nodegrid to an external destinations using the following protocols: http, https, tftp, ftp, sftp or scp. The user must be in the admin group to call this endpoint". Status code 200 is expected. 
	...	Endpoint: /system/toolkit/files/upload

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	filename=${FILENAME}
	Set To Dictionary	${payload}	url=${URL}
	Set To Dictionary	${payload}	username=${USERNAME}
	Set To Dictionary	${payload}	password=${PASSWORD}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/files/upload
	Should Be Equal   ${response.status_code}   ${200}

#Upload a file from the Nodegrid (status_code = 400)
#	[Documentation]	"Upload a file from the Nodegrid to an external destinations using the following protocols: http, https, tftp, ftp, sftp or scp. The user must be in the admin group to call this endpoint". Status code 400 is expected. 
#	...	Endpoint: /system/toolkit/files/upload

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	filename=${FILENAME}
#	Set To Dictionary	${payload}	url=${URL}
#	Set To Dictionary	${payload}	username=${USERNAME}
#	Set To Dictionary	${payload}	password=${PASSWORD}
#
#	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/files/upload
#	Should Be Equal   ${response.status_code}   ${400}

#Upload a file from the Nodegrid (status_code = 403)
#	[Documentation]	"Upload a file from the Nodegrid to an external destinations using the following protocols: http, https, tftp, ftp, sftp or scp. The user must be in the admin group to call this endpoint". Status code 403 is expected. 
#	...	Endpoint: /system/toolkit/files/upload

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	filename=${FILENAME}
#	Set To Dictionary	${payload}	url=${URL}
#	Set To Dictionary	${payload}	username=${USERNAME}
#	Set To Dictionary	${payload}	password=${PASSWORD}
#
#	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/files/upload
#	Should Be Equal   ${response.status_code}   ${403}

#Upload a file from the Nodegrid (status_code = 405)
#	[Documentation]	"Upload a file from the Nodegrid to an external destinations using the following protocols: http, https, tftp, ftp, sftp or scp. The user must be in the admin group to call this endpoint". Status code 405 is expected. 
#	...	Endpoint: /system/toolkit/files/upload

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	filename=${FILENAME}
#	Set To Dictionary	${payload}	url=${URL}
#	Set To Dictionary	${payload}	username=${USERNAME}
#	Set To Dictionary	${payload}	password=${PASSWORD}
#
#	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/files/upload
#	Should Be Equal   ${response.status_code}   ${405}

#Upload a file from the Nodegrid (status_code = 500)
#	[Documentation]	"Upload a file from the Nodegrid to an external destinations using the following protocols: http, https, tftp, ftp, sftp or scp. The user must be in the admin group to call this endpoint". Status code 500 is expected. 
#	...	Endpoint: /system/toolkit/files/upload

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	filename=${FILENAME}
#	Set To Dictionary	${payload}	url=${URL}
#	Set To Dictionary	${payload}	username=${USERNAME}
#	Set To Dictionary	${payload}	password=${PASSWORD}
#
#	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/files/upload
#	Should Be Equal   ${response.status_code}   ${500}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session