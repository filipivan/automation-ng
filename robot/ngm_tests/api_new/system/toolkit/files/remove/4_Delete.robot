*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${FILENAME}=	strongswan_remove.sh

*** Test Cases ***

Remove a file from Nodegrid (status_code = 200)
	[Documentation]	"Remove a file from Nodegrid. The user must be in the admin group to call this endpoint". Status code 200 is expected. 
	...	Endpoint: /system/toolkit/files/remove

	${PAYLOAD}=	Set Variable	{"filename":"${FILENAME}"}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/files/remove
	Should Be Equal   ${response.status_code}   ${200}

#Remove a file from Nodegrid (status_code = 400)
#	[Documentation]	"Remove a file from Nodegrid. The user must be in the admin group to call this endpoint". Status code 400 is expected. 
#	...	Endpoint: /system/toolkit/files/remove

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	filename=${FILENAME}
#
#	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/files/remove
#	Should Be Equal   ${response.status_code}   ${400}

#Remove a file from Nodegrid (status_code = 403)
#	[Documentation]	"Remove a file from Nodegrid. The user must be in the admin group to call this endpoint". Status code 403 is expected. 
#	...	Endpoint: /system/toolkit/files/remove

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	filename=${FILENAME}
#
#	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/files/remove
#	Should Be Equal   ${response.status_code}   ${403}

#Remove a file from Nodegrid (status_code = 405)
#	[Documentation]	"Remove a file from Nodegrid. The user must be in the admin group to call this endpoint". Status code 405 is expected. 
#	...	Endpoint: /system/toolkit/files/remove

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	filename=${FILENAME}
#
#	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/files/remove
#	Should Be Equal   ${response.status_code}   ${405}

#Remove a file from Nodegrid (status_code = 500)
#	[Documentation]	"Remove a file from Nodegrid. The user must be in the admin group to call this endpoint". Status code 500 is expected. 
#	...	Endpoint: /system/toolkit/files/remove

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	filename=${FILENAME}
#
#	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/files/remove
#	Should Be Equal   ${response.status_code}   ${500}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session