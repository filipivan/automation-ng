*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${CUSTOMER_CODE}=	CU5T0M3R
${ENROLLMENT_KEY}=	myOwn!K3y
${URL}=	https://zpecloud.com

*** Test Cases ***

#Cloud Enrollment (status_code = 200)
#	[Documentation]	Executes the enrollment and transfer ownership for ZPE Cloud Platform. Status code 200 is expected.
#	...	Endpoint: /system/toolkit/cloudenrollment
#
#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	customer_code=${CUSTOMER_CODE}
#	Set To Dictionary	${payload}	enrollment_key=${ENROLLMENT_KEY}
#	Set To Dictionary	${payload}	url=${URL}
#
#	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/cloudenrollment
#	Should Be Equal   ${response.status_code}   ${200}

Cloud Enrollment (status_code = 400)
	[Documentation]	Executes the enrollment and transfer ownership for ZPE Cloud Platform. Status code 400 is expected.
	...	Endpoint: /system/toolkit/cloudenrollment

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	customer_code=${CUSTOMER_CODE}
	Set To Dictionary	${payload}	enrollment_key=${ENROLLMENT_KEY}
	Set To Dictionary	${payload}	url=${URL}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/cloudenrollment	ERROR_CONTROL=${FALSE}	EXPECT_ERROR=HTTPError: 400 Client Error: Bad Request for url: https://${HOST}/api/v1/system/toolkit/cloudenrollment
	Should Be Equal   ${response}   HTTPError: 400 Client Error: Bad Request for url: https://${HOST}/api/v1/system/toolkit/cloudenrollment


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session