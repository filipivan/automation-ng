*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***


*** Test Cases ***

#Stop enrollment (status_code = 200)
#	[Documentation]	Stop enrollment process. Status code 200 is expected.
#	...	Endpoint: /system/toolkit/cloudenrollment/stopenrollment
#
#	${PAYLOAD}=	Create Dictionary
#
#	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/cloudenrollment/stopenrollment
#	Should Be Equal   ${response.status_code}   ${200}

Stop enrollment (status_code = 400)
	[Documentation]	Stop enrollment process. Status code 400 is expected.
	...	Endpoint: /system/toolkit/cloudenrollment/stopenrollment

	${PAYLOAD}=	Create Dictionary

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/cloudenrollment/stopenrollment	ERROR_CONTROL=${FALSE}	EXPECT_ERROR=HTTPError: 400 Client Error: Bad Request for url: https://${HOST}/api/v1/system/toolkit/cloudenrollment/stopenrollment
	Should Be Equal   ${response}   HTTPError: 400 Client Error: Bad Request for url: https://${HOST}/api/v1/system/toolkit/cloudenrollment/stopenrollment


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session