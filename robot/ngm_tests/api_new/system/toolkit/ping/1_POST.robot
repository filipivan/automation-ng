*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${IP_ADDRESS}=	${HOSTPEER}
${INTERFACE}=	eth0

*** Test Cases ***
#TODO: We gotta check how to increase the timeout. Command works manually, but it takes some seconds
Ping specified ip address (status_code = 200)
	[Documentation]	Ping specified ip address. Status code 200 is expected. 
	...	Endpoint: /system/toolkit/ping

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	ip_address=192.168.7.25
	Set To Dictionary	${payload}	interface=${INTERFACE}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/ping	ERROR_CONTROL=${FALSE}
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session