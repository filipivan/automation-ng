*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${HOSTNAME}=	www.zpesystems.com

*** Test Cases ***

Perform a DNS Lookup (status_code = 200)
	[Documentation]	Perform a DNS Lookup. Status code 200 is expected. 
	...	Endpoint: /system/toolkit/dnslookup

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	hostname=${HOSTNAME}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/dnslookup
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session