*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${DESTINATION}=	savesettings-local
${FROM}=	applysettings-local
${FILENAME}=	nodegrid_test.cfg

*** Test Cases ***

Apply previously saved settings (status_code = 200)
	[Documentation]	Apply previously saved settings. Status code 200 is expected. 
	...	Endpoint: /system/toolkit/applysettings

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	from=${FROM}
	Set To Dictionary	${payload}	filename=${FILENAME}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/applysettings
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

	#Create an save settings file
	${PAYLOAD_SAVESETTINGS}=	Create Dictionary
	Set To Dictionary	${PAYLOAD_SAVESETTINGS}	destination=${DESTINATION}
	Set To Dictionary	${PAYLOAD_SAVESETTINGS}	filename=${FILENAME}
	API::Send Post Request	PAYLOAD=${PAYLOAD_SAVESETTINGS}	PATH=/system/toolkit/savesettings

SUITE:Teardown
	API::Delete::Session