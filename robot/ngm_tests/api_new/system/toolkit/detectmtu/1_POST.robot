*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${IP_ADDRESS}=	${HOSTPEER}
${SOURCE_IP_ADDRESS}=	${HOST}

*** Test Cases ***

Automatically detect MTU (status_code = 200)
	[Documentation]	Automatically detect MTU for a given route.. Status code 200 is expected. 
	...	Endpoint: /system/toolkit/detectmtu

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	ip_address=${IP_ADDRESS}
	Set To Dictionary	${payload}	source_ip_address=${SOURCE_IP_ADDRESS}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/detectmtu	ERROR_CONTROL=${FALSE}
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session