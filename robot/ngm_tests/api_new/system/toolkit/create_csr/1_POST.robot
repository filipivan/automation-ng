*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${COUNTRY_CODE}=	US
${STATE}=	California
${LOCALITY}=	Fremont
${ORGANIZATION}=	ZPE Systems
${ORGANIZATION_UNIT}=	Nodegrid
${COMMON_NAME}=	www.zpesystems.com
${EMAIL_ADDRESS}=	zpe@zpe.com
#${SELF_SIGN}=	yes

*** Test Cases ***

Create CSR (status_code = 200)
	[Documentation]	Generates a Certificate Signing Request and stores the private key inside Nodegrid. With a new Signed Certificate from the CSR generated, you can apply this Certificate in the /system/toolkit/certificate endpoint.. Status code 200 is expected. 
	...	Endpoint: /system/toolkit/create_csr

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	country_code=${COUNTRY_CODE}
	Set To Dictionary	${payload}	state=${STATE}
	Set To Dictionary	${payload}	locality=${LOCALITY}
	Set To Dictionary	${payload}	organization=${ORGANIZATION}
	Set To Dictionary	${payload}	organization_unit=${ORGANIZATION_UNIT}
	Set To Dictionary	${payload}	common_name=${COMMON_NAME}
	Set To Dictionary	${payload}	email_address=${EMAIL_ADDRESS}
#	Set To Dictionary	${payload}	self_sign=${SELF_SIGN}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/create_csr
	Should Be Equal   ${response.status_code}   ${200}

#Create CSR (status_code = 400)
#	[Documentation]	Generates a Certificate Signing Request and stores the private key inside Nodegrid. With a new Signed Certificate from the CSR generated, you can apply this Certificate in the /system/toolkit/certificate endpoint.. Status code 400 is expected.
#	...	Endpoint: /system/toolkit/create_csr

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	country_code=${COUNTRY_CODE}
#	Set To Dictionary	${payload}	state=${STATE}
#	Set To Dictionary	${payload}	locality=${LOCALITY}
#	Set To Dictionary	${payload}	organization=${ORGANIZATION}
#	Set To Dictionary	${payload}	organization_unit=${ORGANIZATION_UNIT}
#	Set To Dictionary	${payload}	common_name=${COMMON_NAME}
#	Set To Dictionary	${payload}	email_address=${EMAIL_ADDRESS}
#
#	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/create_csr
#	Should Be Equal   ${response.status_code}   ${400}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session