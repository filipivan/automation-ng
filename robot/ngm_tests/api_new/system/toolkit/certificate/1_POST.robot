*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${COUNTRY_CODE}=	US
${STATE}=	California
${LOCALITY}=	Fremont
${ORGANIZATION}=	ZPE Systems
${ORGANIZATION_UNIT}=	Engineering
${COMMON_NAME}=	www.zpesystems.com
${EMAIL_ADDRESS}=	zpe@zpesystems.com
${SELF_SIGN}=	no

${PASSWD}		abcd1234
${CMP_NAME}		ZPE
${PEM_CERT}		nodegrid.pem
${CSR_ROOT}		rootcert.csr
${CRT_ROOT}		rootcert.crt
${SRL_ROOT}		rootcert.srl
${PRIVATE_KEY}	ca_privkey.key

${FROM}=	systemcert-remote
${URL}=	ftp://${FTPSERVER2_IP}/files/${PEM_CERT}
${FTP_USERNAME}=	${FTPSERVER2_USER}
${PASSWORD}=	${FTPSERVER2_PASSWORD}
${PASSPHRASE}=	abcd1234

*** Test Cases ***

Apply certificate (status_code = 200)
	[Documentation]	Apply certificate from one of the locations. This procedure will restart the webserver and may disconnect active sessions. You can apply a signed Certificate from the CSR generated in /system/toolkit/create_csr endpoint.. Status code 200 is expected. 
	...	Endpoint: /system/toolkit/certificate

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	from=${FROM}
	Set To Dictionary	${payload}	url=${URL}
	Set To Dictionary	${payload}	username=${FTP_USERNAME}
	Set To Dictionary	${payload}	password=${PASSWORD}
	Set To Dictionary	${payload}	passphrase=${PASSPHRASE}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/certificate
	Should Be Equal   ${response.status_code}   ${200}
	Sleep	30s

*** Keywords ***
SUITE:Setup
	API::Post::Session
	CLI:open
	SUITE:Create Not Signed CSR
	SUITE:Sign Certificate
	CLI:Close Connection

SUITE:Teardown
	API::Delete::Session

SUITE:Create Not Signed CSR
	CLI:Write 				create_csr
	CLI:Set  				country_code=${COUNTRY_CODE} state=${STATE} locality=${LOCALITY} organization='${ORGANIZATION}'
	CLI:Set 				organization_unit=${ORGANIZATION_UNIT} common_name=${COMMON_NAME} email_address=${EMAIL_ADDRESS}
	CLI:Set 				subject_alternative_names=test.zpesystems.com self_sign=no
	${REQUEST}=				CLI:Write  	generate_csr
	Should Contain			${REQUEST}	-----BEGIN CERTIFICATE REQUEST-----
	Should Contain			${REQUEST}	-----END CERTIFICATE REQUEST-----
	CLI:Write 				finish
	CLI:Connect As Root
	CLI:Enter Path  		/tmp/
	${CSR_PEM}=				CLI:Write  				cat csr.pem
	CLI:Should Be Equal  	${CSR_PEM}	${REQUEST}

SUITE:Sign Certificate
	CLI:Connect As Root
	CLI:Enter Path  	  	/tmp/
	CLI:Write   		 	openssl ecparam -out ${PRIVATE_KEY} -name prime256v1 -genkey
	SUITE:Fill Root CSR
	CLI:Write  		 		openssl x509 -req -sha256 -days 365 -in ${CSR_ROOT} -signkey ${PRIVATE_KEY} -out ${CRT_ROOT}
	CLI:Write 				openssl x509 -req -in /tmp/csr.pem -CA ${CRT_ROOT} -CAkey ${PRIVATE_KEY} -CAcreateserial -out ${PEM_CERT} -days 60 -sha256
	CLI:Reconnect As Root
	@{FILES}= 				Create List 	${CSR_ROOT}	${CRT_ROOT}	${PEM_CERT}	${PRIVATE_KEY}
	Run Keyword If	${NGVERSION} < 5.4	Append To List	${FILES}	${SRL_ROOT}	#in newer versions of OpenSSL this file is not generated anymore.
	${ROOT_FILES}			CLI:Ls			/tmp/
	CLI:Should Contain All	${ROOT_FILES}	${FILES}
	SUITE:Transfer Certificate To Remote Server

SUITE:Fill Root CSR
	Write  					openssl req -new -sha256 -key ${PRIVATE_KEY} -out ${CSR_ROOT}
	@{FIELDS}=	Create List	${COUNTRY_CODE}	${STATE}	${LOCALITY}	 ${ORGANIZATION}	${ORGANIZATION_UNIT}	${COMMON_NAME}	${EMAIL_ADDRESS}	${PASSWD}
	FOR	${FIELD}	IN 	@{FIELDS}
		Write 	${FIELD}
		Read Until Regexp 	(\\]:)
	END
	CLI:Write	${CMP_NAME}

SUITE:Transfer Certificate To Remote Server
	[Documentation]  creates a backup of /home/root/.ssh/known_hosts and then removes the original file
				...	so a new fingerprint is always generated and the prompt is predictable. after the file
				...	is transfered, the backup file becomes the standard know_hosts file again.

	CLI:Write				cp /home/root/.ssh/known_hosts /home/root/.ssh/known_hosts.backup
	CLI:Write  				rm /home/root/.ssh/known_hosts
	Write					scp /tmp/${PEM_CERT} ${FTPSERVER2_USER}@${FTPSERVER2_IP}:/home/${FTPSERVER2_USER}/ftp/files
	Read Until 				(yes/no/[fingerprint])?
	Write					yes
	Read Until				password:
	Write					${FTPSERVER2_PASSWORD}
	CLI:Write  				mv /home/root/.ssh/known_hosts.backup /home/root/.ssh/known_hosts