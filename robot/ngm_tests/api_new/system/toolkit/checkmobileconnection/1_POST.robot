*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CONNECTION}=	ETH0
${IP_ADDRESS}=	${FTPSERVER_IP}

*** Test Cases ***
Test Mobile Broadband Connection (status_code = 200)
	[Documentation]	Must have a SIM card. Execute a health check in the given connection with given IP address. Status code 200 is expected. To run this test, you must create a GSM.
	...	Endpoint: /system/toolkit/checkmobileconnection

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	connection=${CONNECTION}
	Set To Dictionary	${payload}	ip_address=${IP_ADDRESS}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/checkmobileconnection	ERROR_CONTROL=${FALSE}
	${response}=	Convert To String    ${response}
	Should Be Equal   ${response}	<Response [200]>

*** Keywords ***
SUITE:Setup
	CLI:Open
	${HAS_SIMCARD}=	CLI:Has Wireless Modem With SIM Card
	CLI:Close Connection

	API::Post::Session

	Skip If	not ${HAS_SIMCARD}	SIM CARD is required to do this test
	
SUITE:Teardown
	API::Delete::Session