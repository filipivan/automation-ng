*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***

Reboot operating system (status_code = 200)
	[Documentation]	Reboot operating system. Status code 200 is expected. 
	...	Endpoint: /system/toolkit/reboot
	[Tags]	reboot
	${PAYLOAD}=	Create Dictionary

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/reboot
	Should Be Equal   ${response.status_code}   ${200}
	Sleep    100s

	Wait Until Keyword Succeeds	5x	60s	API::Post::Session

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session