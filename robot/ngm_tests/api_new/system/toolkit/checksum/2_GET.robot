*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get System Configuration Checksum (status_code = 200)
	[Documentation]	Get System Configuration Checksum. Status code 200 is expected. 
	...	Endpoint: /system/toolkit/checksum

	${response}=	API::Send Get Request	/system/toolkit/checksum
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get System Configuration Checksum Parameters
	${PARAMETERS}	Create list	'label':	'reference':	'checksum':	'action':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session