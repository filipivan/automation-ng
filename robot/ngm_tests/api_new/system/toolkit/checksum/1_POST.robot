*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${FROM}=	systemcert-localcomputer
${FILENAME}=	cert.pem

*** Test Cases ***

System Configuration Checksum (status_code = 200)
	[Documentation]	System Configuration Checksum. Status code 200 is expected. 
	...	Endpoint: /system/toolkit/checksum

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	from=${FROM}
	Set To Dictionary	${payload}	filename=${FILENAME}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/checksum
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session