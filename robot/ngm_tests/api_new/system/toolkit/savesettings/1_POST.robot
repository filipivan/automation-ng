*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${DESTINATION}=	savesettings-local
${FILENAME}=	nodegrid_test.cfg

*** Test Cases ***

Save nodegrid settings (status_code = 200)
	[Documentation]	Save nodegrid settings. Status code 200 is expected. 
	...	Endpoint: /system/toolkit/savesettings

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	destination=${DESTINATION}
	Set To Dictionary	${payload}	filename=${FILENAME}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/toolkit/savesettings
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session