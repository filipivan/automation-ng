*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***


*** Test Cases ***

Acknowledge alarm state (status_code = 200)
	[Documentation]	Acknowledge alarm state. Status code 200 is expected. 
	...	Endpoint: /system/preferences/alarmstate

	${PAYLOAD}=	Create Dictionary

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/preferences/alarmstate
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session