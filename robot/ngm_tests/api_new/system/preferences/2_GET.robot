*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get system preferences (status_code = 200)
	[Documentation]	Get system preferences. Status code 200 is expected. 
	...	Endpoint: /system/preferences

	${response}=	API::Send Get Request	/system/preferences
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get system preferences Parameters
	${PARAMETERS}	Create list	'address_location':	'coordinates':	'help_url':	'idle_timeout':	'revision_tag':	'webui_header_hostname_color':	'url':	'username':	'password':	'percentage_to_trigger_events':	'unit_ipv4_address':	'unit_netmask':	'iso_url':	'unit_interface':	'show_hostname_on_webui_header':	'logo_image_selection':	'the_path_in_url_to_be_used_as_absolute_path_name':	'enable_banner':	'enable_license_utilization_rate':	'banner':	'logo_image':	'latest_profile_applied':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session