*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

#${ADDRESS_LOCATION}=
#${COORDINATES}=
${HELP_URL}=	https://www.zpesystems.com/ng/v5_6/NodegridManual5.6.html
${IDLE_TIMEOUT}=	0
#${URL}=
#${USERNAME}=
#${PASSWORD}=
${PERCENTAGE_TO_TRIGGER_EVENTS}=	90
${UNIT_IPV4_ADDRESS}=	192.168.160.1
${UNIT_NETMASK}=	255.255.255.0
#${ISO_URL}=	http://ServerIPAddress/PATH/FILENAME.ISO
#${UNIT_INTERFACE}=	eth0
${LOGO_IMAGE_SELECTION}=	false
${THE_PATH_IN_URL_TO_BE_USED_AS_ABSOLUTE_PATH_NAME}=	false
${ENABLE_BANNER}=	true
${ENABLE_LICENSE_UTILIZATION_RATE}=	true
${BANNER}=	This is a test

*** Test Cases ***
Update system preferences (status_code = 200)
	[Documentation]	Update system preferences. Status code 200 is expected. 
	...	Endpoint: /system/preferences

	${PAYLOAD}=	Set variable	{"help_url":"${HELP_URL}", "idle_timeout":"${IDLE_TIMEOUT}", "percentage_to_trigger_events":"${PERCENTAGE_TO_TRIGGER_EVENTS}", "unit_ipv4_address":"${UNIT_IPV4_ADDRESS}", "unit_netmask":"${UNIT_NETMASK}", "logo_image_selection":"${LOGO_IMAGE_SELECTION}", "the_path_in_url_to_be_used_as_absolute_path_name": ${THE_PATH_IN_URL_TO_BE_USED_AS_ABSOLUTE_PATH_NAME}, "enable_banner":"${ENABLE_BANNER}", "enable_license_utilization_rate":"${ENABLE_LICENSE_UTILIZATION_RATE}", "banner":"${BANNER}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/system/preferences
	Should Be Equal   ${response.status_code}   ${200}

Check update system preferences (status_code = 200)
	[Documentation]	Verify if the last update make some change. Status code 200 is expected. 
	...	Endpoint:	/system/preferences

	${response}=	API::Send Get Request	/system/preferences
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})

	@{PARAMETERS}	Create List	'help_url': '${HELP_URL}'	'idle_timeout': '${IDLE_TIMEOUT}'	'percentage_to_trigger_events': '${PERCENTAGE_TO_TRIGGER_EVENTS}'	'unit_ipv4_address': '${UNIT_IPV4_ADDRESS}'	'unit_netmask': '${UNIT_NETMASK}'	'logo_image_selection': False	'the_path_in_url_to_be_used_as_absolute_path_name': False	'enable_banner': True	'enable_license_utilization_rate': True	'banner': '${BANNER}\\n'
	CLI:Should Contain All    ${resp}    ${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session