*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get system licenses (status_code = 200)
	[Documentation]	Get system licenses. Status code 200 is expected. 
	...	Endpoint: /system/licenses

	${response}=	API::Send Get Request	/system/licenses
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get system licenses Parameters
	${PARAMETERS}	Create list	'id':	'serial_number':	'license_key':	'application':	'number_of_licenses':	'expiration_date':	'peer_address':	'type':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session