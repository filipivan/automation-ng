*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

#${LICENSES}=	Create List
${LICENSE_KEYS}=	"${FIFTY_DEVICES_ACCESS_LICENSE}"
#${SERIAL_NUMBERS}=	Create List

*** Test Cases ***

Remove license (status_code = 200)
	[Documentation]	Remove license. Status code 200 is expected. 
	...	Endpoint: /system/licenses

	${PAYLOAD}=	Set Variable	{"license_keys":[${LICENSE_KEYS}]}	#, "licenses":[${LICENSES}], "serial_numbers":${SERIAL_NUMBERS}}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/system/licenses
	Should Be Equal   ${response.status_code}   ${200}

#Remove license (status_code = 400)
#	[Documentation]	Remove license. Status code 400 is expected. 
#	...	Endpoint: /system/licenses

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	licenses=${LICENSES}
#	Set To Dictionary	${payload}	license_keys=${LICENSE_KEYS}
#	Set To Dictionary	${payload}	serial_numbers=${SERIAL_NUMBERS}
#
#	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/system/licenses
#	Should Be Equal   ${response.status_code}   ${400}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session