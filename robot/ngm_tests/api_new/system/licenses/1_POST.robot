*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${LICENSE_KEY}=	${FIFTY_DEVICES_ACCESS_LICENSE}

*** Test Cases ***

Add new license (status_code = 200)
	[Documentation]	Add new license. Status code 200 is expected. 
	...	Endpoint: /system/licenses

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	license_key=${LICENSE_KEY}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/licenses
	Should Be Equal   ${response.status_code}   ${200}

#Add new license (status_code = 400)
#	[Documentation]	Add new license. Status code 400 is expected. 
#	...	Endpoint: /system/licenses

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	license_key=${LICENSE_KEY}
#
#	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/licenses
#	Should Be Equal   ${response.status_code}   ${400}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session