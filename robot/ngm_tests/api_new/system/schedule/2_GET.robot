*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get list of scheduled tasks (status_code = 200)
	[Documentation]	Get list of scheduled tasks. Status code 200 is expected. 
	...	Endpoint: /system/schedule

	${response}=	API::Send Get Request	/system/schedule
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get list of scheduled tasks Parameters
	${PARAMETERS}	Create list	'id':	'user':	'task_name':	'task_description':	'command_to_execute':	'task_status':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session