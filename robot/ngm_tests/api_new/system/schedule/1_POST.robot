*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${TASK_NAME}=	test_task_schedule
${USER}=	daemon
${COMMAND_TO_EXECUTE}=	cd .

*** Test Cases ***

Add new scheduled task (status_code = 200)
	[Documentation]	Add new scheduled task. Status code 200 is expected. 
	...	Endpoint: /system/schedule

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	task_name=${TASK_NAME}
	Set To Dictionary	${payload}	user=${USER}
	Set To Dictionary	${payload}	command_to_execute=${COMMAND_TO_EXECUTE}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/schedule
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session