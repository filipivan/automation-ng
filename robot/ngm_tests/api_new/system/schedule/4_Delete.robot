*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${SCHEDULES}=	"test_task_schedule", "clone_test_task_schedule"

*** Test Cases ***

Remove scheduled task (status_code = 200)
	[Documentation]	Remove scheduled task. Status code 200 is expected. 
	...	Endpoint: /system/schedule

	${PAYLOAD}=	Set variable	{"schedules":[${SCHEDULES}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/system/schedule
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session