*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

*** Test Cases ***

Enable schedule tasks (status_code = 200)
	[Documentation]	Enable schedule tasks. Status code 200 is expected. 
	...	Endpoint: /system/schedule/enable

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	tasks=${TASKS}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/schedule/enable
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session
	${TASKS}	Create List	test_task_schedule
	Set Suite Variable	${TASKS}

SUITE:Teardown
	API::Delete::Session