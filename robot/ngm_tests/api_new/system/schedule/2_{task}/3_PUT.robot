*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${USER}=	daemon
${COMMAND_TO_EXECUTE}=	cd .

*** Test Cases ***

Update scheduled task (status_code = 200)
	[Documentation]	Update scheduled task. Status code 200 is expected. 
	...	Endpoint: /system/schedule/{task}

	${PAYLOAD}=	Set Variable	{"user":"${USER}", "command_to_execute":"${COMMAND_TO_EXECUTE}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/system/schedule/test_task_schedule
	Should Be Equal   ${response.status_code}   ${200}

Check update scheduled task (status_code = 200)
	[Documentation]	Verify if the last update make some change. Status code 200 is expected. 
	...	Endpoint: /system/schedule/{task}
	
	${response}=	API::Send Get Request	/system/schedule/test_task_schedule
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Should Contain	${resp}	{'task_description': '', 'user': '${USER}', 'command_to_execute': '${COMMAND_TO_EXECUTE}', 'minute': '*', 'hour': '*', 'day_of_month': '*', 'month': '*', 'day_of_week': '*', 'task_status': 'enabled', 'task_name': 'test_task_schedule'}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session