*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get scheduled task (status_code = 200)
	[Documentation]	Get scheduled task. Status code 200 is expected. 
	...	Endpoint: /system/schedule/{task}

	${response}=	API::Send Get Request	/system/schedule/test_task_schedule
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get scheduled task Parameters
	${PARAMETERS}	Create list	'task_description':	'user':	'command_to_execute':	'minute':	'hour':	'day_of_month':	'month':	'day_of_week':	'task_status':	'task_name':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session