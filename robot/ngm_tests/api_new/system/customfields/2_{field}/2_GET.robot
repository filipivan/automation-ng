*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get custom field (status_code = 200)
	[Documentation]	Get custom fields. Status code 200 is expected. 
	...	Endpoint: /system/customfields/{field}

	${response}=	API::Send Get Request	/system/customfields/test_field
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

#Validate Get custom field Parameters
#	${PARAMETERS}	Create list	'id':	'chain':	'policy':	'packets':	'bytes':	'type':
#	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session