*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${FIELD_VALUE}=	123456789abc

*** Test Cases ***

Update custom field (status_code = 200)
	[Documentation]	Update custom field. Status code 200 is expected. 
	...	Endpoint: /system/customfields/{field}

	${PAYLOAD}=	Set Variable	{"field_value":"${FIELD_VALUE}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/system/customfields/test_field
	Should Be Equal   ${response.status_code}   ${200}

Test Put /system/customfields/{field} Functionality
	[Documentation]	Get custom fields. Status code 200 is expected.
	...	Endpoint: /system/customfields

	${response}=	API::Send Get Request	/system/customfields
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should contain	${resp}	'field_value': '123456789abc'

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session