*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${FIELD_NAME}=	test_field
${FIELD_VALUE1}=	test_value

*** Test Cases ***

Add new custom field (status_code = 200)
	[Documentation]	Add new custom field. Status code 200 is expected. 
	...	Endpoint: /system/customfields

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	field_name=${FIELD_NAME}
	Set To Dictionary	${payload}	field_value1=${FIELD_VALUE1}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/customfields
	Should Be Equal   ${response.status_code}   ${200}

Test Post system/customfields Functionality
	[Documentation]	Get custom fields. Status code 200 is expected.
	...	Endpoint: /system/customfields

	${response}=	API::Send Get Request	/system/customfields
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should contain	${resp}	${FIELD_NAME}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session