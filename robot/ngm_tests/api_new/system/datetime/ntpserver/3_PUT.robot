*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${ALLOWED_NETWORKS}=	0.0.0.0
${ENABLE_NTP_SERVER}=	true

*** Test Cases ***

Update NTP server configuration (status_code = 200)
	[Documentation]	Update NTP server configuration. Status code 200 is expected. 
	...	Endpoint: /system/datetime/ntpserver

	${PAYLOAD}=	Set Variable	{"allowed_networks":"${ALLOWED_NETWORKS}", "enable_ntp_server":"${ENABLE_NTP_SERVER}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/system/datetime/ntpserver
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session