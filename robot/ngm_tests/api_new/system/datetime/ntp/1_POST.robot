*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${KEY_NUMBER}=	2
${HASH_ALGORITHM}=	SHA1
${PASSWORD}=	password
${CONFIRM_PASSWORD}=	password

*** Test Cases ***

Add new NTP (status_code = 200)
	[Documentation]	Add new NTP. Status code 200 is expected. 
	...	Endpoint: /system/datetime/ntp

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	key_number=${KEY_NUMBER}
	Set To Dictionary	${payload}	hash_algorithm=${HASH_ALGORITHM}
	Set To Dictionary	${payload}	password=${PASSWORD}
	Set To Dictionary	${payload}	confirm_password=${CONFIRM_PASSWORD}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/datetime/ntp
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session