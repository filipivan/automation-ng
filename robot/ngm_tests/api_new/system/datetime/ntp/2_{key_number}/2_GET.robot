*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get NTP info (status_code = 200)
	[Documentation]	Get NTPinfo. Status code 200 is expected. 
	...	Endpoint: /system/datetime/ntp/{key_number}

	${response}=	API::Send Get Request	/system/datetime/ntp/2
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get NTP info Parameters
	${PARAMETERS}	Create list	'id':	'label':	'hash_algorithm':	'password':	'key_number':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session