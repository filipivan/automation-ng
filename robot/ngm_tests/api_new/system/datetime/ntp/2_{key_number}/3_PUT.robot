*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${HASH_ALGORITHM}=	SHA256

*** Test Cases ***

Update NTP (status_code = 200)
	[Documentation]	Update NTP. Status code 200 is expected. 
	...	Endpoint: /system/datetime/ntp/{key_number}

	${PAYLOAD}=	Set Variable	{"hash_algorithm":"${HASH_ALGORITHM}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/system/datetime/ntp/2
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session