*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${KEY_NUMBER}=	2

*** Test Cases ***

Delete NTP (status_code = 200)
	[Documentation]	Delete NTP. Status code 200 is expected. 
	...	Endpoint: /system/datetime/ntp

	${PAYLOAD}=	Set Variable	{"key_number":"${KEY_NUMBER}"}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/system/datetime/ntp
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session