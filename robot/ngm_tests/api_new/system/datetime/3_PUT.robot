*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${DATE_AND_TIME}=	manual
${DAY}=	20
${MONTH}=	11
${YEAR}=	2018
${HOUR}=	20
${MINUTE}=	20
${SECOND}=	20
${SERVER}=	pool.ntp.org
${ZONE}=	UTC

*** Test Cases ***

Update date and time configuration (status_code = 200)
	[Documentation]	Update date and time configuration. Status code 200 is expected. 
	...	Endpoint: /system/datetime

	${PAYLOAD}=	Set Variable	{"date_and_time":"${DATE_AND_TIME}", "day":"${DAY}", "month":"${MONTH}", "year":"${YEAR}", "hour":"${HOUR}", "minute":"${MINUTE}", "second":"${SECOND}", "server":"${SERVER}", "zone":"${ZONE}"}


	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/system/datetime
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session