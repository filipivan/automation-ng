*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get date and time configuration (status_code = 200)
	[Documentation]	Get date and time configuration. Status code 200 is expected. 
	...	Endpoint: /system/datetime

	${response}=	API::Send Get Request	/system/datetime
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get date and time configuration Parameters
	${PARAMETERS}	Create list	'server':	'month':	'day':	'year':	'hour':	'minute':	'second':	'zone':	'date_and_time':	'system_time':	'last_update':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session