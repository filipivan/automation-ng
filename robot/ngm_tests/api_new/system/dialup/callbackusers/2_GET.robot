*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get dialup callback users (status_code = 200)
	[Documentation]	Get dialup callback users. Status code 200 is expected. 
	...	Endpoint: /system/dialup/callbackusers

	${response}=	API::Send Get Request	/system/dialup/callbackusers
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get dialup callback users Parameters
	${PARAMETERS}	Create list	'id':	'callback_user':	'callback_number':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session