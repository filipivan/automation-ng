*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${CALLBACK_USERS}=	"test_user"

*** Test Cases ***

Delete dialup callback user (status_code = 200)
	[Documentation]	Delete dialup callback user. Status code 200 is expected. 
	...	Endpoint: /system/dialup/callbackusers

	${PAYLOAD}=	Set variable	{"callback_users":[${CALLBACK_USERS}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/system/dialup/callbackusers
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session