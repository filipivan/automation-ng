*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${CALLBACK_NUMBER}=	9876543210

*** Test Cases ***

Update callback user (status_code = 200)
	[Documentation]	Update callback user. Status code 200 is expected. 
	...	Endpoint: /system/dialup/callbackusers/{user}

	${PAYLOAD}=	Set Variable	{"callback_number":"${CALLBACK_NUMBER}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/system/dialup/callbackusers/test_user
	Should Be Equal   ${response.status_code}   ${200}

Check update callback user (status_code = 200)
	[Documentation]	Verify if the last update make some change. Status code 200 is expected. 
	...	Endpoint:	/system/dialup/callbackusers/test_user

	${response}=	API::Send Get Request	/system/dialup/callbackusers/test_user
	${resp}=    Evaluate   str(${response.json()})

	${PAYLOAD}=	Set Variable	{'callback_number': '${CALLBACK_NUMBER}', 'callback_user': 'test_user'}
	Should Be Equal	${resp}	${PAYLOAD}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session