*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${CALLBACK_USER}=	test_user
${CALLBACK_NUMBER}=	123456789

*** Test Cases ***

Add new dialup callback user (status_code = 200)
	[Documentation]	Add new dialup callback user. Status code 200 is expected. 
	...	Endpoint: /system/dialup/callbackusers

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	callback_user=${CALLBACK_USER}
	Set To Dictionary	${payload}	callback_number=${CALLBACK_NUMBER}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/dialup/callbackusers
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session