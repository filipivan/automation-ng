*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${LOGIN_SESSION}=	enabled
${PPP_CONNECTION}=	enabled

*** Test Cases ***

Update dialup configurarion (status_code = 200)
	[Documentation]	Update dialup configurarion. Status code 200 is expected. 
	...	Endpoint: /system/dialup

	${PAYLOAD}=	Set Variable	{"login_session":"${LOGIN_SESSION}", "ppp_connection":"${PPP_CONNECTION}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/system/dialup
	Should Be Equal   ${response.status_code}   ${200}

Check update scheduled task (status_code = 200)
	[Documentation]	Verify if the last update make some change. Status code 200 is expected. 
	...	Endpoint: /system/dialup
	
	${response}=	API::Send Get Request	/system/dialup
	${resp}=    Evaluate   str(${response.json()})
	Should Contain	${resp}	{'login_session': '${LOGIN_SESSION}', 'ppp_connection': '${PPP_CONNECTION}'}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session