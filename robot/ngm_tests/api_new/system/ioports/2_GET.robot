*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Test Cases ***

Get I/O Ports configuration (status_code = 200)
	[Documentation]	Get I/O Ports configuration. Status code 200 is expected. 
	...	Endpoint: /system/ioports

	${response}=	API::Send Get Request	/system/ioports
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

#Validate Get NAT rules Parameters
#	${PARAMETERS}	Create list	'id':	'chain':	'policy':	'packets':	'bytes':	'type':
#	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session
	${IS_GSR}	API::Is GSR
	Skip If	not ${IS_GSR}	I/O Ports are only available on GSR device

SUITE:Teardown
	API::Delete::Session