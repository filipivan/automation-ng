*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	NEED-CREATION
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${OUT0_DESCRIPTION}=	Digital Output OUT0 Description

*** Test Cases ***

Update I/O Ports configuration (status_code = 200)
	[Documentation]	Update I/O Ports configuration. Status code 200 is expected. 
	...	Endpoint: /system/ioports

	${PAYLOAD}=	Set Variable	{"out0_description":"${OUT0_DESCRIPTION}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/system/ioports
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session
	${IS_GSR}	API::Is GSR
	Skip If	not ${IS_GSR}	I/O Ports are only available on GSR device

SUITE:Teardown
	API::Delete::Session