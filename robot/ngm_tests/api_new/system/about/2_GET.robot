*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get about information (status_code = 200)
	[Documentation]	Get about information. Status code 200 is expected. 
	...	Endpoint: /system/about

	${response}=	API::Send Get Request	/system/about
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get about information Parameters
	${PARAMETERS}	Create list	'system':	'version':	'boot_mode':	'secure_boot':	'revision_tag':	'licenses':	'cpu':	'cpu_core':	'bogomips':	'serial_number':	'model':	'uptime':	'system':	'version':	'boot_mode':	'secure_boot':	'revision_tag':	'licenses':	'cpu':	'cpu_core':	'bogomips':	'serial_number':	'model':	'uptime':
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session