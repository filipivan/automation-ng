*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${PHONE_NUMBER}=	+5547997836449

*** Test Cases ***

Update whitelist information (status_code = 200)
	[Documentation]	Update whitelist information. Status code 200 is expected. 
	...	Endpoint: /system/sms/whitelist/{name}

	${PAYLOAD}=	Set Variable	{"phone_number":"${PHONE_NUMBER}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/system/sms/whitelist/test_name
	Should Be Equal   ${response.status_code}   ${200}

Test Put /system/sms/whitelist/{name} Functionality
	${response}=	API::Send Get Request	/system/sms/whitelist/test_name
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}
	Should Contain	${resp}	${PHONE_NUMBER}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session