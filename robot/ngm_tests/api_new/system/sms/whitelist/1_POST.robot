*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${NAME}=	test_name
${PHONE_NUMBER}=	+15102983022

*** Test Cases ***

Add new entry in whitelist (status_code = 200)
	[Documentation]	Add new entry in whitelist. Status code 200 is expected. 
	...	Endpoint: /system/sms/whitelist

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	name=${NAME}
	Set To Dictionary	${payload}	phone_number=${PHONE_NUMBER}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/system/sms/whitelist
	Should Be Equal   ${response.status_code}   ${200}

Test Post /system/sms/whitelist Functionality
	${response}=	API::Send Get Request	/system/sms/whitelist
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	${LIST}	Create List	${NAME}	${PHONE_NUMBER}
	CLI:Should Contain All	${resp}	${LIST}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session