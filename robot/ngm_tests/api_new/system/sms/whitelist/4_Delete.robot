*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${WHITELISTS}=	"test_name"

*** Test Cases ***

Delete whitelist entry (status_code = 200)
	[Documentation]	Delete whitelist entry. Status code 200 is expected. 
	...	Endpoint: /system/sms/whitelist

	${PAYLOAD}=	Set variable	{"whitelists":[${WHITELISTS}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/system/sms/whitelist
	Should Be Equal   ${response.status_code}   ${200}

Test Delete /system/sms/whitelist Functionality
	${response}=	API::Send Get Request	/system/sms/whitelist
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	Should Not Contain	${resp}	${WHITELISTS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session