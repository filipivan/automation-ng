*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get sms settings (status_code = 200)
	[Documentation]	Get sms settings. Status code 200 is expected. 
	...	Endpoint: /system/sms

	${response}=	API::Send Get Request	/system/sms
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Set Suite Variable	${resp}
	Log    ${resp}

Validate Get sms settings Parameters
	${PARAMETERS}	Create list	'enable_actions_via_incoming_sms':	'allow_apn':	'allow_simswap':	'allow_connect_and_disconnect':	'allow_mstatus':	'allow_reset':	'allow_info':	'allow_factorydefault':	'allow_reboot':	'password':	
	CLI:Should Contain All	${resp}	${PARAMETERS}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session