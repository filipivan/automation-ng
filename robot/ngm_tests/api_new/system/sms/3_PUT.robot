*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${ENABLE_ACTIONS_VIA_INCOMING_SMS}=	true
${ALLOW_CONNECT_AND_DISCONNECT}=	false
${ALLOW_INFO}=	true

*** Test Cases ***

Update sms settings (status_code = 200)
	[Documentation]	Update sms settings. Status code 200 is expected. 
	...	Endpoint: /system/sms

	${PAYLOAD}=	Set Variable	{"enable_actions_via_incoming_sms": "${ENABLE_ACTIONS_VIA_INCOMING_SMS}", "allow_connect_and_disconnect": "${ALLOW_CONNECT_AND_DISCONNECT}", "allow_info": "${ALLOW_INFO}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/system/sms
	Should Be Equal   ${response.status_code}   ${200}
	
Check update sms settings (status_code = 200)
	[Documentation]	Verify if the last update make some change. Status code 200 is expected. 
	...	Endpoint: /system/sms
	
	${BODY_EXPECTED}=	Set Variable	{'enable_actions_via_incoming_sms': True, 'allow_apn': True, 'allow_simswap': True, 'allow_connect_and_disconnect': False, 'allow_mstatus': True, 'allow_reset': True, 'allow_info': True, 'allow_factorydefault': True, 'allow_reboot': True, 'password': '********'}
	API::Check Body Request    /system/sms    ${BODY_EXPECTED}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session