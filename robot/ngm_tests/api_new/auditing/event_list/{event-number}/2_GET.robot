*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get individual event settings (status_code = 200)
	[Documentation]	Get individual event settings. Status code 200 is expected. 
	...	Endpoint: /auditing/event_list/{event-number}

	${response}=	API::Send Get Request	/auditing/event_list/100
	${resp}=	Evaluate	str(${response.json()})
	Set Suite Variable	${resp}
	Should Be Equal   ${response.status_code}   ${200}

Test Get /auditing/event_list/{event-number} Functionality
	[Documentation]	Check that GET returns the correct fields
	@{PARAMETERS}	Create List	'action_script'	'enable'	'selected_events'	'description'	'category'
	CLI:Should Contain All	${resp}	${PARAMETERS}
#workaround to use a single keyword to validate all fields in the response

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session