*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${EVENT_ID}	100
${EVENT_DESCRIPTION}	Nodegrid System Rebooting
${ENABLE}=	yes
${DISABLE}=	no
${ACTION_SCRIPT}=	ActionScript_sample.sh

*** Test Cases ***

Update individual event settings to disabled (status_code = 200)
	[Documentation]	Update individual event settings. Status code 200 is expected. 
	...	Endpoint: /auditing/event_list/{event-number}

	${PAYLOAD}=	Set Variable	{"enable":"${DISABLE}", "action_script":"${ACTION_SCRIPT}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/auditing/event_list/${EVENT_ID}
	Should Be Equal   ${response.status_code}   ${200}

Test Put /auditing/event_list/${EVENT_ID} to disable Functionality
	[Documentation]	Check that PUT request to disable event ${EVENT_ID} using GET
	${response}=	API::Send Get Request	/auditing/event_list/${EVENT_ID}
	${BODY}=	Evaluate	str(${response.json()})
	Should Contain	${BODY}	{'action_script': '${ACTION_SCRIPT}', 'enable': False, 'selected_events': '${EVENT_ID}', 'description': '${EVENT_DESCRIPTION}', 'category': 'System Event'}

Update individual event settings to enabled (status_code = 200)
	[Documentation]	Update individual event settings. Status code 200 is expected.
	...	Endpoint: /auditing/event_list/{event-number}

	${PAYLOAD}=	Set Variable	{"enable":"${ENABLE}", "action_script":"${ACTION_SCRIPT}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/auditing/event_list/100
	Should Be Equal   ${response.status_code}   ${200}

Test Put /auditing/event_list/${EVENT_ID} to enable Functionality
	[Documentation]	Check that PUT request to enable event ${EVENT_ID} using GET
	${response}=	API::Send Get Request	/auditing/event_list/${EVENT_ID}
	${BODY}=	Evaluate	str(${response.json()})
	Should Contain	${BODY}	{'action_script': '${ACTION_SCRIPT}', 'enable': True, 'selected_events': '${EVENT_ID}', 'description': '${EVENT_DESCRIPTION}', 'category': 'System Event'}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	SUITE:Set Event Back to Default	${EVENT_ID}
	API::Delete::Session

SUITE:Set Event Back to Default
	[Documentation]	Update individual event settings back to default. Status code 200 is expected.
	...	Endpoint: /auditing/event_list/{event-number}
	[Arguments]	${ID}
	${PAYLOAD}=	Set Variable	{"enable":"${ENABLE}", "action_script":""}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/auditing/event_list/${ID}
	Should Be Equal   ${response.status_code}   ${200}