*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get event list settings (status_code = 200)
	[Documentation]	Get event list settings. Status code 200 is expected. 
	...	Endpoint: /auditing/event_list/

	${response}=	API::Send Get Request	/auditing/event_list/
	${resp}=	Evaluate	str(${response.json()})
	Set Suite Variable	${resp}
	Should Be Equal   ${response.status_code}   ${200}

Test Get /auditing/event_list Functionality
	[Documentation]	Check that GET returns the correct fields
	@{PARAMETERS}	Create List
	...  {'id': '100', 'status': True, 'category': 'System Event', 'event_number': '100', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid System Rebooting'}
	...  {'id': '101', 'status': True, 'category': 'System Event', 'event_number': '101', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid System Started'}
	...  {'id': '102', 'status': True, 'category': 'System Event', 'event_number': '102', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Software Upgrade Started'}
	...  {'id': '103', 'status': True, 'category': 'System Event', 'event_number': '103', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Software Upgrade Completed'}
	...  {'id': '104', 'status': True, 'category': 'System Event', 'event_number': '104', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Configuration Settings Saved to File'}
	...  {'id': '105', 'status': True, 'category': 'System Event', 'event_number': '105', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Configuration Settings Applied'}
	...  {'id': '106', 'status': True, 'category': 'System Event', 'event_number': '106', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid ZTP Started'}
	...  {'id': '107', 'status': True, 'category': 'System Event', 'event_number': '107', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid ZTP Completed'}
	...  {'id': '108', 'status': True, 'category': 'System Event', 'event_number': '108', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Configuration Changed'}
	...  {'id': '109', 'status': True, 'category': 'System Event', 'event_number': '109', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid SSD Life Left'}
	...  {'id': '110', 'status': True, 'category': 'System Event', 'event_number': '110', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Local User Added to System Datastore'}
	...  {'id': '111', 'status': True, 'category': 'System Event', 'event_number': '111', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Local User Deleted from System Datastore'}
	...  {'id': '112', 'status': True, 'category': 'System Event', 'event_number': '112', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Local User Modified in System Datastore'}
	...  {'id': '113', 'status': True, 'category': 'System Event', 'event_number': '113', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid ZTP Execution Success'}
	...  {'id': '114', 'status': True, 'category': 'System Event', 'event_number': '114', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid ZTP Execution Failure'}
	...  {'id': '115', 'status': True, 'category': 'System Event', 'event_number': '115', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Session Terminated'}
	...  {'id': '116', 'status': True, 'category': 'System Event', 'event_number': '116', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Session Timed Out'}
	...  {'id': '118', 'status': True, 'category': 'System Event', 'event_number': '118', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Power Supply State Changed'}
	...  {'id': '119', 'status': True, 'category': 'System Event', 'event_number': '119', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Power Supply Sound Alarm Stopped by User'}
	...  {'id': '120', 'status': True, 'category': 'System Event', 'event_number': '120', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Utilization Rate Exceeded'}
	...  {'id': '121', 'status': True, 'category': 'System Event', 'event_number': '121', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Thermal Temperature ThrottleUp'}
	...  {'id': '122', 'status': True, 'category': 'System Event', 'event_number': '122', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Thermal Temperature Dropping'}
	...  {'id': '123', 'status': True, 'category': 'System Event', 'event_number': '123', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Thermal Temperature Warning'}
	...  {'id': '124', 'status': True, 'category': 'System Event', 'event_number': '124', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Thermal Temperature Critical'}
	...  {'id': '126', 'status': True, 'category': 'System Event', 'event_number': '126', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Fan Status Changed'}
	...  {'id': '127', 'status': True, 'category': 'System Event', 'event_number': '127', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Fan Sound Alarm Stopped by User'}
	...  {'id': '128', 'status': True, 'category': 'System Event', 'event_number': '128', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Total number of local serial ports mismatch'}
	...  {'id': '129', 'status': True, 'category': 'System Event', 'event_number': '129', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Dry Contact changed state'}
	...  {'id': '130', 'status': True, 'category': 'System Event', 'event_number': '130', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid License Added'}
	...  {'id': '131', 'status': True, 'category': 'System Event', 'event_number': '131', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid License Removed'}
	...  {'id': '132', 'status': True, 'category': 'System Event', 'event_number': '132', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid License Conflict'}
	...  {'id': '133', 'status': True, 'category': 'System Event', 'event_number': '133', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid License Scarce'}
	...  {'id': '134', 'status': True, 'category': 'System Event', 'event_number': '134', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid License Expiring'}
	...  {'id': '135', 'status': True, 'category': 'System Event', 'event_number': '135', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Shell Started'}
	...  {'id': '136', 'status': True, 'category': 'System Event', 'event_number': '136', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Shell Stopped'}
	...  {'id': '137', 'status': True, 'category': 'System Event', 'event_number': '137', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Sudo Executed'}
	...  {'id': '138', 'status': True, 'category': 'System Event', 'event_number': '138', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid SMS Executed'}
	...  {'id': '139', 'status': True, 'category': 'System Event', 'event_number': '139', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid SMS Invalid'}
	...  {'id': '140', 'status': True, 'category': 'System Event', 'event_number': '140', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Connection Up'}
	...  {'id': '141', 'status': True, 'category': 'System Event', 'event_number': '141', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Connection Down'}
	...  {'id': '142', 'status': True, 'category': 'System Event', 'event_number': '142', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid SIM Card Swap'}
	...  {'id': '143', 'status': True, 'category': 'System Event', 'event_number': '143', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Network Connection State Changed'}
	...  {'id': '144', 'status': True, 'category': 'System Event', 'event_number': '144', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Network Failover Executed'}
	...  {'id': '145', 'status': True, 'category': 'System Event', 'event_number': '145', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Network Failback Executed'}
	...  {'id': '146', 'status': True, 'category': 'System Event', 'event_number': '146', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Network Connection Health Monitoring Success'}
	...  {'id': '147', 'status': True, 'category': 'System Event', 'event_number': '147', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Network Connection Health Monitoring Failure'}
	...  {'id': '150', 'status': True, 'category': 'System Event', 'event_number': '150', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Cluster Peer Online'}
	...  {'id': '151', 'status': True, 'category': 'System Event', 'event_number': '151', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Cluster Peer Offline'}
	...  {'id': '152', 'status': True, 'category': 'System Event', 'event_number': '152', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Cluster Peer Signed On'}
	...  {'id': '153', 'status': True, 'category': 'System Event', 'event_number': '153', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Cluster Peer Signed Off'}
	...  {'id': '154', 'status': True, 'category': 'System Event', 'event_number': '154', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Cluster Peer Removed'}
	...  {'id': '155', 'status': True, 'category': 'System Event', 'event_number': '155', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Cluster Peer Became Coordinator'}
	...  {'id': '156', 'status': True, 'category': 'System Event', 'event_number': '156', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Cluster Coordinator Became Peer'}
	...  {'id': '157', 'status': True, 'category': 'System Event', 'event_number': '157', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Cluster Coordinator Deleted'}
	...  {'id': '158', 'status': True, 'category': 'System Event', 'event_number': '158', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Cluster Coordinator Created'}
	...  {'id': '159', 'status': True, 'category': 'System Event', 'event_number': '159', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Cluster Peer Configured'}
	...  {'id': '160', 'status': True, 'category': 'System Event', 'event_number': '160', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Search Unavailable'}
	...  {'id': '161', 'status': True, 'category': 'System Event', 'event_number': '161', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Search Restored'}
	...  {'id': '162', 'status': True, 'category': 'System Event', 'event_number': '162', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid IPsec Tunnel Up'}
	...  {'id': '163', 'status': True, 'category': 'System Event', 'event_number': '163', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid IPsec Tunnel Down'}
	...  {'id': '164', 'status': True, 'category': 'System Event', 'event_number': '164', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid System is outside of GEO Fence'}
	...  {'id': '165', 'status': True, 'category': 'System Event', 'event_number': '165', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid System is inside GEO Fence'}
	...  {'id': '166', 'status': True, 'category': 'System Event', 'event_number': '166', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Wireguard Tunnel Up'}
	...  {'id': '167', 'status': True, 'category': 'System Event', 'event_number': '167', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Wireguard Tunnel Down'}
	...  {'id': '200', 'status': True, 'category': 'AAA Event', 'event_number': '200', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid User Logged In'}
	...  {'id': '201', 'status': True, 'category': 'AAA Event', 'event_number': '201', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid User Logged Out'}
	...  {'id': '202', 'status': True, 'category': 'AAA Event', 'event_number': '202', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid System Authentication Failure'}
	...  {'id': '203', 'status': True, 'category': 'AAA Event', 'event_number': '203', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid System Authentication Ban'}
	...  {'id': '300', 'status': True, 'category': 'Device Event', 'event_number': '300', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Device Session Started'}
	...  {'id': '301', 'status': True, 'category': 'Device Event', 'event_number': '301', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Device Session Stopped'}
	...  {'id': '302', 'status': True, 'category': 'Device Event', 'event_number': '302', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Device Created'}
	...  {'id': '303', 'status': True, 'category': 'Device Event', 'event_number': '303', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Device Deleted'}
	...  {'id': '304', 'status': True, 'category': 'Device Event', 'event_number': '304', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Device Renamed'}
	...  {'id': '305', 'status': True, 'category': 'Device Event', 'event_number': '305', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Device Cloned'}
	...  {'id': '306', 'status': True, 'category': 'Device Event', 'event_number': '306', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Device Up'}
	...  {'id': '307', 'status': True, 'category': 'Device Event', 'event_number': '307', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Device Down'}
	...  {'id': '308', 'status': True, 'category': 'Device Event', 'event_number': '308', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Device Session Terminated'}
	...  {'id': '310', 'status': True, 'category': 'Device Event', 'event_number': '310', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Power On Command Executed on a Device'}
	...  {'id': '311', 'status': True, 'category': 'Device Event', 'event_number': '311', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Power Off Command Executed on a Device'}
	...  {'id': '312', 'status': True, 'category': 'Device Event', 'event_number': '312', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Power Cycle Command Executed on a Device'}
	...  {'id': '313', 'status': True, 'category': 'Device Event', 'event_number': '313', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Suspend Command Executed on a Device'}
	...  {'id': '314', 'status': True, 'category': 'Device Event', 'event_number': '314', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Reset Command Executed on a Device'}
	...  {'id': '315', 'status': True, 'category': 'Device Event', 'event_number': '315', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Shutdown Command Executed on a Device'}
	...  {'id': '316', 'status': True, 'category': 'Device Event', 'event_number': '316', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid OCP System has sensor above threshold'}
	...  {'id': '330', 'status': True, 'category': 'Device Event', 'event_number': '330', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid USB Device Connected'}
	...  {'id': '331', 'status': True, 'category': 'Device Event', 'event_number': '331', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid USB Device Disconnected'}
	...  {'id': '400', 'status': True, 'category': 'Logging Event', 'event_number': '400', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid System Alert Detected'}
	...  {'id': '401', 'status': True, 'category': 'Logging Event', 'event_number': '401', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Alert String Detected on a Device Session'}
	...  {'id': '402', 'status': True, 'category': 'Logging Event', 'event_number': '402', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Event Log String Detected on a Device Event Log'}
	...  {'id': '410', 'status': True, 'category': 'Logging Event', 'event_number': '410', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid System NFS Failure'}
	...  {'id': '411', 'status': True, 'category': 'Logging Event', 'event_number': '411', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid System NFS Recovered'}
	...  {'id': '450', 'status': True, 'category': 'Logging Event', 'event_number': '450', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Datapoint State High Critical'}
	...  {'id': '451', 'status': True, 'category': 'Logging Event', 'event_number': '451', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Datapoint State High Warning'}
	...  {'id': '452', 'status': True, 'category': 'Logging Event', 'event_number': '452', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Datapoint State Normal'}
	...  {'id': '453', 'status': True, 'category': 'Logging Event', 'event_number': '453', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Datapoint State Low Warning'}
	...  {'id': '454', 'status': True, 'category': 'Logging Event', 'event_number': '454', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Datapoint State Low Critical'}
	...  {'id': '460', 'status': True, 'category': 'Logging Event', 'event_number': '460', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Door Unlocked'}
	...  {'id': '461', 'status': True, 'category': 'Logging Event', 'event_number': '461', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Door Locked'}
	...  {'id': '462', 'status': True, 'category': 'Logging Event', 'event_number': '462', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Door Open'}
	...  {'id': '463', 'status': True, 'category': 'Logging Event', 'event_number': '463', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Door Close'}
	...  {'id': '464', 'status': True, 'category': 'Logging Event', 'event_number': '464', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Door Access Denied'}
	...  {'id': '465', 'status': True, 'category': 'Logging Event', 'event_number': '465', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Door Alarm Active'}
	...  {'id': '466', 'status': True, 'category': 'Logging Event', 'event_number': '466', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Door Alarm Inactive'}
	...  {'id': '467', 'status': True, 'category': 'Logging Event', 'event_number': '467', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid PoE Power Fault'}
	...  {'id': '468', 'status': True, 'category': 'Logging Event', 'event_number': '468', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid PoE Power Budget Exceeded'}
	...  {'id': '469', 'status': True, 'category': 'Logging Event', 'event_number': '469', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Self encrypting drive enabled'}
	...  {'id': '470', 'status': True, 'category': 'Logging Event', 'event_number': '470', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Self encrypting drive disabled'}
	...  {'id': '471', 'status': True, 'category': 'Logging Event', 'event_number': '471', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid IEEE 802.1X Authentication Success'}
	...  {'id': '472', 'status': True, 'category': 'Logging Event', 'event_number': '472', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid IEEE 802.1X Authentication Failure'}
	 ...  {'id': '500', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '500', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Backup Requested'}
	 ...  {'id': '501', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '501', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Backup Request Failed'}
	 ...  {'id': '502', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '502', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Backup Started'}
	 ...  {'id': '503', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '503', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Backup Successful Finished'}
	 ...  {'id': '504', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '504', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Backup Failed'}
	 ...  {'id': '505', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '505', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Remote Access Started'}
	 ...  {'id': '506', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '506', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Remote Access Successful Finished'}
	 ...  {'id': '507', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '507', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Remote Access Failed'}
	 ...  {'id': '508', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '508', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Software Upgrade Started'}
	 ...  {'id': '509', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '509', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Software Upgrade Successful Finished'}
	 ...  {'id': '510', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '510', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Software Upgrade Failed'}
	 ...  {'id': '511', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '511', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Configuration Apply Started'}
	 ...  {'id': '512', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '512', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Configuration Apply Successful Finished'}
	 ...  {'id': '513', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '513', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Configuration Apply Failed'}
	 ...  {'id': '514', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '514', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Script Apply Started'}
	 ...  {'id': '515', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '515', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Script Apply Successful Finished'}
	 ...  {'id': '516', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '516', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Script Apply Failed'}
	 ...  {'id': '517', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '517', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Modem Firmware Upgrade Started'}
	 ...  {'id': '518', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '518', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Modem Firmware Upgrade Successful Finished'}
	 ...  {'id': '519', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '519', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Modem Firmware Upgrade Failed'}
	 ...  {'id': '520', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '520', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Extended Storage Started'}
	 ...  {'id': '521', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '521', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Extended Storage Successful Finished'}
	 ...  {'id': '522', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '522', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Extended Storage Failed'}
	 ...  {'id': '523', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '523', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Data Lake app has been activated'}
	 ...  {'id': '524', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '524', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Data Lake app has been deactivated'}
	 ...  {'id': '525', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '525', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid Data Lake app has failed'}
	 ...  {'id': '526', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '526', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid SD-WAN enabled'}
	 ...  {'id': '527', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '527', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid SD-WAN disabled'}
	 ...  {'id': '528', 'status': True, 'category': 'ZPE Cloud Event', 'event_number': '528', 'enabled': 'Yes', 'action_script': '', 'description': 'Nodegrid SD-WAN Traffic Switchover'
	CLI:Should Contain All	${resp}	${PARAMETERS}
#workaround to use a single keyword to validate all fields in the response

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session