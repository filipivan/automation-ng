*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get auditing settings (status_code = 200)
	[Documentation]	Get auditing settings. Status code 200 is expected. 
	...	Endpoint: /auditing/settings

	${response}=	API::Send Get Request	/auditing/settings
	${resp}=	Evaluate	str(${response.json()})
	Set Suite Variable	${resp}
	Should Be Equal   ${response.status_code}   ${200}

Test Get /auditing/settings Functionality
	[Documentation]	Check that GET returns the correct fields
	@{PARAMETERS}	Create List	'persistent_log_files'	'enable_file_destination'	'enable_syslog_destination'	'datalog_add_timestamp'	'enable_persistent_logs'	'event_timestamp_format'	'datalog_timestamp_format'
	CLI:Should Contain All	${resp}	${PARAMETERS}
#workaround to use a single keyword to validate all fields in the response

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session