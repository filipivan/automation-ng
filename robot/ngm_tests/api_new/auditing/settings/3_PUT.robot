*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${ENABLE_FILE_DESTINATION}=	true
${ENABLE_SYSLOG_DESTINATION}=	false
${DATALOG_ADD_TIMESTAMP}=	false
${EVENT_TIMESTAMP_FORMAT}=	utc
${DATALOG_TIMESTAMP_FORMAT}=	utc

*** Test Cases ***

Update auditing settings (status_code = 200)
	[Documentation]	Update auditing settings. Status code 200 is expected. 
	...	Endpoint: /auditing/settings

	${PAYLOAD}=	Set Variable	{"enable_file_destination":${ENABLE_FILE_DESTINATION}, "enable_syslog_destination":${ENABLE_SYSLOG_DESTINATION}, "datalog_add_timestamp":${DATALOG_ADD_TIMESTAMP}, "event_timestamp_format":"${EVENT_TIMESTAMP_FORMAT}", "datalog_timestamp_format":"${DATALOG_TIMESTAMP_FORMAT}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/auditing/settings
	Should Be Equal   ${response.status_code}   ${200}

Test Put /auditing/settings Functionality
	[Documentation]	Check that PUT returns the correct field and values using GET
	${response}=	API::Send Get Request	/auditing/settings
	${BODY}=	Evaluate	str(${response.json()})
	Should Contain	${BODY}	{'persistent_log_files': 'dlog.log,messages,wtmp,', 'enable_file_destination': ${ENABLE_FILE_DESTINATION}, 'enable_syslog_destination': ${ENABLE_SYSLOG_DESTINATION}, 'datalog_add_timestamp': ${DATALOG_ADD_TIMESTAMP}, 'enable_persistent_logs': False, 'event_timestamp_format': '${EVENT_TIMESTAMP_FORMAT}', 'datalog_timestamp_format': '${DATALOG_TIMESTAMP_FORMAT}'}	ignore_case=True

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session