*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get auditing SNMP trap destination configuration (status_code = 200)
	[Documentation]	Get auditing SNMP trap destination configuration. Status code 200 is expected. 
	...	Endpoint: /auditing/destination/snmptrap

	${response}=	API::Send Get Request	/auditing/destination/snmptrap
	${resp}=	Evaluate	str(${response.json()})
	Set Suite Variable	${resp}
	Should Be Equal   ${response.status_code}   ${200}

Test Get /auditing/destination/snmptrap Functionality
	[Documentation]	Check that GET returns the correct fields
	@{PARAMETERS}	Create List	'label'	'snmptrap_server'	'snmptrap_port'	'client_address'	'snmptrap_community'	'snmptrap_user'	'snmptrap_transport_protocol'	'snmptrap_security_level'	'snmptrap_authentication'	snmptrap_privacy_algo'	'snmptrap_version'	'snmptrap_authentication_password'	'snmptrap_privacy_passphrase'	'snmp_engine_id'
	CLI:Should Contain All	${resp}	${PARAMETERS}
#workaround to use a single keyword to validate all fields in the response


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session