*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${SNMPTRAP_SERVER}=	192.168.0.1
${SNMPTRAP_PORT}=	161
${SNMPTRAP_COMMUNITY}=	public
${SNMPTRAP_USER}=	secname
${SNMPTRAP_TRANSPORT_PROTOCOL}=	udp
${SNMPTRAP_SECURITY_LEVEL}=	noAuthNoPriv
${SNMPTRAP_AUTHENTICATION}=	SHA
${SNMPTRAP_PRIVACY_ALGO}=	AES
${SNMPTRAP_VERSION}=	2c
${SNMPTRAP_AUTHENTICATION_PASSWORD}=	zpesystems
${SNMPTRAP_PRIVACY_PASSPHRASE}=	zpesystems
${CLIENT_ADDRESS}=

*** Test Cases ***

Update auditing SNMP trap destination configuration (status_code = 200)
	[Documentation]	Update auditing SNMP trap destination configuration. Status code 200 is expected. 
	...	Endpoint: /auditing/destination/snmptrap

	${PAYLOAD}	Set Variable	{"snmptrap_server":"${SNMPTRAP_SERVER}", "snmptrap_port":"${SNMPTRAP_PORT}", "snmptrap_community":"${SNMPTRAP_COMMUNITY}", "snmptrap_user":"${SNMPTRAP_USER}", "snmptrap_transport_protocol":"${SNMPTRAP_TRANSPORT_PROTOCOL}", "snmptrap_security_level":"${SNMPTRAP_SECURITY_LEVEL}", "snmptrap_authentication":"${SNMPTRAP_AUTHENTICATION}", "snmptrap_privacy_algo":"${SNMPTRAP_PRIVACY_ALGO}", "snmptrap_version":"${SNMPTRAP_VERSION}", "snmptrap_authentication_password":"${SNMPTRAP_AUTHENTICATION_PASSWORD}", "snmptrap_privacy_passphrase":"${SNMPTRAP_PRIVACY_PASSPHRASE}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/auditing/destination/snmptrap
	Should Be Equal   ${response.status_code}   ${200}

Test Put /auditing/destination/snmptrap Functionality
	[Documentation]	Check that PUT returns the correct field and values using GET
	${response}=	API::Send Get Request	/auditing/destination/snmptrap
	${BODY}=	Evaluate	str(${response.json()})
	Should Contain	${BODY}	 'snmptrap_server': '${SNMPTRAP_SERVER}', 'snmptrap_port': '${SNMPTRAP_PORT}', 'client_address': '${CLIENT_ADDRESS}', 'snmptrap_community': '${SNMPTRAP_COMMUNITY}', 'snmptrap_user': '${SNMPTRAP_USER}', 'snmptrap_transport_protocol': '${SNMPTRAP_TRANSPORT_PROTOCOL}', 'snmptrap_security_level': '${SNMPTRAP_SECURITY_LEVEL}', 'snmptrap_authentication': '${SNMPTRAP_AUTHENTICATION}', 'snmptrap_privacy_algo': '${SNMPTRAP_PRIVACY_ALGO}', 'snmptrap_version': '${SNMPTRAP_VERSION}', 'snmptrap_authentication_password': '********', 'snmptrap_privacy_passphrase': '********'

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session