*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${DESTINATION}=	local
${FILE_SIZE}=	256
${NUMBER_OF_ARCHIVES}=	1
${ARCHIVE_BY_TIME}=	
${NFS_SERVER}=	
${NFS_PATH}=	
${NFS_FILE_SIZE}=	1024
${NUMBER_OF_ARCHIVES_IN_NFS}=	10
${NFS_ARCHIVE_BY_TIME}=	

*** Test Cases ***

Update auditing file destination configuration (status_code = 200)
	[Documentation]	Update auditing file destination configuration. Status code 200 is expected. 
	...	Endpoint: /auditing/destination/file

	${PAYLOAD}	Set Variable	{"destination":"${DESTINATION}", "file_size":"${FILE_SIZE}", "number_of_archives":"${NUMBER_OF_ARCHIVES}", "archive_by_time":"${ARCHIVE_BY_TIME}", "nfs_server":"${NFS_SERVER}", "nfs_path":"${NFS_PATH}", "nfs_file_size":"${NFS_FILE_SIZE}", "number_of_archives_in_nfs":"${NUMBER_OF_ARCHIVES_IN_NFS}", "nfs_archive_by_time":"${NFS_ARCHIVE_BY_TIME}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/auditing/destination/file
	Should Be Equal   ${response.status_code}   ${200}

Test Put /auditing/destination/email Functionality
	[Documentation]	Check that PUT returns the correct field and values using GET
	${response}=	API::Send Get Request	/auditing/destination/file
	${BODY}=	Evaluate	str(${response.json()})
	Should Contain	${BODY}	{'label': 'destination', 'file_size': '${FILE_SIZE}', 'number_of_archives': '${NUMBER_OF_ARCHIVES}', 'archive_by_time': '${ARCHIVE_BY_TIME}', 'nfs_server': '${NFS_SERVER}', 'nfs_path': '${NFS_PATH}', 'nfs_file_size': '${NFS_FILE_SIZE}', 'number_of_archives_in_nfs': '${NUMBER_OF_ARCHIVES_IN_NFS}', 'nfs_archive_by_time': '${NFS_ARCHIVE_BY_TIME}', 'destination': '${DESTINATION}'

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session