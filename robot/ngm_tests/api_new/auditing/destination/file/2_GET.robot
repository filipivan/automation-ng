*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get auditing file destination configuration (status_code = 200)
	[Documentation]	Get auditing file destination configuration. Status code 200 is expected. 
	...	Endpoint: /auditing/destination/file

	${response}=	API::Send Get Request	/auditing/destination/file
	${resp}=	Evaluate	str(${response.json()})
	Set Suite Variable	${resp}
	Should Be Equal   ${response.status_code}   ${200}

Test Get /auditing/destination/file Functionality
	[Documentation]	Check that GET returns the correct fields
	@{PARAMETERS}	Create List	'label'	'file_size'	'number_of_archives'	'archive_by_time'	'nfs_server'	'nfs_path'	'nfs_file_size'	'number_of_archives_in_nfs'	'nfs_archive_by_time'	'destination'
	CLI:Should Contain All	${resp}	${PARAMETERS}
#workaround to use a single keyword to validate all fields in the response

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session