*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get auditing syslog destination configuration (status_code = 200)
	[Documentation]	Get auditing syslog destination configuration. Status code 200 is expected. 
	...	Endpoint: /auditing/destination/syslog

	${response}=	API::Send Get Request	/auditing/destination/syslog
	${resp}=	Evaluate	str(${response.json()})
	Set Suite Variable	${resp}
	Should Be Equal   ${response.status_code}   ${200}

Test Get /auditing/destination/syslog Functionality
	[Documentation]	Check that GET returns the correct fields
	@{PARAMETERS}	Create List	'label'	'ipv4_address'	'ipv6_address'	'event_facility'	'datalog_facility'	'system_console'	'admin_session'	'ipv4_remote_server'	'ipv6_remote_server'
	CLI:Should Contain All	${resp}	${PARAMETERS}
#workaround to use a single keyword to validate all fields in the response

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session