*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${SYSTEM_CONSOLE}=	true
${ADMIN_SESSION}=	false
${IPV4_REMOTE_SERVER}=	false
${IPV4_ADDRESS}=
${IPV6_REMOTE_SERVER}=	false
${IPV6_ADDRESS}=
${EVENT_FACILITY_NAME}=	LOG_LOCAL0
${DATALOG_FACILITY_NAME}=	LOG_LOCAL0

*** Test Cases ***

Update auditing syslog destination configuration (status_code = 200)
	[Documentation]	Update auditing syslog destination configuration. Status code 200 is expected. 
	...	Endpoint: /auditing/destination/syslog

	${PAYLOAD}=	Set Variable	{"system_console":"${SYSTEM_CONSOLE}", "admin_session":"${ADMIN_SESSION}", "ipv4_remote_server":"${IPV4_REMOTE_SERVER}", "ipv6_remote_server":"${IPV6_REMOTE_SERVER}", "event_facility":"${EVENT_FACILITY_NAME}", "datalog_facility":"${DATALOG_FACILITY_NAME}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/auditing/destination/syslog
	Should Be Equal   ${response.status_code}   ${200}

Test Put /auditing/destination/syslog Functionality
	[Documentation]	Check that PUT returns the correct field and values using GET
	${response}=	API::Send Get Request	/auditing/destination/syslog
	${BODY}=	Evaluate	str(${response.json()})
	Should Contain	${BODY}	'ipv4_address': '${IPV4_ADDRESS}', 'ipv6_address': '${IPV6_ADDRESS}', 'event_facility': '${EVENT_FACILITY_NAME}', 'datalog_facility': '${DATALOG_FACILITY_NAME}', 'system_console': ${SYSTEM_CONSOLE}, 'admin_session': ${ADMIN_SESSION}, 'ipv4_remote_server': ${IPV4_REMOTE_SERVER}, 'ipv6_remote_server': ${IPV6_REMOTE_SERVER}	ignore_case=True

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session