*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get auditing email destination configuration (status_code = 200)
	[Documentation]	Get auditing email destination configuration. Status code 200 is expected. 
	...	Endpoint: /auditing/destination/email

	${response}=	API::Send Get Request	/auditing/destination/email
	${resp}=	Evaluate	str(${response.json()})
	Set Suite Variable	${resp}
	Should Be Equal   ${response.status_code}   ${200}

Test Get /auditing/destination/email Functionality
	[Documentation]	Check that GET returns the correct fields
	@{PARAMETERS}	Create List	'label'	'email_server'	'email_port'	'username'	'destination_email'	'sender'	'start_tls'	'password'	'confirm_password'
	CLI:Should Contain All	${resp}	${PARAMETERS}
#workaround to use a single keyword to validate all fields in the response

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session