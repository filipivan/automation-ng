*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${SERVER_EMAIL}=	${EMAIL_SERVER2}
${EMAIL_PORT}=	${EMAIL_PORT2}
${USERNAME_EMAIL}=	${EMAIL_ACCOUNT2}
${PASSWORD}=	${EMAIL_PASSWD2}
${CONFIRM_PASSWORD}=	${EMAIL_PASSWD2}
${DESTINATION_EMAIL}=	${EMAIL_DESTINATION2}
${SENDER}=	API_Jenkins_new_sender
${START_TLS}=	true

*** Test Cases ***

Update auditing email destination configuration (status_code = 200)
	[Documentation]	Update auditing email destination configuration. Status code 200 is expected. 
	...	Endpoint: /auditing/destination/email

	${PAYLOAD}=	Set Variable	{"email_server":"${SERVER_EMAIL}", "email_port":"${EMAIL_PORT}", "username":"${USERNAME_EMAIL}", "password":"${PASSWORD}", "confirm_password":"${CONFIRM_PASSWORD}", "destination_email":"${DESTINATION_EMAIL}", "sender":"${SENDER}", "start_tls":"${START_TLS}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/auditing/destination/email
	Should Be Equal   ${response.status_code}   ${200}

Test Put /auditing/destination/email Functionality
	[Documentation]	Check that PUT returns the correct field and values using GET
	${response}=	API::Send Get Request	/auditing/destination/email
	${BODY}=	Evaluate	str(${response.json()})
	Should Contain	${BODY}	{'label': 'server settings', 'email_server': '${SERVER_EMAIL}', 'email_port': '${EMAIL_PORT}', 'username': '${USERNAME_EMAIL}', 'destination_email': '${DESTINATION_EMAIL}', 'sender': '${SENDER}', 'start_tls': True, 'password': '********', 'confirm_password': '********'

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session