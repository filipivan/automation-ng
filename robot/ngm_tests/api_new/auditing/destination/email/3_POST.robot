*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${SERVER_EMAIL}=	${EMAIL_SERVER2}
${EMAIL_PORT}=	${EMAIL_PORT2}
${USERNAME_EMAIL}=	${EMAIL_ACCOUNT2}
${PASSWORD}=	${EMAIL_PASSWD2}
${CONFIRM_PASSWORD}=	${EMAIL_PASSWD2}
${DESTINATION_EMAIL}=	${EMAIL_DESTINATION2}
${SENDER}=	API_Jenkins_new_sender
${START_TLS}=	true

*** Test Cases ***

Test email (status_code = 200)
	[Documentation]	Test email. Status code 200 is expected. 
	...	Endpoint: /auditing/destination/email/testemail

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	email_server=${SERVER_EMAIL}
	Set To Dictionary	${payload}	email_port=${EMAIL_PORT}
	Set To Dictionary	${payload}	username=${USERNAME_EMAIL}
	Set To Dictionary	${payload}	password=${PASSWORD}
	Set To Dictionary	${payload}	confirm_password=${CONFIRM_PASSWORD}
	Set To Dictionary	${payload}	destination_email=${DESTINATION_EMAIL}
	Set To Dictionary	${payload}	sender=${SENDER}
	Set To Dictionary	${payload}	start_tls=${START_TLS}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/auditing/destination/email/testemail
	Should Be Equal   ${response.status_code}   ${200}

Test Email Functionality using API
	[Documentation]	Login into email server and checks test email was received
	CLI:Open	USERNAME=${USERNAME_EMAIL}	PASSWORD=${PASSWORD}	session_alias=mail_server
	...	TYPE=shell	HOST_DIFF=${SERVER_EMAIL}
	SUITE:Check If Test Email Was Received
	CLI:Close Current Connection

Test Post /auditing/destination/email Functionality
	[Documentation]	Check that POST returns the correct field and values using GET
	${response}=	API::Send Get Request	/auditing/destination/email
	${BODY}=	Evaluate	str(${response.json()})
	LOG	${BODY}
	Should Contain	${BODY}	{'label': 'server settings', 'email_server': '${SERVER_EMAIL}', 'email_port': '${EMAIL_PORT}', 'username': '${USERNAME_EMAIL}', 'destination_email': '${DESTINATION_EMAIL}', 'sender': '${SENDER}', 'start_tls': True, 'password': '********', 'confirm_password': '********'}

Test email with wrong port(status_code = 400)
	[Documentation]	Test email. Status code 400 is expected.
	...	Endpoint: /auditing/destination/email/testemail

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	email_server=${SERVER_EMAIL}
	Set To Dictionary	${payload}	email_port=${EMAIL_PORT}_WRONG!
	Set To Dictionary	${payload}	username=${USERNAME_EMAIL}
	Set To Dictionary	${payload}	password=${PASSWORD}
	Set To Dictionary	${payload}	confirm_password=${CONFIRM_PASSWORD}
	Set To Dictionary	${payload}	destination_email=${DESTINATION_EMAIL}
	Set To Dictionary	${payload}	sender=${SENDER}
	Set To Dictionary	${payload}	start_tls=${START_TLS}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/auditing/destination/email/testemail
	...	ERROR_CONTROL=${FALSE}	EXPECT_ERROR=HTTPError: 400 Client Error: Bad Request for url: https://${HOST}/api/v1/auditing/destination/email/testemail
	Should Be Equal   ${response}   HTTPError: 400 Client Error: Bad Request for url: https://${HOST}/api/v1/auditing/destination/email/testemail

Test email with wrong confim_password(status_code = 400)
	[Documentation]	Test email. Status code 400 is expected.
	...	Endpoint: /auditing/destination/email/testemail

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	email_server=${SERVER_EMAIL}
	Set To Dictionary	${payload}	email_port=${EMAIL_PORT}
	Set To Dictionary	${payload}	username=${USERNAME_EMAIL}
	Set To Dictionary	${payload}	password=${PASSWORD}
	Set To Dictionary	${payload}	confirm_password=${CONFIRM_PASSWORD}_WRONG!
	Set To Dictionary	${payload}	destination_email=${DESTINATION_EMAIL}
	Set To Dictionary	${payload}	sender=${SENDER}
	Set To Dictionary	${payload}	start_tls=${START_TLS}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/auditing/destination/email/testemail
	...	ERROR_CONTROL=${FALSE}	EXPECT_ERROR=HTTPError: 400 Client Error: Bad Request for url: https://${HOST}/api/v1/auditing/destination/email/testemail
	Should Be Equal   ${response}   HTTPError: 400 Client Error: Bad Request for url: https://${HOST}/api/v1/auditing/destination/email/testemail

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session

SUITE:Check If Test Email Was Received
	Write	tail /var/mail/${EMAIL_ACCOUNT2}
	Set Client Configuration	prompt=:~$
	${OUTPUT}=	CLI:Read Until Prompt
	Should Not Contain	${OUTPUT}	No mail for ${EMAIL_ACCOUNT2}
	Run Keyword If	'${NGVERSION}' < '4.2'	Should Contain  ${OUTPUT}	Email test from NodeGrid
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain  ${OUTPUT}	Email test from Nodegrid
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Contain   ${OUTPUT}	From: "${SENDER}"