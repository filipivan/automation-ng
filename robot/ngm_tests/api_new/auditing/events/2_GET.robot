*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get auditing file events configuration (status_code = 200)
	[Documentation]	Get auditing file events configuration. Status code 200 is expected. 
	...	Endpoint: /auditing/events

	${response}=	API::Send Get Request	/auditing/events
	${resp}=	Evaluate	str(${response.json()})
	Set Suite Variable	${resp}
	Should Be Equal   ${response.status_code}   ${200}

Test Get /auditing/events Functionality
	[Documentation]	Check that GET returns the correct fields
	@{PARAMETERS}	Create List
	...	{'id': 'cloud', 'events': 'zpe cloud', 'system_events': '-', 'aaa_events': '-', 'device_events': '-', 'logging_events': '-', 'zpe_cloud_events': 'yes'}
	...	{'id': 'email', 'events': 'email', 'system_events': '-', 'aaa_events': '-', 'device_events': '-', 'logging_events': '-', 'zpe_cloud_events': '-'}
	...	{'id': 'file', 'events': 'file', 'system_events': 'yes', 'aaa_events': 'yes', 'device_events': 'yes', 'logging_events': 'yes', 'zpe_cloud_events': 'yes'}
	...	{'id': 'snmptrap', 'events': 'snmp trap', 'system_events': '-', 'aaa_events': '-', 'device_events': '-', 'logging_events': '-', 'zpe_cloud_events': '-'}
	...	{'id': 'syslog', 'events': 'syslog', 'system_events': 'yes', 'aaa_events': 'yes', 'device_events': 'yes', 'logging_events': 'yes', 'zpe_cloud_events': 'yes'}
	CLI:Should Contain All	${resp}	${PARAMETERS}
#workaround to use a single keyword to validate all fields in the response

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session