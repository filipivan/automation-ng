*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${SYSTEM_EVENTS}=	yes
${AAA_EVENTS}=	yes
${DEVICE_EVENTS}=	yes
${LOGGING_EVENTS}=	yes

*** Test Cases ***

Update auditing file event configuration (status_code = 200)
	[Documentation]	Update auditing file event configuration. Status code 200 is expected. 
	...	Endpoint: /auditing/events/{event}

	${PAYLOAD}=	Set Variable	{"system_events":"${SYSTEM_EVENTS}", "aaa_events":"${AAA_EVENTS}", "device_events":"${DEVICE_EVENTS}", "logging_events":"${LOGGING_EVENTS}"}
	@{EVENT}=	SUITE:Get Events
	FOR	${EVENT}	IN	@{EVENT}
		${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/auditing/events/${EVENT}
		${resp}=	Evaluate	str(${response.json()})
		Set Suite Variable	${resp}
		Should Be Equal   ${response.status_code}   ${200}
	END

Test Put /auditing/events/cloud Functionality
	[Documentation]	Check that PUT request displays correct parameters using GET
	${response}=	API::Send Get Request	/auditing/events
	${BODY}=	Evaluate	str(${response.json()})
	Should Contain	${BODY}	{'id': 'cloud', 'events': 'zpe cloud', 'system_events': '${SYSTEM_EVENTS}', 'aaa_events': '${AAA_EVENTS}', 'device_events': '${DEVICE_EVENTS}', 'logging_events': '${LOGGING_EVENTS}', 'zpe_cloud_events': 'yes'}

Test Put /auditing/events/email Functionality
	[Documentation]	Check that PUT request displays correct parameters using GET
	${response}=	API::Send Get Request	/auditing/events
	${BODY}=	Evaluate	str(${response.json()})
	Should Contain	${BODY}	{'id': 'email', 'events': 'email', 'system_events': '${SYSTEM_EVENTS}', 'aaa_events': '${AAA_EVENTS}', 'device_events': '${DEVICE_EVENTS}', 'logging_events': '${LOGGING_EVENTS}', 'zpe_cloud_events': '-'}

Test Put /auditing/events/file Functionality
	[Documentation]	Check that PUT request displays correct parameters using GET
	${response}=	API::Send Get Request	/auditing/events
	${BODY}=	Evaluate	str(${response.json()})
	Should Contain	${BODY}	{'id': 'file', 'events': 'file', 'system_events': '${SYSTEM_EVENTS}', 'aaa_events': '${AAA_EVENTS}', 'device_events': '${DEVICE_EVENTS}', 'logging_events': '${LOGGING_EVENTS}', 'zpe_cloud_events': 'yes'}

Test Put /auditing/events/snmptrap Functionality
	[Documentation]	Check that PUT request displays correct parameters using GET
	${response}=	API::Send Get Request	/auditing/events
	${BODY}=	Evaluate	str(${response.json()})
	Should Contain	${BODY}	{'id': 'snmptrap', 'events': 'snmp trap', 'system_events': 'yes', 'aaa_events': 'yes', 'device_events': 'yes', 'logging_events': 'yes', 'zpe_cloud_events': '-'}

Test Put /auditing/events/syslog Functionality
	[Documentation]	Check that PUT request displays correct parameters using GET
	${response}=	API::Send Get Request	/auditing/events
	${BODY}=	Evaluate	str(${response.json()})
	Should Contain	${BODY}	{'id': 'syslog', 'events': 'syslog', 'system_events': 'yes', 'aaa_events': 'yes', 'device_events': 'yes', 'logging_events': 'yes', 'zpe_cloud_events': 'yes'}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session

SUITE:Get Events
	${response}=	API::Send Get Request	/auditing/events
	Should Be Equal   ${response.status_code}   ${200}
	${EVENT}=	evaluate	str(${response.json()})
	LOG	${EVENT}
	${EVENT}=	Remove String	${EVENT}	[
	${EVENT}=	Remove String	${EVENT}	]
	${EVENT}	split string	${EVENT}	},
	@{EVENTS}	Create List
	FOR	${E}	IN	@{EVENT}
		${COLUMNS}=	Split String	${E}	',
		FOR	${COLUMN}	IN	@{COLUMNS}
			${CONTAINS}	Run Keyword And Return Status	Should Contain	${COLUMN}	'id':
			IF	${CONTAINS}
				${ID}=	Replace String Using Regexp	${COLUMN}	\\s*\{\'id|\'\:\\s|\'	${EMPTY}
				Append to List	${EVENTS}	${ID}
			END
			Exit For Loop If	${CONTAINS}
		END
	END
	[Return]	@{EVENTS}