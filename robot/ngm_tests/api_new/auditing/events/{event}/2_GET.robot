*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get auditing file event configuration (status_code = 200)
	[Documentation]	Get auditing file event configuration. Status code 200 is expected. 
	...	Endpoint: /auditing/events/{event}

	${response}=	API::Send Get Request	/auditing/events/email
	${resp}=	Evaluate	str(${response.json()})
	Set Suite Variable	${resp}
	Should Be Equal   ${response.status_code}   ${200}

Test Get /auditing/events/{event} Functionality
	[Documentation]	Check that GET returns the correct fields
	@{PARAMETERS}	Create List	'system_events'	'aaa_events'	'device_events'	'logging_events'	'zpe_cloud_events'
	CLI:Should Contain All	${resp}	${PARAMETERS}
#workaround to use a single keyword to validate all fields in the response

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session