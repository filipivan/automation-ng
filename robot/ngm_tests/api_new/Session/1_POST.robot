*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Teardown	SUITE:Teardown

*** Variables ***

${USERNAME}=	admin
${PASSWORD}=	admin

*** Test Cases ***

Authenticate user and password (status_code = 200)
	[Documentation]	Authenticate user and password and return a valid ticket. Status code 200 is expected. 
	...	Endpoint: /Session

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	username=${USERNAME}
	Set To Dictionary	${payload}	password=${PASSWORD}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/Session	RAW_MODE=no	MODE=yes
	Should Be Equal   ${response.status_code}   ${200}
	API::Set Suite Ticket	${response.json()}

Authenticate user and password (status_code = 401)
	[Documentation]	Authenticate user and password and return a valid ticket. Status code 401 is expected.
	...	Endpoint: /Session

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	username=${USERNAME}
	Set To Dictionary	${payload}	password=wrongpasswd

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/Session	ERROR_CONTROL=${FALSE}	EXPECT_ERROR=HTTPError: 401 Client Error: Unauthorized for url: https://${HOST}/api/v1/Session	RAW_MODE=no	MODE=yes
	Should Be Equal   ${response}   HTTPError: 401 Client Error: Unauthorized for url: https://${HOST}/api/v1/Session


*** Keywords ***
SUITE:Teardown
	API::Delete::Session