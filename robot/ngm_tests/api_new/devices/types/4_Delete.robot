*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${DEVICES_TYPES}=	device_console

*** Test Cases ***

Delete devices types (status_code = 200)
	[Documentation]	Delete devices types. Status code 200 is expected. 
	...	Endpoint: /devices/types

	${PAYLOAD}=	Set Variable	{"devices_types":["${DEVICES_TYPES}"]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/devices/types
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session