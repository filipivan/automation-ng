*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${DEVICE_TYPE_NAME}=	clone_device123

*** Test Cases ***

Clone device type (status_code = 200)
	[Documentation]	Clone device type. Status code 200 is expected. 
	...	Endpoint: /devices/types/{type}/clone

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	device_type_name=${DEVICE_TYPE_NAME}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/devices/types/device_console/clone
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session