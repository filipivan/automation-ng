*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${PROTOCOL}=	ssh

*** Test Cases ***

Update devices types (status_code = 200)
	[Documentation]	Update devices types. Status code 200 is expected. 
	...	Endpoint: /devices/types/{type}

	${PAYLOAD}=	Set Variable	{"protocol":"${PROTOCOL}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/devices/types/device_console
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session