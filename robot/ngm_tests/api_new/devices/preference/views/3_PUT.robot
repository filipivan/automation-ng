*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${PREDEFINED_COLUMNS}=	"Mode","Type"
${CUSTOM_COLUMNS}=	Test

*** Test Cases ***

Update device views preference (status_code = 200)
	[Documentation]	Update device views preference. Status code 200 is expected. 
	...	Endpoint: /devices/preference/views

	${PAYLOAD}=	Set Variable	{"predefined_columns":[${PREDEFINED_COLUMNS}], "custom_columns":"${CUSTOM_COLUMNS}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/devices/preference/views
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session