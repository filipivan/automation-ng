*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${DISCONNECT_HOTKEY}=	^E

*** Test Cases ***

Update device session preference (status_code = 200)
	[Documentation]	Update device session preference. Status code 200 is expected. 
	...	Endpoint: /devices/preference/session

	${PAYLOAD}=	Set Variable	{"disconnect_hotkey":"${DISCONNECT_HOTKEY}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/devices/preference/session
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session