*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${EXIT_OPTION}=	1
${STATUS_OPTION}=	2

*** Test Cases ***

Update device power preference (status_code = 200)
	[Documentation]	Update device power preference. Status code 200 is expected. 
	...	Endpoint: /devices/preference/power

	${PAYLOAD}=	Set Variable	{"exit_option":"${EXIT_OPTION}", "status_option":"${STATUS_OPTION}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/devices/preference/power
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session