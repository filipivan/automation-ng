*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${DEVICES}=	"testdevice", "new_device_3"

*** Test Cases ***

Delete devices (status_code = 200)
	[Documentation]	Delete device. Status code 200 is expected. 
	...	Endpoint: /devices/table

	${PAYLOAD}=	Set Variable	{"devices":[${DEVICES}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session