*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${TYPE}=	device_console
${NAME}=	testdevice
${IP_ADDRESS}=	192.168.16.123
${MODE}=	ondemand

*** Test Cases ***

Add new device (status_code = 200)
	[Documentation]	Add new device. Status code 200 is expected. 
	...	Endpoint: /devices/table

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	type=${TYPE}
	Set To Dictionary	${payload}	name=${NAME}
	Set To Dictionary	${payload}	ip_address=${IP_ADDRESS}
	Set To Dictionary	${payload}	mode=${MODE}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session