*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${SSHKEYTYPE}=	rsa-1024

*** Test Cases ***

Execute device's SSH Keys command to generate key-pairs (status_code = 200)
	[Documentation]	Execute device's SSH Keys command to generate key-pairs. Status code 200 is expected. 
	...	Endpoint: /devices/table/{device}/sshkeys/generate

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	sshkeytype=${SSHKEYTYPE}

	SUITE:ENABLE SSH Keys

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table/testdevice/sshkeys/generate
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session

SUITE:ENABLE SSH Keys
	${PAYLOAD}=	Set Variable	{"allow_pre-shared_ssh_key":"yes"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table/testdevice
	Should Be Equal   ${response.status_code}   ${200}