*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get device's SSH Keys form (status_code = 200)
	[Documentation]	Get device's SSH Keys form. Status code 200 is expected. 
	...	Endpoint: /devices/table/{device}/sshkeys

	SUITE:ENABLE SSH Keys

	${response}=	API::Send Get Request	/devices/table/testdevice/sshkeys
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session

SUITE:ENABLE SSH Keys
	${PAYLOAD}=	Set Variable	{"allow_pre-shared_ssh_key":"yes"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table/testdevice
	Should Be Equal   ${response.status_code}   ${200}