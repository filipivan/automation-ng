*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${IP_ADDRESS}=	${SSHKEYS_SERVER}
${SERVER_USERNAME}=	${SSHKEYS_SERVER_USER}
${PASSWORD_USE}=	use_specific
${PASSWORD}=	${SSHKEYS_SERVER_PASSWORD}
${CONFIRM_PASSWORD}=	${SSHKEYS_SERVER_PASSWORD}

*** Test Cases ***

Execute device's SSH Keys command to send key (status_code = 200)
	[Documentation]	Execute device's SSH Keys command to send key. Status code 200 is expected. 
	...	Endpoint: /devices/table/{device}/sshkeys/send

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	ip_address=${IP_ADDRESS}
	Set To Dictionary	${payload}	username=${SERVER_USERNAME}
	Set To Dictionary	${payload}	password_use=${PASSWORD_USE}
	Set To Dictionary	${payload}	password=${PASSWORD}
	Set To Dictionary	${payload}	confirm_password=${CONFIRM_PASSWORD}

	SUITE:ENABLE SSH Keys

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table/testdevice/sshkeys/send
	Should Be Equal   ${response.status_code}   ${200}

#Execute device's SSH Keys command to send key (status_code = 400)
#	[Documentation]	Execute device's SSH Keys command to send key. Status code 400 is expected. 
#	...	Endpoint: /devices/table/2_{vlan}{device}/sshkeys/send

#	${PAYLOAD}=	Create Dictionary
#	Set To Dictionary	${payload}	ip_address=${IP_ADDRESS}
#	Set To Dictionary	${payload}	port=${PORT}
#	Set To Dictionary	${payload}	username=${USERNAME}
#	Set To Dictionary	${payload}	password_use=${PASSWORD_USE}
#	Set To Dictionary	${payload}	password=${PASSWORD}
#	Set To Dictionary	${payload}	confirm_password=${CONFIRM_PASSWORD}
#
#	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table/2_{vlan}{device}/sshkeys/send
#	Should Be Equal   ${response.status_code}   ${400}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session

SUITE:ENABLE SSH Keys
	${PAYLOAD}=	Set Variable	{"allow_pre-shared_ssh_key":"yes"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table/testdevice
	Should Be Equal   ${response.status_code}   ${200}