*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***


*** Test Cases ***

Update device's mode to disabled (status_code = 200)
	[Documentation]	Update device's mode to disabled. Status code 200 is expected. 
	...	Endpoint: /devices/table/{device}/disable

	${PAYLOAD}=	Create Dictionary

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table/testdevice/disable
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session