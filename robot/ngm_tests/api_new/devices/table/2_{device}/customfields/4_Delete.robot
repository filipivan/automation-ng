*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${CUSTOM_FIELDS}=	Create List

*** Test Cases ***

Delete device custom fields configuration (status_code = 200)
	[Documentation]	Delete device custom fields configuration. Status code 200 is expected. 
	...	Endpoint: /devices/table/{device}/customfields

	${PAYLOAD}=	Set Variable	{"custom_fields":["test_field2"]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table/testdevice/customfields
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session