*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${FIELD_NAME}=	test_field2
${FIELD_VALUE}=	test_value2

*** Test Cases ***

Add device custom field configuration (status_code = 200)
	[Documentation]	Add device custom field configuration. Status code 200 is expected. 
	...	Endpoint: /devices/table/{device}/customfields

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	field_name=${FIELD_NAME}
	Set To Dictionary	${payload}	field_value=${FIELD_VALUE}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table/testdevice/customfields
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session