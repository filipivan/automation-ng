*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${IP_ADDRESS}=	192.168.16.16
${USERNAME}=	admin123

*** Test Cases ***

Update managed device info (status_code = 200)
	[Documentation]	Update managed device info. Status code 200 is expected. 
	...	Endpoint: /devices/table/{device}

	${PAYLOAD}=	Set Variable	{"ip_address":"${IP_ADDRESS}", "username":"${USERNAME}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table/testdevice
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session