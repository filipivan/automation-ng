*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${MONITORING_SNMP}=	true

*** Test Cases ***

Update device management configuration (status_code = 200)
	[Documentation]	Update device management configuration. Status code 200 is expected. 
	...	Endpoint: /devices/table/{device}/management

	${PAYLOAD}=	Set Variable	{"monitoring_snmp":"${MONITORING_SNMP}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table/testdevice/management
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session