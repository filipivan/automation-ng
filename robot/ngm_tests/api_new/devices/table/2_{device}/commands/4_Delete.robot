*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${COMMAND}=	"kvm"
${COMMAND2}=	"custom_commands"
*** Test Cases ***

Delete device command configuration (status_code = 200)
	[Documentation]	Delete device command configuration. Status code 200 is expected. 
	...	Endpoint: /devices/table/{device}/commands

	${PAYLOAD}=	Set Variable	{"commands":[${COMMAND}, ${COMMAND2}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table/testdevice/commands
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session