*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${CUSTOM_COMMAND_LABEL1}=	testing123

*** Test Cases ***

Update device command configuration (status_code = 200)
	[Documentation]	Update device command configuration. Status code 200 is expected. 
	...	Endpoint: /devices/table/{device}/commands/{cmd}

	${PAYLOAD}=	Set Variable	{"custom_command_label1":"${CUSTOM_COMMAND_LABEL1}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table/testdevice/commands/custom_commands
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session