*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${COMMAND}=	KVM
${COMMAND2}=	custom_commands
${ENABLED}=	true
${PROTOCOL}=	RDP
${TYPE_EXTENSION}=	idrac.py

*** Test Cases ***

Add device command configuration KVM (status_code = 200)
	[Documentation]	Add device custom field configuration. Status code 200 is expected. 
	...	Endpoint: /devices/table/{device}/commands

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	command=${COMMAND}
	Set To Dictionary	${payload}	enabled=${ENABLED}
	Set To Dictionary	${payload}	protocol=${PROTOCOL}
	Set To Dictionary	${payload}	type_extension=${TYPE_EXTENSION}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table/testdevice/commands
	Should Be Equal   ${response.status_code}   ${200}

Add device command configuration custom_command (status_code = 200)
	[Documentation]	Add device custom field configuration. Status code 200 is expected.
	...	Endpoint: /devices/table/{device}/commands

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	command=${COMMAND2}
	Set To Dictionary	${payload}	enabled=${ENABLED}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table/testdevice/commands
	Should Be Equal   ${response.status_code}   ${200}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session