*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${DATA_LOGGING}=	true
${ENABLE_DATA_LOGGING_ALERTS}=	true
${DATA_STRING_1}=	abcdefg

*** Test Cases ***

Update device logging configuration (status_code = 200)
	[Documentation]	Update device logging configuration. Status code 200 is expected. 
	...	Endpoint: /devices/table/{device}/logging

	${PAYLOAD}=	Set Variable	{"data_logging":"${DATA_LOGGING}", "enable_data_logging_alerts":"${ENABLE_DATA_LOGGING_ALERTS}", "data_string_1":"${DATA_STRING_1}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table/testdevice/logging
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session