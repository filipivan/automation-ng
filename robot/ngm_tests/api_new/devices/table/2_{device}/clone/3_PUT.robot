*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${CLONE_FROM}=	testdevice
${NAME}=	new_device_3
${NUMBER_OF_CLONES}=	1
${IP_ADDRESS}=	127.0.0.2
${MODE}=	ondemand
${INCREMENT_IP_ADDRESS_ON_EVERY_CLONED_DEVICE}=	false

*** Test Cases ***

Clone device (status_code = 200)
	[Documentation]	Clone device. Status code 200 is expected. 
	...	Endpoint: /devices/table/{device}/clone

	${PAYLOAD}=	Set Variable	{"clone_from":"${CLONE_FROM}", "name":"${NAME}", "number_of_clones":"${NUMBER_OF_CLONES}", "ip_address":"${IP_ADDRESS}", "mode":"${MODE}", "increment_ip_address_on_every_cloned_device":"${INCREMENT_IP_ADDRESS_ON_EVERY_CLONED_DEVICE}"}
	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table/testdevice/clone
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session