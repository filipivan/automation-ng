*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***


*** Test Cases ***

Restore serial default configuration (status_code = 200)
	[Documentation]	Restore serial default configuration. Status code 200 is expected. 
	...	Endpoint: /devices/table/{device}/default

	${PAYLOAD}=	Create Dictionary

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table/testdevice/default
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session