*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get list of device's outlets (status_code = 200)
	[Documentation]	Get list of device's outlets . Status code 200 is expected. 
	...	Endpoint: /devices/table/{device}/outlets/list

	${response}=	API::Send Get Request	/devices/table/testdevice/outlets/list
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session