*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../init.robot
Force Tags	API	NEED-CREATION
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${OUTLETS}=	"spmmgmt_nav"

*** Test Cases ***

Execute a power cycle on the specified device's outlets (status_code = 200)
	[Documentation]	Execute a power cycle on the specified device's outlets . Status code 200 is expected. 
	...	Endpoint: /devices/table/{device}/outlets/cycle

	${PAYLOAD}=	Set Variable	{"outlets":[${OUTLETS}]}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/devices/table/testdevice/outlets/cycle
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session