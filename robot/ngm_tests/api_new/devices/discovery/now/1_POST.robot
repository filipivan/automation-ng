*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${NAMES}=	Create List

*** Test Cases ***

Execute discovery now for the specified list (status_code = 200)
	[Documentation]	Execute discovery now for the specified list. Status code 200 is expected. 
	...	Endpoint: /devices/discovery/now

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	names=${NAMES}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/devices/discovery/now
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session