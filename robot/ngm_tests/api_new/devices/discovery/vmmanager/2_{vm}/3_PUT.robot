*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${DISCOVER_VIRTUAL_MACHINES}=	false

*** Test Cases ***

Update VM manager (status_code = 200)
	[Documentation]	Update VM manager. Status code 200 is expected. 
	...	Endpoint: /devices/discovery/vmmanager/{vm}

	${PAYLOAD}=	Set Variable	{"discover_virtual_machines":${DISCOVER_VIRTUAL_MACHINES}}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/devices/discovery/vmmanager/ESXI2
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session