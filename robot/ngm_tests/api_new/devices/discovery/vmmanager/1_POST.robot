*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${MANAGER}=	ESXI2
${USERNAME}=	root
${PASSWORD}=	root
${CONFIRM_PASSWORD}=	root
${TYPE}=	VMware
${HTML_CONSOLE_PORT}=	7331,7343

*** Test Cases ***

Add VM manager (status_code = 200)
	[Documentation]	Add VM manager. Status code 200 is expected. 
	...	Endpoint: /devices/discovery/vmmanager

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	vmmanager=${MANAGER}
	Set To Dictionary	${payload}	username=${USERNAME}
	Set To Dictionary	${payload}	password=${PASSWORD}
	Set To Dictionary	${payload}	confirm_password=${CONFIRM_PASSWORD}
	Set To Dictionary	${payload}	type=${TYPE}
	Set To Dictionary	${payload}	html_console_port=${HTML_CONSOLE_PORT}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/devices/discovery/vmmanager
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	SUITE:Install VM Manager
	API::Post::Session

SUITE:Teardown
	API::Delete::Session

SUITE:Install VM Manager
	CLI:Open

	CLI:Close Connection