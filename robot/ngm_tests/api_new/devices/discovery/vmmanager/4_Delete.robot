*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${VM_SERVERS}=	"ESXI2"

*** Test Cases ***

Delete VM managers (status_code = 200)
	[Documentation]	Delete VM managers. Status code 200 is expected. 
	...	Endpoint: /devices/discovery/vmmanager

	${PAYLOAD}=	Set Variable	{"vm_servers":[${VM_SERVERS}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/devices/discovery/vmmanager
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session