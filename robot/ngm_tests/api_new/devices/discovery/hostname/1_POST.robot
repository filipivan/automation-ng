*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${STRING}=	abcdefgh122
${STRING_TYPE}=	probe

*** Test Cases ***

Add discovery hostname (status_code = 200)
	[Documentation]	Add discovery hostname. Status code 200 is expected. 
	...	Endpoint: /devices/discovery/hostname

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	string=${STRING}
	Set To Dictionary	${payload}	string_type=${STRING_TYPE}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/devices/discovery/hostname
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session