*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${STRING}=	test_string
${ID}=	${EMPTY}

*** Test Cases ***

Delete hostname discovery rules (status_code = 200)
	[Documentation]	Delete hostname discovery rules. Status code 200 is expected. 
	...	Endpoint: /devices/discovery/hostname

	${HOSTNAME}	SUITE:Get Hostname name
	${PAYLOAD}=	Set Variable	{"hostnames":[${HOSTNAME}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/devices/discovery/hostname
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session

SUITE:Get Hostname name
	${RESPONSE}=	API::Send Get Request	/devices/discovery/hostname
	Should Be Equal   ${response.status_code}   ${200}
	${HOSTNAME}=	evaluate	str(${response.json()})
	${HOSTNAME}=	Remove String	${HOSTNAME}	[
	${HOSTNAME}=	Remove String	${HOSTNAME}	]
	${HOSTNAME}	split string	${HOSTNAME}	},
	FOR	${HOST}	IN	@{HOSTNAME}
		LOG	${HOST}
		${COLUMNS}=	Split String	${HOST}	',
		FOR	${COLUMN}	IN	@{COLUMNS}
			${CONTAINS}	Run Keyword And Return Status	Should Contain	${COLUMN}	'id':
			IF	${CONTAINS}
				${ID}=	Replace String Using Regexp	${COLUMN}	\\s*\{\'id\'\:\\s\'	${EMPTY}
				Continue For Loop If	${TRUE}
			END
			${CONTAINS}	Run Keyword And Return Status	Should Contain	${COLUMN}	${STRING}
			LOG	${ID}
			Exit For Loop If	${CONTAINS}
		END
		Exit For Loop If	${CONTAINS}
	END
	[Return]	"${ID}"