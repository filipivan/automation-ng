*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${PROBE_TIMEOUT}=	10
${NUMBER_OF_RETRIES}=	3
${UPDATE_DEVICE_NAME}=	true
${NEW_DISCOVERED_DEVICE_RECEIVES_THE_NAME_DURING_CONFLICT}=	true

*** Test Cases ***

Update hostname detection global settings (status_code = 200)
	[Documentation]	Update hostname detection global settings. Status code 200 is expected. 
	...	Endpoint: /devices/discovery/hostname_globalsettings

	${PAYLOAD}=	Set Variable	{"probe_timeout":"${PROBE_TIMEOUT}", "number_of_retries":"${NUMBER_OF_RETRIES}", "update_device_name":"${UPDATE_DEVICE_NAME}", "new_discovered_device_receives_the_name_during_conflict":"${NEW_DISCOVERED_DEVICE_RECEIVES_THE_NAME_DURING_CONFLICT}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/devices/discovery/hostname_globalsettings
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session