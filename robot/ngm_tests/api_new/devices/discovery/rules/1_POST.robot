*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${MAC_ADDRESS}=	
${PORT_URI_VM}=	
${DATACENTER}=	
${CLUSTER}=	
${PORT_URI_CONSOLE}=	
${HOST_IDENTIFIER}=	aaaa
${STATUS}=	disabled
${SCAN_ID}=	
${ACTION}=	clone
${CLONE_FROM}=	Test
${METHOD}=	kvmport
${RULE_NAME}=	teste_rule
${RULE_NAME2}=	teste_rule2
${PORT_URI_KVM}=	21

*** Test Cases ***

Add discovery rules (status_code = 200)
	[Documentation]	Add discovery rules. Status code 200 is expected. 
	...	Endpoint: /devices/discovery/rules

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	mac_address=${MAC_ADDRESS}
	Set To Dictionary	${payload}	port_uri_vm=${PORT_URI_VM}
	Set To Dictionary	${payload}	datacenter=${DATACENTER}
	Set To Dictionary	${payload}	cluster=${CLUSTER}
	Set To Dictionary	${payload}	port_uri_console=${PORT_URI_CONSOLE}
	Set To Dictionary	${payload}	host_identifier=${HOST_IDENTIFIER}
	Set To Dictionary	${payload}	status=${STATUS}
	Set To Dictionary	${payload}	scan_id=${SCAN_ID}
	Set To Dictionary	${payload}	action=${ACTION}
	Set To Dictionary	${payload}	clone_from=${CLONE_FROM}
	Set To Dictionary	${payload}	method=${METHOD}
	Set To Dictionary	${payload}	rule_name=${RULE_NAME}
	Set To Dictionary	${payload}	port_uri_kvm=${PORT_URI_KVM}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/devices/discovery/rules
	Should Be Equal   ${response.status_code}   ${200}

Add Second Discovery Rules of Same Type (status_code = 200)
	[Documentation]	Add discovery rules. Status code 200 is expected.
	...	Endpoint: /devices/discovery/rules

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	mac_address=${MAC_ADDRESS}
	Set To Dictionary	${payload}	port_uri_vm=${PORT_URI_VM}
	Set To Dictionary	${payload}	datacenter=${DATACENTER}
	Set To Dictionary	${payload}	cluster=${CLUSTER}
	Set To Dictionary	${payload}	port_uri_console=${PORT_URI_CONSOLE}
	Set To Dictionary	${payload}	host_identifier=${HOST_IDENTIFIER}
	Set To Dictionary	${payload}	status=${STATUS}
	Set To Dictionary	${payload}	scan_id=${SCAN_ID}
	Set To Dictionary	${payload}	action=${ACTION}
	Set To Dictionary	${payload}	clone_from=${CLONE_FROM}
	Set To Dictionary	${payload}	method=${METHOD}
	Set To Dictionary	${payload}	rule_name=${RULE_NAME2}
	Set To Dictionary	${payload}	port_uri_kvm=${PORT_URI_KVM}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/devices/discovery/rules
	Should Be Equal   ${response.status_code}   ${200}



*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session