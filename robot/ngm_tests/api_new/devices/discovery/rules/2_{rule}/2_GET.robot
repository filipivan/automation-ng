*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
##ADD_VARIABLES##

*** Test Cases ***

Get discovery rule information (status_code = 200)
	[Documentation]	Get discovery rule information. Status code 200 is expected. 
	...	Endpoint: /devices/discovery/rules/{rule}

	${response}=	API::Send Get Request	/devices/discovery/rules/teste_rule
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session