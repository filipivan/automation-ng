*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${DISCOVERY_NAME}=	test
${SEED}=	idrac

*** Test Cases ***

Update discovery rule (status_code = 200)
	[Documentation]	Update discovery rule. Status code 200 is expected. 
	...	Endpoint: /devices/discovery/rules/{rule}

	${PAYLOAD}=	Set Variable	{"discovery_name":"${DISCOVERY_NAME}", "seed":"${SEED}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/devices/discovery/rules/teste_rule
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session