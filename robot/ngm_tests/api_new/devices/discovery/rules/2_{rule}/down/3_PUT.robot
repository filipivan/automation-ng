*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***


*** Test Cases ***

Move discovery rule up (status_code = 200)
	[Documentation]	Move discovery rule up. Status code 200 is expected. 
	...	Endpoint: /devices/discovery/rules/{rule}/down

	${PAYLOAD}=	Set Variable	${EMPTY}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/devices/discovery/rules/teste_rule/down
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session