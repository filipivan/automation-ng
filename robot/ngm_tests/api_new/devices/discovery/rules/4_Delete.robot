*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${RULES}=	"teste_rule"
${RULE_NAME2}=	"teste_rule2"

*** Test Cases ***

Delete discovery rules (status_code = 200)
	[Documentation]	Delete discovery rules. Status code 200 is expected. 
	...	Endpoint: /devices/discovery/rules

	${PAYLOAD}=	Set Variable	{"rules":[${RULES}, ${RULE_NAME2}]}

	${response}=	API::Send Delete Request	PAYLOAD=${PAYLOAD}	PATH=/devices/discovery/rules
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session