*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${SCAN_ID}=	test_scan
${IP_RANGE_START}=	192.168.1.1
${IP_RANGE_END}=	192.168.1.255
${ENABLE_SCANNING}=	true
${DEVICE}=	idrac
${PORT_SCAN}=	true
${PORT_LIST}=	22-23,623
${PING}=	true
${SCAN_INTERVAL}=	60
${SIMILAR_DEVICES}=	true

*** Test Cases ***

Add network scan (status_code = 200)
	[Documentation]	Add network scan. Status code 200 is expected. 
	...	Endpoint: /devices/discovery/network

	${PAYLOAD}=	Create Dictionary
	Set To Dictionary	${payload}	scan_id=${SCAN_ID}
	Set To Dictionary	${payload}	ip_range_start=${IP_RANGE_START}
	Set To Dictionary	${payload}	ip_range_end=${IP_RANGE_END}
	Set To Dictionary	${payload}	enable_scanning=${ENABLE_SCANNING}
	Set To Dictionary	${payload}	device=${DEVICE}
	Set To Dictionary	${payload}	port_scan=${PORT_SCAN}
	Set To Dictionary	${payload}	port_list=${PORT_LIST}
	Set To Dictionary	${payload}	ping=${PING}
	Set To Dictionary	${payload}	scan_interval=${SCAN_INTERVAL}
	Set To Dictionary	${payload}	similar_devices=${SIMILAR_DEVICES}

	${response}=	API::Send Post Request	PAYLOAD=${PAYLOAD}	PATH=/devices/discovery/network
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session