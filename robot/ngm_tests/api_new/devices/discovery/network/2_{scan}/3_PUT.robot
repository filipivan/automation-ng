*** Settings ***
Documentation	Test API Documentation samples
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../../../../init.robot
Force Tags	API	
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***

${SCAN_ID}=	test_scan
${IP_RANGE_START}=	192.168.1.10
${IP_RANGE_END}=	192.168.1.205
${ENABLE_SCANNING}=	true, deviceidrac
${PORT_SCAN}=	true
${PORT_LIST}=	22-23,623
${PING}=	true
${SCAN_INTERVAL}=	60
${SIMILAR_DEVICES}=	true

*** Test Cases ***

Update network scan (status_code = 200)
	[Documentation]	Update network scan. Status code 200 is expected. 
	...	Endpoint: /devices/discovery/network/{scan}

	${PAYLOAD}=	Set Variable	{"scan_id":"${SCAN_ID}", "ip_range_start":"${IP_RANGE_START}", "ip_range_end":"${IP_RANGE_END}", "enable_scanning":"${ENABLE_SCANNING}", "port_scan":"${PORT_SCAN}", "port_list":"${PORT_LIST}", "ping":"${PING}", "scan_interval":"${SCAN_INTERVAL}", "similar_devices":"${SIMILAR_DEVICES}"}

	${response}=	API::Send Put Request	PAYLOAD=${PAYLOAD}	PATH=/devices/discovery/network/test_scan
	Should Be Equal   ${response.status_code}   ${200}


*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete::Session