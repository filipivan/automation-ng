*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../../init.robot
Force Tags    API	
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Test Cases ***

Get SSO Identity Provider (status_code = 200)
	[Documentation]	Get SSO Identity Provider. Status code 200 is expected. 
	...	Endpoint: /security/authentication/sso/{method}

	${response}=    API::Send Get Request    /security/authentication/sso/{method}
	Should Be Equal   ${response.status_code}   ${200}

*** Keywords ***
SUITE:Setup
    API::Post::Session

SUITE:Teardown
    API::Delete::Session