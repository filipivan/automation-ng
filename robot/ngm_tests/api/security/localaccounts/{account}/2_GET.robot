*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Test Cases ***

Get local account (status_code = 200)
	[Documentation]	Get local account. Status code 200 is expected. 
	...	Endpoint: /security/localaccounts/{account}

	${response}=    API::Send Get Request    /security/localaccounts/{account}
	Should Be Equal   ${response.status_code}   ${200}
Get local account (status_code = 400)
	[Documentation]	Get local account. Status code 400 is expected. 
	...	Endpoint: /security/localaccounts/{account}

	${response}=    API::Send Get Request    /security/localaccounts/{account}
	Should Be Equal   ${response.status_code}   ${200}

*** Keywords ***
SUITE:Setup
    API::Post::Session

SUITE:Teardown
    API::Delete::Session