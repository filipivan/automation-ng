*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../init.robot
Force Tags    API	
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Test Cases ***

Get local accounts (status_code = 200)
	[Documentation]	Get local accounts. Status code 200 is expected. 
	...	Endpoint: /security/localaccounts

	${response}=    API::Send Get Request    /security/localaccounts
	Should Be Equal   ${response.status_code}   ${200}

*** Keywords ***
SUITE:Setup
    API::Post::Session

SUITE:Teardown
    API::Delete::Session