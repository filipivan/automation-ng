*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../../init.robot
Force Tags    API	
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Test Cases ***

Get authorization group members (status_code = 200)
	[Documentation]	Get authorization group members. Status code 200 is expected. 
	...	Endpoint: /security/authorization/{group}/members

	${response}=    API::Send Get Request    /security/authorization/{group}/members
	Should Be Equal   ${response.status_code}   ${200}

*** Keywords ***
SUITE:Setup
    API::Post::Session

SUITE:Teardown
    API::Delete::Session