*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../../../init.robot
Force Tags    API	
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Test Cases ***

Get a VLAN DHCP Snooping (status_code = 200)
	[Documentation]	Get DHCP Snooping and DHCP Option-82 information of a single switch VLAN. Status code 200 is expected. 
	...	Endpoint: /network/switch/vlan/dhcpsnooping/{vlan}

	${response}=    API::Send Get Request    /network/switch/vlan/dhcpsnooping/{vlan}
	Should Be Equal   ${response.status_code}   ${200}

*** Keywords ***
SUITE:Setup
    API::Post::Session

SUITE:Teardown
    API::Delete::Session