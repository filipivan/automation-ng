*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Test Cases ***

Get SD-WAN Path Steering (status_code = 200)
	[Documentation]	Get SD-WAN Path Steering. Status code 200 is expected. 
	...	Endpoint: /network/sdwan/path_steering

	${response}=    API::Send Get Request    /network/sdwan/path_steering
	Should Be Equal   ${response.status_code}   ${200}

*** Keywords ***
SUITE:Setup
    API::Post::Session

SUITE:Teardown
    API::Delete::Session