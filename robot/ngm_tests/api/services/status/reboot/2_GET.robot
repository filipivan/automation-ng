*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Test Cases ***

Reboot the machine (status_code = 200)
	[Documentation]	Endpoint to reboot the machine when standard access is not available. Status code 200 is expected. 
	...	Endpoint: /services/status/reboot

	${response}=    API::Send Get Request    /services/status/reboot
	Should Be Equal   ${response.status_code}   ${200}
Reboot the machine (status_code = 401)
	[Documentation]	Endpoint to reboot the machine when standard access is not available. Status code 401 is expected. 
	...	Endpoint: /services/status/reboot

	${response}=    API::Send Get Request    /services/status/reboot
	Should Be Equal   ${response.status_code}   ${200}

*** Keywords ***
SUITE:Setup
    API::Post::Session

SUITE:Teardown
    API::Delete::Session