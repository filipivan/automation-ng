*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	NEED-CREATION
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Test Cases ***

Get slot information (status_code = 200)
	[Documentation]	Get slot information. Status code 200 is expected. 
	...	Endpoint: /system/slots/{slot}
	${response}=    API::Send Get Request    /system/slots/1
	Should Be Equal   ${response.status_code}   ${200}

Get wrong slot information (status_code = 500)
	[Documentation]	Get slot information. Status code 500 is expected. Because take a timeout error 
	...	Endpoint: /system/slots/{slot}
	${response}=    API::Send Get Request    /system/slots/10|usb
	Should Be Equal   ${response.status_code}   ${500}

*** Keywords ***
SUITE:Setup
    API::Post::Session  admin  .ZPE5ystems!2020

SUITE:Teardown
    API::Delete::Session