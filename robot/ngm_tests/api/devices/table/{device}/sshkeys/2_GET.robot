*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../../init.robot
Force Tags    API	
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Test Cases ***

Get device's SSH Keys form (status_code = 200)
	[Documentation]	Get device's SSH Keys form. Status code 200 is expected. 
	...	Endpoint: /devices/table/{device}/sshkeys

	${response}=    API::Send Get Request    /devices/table/{device}/sshkeys
	Should Be Equal   ${response.status_code}   ${200}

*** Keywords ***
SUITE:Setup
    API::Post::Session

SUITE:Teardown
    API::Delete::Session