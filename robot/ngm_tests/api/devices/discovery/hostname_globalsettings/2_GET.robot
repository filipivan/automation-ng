*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Test Cases ***

Get hostname detection global settings (status_code = 200)
	[Documentation]	Get hostname detection global settings. Status code 200 is expected. 
	...	Endpoint: /devices/discovery/hostname_globalsettings

	${response}=    API::Send Get Request    /devices/discovery/hostname_globalsettings
	Should Be Equal   ${response.status_code}   ${200}

*** Keywords ***
SUITE:Setup
    API::Post::Session

SUITE:Teardown
    API::Delete::Session