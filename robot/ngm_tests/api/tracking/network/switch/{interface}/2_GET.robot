*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../../init.robot
Force Tags    API	NEED-CREATION
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Test Cases ***
Get network switch interface information (status_code = 200)	[Documentation]	Get network switch interface information. Status code 200 is expected. 
	...	Endpoint: /tracking/network/switch/{interface}
	${response}=    API::Send Get Request    /tracking/network/switch/{interface}	Should Be Equal   ${response.status_code}   ${200}

*** Keywords ***
SUITE:Setup
    API::Post::Session

SUITE:Teardown
    API::Delete::Session