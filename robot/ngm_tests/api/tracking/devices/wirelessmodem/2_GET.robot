*** Settings ***
Documentation    Test API Documentation samples
Metadata    Version            1.0
Metadata    Executed At        ${HOST}
Resource    ../../../../init.robot
Force Tags    API	NEED-CREATION
Default Tags    EXCLUDEIN3_2

Suite Setup    SUITE:Setup
Suite Teardown    SUITE:Teardown

*** Test Cases ***
Get Tracking Devices Wireless Modem information (status_code = 200)	[Documentation]	Get Tracking Devices Wireless Modem information. Status code 200 is expected. 
	...	Endpoint: /tracking/devices/wirelessmodem
	${response}=    API::Send Get Request    /tracking/devices/wirelessmodem	Should Be Equal   ${response.status_code}   ${200}

*** Keywords ***
SUITE:Setup
    API::Post::Session

SUITE:Teardown
    API::Delete::Session