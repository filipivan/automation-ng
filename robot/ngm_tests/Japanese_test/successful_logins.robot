*** Settings ***
Documentation    Successfull Login Test Suite for NodeGrid devices
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    init.robot

Test Template  Login with valid credentails should succeed
Suite Setup     GUI::Basic::Open NodeGrid      ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
Suite Teardown  Close all Browsers

Force Tags         GUI     ${BROWSER}     ${SYSTEM}       ${MODEL}        ${VERSION}    WINDOWS     CHROME  FIREFOX   EDGE     IE


*** Test Cases ***              USERNAME            PASSWORD
Scuccessfull admin login        admin               admin       TRUE

*** Keywords ***
Provided precondition
    Setup system under test

Login with valid credentails should succeed
        [Arguments]     ${USERNAME}     ${PASSWORD}     ${SCREENSHOT}=FALSE
        [Documentation]         Testest SUccessful logins with valid username and password combinations
        ...     == REQUIREMENTS ==
        ...     None
        ...     == ARGUMENTS ==
        ...     - USERNAME =   Username which should be used
        ...     - PASSWORD  =   Password which should be used
        ...     == EXPECTED RESULT ==
        ...     User Sessions are expected to succeed
        [Tags]      GUI    LOGIN
        GUI::Basic::Login           ${NGVERSION}    ${USERNAME}     ${PASSWORD}
        Run Keyword If      '${SCREENSHOT}' != 'FALSE'      Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png
        GUI::Basic::Logout
