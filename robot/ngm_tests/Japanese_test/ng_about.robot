*** Settings ***
Documentation    Successfully Display the about information
Resource    init.robot
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Force Tags         GUI    ${SYSTEM}   ${BROWSER}    ${MODEL}    ${VERSION}  WINDOWS     CHROME  FIREFOX   EDGE


Suite Teardown  Run Keywords    GUI::Basic::Logout  Close all Browsers


*** Variables ***


*** Test Cases ***

Display About information
    GUI::Basic::Open NodeGrid      ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    GUI::Basic::Login       ${NGVERSION}    admin       admin
    GUI::Basic::About       ${NGVERSION}

*** Keywords ***
Provided precondition
    Setup system under test