*** Settings ***
Documentation	
Resource	init.robot
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Force Tags	GUI	${SYSTEM}	${BROWSER}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE

Suite Teardown	Run Keywords    GUI::Basic::Logout  Close all Browsers

*** Variables ***

*** Test Cases ***

Check If user@hostname is correct
	GUI::Basic::Open NodeGrid      ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
	GUI::Basic::Login       ${NGVERSION}    admin       admin
	Page Should Not Contain	admin@undefined

*** Keywords ***
Provided precondition
    Setup system under test
