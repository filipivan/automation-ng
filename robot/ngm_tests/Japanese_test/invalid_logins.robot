*** Settings ***
Documentation	Invalid Login Test Suite for NodeGrid devices
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	init.robot
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE	LOGIN	NON-CRITICAL
Default Tags	GUI	${BROWSER}

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

Test Template  SUITE:Login with invalid credentails should fail

*** Test Cases ***
GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}
Case Sensitiv username	aDmin	admin
Case Sensitive password	admin	Admin
Invalid username	invalid	admin
Invalid password	admin	invalid
Invalid Username and password	invalid	invalid
Empty username	${EMPTY}	admin
Empty password	admin	${EMPTY}
Empty username and password	${EMPTY}	${EMPTY}
root login	root	root

*** Keywords ***
SUITE:Setup
	GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}

SUITE:Teardown
	Close all Browsers

SUITE:Login with invalid credentails should fail
	[Arguments]	${USERNAME}	${PASSWORD}	${SCREENSHOT}=${FALSE}
	[Documentation]	Test Successful logins with valid username and password combinations
	...     == REQUIREMENTS ==
	...     None
	...     == ARGUMENTS ==
	...     - USERNAME =   Username which should be used
	...     - PASSWORD  =   Password which should be used
	...     == EXPECTED RESULT ==
	...     User Sessions are expected to succeed
	GUI::Basic::Login Failure	${USERNAME}	${PASSWORD}
	Run Keyword If	'${SCREENSHOT}' != '${FALSE}'	Capture Page Screenshot	filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png
	Reload Page
	Page Should Not Contain	Login failed