*** Settings ***
Documentation    Testing if Elements in the System -> Scheduler Add are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL

*** Test Cases ***

Add Scheduler Task and Set Name
    Click element   jquery=#addButton
    GUI::Basic::Spinner Should Be Invisible
    Input Text      jquery=#minute  61
    Input Text      jquery=#command     Dummy

Test Task Name
    gui::auditing::auto input tests  taskName   Task Name   0taskname   TaskName/   TaskName

Test Command Name
    gui::auditing::auto input tests  command    cmd     Command To Execute

Test User Name
    gui::auditing::auto input tests  user   user    valid   daemon

Test Duplicate Task Name
    Input Text      jquery=#minute  59
    Input Text      jquery=#taskName    Dummy
    Input Text      jquery=#command     Dummy
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=#addButton
    GUI::Basic::Spinner Should Be Invisible
    Input Text      jquery=#minute  59
    Input Text      jquery=#taskName    Dummy
    Input Text      jquery=#command     Dummy
    GUI::Basic::Save
    GUI::Basic::Japanese::Test Translation      //*[@id="errormsg"]
    GUI::Basic::Cancel
    Click Element       //*[@id="thead"]/tr/th[1]/input
    GUI::Basic::Delete

*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Open System tab
    GUI::Basic::Spinner Should Be Invisible
    GUI::System::Open Scheduler tab
    GUI::Basic::Spinner Should Be Invisible


Suite Teardown
    GUI::Basic::Logout
    Close all Browsers