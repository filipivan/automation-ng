*** Settings ***
Documentation    Testing if Elements in the System -> Scheduler Add are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL

*** Test Cases ***

Add Scheduler Task and Set Name
    Click element   jquery=#addButton
    GUI::Basic::Spinner Should Be Invisible
    Input Text      jquery=#taskName    ./.;'

Test Setting Minutes
    ${RAND}=    evaluate  random.randint(1,59)  modules=random
    gui::auditing::auto input tests     minute      str     60  ${RAND}

Test Setting Hours
    ${RAND}=    evaluate    random.randint(1,23)    modules=random
    gui::auditing::auto input tests     hour        str     24  ${RAND}

Test Setting Day of Week
    ${RAND}=    evaluate    random.randint(0,6)     modules=random
    gui::auditing::auto input tests     dow         str     8   ${RAND}

Test Setting Day of Month
    ${RAND}=    evaluate    random.randint(1,28)    modules=random
    gui::auditing::auto input tests     dom         str     32  0   ${RAND}

Test Setting Month
    ${RAND}=    evaluate    random.randint(1,12)    modules=random
    gui::auditing::auto input tests     month       str     13  0   ${RAND}

*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Open System tab
    GUI::Basic::Spinner Should Be Invisible
    GUI::System::Open Scheduler tab
    GUI::Basic::Spinner Should Be Invisible


Suite Teardown
    GUI::Basic::Logout
    Close all Browsers