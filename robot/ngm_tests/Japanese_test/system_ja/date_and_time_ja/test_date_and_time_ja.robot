*** Settings ***
Documentation    Testing if Elements in the System ->  Date and Time are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL


*** Test Cases ***
Test Date and Time Tab
    GUI::Basic::Japanese::Test Translation      jquery=body > div.main_menu > div > ul > li:nth-child(3)

Test Date and Time Save Button
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]

Test Date and Time
    GUI::Basic::Japanese::Test Translation      //*[@id="datetime"]
    GUI::Basic::Japanese::Test Translation      //*[@id="dateTimeForm"]/div[2]/div[1]/div/div/div[2]/div/div/div[1]/label
    GUI::Basic::Japanese::Test Translation      //*[@id="dateTimeForm"]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/label
    : FOR    ${INDEX}    IN RANGE    1    5
    \    GUI::Basic::Japanese::Test Translation      //*[@id="dateTimeForm"]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[${INDEX}]/label

Test Time Zone
    GUI::Basic::Japanese::Test Translation      //*[@id="timezone"]
    GUI::Basic::Japanese::Test Translation      //*[@id="dateTimeForm"]/div[2]/div[2]/div/div/div[2]/div/div/div/label
    : FOR    ${INDEX}    IN RANGE    1    35
    \    GUI::Basic::Japanese::Test Translation      //*[@id="timezone_list"]/option[${INDEX}]

Test invalid input
    #pool.ntp.org
    Wait Until Element Is Visible       jquery=#saveButton
    gui::auditing::auto input tests     server      ...../////...//.././.   www.google.com
    Input Text          xpath=//input[@id='server']     pool.ntp.org
    Click Element       jquery=#saveButton

Test Manual Time Setting
    ${BOT}=     Is Bottom Button Checked
    run keyword if      ${BOT}==${FALSE}        Click Element   //input[@value='manual']
    #Getting and Setting the Day
    Click Element   //a[@class='ui-datepicker-prev ui-corner-all']
    Click Element   //td[@class=' ui-datepicker-week-end ']
    #Resetting the cursor
    Click Element   //input[@value='manual']
    #Setting the H/M/S
    Execute Javascript	window.scrollTo(0,document.body.scrollHeight);
    Click Element At Coordinates    //div[@class='ui_tpicker_hour_slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all']     -30     0
    Click Element At Coordinates    //div[@class='ui_tpicker_minute_slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all']     20     0
    Click Element At Coordinates    //div[@class='ui_tpicker_second_slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all']     -45     0
    Click Element   jquery=#saveButton

Checking Time Change
    ${DAY}=     Get Text    //a[@class='ui-state-default ui-state-active']
    ${MONTH_TXT}=       Get Text        //span[@class='ui-datepicker-month']
    @{MONTH_LST}      Create List   January     February    March   April   May     June    July    August      September       October     November        December
    ${MONTH_NUMBER}=    get index from list   ${MONTH_LST}  ${MONTH_TXT}
    ${MONTH_NUMBER}=    Evaluate   ${MONTH_NUMBER} + 1
    @{REAL_TIME}=    Get Time    year month day hour minute second
    should not be true      ${MONTH_NUMBER}==@{REAL_TIME}[1]
    should not be true      ${DAY}==@{REAL_TIME}[2]

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[3]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(3)
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    Execute Javascript	window.scrollTo(0,document.body.scrollHeight);
    GUI::Basic::Wait Until Element Is Accessible  //input[@value='auto']
    Click Element   //input[@value='auto']
    Click Element   jquery=#saveButton
    GUI::Basic::Logout
    Close all Browsers

Is Top Button Checked
    ${TOP_BTN}=     run keyword and return status   Element Should Not Be Visible   jquery=#datepicker
    [Return]    ${TOP_BTN}

Is Bottom Button Checked
    ${BOT_BTN}=     run keyword and return status   Element Should Not Be Visible   jquery=#ntpUpdate
    [Return]    ${BOT_BTN}