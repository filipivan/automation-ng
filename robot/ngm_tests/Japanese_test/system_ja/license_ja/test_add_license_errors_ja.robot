*** Settings ***
Documentation	Testing if Errors in the System ->	License add Tab are translated
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2	NON-CRITICAL

*** Variables ***
${LICENSE100}	${ONE_HUNDRED_DEVICES_ACCESS_LICENSE}
${INVLICENSE}	Invalid-License
${EXPLICENSE}	${FIFTY_DEVICES_ACCESS_LICENSE_EXPIRED}

*** Test Cases ***
Test Add Duplicate License Error
	GUI::System::License::Add License	${LICENSE100}
	GUI::Basic::Spinner Should Be Invisible
	GUI::System::License::Add License	${LICENSE100}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Japanese::Test Translation	//*[@id="errormsg"]
	GUI::Basic::Japanese::Test Translation	//*[@id="globalerr"]/div/div/p

Test License Add Invalid License Error
	GUI::Basic::Cancel
	GUI::System::License::Add License	${INVLICENSE}
	GUI::Basic::Japanese::Test Translation	//*[@id="errormsg"]
	GUI::Basic::Japanese::Test Translation	//*[@id="globalerr"]/div/div/p

Test Add Expired License Message
	GUI::Basic::Cancel
	GUI::System::Open License tab
	GUI::System::License::Add License	${EXPLICENSE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Japanese::Test Translation	//*[@id="xxxxx-xxxxx-xxxxx-33MGM:00000000042:targets:50:2018-09-01:local:local::error.license.time.expired"]/td[9]

Test Delete License
	Suite delete License	${LICENSE100}
	Suite delete License	${EXPLICENSE}

*** Keywords ***
Suite Setup
	${PROFILE}=	Evaluate	sys.modules['selenium.webdriver'].FirefoxProfile()	sys
	Call Method	${PROFILE}	set_preference	intl.accept_languages	ja
	Create WebDriver	Firefox	firefox_profile=${PROFILE}
	#GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	Go To	${HOMEPAGE}
	Wait Until element Is Visible	jquery=#login-btn
	GUI::Basic::Login	${NGVERSION}
	Click Element	//*[@id="main_menu"]/li[3]
	GUI::Basic::Spinner Should Be Invisible

Suite Teardown
	GUI::Basic::Logout
	Close all Browsers

Suite delete License
	[Arguments]	${LICENSE}
	${key}=	Fetch From Right	${LICENSE}	-
	${Licensekey}=	Catenate	SEPARATOR=	xxxxx-xxxxx-xxxxx-	${key}
	GUI::Basic::Select Rows In Table Containing Value	license_table	${Licensekey}
	GUI::Basic::Delete