*** Settings ***
Documentation	Testing if Elements in the System ->	License Tab are translated
Metadata	Version	1.0
Metadata	Executed At	${HOST}
Metadata	Executed with	${BROWSER}
Resource	../../init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2	NON-CRITICAL

*** Variables ***
${LICENSE100}	${ONE_HUNDRED_DEVICES_ACCESS_LICENSE}


*** Test Cases ***

Test License nonAccess Controls
	GUI::Basic::Japanese::Test Translation With Value	//*[@id="addButton"]
	GUI::Basic::Japanese::Test Translation With Value	//*[@id="delButton"]
	GUI::Basic::Japanese::Test Translation	//*[@id="nonAccessControls"]/div/div[1]
	GUI::Basic::Japanese::Test Translation	//*[@id="nonAccessControls"]/div/div[2]

Test License Table
	GUI::Basic::Japanese::Test Translation Table Header With Checkbox	9
	GUI::System::License::Add License	${LICENSE100}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Japanese::Test Table Elements	license_table	4
	GUI::Basic::Japanese::Test Table Elements	license_table	6
	GUI::Basic::Japanese::Test Table Elements	license_table	7
	Suite delete License	${LICENSE100}

*** Keywords ***
Suite Setup
	${PROFILE}=	Evaluate	sys.modules['selenium.webdriver'].FirefoxProfile()	sys
	Call Method	${PROFILE}	set_preference	intl.accept_languages	ja
	Create WebDriver	Firefox	firefox_profile=${PROFILE}
	#GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	Go To	${HOMEPAGE}
	Wait Until element Is Visible	jquery=#login-btn
	GUI::Basic::Login	${NGVERSION}
	Click Element	//*[@id="main_menu"]/li[3]
	GUI::Basic::Spinner Should Be Invisible

Suite Teardown
	GUI::Basic::Logout
	Close all Browsers

Suite delete License
	[Arguments]	${LICENSE}
	${key}=	Fetch From Right	${LICENSE}	-
	${Licensekey}=	Catenate	SEPARATOR=	xxxxx-xxxxx-xxxxx-	${key}
	GUI::Basic::Select Rows In Table Containing Value	license_table	${Licensekey}
	GUI::Basic::Delete