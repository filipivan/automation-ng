*** Settings ***
Documentation    Testing if Elements in the System ->  Logging Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL


*** Test Cases ***
Test Logging Title and Save
    GUI::Basic::Japanese::Test Translation      //*[@id="sessiontitle"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]

Test Logging Elements
    Select Checkbox     //*[@id="sessionlogenable"]
    Select Checkbox     //*[@id="alertenable"]
    GUI::Basic::Japanese::Test Translation      //*[@id="sessionform"]/div[2]/div/div/div/div[2]/div/div/div/div[1]/label
    GUI::Basic::Japanese::Test Translation      //*[@id="sessionform"]/div[2]/div/div/div/div[2]/div/div/div/div[2]/div/div[1]/label

    : FOR    ${INDEX}    IN RANGE    1    6
    \    GUI::Basic::Japanese::Test Translation      //*[@id="sessionform"]/div[2]/div/div/div/div[2]/div/div/div/div[2]/div/div[2]/div[${INDEX}]/label


*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[3]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(5)
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers