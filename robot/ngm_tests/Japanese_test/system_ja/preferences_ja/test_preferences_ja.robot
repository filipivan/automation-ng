*** Settings ***
Documentation    Testing if Elements in the System ->  Preferences Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL

*** Test Cases ***
Test Preferences Tab
    GUI::Basic::Japanese::Test Translation      jquery=body > div.main_menu > div > ul > li:nth-child(2)

Test Preferences Save Button
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]

Test Preferences Nodegrid Location
    GUI::Basic::Japanese::Test Translation      //*[@id="location"]
    : FOR    ${INDEX}    IN RANGE    1    4
    \    GUI::Basic::Japanese::Test Translation      //*[@id="securityProfile"]/div[2]/div/div/div/div[2]/div/div/div[${INDEX}]

Test Preferences Session Idle Timeout
    GUI::Basic::Japanese::Test Translation      //*[@id="sessionTimeout"]
    GUI::Basic::Japanese::Test Translation      //*[@id="securityProfile"]/div[3]/div/div/div[1]/div[2]/div/div/div[1]/label
    GUI::Basic::Japanese::Test Translation      //*[@id="notice"]

Test Preferences Login Page Logo Image Default
    GUI::Basic::Japanese::Test Translation      //*[@id="Logo"]
    GUI::Basic::Japanese::Test Translation      //*[@id="securityProfile"]/div[3]/div/div/div[2]/div[2]/div/div/div/div[1]/label
    GUI::Basic::Select Checkbox	                //*[@id='LogoOptions']
    GUI::Basic::Wait Until Element Is Accessible	jquery=#applytypeselection
    GUI::Basic::Japanese::Test Translation      //*[@id="securityProfile"]/div[3]/div/div/div[2]/div[2]/div/div/div/div[2]/div/label
    GUI::Basic::Japanese::Test Translation      //*[@id="securityProfile"]/div[3]/div/div/div[2]/div[2]/div/div/div/div[2]/div/div/div[1]/label

Test Preferences Login Page Logo Image Remote
	Select Radio Button     applytypeselection	applylogo-remote
	: FOR    ${INDEX}    IN RANGE    3    7
    \    GUI::Basic::Japanese::Test Translation      //*[@id="securityProfile"]/div[3]/div/div/div[2]/div[2]/div/div/div/div[2]/div/div/div[3]/label
    GUI::Basic::Japanese::Test Translation      //*[@id="securityProfile"]/div[3]/div/div/div[2]/div[2]/div/div/div/div[2]/div/div/div[7]/div/label

Test Preferences Login Page Logo Image Local
    Select Radio Button     applytypeselection	applylogo-localcomputer
    GUI::Basic::Japanese::Test Translation      //*[@id="securityProfile"]/div[3]/div/div/div[2]/div[2]/div/div/div/div[2]/div/div/div[2]/label
    GUI::Basic::Japanese::Test Translation      //*[@id="file_form"]/div/label
    GUI::Basic::Unselect Checkbox               //*[@id='LogoOptions']

Test Preferences Login Banner Message
    GUI::Basic::Japanese::Test Translation      //*[@id="loginBanner"]
    GUI::Basic::Japanese::Test Translation      //*[@id="securityProfile"]/div[4]/div/div/div[1]/div[2]/div/div/div[1]/div[1]/label
    GUI::Basic::Japanese::Test Translation      //*[@id="loginBannerNote"]
    Select Checkbox                             //*[@id="loginBannerStatus"]
    GUI::Basic::Japanese::Test Translation      //*[@id="securityProfile"]/div[4]/div/div/div[1]/div[2]/div/div/div[1]/div[2]/div/label

Test Preferences Utilization Rate Events
    GUI::Basic::Japanese::Test Translation      //*[@id="utilRate"]
    GUI::Basic::Japanese::Test Translation      //*[@id="securityProfile"]/div[4]/div/div/div[2]/div[2]/div/div/div[1]/div/label
    GUI::Basic::Japanese::Test Translation      //*[@id="securityProfile"]/div[4]/div/div/div[2]/div[2]/div/div/div[2]/label

Test Preferences Network Boot
    GUI::Basic::Japanese::Test Translation      //*[@id="netbootConf"]

    : FOR    ${INDEX}    IN RANGE    1    5
    \    GUI::Basic::Japanese::Test Translation      //*[@id="securityProfile"]/div[4]/div/div/div[3]/div[2]/div/div/div[${INDEX}]/label

Test Coordinates Field
    ${RAND}=    evaluate    random.randint(-90,90)  modules=random
    gui::auditing::auto input tests     coordinates     -91,180     91,180      90,-181     90,181      ${RAND},${RAND}

Test Timeout Field
    ${RAND}=    evaluate    random.randint(1,10000)  modules=random
    gui::auditing::auto input tests     idletimeout     -1      100000      str     ${RAND}

Test Utilization Trigger Field
    ${RAND}=    evaluate    random.randint(1,99)    modules=random
    gui::auditing::auto input tests     utilRateTrigger     -1      str     100     ${RAND}

Test Netboot IP Field
    ${RAND}=    evaluate    random.randint(0,255)   modules=random
    gui::auditing::auto input tests     netboot_ipaddr      str     256.256.256.256     002c::002c  123.123.123.${RAND}

Test Netboot Netmask Field
    ${RAND}=    evaluate    random.randint(0,255)   modules=random
    gui::auditing::auto input tests     netboot_netmask      str     256.256.256.256     002c::002c  123.123.123.${RAND}

Reset Fields
    Input Text      jquery=#idletimeout         0
    Input Text      jquery=#utilRateTrigger     90
    input Text      jquery=#netboot_ipaddr      192.168.160.1
    Input Text      jquery=#netboot_netmask     255.255.255.0
    Click Element   jquery=#saveButton

*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[3]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(2)
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers