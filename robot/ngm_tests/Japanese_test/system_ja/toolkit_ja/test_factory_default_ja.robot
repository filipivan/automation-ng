*** Settings ***
Documentation    Testing if Elements in the System ->  Toolkit -> Restore to Factory Default Settings Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL


*** Test Cases ***
Test Factory Defualt nonAccess Controls Buttons
    Click Element   //*[@id="icon_factoryDefault"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="nonAccessControls"]/input[1]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="nonAccessControls"]/input[2]

Test Factory Defualt Elements
    GUI::Basic::Japanese::Test Translation      //*[@id="factoryDefaultForm"]/div[2]/div/div/div[1]/div/label
    GUI::Basic::Japanese::Test Translation      //*[@id="factorydefault_result"]

    Click Element   //*[@id="cancelButton"]


*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[3]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(4)
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers