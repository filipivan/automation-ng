*** Settings ***
Documentation    Testing if Elements in the System ->  Toolkit -> Apply Settings are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL


*** Test Cases ***
Test Apply nonAccess Controls Buttons
    Click Element   //*[@id="icon_applySettings"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="cancelButton"]

Test Apply Elements
    GUI::Basic::Japanese::Test Translation      //*[@id="applySettingsForm"]/div[2]/div/div/div[1]/label
    : FOR    ${INDEX}    IN RANGE    1    5
    \    GUI::Basic::Japanese::Test Translation      //*[@id="applySettingsForm"]/div[2]/div/div/div[1]/div/div[${INDEX}]/label
    GUI::Basic::Japanese::Test Translation      //*[@id="warning"]

Test Apply From Local System Invalid File Name Error
    Input Text      //*[@id="localFilename"]      ...
    GUI::Basic::Save        True
    GUI::Basic::Japanese::Test Translation      //*[@id="errormsg"]

Test Apply From Local Computer
    Select Radio Button     applytypeselection     applysettings-localcomputer
    GUI::Basic::Japanese::Test Translation      //*[@id="file_form"]/div/label

Test Local System Apply Errors
    Input Text      //*[@id="localFilename"]      ./;',[]\!@#$%^&*(
    Click Element   //*[@id="saveButton"]
    Handle Alert
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation      //*[@id="errormsg"]

Test Apply From Remote
    Select Radio Button     applytypeselection     applysettings-remote
    GUI::Basic::Japanese::Test Translation      //*[@id="applySettingsForm"]/div[2]/div/div/div[1]/div/div[5]/label
    GUI::Basic::Japanese::Test Translation      //*[@id="applySettingsForm"]/div[2]/div/div/div[1]/div/div[6]/label
    GUI::Basic::Japanese::Test Translation      //*[@id="applySettingsForm"]/div[2]/div/div/div[1]/div/div[7]/label
    GUI::Basic::Japanese::Test Translation      //*[@id="applySettingsForm"]/div[2]/div/div/div[1]/div/div[8]/div/label

Test Apply To Remote No URL Error
    GUI::Basic::Save        True
    GUI::Basic::Japanese::Test Translation      //*[@id="errormsg"]

Test Apply To Remote Invalid URL Error
    Input Text      //*[@id="url"]      Dummy
    GUI::Basic::Save        True
    GUI::Basic::Japanese::Test Translation      //*[@id="errormsg"]

Test Remote Apply Errors
    Input text      jquery=#userName    a
    INput Text      jquery=#password    a
    gui::auditing::auto input tests     url     notaurl     12.12.12.12     https://aa.aa.aa    ftp://aa.aa.aa.aa.aaa

Exit Page
    Click Element   //*[@id="cancelButton"]


*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[3]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(4)
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers