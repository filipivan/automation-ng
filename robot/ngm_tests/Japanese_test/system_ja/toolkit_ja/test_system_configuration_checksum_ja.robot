*** Settings ***
Documentation    Testing if Elements in the System ->  Toolkit -> System Configuration Checksum Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL


*** Test Cases ***
Test System Configuration Checksum nonAccess Controls Buttons
    Click Element       //*[@id="icon_systemCfgChk"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="cancelButton"]

Test System Configuration Checksum Type
    GUI::Basic::Japanese::Test Translation      //*[@id="systemCfgChkForm"]/div[2]/div/div/div/label

Test System Configuration Checksum MD5SUM Radio
    Select Radio Button     systemCfgChkActionMD5   md5verify
    GUI::Basic::Japanese::Test Translation      //*[@id="systemCfgChkForm"]/div[2]/div/div/div/div/div[1]/label
    : FOR    ${INDEX}    IN RANGE    1    5
    \    GUI::Basic::Japanese::Test Translation      //*[@id="systemCfgChkForm"]/div[2]/div/div/div/div/div[2]/div/div[${INDEX}]/label

Test System Configuration Checksum MD5SUM Create Baseline
    Select Radio Button     systemCfgChkActionMD5   md5baseline
    Save and Test Finish and Main Doc    5

Test System Configuration Checksum MD5SUM Compare Baseline
    Click Element   //*[@id="icon_systemCfgChk"]
    GUI::Basic::Spinner Should Be Invisible
    Select Radio Button     systemCfgChkActionMD5   md5verify
    Save and Test Finish and Main Doc    7

Test System Configuration Checksum SHA256SUM
    Open Checksum SHA with Radio   shaverify
    GUI::Basic::Japanese::Test Translation      //*[@id="systemCfgChkForm"]/div[2]/div/div/div/div/div[3]/label
    : FOR    ${INDEX}    IN RANGE    1    5
    \    GUI::Basic::Japanese::Test Translation      //*[@id="systemCfgChkForm"]/div[2]/div/div/div/div/div[4]/div/div[${INDEX}]/label

Test System Configuration Checksum SHA256SUM View Current
    Select Radio Button     systemCfgChkActionSHA     shaview
    Save and Test Finish and Main Doc    4

Test System Configuration Checksum SHA256SUM Create Baseline
    Open Checksum SHA with Radio     shabaseline
    Save and Test Finish and Main Doc    5

Test System Configuration Checksum SHA256SUM Compare Baseline
    Open Checksum SHA with Radio     shaverify
    Save and Test Finish and Main Doc    7

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[3]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(4)
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers

Open Checksum SHA with Radio
    [Arguments]	${SHA_Radio}
    Click Element   //*[@id="icon_systemCfgChk"]
    GUI::Basic::Spinner Should Be Invisible
    Select Radio Button     systemCfgChkselection     sha256check
    Select Radio Button     systemCfgChkActionSHA   ${SHA_Radio}

Save and Test Finish and Main Doc
    [Arguments]	${Num Items}
    GUI::Basic::Save
    GUI::Basic::Japanese::Test Translation With Value       //*[@id="finish"]
    : FOR    ${INDEX}    IN RANGE    2    ${Num Items}
    \    GUI::Basic::Japanese::Test Translation      //*[@id="main_doc"]/div[${INDEX}]
    Click Element       //*[@id="finish"]
    GUI::Basic::Spinner Should Be Invisible