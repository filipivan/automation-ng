*** Settings ***
Documentation    Testing if Elements in the System ->  Toolkit -> System Certificate Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL


*** Test Cases ***
Test System Certificate nonAccess Controls Buttons
    Click Element   //*[@id="icon_systemCert"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="cancelButton"]

Test System Certificate Elements
    GUI::Basic::Japanese::Test Translation      //*[@id="systemcertForm"]/div[2]/div/div/div[1]/label       #From
    GUI::Basic::Japanese::Test Translation      //*[@id="notice"]

Test System Certificate From Remote Server
    Select Radio Button     systemcertselection     systemcert-remote
    : FOR    ${INDEX}    IN RANGE    1    6
    \    GUI::Basic::Japanese::Test Translation      //*[@id="systemcertForm"]/div[2]/div/div/div[1]/div/div[${INDEX}]/label
    GUI::Basic::Japanese::Test Translation      //*[@id="systemcertForm"]/div[2]/div/div/div[1]/div/div[6]/div/label

Test System Certificate From Remote No URL Error
    Input Text      //*[@id="userName"]      Dummy
    GUI::Basic::Save        True
    GUI::Basic::Japanese::Test Translation      //*[@id="errormsg"]

Test System Certificate From Remote Invalid URL Error
    Input Text      //*[@id="url"]      Dummy
    GUI::Basic::Save        True
    GUI::Basic::Japanese::Test Translation      //*[@id="errormsg"]

Test System Certificate From Local Computer
    Select Radio Button     systemcertselection     systemcert-localcomputer
    GUI::Basic::Japanese::Test Translation      //*[@id="file_form"]/div/label      #Filename

Test Remote Apply Errors
    Input text      jquery=#userName    a
    INput Text      jquery=#password    a
    gui::auditing::auto input tests     url     notaurl     12.12.12.12     https://aa.aa.aa    ftp://aa.aa.aa.aa.aaa

Exit Page
    Click Element   //*[@id="cancelButton"]


*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[3]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(4)
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers