*** Settings ***
Documentation    Testing if Elements in the System ->  Toolkit -> Software Upgrade are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL


*** Test Cases ***
Test Upgrade nonAccess Controls Buttons
    Click Element   //*[@id="icon_upgrade"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="cancelButton"]

Test Upgrade Elements
    GUI::Basic::Japanese::Test Translation      //*[@id="upgradeForm"]/div[2]/div/div/div[1]/label
    : FOR    ${INDEX}    IN RANGE    1    6
    \    GUI::Basic::Japanese::Test Translation      //*[@id="upgradeForm"]/div[2]/div/div/div[1]/div/div[${INDEX}]

    Select Radio Button     upgradetypeselection     upgrade-localcomputer
    GUI::Basic::Japanese::Test Translation      //*[@id="file_form"]/div/label

    Select Radio Button     upgradetypeselection     upgrade-remote
    GUI::Basic::Japanese::Test Translation      //*[@id="upgradeForm"]/div[2]/div/div/div[1]/div/div[6]/label
    GUI::Basic::Japanese::Test Translation      //*[@id="upgradeForm"]/div[2]/div/div/div[1]/div/div[7]/label
    GUI::Basic::Japanese::Test Translation      //*[@id="upgradeForm"]/div[2]/div/div/div[1]/div/div[8]/label
    GUI::Basic::Japanese::Test Translation      //*[@id="upgradeForm"]/div[2]/div/div/div[1]/div/div[9]/div/label

Test Upgrade Elements URL Error
    Input Text      //*[@id="url"]      Dummy
    Click Element   //*[@id="saveButton"]
    Handle Alert
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation      //*[@id="errormsg"]
    Input text      jquery=#userName    a
    INput Text      jquery=#password    a
    gui::auditing::auto input tests     url     notaurl     12.12.12.12     https://aa.aa.aa    ftp://aa.aa.aa.aa.aaa

Test Downgrading Elements
    GUI::Basic::Japanese::Test Translation      //*[@id="upgradeForm"]/div[2]/div/div/div[2]/div/label
    GUI::Basic::Japanese::Test Translation      //*[@id="upgradeForm"]/div[2]/div/div/div[3]/label
    GUI::Basic::Japanese::Test Translation      //*[@id="upgradeForm"]/div[2]/div/div/div[3]/div/div[1]/label
    GUI::Basic::Japanese::Test Translation      //*[@id="upgradeForm"]/div[2]/div/div/div[3]/div/div[2]/label
    GUI::Basic::Japanese::Test Translation      //*[@id="upgradeForm"]/div[2]/div/div/div[4]

    Click Element   //*[@id="cancelButton"]


*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[3]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(4)
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers