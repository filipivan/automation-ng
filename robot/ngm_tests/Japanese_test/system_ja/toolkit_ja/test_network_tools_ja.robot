*** Settings ***
Documentation    Testing if Elements in the System ->  Toolkit -> Network Tools are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL


*** Test Cases ***
Test Network Tools nonAccess Controls Buttons
    Click Element   //*[@id="icon_NetworkTools"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="cancelButton"]

Test Network Tools Ping or Trace an IP Address
    GUI::Basic::Japanese::Test Translation      //*[@id="titlepingtrace"]
    GUI::Basic::Japanese::Test Translation      //*[@id="networktools"]/div[2]/div[1]/div/div[2]/label
    GUI::Basic::Japanese::Test Translation      //*[@id="networktools"]/div[2]/div[1]/div/input[1]
    GUI::Basic::Japanese::Test Translation      //*[@id="networktools"]/div[2]/div[1]/div/input[2]

Test Network Tools Perform a DNS Lookup
    GUI::Basic::Japanese::Test Translation      //*[@id="titlelookup"]
    GUI::Basic::Japanese::Test Translation      //*[@id="networktools"]/div[2]/div[2]/div/div[2]/label
    GUI::Basic::Japanese::Test Translation      //*[@id="networktools"]/div[2]/div[2]/div/input

Test Network Tools Command Output
    GUI::Basic::Japanese::Test Translation      //*[@id="networktools"]/div[3]/div/div/div[1]/div[1]
    Click Element   //*[@id="cancelButton"]


*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[3]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(4)
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers