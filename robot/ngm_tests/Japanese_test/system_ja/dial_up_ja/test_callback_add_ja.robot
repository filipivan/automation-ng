*** Settings ***
Documentation    Testing if Elements in the System ->  Dial Up -> CallBack Users Add are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL


*** Test Cases ***
Test CallBack Users nonAccessControls
    Click Element   //*[@id="dialupCallback"]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   //*[@id="addButton"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="cancelButton"]
    GUI::Basic::Japanese::Test Translation      //*[@id="dialupCbAdd_form"]/div[2]/div/div/div[1]
    GUI::Basic::Japanese::Test Translation      //*[@id="dialupCbAdd_form"]/div[2]/div/div/div[2]

Test Textboxes
    Input Text      jquery=#dialupCbNumber      yes
    GUI::Auditing::Auto Input Tests     dialupCbUser    \#inv   notv@lid    a/a     valid
    GUI::Basic::Spinner Should Be Invisible
    gui::auditing::auto input tests     dialupCbNumber  12.12.12.12         (232)2322323    9999999999
    GUI::Basic::Spinner Should Be Invisible
    Select Checkbox     xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[1]/input
    Click Element       jquery=#delButton
    GUI::Basic::Spinner Should Be Invisible



*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[3]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(7)
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers