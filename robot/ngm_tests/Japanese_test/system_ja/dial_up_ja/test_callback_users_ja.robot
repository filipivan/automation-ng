*** Settings ***
Documentation    Testing if Elements in the System ->  Dial Up -> CallBack Users are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL


*** Test Cases ***
Test CallBack Users nonAccessControls
    Click Element   //*[@id="dialupCallback"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation      //*[@id="dialupCallback"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="addButton"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="delButton"]

Test Custom Fields Table
    GUI::Basic::Japanese::Test Translation Table Header With Checkbox   3

*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[3]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(7)
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers