*** Settings ***
Documentation    Testing if Elements in the System ->  Dial Up -> Services are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL


*** Test Cases ***
Test Services nonAccessControls
    GUI::Basic::Japanese::Test Translation      //*[@id="dialupSettings"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]

Test Services Drop Down Menu
    GUI::Basic::Japanese::Test Translation      //*[@id="dialupSettings_form"]/div[2]/div/div/div[1]/label
    GUI::Basic::Japanese::Test Translation      //*[@id="dialupSettings_form"]/div[2]/div/div/div[2]/label

    : FOR    ${INDEX}    IN RANGE    1    4
    \    GUI::Basic::Japanese::Test Translation      //*[@id="dialupSetLogin"]/option[${INDEX}]

    : FOR    ${INDEX}    IN RANGE    1    4
    \    GUI::Basic::Japanese::Test Translation      //*[@id="dialupSetPPP"]/option[${INDEX}]


*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[3]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(7)
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers