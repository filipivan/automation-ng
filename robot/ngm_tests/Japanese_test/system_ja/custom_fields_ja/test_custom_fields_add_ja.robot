*** Settings ***
Documentation    Testing if Elements in the System ->  Custom Fields add Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL


*** Test Cases ***
Test Custom Fields Add Tab
    Click Element  //*[@id="addButton"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="cancelButton"]
    GUI::Basic::Japanese::Test Translation      //*[@id="addCustomFields_Form"]/div[2]/div/div/div[1]
    GUI::Basic::Japanese::Test Translation      //*[@id="addCustomFields_Form"]/div[2]/div/div/div[2]

Test Custom Fields Add Too Long Name Error
    Input Text	//*[@id="custom_name"]	1234567890123456789012345678901234567890123456789012345678901234
    GUI::Basic::Save
    GUI::Basic::Japanese::Test Translation      //*[@id="errormsg"]

Test Custom Fields Add Too Long Value Error
    Input Text	//*[@id="custom_name"]	Dummy
    Input Text	//*[@id="custom_value"]	1234567890123456789012345678901234567890123456789012345678901234
    GUI::Basic::Save
    GUI::Basic::Japanese::Test Translation      //*[@id="errormsg"]
    GUI::Basic::Cancel

Test insert custom field and check the insertion
    GUI::Open System tab
	GUI::System::Open Custom Fields tab
	GUI::System::Custom Fields::Add Custom Field	test test	val val
	GUI::System::Custom Fields::Add::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::System::Custom Fields::Add Custom Field	test2 test2	val val
	GUI::System::Custom Fields::Add::Save
	GUI::Basic::Spinner Should Be Invisible
	GUI::System::Custom Fields::Add Custom Field	test3 test3	val val
	GUI::System::Custom Fields::Add::Save
	GUI::Basic::Spinner Should Be Invisible

	GUI::Basic::Check Rows In Table	CustomFieldsForm	3

Test editing one field
	GUI::Basic::Edit Rows In Table	\#CustomFieldsForm	test test
	Input Text	//*[@id="custom_value"]	custom_value
	GUI::Basic::Save

	GUI::Basic::Check Rows In Table	CustomFieldsForm	3

Test multi edit custom fields
 	# select all fields
 	Select Checkbox	jquery=#CustomFieldsForm >thead input[type=checkbox]
	GUI::Basic::Click Element	//*[@id='editButton']
 	GUI::Basic::Spinner Should Be Invisible

 	Wait Until Element Is Visible	jquery=#custom_value
	GUI::Basic::Input Text	//*[@id='custom_value']	foo foo
 	GUI::System::Custom Fields::Add::Save
 	GUI::Basic::Spinner Should Be Invisible

	Wait Until Element Is Visible	jquery=#CustomFieldsForm
	GUI::Basic::Table Should Not Contain	table	val val

	Click Element   xpath=//*[@id="thead"]/tr/th[1]/input
	Click Element   jquery=#delButton
	GUI::Basic::Spinner Should Be Invisible


*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[3]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(6)
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers