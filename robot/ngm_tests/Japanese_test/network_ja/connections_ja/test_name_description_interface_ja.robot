*** Settings ***
Documentation    Testing the Host and DNS options for the Settings tab
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup     Suite Setup
Suite Teardown  Run Keywords    GUI::Basic::Logout      Close all Browsers
Force Tags         GUI     ${BROWSER}     ${SYSTEM}       ${MODEL}        ${VERSION}    WINDOWS     CHROME  FIREFOX   EDGE     IE

*** Test Cases ***

Add Network Connection
    Element Should Be Visible       jquery=#addButton
    Click Element                   jquery=#addButton
    GUI::Basic::Spinner Should Be Invisible
    Input Text                      jquery=#ipv4dns     .--.--.--.9
    Click Element                   jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain Element     //*[@id="globalerr"]/div/div/p
    Page Should Contain Element     jquery=#description

Test Network Name
    @{TESTS}=   Create List             n\\o    9alsonot    \#24network     ..no..  .//./\\##   validname
    GUI::Auditing::Auto Input Tests     connName    @{TESTS}

Test Connection Type Dropdown List
    #@{CHK}=     Create List             Bonding     Ethernet    Mobile Broadband GSM    VLAN    WiFi    Bridge      Analog MODEM
    @{LST}=     Get List Items          jquery=#connType
    :FOR    ${I}    IN      @{LST}
    \   run keyword and continue on failure     Should Contain  ${I}    [       msg=${I} is not translated

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Basic::Spinner Should Be Invisible
    GUI::Network::Open Connections Tab