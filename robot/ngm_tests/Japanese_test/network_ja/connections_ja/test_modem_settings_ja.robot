*** Settings ***
Documentation    Testing the Host and DNS options for the Settings tab
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup     Suite Setup
Suite Teardown  Run Keywords    GUI::Basic::Logout      Close all Browsers
Force Tags         GUI     ${BROWSER}     ${SYSTEM}       ${MODEL}        ${VERSION}    WINDOWS     CHROME  FIREFOX   EDGE     IE

*** Test Cases ***
Select Modem Type
    Select From List By Value       jquery=#connType        generic
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be Visible   jquery=#pppTitle

Test Modem Translation
    gui::basic::check translation no id previous sibling    pppStatus   pppModem    pppSpeed    pppPhoneNumber
    ...     pppInitChat     pppIdleTmo
    GUI::Basic::Japanese::Test Translation      jquery=#pppTitle

Test Status Dropdown
    @{TXT}=     Get List Items  jquery=#pppStatus
    :FOR    ${I}    IN      @{TXT}
    \   Should Contain  ${I}    [

Test Device Name
    @{TESTS}=       Create List     ..test  .test   3test   te'st   te[]st  te,st   te.st
    GUI::Auditing::Auto Input Tests     pppModem    @{TESTS}

Test Speed Dropdown
    @{TXT}=     Get List Items  jquery=#pppSpeed
    :FOR    ${I}    IN      @{TXT}
    \   Should Contain  ${I}    [

Test Phone Number
    @{TESTS}=       Create List     555-555-5555    phoneNumber     (432)341-1245   +5524521245     9999999999
    GUI::Auditing::Auto Input Tests     pppPhoneNumber      @{TESTS}

Test Chat Presence
    Element Should Be Visible       jquery=#pppInitChat

Test Timeout
    @{TESTS}=       Create List     11111111111    string  -5524  12345678
    GUI::Auditing::Auto Input Tests     pppIdleTmo      @{TESTS}

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Basic::Spinner Should Be Invisible
    GUI::Network::Open Connections Tab
    Element Should Be Visible       jquery=#addButton
    Click Element                   jquery=#addButton
    GUI::Basic::Spinner Should Be Invisible
    Input Text                      jquery=#connName    9.'./placeholder
    Click Element                   jquery=#saveButton
