*** Settings ***
Documentation    Testing the Host and DNS options for the Settings tab
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup     Suite Setup
Suite Teardown  Run Keywords    GUI::Basic::Logout      Close all Browsers
Force Tags         GUI     ${BROWSER}     ${SYSTEM}       ${MODEL}        ${VERSION}    WINDOWS     CHROME  FIREFOX   EDGE     IE

*** Test Cases ***

Select Bridge Type
    Select From List By Value       jquery=#connType    bridge
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be Visible   jquery=#bridgeTitle

Check Bridge Interfaces Presence
    Element Should Be Visible       jquery=#bridgeSlave

Test Bridge Hello
    @{TESTS}=   Create List         es  1-  d4  ae;     99999999    12.21.21.12     11  0   9
    GUI::Auditing::Auto Input Tests     bridgeHelloTime     @{TESTS}

Test Bridge Forward
    @{TESTS}=   Create List         es  1-  d4  ae;     99999999    12.21.21.12     31  3   9
    GUI::Auditing::Auto Input Tests     bridgeForwardDelay     @{TESTS}

Test Bridge Max
    @{TESTS}=   Create List         es  1-  d4  ae;     99999999    12.21.21.12     41  5   9
    GUI::Auditing::Auto Input Tests     bridgeMaxAge        @{TESTS}

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Basic::Spinner Should Be Invisible
    GUI::Network::Open Connections Tab
    Element Should Be Visible       jquery=#addButton
    Click Element                   jquery=#addButton
    GUI::Basic::Spinner Should Be Invisible
    Input Text                      jquery=#connName    9.'./placeholder
    Click Element                   jquery=#saveButton