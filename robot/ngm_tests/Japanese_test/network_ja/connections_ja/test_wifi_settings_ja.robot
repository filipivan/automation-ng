*** Settings ***
Documentation    Testing the Host and DNS options for the Settings tab
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup     Suite Setup
Suite Teardown  Run Keywords    GUI::Basic::Logout      Close all Browsers
Force Tags         GUI     ${BROWSER}     ${SYSTEM}       ${MODEL}        ${VERSION}    WINDOWS     CHROME  FIREFOX   EDGE     IE

*** Test Cases ***

Select Wifi Type
    Select From List By Value       jquery=#connType    wifi
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be Visible   jquery=#wifiTitle

Verify SSID is Present
    Element Should Be Visible       jquery=#wifiSsid

Verify BSSID is Present
    Element Should Be Visible       jquery=#wifiBssid

Verify Security Disabled
    Select Radio Button             wifiSec             none
    Element Should Not Be Visible   jquery=#wifiSecPsk

Veridy Security Enabled
    Select Radio Button             wifiSec             wpa-psk
    Element Should Be Visible       jquery=#wifiSecPsk

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Basic::Spinner Should Be Invisible
    GUI::Network::Open Connections Tab
    Element Should Be Visible       jquery=#addButton
    Click Element                   jquery=#addButton
    GUI::Basic::Spinner Should Be Invisible
    Input Text                      jquery=#connName    9.'./placeholder
    Click Element                   jquery=#saveButton
