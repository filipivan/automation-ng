*** Settings ***
Documentation    Testing if Elements in the Network -> Connections Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL


*** Test Cases ***
Test Connections Add
    Click Element   //*[@id="addButton"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="cancelButton"]

Test Left Side With Ethernet
    : FOR    ${INDEX}    IN RANGE    1    8
    \    GUI::Basic::Japanese::Test Translation    //*[@id="netmngConnAddForm"]/div[2]/div[1]/div/div[${INDEX}]

    : FOR    ${INDEX}    IN RANGE    1    8
    \    GUI::Basic::Japanese::Test Translation    //*[@id="connType"]/option[${INDEX}]

    GUI::Basic::Japanese::Test Translation    //*[@id="connIeth"]/option

Test Left Side With Bonding
    Click Element   //*[@id="connType"]
    Click Element   //*[@id="connType"]/option[1]

    GUI::Basic::Japanese::Test Translation    //*[@id="bondTitle"]
    : FOR    ${INDEX}    IN RANGE    1    11
    \    GUI::Basic::Japanese::Test Translation    //*[@id="netmngConnAddForm"]/div[2]/div[1]/div/div[10]/div[2]/div/div/div[${INDEX}]

    GUI::Basic::Japanese::Test Translation    //*[@id="bondPrimary"]/option
    GUI::Basic::Japanese::Test Translation    //*[@id="bondSlave"]/option
    GUI::Basic::Japanese::Test Translation    //*[@id="bondMode"]/option[1]
    GUI::Basic::Japanese::Test Translation    //*[@id="bondMode"]/option[2]
    GUI::Basic::Japanese::Test Translation    //*[@id="bondMonitor"]/option[1]
    GUI::Basic::Japanese::Test Translation    //*[@id="bondMonitor"]/option[2]

    : FOR    ${I}    IN RANGE    1    5
    \    GUI::Basic::Japanese::Test Translation    //*[@id="bondArpval"]/option[${I}]

    : FOR    ${I}    IN RANGE    1    4
    \    GUI::Basic::Japanese::Test Translation    //*[@id="bondFailMac"]/option[${I}]

Test Left Side With Mobile Broadband GSM
    Click Element   //*[@id="connType"]
    Click Element   //*[@id="connType"]/option[3]

    GUI::Basic::Japanese::Test Translation    //*[@id="mobileTitle"]
    : FOR    ${I}    IN RANGE    1    7
    \    GUI::Basic::Japanese::Test Translation    //*[@id="netmngConnAddForm"]/div[2]/div[1]/div/div[8]/div[2]/div/div/div[${I}]

    Select Checkbox     //*[@id="mobileDualSim"]
    : FOR    ${I}    IN RANGE    1    7
    \    GUI::Basic::Japanese::Test Translation    //*[@id="netmngConnAddForm"]/div[2]/div[1]/div/div[8]/div[2]/div/div/div[6]/div[2]/div[${I}]

Test Left Side With VLAN
    Click Element   //*[@id="connType"]
    Click Element   //*[@id="connType"]/option[4]
    GUI::Basic::Japanese::Test Translation    //*[@id="vlanTitle"]
    GUI::Basic::Japanese::Test Translation    //*[@id="netmngConnAddForm"]/div[2]/div[1]/div/div[9]/div[2]/div/div/div/label

Test Left Side With Wifi
    Click Element   //*[@id="connType"]
    Click Element   //*[@id="connType"]/option[5]
    GUI::Basic::Japanese::Test Translation    //*[@id="wifiTitle"]
    : FOR    ${I}    IN RANGE    1    5
    \    GUI::Basic::Japanese::Test Translation    //*[@id="netmngConnAddForm"]/div[2]/div[1]/div/div[11]/div[2]/div/div/div[${I}]
    Select Radio Button     wifiSec     wpa-psk

    : FOR    ${I}    IN RANGE    1    4
    \    GUI::Basic::Japanese::Test Translation    //*[@id="netmngConnAddForm"]/div[2]/div[1]/div/div[11]/div[2]/div/div/div[4]/div/div[${I}]

Test Left Side With Bridge Connection
    Click Element   //*[@id="connType"]
    Click Element   //*[@id="connType"]/option[6]
    GUI::Basic::Japanese::Test Translation    //*[@id="bridgeTitle"]
    : FOR    ${I}    IN RANGE    1    6
    \    GUI::Basic::Japanese::Test Translation    //*[@id="netmngConnAddForm"]/div[2]/div[1]/div/div[12]/div[2]/div/div/div[${I}]

Test Right Side Of Page For Bonding - Bridge
    : FOR    ${INDEX}    IN RANGE    1    7
    \    GUI::Basic::Japanese::Test Translation    //*[@id="netmngConnAddForm"]/div[2]/div[2]/div/div[${INDEX}]

    Select Radio Button     method      manual
    Select Radio Button     method6     manual

    : FOR    ${INDEX}    IN RANGE    1    7
    \    GUI::Basic::Japanese::Test Translation    //*[@id="netmngConnAddForm"]/div[2]/div[2]/div/div[1]/div/div[${INDEX}]
    : FOR    ${INDEX}    IN RANGE    1    8
    \    GUI::Basic::Japanese::Test Translation    //*[@id="netmngConnAddForm"]/div[2]/div[2]/div/div[4]/div/div[${INDEX}]


Test Left Side With Analog MODEM / PPP Connection
    Click Element   //*[@id="connType"]
    Click Element   //*[@id="connType"]/option[7]
    GUI::Basic::Japanese::Test Translation    //*[@id="pppTitle"]
    : FOR    ${I}    IN RANGE    1    7
    \    GUI::Basic::Japanese::Test Translation    //*[@id="netmngConnAddForm"]/div[2]/div[1]/div/div[13]/div[2]/div/div/div[${I}]

    GUI::Basic::Japanese::Test Translation    //*[@id="pppStatus"]/option[1]
    GUI::Basic::Japanese::Test Translation    //*[@id="pppStatus"]/option[2]

Test Right Side Of Page With Analog MODEM / PPP Connection
    : FOR    ${I}    IN RANGE    7    10
    \    GUI::Basic::Japanese::Test Translation    //*[@id="netmngConnAddForm"]/div[2]/div[2]/div/div[9]

    Select Radio Button     pppIP4type     local
    : FOR    ${I}    IN RANGE    1    6
    \    GUI::Basic::Japanese::Test Translation     //*[@id="netmngConnAddForm"]/div[2]/div[2]/div/div[7]/div/div[${I}]

    Select Radio Button     pppIP6type     local
    : FOR    ${I}    IN RANGE    1    6
    \    GUI::Basic::Japanese::Test Translation     //*[@id="netmngConnAddForm"]/div[2]/div[2]/div/div[8]/div/div[${I}]

    Select Radio Button     pppAuthType     local
    : FOR    ${I}    IN RANGE    1    5
    \    GUI::Basic::Japanese::Test Translation     //*[@id="netmngConnAddForm"]/div[2]/div[2]/div/div[9]/div/div[${I}]
    : FOR    ${I}    IN RANGE    1    4
    \    GUI::Basic::Japanese::Test Translation     //*[@id="pppAuthProto"]/option[${I}]

    Select Radio Button     pppAuthType     remote
    GUI::Basic::Japanese::Test Translation     //*[@id="netmngConnAddForm"]/div[2]/div[2]/div/div[9]/div/div[5]
    GUI::Basic::Japanese::Test Translation     //*[@id="netmngConnAddForm"]/div[2]/div[2]/div/div[9]/div/div[6]


*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[4]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(2)
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers