*** Settings ***
Documentation    Testing the ip and ppp for connections
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup     Suite Setup
Suite Teardown  Run Keywords    GUI::Basic::Logout      Close all Browsers
Force Tags         GUI     ${BROWSER}     ${SYSTEM}       ${MODEL}        ${VERSION}    WINDOWS     CHROME  FIREFOX   EDGE     IE

*** Test Cases ***

Select Analog MODEM Type
    Select From List By Value       jquery=#connType    generic
    GUI::Basic::Spinner Should Be Invisible
    Element Should Not Be Visible   jquery=#connIeth

Test PPP IPv4 Settings
    test ppp ip settings  4

Test PPP IPv6 Settings
    test ppp ip settings  6

Test PPP Auth Settings
    Select Radio Button         pppAuthType     none
    GUI::Basic::Spinner Should Be Invisible
    Element Should Not Be Visible           jquery=#pppAuthProto
    Select Radio Button         pppAuthType     local
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be Visible               jquery=#pppAuthProto
    @{TXT}=     Get List Items  jquery=#pppAuthProto
    :FOR    ${I}    IN      @{TXT}
    \   Should Contain  ${I}    [
    Select Radio Button         pppAuthType     remote
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be Visible               jquery=#pppAuthUser
    Element Should Be Visible               jquery=#pppAuthPasswd

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Basic::Spinner Should Be Invisible
    GUI::Network::Open Connections Tab
    Element Should Be Visible       jquery=#addButton
    Click Element                   jquery=#addButton
    GUI::Basic::Spinner Should Be Invisible

Test PPP IP Settings
    [Arguments]     ${FOS}
    [Documentation]  FOS is 'four or six' its expecting either 4 or 6
    @{LST}=     Run Keyword If      ${FOS}==4     Create List     1..245.2.1  .   12.53.12.53/2   12/25\\12#24     12.12.12.12
    ...         ELSE        Create List     20f3::245f      32:::425v   002c::002c  2d4f:2352:2453::2ddc    ::002c
    Select Radio Button     pppIP${FOS}type     no
    GUI::Basic::Spinner Should Be Invisible
    Element Should Not Be Visible           jquery=#pppIP${FOS}localaddr
    Select Radio Button     pppIP${FOS}type     local
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be Visible               jquery=#pppIP${FOS}localaddr
    GUI::Auditing::Auto Input Tests         pppIP${FOS}localaddr    @{LST}
    GUI::Auditing::Auto Input Tests         pppIP${FOS}remaddr      @{LST}
    Select Radio Button     pppIP${FOS}type     remote
    GUI::Basic::Spinner Should Be Invisible
    Element Should Not Be Visible           jquery=#pppIP${FOS}localaddr
