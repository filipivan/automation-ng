*** Settings ***
Documentation    Testing the Host and DNS options for the Settings tab
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup     Suite Setup
Suite Teardown  Run Keywords    GUI::Basic::Logout      Close all Browsers
Force Tags         GUI     ${BROWSER}     ${SYSTEM}       ${MODEL}        ${VERSION}    WINDOWS     CHROME  FIREFOX   EDGE     IE

*** Test Cases ***

Select Broadband Type
    Select From List By Value       jquery=#connType    gsm
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be Visible   jquery=#mobileTitle

Test SIM1
    Test GSM

Test SIM2
    Select Checkbox     jquery=#mobileDualSim
    Wait Until Element Is Visible       jquery=#mobileActiveSim
    Test GSM    2
    @{CHK}=     Create List     1   2
    @{ACTV}=    Get List Items  jquery=#mobileActiveSim
    Should Be Equal     ${CHK}  ${ACTV}

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Basic::Spinner Should Be Invisible
    GUI::Network::Open Connections Tab
    Element Should Be Visible       jquery=#addButton
    Click Element                   jquery=#addButton
    GUI::Basic::Spinner Should Be Invisible
    Input Text                      jquery=#connName    9.'./placeholder
    Click Element                   jquery=#saveButton

Test GSM
    [Arguments]     ${VER}=${EMPTY}
    @{NUMT}=    Create List       124-13  .3112.1     2.23    12;124  124\\124    24-124  12
    GUI::Auditing::Auto Input Tests     mobilePhonen${VER}      @{NUMT}
    GUI::Auditing::Auto Input Tests     mobilePIN${VER}         @{NUMT}
    Element Should Be Visible           jquery=#mobileUser${VER}
    Element Should Be Visible           jquery=#mobilePasswd${VER}
    Element Should Be Visible           jquery=#mobileAPN${VER}


    #Execute Javascript  return  document.getElementById("${LOCATOR}").parentNode.nextSibling.id
