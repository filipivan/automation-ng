*** Settings ***
Documentation    Testing the Host and DNS options for the Settings tab
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup     Suite Setup
Suite Teardown  Run Keywords    GUI::Basic::Logout      Close all Browsers
Force Tags         GUI     ${BROWSER}     ${SYSTEM}       ${MODEL}        ${VERSION}    WINDOWS     CHROME  FIREFOX   EDGE     IE

*** Test Cases ***

Test IPv4 DNS
    @{TEST}=    Create List     1..245.2.1  .   12.53.12.53/2   12/25\\12#24     12.12.12.12
    @{MSK}=     Create List     [34     /24     ..1     \\12    25
    Select Radio Button     method     disabled
    GUI::Basic::Spinner Should Be Invisible
    Element Should Not Be Visible       jquery=#address
    Select Radio Button     method     auto
    GUI::Basic::Spinner Should Be Invisible
    Element Should Not Be Visible       jquery=#address
    Select Radio Button     method     manual
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be Visible       jquery=#address
    GUI::Auditing::Auto Input Tests     address     @{TEST}
    GUI::Auditing::Auto Input Tests     netmask     @{MSK}
    GUI::Auditing::Auto Input Tests     gateway     @{TEST}
    GUI::Auditing::Auto Input Tests     ipv4dns     @{TEST}
    Element Should Be Visible       jquery=#ipv4dnsSearch

Test IPv6 DNS
    @{TEST}=    Create List     20f::3::245f      32:::425v   002g::002c  2d4f:2352d:2453::2ddc    002c::002c
    @{MSK}=     Create List     [34     /24     ..1     \\12    25
    Select Radio Button     method6     ignore
    GUI::Basic::Spinner Should Be Invisible
    Element Should Not Be Visible       jquery=#address6
    Select Radio Button     method6     auto
    GUI::Basic::Spinner Should Be Invisible
    Element Should Not Be Visible       jquery=#address6
    Select Radio Button     method6     dhcp
    GUI::Basic::Spinner Should Be Invisible
    Element Should Not Be Visible       jquery=#address6
    Select Radio Button     method6     manual
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be Visible       jquery=#address6
    GUI::Auditing::Auto Input Tests     address6     @{TEST}
    GUI::Auditing::Auto Input Tests     netmask6     @{MSK}
    GUI::Auditing::Auto Input Tests     gateway6     @{TEST}
    GUI::Auditing::Auto Input Tests     ipv6dns     @{TEST}
    Element Should Be Visible       jquery=#ipv6dnsSearch


*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Basic::Spinner Should Be Invisible
    GUI::Network::Open Connections Tab
    Element Should Be Visible       jquery=#addButton
    Click Element                   jquery=#addButton
    GUI::Basic::Spinner Should Be Invisible
    Input Text                      jquery=#connName    9.'./placeholder
    Click Element                   jquery=#saveButton