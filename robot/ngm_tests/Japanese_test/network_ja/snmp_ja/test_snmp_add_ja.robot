*** Settings ***
Documentation    Testing if Elements in the Network -> SNMP Add Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL


*** Test Cases ***
Test SNMP Add nonAccess Controls Buttons
    Click Element   //*[@id="addButton"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="cancelButton"]

Test SNMP Add Elements V1/V2
    GUI::Basic::Japanese::Test Translation    //*[@id="addSNMP_Form"]/div[2]/div[1]/div/div/label
    : FOR    ${INDEX}    IN RANGE    1    6
    \    GUI::Basic::Japanese::Test Translation    //*[@id="addSNMP_Form"]/div[2]/div[1]/div/div/div/div[${INDEX}]
    Input Text  jquery=#oid     !@#$
    GUI::Auditing::Auto Input Tests     name    ${EMPTY}    ////    !@#$    \\  valid
    GUI::Auditing::Auto Input Tests     source  ////    !@#$    \\  valid

Test SNMP Add Elements V3
    Select Radio Button     snmpVersion     user
    : FOR    ${INDEX}    IN RANGE    6    12
    \    GUI::Basic::Japanese::Test Translation    //*[@id="addSNMP_Form"]/div[2]/div[1]/div/div/div/div[${INDEX}]

    GUI::Auditing::Auto Input Tests     user_name    ${EMPTY}    ////    !@#$    \\  valid
    Input Text  jquery=#user_name     hi
    GUI::Basic::Japanese::Test Translation      //*[@id="authLevel"]/option[1]
    GUI::Basic::Japanese::Test Translation      //*[@id="authLevel"]/option[2]
    GUI::Basic::Japanese::Test Translation      //*[@id="authLevel"]/option[3]
    GUI::Basic::Japanese::Test Translation      //*[@id="authType"]/option[1]
    GUI::Basic::Japanese::Test Translation      //*[@id="authType"]/option[2]
    GUI::Basic::Japanese::Test Translation      //*[@id="privType"]/option[1]
    GUI::Basic::Japanese::Test Translation      //*[@id="privType"]/option[2]

Tests SNMP Add Elements OID
    GUI::Basic::Japanese::Test Translation      //*[@id="addSNMP_Form"]/div[2]/div[2]/div/div[1]
    GUI::Basic::Japanese::Test Translation      //*[@id="addSNMP_Form"]/div[2]/div[2]/div/div[2]
    GUI::Basic::Japanese::Test Translation      //*[@id="permission"]/option[1]
    GUI::Basic::Japanese::Test Translation      //*[@id="permission"]/option[2]
    GUI::Auditing::Auto Input Tests     oid     !@#$    string      67   1.3.6.1.4.1.343
    Click Element       //*[@id="user|hi"]/td[1]/input
    GUI::Basic::Delete


*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[4]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(5)
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers