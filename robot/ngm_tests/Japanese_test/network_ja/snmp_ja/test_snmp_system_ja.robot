*** Settings ***
Documentation    Testing if Elements in the Network -> SNMP System Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL

*** Variables ***
${Emp}

*** Test Cases ***
Test SNMP System nonAccess Controls Buttons
    Click Element   //*[@id="nonAccessControls"]/input[3]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="cancelButton"]

Test SNMP Sysytem Elements
    GUI::Basic::Japanese::Test Translation      //*[@id="sysTitle"]
    : FOR    ${INDEX}    IN RANGE    1    5
    \    GUI::Basic::Japanese::Test Translation    //*[@id="systemForm"]/div[2]/div/div/div/div[2]/div/div/div[${INDEX}]

Test SNMP System No SysContact
    Input Text        //*[@id="sys_contact"]        ${Emp}
    GUI::Basic::Save
    GUI::Basic::Japanese::Test Translation      //*[@id="errormsg"]

Test SNMP System No SysLocation
    Input Text        //*[@id="sys_location"]        ${Emp}
    GUI::Basic::Save
    GUI::Basic::Japanese::Test Translation      //*[@id="errormsg"]
    GUI::Basic::Cancel

*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[4]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(5)
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers