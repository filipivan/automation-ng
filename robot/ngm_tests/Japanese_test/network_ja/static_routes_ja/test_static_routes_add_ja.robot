*** Settings ***
Documentation    Testing if Elements in the Network -> Static Routes Add Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL


*** Test Cases ***
Test Static Routes Add
    Click Element   //*[@id="addButton"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="cancelButton"]

Test Static Routes Add Elements
    GUI::Basic::Japanese::Test Translation    //*[@id="dest"]/div[2]/div[1]/div/div[1]
    GUI::Basic::Japanese::Test Translation    //*[@id="dest"]/div[2]/div[1]/div/div[2]

    : FOR    ${INDEX}    IN RANGE    1      5
    \    GUI::Basic::Japanese::Test Translation    //*[@id="dest"]/div[2]/div[2]/div/div[${INDEX}]

    : FOR    ${INDEX}    IN RANGE    1      4
    \    GUI::Basic::Japanese::Test Translation    //*[@id="AddconnName"]/option[${INDEX}]

    GUI::Basic::Japanese::Test Translation    //*[@id="dest"]/div[2]/div[1]/div/div[2]/div/div[1]
    GUI::Basic::Japanese::Test Translation    //*[@id="dest"]/div[2]/div[1]/div/div[2]/div/div[2]

Test IPv4 Settings
    Select Radio Button     type    ipv4

Test IPv4 Destination IP
    @{TEST}=    Create List     12..1244    267.266.267.267     12.35.23.124/24     12.12.12.12
    GUI::Auditing::Auto Input Tests     destIp      @{TEST}

Check Bitmask
    Element Should Be Visible       destMask
    @{MSK}=     Create List     [34     /24     ..1     \\12    25
    GUI::Auditing::Auto Input Tests     destMask     @{MSK}
    Select Checkbox     //*[@id="ETH0|ipv4|route1"]/td[1]/input
    GUI::Basic::Delete
    GUI::Basic::Add

Test IPv4 Gateway IP
    @{TEST}=    Create List     12..1244    267.266.267.267     12.35.23.124/24     12.12.12.12
    GUI::Auditing::Auto Input Tests     gateway      @{TEST}

Test IPv4 Metric
    Input Text      destIp      12..1244
    @{TEST}=    Create List     test    12.12   1..11   12/12   12345
    GUI::Auditing::Auto Input Tests     metric      @{TEST}

Test IPv6 Settings
    Select Radio Button     type    ipv6

Test IPv6 Destination IP
    @{TEST}=    Create List     00cd::2314::234c    267.266.267.267     12.35.23.124/24     12.12.12.12     2341::dfga  002c::002c
    GUI::Auditing::Auto Input Tests     destIp      @{TEST}

Test IPv6 Gateway IP
    @{TEST}=    Create List     00cd::2314::234c    267.266.267.267     12.35.23.124/24     12.12.12.12     2341::dfga  002c::002c
    GUI::Auditing::Auto Input Tests     gateway      @{TEST}

Test IPv6 Metric
    @{TEST}=    Create List     test    12.12   1..11   12/12   12345
    GUI::Auditing::Auto Input Tests     metric      @{TEST}


*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[4]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(3)
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers