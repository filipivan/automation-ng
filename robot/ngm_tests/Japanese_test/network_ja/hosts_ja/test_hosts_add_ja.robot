*** Settings ***
Documentation    Testing if Elements in the Network -> Hosts Add Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL


*** Test Cases ***
Test Hosts Add nonAccess Controls Buttons
    Click Element   //*[@id="addButton"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="cancelButton"]

Test Hosts Add Elements
    : FOR    ${INDEX}    IN RANGE    1    4
    \    GUI::Basic::Japanese::Test Translation    //*[@id="addHost_Form"]/div[2]/div/div/div[${INDEX}]

Test Fields
    gui::auditing::auto input tests     hostip      not.valid.ip.add    345.345.345.345     234.234.234.234
    gui::auditing::auto input tests     name        n[]name     not/valid   @name   valid.name
    GUI::Basic::Spinner Should Be Invisible
    click Element                       xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[2]/a
    GUI::Basic::Spinner Should Be Invisible
    gui::auditing::auto input tests     alias       n[]name     not/valid   @name   valid.name
    gui::basic::spinner should be invisible
    click element                       xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[1]/input
    click element                       jquery=#delButton
    GUI::Basic::Spinner Should Be Invisible

*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[4]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(4)
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers