*** Settings ***
Documentation    Testing if Elements in the Network -> Settings Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL


*** Test Cases ***
Test Network Settings Save Button
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]

Test Network Settings Host and DNS
    GUI::Basic::Japanese::Test Translation      //*[@id="host_dns"]
    : FOR    ${INDEX}    IN RANGE    1    5
    \    GUI::Basic::Japanese::Test Translation      //*[@id="bondform"]/div[2]/div[1]/div/div/div[2]/div/div/div[${INDEX}]

Test Network Settings IPv4 and IPv6 Profile
    GUI::Basic::Japanese::Test Translation      //*[@id="Ipv4Ipv6"]
    GUI::Basic::Japanese::Test Translation      //*[@id="bondform"]/div[2]/div[2]/div/div/div[2]/div/div/div[1]/div/label
    GUI::Basic::Japanese::Test Translation      //*[@id="bondform"]/div[2]/div[2]/div/div/div[2]/div/div/div[2]/div/label
    GUI::Basic::Japanese::Test Translation      //*[@id="bondform"]/div[2]/div[2]/div/div/div[2]/div/div/div[3]/label
    GUI::Basic::Japanese::Test Translation      //*[@id="bondform"]/div[2]/div[2]/div/div/div[2]/div/div/div[4]/label
    GUI::Basic::Japanese::Test Translation      //*[@id="bondform"]/div[2]/div[2]/div/div/div[2]/div/div/div[5]/div/label
    GUI::Basic::Japanese::Test Translation      //*[@id="notice"]

Test Network Settings IPv4 and IPv6 Profile Reverse Path Filtering
    : FOR    ${INDEX}    IN RANGE    1    4
    \    GUI::Basic::Japanese::Test Translation      //*[@id="rpfilter"]/option[${INDEX}]

Test Network Settings Enable Network FailOver
    GUI::Basic::Japanese::Test Translation      //*[@id="failoverTitle"]
    GUI::Basic::Japanese::Test Translation      //*[@id="bondform"]/div[3]/div/div/div/div[2]/div/div/div/div[1]/label
    Select Checkbox     //*[@id="failover"]
    : FOR    ${INDEX}    IN RANGE    1    7
    \    GUI::Basic::Japanese::Test Translation      //*[@id="bondform"]/div[3]/div/div/div/div[2]/div/div/div/div[2]/div[${INDEX}]

Test Network Settings Network FailOver Primary Connection
    : FOR    ${I}    IN RANGE    1    4
    \    GUI::Basic::Japanese::Test Translation      //*[@id="primaryConn"]/option[${I}]

Test Network Settings Network FailOver Secondary Connection
    : FOR    ${I}    IN RANGE    1    4
    \    GUI::Basic::Japanese::Test Translation      //*[@id="secondaryConn"]/option[${I}]

Test Network Settings Network FailOver Trigger
    Select Radio Button     trigger     ipaddress
    : FOR    ${I}    IN RANGE    1    4
    \    GUI::Basic::Japanese::Test Translation     //*[@id="bondform"]/div[3]/div/div/div/div[2]/div/div/div/div[2]/div[3]/div/div[${I}]/label

Test Network Settings Enable Dynamic DNS
    GUI::Basic::Japanese::Test Translation      //*[@id="bondform"]/div[3]/div/div/div/div[2]/div/div/div/div[2]/div[7]/div[1]/label
    Select Checkbox     //*[@id="ddns"]
    GUI::Basic::Japanese::Test Translation With Value       //*[@id="ddnsShow"]
    : FOR    ${INDEX}    IN RANGE    1    9
    \    GUI::Basic::Japanese::Test Translation      //*[@id="bondform"]/div[3]/div/div/div/div[2]/div/div/div/div[2]/div[7]/div[2]/div[${INDEX}]

Test Network Settings Dynamic DNS Algorithm
    : FOR    ${I}    IN RANGE    1    7
    \    GUI::Basic::Japanese::Test Translation     //*[@id="ddnsKeyAlg"]/option[${I}]


*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[4]
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers