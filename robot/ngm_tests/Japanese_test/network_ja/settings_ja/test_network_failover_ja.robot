*** Settings ***
Documentation    Testing the Host and DNS options for the Settings tab
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup     Suite Setup
Suite Teardown  suite teardown
Force Tags         GUI     ${BROWSER}     ${SYSTEM}       ${MODEL}        ${VERSION}    WINDOWS     CHROME  FIREFOX   EDGE     IE

*** Test Cases ***

Enable Network FailOver
    ${CHK}=     Run Keyword And Return Status               Checkbox Should Be Selected     jquery=#failover
    Run Keyword Unless      ${CHK}      Select Checkbox     jquery=#failover

Test Network FailOver Trigger IP Address
    ${RAND}=    evaluate    random.randint(1,256)  modules=random
    Select Radio button                 trigger     ipaddress
    Element Should Be Visible           jquery=#address
    @{TESTS}=   Create List             1..245.2.1  .   12.53.12.53/2   12/25\\12#24     12.12.12.${RAND}
    GUI::Auditing::Auto Input Tests     address     @{TESTS}
    Select Radio Button                 trigger     ipv4gw
    Element Should Not Be Visible       jquery=#address

Test Network FailOver Number Retries
    ${RAND}=    evaluate    random.randint(1,256)  modules=random
    @{TESTS}=   Create List             12.2431  .   53/2   12/25\\12#24     12-12  12345678901     aLot      ${RAND}
    GUI::Auditing::Auto Input Tests     fail_retries        @{TESTS}
    GUI::Auditing::Auto Input Tests     success_retries     @{TESTS}
    GUI::Auditing::Auto Input Tests     interval_retries    @{TESTS}

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Network::Open Settings Tab

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers