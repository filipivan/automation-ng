*** Settings ***
Documentation    Testing the Host and DNS options for the Settings tab
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup     Suite Setup
Suite Teardown  suite teardown
Force Tags         GUI     ${BROWSER}     ${SYSTEM}       ${MODEL}        ${VERSION}    WINDOWS     CHROME  FIREFOX   EDGE     IE

*** Test Cases ***

Enable DDNS
    ${CHK}=     Run Keyword And Return Status               Checkbox Should Be Selected     jquery=#failover
    Run Keyword Unless      ${CHK}      Select Checkbox     jquery=#failover
    wait until element is visible   jquery=#ddns
    Select Checkbox     jquery=#ddns

Test DDNS Server Name
    @{TESTS}=   Create List     not/valid  a'm'i'   sec/nonam][   test#works   a\\s\\   no..work    not valid     thvalid
    GUI::Auditing::Auto Input Tests     ddnsMaster  @{TESTS}
    Input text  jquery=#ddnsMaster  thisvalid
    Click element   jquery=#saveButton

Test DDNS TCP Port
    ${RAND}=    evaluate    random.randint(1,256)  modules=random
    @{TESTS}=   Create List     12.4  .   aaaa      1 2     Five!     2\\24     24/1    ${RAND}
    GUI::Auditing::Auto Input Tests     ddnsPort  @{TESTS}

Test DDNS Zone
    @{TESTS}=   Create List     not/valid  a'm'i'   sec/nonam][   test#works   a\\s\\     com
    GUI::Auditing::Auto Input Tests     ddnsZone  @{TESTS}
    Input text  jquery=#ddnsZone  this.valid
    Click element   jquery=#saveButton

Test Failover Host Name
    @{TESTS}=   Create List     not/valid  a'm'i'   sec/nonam][   test#works   a\\s\\   no..work        not valid     thisvalid
    GUI::Auditing::Auto Input Tests     ddnsFailHostname  @{TESTS}
    Input text  jquery=#ddnsFailHostname  thsvalid
    Click element   jquery=#saveButton

Test User Name
    @{TESTS}=   Create List     not/valid  a'm'i'   sec/nonam][   test#works   a\\s\\   no..work        not valid     thisvalid
    GUI::Auditing::Auto Input Tests     ddnsKeyUser  @{TESTS}
    Input text  jquery=#ddnsKeyUser  thsvalid
    Click element   jquery=#saveButton

Test DDNS Key Size
    ${RAND}=    evaluate    random.randint(1,256)  modules=random
    @{TESTS}=   Create List     12.4  .   aaaa     2\\24     24/1       7 8 9    ${RAND}
    GUI::Auditing::Auto Input Tests     ddnsKeySize  @{TESTS}

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Network::Open Settings Tab

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers