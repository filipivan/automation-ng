*** Settings ***
Documentation    Testing the Host and DNS options for the Settings tab
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup     Suite Setup
Suite Teardown  suite teardown
Force Tags         GUI     ${BROWSER}     ${SYSTEM}       ${MODEL}        ${VERSION}    WINDOWS     CHROME  FIREFOX   EDGE     IE

*** Variables ***
${Emp}
@{TESTS}=     ../not..valid    not/valid   st'llnot    not.va/lid   1234567890123456789012345678901234567890123456789012345678901234    valid

*** Test Cases ***

Check Visible Address and Search
    Element Should Be Visible   jquery=#dnsAddress
    Element Should Be Visible   jquery=#dnsSearch

Test Hostname Input Field
    GUI::Auditing::Auto Input Tests         hostname    @{TESTS}
    Input text  jquery=#hostname  ${Emp}
    Click element   jquery=#saveButton
    gui::basic::spinner should be invisible
    GUI::Basic::Japanese::Test Translation      //*[@id="errormsg"]
    Input text  jquery=#hostname  thisvalid
    Click element   jquery=#saveButton
    gui::basic::spinner should be invisible

Test Domain Name Input Field
    GUI::Auditing::Auto Input Tests         domainname    @{TESTS}
    Input text  jquery=#domainname  thisvalid
    Click element   jquery=#saveButton

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Network::Open Settings Tab

Suite Teardown
    Input Text 			jquery=#hostname     nodegrid
	Input Text          jquery=#domainname      localdomain
	GUI::Basic::Save
	GUI::Basic::Logout
    GUI::Basic::Logout
    Close all Browsers