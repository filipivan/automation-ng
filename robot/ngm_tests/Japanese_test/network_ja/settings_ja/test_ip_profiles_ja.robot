*** Settings ***
Documentation    Testing the IPv4 and IPv6 Profiles
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup     Suite Setup
Suite Teardown  Suite Teardown
Force Tags         GUI     ${BROWSER}     ${SYSTEM}       ${MODEL}        ${VERSION}    WINDOWS     CHROME  FIREFOX   EDGE     IE

*** Variables ***



*** Test Cases ***

Test IPv4 Box
    Check Box   ipv4-ipforward      ${FALSE}

Test IPv6 Box
    Check Box   ipv6-forward        ${FALSE}

Test Loopback Address
    ${RAND}=    evaluate    random.randint(1,255)  modules=random
    @{TESTS}=   Create List     165.124.456.123     not./valid  124.35.32.12/124    234]124[12\\12  12.12.12.${RAND}
    GUI::Auditing::Auto Input Tests     loopback4   @{TESTS}

Test Dropdown List
    @{COMPARE}=     Create List     Disabled    Strict Mode     Loose Mode
    @{FILTER}=      Get List Items  jquery=#rpfilter
    Should Be Equal     ${COMPARE}  ${FILTER}

Test Routing Table Box
    Check Box   multiroute           ${TRUE}

Test Notice Visibility
    Element Should Be Visible       jquery=#notice

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Network::Open Settings Tab

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers

Check Box
    [Arguments]  ${LOCATION}    ${CHK}
    [Documentation]  CHK should be true if the checkbox being passed is already checked
    Execute Javascript  document.getElementById("${LOCATION}").scrollIntoView(true);
    Click Element     jquery=#${LOCATION}
    Run Keyword If      ${CHK}==${FALSE}    Checkbox Should Be Selected         jquery=#${LOCATION}
    ...     ELSE    Checkbox Should Not Be Selected         jquery=#${LOCATION}
    Click Element       jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    Execute Javascript  document.getElementById("${LOCATION}").scrollIntoView(true);
    Click Element     jquery=#${LOCATION}
    Run Keyword If      ${CHK}==${FALSE}    Checkbox Should Not Be Selected         jquery=#${LOCATION}
    ...     ELSE    Checkbox Should Be Selected         jquery=#${LOCATION}
    Click Element       jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible