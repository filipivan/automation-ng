*** Settings ***
Documentation    Testing if Elements in the Network -> SSL VPN Server Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL


*** Test Cases ***
Test SSL VPN Server nonAccess Controls Buttons
    Click Element       //*[@id="sslvpnServer"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation             //*[@id="sslvpnServer"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]

Test SSL VPN Server Top Left
    : FOR    ${INDEX}    IN RANGE    1    7
    \    GUI::Basic::Japanese::Test Translation    //*[@id="sslvpnServerForm"]/div[2]/div[1]/div/div[${INDEX}]

    : FOR    ${INDEX}    IN RANGE    1    5
    \    GUI::Basic::Japanese::Test Translation    //*[@id="vpnProto"]/option[${INDEX}]

    GUI::Basic::Japanese::Test Translation    //*[@id="status"]/option[1]
    GUI::Basic::Japanese::Test Translation    //*[@id="status"]/option[2]
    Input Text  jquery=#listenPort  .
    gui::auditing::auto input tests     listenIP    string      345.345.345.345     002c::1234::002c    1.1.1.1
    Input Text  jquery=#listenIP    .
    gui::auditing::auto input tests     listenPort  str     3:1     4-5     6
    gui::auditing::auto input tests     tunnelMtu   str     3:1     4-5     6
    gui::auditing::auto input tests     maxClients  str     3:1     4-5     6


Test SSL VPN Server Top Right
    : FOR    ${INDEX}    IN RANGE    1    5
    \    GUI::Basic::Japanese::Test Translation    //*[@id="authType"]/option[${INDEX}]

    Click Element  //*[@id="authType"]
    Click Element  //*[@id="authType"]/option[2]
    GUI::Basic::Japanese::Test Translation    //*[@id="sslvpnServerForm"]/div[2]/div[2]/div/div[1]
    GUI::Basic::Japanese::Test Translation    //*[@id="sslvpnServerForm"]/div[2]/div[2]/div/div[2]
    Click Element  //*[@id="authType"]
    Click Element  //*[@id="authType"]/option[1]

    : FOR    ${INDEX}    IN RANGE    3    7
    \    GUI::Basic::Japanese::Test Translation    //*[@id="sslvpnServerForm"]/div[2]/div[2]/div/div[${INDEX}]

Test SSL VPN Server Bottom Left
    GUI::Basic::Japanese::Test Translation    //*[@id="sslvpnServerForm"]/div[3]/div[1]/div/div/label

    Select Radio Button     ipAddressing        net
    : FOR    ${INDEX}    IN RANGE    1    4
    \    GUI::Basic::Japanese::Test Translation    //*[@id="sslvpnServerForm"]/div[3]/div[1]/div/div/div/div[${INDEX}]
    gui::auditing::auto input tests  tunnelNetIPv4  string      345.345.345.345     002c::1234::002c    1.1.1.1/255.255.255.0
    gui::auditing::auto input tests  tunnelNetIPv6  string      345.345.345.345     002c::1234::002c    ::1/::1

    Select Radio Button     ipAddressing        p2p
    : FOR    ${INDEX}    IN RANGE    4    7
    \    GUI::Basic::Japanese::Test Translation    //*[@id="sslvpnServerForm"]/div[3]/div[1]/div/div/div/div[${INDEX}]
    gui::auditing::auto input tests  localIP  string      345.345.345.345     002c::1234::002c    192.168.2.1
    gui::auditing::auto input tests  remoteIP  string      345.345.345.345     002c::1234::002c    1.1.1.1

    Select Radio Button     ipAddressing        p2p-ipv6
    : FOR    ${INDEX}    IN RANGE    7    10
    \    GUI::Basic::Japanese::Test Translation    //*[@id="sslvpnServerForm"]/div[3]/div[1]/div/div/div/div[${INDEX}]
    gui::auditing::auto input tests  localIPv6  string      345.345.345.345     002c::1234::002c    ::002c
    gui::auditing::auto input tests  remoteIPv6  string      345.345.345.345     002c::1234::002c    002c::002c

Test SSL VPN Server Bottom Right
    : FOR    ${INDEX}    IN RANGE    1    6
    \    GUI::Basic::Japanese::Test Translation    //*[@id="sslvpnServerForm"]/div[3]/div[2]/div/div[${INDEX}]


*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[4]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(7)
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers