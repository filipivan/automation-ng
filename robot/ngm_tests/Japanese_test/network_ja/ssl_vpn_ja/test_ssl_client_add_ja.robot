*** Settings ***
Documentation    Testing if Elements in the Network -> SSL VPN Client Add Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL

*** Test Cases ***
Test SSL VPN Client Add nonAccess Controls Buttons
    Click Element   //*[@id="addButton"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="cancelButton"]

Test SSL VPN Client Add Elements Left Side
    : FOR    ${INDEX}    IN RANGE    1    8
    \    GUI::Basic::Japanese::Test Translation    //*[@id="sslvpnClientAddForm"]/div[2]/div[1]/div/div[${INDEX}]
    : FOR    ${I}    IN RANGE    1    5
    \    GUI::Basic::Japanese::Test Translation    //*[@id="connName"]/option[${I}]
    GUI::Basic::Japanese::Test Translation    //*[@id="vpnProto"]/option[1]
    GUI::Basic::Japanese::Test Translation    //*[@id="vpnProto"]/option[2]

Test SSL VPN Client Add Elements Password Plus TSL
    : FOR    ${INDEX}    IN RANGE    1    5
    \    GUI::Basic::Japanese::Test Translation    //*[@id="authType"]/option[${INDEX}]
    Click Element       //*[@id="authType"]
    Click Element       //*[@id="authType"]/option[4]
    : FOR    ${I}    IN RANGE    1    11
    \    GUI::Basic::Japanese::Test Translation    //*[@id="sslvpnClientAddForm"]/div[2]/div[2]/div/div[${I}]

Test SSL VPN Client Add Elements Static Key
    Click Element       //*[@id="authType"]
    Click Element       //*[@id="authType"]/option[2]
    : FOR    ${I}    IN RANGE    11    14
    \    GUI::Basic::Japanese::Test Translation    //*[@id="sslvpnClientAddForm"]/div[2]/div[2]/div/div[${I}]

Test SSL VPN Client Add HMAC/Message Digest Alg
    : FOR    ${INDEX}    IN RANGE    1    40
    \    GUI::Basic::Japanese::Test Translation    //*[@id="authHMAC"]/option[${INDEX}]

Test SSL VPN Client Add Cipher Alg
    : FOR    ${INDEX}    IN RANGE    1    88
    \    GUI::Basic::Japanese::Test Translation    //*[@id="authCipher"]/option[${INDEX}]

Test Fields
    ${RAND}=    evaluate    random.randint(1,255)   modules=random
    Input text  jquery=#gwIpAddr    .
    gui::auditing::auto input tests     vpnName     !@#     not/val/id  123.    validname
    Input text  jquery=#vpnName    .
    GUI::Auditing::Auto Input Tests     gwIpAddr     .    2-!-3     192.168.2.${RAND}
    GUI::Auditing::Auto Input Tests     gwTCPPort   .   12.1    2-3     12345678901     ${RAND}
    GUI::Auditing::Auto Input Tests     tunnelMtu   .   12.1    2-3     ${RAND}
    GUI::Auditing::Auto Input Tests     authLocalIP     .   str    2-3      345.345.345.345     192.168.2.${RAND}
    GUI::Auditing::Auto Input Tests     authRemoteIP    .   str    2-3      345.345.345.345     245.214.56.${RAND}

*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[4]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(7)
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers