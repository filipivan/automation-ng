*** Settings ***
Documentation    Testing if Elements in the Network -> DHCP Server Add Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL


*** Test Cases ***
Test DHCP Server Add nonAccess Controls Buttons
    Click Element   //*[@id="addButton"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="cancelButton"]

Test DHCP Add Elements
    GUI::Basic::Japanese::Test Translation      //*[@id="addDhcpd_Form"]/div[2]/div/div/div[1]
    GUI::Basic::Japanese::Test Translation      //*[@id="addDhcpd_Form"]/div[2]/div/div/div[2]
    GUI::Basic::Japanese::Test Translation      //*[@id="options"]
    GUI::Basic::Japanese::Test Translation      //*[@id="addDhcpd_Form"]/div[2]/div/div/div[3]/div[2]/div/div/div[1]
    GUI::Basic::Japanese::Test Translation      //*[@id="addDhcpd_Form"]/div[2]/div/div/div[3]/div[2]/div/div/div[2]
    GUI::Basic::Japanese::Test Translation      //*[@id="addDhcpd_Form"]/div[2]/div/div/div[3]/div[2]/div/div/div[3]

Test Fields
    gui::auditing::auto input tests     dhcpdsubnet      string  not.valid.sub.net   345.345.345.345     255.255.255.0
    gui::auditing::auto input tests     dhcpdnetmask     string  not.valid.sub.net   345.345.345.345     255.255.255.255
    GUI::Basic::Spinner Should Be Invisible
    click element                       xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[2]/a
    gui::basic::spinner should be invisible
    gui::auditing::auto input tests     domainname      qwe/123     asd;asd     'zxc'   qwe.qwe
    gui::auditing::auto input tests     domainnameserver    345.345.345.345     not.val.ip.add    123.123.123.123
    gui::auditing::auto input tests     routers             345.345.345.345     not.val.ip.add    123.123.123.123
    Click element                       jquery=#returnButton
    gui::basic::spinner should be invisible
    click element                       xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[1]/input
    click element                       jquery=#delButton

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[4]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(6)
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers