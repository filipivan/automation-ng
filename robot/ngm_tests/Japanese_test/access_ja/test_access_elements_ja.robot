*** Settings ***
Documentation    Testing if Elements in the Access Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL

*** Test Cases ***

Test Access Main Menu Tab
    GUI::Basic::Japanese::Test Translation      //*[@id="main_menu"]/li[1]

Test Access SubMenu Tabs
    GUI::Basic::Japanese::Test Translation      jquery=body > div.main_menu > div > ul > li:nth-child(1)
    GUI::Basic::Japanese::Test Translation      jquery=body > div.main_menu > div > ul > li:nth-child(2)
    GUI::Basic::Japanese::Test Translation      jquery=body > div.main_menu > div > ul > li:nth-child(3)
    GUI::Basic::Japanese::Test Translation      jquery=body > div.main_menu > div > ul > li:nth-child(4)
    GUI::Basic::Japanese::Test Translation      jquery=body > div.main_menu > div > ul > li:nth-child(5)

Test Access-Table Peer_Header Contents
    GUI::Basic::Japanese::Test Translation      //*[@id="|nodegrid"]/div[1]/div[2]/a
    GUI::Basic::Japanese::Test Translation      //*[@id="|nodegrid"]/div[1]/div[3]/a

Test Access-Table Peer_Contents Contents
    GUI::Basic::Japanese::Test Translation      //*[@id="|nodegrid"]/div[2]/div/table/thead/tr/th[1]
    GUI::Basic::Japanese::Test Translation      //*[@id="|nodegrid"]/div[2]/div/table/thead/tr/th[2]

Test Access-Table Search
    GUI::Basic::Japanese::Test Translation      //*[@id="search_field"]/label

Test Access-Table Widgets
    GUI::Basic::Japanese::Test Translation      //*[@id="Pushpin"]
    GUI::Basic::Japanese::Test Translation      //*[@id="refresh-icon"]

Test Access-Table Status Bar
    : FOR    ${INDEX}    IN RANGE    1    5
    \    GUI::Basic::Japanese::Test Translation      //*[@id="main_doc"]/div[2]/div[2]/div/button[${INDEX}]

Test Access-Tree Header and Contents
    Click Element      jquery=body > div.main_menu > div > ul > li:nth-child(2)
    GUI::Basic::Japanese::Test Translation      //*[@id="tree_div"]/ol[1]/li[1]
    GUI::Basic::Japanese::Test Translation      //*[@id="tree_content"]/li/span[1]
    GUI::Basic::Japanese::Test Translation      //*[@id="tree_content"]/li/span[2]
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/div[6]/div[2]/ol[2]/div/li/div/label/a
    GUI::Basic::Japanese::Test Translation      //*[@id="tree_details"]/li/div[1]/span

Test Access-Tree Search
    GUI::Basic::Japanese::Test Translation      //*[@id="search_field"]/label

Test Access-Tree Widgets
    GUI::Basic::Japanese::Test Translation      //*[@id="Pushpin"]
    GUI::Basic::Japanese::Test Translation      //*[@id="refresh-icon"]

Test Access-Tree Status Bar
    : FOR    ${INDEX}    IN RANGE    1    5
    \    GUI::Basic::Japanese::Test Translation      //*[@id="main_doc"]/div[2]/div[2]/div/button[${INDEX}]

Test Access-Node Search and Widgets
    Click Element      jquery=body > div.main_menu > div > ul > li:nth-child(3)
    GUI::Basic::Japanese::Test Translation      //*[@id="search_field"]/label
    GUI::Basic::Japanese::Test Translation      //*[@id="Pushpin"]
    GUI::Basic::Japanese::Test Translation      //*[@id="refresh-icon"]

Test Access-Map Search and Widgets
    Click Element      jquery=body > div.main_menu > div > ul > li:nth-child(4)
    GUI::Basic::Japanese::Test Translation      //*[@id="search_field"]/label
    GUI::Basic::Japanese::Test Translation      //*[@id="Pushpin"]
    GUI::Basic::Japanese::Test Translation      //*[@id="refresh-icon"]
Test Access-Map Geomap Popup
    Mouse Over                                  //*[@id="geomap_div"]/div[1]/div[2]/div[3]/img
    : FOR    ${INDEX}    IN RANGE    1    5
    \    GUI::Basic::Japanese::Test Translation      //*[@id="geomap_div"]/div[1]/div[2]/div[4]/div/div[1]/div/ul/li[${INDEX}]

Test Access-Image Widgets
    Click Element      jquery=body > div.main_menu > div > ul > li:nth-child(5)
    GUI::Basic::Japanese::Test Translation      //*[@id="Pushpin"]
    GUI::Basic::Japanese::Test Translation      //*[@id="refresh-icon"]


*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers