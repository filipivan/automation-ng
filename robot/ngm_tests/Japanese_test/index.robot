*** Settings ***
Documentation    index.html behaviour tests
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    init.robot

Suite Setup     GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
Suite Teardown  Run Keywords    GUI::Basic::Logout      Close all Browsers
Force Tags         GUI    BASIC    LOGIN     ${BROWSER}     ${SYSTEM}       ${MODEL}        ${VERSION}      WINDOWS     CHROME  FIREFOX   EDGE     IE

*** Variables ***
${USERNAME}	admin
${PASSWORD}	admin

*** Test Cases ***
Username field focus
	[Documentation]	Test to verify if the username fields has the focus
	Page Should Contain Element	jquery=#username[autofocus]

Enter should submit login form
	[Documentation]	Login form should allow user submit the form pressing ENTER
	GUI::Basic::LoginWithEnter	${USERNAME}	${PASSWORD}