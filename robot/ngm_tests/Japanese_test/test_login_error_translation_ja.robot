*** Settings ***
Documentation    Testing if Elements in the Access Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL

*** Test Cases ***
Test Login Button Translation
    GUI::Basic::Japanese::Test Translation With Value  jquery=#login-btn

Test Wrong Credentials
    GUI::Basic::Input Text  jquery=#username    a
    GUI::Basic::Input Text  xpath=/html/body/div[5]/div/form/div[3]/input   a
    Click element   jquery=#login-btn
    gui::basic::spinner should be invisible
    ${ERR}=     Get Text    xpath=/html/body/div[5]/div/form/div[2]/div/div[1]/strong
    Should Contain  ${ERR}  [

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers