*** Settings ***
Resource    init.robot
Documentation    Get System Test System Details
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Force Tags         GUI  WINDOWS    CHROME  FIREFOX   EDGE

Suite Setup     GUI::Basic::Open NodeGrid   ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
Suite Teardown  Run Keywords    GUI::Basic::Logout  Close all Browsers

*** Variables ***

*** Test Cases ***
Get Information
    GUI::Basic::Login       ${NGVERSION}    admin       admin
    ${VERSION}=     Run Keyword If 			'${NGVERSION}' >= '4.0'     GUI4.0::Get Version
    ${MODEL}=       Run Keyword If 			'${NGVERSION}' >= '4.0'     GUI4.0::Get Model
    ${SYSTEM}=      Run Keyword If 			'${NGVERSION}' >= '4.0'     GUI4.0::Get System
    ${VERSION}=     Run Keyword If 			'${NGVERSION}' == '3.2'     GUI3.x::Get Version
    ${MODEL}=       Run Keyword If 			'${NGVERSION}' == '3.2'     GUI3.x::Get Model
    ${SYSTEM}=      Run Keyword If 			'${NGVERSION}' == '3.2'     GUI3.x::Get System
    Set Global Variable     ${VERSION}
    Set Global Variable     ${MODEL}
    Set Global Variable     ${SYSTEM}


*** Keywords ***

GUI4.0::Get System
        Wait Until Element Is Enabled       pwl
        Click Element   pwl
        Wait Until Element Is Visible       abt
        Click Element   abt
        Wait Until Page Contains   Serial Number
        ${SYSTEM}=     Get Text      jquery=div.modal \#system
        Click Button    jquery=div.modal button
        Wait Until Element Is Not Visible       jquery=div.modal
        [Return]      ${SYSTEM}

GUI4.0::Get Model
        Wait Until Element Is Enabled       pwl
        Click Element   pwl
        Wait Until Element Is Visible       abt
        Click Element   abt
        Wait Until Page Contains   Serial Number
        ${MODEL}=    Get Text        jquery=div.modal \#system
        Click Button    jquery=div.modal button
        Wait Until Element Is Not Visible       jquery=div.modal
        [Return]      ${MODEL}

GUI4.0::Get Version
        Wait Until Element Is Enabled       pwl
        Click Element   pwl
        Wait Until Element Is Visible       abt
        Click Element   abt
        Wait Until Page Contains   Serial Number
        ${VERSION}=     Get Text      jquery=div.modal \#version
        Click Button    jquery=div.modal button
        Wait Until Element Is Not Visible       jquery=div.modal
        [Return]      ${VERSION}

GUI3.x::Get System
        Wait Until Element Is Visible       //*[@id="action"]/div/div/div[1]/a
        Wait Until Element Is Enabled       id=pwl
        Click Element   id=pwl
        Wait Until Page Contains Element    id=abt
        Click Element   id=abt
        Wait Until Page Contains   Serial Number
        ${SYSTEM}=     Get Text      //table/tbody/tr[2]/td/table/tbody/tr[1]/td[2]
        Click Element           //*[@id="modal_b"]/button
        [Return]      ${SYSTEM}

GUI3.x::Get Model
        Wait Until Element Is Visible       //*[@id="action"]/div/div/div[1]/a
        Wait Until Element Is Enabled       id=pwl
        Click Element   id=pwl
        Wait Until Page Contains Element    id=abt
        Click Element   id=abt
        Wait Until Page Contains   Serial Number
        ${MODEL}=       Run Keyword And Continue On Failure    Get Text        //table/tbody/tr[2]/td/table/tbody/tr[8]/td[2]
        Click Element           //*[@id="modal_b"]/button
        [Return]      ${MODEL}

GUI3.x::Get Version
        Wait Until Element Is Visible       //*[@id="action"]/div/div/div[1]/a
        Wait Until Element Is Enabled       id=pwl
        Click Element   id=pwl
        Wait Until Page Contains Element    id=abt
        Click Element   id=abt
        Wait Until Page Contains   Serial Number
        ${VERSION}=     Get Text      //table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]
        Click Element           //*[@id="modal_b"]/button
        [Return]      ${VERSION}
