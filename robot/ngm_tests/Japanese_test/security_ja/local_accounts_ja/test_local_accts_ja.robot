*** Settings ***
Documentation    Editing System > Preferences Test Suite for NodeGrid devices
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Run Keywords    GUI::Basic::Logout	Close all Browsers
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Test Button Translations
    GUI::Basic::Japanese::Test Translation With Value   jquery=#addButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#editButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#delButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#lockUser
    GUI::Basic::Japanese::Test Translation With Value   jquery=#unlockUser

Test Table Translations
    GUI::Basic::Japanese::Test Translation  xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/thead/tr/th[2]
    GUI::Basic::Japanese::Test Translation  xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/thead/tr/th[3]
    GUI::Basic::Japanese::Test Translation  xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/thead/tr/th[4]
    gui::basic::japanese::test translation  xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[3]

Attempt Delete Admin
    gui::basic::spinner should be invisible
    Select Checkbox   xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[1]/input
    Click Element   jquery=#delButton
    Handle Alert
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div/div

Test Add Options Translations
    Click Element   jquery=#addButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#saveButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#cancelButton
    @{LST}=     Create List     uName   uPasswd     cPasswd     accExpiredDate
    Check Translation No ID Previous Sibling    @{LST}
    @{LST_T}=   Create List     hashPwd     pwForceChange
    Check Translation No ID     @{LST_T}
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[7]/label
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[7]/div/div[1]/div[2]/div/div[1]/button
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[7]/div/div[1]/div[2]/div/div[2]/button
    run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[7]/div/div[1]/div[1]/select/option
    run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[7]/div/div[1]/div[3]/select/option

Check Account Expiration Date
    Input text      jquery=#uName   aaaaa
    Input text      jquery=#accExpiredDate  placeholder
    AER Pass Input Text     password    p4assword   ${TRUE}
    AER Pass Input Text     thesedo     ntmatch     ${TRUE}
    AER Pass Input Text     asdfg,.;'   ,.;'asdfg   ${TRUE}
    AER Pass Input Text     P4$sword    P4$sword    ${FALSE}
    @{LST}=     Create List     aaaa-aaa    1234-12-12  1969-12-31  2019-7-2    2038-01-01  2020-01-01
    GUI::Auditing::Auto Input Tests     accExpiredDate  @{LST}
    GUI::Basic::Spinner Should Be Invisible
    Click element   xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[2]/td[1]/input
    Click element   jquery=#delButton
    Handle Alert    action=ACCEPT

Check Lock Translation
    gui::basic::spinner should be invisible
    Click Element   xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[1]/input
    Click Element   jquery=#lockUser
    gui::basic::spinner should be invisible
    gui::basic::japanese::test translation  xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[3]
    Click Element   xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[1]/input
    Click Element   jquery=#unlockUser
    gui::basic::spinner should be invisible

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Security::Open Local Accounts tab

Check Translation No ID
    [Arguments]     @{IDS}
    [Documentation]     Gets the Id of the sibling element and uses it to figure out if translated
    ${LEN_T}=     Get Length      ${IDS}
    :FOR    ${I}    IN RANGE    0   ${LEN_T}
    \       ${LOC}=     Get From List   ${IDS}  ${I}
    \       ${element}=     Execute Javascript          return document.getElementById('${LOC}').parentNode;
    \       ${VAL_EL}=      Get Text                    ${element}
    \       Should Contain      ${VAL_EL}       [

Check Translation No ID Previous Sibling
    [Arguments]     @{IDS}
    [Documentation]     Gets the Id of the sibling element and uses it to figure out if translated
    ${LEN_T}=     Get Length      ${IDS}
    :FOR    ${I}    IN RANGE    0   ${LEN_T}
    \       ${LOC}=     Get From List   ${IDS}  ${I}
    \       ${element}=     Execute Javascript          return document.getElementById('${LOC}').parentNode.previousSibling;
    \       ${VAL_EL}=      Get Text                    ${element}
    \       Should Contain      ${VAL_EL}       [

AER Pass Input Text
    [Arguments]  ${IP_ONE}     ${IP_TWO}      ${EXPECTS}
    [Documentation]     IP_ONE(TWO) expect the input for the two fields
    ...                 EXPECTS wants a t/f of whether an error should be thrown
    ${LOC}=     run keyword and return status  Page Should Contain Element      jquery=#cancelButton
    Run Keyword If      ${LOC}==${FALSE}    Click Element   jquery=#addButton
    Wait Until Element Is Not Visible   xpath=//*[@id='loading']    timeout=20s
    Input Text      jquery=#uPasswd      ${IP_ONE}
    Input Text      jquery=#cPasswd      ${IP_TWO}
    Click Element   jquery=#saveButton
    Wait Until Element Is Not Visible   xpath=//*[@id='loading']    timeout=20s
    ${TST}=     Execute Javascript      var er=document.getElementById('cPasswd');if(er==null){return false;}var ro=er.parentNode.nextSibling;if(ro==null){return false;}var txt=ro.textContent;return txt.includes("[");
    Log     ${TST}
    Run Keyword If      ${EXPECTS}==${TRUE}     Should Be True     ${TST}
    ...     ELSE    Should Not Be True     ${TST}