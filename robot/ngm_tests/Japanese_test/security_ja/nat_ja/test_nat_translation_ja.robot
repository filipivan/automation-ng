*** Settings ***
Documentation    Editing System > Preferences Test Suite for NodeGrid devices
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Run Keywords    GUI::Basic::Logout	Close all Browsers
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Check NAT Table Translation
    GUI::Basic::Japanese::Test Translation Table Header With Checkbox   6
    run keyword and continue on failure     gui::basic::japanese::test translation  xpath=/html/body/div[6]/div/ul/li[6]/a
    :FOR    ${I}    IN RANGE    1   9
    \   run keyword and continue on failure     gui::basic::japanese::test translation  xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[${I}]/td[2]/a
    \   run keyword and continue on failure     gui::basic::japanese::test translation  xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[${I}]/td[3]


Drilldown and Check Translations
    Click Element  xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[1]/td[2]/a
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be visible   jquery=#nat_rulesTable
    GUI::Basic::Japanese::Test Translation Table Header With Checkbox   10
    GUI::Basic::Japanese::Test Translation With Value  jquery=#returnButton
    GUI::Basic::Japanese::Test Translation With Value  jquery=#addButton
    GUI::Basic::Japanese::Test Translation With Value  jquery=#delButton
    GUI::Basic::Japanese::Test Translation With Value  jquery=#upButton
    GUI::Basic::Japanese::Test Translation With Value  jquery=#downButton
    GUI::Basic::Japanese::Test Translation With Value  jquery=#editButton

Add Rule Header Translation
    Click Element   jquery=#addButton
    GUI::Basic::Spinner Should Be Invisible
    #check headers
    gui::basic::japanese::test translation      jquery=#nat_targetTitle
    gui::basic::japanese::test translation      jquery=#nat_matchTitle

Rule Checkbox Translations
    Select Checkbox     jquery=#nat_stateM_enabled
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Check Translation No ID         nat_inv_source  nat_inv_dest    nat_inv_inIface     nat_stateM_enabled
    ...     nat_st_state_new    nat_st_state_est    nat_st_state_rel    nat_st_state_inv    nat_stateM_state-inv

Rule Dropdown and Textbox Translations
    Select From List By Value   nat_target      DNAT
    gui::auditing::auto input tests     nat_source      345.345.345.345     stringinv   ::2c    123.123.123.123
    gui::auditing::auto input tests     nat_dest        345.345.345.345     stringinv   ::2c    123.123.123.123
    gui::auditing::auto input tests     nat_todest      345.345.345.345     stringinv   ::2c    123.123.123.123
    gui::basic::check translation no id previous sibling     nat_target      nat_source      nat_dest    nat_inIface
    ...     nat_frags   nat_todest
    Test List Dropdowns  nat_target     nat_inIface          nat_frags
    GUI::Basic::Japanese::Test Translation      jquery=#nat_ipnotice


Test Right Side Radio Buttons
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[3]/div[2]/div/div/div[2]/div/div/div[1]/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[3]/div[2]/div/div/div[2]/div/div/div[1]/div/div[1]/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[3]/div[2]/div/div/div[2]/div/div/div[1]/div/div[3]/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[3]/div[2]/div/div/div[2]/div/div/div[1]/div/div[14]/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[3]/div[2]/div/div/div[2]/div/div/div[1]/div/div[18]/label

Test Numeric Protocol
    Select Radio Button     nat_protocol    numeric
    gui::basic::check translation no id previous sibling    nat_protoNum
    GUI::Auditing::Auto Input Tests     nat_protoNum    -1  str     65537   100

Test TCP Protocol
    Select Radio Button     nat_protocol    tcp
    gui::basic::check translation no id previous sibling    nat_sportTCP    nat_dportTCP    nat_toportsTCP    nat_SYN
    ...     nat_ACK     nat_FIN     nat_RST     nat_URG     nat_PSH
    GUI::Auditing::Auto Input Tests     nat_sportTCP    -1  str     65537   1000
    GUI::Auditing::Auto Input Tests     nat_dportTCP    -1  str     65537   1000
    GUI::Auditing::Auto Input Tests     nat_toportsTCP    -1  str   6.5.5.3   1000
    gui::basic::check translation no id     nat_inv_tcp-flags

Test UDP Protocol
    Select Radio Button     nat_protocol    udp
    gui::basic::check translation no id previous sibling    nat_sportUDP    nat_dportUDP
    GUI::Auditing::Auto Input Tests     nat_sportUDP    -1  str     65537   1000
    GUI::Auditing::Auto Input Tests     nat_dportUDP    -1  str     65537   1000

Test ICMP Protocol
    Select Radio Button     nat_protocol    nat_icmp
    gui::basic::check translation no id previous sibling    nat_icmp-type
    gui::basic::check translation no id     nat_inv_icmp-type

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Security::Open NAT tab

Test List Dropdowns
    [Arguments]  @{LOC}
    :FOR    ${I}    IN  @{LOC}
    \   Test Dropdown Options  ${I}

Test Dropdown Options
    [Arguments]  ${LOC}
    @{TXT}=     Get List Items  jquery=#${LOC}
    :FOR    ${I}    IN      @{TXT}
    \   run keyword and continue on failure  Should Contain  ${I}    [