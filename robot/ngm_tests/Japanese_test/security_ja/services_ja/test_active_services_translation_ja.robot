*** Settings ***
Documentation    Editing System > Preferences Test Suite for NodeGrid devices
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Run Keywords    GUI::Basic::Logout	Close all Browsers
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Test Save Button and Header
    GUI::Basic::Japanese::Test Translation With Value   jquery=#saveButton
    GUI::Basic::Japanese::Test Translation      xpath=//*[@id="services"]
    gui::basic::spinner should be invisible
    Select Checkbox   jquery=#grpc
    Select Checkbox   jquery=#telnet

Test Checkbox Translations
    GUI::Basic::Check Translation No ID     usbD    rpc     grpc    cloud   ftp     snmp    telnet  telnetDev   icmp
    ...     usbip   virtualization  autoEnroll  essecure    vmserial    ztp     pxeState
    GUI::Basic::Check Translation No ID Previous Sibling    grpcPort    telnetPort  clusterport     esport
    ...     vmserialport    vmotiontimeout

Test Textbox Errors
    ${RAND}=    evaluate    random.randint(1,3600)      modules=random
    gui::auditing::auto input tests     grpcPort        a   1-2     12.3    12/3    ${RAND}
    gui::auditing::auto input tests     telnetPort      a   1-2     12.3    12/3    ${RAND}
    gui::auditing::auto input tests     clusterport     a   1-2     12.3    12/3    ${RAND}
    gui::auditing::auto input tests     esport          a   1-2     12.3    12/3    ${RAND}
    gui::auditing::auto input tests     vmserialport    a   1-2     12.3    12/3    ${RAND}
    gui::auditing::auto input tests     vmotiontimeout  a   -1      12.3    3601    ${RAND}

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Security::Open Services tab
