*** Settings ***
Documentation    Editing System > Preferences Test Suite for NodeGrid devices
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Run Keywords    GUI::Basic::Logout	Close all Browsers
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Test Headers
    select checkbox   jquery=#authfail
    select checkbox   jquery=#bootPasswdProtected
    click element   jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    checkbox should be selected     jquery=#authfail
    checkbox should be selected     jquery=#bootPasswdProtected
    gui::basic::spinner should be invisible
    GUI::Basic::Japanese::Test Translation      xpath=//*[@id="managedDevices"]
    GUI::Basic::Japanese::Test Translation      xpath=//*[@id="fail2ban"]
    GUI::Basic::Japanese::Test Translation      xpath=//*[@id="bootPasswordNotice"]

Test Checkbox Translations
    GUI::Basic::Check Translation No ID     authorization   discEnabled     discRules   authfail    rescpwd     bootPasswdProtected
    GUI::Basic::Check Translation No ID Previous Sibling    bantime     findtime    maxretry    bootPassword    bootPasswordConf

Test Textbox Errors
    ${RAND}=    evaluate    random.randint(1,3601)      modules=random
    gui::basic::spinner should be invisible
    gui::auditing::auto input tests     bantime         a   1-2     12.3    12/3    ${RAND}
    gui::auditing::auto input tests     findtime        a   1-2     12.3    12/3    ${RAND}
    gui::auditing::auto input tests     maxretry        a   1-2     12.3    12/3    ${RAND}
    Test Passwords   password    p4ssword
    Test Passwords   inv     inv1
    test passwords   password    password



*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Security::Open Services tab

Test Passwords
    [Arguments]  ${PASSO}   ${PASST}
    Input Text  jquery=#bootPassword        ${PASSO}
    Input Text  jquery=#bootPasswordConf    ${PASST}
    Click element   jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    ${TST}=     Execute Javascript      var er=document.getElementById('bootPassword');if(er==null){return false;}var ro=er.parentNode.nextSibling;if(ro==null){return false;}var txt=ro.textContent;return txt.includes("擬");
    Run Keyword if  ${PASSO}==${PASST}  Should Not Be True     ${TST}
    ...     ELSE    Should Be True      ${TST}
