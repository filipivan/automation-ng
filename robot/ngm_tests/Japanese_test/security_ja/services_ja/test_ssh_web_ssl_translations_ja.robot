*** Settings ***
Documentation    Editing System > Preferences Test Suite for NodeGrid devices
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Run Keywords    GUI::Basic::Logout	Close all Browsers
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Test Headers
    GUI::Basic::Japanese::Test Translation      xpath=//*[@id="sshServer"]
    GUI::Basic::Japanese::Test Translation      xpath=//*[@id="webService"]
    GUI::Basic::Japanese::Test Translation      xpath=//*[@id="crypto"]


Test Checkbox Translations
    GUI::Basic::Check Translation No ID     allow   http    https   redirect    httpsTLS1_2     httpsTLS1_1     httpsTLS1
    GUI::Basic::Check Translation No ID Previous Sibling    sshport     sshciphers  sshmacs     sshkexalgorithms    httpport    httpsport
    GUI::Basic::Japanese::Test Translation      xpath=//*[@id="webprofileWarning"]


Test Textbox Errors
    gui::auditing::auto input tests     sshport         a   1-2     12.3    12/3    23
    input text      jquery=#sshport     22
    click element   jquery=#saveButton
    Wait Until Element Is Not Visible     xpath=//*[@id='loading']    timeout=20s
    Check Error Thrown      sshciphers  qwer
    Check Error Thrown      sshmacs     qwer
    Check Error Thrown      sshkexalgorithms    qwer
    Check Error Thrown      httpport    qwer
    Check Error Thrown      httpsport   qwer

Test Minimum Cipher Translations
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[2]/div/div[3]/div[2]/div/div/div[4]/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[2]/div/div[3]/div[2]/div/div/div[4]/div/div[1]/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[2]/div/div[3]/div[2]/div/div/div[4]/div/div[2]/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[2]/div/div[3]/div[2]/div/div/div[4]/div/div[3]/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[2]/div/div[3]/div[2]/div/div/div[4]/div/div[4]/label
    gui::basic::japanese::test translation      xpath=//*[@id="webprofileWarning"]

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Security::Open Services tab

Check Error Thrown
    [Arguments]     ${LOC}  ${TST}
    Input Text      jquery=#${LOC}  ${TST}
    Click element   jquery=#saveButton
    Wait Until Element Is Not Visible     xpath=//*[@id='loading']    timeout=20s
    ${TXT}=     Execute Javascript      var er=document.getElementById('${LOC}');if(er==null){return false;}var ro=er.parentNode.nextSibling;if(ro==null){return false;}var txt=ro.textContent;return txt.includes("[");
    Should Be True     ${TXT}
