*** Settings ***
Documentation    Editing System > Preferences Test Suite for NodeGrid devices
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Run Keywords    GUI::Basic::Logout	Close all Browsers
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Check Buttons Translation
    GUI::Basic::Japanese::Test Translation With Value   jquery=#addButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#delButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#policyButton

Check Table Header Translation
    :FOR    ${I}    IN RANGE    2   7
    \   run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/thead/tr/th[${I}]

Check Table Body Translation
    :FOR    ${I}    IN RANGE    1   7
    \   run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[${I}]/td[2]/a
    \   run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[${I}]/td[3]
    \   run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[${I}]/td[6]

Check Add Translation
    Click Element   jquery=#addButton
    GUI::Basic::Spinner Should Be Invisible
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[1]/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[1]/div/div[1]/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[1]/div/div[2]/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[2]/label
    Click Element   jquery=#cancelButton
    GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Security::Open Firewall tab

