*** Settings ***
Documentation    Editing System > Preferences Test Suite for NodeGrid devices
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Run Keywords    GUI::Basic::Logout	Close all Browsers
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Add Firewall Chain
    click element  jquery=#addButton
    GUI::Basic::Spinner Should Be Invisible
    Input Text  jquery=#chain   testingrule
    Click Element  jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible

Drilldown and Check Translations
    Click Element  xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[4]/td[2]/a
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be visible   jquery=#rulesTable
    GUI::Basic::Japanese::Test Translation Table Header With Checkbox   10
    GUI::Basic::Japanese::Test Translation With Value  jquery=#returnButton
    GUI::Basic::Japanese::Test Translation With Value  jquery=#addButton
    GUI::Basic::Japanese::Test Translation With Value  jquery=#delButton
    GUI::Basic::Japanese::Test Translation With Value  jquery=#upButton
    GUI::Basic::Japanese::Test Translation With Value  jquery=#downButton
    GUI::Basic::Japanese::Test Translation With Value  jquery=#editButton

Add Rule Header Translation
    Click Element   jquery=#addButton
    GUI::Basic::Spinner Should Be Invisible
    #select reject
    Select From List By Value   jquery=#target  REJECT
    gui::basic::spinner should be invisible
    #check headers
    gui::basic::japanese::test translation      jquery=#targetTitle
    gui::basic::japanese::test translation      jquery=#matchTitle
    gui::basic::japanese::test translation      jquery=#logTitle
    gui::basic::japanese::test translation      jquery=#rejectTitle

Rule Checkbox Translations
    Select Checkbox     jquery=#stateM_enabled
    Select Checkbox     jquery=#inv_inIface
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Check Translation No ID         inv_source  inv_dest    inv_inIface     inv_outIface    stateM_enabled
    ...     st_state_new    st_state_est    st_state_rel    st_state_inv    stateM_state-inv    log_tcp_seq
    ...     log_tcp_opt     log_ip_opt      inv_proto   inv_sport   inv_dport

Rule Dropdown and Textbox Translations
    gui::auditing::auto input tests     source      345.345.345.345     stringinv   ::2c    123.123.123.123
    gui::auditing::auto input tests     dest        345.345.345.345     stringinv   ::2c    123.123.123.123
    gui::basic::check translation no id previous sibling     target      reject_with     source      dest    inIface
    ...     outIface    frags   log_level   log_prefix
    Test List Dropdowns  target  reject_with     inIface     outIface    frags   log_level

Test Right Side Radio Buttons
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[3]/div[2]/div/div/div[2]/div/div/div[1]/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[3]/div[2]/div/div/div[2]/div/div/div[1]/div/div[1]/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[3]/div[2]/div/div/div[2]/div/div/div[1]/div/div[3]/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[3]/div[2]/div/div/div[2]/div/div/div[1]/div/div[13]/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[3]/div[2]/div/div/div[2]/div/div/div[1]/div/div[16]/label
    Select Radio Button     protocol    numeric
    gui::basic::check translation no id previous sibling    protoNum
    GUI::Auditing::Auto Input Tests     protoNum    -1  str     65537   100
    Select Radio Button     protocol    tcp
    gui::basic::check translation no id previous sibling    sportTCP    dportTCP    SYN     ACK     FIN     RST
    ...     URG     PSH
    GUI::Auditing::Auto Input Tests     sportTCP    -1  str     65537   1000
    GUI::Auditing::Auto Input Tests     dportTCP    -1  str     65537   1000
    gui::basic::check translation no id     inv_tcp-flags
    Select Radio Button     protocol    udp
    gui::basic::check translation no id previous sibling    sportUDP    dportUDP
    GUI::Auditing::Auto Input Tests     sportUDP    -1  str     65537   1000
    GUI::Auditing::Auto Input Tests     dportUDP    -1  str     65537   1000
    Select Radio Button     protocol    icmp
    gui::basic::check translation no id previous sibling    icmp-type
    gui::basic::check translation no id     inv_icmp-type

Delete Chain
    Click Element       jquery=#cancelButton
    GUI::Security::Open Firewall tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element       xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[4]/td[1]/input
    Click Element       jquery=#delButton
    GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Security::Open Firewall tab

Test List Dropdowns
    [Arguments]  @{LOC}
    :FOR    ${I}    IN  @{LOC}
    \   Test Dropdown Options  ${I}

Test Dropdown Options
    [Arguments]  ${LOC}
    @{TXT}=     Get List Items  jquery=#${LOC}
    :FOR    ${I}    IN      @{TXT}
    \   run keyword and continue on failure  Should Contain  ${I}    [