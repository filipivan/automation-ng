*** Settings ***
Documentation    Test common behaviour in authentication page
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup     Suite Setup
Suite Teardown  Run Keywords    GUI::Basic::Logout      Close all Browsers
Test Setup     Setup
Force Tags         GUI     ${BROWSER}     ${SYSTEM}       ${MODEL}        ${VERSION}    WINDOWS     CHROME  FIREFOX   EDGE     IE
Default Tags    EXCLUDEIN3_2

*** Variable ***
${USERNAME}	admin
${PASSWORD}	admin

*** Test Cases ***

Check Buttons Translation
    GUI::Basic::Japanese::Test Translation With Value   jquery=#addButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#delButton

Check Table Translation
    :FOR    ${I}    IN RANGE    2   5
    \   run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/thead/tr/th[${I}]

Check Add Fields Translation
    Click button    jquery=#addButton
    gui::basic::spinner should be invisible
    run keyword and continue on failure     gui::basic::check translation no id previous sibling    secondFactorName
    ...     secondFactorMethod  secondFactorStatus  rest-url    client-key  client-id   read-timeout    connect-timeout    max-retries
    log     READ AND CONNECT TIMEOUT PASS BUT ARE NOT TRANSLATED
    @{LST}=     Get List Items      jquery=#secondFactorMethod
    :FOR    ${I}    IN  @{LST}
    \   run keyword and continue on failure     Should Contain  ${I}    ]
    @{LST}=     Get List Items      jquery=#secondFactorStatus
    :FOR    ${I}    IN  @{LST}
    \   run keyword and continue on failure     Should Contain  ${I}    ]
    run keyword and continue on failure     gui::basic::japanese::test translation      xpath=//*[@id="method"]

Test Fields
    Input Text      jquery=#secondFactorName    name
    Input Text      jquery=#client-id   ,.;'123notit[]
    Click Element   jquery=#saveButton
    ${TST}=     Execute Javascript      var er=document.getElementById('client-id');if(er==null){return false;}var ro=er.parentNode.nextSibling;if(ro==null){return false;}var txt=ro.textContent;return txt.includes("[");
    run keyword and continue on failure  Should Be True     ${TST}
    ${TST}=     Execute Javascript      var er=document.getElementById('client-key');if(er==null){return false;}var ro=er.parentNode.nextSibling;if(ro==null){return false;}var txt=ro.textContent;return txt.includes("[");
    run keyword and continue on failure  Should Be True     ${TST}
    gui::auditing::auto input tests     read-timeout    str     29  241     239
    gui::auditing::auto input tests     connect-timeout     str     9   121     119
    gui::auditing::auto input tests     max-retries     str     0   6   4

Add 2Factor and Test Certificate
    Input Text      jquery=#rest-url        a
    Input Text      jquery=#client-key      a
    Input Text      jquery=#client-id       a
    Click Element   jquery=#saveButton
    gui::basic::spinner should be invisible
    Click Element   xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[2]/a
    GUI::Basic::Spinner Should Be Invisible
    run keyword and continue on failure     gui::basic::japanese::test translation      jquery=#certificate
    Click Element   jquery=#certificate
    GUI::Basic::Spinner Should Be Invisible
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div/label
    Select Radio Button     systemcertselection     systemcert-localcomputer
    GUI::Basic::Spinner Should Be Invisible
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div/div/div[1]/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div/div/form/div/label
    Select Radio Button     systemcertselection     systemcert-remote
    gui::basic::spinner should be invisible
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div/div/div[2]/label
    gui::basic::check translation no id previous sibling    url     userName    password
    GUI::Basic::Check Translation No ID     absolutepath

Test Errors
    Check Error Banner      url     !@#$%
    Check Error Banner      url     http://aa.aa
    Check Error Banner      url     ftp://aa.aa
    Check Error Banner      userName    name

Delete Rule
    Click Element       jquery=#cancelButton
    GUI::Basic::Spinner Should Be Invisible
    Click Element       jquery=#cancelButton
    GUI::Basic::Spinner Should Be Invisible
    Select Checkbox     xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[1]/input
    Click Element       jquery=#delButton
    GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
Suite Setup
	GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
	GUI::Basic::Login	${NGVERSION}	${USERNAME}	${PASSWORD}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Open Authentication Tab
	Click Element   xpath=/html/body/div[7]/div[1]/a[2]/span
	GUI::Basic::Spinner Should Be Invisible

Check Error Banner
    [Arguments]     ${LOC}      ${INP}
    Input Text      jquery=#${LOC}     ${INP}
    Click Element   Jquery=#saveButton
    Handle Alert
    GUI::Basic::Spinner Should Be Invisible
    ${TXT}=     Get Text    xpath=/html/body/div[7]/div[2]/div[2]/form/div[1]/div/div/p
    run keyword and continue on failure  Should Contain      ${TXT}      [