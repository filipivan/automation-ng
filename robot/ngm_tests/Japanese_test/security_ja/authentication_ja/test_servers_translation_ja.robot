*** Settings ***
Documentation    Editing System > Preferences Test Suite for NodeGrid devices
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Run Keywords    GUI::Basic::Logout	Close all Browsers
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Check Buttons Translation
    GUI::Basic::Japanese::Test Translation With Value   jquery=#addButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#delButton
    Log     NOTE::::: UP BUTTON PASSES TEST BUT DOES NOT HAVE TRANSLATION
    GUI::Basic::Japanese::Test Translation With Value   jquery=#upButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#downButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#consoleButton
    gui::basic::japanese::test translation with value   jquery=#defGroup

Check Table Translation
    #xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/thead/tr/th[2]
    #xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[3]
    :FOR    ${I}    IN RANGE    2   7
    \   run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/thead/tr/th[${I}]
    \   Run Keyword If      ${I} > 2
    \   ...     Run Keyword if  ${I}==3     Log  NoTranslateHere
    \   ...     ELSE    run keyword and continue on failure  gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[${I}]

Check Console Translation
    Click Element       jquery=#consoleButton
    GUI::Basic::Spinner Should Be Invisible
    gui::basic::japanese::test translation      xpath=//*[@id="consoleAuthTitle"]
    gui::basic::japanese::test translation      xpath=//*[@id="consoleAuthNote"]
    GUI::Basic::Check Translation No ID         localBackdoor
    Click Element       jquery=#cancelButton
    GUI::Basic::Spinner Should Be Invisible

Check Default Group Translation
    Click Element       jquery=#defGroup
    GUI::Basic::Spinner Should Be Invisible
    run keyword and continue on failure     gui::basic::japanese::test translation      xpath=//*[@id="defGroupTitle"]
    run keyword and continue on failure     GUI::Basic::Check Translation No ID Previous Sibling    defGroup
    @{LST}=     Get List Items      jquery=#defGroup
    :FOR    ${I}    IN  @{LST}
    \   run keyword and continue on failure     Should Contain  ${I}    ]
    run keyword and continue on failure     gui::basic::japanese::test translation      xpath=//*[@id="defGroupNote"]
    Click Element       jquery=#cancelButton
    GUI::Basic::Spinner Should Be Invisible

Test Index 1 Translation
    wait until element is visible   jquery=#addButton
    Click Element       xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[2]/a
    gui::basic::spinner should be invisible
    run keyword and continue on failure     gui::basic::check translation no id previous sibling    localMethod
    run keyword and continue on failure     GUI::Basic::Check Translation No ID Previous Sibling    mauthStatus
    run keyword and continue on failure     gui::basic::check translation no id previous sibling    mauth2factor
    @{LST}=     Get List Items      jquery=#mauth2factor
    :FOR    ${I}    IN  @{LST}
    \   run keyword and continue on failure     Should Contain  ${I}    ]
    run keyword and continue on failure     gui::basic::check translation no id previous sibling    mauthStatus
    @{LST}=     Get List Items      jquery=#mauthStatus
    :FOR    ${I}    IN  @{LST}
    \   run keyword and continue on failure     Should Contain  ${I}    ]
    Click Element   jquery=#cancelButton
    GUI::Basic::Spinner Should Be Invisible

Test Add Server Translation
    Click Element   jquery=#addButton
    gui::basic::spinner should be invisible
    gui::basic::japanese::test translation with value   jquery=#saveButton
    gui::basic::japanese::test translation with value   jquery=#cancelButton
    run keyword and continue on failure     gui::basic::check translation no id previous sibling    mauthMethod
    ...     mauth2factor    mauthStatus     mauthServer     ldapbase    ldapsecure  ldapPort    ldapbinddn
    ...     ldapbindpw  ldapbindpw2     ldaploginattributes     ldapgroupattributes     ldapsearchfilter
    run keyword and continue on failure     gui::basic::check translation no id     mauthFallback   globalCatalog   ldapSearchNestGN
    run keyword and continue on failure     gui::basic::japanese::test translation  ldapMethod

Test Fields
    Input text      jquery=#ldapbinddn  !@#$%^&*()
    ${RAND}=    evaluate    random.randint(1,1000)  modules=random
    gui::auditing::auto input tests  ldapPort   str     12.3    54/1    12,14   ${RAND}
    Input text  jquery=#ldapbindpw      a
    input text  jquery=#ldapbindpw2     s
    Click element   jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    ${TST}=     Execute Javascript      var er=document.getElementById('ldapbindpw2');if(er==null){return false;}var ro=er.parentNode.nextSibling;if(ro==null){return false;}var txt=ro.textContent;return txt.includes("[");
    Should Be True     ${TST}
    Click element   jquery=#ldapSearchNestGN
    GUI::Basic::Elements Should Be Visible  jquery=#ldapGNbase
    run keyword and continue on failure     gui::basic::check translation no id previous sibling    ldapGNbase

    gui::auditing::auto input tests  mauthServer    ${EMPTY}    !@#$%^&    1.1.1.1
    gui::auditing::auto input tests  ldaploginattributes    !@#$%^&    valid
    gui::auditing::auto input tests  ldapgroupattributes    !@#$%^&    valid
    gui::auditing::auto input tests  ldapGNbase     !@#$%^&    valid



*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Security::Authentication::Open Servers Tab
