*** Settings ***
Documentation    Test common behaviour in security ->password rules page
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup     Suite Setup
Suite Teardown  Run Keywords    GUI::Basic::Logout      Close all Browsers
Force Tags         GUI     ${BROWSER}     ${SYSTEM}       ${MODEL}        ${VERSION}    WINDOWS     CHROME  FIREFOX   EDGE     IE
Default Tags    EXCLUDEIN3_2


*** Variable ***
${USERNAME}	            admin
${PASSWORD}	            admin
${New User}             User123

*** Test Cases ***

Test Translation All Fields
    GUI::Basic::Japanese::Test Translation      jquery=#password_enforcement
    gui::basic::japanese::test translation      jquery=#default_expiration
    GUI::Basic::Japanese::Test Translation With Value   jquery=#saveButton
    @{LST}=     Create List     min_digits  min_upper_char  min_special_char    min_size    remember    min_days    max_days    warn_days
    Check Translation No ID Previous Sibling    @{LST}

Test Only Length
    #000
    Suite Set Pass Rules    0   0   0
    Set Password Test  passw     ${TRUE}
    Set Password Test  passwo    ${FALSE}
    GUI::Basic::Delete Rows In Table Containing Value       user_namesTable     ${New User}
Test Upper
    #010
    Create Set and Del  0   1   0   p4sswo  Passwo
Test Special
    #001
    Create Set and Del  0   0   1   Passwo  pa$swo
Test Upper and Special
    #011
    Create Set and Del  0   1   1   p4sswo  Pa$swo
Test Digit
    #100
    Create Set and Del  1   0   0   Passwo  p4sswo
Test Digit and Upper
    #110
    Create Set and Del  1   1   0   pa$swo  P4sswo
Test Digit and Special
    #101
    Create Set and Del  1   0   1   Passwo  p4$swo
Test Digit and Upper and Special
    #111
    Create Set and Del  1   1   1   passwo  P4$swo

Test Min Days
    GUI::Security::Open Password Rules tab
    Input text      jquery=#max_days    10
    @{TESTS}=       Create List     a   10  11  9
    GUI::Auditing::Auto Input Tests     min_days    @{TESTS}

Test Max Days
    @{TESTS}=       Create List     a   8   9   10
    GUI::Auditing::Auto Input Tests     max_days    @{TESTS}

Test Warn Days
    @{TESTS}=       Create List     a   2   1   ${EMPTY}
    GUI::Auditing::Auto Input Tests     warn_days   @{TESTS}
    Input Text      jquery=#min_days    0
    Input Text      jquery=#max_days    99999
    Input Text      jquery=#warn_days   7
    Click element   jquery=#saveButton
    Suite Set Pass Rules    0   0   0


*** Keywords ***
Suite Setup
	${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
	GUI::Security::Open Password Rules Tab
	Wait until element is Visible       jquery=#enabled
	${IS_SETTING_ENABLED}=	Run Keyword And Return Status	Checkbox Should Be Selected	    //*[@id="enabled"]
    Run Keyword If	${IS_SETTING_ENABLED}==${FALSE}	Select Checkbox	    jquery=#enabled
	Wait until element is visible       jquery=#min_size
	Input Text      jquery=#min_size    6
	Click Element       jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible

Suite Set Pass Rules
    [Arguments]	${Digits}=0     ${Upper}=0      ${Special}=0       #${Min Size}=6
    GUI::Security::Open Password Rules Tab
    #Select Checkbox	    jquery=#enabled
    Wait until element is visible       jquery=#min_size
    Input Text      //*[@id='min_digits']             ${Digits}
    Input Text      //*[@id='min_upper_char']         ${Upper}
    Input Text      //*[@id='min_special_char']       ${Special}
    #GUI::Basic::Input Text      //*[@id='min_size']               ${Min Size}
    ${IS_SAVE_ENABLED}=	Run Keyword And Return Status	Element Should Be Enabled	//*[@id='saveButton']
    Run Keyword If	${IS_SAVE_ENABLED}	GUI::Basic::Save

Make New Account
    GUI::Security::Open Local Accounts tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element       jquery=#addButton
    GUI::Basic::Spinner Should Be Invisible
    Input Text          jquery=#uName       ${New User}

Set Password Test
    [Arguments]  ${PASS}    ${EXPECTS}
    [Documentation]  pass is expecting the password to test
    ...             expects wants a t/f on whether there should be an error
    ${WHERE_AM_I}=      run keyword and return status  Element Should Not Be Visible    jquery=#cancelButton
    run keyword if  ${WHERE_AM_I}   Make New Account
    Input Text      jquery=#uPasswd      ${PASS}
    Input Text      jquery=#cPasswd      ${PASS}
    Click Element               jquery=#saveButton
    Wait Until Element Is Not Visible   xpath=//*[@id='loading']    timeout=20s
    ${TST}=     Execute Javascript      var er=document.getElementById('uPasswd');if(er==null){return false;}var ro=er.parentNode.nextSibling;if(ro==null){return false;}var txt=ro.textContent;return txt.includes("[");
    Log     ${TST}
    Run Keyword If      ${EXPECTS}==${TRUE}     Should Be True     ${TST}
    ...     ELSE    Should Not Be True     ${TST}

Create Set and Del
    [Arguments]  ${DIGITS}  ${UPPER}    ${SPECIAL}      ${P_INV}     ${P_YES}
    [Documentation]  Digits is the min number of digits in pass
    ...                 Upper is the min number of upper case characters
    ...                 Special is the min number of special characters
    ...                 Pass_INV is the first test, MUST BE INVALID
    ...                 Pass_YES is the second test, MUST BE VALID
    Suite Set Pass Rules    ${DIGITS}   ${UPPER}   ${SPECIAL}
    Set Password Test  passw            ${TRUE}
    Set Password Test  ${P_INV}      ${TRUE}
    Set Password Test  ${P_YES}      ${FALSE}
    GUI::Basic::Delete Rows In Table Containing Value       user_namesTable     ${New User}

Check Translation No ID Previous Sibling
    [Arguments]     @{IDS}
    [Documentation]     Gets the Id of the sibling element and uses it to figure out if translated
    ${LEN_T}=     Get Length      ${IDS}
    :FOR    ${I}    IN RANGE    0   ${LEN_T}
    \       ${LOC}=     Get From List   ${IDS}  ${I}
    \       ${element}=     Execute Javascript          return document.getElementById('${LOC}').parentNode.previousSibling;
    \       ${VAL_EL}=      Get Text                    ${element}
    \       Should Contain      ${VAL_EL}       [