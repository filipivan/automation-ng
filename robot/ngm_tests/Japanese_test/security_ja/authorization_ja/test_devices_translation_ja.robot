*** Settings ***
Documentation    Editing System > Preferences Test Suite for NodeGrid devices
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Run Keywords    GUI::Basic::Logout	Close all Browsers
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Check Return Translation
    GUI::Basic::Japanese::Test Translation With Value   jquery=#returnButton

Test Table Translations
    :FOR    ${C}    IN RANGE    1   15
    \   run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/thead/tr/th[${C}]
    \   run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[${C}]

Test Add Device User Group
    GUI::Security::Open Authorization tab
	Wait Until Element Is Visible   jquery=#refresh-icon
	Click element   xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[2]/td[2]/a
	Wait until Element Is Visible   jquery=#returnButton
	Click Element   jquery=#spm > span:nth-child(4)
    Wait Until element is Visible   jquery=#agasSpm_Table
    GUI::Basic::Japanese::Test Translation With Value   jquery=#returnButton
    gui::basic::japanese::test translation with value   jquery=#addButton
    gui::basic::japanese::test translation with value   jquery=#delButton
    gui::basic::japanese::test translation with value   jquery=#editButton
    Click Element   jquery=#addButton
    GUI::Basic::Spinner Should Be Invisible
    gui::basic::japanese::test translation      jquery=#targetAssign
    gui::basic::japanese::test translation      jquery=#tarAccessRights
    gui::basic::japanese::test translation      jquery=#capNotice

Test Session Mode Tranlsation
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[2]/div[2]/div/div[1]/div/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[2]/div[2]/div/div[1]/div/div/div[1]/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[2]/div[2]/div/div[1]/div/div/div[2]/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[2]/div[2]/div/div[1]/div/div/div[3]/label
Test Power Mode Translation
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[2]/div[2]/div/div[2]/div/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[2]/div[2]/div/div[2]/div/div/div[1]/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[2]/div[2]/div/div[2]/div/div/div[2]/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[2]/div[2]/div/div[2]/div/div/div[3]/label
Test Door Mode Translation
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[2]/div[2]/div/div[3]/div/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[2]/div[2]/div/div[3]/div/div/div[1]/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[2]/div[2]/div/div[3]/div/div/div[2]/label
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[2]/div[2]/div/div[3]/div/div/div[3]/label

Test Checkbox Translations
    gui::basic::check translation no id         tMKS    tKVM    tReset  tSPConsole  tVirtualmedia   tAccessLogAudit
    ...     tAccessLogClear     tEventLogAudit  tEventLogClear  tSensorInfo     tMonitoring     tCustomCommands

Save Settings and Check Addition
    Click Element       xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[1]/div[1]/select/option
    Click Element       xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[1]/div[2]/div/div[1]/button
    Select Checkbox     jquery=#tMKS
    Click element       jquery=#saveButton
    Handle Alert
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain Element     xpath=//*[@id="tbody"]/tr
    Select Checkbox     xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[1]/input
    Click element       jquery=#delButton
    GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Security::Authorization::Open Acct Devices tab
