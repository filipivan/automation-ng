*** Settings ***
Documentation    Editing System > Preferences Test Suite for NodeGrid devices
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Run Keywords    GUI::Basic::Logout	Close all Browsers
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Check Return Translation
    GUI::Basic::Japanese::Test Translation With Value   jquery=#returnButton

Test Table Translations
    :FOR    ${C}    IN RANGE    1   5
    \   run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/thead/tr/th[${C}]
    \   run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[${C}]

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Security::Authorization::Open Acct Outlets tab
