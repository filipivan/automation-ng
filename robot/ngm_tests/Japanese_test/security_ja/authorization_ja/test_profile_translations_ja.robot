*** Settings ***
Documentation    Test common behaviour in security ->password rules page
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup     Suite Setup
Suite Teardown  Run Keywords    GUI::Basic::Logout      Close all Browsers
Force Tags         GUI     ${BROWSER}     ${SYSTEM}       ${MODEL}        ${VERSION}    WINDOWS     CHROME  FIREFOX   EDGE     IE
Default Tags    EXCLUDEIN3_2


*** Variable ***
${USERNAME}	            admin
${PASSWORD}	            admin

*** Test Cases ***

Test Translation Buttons
    GUI::Basic::Japanese::Test Translation With Value   jquery=#returnButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#saveButton

Test Header Translation
    GUI::Basic::Japanese::Test Translation      jquery=#systemPermissions
    GUI::Basic::Japanese::Test Translation      jquery=#systemProfile
    gui::basic::japanese::test translation      jquery=#groupEmail

Test Selection Box Translation
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[1]/div[2]/div/div/div/label
    @{LST}=     Get List Items      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[1]/div[3]/select
    :FOR    ${I}     IN     @{LST}
    \   run keyword and continue on failure     Should Contain  ${I}    [
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[1]/div[2]/div/div[1]/button
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[1]/div[2]/div/div[2]/button

Test Checkbox Text Translations
    @{LST}=     Create List     restrictConfsys     enablelogprof   custom_session_timeout
    gui::basic::check translation no id     @{LST}
    Select Checkbox   jquery=#custom_session_timeout
    gui::basic::spinner should be invisible
    gui::basic::check translation no id previous sibling  custom_timeout
    gui::auditing::auto input tests  custom_timeout     aaa     89      93.24   12:30   99
    Input text  jquery=#custom_timeout  300

Test Radio Button Translations
    run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[4]/label
    run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[4]/div/div[1]/label
    run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[4]/div/div[2]/label

Test Email Translations
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[5]/div[2]/div/div/div[1]/label
    gui::basic::japanese::test translation      xpath=//*[@id="group_emailwarn"]

*** Keywords ***
Suite Setup
	${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
	GUI::Security::Authorization::Open Acct Profile tab
	GUI::Basic::Spinner Should Be Invisible