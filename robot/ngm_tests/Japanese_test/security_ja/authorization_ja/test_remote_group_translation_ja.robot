*** Settings ***
Documentation    Editing System > Preferences Test Suite for NodeGrid devices
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Run Keywords    GUI::Basic::Logout	Close all Browsers
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Check Options Translations Local
    GUI::Basic::Spinner Should Be Invisible
    @{LST}=     Create List     remoteGroup
    GUI::Basic::Check Translation No ID Previous Sibling    @{LST}

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Security::Authorization::Open Acct Remote Groups tab
