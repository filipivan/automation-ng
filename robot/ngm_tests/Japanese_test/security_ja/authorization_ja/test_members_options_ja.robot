*** Settings ***
Documentation    Test common behaviour in security ->password rules page
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup     Suite Setup
Suite Teardown  Run Keywords    GUI::Basic::Logout      Close all Browsers
Force Tags         GUI     ${BROWSER}     ${SYSTEM}       ${MODEL}        ${VERSION}    WINDOWS     CHROME  FIREFOX   EDGE     IE
Default Tags    EXCLUDEIN3_2


*** Variable ***
${USERNAME}	            admin
${PASSWORD}	            admin

*** Test Cases ***

Test Translation All Fields
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/thead/tr/th[2]
    GUI::Basic::Japanese::Test Translation With Value   jquery=#returnButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#addButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#delButton

Test Add Page Translations
    Click element   jquery=#addButton
    gui::basic::spinner should be invisible
    gui::basic::japanese::test translation with value  jquery=#saveButton
    gui::basic::japanese::test translation with value  jquery=#cancelButton
    gui::basic::japanese::test translation with value  jquery=#returnButton
    gui::basic::japanese::test translation  jquery=#memberAssign
    gui::basic::japanese::test translation  xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[1]/div[2]/div/div/div/label
    gui::basic::japanese::test translation  xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[1]/div[2]/div/div[1]/button
    gui::basic::japanese::test translation  xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[1]/div[2]/div/div[2]/button
    gui::basic::check translation no id previous sibling  memberRemote

Test Add and Delete
    gui::auditing::auto input tests   memberRemote   231/231/231/2341    aa_aa_aa_aa-aa-aa-aa
    gui::basic::spinner should be invisible
    click element   xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[2]/td[1]/input
    click element   jquery=#delButton
    gui::basic::spinner should be invisible
    ${CT}=  Get Element Count   xpath=//*[@id="tbody"]/tr
    Should Be Equal     '${CT}'     '1'

*** Keywords ***
Suite Setup
	${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
	GUI::Security::Authorization::Open Acct Members tab
	GUI::Basic::Spinner Should Be Invisible