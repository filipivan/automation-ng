*** Settings ***
Documentation    Test that the initialization of the settings is correct
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../init.robot

Suite Setup	        Suite Setup
Suite Teardown	    Suite Teardown
Force Tags	    GUI     ${BROWSER}       ${MODEL}        ${VERSION}    WINDOWS     CHROME  FIREFOX   EDGE     IE	EXCLUDEIN3_2

*** Test Cases ***

Test Services Translations
    run keyword and continue on failure  gui::basic::japanese::test translation  xpath=//*[@id="clusterServices"]
    run keyword and continue on failure  gui::basic::check translation no id        management_enabled  lps_enabled
    run keyword and continue on failure  gui::basic::japanese::test translation  xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[2]/div/div/div[2]/div/div/div[2]/div[2]/div/label
    run keyword and continue on failure  gui::basic::japanese::test translation  xpath=//*[@id="clusterServices"]
    Select Radio Button    lps_type     server
    run keyword and continue on failure  gui::basic::check translation no id previous sibling   renew_time  lease_time
    Select Radio Button    lps_type     client

Test Management Checkbox
    #Test that the checkbox works
    Select Checkbox     jquery=#management_enabled
    Click Element       jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    Checkbox Should Be Selected     jquery=#management_enabled
    Click Element     jquery=#management_enabled
    Click Element       jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    Checkbox Should Not Be Selected     jquery=#management_enabled

Test Server and Client Buttons
    #Test that the Server and Client buttons work and text boxes work
    Click Element       xpath=//div/div[1]/label/input[@id="lps_type"]
    Click Element       jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain Element     jquery=#renew_time
    Page Should Contain Element     jquery=#lease_time
    Execute Javascript	window.scrollTo(0,document.body.scrollHeight);

Test Renew Errors
    ${RAND}=    Evaluate    random.randint(1,7)     modules=random
    gui::auditing::auto input tests     renew_time      ${EMPTY}    0   aaaaa   8   ${RAND}

Test Lease Errors
    ${RAND}=    Evaluate    random.randint(7,14)     modules=random
    gui::auditing::auto input tests     lease_time      ${EMPTY}    0   aaaaa   1   ${RAND}

Test Auto Enrollment Settings
    #Test that the text box is working, no errors should be given i dont think
    #nodegrid-key <--important
    run keyword and continue on failure  gui::basic::japanese::test translation     xpath=//*[@id="clusterTitle"]
    Checkbox Should Be Selected     jquery=#autoEnroll
    run keyword and continue on failure  gui::basic::japanese::test translation     xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[1]/div/div/div[2]/div/div/div[1]/div[1]/label
    Page Should Contain Element     jquery=#autoPreSharedKey
    run keyword and continue on failure  gui::basic::japanese::test translation     xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[1]/div/div/div[2]/div/div/div[1]/div[2]/div/label
    #does it work?
    gui::auditing::text input error tests      autoPreSharedKey    ${FALSE}    .//./.a22sddd
    #reset it
    GUI::Auditing::Text Input Error Tests      autoPreSharedKey    ${FALSE}    nodegrid-key

Test Enable Cluster
    #Test settings under the 2_enable cluster
    #Enable clustering access=????? what do i test?
    #error on no settings picked
    Checkbox Should Not Be Selected     jquery=#enabled
    Click Element       jquery=#enabled
    run keyword and continue on failure     gui::basic::japanese::test translation     xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div[1]/label
    Text Input Error Tests      clusterName     ${FALSE}     myCluster

Test Cluster Translations
    run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/label
    run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div[2]/div[2]/label
    run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div[2]/div[2]/div/div[1]/label
    run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div[2]/div[2]/div/div[3]/label
    run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div[2]/div[3]/div/label
    run keyword and continue on failure     gui::basic::japanese::test translation      xpath=//*[@id="cluster_notice"]

Test Coordinator Base Settings
    #check base coordinator
    Click Element       xpath=//input[@value="master"]
    Click Element       jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain Element         xpath=//p[@class="bg-danger text-danger"]
    Checkbox Should Be Selected         jquery=#allowEnroll
    Page Should Contain Element         jquery=#preSharedKey
    Page Should Contain Element         jquery=#starMode
    Page Should Contain Element         jquery=#pollRate
    #Check the translations
    run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div[2]/div[2]/div/div[2]/div[2]/div/div/div[2]/label
    run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div[2]/div[2]/div/div[2]/div[2]/div/div/div[2]/div/div[1]/label
    run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div[2]/div[2]/div/div[2]/div[2]/div/div/div[2]/div/div[2]/label
    run keyword and continue on failure     gui::basic::check translation no id previous sibling    preSharedKey    pollRate


Test Coordinator Auto Enroll Settings
    #check enroll
    Click Element       jquery=#allowEnroll
    Element Should Not Be Visible     jquery=#preSharedKey
    Click Element       jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    ${TXT}=     Get Text    xpath=//p[@class="bg-danger text-danger"]
    Should Contain      ${TXT}      [
    Click Element       jquery=#allowEnroll
    GUI::Auditing::Text Input Error Tests      preSharedKey    ${FALSE}    thisIsMyKey

Test Coordinator Polling Rate
    #Coordinator: Everything besides polling rate is hunky dory. Check that error if <10 and >600
    #valid
    GUI::Auditing::Text Input Error Tests     pollRate      ${FALSE}     11
    GUI::Auditing::Auto Input Tests     pollRate     ${EMPTY}   9   601     asdfg   30

Test Peer Settings
    #Peer: Ip must be valid or error thrown
    Click Element       xpath=//input[@value="peer"]
    run keyword and continue on failure     gui::basic::check translation no id previous sibling    masterAddr  preSharedKey2
    #IP
    #input invalid ip address
    GUI::Auditing::Auto Input Tests      masterAddr     not.val.idi.pad    256.2.192.35     56.245.224.13
    #KEYS
    #input whatever
    GUI::Auditing::Text Input Error Tests      preSharedKey2   ${FALSE}     iDontKnowValOp
    Click Element               xpath=//input[@value="master"]
    Click Element               jquery=#allowEnroll
    GUI::Auditing::Text Input Error Tests      preSharedKey    ${FALSE}    nodegrid-key
    Wait Until Element Is Not Visible   xpath=//*[@id='loading']    timeout=20s
    Click Element               jquery=#saveButton


Test Automatic Enrollment Range Tab
    #Test that the tab is open and that the table works.
    #Errors thrown with invalid ips
        #ranges can be whatever you want
    GUI::Cluster::Open Automatic Enrollment Range
    #Check Translations Buttons
    run keyword and continue on failure     gui::basic::japanese::test translation  xpath=/html/body/div[7]/div[1]/a[1]/span
    run keyword and continue on failure     gui::basic::japanese::test translation  xpath=/html/body/div[7]/div[1]/a[2]/span
    #Check Add IP Range Translations
    run keyword and continue on failure     gui::basic::japanese::test translation with value  xpath=//*[@id="addButton"]
    run keyword and continue on failure     gui::basic::japanese::test translation with value  xpath=//*[@id="delButton"]
    run keyword and continue on failure     gui::basic::japanese::test translation  xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/thead/tr/th[2]
    Click element   jquery=#addButton
    run keyword and continue on failure     gui::basic::japanese::test translation with value  xpath=//*[@id="saveButton"]
    run keyword and continue on failure     gui::basic::japanese::test translation with value  xpath=//*[@id="cancelButton"]
    run keyword and continue on failure     gui::basic::check translation no id previous sibling    clusterRangeLo  clusterRangeHi
    Click Element   jquery=#cancelButton
    #test add valid
    run keyword and continue on failure     AER IP Input Text  12.12.12.12      12.12.12.192    ${FALSE}
    GUI::Basic::Spinner Should Be Invisible
    ${COUNT}=   GUI::Basic::Count Table Rows  peerTable
    Should be Equal     '${COUNT}'     '1'
    #test add blatant
    run keyword and continue on failure     AER IP Input Text  im.dnm.not.val   129.255.12.125  ${TRUE}
    #test add invalid ip
    run keyword and continue on failure     AER IP Input Text  12.12.12.245     124.355.124.21  ${TRUE}
    #test duplicate ip
    run keyword and continue on failure     AER IP Input Text  12.12.12.12      12.12.12.192    ${TRUE}
    #test new ip
    run keyword and continue on failure     AER IP Input Text  2.5.2.124        12.25.36.124    ${FALSE}
    GUI::Basic::Spinner Should Be Invisible
    ${COUNT}=   GUI::Basic::Count Table Rows  peerTable
    Should be Equal     '${COUNT}'     '2'


*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Cluster::Open Settings Tab

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers


Text Input Error Tests
    [Arguments]  ${LOCATOR}     ${EXPECTS}      ${TEXT_ENT}
    [Documentation]     locator wants the id of the element
    ...                 expects wants a t/f of whether an error should be thrown
    ...                 text_ent wants the input
    Execute Javascript  document.getElementById("${LOCATOR}").scrollIntoView(true);
    Input Text          jquery=#${LOCATOR}      ${empty}
    Input Text          jquery=#${LOCATOR}      ${TEXT_ENT}
    Execute Javascript	window.scrollTo(0,0);
    Wait Until Element Is Not Visible     xpath=//*[@id='loading']    timeout=20s
    Click Element       jquery=#saveButton
    Wait Until Element Is Not Visible     xpath=//*[@id='loading']    timeout=20s
#    Run Keyword if      ${EXPECTS}==${TRUE}     Page Should Contain Element     xpath=//p[@class="bg-danger text-danger"]
#    Page Should Not Contain Element       Page Should Not Contain Element       xpath=//p[@class="bg-danger text-danger"]
    Run Keyword if      ${EXPECTS}==${TRUE}     Page Should Contain Element     xpath=//*[@class='form-group row rwtext has-error']//input[@id='${LOCATOR}']
    Page Should Not Contain Element       Page Should Not Contain Element       xpath=//*[@class='form-group row rwtext has-error']//input[@id='${LOCATOR}']

AER IP Input Text
    [Arguments]  ${IP_ONE}     ${IP_TWO}      ${EXPECTS}
    [Documentation]     IP_ONE(TWO) expect the input for the two fields
    ...                 EXPECTS wants a t/f of whether an error should be thrown
    ${LOC}=     run keyword and return status  Page Should Contain Element      jquery=#cancelButton
    Run Keyword If      ${LOC}==${FALSE}    Click Element   jquery=#addButton
    Wait Until Element Is Not Visible   xpath=//*[@id='loading']    timeout=20s
    Input Text      jquery=#clusterRangeLo      ${IP_ONE}
    Input Text      jquery=#clusterRangeHi      ${IP_TWO}
    Click Element   jquery=#saveButton
    Wait Until Element Is Not Visible   xpath=//*[@id='loading']    timeout=20s
    ${TST}=     run keyword if  ${EXPECTS}     Get Text    xpath=/html/body/div[7]/div[2]/div[2]/form/div[1]/div/div/p
    run keyword if  ${EXPECTS}  Should Contain  ${TST}  [