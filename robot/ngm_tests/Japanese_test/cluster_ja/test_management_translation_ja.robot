*** Settings ***
Documentation    Test that the management tab works properly
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../init.robot

Suite Setup	        Suite Setup
Suite Teardown	    Suite Teardown
Force Tags	    GUI     ${BROWSER}       ${MODEL}        ${VERSION}    WINDOWS     CHROME  FIREFOX   EDGE     IE	EXCLUDEIN3_2

*** Test Cases ***

Test Table Display
    #Table Should Be Displayed
    GUI::Basic::Spinner Should Be Invisible
    run keyword and continue on failure     gui::basic::japanese::test translation  xpath=//*[@id="swupgrade"]
    Element Should Be Visible                   jquery=#peerMngtTable
    ${COUNT}=   GUI::Basic::Count Table Rows    jquery=#peerMngtTable
    should be equal                             '${COUNT}'    '0'
    Page Should Not Contain Element             xpath=//div[@id="notice"]
    :FOR    ${I}    IN RANGE    2   7
    \   run keyword and continue on failure  gui::basic::japanese::test translation  xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/thead/tr/th[${I}]


#Test Software Upgrade
    #DONT KNOW HOW TO TEST THIS ONE WITHOUT A VALID IP RANGE

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Cluster::Open Management Tab

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers