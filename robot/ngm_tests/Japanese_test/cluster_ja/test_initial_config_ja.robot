*** Settings ***
Documentation    Test that the default settings are correct
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../init.robot

Suite Setup	        Suite Setup
Suite Teardown	    Suite Teardown
Force Tags	    GUI     ${BROWSER}       ${MODEL}        ${VERSION}    WINDOWS     CHROME  FIREFOX   EDGE     IE	EXCLUDEIN3_2

*** Test Cases ***

Check Peers
    GUI::Cluster::Open Peers Tab
    run keyword and continue on failure  GUI::Basic::Japanese::Test Translation  xpath=/html/body/div[6]/div/ul/li[1]/a
    Page Should Contain Element    xpath=//div[@id="notice"]
    run keyword and continue on failure  gui::basic::japanese::test translation  xpath=//*[@id="notice"]

Check Default Settings
    GUI::Cluster::Open Settings Tab
    run keyword and continue on failure  gui::basic::japanese::test translation  xpath=/html/body/div[6]/div/ul/li[2]/a
    Checkbox Should Be Selected         jquery=#autoEnroll
    Checkbox Should Not Be Selected     jquery=#enabled
    Element Should Be Visible           jquery=#management_enabled
    Checkbox Should Not Be Selected     jquery=#management_enabled
    Element Should Be Visible           jquery=#lps_enabled
    ${IS_LPS_ENABLED}=	Run Keyword And Return Status	    Checkbox Should Be Selected 	//*[@id='lps_enabled']
    Run Keyword If	${IS_LPS_ENABLED}==${FALSE}	Fix Checkbox    //*[@id='lps_enabled']
    Select Radio Button    lps_type     client
    Click Element       jquery=#saveButton


Check Management
    GUI::Cluster::Open Management Tab
    run keyword and continue on failure  gui::basic::japanese::test translation  xpath=/html/body/div[6]/div/ul/li[3]/a
    Page Should Contain Element    xpath=//div[@id="notice"]
    run keyword and continue on failure  gui::basic::japanese::test translation  xpath=//*[@id="notice"]

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[5]
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers

Fix Checkbox
    [Arguments]  ${LOCATOR}
    Click Element       xpath=${LOCATOR}
    GUI::Basic::Save