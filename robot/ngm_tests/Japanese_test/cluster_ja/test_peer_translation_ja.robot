*** Settings ***
Documentation    Test that the peer tab works properly
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../init.robot

Suite Setup	        Suite Setup
Suite Teardown	    Suite Teardown
Force Tags	    GUI     ${BROWSER}       ${MODEL}        ${VERSION}    WINDOWS     CHROME  FIREFOX   EDGE     IE	EXCLUDEIN3_2

*** Test Cases ***

Test Peer Table Display
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be Visible                   jquery=#peerTable
    Page Should Not Contain Element             xpath=//div[@id="notice"]
    run keyword and continue on failure  gui::basic::japanese::test translation with value  xpath=//*[@id="delButton"]

Test Table Translation
    run keyword and continue on failure  gui::basic::japanese::test translation table header with checkbox  6
    :FOR    ${I}    IN RANGE   3   6
    \   run keyword and continue on failure     gui::basic::japanese::test translation  xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[${I}]

Test Deleting Main Item
    #Should throw error and complain about not being able to delete NG.localdomain and need to go to the settings or something
    Click Element                   xpath=//th[@class="sorting_disabled selectable"]
    Click Element                   jquery=#delButton
    gui::basic::spinner should be invisible
    ${TXT}=     Get Text     jquery=#main_doc > div:nth-child(1) > div > div
    Should Contain  ${TXT}  [

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
	GUI::Cluster::Open Peers Tab

Suite Teardown
    GUI::Cluster::Open Automatic Enrollment Range
    Click Element       xpath=//*[@id="thead"]/tr/th[1]/input
    Click Element       jquery=#delButton
    GUI::Basic::Spinner Should Be Invisible
    GUI::Cluster::Open Settings Tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element       jquery=#enabled
    Click Element       xpath=//div/div[4]/label/input[@id="lps_type"]
    Click Element       jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Logout
	Close all Browsers