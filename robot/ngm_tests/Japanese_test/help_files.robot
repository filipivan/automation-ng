*** Settings ***
Documentation    Successfully Display the Help Files
Resource    init.robot
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}

Suite Teardown      Run Keywords    GUI::Basic::Logout  Close all Browsers
Force Tags         GUI    ${SYSTEM}   ${BROWSER}    ${MODEL}     ${VERSION}     WINDOWS     CHROME  FIREFOX   EDGE


*** Variables ***


*** Test Cases ***

Display Help Details
    GUI::Basic::Open NodeGrid   ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    GUI::Basic::Login       ${NGVERSION}    admin       admin
    GUI::Basic::Help

*** Keywords ***
Provided precondition
    Setup system under test