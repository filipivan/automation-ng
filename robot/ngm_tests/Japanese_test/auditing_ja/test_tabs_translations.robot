*** Settings ***
Documentation    Editing System > Preferences Test Suite for NodeGrid devices
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON_CRITICAL

*** Test Cases ***

Check Translation Main
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[6]/ul[1]/li[8]/a/span

Check Settings Translation
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[6]/div/ul/li[1]/a

Check Events Translation
    gui::basic::japanese::test translation      xpath=/html/body/div[6]/div/ul/li[2]/a

Check Destinations Translation
    gui::basic::japanese::test translation      xpath=/html/body/div[6]/div/ul/li[3]/a

Check File Translation
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[1]/a[1]/span

Check SYSLOG Translation
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[1]/a[2]/span

Check SNMP Translation
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[1]/a[3]/span

Check Email Translation
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[1]/a[4]/span

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Auditing::Open SNMPTrap Tab

Suite Teardown
    Click element       //*[@value='2c']
    Click Element       jquery=#saveButton
    GUI::Basic::Logout
    Close all Browsers
