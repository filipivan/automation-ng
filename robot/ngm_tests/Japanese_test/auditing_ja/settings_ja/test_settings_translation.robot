*** Settings ***
Documentation    Editing System > Preferences Test Suite for NodeGrid devices
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON_CRITICAL

*** Test Cases ***

Check Translation Page Title
    GUI::Basic::Japanese::Test Translation      xpath=//*[@id="eventglobal"]

Check Save Button Translation
    GUI::Basic::Japanese::Test Translation With Value   jquery=#saveButton

Check Eventglobal Radio Translation
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[1]/div[2]/div/div/div/label
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[1]/label
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[2]/label
    GUI::Basic::Check Radio Button Translation      evttimeType

Check Data Logging Title Translation
    GUI::Basic::Japanese::Test Translation      xpath=//*[@id="datalogging"]

Check Checkbox Translations
    @{IDS}=     Create List     datalogTypeF    datalogTypeS    timestamp
    GUI::Basic::Check Translation No ID     @{IDS}

Check Data Logging Radio Translation
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[2]/div[2]/div/div/div[4]/label
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[2]/div[2]/div/div/div[4]/div/div[1]/label
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[2]/div[2]/div/div/div[4]/div/div[2]/label

Test Error Location
    Unselect Checkbox       jquery=#datalogTypeF
    Click Element           jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[1]/div/div/p

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Auditing::Open

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers

