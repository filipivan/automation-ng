*** Settings ***
Documentation    Editing System > Preferences Test Suite for NodeGrid devices
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Run Keywords    GUI::Basic::Logout	Close all Browsers
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***
Test Table Translation
    :FOR    ${I}    IN RANGE    1   6
    \   GUI::Basic::Japanese::Test Translation    xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/thead/tr/th[${I}]
    :FOR    ${C}    IN RANGE    1   5
    \   Table Loop Through For Translate      xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[${C}]
    \   Event Persistance Checking      ${C}

Check Options Translations
    Click Element       xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[1]/td[1]/a
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value   jquery=#saveButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#cancelButton
    GUI::Basic::Japanese::Test Translation              jquery=#event_category
    @{LST}=     Create List     cat1    cat2    cat3    cat4
    Check Translation No ID     @{LST}


*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Auditing::Open Events tab

Table Loop Through For Translate
    [Arguments]     ${LOC}
    :FOR    ${I}    IN RANGE    1   6
    \   run keyword if  '${I}'=='1'     GUI::Basic::Japanese::Test Translation      ${LOC}/td[${I}]/a
    \   ...     ELSE    GUI::Basic::Japanese::Test Translation      ${LOC}/td[${I}]

Check Translation No ID
    [Arguments]     @{IDS}
    [Documentation]     Gets the Id of the sibling element and uses it to figure out if translated
    ${LEN_T}=     Get Length      ${IDS}
    :FOR    ${I}    IN RANGE    0   ${LEN_T}
    \       ${LOC}=     Get From List   ${IDS}  ${I}
    \       ${element}=     Execute Javascript          return document.getElementById('${LOC}').parentNode;
    \       ${VAL_EL}=      Get Text                    ${element}
    \       Should Contain      ${VAL_EL}       [


Check Change Events
    [Arguments]     ${ROW}  ${SBY}
    [Documentation]     ROW is the row number that needs to be checked
    ...                 SBY is a boolean on whether the values should be Yes or -
    :FOR    ${I}    IN RANGE    2   6
    \   ${TXT}=     Get Text     xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[${ROW}]/td[${I}]
    \   Run Keyword If      ${SBY}      Should Be Equal     ${TXT}  	[Yes 擬]
    \   ...     ELSE    Should Be Equal     ${TXT}  	[- ]

Drilldown and Change Events
    [Arguments]     ${ROW}
    [Documentation]     ROW is the row number that needs to be changed
    Click Element   xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[${ROW}]/td[1]/a
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=#cat1
    Click Element   jquery=#cat2
    Click Element   jquery=#cat3
    Click Element   jquery=#cat4
    Click Element   jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible

Event Persistance Checking
    [Arguments]     ${ROW}
    [Documentation]     ROW is the row number that needs to be changed
    ${CHK}=     Get Text    xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[${ROW}]/td[2]
    ${INI}=     Run Keyword and Return Status   Should Be Equal     ${CHK}      [Yes 擬]
    ${FIN}=     Run Keyword If  ${INI}  Set Variable    ${FALSE}    ELSE    Set Variable    ${TRUE}
    Check Change Events     ${ROW}      ${INI}
    Drilldown and Change Events         ${ROW}
    Check Change Events     ${ROW}      ${FIN}
    Drilldown and Change Events         ${ROW}
    Check Change Events     ${ROW}      ${INI}