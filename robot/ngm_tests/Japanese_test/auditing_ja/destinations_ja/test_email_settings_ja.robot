*** Settings ***
Documentation    Editing System > Preferences Test Suite for NodeGrid devices
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Test Translations
    GUI::Basic::Japanese::Test Translation With Value   jquery=#saveButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#testemail
    @{TRAN}=    Create List     emailServer     emailPort   emailUser   emailPass   emailPassAgain  destinationEmail
    Check Translation No ID Previous Sibling    @{TRAN}
    @{LONELY}=  Create List     startTLS
    Check Translation No ID     @{LONELY}

Test Email Server Name
    @{TESTS}=   Create List     wrong/      doesn\\twork    not''real   th[]no    still..no   this.works
    GUI::Auditing::Auto Input Tests     emailServer  @{TESTS}

Test Email Port
    ${RAND}=    Evaluate    random.randint(1,255)       modules=random
    @{TESTS_2}=   Create List     12.4  .   aaaa     2\\24     24/1    ${RAND}
    GUI::Auditing::Auto Input Tests     emailPort  @{TESTS_2}

Test Passwords
    Check Passwords  one        two                 emailPass   emailPassAgain  ${TRUE}
    Check Passwords  .//./##    ../..//.[]././      emailPass   emailPassAgain  ${TRUE}
    Check Passwords  itsnot     thesame             emailPass   emailPassAgain  ${TRUE}
    check passwords  two        two                 emailPass   emailPassAgain  ${FALSE}

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Auditing::Open Email Tab

Suite Teardown
    Input Text  jquery=#emailServer     ${EMPTY}
    Input Text  jquery=#emailPort       25
    Input Text  jquery=#emailPass       aaaaaa
    Input Text  jquery=#emailPassAgain  aaaaaa
    Click Element   jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Logout
    Close all Browsers

Check Passwords
    [Arguments]  ${P_ONE}   ${P_TWO}    ${LOCATION_ONE}     ${LOCATION_TWO}     ${EXPECTS}
    Input Text  ${LOCATION_ONE}     ${EMPTY}
    Input Text  ${LOCATION_ONE}     ${P_ONE}
    Input Text  ${LOCATION_TWO}     ${EMPTY}
    Input Text  ${LOCATION_TWO}     ${P_TWO}
    Click Element                   jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    ${TST}=     Execute Javascript  var er=document.getElementById('${LOCATION_ONE}').parentNode.nextSibling;if(er==null){return false;}var txt=er.textContent;return txt.includes("[");
    Log     ${TST}
    Run Keyword If      ${EXPECTS}==${TRUE}     Should Be True     ${TST}
    ...     ELSE    Should Not Be True     ${TST}

Check Translation No ID
    [Arguments]     @{IDS}
    [Documentation]     Gets the Id of the sibling element and uses it to figure out if translated
    ${LEN_T}=     Get Length      ${IDS}
    :FOR    ${I}    IN RANGE    0   ${LEN_T}
    \       ${LOC}=     Get From List   ${IDS}  ${I}
    \       ${element}=     Execute Javascript          return document.getElementById('${LOC}').parentNode;
    \       ${VAL_EL}=      Get Text                    ${element}
    \       Should Contain      ${VAL_EL}       [

Check Translation No ID Previous Sibling
    [Arguments]     @{IDS}
    [Documentation]     Gets the Id of the sibling element and uses it to figure out if translated
    ${LEN_T}=     Get Length      ${IDS}
    :FOR    ${I}    IN RANGE    0   ${LEN_T}
    \       ${LOC}=     Get From List   ${IDS}  ${I}
    \       ${element}=     Execute Javascript          return document.getElementById('${LOC}').parentNode.previousSibling;
    \       ${VAL_EL}=      Get Text                    ${element}
    \       Should Contain      ${VAL_EL}       [