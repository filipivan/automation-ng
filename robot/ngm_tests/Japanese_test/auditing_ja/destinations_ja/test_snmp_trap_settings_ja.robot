*** Settings ***
Documentation    Editing System > Preferences Test Suite for NodeGrid devices
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Test Trap Server
    ${RND}=     Evaluate    random.randint(1,256)   modules=random
    @{TESTS_1}=   Create List     1..245.2.1  .   12.53.12.53/2   12/25\\12#24     1.1.1.${RND}
    GUI::Auditing::Auto Input Tests     trapServer  @{TESTS_1}

Test Trap Protocol
    #Sleep       20s
    @{PROT}=        Get List Items      jquery=#trapProtocol
    :FOR    ${I}    IN   @{PROT}
    \   Should Contain  ${I}    [   msg='${I}' is not translated

Test Trap Port
    ${RND}=     Evaluate    random.randint(1,256)   modules=random
    Log     ${RND}
    @{TESTS_2}=   Create List     12.4  .   aaaa     2\\24     24/1    ${RND}
    GUI::Auditing::Auto Input Tests     trapPort  @{TESTS_2}

Test Version 2c
    Click Element   //input[@value='2c']
    Execute Javascript  document.getElementById("trapCommunity").scrollIntoView(true);
    Element Should Be Visible       //input[@id='trapCommunity']

Test Version 3 Display
    Click Element   //input[@value='3']
    @{CHECKS}=  Create List     trapSecName     trapSecLevel    trapAuthType    trapAuthPass    trapPrivType    trapPrivPass
    :FOR    ${I}    IN  @{CHECKS}
    \       Execute Javascript  document.getElementById("${I}").scrollIntoView(true);
    \       Element Should Be Visible       //*[@id='${I}']

Test Username
    @{TESTS_3}=   Create List     not/valid  a'm'i'   sec/nonam][   test#works   a\\s\\     this.valid
    GUI::Auditing::Auto Input Tests     trapSecName  @{TESTS_3}

Test Trap Security
    @{SECL}=        Get List Items      jquery=#trapSecLevel
    :FOR    ${I}    IN  @{SECL}
    \   Should Contain  ${I}    [   msg='${I}' is not translated

Test Auth and Priv Types
    @{PROT_A}=        Get List Items      jquery=#trapAuthType
    :FOR    ${I}    IN  @{PROT_A}
    \   Should Contain  ${I}    [   msg='${I}' is not translated
    @{PROT_P}=        Get List Items      jquery=#trapPrivType
    :FOR    ${I}    IN  @{PROT_P}
    \   Should Contain  ${I}    [   msg='${I}' is not translated

Test Auth Password
    @{TESTS_4}=   Create List     short     \#thisaintit    passwordYES
    GUI::Auditing::Auto Input Tests     trapAuthPass  @{TESTS_4}

Test Priv Password
    @{TESTS_5}=   Create List     short     \#thisaintit    passwordYES
    GUI::Auditing::Auto Input Tests     trapPrivPass  @{TESTS_5}

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Auditing::Open SNMPTrap Tab

Suite Teardown
    Click element       //*[@value='2c']
    Click Element       jquery=#saveButton
    GUI::Basic::Logout
    Close all Browsers


#var er=document.getElementById('${LOCATOR}').parentNode.nextSibling;if(er==null){return false;}var txt=er.textContent;return txt.includes("[");