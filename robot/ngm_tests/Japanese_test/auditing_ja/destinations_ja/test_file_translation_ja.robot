*** Settings ***
Documentation    Editing System > Preferences Test Suite for NodeGrid devices
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Run Keywords    GUI::Basic::Logout	Close all Browsers
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Check Options Translations Local
    GUI::Basic::Japanese::Test Translation With Value   jquery=#saveButton
    Select From List By Value   //*[@id="filetype"]     local
    gui::basic::japanese::test translation      jquery=#local
    @{LST}=     Create List     local_size   local_segments    local_close_time     filetype
    Check Translation No ID Previous Sibling    @{LST}
    @{LST_DRP}=     Get List Items      jquery=#filetype
    :FOR    ${ITEM}     IN      @{LST_DRP}
    \   run keyword and continue on failure     Should Contain  ${ITEM}     [

Test Local Selection
    Select From List By Value   //*[@id="filetype"]     local
    Element Should Be Visible   //*[@id="local_size"]

Test File Size
    ${RAND}=    evaluate    random.randint(1,2000)  modules=random
    @{TEXTS}=   Create List     2049    9999     -1    ${RAND}
    GUI::Auditing::Auto Input Tests            local_size      @{TEXTS}

Test Number of Archives
    ${RAND}=    evaluate    random.randint(1,10)  modules=random
    @{TEXTS}=   Create List     0    a    ${RAND}
    GUI::Auditing::Auto Input Tests            local_segments      @{TEXTS}

Test Archive by Time
    ${RAND}=    evaluate    random.randint(1,60)  modules=random
    @{TEXTS}=   Create List     aaaaa    25:01     15:${RAND}
    GUI::Auditing::Auto Input Tests            local_close_time      @{TEXTS}
    #reset the values
    Input Text      jquery=#local_size      256
    Input Text      jquery=#local_segments  1
    Input Text      jquery=#local_close_time    ${EMPTY}
    Click Element   jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible

Test NFS Selection
    Select From List By Value   //*[@id="filetype"]     nfs
    Element Should Be Visible   //*[@id="nfs_server"]
    GUI::Basic::Japanese::Test Translation      jquery=#nfs
    GUI::Basic::Japanese::Test Translation              jquery=#nfs
    @{LST}=     Create List     nfs_server   nfs_path    nfs_size     nfs_segments  nfs_close_time
    Check Translation No ID Previous Sibling    @{LST}
    GUI::Basic::Japanese::Test Translation              jquery=#nfs_notice

Test Server IP
    ${RAND}=    evaluate    random.randint(1,256)  modules=random
    @{TEXTS}=   Create List     .//./##    127.0.0.${RAND}
    GUI::Auditing::Auto Input Tests            nfs_server      @{TEXTS}

Test NFS File Size
    ${RAND}=    evaluate    random.randint(1,2000)  modules=random
    @{TEXTS}=   Create List     -1    big    ${RAND}
    GUI::Auditing::Auto Input Tests            nfs_size      @{TEXTS}

Test Number of NFS Archives
    ${RAND}=    evaluate    random.randint(1,10)  modules=random
    @{TEXTS}=   Create List     a    -1    ${RAND}
    GUI::Auditing::Auto Input Tests            nfs_segments      @{TEXTS}

Test NFS Archive by Time
    ${RAND}=    evaluate    random.randint(1,60)  modules=random
    @{TEXTS}=   Create List     aaaaa    25:01     15:${RAND}
    GUI::Auditing::Auto Input Tests            nfs_close_time      @{TEXTS}
    #reset fields
    Input Text  jquery=#nfs_server  ${EMPTY}
    Input Text  jquery=#nfs_size    1024
    Input Text  jquery=#nfs_segments    1
    Input Text  jquery=#nfs_close_time  ${EMPTY}
    Click Element   jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Auditing::Open File Tab

Table Loop Through For Translate
    [Arguments]     ${LOC}
    :FOR    ${I}    IN RANGE    1   6
    \   run keyword if  '${I}'=='1'     GUI::Basic::Japanese::Test Translation      ${LOC}/td[${I}]/a
    \   ...     ELSE    GUI::Basic::Japanese::Test Translation      ${LOC}/td[${I}]

Check Translation No ID
    [Arguments]     @{IDS}
    [Documentation]     Gets the Id of the sibling element and uses it to figure out if translated
    ${LEN_T}=     Get Length      ${IDS}
    :FOR    ${I}    IN RANGE    0   ${LEN_T}
    \       ${LOC}=     Get From List   ${IDS}  ${I}
    \       ${element}=     Execute Javascript          return document.getElementById('${LOC}').parentNode;
    \       ${VAL_EL}=      Get Text                    ${element}
    \       Should Contain      ${VAL_EL}       [

Check Translation No ID Previous Sibling
    [Arguments]     @{IDS}
    [Documentation]     Gets the Id of the sibling element and uses it to figure out if translated
    ${LEN_T}=     Get Length      ${IDS}
    :FOR    ${I}    IN RANGE    0   ${LEN_T}
    \       ${LOC}=     Get From List   ${IDS}  ${I}
    \       ${element}=     Execute Javascript          return document.getElementById('${LOC}').parentNode.previousSibling;
    \       ${VAL_EL}=      Get Text                    ${element}
    \       Should Contain      ${VAL_EL}       [