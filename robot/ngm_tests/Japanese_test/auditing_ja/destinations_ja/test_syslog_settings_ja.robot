*** Settings ***
Documentation    Editing System > Preferences Test Suite for NodeGrid devices
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***
Check Permanents
    @{EVENTS_L}=    Get List Items      jquery=#eventfacility
    @{DATA_L}=      Get List Items      jquery=#datafacility
    :FOR    ${I}    IN      @{EVENTS_L}
    \   Should Contain      ${I}    [   msg='${I}' is not translated
    :FOR    ${I}    IN      @{DATA_L}
    \   Should Contain      ${I}    [   msg='${I}' is not translated
    @{LST}=     create list     syslogConsole   syslogRootuser  syslogRemote    syslogRemote6
    Check Translation No ID Parent      @{LST}
    GUI::Basic::Check Translation No ID Previous Sibling  eventfacility     datafacility

Test IPv4 Settings
    ${IS_SETTING_ENABLED}=	Run Keyword And Return Status	Checkbox Should Be Selected	    //*[@id="syslogRemote"]
    Run Keyword If	${IS_SETTING_ENABLED}==${FALSE}	Select Checkbox	    jquery=#syslogRemote
    Element Should Be Visible   jquery=#syslogRemoteIP
    @{TEST_F}=      Create List    .....   .//./##     123.245.234.321     127.0.0.2
    GUI::Auditing::Auto Input Tests    syslogRemoteIP   @{TEST_F}
    @{LST}=     Create List     syslogRemoteIP
    Check Translation No ID Previous Sibling      @{LST}
    gui::basic::japanese::test translation      jquery=#syslogRemoteNotes

Test IPv6 Settings
    ${IS_SETTING_ENABLED}=	Run Keyword And Return Status	Checkbox Should Be Selected	    //*[@id="syslogRemote6"]
    Run Keyword If	${IS_SETTING_ENABLED}==${FALSE}	Select Checkbox	    jquery=#syslogRemote6
    Element Should Be Visible   jquery=#syslogRemoteIP6
    @{TEST_S}=      Create List    .....   .//./##     123.245.234.321     127.0.0.2    293d:29g2::000a     002d::2491
    GUI::Auditing::Auto Input Tests    syslogRemoteIP6   @{TEST_S}
    @{LST}=     Create List     syslogRemoteIP6
    Check Translation No ID Previous Sibling      @{LST}
    gui::basic::japanese::test translation      jquery=#syslogRemoteNotes6

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    GUI::Basic::Login	${NGVERSION}
    GUI::Auditing::Open Syslog Tab

Suite Teardown
    ${IS_SETTING_ENABLED}=	Run Keyword And Return Status	Checkbox Should Not Be Selected	    //*[@id="syslogRemote"]
    Run Keyword If	${IS_SETTING_ENABLED}==${FALSE}	Unselect Checkbox	    jquery=#syslogRemote
    Checkbox Should Not Be Selected     //*[@id="syslogRemote"]
    ${IS_SETTING_ENABLED}=	Run Keyword And Return Status	Checkbox Should Not Be Selected	    //*[@id="syslogRemote6"]
    Run Keyword If	${IS_SETTING_ENABLED}==${FALSE}	Unselect Checkbox	    jquery=#syslogRemote6
    Checkbox Should Not Be Selected     //*[@id="syslogRemote6"]
    Click Element       jquery=#saveButton
    GUI::Basic::Logout
    Close all Browsers

Check Translation No ID Parent
    [Arguments]     @{IDS}
    [Documentation]     Gets the Id of the sibling element and uses it to figure out if translated
    ${LEN_T}=     Get Length      ${IDS}
    :FOR    ${I}    IN RANGE    0   ${LEN_T}
    \       ${LOC}=     Get From List   ${IDS}  ${I}
    \       ${element}=     Execute Javascript          return document.getElementById('${LOC}').parentNode;
    \       ${VAL_EL}=      Get Text                    ${element}
    \       Should Contain      ${VAL_EL}       [   msg='${VAL_EL}' is not translated

Check Translation No ID Previous Sibling
    [Arguments]     @{IDS}
    [Documentation]     Gets the Id of the sibling element and uses it to figure out if translated
    ${LEN_T}=     Get Length      ${IDS}
    :FOR    ${I}    IN RANGE    0   ${LEN_T}
    \       ${LOC}=     Get From List   ${IDS}  ${I}
    \       ${element}=     Execute Javascript          return document.getElementById('${LOC}').parentNode.previousSibling;
    \       ${VAL_EL}=      Get Text                    ${element}
    \       Should Contain      ${VAL_EL}       [   msg='${VAL_EL}' is not translated