*** Settings ***
Documentation    Testing if Errors in the Managed Devieces -> Auto Discovery -> Hostname Detection are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2


*** Test Cases ***


Test Global Setting Probe Legth Error
    click element   jquery=#discNameGlobalSet
    GUI::Basic::Spinner Should Be Invisible
    Input Text          //*[@id="probetmo"]      123456
    GUI::Basic::Save
    Suite Check Error Translations

Test Global Setting Probe Invalid Error
    Input Text          //*[@id="probetmo"]      **
    GUI::Basic::Save
    Suite Check Error Translations

Test Global Setting Retries Legth Error
    Input Text          //*[@id="probetmo"]      10
    Input Text          //*[@id="nretries"]      123456
    GUI::Basic::Save
    Suite Check Error Translations

Test Global Setting Retries Invalid Error
    Input Text          //*[@id="nretries"]      **
    GUI::Basic::Save
    Suite Check Error Translations


*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    gui::basic::spinner should be invisible
    GUI::ManagedDevices::Open Hostname Detection

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers

Suite Check Error Translations
    GUI::Basic::Japanese::Test Translation      //*[@id="errormsg"]
   # GUI::Basic::Japanese::Test Translation      //*[@id="globalerr"]/div/div/p