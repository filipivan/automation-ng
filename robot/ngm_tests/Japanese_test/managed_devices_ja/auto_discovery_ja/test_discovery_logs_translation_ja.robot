*** Settings ***
Documentation    Testing if Elements in the Managed Devieces -> Auto Discovery -> Discovery Logs are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Test Button and Header
    GUI::Basic::Japanese::Test Translation With Value  jquery=#reset_logs
    Click Element   jquery=#reset_logs
    GUI::Basic::Spinner Should Be Invisible
    Page Should Not Contain Element     xpath=//*[@id="tbody"]/tr

Test Table Header Translation
    gui::basic::japanese::test translation table header  5

*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    gui::basic::spinner should be invisible
    GUI::ManagedDevices::Open Discovery Logs

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers