*** Settings ***
Documentation    Testing if Errors in the Managed Devieces -> Auto Discovery are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2


*** Test Cases ***

Test Add With No Name
    GUI::Basic::Add
    Click Element       //*[@id="operation"]
    Click Element       //*[@id="operation"]/option[4]
    GUI::Basic::Save
    Suite Check Error Translations

Test Add With Invalid Name
    Input Text  jquery=#discovery_name   **
    GUI::Basic::Save
    Suite Check Error Translations

Test Add With Invalid MAC Address
    Input Text  jquery=#discovery_name   Dummy
    Select Radio Button     source  dhcp
    Input Text  jquery=#macaddr   **
    GUI::Basic::Save
    Suite Check Error Translations

Test Add With Invalid Console Server Port List
    Select Radio Button     source  consoleserver
    Input Text  jquery=#portlistcas   **
    GUI::Basic::Save
    Suite Check Error Translations

Test Add With Invalid KVM Port List
    Select Radio Button     source  kvmport
    Input Text  jquery=#portlistkvm   **
    GUI::Basic::Save
    Suite Check Error Translations


*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    gui::basic::spinner should be invisible
    GUI::ManagedDevices::Auto::Open Auto Discovery Tab

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers


Suite Check Error Translations
    GUI::Basic::Japanese::Test Translation      //*[@id="errormsg"]
   # GUI::Basic::Japanese::Test Translation      //*[@id="globalerr"]/div/div/p