*** Settings ***
Documentation    Testing if Elements in the Managed Devieces -> Auto Discovery :: Network Scan are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2


*** Test Cases ***

Add Dummy Device
    GUI::ManagedDevices::Open
    ${count}=	GUI::Basic::Count Table Rows	SPMTable
	Run Keyword If	${count} > 0	Log	"Device already exists"
	Run Keyword If	${count} == 0	Add Missing Device

Test Buttons Translation
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Open Network Scan
    GUI::Basic::Spinner Should Be Invisible
    gui::basic::japanese::test translation with value  jquery=#addButton
    gui::basic::japanese::test translation with value  jquery=#delButton

Test Table Translation
    gui::basic::japanese::test translation table header with checkbox  8

Test Add Element Translation
    Click element   jquery=#addButton
    gui::basic::spinner should be invisible
    gui::basic::japanese::test translation with value  jquery=#saveButton
    gui::basic::japanese::test translation with value  jquery=#cancelButton
    gui::basic::check translation no id previous sibling  netdiscover_id    ipfirst     iplast  seed    portlist    interval
    gui::basic::check translation no id  enabled    morelikethis    portscan    ping
    gui::basic::japanese::test translation  jquery=#noticeportlist

Test Text Fields
    ${RAND}=    evaluate  random.randint(1,256)     modules=random
    gui::auditing::auto input tests     netdiscover_id      ,./;'[]\    not/work    sti]]no     wor.ks
    gui::auditing::auto input tests     ipfirst             345.345.345.345     12.1212.12.12   notanipaddress  123.123.123.${RAND}
    gui::auditing::auto input tests     iplast              345.345.345.345     12.1212.12.12   notanipaddress  123.123.123.${RAND}
    GUI::Basic::Spinner Should Be Invisible
    Wait until element is visible   xpath=//*[@id="wor.ks"]
    Click Element   xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[2]/a
    GUI::Basic::Spinner Should Be Invisible
    gui::auditing::auto input tests     portlist            ./      12.3    12/13   3:8     ${RAND}
    GUI::Basic::Spinner Should Be Invisible
    Wait until element is visible   xpath=//*[@id="wor.ks"]
    Click Element   xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[2]/a
    GUI::Basic::Spinner Should Be Invisible
    gui::auditing::auto input tests     interval            ./      12.3    12/13   3-8     ${RAND}

Add Network Scan
    Execute Javascript	window.scrollTo(0,document.body.scrollHeight);
    GUI::Basic::Wait Until Element Is Accessible	jquery=body > div.main_menu > div > ul > li:nth-child(4)
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(4)
	Wait Until Element Is Visible       jquery=#addButton
	GUI::ManagedDevices::Delete Network Scan	dummy_network
	GUI::ManagedDevices::Add Dummy Network Scan	dummy_network	127.0.0.1	127.0.0.5

Edit Device
    GUI::ManagedDevices::Enter Network Scan	dummy_network
	Input Text	jquery=#ipfirst	127.0.0.6
	Input Text	jquery=#iplast	127.0.0.10
	Input Text	jquery=#portlist	28-30,625
	Input Text	jquery=#interval	10
	GUI::Basic::Save

	Click Element	jquery=a#dummy_network
	GUI::Basic::Wait Until Element Is Accessible	jquery=#interval
	${IPFIRST}=	Get Value	jquery=#ipfirst
	${IPLAST}=	Get Value	jquery=#iplast
	${PORTLIST}=	Get Value	jquery=#portlist
	${INTERVAL}=	Get Value	jquery=#interval
	Should Be Equal	${IPFIRST}	127.0.0.6
	Should Be Equal	${IPLAST}	127.0.0.10
	Should Be Equal	${PORTLIST}	28-30,625
	Should Be Equal	${INTERVAL}	10

Delete device
	GUI::ManagedDevices::Delete Network Scan	dummy_network
	GUI::ManagedDevices::Add Dummy Network Scan	dummy_network	127.0.0.1	127.0.0.5
	GUI::ManagedDevices::Delete Network Scan	dummy_network

Delete devices
	GUI::ManagedDevices::Add Dummy Network Scan	dummy_network	127.0.0.1	127.0.0.5
	GUI::ManagedDevices::Add Dummy Network Scan	dummy_network2	127.0.0.6	127.0.0.10
	GUI::ManagedDevices::Delete Network Scan	dummy_network	dummy_network2
	Page Should Not Contain Element	id=dummy_network
	Page Should Not Contain Element	id=dummy_network2

*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    gui::basic::spinner should be invisible
    GUI::ManagedDevices::Open Network Scan

Suite Teardown
    Click element   xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[1]/input
    Click Element  jquery=#delButton
    Handle Alert
    GUI::Basic::Logout
    Close all Browsers

Add Missing Device
    Wait Until Element Is Visible       jquery=#addButton
    Click Element   jquery=#addButton
    Wait Until Element Is Visible       jquery=#cancelButton
    Input Text      xpath=//input[@id="spm_name"]       aaaa
    Wait Until Element Is Visible       jquery=#saveButton
    Click Element   jquery=#saveButton