*** Settings ***
Documentation    Testing if Elements in the Managed Devieces -> Auto Discovery -> VM Managers are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2


*** Test Cases ***

Test Buttons Translation
    gui::basic::japanese::test translation with value  jquery=#addButton
    gui::basic::japanese::test translation with value  jquery=#delButton

Test Table Translation
    gui::basic::japanese::test translation table header with checkbox  5

Test Add Element Translation
    Click element   jquery=#addButton
    gui::basic::spinner should be invisible
    gui::basic::japanese::test translation with value  jquery=#saveButton
    gui::basic::japanese::test translation with value  jquery=#cancelButton
    gui::basic::check translation no id previous sibling  vmmanager     username    fpasswd     cpasswd     type    mkswebport

Test Dropdown Menu Translations
    test dropdown options  type
    Click element   jquery=#cancelButton
    GUI::Basic::Spinner Should Be Invisible

Test Add VM
    Input Text  jquery=#vmmanager   ----
    GUI::Auditing::Auto Input Tests     mkswebport  1-1     9999999999  str     123
    gui::auditing::auto input tests     vmmanager   @#$     -   vm/manager  vm,manager  vmmanager
    GUI::Basic::Spinner Should Be Invisible

Test Drilldown VM
    Click Element       xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[2]/a
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div/div
    GUI::Basic::Check Translation No ID         jquery=#vmdiscovery
    GUI::Basic::Select Checkbox                 jquery=#vmdiscovery
    GUI::Basic::Spinner Should Be Invisible
    gui::basic::check translation no id previous sibling    jquery=#interval

Test Interval Field Errors
    gui::auditing::auto input tests     interval    str     -1      12.3    1000
    Click Element       xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[2]/a
    GUI::Basic::Spinner Should Be Invisible

Test Interval Translations
    gui::basic::japanese::test translation      jquery=.parameter_title
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[7]/div[2]/div[2]/div[2]/div/div/div/div[2]/div[1]/span
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[7]/div[2]/div[2]/div[2]/div/div/div/div[2]/div[2]/button[1]
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[7]/div[2]/div[2]/div[2]/div/div/div/div[2]/div[2]/button[2]
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[7]/div[2]/div[2]/div[2]/div/div/div/div[2]/div[3]/span
    Click Element   jquery=#cancelButton

Test Delete VM
    Select Checkbox     xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[1]/input
    Click element   jquery=#delButton
    Handle Alert
    GUI::Basic::Spinner Should Be Invisible

*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    gui::basic::spinner should be invisible
    GUI::ManagedDevices::Open VM Manager

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers

Test Dropdown Options
    [Arguments]  ${LOC}
    @{TXT}=     Get List Items  jquery=#${LOC}
    :FOR    ${I}    IN      @{TXT}
    \   Should Contain  ${I}    [