*** Settings ***
Documentation    Testing if Elements in the Managed Devieces -> Auto Discovery are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2


*** Test Cases ***

Test Buttons Translation
    Add Missing Device
    gui::basic::japanese::test translation with value  jquery=#addButton
    gui::basic::japanese::test translation with value  jquery=#delButton
    gui::basic::japanese::test translation with value  jquery=#upButton
    gui::basic::japanese::test translation with value  jquery=#downButton

Test Table Translation
    gui::basic::japanese::test translation table header with checkbox  9

Test Add Element Translation
    Click element   jquery=#addButton
    gui::basic::spinner should be invisible
    gui::basic::japanese::test translation with value  jquery=#saveButton
    gui::basic::japanese::test translation with value  jquery=#cancelButton
    gui::basic::check translation no id previous sibling  discovery_name    status    hostname    operation   seed

Test Add Without Name
    Select Radio Button     source  vmserial
    Click element   jquery=#saveButton
    Wait Until Element Is Not Visible     xpath=//*[@id='loading']    timeout=20s
    ${TXT}=     Execute Javascript      var er=document.getElementById('discovery_name');if(er==null){return false;}var ro=er.parentNode.nextSibling;if(ro==null){return false;}var txt=ro.textContent;return txt.includes("[");
    Should Be True     ${TXT}

Test Radio Button Translations
    GUI::Basic::Japanese::Test Translation      //*[@id="addSPMD_Form"]/div[2]/div/div/div[3]/div/div[1]
    GUI::Basic::Japanese::Test Translation      //*[@id="addSPMD_Form"]/div[2]/div/div/div[3]/div/div[4]
    GUI::Basic::Japanese::Test Translation      //*[@id="addSPMD_Form"]/div[2]/div/div/div[3]/div/div[7]
    GUI::Basic::Japanese::Test Translation      //*[@id="addSPMD_Form"]/div[2]/div/div/div[3]/div/div[11]
    GUI::Basic::Japanese::Test Translation      //*[@id="addSPMD_Form"]/div[2]/div/div/div[3]/div/div[14]
    GUI::Basic::Japanese::Test Translation      //*[@id="addSPMD_Form"]/div[2]/div/div/div[3]/div/div[17]
    Test Radio Options  vmserial    porturi     noticeuri
    test radio options  vmmanager   datacenter  cluster     noticedc
    Input Text      jquery=#discovery_name      name

Test DHCP Button
    Test Radio Options  dhcp    macaddr     noticemac
    Test Error Throw    macaddr

Test Console Server Button
    test radio options  consoleserver   portlistcas     noticeportlistcas
    Test Error Throw    portlistcas

Test KVM Button
    test radio options  kvmport     portlistkvm     noticeportlistkvm
    Test Error Throw    portlistkvm

Test Net Discover Button
    test radio options  network     netdiscover
    test dropdown   netdiscover

Test Dropdown Menu Translations
    test dropdown  status
    test dropdown  operation
    test dropdown  seed

Test Operation Dropdown Behavior
    Select From List By Value   operation   discard
    Element Should Not Be Visible   jquery=#seed
    Click element   jquery=#cancelButton
    GUI::Basic::Spinner Should Be Invisible


*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    gui::basic::spinner should be invisible
    GUI::ManagedDevices::Auto::Open Auto Discovery Tab

Suite Teardown
    GUI::ManagedDevices::Delete Device If Exists        aaaa
    GUI::Basic::Logout
    Close all Browsers

Test Error Throw
    [Arguments]  ${LOC}
    Input Text      jquery=#${LOC}      !@#<>>?":":
    Click Element   jquery=#saveButton
    Wait Until Element Is Not Visible     xpath=//*[@id='loading']    timeout=20s
    ${TXT}=     Execute Javascript      var er=document.getElementById('${LOC}');if(er==null){return false;}var ro=er.parentNode.nextSibling;if(ro==null){return false;}var txt=ro.textContent;return txt.includes("[");
    Should Be True     ${TXT}

Test Dropdown
    [Arguments]  ${LOC}
    @{TXT}=     Get List Items  jquery=#${LOC}
    ${LEN_LST}=     get length  ${TXT}
    Run Keyword Unless  '${LEN_LST}'=='1'   Test Dropdown Options  ${LOC}

Test Dropdown Options
    [Arguments]  ${LOC}
    @{TXT}=     Get List Items  jquery=#${LOC}
    :FOR    ${I}    IN      @{TXT}
    \   Should Contain  ${I}    [

Test Radio Options
    [Arguments]  ${RADVAL}  @{ITEMS}
    Select Radio Button     source  ${RADVAL}
    ${LEN_LST}=     get length  ${ITEMS}
    :FOR    ${I}    IN RANGE    0   ${LEN_LST}
    \   ${ALMOST}=  Evaluate    ${LEN_LST}-1
    \   Run keyword if  '${I}'=='0'     gui::basic::check translation no id previous sibling   @{ITEMS}[${I}]
    \   ...     ELSE    Run keyword if  '${I}'=='${ALMOST}'     gui::basic::japanese::test translation  @{ITEMS}[${I}]
    \   ...     ELSE    gui::basic::check translation no id previous sibling  @{ITEMS}[${I}]

Add Missing Device
    GUI::ManagedDevices::Open Devices tab
    GUI::Basic::Spinner Should Be Invisible
    ${count}=	GUI::Basic::Count Table Rows	SPMTable
	Run Keyword If	${count} > 0	Log	"Device already exists"
	Run Keyword If	${count} == 0	AMD
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Auto::Open Auto Discovery Tab
    gui::basic::spinner should be invisible

AMD
    Wait Until Element Is Visible       jquery=#addButton
    Click Element   jquery=#addButton
    Wait Until Element Is Visible       jquery=#cancelButton
    Input Text      xpath=//input[@id="spm_name"]       aaaa
    Wait Until Element Is Visible       jquery=#saveButton
    Select From List By Value   type    device_console
    Wait Until element Is Visible       jquery=#saveButton
    Click Element   jquery=#saveButton