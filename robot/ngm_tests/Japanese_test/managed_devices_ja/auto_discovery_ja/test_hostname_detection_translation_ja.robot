*** Settings ***
Documentation    Testing if Elements in the Managed Devieces -> Auto Discovery -> Hostname Detection are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2


*** Test Cases ***

Test Buttons Translation
    gui::basic::japanese::test translation with value  jquery=#addButton
    gui::basic::japanese::test translation with value  jquery=#delButton
    gui::basic::japanese::test translation with value  jquery=#discNameGlobalSet

Test Table Translation
    gui::basic::japanese::test translation table header with checkbox  4
    gui::basic::japanese::test table elements  discNameTable    2
    gui::basic::japanese::test table elements  discNameTable    4

Test Add Element Translation
    Click element   jquery=#addButton
    gui::basic::spinner should be invisible
    gui::basic::japanese::test translation with value  jquery=#saveButton
    gui::basic::japanese::test translation with value  jquery=#cancelButton
    gui::basic::check translation no id previous sibling  strType   strData
    @{TXT}=     Get List Items  jquery=#strType
    :FOR    ${I}    IN      @{TXT}
    \   Should Contain  ${I}    [
    Click element   jquery=#cancelButton
    GUI::Basic::Spinner Should Be Invisible

Test Global Setting
    click element   jquery=#discNameGlobalSet
    GUI::Basic::Spinner Should Be Invisible
    gui::basic::check translation no id previous sibling  probetmo  nretries
    GUI::Basic::Check Translation No ID  updateName     lastWin
    GUI::Auditing::Text Input Error Tests       probetmo    ${TRUE}     notsec
    ${RAND}=    evaluate  random.randint(5,20)  modules=random
    gui::auditing::auto input tests     nretries    -1  111111111   string  34.12   ${RAND}
    gui::auditing::auto input tests     probetmo    -1  111111111   string  34.12   ${RAND}
    gui::basic::spinner should be invisible
    click element   jquery=#discNameGlobalSet
    GUI::Basic::Spinner Should Be Invisible
    Input Text  jquery=#probetmo    5
    Input Text  jquery=#nretries    3
    Click Element   jquery=#saveButton

Test Add and Edit Detection
    Click element   jquery=#addButton
    GUI::Basic::Spinner Should Be Invisible
    Input text      jquery=#strData     !@#$%^&*()<>?:"{}|
    Click element   jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    Page should Not Contain Element     jquery=#errormsg
    Click Element   xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[8]/td[2]/a
    GUI::Basic::Spinner Should Be Invisible
    Input Text      jquery=#strData     |{}":?><)(*&^%$#@!
    Click Element   jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    Page Should Not Contain Element     jquery=#errormsg
    Select Checkbox     xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[8]/td[1]/input
    Click Element   jquery=#delButton
    Handle Alert
    GUI::Basic::Spinner Should Be Invisible

*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    gui::basic::spinner should be invisible
    GUI::ManagedDevices::Open Hostname Detection

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers