*** Settings ***
Documentation    Testing if Errors in the Managed Devieces -> Auto Discovery -> VM Managers are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2


*** Test Cases ***



Test Add VM Manager Invalid VM Server Error
    Click element   jquery=#addButton
    GUI::Basic::Spinner Should Be Invisible
    Input Text  jquery=#vmmanager   **
    GUI::Basic::Save
    Suite Check Error Translations

Test Add VM Manager Invalid Username Error
    Input Text  jquery=#vmmanager   Dummy
    Input Text  jquery=#username   **
    GUI::Basic::Save
    Suite Check Error Translations

Test Add VM Manager Mismatch Password Error
    Input Text  jquery=#username   Dummy
    Input Text  jquery=#fpasswd   Dummy123
    Input Text  jquery=#cpasswd   Dummy456
    GUI::Basic::Save
    Suite Check Error Translations


*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    gui::basic::spinner should be invisible
    GUI::ManagedDevices::Open VM Manager

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers

Test Dropdown Options
    [Arguments]  ${LOC}
    @{TXT}=     Get List Items  jquery=#${LOC}
    :FOR    ${I}    IN      @{TXT}
    \   Should Contain  ${I}    [

Suite Check Error Translations
    GUI::Basic::Japanese::Test Translation      //*[@id="errormsg"]
   # GUI::Basic::Japanese::Test Translation      //*[@id="globalerr"]/div/div/p