*** Settings ***
Documentation    Testing if Elements in the Managed Devieces -> Devices Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2


*** Test Cases ***
Test Devices Tab nonAccess Controls Buttons
    : FOR    ${INDEX}    IN RANGE    1    9
    \    GUI::Basic::Japanese::Test Translation With Value    //*[@id="nonAccessControls"]/input[${INDEX}]

    GUI::Basic::Japanese::Test Translation    //*[@id="main_doc"]/div[1]/div/div[1]/div/label
    GUI::Basic::Japanese::Test Translation    //*[@id="main_doc"]/div[1]/div/div[2]/span[1]
    GUI::Basic::Japanese::Test Translation    //*[@id="main_doc"]/div[1]/div/div[2]/span[2]


Test Devices Table Header
    GUI::Basic::Japanese::Test Translation Table Header With Checkbox     6

Test Devices Table Elements
    GUI::Basic::Japanese::Test Table Elements  SPMTable   5
    GUI::Basic::Japanese::Test Table Elements  SPMTable   6
    GUI::Basic::Japanese::Test Table Elements  SPMTable   4

*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[5]
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers