*** Settings ***
Documentation    Testing if Errors in the Managed Devices -> Devices -> Add Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Variables ***
${Empty}

*** Test Cases ***


Test Devices Add No Name Errors
    Click Element   //*[@id="addButton"]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   //*[@id="type"]
    Click Element   //*[@id="type"]/option[10]
    GUI::Basic::Save
    Suite Check Error Translations

Test Devices Add Invalid Name Errors
    Input Text          //*[@id="spm_name"]  **
    GUI::Basic::Save
    Suite Check Error Translations

Test Devices Add Invalid IP Errors
    Input Text          //*[@id="spm_name"]  Dummy
    Input Text          //*[@id="phys_addr"]  **
    GUI::Basic::Save
    Suite Check Error Translations

Test Devices Add Invalid Coordinates Errors
    Input Text          //*[@id="coordinates"]  Dummy
    Input Text          //*[@id="phys_addr"]  ${Empty}
    GUI::Basic::Save
    Suite Check Error Translations

Test Devices Add Mismatch Passwords Errors
    Input Text          //*[@id="coordinates"]  ${Empty}
    Input Text          //*[@id="passwordfirst"]    lol
    Input Text          //*[@id="passwordconf"]     lmao
    GUI::Basic::Save
    Suite Check Error Translations

Test Devices Add Break Sequence Errors
    Select Radio Button     askpassword     yes
    Select Checkbox     //*[@id="enable-break"]
    Input Text          //*[@id="break-sequence"]  l
    GUI::Basic::Save
    Suite Check Error Translations

Test Devices Add Expiration Date Errors
    Unselect Checkbox     //*[@id="enable-break"]
    Select Radio Button     ttlType     date
    Input Text          //*[@id="ttlDate"]  Dummy
    GUI::Basic::Save
    Suite Check Error Translations

Test Devices Add Expiration Days Errors
    Select Radio Button     ttlType     days
    Input Text          //*[@id="ttlDays"]  Dummy
    GUI::Basic::Save
    Suite Check Error Translations


Test Devices Add Escape Sequence Too Long Errors
    Select Radio Button     ttlType     never
    Input Text          //*[@id="inbhotkey"]  Dummy
    GUI::Basic::Save
    Suite Check Error Translations

Test Devices Add Escape Sequence Too Short Errors
    Input Text          //*[@id="inbhotkey"]  ^
    GUI::Basic::Save
    Suite Check Error Translations

Test Devices Add Power Control Too Short Errors
    Input Text          //*[@id="inbhotkey"]  ^Ec
    Input Text          //*[@id="inbpowerctlkey"]  ^
    GUI::Basic::Save
    Suite Check Error Translations

Test Devices Add Power Control Too Long Errors
    Input Text          //*[@id="inbpowerctlkey"]  Dummy
    GUI::Basic::Save
    Suite Check Error Translations

Test Devices Add Enable Alias Invalid IP
    Select Checkbox     //*[@id="inbipEnable"]
    Input Text          //*[@id="inbipAddr"]  Dummy
    GUI::Basic::Save
    Suite Check Error Translations

Test Devices Add Enable Alias Invalid Binary Socket Port
    Select Checkbox     //*[@id="inbipRaw"]
    Input Text          //*[@id="inbipAddr"]  1.1.1.1
    Input Text          //*[@id="inbipRawPort"]  Dummy
    GUI::Basic::Save
    Suite Check Error Translations

Test Devices Add Enable Alias Too Long Binary Socket Port
    Input Text          //*[@id="inbipRawPort"]  12345678901
    GUI::Basic::Save
    Suite Check Error Translations

Test Devices Add Enable Alias Invalid Telenet Protocol Port
    Select Checkbox     //*[@id="inbipTeln"]
    Input Text          //*[@id="inbipRawPort"]  23
    Input Text          //*[@id="inbipTelnPort"]  Dummy
    GUI::Basic::Save
    Suite Check Error Translations

Test Devices Add Enable Alias Too Long Telenet Protocol Port
    Input Text          //*[@id="inbipTelnPort"]  12345678901
    GUI::Basic::Save
    Suite Check Error Translations

Test Devices Add Invalid SSH Port
    Unselect Checkbox     //*[@id="inbipEnable"]
    Select Checkbox     //*[@id="inbssh"]
    Input Text          //*[@id="inbsshTCP"]  23
    GUI::Basic::Save
    Suite Check Error Translations


*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[5]
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    Click Element   //*[@id="cancelButton"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Logout
    Close all Browsers

Suite Check Error Translations
    GUI::Basic::Japanese::Test Translation      //*[@id="errormsg"]
   # GUI::Basic::Japanese::Test Translation      //*[@id="globalerr"]/div/div/p