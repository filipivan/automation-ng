*** Settings ***
Documentation    Testing if Elements in the ILO Management tab is functional
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***
Create ILO Device
    ${count}=	GUI::Basic::Count Table Rows	SPMTable
	Run Keyword If	${count} > 0	Log	"Device already exists"
	Run Keyword If	${count} == 0	Add Missing Device

Drilldown and Check Tab Translations
    GUI::Basic::Spinner Should Be Invisible
    Click Element   xpath=/html/body/div[7]/div[2]/div[2]/div[2]/div[3]/table/tbody/tr/td[2]/a
    GUI::Basic::Spinner Should Be Invisible
    ${HEADERS}=     Get Element Count   xpath=//*[@id="pod_menu"]/a
    ${HEADERS}=     Evaluate    ${HEADERS}+1
    :FOR    ${I}    IN RANGE    1   ${HEADERS}
    \   GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[1]/a[${I}]/span

Open Management Tab and Test Titles
    Click element   jquery=#spmmgmt_nav
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be Visible   jquery=#saveButton
    GUI::Basic::Japanese::Test Translation      jquery=#dev_name_title
    GUI::Basic::Japanese::Test Translation      jquery=#script_title
    GUI::Basic::Japanese::Test Translation      jquery=#proto_title
    GUI::Basic::Japanese::Test Translation      jquery=#monit_title
    GUI::Basic::Japanese::Test Translation With Value   jquery=#saveButton
    gui::basic::japanese::test translation with value   jquery=#returnButton
    Select Checkbox     jquery=#ipmi_monit_enabled
    Select Checkbox     jquery=#nominal_monit_enabled

Test Translations Textboxes and Dropdowns
    GUI::Basic::Check Translation No ID Previous Sibling    spm_name    username    passwordfirst   passwordconf
    ...     ipmiusername    ipmipasswordfirst   ipmipasswordconf    ipmi_monit_template     ipmi_monit_interval
    ...     nominal_monit_name  nominal_monit_type  nominal_monit_value     nominal_monit_interval  on_session_start
    ...     on_session_stop     on_device_up    on_device_down
    Test List Dropdowns     ipmi_monit_template     nominal_monit_type  on_session_start    on_session_stop
    ...     on_device_up    on_device_down

Test Translations Checkboxes
    GUI::Basic::Check Translation No ID     ssh_proto_enabled   ipmi_proto_enabled  ipmi_monit_enabled
    ...     nominal_monit_enabled

Test Radio Buttons
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[1]/div/div[2]/div[2]/div/div/div[1]/div[2]/div/label
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[1]/div/div[2]/div[2]/div/div/div[1]/div[2]/div/div/div[1]/label
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[1]/div/div[2]/div[2]/div/div/div[1]/div[2]/div/div/div[2]/label
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[1]/div/div[2]/div[2]/div/div/div[2]/div[2]/div/label
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[1]/div/div[2]/div[2]/div/div/div[2]/div[2]/div/div/div[1]/label
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[1]/div/div[2]/div[2]/div/div/div[2]/div[2]/div/div/div[2]/label
    Select Radio Button     sameasaccess    yes
    GUI::Basic::Spinner Should Be Invisible
    Element Should Not Be Visible   jquery=#username
    Select Radio Button     sameasaccess    no
    GUI::Basic::Spinner Should Be Invisible
    Select Radio Button     sameasaccessipmi    yes
    GUI::Basic::Spinner Should Be Invisible
    Element Should Not Be Visible   jquery=#ipmiusername
    Select Radio Button     sameasaccessipmi    no
    GUI::Basic::Spinner Should Be Invisible
    Click element   jquery=#returnButton


*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::ManagedDevices::Open Devices tab

Suite Teardown
    GUI::ManagedDevices::Delete Device If Exists        aaaa
    GUI::Basic::Logout
    Close all Browsers

Test List Dropdowns
    [Arguments]  @{LOC}
    :FOR    ${I}    IN  @{LOC}
    \   Test Dropdown Options  ${I}

Test Dropdown Options
    [Arguments]  ${LOC}
    @{TXT}=     Get List Items  jquery=#${LOC}
    :FOR    ${I}    IN      @{TXT}
    \   run keyword and continue on failure  Should Contain  ${I}    [

Add Missing Device
    Wait Until Element Is Visible       jquery=#addButton
    Click Element   jquery=#addButton
    Wait Until Element Is Visible       jquery=#cancelButton
    Input Text      xpath=//input[@id="spm_name"]       aaaa
    Wait Until Element Is Visible       jquery=#saveButton
    Click Element   jquery=#saveButton