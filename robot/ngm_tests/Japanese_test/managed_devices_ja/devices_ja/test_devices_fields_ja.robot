*** Settings ***
Documentation    Testing if Elements in the Managed Devieces -> Devices Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Add Dummy Device
    GUI::ManagedDevices::Open
    ${count}=	GUI::Basic::Count Table Rows	SPMTable
	Run Keyword If	${count} > 0	Log	"Device already exists"
	Run Keyword If	${count} == 0	Add Missing Device

Set Device Type
    gui::basic::spinner should be invisible
    wait until element is visible   xpath=//*[@id="aaaa"]
    Click element   xpath=/html/body/div[7]/div[2]/div[2]/div[2]/div[3]/table/tbody/tr/td[2]/a
    gui::basic::spinner should be invisible
    Select From List By Value      jquery=#type    device_console
    gui::basic::spinner should be invisible
    Click element   jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible

Select Boxes
    @{CHK}=     Create List     enable-break    inbipEnable     inbipTeln   inbipRaw    inbSecipEnable  inbSecipTeln    inbSecipRaw     inbssh  inbteln     inbraw
    :FOR    ${I}    IN  @{CHK}
    \   Select Checkbox     jquery=#${I}

Test Coordinates
    ${RAND}=    evaluate    random.randint(-90,90)  modules=random
    gui::auditing::auto input tests     coordinates     -91,180     91,180      90,-181     90,181      ${RAND},${RAND}

Test IPs
    ${RAND}=    evaluate    random.randint(0,255)   modules=random
    @{TEST}=    Create List     obv-n[]t-ip     002c::0002c::002c   123.123.123.${RAND}
    gui::auditing::auto input tests     phys_addr     @{TEST}
    gui::auditing::auto input tests     inbipAddr     @{TEST}
    gui::auditing::auto input tests     inbSecipAddr     @{TEST}

Test Ports
    ${RAND}=    evaluate    random.randint(1,99)    modules=random
    ${RANDS}=    evaluate    random.randint(2001,64999)   modules=random
    ${RANDT}=    evaluate    random.randint(2001,64999)   modules=random
    ${RANDR}=    evaluate    random.randint(2001,64999)   modules=random
    @{TEST}=    Create List           str     12::56     ${RAND}
    @{TESTS}=    Create List           str     12::56     ${RANDS}
    @{TESTT}=    Create List           str     12::56     ${RANDT}
    @{TESTR}=    Create List           str     12::56     ${RANDR}
    gui::auditing::auto input tests     tcpport             @{TEST}
    gui::auditing::auto input tests     inbipTelnPort       @{TEST}
    gui::auditing::auto input tests     inbipRawPort        @{TEST}
    gui::auditing::auto input tests     inbSecipTelnPort    @{TEST}
    gui::auditing::auto input tests     inbSecipRawPort     @{TEST}
    gui::auditing::auto input tests     inbsshTCP           @{TESTS}
    gui::auditing::auto input tests     inbtelnTCP          @{TESTT}
    gui::auditing::auto input tests     inbrawTCP           @{TESTR}

Test Password
    #Mismatch
    AER Pass Input Text  asd    qwe     ${TRUE}
    #valid
    AER Pass Input Text  zxc    zxc     ${FALSE}

Test Commands
    ${RAND}=    evaluate  random.randint(0,9)   modules=random
    @{TEST}=    Create List     ,.,     9jk     j$c     ^$      ^p${RAND}
    @{TESTC}=    Create List     ,.,     9jk     j$c     ^$      ^[
    gui::auditing::auto input tests     inbhotkey           @{TEST}
    gui::auditing::auto input tests     inbpowerctlkey      @{TESTC}

Delete Device
    Click element   jquery=#returnButton
    GUI::Basic::Spinner Should Be Invisible
    Click Element   xpath=/html/body/div[7]/div[2]/div[2]/div[2]/div[3]/table/tbody/tr/td[1]/input
    Click element   jquery=#delButton
    Handle Alert
    GUI::Basic::Spinner Should Be Invisible


*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    gui::basic::spinner should be invisible
    GUI::ManagedDevices::Open Devices tab

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers

Add Missing Device
    Wait Until Element Is Visible       jquery=#addButton
    Click Element   jquery=#addButton
    Wait Until Element Is Visible       jquery=#cancelButton
    Input Text      xpath=//input[@id="spm_name"]       aaaa
    Wait Until Element Is Visible       jquery=#saveButton
    Click Element   jquery=#saveButton

AER Pass Input Text
    [Arguments]  ${IP_ONE}     ${IP_TWO}      ${EXPECTS}
    [Documentation]     IP_ONE(TWO) expect the input for the two fields
    ...                 EXPECTS wants a t/f of whether an error should be thrown
    Wait Until Element Is Not Visible   xpath=//*[@id='loading']    timeout=20s
    Input Text      jquery=#passwordfirst      ${IP_ONE}
    Input Text      jquery=#passwordconf      ${IP_TWO}
    Click Element   jquery=#saveButton
    Wait Until Element Is Not Visible   xpath=//*[@id='loading']    timeout=20s
    ${TST}=     Execute Javascript      var er=document.getElementById('passwordfirst');if(er==null){return false;}var ro=er.parentNode.nextSibling;if(ro==null){return false;}var txt=ro.textContent;return txt.includes("[");
    Log     ${TST}
    Run Keyword If      ${EXPECTS}==${TRUE}     Should Be True     ${TST}
    ...     ELSE    Should Not Be True     ${TST}