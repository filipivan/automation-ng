*** Settings ***
Documentation    Testing if Elements in the Managed Devices -> Devices Add Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2


*** Test Cases ***
Test Devices Add nonAccess Controls Buttons
    Click Element   //*[@id="addButton"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="cancelButton"]

Test Devices Add Elements
    GUI::Basic::Japanese::Test Translation      //*[@id="addSPM_Form"]/div[2]/div[1]/div/div[1]
    GUI::Basic::Japanese::Test Translation      //*[@id="addSPM_Form"]/div[2]/div[1]/div/div[2]
    GUI::Basic::Japanese::Test Translation      //*[@id="addSPM_Form"]/div[2]/div[1]/div/div[4]
    : FOR    ${INDEX}    IN RANGE    1    5
    \    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[2]/div[2]/div/div[${INDEX}]

    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[3]/div[2]/div/div[1]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="addSPM_Form"]/div[3]/div[2]/div/div[1]/div/div[2]/input[1]
    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[3]/div[2]/div/div[3]
    : FOR    ${INDEX}    IN RANGE    1    4
    \    GUI::Basic::Japanese::Test Translation    //*[@id="status"]/option[${INDEX}]

    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[3]/div[2]/div/div[5]
    Select Radio Button     ttlType     date
    : FOR    ${INDEX}    IN RANGE    1    5
    \    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[3]/div[2]/div/div[5]/div/div[${INDEX}]

    Select Radio Button     ttlType     days
    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[3]/div[2]/div/div[5]/div/div[5]



Test Devices Add Elements with ilo
    Click Element       //*[@id="type"]
    Click Element       //*[@id="type"]/option[1]
    GUI::Basic::Japanese::Test Translation      //*[@id="addSPM_Form"]/div[3]/div[1]/div/div[1]
    GUI::Basic::Japanese::Test Translation      //*[@id="addSPM_Form"]/div[3]/div[1]/div/div[2]
    GUI::Basic::Japanese::Test Translation      //*[@id="addSPM_Form"]/div[3]/div[1]/div/div[3]
    : FOR    ${INDEX}    IN RANGE    1    5
    \    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[3]/div[1]/div/div[2]/div/div[${INDEX}]

    : FOR    ${INDEX}    IN RANGE    5    9
    \    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[3]/div[1]/div/div[${INDEX}]

Test Devices Add Elements with virtual console vmware
    Click Element       //*[@id="type"]
    Click Element       //*[@id="type"]/option[12]
    GUI::Basic::Japanese::Test Translation      //*[@id="typeNotice"]
    GUI::Basic::Japanese::Test Translation      //*[@id="addSPM_Form"]/div[3]/div[1]/div/div[4]
    GUI::Basic::Japanese::Test Translation      //*[@id="addSPM_Form"]/div[2]/div[1]/div/div[5]

Test Devices Add Elements with cimc ucs
    Click Element       //*[@id="type"]
    Click Element       //*[@id="type"]/option[8]
    GUI::Basic::Japanese::Test Translation      //*[@id="addSPM_Form"]/div[2]/div[1]/div/div[6]
    GUI::Basic::Japanese::Test Translation      //*[@id="addSPM_Form"]/div[2]/div[1]/div/div[7]

Test Devices Add Elements with Device Console
    Click Element       //*[@id="type"]
    Click Element       //*[@id="type"]/option[10]
    GUI::Basic::Japanese::Test Translation      //*[@id="addSPM_Form"]/div[2]/div[1]/div/div[5]

Test Devices Add Inbound Access Left Side
    : FOR    ${INDEX}    IN RANGE    1    8
    \    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[4]/div[1]/div/div[${INDEX}]

    Select Checkbox     //*[@id="inbipEnable"]
    Select Checkbox     //*[@id="inbipTeln"]
    Select Checkbox     //*[@id="inbipRaw"]
    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[4]/div[1]/div/div[6]/div[2]/div[1]
    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[4]/div[1]/div/div[6]/div[2]/div[2]
    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[4]/div[1]/div/div[6]/div[2]/div[3]/div[2]/div
    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[4]/div[1]/div/div[6]/div[2]/div[4]/div[2]/div
    Select Checkbox     //*[@id="inbSecipEnable"]
    Select Checkbox     //*[@id="inbSecipTeln"]
    Select Checkbox     //*[@id="inbSecipRaw"]
    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[4]/div[1]/div/div[7]/div[2]/div[1]
    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[4]/div[1]/div/div[7]/div[2]/div[2]
    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[4]/div[1]/div/div[7]/div[2]/div[3]/div[2]/div
    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[4]/div[1]/div/div[7]/div[2]/div[4]/div[2]/div

Test Devices Add Inbound Access Right Side
    Select Checkbox     //*[@id="inbssh"]
    Select Checkbox     //*[@id="inbteln"]
    Select Checkbox     //*[@id="inbraw"]
    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[4]/div[2]/div/div[2]/div[2]/div
    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[4]/div[2]/div/div[4]/div[2]/div
    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[4]/div[2]/div/div[5]/div[2]/div
    GUI::Basic::Japanese::Test Translation    //*[@id="inbtelnNotice"]




*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[5]
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    Click Element   //*[@id="cancelButton"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Logout
    Close all Browsers