*** Settings ***
Documentation    Testing if Elements in the ILO Devices Custom Fields tab is functional
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***
Create ILO Device
    ${count}=	GUI::Basic::Count Table Rows	SPMTable
	Run Keyword If	${count} > 0	Log	"Device already exists"
	Run Keyword If	${count} == 0	Add Missing Device

Drilldown and Check Tab Translations
    GUI::Basic::Spinner Should Be Invisible
    Click Element   xpath=/html/body/div[7]/div[2]/div[2]/div[2]/div[3]/table/tbody/tr/td[2]/a
    GUI::Basic::Spinner Should Be Invisible
    ${HEADERS}=     Get Element Count   xpath=//*[@id="pod_menu"]/a
    ${HEADERS}=     Evaluate    ${HEADERS}+1
    :FOR    ${I}    IN RANGE    1   ${HEADERS}
    \   GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[1]/a[${I}]/span

Drilldown and Access Management Tab
    GUI::Basic::Spinner Should Be Invisible
    Click element   jquery=#spmmgmt_nav
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be Visible   jquery=#saveButton

Test SNMP
    Select Checkbox     jquery=#snmp_proto_enabled
    Element Should Be Visible   xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[1]/div/div[2]/div[2]/div/div/div/div[2]/div/label
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[1]/div/div[2]/div[2]/div/div/div/div[2]/div/label

    Select Radio Button     snmpVersion     1
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[1]/div/div[2]/div[2]/div/div/div/div[2]/div/div/div[1]/label
    GUI::Basic::Check Translation No ID Previous Sibling    community
    Input Text      jquery=#community   no#
    Click element   jquery=#saveButton
    ${TXT}=     get text    xpath=/html/body/div[7]/div[2]/div[2]/form/div[1]/div/div/p
    Should Contain  ${TXT}  [

    Select Radio Button     snmpVersion     2
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[1]/div/div[2]/div[2]/div/div/div/div[2]/div/div/div[3]/label
    GUI::Basic::Check Translation No ID Previous Sibling    community2
    Input Text      jquery=#community2   no#
    Click element   jquery=#saveButton
    ${TXT}=     get text    xpath=/html/body/div[7]/div[2]/div[2]/form/div[1]/div/div/p
    Should Contain  ${TXT}  [

    Select Radio Button     snmpVersion     3
    gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div[1]/div/div[2]/div[2]/div/div/div/div[2]/div/div/div[5]/label
    GUI::Basic::Check Translation No ID Previous Sibling    user    securityLevel   authType    authpassword    privType    privPassPhrase
    Test List Dropdowns     securityLevel   authType    privType
    Input Text      jquery=#user   no#
    Click element   jquery=#saveButton
    ${TXT}=     get text    xpath=/html/body/div[7]/div[2]/div[2]/form/div[1]/div/div/p
    Should Contain  ${TXT}  [

    Click Element   jquery=#returnButton




*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::ManagedDevices::Open Devices tab

Suite Teardown
    GUI::ManagedDevices::Delete Device If Exists        aaaa
    GUI::Basic::Logout
    Close all Browsers

Test List Dropdowns
    [Arguments]  @{LOC}
    :FOR    ${I}    IN  @{LOC}
    \   Test Dropdown Options  ${I}

Test Dropdown Options
    [Arguments]  ${LOC}
    @{TXT}=     Get List Items  jquery=#${LOC}
    :FOR    ${I}    IN      @{TXT}
    \   run keyword and continue on failure  Should Contain  ${I}    [

Add Missing Device
    Wait Until Element Is Visible       jquery=#addButton
    Click Element   jquery=#addButton
    Wait Until Element Is Visible       jquery=#cancelButton
    Input Text      xpath=//input[@id="spm_name"]       aaaa
    Wait Until Element Is Visible       jquery=#saveButton
    Select From List By Value   type    device_console
    Wait Until element Is Visible       jquery=#saveButton
    Click Element   jquery=#saveButton