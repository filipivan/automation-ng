*** Settings ***
Documentation    Testing if Elements in the Managed Devices -> Devices Add Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2


*** Variables ***
${Emp}


*** Test Cases ***
Test Devices Access
    GUI::ManagedDevices::Add Dummy Device Console       Dummy   device_console  ${Emp}  ${Emp}  ${Emp}  no  ${Emp}  ondemand
    GUI::Basic::Spinner Should Be Invisible
    Click Element       //*[@id="Dummy"]/td[2]/a
    GUI::Basic::Spinner Should Be Invisible
    : FOR    ${INDEX}    IN RANGE    1    6
    \    GUI::Basic::Japanese::Test Translation    //*[@id="pod_menu"]/a[${INDEX}]

Test Devices Managment-Device
    Click Element       //*[@id="spmmgmt_nav"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="returnButton"]
    GUI::Basic::Japanese::Test Translation    //*[@id="dev_name_title"]
    GUI::Basic::Japanese::Test Translation    //*[@id="spmmgmt_form"]/div[2]/div[1]/div/div[1]/div[2]/div/div/div


Test Devices Managment-Protocol And Monitoring
    GUI::Basic::Japanese::Test Translation    //*[@id="proto_title"]
    GUI::Basic::Japanese::Test Translation    //*[@id="monit_title"]

Test Devices Managment-Scripts
    GUI::Basic::Japanese::Test Translation    //*[@id="script_title"]
    : FOR    ${INDEX}    IN RANGE    1    5
    \    GUI::Basic::Japanese::Test Translation    //*[@id="spmmgmt_form"]/div[2]/div[2]/div/div/div[2]/div/div/div[${INDEX}]


Test Devices Managment-Commands
    Click Element       //*[@id="spmcommands_nav"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="addButton"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="delButton"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="returnButton"]
    GUI::Basic::Japanese::Test Translation Table Header With Checkbox       5
    GUI::Basic::Japanese::Test Table Elements   SPMCommandsTable    2
    GUI::Basic::Japanese::Test Table Elements   SPMCommandsTable    3
    GUI::Basic::Japanese::Test Table Elements   SPMCommandsTable    4
    GUI::Basic::Japanese::Test Table Elements   SPMCommandsTable    5


*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[5]
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    Click Element       //*[@id="returnButton"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists        Dummy
    GUI::Basic::Logout
    Close all Browsers