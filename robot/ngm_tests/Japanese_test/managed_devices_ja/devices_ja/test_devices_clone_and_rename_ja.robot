*** Settings ***
Documentation    Testing if Elements in the Managed Devieces -> Devices -> Clone and Rename Tabs are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Variables ***
${Emp}

*** Test Cases ***
Test Devices Rename
    GUI::ManagedDevices::Add Dummy Device Console       Dummy   device_console  ${Emp}  ${Emp}  ${Emp}  no  ${Emp}  ondemand
    GUI::Basic::Spinner Should Be Invisible
    Click Element       //*[@id="Dummy"]/td[1]/input
    Click Element       //*[@id="nonAccessControls"]/input[4]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="cancelButton"]
    GUI::Basic::Japanese::Test Translation    //*[@id="devRenameForm"]/div[2]/div/div/div[1]
    GUI::Basic::Japanese::Test Translation    //*[@id="devRenameForm"]/div[2]/div/div/div[2]
    Click Element       //*[@id="cancelButton"]
    GUI::Basic::Spinner Should Be Invisible

Test Devices Clone
    Click Element       //*[@id="Dummy"]/td[1]/input
    Click Element       //*[@id="nonAccessControls"]/input[5]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="cancelButton"]
    : FOR    ${INDEX}    IN RANGE    1    8
    \    GUI::Basic::Japanese::Test Translation    //*[@id="devCloneForm"]/div[2]/div/div/div[${INDEX}]
    : FOR    ${INDEX}    IN RANGE    1    4
    \    GUI::Basic::Japanese::Test Translation    //*[@id="status"]/option[${INDEX}]

*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[5]
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    Click Element       //*[@id="cancelButton"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists        Dummy
    GUI::Basic::Logout
    Close all Browsers