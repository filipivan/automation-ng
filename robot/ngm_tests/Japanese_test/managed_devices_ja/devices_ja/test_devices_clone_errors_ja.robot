*** Settings ***
Documentation    Testing if Errors in the Managed Devieces -> Devices -> Clone and Rename Tabs are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Variables ***
${Emp}

*** Test Cases ***
Test Devices Clone Errors
    GUI::ManagedDevices::Add Dummy Device Console       Dummy   device_console  ${Emp}  ${Emp}  ${Emp}  no  ${Emp}  ondemand
    GUI::Basic::Spinner Should Be Invisible
    Click Element       //*[@id="Dummy"]/td[1]/input
    Click Element       //*[@id="nonAccessControls"]/input[5]
    GUI::Basic::Spinner Should Be Invisible
    Input Text          //*[@id="phys_addr"]  1
    GUI::Basic::Save
    Suite Check Error Translations

Test Clone Devices Invalid Name Errors
    Input Text          //*[@id="spm_name"]  **
    GUI::Basic::Save
    Suite Check Error Translations

Test Error With Too Large Num Clones
    Input Text          //*[@id="num_clones"]  5000
    Input Text          //*[@id="spm_name"]  Dum
    GUI::Basic::Save
    Suite Check Error Translations

Test Clone Error With Not Enough Licences
    Input Text          //*[@id="num_clones"]  4999
    GUI::Basic::Save
    Suite Check Error Translations

Test Error With String Num Clones
    Input Text          //*[@id="num_clones"]  Dum
    GUI::Basic::Save
    Suite Check Error Translations

Test Rename Device Error
    GUI::Basic::Cancel
    Click Element       //*[@id="Dummy"]/td[1]/input
    Click Element       //*[@id="nonAccessControls"]/input[4]
    GUI::Basic::Spinner Should Be Invisible
    Input Text          //*[@id="spm_name"]  Dummy
    GUI::Basic::Save
    Suite Check Error Translations


*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[5]
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    Click Element       //*[@id="cancelButton"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists        Dummy
    GUI::Basic::Logout
    Close all Browsers

Suite Check Error Translations
    GUI::Basic::Japanese::Test Translation      //*[@id="errormsg"]
   # GUI::Basic::Japanese::Test Translation      //*[@id="globalerr"]/div/div/p