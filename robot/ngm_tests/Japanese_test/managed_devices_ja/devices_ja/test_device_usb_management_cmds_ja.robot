*** Settings ***
Documentation    Testing if Elements in the ILO Devices Custom Fields tab is functional
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***
Create ILO Device
    ${count}=	GUI::Basic::Count Table Rows	SPMTable
	Run Keyword If	${count} > 0	Log	"Device already exists"
	Run Keyword If	${count} == 0	Add Missing Device

Drilldown and Access Management Tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element   xpath=/html/body/div[7]/div[2]/div[2]/div[2]/div[3]/table/tbody/tr/td[2]/a
    GUI::Basic::Spinner Should Be Invisible
    run keyword and continue on failure     GUI::Basic::Japanese::Test Translation      xpath=//*[@id="typeNotice"]
    Click element   jquery=#spmmgmt_nav
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be Visible   jquery=#saveButton

Check Sensor Monitoring
    GUI::Basic::Check Translation No ID     usb_sensor_monit_enabled
    Select Checkbox     jquery=#usb_sensor_monit_enabled
    GUI::Basic::Spinner Should Be Invisible
    run keyword and continue on failure  GUI::Basic::Japanese::Test Translation      jquery=#usb_sensor_temp_title
    run keyword and continue on failure  gui::basic::japanese::test translation      jquery=#usb_sensor_hum_title
    @{FLDS}=    Create List     usb_sensor_monit_template   usb_sensor_monit_interval
    ...     usb_sensor_temp_unit    usb_sensor_temp_thrHC   usb_sensor_temp_thrHW   usb_sensor_temp_thrLW
    ...     usb_sensor_temp_thrLC   usb_sensor_hum_thrHC    usb_sensor_hum_thrHW    usb_sensor_hum_thrLW
    ...     usb_sensor_hum_thrLC
    run keyword and continue on failure  gui::basic::check translation no id previous sibling    @{FLDS}
    run keyword and continue on failure  Test List Input Tests   usb_sensor_monit_interval
    ...     usb_sensor_temp_thrHC   usb_sensor_temp_thrHW   usb_sensor_temp_thrLW   usb_sensor_temp_thrLC
    ...     usb_sensor_hum_thrHC    usb_sensor_hum_thrHW    usb_sensor_hum_thrLW    usb_sensor_hum_thrLC
    run keyword and continue on failure  test list dropdowns    usb_sensor_monit_template   usb_sensor_temp_unit

Check Commands
    Click element   jquery=#spmcommands_nav
    GUI::Basic::Spinner Should Be Invisible
    Click element   jquery=#addButton
    gui::basic::spinner should be invisible
    Select From List By Value   command     web
    click element   jquery=#returnButton

*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::ManagedDevices::Open Devices tab

Suite Teardown
    GUI::ManagedDevices::Delete Device If Exists        aaaa
    GUI::Basic::Logout
    Close all Browsers

Test List Input Tests
    [Arguments]  @{LOC}
    @{TEST}=     Create List     str     -123    12.3    123
    :FOR    ${I}    IN      @{LOC}
    \   run keyword and continue on failure     GUI::Auditing::Auto Input Tests  ${I}   @{TEST}

Test List Dropdowns
    [Arguments]  @{LOC}
    :FOR    ${I}    IN  @{LOC}
    \   Test Dropdown Options  ${I}

Test Dropdown Options
    [Arguments]  ${LOC}
    @{TXT}=     Get List Items  jquery=#${LOC}
    :FOR    ${I}    IN      @{TXT}
    \   run keyword and continue on failure  Should Contain  ${I}    [

Add Missing Device
    Wait Until Element Is Visible       jquery=#addButton
    Click Element   jquery=#addButton
    Wait Until Element Is Visible       jquery=#cancelButton
    Input Text      xpath=//input[@id="spm_name"]       aaaa
    Wait Until Element Is Visible       jquery=#saveButton
    Select From List By Value   type    usb_sensor
    Wait Until element Is Visible       jquery=#saveButton
    Click Element   jquery=#saveButton