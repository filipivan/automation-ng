*** Settings ***
Documentation    Testing if Elements in the ILO Devices commands tab is functional
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***
Create ILO Device
    ${count}=	GUI::Basic::Count Table Rows	SPMTable
	Run Keyword If	${count} > 0	Log	"Device already exists"
	Run Keyword If	${count} == 0	Add Missing Device

Drilldown and Access Commands Tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element   xpath=/html/body/div[7]/div[2]/div[2]/div[2]/div[3]/table/tbody/tr/td[2]/a
    GUI::Basic::Spinner Should Be Invisible
    Click element   jquery=#spmcommands_nav
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be Visible   jquery=#addButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#addButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#delButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#returnButton
    GUI::Basic::Japanese::Test Translation Table Header With Checkbox   5

Test Table Body Translations
    :FOR    ${I}    IN RANGE    1   7
    \   Check table Row     ${I}

Test Translations Custom Commands
    Click Element       jquery=#addButton
    GUI::Basic::Spinner Should Be Invisible
    Test Dropdown Options       command
    Select From List By Value   command     custom_commands
    GUI::Basic::Check Translation No ID     enabled
    gui::basic::check translation no id previous sibling    protocol
    GUI::Basic::Japanese::Test Translation      jquery=#proto_dis_warn
    :FOR    ${I}    IN RANGE    1   11
    \   gui::basic::check translation no id previous sibling    script_customcommand${I}    label_customcommand${I}
    \   GUI::Basic::Check Translation No ID     enabled_customcommand${I}
    \   Test Dropdown Options   script_customcommand${I}
    \   GUI::Auditing::Auto Input Tests     label_customcommand${I}     ,./;'   not.a.label     notl@b#l    label

Test Translations KVM
    Select From List By Value   command     kvm
    gui::basic::check translation no id previous sibling    pymod_file
    Test Dropdown Options   pymod_file

Test Translations Outlet
    Select From List By Value   command     outlet
    run keyword and continue on failure     GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[3]/div[2]/div[4]/div[2]/div[1]/span
    run keyword and continue on failure     GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[3]/div[2]/div[4]/div[2]/div[2]/button[1]
    run keyword and continue on failure     GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[3]/div[2]/div[4]/div[2]/div[2]/button[2]
    run keyword and continue on failure     GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div[3]/div[2]/div[4]/div[2]/div[3]/span
    run keyword and continue on failure     GUI::Basic::Check Translation No ID Previous Sibling    cycle
    Click element       jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    ${ERR}=     Get Text    xpath=/html/body/div[7]/div[2]/div[2]/form/div[1]/div/div/p
    run keyword and continue on failure     Should Contain      ${ERR}      [
    Click Element       jquery=#returnButton

*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::ManagedDevices::Open Devices tab

Suite Teardown
    GUI::ManagedDevices::Delete Device If Exists        aaaa
    GUI::Basic::Logout
    Close all Browsers

Test List Dropdowns
    [Arguments]  @{LOC}
    :FOR    ${I}    IN  @{LOC}
    \   Test Dropdown Options  ${I}

Test Dropdown Options
    [Arguments]  ${LOC}
    @{TXT}=     Get List Items  jquery=#${LOC}
    :FOR    ${I}    IN      @{TXT}
    \   run keyword and continue on failure  Should Contain  ${I}    [

Add Missing Device
    Wait Until Element Is Visible       jquery=#addButton
    Click Element   jquery=#addButton
    Wait Until Element Is Visible       jquery=#cancelButton
    Input Text      xpath=//input[@id="spm_name"]       aaaa
    Wait Until Element Is Visible       jquery=#saveButton
    Click Element   jquery=#saveButton

Check Table Row
    [Arguments]     ${ROW}
    :FOR    ${I}    IN RANGE    2   6
    \   Run Keyword If      '${I}'=='2'     run keyword and continue on failure     GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[${ROW}]/td[${I}]/a
    \   ...     ELSE    run keyword and continue on failure     GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[${ROW}]/td[${I}]