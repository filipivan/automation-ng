*** Settings ***
Documentation    Testing if Errors in the Managed Devieces -> Devices Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2


*** Test Cases ***

Test Invalid Name
    GUI::Basic::Add
    Unselect Checkbox   //*[@id="enabled"]
    Input Text          //*[@id="netdiscover_id"]  **
    Input Text          //*[@id="ipfirst"]  0.0.0.0
    Input Text          //*[@id="iplast"]  0.0.0.1
    GUI::Basic::Save
    Suite Check Error Translations

Test Invalid IP Range
    Input Text          //*[@id="netdiscover_id"]  Dummy
    Input Text          //*[@id="ipfirst"]  1.1.1.5
    Input Text          //*[@id="iplast"]  1.1.1.1
    GUI::Basic::Save
    Suite Check Error Translations

Test Scan Interval Exceeded The Maximum Size
    Input Text          //*[@id="interval"]  12345678901
    Input Text          //*[@id="ipfirst"]  1.1.1.1
    Input Text          //*[@id="iplast"]  1.1.1.5
    GUI::Basic::Save
    Suite Check Error Translations



*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    gui::basic::spinner should be invisible
    GUI::ManagedDevices::Open Network Scan

Suite Teardown
    GUI::Basic::Cancel
    Click element   xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[1]/input
    Click Element  jquery=#delButton
    Handle Alert
    GUI::Basic::Logout
    Close all Browsers

Suite Check Error Translations
    GUI::Basic::Japanese::Test Translation      //*[@id="errormsg"]
   # GUI::Basic::Japanese::Test Translation      //*[