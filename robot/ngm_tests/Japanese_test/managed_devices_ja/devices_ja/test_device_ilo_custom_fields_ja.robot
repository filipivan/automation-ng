*** Settings ***
Documentation    Testing if Elements in the ILO Devices Custom Fields tab is functional
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***
Create ILO Device
    ${count}=	GUI::Basic::Count Table Rows	SPMTable
	Run Keyword If	${count} > 0	Log	"Device already exists"
	Run Keyword If	${count} == 0	Add Missing Device

Drilldown and Access Custom Tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element   xpath=/html/body/div[7]/div[2]/div[2]/div[2]/div[3]/table/tbody/tr/td[2]/a
    GUI::Basic::Spinner Should Be Invisible
    Click element   jquery=#spmcustom_nav
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be Visible   jquery=#addButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#addButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#delButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#editButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#returnButton
    GUI::Basic::Japanese::Test Translation Table Header With Checkbox   3

Test Add Custom Command
    Click element   jquery=#addButton
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value   jquery=#saveButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#cancelButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#returnButton
    gui::basic::check translation no id previous sibling    custom_name     custom_value
    Input Text      jquery=#custom_name     thisIsMyCmd
    Input Text      jquery=#custom_value    thereAreManyLikeIt
    Click element   jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    Page should Contain Element     xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[2]

Test Edit Custom Command
    Select Checkbox     xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[1]/input
    Click Element       jquery=#editButton
    GUI::Basic::Spinner Should Be Invisible
    Input Text          jquery=#custom_value    butThisOneIsMine
    Click Element       jquery=#saveButton
    GUI::Basic::Spinner Should Be Invisible
    ${TXT}=     Get Text    xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[3]
    Should Be Equal     ${TXT}      butThisOneIsMine

Test Delete Custom Command
    Select Checkbox     xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr/td[1]/input
    Click element       jquery=#delButton
    GUI::Basic::Spinner Should Be Invisible
    Page Should Not COntain Element     xpath=//*[@id="tbody"]/tr
    Click Element       jquery=#returnButton



*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::ManagedDevices::Open Devices tab

Suite Teardown
    GUI::ManagedDevices::Delete Device If Exists        aaaa
    GUI::Basic::Logout
    Close all Browsers

Test List Dropdowns
    [Arguments]  @{LOC}
    :FOR    ${I}    IN  @{LOC}
    \   Test Dropdown Options  ${I}

Test Dropdown Options
    [Arguments]  ${LOC}
    @{TXT}=     Get List Items  jquery=#${LOC}
    :FOR    ${I}    IN      @{TXT}
    \   run keyword and continue on failure  Should Contain  ${I}    [

Add Missing Device
    Wait Until Element Is Visible       jquery=#addButton
    Click Element   jquery=#addButton
    Wait Until Element Is Visible       jquery=#cancelButton
    Input Text      xpath=//input[@id="spm_name"]       aaaa
    Wait Until Element Is Visible       jquery=#saveButton
    Click Element   jquery=#saveButton