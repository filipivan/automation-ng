*** Settings ***
Documentation    Testing if Elements in the device console Devices Logging tab is functional
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Test Cases ***
Create ILO Device
    ${count}=	GUI::Basic::Count Table Rows	SPMTable
	Run Keyword If	${count} > 0	Log	"Device already exists"
	Run Keyword If	${count} == 0	Add Missing Device

Drilldown and Access Logging Tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element   xpath=/html/body/div[7]/div[2]/div[2]/div[2]/div[3]/table/tbody/tr/td[2]/a
    GUI::Basic::Spinner Should Be Invisible
    Click element   jquery=#spmextended_nav
    GUI::Basic::Spinner Should Be Invisible
    Element Should Be Visible   jquery=#saveButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#saveButton
    GUI::Basic::Japanese::Test Translation With Value   jquery=#returnButton


Select all Checkboxes and Check Translation
    Select Checkbox     jquery=#databuf
    Select Checkbox     jquery=#alertenable
    GUI::Basic::Check Translation No ID     databuf     alertenable

Test Textbox and Dropdown Translations
    gui::basic::check translation no id previous sibling    spm_name
    :FOR    ${I}    IN RANGE    1   6
    \   GUI::Basic::Check Translation No ID Previous Sibling    alertstring${I}     dbscript${I}
    \   Test List Dropdowns     dbscript${I}
    \   GUI::Auditing::Auto Input Tests     alertstring${I}     12  .................................   str

    Click Element   jquery=#returnButton


*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::ManagedDevices::Open Devices tab

Suite Teardown
    GUI::ManagedDevices::Delete Device If Exists        aaaa
    GUI::Basic::Logout
    Close all Browsers

Test List Dropdowns
    [Arguments]  @{LOC}
    :FOR    ${I}    IN  @{LOC}
    \   Test Dropdown Options  ${I}

Test Dropdown Options
    [Arguments]  ${LOC}
    @{TXT}=     Get List Items  jquery=#${LOC}
    :FOR    ${I}    IN      @{TXT}
    \   run keyword and continue on failure  Should Contain  ${I}    [

Add Missing Device
    Wait Until Element Is Visible       jquery=#addButton
    Click Element   jquery=#addButton
    Wait Until Element Is Visible       jquery=#cancelButton
    Input Text      xpath=//input[@id="spm_name"]       aaaa
    Wait Until Element Is Visible       jquery=#saveButton
    Select From List By Value   type    device_console
    Wait Until element Is Visible       jquery=#saveButton
    Click Element   jquery=#saveButton