*** Settings ***
Documentation    Testing if Elements in the Managed Devieces -> Devices -> Edit Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2

*** Variables ***
${Emp}

*** Test Cases ***
Test Devices Edit
    GUI::ManagedDevices::Add Dummy Device Console       Dummy   device_console  ${Emp}  ${Emp}  ${Emp}  no  ${Emp}  ondemand
    GUI::Basic::Spinner Should Be Invisible
    Click Element       //*[@id="Dummy"]/td[1]/input
    Click Element       //*[@id="editButton"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="cancelButton"]

Test Devices Edit Data Logging
    GUI::Basic::Japanese::Test Translation  //*[@id="logging"]
    Select Checkbox     //*[@id="databuf"]
    Select Checkbox     //*[@id="alertenable"]
    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[5]/div[1]/div/div/div[2]/div/div/div/div[1]
    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[5]/div[1]/div/div/div[2]/div/div/div/div[2]/div/div[1]
    : FOR    ${INDEX}    IN RANGE    1    11
    \    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[5]/div[1]/div/div/div[2]/div/div/div/div[2]/div/div[2]/div[${INDEX}]

Test Devices Edit Event Logging
    ${Event}=   Run Keyword And Return Status   Page Should Not Contain Checkbox    //*[@id="eventlog"]
    Pass Execution If   ${Event}==True      No Event Logging Possible
    Select Checkbox     //*[@id="eventlog"]
    Select Checkbox     //*[@id="eventalarm"]
    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[5]/div[2]/div/div/div[1]
    GUI::Basic::Japanese::Test Translation    //*[@id="eventintunit"]/option[1]
    GUI::Basic::Japanese::Test Translation    //*[@id="eventintunit"]/option[2]
    : FOR    ${INDEX}    IN RANGE    1      4
    \    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[5]/div[2]/div/div/div[2]/div[${INDEX}]
    : FOR    ${INDEX}    IN RANGE    1    11
    \    GUI::Basic::Japanese::Test Translation    //*[@id="addSPM_Form"]/div[5]/div[2]/div/div/div[2]/div[1]/div[2]/div[${INDEX}]

*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[5]
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    Click Element       //*[@id="cancelButton"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Delete Device If Exists        Dummy
    GUI::Basic::Logout
    Close all Browsers