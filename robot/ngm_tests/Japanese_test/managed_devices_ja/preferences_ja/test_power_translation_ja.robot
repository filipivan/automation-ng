*** Settings ***
Documentation    Testing if Elements in the Managed Devieces -> Devices Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2


*** Test Cases ***

Test Tab Translation
    gui::basic::japanese::test translation  xpath=/html/body/div[7]/div[1]/a[1]/span

Test Save Button
    GUI::Basic::Japanese::Test Translation With Value  jquery=#saveButton

Test Field Labels
    gui::basic::check translation no id previous sibling    exitOption  exitLabel   statusOption    statusLabel
    ...     onOption    onLabel     offOption   offLabel    cycleOption     cycleLabel

Test Error Throw Exit Option
    Dropdown Loop Testing  exitOption   1

Test Error Throw Menu Option
    Dropdown Loop Testing  statusOption     2

Test Error Throw Power On Option
    Dropdown Loop Testing  onOption     3

Test Error Throw Power Off Option
    Dropdown Loop Testing  offOption    4

Test Error Throw Power Cycle Option
    Dropdown Loop Testing   cycleOption     5


*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[5]
    GUI::Basic::Spinner Should Be Invisible
    Click Element       xpath=/html/body/div[6]/div/ul/li[5]
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers
    
Dropdown Loop Testing
    [Arguments]     ${LOC}  ${NUM}
    :FOR    ${I}    IN RANGE    1   6
    \   ${EQ}=      run keyword and return status   Should Not Be Equal     '${I}'    '${NUM}'
    \   ${IS}=      Convert To String   ${I}
    \   run keyword if  ${EQ}   run keyword and continue on failure  Check Error Throw  ${LOC}  ${IS}
    Select From List By Value   jquery=#${LOC}  ${NUM}
    Click element   jquery=#saveButton
    
Check Error Throw
    [Arguments]     ${LOC}  ${NUM}
    Select From List By Value   jquery=#${LOC}  ${NUM}
    Click element   jquery=#saveButton
    ${TST}=     Execute Javascript      var er=document.getElementById('${LOC}');if(er==null){return false;}var ro=er.parentNode.nextSibling;if(ro==null){return false;}var txt=ro.textContent;return txt.includes("[");
    Should Be True     ${TST}