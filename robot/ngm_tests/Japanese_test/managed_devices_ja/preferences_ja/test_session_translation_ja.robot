*** Settings ***
Documentation    Testing if Elements in the Managed Devieces -> Devices Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2


*** Test Cases ***

Test Button and Header
    GUI::Basic::Japanese::Test Translation With Value  jquery=#saveButton
    gui::basic::japanese::test translation  xpath=/html/body/div[7]/div[1]/a[2]/span

Test Disconnect Label
    gui::basic::japanese::test translation  xpath=/html/body/div[7]/div[2]/div[2]/form/div[2]/div/div/div/label
    ${RAND}=    evaluate    random.randint(0,10)    modules=random
    gui::auditing::auto input tests  discSeq    asd     ^#$     &^t     :"?     ^p${RAND}

Test Disconnect HotKey Error
    Input Text          //*[@id="discSeq"]  **
    GUI::Basic::Save
    Suite Check Error Translations


*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Basic::Spinner Should Be Invisible
    Click Element       //*[@id="main_menu"]/li[5]
    GUI::Basic::Spinner Should Be Invisible
    Click Element       xpath=/html/body/div[6]/div/ul/li[5]
    GUI::Basic::Spinner Should Be Invisible
    Click Element       xpath=/html/body/div[7]/div[1]/a[2]/span
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers

Suite Check Error Translations
    GUI::Basic::Japanese::Test Translation      //*[@id="errormsg"]
   # GUI::Basic::Japanese::Test Translation      //*[@id="globalerr"]/div/div/p