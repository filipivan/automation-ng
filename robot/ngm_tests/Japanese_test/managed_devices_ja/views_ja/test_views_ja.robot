*** Settings ***
Documentation    Testing if Elements in the Managed Devieces -> Views Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2


*** Variables ***
${Empty}

*** Test Cases ***
Test Views Tab nonAccess Controls Buttons
    GUI::Basic::Japanese::Test Translation  //*[@id="tree"]
    GUI::Basic::Japanese::Test Translation  //*[@id="image"]

Test Views Tree Table Header
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="addButton"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="delButton"]
    GUI::Basic::Japanese::Test Translation Table Header With Checkbox     2

Test Views Tree
    Click Element   //*[@id="tbody"]/tr/td[2]/a[2]
    GUI::Basic::Spinner Should Be Invisible
    : FOR    ${INDEX}    IN RANGE    1    10
    \    GUI::Basic::Japanese::Test Translation     //*[@id="addSPMContainers_Form"]/div[2]/div/div/div[${INDEX}]
    GUI::Basic::Japanese::Test Translation          //*[@id="addSPMContainers_Form"]/div[2]/div/div/div[2]/div/div[1]
    GUI::Basic::Japanese::Test Translation          //*[@id="addSPMContainers_Form"]/div[2]/div/div/div[2]/div/div[2]
    GUI::Basic::Japanese::Test Translation          //*[@id="targetSelection"]/div/div[1]/div[2]/div/div[1]/button
    GUI::Basic::Japanese::Test Translation          //*[@id="targetSelection"]/div/div[1]/div[2]/div/div[2]/button
    Select Radio Button  spm_container_type     search
    GUI::Basic::Japanese::Test Translation          //*[@id="addSPMContainers_Form"]/div[2]/div/div/div[2]/div/div[3]
    GUI::Basic::Japanese::Test Translation          //*[@id="addSPMContainers_Form"]/div[2]/div/div/div[2]/div/div[4]

Test Aggregation Boxs
    : FOR    ${INDEX}    IN RANGE    1    6
    \    Suite Check Aggregation       ${INDEX}
    Click Element   //*[@id="cancelButton"]
    GUI::Basic::Spinner Should Be Invisible

Test Views Tree Add Error Empty Name
    GUI::Basic::Add
    Select Radio Button  spm_container_type     search
    GUI::Basic::Save
    Suite Check Error Translations

Test Views Tree Add Error Special Character Name
    Input and Test Errors          //*[@id="spm_container_name"]    **

Test Views Tree Add Error No Parent
    Input and Test Errors          //*[@id="spm_container_name"]    Dummy

Test Views Image Tab
    Click Element   //*[@id="cancelButton"]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   //*[@id="image"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="addButton"]

Test Views Image Add Tab
    Click Element   //*[@id="addButton"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="saveButton"]
    GUI::Basic::Japanese::Test Translation With Value  //*[@id="cancelButton"]
    : FOR    ${INDEX}    IN RANGE    2    5
    \    GUI::Basic::Japanese::Test Translation    //*[@id="form"]/div[2]/div/div/div[${INDEX}]
    : FOR    ${INDEX}    IN RANGE    1    4
    \    GUI::Basic::Japanese::Test Translation    //*[@id="form"]/div[2]/div/div/div[3]/div/div[${INDEX}]
    Select Radio Button  image_sel  image_sel_client
    GUI::Basic::Japanese::Test Translation    //*[@id="file_form"]

Test Views Image Add Empty Refresh Error
    Select Radio Button  image_sel  image_sel_local
    Input Text                     //*[@id="name"]         Dummy
    Input and Test Errors          //*[@id="refresh"]      ${Empty}

Test Views Image Add Zero Value Refresh Error
    Input and Test Errors          //*[@id="refresh"]  0

Test Views Image Add String Value Refresh Error
    Input and Test Errors          //*[@id="refresh"]  Dummy

Test Views Image Add Empty Name Error
    Input Text                     //*[@id="name"]         ${Empty}
    Input and Test Errors          //*[@id="refresh"]      20

Test Views Image Add Special Character Name Error
    Input and Test Errors          //*[@id="name"]     **

*** Keywords ***

Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[5]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(2)
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers
Suite Check Aggregation
    [Arguments]     ${agg_num}
    ${Div_num}=     Evaluate    ${agg_num} + 4
    Select Checkbox    //*[@id="aggregation${agg_num}"]
    : FOR    ${INDEX}    IN RANGE    1    5
    \    GUI::Basic::Japanese::Test Translation    //*[@id="addSPMContainers_Form"]/div[2]/div/div/div[${Div_num}]/div[2]/div[${INDEX}]/label
    GUI::Basic::Japanese::Test Translation         //*[@id="addSPMContainers_Form"]/div[2]/div/div/div[${Div_num}]/div[2]/div[5]/div/label
    GUI::Basic::Japanese::Test Translation         //*[@id="addSPMContainers_Form"]/div[2]/div/div/div[${Div_num}]/div[2]/div[6]/div/label
    Test Aggregation Errors                        ${agg_num}

Test Aggregation Errors
    [Arguments]     ${agg_num}
    Select Checkbox                 //*[@id="aggregation${agg_num}"]
    GUI::Basic::Save
    Suite Check Error Translations
    Input and Test Errors           //*[@id="agg_name${agg_num}"]       **
    Input Text                      //*[@id="agg_name${agg_num}"]       Dummy
    Input and Test Errors           //*[@id="agg_interval${agg_num}"]   0
    Input Text                      //*[@id="agg_name${agg_num}"]       Dummy
    Input and Test Errors           //*[@id="agg_interval${agg_num}"]   Dummy
    : FOR    ${INDEX}    IN RANGE    1    13
    \    GUI::Basic::Japanese::Test Translation    //*[@id="agg_type${agg_num}"]/option[${INDEX}]
    Unselect Checkbox               //*[@id="aggregation${agg_num}"]

Input and Test Errors
    [Arguments]     ${loc}      ${Val}
    Input Text          ${Loc}    ${Val}
    GUI::Basic::Save
    Suite Check Error Translations

Suite Check Error Translations
    GUI::Basic::Japanese::Test Translation      //*[@id="errormsg"]
    #GUI::Basic::Japanese::Test Translation      //*[@id="globalerr"]/div/div/p