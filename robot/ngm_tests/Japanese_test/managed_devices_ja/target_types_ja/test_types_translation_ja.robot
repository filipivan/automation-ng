*** Settings ***
Documentation    Testing if Elements in the Managed Devices Menu and Sub Menu are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2


*** Test Cases ***

Test Button Translations
    gui::basic::japanese::test translation with value  xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[2]/input[1]
    gui::basic::japanese::test translation with value  jquery=#delButton

Test Table Header Translation
    gui::basic::japanese::test translation table header with checkbox  4

Test Table Body Translation
    :FOR    ${I}    IN RANGE    1   37
    \   run keyword and continue on failure  Table Row Check  ${I}

Test Device Clone
    Select Checkbox     xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[1]/td[1]/input
    Click element       xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[2]/input[1]
    gui::basic::spinner should be invisible
    gui::basic::check translation no id previous sibling    spmtargettype_name  targettype_source
    gui::auditing::auto input tests     spmtargettype_name      ;   @#$     ,./;'   name
    gui::basic::spinner should be invisible
    gui::basic::delete rows in table    \#SPMTargettypeTable      name

Test Protected Type Error
    GUI::Basic::Spinner Should Be Invisible
    Select Checkbox     xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[1]/td[1]/input
    Click Element       jquery=#delButton
    Handle Alert
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation      xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div/div

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    GUI::Basic::Spinner Should Be Invisible
    GUI::ManagedDevices::Open Types tab

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers

Device Check Translation
    [Arguments]  ${RN}
    Click Element   xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[${RN}]/td[2]/a
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value  jquery=#saveButton
    gui::basic::japanese::test translation with value  jquery=#cancelButton
    gui::basic::check translation no id previous sibling  spmtargettype_name    family  protocol    login_prompt
    ...     passwd_prompt   shell_prompt    console_quit
    ${CONT}=    run keyword and return status  page Should Contain element     jquery=#ssh_options
    run keyword if  ${CONT}     gui::basic::check translation no id previous sibling  ssh_options
    ${CONT2}=    run keyword and return status  page Should Contain element     jquery=#oemsupport
    run keyword if  ${CONT2}    test ipmi box
    Test Dropdown Options  protocol
    Click Element   jquery=#cancelButton
    GUI::Basic::Spinner Should Be Invisible

Test Dropdown Options
    [Arguments]  ${LOC}
    @{TXT}=     Get List Items  jquery=#${LOC}
    :FOR    ${I}    IN      @{TXT}
    \   run keyword and continue on failure  Should Contain  ${I}    [

Test IPMI Box
    gui::basic::check translation no id previous sibling    oemsupport
    test dropdown options  oemsupport

Table Row Check
    [Arguments]  ${ROWNUM}
    :FOR    ${I}    IN RANGE    2   5
    \   run keyword if  ${I}==2     run keyword and continue on failure  Device Check Translation    ${ROWNUM}
    \   run keyword if  ${I}==2     run keyword and continue on failure  GUI::Basic::Japanese::Test Translation  xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[${ROWNUM}]/td[${I}]/a
    \   ...     ELSE    run keyword and continue on failure  GUI::Basic::Japanese::Test Translation  xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[${ROWNUM}]/td[${I}]
