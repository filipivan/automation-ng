*** Settings ***
Resource    ngmGuiBasicKeywords_ja.robot
Resource    ngmGUIAccessKeywords_ja.robot
Resource    ngmGUISystemKeywords_ja.robot
Resource    ngmGUIManagedDevicesKeywords_ja.robot
Resource    ngmGUISecurityKeywords_ja.robot
Resource    ngmGUIAuditingKeywords_ja.robot
Resource    ngmGUIDashboardKeywords_ja.robot
#Resource    ngmCLIKeywords_ja.robot
Resource    systemKeywords_ja.robot
Resource    commonKeywords_ja.robot
Resource    apiKeywords_ja.robot
Resource    ngmGUICloudKeywords_ja.robot
Resource    ngmGUIClusterKeywords_ja.robot
Resource    ngmGUINetworkKeywords_ja.robot
Resource    ngmGUITrackingKeywords_ja.robot



Library     SeleniumLibrary    run_on_failure=Capture Page Screenshot      screenshot_root_directory=${SCREENSHOTDIR}
Library     SSHLibrary
Library     String
Library     Collections
Library     OperatingSystem
Library     Process
Library     DateTime
Library     RequestsLibrary

*** Variables ***
${SCREENSHOTDIR}    ./Screenshots/
