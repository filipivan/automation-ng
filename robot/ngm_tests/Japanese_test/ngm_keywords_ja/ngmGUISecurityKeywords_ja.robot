*** Keywords ***

GUI::Security::Open Security
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid security tab
	...     == ARGUMENTS ==
	...     -   SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Managed devices Tab is open and all elements are accessable
	GUI::Basic::Wait Until Element Is Accessible	jquery=#main_menu > li:nth-child(7) > a
	Execute Javascript	window.document.evaluate("//*[@id='main_menu']", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element	jquery=#main_menu > li:nth-child(7) > a
	GUI::Basic::Spinner Should Be Invisible

GUI::Security::Open Firewall tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Security::Firewall tab
	...     == ARGUMENTS ==
	...     -   SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Firewall Tab is open and all elements are accessible
	[Tags]      GUI     SYSTEM      LICENSE
	GUI::Security::Open Security
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(5) > a
	GUI::Security::Wait For Firewall Table

GUI::Security::Wait For Firewall Table
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=body > div.main_menu > div > ul > li.active > a	Firewall
	Wait Until Element Is Visible	jquery=#addButton
	Wait Until Element Is Visible	jquery=#policyButton
	Wait Until Element Is Visible	jquery=#thead > tr > th:nth-child(1) > input[type="checkbox"]
#	${FIRST_COL_NAME}=	Get Text	jquery=#thead > tr > th:nth-child(2)
#	Should Be Equal	${FIRST_COL_NAME}	Chain

GUI::Security::Add Firewall Chain
	[Arguments]	${NAME}	${TYPE}
	Wait Until Element Is Visible	jquery=#addButton
	Click Element	jquery=#addButton
	Wait Until Element Is Visible	jquery=#saveButton
	Input Text	jquery=#chain	${NAME}
	Select Radio Button	type	${TYPE}
	Click Element	jquery=#saveButton
	GUI::Security::Wait For Firewall Table
	Table Should Contain	jquery=#filterChains	${NAME}

GUI::Security::Open Local Accounts tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Security::Local Accounts tab
	...     == ARGUMENTS ==
	...     -   SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Local Accounts Tab is open and all elements are accessible
	[Tags]      GUI     SYSTEM      LICENSE
	GUI::Security::Open Security
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(1) > a
	GUI::Basic::Spinner Should Be Invisible

GUI::Security::Local Accounts::Add
	[Arguments]	${NAME}	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Security::Local Accounts tab and add a new user
	...     == ARGUMENTS ==
	...     -   NAME = The username of the new to be created user (the username will be the password too)
	...     -   SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     A new users will appear in the users table
	[Tags]      GUI     SECURITY	LOCAL ACCOUNTS
	GUI::Security::Open Local Accounts tab
	GUI::Security::Local Accounts::Delete	${NAME}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=#addButton
	Wait Until Element Is Visible	jquery=#uName
	Input Text	jquery=#uName	${NAME}
	Wait Until Element Is Visible	jquery=#uPasswd
	Input Text	jquery=#uPasswd	${NAME}
	Wait Until Element Is Visible	jquery=#cPasswd
	Input Text	jquery=#cPasswd	${NAME}
	GUI::Basic::Save

GUI::Security::Local Accounts::Wait For Local Accounts Table
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#addButton
	Wait Until Element Is Visible	jquery=#thead > tr > th:nth-child(1) > input[type="checkbox"]

GUI::Security::Local Accounts::Delete
	[Arguments]	${USERNAME}	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Security::Local Accounts tab and delete an user
	...     == ARGUMENTS ==
	...     -   NAME = The username of the new to be deleted user (the username will be the password too)
	...     -   SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     An user will be deleted/disappears in the users table
	[Tags]      GUI     SECURITY	LOCAL ACCOUNTS
	${USERNAMES}=	Create List	${USERNAME}
	GUI::Security::Open Local Accounts tab
	GUI::Basic::Delete Rows In Table If Exists	jquery=table\#user_namesTable	${USERNAMES}

GUI::Security::Open Password Rules tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Security::Password Rules tab
	...     == ARGUMENTS ==
	...     -   SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Local Accounts Tab is open and all elements are accessible
	[Tags]      GUI     SYSTEM      LICENSE
	GUI::Security::Open Security
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(2) > a
	GUI::Basic::Spinner Should Be Invisible

GUI::Security::Open Authorization tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Security::Authorization page
	...     == ARGUMENTS ==
	...     -   SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Authorization page is open and all elements are accessible
	[Tags]	GUI	SECURITY	AUTHORIZATION
	GUI::Security::Open Security
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(3) > a
	GUI::Security::Wait For Authorization Table

GUI::Security::Authorization::Open Acct Members tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Security::Authorization page
	...     == ARGUMENTS ==
	...     -   SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Authorization page is open and all elements are accessible
	[Tags]	GUI	SECURITY	AUTHORIZATION
	GUI::Security::Open Authorization tab
	Wait Until Element Is Visible   jquery=#refresh-icon
	Click element   jquery=.odd > td:nth-child(2) > a:nth-child(1)
	Wait until Element Is Visible   jquery=#returnButton
	Click Element   jquery=#members > span:nth-child(1)
    Wait Until element is Visible   jquery=#agMember_Table

GUI::Security::Authorization::Open Acct Profile tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Security::Authorization page
	...     == ARGUMENTS ==
	...     -   SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Authorization page is open and all elements are accessible
	[Tags]	GUI	SECURITY	AUTHORIZATION
	GUI::Security::Open Authorization tab
	Wait Until Element Is Visible   jquery=#refresh-icon
	Click element   jquery=.odd > td:nth-child(2) > a:nth-child(1)
	Wait until Element Is Visible   jquery=#returnButton
	Click Element   jquery=#profile > span:nth-child(1)
    Wait Until element is Visible   jquery=#systemPermissions

GUI::Security::Authorization::Open Acct Remote Groups tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Security::Authorization page
	...     == ARGUMENTS ==
	...     -   SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Authorization page is open and all elements are accessible
	[Tags]	GUI	SECURITY	AUTHORIZATION
	GUI::Security::Open Authorization tab
	Wait Until Element Is Visible   jquery=#refresh-icon
	Click element   jquery=.odd > td:nth-child(2) > a:nth-child(1)
	Wait until Element Is Visible   jquery=#returnButton
	Click Element   jquery=#rem_group > span:nth-child(1)
    Wait Until element is Visible   jquery=#remoteGroup

GUI::Security::Authorization::Open Acct Devices tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Security::Authorization page
	...     == ARGUMENTS ==
	...     -   SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Authorization page is open and all elements are accessible
	[Tags]	GUI	SECURITY	AUTHORIZATION
	GUI::Security::Open Authorization tab
	Wait Until Element Is Visible   jquery=#refresh-icon
	Click element   jquery=.odd > td:nth-child(2) > a:nth-child(1)
	Wait until Element Is Visible   jquery=#returnButton
	Click Element   jquery=#spm > span:nth-child(1)
    Wait Until element is Visible   jquery=#agasSpm_Table

GUI::Security::Authorization::Open Acct Outlets tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Security::Authorization page
	...     == ARGUMENTS ==
	...     -   SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Authorization page is open and all elements are accessible
	[Tags]	GUI	SECURITY	AUTHORIZATION
	GUI::Security::Open Authorization tab
	Wait Until Element Is Visible   jquery=#refresh-icon
	Click element   jquery=.odd > td:nth-child(2) > a:nth-child(1)
	Wait until Element Is Visible   jquery=#returnButton
	Click Element   jquery=#outlets > span:nth-child(1)
    Wait Until element is Visible   jquery=#agasOutlet_Table

GUI::Security::Wait For Authorization Table
	GUI::Basic::Spinner Should Be Invisible
	[Documentation]	Wait for the authorization page under security menu be ready for use
	Wait Until Element Contains	jquery=body > div.main_menu > div > ul > li.active > a	Authorization
	Wait Until Element Is Visible	jquery=#addButton
	Wait Until Element Is Visible	jquery=#delButton
	Table Header Should Contain	jquery=#author_groupsTable	Group

GUI::Security::Check Default User Groups
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]     Checks if there are the default user groups
	...     == ARGUMENTS ==
	...     -   SCREENSHOT = Should a Screenshot be taken Default is false
	[Tags]      GUI     SECURITY      AUTHORIZATION
	GUI::Security::Wait For Authorization Table

GUI::Security::Drilldown User Group
	[Arguments]	${GROUP}	${SCREENSHOT}=FALSE
	[Documentation]     Open the page to edit the given user group
	GUI::Security::Open Authorization tab
	GUI::Basic::Drilldown table row	\#author_groupsTable	${GROUP}
	GUI::Basic::Add Button Should Be Visible
	GUI::Basic::Delete Button Should Be Visible
	GUI::Basic::Return Button Should Be Visible

GUI::Security::Open User Group Outlet Tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]	Open the user group outlet page be
	Click Element	jquery=div#pod_menu > a:nth-child(5)
	GUI::Security::Wait For User Group Outlet Table

GUI::Security::Wait For User Group Outlet Table
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]	Wait for the user group outlet page be ready for use
	GUI::Basic::Return Button Should Be Visible
	Wait Until Element Is Visible	jquery=#returnButton
	Table Header Should Contain	jquery=#agasOutlet_Table	Device
	Table Header Should Contain	jquery=#agasOutlet_Table	PDU ID
	Table Header Should Contain	jquery=#agasOutlet_Table	Outlet ID
	Table Header Should Contain	jquery=#agasOutlet_Table	Power Mode

GUI::Security::Wait For Authentication Table
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]	Wait until the Authentication table is ready to be used
	GUI::Basic::Add Button Should Be Visible
	GUI::Basic::Delete Button Should Be Visible
	GUI::Basic::Up Button Should Be Visible
	GUI::Basic::Down Button Should Be Visible
	GUI::Basic::Console Button Should Be Visible
	Table Header Should Contain	jquery=#multiAuthTable	Index
	Table Header Should Contain	jquery=#multiAuthTable	Method
	Table Header Should Contain	jquery=#multiAuthTable	Remote Server
	Table Header Should Contain	jquery=#multiAuthTable	Status
	Table Header Should Contain	jquery=#multiAuthTable	Fallback


GUI::Security::Open Authentication Tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]	Open the Authentication page under Security menu
	GUI::Security::Open Security
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(4) > a
	GUI::Security::Wait For Authentication Table

GUI::Security::Authentication::Open Servers Tab
    [Arguments]     ${SCREENSHOT}=FALSE
    GUI::Security::Open Authentication Tab
	Wait Until Element Is Visible   jquery=#refresh-icon
	Click element   xpath=//*[@id="serversTable"]
    Wait Until element is Visible   jquery=#consoleButton

GUI::Security::Authentication::Open Two Factor Tab
    [Arguments]     ${SCREENSHOT}=FALSE
    GUI::Security::Open Authentication Tab
	Wait Until Element Is Visible   jquery=#refresh-icon
	Click element   xpath=//*[@id="secondFactorTable"]
    Wait Until element is Visible   jquery=#multiSecondFactorTable

GUI::Security::Open Services tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Security::Authorization page
	...     == ARGUMENTS ==
	...     -   SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Services page is open and all elements are accessible
	[Tags]	GUI	SECURITY
	GUI::Security::Open Security
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(7) > a
	Wait Until Element Is Visible	jquery=#webService
	Wait Until Element Is Visible	jquery=#services
	Wait Until Element Is Visible	jquery=#sshServer
	Wait Until Element Is Visible	jquery=#crypto
	Wait Until Element Is Visible	jquery=#managedDevices
	Run Keyword If	"${NGVERSION}" <= "4.2"
	...	Wait Until Element Is Visible	jquery=#fail2ban

GUI::Security::Open NAT tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Security::NAT page
	...     == ARGUMENTS ==
	...     -   SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     NAT page is open and all elements are accessible
	[Tags]	GUI	SECURITY
	GUI::Security::Open Security
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(6) > a
	Wait until element is visible   jquery=#nat_natChains

