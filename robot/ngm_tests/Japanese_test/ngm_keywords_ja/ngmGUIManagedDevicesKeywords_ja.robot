*** Keywords ***

GUI::ManagedDevices::Open
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Managed Devices tab
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Managed devices Tab is open and all elements are accessable
	Click Element	jquery=#main_menu > li:nth-child(5) > a
	GUI::Basic::Spinner Should Be Invisible

GUI::ManagedDevices::Open Devices tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Managed Devices::Devices
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Devices Tab is open and all elements are accessable
	[Tags]	GUI	SYSTEM	LICENSE
	Execute Javascript	window.document.evaluate("//*[@id='main_menu']", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	GUI::ManagedDevices::Open
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(1)
	GUI::ManagedDevices::Wait For Managed Devices Table

GUI::ManagedDevices::Devices::Open Logging Menu
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Managed Devices::Devices::Logging
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Devices Tab is open and Logging menu is activated
	[Tags]	GUI	SYSTEM	LICENSE
	Execute Javascript	window.document.evaluate("//*[@id='main_menu']", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element	jquery=#pod_menu > a:nth-child(3)
	GUI::Basic::Spinner Should Be Invisible

GUI::ManagedDevices::Devices::Open Commands Menu
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Managed Devices::Devices::Commands
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Devices Tab is open and Logging menu is activated
	[Tags]	GUI	SYSTEM	LICENSE
	Execute Javascript	window.document.evaluate("//*[@id='main_menu']", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element	jquery=#pod_menu > a:nth-child(5)
	GUI::Basic::Spinner Should Be Invisible
	GUI::ManagedDevices::Wait For Device Commands

GUI::ManagedDevices::Wait For Managed Devices Table
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#main_doc > div > #SPMTable_wrapper
	Wait Until Element Is Visible	jquery=#addButton
	Wait Until Element Is Visible	jquery=table#SPMTable
	Wait Until Element Is Visible	jquery=#thead > tr > th:nth-child(1) > input[type="checkbox"]

GUI::ManagedDevices::Add Dummy Device Console
	[Arguments]	${DEVCON_NAME}	${DEVCON_TYPE}	${DEVCON_IPADDR}	${DEVCON_TCPPORT}	${DEVCON_USERNAME}	${DEVCON_ASKPASS}	${DEVCON_PASSWORD}	${DEVCON_STATUS}
	GUI::ManagedDevices::Open Devices tab
	Click Element	jquery=#addButton
	Wait Until Element Is Visible	jquery=#saveButton
	Input Text	jquery=#spm_name	${DEVCON_NAME}
	Select From List By Value	jquery=#type	${DEVCON_TYPE}
	Input Text	jquery=#phys_addr	${DEVCON_IPADDR}
	Input Text	jquery=#tcpport	${DEVCON_TCPPORT}
	Input Text	jquery=#username	${DEVCON_USERNAME}
	Select Radio Button	askpassword	${DEVCON_ASKPASS}
	Input Text	jquery=#passwordfirst	${DEVCON_PASSWORD}
	Input Text	jquery=#passwordconf	${DEVCON_PASSWORD}
	Select From List By Value	jquery=#status	${DEVCON_STATUS}
	GUI::Basic::Click Element	//*[@id='saveButton']
	GUI::ManagedDevices::Wait For Managed Devices Table
	Table Should Contain	jquery=#SPMTable	${DEVCON_NAME}

GUI::ManagedDevices::Delete Devices
	[Arguments]	@{DEVICES}
	GUI::ManagedDevices::Open Devices tab
	FOR	${DEVICE}	IN	@{DEVICES}
		${DEVICE_COUNT}=	Get Element Count	xpath=//tr[@id='${DEVICE}']
		Run Keyword If	${DEVICE_COUNT} > 0	GUI::Basic::Select Checkbox	//*/tr[@id='${DEVICE}']/*/input
	END
	${DELBUTTON_DISABLED}=	Get Element Attribute	jquery=#delButton	disabled
	Run Keyword If	'${DELBUTTON_DISABLED}'=='None'	GUI::ManagedDevices::Delete With Alert

GUI::ManagedDevices::Select Checkboxes
	[Arguments]	@{DEVICES}
	FOR	${DEVICE}	IN	@{DEVICES}
		${DEVICE_COUNT}=	Get Element Count	xpath=//tr[@id='${DEVICE}']
		Run Keyword If	${DEVICE_COUNT} > 0	GUI::Basic::Select Checkbox	//*/tr[@id='${DEVICE}']/*/input
	END

GUI::ManagedDevices::Save And Return
	Click Element	jquery=#saveButton
	Wait Until Element Is Not Visible	jquery=#globalerr
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=#returnButton
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=a[id="${DUMMY_DEVICE_CONSOLE_NAME}"]
	GUI::ManagedDevices::Enter Device	${DUMMY_DEVICE_CONSOLE_NAME}
	Wait Until Element Is Visible	jquery=a#dummy_device_console

GUI::ManagedDevices::Open Auto Discovery tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Managed Devices::Devices
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Devices Tab is open and all elements are accessable
	[Tags]	GUI	MANAGED_DEVICES	LICENSE
	GUI::ManagedDevices::Open
	GUI::Basic::Wait Until Element Is Accessible	jquery=body > div.main_menu > div > ul > li:nth-child(4)
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(4)
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#addButton

GUI::ManagedDevices::Open Network Scan
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Manage::Auto Discovery::Network Scan
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Network scan Tab is open and all elements are accessable
	[Tags]	GUI	MANAGED_DEVICES	LICENSE
	GUI::ManagedDevices::Open Auto Discovery tab

GUI::ManagedDevices::Add Dummy Network Scan
	[Arguments]	${NETWORK_NAME}	${IP_RANGE_START}	${IP_RANGE_END}
	GUI::ManagedDevices::Open Auto Discovery tab
	GUI::Basic::Add
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#netdiscover_id	jquery=#ipfirst	jquery=#iplast
	Input Text	jquery=#netdiscover_id	${NETWORK_NAME}
	Input Text	jquery=#ipfirst	${IP_RANGE_START}
	Input Text	jquery=#iplast	${IP_RANGE_END}
	GUI::Basic::Wait Until Element Is Accessible	jquery=#saveButton
	GUI::Basic::Save
	GUI::Basic::Wait Until Element Is Accessible	jquery=a#${NETWORK_NAME}

GUI::ManagedDevices::Enter Network Scan
	[Arguments]	${NETWORK_NAME}
	GUI::ManagedDevices::Open Auto Discovery tab
	Click Element	jquery=a#${NETWORK_NAME}
	GUI::Basic::Wait Until Elements Are Accessible	jquery=#ipfirst	jquery=#iplast

GUI::ManagedDevices::Delete With Alert
	GUI::Basic::Click Element Accept Alert	//*[@id='delButton']

GUI::ManagedDevices::Delete Network Scan
	[Arguments]	@{NETWORK_NAMES}
	GUI::ManagedDevices::Open Auto Discovery tab
	FOR	${NETWORK_NAME}	IN	@{NETWORK_NAMES}
		${NETWORK_COUNT}=	Get Element Count	xpath=//tr[@id='${NETWORK_NAME}']
		Run Keyword If	${NETWORK_COUNT} > ${0}	Select Checkbox	jquery=tr#${NETWORK_NAME} input[type=checkbox]
	END
	${DELBUTTON_DISABLED}=	Get Element Attribute	jquery=#delButton	disabled
	Run Keyword If	'${DELBUTTON_DISABLED}'=='None'	GUI::ManagedDevices::Delete With Alert
	FOR	${NETWORK_NAME}	IN	@{NETWORK_NAMES}
		${NETWORK_COUNT}=	Get Element Count	xpath=//tr[@id='${NETWORK_NAME}']
		Should Be Equal	${NETWORK_COUNT}	${0}
	END

GUI::ManagedDevices::Enter Device
	[Arguments]	${DEVICE_NAME}
	GUI::Basic::Click Element	//*/a[@id='${DEVICE_NAME}']
	GUI::Basic::Spinner Should Be Invisible

GUI::ManagedDevices::Add Device
	[Arguments] 	 ${DEVCON_NAME} 	 ${DEVCON_TYPE} 	 ${DEVCON_IPADDR} 	 ${DEVCON_USERNAME} 	 ${DEVCON_ASKPASS} 	 ${DEVCON_PASSWORD} 	 ${DEVCON_STATUS}	${DEVCON_URL}=http://%IP
	GUI::ManagedDevices::Open Devices tab
	Click Element	jquery=#addButton
	Wait Until Element Is Visible	jquery=#saveButton
	Input Text	jquery=#spm_name	${DEVCON_NAME}
	Select From List By Value	jquery=#type	${DEVCON_TYPE}
	Input Text	jquery=#phys_addr	${DEVCON_IPADDR}
	Run Keyword If	'${DEVCON_TYPE}' == 'virtual_console_vmware'	Run Keywords	Element Should Not Be Visible	id=username
	...	AND	Element Should Not Be Visible	id=askpassword
	...	AND	Element Should Not Be Visible	id=passwordfirst
	...	AND	Element Should Not Be Visible	id=passwordconf
	...	AND	Element Should Be Visible	id=vmmanager
	...	AND	Select From List By Index	id=vmmanager	1
	...	ELSE	Run Keywords	Element Should Be Visible	id=username
	...	AND	Element Should Be Visible	id=askpassword
	...	AND	Element Should Be Visible	id=passwordfirst
	...	AND	Element Should Be Visible	id=passwordconf
	...	AND	Input Text	jquery=#username	${DEVCON_USERNAME}
	...	AND	Select Radio Button	askpassword	${DEVCON_ASKPASS}
	...	AND	Input Text	jquery=#passwordfirst	${DEVCON_PASSWORD}
	...	AND	Input Text	jquery=#passwordconf	${DEVCON_PASSWORD}
	Select From List By Value	jquery=#status	${DEVCON_STATUS}
	Input Text	jquery=#url	${DEVCON_URL}
	GUI::Basic::Click Element	//*[@id='saveButton']
	GUI::ManagedDevices::Wait For Managed Devices Table
	Table Should Contain	jquery=#SPMTable	${DEVCON_NAME}

GUI::ManagedDevices::Add Device If Not Exists
	[Arguments]	${DEVCON_NAME}	${DEVCON_TYPE}	${DEVCON_IPADDR}	${DEVCON_USERNAME}	${DEVCON_ASKPASS}	${DEVCON_PASSWORD}	${DEVCON_STATUS}
	GUI::ManagedDevices::Open Devices tab
	${count}=	GUI::Basic::Count Table Rows With Id	SPMTable	${DEVCON_NAME}
	Run Keyword If	${count} > 0	Log	"Device already exists"
	Run Keyword If	${count} == 0	GUI::ManagedDevices::Add Device	${DEVCON_NAME}	${DEVCON_TYPE}	${DEVCON_IPADDR}	${DEVCON_USERNAME}	${DEVCON_ASKPASS}	${DEVCON_PASSWORD}	${DEVCON_STATUS}

GUI::ManagedDevices::Open Discovery Now
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the discovery now tab under Managed Devices
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Discovery now tab open and ready to interaction
	[Tags]	GUI	MANAGED_DEVICES	DISCOVERY
	GUI::ManagedDevices::Open Auto Discovery tab
	Click Element	jquery=#pod_menu > a:nth-child(6)
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Discovery Now Button Should Be Visible

GUI::ManagedDevices::Auto::Open Auto Discovery Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the discovery now tab under Managed Devices
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Discovery now tab open and ready to interaction
	[Tags]	GUI	MANAGED_DEVICES	DISCOVERY
	GUI::ManagedDevices::Open Auto Discovery tab
	Click Element	jquery=#pod_menu > a:nth-child(3)
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible   jquery=#SPMDTable

GUI::ManagedDevices::Open Discovery Logs
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the discovery logs tab under Managed Devices
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Discovery rules tab open and ready to interaction
	[Tags]	GUI	MANAGED_DEVICES	DISCOVERY
	GUI::ManagedDevices::Open Auto Discovery tab
	Click Element	jquery=#pod_menu > a:nth-child(5)
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	id=DiscoveryLogTable

GUI::ManagedDevices::Add Discovery Rule
	[Arguments]	${DISC_NAME}	${SCREENSHOT}=FALSE
	[Tags]	GUI	MANAGED_DEVICES	DISCOVERY
	Wait Until Element Is Visible	id=SPMDTable
	GUI::Basic::Add
	Wait Until Element Is Visible	jquery=#discovery_name
	Input Text	jquery=#discovery_name	${DISC_NAME}
	GUI::Basic::Save
	GUI::Basic::Table Should Has Row With Id	SPMDTable	${DISC_NAME}

GUI::ManagedDevices::Add VM Manager Discovery Rule
	[Arguments]	${FIELDS}	${SCREENSHOT}=FALSE
	[Documentation]     Adds new VM Manager
	...	== ARGUMENTS ==
	...     -   FIELDS = Dictionary with fields values. The key should be the
	...                  field id.
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	New VM Manager added
	[Tags]	GUI	MANAGED_DEVICES	DISCOVERY
	Wait Until Element Is Visible	id=SPMDTable
	GUI::Basic::Add

	Wait Until Element Is Visible	id=discovery_name
	Input Text	id=discovery_name	${FIELDS["discovery_name"]}
	Wait Until Element Is Visible	id=source
	Select Radio Button	source	${FIELDS["source"]}
	Wait Until Element Is Visible	id=operation
	Select From List By Value	id=operation	${FIELDS["operation"]}
	Wait Until Element Is Visible	id=seed
	Select From List By Value	id=seed	${FIELDS["seed"]}

	GUI::Basic::Save
	GUI::Basic::Table Should Has Row With Id	SPMDTable	${FIELDS["discovery_name"]}

GUI::ManagedDevices::Delete Discovery Rule
	[Arguments]	${DISC_NAME}	${SCREENSHOT}=FALSE
	[Tags]	GUI	MANAGED_DEVICES	DISCOVERY
	GUI::Basic::Delete Rows In Table	\#SPMDTable	${DISC_NAME}
	GUI::Basic::Table Should Not Have Row With Id	table	${DISC_NAME}

GUI::ManagedDevices::Launch Discovery By Id
	[Arguments]	${IDS}	${SCREENSHOT}=FALSE
	[Documentation]     Launch the discovery
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Launch the given discovery process
	[Tags]	GUI	MANAGED_DEVICES	DISCOVERY
	GUI::ManagedDevices::Open Discovery Now
	GUI::Basic::Select Table Rows	jquery=#discovernowTable	${IDS}
	GUI::Basic::Discovery Now

GUI::ManagedDevices::Wait For Outlet Discovery
	[Arguments]	${PDU}	${SCREENSHOT}=FALSE
	[Documentation]	Wait for discovery found some outlet
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Return when some outlet has been discovery
	[Tags]	GUI	MANAGED_DEVICES	DISCOVERY
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Drilldown table row	\#SPMTable	${PDU}
	Wait Until Keyword Succeeds	10 times	30 seconds	Run Keywords	Click Element	jquery=#pod_menu > a:nth-child(6)	AND	GUI::Basic::Table Rows Count Should Be Greater	SPMOutletsTable	0

GUI::ManagedDevices::Delete Device If Exists
	[Arguments]	${DEVICE}	${SCREENSHOT}=FALSE
	[Documentation]	Delete device if they exists in the device table
	...	== ARGUMENTS ==
	...	-   DEVICES = Device to be deleted
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Delete the device in the given list that exists in the devices table
	${DEVICES} =	Create List	${DEVICE}
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Delete Rows In Table If Exists	jquery=table\#SPMTable	${DEVICES}

GUI::ManagedDevices::Delete Devices If Exists
	[Arguments]	${DEVICES}	${SCREENSHOT}=FALSE
	[Documentation]	Delete devices if they exists in the devices table
	...	== ARGUMENTS ==
	...	-   DEVICES = Device to be deleted
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Delete all devices in the given list that exists in the devices table
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Delete Rows In Table If Exists	jquery=table\#SPMTable	${DEVICES}

GUI::ManagedDevices::Delete All Devices
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Delete all devices
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Delete all managed devices
	GUI::ManagedDevices::Open Devices tab
	GUI::Basic::Delete All Rows In Table If Exists	\#SPMTable

GUI::ManagedDevices::Delete All VM Managers
	[Documentation]	Delete all VM Managers
	GUI::ManagedDevices::Open Discovery Rules
	GUI::ManagedDevices::Open VM Manager
	GUI::Basic::Delete All Rows In Table If Exists	\#VMManagerTable

GUI::ManagedDevices::Delete All Discovery Rules
	[Documentation]	Delete all discovery rules
	GUI::ManagedDevices::Open Discovery Rules
	GUI::Basic::Delete All Rows In Table If Exists	\#SPMDTable

GUI::ManagedDevices::Get Device Type
	[Arguments]	${DEVICE_NAME}
	[Documentation]	Access Managed Devices > Devices and return the type of the ${DEVICE_NAME} in the table
	...	== ARGUMENTS ==
	...	-   DEVICE_NAME = Name of the device
	...	== EXPECTED RESULT ==
	...	Return the type of the specified device in the devices table
	GUI::ManagedDevices::Open Devices tab
	${XPATH}=	Set Variable	//*[@id='SPMTable']/tbody/tr
	${QT_ROWS}=	Get Element Count	xpath=${XPATH}
	FOR	${I}	IN RANGE	1	${QT_ROWS} + 1
		${ID}=	Get Element Attribute	xpath=${XPATH}[${I}]	id
		Run Keyword If	'${ID}'!='${DEVICE_NAME}'	Continue For Loop
		Exit For Loop
	END
	${TYPE}=	Get Text	xpath=${XPATH}[${I}]/td[4]
	[Return]	${TYPE}

GUI::Basic::Pod Menu Must Be
	[Arguments]	@{MENUS}
	[Documentation]	Check if the pod menu (3rd lvl menu) has the exact number of menus and their text match
	...	== ARGUMENTS ==
	...	-   MENUS = List of string of the expected menus
	[Tags]	GUI	MANAGED_DEVICES
	${length}=	Get Length	${MENUS}
	Page Should Contain Element	jquery=#pod_menu > a	limit=${length}
	FOR	${index}	${menu}	IN ENUMERATE	@{MENUS}
		${index}=	Set Variable	${index} + 1
		Element Text Should Be	//div[@id='pod_menu']/a[${index}]/span	${menu}
	END

GUI::ManagedDevices::Wait For Device Commands
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Wait until device's commands table shows up
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Return when device's commands table is visible
	[Tags]	GUI	MANAGED_DEVICES
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=table#SPMCommandsTable
	Wait Until Element Is Visible	jquery=#addButton
	Wait Until Element Is Visible	jquery=#returnButton
	Wait Until Element Is Visible	jquery=#delButton

GUI::ManagedDevices::Open VM Manager
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Manage::Auto Discovery::VM Managers
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	VM Managers Tab is open and all elements are accessible
	[Tags]	GUI	MANAGED_DEVICES	VMMANAGER
	GUI::ManagedDevices::Open Auto Discovery tab
	Click Element	jquery=#pod_menu > a:nth-child(2)
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=table#VMManagerTable

GUI::ManagedDevices::Open Hostname Detection
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Manage::Auto Discovery::Hostname detection
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Hostname detection tab is open and all elements are accessible
	[Tags]	GUI	MANAGED_DEVICES	HOSTNAME_DETECTION	AUTO_DISCOVERY
	GUI::ManagedDevices::Open Auto Discovery tab
	Click Element	jquery=#pod_menu > a:nth-child(4)
	GUI::ManagedDevices::Wait For Hostname Detection

GUI::ManagedDevices::Wait For Hostname Detection
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Wait until hostname detection table shows up
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Return when hostname detection table is visible
	[Tags]	GUI	MANAGED_DEVICES	AUTO_DISCOVERY
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=table#discNameTable
	Wait Until Element Is Visible	jquery=#addButton
	Wait Until Element Is Visible	jquery=#delButton

GUI::ManagedDevices::Check If VM Manager Index ${INDEX} Is Connected
	[Documentation]	Check if the given VM Manager is already connected
	...	== ARGUMENTS ==
	...     -   INDEX = VM Manager index in the VM manager table
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Keyword succeeds if the VM manager is connected
	[Tags]	GUI	MANAGED_DEVICES	AUTO_DISCOVERY
	GUI::Basic::Drilldown Table Row By Index	\#VMManagerTable	${INDEX}
	${count}=	Get Element Count	jquery=div.alert.alert-danger
	GUI::Basic::Cancel
	Should Be Equal As Integers	${count}	0

GUI::ManagedDevices::Wait Until VM Manager Index ${INDEX} Is Connected
	[Documentation]	Wait until given VM manager is connected
	...	== ARGUMENTS ==
	...     -   INDEX = VM Manager index in the VM manager table
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Keyword succeeds if the VM manager is connected
	[Tags]	GUI	MANAGED_DEVICES	AUTO_DISCOVERY
	Wait Until Keyword Succeeds	15 times	5 seconds	GUI::ManagedDevices::Check If VM Manager Index ${INDEX} Is Connected

GUI::ManagedDevices::Enable VM Manager Discover By Index
	[Arguments]	${INDEX}
	[Documentation]	Enable VM discover in the given VM manager
	...	== ARGUMENTS ==
	...     -   INDEX = VM Manager index in the VM manager table
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	VM manager should be configure to discover VMs
	[Tags]	GUI	MANAGED_DEVICES	AUTO_DISCOVERY
	GUI::ManagedDevices::Open Discovery Rules
	GUI::ManagedDevices::Open VM Manager
	${STATUS}=  run keyword and return status  GUI::ManagedDevices::Check If VM Manager Index 1 Is Connected
    Run Keyword if  '${STATUS}' == 'False'      GUI::Basic::Drilldown Table Row By Index	\#VMManagerTable	1
	Run Keyword if  '${STATUS}' == 'False'      GUI::Basic::Select Checkbox	//*[@id='vmdiscovery']
	Run Keyword if  '${STATUS}' == 'False'      Input Text	id=interval 	1
	Run Keyword if  '${STATUS}' == 'False'      GUI::Basic::Save
	GUI::ManagedDevices::Wait Until VM Manager Index ${INDEX} Is Connected
#	GUI::ManagedDevices::Open VM Manager
#	GUI::Basic::Drilldown Table Row By Index	\#VMManagerTable	${INDEX}
#	Select Checkbox	id=vmdiscovery
#	GUI::Basic::Select All Multilist Values
#	GUI::Basic::Save


GUI::ManagedDevices::Open Views tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Managed Devices::Views
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Views Tab is open and all elements are accessable
	[Tags]	GUI	MANAGED_DEVICES	LICENSE
	GUI::ManagedDevices::Open
	GUI::Basic::Wait Until Element Is Accessible	jquery=body > div.main_menu > div > ul > li:nth-child(2)
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(2)
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#addButton


GUI::ManagedDevices::Open Types tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Managed Devices::Types
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Types Tab is open and all elements are accessable
	[Tags]	GUI	MANAGED_DEVICES	LICENSE
	GUI::ManagedDevices::Open
	GUI::Basic::Wait Until Element Is Accessible	jquery=body > div.main_menu > div > ul > li:nth-child(3)
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(3)
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	//*[@id="SPMTargettypeTable_wrapper"]


GUI::ManagedDevices::Open Preferences tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Managed Devices::Preferences
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Preferences Tab is open and all elements are accessable
	[Tags]	GUI	MANAGED_DEVICES	LICENSE
	GUI::ManagedDevices::Open
	GUI::Basic::Wait Until Element Is Accessible	jquery=body > div.main_menu > div > ul > li:nth-child(5)
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(5)
	GUI::Basic::Spinner Should Be Invisible
