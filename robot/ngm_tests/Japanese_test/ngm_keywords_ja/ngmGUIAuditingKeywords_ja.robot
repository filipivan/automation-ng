*** Keywords ***

GUI::Auditing::Open
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Auditing tab
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Managed devices Tab is open and all elements are accessable
	Click Element	jquery=#main_menu > li:nth-child(8) > a
	GUI::Basic::Spinner Should Be Invisible

GUI::Auditing::Open Settings tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Auditing::Settings tab
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Managed devices Tab is open and all elements are accessable
	GUI::Auditing::Open
	Wait Until Element Is Visible	jquery=.submenu ul li:nth-child(1) a
	Click Element	jquery=.submenu ul li:nth-child(1) a
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=.widget-title h4
	#${WIDGET_TILE}=	Get Text	jquery=.widget-title h4
	#Should Be Equal	${WIDGET_TILE}	Auditing :: Settings

GUI::Auditing::Open Events tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Auditing::Events tab
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Managed devices Tab is open and all elements are accessable
	GUI::Auditing::Open
	Wait Until Element Is Visible	jquery=.submenu ul li:nth-child(2) a
	Click Element	jquery=.submenu ul li:nth-child(2) a
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=.widget-title h4
	#${WIDGET_TILE}=	Get Text	jquery=.widget-title h4
	#Should Be Equal	${WIDGET_TILE}	Auditing :: Events

GUI::Auditing::Open Destinations tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Auditing::Destinations tab
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Managed devices Tab is open and all elements are accessable
	GUI::Auditing::Open
	Wait Until Element Is Visible	jquery=.submenu ul li:nth-child(3) a
	Click Element	jquery=.submenu ul li:nth-child(3) a
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=.widget-title h4
	#${WIDGET_TILE}=	Get Text	jquery=.widget-title h4
	#Should Be Equal	${WIDGET_TILE}	Auditing :: Destinations :: File

GUI::Auditing::Open File Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Auditing::Destinations::File
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	VM Auditing Tab is open and all elements are accessible
	[Tags]	GUI	AUDITING	FILE
	GUI::Auditing::Open Destinations tab
	Click Element	jquery=#pod_menu > a:nth-child(1)
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#refresh-icon

GUI::Auditing::Open Syslog Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Auditing::Destinations::Syslog
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	VM Auditing Tab is open and all elements are accessible
	[Tags]	GUI	AUDITING	SYSLOG
	GUI::Auditing::Open Destinations tab
	Click Element	jquery=#pod_menu > a:nth-child(2)
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#refresh-icon

GUI::Auditing::Open SNMPTrap Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Auditing::Destinations::SNMPTrap
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	VM Auditing Tab is open and all elements are accessible
	[Tags]	GUI	AUDITING	SNMPTRAP
	GUI::Auditing::Open Destinations tab
	Click Element	jquery=#pod_menu > a:nth-child(3)
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#refresh-icon

GUI::Auditing::Open Email Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Auditing::Destinations::Email
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	VM Auditing Tab is open and all elements are accessible
	[Tags]	GUI	AUDITING	EMAIL
	GUI::Auditing::Open Destinations tab
	Click Element	jquery=#pod_menu > a:nth-child(4)
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#refresh-icon

GUI::Auditing::Settings::Go To Events And Back to Settings
	GUI::Auditing::Open Events tab
	GUI::Auditing::Open Settings tab

GUI::Auditing::Settings::Switch EvttimeType
	${UTC_SELECTED}=	Run Keyword And Return Status	Radio Button Should Be Set To	evttimeType	utc
	${EVT_TIME_TYPE}=	Set Variable If	${UTC_SELECTED}	local	utc
	Select Radio Button	evttimeType	${EVT_TIME_TYPE}
	[Return]	${EVT_TIME_TYPE}

GUI::Auditing::Settings::Set And Validate EvttimeType
	${EVT_TIME_TYPE}=	GUI::Auditing::Settings::Switch EvttimeType
	GUI::Basic::Save
	GUI::Auditing::Settings::Go To Events And Back to Settings
	Radio Button Should Be Set To	evttimeType	${EVT_TIME_TYPE}

GUI::Auditing::Settings::Switch DatatimeType
	${UTC_SELECTED}=	Run Keyword And Return Status	Radio Button Should Be Set To	datatimeType	utc
	${DATA_TIME_TYPE}=	Set Variable If	${UTC_SELECTED}	local	utc
	Select Radio Button	datatimeType	${DATA_TIME_TYPE}
	[Return]	${DATA_TIME_TYPE}

GUI::Auditing::Settings::Set And Validate DatatimeType
	${TIMESTAMP}=	Run Keyword And Return Status	Checkbox Should Be Selected	jquery=#timestamp
	Click Element	jquery=#timestamp
	GUI::Basic::Save
	GUI::Auditing::Settings::Go To Events And Back to Settings
	Run Keyword If	${TIMESTAMP}	Checkbox Should Not Be Selected	jquery=#timestamp
	Run Keyword Unless	${TIMESTAMP}	Checkbox Should Be Selected	jquery=#timestamp

GUI::Auditing::Settings::Set And Validate Timestamp Format
	${DATA_TIME_TYPE}=	GUI::Auditing::Settings::Switch DatatimeType
	GUI::Basic::Save
	GUI::Auditing::Settings::Go To Events And Back to Settings
	Radio Button Should Be Set To	datatimeType	${DATA_TIME_TYPE}


GUI::Auditing::Auto Input Tests
    [Arguments]  ${LOCATOR}     @{TESTS}
    [Documentation]     locator gets the id of the element to test. You must be on the proper page
    ...                 tests is the texts to be tested. All must be wrong except for the last input
    ${LEN_T}=     Get Length      ${TESTS}
    :FOR    ${I}    IN RANGE    0   ${LEN_T}-1
    \       run keyword and continue on failure  GUI::Auditing::Text Input Error Tests  ${LOCATOR}  ${TRUE}     @{TESTS}[${I}]
    ${LEN_T}=       Evaluate    ${LEN_T} - 1
    run keyword and continue on failure  GUI::Auditing::Text Input Error Tests  ${LOCATOR}  ${FALSE}    @{TESTS}[${LEN_T}]

GUI::Auditing::Text Input Error Tests
    [Arguments]  ${LOCATOR}     ${EXPECTS}      ${TEXT_ENT}
    [Documentation]     locator wants the id of the element
    ...                 expects wants a t/f of whether an error should be thrown
    ...                 text_ent wants the input
    Execute Javascript  document.getElementById("${LOCATOR}").scrollIntoView(true);
    Input Text          jquery=#${LOCATOR}      ${empty}
    Input Text          jquery=#${LOCATOR}      ${TEXT_ENT}
    Execute Javascript	window.scrollTo(0,0);
    Wait Until Element Is Not Visible     xpath=//*[@id='loading']    timeout=20s
    Click Element       jquery=#saveButton
    Wait Until Element Is Not Visible     xpath=//*[@id='loading']    timeout=20s
    Element Should Be Visible               jquery=#refresh-icon
#    ${TST}=     Execute Javascript      var er=document.getElementById('${LOCATOR}');if(er==null){return false;}var ro=er.parentNode.nextSibling;if(ro==null){return false;}var txt=ro.textContent;return txt.includes("[");
#    Log     ${TST}
#    Run Keyword If      ${EXPECTS}==${TRUE}     Should Be True     ${TST}
#    ...     ELSE    Should Not Be True     ${TST}
    #TESTING NEW OUTPUT
    Log     ${EXPECTS}
    ${TST}=     Execute Javascript      var er=document.getElementById('${LOCATOR}');if(er==null){return 1;}var ro=er.parentNode.nextSibling;if(ro==null){return 2;}var txt=ro.textContent.includes("[");if(txt){return 4;}else{return 3;}
    ${ERRCODE}=     execute javascript  if("${EXPECTS}"==="True"){if(${TST}==1){return 3;}else if(${TST}==2){return 4;}else if(${TST}==3){return 5;}else{return 2;}}else{if(${TST}<=2){return 2;}else{return 1;}}
    Run Keyword if  '${ERRCODE}'=='1'   Should Be True  ${FALSE}    msg=No error should be thrown
    Run Keyword if  '${ERRCODE}'=='2'   Should Be True  ${TRUE}
    Run Keyword if  '${ERRCODE}'=='3'   Should Be True  ${FALSE}    msg=Element doesnt exist
    Run Keyword if  '${ERRCODE}'=='4'   Should Be True  ${FALSE}    msg=Expected error but didnt find one
    Run Keyword if  '${ERRCODE}'=='5'   Should Be True  ${FALSE}    msg=Error isnt translated

#var er=document.getElementById('${LOCATOR}');if(er==null){return false;}var ro=er.parentNode.nextSibling;if(ro==null){return false;}var txt=ro.textContent;return txt.includes("[");
#if(${EXPECTS}){if(${TST}==1){return 3;}else if(${TST}==2){return 4;}else if(${TST}==3){return 5;}else{return 2;}}else{if(${TST}<=2){return 2;}else{return 1;}}