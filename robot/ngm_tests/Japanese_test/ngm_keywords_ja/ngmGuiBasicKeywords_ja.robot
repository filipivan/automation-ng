*** Keywords ***
#
# First keyword
#        [Arguments]     ${HOMEPAGE}     ${BROWSER}
#        [Documentation]     Opens a browser instance and navigates to the provided URL
#        ...     == REQUIREMENTS ==
#        ...     == ARGUMENTS ==
#        ...     == EXPECTED RESULT ==
#        [Tags]      GUI     BROWSER

GUI::Basic::Open NodeGrid
	[Arguments]     ${HOMEPAGE}     ${BROWSER}     ${NGVERSION}     ${SCREENSHOT}=FALSE
	[Documentation]         Opens a browser instance and navigates to the provided URL
	...     == REQUIREMENTS ==
	...     None
	...     == ARGUMENTS ==
	...     - HOMEPAGE =   URL to which should be navidated i.e. https://localhost
	...     - BROWSER  =   Which bowser should be used. It has to be a Browser which is supported by the Selenium Framnework and the driver has to be installed on the local system
	...     == EXPECTED RESULT ==
	...     Browser session was opened and the Login window of the NGM is presented
	[Tags]      GUI     BROWSER
	${BROWSER}	Convert To Lower Case	${BROWSER}
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nOpening Browser: ${BROWSER}	INFO	console=yes
	Run Keyword If	'${BROWSER}' == 'chrome' or '${BROWSER}' == 'headlesschrome'	Wait Until Keyword Succeeds	3x	3s	Open Browser	${HOMEPAGE}	${BROWSER}	options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors")
	Run Keyword If	'${BROWSER}' == 'firefox' or '${BROWSER}' == 'headlessfirefox'	Wait Until Keyword Succeeds	3x	3s	Open Browser	${HOMEPAGE}	${BROWSER}	options=set_preference("moz:dom.disable_beforeunload", "true")
	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\nBrowser ${BROWSER} opened	INFO	console=yes
	Maximize Browser Window
	Run Keyword If 			'${BROWSER}' == 'edge'		Wait Until Element Is Visible 		id=invalidcert_header
	Run Keyword If 			'${BROWSER}' == 'edge'		Click Element						id=moreInformationDropdownLink
	Run Keyword If 			'${BROWSER}' == 'edge'		Click Element						id=invalidcert_continue
	Run Keyword If 			'${BROWSER}' == 'ie'		Sleep		2s
	Run Keyword If 			'${BROWSER}' == 'ie'		Wait Until Element Is Visible 		id=overridelink
	Run Keyword If 			'${BROWSER}' == 'ie'		Click Element						id=overridelink
	Run Keyword If 			'${BROWSER}' == 'firefox'	Sleep		2s
	Run Keyword If 			'${NGVERSION}' >= '4.0'     GUI4.x::Basic::Open NodeGrid
	Run Keyword If 			'${NGVERSION}' == '3.2'        GUI3.x::Basic::Open NodeGrid
#	Run Keyword If          '${SCREENSHOT}' != 'FALSE'      Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI3.x::Basic::Open NodeGrid
	Wait Until Page Contains Element    id=username
	Wait Until Element Is Visible      css=input[id=password]

GUI4.x::Basic::Open NodeGrid
	Wait Until Page Contains Element	username
	Wait Until Element Is Visible		password

GUI::Basic::Login
	[Arguments]     ${NGVERSION}    ${USERNAME}=admin     ${PASSWORD}=admin     ${SCREENSHOT}=FALSE
	[Documentation]     A user will be authenticated agains the NodeGrid Manager with he provided USERNAME and PASSWORD
	...     == REQUIREMENTS ==
	...     NGM GUI is open, User will be logod out if a current active session exist
	...     == ARGUMENTS ==
	...     - USERNAME  Username which should be used. Default: admin
	...     - PASSWORD  Password which should be used. Default: admin
	...     == EXPECTED RESULT ==
	...     User is successfuly authenticated and the NodeGrid access page is displayed
	[Tags]      GUI     BASIC   LOGIN
	Run Keyword If	'${NGVERSION}' >= '4.0'	GUI4.x::Basic::Login	${USERNAME}	${PASSWORD}	${SCREENSHOT}
	Run Keyword If	'${NGVERSION}' == '3.2'	GUI3.x::Basic::Login	${USERNAME}	${PASSWORD}	${SCREENSHOT}

GUI3.x::Basic::Login
	[Arguments]     ${USERNAME}=admin     ${PASSWORD}=admin     ${SCREENSHOT}=FALSE
	Wait Until Element Is Visible      css=input[id=password]
	Input Text  css=#username     ${USERNAME}
	Input Text  css=input[id=password]    ${PASSWORD}
	Click Element   id=login-btn
	Wait Until Page Contains Element   id=main_doc
	Wait Until Element Is Visible      id=pwl
	Wait Until Element Is Visible       //*[@id="action"]/div/div/div[1]/a
	Run Keyword If      '${SCREENSHOT}' != 'FALSE'      Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI4.x::Basic::Login
	[Arguments]     ${USERNAME}=admin     ${PASSWORD}=admin     ${SCREENSHOT}=FALSE
	Wait Until Element Is Visible	password
#	Sleep   10s
	Input Text    username     ${USERNAME}
#    Input Text      css=input[id=password]     ${PASSWORD}
	Input Text    password    ${PASSWORD}
	Click Element   id=login-btn
#	Sleep   10s
	#Wait Until Element Is Visible	id=main_doc
	Wait Until Element Is Visible	id=main_menu
	Wait Until Element Is Visible	id=pwl
	Wait Until Element Is Visible	jquery=div.widget-title > h4
	#Wait Until Element Contains	    jquery=div.widget-title > h4	Access :: Table
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If      '${SCREENSHOT}' != 'FALSE'      Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI3.x::Basic::LoginWithEnter
	[Arguments]     ${USERNAME}=admin     ${PASSWORD}=admin     ${SCREENSHOT}=FALSE
	Wait Until Element Is Visible      css=input[id=password]
	Input Text  css=#username     ${USERNAME}
	Input Text  css=input[id=password]    ${PASSWORD}
	Press Key	password	\\13
	Wait Until Page Contains Element   id=main_doc
	Wait Until Element Is Visible      id=pwl
	Wait Until Element Is Visible       //*[@id="action"]/div/div/div[1]/a
	Run Keyword If      '${SCREENSHOT}' != 'FALSE'      Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI::Basic::Login Failure
	[Arguments]     ${NGVERSION}    ${USERNAME}     ${PASSWORD}     ${SCREENSHOT}=FALSE
	[Documentation]     A user will be authenticated agains the NodeGrid Manager with he provided USERNAME and PASSWORD. It is expected that the authentication fails
	...     == REQUIREMENTS ==
	...     NGM GUI is open, User will be logod out if a current active session exist
	...     == ARGUMENTS ==
	...     - USERNAME  Username which should be used
	...     - PASSWORD  Password which should be used
	...     -   SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     User authenticatin is expected to fail and the NodeGrid access page is *not* displayed
	[Tags]      GUI     BASIC   LOGIN
	Run Keyword If	'${NGVERSION}' == '4.0'	GUI4.x::Basic::Login Failure	${USERNAME}	${PASSWORD}	${SCREENSHOT}
	Run Keyword If	'${NGVERSION}' == '3.2'	GUI3.x::Basic::Login Failure	${USERNAME}	${PASSWORD}	${SCREENSHOT}

GUI3.x::Basic::Login Failure
	[Arguments]     ${USERNAME}     ${PASSWORD}     ${SCREENSHOT}=FALSE
	Wait Until Element Is Visible      css=input[id=password]
	Input Text  css=#username     ${USERNAME}
	Input Text  css=input[id=password]    ${PASSWORD}
	Click Element   id=login-btn
	Wait Until Element Is Visible      css=input[id=password]
	Page Should Contain     Login failed
	Run Keyword If      '${SCREENSHOT}' != 'FALSE'      Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI4.x::Basic::Login Failure
	[Arguments]     ${USERNAME}     ${PASSWORD}     ${SCREENSHOT}=FALSE
	Wait Until Element Is Visible      password
	Input Text  css=#username     ${USERNAME}
	Input Text  password    ${PASSWORD}
	Click Element   id=login-btn
	Wait Until Element Is Visible      password
	Wait Until Element Is Visible	jquery=div.alert-danger    error=Error message not found
	Run Keyword If      '${SCREENSHOT}' != 'FALSE'      Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI::Basic::Help
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]     Open thes the NodeGrid Managers Help File
	...     == REQUIREMENTS ==
	...     Valid session to the NodeGride Manger exist
	...     == ARGUMENTS ==
	...     NONE
	...     == EXPECTED RESULT ==
	...     Help File is opened in a new tab/window
	[Tags]      GUI     BASIC   HELP
	Wait Until Element Is Visible      id=hel
	Click Element   id=hel
	Sleep   5s
	Switch Window   url=https://www.zpesystems.com/ng/v4_0/NodegridManual4.0.html
	#Wait Until Page Contains Element     //*[@id="plugin"]      10s
	#Title Should Be    NodeGrid-UserGuide-v3_0.pdf
	Run Keyword If      '${SCREENSHOT}' != 'FALSE'      Sleep   5s
	Run Keyword If      '${SCREENSHOT}' != 'FALSE'      Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png
	Switch Window

GUI::Basic::ZPE Link
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]     Clicks on the ZPE Web Side Link and confirms that the ZPE Web page is displayed
	...     == REQUIREMENTS ==
	...     Valid session to the NodeGride Manger exist
	...     == ARGUMENTS ==
	...     NONE
	...     == EXPECTED RESULT ==
	...     The ZPE web page is opened in a new tab/window
	[Tags]      GUI     BROWSER     ZPE WEBPAGE
	Click Element   id=pwl
	Wait Until Page Contains Element   id=abt
	Click Element   //*[@id="header_inbox_bar"]/ul/li[1]/a
	Sleep   2s
	Switch Window   Home - ZPE Systems - Helping You Reduce Downtime
	Title Should Be    Home - ZPE Systems - Helping You Reduce Downtime
	Run Keyword If      '${SCREENSHOT}' != 'FALSE'      Sleep   5s
	Run Keyword If      '${SCREENSHOT}' != 'FALSE'      Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png
	Switch Window

GUI::Basic::Logout
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]     Logs the current user out and returns back to the NodeGrid Manager Login page
	...     == REQUIREMENTS ==
	...     NONE
	...     == ARGUMENTS ==
	...     NONE
	...     == EXPECTED RESULT ==
	[Tags]      GUI     BROWSER     LOGOUT
	Wait Until Element Is Visible       id=lo
	GUI::Basic::Click Element   //*[@id='lo']
	Wait Until Element Is Visible    id=username
	Run Keyword If      '${SCREENSHOT}' != 'FALSE'      Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI::Basic::Get NodeGrid System
	[Documentation]	Return the System setting from About page
	[Arguments]	${SCREENSHOT}=FALSE
	[Tags]	GUI	BROWSER
	Run Keyword and Return If	'${NGVERSION}' >= '4.0'	GUI4.x::Basic::Get System	${SCREENSHOT}
	Return From Keyword	GUI3.x::Basic::Get System	${SCREENSHOT}

GUI::Basic::Is Serial Console
	[Documentation]	Return 1 when the HOST is a serial console and 0 if it's not
	[Arguments]	${SCREENSHOT}=FALSE
	[Tags]	GUI	BROWSER
	${ret}=	GUI::Basic::Get NodeGrid System	${NGVERSION}
	Return From Keyword If	'${ret}' == 'NodeGrid Serial Console'	1
	Return From Keyword	0

GUI::Basic::About
	[Arguments]     ${NGVERSION}    ${SCREENSHOT}=FALSE
	[Documentation]     Opens the About information page and confirms it details
	...     == REQUIREMENTS ==
	...     Requires a valid bowser session see keyword:  login ngm
	...     == ARGUMENTS ==
	...     None
	...     == EXPECTED RESULT ==
	...     The About page is displayed and contains the following Text Elements
	...     - System:
	...     - Version
	...     - Licenses:
	...     - CPU:
	...     - CPU Cores:
	...     - Bogomips per core:
	...     - Serial Number:
	[Tags]      GUI     BROWSER
	Run Keyword If 			'${NGVERSION}' == '4.0'     GUI4.x::Basic::About    ${SCREENSHOT}
	Run Keyword If 			'${NGVERSION}' == '3.2'     GUI3.x::Basic::About    ${SCREENSHOT}

GUI3.x::Basic::About
	[Arguments]     ${SCREENSHOT}=FALSE
	Wait Until Element Is Visible       //*[@id="action"]/div/div/div[1]/a
	Wait Until Element Is Enabled       id=pwl
	Click Element   id=pwl
	Wait Until Page Contains Element    id=abt
	Click Element   id=abt
	Wait Until Page Contains   Serial Number
	${Text1}=     Get Text      //table/tbody/tr[2]/td/table/tbody/tr[1]/td[2]
	Element Text Should Be      //table/tbody/tr[2]/td/table/tbody/tr[1]/td[1]         System:
	Element Text Should Be      //table/tbody/tr[2]/td/table/tbody/tr[2]/td[1]         Version:
	Element Text Should Be      //table/tbody/tr[2]/td/table/tbody/tr[3]/td[1]         Licenses:
	Element Text Should Be      //table/tbody/tr[2]/td/table/tbody/tr[4]/td[1]         CPU:
	Element Text Should Be      //table/tbody/tr[2]/td/table/tbody/tr[5]/td[1]         CPU Cores:
	Element Text Should Be      //table/tbody/tr[2]/td/table/tbody/tr[6]/td[1]         Bogomips per core:
	Element Text Should Be      //table/tbody/tr[2]/td/table/tbody/tr[7]/td[1]         Serial Number:

	RUN KEYWORD IF      '${Text1}' == 'NodeGrid Serial Console'     Element Text Should Be      //table/tbody/tr[2]/td/table/tbody/tr[8]/td[1]         Model:
	RUN KEYWORD IF      '${Text1}' == 'NodeGrid Serial Console'     Element Text Should Be      //table/tbody/tr[2]/td/table/tbody/tr[9]/td[1]         Part Number:
	RUN KEYWORD IF      '${Text1}' == 'NodeGrid Serial Console'     Element Text Should Be      //table/tbody/tr[2]/td/table/tbody/tr[10]/td[1]        BIOS Version:
	RUN KEYWORD IF      '${Text1}' == 'NodeGrid Serial Console'     Element Text Should Be      //table/tbody/tr[2]/td/table/tbody/tr[11]/td[1]        PSU:


	RUN KEYWORD IF      '${Text1}' == 'NodeGrid Serial Console'     Element Text Should Be      //table/tbody/tr[2]/td/table/tbody/tr[1]/td[2]         NodeGrid Serial Console
	...     ELSE IF     '${Text1}' == 'NodeGrid Manager'        Element Text Should Be      //table/tbody/tr[2]/td/table/tbody/tr[1]/td[2]         NodeGrid Manager
	...     ELSE        FAIL        Not a known About page
	Run Keyword If      '${SCREENSHOT}' != 'FALSE'      Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png
	Click Element           //*[@id="modal_b"]/button

GUI3.x::Basic::Get System
	[Arguments]	${SCREENSHOT}=FALSE
	Wait Until Element Is Visible	//*[@id="action"]/div/div/div[1]/a
	Wait Until Element Is Enabled	id=pwl
	Click Element	id=pwl
	Wait Until Page Contains Element	id=abt
	Click Element	id=abt
	Wait Until Page Contains	Serial Number
	${ret}=	Get Text	//table/tbody/tr[2]/td/table/tbody/tr[1]/td[2]
        Click Element           //*[@id="modal_b"]/button
	[Return]	${ret}

GUI4.x::Basic::About
	[Arguments]     ${SCREENSHOT}=FALSE
	Wait Until Element Is Visible       jquery=#main_doc
	Wait Until Element Is Enabled       id=pwl
	Click Element   id=pwl
	Wait Until Page Contains Element    id=abt
	Click Element   id=abt
	Wait Until Page Contains   Serial Number
	${Text1}=     Get Text      jquery=#system
	Element Text Should Be      jquery=body > div.modal.in > div > div > div.modal-body > div:nth-child(2) > div.col-xs-3 > span         System:
	Element Text Should Be      jquery=body > div.modal.in > div > div > div.modal-body > div:nth-child(3) > div.col-xs-3 > span         Version:
	Element Text Should Be      jquery=body > div.modal.in > div > div > div.modal-body > div:nth-child(4) > div.col-xs-3 > span         Licenses:
	Element Text Should Be      jquery=body > div.modal.in > div > div > div.modal-body > div:nth-child(5) > div.col-xs-3 > span         CPU:
	Element Text Should Be      jquery=body > div.modal.in > div > div > div.modal-body > div:nth-child(6) > div.col-xs-3 > span         CPU Cores:
	Element Text Should Be      jquery=body > div.modal.in > div > div > div.modal-body > div:nth-child(7) > div.col-xs-3 > span         Bogomips per core:
	Element Text Should Be      jquery=body > div.modal.in > div > div > div.modal-body > div:nth-child(8) > div.col-xs-3 > span         Serial Number:
	Element Text Should Be      jquery=body > div.modal.in > div > div > div.modal-body > div:nth-child(9) > div.col-xs-3 > span         Uptime:

	RUN KEYWORD IF      '${Text1}' == 'NodeGrid Serial Console'     Element Text Should Be      jquery=body > div.modal.in > div > div > div.modal-body > div:nth-child(10) > div.col-xs-3 > span         Model:
	RUN KEYWORD IF      '${Text1}' == 'NodeGrid Serial Console'     Element Text Should Be      jquery=body > div.modal.in > div > div > div.modal-body > div:nth-child(11) > div.col-xs-3 > span         Part Number:
	RUN KEYWORD IF      '${Text1}' == 'NodeGrid Serial Console'     Element Text Should Be      jquery=body > div.modal.in > div > div > div.modal-body > div:nth-child(12) > div.col-xs-3 > span        BIOS Version:
	RUN KEYWORD IF      '${Text1}' == 'NodeGrid Serial Console'     Element Text Should Be      jquery=body > div.modal.in > div > div > div.modal-body > div:nth-child(13) > div.col-xs-3 > span        PSU:


	RUN KEYWORD IF      '${Text1}' == 'NodeGrid Serial Console'     Element Text Should Be      id=system         NodeGrid Serial Console
	...     ELSE IF     '${Text1}' == 'NodeGrid Manager'        Element Text Should Be      id=system         NodeGrid Manager
	...     ELSE        FAIL        Not a known About page
	Run Keyword If      '${SCREENSHOT}' != 'FALSE'      Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png
	Click Button	jquery=div.modal button

GUI4.x::Basic::Get System
	[Arguments]	${SCREENSHOT}=FALSE
	Wait Until Element Is Visible       jquery=#main_doc
	Wait Until Element Is Enabled       id=pwl
	GUI::Basic::Click Element	//*[@id='pwl']
	Wait Until Page Contains Element    id=abt
	GUI::Basic::Click Element	//*[@id='abt']
	Wait Until Page Contains   Serial Number
	${ret}=	Get Text	jquery=#system
	Click Button	jquery=div.modal button
	[Return]	${ret}

GUI::Basic::Check Rows In Table
	[Arguments]	${LOCATOR}	${LINES}
	[Documentation]     Count table rows
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	    //table[@id='${LOCATOR}']/tbody/tr	    limit=${LINES}

GUI::Basic::Delete All Rows In Table
	[Arguments]	${LOCATOR}	${HAS_ALERT}=True
	[Documentation]     Delete all table rows
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	jquery=${LOCATOR} >thead input[type=checkbox]
	GUI::Basic::Delete	${HAS_ALERT}

GUI::Basic::Delete All Rows In Table If Exists
	[Arguments]	${LOCATOR}	${HAS_ALERT}=True
	[Documentation]     Delete all table rows if there are some
	GUI::Basic::Spinner Should Be Invisible
	${count}=	Get Element Count	jquery=${LOCATOR} > tbody > tr
	Run Keyword If	${count} > 0	GUI::Basic::Delete All Rows In Table	${LOCATOR}	${HAS_ALERT}

GUI::Basic::Table Should Not Contain
	[Arguments]	${LOCATOR}	${CONTENT}
	[Documentation]     Check if table does not contain the parameter's element
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	    //table[@id='${LOCATOR}']/tbody/tr[td='${CONTENT}']	    limit=0

GUI::Basic::Delete Rows In Table
	[Arguments]	${LOCATOR}	@{IDS}
	[Documentation]     Delete table rows
	:FOR	${ID}	IN	@{IDS}
	\	Select Checkbox	jquery=${LOCATOR} tr#${ID} input[type=checkbox]
	GUI::Basic::Delete With Alert

GUI::Basic::Delete Rows In Table Without Alert
	[Arguments]	${LOCATOR}	@{IDS}
	[Documentation]     Delete table rows
	:FOR	${ID}	IN	@{IDS}
	\	Select Checkbox	jquery=${LOCATOR} tr#${ID} input[type=checkbox]
	GUI::Basic::Delete

GUI::Basic::Delete Rows In Table If Exists
	[Arguments]	${LOCATOR}=jquery=table	${IDS}=[]
	[Documentation]	Delete table rows when ID exists
	${COUNT_SELECTIONS}=	Set Variable	0
	${TABLE_ID}=	Get Element Attribute	${LOCATOR}	id
	:FOR	${ID}	IN	@{IDS}
	\	${count}=	GUI::Basic::Count Table Rows With Id	${LOCATOR}	${ID}
	\	${COUNT_SELECTIONS}=	Set Variable	${COUNT_SELECTIONS} + ${count}
	\	Run Keyword If	${count} > 0
	\	...	GUI::Basic::Select Table Row By Id	${LOCATOR}	${ID}

	Run Keyword If	${COUNT_SELECTIONS} != 0	GUI::Basic::Delete With Alert

GUI::Basic::Edit Rows In Table
	[Arguments]	${LOCATOR}	@{IDS}
	[Documentation]     Edit table rows
	:FOR	${ID}	IN	@{IDS}
	\	Select Checkbox	jquery=${LOCATOR} tr[id='${ID}'] input[type=checkbox]
	Click Element	jquery=#editButton
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Drilldown table row
	[Arguments]	${LOCATOR}	${ID}
	[Documentation]     Fires a drilldown action in a table row
	Click Element	jquery=${LOCATOR} tr#${ID} a
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Scope Should Be
	[Arguments]	${EXPECTED}
	[Documentation]     Test if scope is equal the expected
	${SCOPE} =	Execute Javascript	return document.getElementById("scope").innerHTML;
	Should Be Equal	${SCOPE}	${EXPECTED}

GUI::Basic::Return Button
	GUI::Basic::Click Element	//*[@id='returnButton']
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Title Should Contain
	[Arguments]	${EXPECTED}
	[Documentation]     Check if page title has the expected string
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	jquery=div.widget-title > h4
	Element Should Contain	jquery=div.widget-title > h4	${EXPECTED}

GUI::Basic::Spinner Should Be Invisible
	[Documentation]     Verify if spinner is invisible
	${EXISTS}=	Run Keyword And Return Status	GUI::Element Exists	//*[@id='loading']
	Run Keyword If	${EXISTS}	Wait Until Element Is Not Visible	jquery=#loading
	${EXISTS}=	Run Keyword And Return Status	GUI::Element Exists	//*[@id='back_ground']
	Run Keyword If	${EXISTS}	Wait Until Element Is Not Visible	jquery=#back_ground

GUI::Basic::Wait Until Element Is Accessible
	[Arguments]	${ELEMENT}
	[Documentation]     Verify if spinner is invisible and Element is visible and enabled
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	${ELEMENT}
	Wait Until Element Is Enabled	${ELEMENT}

GUI::Basic::Wait Until Elements Are Accessible
	[Arguments]	@{ELEMENTS}
	[Documentation]	Waits until all elements are accessible
	GUI::Basic::Spinner Should Be Invisible
	:FOR	${ELEMENT}	IN	@{ELEMENTS}
	\	Wait Until Element Is Visible	${ELEMENT}
	\	Wait Until Element Is Enabled	${ELEMENT}

GUI::Basic::Wait Until Elements Are Visible
	[Arguments]	@{ELEMENTS}
	[Documentation]	Waits until all elements are visible
	GUI::Basic::Spinner Should Be Invisible
	:FOR	${ELEMENT}	IN	@{ELEMENTS}
	\	Wait Until Element Is Visible	${ELEMENT}

GUI::Basic::Wait Until Elements Are Not Visible
	[Arguments]	@{ELEMENTS}
	[Documentation]	Waits until all elements are not visible
	GUI::Basic::Spinner Should Be Invisible
	:FOR	${ELEMENT}	IN	@{ELEMENTS}
	\	Wait Until Element Is Not Visible	${ELEMENT}

GUI::Basic::Elements Should Be Visible
	[Arguments]	@{ELEMENTS}
	[Documentation]	Verify if elements are visible
	:FOR	${ELEMENT}	IN	@{ELEMENTS}
	\	Element Should Be Visible	${ELEMENT}

GUI::Basic::Elements Should Not Be Visible
	[Arguments]	@{ELEMENTS}
	[Documentation]	Verify if elements are not visible
	:FOR	${ELEMENT}	IN	@{ELEMENTS}
	\	Element Should Not Be Visible	${ELEMENT}

GUI::Element Exists
	[Documentation]	Returns true or false whether the element exists or not
	[Arguments]	${xpath}
	${COUNT}=	Get Element Count	${xpath}
	${EXISTS}=	Evaluate	${COUNT} > 0
	[Return]	${EXISTS}

GUI::Basic::Guarantees The Field Is Optically visible
	[Arguments]	${LOCATOR}
	[Documentation]	Await field become accessible and scrolls until its optically visible
	...	== ARGUMENTS ==
	...	-   LOCATOR = field path using XPATH format
	...	== EXPECTED RESULT ==
	...	Await until the element is visible
	GUI::Basic::Wait Until Element Is Accessible	${LOCATOR}\
	Execute Javascript	window.document.evaluate("${LOCATOR}", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);

GUI::Basic::Click Element
	[Documentation]	Await field become accessible, scrolls until its optically visible and then click
	[Arguments]	${LOCATOR}
	GUI::Basic::Guarantees The Field Is Optically visible	${LOCATOR}
	Click Element	${LOCATOR}
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Click Element Accept Alert
	[Documentation]	Await field become accessible, scrolls until its optically visible and then click
	[Arguments]	${LOCATOR}
	GUI::Basic::Guarantees The Field Is Optically visible	${LOCATOR}
	Click Element	${LOCATOR}
	Run Keyword And Return Status	Handle Alert
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Save
	[Documentation]	Click in the Save button and wait for the Spinner figure to disappear
	[Arguments]	${HAS_ALERT}=False
	GUI::Basic::Wait Until Element Is Accessible	jquery=#saveButton
	Run Keyword If	${HAS_ALERT}	GUI::Basic::Click Element Accept Alert      //*[@id='saveButton']
	...     ELSE    GUI::Basic::Click Element	//*[@id='saveButton']
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Add
	[Documentation]	Click in the Add button and wait for the Spinner figure to disappear
	GUI::Basic::Click Element	//*[@id='addButton']
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Edit
	[Documentation]	Click in the Edit button and wait for the Spinner figure to disappear
	GUI::Basic::Click Element	//*[@id='editButton']
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Return
	[Documentation]	Click in the Return button and wait for the Spinner figure to disappear
	GUI::Basic::Click Element	//*[@id='returnButton']
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Input Text
	[Documentation]	Await field become accessible, scrolls until its optically visible and the input the text
	[Arguments]	${LOCATOR}	${TEXT}
	GUI::Basic::Guarantees The Field Is Optically visible	${LOCATOR}
	Input Text	${LOCATOR}	${TEXT}

GUI::Basic::Select Checkbox
	[Documentation]	Await field become accessible, scrolls until its optically visible and select the checkbox
	[Arguments]	${LOCATOR}
	GUI::Basic::Guarantees The Field Is Optically visible	${LOCATOR}
	Select Checkbox	${LOCATOR}

GUI::Basic::Unselect Checkbox
	[Documentation]	Await field become accessible, scrolls until its optically visible and unselect the checkbox
	[Arguments]	${LOCATOR}
	GUI::Basic::Guarantees The Field Is Optically visible	${LOCATOR}
	Unselect Checkbox	${LOCATOR}

GUI::Basic::Select Radio Button
	[Documentation]	Await field become accessible, scrolls until its optically visible and select the radio item
	[Arguments]	${RADIO_BUTTON_ID}	${VALUE}
	GUI::Basic::Guarantees The Field Is Optically visible	//*[@id='${RADIO_BUTTON_ID}']
	Select Radio Button	${RADIO_BUTTON_ID}	${VALUE}

GUI::Basic::Get Page Title
	[Documentation]	Returns the title that is below the menus like 'Access :: Table'
	Wait Until Element Is Visible	jquery=.widget-title h4
	${WIDGET_TILE}=	Get Text	jquery=.widget-title h4
	[Return]	${WIDGET_TILE}

GUI::Basic::Page Title Should Be
	[Documentation]	Check if the title that is below the menus is the equal the expected
	[Arguments]	${EXPECTED_TITLE}
	${WIDGET_TILE}=	GUI::Basic::Get Page Title
	Should Be Equal	${WIDGET_TILE}	${EXPECTED_TITLE}

GUI::Basic::Open and Login Nodegrid
	[Documentation]   A wrapper of the GUI::Basic::Open Nodegrid and GUI::Basic::Login keywords.
	...               Hence, the tests can open and log in the system in one keyword.
	...               The current implementation uses default credentials (admin/admin)
	GUI::Basic::Open NodeGrid	${HOMEPAGE}	${BROWSER}	${NGVERSION}
	GUI::Basic::Login	${NGVERSION}

GUI::Basic::Count Table Rows With Id
	[Arguments]	${LOCATOR}	${ID}
	[Documentation]	Count table rows and return it
	${TABLE_ID}=	Get Element Attribute	${LOCATOR}	id
	${count}=	Get Element Count	//table[@id='${TABLE_ID}']/tbody/tr[@id='${ID}']
	${return}=	Convert To Integer	${count}
	[Return]	${return}

GUI::Basic::Add Button Should Be Visible
	[Documentation]	Check if there is some add button in the current page
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#addButton

GUI::Basic::Delete Button Should Be Visible
	[Documentation]	Check if there is some delete button in the current page
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#delButton

GUI::Basic::Return Button Should Be Visible
	[Documentation]	Check if there is some return button in the current page
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#returnButton

GUI::Basic::Listbuilder Should Be Visible
	[Documentation]	Check if there is some listbuild in the current page
	Element Should Be Visible	jquery=.listbuilder	There is no listbuilder

GUI::Basic::Select Listbuilder Values By Index
	[Arguments]	${LOCATOR}	${INDEXES}	${SCREENSHOT}=FALSE
	[Documentation]	Select listerbuilder values
	GUI::Basic::Listbuilder Should Be Visible
	:FOR	${element}	IN	@{INDEXES}
	\	Select From List By Index	jquery=${LOCATOR} select.groupFrom	${element}
	Click Element	jquery=${LOCATOR} > div > div > div:nth-child(2) > div > div:first-child button

GUI::Basic::Get Selected Listbuilder Items
	[Arguments]	${LOCATOR}	${SCREENSHOT}=FALSE
	[Documentation]	Get listerbuilder selected values
	GUI::Basic::Listbuilder Should Be Visible
	@{items}=	Get Selected List Values	jquery=${LOCATOR} select.groupTo
	[Return]	@{items}

GUI::Basic::Table Should Has Row With Id
	[Arguments]	${LOCATOR}	${ID}	${SCREENSHOT}=FALSE
	[Documentation]	Check if the given table has a row with the given id
	${count}=	Get Element Count	//table[@id='${LOCATOR}']/tbody/tr[@id='${ID}']
	Should Be True	${count} > 0	There is a row with the given ID

GUI::Basic::Table Should Not Have Row With Id
	[Arguments]	${LOCATOR}	${ID}	${SCREENSHOT}=FALSE
	[Documentation]	Check if the given table has a row with the given id
	${count}=	Get Element Count	//table[@id='${LOCATOR}']/tbody/tr[@id='${ID}']
	Should Be True	${count} == 0	There is no row with the given ID

GUI::Basic::Discovery Now Button Should Be Visible
	[Documentation]	Check if there is some discovery now button in the current page
	Wait Until Element Is Visible	jquery=#discovernow

GUI::Basic::Discovery Now
	[Documentation]	Click in the Discovery now button and wait for the Spinner figure to disappear
	GUI::Basic::Click Element	//*[@id='discovernow']
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Select Table Rows
	[Arguments]	${LOCATOR}	${IDS}	${SCREENSHOT}=FALSE
	[Documentation]     Select table rows
	...	== ARGUMENTS ==
	...	-   LOCATOR = Table locator in jquery format
	...	-   IDS = Rows ids which should be selected
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Select the table row of the given ids
	[Tags]	GUI	BASIC	TABLE
	${TABLE_ID}=	Get Element Attribute	${LOCATOR}	id
	:FOR	${ID}	IN	@{IDS}
	\	Select Checkbox	//*[@id="${TABLE_ID}"]//tbody//tr[@id="${ID}"]//input[@type="checkbox"]

GUI::Basic::Select Table Row By Id
	[Arguments]	${LOCATOR}	${ID}	${SCREENSHOT}=FALSE
	[Documentation]     Select table rows
	...	== ARGUMENTS ==
	...	-   LOCATOR = Table locator
	...	-   IDS = Row id which should be selected
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Select the table row of the given id
	[Tags]	GUI	BASIC	TABLE
	${TABLE_ID}=	Get Element Attribute	${LOCATOR}	id
	Select Checkbox	//*[@id="${TABLE_ID}"]//tbody//tr[@id="${ID}"]//input[@type="checkbox"]

GUI::Basic::Count Table Rows
	[Arguments]	${LOCATOR}	${SCREENSHOT}=FALSE
	[Documentation]     Count the number of the rows (tr) for the given table
	...	== ARGUMENTS ==
	...	-   LOCATOR = Table id
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Returns the number table's rows count
	[Tags]	GUI	BASIC	TABLE
	GUI::Basic::Spinner Should Be Invisible
	${count}=	Get Element Count	//table[@id='${LOCATOR}']/tbody/tr
	${count}=	Convert To Integer	${count}
	[Return]	${count}

GUI::Basic::Table Rows Count Should Be Greater
	[Arguments]	${LOCATOR}	${EXPECTED}	${SCREENSHOT}=FALSE
	[Documentation]     Check if the total table rows count is greater then the given value
	GUI::Basic::Spinner Should Be Invisible
	${count}=	GUI::Basic::Count Table Rows	${LOCATOR}
	Should Be True	${count} > ${EXPECTED}

GUI::Basic::Get Table Values
	[Arguments]	${LOCATOR}	${SCREENSHOT}=FALSE
	[Documentation]    Get the values displayed in the given table
	...	== ARGUMENTS ==
	...	-   LOCATOR = Table id
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Returns the number table's rows count
	[Tags]	GUI	BASIC	TABLE
	${count} =	GUI::Basic::Count Table Rows	${LOCATOR}
	${rows} =	Create List
	:FOR	${i}	IN RANGE	2	${count}+2
	\	${row} =	GUI::Basic::Get Table Row Values By Index	${LOCATOR}	${i}
	\	Append To List	${rows}	${row}
	[Return]	${rows}

GUI::Basic::Table Values Should Be Equal
	[Arguments]	${T1}	${T2}	${SCREENSHOT}=FALSE
	[Documentation]    Compare two table values generated by  GUI::Basic::Get Table Values
	...	== ARGUMENTS ==
	...	-   T1 = First table values
	...	-   T2 = First table values
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	[Tags]	GUI	BASIC	TABLE
	Lists Should Be Equal	${T1}	${T2}

GUI::Basic::Get Table Row Values By Index
	[Arguments]	${LOCATOR}	${ROW}	${SCREENSHOT}=FALSE
	[Documentation]    Get the values from the given table row
	...	== ARGUMENTS ==
	...	-   LOCATOR = Table id
	...	-   ROW = Row index
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Returns the number table rows content
	[Tags]	GUI	BASIC	TABLE
	${headers} =	GUI::Basic::Get Table Headers	${LOCATOR}
	${values} = 	Create Dictionary
	:FOR	${header}	IN	@{headers}
	\	${col} =	GUI::Basic::Get Table Header Index	${LOCATOR}	${header}
	\	${value} = 	Get Table Cell	jquery=table\#${LOCATOR}	${ROW}	${col}
	\	Set To Dictionary	${values}	${header}	${value}
	[Return]	${values}

GUI::Basic::Get Table Header Index
	[Arguments]	${LOCATOR}	${HEADER}	${SCREENSHOT}=FALSE
	[Documentation]    Get the header index
	...	== ARGUMENTS ==
	...	-   LOCATOR = Table id
	...	-   HEADER = Row index
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Returns the index of the header inside its table
	[Tags]	GUI	BASIC	TABLE
	${count}=	GUI::Basic:: Get Table Columns Count	${LOCATOR}
	:FOR	${I}	IN RANGE	1	${count}+1
	\	${is_selectable} =	GUI::Basic::Is Table Column Selectable	${LOCATOR}	${I}
	\	Continue For Loop If	${is_selectable}
	\	${h} =	GUI::Basic::Get Table Header By Index	${LOCATOR}	${I}
	\	Return From Keyword If	'${h}' == '${HEADER}'	${I}
	${index} =	Set Variable	-1
	[Return]	${index}

GUI::Basic::Get Table Header By Index
	[Arguments]	${LOCATOR}	${INDEX}	${SCREENSHOT}=FALSE
	[Documentation]    Get the table header by index
	...	== ARGUMENTS ==
	...	-   LOCATOR = Table locator
	...	-   HEADER = Header index
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Returns the table header value
	[Tags]	GUI	BASIC	TABLE
	${is_selectable} =	GUI::Basic::Is Table Column Selectable	${LOCATOR}	${INDEX}
	Run Keyword If	${is_selectable}	Fail	Invalid header column
	${text}=	Get Table Cell	${LOCATOR}	1	${INDEX}
	[Return]	${text}

GUI::Basic::Get Table Headers
	[Arguments]	${LOCATOR}	${SCREENSHOT}=FALSE
	[Documentation]    Get table headers
	...	== ARGUMENTS ==
	...	-   LOCATOR = Table id
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Returns a list with the table headers
	${headers} =	Create List
	${count}=	GUI::Basic:: Get Table Columns Count	${LOCATOR}
	:FOR	${I}	IN RANGE	1	${count}+1
	\	${is_selectable} =	GUI::Basic::Is Table Column Selectable	${LOCATOR}	${I}
	\	Continue For Loop If	${is_selectable}
	\	${TEXT}=	Get Text	//table[@id='${LOCATOR}']/thead/tr/th[${I}]
	\	Append To List	${headers}	${TEXT}
	[Return]	${headers}

GUI::Basic::Is Table Column Selectable
	[Arguments]	${LOCATOR}	${INDEX}	${SCREENSHOT}=FALSE
	[Documentation]    Check if the table column is selectable or not
	...	== ARGUMENTS ==
	...	-   LOCATOR = Table id
	...	-   INDEX = Column index
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Return a boolean value showing if the columns is selectable
	${count}=	Get Element Count	//table[@id='${LOCATOR}']/thead/tr/th[${index}][contains(@class, 'selectable')]
	${is_selectable} =	Evaluate	${count} > 0
	[Return]	${is_selectable}

GUI::Basic::Get Table Columns Count
	[Arguments]	${LOCATOR}	${SCREENSHOT}=FALSE
	[Documentation]    Get table columns count
	...	== ARGUMENTS ==
	...	-   LOCATOR = Table id
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Returns the table columns count
	${count}=	Get Element Count	//table[@id='${LOCATOR}']/thead/tr/th
	[Return]	${count}

GUI::Basic::Get Table Rows Count
	[Arguments]	${LOCATOR}	${SCREENSHOT}=FALSE
	[Documentation]    Get table rows count
	...	== ARGUMENTS ==
	...	-   LOCATOR = Table id
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Returns the table rows count
	${count}=	Get Element Count	//table[@id='${LOCATOR}']/tbody/tr
	[Return]	${count}

GUI::Basic::Up Button Should Be Visible
	[Documentation]	Check if there is some up button in the current page
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#upButton

GUI::Basic::Down Button Should Be Visible
	[Documentation]	Check if there is some add button in the current page
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#downButton

GUI::Basic::Console Button Should Be Visible
	[Documentation]	Check if there is some add button in the current page
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#consoleButton

GUI::Basic::Save Button Should Be Visible
	[Documentation]	Check if there is some save button in the current page
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#saveButton

GUI::Basic::Cancel Button Should Be Visible
	[Documentation]	Check if there is some cancel button in the current page
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#cancelButton

GUI::Basic::Drilldown Table Row By Index
	[Arguments]	${LOCATOR}	${INDEX}
	[Documentation]     Fires a drilldown action in a table row by index
	Click Element	jquery=${LOCATOR} tr:nth-child(${INDEX}) a
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Console
	[Documentation]	Click in the console button and wait for the Spinner figure to disappear
	GUI::Basic::Click Element	//*[@id='consoleButton']
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Cancel
	[Documentation]	Click in the cancel button and wait for the Spinner figure to disappear
	GUI::Basic::Click Element	//*[@id='cancelButton']
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Delete
	[Arguments]	${HAS_ALERT}=False
	[Documentation]	Click in the delete button and wait for the Spinner figure to disappear
	Wait Until Element Is Enabled	id=delButton	error=Cannot delete because delete button is disable
	Click Element	//*[@id='delButton']
	Run Keyword If	${HAS_ALERT}	Handle Alert	ACCEPT
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Delete With Alert
	[Documentation]	Click in the delete button, dismiss the alert message and wait for the Spinner figure to disappear
	GUI::Basic::Delete	True

GUI:Basic::Exist Window With Title
	[Arguments]	${TITLE}
	${TITLES}=	Get Window Names
	:FOR	${T}	IN	@{TITLES}
	\	Run Keyword If	${TITLE} == ${T}	Return From Keyword	${TRUE}
	[Return]	${FALSE}

GUI::Basic::Error Count Should Be
	[Arguments]	${ERROR_COUNT}	${SCREENSHOT}=FALSE
	[Documentation]	Current web page should contain the given error count
	Page Should Contain Element	jquery=div#globalerr p.text-danger	limit=${ERROR_COUNT}	message=There are more global errors than expected

GUI::Basic::Field Error Count Should Be
	[Arguments]	${ERROR_COUNT}	${SCREENSHOT}=FALSE
	[Documentation]	Current web page should contain the given error count
	Page Should Contain Element	jquery=span#errormsg.text-danger	limit=${ERROR_COUNT}	message=There are more field errors than expected

GUI::Basic::Page Should Not Contain Error
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Current web page should not contain error
	GUI::Basic::Spinner Should Be Invisible
	${ret1}=	Run Keyword And Return Status	GUI::Basic::Error Count Should Be	0
	${ret2}=	Run Keyword And Return Status	GUI::Basic::Field Error Count Should Be	0
	Should Be True	${ret1}
	Should Be True	${ret2}

GUI::Basic::Select Rows In Table Containing Value
	[Arguments]	${TABLE_ID}=table	${VALUE}=""
	[Documentation]	Select table rows which contains the given value

	${count}=	Get Element Count	xpath://table[@id="${TABLE_ID}"]/tbody/tr
	${COUNT_SELECTIONS}=	Set Variable	0

	:FOR	${index}	IN RANGE	1	${count}+1
	\	${has_value} =	Run Keyword And Return Status	Table Row Should Contain	id=${TABLE_ID}	${index}	${VALUE}
	\	Run Keyword If	${has_value}	Select Checkbox	xpath://table[@id="${TABLE_ID}"]/tbody/tr[${index}]/td[1]/input

GUI::Basic::Delete Rows In Table Containing Value
	[Arguments]	${TABLE_ID}=table	${VALUE}=""	${HAS_ALERT}=True
	[Documentation]	Delete table rows which contains the given value
	GUI::Basic::Select Rows In Table Containing Value	${TABLE_ID}	${VALUE}
	GUI::Basic::Delete	${HAS_ALERT}

GUI::Basic::Select All Multilist Values
	[Documentation]	Selects from a multilist widget
	Select All From List	jquery=.multilist > div:nth-child(2) > div:first-child() > select
	Click Element	jquery=.multilist > div:nth-child(2) > div:nth-child(3) > button:first-child()


GUI::Basic::Japanese::Test Translation
    [Arguments]     ${LOCATOR}
	[Documentation]	Checks if the given location label starts with an "["
	${Label}=   Get Text    ${LOCATOR}
	Log     ${Label}
	Element Should Contain  ${LOCATOR}  [     "${Label}" is not translated


GUI::Basic::Japanese::Test Translation With Value
    [Arguments]     ${LOCATOR}
	[Documentation]	Checks if the given location value starts with an "["
	${Label}=   Get Value    ${LOCATOR}
	Log     ${Label}
	Should Match Regexp     ${Label}    [\[.*]     "${Label}" is not translated


GUI::Basic::Japanese::If Element Exists then Test Translation
    [Arguments]     ${LOCATOR}
	[Documentation]	Checks if the given location exists and then checks if it starts with an "["
	${Exists}=   Run Keyword And Return Status    Page Should Contain Element    ${LOCATOR}
	Run Keyword If  ${Exists}   GUI::Basic::Japanese::Test Translation  ${LOCATOR}


GUI::Basic::Japanese::Test Translation Table Header
    [Arguments]     ${Num_Of_Cols}
    [Documentation]	Checks if the table on the page opened has translated header
    ${End}=     Evaluate    ${Num_Of_Cols} + 1
    : FOR    ${INDEX}    IN RANGE    1    ${End}
    \    run keyword and continue on failure  GUI::Basic::Japanese::Test Translation      //*[@id="thead"]/tr/th[${INDEX}]


GUI::Basic::Japanese::Test Translation Table Header With Checkbox
    [Arguments]     ${Num_Of_Cols}
    [Documentation]	Checks if the table on the page opened has translated header skipping the first col (checkbox)
    ${End}=     Evaluate    ${Num_Of_Cols} + 1
    : FOR    ${INDEX}    IN RANGE    2    ${End}
    \    run keyword and continue on failure  GUI::Basic::Japanese::Test Translation      //*[@id="thead"]/tr/th[${INDEX}]


GUI::Basic::Japanese::Test Table Elements
    [Arguments]     ${Table_Name}   ${Col_Num}
    [Documentation]	Checks if the elements in the given col are translated in the given table
    ${Table_Size}=  GUI::Basic::Count Table Rows    ${Table_Name}
    Pass Execution If   ${Table_Size}==0    ${Table_Name} is Empty
    ${Table_Size}=  Evaluate    ${Table_Size} + 1
    : FOR    ${INDEX}    IN RANGE    1    ${Table_Size}
    \    run keyword and continue on failure  GUI::Basic::Japanese::Test Translation      //*[@id="tbody"]/tr[${INDEX}]/td[${Col_Num}]

GUI::Basic::Check Translation No ID
    [Arguments]     @{IDS}
    [Documentation]     Gets the Id of the sibling element and uses it to figure out if translated
    ${LEN_T}=     Get Length      ${IDS}
    :FOR    ${I}    IN RANGE    0   ${LEN_T}
    \       ${LOC}=     Get From List   ${IDS}  ${I}
    \       ${element}=     Execute Javascript          return document.getElementById('${LOC}').parentNode;
    \       ${VAL_EL}=      Get Text                    ${element}
    \       run keyword and continue on failure     Should Contain      ${VAL_EL}       [   msg='${VAL_EL}' is not translated

GUI::Basic::Check Translation No ID Previous Sibling
    [Arguments]     @{IDS}
    [Documentation]     Gets the Id of the sibling element and uses it to figure out if translated. Usually a textbox
    ${LEN_T}=     Get Length      ${IDS}
    :FOR    ${I}    IN RANGE    0   ${LEN_T}
    \       ${LOC}=     Get From List   ${IDS}  ${I}
    \       ${element}=     Execute Javascript          return document.getElementById('${LOC}').parentNode.previousSibling;
    \       ${VAL_EL}=      Get Text                    ${element}
    \       run keyword and continue on failure     Should Contain      ${VAL_EL}       [   msg='${VAL_EL}' is not translated

GUI::Basic::Check Translation No ID Next Sibling
    [Arguments]     @{IDS}
    [Documentation]     Gets the Id of the sibling element and uses it to figure out if translated
    ${LEN_T}=     Get Length      ${IDS}
    :FOR    ${I}    IN RANGE    0   ${LEN_T}
    \       ${LOC}=     Get From List   ${IDS}  ${I}
    \       ${element}=     Execute Javascript          return document.getElementById('${LOC}').parentNode.nextSibling;
    \       ${VAL_EL}=      Get Text                    ${element}
    \       run keyword and continue on failure     Should Contain      ${VAL_EL}       [   msg='${VAL_EL}' is not translated

GUI::Basic::Check Radio Button Translation
    [Arguments]     ${ID}
    [Documentation]     Method for translating a set of radio buttons
    ...                 The id for the buttons should be passed
    @{TEXT_LST}=    Execute Javascript
    ...     var lst= document.getElementsByName("${ID}");
    ...     var finList=[];
    ...     for(i=0;i<lst.length;i++){
    ...         var txt= lst[i].parentNode.textContent;
    ...         finList.push(txt);
    ...     }
    ...     return finList;
    ${TITLE}=   Execute Javascript
    ...     var lst= document.getElementsByName("${ID}");
    ...     var txt= lst[0].parentNode.parentNode.parentNode.previousSibling.textContent;
    ...     return txt;
    run keyword and continue on failure  Should Contain  ${TITLE}    ]   msg=${TITLE} is not translated
    :FOR    ${I}    IN  @{TEXT_LST}
    \   run keyword and continue on failure  Should Contain      ${I}  ]   msg=${I} is not translated

#var lst= document.getElementsByName("${ID}");var finList=[];for(i=0;i<lst.length;i++){var txt= lst[i].parentNode.textContent;finList.push(txt);}return finList;
#var lst= document.getElementsByName("${ID}");
#var txt= lst[0].parentNode.parentNode.parentNode.previousSibling.textContent;
#return txt;


