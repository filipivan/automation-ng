*** Keywords ***

GUI::Cluster::Open
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid cloud tab
	...     == ARGUMENTS ==
	...     -   SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Cluster Tab is open and all elements are accessable
	GUI::Basic::Wait Until Element Is Accessible	jquery=#main_menu > li:nth-child(6) > a
	Execute Javascript	window.document.evaluate("//*[@id='main_menu']", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element	jquery=#main_menu > li:nth-child(6) > a
	GUI::Basic::Spinner Should Be Invisible

GUI::Cluster::Open Peers Tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Cluster::Settings tab
	...     == ARGUMENTS ==
	...     -   SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Settings Tab is open and all elements are accessible
	[Tags]      GUI     CLUSTER      LICENSE
	GUI::Cluster::Open
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(1) > a
	GUI::Basic::Spinner Should Be Invisible

GUI::Cluster::Open Settings Tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Cluster::Settings tab
	...     == ARGUMENTS ==
	...     -   SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Settings Tab is open and all elements are accessible
	[Tags]      GUI     CLUSTER      LICENSE
	GUI::Cluster::Open
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(2) > a
	GUI::Basic::Spinner Should Be Invisible

GUI::Cluster::Open Management Tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Cluster::Settings tab
	...     == ARGUMENTS ==
	...     -   SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Settings Tab is open and all elements are accessible
	[Tags]      GUI     CLUSTER      LICENSE
	GUI::Cluster::Open
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible

GUI::Cluster::Open Automatic Enrollment Range
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Cluster::Settings::Automatic Enrollment Range
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	-   A.E.R. Tab is open and all elements are accessible
	[Tags]	GUI	CLUSTER	AUTOMATIC_ENROLLMENT_RANGE
	GUI::Cluster::Open Settings Tab
	Click Element	jquery=#pod_menu > a:nth-child(2)
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#peerTable

GUI::Cluster::Wait For Peers
	GUI::Basic::Spinner Should Be Invisible
	Table Header Should Contain	    id=peerTable	Name
	Table Header Should Contain	    id=peerTable	Address
	Table Header Should Contain	    id=peerTable	Type
	Table Header Should Contain	    id=peerTable	Status
	Table Header Should Contain	    id=peerTable	Peer Status

GUI::Cluster::Wait For Settings
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=body > div.main_menu > div > ul > li.active > a	Settings
	Wait Until Element Is Visible	jquery=#saveButton
	Wait Until Element Is Visible	id=autoEnroll
	Wait Until Element Is Visible	id=enabled
	Wait Until Element Is Visible	id=management_enabled
	Wait Until Element Is Visible	id=lps_enabled

GUI::Cluster::Wait For Management
	GUI::Basic::Spinner Should Be Invisible
	Table Header Should Contain	    id=peerMngtTable	Name
	Table Header Should Contain	    id=peerMngtTable	Address
	Table Header Should Contain	    id=peerMngtTable    Status
	Table Header Should Contain	    id=peerMngtTable	SW Version
	Table Header Should Contain	    id=peerMngtTable	Management Status


