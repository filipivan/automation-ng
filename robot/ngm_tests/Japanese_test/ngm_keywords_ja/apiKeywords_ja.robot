*** Settings ***
Documentation
...	= Table of Content =
...	- Overview
...	- Useage
...	- Supported Nodegrid Versions
...	 == Overview ==
...	This Library supports specific keywords relating to the ZPE Systems Nodegrid API.
...	 == Useage ==
...	 This file needs to be added as a resource library to the robot test files. In teh inhouse test automation
...	system this is perfformed through the init.robot file which combines all required resources to run the actual test cases
...	== Supported Nodegrid Versions ==
...	- 4.0


*** Keywords ***
API::Setup HTTP Context
	[Arguments]     ${HOST}
	[Documentation]     Setup http library used to perform the http request
	...	== REQUIREMENTS ==
	...	None
	...	== ARGUMENTS ==
	...	- HOST  Host which the request will be send
	[Tags]	API
	Should Not Be Empty	${HOST}	Invalid host
	#robotframework-requests
	${headers}=	API::Get Headers
	Create Session	api	https://${HOST}	headers=${headers}	disable_warnings=1

API::Get Headers
	${headers}=	Create Dictionary
	Set To Dictionary	${headers}	Origin=https://${HOST}
	Set To Dictionary	${headers}	Referer=https://${HOST}/
	Set To Dictionary	${headers}	Accept-Language=en-GB,en;q=0.8,en-US;q=0.6
	Set To Dictionary	${headers}	Accept-Encoding=gzip, deflate, br
	Set To Dictionary	${headers}	Accept=application/json
	Set To Dictionary	${headers}	Cache-control=no-cache
	Set To Dictionary	${headers}	Pragma=no-cache
	Set To Dictionary	${headers}	Content-Type=application/json
	[return]	${headers}

API::Get Ticket Header
	${headers}=	Create Dictionary
	Set To Dictionary	${headers}	ticket=${SESSION_TICKET}
	[return]	${headers}

API::Authentication
	[Arguments]     ${USERNAME}	${PASSWORD}
	 [Documentation]     Authenticate the given credential using the REST API
	...	== REQUIREMENTS ==
	...	None
	...	== ARGUMENTS ==
	...	- USERNAME  Username used in the authentication
	...	- PASSWORD  Username's password used in the authentication
	...	== EXPECTED RESULT ==
	...	Login successfully in the API
	[Tags]	API
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	username=${USERNAME}
	Set To Dictionary	${payload}	password=${PASSWORD}
	${response}=	Post Request	api	/api/${API_VERSION}/Session	data=${payload}
	${body}=	Set Variable	${response.json()}
	Should Be Equal As Strings	${body["status"]}	success
	Should Not Be Empty	${body["session"]}
	Set Suite Variable	${SESSION_TICKET}	${body["session"]}

API::Destroy Session
	[Documentation]     Destroy the session previously created by API::Authenticate
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Destroy the session set in the "ticket" header
	[Tags]	API
	${body}=	API::Send Delete Request	Session
	Should Be Equal As Strings	${body["status"]}	success

API::Get System Preferences
	[Documentation]     Get the system preferences using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the system preferences
	[Tags]	API
	${body}=	API::Send Get Request	system/preferences
	[return]	${body}

API::Update System Preferences
	[Arguments]	${PAYLOAD}
	[Documentation]     Updates the system preferences using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the system preferences
	[Tags]	API
	${body}=	API::Send Put Request	system/preferences	${PAYLOAD}
	[return]	${body}

API::Get System Date Time
	[Documentation]     Get the system date time using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the system date time
	[Tags]	API
	${body}=	API::Send Get Request	system/datetime
	[return]	${body}

API::Send Post Request
	[Arguments]	${PATH}	${PAYLOAD}
	${headers}=	API::Get Ticket Header
	${response}=	Post Request	api	/api/${API_VERSION}/${PATH}	data=${PAYLOAD}	headers=${headers}
	Should Be Equal As Strings	${response.status_code}	200

API::Send Get Request
	[Arguments]	${PATH}
	${headers}=	API::Get Ticket Header
	${response}=	Get Request	api	/api/${API_VERSION}/${PATH}	headers=${headers}
	Should Be Equal As Strings	${response.status_code}	200
	[return]	${response.json()}

API::Update Date Time
	[Arguments]	${FIELDS}
	[Documentation]     Update the system date time using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the system date time fields
	[Tags]	API
	API::Send Put Request	system/datetime	${FIELDS}

API::Get System Logging
	[Documentation]     Get the system logging using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the system logging data
	[Tags]	API
	${response}=	API::Send Get Request	system/logging
	[return]	${response}

API::Update Logging
	[Arguments]	${FIELDS}
	[Documentation]     Update the system logging using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the system logging fields
	[Tags]	API
	API::Send Put Request	system/logging	${FIELDS}

API::Get System Custom Fields
	[Documentation]     Get the system custom fields using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the system custom fields data
	[Tags]	API
	${response}=	API::Send Get Request	system/customfields
	[return]	${response}

API::Add Custom Field
	[Arguments]	${NAME}	${VALUE}
	[Documentation]     Adds new custom field using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds new custom field
	[Tags]	API
	${FIELDS}=	Create Dictionary
	Set To Dictionary	${FIELDS}	field_name=${NAME}
	Set To Dictionary	${FIELDS}	field_value=${VALUE}
	API::Send Post Request	system/customfields	${FIELDS}

API::Update Custom Field
	[Arguments]	${NAME}	${VALUE}
	[Documentation]     Updates custom field value using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update custom field value
	[Tags]	API
	${FIELDS}=	Create Dictionary
	Set To Dictionary	${FIELDS}	custom_value=${VALUE}
	API::Send Put Request	system/customfields/${NAME}	${FIELDS}

API::Delete Custom Field
	[Arguments]	${NAME}
	[Documentation]     Updates custom field value using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update custom field value
	[Tags]	API
	API::Send Delete Request	system/customfields/${NAME}

API::Send Put Request
	[Arguments]	${PATH}	${PAYLOAD}=${EMPTY}
	${headers}=	API::Get Ticket Header
	${response}=	Put Request	api	/api/${API_VERSION}/${PATH}	data=${PAYLOAD}	headers=${headers}
	Should Be Equal As Strings	${response.status_code}	200
	[return]	${response.json()}

API::Send Delete Request
	[Arguments]	${PATH}
	${headers}=	API::Get Ticket Header
	${response}=	Delete Request	api	/api/${API_VERSION}/${PATH}	headers=${headers}
	Should Be Equal As Strings	${response.status_code}	200
	[return]	${response.json()}

API::Get Dial Up Configuration
	[Documentation]     Get the dialup configuration using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the dialup configuration
	[Tags]	API
	${response}=	API::Send Get Request	system/dialup
	[return]	${response}

API::Get Dialup Devices
	[Documentation]     Get the dialup devices using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the dialup devices fields data
	[Tags]	API
	${response}=	API::Send Get Request	system/dialup/devices
	[return]	${response}

API::Get Dialup Callback Users
	[Documentation]     Get the dialup callback users using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the dialup callback users data
	[Tags]	API
	${response}=	API::Send Get Request	system/dialup/callbackusers
	[return]	${response}

API::Update Dial Up Configuration
	[Arguments]	${DIALUP_LOGIN}	${DIALUP_PPP}
	[Documentation]     Get the dialup configuration using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the dialup configuration
	[Tags]	API
	${FIELDS}=	Create Dictionary
	Set To Dictionary	${FIELDS}	dialupSetLogin=${DIALUP_LOGIN}
	Set To Dictionary	${FIELDS}	dialupSetPPP=${DIALUP_PPP}
	${response}=	API::Send Put Request	system/dialup	${FIELDS}
	[return]	${response}

API::Add Dialup Device
	[Arguments]	${PAYLOAD}
	[Documentation]     Add dialup device using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Dialup device added in NG
	[Tags]	API
	${response}=	API::Send Post Request	system/dialup/devices	${PAYLOAD}
	[return]	${response}

API::Get Dialup Device
	[Arguments]	${DEV}
	[Documentation]     Get dialup device list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return dialup devices list
	[Tags]	API
	${response}=	API::Send Get Request	system/dialup/devices/${DEV}
	[return]	${response}

API::Delete Dialup Device
	[Arguments]	${DEV}
	[Documentation]     Deletes dialup device using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Dialup device removed from NG
	[Tags]	API
	${response}=	API::Send Delete Request	system/dialup/devices/${DEV}
	[return]	${response}

API::Add Dialup Callback User
	[Arguments]	${PAYLOAD}
	[Documentation]     Adds dialup callback user using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add new dialup callback user in NG
	[Tags]	API
	${response}=	API::Send Post Request	system/dialup/callbackusers	${PAYLOAD}
	[return]	${response}

API::Get Dialup Callback User
	[Arguments]	${USER}
	[Documentation]     Get dialup callback user using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Returns the dialup callback users
	[Tags]	API
	${response}=	API::Send Get Request	system/dialup/callbackusers/${USER}
	[return]	${response}

API::Delete Dialup Callback User
	[Arguments]	${USER}
	[Documentation]     Removes dialup callback user using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	License removed from NG
	[Tags]	API
	${response}=	API::Send Delete Request	system/dialup/callbackusers/${USER}
	[return]	${response}

API::Get Licenses
	[Documentation]     Get the licenses using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the licenses
	[Tags]	API
	${response}=	API::Send Get Request	system/licenses
	[return]	${response}

API::Add License
	[Arguments]	${LICENSE}
	[Documentation]     Adds licenses using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds the given license in the NG
	[Tags]	API
	${response}=	API::Send Post Request	system/licenses	${LICENSE}
	[return]	${response}

API::Delete License
	[Arguments]	${LICENSEID}
	[Documentation]     Delete license using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Remove license from NG
	[Tags]	API
	API::Send Delete Request	system/licenses/${LICENSEID}

API::Get Devices
	[Documentation]     Get managed devices using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return managed devices
	[Tags]	API
	${response}=	API::Send Get Request	devices
	[return]	${response}

API::Add Device
	[Arguments]	${PAYLOAD}
	[Documentation]     Adds device using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds new device in NG
	[Tags]	API
	${response}=	API::Send Post Request	devices	${PAYLOAD}
	[return]	${response}

API::Delete Device
	[Arguments]	${DEVICE}
	[Documentation]     Delete managed device using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Remove device from NG
	[Tags]	API
	API::Send Delete Request	devices/${DEVICE}

API::Get Device
	[Arguments]	${DEVICE}
	[Documentation]     Get managed device using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Returns the managed device info
	[Tags]	API
	${response}=	API::Send Get Request	devices/${DEVICE}
	[return]	${response}

API::Disable Device
	[Arguments]	${DEVICE}
	[Documentation]     Disable device using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Disable managed device
	[Tags]	API
	${response}=	API::Send Put Request	devices/${DEVICE}/disable	${EMPTY}
	[return]	${response}

API::Enable Device
	[Arguments]	${DEVICE}
	[Documentation]     Disable device using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Disable managed device
	[Tags]	API
	${response}=	API::Send Put Request	devices/${DEVICE}/enable	${EMPTY}
	[return]	${response}

API::Ondemand Device
	[Arguments]	${DEVICE}
	[Documentation]     Updates the device mode configuration to On-demand using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update device mode configuration
	[Tags]	API
	${response}=	API::Send Put Request	devices/${DEVICE}/ondemand	${EMPTY}
	[return]	${response}

API::Rename Device
	[Arguments]	${DEVICE}	${NEW_NAME}
	[Documentation]     Updates device name using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Rename device name
	[Tags]	API
	${response}=	API::Send Put Request	devices/${DEVICE}/rename	${NEW_NAME}
	[return]	${response}

API::Default Device
	[Arguments]	${DEVICE}
	[Documentation]     Updates the local_serial device to its default configs using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Reset local_serial device configuration
	[Tags]	API
	${response}=	API::Send Put Request	devices/${DEVICE}/default	${EMPTY}
	[return]	${response}

API::Bounce DTR Device
	[Arguments]	${DEVICE}
	[Documentation]     Updates the local_serial device to its default configs using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Reset local_serial device configuration
	[Tags]	API
	${response}=	API::Send Put Request	devices/${DEVICE}/bouncedtr	${EMPTY}
	[return]	${response}

API::Clone Device
	[Arguments]	${DEVICE}	${FIELDS}
	[Documentation]     Updates the local_serial device to its default configs using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Reset local_serial device configuration
	[Tags]	API
	${response}=	API::Send Put Request	devices/${DEVICE}/clone	${FIELDS}
	[return]	${response}

API::Update Device
	[Arguments]	${DEVICE}	${PAYLOAD}
	[Documentation]     Updates managed device using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update device from NG
	[Tags]	API
	${response}=	API::Send Put Request	devices/${DEVICE}	${PAYLOAD}
	[return]	${response}

API::Get Network Settings
	[Documentation]     Get network settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Returns network settings info
	[Tags]	API
	${response}=	API::Send Get Request	network/settings
	[return]	${response}

API::Update Network Settings
	[Arguments]	${PAYLOAD}
	[Documentation]     Updates network settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update network settings from NG
	[Tags]	API
	${response}=	API::Send Put Request	network/settings	${PAYLOAD}
	[return]	${response}

API::Get Network Connections
	[Documentation]     Get network connections using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Network connections from NG
	[Tags]	API
	${response}=	API::Send Get Request	network/connections
	[return]	${response}

API::Get Network Connection
	[Arguments]	${CONNECTION}
	[Documentation]     Get network connection using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Returns network connection info
	[Tags]	API
	${response}=	API::Send Get Request	network/connections/${CONNECTION}
	[return]	${response}

API::Add Network Settings
	[Arguments]	${PAYLOAD}
	[Documentation]     Adds network settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds network connection from NG
	[Tags]	API
	${response}=	API::Send Post Request	network/connections	${PAYLOAD}
	[return]	${response}

API::Delete Network Settings
	[Arguments]	${CONNECTION}
	[Documentation]     Deletes network settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Delete network connection from NG
	[Tags]	API
	API::Send Delete Request	network/connections/${CONNECTION}

API::Update Network Connection
	[Arguments]	${CONNECTION}	${PAYLOAD}
	[Documentation]     Updates network settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update network connection from NG
	[Tags]	API
	API::Send Put Request	network/connections/${CONNECTION}	${PAYLOAD}

API::Up Network Connection
	[Arguments]	${CONNECTION}
	[Documentation]     Up network connection using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Up network connection from NG
	[Tags]	API
	API::Send Post Request	network/connections/${CONNECTION}/up	${EMPTY}

API::Down Network Connection
	[Arguments]	${CONNECTION}
	[Documentation]     Down network connection using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Down network connection from NG
	[Tags]	API
	API::Send Post Request	network/connections/${CONNECTION}/down	${EMPTY}

API::Get Static Routes
	[Documentation]     Get static routes using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return static routes from NG
	[Tags]	API
	${response}=	API::Send Get Request	network/staticroutes
	[return]	${response}

API::Get Static Route
	[Arguments]	${ROUTE}
	[Documentation]     Get static route using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Returns static route info
	[Tags]	API
	${response}=	API::Send Get Request	network/staticroutes/${ROUTE}
	[return]	${response}

API::Add Static Route
	[Arguments]	${PAYLOAD}
	[Documentation]     Adds static route using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds static route from NG
	[Tags]	API
	${response}=	API::Send Post Request	network/staticroutes	${PAYLOAD}
	[return]	${response}

API::Delete Static Route
	[Arguments]	${ROUTE}
	[Documentation]     Deletes static route using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Delete static route from NG
	[Tags]	API
	API::Send Delete Request	network/staticroutes/${ROUTE}

API::Update Static Route
	[Arguments]	${ROUTE}	${PAYLOAD}
	[Documentation]     Updates static route using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update static route from NG
	[Tags]	API
	API::Send Put Request	network/staticroutes/${ROUTE}	${PAYLOAD}

API::Get Hosts
	[Documentation]     Get hosts using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return hosts from NG
	[Tags]	API
	${response}=	API::Send Get Request	network/hosts
	[return]	${response}

API::Get Host
	[Arguments]	${host}
	[Documentation]     Get host using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Returns host info
	[Tags]	API
	${response}=	API::Send Get Request	network/hosts/${host}
	[return]	${response}

API::Add Host
	[Arguments]	${PAYLOAD}
	[Documentation]     Adds host using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds host from NG
	[Tags]	API
	${response}=	API::Send Post Request	network/hosts	${PAYLOAD}
	[return]	${response}

API::Delete Host
	[Arguments]	${HOST}
	[Documentation]     Deletes host using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Delete host from NG
	[Tags]	API
	API::Send Delete Request	network/hosts/${HOST}

API::Update Host
	[Arguments]	${HOST}	${PAYLOAD}
	[Documentation]     Updates host using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update host from NG
	[Tags]	API
	API::Send Put Request	network/hosts/${HOST}	${PAYLOAD}

API::Get DHCP Servers
	[Documentation]     Get DHCP servers using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return DHCP servers from NG
	[Tags]	API
	${response}=	API::Send Get Request	network/dhcp
	[return]	${response}

API::Get DHCP Server
	[Arguments]	${DHCP}
	[Documentation]     Get DHCP server using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Returns DHCP server info
	[Tags]	API
	${response}=	API::Send Get Request	network/dhcp/${DHCP}
	[return]	${response}

API::Add DHCP Server
	[Arguments]	${PAYLOAD}
	[Documentation]     Adds DHCP server using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds DHCP from NG
	[Tags]	API
	${response}=	API::Send Post Request	network/dhcp	${PAYLOAD}
	[return]	${response}

API::Delete DHCP Server
	[Arguments]	${DHCP}
	[Documentation]     Deletes DHCP server using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Delete DHCP server from NG
	[Tags]	API
	API::Send Delete Request	network/dhcp/${DHCP}

API::Update DHCP Server
	[Arguments]	${DHCP}	${PAYLOAD}
	[Documentation]     Updates DHCP server using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update DHCP server from NG
	[Tags]	API
	API::Send Put Request	network/dhcp/${DHCP}	${PAYLOAD}

API::Get SSL VPN Clients
	[Documentation]     Get SSL VPN clients using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return SSL VPN clients from NG
	[Tags]	API
	${response}=	API::Send Get Request	network/sslvpn/clients
	[return]	${response}

API::Get SSL VPN Server
	[Documentation]     Get SSL VPN server using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return SSL VPN server info from NG
	[Tags]	API
	${response}=	API::Send Get Request	network/sslvpn/server
	[return]	${response}

API::Get SSL VPN Server Status
	[Documentation]     Get SSL VPN server status using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return SSL VPN server status info from NG
	[Tags]	API
	${response}=	API::Send Get Request	network/sslvpn/server/status
	[return]	${response}

API::Update SSL VPN Server
	[Arguments]	${PAYLOAD}
	[Documentation]     Update SSL VPN server using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Updated SSL VPN server info from NG
	[Tags]	API
	${response}=	API::Send Put Request	network/sslvpn/server	${PAYLOAD}
	[return]	${response}

API::Add SSL VPN Client
	[Arguments]	${PAYLOAD}
	[Documentation]     Adds SSL VPN client using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add SSL VPN clients from NG
	[Tags]	API
	${response}=	API::Send Post Request	network/sslvpn/clients	${PAYLOAD}
	[return]	${response}

API::Delete SSL VPN Client
	[Arguments]	${CLIENT}
	[Documentation]     Deletes SSL VPN client using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Deletes SSL VPN clients from NG
	[Tags]	API
	${response}=	API::Send Delete Request	network/sslvpn/clients/${CLIENT}

API::Get SSL VPN Client
	[Arguments]	${CLIENT}
	[Documentation]     Get SSL VPN client using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return SSL VPN client from NG
	[Tags]	API
	${response}=	API::Send Get Request	network/sslvpn/clients/${CLIENT}
	[return]	${response}

API::Update SSL VPN Client
	[Arguments]	${CLIENT}	${PAYLOAD}
	[Documentation]     Update SSL VPN client using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update SSL VPN clients from NG
	[Tags]	API
	${response}=	API::Send Put Request	network/sslvpn/clients/${CLIENT}	${PAYLOAD}
	[return]	${response}

API::Start SSL VPN Client
	[Arguments]	${CLIENT}
	[Documentation]     Start SSL VPN client using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Start SSL VPN clients from NG
	[Tags]	API
	${response}=	API::Send Post Request	network/sslvpn/clients/${CLIENT}/start	${EMPTY}
	[return]	${response}

API::Stop SSL VPN Client
	[Arguments]	${CLIENT}
	[Documentation]     Stop SSL VPN client using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Stop SSL VPN clients from NG
	[Tags]	API
	${response}=	API::Send Post Request	network/sslvpn/clients/${CLIENT}/stop	${EMPTY}
	[return]	${response}

API::Get Open Session
	[Documentation]     Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	[Tags]	API
	${response}=	API::Send Get Request	tracking/opensessions
	[return]	${response}

API::Get Devices Session
	[Documentation]     Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	[Tags]	API
	${response}=	API::Send Get Request	tracking/devicessessions
	[return]	${response}

API::Get Events
	[Documentation]     Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	[Tags]	API
	${response}=	API::Send Get Request	tracking/events
	[return]	${response}

API::Get Discovery Logs
	[Documentation]     Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	[Tags]	API
	${response}=	API::Send Get Request	tracking/discoverylogs
	[return]	${response}

API::Get CPU Usage
	[Documentation]     Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	[Tags]	API
	${response}=	API::Send Get Request	tracking/system/cpu
	[return]	${response}

API::Get Memory Usage
	[Documentation]     Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	[Tags]	API
	${response}=	API::Send Get Request	tracking/system/memory
	[return]	${response}

API::Get Disk Usage
	[Documentation]     Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	[Tags]	API
	${response}=	API::Send Get Request	tracking/system/disk
	[return]	${response}

API::Get Network Interfaces
	[Documentation]     Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	[Tags]	API
	${response}=	API::Send Get Request	tracking/network/interfaces
	[return]	${response}

API::Get Network LLDP
	[Documentation]     Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	[Tags]	API
	${response}=	API::Send Get Request	tracking/network/lldp
	[return]	${response}

API::Get Network Routing Table
	[Documentation]     Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	[Tags]	API
	${response}=	API::Send Get Request	tracking/network/routingtable
	[return]	${response}

API::Get USB Devices
	[Documentation]     Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	[Tags]	API
	${response}=	API::Send Get Request	tracking/usbdevices
	[return]	${response}

API::Get Password Rules
	[Documentation]     Get password rules info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return password rules from NG
	[Tags]	API
	${response}=	API::Send Get Request	security/passwordrules
	[return]	${response}

API::Update Password Rules
	[Arguments]	${PAYLOAD}
	[Documentation]     Update password rules info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update password rules from NG
	[Tags]	API
	${response}=	API::Send Put Request	security/passwordrules	${PAYLOAD}
	[return]	${response}

API::Get Security Services
	[Documentation]     Get security services info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return security services from NG
	[Tags]	API
	${response}=	API::Send Get Request	security/services
	[return]	${response}

API::Update Security Services
	[Arguments]	${PAYLOAD}
	[Documentation]     Update security services info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update security services from NG
	[Tags]	API
	API::Send Put Request	security/services	${PAYLOAD}

API::Get Security Local Accounts
	[Documentation]     Get security services info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return security services from NG
	[Tags]	API
	${response}=	API::Send Get Request	security/localaccounts
	[return]	${response}

API::Get Security Authentication
	[Documentation]     Get security authentication info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return security authentication from NG
	[Tags]	API
	${response}=	API::Send Get Request	security/authentication
	[return]	${response}

API::Add Authentication Method
	[Arguments]	${PAYLOAD}
	[Documentation]     Add security authentication info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add security authentication from NG
	[Tags]	API
	API::Send Post Request	security/authentication	${PAYLOAD}

API::Delete Authentication Method
	[Arguments]	${METHOD}
	[Documentation]     Delete security authentication info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Delete security authentication from NG
	[Tags]	API
	API::Send Delete Request	security/authentication/${METHOD}

API::Get Authentication Method Info
	[Arguments]	${METHOD}
	[Documentation]     Get security authentication info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get security authentication from NG
	[Tags]	API
	${response}=	API::Send Get Request	security/authentication/${METHOD}
	[return]	${response}

API::Update Authentication Method Info
	[Arguments]	${METHOD}	${PAYLOAD}
	[Documentation]     Update security authentication info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update security authentication from NG
	[Tags]	API
	API::Send Put Request	security/authentication/${METHOD}	${PAYLOAD}

API::Move Authentication Method Up
	[Arguments]	${METHOD}
	[Documentation]     Move security authentication method up using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Move security authentication up from NG
	[Tags]	API
	API::Send Put Request	security/authentication/${METHOD}/up

API::Move Authentication Method Down
	[Arguments]	${METHOD}
	[Documentation]     Move security authentication method down using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Move security authentication down from NG
	[Tags]	API
	API::Send Put Request	security/authentication/${METHOD}/down

API::Get Security Firewall Chain
	[Documentation]     Get security firewall info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return security firewall from NG
	[Tags]	API
	${response}=	API::Send Get Request	security/firewall
	[return]	${response}

API::Add Security Firewall Chain
	[Arguments]	${PAYLOAD}
	[Documentation]     Add new firewall chain info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add new firewall chain from NG
	[Tags]	API
	API::Send Post Request	security/firewall	${PAYLOAD}

API::Delete Security Firewall Chain
	[Arguments]	${CHAIN}
	[Documentation]     Add new firewall chain info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add new firewall chain from NG
	[Tags]	API
	API::Send Delete Request	security/firewall/${CHAIN}

API::Get Security Firewall Chain Rules
	[Arguments]	${CHAIN}
	[Documentation]     Get chain's rules info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return chain rules
	[Tags]	API
	${response}=	API::Send Get Request	security/firewall/${CHAIN}/rules
	[return]	${response}

API::Add Security Firewall Chain Rule
	[Arguments]	${CHAIN}	${PAYLOAD}
	[Documentation]     Add new firewall chain rule info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add new firewall chain rule from NG
	[Tags]	API
	API::Send Post Request	security/firewall/${CHAIN}/rules	${PAYLOAD}

API::Delete Security Firewall Chain Rule
	[Arguments]	${CHAIN}	${RULE}
	[Documentation]     Add new firewall chain info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add new firewall chain from NG
	[Tags]	API
	API::Send Delete Request	security/firewall/${CHAIN}/rules/${RULE}

API::Update Security Firewall Chain Rule
	[Arguments]	${CHAIN}	${RULE}	${PAYLOAD}
	[Documentation]     Update firewall chain rule info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update firewall chain rule from NG
	[Tags]	API
	API::Send Put Request	security/firewall/${CHAIN}/rules/${RULE}	${PAYLOAD}

API::Get Security Firewall Chain Rule
	[Arguments]	${CHAIN}	${RULE}
	[Documentation]     Get firewall chain rule info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get firewall chain rule from NG
	[Tags]	API
	${response}=	API::Send Get Request	security/firewall/${CHAIN}/rules/${RULE}
	[return]	${response}

API::Add Local Account
	[Arguments]	${PAYLOAD}
	[Documentation]     Adds local accounts using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add new local account
	[Tags]	API
	${response}=	API::Send Post Request	security/localaccounts	${PAYLOAD}

API::Delete Local Account
	[Arguments]	${ACCOUNT}
	[Documentation]     Deletes local accounts using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Delete given local account
	[Tags]	API
	${response}=	API::Send Delete Request	security/localaccounts/${ACCOUNT}

API::Get Security Local Account
	[Arguments]	${ACCOUNT}
	[Documentation]     Get local account  info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return local account from NG
	[Tags]	API
	${response}=	API::Send Get Request	security/localaccounts/${ACCOUNT}
	[return]	${response}

API::Update Security Local Account
	[Arguments]	${ACCOUNT}	${PAYLOAD}
	[Documentation]     Update local account  info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update local account from NG
	[Tags]	API
	${response}=	API::Send Put Request	security/localaccounts/${ACCOUNT}	${PAYLOAD}

API::Get Security Authorization
	[Documentation]     Get security authorization using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get new security authorization group
	[Tags]	API
	${response}=	API::Send Get Request	security/authorization
	[return]	${response}

API::Add Authorization
	[Arguments]	${PAYLOAD}
	[Documentation]     Adds security authorization using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Added new security authorization group
	[Tags]	API
	${response}=	API::Send Post Request	security/authorization	${PAYLOAD}

API::Delete Authorization
	[Arguments]	${AUTHORIZATION}
	[Documentation]     Deletes security authorization using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Delete given security authorization
	[Tags]	API
	${response}=	API::Send Delete Request	security/authorization/${AUTHORIZATION}

API::Get Security Authorization Members
	[Arguments]	${AUTHORIZATION}
	[Documentation]     Get security authorization members info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return security authorization members from NG
	[Tags]	API
	${response}=	API::Send Get Request	security/authorization/${AUTHORIZATION}/members
	[return]	${response}

API::Get Security Authorization Profile
	[Arguments]	${AUTHORIZATION}
	[Documentation]     Get security authorization profile info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return security authorization profile from NG
	[Tags]	API
	${response}=	API::Send Get Request	security/authorization/${AUTHORIZATION}/profile
	[return]	${response}

API::Get Security Authorization Remote Groups
	[Arguments]	${AUTHORIZATION}
	[Documentation]     Get security authorization remote groups info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return security authorization remote groups from NG
	[Tags]	API
	${response}=	API::Send Get Request	security/authorization/${AUTHORIZATION}/remotegroups
	[return]	${response}

API::Get Security Authorization Devices
	[Arguments]	${AUTHORIZATION}
	[Documentation]     Get security authorization devices info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return security authorization devices from NG
	[Tags]	API
	${response}=	API::Send Get Request	security/authorization/${AUTHORIZATION}/devices
	[return]	${response}

API::Get Security Authorization Outlets
	[Arguments]	${AUTHORIZATION}
	[Documentation]     Get security authorization outlets info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return security authorization outlets from NG
	[Tags]	API
	${response}=	API::Send Get Request	security/authorization/${AUTHORIZATION}/outlets
	[return]	${response}

API::Add Security Authorization Devices
	[Arguments]	${AUTHORIZATION}	${PAYLOAD}
	[Documentation]     Adds security authorization device info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	New security authorization member added
	[Tags]	API
	API::Send Post Request	security/authorization/${AUTHORIZATION}/devices	${PAYLOAD}

API::Get Security Authorization Device
	[Arguments]	${AUTHORIZATION}	${DEVICE}
	[Documentation]     Gets security authorization device info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return security authorization device info
	[Tags]	API
	${response}=	API::Send Get Request	security/authorization/${AUTHORIZATION}/devices/${DEVICE}
	[return]	${response}

API::Update Security Authorization Device
	[Arguments]	${AUTHORIZATION}	${DEVICE}	${PAYLOAD}
	[Documentation]     Updates security authorization device info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Security authorization devices updated
	[Tags]	API
	API::Send Put Request	security/authorization/${AUTHORIZATION}/devices/${DEVICE}	${PAYLOAD}

API::Delete Security Authorization Devices
	[Arguments]	${AUTHORIZATION}	${DEVICE}
	[Documentation]     Adds security authorization device info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	New security authorization device added
	[Tags]	API
	API::Send Delete Request	security/authorization/${AUTHORIZATION}/devices/${DEVICE}

API::Add Security Authorization Member
	[Arguments]	${AUTHORIZATION}	${PAYLOAD}
	[Documentation]     Adds security authorization member info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Security authorization member added
	[Tags]	API
	API::Send Post Request	security/authorization/${AUTHORIZATION}/members	${PAYLOAD}

API::Delete Security Authorization Member
	[Arguments]	${AUTHORIZATION}	${MEMBER}
	[Documentation]     Deletes security authorization member info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Security authorization member deleted
	[Tags]	API
	API::Send Delete Request	security/authorization/${AUTHORIZATION}/members/${MEMBER}

API::Add Security Authorization Outlet
	[Arguments]	${AUTHORIZATION}	${PAYLOAD}
	[Documentation]     Adds security authorization outlet info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Security authorization outlet added
	[Tags]	API
	API::Send Post Request	security/authorization/${AUTHORIZATION}/outlets	${PAYLOAD}

API::Get Security Authorization Outlet
	[Arguments]	${AUTHORIZATION}	${OUTLET}
	[Documentation]     Gets security authorization outlet info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return security authorization outlet info
	[Tags]	API
	${response}=	API::Send Get Request	security/authorization/${AUTHORIZATION}/outlets/${OUTLET}
	[return]	${response}

API::Update Security Authorization Outlet
	[Arguments]	${AUTHORIZATION}	${OUTLET}	${PAYLOAD}
	[Documentation]     Updates security authorization outlet info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Security authorization outlet updated
	[Tags]	API
	API::Send Put Request	security/authorization/${AUTHORIZATION}/outlets/${OUTLET}	${PAYLOAD}

API::Delete Security Authorization Outlet
	[Arguments]	${AUTHORIZATION}	${OUTLET}
	[Documentation]     Deletes security authorization outlet info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Security authorization outlet removed
	[Tags]	API
	API::Send Delete Request	security/authorization/${AUTHORIZATION}/outlets/${OUTLET}

API::Get Auditing Settings
	[Documentation]     Get the auditing settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the auditing settings
	[Tags]	API
	${response}=	API::Send Get Request	auditing/settings
	[return]	${response}

API::Update Auditing Settings
	[Arguments]	${PAYLOAD}
	[Documentation]     Update the auditing settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update auditing settings
	[Tags]	API
	API::Send Put Request	auditing/settings	${PAYLOAD}

API::Get Auditing Events File
	[Documentation]     Get the auditing events file info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the auditing events file info
	[Tags]	API
	${response}=	API::Send Get Request	auditing/events/file
	[return]	${response}

API::Get Auditing Events Email
	[Documentation]     Get the auditing events email info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the auditing events email info
	[Tags]	API
	${response}=	API::Send Get Request	auditing/events/email
	[return]	${response}

API::Get Auditing Events SNMP Trap
	[Documentation]     Get the auditing events SNMP trap info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the auditing events SNMP trap info
	[Tags]	API
	${response}=	API::Send Get Request	auditing/events/snmptrap
	[return]	${response}

API::Get Auditing Events Syslog
	[Documentation]     Get the auditing events syslog info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the auditing events syslog info
	[Tags]	API
	${response}=	API::Send Get Request	auditing/events/syslog
	[return]	${response}

API::Update Auditing Events File
	[Arguments]	${PAYLOAD}
	[Documentation]     Update the auditing events file info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the auditing events file info
	[Tags]	API
	API::Send Put Request	auditing/events/file	${PAYLOAD}

API::Update Auditing Events Email
	[Arguments]	${PAYLOAD}
	[Documentation]     Update the auditing events email info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the auditing events email info
	[Tags]	API
	API::Send Put Request	auditing/events/email	${PAYLOAD}

API::Update Auditing Events SNMP Trap
	[Arguments]	${PAYLOAD}
	[Documentation]     Update the auditing events SNMP trap info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the auditing events SNMP trap info
	[Tags]	API
	API::Send Put Request	auditing/events/snmptrap	${PAYLOAD}

API::Update Auditing Events Syslog
	[Arguments]	${PAYLOAD}
	[Documentation]     Update the auditing events syslog info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the auditing events syslog info
	[Tags]	API
	API::Send Put Request	auditing/events/syslog	${PAYLOAD}

API::Get Auditing File Destination
	[Documentation]     Get the auditing file destination using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the auditing file destination
	[Tags]	API
	${response}=	API::Send Get Request	auditing/destination/file
	[return]	${response}

API::Get Auditing Syslog Destination
	[Documentation]     Get the auditing syslog destination using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the auditing syslog destination
	[Tags]	API
	${response}=	API::Send Get Request	auditing/destination/syslog
	[return]	${response}

API::Get Auditing SNMP Trap Destination
	[Documentation]     Get the auditing SNMP Trap destination using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the auditing SNMP Trap destination
	[Tags]	API
	${response}=	API::Send Get Request	auditing/destination/snmptrap
	[return]	${response}

API::Get Auditing Email Destination
	[Documentation]     Get the auditing email destination using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the auditing email destination
	[Tags]	API
	${response}=	API::Send Get Request	auditing/destination/email
	[return]	${response}

API::Update Auditing File Destination
	[Arguments]	${PAYLOAD}
	[Documentation]     Update the auditing file destination info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the auditing file destination info
	[Tags]	API
	API::Send Put Request	auditing/destination/file	${PAYLOAD}

API::Update Auditing Email Destination
	[Arguments]	${PAYLOAD}
	[Documentation]     Update the auditing email destination info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the auditing email destination info
	[Tags]	API
	API::Send Put Request	auditing/destination/email	${PAYLOAD}

API::Update Auditing Syslog Destination
	[Arguments]	${PAYLOAD}
	[Documentation]     Update the auditing syslog destination info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the auditing syslog destination info
	[Tags]	API
	API::Send Put Request	auditing/destination/syslog	${PAYLOAD}

API::Update Auditing SNMP Trap Destination
	[Arguments]	${PAYLOAD}
	[Documentation]     Update the auditing SNMP trap destination info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the auditing SNMP trap destination info
	[Tags]	API
	API::Send Put Request	auditing/destination/snmptrap	${PAYLOAD}

API::Get SNMP Entries
	[Documentation]     Get SNMP info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return SNMP info data from NG
	[Tags]	API
	${response}=	API::Send Get Request	network/snmp
	[return]	${response}

API::Add SNMP Entry
	[Arguments]	${PAYLOAD}
	[Documentation]     Adds SNMP entry using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add SNMP entry from NG
	[Tags]	API
	API::Send Post Request	network/snmp	${PAYLOAD}

API::Delete SNMP Entry
	[Arguments]	${ENTRY}
	[Documentation]     Deletes SNMP entry using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Deletes SNMP entry from NG
	[Tags]	API
	API::Send Delete Request	network/snmp/${ENTRY}

API::Update SNMP Entry
	[Arguments]	${ENTRY}	${PAYLOAD}
	[Documentation]     Update SNMP entry using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update SNMP entry from NG
	[Tags]	API
	API::Send Put Request	network/snmp/${ENTRY}	${PAYLOAD}

API::Get SNMP Entry
	[Arguments]	${ENTRY}
	[Documentation]     Get SNMP entry using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get SNMP entry from NG
	[Tags]	API
	${response}=	API::Send Get Request	network/snmp/${ENTRY}
	[return]	${response}

API::Get Devices Power Preference
	[Documentation]     Get devices power settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return devices power settings info
	[Tags]	API
	${response}=	API::Send Get Request	devices/preference/power
	[return]	${response}

API::Update Devices Power Preference
	[Arguments]	${PAYLOAD}
	[Documentation]     Get devices power settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return devices power settings info
	[Tags]	API
	API::Send Put Request	devices/preference/power	${PAYLOAD}

API::Get Devices Session Preference
	[Documentation]     Get devices power settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return devices power settings info
	[Tags]	API
	${response}=	API::Send Get Request	devices/preference/session
	[return]	${response}

API::Update Devices Session Preference
	[Arguments]	${PAYLOAD}
	[Documentation]     Get devices session settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return devices session settings info
	[Tags]	API
	API::Send Put Request	devices/preference/session	${PAYLOAD}

API::Get Devices Types
	[Documentation]     Get devices types using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return devices types info
	[Tags]	API
	${response}=	API::Send Get Request	devices/types
	[return]	${response}

API::Clone Device Type
	[Arguments]	${SOURCE}	${NEW}
	[Documentation]     Clones device type using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Create a new device type cloning another one
	[Tags]	API
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	device_type_name=${NEW}
	API::Send Post Request	devices/types/clone/${SOURCE}	${payload}

API::Delete Device Type
	[Arguments]	${TYPE}
	[Documentation]     Deletes device type using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Delete the given device type from NG
	[Tags]	API
	API::Send Delete Request	devices/types/${TYPE}

API::Get Device Type
	[Arguments]	${TYPE}
	[Documentation]     Get device type using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device type info from NG
	[Tags]	API
	${response}=	API::Send Get Request	devices/types/${TYPE}
	[return]	${response}

API::Update Device Type
	[Arguments]	${TYPE}	${PAYLOAD}
	[Documentation]     Update device type using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update device type
	[Tags]	API
	API::Send Put Request	devices/types/${TYPE}	${PAYLOAD}

API::Get Device Outlets
	[Arguments]	${DEVICE}
	[Documentation]     Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	[Tags]	API
	${response}=	API::Send Get Request	devices/${DEVICE}/outlets
	[return]	${response}

API::Get Auto Discovery Network Scan
	[Documentation]     Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	[Tags]	API
	${response}=	API::Send Get Request	devices/discovery/network
	[return]	${response}

API::Get Auto Discovery VM Managers
	[Documentation]     Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	[Tags]	API
	${response}=	API::Send Get Request	devices/discovery/vmmanager
	[return]	${response}

API::Get Auto Discovery Rules
	[Documentation]     Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	[Tags]	API
	${response}=	API::Send Get Request	devices/discovery/rules
	[return]	${response}

API::Get Auto Discovery Hostname Detection
	[Documentation]     Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	[Tags]	API
	${response}=	API::Send Get Request	devices/discovery/hostname
	[return]	${response}

API::Get Discovery Logs From Auto Discovery
	[Documentation]     Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	[Tags]	API
	${response}=	API::Send Get Request	devices/discovery/logs
	[return]	${response}

API::Get Discovery Now
	[Documentation]     Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	[Tags]	API
	${response}=	API::Send Get Request	devices/discovery/now
	[return]	${response}

API::Add Network Scan
	[Arguments]	${PAYLOAD}
	API::Send Post Request	devices/discovery/network	${PAYLOAD}

API::Update Network Scan
	[Arguments]	${NETSCAN}	${PAYLOAD}
	API::Send Put Request	devices/discovery/network/${NETSCAN}	${PAYLOAD}

API::Delete Network Scan
	[Arguments]	${NETSCAN}
	API::Send Delete Request	devices/discovery/network/${NETSCAN}

API::Get Network Scan
	[Arguments]	${NETSCAN}
	${response}=	API::Send Get Request	devices/discovery/network/${NETSCAN}
	[return]	${response}

API::Add VM Manager
	[Arguments]	${PAYLOAD}
	API::Send Post Request	devices/discovery/vmmanager	${PAYLOAD}

API::Delete VM Manager
	[Arguments]	${MANAGER}
	API::Send Delete Request	devices/discovery/vmmanager/${MANAGER}

API::Update VM Manager
	[Arguments]	${MANAGER}	${PAYLOAD}
	API::Send Put Request	devices/discovery/vmmanager/${MANAGER}	${PAYLOAD}

API::Get VM Manager
	[Arguments]	${MANAGER}
	${response}=	API::Send Get Request	devices/discovery/vmmanager/${MANAGER}
	[return]	${response}

API::Add Discovery Rule
	[Arguments]	${PAYLOAD}
	API::Send Post Request	devices/discovery/rules	${PAYLOAD}

API::Delete Discovery Rule
	[Arguments]	${RULE}
	API::Send Delete Request	devices/discovery/rules/${RULE}

API::Update Discovery Rule
	[Arguments]	${RULE}	${PAYLOAD}
	API::Send Put Request	devices/discovery/rules/${RULE}	${PAYLOAD}

API::Get Discovery Rule
	[Arguments]	${RULE}
	${response}=	API::Send Get Request	devices/discovery/rules/${RULE}
	[return]	${response}

API::Move Discovery Rule Up
	[Arguments]	${RULE}
	[Documentation]	Move the discovery rule up using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Move the discovery rule up using the NG API
	[Tags]	API
	API::Send Put Request	devices/discovery/rules/${RULE}/up

API::Move Discovery Rule Down
	[Arguments]	${RULE}
	[Documentation]	Move the discovery rule down using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Move the discovery rule down using the NG API
	[Tags]	API
	API::Send Put Request	devices/discovery/rules/${RULE}/down

API::Add Hostname Detection Rule
	[Arguments]	${PAYLOAD}
	API::Send Post Request	devices/discovery/hostname	${PAYLOAD}

API::Delete Hostname Detection Rule
	[Arguments]	${HOSTNAME}
	API::Send Delete Request	devices/discovery/hostname/${HOSTNAME}

API::Update Hostname Detection Rule
	[Arguments]	${HOSTNAME}	${PAYLOAD}
	API::Send Put Request	devices/discovery/hostname/${HOSTNAME}	${PAYLOAD}

API::Get Hostname Detection Rule
	[Arguments]	${HOSTNAME}
	${response}=	API::Send Get Request	devices/discovery/hostname/${HOSTNAME}
	[return]	${response}

API::Get Hostname Detection Global Settings
	[Documentation]     Get hostname detection global settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return hostname global settings
	[Tags]	API
	${response}=	API::Send Get Request	devices/discovery/hostname/globalsettings
	[return]	${response}

API::Update Hostname Detection Global Settings
	[Arguments]	${PAYLOAD}
	[Documentation]     Update hostname detection global settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update hostname global settings
	[Tags]	API
	API::Send Put Request	devices/discovery/hostname/globalsettings	${PAYLOAD}

API::Reset Discovery Logs
	API::Send Delete Request	devices/discovery/logs

API::Get Device Logging
	[Arguments]	${DEVICE}
	[Documentation]     Get device logging configuration using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's logging info from NG
	${response}=	API::Send Get Request	devices/${DEVICE}/logging
	[return]	${response}

API::Update Device Logging
	[Arguments]	${DEVICE}	${PAYLOAD}
	[Documentation]     Updates device logging configuration using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the given device's logging info from NG
	API::Send Put Request	devices/${DEVICE}/logging	${PAYLOAD}

API::Get Device Management
	[Arguments]	${DEVICE}
	[Documentation]     Get device management configuration using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's management info from NG
	${response}=	API::Send Get Request	devices/${DEVICE}/management
	[return]	${response}

API::Update Device Management
	[Arguments]	${DEVICE}	${PAYLOAD}
	[Documentation]     Updates device management configuration using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the given device's management info from NG
	API::Send Put Request	devices/${DEVICE}/management	${PAYLOAD}

API::Get Device Custom Fields
	[Arguments]	${DEVICE}
	[Documentation]     Get device custom fields using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's custom fields info from NG
	${response}=	API::Send Get Request	devices/${DEVICE}/customfields
	[return]	${response}

API::Delete Device Custom Field
	[Arguments]	${DEVICE}	${FIELD}
	[Documentation]     Delete device custom fields using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Delete the given device's custom fields info from NG
	API::Send Delete Request	devices/${DEVICE}/customfields/${FIELD}

API::Add Device Custom Field
	[Arguments]	${DEVICE}	${PAYLOAD}
	[Documentation]     Adds device custom fields using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add the given device's custom fields info from NG
	API::Send Post Request	devices/${DEVICE}/customfields	${PAYLOAD}

API::Get Device Custom Field
	[Arguments]	${DEVICE}	${FIELD}
	[Documentation]     Get device custom fields using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's custom fields info from NG
	${response}=	API::Send Get Request	devices/${DEVICE}/customfields/${FIELD}
	[return]	${response}

API::Update Device Custom Field
	[Arguments]	${DEVICE}	${FIELD}	${PAYLOAD}
	[Documentation]     Update device custom fields using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the given device's custom fields info from NG
	API::Send Put Request	devices/${DEVICE}/customfields/${FIELD}	${PAYLOAD}

API::Get Device Commands
	[Arguments]	${DEVICE}
	[Documentation]     Get device commands using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's commands info from NG
	${response}=	API::Send Get Request	devices/${DEVICE}/commands
	[return]	${response}

API::Delete Device Command
	[Arguments]	${DEVICE}	${COMMAND}
	[Documentation]     Delete device command using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Delete the given device's command info from NG
	API::Send Delete Request	devices/${DEVICE}/commands/${COMMAND}

API::Add Device Command
	[Arguments]	${DEVICE}	${PAYLOAD}
	[Documentation]     Adds device command using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add the given device's command info from NG
	API::Send Post Request	devices/${DEVICE}/commands	${PAYLOAD}

API::Get Device Command
	[Arguments]	${DEVICE}	${COMMAND}
	[Documentation]     Get device command using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's command info from NG
	${response}=	API::Send Get Request	devices/${DEVICE}/commands/${COMMAND}
	[return]	${response}

API::Update Device Command
	[Arguments]	${DEVICE}	${COMMAND}	${payload}
	[Documentation]     Update device command using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the given device's command info from NG
	API::Send Put Request	devices/${DEVICE}/commands/${COMMAND}	${PAYLOAD}

API::Get Cloud Peers
	[Documentation]     Get cloud peer using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get cloud peers info from NG
	${response}=	API::Send Get Request	cloud/peers
	[return]	${response}

API::Get Cloud Settings
	[Documentation]     Get cloud settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get cloud settings info from NG
	${response}=	API::Send Get Request	cloud/settings
	[return]	${response}

API::Get Cloud Management
	[Documentation]     Get cloud management using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get cloud management info from NG
	${response}=	API::Send Get Request	cloud/management
	[return]	${response}

API::Update Cloud Settings
	[Arguments]	${PAYLOAD}
	[Documentation]     Update cloud settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update cloud settings info from NG
	API::Send Put Request	cloud/settings	${PAYLOAD}
