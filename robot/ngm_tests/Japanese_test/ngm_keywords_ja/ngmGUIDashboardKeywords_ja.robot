*** Keywords ***

GUI::Dashboard::Open Dashboard Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid dashboard tab
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Dashboard Tab is open and the kibana iframe will be shown
	GUI::Basic::Wait Until Element Is Accessible	jquery=#main_menu > li:nth-child(9) > a
	Click Element	jquery=#main_menu > li:nth-child(9) > a
	GUI::Basic::Spinner Should Be Invisible