*** Keywords ***

GUI::Access::Open
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Access tab
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Access Tab is open and all elements are accessible
	Set Selenium Timeout    15s
	Wait Until Element Is Visible	//*[@id='main_menu']/li[1]/a
	GUI::Basic::Click Element	//*[@id='main_menu']/li[1]/a
	GUI::Basic::Spinner Should Be Invisible

GUI:Access::Open Table tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Access::Table tab
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Table Tab is open and all elements are accessible
	GUI::Access::Open
	Wait Until Element Is Visible	//*[contains(@class, 'submenu')]/ul/li[1]/a
	GUI::Basic::Click Element	//*[contains(@class, 'submenu')]/ul/li[1]/a
	Wait Until Element Is Visible	//*[contains(@class, 'widget-title')]/h4
	${WIDGET_TILE}=	Get Text	//*[contains(@class, 'widget-title')]/h4
	Should Be Equal	${WIDGET_TILE}	Access :: Table

GUI:Access::Open Tree tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Access::Tree tab
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Tree Tab is open and all elements are accessible
	GUI::Access::Open
	Wait Until Element Is Visible	jquery=.submenu ul li:nth-child(2) a
	GUI::Basic::Click Element	//*[contains(@class, 'submenu')]/ul/li[2]/a
	Wait Until Element Is Visible	jquery=.widget-title h4
	${WIDGET_TILE}=	Get Text	jquery=.widget-title h4
	Should Be Equal	${WIDGET_TILE}	Access :: Tree

GUI:Access::Open Node tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Access::Node tab
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Node Tab is open and all elements are accessible
	GUI::Access::Open
	Wait Until Element Is Visible	jquery=.submenu ul li:nth-child(3) a
	GUI::Basic::Click Element	//*[contains(@class, 'submenu')]/ul/li[3]/a
	Wait Until Element Is Visible	jquery=.widget-title h4
	${WIDGET_TILE}=	Get Text	jquery=.widget-title h4
	Should Be Equal	${WIDGET_TILE}	Access :: Node

GUI:Access::Open Map tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Access::Map tab
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Map Tab is open and all elements are accessible
	GUI::Access::Open
	Wait Until Element Is Visible	jquery=.submenu ul li:nth-child(4) a
	GUI::Basic::Click Element	//*[contains(@class, 'submenu')]/ul/li[4]/a
	Wait Until Element Is Visible	jquery=.widget-title h4
	${WIDGET_TILE}=	Get Text	jquery=.widget-title h4
	Should Be Equal	${WIDGET_TILE}	Access :: Map

GUI:Access::Open Image tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Access::Image tab
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Image Tab is open and all elements are accessible
	GUI::Access::Open
	Wait Until Element Is Visible	jquery=.submenu ul li:nth-child(5) a
	GUI::Basic::Click Element	//*[contains(@class, 'submenu')]/ul/li[5]/a
	Wait Until Element Is Visible	jquery=.widget-title h4
	${WIDGET_TILE}=	Get Text	jquery=.widget-title h4
	Should Be Equal	${WIDGET_TILE}	Access :: Image

GUI::Access::Get Device Type in Table Page
	[Arguments]	${DEVICE_NAME}
	[Documentation]	Go to Access > Table and return the type of the ${DEVICE_NAME} by clicking in device name
	...	and getting the device TYPE from the modal table
	...	== ARGUMENTS ==
	...	-   DEVICE_NAME = Target device name
	...	== EXPECTED RESULT ==
	...	Returns the target device TYPE
	GUI:Access::Open Table tab
	Click Element	xpath=//span[.="${DEVICE_NAME}"]
	Wait Until Page Contains Element	xpath=//tbody[@id='tbody']
	${DEVICE_TYPE}=	Get Text	xpath=//tbody[@id='tbody']/tr[3]/td[2]
	Press Keys	None	ESC
	[Return]	${DEVICE_TYPE}

GUI:Access::Device
	[Arguments]	${JQUERY}	${DEVICE_NAME}
	[Timeout]	2 minutes
	${DEVICE_TYPE}=	GUI::Access::Get Device Type in Table Page	${DEVICE_NAME}

	GUI:Access::Open Table tab
	${MAIN_WINDOW_TITLE}=	Get Title
	Click Element	jquery=${JQUERY}
	GUI:Basic::Wait Until Window Exists	${DEVICE_NAME}
	Switch Window	title=${DEVICE_NAME}
	${FOUND_ALERT}=	Run Keyword And Return Status	Alert Should Be Present
	Run Keyword If	${FOUND_ALERT}  Switch Window	title=ZPE Systems®, Inc.
	Run Keyword If	${FOUND_ALERT}	Fail	No alert should be visible after ${DEVICE_NAME} window launch
	Wait Until Page Contains Element	xpath=//*[@id='targetParameters']
#	buttons: text input and clipboard removed from console window button - ttyd_template.html
	Run Keyword If	'${DEVICE_TYPE}' != 'device_console'	Wait Until Page Contains Element	xpath=//*[@id='txt']
	Run Keyword If	'${DEVICE_TYPE}' != 'device_console'	Wait Until Page Contains Element	xpath=//*[@id='cpb']
	Wait Until Page Contains Element	xpath=//*[@id='inf']
	Wait Until Page Contains Element	xpath=//*[@id='fs']
	Wait Until Page Contains Element	xpath=//*[@id='cw']
	Wait Until Page Contains Element	xpath=//*[@id='expand_button']
	${POWER_BUTTON_DEVICES}=	Create List	ilo	imm	drac	idrac6	ipmi_1.5	ipmi_2.0	ilom	cimc_ucs
	...	netapp	virtual_console_kvm	kvm_aten	kvm_raritan	usb	cimc_ucs2
	${SHOULD_CONTAIN_POWER_BUTTONS}=	Run Keyword And Return Status	Should Contain	${POWER_BUTTON_DEVICES}	${DEVICE_TYPE}
	Run Keyword If	${SHOULD_CONTAIN_POWER_BUTTONS}
	...	Run Keywords
	...	Wait Until Page Contains Element	xpath=//*[@id='po']
	...	AND	Wait Until Page Contains Element	xpath=//*[@id='pon']
	...	AND	Wait Until Page Contains Element	xpath=//*[@id='cyc']
	...	AND	Wait Until Page Contains Element	xpath=//*[@id='shut']
	...	AND	Wait Until Page Contains Element	xpath=//*[@id='pst']
	Close Window
	Switch Window	title=${MAIN_WINDOW_TITLE}
GUI:Access::Device Check Terminal
	[Arguments]	${JQUERY}	${DEVICE_NAME}
	[Documentation]     Goes to the Access::Table page and opens the given device console
	...	== ARGUMENTS ==
	...	-   JQUERY = Console button of the given target device
	...	-   DEVICE_NAME = Target device name
	...	== EXPECTED RESULT ==
	...	Access Tab is open and click the console button of the given target device validating the TERMINAL page elements
	GUI:Access::Open Table tab
	${MAIN_WINDOW_TITLE}=	Get Title
	Click Element	jquery=${JQUERY}
	GUI:Basic::Wait Until Window Exists	${DEVICE_NAME}
	Switch Window	title=${DEVICE_NAME}
	${FOUND_ALERT}=	Run Keyword And Return Status	Alert Should Be Present
	Run Keyword If	${FOUND_ALERT}  Switch Window	title=ZPE Systems®, Inc.
	Run Keyword If	${FOUND_ALERT}	Fail	No alert should be visible after ${DEVICE_NAME} window launch
#	TTYD terminal elements
	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
	Select Frame	xpath=//*[@id='termwindow']
	Current Frame Should Contain	terminal-container
	Current Frame Should Contain	xterm-viewport
	Current Frame Should Contain	xterm-screen
#	TTYD xterm.js elements
	Wait Until Page Contains Element	css=.xterm-helpers
	Wait Until Page Contains Element	css=.xterm-helper-textarea
	Wait Until Page Contains Element	css=.xterm-text-layer
	Wait Until Page Contains Element	css=.xterm-selection-layer
	Wait Until Page Contains Element	css=.xterm-link-layer
	Wait Until Page Contains Element	css=.xterm-cursor-layer
#	Resizing terminal window
	${PAGE_WIDTH}	${PAGE_HEIGHT}=	Get Window Size
	Set Window Size	800	600
	Sleep	1
	Set Window Size	${PAGE_WIDTH}	${PAGE_HEIGHT}

	Close Window
	Switch Window	title=${MAIN_WINDOW_TITLE}

GUI:Access::Device Access Console
	[Arguments]	${JQUERY}	${DEVICE_NAME}
	[Documentation]     Goes to the Access::Table page and opens the given device console and keeps
	...	the console window open
	...	== ARGUMENTS ==
	...	-   JQUERY = Console button of the given target device
	...	-   DEVICE_NAME = Target device name
	...	== EXPECTED RESULT ==
	...	Access Tab is open and click the console button of the given target device validating the CONSOLE and then,
	...	funcionalities and keeps the console window open

	GUI:Access::Open Table tab
	Click Element	jquery=${JQUERY}
	GUI:Basic::Wait Until Window Exists	${DEVICE_NAME}
	Switch Window	title=${DEVICE_NAME}
	${FOUND_ALERT}=	Run Keyword And Return Status	Alert Should Be Present
	Run Keyword If	${FOUND_ALERT}  Switch Window	title=ZPE Systems®, Inc.
	Run Keyword If	${FOUND_ALERT}	Fail	No alert should be visible after ${DEVICE_NAME} window launch
#	TTYD terminal elements
	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
	Select Frame	xpath=//*[@id='termwindow']
	Current Frame Should Contain	terminal-container
	Current Frame Should Contain	xterm-viewport
	Current Frame Should Contain	xterm-screen
#	TTYD xterm.js elements
	Wait Until Page Contains Element	css=.xterm-helpers
	Wait Until Page Contains Element	css=.xterm-helper-textarea
	Wait Until Page Contains Element	css=.xterm-text-layer
	Wait Until Page Contains Element	css=.xterm-selection-layer
	Wait Until Page Contains Element	css=.xterm-link-layer
	Wait Until Page Contains Element	css=.xterm-cursor-layer
	Press Keys 	None	RETURN

GUI:Access::Console Command Output
	[Arguments]	${COMMAND}
	[Documentation]     Input the given command into the ttyd terminal and retrieve the output after prompt lines
	...	== ARGUMENTS ==
	...	-   COMMAND = Command to be executed
	...	== EXPECTED RESULT ==
	...	Input the command into the ttyd terminal and returns the OUTPUT between the command and the last prompt

	${CHECK_TAB}=	Run Keyword And Return Status	Should Not Contain	${COMMAND}	TAB
	Run Keyword If	${CHECK_TAB}	Press Keys 	None	${COMMAND}	RETURN
	...	ELSE	Press Keys 	None	${COMMAND}
	Sleep	1
#	Since ttyd(xterm) draw everything in canvases, text cannot be easily extracted, and so far
#	the best approach I've found is by calling term.getSelection().trim() to get a copy of all text in the canvas.
#	Create selection, then get selection:
	${SELECTION}=	Execute JavaScript	term.selectAll(); return term.getSelection().trim();
	Should Contain  ${SELECTION}	[Enter '^Ec?' for help]
	Should Contain  ${SELECTION}	[Enter '^Ec.' to cli ]
	Should Not Contain  ${SELECTION}	[error.connection.failure] Could not establish a connection to device

	${LINES}=	Split String	${SELECTION}	\n
	${INDEXES_OCCURRED}=	Create List
	${LENGTH}=	Get Length	${LINES}
	:FOR	${INDEX}	IN RANGE	0	${LENGTH}
	\	${LINE}=	Get From List	${LINES}	${INDEX}
	\	${CHECK}=	Run Keyword And Return Status	Should Contain	${LINE}	]#
	\	Run Keyword If	${CHECK}	Append to List	${INDEXES_OCCURRED}	${INDEX}

	${STRING}=	Set Variable
	${END}=	Get From List	${INDEXES_OCCURRED}	-1
	${PREVIOUS}=	Get From List	${INDEXES_OCCURRED}	-2
	:FOR	${INDEX}	IN RANGE	${PREVIOUS}	${END}
	\	${LINE}=	Get From List	${LINES}	${INDEX}
	\	${STRING}=	Set Variable	${STRING}\n${LINE}
	${STRING}=	Get Substring	${STRING}	1
	[Return]	${STRING}

GUI::Access::Get Userline Length
	[Documentation]     Retrieves the current userline char length
	...	eg.: 'admin@nodegrid.localdomain' turns to '[admin@nodegrid /]# ' and will return 20
	...	== EXPECTED RESULT ==
	...	Returns the length of the userline to use as a reference in the console window to simulate copy/paste

	${NAME}=	Get Text	pwl
	${NAME}=	Fetch From Left	${NAME}	.
	${NAME}=	Get Substring	${NAME}	1
	${NAME}=	Set Variable	[${NAME} /]#${SPACE}
	${LENGTH}=	Get Length	${NAME}
	[Return]	${LENGTH}

GUI:Access::Inspect Device In Row
	[Arguments]	${ROW}	${BUTTON}	${TERMINAL}=${FALSE}	${ALIVE}=False
	${ID_DEVICE}=	Get Element Attribute	${ROW}	id
	${ID_DEVICE}=	Escape jQuery Selector	${ID_DEVICE}
	${JQUERY}=	Set Variable	div:visible > table > tbody > tr\#${ID_DEVICE} > td:nth-child(2) > div > a
	${BUTTONS}=	Get Element Count	jquery=${JQUERY}
	:FOR	${INDEX}	IN RANGE	${BUTTONS}
	\	${TEXT}=	Get Text	jquery=${JQUERY}:nth-child(${INDEX+1})
	\	${IS_EXPECTED_BUTTON}=	Run Keyword And Return Status	Should Be Equal	${TEXT}	${BUTTON}
	\	${DEVICE_NAME}=	Get Text	jquery=div:visible > table > tbody > tr\#${ID_DEVICE} > td:first-child() > div > a > span
	\	Run Keyword If	${IS_EXPECTED_BUTTON} and '${TERMINAL}' == 'False' and '${ALIVE}' == 'False'	GUI:Access::Device	${JQUERY}:nth-child(${INDEX+1})	${DEVICE_NAME}
	\	Run Keyword If	${IS_EXPECTED_BUTTON} and ${TERMINAL} and '${BUTTON}' == 'Console'	GUI:Access::Device Check Terminal	${JQUERY}:nth-child(${INDEX+1})	${DEVICE_NAME}
	\	Run Keyword If	${IS_EXPECTED_BUTTON} and '${TERMINAL}' == 'False' and '${ALIVE}' == 'True'	GUI:Access::Device Access Console	${JQUERY}:nth-child(${INDEX+1})	${DEVICE_NAME}
	\	Exit for Loop If	${IS_EXPECTED_BUTTON}

GUI:Access::Access Devices With Button
	[Arguments]	${BUTTON}	${IGNORE}=${EMPTY}	${MAX}=5
	${ROWS}=	Get WebElements	jquery=div:visible > table.SPMTable > tbody > tr
	${COUNT}=	Set Variable	0
	:FOR	${ROW}	IN	@{ROWS}
	\	Run Keyword If	${COUNT} >= ${MAX}	Exit For Loop
	\	${ID_DEVICE}=	Get Element Attribute	${ROW}	id
	\	Run Keyword If	'${IGNORE}' == '${ID_DEVICE}'	Continue For Loop
	\	${COUNT}=	Set Variable	'${COUNT}' + '1'
	\	GUI:Access::Inspect Device In Row	${ROW}	${BUTTON}

GUI::Access::Access ${DEVICE} Device With ${BUTTON}
	[Documentation]     Launch the given action in the given device in the access page
	${ID_DEVICE}=	Escape jQuery Selector	|${DEVICE}
	${ROW}=	Get WebElement	jquery=div:visible > table.SPMTable > tbody > tr\#${ID_DEVICE}
	GUI:Access::Inspect Device In Row	${ROW}	${BUTTON}

GUI::Access::Access ${DEVICE} Device and Check Terminal ${TERMINAL}
	[Documentation]     Launch the given action in the given device in the access page
	${ID_DEVICE}=	Escape jQuery Selector	|${DEVICE}
	${ROW}=	Get WebElement	jquery=div:visible > table.SPMTable > tbody > tr\#${ID_DEVICE}
	GUI:Access::Inspect Device In Row	${ROW}	Console	${TERMINAL}

GUI::Access::Access ${DEVICE} Device and Console Window Open ${ALIVE}
	[Documentation]     Launch the given action in the given device in the access page
	${ID_DEVICE}=	Escape jQuery Selector	|${DEVICE}
	${ROW}=	Get WebElement	jquery=div:visible > table.SPMTable > tbody > tr\#${ID_DEVICE}
	GUI:Access::Inspect Device In Row	${ROW}	Console	ALIVE=${ALIVE}