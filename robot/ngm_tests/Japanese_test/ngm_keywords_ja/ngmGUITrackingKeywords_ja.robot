

*** Keywords ***
GUI::Tracking::Open
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Tracking tab
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Tracking Tab is open and all elements are accessable
	Click Element	jquery=#main_menu > li:nth-child(2) > a
	GUI::Basic::Spinner Should Be Invisible


GUI::Tracking::Open Open Sessions Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Tracking::Open Sessions
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Open Sessions Tab is open and all elements are accessable
	[Tags]	GUI
	GUI::Tracking::Open
	GUI::Basic::Wait Until Element Is Accessible	jquery=body > div.main_menu > div > ul > li:nth-child(1)
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(1)
	GUI::Basic::Spinner Should Be Invisible


GUI::Tracking::Open Event List Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Tracking::Event List
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Event List Tab is open and all elements are accessable
	[Tags]	GUI
	GUI::Tracking::Open
	GUI::Basic::Wait Until Element Is Accessible	jquery=body > div.main_menu > div > ul > li:nth-child(2)
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(2)
	GUI::Basic::Spinner Should Be Invisible


GUI::Tracking::Open System Usage Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Tracking::System Usage
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	System Usage Tab is open and all elements are accessable
	[Tags]	GUI
	GUI::Tracking::Open
	GUI::Basic::Wait Until Element Is Accessible	jquery=body > div.main_menu > div > ul > li:nth-child(3)
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(3)
	GUI::Basic::Spinner Should Be Invisible


GUI::Tracking::Open Discovery Logs Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Tracking::Discovery Logs
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Discovery Logs Tab is open and all elements are accessable
	[Tags]	GUI
	GUI::Tracking::Open
	GUI::Basic::Wait Until Element Is Accessible	jquery=body > div.main_menu > div > ul > li:nth-child(4)
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(4)
	GUI::Basic::Spinner Should Be Invisible


GUI::Tracking::Open Network Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Tracking::Network
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Network Tab is open and all elements are accessable
	[Tags]	GUI
	GUI::Tracking::Open
	GUI::Basic::Wait Until Element Is Accessible	jquery=body > div.main_menu > div > ul > li:nth-child(5)
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(5)
	GUI::Basic::Spinner Should Be Invisible


GUI::Tracking::Open Devices Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Tracking::Devices
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Devices Tab is open and all elements are accessable
	[Tags]	GUI
	GUI::Tracking::Open
	GUI::Basic::Wait Until Element Is Accessible	jquery=body > div.main_menu > div > ul > li:nth-child(6)
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(6)
	GUI::Basic::Spinner Should Be Invisible



GUI::Tracking::Open Scheduler Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Tracking::Scheduler
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Scheduler Tab is open and all elements are accessable
	[Tags]	GUI
	GUI::Tracking::Open
	GUI::Basic::Wait Until Element Is Accessible	jquery=body > div.main_menu > div > ul > li:nth-child(7)
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(7)
	GUI::Basic::Spinner Should Be Invisible





