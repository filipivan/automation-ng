*** Keywords ***

GUI::Network::Open
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Network tab
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Network Tab is open and all elements are accessable
	Click Element	jquery=#main_menu > li:nth-child(4) > a
	GUI::Basic::Spinner Should Be Invisible

GUI::Network::Open Settings Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Network::Settings
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Settings Tab is open and all elements are accessable
	[Tags]	GUI	NETWORK	SETTINGS
	GUI::Network::Get Tab       1
	Element Should Be Visible       jquery=#hostname

GUI::Network::Open Connections Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Network::Connections
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Connections Tab is open and all elements are accessable
	[Tags]	GUI	NETWORK     CONNECTIONS
	GUI::Network::Get Tab       2
	Element Should Be Visible       jquery=#peerTable

GUI::Network::Open Static Routes Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Network::Static Routes
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Static Routes Tab is open and all elements are accessable
	[Tags]	GUI	NETWORK     STATIC_ROUTES
	GUI::Network::Get Tab       3
	Element Should Be Visible       jquery=#netmnt_StroutTable

GUI::Network::Open Hosts Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Network::Hosts
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Hosts Tab is open and all elements are accessable
	[Tags]	GUI	NETWORK     HOSTS
	GUI::Network::Get Tab       4
	Element Should Be Visible       jquery=#hostTable

GUI::Network::Open SNMP Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Network::SNMP
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	SNMP Tab is open and all elements are accessable
	[Tags]	GUI	NETWORK     SNMP
	GUI::Network::Get Tab       5
	Element Should Be Visible       jquery=#snmpTable

GUI::Network::Open DHCP Server Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Network::DHCP Server
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	DHCP Server Tab is open and all elements are accessable
	[Tags]	GUI	NETWORK     DHCP_SERVER
	GUI::Network::Get Tab       6
	Element Should Be Visible       jquery=#hostTable

GUI::Network::Open SSL VPN Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Network::SSL VPN
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	SSL VPN Tab is open and all elements are accessable
	[Tags]	GUI	NETWORK     STATIC_ROUTES
	GUI::Network::Get Tab       7
	Element Should Be Visible       jquery=#sslvpnClientTable

GUI::Network::SSL VPN::Open Client Tab
    [Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Network::SSL VPN::Client
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Client Tab is open and Logging menu is activated
	[Tags]	GUI	NETWORK     SSL_VPN     CLIENT
	Execute Javascript	window.document.evaluate("//*[@id='main_menu']", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element	jquery=#pod_menu > a:nth-child(1)
	GUI::Basic::Spinner Should Be Invisible

GUI::Network::SSL VPN::Open Server Tab
    [Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Network::SSL VPN::Server
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Server Tab is open and Logging menu is activated
	[Tags]	GUI	NETWORK     SSL_VPN     CLIENT
	Execute Javascript	window.document.evaluate("//*[@id='main_menu']", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element	jquery=#pod_menu > a:nth-child(2)
	GUI::Basic::Spinner Should Be Invisible

GUI::Network::SSL VPN::Open Server Status Tab
    [Arguments]	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Manager Network::SSL VPN::Server Status
	...	== ARGUMENTS ==
	...	-   SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Server Status Tab is open and Logging menu is activated
	[Tags]	GUI	NETWORK     SSL_VPN     SERVER_STATUS
	Execute Javascript	window.document.evaluate("//*[@id='main_menu']", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element	jquery=#pod_menu > a:nth-child(3)
	GUI::Basic::Spinner Should Be Invisible

GUI::Network::Get Tab
    [Arguments]  ${TAB_NUM}
    Execute Javascript	window.document.evaluate("//*[@id='main_menu']", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	GUI::Network::Open
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(${TAB_NUM})
	GUI::Basic::Spinner Should Be Invisible
