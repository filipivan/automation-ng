*** Keywords ***
Should Be Integer
	[Arguments]	${VALUE}
	Should Match Regexp	${VALUE}	\\d+

Should Be Real
	[Arguments]	${VALUE}
	Should Match Regexp	${VALUE}	(\\d+(\\.\\d+)*)|(\\.\\d+)

Split String In Words
	[Arguments]	${STR}
	[Documentation]     Split the given string in words
	@{WORDS}=	Get Regexp Matches	${STR}	\\w+
	[Return]	@{WORDS}

Escape jQuery Selector
	[Arguments]	${SELECTOR}
	[Documentation]	Format the selector string to be used in the jquery locators
	${RET} =	Replace String	${SELECTOR}	|	\\\\|
	${RET} =	Replace String	${RET}	${SPACE}	\\\\${SPACE}
	[return]	${RET}

Escape License
	[Arguments]	${LICENSE}
	${parts}=	Split String	${LICENSE}	-
	${encoded}=	Catenate	SEPARATOR=	xxxxx-xxxxx-xxxxx-	${parts[3]}
	[Return]	${encoded}
