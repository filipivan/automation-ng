*** Settings ***
Documentation    Testing if Elements in the Tracking -> Open Sessions Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL


*** Test Cases ***

Test Open Sessions Sub Menu Tabs
    GUI::Basic::Japanese::Test Translation      //*[@id="sessionsTable"]
    GUI::Basic::Japanese::Test Translation      //*[@id="deviceSessionsTable"]

Test Sessions Table Non-Access Controls
    GUI::Basic::Japanese::Test Translation With Value      //*[@id="nonAccessControls"]/input
    GUI::Basic::Japanese::Test Translation      //*[@id="nonAccessControls"]/div/div

Test Session Table Header
    GUI::Basic::Japanese::Test Translation Table Header With Checkbox  8

Test Session Table Elements
    GUI::Basic::Japanese::Test Table Elements    activeSessionsTable     3
    GUI::Basic::Japanese::Test Table Elements    activeSessionsTable     5

Test Self Terminate Error
    ${Table_Size}=  GUI::Basic::Count Table Rows    activeSessionsTable
    Select Checkbox     //*[@id="tbody"]/tr[${Table_Size}]/td/input
	Click Element       //*[@id="nonAccessControls"]/input
	GUI::Basic::Japanese::Test Translation      //*[@id="main_doc"]/div[1]/div/div

Test Devices Table Elements
    Click Element   //*[@id="deviceSessionsTable"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value      //*[@id="nonAccessControls"]/input

Test Devices Table Header
    GUI::Basic::Japanese::Test Translation Table Header With Checkbox  4

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[2]
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers