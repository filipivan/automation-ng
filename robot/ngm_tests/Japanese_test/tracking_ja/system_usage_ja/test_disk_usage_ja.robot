*** Settings ***
Documentation    Testing if Elements in the Tracking ->  System Usage -> Disk Usage Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL


*** Test Cases ***
Test Disk Usage
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(3)
    GUI::Basic::Spinner Should Be Invisible
    Click Element   //*[@id="diskInfo"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation      //*[@id="diskInfo"]

Test Disk Usage Boot Chart
    Test Chart  1   chart-0

Test Disk Usage Configuration Chart
    Test Chart  2   chart-1

Test Disk Usage Root Chart
    Test Chart  3   chart-2

Test Disk Usage Backup Chart
    Test Chart  4   chart-3

Test Disk Usage Swap Chart
    Test Chart  5   chart-4

Test Disk Usage Logs Chart
    Test Chart  6   chart-5

Test Disk Usage Table Header
    GUI::Basic::Japanese::Test Translation Table Header  6

Test Disk Usage Table Elements
    GUI::Basic::Japanese::Test Table Elements    diskTable     6

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[2]
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers

Test Chart
    [Arguments]     ${Div_Num}  ${Chart_Name}
    [Documentation]	Test if the elements and title in the chart are translated
    GUI::Basic::Japanese::Test Translation      //*[@id="chart_form"]/div[1]/div[${Div_Num}]/div[1]
    GUI::Basic::Japanese::Test Translation      //*[@id="${Chart_Name}"]/div[2]/table/tbody/tr[1]/td[2]
    GUI::Basic::Japanese::Test Translation      //*[@id="${Chart_Name}"]/div[2]/table/tbody/tr[2]/td[2]
