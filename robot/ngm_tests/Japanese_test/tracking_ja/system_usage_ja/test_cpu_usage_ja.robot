*** Settings ***
Documentation    Testing if Elements in the Tracking ->  System Usage -> CPU Usage Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL


*** Test Cases ***
Test CPU Usage
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(3)
    GUI::Basic::Spinner Should Be Invisible
    Click Element   //*[@id="cpuInfo"]
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation      //*[@id="cpuInfo"]
    GUI::Basic::Japanese::Test Translation      //*[@id="refresh-icon"]

Test CPU Usage Chart
    GUI::Basic::Japanese::Test Translation      //*[@id="chart_form"]/div[1]/div[1]/div[1]
    : FOR    ${INDEX}    IN RANGE    1    5
    \    GUI::Basic::Japanese::Test Translation      //*[@id="chart-0"]/div/table/tbody/tr[${INDEX}]/td[2]

    : FOR    ${INDEX}    IN RANGE    0    4
    \    GUI::Basic::Japanese::If Element Exists then Test Translation       //*[@id="pieLabel${INDEX}"]/div

Test CPU Usage Table
    GUI::Basic::Japanese::Test Translation Table Header  4


*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[2]
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers
