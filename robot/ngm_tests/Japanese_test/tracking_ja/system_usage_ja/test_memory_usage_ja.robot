*** Settings ***
Documentation    Testing if Elements in the Tracking ->  System Usage -> Memory Usage Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL


*** Test Cases ***
Test Memory Usage
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(3)
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation      //*[@id="memoryInfo"]

Test Memory Usage Memory Chart
    Test Chart  1   chart-0
    GUI::Basic::Japanese::If Element Exists then Test Translation      //*[@id="pieLabel0"]/div
    GUI::Basic::Japanese::If Element Exists then Test Translation      //*[@id="pieLabel1"]/div

Test Memory Usage Swap Chart
    Test Chart  2   chart-1
    GUI::Basic::Japanese::If Element Exists then Test Translation      //*[@id="chart_form"]/div[1]/div[2]/div[2]/span[1]/div
    GUI::Basic::Japanese::If Element Exists then Test Translation      //*[@id="chart_form"]/div[1]/div[2]/div[2]/span[2]/div

Test Memory Usage Table Header
    GUI::Basic::Japanese::Test Translation Table Header  4

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[2]
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers

Test Chart
    [Arguments]     ${Div_Num}  ${Chart_Name}
    [Documentation]	Test if the elements in the chart are translated
    GUI::Basic::Japanese::Test Translation      //*[@id="chart_form"]/div[1]/div[${Div_Num}]/div[1]
    GUI::Basic::Japanese::Test Translation      //*[@id="${Chart_Name}"]/div/table/tbody/tr[1]/td[2]
    GUI::Basic::Japanese::Test Translation      //*[@id="${Chart_Name}"]/div/table/tbody/tr[2]/td[2]