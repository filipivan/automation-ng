*** Settings ***
Documentation    Testing if Elements in the Tracking -> Network -> Interface Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL

*** Test Cases ***
Test Interface
    GUI::Basic::Japanese::Test Translation      //*[@id="netItfStats"]

Test Interface Table Header
    GUI::Basic::Japanese::Test Translation Table Header  8

Test Interface Table Elements
    GUI::Basic::Japanese::Test Table Elements   netitfStatsTable    3

Test Interface Elements
    ${CT}=      Get Element Count   xpath=//*[@id="tbody"]/tr
    ${CT1}=     Evaluate    ${CT} + 1
    :FOR    ${I}    IN RANGE    1   ${CT1}
    \   Click Element   xpath=/html/body/div[7]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[${I}]/td[1]/a
    \   Check Settings
    \   Click element   jquery=#cancelButton

*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[2]
    GUI::Basic::Spinner Should Be Invisible
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(5)
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers

Check Settings
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Japanese::Test Translation With Value   jquery=#cancelButton
    GUI::Basic::Japanese::Test Translation              jquery=#itfCommon
    GUI::Basic::Japanese::Test Translation              jquery=#itfRxStat
    GUI::Basic::Japanese::Test Translation              jquery=#itfTxStat
    gui::basic::check translation no id previous sibling    itfName     itfSpeed    itfCollisions
    gui::basic::check translation no id previous sibling    itfRxPack   itfRxBytes  itfRxErr    itfRxCRC    itfRxDrop
    ...     itfRxFifo   itfRxCompressed     itfRxFrame      itfRxLength     itfRxMissed     itfRxOver
    gui::basic::check translation no id previous sibling    itfTxPack   itfTxBytes  itfTxErr    itfTxCRC    itfTxDrop
    ...     itfTxFifo   itfTxCompressed     itfTxAbort  itfTxHeartB     itfTxWind