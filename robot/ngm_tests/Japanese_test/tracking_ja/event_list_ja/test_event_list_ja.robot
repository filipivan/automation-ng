*** Settings ***
Documentation    Testing if Elements in the Tracking ->  Event List Tab are translated
Metadata    Version     1.0
Metadata    Executed At     ${HOST}
Metadata    Executed with   ${BROWSER}
Resource    ../../init.robot

Suite Setup		Suite Setup
Suite Teardown	Suite Teardown
Force Tags	GUI	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}	WINDOWS	CHROME	FIREFOX	EDGE	IE
Default Tags	EXCLUDEIN3_2    NON-CRITICAL

*** Test Cases ***
Test Event List
    Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(2)
    GUI::Basic::Spinner Should Be Invisible

Test Event List Chart
    GUI::Basic::Japanese::Test Translation      //*[@id="chart_form"]/div[1]/div[1]/div[1]
    : FOR    ${INDEX}    IN RANGE    1    5
    \    GUI::Basic::Japanese::Test Translation      //*[@id="chart-0"]/div[2]/table/tbody/tr[${INDEX}]/td[2]
    :FOR    ${I}    IN RANGE    1   85
    \   run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/div[3]/div[3]/table/tbody/tr[${I}]/td[3]
    \   run keyword and continue on failure     gui::basic::japanese::test translation      xpath=/html/body/div[7]/div[2]/div[2]/div[3]/div[3]/table/tbody/tr[${I}]/td[5]

Test Event List Table Translations
    GUI::Basic::Japanese::Test Translation With Value      //*[@id="nonAccessControls"]/input
    GUI::Basic::Japanese::Test Translation Table Header With Checkbox   5


Test Event List Table Elements
    GUI::Basic::Japanese::Test Table Elements  eventListTable   5
    GUI::Basic::Japanese::Test Table Elements  eventListTable   3

Test Reset Counter
    ${BEG}=     Get Text    xpath=/html/body/div[7]/div[2]/div[2]/div[3]/div[3]/table/tbody/tr[48]/td[4]
    Click Element           xpath=/html/body/div[7]/div[2]/div[2]/div[3]/div[3]/table/tbody/tr[48]/td[1]/input
    Click Element           xpath=/html/body/div[7]/div[2]/div[2]/div[3]/div[2]/input
    ${END}=     Get Text    xpath=/html/body/div[7]/div[2]/div[2]/div[3]/div[3]/table/tbody/tr[48]/td[4]
    Should Be Equal     ${END}  0
    Should Not Be Equal     ${BEG}      ${END}


*** Keywords ***
Suite Setup
    ${PROFILE}=     Evaluate    sys.modules['selenium.webdriver'].FirefoxProfile()   sys
    Call Method      ${PROFILE}      set_preference      intl.accept_languages      ja
    Create WebDriver     Firefox     firefox_profile=${PROFILE}
    #GUI::Basic::Open NodeGrid              ${HOMEPAGE}     ${BROWSER}      ${NGVERSION}
    Go To       ${HOMEPAGE}
    Wait Until element Is Visible       jquery=#login-btn
    GUI::Basic::Login	${NGVERSION}
    Click Element       //*[@id="main_menu"]/li[2]
    GUI::Basic::Spinner Should Be Invisible

Suite Teardown
    GUI::Basic::Logout
    Close all Browsers
