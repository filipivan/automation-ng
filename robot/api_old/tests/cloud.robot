*** Settings ***
Documentation	Test /api/<version>/cloud endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	./init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Get Cloud Settings
	${settings}=	API::Get::Cloud::Settings
	#Length Should Be	${settings}	16
	Should Be Equal As Strings	${settings["auto_psk"]}	nodegrid-key
	Should Be Equal As Strings	${settings["cloud_name"]}	nodegrid
	Should Be Equal As Strings	${settings["lps_type"]}	client
	Should Be Equal As Strings	${settings["coordinator_address"]}	${EMPTY}
	Should Be Equal As Strings	${settings["type"]}	disabled
	Should Be Equal As Strings	${settings["polling_rate"]}	30
	Should Be Equal As Strings	${settings["psk"]}	${EMPTY}
	Should Be Equal As Strings	${settings["renew_time"]}	1
	Should Be Equal As Strings	${settings["lease_time"]}	7
	Should Be True	${settings["allow_enrollment"]}
	Should Be True	${settings["auto_enroll"]}
	Should Be True	${settings["enable_license_pool"]}
	Should Not Be True	${settings["enable_clustering"]}
	Should Not Be True	${settings["enable_cloud"]}
	Should Not Be True	${settings["enable_peer_management"]}

Update Cloud Settings
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	allow_enrollment=${TRUE}
	Set To Dictionary	${payload}	auto_enroll=${FALSE}
	Set To Dictionary	${payload}	auto_psk=nodegrid-key
	Set To Dictionary	${payload}	cloud_name=nodegrid-cloud
	Set To Dictionary	${payload}	enable_clustering=${TRUE}
	Set To Dictionary	${payload}	enable_cloud=${TRUE}
	Set To Dictionary	${payload}	lease_time=7
	Set To Dictionary	${payload}	enable_license_pool=${FALSE}
	Set To Dictionary	${payload}	lps_type=client
	Set To Dictionary	${payload}	enable_peer_management=${TRUE}
	Set To Dictionary	${payload}	coordinator_address=${EMPTY}
	Set To Dictionary	${payload}	type=master
	Set To Dictionary	${payload}	polling_rate=30
	Set To Dictionary	${payload}	psk=key
	Set To Dictionary	${payload}	renew_time=1

	API::Put::Cloud::Settings	${payload}

Get Cloud Settings After Update
	${settings}=	API::Get::Cloud::Settings
	#Length Should Be	${settings}	16
	Should Be Equal As Strings	${settings["auto_psk"]}	nodegrid-key
	Should Be Equal As Strings	${settings["cloud_name"]}	nodegrid-cloud
	Should Be Equal As Strings	${settings["lease_time"]}	7
	Should Be Equal As Strings	${settings["lps_type"]}	client
	Should Be Equal As Strings	${settings["coordinator_address"]}	localhost
	Should Be Equal As Strings	${settings["type"]}	master
	Should Be Equal As Strings	${settings["polling_rate"]}	30
	Should Be Equal As Strings	${settings["psk"]}	key
	Should Be Equal As Strings	${settings["renew_time"]}	1
	Should Be True	${settings["allow_enrollment"]}
	Should Be True	${settings["enable_clustering"]}
	Should Be True	${settings["enable_cloud"]}
	Should Be True	${settings["enable_peer_management"]}
	Should Not Be True	${settings["auto_enroll"]}
	Should Not Be True	${settings["enable_license_pool"]}

Get Cloud Peers
	${peers}=	API::Get::Cloud::Peers
	:FOR	${peer}	IN	@{peers}
	\	Dictionary Should Contain Key	${peer}	address
	\	Dictionary Should Contain Key	${peer}	id
	\	Dictionary Should Contain Key	${peer}	name
	\	Dictionary Should Contain Key	${peer}	peer_status
	\	Dictionary Should Contain Key	${peer}	status
	\	Dictionary Should Contain Key	${peer}	type
	\	Length Should Be	${peer}	6


Get Cloud Management
	${mngts}=	API::Get::Cloud::Management
	:FOR	${mngt}	IN	@{mngts}
	\	Dictionary Should Contain Key	${mngt}	address
	\	Dictionary Should Contain Key	${mngt}	id
	\	Dictionary Should Contain Key	${mngt}	management_status
	\	Dictionary Should Contain Key	${mngt}	name
	\	Dictionary Should Contain Key	${mngt}	status
	\	Dictionary Should Contain Key	${mngt}	sw_version
	\	Length Should Be	${mngt}	6

*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin

Suite Teardown
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	allow_enrollment=${FALSE}
	Set To Dictionary	${payload}	auto_enroll=${TRUE}
	Set To Dictionary	${payload}	auto_psk=nodegrid-key
	Set To Dictionary	${payload}	cloud_name=nodegrid
	Set To Dictionary	${payload}	enable_clustering=${FALSE}
	Set To Dictionary	${payload}	enable_cloud=${TRUE}
	Set To Dictionary	${payload}	lease_time=7
	Set To Dictionary	${payload}	enable_license_pool=${TRUE}
	Set To Dictionary	${payload}	lps_type=client
	Set To Dictionary	${payload}	enable_peer_management=${FALSE}
	Set To Dictionary	${payload}	coordinator_address=${EMPTY}
	Set To Dictionary	${payload}	type=master
	Set To Dictionary	${payload}	polling_rate=30
	Set To Dictionary	${payload}	psk=key
	Set To Dictionary	${payload}	preSharedKey2=${EMPTY}
	Set To Dictionary	${payload}	renew_time=1
	API::Put::Cloud::Settings	${payload}
	Set To Dictionary	${payload}	enable_cloud=${FALSE}
	API::Put::Cloud::Settings	${payload}

	API::Delete::Session

