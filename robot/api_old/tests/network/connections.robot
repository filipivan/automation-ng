*** Settings ***
Documentation	Test /api/<version>/network/connections endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Add Network Connection
	${payload}=	Get Connection Payload
	API::Post::Network::Connections	${payload}

	${connections}=	API::Get::Network::Connections
	Check Valid Network Connections	${connections}
	${connections_count}=	Get Length	${connections}
	Should Be Equal As Integers	${connections_count}	${ORIGINAL_CONNECTIONS_COUNT+1}

Get Network Connections
	${connections}=	API::Get::Network::Connections
	Check Valid Network Connections	${connections}

Get Network Connection
	${connection}=	API::Get::Network::Connections::dummy
	Should Be Equal As Strings	${connection["name"]}	dummy
	Should Be Equal As Strings	${connection["type"]}	VLAN
	Should Be Equal As Strings	${connection["ethernet_interface"]}	eth1
	Should Not Be True	${connection["connect_automatically"]}
	Should Be Equal As Strings	${connection["ipv4_mode"]}	auto
	Should Be Equal As Strings	${connection["ipv4_dns_server"]}	8.8.8.8
	Should Be Equal As Strings	${connection["ipv4_dns_search"]}	8.8.8.8
	Should Be Equal As Strings	${connection["ipv6_mode"]}	dhcp
	Should Be Equal As Strings	${connection["ipv6_dns_server"]}	::
	Should Be Equal As Strings	${connection["ipv6_dns_search"]}	::
	Should Be Equal As Strings	${connection["vlan_id"]}	7

Update Network Connection
	${payload}=	Get Connection Payload
	Set To Dictionary	${payload}	ipv6_mode=ignore
	API::Put::Network::Connections	dummy	${payload}

	${connection}=	API::Get::Network::Connections::dummy
	Should Be Equal As Strings	${connection["ipv6_mode"]}	ignore

Delete Network Connection
	API::Delete::Network::Connections	dummy
	${connections}=	API::Get::Network::Connections
	Check Valid Network Connections	${connections}
	${connections_count}=	Get Length	${connections}
	Should Be Equal As Integers	${connections_count}	${ORIGINAL_CONNECTIONS_COUNT}

Down Connection
	${connections}=	API::Get::Network::Connections
	:FOR	${con}	IN	@{connections}
	\	Run Keyword If	'${con["id"]}' == 'ETH1' and 'connected' == '${con["status"]}'	API::Put::Network::Connections::Down	ETH1
	Wait Until Connection ETH1 Is Down

Up Connection
	${connections}=	API::Get::Network::Connections
	:FOR	${con}	IN	@{connections}
	\	Run Keyword If	'${con["id"]}' == 'ETH1' and 'not active' == '${con["status"]}'	API::Put::Network::Connections::Up	ETH1
	Wait Until Connection ETH1 Is Up


*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin
	Reset Values

	${connections}=	API::Get::Network::Connections
	${ORIGINAL_CONNECTIONS_COUNT}=	Get Length	${connections}
	Set Suite Variable	${ORIGINAL_CONNECTIONS_COUNT}

Suite Teardown
	Reset Values
	API::Delete::Session

Check Valid Network Connections
	[Arguments]	${CONNECTIONS}
	:FOR	${con}	IN	@{CONNECTIONS}
	\	Dictionary Should Contain Key	${con}	interface
	\	Dictionary Should Contain Key	${con}	name
	\	Dictionary Should Contain Key	${con}	carrier_state
	\	Dictionary Should Contain Key	${con}	status
	\	Dictionary Should Contain Key	${con}	type
	\	Dictionary Should Contain Key	${con}	id
	\	Dictionary Should Contain Key	${con}	ipv4_address
	\	Dictionary Should Contain Key	${con}	ipv6_address
	\	Dictionary Should Contain Key	${con}	mac_address

Get Connection Payload
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	name=dummy
	Set To Dictionary	${payload}	type=vlan
	Set To Dictionary	${payload}	ethernet_interface=eth1
	Set To Dictionary	${payload}	connect_automatically=${FALSE}
	Set To Dictionary	${payload}	primary_connection=${FALSE}
	Set To Dictionary	${payload}	ipv4_mode=auto
	Set To Dictionary	${payload}	ipv4_dns_server=8.8.8.8
	Set To Dictionary	${payload}	ipv4_dns_search=8.8.8.8
	Set To Dictionary	${payload}	ipv6_mode=dhcp
	Set To Dictionary	${payload}	ipv6_dns_server=::
	Set To Dictionary	${payload}	ipv6_dns_search=::
	Set To Dictionary	${payload}	vlan_id=7

	[return]	${payload}

Reset Values
	API::Delete::Network::Connections	dummy

Wait Until Connection ${CONNECTION} Is Down
	Wait Until Keyword Succeeds	3 times	10 seconds	Connection ${CONNECTION} Should Be Down

Connection ${CONNECTION} Should Be Down
	${connections}=	API::Get::Network::Connections
	:FOR	${con}	IN	@{connections}
	\	Run Keyword If	'${con["id"]}' == '${CONNECTION}'	Should Be Equal As Strings	${con["status"]}	not active

Wait Until Connection ${CONNECTION} Is Up
	Wait Until Keyword Succeeds	3 times	10 seconds	Connection ${CONNECTION} Should Be Up

Connection ${CONNECTION} Should Be Up
	${connections}=	API::Get::Network::Connections
	:FOR	${con}	IN	@{connections}
	\	Run Keyword If	'${con["id"]}' == '${CONNECTION}'	Should Be Equal As Strings	${con["status"]}	connected
