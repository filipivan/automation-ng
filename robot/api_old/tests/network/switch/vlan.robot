*** Settings ***
Documentation	Test /api/<version>/network/switch... endpoints
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../../init.robot
Force Tags		API	${VERSION}
Default Tags	EXCLUDEIN3_2	EXCLUDEIN4_0

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NAME}	55
${NAME2}	56

*** Test Cases ***
Get Network::Switch::Vlan
	[Setup]	API::Add Network::Switch::Vlan	${NAME}
	${vlan_list}=	API::Get::Network::Switch::Vlan
	${vlan_list}=	Set Variable	${vlan_list.json()}
	:FOR	${vlan_dict}	IN	@{vlan_list}
	\	SUITE:Should Contain Fields Network::Switch::Vlan	${vlan_dict}
	[Teardown]	API::Delete Network::Switch::Vlan	${NAME}

Add One Network::Switch::Vlan
	[Setup]	API::Delete If Exists Network::Switch::Vlans	${NAME}
	API::Add Network::Switch::Vlan	${NAME}
	API::Aux::Should Exist Network::Switch::Vlan	${NAME}
	[Teardown]	API::Delete Network::Switch::Vlan	${NAME}

Add Two Network::Switch::Vlan
	[Setup]	API::Delete If Exists Network::Switch::Vlans	${NAME}	${NAME2}
	API::Add Network::Switch::Vlan	${NAME}
	API::Add Network::Switch::Vlan	${NAME2}
	API::Aux::Should Exist Network::Switch::Vlans	${NAME}	${NAME2}
	[Teardown]	API::Delete Network::Switch::Vlans	${NAME}	${NAME2}

Add Duplicate Network::Switch::Vlan
	[Setup]	API::Delete If Exists Network::Switch::Vlans	${NAME}
	API::Add Network::Switch::Vlan	${NAME}
	API::Aux::Should Exist Network::Switch::Vlan	${NAME}
	${response}=	API::Add Network::Switch::Vlan	${NAME}	RAW_MODE=Raw
	${response}=	Set Variable	${response.json()}
#	Comparing response of request with expected message created
	API::Msg::Compare Simple Message	${response}	vlan	Entry already exists.
	[Teardown]	API::Delete Network::Switch::Vlans	${NAME}

#Add Invalid Network::Switch::Vlan | name, untagged_ports, tagged_ports | EMPTY
#	[Tags]	NON-CRITICAL	BUG_API_9
#	Understand name and phone_number values as EMPTY by Default
#	${response}=	API::Add Network::Switch::Vlan	RAW_MODE=Raw
#	${response}=	Set Variable	${response.json()}
#	Comparing response of request with expected message created
#	${fields}=	Create List	name
#	${values}=	Create List	This field is empty or it contains invalid character	Field must not be empty.
#	API::Msg::Compare Complex Message	${response}	${fields}	${values}

Add Invalid Network::Switch::Vlan | vlan | EMPTY
	${response}=	API::Add Network::Switch::Vlan	${EMPTY}	Raw
	${response}=	Set Variable	${response.json()}
#	Comparing response of request with expected message created
	API::Msg::Compare Simple Message	${response}	vlan	Field must not be empty.	#Validation error.

Add Invalid Network::Switch::Vlan | untagged_ports | EMPTY
	${response}=	API::Add Network::Switch::Vlan	${NAME}	${EMPTY}	RAW_MODE=Raw
	${response}=	Set Variable	${response.json()}
#	Comparing response of request with expected message created
	API::Msg::Compare Simple Message	${response}	untagged_ports	Validation error.
	[Teardown]	Run Keyword If Test Failed	API::Delete If Exists Network::Switch::Vlans	${NAME}

Add Invalid Network::Switch::Vlan | tagged_ports | EMPTY
	${response}=	API::Add Network::Switch::Vlan	${NAME}	${EMPTY}	RAW_MODE=Raw
	${response}=	Set Variable	${response.json()}
#	Comparing response of request with expected message created
	API::Msg::Compare Simple Message	${response}	tagged_ports	Validation error.
	[Teardown]	Run Keyword If Test Failed	API::Delete If Exists Network::Switch::Vlans	${NAME}

Add Invalid Network::Switch::Vlan| name | EXCEEDED
#	[Tags]	NON-CRITICAL	BUG_API_9
	${response}=	API::Add Network::Switch::Vlan	${EXCEEDED}	Raw
	${response}=	Set Variable	${response.json()}
#	Comparing response of request with expected message created
	API::Msg::Compare Simple Message	${response}	name	Validation error.	#Validation error.
	[Teardown]	Run Keyword If Test Failed	API::Delete If Exists Network::Switch::Vlans	${EXCEEDED}

Delete One Network::Switch::Vlan
	[Setup]	API::Add Network::Switch::Vlan	${NAME}
	API::Delete Network::Switch::Vlan	${NAME}
	API::Aux::Should Not Exist Network::Switch::Vlan	${NAME}
	[Teardown]	Run Keyword If Test Failed	API::Delete If Exists Network::Switch::Vlans	${NAME}

Delete Two Network::Switch::Vlan
	[Setup]	SUITE:Add Two Network::Switch::Vlan
	API::Delete Network::Switch::Vlans	${NAME}	${NAME2}
	API::Aux::Should Not Exist Network::Switch::Vlans	${NAME}	${NAME2}
	[Teardown]	Run Keyword If Test Failed	API::Delete If Exists Network::Switch::Vlans	${NAME}	${NAME2}

#	@todo tests of funcionality
#	@todo tests with wrong fields

Update Network::Switch::Vlan
	[Setup]	API::Add Network::Switch::Vlan	${NAME}
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	phone_number=${PHONE_NUMBER2}
	API::Put::Network::Switch::Vlan::Name	${NAME}	${payload}

	${vlan_list}=	API::Get::Network::Switch::Vlan
	${vlan_list}=	Set Variable	${vlan_list.json()}
	Should Not Be Empty	${vlan_list}
	${expected_dict}=	Create Dictionary
	Set To Dictionary	${expected_dict}	id=${NAME}
	Set To Dictionary	${expected_dict}	name=${NAME}
	Set To Dictionary	${expected_dict}	phone_number=${PHONE_NUMBER2}
	List Should Contain Value	${vlan_list}	${expected_dict}

*** Keywords ***
SUITE:Setup
	API::Post::Session	admin	ZPE.Cl0ud.2019
	API::Delete If Exists Network::Switch::Vlans	${NAME}	${NAME2}

SUITE:Teardown
	API::Delete If Exists Network::Switch::Vlans	${NAME}	${NAME2}
	API::Delete::Session

SUITE:Should Be Equal
	[Arguments]	${dict}	${field}	${value}
	${get_from_field}=	Get From Dictionary	${dict}	${field}
	Should Be Equal	${get_from_field}	${value}

SUITE:Add Two Network::Switch::Vlan
	API::Add Network::Switch::Vlan	${NAME}
	API::Add Network::Switch::Vlan	${NAME2}

SUITE:Should Contain Fields Network::Switch::Vlan
	[Arguments]	${vlan_dict}
	${vlan_fields}=	Create List	id	vlan	untagged_ports	tagged_ports
	:FOR	${element}	IN	@{vlan_fields}
	\	Dictionary Should Contain Key	${vlan_dict}	${element}
	Length Should Be	${vlan_dict}	4
