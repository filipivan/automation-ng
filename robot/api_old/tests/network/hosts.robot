*** Settings ***
Documentation	Test /api/<version>/network/hosts endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Add Host
	${payload}=	Get Payload
	API::Post::Network::Hosts	${payload}
	${hosts}=	API::Get::Network::Hosts
	${hosts_count}=	Get Length	${hosts}
	Should Be Equal As Integers	${hosts_count}	${ORIGINAL_HOSTS_COUNT+1}
	${last_host}=	Get From List	${hosts}	-1
	Set Suite Variable	${LAST_ID}	${last_host["id"]}

Get Hosts
	${hosts}=	API::Get::Network::Hosts
	:FOR	${host}	IN	@{hosts}
	\	Dictionary Should Contain Key	${host}	alias
	\	Dictionary Should Contain Key	${host}	ip_address
	\	Dictionary Should Contain Key	${host}	id
	\	Dictionary Should Contain Key	${host}	hostname

Get Host
	${host}=	API::Get::Network::Hosts::${NAME}	${LAST_ID}
	Should Be Equal As Strings	${host["ip_address"]}	192.168.15.20
	Should Be Equal As Strings	${host["hostname"]}	dummylocalhost
	Should Be Equal As Strings	${host["alias"]}	dummyalias

Update Host
	${payload}=	Get Payload
	Set To Dictionary	${payload}	hostname=dummylocalhost2
	Set To Dictionary	${payload}	alias=dummyalias2
	API::Put::Network::Hosts	${LAST_ID}	${payload}
	${host}=	API::Get::Network::Hosts::${NAME}	${LAST_ID}
	Should Be Equal As Strings	${host["ip_address"]}	192.168.15.20
	Should Be Equal As Strings	${host["hostname"]}	dummylocalhost2
	Should Be Equal As Strings	${host["alias"]}	dummyalias2

Delete Host
	API::Delete::Network::Hosts	${LAST_ID}
	${hosts}=	API::Get::Network::Hosts
	${hosts_count}=	Get Length	${hosts}
	Should Be Equal As Integers	${hosts_count}	${ORIGINAL_HOSTS_COUNT}

*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin

	${hosts}=	API::Get::Network::Hosts
	${ORIGINAL_HOSTS_COUNT}=	Get Length	${hosts}
	Set Suite Variable	${ORIGINAL_HOSTS_COUNT}

Suite Teardown
	API::Delete::Network::Hosts	${LAST_ID}
	API::Delete::Session

Get Payload
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	ip_address=192.168.15.20
	Set To Dictionary	${payload}	hostname=dummylocalhost
	Set To Dictionary	${payload}	alias=dummyalias
	[return]	${payload}
