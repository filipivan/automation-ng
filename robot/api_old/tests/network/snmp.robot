*** Settings ***
Documentation	Test /api/<version>/network/snmp endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Add SNMP entry
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	security_level=noauth
	Set To Dictionary	${payload}	authentication_password=${EMPTY}
	Set To Dictionary	${payload}	privacy_algorithm=MD5
	Set To Dictionary	${payload}	community=comm
	Set To Dictionary	${payload}	oid=${EMPTY}
	Set To Dictionary	${payload}	access_type=read_only
	Set To Dictionary	${payload}	privacy_password=${EMPTY}
	Set To Dictionary	${payload}	privacy_algorithm=DES
	Set To Dictionary	${payload}	snmp_for_ipv6=${FALSE}
	Set To Dictionary	${payload}	version=community
	Set To Dictionary	${payload}	source=source
	Set To Dictionary	${payload}	user_name=${EMPTY}
	API::Post::Network::SNMP	${payload}

	${entries}=	API::Get::Network::SNMP
	Length Should Be	${entries}	${ORIGINAL_ENTRY_COUNT+1}

Get SNMP entries
	${snmps}=	API::Get::Network::SNMP
	:FOR	${snmp}	IN	@{snmps}
	\	Dictionary Should Contain Key	${snmp}	id
	\	Dictionary Should Contain Key	${snmp}	name
	\	Dictionary Should Contain Key	${snmp}	oid
	\	Dictionary Should Contain Key	${snmp}	access_type
	\	Dictionary Should Contain Key	${snmp}	source
	\	Dictionary Should Contain Key	${snmp}	version
	\	Length Should Be	${snmp}	6

Get SNMP Entry
	${entry}=	API::Get::Network::SNMP::${NAME}	${ENTRY_ID}
	Should Be Equal As Strings	${entry["community"]}	comm
	Should Be Equal As Strings	${entry["oid"]}	${EMPTY}
	Should Be Equal As Strings	${entry["access_type"]}	read_only
	Should Be Equal As Strings	${entry["version"]}	Version v1/v2
	#Should Be Equal As Strings	${entry["source"]}	source

Update SNMP entry
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	oid=${EMPTY}
	Set To Dictionary	${payload}	access_type=read_write
	Set To Dictionary	${payload}	version=v1/v2
	Set To Dictionary	${payload}	community=comm
	Set To Dictionary	${payload}	source=source
	Set To Dictionary	${payload}	old_source=source
	API::Put::Network::SNMP	${ENTRY_ID}	${payload}

Get SNMP Entry After Update
	${entry}=	API::Get::Network::SNMP::${NAME}	${ENTRY_ID}
	Should Be Equal As Strings	${entry["community"]}	comm
	Should Be Equal As Strings	${entry["oid"]}	${EMPTY}
	Should Be Equal As Strings	${entry["access_type"]}	read_write
	Should Be Equal As Strings	${entry["version"]}	Version v1/v2
	Should Be Equal As Strings	${entry["source"]}	source

Delete SNMP Entry
	API::Delete::Network::SNMP	${ENTRY_ID}
	Wait until SNMP entries should be ${ORIGINAL_ENTRY_COUNT}

*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin
	#this is should be consistent to the entry added during the test cases
	Set Suite Variable	${ENTRY_ID}	community|comm~source
	API::Delete::Network::SNMP	${ENTRY_ID}
	${snmps}=	API::Get::Network::SNMP

	${count}=	Get Length	${snmps}
	Set Suite Variable	${ORIGINAL_ENTRY_COUNT}	${count}

Suite Teardown
	API::Delete::Network::SNMP	${ENTRY_ID}
	API::Delete::Session


Wait until SNMP entries should be ${COUNT}
	Wait Until Keyword Succeeds	3 times	10 seconds	SNMP entries should be ${COUNT}

SNMP entries should be ${COUNT}
	${entries}=	API::Get::Network::SNMP
	Length Should Be	${entries}	${COUNT}
