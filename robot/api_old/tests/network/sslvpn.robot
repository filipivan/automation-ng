*** Settings ***
Documentation	Test /api/<version>/network/sslvpn endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***



Add SSL VPN Client
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	name=dummy
	Set To Dictionary	${payload}	network_connection=ETH1
	Set To Dictionary	${payload}	gateway_ip_address=192.168.15.1
	Set To Dictionary	${payload}	gateway_tcp_port=1195
	Set To Dictionary	${payload}	connection_protocol=tcp
	Set To Dictionary	${payload}	tunnel_mtu=1501
	Set To Dictionary	${payload}	use_lzo_data_compress_algorithm=${TRUE}
	Set To Dictionary	${payload}	hmac/message_digest_alg=RSA-SHA512
	Set To Dictionary	${payload}	cipher_alg=CAMELLIA-128-CFB
	Set To Dictionary	${payload}	authentication_method=static-key
	Set To Dictionary	${payload}	local_endpoint=${TESTSYSTEMIP}
	Set To Dictionary	${payload}	remote_endpoint=${CLIENTIP}
	API::Post::Network::SSL VPN::Clients	${payload}


	${clients}=	API::Get::Network::SSL VPN::Clients
	${clients_count}=	Get Length	${clients}
	Should Be Equal As Integers	${clients_count}	${ORIGINAL_CLIENTS_COUNT+1}
	${last_client}=	Get From List	${clients}	-1
	Set Suite Variable	${LAST_ID}	${last_client["id"]}

Get SSL VPN Clients
	${clients}=	API::Get::Network::SSL VPN::Clients
	:FOR	${client}	IN	@{clients}
	\	Dictionary Should Contain Key	${client}	connection
	\	Dictionary Should Contain Key	${client}	id
	\	Dictionary Should Contain Key	${client}	ipv4_tunnel_net
	\	Dictionary Should Contain Key	${client}	ipv6_tunnel_net
	\	Dictionary Should Contain Key	${client}	vpn_gateway
	\	Dictionary Should Contain Key	${client}	name
	\	Dictionary Should Contain Key	${client}	status

Get SSL VPN Client
	${client}=	API::Get::Network::SSL VPN::Clients::${NAME}	${LAST_ID}

	Should Be Equal As Strings	${client["cipher_alg"]}	CAMELLIA-128-CFB
	Should Be Equal As Strings	${client["hmac/message_digest_alg"]}	RSA-SHA512
	Should Be Equal As Strings	${client["local_endpoint"]}	${TESTSYSTEMIP}
	Should Be Equal As Strings	${client["password"]}	${EMPTY}
	Should Be Equal As Strings	${client["remote_endpoint"]}	${CLIENTIP}
	Should Be Equal As Strings	${client["authentication_method"]}	static-key
	Should Be Equal As Strings	${client["username"]}	${EMPTY}
	Should Be Equal As Strings	${client["network_connection"]}	ETH1
	Should Be Equal As Strings	${client["gateway_ip_address"]}	192.168.15.1
	Should Be Equal As Strings	${client["gateway_tcp_port"]}	1195
	Should Be Equal As Strings	${client["tunnel_mtu"]}	1501
	Should Be Equal As Strings	${client["name"]}	dummy
	Should Be Equal As Strings	${client["connection_protocol"]}	tcp
	Should Be True	${client["use_lzo_data_compress_algorithm"]}

Update SSL VPN Client
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	gateway_tcp_port=1194
	Set To Dictionary	${payload}	tunnel_mtu=1500
	API::Put::Network::SSL VPN::Clients	${LAST_ID}	${payload}

Get SSL VPN Client After Updated
	${client}=	API::Get::Network::SSL VPN::Clients::${NAME}	${LAST_ID}
	Should Be Equal As Strings	${client["gateway_tcp_port"]}	1194
	Should Be Equal As Strings	${client["tunnel_mtu"]}	1500

Start SSL VPN Client
	API::Put::Network::SSL VPN::Clients::${NAME}::Start	${LAST_ID}

Stop SSL VPN Client
	API::Put::Network::SSL VPN::Clients::${NAME}::Stop	${LAST_ID}

Delete SSL VPN Client
	API::Delete::Network::SSL VPN::Clients	${LAST_ID}
	${clients}=	API::Get::Network::SSL VPN::Clients
	${clients_count}=	Get Length	${clients}
	Should Be Equal As Integers	${clients_count}	${ORIGINAL_CLIENTS_COUNT}

Get SSL VPN Server
	${server}=	API::Get::Network::SSL VPN::Server
	Dictionary Should Contain Key	${server}	use_lzo_data_compress_algorithm
	Dictionary Should Contain Key	${server}	cipher
	Dictionary Should Contain Key	${server}	hmac/message_digest
	Dictionary Should Contain Key	${server}	authentication_method
	Dictionary Should Contain Key	${server}	ip_addr
	Dictionary Should Contain Key	${server}	listen_ip_address
	Dictionary Should Contain Key	${server}	listen_port_number
	Dictionary Should Contain Key	${server}	local_endpoint
	Dictionary Should Contain Key	${server}	local_ipv6_endpoint
	Dictionary Should Contain Key	${server}	number_of_concurrent_tunnels
	Dictionary Should Contain Key	${server}	redirect_gateway
	Dictionary Should Contain Key	${server}	remote_endpoint
	Dictionary Should Contain Key	${server}	remote_ipv6_endpoint
	Dictionary Should Contain Key	${server}	status
	Dictionary Should Contain Key	${server}	min_tls_version
	Dictionary Should Contain Key	${server}	tunnel_mtu
	Dictionary Should Contain Key	${server}	ipv4_tunnel
	Dictionary Should Contain Key	${server}	ipv6_tunnel
	Dictionary Should Contain Key	${server}	protocol

	Should Not Be True	${server["use_lzo_data_compress_algorithm"]}
	Should Be Equal As Strings	${server["cipher"]}	BF-CBC
	Should Be Equal As Strings	${server["hmac/message_digest"]}	SHA1
	Should Be Equal As Strings	${server["authentication_method"]}	tls
	Should Be Equal As Strings	${server["ip_addr"]}	net
	Should Be Equal As Strings	${server["listen_ip_address"]}	${EMPTY}
	Should Be Equal As Strings	${server["listen_port_number"]}	1194
	Should Be Equal As Strings	${server["local_endpoint"]}	${EMPTY}
	Should Be Equal As Strings	${server["number_of_concurrent_tunnels"]}	256
	Should Not Be True	${server["redirect_gateway"]}
	Should Be Equal As Strings	${server["remote_endpoint"]}	${EMPTY}
	Should Be Equal As Strings	${server["status"]}	no
	Should Be Equal As Strings	${server["min_tls_version"]}	none
	Should Be Equal As Strings	${server["tunnel_mtu"]}	1500
	Should Be Equal As Strings	${server["protocol"]}	udp

Update SSL VPN Server
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	use_lzo_data_compress_algorithm=${TRUE}
	Set To Dictionary	${payload}	cipher=SEED-OFB
	Set To Dictionary	${payload}	hmac/message_digest=SHA512
	Set To Dictionary	${payload}	authentication_method=static-key
	Set To Dictionary	${payload}	ip_addr=p2p
	Set To Dictionary	${payload}	listen_ip_address=${TESTSYSTEMIP}
	Set To Dictionary	${payload}	listen_port_number=1195
	Set To Dictionary	${payload}	local_endpoint=${TESTSYSTEMIP}
	Set To Dictionary	${payload}	number_of_concurrent_tunnels=128
	Set To Dictionary	${payload}	redirect_gateway=${TRUE}
	Set To Dictionary	${payload}	remote_endpoint=${CLIENTIP}
	Set To Dictionary	${payload}	status=yes
	Set To Dictionary	${payload}	min_tls_version=1.2
	Set To Dictionary	${payload}	tunnel_mtu=1501
	Set To Dictionary	${payload}	protocol=tcp-server
	API::Put::Network::SSL VPN::Server	${payload}

Get SSL VPN Server After Update
	${server}=	API::Get::Network::SSL VPN::Server
	Should Be True	${server["use_lzo_data_compress_algorithm"]}
	Should Be Equal As Strings	${server["cipher"]}	SEED-OFB
	Should Be Equal As Strings	${server["hmac/message_digest"]}	SHA512
	Should Be Equal As Strings	${server["authentication_method"]}	static-key
	Should Be Equal As Strings	${server["ip_addr"]}	p2p
	Should Be Equal As Strings	${server["listen_ip_address"]}	${TESTSYSTEMIP}
	Should Be Equal As Strings	${server["listen_port_number"]}	1195
	Should Be Equal As Strings	${server["local_endpoint"]}	${TESTSYSTEMIP}
	Should Be Equal As Strings	${server["number_of_concurrent_tunnels"]}	128
	Should Be True	${server["redirect_gateway"]}
	Should Be Equal As Strings	${server["remote_endpoint"]}	${CLIENTIP}
	Should Be Equal As Strings	${server["status"]}	yes
	Should Be Equal As Strings	${server["min_tls_version"]}	1.2
	Should Be Equal As Strings	${server["tunnel_mtu"]}	1501
	Should Be Equal As Strings	${server["protocol"]}	tcp-server

Get SSL VPN Server Status
	${status}=	API::Get::Network::SSL VPN::Server Status
	:FOR	${stat}	IN	@{status}
	\	Dictionary Should Contain Key	${stat}	vpnConn
	\	Dictionary Should Contain Key	${stat}	realAddr
	\	Dictionary Should Contain Key	${stat}	rxbytes
	\	Dictionary Should Contain Key	${stat}	txbytes
	\	Dictionary Should Contain Key	${stat}	since
	\	Dictionary Should Contain Key	${stat}	virtualAddr

*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin
	Reset data

	${clients}=	API::Get::Network::SSL VPN::Clients
	${ORIGINAL_CLIENTS_COUNT}=	Get Length	${clients}
	Set Suite Variable	${ORIGINAL_CLIENTS_COUNT}

Suite Teardown
	Reset data
	API::Delete::Network::SSL VPN::Clients	${LAST_ID}
	API::Delete::Session

Reset data
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	use_lzo_data_compress_algorithm=${FALSE}
	Set To Dictionary	${payload}	cipher=BF-CBC
	Set To Dictionary	${payload}	hmac/message_digest=SHA1
	Set To Dictionary	${payload}	authentication_method=tls
	Set To Dictionary	${payload}	ip_addr=p2p
	Set To Dictionary	${payload}	listen_ip_address=${EMPTY}
	Set To Dictionary	${payload}	listen_port_number=1194
	Set To Dictionary	${payload}	local_endpoint=${EMPTY}
	Set To Dictionary	${payload}	number_of_concurrent_tunnels=256
	Set To Dictionary	${payload}	redirect_gateway=${FALSE}
	Set To Dictionary	${payload}	remote_endpoint=${EMPTY}
	Set To Dictionary	${payload}	status=no
	Set To Dictionary	${payload}	min_tls_version=none
	Set To Dictionary	${payload}	tunnel_mtu=1500
	Set To Dictionary	${payload}	protocol=udp
	API::Put::Network::SSL VPN::Server	${payload}
	Set To Dictionary	${payload}	ip_addr=net
	API::Put::Network::SSL VPN::Server	${payload}
