*** Settings ***
Documentation	Test /api/<version>/network/settings endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Get Network Settings
	${settings}=	API::Get::Network::Settings
	${keys}=	Get Dictionary Keys	${settings}
	${len}=	Get Length	${keys}
	Should Be Equal As Integers	${len}	22
	Dictionary Should Contain Key	${settings}	trigger_ip_address
	Dictionary Should Contain Key	${settings}	enable_dynamic_dns
	Dictionary Should Contain Key	${settings}	failover_hostname
	Dictionary Should Contain Key	${settings}	key_information_for_dnssec
	Dictionary Should Contain Key	${settings}	algorithm
	Dictionary Should Contain Key	${settings}	key_size
	Dictionary Should Contain Key	${settings}	user_name
	Dictionary Should Contain Key	${settings}	ddns_server_name
	Dictionary Should Contain Key	${settings}	ddns_server_tcp_port
	Dictionary Should Contain Key	${settings}	zone
	Dictionary Should Contain Key	${settings}	dns_server
	Dictionary Should Contain Key	${settings}	dns_search
	Dictionary Should Contain Key	${settings}	domain_name
	Dictionary Should Contain Key	${settings}	enable_network_failover
	Dictionary Should Contain Key	${settings}	hostname
	Dictionary Should Contain Key	${settings}	enable_ipv4_ip_forward
	Dictionary Should Contain Key	${settings}	enable_ipv6_ip_forward
	Dictionary Should Contain Key	${settings}	ipv4_loopback
	Dictionary Should Contain Key	${settings}	ipv6_loopback
	Dictionary Should Contain Key	${settings}	primary_connection
	Dictionary Should Contain Key	${settings}	secondary_connection
	Dictionary Should Contain Key	${settings}	trigger
	Should Be Equal As Strings	${settings["trigger_ip_address"]}	${EMPTY}
	Should Be Equal As Strings	${settings["enable_dynamic_dns"]}	${FALSE}
	Should Be Equal As Strings	${settings["failover_hostname"]}	${EMPTY}
	Should Be Equal As Strings	${settings["algorithm"]}	HMAC-MD5
	Should Be Equal As Strings	${settings["key_size"]}	512
	Should Be Equal As Strings	${settings["user_name"]}	${EMPTY}
	Should Be Equal As Strings	${settings["ddns_server_name"]}	${EMPTY}
	Should Be Equal As Strings	${settings["ddns_server_tcp_port"]}	53
	Should Be Equal As Strings	${settings["zone"]}	${EMPTY}
	Should Be Equal As Strings	${settings["domain_name"]}	localdomain
	Should Be Equal As Strings	${settings["enable_network_failover"]}	${FALSE}
	Should Be Equal As Strings	${settings["hostname"]}	nodegrid
	Should Be Equal As Strings	${settings["enable_ipv4_ip_forward"]}	${FALSE}
	Should Be Equal As Strings	${settings["enable_ipv6_ip_forward"]}	${FALSE}
	Should Be Equal As Strings	${settings["ipv4_loopback"]}	${EMPTY}
	Should Be Equal As Strings	${settings["ipv6_loopback"]}	${EMPTY}
	Should Be Equal As Strings	${settings["primary_connection"]}	ETH0
	Should Be Equal As Strings	${settings["secondary_connection"]}	ETH1
	Should Be Equal As Strings	${settings["trigger"]}	ipv4gw

Update Network Settings
	${payload}=	Get Update Network Payload
	API::Put::Network::Settings	${payload}
	${settings}=	API::Get::Network::Settings
	${keys}=	Get Dictionary Keys	${settings}
	Should Be Equal As Strings	${settings["trigger_ip_address"]}	127.0.0.1
	Should Be Equal As Strings	${settings["enable_dynamic_dns"]}	${TRUE}
	Should Be Equal As Strings	${settings["failover_hostname"]}	failover
	Should Be Equal As Strings	${settings["algorithm"]}	HMAC-SHA512
	Should Be Equal As Strings	${settings["key_size"]}	1024
	Should Be Equal As Strings	${settings["user_name"]}	user
	Should Be Equal As Strings	${settings["ddns_server_name"]}	8.8.8.8
	Should Be Equal As Strings	${settings["ddns_server_tcp_port"]}	54
	Should Be Equal As Strings	${settings["zone"]}	ZONE
	Should Be Equal As Strings	${settings["domain_name"]}	apidomain
	Should Be Equal As Strings	${settings["enable_network_failover"]}	${TRUE}
	Should Be Equal As Strings	${settings["hostname"]}	apiserver
	Should Be Equal As Strings	${settings["enable_ipv4_ip_forward"]}	${TRUE}
	Should Be Equal As Strings	${settings["enable_ipv6_ip_forward"]}	${TRUE}
	Should Be Equal As Strings	${settings["ipv4_loopback"]}	127.0.0.1
	Should Be Equal As Strings	${settings["ipv6_loopback"]}	::
	Should Be Equal As Strings	${settings["primary_connection"]}	ETH1
	Should Be Equal As Strings	${settings["secondary_connection"]}	ETH0
	Should Be Equal As Strings	${settings["trigger"]}	ipaddress
	${len}=	Get Length	${keys}
	Should Be Equal As Integers	${len}	22

*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin
	Reset Default Values

Suite Teardown
	Reset Default Values
	API::Delete::Session

Reset Default Values
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	trigger_ip_address=${EMPTY}
	Set To Dictionary	${payload}	enable_dynamic_dns=${TRUE}
	Set To Dictionary	${payload}	failover_hostname=${EMPTY}
	Set To Dictionary	${payload}	algorithm=HMAC-MD5
	Set To Dictionary	${payload}	key_size=512
	Set To Dictionary	${payload}	user_name=${EMPTY}
	Set To Dictionary	${payload}	ddns_server_name=${EMPTY}
	Set To Dictionary	${payload}	ddns_server_tcp_port=53
	Set To Dictionary	${payload}	zone=${EMPTY}
	Set To Dictionary	${payload}	domain_name=localdomain
	Set To Dictionary	${payload}	enable_network_failover=${TRUE}
	Set To Dictionary	${payload}	hostname=nodegrid
	Set To Dictionary	${payload}	enable_ipv4_ip_forward=${FALSE}
	Set To Dictionary	${payload}	enable_ipv6_ip_forward=${FALSE}
	Set To Dictionary	${payload}	ipv4_loopback=${EMPTY}
	Set To Dictionary	${payload}	ipv6_loopback=${EMPTY}
	Set To Dictionary	${payload}	primary_connection=ETH0
	Set To Dictionary	${payload}	secondary_connection=ETH1
	Set To Dictionary	${payload}	trigger=ipaddress
	API::Put::Network::Settings	${payload}
	Set To Dictionary	${payload}	trigger=ipv4gw
	API::Put::Network::Settings	${payload}
	Set To Dictionary	${payload}	enable_dynamic_dns=${FALSE}
	API::Put::Network::Settings	${payload}
	Set To Dictionary	${payload}	enable_network_failover=${FALSE}
	Set To Dictionary	${payload}	enable_dynamic_dns=${FALSE}
	API::Put::Network::Settings	${payload}

Get Update Network Payload
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	trigger_ip_address=127.0.0.1
	Set To Dictionary	${payload}	enable_dynamic_dns=${TRUE}
	Set To Dictionary	${payload}	failover_hostname=failover
	Set To Dictionary	${payload}	algorithm=HMAC-SHA512
	Set To Dictionary	${payload}	key_size=1024
	Set To Dictionary	${payload}	user_name=user
	Set To Dictionary	${payload}	ddns_server_name=8.8.8.8
	Set To Dictionary	${payload}	ddns_server_tcp_port=54
	Set To Dictionary	${payload}	zone=ZONE
	Set To Dictionary	${payload}	domain_name=apidomain
	Set To Dictionary	${payload}	enable_network_failover=${TRUE}
	Set To Dictionary	${payload}	hostname=apiserver
	Set To Dictionary	${payload}	enable_ipv4_ip_forward=${TRUE}
	Set To Dictionary	${payload}	enable_ipv6_ip_forward=${TRUE}
	Set To Dictionary	${payload}	ipv4_loopback=127.0.0.1
	Set To Dictionary	${payload}	ipv6_loopback=::
	Set To Dictionary	${payload}	primary_connection=ETH1
	Set To Dictionary	${payload}	secondary_connection=ETH0
	Set To Dictionary	${payload}	trigger=ipaddress
	[return]	${payload}
