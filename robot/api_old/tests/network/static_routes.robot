*** Settings ***
Documentation	Test /api/<version>/network/staticroutes endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Add Static Route
	${payload}=	Get Payload
	API::Post::Network::Static Routes	${payload}
	${routes}=	API::Get::Network::Static Routes
	${routes_count}=	Get Length	${routes}
	Should Be Equal As Integers	${routes_count}	${ORIGINAL_ROUTES_COUNT+1}
	${last_route}=	Get From List	${routes}	-1
	Set Suite Variable	${LAST_ID}	${last_route["id"]}

Get Static Routes
	${routes}=	API::Get::Network::Static Routes
	:FOR	${route}	IN	@{routes}
	\	Dictionary Should Contain Key	${route}	connection
	\	Dictionary Should Contain Key	${route}	destination_ip/mask_bits
	\	Dictionary Should Contain Key	${route}	gateway_ip
	\	Dictionary Should Contain Key	${route}	id
	\	Dictionary Should Contain Key	${route}	index
	\	Dictionary Should Contain Key	${route}	metric
	\	Dictionary Should Contain Key	${route}	type

Get Static Route
	${route}=	API::Get::Network::Static Routes::${NAME}	${LAST_ID}
	Should Be Equal As Strings	${route["connection"]}	ETH0
	Should Be Equal As Strings	${route["destination_bitmask"]}	24
	Should Be Equal As Strings	${route["destination_ip"]}	127.0.0.1
	Should Be Equal As Strings	${route["gateway_ip"]}	1.1.1.1
	Should Be Equal As Strings	${route["index"]}	route1
	Should Be Equal As Strings	${route["metric"]}	1
	Should Be Equal As Strings	${route["type"]}	ipv4

Update Static Route
	${payload}=	Get Payload
	Set To Dictionary	${payload}	destination_bitmask=32
	Set To Dictionary	${payload}	gateway_ip=1.1.1.2
	Set To Dictionary	${payload}	metric=2
	API::Put::Network::Static Routes	${LAST_ID}	${payload}

	${route}=	API::Get::Network::Static Routes::${NAME}	${LAST_ID}
	Should Be Equal As Strings	${route["type"]}	ipv4
	Should Be Equal As Strings	${route["destination_ip"]}	127.0.0.1
	Should Be Equal As Strings	${route["destination_bitmask"]}	32
	Should Be Equal As Strings	${route["gateway_ip"]}	1.1.1.2
	Should Be Equal As Strings	${route["metric"]}	2

Delete Static Routes
	API::Delete::Network::Static Routes	${LAST_ID}
	${routes}=	API::Get::Network::Static Routes
	${routes_count}=	Get Length	${routes}
	Should Be Equal As Integers	${routes_count}	${ORIGINAL_ROUTES_COUNT}

*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin

	${routes}=	API::Get::Network::Static Routes
	${ORIGINAL_ROUTES_COUNT}=	Get Length	${routes}
	Set Suite Variable	${ORIGINAL_ROUTES_COUNT}

Suite Teardown
	API::Delete::Network::Static Routes	${LAST_ID}
	API::Delete::Session

Get Payload
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	connection=ETH0
	Set To Dictionary	${payload}	type=ipv4
	Set To Dictionary	${payload}	destination_ip=127.0.0.1
	Set To Dictionary	${payload}	destination_bitmask=255.255.255.0
	Set To Dictionary	${payload}	gateway_ip=1.1.1.1
	Set To Dictionary	${payload}	metric=1
	[return]	${payload}
