*** Settings ***
Documentation	Test /api/<version>/network/dhcp endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Add DHCP server
	${payload}=	Get Payload
	API::Post::Network::DHCP Servers	${payload}
	${dhcps}=	API::Get::Network::DHCP Servers
	${dhcps_count}=	Get Length	${dhcps}
	Should Be Equal As Integers	${dhcps_count}	${ORIGINAL_DHCP_COUNT+1}
	${last_dhcp}=	Get From List	${dhcps}	-1
	Set Suite Variable	${LAST_ID}	${last_dhcp["id"]}

Get DHCP servers
	${dhcps}=	API::Get::Network::DHCP Servers
	:FOR	${dhcp}	IN	@{dhcps}
	\	Dictionary Should Contain Key	${dhcp}	id
	\	Dictionary Should Contain Key	${dhcp}	subnet/netmask
	\	Dictionary Should Contain Key	${dhcp}	domain
	\	Dictionary Should Contain Key	${dhcp}	domain_name_servers
	\	Dictionary Should Contain Key	${dhcp}	router_ip

Get DHCP Server
	${dhcp}=	API::Get::Network::DHCP Servers::${NAME}	${LAST_ID}
	Should Be Equal As Strings	${dhcp["subnet/netmask"]}	192.168.15.1/255.255.255.0
	Should Be Equal As Strings	${dhcp["domain"]}	domain
	Should Be Equal As Strings	${dhcp["domain_name_servers"]}	8.8.8.8
	Should Be Equal As Strings	${dhcp["router_ip"]}	192.168.15.2

Update DHCP Server
	${payload}=	Get Payload
	Set To Dictionary	${payload}	domain=domain2
	Set To Dictionary	${payload}	domain_name_servers=7.7.7.7
	Set To Dictionary	${payload}	router_ip=192.168.15.3
	API::Put::Network::DHCP Servers	${LAST_ID}	${payload}
	${dhcp}=	API::Get::Network::DHCP Servers::${NAME}	${LAST_ID}
	Should Be Equal As Strings	${dhcp["subnet/netmask"]}	192.168.15.1/255.255.255.0
	Should Be Equal As Strings	${dhcp["domain"]}	domain2
	Should Be Equal As Strings	${dhcp["domain_name_servers"]}	7.7.7.7
	Should Be Equal As Strings	${dhcp["router_ip"]}	192.168.15.3

Delete Host
	API::Delete::Network::DHCP Servers	${LAST_ID}
	${dhcps}=	API::Get::Network::DHCP Servers
	${dhcps_count}=	Get Length	${dhcps}
	Should Be Equal As Integers	${dhcps_count}	${ORIGINAL_DHCP_COUNT}

*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin

	${dhcps}=	API::Get::Network::DHCP Servers
	${ORIGINAL_DHCP_COUNT}=	Get Length	${dhcps}
	Set Suite Variable	${ORIGINAL_DHCP_COUNT}

Suite Teardown
	API::Delete::Network::DHCP Servers	${LAST_ID}
	API::Delete::Session

Get Payload
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	subnet=192.168.15.1
	Set To Dictionary	${payload}	netmask=255.255.255.0
	Set To Dictionary	${payload}	domain=domain
	Set To Dictionary	${payload}	domain_name_servers=8.8.8.8
	Set To Dictionary	${payload}	router_ip=192.168.15.2
	[return]	${payload}
