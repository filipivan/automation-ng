
*** Settings ***
Documentation	Test /api/<version>/logging endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	./init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown
Test setup	Set Default Values

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Get logging
	${logging}=	API::Get System Logging
	API:Json Should Contains Required Logging Fields	${logging}
	Should Not Be True	${logging["enable_session_logging_alerts"]}
	Should Not Be True	${logging["enable_session_logging"]}
	Should Be Empty	${logging["session_string_1"]}
	Should Be Empty	${logging["session_string_2"]}
	Should Be Empty	${logging["session_string_3"]}
	Should Be Empty	${logging["session_string_4"]}
	Should Be Empty	${logging["session_string_5"]}

Update logging
	${FIELDS}=	Create Dictionary
	Set To Dictionary	${FIELDS}	enable_session_logging_alerts=${TRUE}
	Set To Dictionary	${FIELDS}	enable_session_logging=${TRUE}
	Set To Dictionary	${FIELDS}	session_string_1=Alert string 1
	Set To Dictionary	${FIELDS}	session_string_2=Alert string 2
	Set To Dictionary	${FIELDS}	session_string_3=Alert string 3
	Set To Dictionary	${FIELDS}	session_string_4=Alert string 4
	Set To Dictionary	${FIELDS}	session_string_5=Alert string 5
	API::Update Logging	${FIELDS}
	${logging}=	API::Get System Logging
	API:Json Should Contains Required Logging Fields	${logging}
	Should Be True	${logging["enable_session_logging_alerts"]}
	Should Be True	${logging["enable_session_logging"]}
	Should Be Equal As Strings	${logging["session_string_1"]}	Alert string 1
	Should Be Equal As Strings	${logging["session_string_2"]}	Alert string 2
	Should Be Equal As Strings	${logging["session_string_3"]}	Alert string 3
	Should Be Equal As Strings	${logging["session_string_4"]}	Alert string 4
	Should Be Equal As Strings	${logging["session_string_5"]}	Alert string 5


*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Authentication	admin	admin

Suite Teardown
	Set Default Values
	API::Destroy Session

Set Default Values
	${FIELDS}=	Create Dictionary
	Set To Dictionary	${FIELDS}	enable_session_logging_alerts=${FALSE}
	Set To Dictionary	${FIELDS}	enable_session_logging=${FALSE}
	Set To Dictionary	${FIELDS}	session_string_1=${EMPTY}
	Set To Dictionary	${FIELDS}	session_string_2=${EMPTY}
	Set To Dictionary	${FIELDS}	session_string_3=${EMPTY}
	Set To Dictionary	${FIELDS}	session_string_4=${EMPTY}
	Set To Dictionary	${FIELDS}	session_string_5=${EMPTY}
	API::Update Logging	${FIELDS}

API:Json Should Contains Required Logging Fields
	[Arguments]	${data}
	[Documentation]	Check if the given data contains all required date and time fields
	[Tags]	API
	Should Contain	${data}	enable_session_logging
	Should Contain	${data}	enable_session_logging_alerts
	Should Contain	${data}	session_string_1
	Should Contain	${data}	session_string_2
	Should Contain	${data}	session_string_3
	Should Contain	${data}	session_string_4
	Should Contain	${data}	session_string_5

