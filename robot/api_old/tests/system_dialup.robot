*** Settings ***
Documentation	Test /api/<version>/system/dialup endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	./init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown
Test setup	Reset Dialup Values

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Get dial up configuration
	${info}=	API::Get Dial Up Configuration
	Check Dialup Configuration Fields	${info}

Enable dialup
	API::Update Dial Up Configuration	enabled	enabled
	${info}=	API::Get Dial Up Configuration
	Check Dialup Configuration Fields	${info}
	Should Be Equal As Strings	${info["login_session"]}	enabled
	Should Be Equal As Strings	${info["ppp_connection"]}	enabled

Set dialup to callback
	API::Update Dial Up Configuration	callback	callback
	${info}=	API::Get Dial Up Configuration
	Check Dialup Configuration Fields	${info}
	Should Be Equal As Strings	${info["login_session"]}	callback
	Should Be Equal As Strings	${info["ppp_connection"]}	callback

Add Dialup Device
	${payload}=	Get Device Payload
	API::Add Dialup Device	${payload}

Get Dialup Devices
	${devices}=	API::Get Dialup Devices
	Test If Dummy Dialup Device Exists	${devices}	testdev


Get Dialup Device
	${device}=	API::Get Dialup Device	testdev
	Check Dialup Device Fields	${device}

Delete Dialup Device
	API::Delete Dialup Device	testdev
	${devices}=	API::Get Dialup Devices
	Test If Dummy Dialup Device Not Exists	${devices}	testdev


Add Dialup Callback User
	${payload}=	Get Callback User Payload
	API::Add Dialup Callback User	${payload}
	#Wait Until ${payload["callback_user"]} Dialup Callback User Exists

Get Dialup Callback Users
	${users}=	API::Get Dialup Callback Users
	Test If Dummy Dialup Callback User Exists	cbuser

Get Dialup Callback User
	${user}=	API::Get Dialup Callback User	cbuser
	Check Dialup Callback User Fields	${user}

Delete Dialup Callback User
	API::Delete Dialup Callback User	cbuser
	${users}=	API::Get Dialup Callback Users
	Test If Dummy Dialup Callback User Not Exists	${users}	cbuser

*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Authentication	admin	admin
	Reset Dialup Values

Suite Teardown
	Reset Dialup Values
	API::Destroy Session


Check Dialup Device Data
	[Arguments]	${DEVICE}
	Should Contain	${DEVICE}	id
	Should Contain	${DEVICE}	name
	Should Contain	${DEVICE}	state
	Should Contain	${DEVICE}	name
	Should Contain	${DEVICE}	status
	Should Contain	${DEVICE}	session_info

Check Dialup Configuration Fields
	[Arguments]	${FIELDS}
	Dictionary Should Contain Key	${FIELDS}	login_session
	Dictionary Should Contain Key	${FIELDS}	ppp_connection

Reset Dialup Values
	API::Update Dial Up Configuration	disabled	disabled

Test If Dummy Dialup Device Exists
	[Arguments]	${DEVICES}	${DEV}
	:FOR	${device}	IN	@{DEVICES}
	\	Check Dialup Device Data	${device}
	\	${id}=	Set Variable	${device["id"]}
	\	Pass Execution If	'${DEV}' == '${id}'	Device found
	Fail	Device not found

Test If Dummy Dialup Device Not Exists
	[Arguments]	${DEVICES}	${DEV}
	:FOR	${device}	IN	@{DEVICES}
	\	Check Dialup Device Data	${device}
	\	${id}=	Set Variable	${device["id"]}
	\	Run Keyword If	'${DEV}' == '${id}'	Fail	Deleted device found


Get Device Payload
	${payload}= 	Create Dictionary
	Set To Dictionary	${payload}	name=testdev
	Set To Dictionary	${payload}	status=disabled
	Set To Dictionary	${payload}	device_name=device
	Set To Dictionary	${payload}	speed=38400
	Set To Dictionary	${payload}	phone_number=999999
	Set To Dictionary	${payload}	ppp_idle_timeout=1
	Set To Dictionary	${payload}	init_chat=chat
	Set To Dictionary	${payload}	ppp_ipv4_address=local
	Set To Dictionary	${payload}	remote_ipv4_address=127.0.0.1
	Set To Dictionary	${payload}	local_ipv4_address=127.0.0.1
	Set To Dictionary	${payload}	ppp_ipv6_address=local
	Set To Dictionary	${payload}	remote_ipv6_address=::1
	Set To Dictionary	${payload}	local_ipv6_address=::1
	Set To Dictionary	${payload}	ppp_authentication=remote
	Set To Dictionary	${payload}	authentication_protocol=pap
	Set To Dictionary	${payload}	remote_username=remote
	Set To Dictionary	${payload}	remote_passphrase=remote
	[return]	${payload}

Check Dialup Device Fields
	[Arguments]	${FIELDS}
	Should Be Equal As Strings	${FIELDS["name"]}	testdev
	Should Be Equal As Strings	${FIELDS["status"]}	disabled
	Should Be Equal As Strings	${FIELDS["device_name"]}	device
	Should Be Equal As Strings	${FIELDS["speed"]}	38400
	Should Be Equal As Strings	${FIELDS["phone_number"]}	999999
	Should Be Equal As Strings	${FIELDS["ppp_idle_timeout"]}	1
	Should Be Equal As Strings	${FIELDS["init_chat"]}	chat
	Should Be Equal As Strings	${FIELDS["ppp_ipv4_address"]}	local
	Should Be Equal As Strings	${FIELDS["remote_ipv4_address"]}	127.0.0.1
	Should Be Equal As Strings	${FIELDS["local_ipv4_address"]}	127.0.0.1
	Should Be Equal As Strings	${FIELDS["ppp_ipv6_address"]}	local
	Should Be Equal As Strings	${FIELDS["remote_ipv6_address"]}	::1
	Should Be Equal As Strings	${FIELDS["local_ipv6_address"]}	::1
	Should Be Equal As Strings	${FIELDS["ppp_authentication"]}	remote
	Should Be Equal As Strings	${FIELDS["authentication_protocol"]}	pap
	Should Be Equal As Strings	${FIELDS["remote_username"]}	remote

Get Callback User Payload
	${payload}= 	Create Dictionary
	Set To Dictionary	${payload}	callback_user=cbuser
	Set To Dictionary	${payload}	callback_number=999999
	[return]	${payload}

Check Dialup Callback User Data
	[Arguments]	${USER}
	Should Contain	${USER}	id
	Should Contain	${USER}	callback_user
	Should Contain	${USER}	callback_number

Wait Until ${USERID} Dialup Callback User Exists
	Wait Until Keyword Succeeds	3 times	10 seconds	Test If Dummy Dialup Callback User Exists	${USERID}

Test If Dummy Dialup Callback User Exists
	[Arguments]	${USERID}
	${USERS}=	API::Get Dialup Callback Users
	:FOR	${user}	IN	@{USERS}
	\	Check Dialup Callback User Data	${user}
	\	${id}=	Set Variable	${user["id"]}
	\	Pass Execution If	'${USERID}' == '${id}'	User found
	Fail	User not found

Check Dialup Callback User Fields
	[Arguments]	${FIELDS}
	Should Be Equal As Strings	${FIELDS["callback_user"]}	cbuser
	Should Be Equal As Strings	${FIELDS["callback_number"]}	999999

Test If Dummy Dialup Callback User Not Exists
	[Arguments]	${USERS}	${USERID}
	:FOR	${user}	IN	@{USERS}
	\	Check Dialup Device Data	${user}
	\	${id}=	Set Variable	${user["id"]}
	\	Run Keyword If	'${USERID}' == '${id}'	Fail	Deleted user found
