*** Settings ***
Documentation	Test /api/<version>/security/nat endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Suite Setup
Suite Teardown	API::Destroy Session

Force Tags		CLI	API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Variable ***
${IPv4}=	IPv4
${IPv6}=	IPv6
${CHAIN}=	ZPE

*** Keywords ***
Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Authentication	admin	admin

*** Test Cases ***
Get NAT Chains
	${chains}=	API::Send Get Request	security/nat
	Length Should Be 	${chains}	8
	${LIST}=	Create List 	PREROUTING:IPv4		INPUT:IPv4		OUTPUT:IPv4		POSTROUTING:IPv4	PREROUTING:IPv6
	...	INPUT:IPv6		OUTPUT:IPv6		POSTROUTING:IPv6
	${i}=	Evaluate 	0
	:FOR	${fw}	IN	@{chains}
	\	${keys}=	Get Dictionary Keys	${fw}
	\	Length Should Be	${keys}	6
	\	Dictionary Should Contain Key	${fw}	id
	\	Dictionary Should Contain Key	${fw}	bytes
	\	Dictionary Should Contain Key	${fw}	chain
	\	Dictionary Should Contain Key	${fw}	packets
	\	Dictionary Should Contain Key	${fw}	policy
	\	Dictionary Should Contain Key	${fw}	type
	\	Dictionary Should Contain Item 	${fw}	id 	@{LIST}[${i}]
	\	${i}=	Evaluate 	${i} + 1

Add NAT Custom Chain
	${chains}=	API::Send Get Request	security/nat
	${count}=	Get Length	${chains}
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	type=${IPv4}
	Set To Dictionary	${payload}	chain=${CHAIN}
	API::Send Post Request	security/nat	${payload}
	${chains}=	API::Send Get Request	security/nat
	Length Should Be 	${chains}	${count+1}

Delete NAT Custom Chain
	${chains}=	API::Send Get Request	security/nat
	${count}=	Get Length	${chains}

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	chains=${CHAIN}:IPv4
	${headers}=	API::Get Ticket Header
	${response}=	Delete Request	api	/api/${API_VERSION}/security/nat	${payload}	headers=${headers}
	Should Be Equal As Strings	${response.status_code}	200

	${chains}=	API::Send Get Request	security/nat
	Length Should Be	${chains}	${count-1}

#########################################################
#														#
#					IPv4 tests							#
#														#
#########################################################

IPv4: Get PREROUTING Rules
	${rules}=	API::Send Get Request	security/nat/PREROUTING:${IPv4}/rules
	:FOR	${rule}	IN	@{rules}
	\	Dictionary Should Contain Key	${rule}	bytes
	\	Dictionary Should Contain Key	${rule}	destination_net4
	\	Dictionary Should Contain Key	${rule}	id
	\	Dictionary Should Contain Key	${rule}	input_interface
	\	Dictionary Should Contain Key	${rule}	output_interface
	\	Dictionary Should Contain Key	${rule}	packets
	\	Dictionary Should Contain Key	${rule}	protocol
	\	Dictionary Should Contain Key	${rule}	rules
	\	Dictionary Should Contain Key	${rule}	source_net4
	\	Dictionary Should Contain Key	${rule}	target
	\	Length Should Be	${rule}	10


IPv4: Add Rule to PREROUTING Chain
	${rules}=	API::Send Get Request	security/nat/PREROUTING:${IPv4}/rules
	${count}=	Get Length	${rules}

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	target=LOG
	Set To Dictionary	${payload}	source_net4=${EMPTY}
	Set To Dictionary	${payload}	destination_net4=${EMPTY}
	Set To Dictionary	${payload}	input_interface=any
	Set To Dictionary	${payload}	fragments=all
	Set To Dictionary	${payload}	protocol=numeric
	Set To Dictionary	${payload}	protocol_number=7
	Set To Dictionary	${payload}	source_port=${EMPTY}
	Set To Dictionary	${payload}	destination_port=${EMPTY}
	Set To Dictionary	${payload}	tcp_flag_syn=any
	Set To Dictionary	${payload}	tcp_flag_ack=any
	Set To Dictionary	${payload}	tcp_flag_fin=any
	Set To Dictionary	${payload}	tcp_flag_rst=any
	Set To Dictionary	${payload}	tcp_flag_urg=any
	Set To Dictionary	${payload}	tcp_flag_psh=any
	Set To Dictionary	${payload}	source_udp_port=${EMPTY}
	Set To Dictionary	${payload}	destination_udp_port=${EMPTY}
	Set To Dictionary	${payload}	icmp_type=any
	Set To Dictionary	${payload}	log_level=debug
	Set To Dictionary	${payload}	log_prefix=ZPE_PREFIXX
	Set To Dictionary	${payload}	reverse_match_for_source_ip/mask=${FALSE}}
	Set To Dictionary	${payload}	reverse_match_for_destination_ip/mask=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_input_interface=${FALSE}
	Set To Dictionary	${payload}	enable_state_match=${FALSE}
	Set To Dictionary	${payload}	reverse_state_match=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_tcp_flags=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_icmp_type=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_protocol=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_source_port=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_destination_port=${FALSE}
	Set To Dictionary	${payload}	log_tcp_sequence_numbers=${FALSE}
	Set To Dictionary	${payload}	log_options_from_the_tcp_packet_header=${FALSE}
	Set To Dictionary	${payload}	log_options_from_the_ip_packet_header=${FALSE}

	${headers}=	API::Get Ticket Header
	${response}=	Post Request	api	/api/${API_VERSION}/security/nat/PREROUTING:${IPv4}/rules	data=${payload}	 headers=${headers}
	Should Be Equal As Strings	${response.status_code}	200

	${rules}=	API::Send Get Request	security/nat/PREROUTING:${IPv4}/rules
	Length Should Be	${rules}	${count+1}

IPv4: Get PREROUTING Rule
	${rules}=	API::Send Get Request	security/nat/PREROUTING:${IPv4}/rules
	${count}=	Get Length	${rules}
	${rule}=	API::Send Get Request	security/nat/PREROUTING:${IPv4}/rules/${count-1}

	Should Be Equal As Strings	${rule["target"]}	LOG
	Should Be Equal As Strings	${rule["source_net4"]}	${EMPTY}
	Should Be Equal As Strings	${rule["destination_net4"]}	${EMPTY}
	Should Be Equal As Strings	${rule["input_interface"]}	any
	Should Be Equal As Strings	${rule["fragments"]}	all
	Should Be Equal As Strings	${rule["protocol"]}	numeric
	Should Be Equal As Strings	${rule["protocol_number"]}	7
	Should Be Equal As Strings	${rule["source_port"]}	${EMPTY}
	Should Be Equal As Strings	${rule["destination_port"]}	${EMPTY}
	Should Be Equal As Strings	${rule["tcp_flag_syn"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_ack"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_fin"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_rst"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_urg"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_psh"]}	any
	Should Be Equal As Strings	${rule["source_udp_port"]}	${EMPTY}
	Should Be Equal As Strings	${rule["destination_udp_port"]}	${EMPTY}
	Should Be Equal As Strings	${rule["icmp_type"]}	any
	Should Be Equal As Strings	${rule["log_level"]}	debug
	Should Be Equal As Strings	${rule["log_prefix"]}	ZPE_PREFIXX
	Should Not Be True	${rule["reverse_match_for_source_ip/mask"]}
	Should Not Be True	${rule["reverse_match_for_destination_ip/mask"]}
	Should Not Be True	${rule["reverse_match_for_input_interface"]}
	Should Not Be True	${rule["enable_state_match"]}
	Should Not Be True	${rule["reverse_state_match"]}
	Should Not Be True	${rule["reverse_match_for_tcp_flags"]}
	Should Not Be True	${rule["reverse_match_for_icmp_type"]}
	Should Not Be True	${rule["reverse_match_for_protocol"]}
	Should Not Be True	${rule["reverse_match_for_source_port"]}
	Should Not Be True	${rule["reverse_match_for_destination_port"]}
	Should Not Be True	${rule["log_tcp_sequence_numbers"]}
	Should Not Be True	${rule["log_options_from_the_tcp_packet_header"]}
	Should Not Be True	${rule["log_options_from_the_ip_packet_header"]}

IPv4: Update PREROUTING Rule
	${rules}=	API::Send Get Request	security/nat/PREROUTING:${IPv4}/rules
	${count}=	Get Length	${rules}

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	target=LOG
	Set To Dictionary	${payload}	source_net4=${EMPTY}
	Set To Dictionary	${payload}	destination_net4=${EMPTY}
	Set To Dictionary	${payload}	input_interface=any
	Set To Dictionary	${payload}	fragments=all
	Set To Dictionary	${payload}	protocol=numeric
	Set To Dictionary	${payload}	protocol_number=3
	Set To Dictionary	${payload}	source_port=${EMPTY}
	Set To Dictionary	${payload}	destination_port=${EMPTY}
	Set To Dictionary	${payload}	tcp_flag_syn=any
	Set To Dictionary	${payload}	tcp_flag_ack=any
	Set To Dictionary	${payload}	tcp_flag_fin=any
	Set To Dictionary	${payload}	tcp_flag_rst=any
	Set To Dictionary	${payload}	tcp_flag_urg=any
	Set To Dictionary	${payload}	tcp_flag_psh=any
	Set To Dictionary	${payload}	source_udp_port=${EMPTY}
	Set To Dictionary	${payload}	destination_udp_port=${EMPTY}
	Set To Dictionary	${payload}	icmp_type=any
	Set To Dictionary	${payload}	log_level=debug
	Set To Dictionary	${payload}	log_prefix=ZPE_PREFIX
	Set To Dictionary	${payload}	reverse_match_for_source_ip/mask=${FALSE}}
	Set To Dictionary	${payload}	reverse_match_for_destination_ip/mask=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_input_interface=${FALSE}
	Set To Dictionary	${payload}	enable_state_match=${FALSE}
	Set To Dictionary	${payload}	reverse_state_match=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_tcp_flags=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_icmp_type=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_protocol=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_source_port=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_destination_port=${FALSE}
	Set To Dictionary	${payload}	log_tcp_sequence_numbers=${FALSE}
	Set To Dictionary	${payload}	log_options_from_the_tcp_packet_header=${FALSE}
	Set To Dictionary	${payload}	log_options_from_the_ip_packet_header=${FALSE}

	API::Send Put Request	security/nat/PREROUTING:${IPv4}/rules/${count-1}	${payload}

IPv4: Get PREROUTING Rule After Update
	${rules}=	API::Send Get Request	security/nat/PREROUTING:${IPv4}/rules
	${count}=	Get Length	${rules}
	${rule}=	API::Send Get Request	security/nat/PREROUTING:${IPv4}/rules/${count-1}

	Should Be Equal As Strings	${rule["target"]}	LOG
	Should Be Equal As Strings	${rule["source_net4"]}	${EMPTY}
	Should Be Equal As Strings	${rule["destination_net4"]}	${EMPTY}
	Should Be Equal As Strings	${rule["input_interface"]}	any
	Should Be Equal As Strings	${rule["fragments"]}	all
	Should Be Equal As Strings	${rule["protocol"]}	numeric
	Should Be Equal As Strings	${rule["protocol_number"]}	3
	Should Be Equal As Strings	${rule["source_port"]}	${EMPTY}
	Should Be Equal As Strings	${rule["destination_port"]}	${EMPTY}
	Should Be Equal As Strings	${rule["tcp_flag_syn"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_ack"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_fin"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_rst"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_urg"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_psh"]}	any
	Should Be Equal As Strings	${rule["source_udp_port"]}	${EMPTY}
	Should Be Equal As Strings	${rule["destination_udp_port"]}	${EMPTY}
	Should Be Equal As Strings	${rule["icmp_type"]}	any
	Should Be Equal As Strings	${rule["log_level"]}	debug
	Should Be Equal As Strings	${rule["log_prefix"]}	ZPE_PREFIX
	Should Not Be True	${rule["reverse_match_for_source_ip/mask"]}
	Should Not Be True	${rule["reverse_match_for_destination_ip/mask"]}
	Should Not Be True	${rule["reverse_match_for_input_interface"]}
	Should Not Be True	${rule["enable_state_match"]}
	Should Not Be True	${rule["reverse_state_match"]}
	Should Not Be True	${rule["reverse_match_for_tcp_flags"]}
	Should Not Be True	${rule["reverse_match_for_icmp_type"]}
	Should Not Be True	${rule["reverse_match_for_protocol"]}
	Should Not Be True	${rule["reverse_match_for_source_port"]}
	Should Not Be True	${rule["reverse_match_for_destination_port"]}
	Should Not Be True	${rule["log_tcp_sequence_numbers"]}
	Should Not Be True	${rule["log_options_from_the_tcp_packet_header"]}
	Should Not Be True	${rule["log_options_from_the_ip_packet_header"]}

IPv4: Delete Chain Rule
	${rules}=	API::Send Get Request	security/nat/PREROUTING:${IPv4}/rules
	${count}=	Get Length	${rules}

	${payload}=	Create Dictionary
	${string}=	convert to string  ${count-1}
	${list}= 	Create List 	${string}
	Set To Dictionary	${payload}	rules=${list}
	${headers}=	API::Get Ticket Header
	${response}=	Delete Request	api	/api/${API_VERSION}/security/nat/PREROUTING:${IPv4}/rules	${payload}	headers=${headers}
	Should Be Equal As Strings	${response.status_code}	200

	${rules}=	API::Send Get Request	security/nat/PREROUTING:${IPv4}/rules
	Length Should Be	${rules}	${count-1}

#########################################################
#														#
#					IPv6 tests							#
#														#
#########################################################

IPv6: Get PREROUTING Rules
	${rules}=	API::Send Get Request	security/nat/PREROUTING:${IPv6}/rules
	:FOR	${rule}	IN	@{rules}
	\	Dictionary Should Contain Key	${rule}	bytes
	\	Dictionary Should Contain Key	${rule}	destination_net4
	\	Dictionary Should Contain Key	${rule}	id
	\	Dictionary Should Contain Key	${rule}	input_interface
	\	Dictionary Should Contain Key	${rule}	output_interface
	\	Dictionary Should Contain Key	${rule}	packets
	\	Dictionary Should Contain Key	${rule}	protocol
	\	Dictionary Should Contain Key	${rule}	rules
	\	Dictionary Should Contain Key	${rule}	source_net4
	\	Dictionary Should Contain Key	${rule}	target
	\	Length Should Be	${rule}	10


IPv6: Add Rule to PREROUTING Chain
	${rules}=	API::Send Get Request	security/nat/PREROUTING:${IPv6}/rules
	${count}=	Get Length	${rules}

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	target=LOG
	Set To Dictionary	${payload}	source_net6=${EMPTY}
	Set To Dictionary	${payload}	destination_net6=${EMPTY}
	Set To Dictionary	${payload}	input_interface=any
	Set To Dictionary	${payload}	fragments=all
	Set To Dictionary	${payload}	protocol=numeric
	Set To Dictionary	${payload}	protocol_number=7
	Set To Dictionary	${payload}	source_port=${EMPTY}
	Set To Dictionary	${payload}	destination_port=${EMPTY}
	Set To Dictionary	${payload}	tcp_flag_syn=any
	Set To Dictionary	${payload}	tcp_flag_ack=any
	Set To Dictionary	${payload}	tcp_flag_fin=any
	Set To Dictionary	${payload}	tcp_flag_rst=any
	Set To Dictionary	${payload}	tcp_flag_urg=any
	Set To Dictionary	${payload}	tcp_flag_psh=any
	Set To Dictionary	${payload}	source_udp_port=${EMPTY}
	Set To Dictionary	${payload}	destination_udp_port=${EMPTY}
	Set To Dictionary	${payload}	icmp_type=any
	Set To Dictionary	${payload}	log_level=debug
	Set To Dictionary	${payload}	log_prefix=ZPE_PREFIXX
	Set To Dictionary	${payload}	reverse_match_for_source_ip/mask=${FALSE}}
	Set To Dictionary	${payload}	reverse_match_for_destination_ip/mask=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_input_interface=${FALSE}
	Set To Dictionary	${payload}	enable_state_match=${FALSE}
	Set To Dictionary	${payload}	reverse_state_match=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_tcp_flags=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_icmp_type=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_protocol=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_source_port=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_destination_port=${FALSE}
	Set To Dictionary	${payload}	log_tcp_sequence_numbers=${FALSE}
	Set To Dictionary	${payload}	log_options_from_the_tcp_packet_header=${FALSE}
	Set To Dictionary	${payload}	log_options_from_the_ip_packet_header=${FALSE}

	${headers}=	API::Get Ticket Header
	${response}=	Post Request	api	/api/${API_VERSION}/security/nat/PREROUTING:${IPv6}/rules	data=${payload}	 headers=${headers}
	Should Be Equal As Strings	${response.status_code}	200

	${rules}=	API::Send Get Request	security/nat/PREROUTING:${IPv6}/rules
	Length Should Be	${rules}	${count+1}

IPv6: Get PREROUTING Rule
	${rules}=	API::Send Get Request	security/nat/PREROUTING:${IPv6}/rules
	${count}=	Get Length	${rules}
	${rule}=	API::Send Get Request	security/nat/PREROUTING:${IPv6}/rules/${count-1}

	Should Be Equal As Strings	${rule["target"]}	LOG
	Should Be Equal As Strings	${rule["source_net6"]}	${EMPTY}
	Should Be Equal As Strings	${rule["destination_net6"]}	${EMPTY}
	Should Be Equal As Strings	${rule["input_interface"]}	any
	Should Be Equal As Strings	${rule["fragments"]}	all
	Should Be Equal As Strings	${rule["protocol"]}	numeric
	Should Be Equal As Strings	${rule["protocol_number"]}	7
	Should Be Equal As Strings	${rule["source_port"]}	${EMPTY}
	Should Be Equal As Strings	${rule["destination_port"]}	${EMPTY}
	Should Be Equal As Strings	${rule["tcp_flag_syn"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_ack"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_fin"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_rst"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_urg"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_psh"]}	any
	Should Be Equal As Strings	${rule["source_udp_port"]}	${EMPTY}
	Should Be Equal As Strings	${rule["destination_udp_port"]}	${EMPTY}
	Should Be Equal As Strings	${rule["icmp_type"]}	any
	Should Be Equal As Strings	${rule["log_level"]}	debug
	Should Be Equal As Strings	${rule["log_prefix"]}	ZPE_PREFIXX
	Should Not Be True	${rule["reverse_match_for_source_ip/mask"]}
	Should Not Be True	${rule["reverse_match_for_destination_ip/mask"]}
	Should Not Be True	${rule["reverse_match_for_input_interface"]}
	Should Not Be True	${rule["enable_state_match"]}
	Should Not Be True	${rule["reverse_state_match"]}
	Should Not Be True	${rule["reverse_match_for_tcp_flags"]}
	Should Not Be True	${rule["reverse_match_for_icmp_type"]}
	Should Not Be True	${rule["reverse_match_for_protocol"]}
	Should Not Be True	${rule["reverse_match_for_source_port"]}
	Should Not Be True	${rule["reverse_match_for_destination_port"]}
	Should Not Be True	${rule["log_tcp_sequence_numbers"]}
	Should Not Be True	${rule["log_options_from_the_tcp_packet_header"]}
	Should Not Be True	${rule["log_options_from_the_ip_packet_header"]}

IPv6: Update PREROUTING Rule
	${rules}=	API::Send Get Request	security/nat/PREROUTING:${IPv6}/rules
	${count}=	Get Length	${rules}

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	target=LOG
	Set To Dictionary	${payload}	source_net6=${EMPTY}
	Set To Dictionary	${payload}	destination_net6=${EMPTY}
	Set To Dictionary	${payload}	input_interface=any
	Set To Dictionary	${payload}	fragments=all
	Set To Dictionary	${payload}	protocol=numeric
	Set To Dictionary	${payload}	protocol_number=3
	Set To Dictionary	${payload}	source_port=${EMPTY}
	Set To Dictionary	${payload}	destination_port=${EMPTY}
	Set To Dictionary	${payload}	tcp_flag_syn=any
	Set To Dictionary	${payload}	tcp_flag_ack=any
	Set To Dictionary	${payload}	tcp_flag_fin=any
	Set To Dictionary	${payload}	tcp_flag_rst=any
	Set To Dictionary	${payload}	tcp_flag_urg=any
	Set To Dictionary	${payload}	tcp_flag_psh=any
	Set To Dictionary	${payload}	source_udp_port=${EMPTY}
	Set To Dictionary	${payload}	destination_udp_port=${EMPTY}
	Set To Dictionary	${payload}	icmp_type=any
	Set To Dictionary	${payload}	log_level=debug
	Set To Dictionary	${payload}	log_prefix=ZPE_PREFIX
	Set To Dictionary	${payload}	reverse_match_for_source_ip/mask=${FALSE}}
	Set To Dictionary	${payload}	reverse_match_for_destination_ip/mask=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_input_interface=${FALSE}
	Set To Dictionary	${payload}	enable_state_match=${FALSE}
	Set To Dictionary	${payload}	reverse_state_match=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_tcp_flags=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_icmp_type=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_protocol=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_source_port=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_destination_port=${FALSE}
	Set To Dictionary	${payload}	log_tcp_sequence_numbers=${FALSE}
	Set To Dictionary	${payload}	log_options_from_the_tcp_packet_header=${FALSE}
	Set To Dictionary	${payload}	log_options_from_the_ip_packet_header=${FALSE}

	API::Send Put Request	security/nat/PREROUTING:${IPv6}/rules/${count-1}	${payload}

IPv6: Get PREROUTING Rule After Update
	${rules}=	API::Send Get Request	security/nat/PREROUTING:${IPv6}/rules
	${count}=	Get Length	${rules}
	${rule}=	API::Send Get Request	security/nat/PREROUTING:${IPv6}/rules/${count-1}

	Should Be Equal As Strings	${rule["target"]}	LOG
	Should Be Equal As Strings	${rule["source_net6"]}	${EMPTY}
	Should Be Equal As Strings	${rule["destination_net6"]}	${EMPTY}
	Should Be Equal As Strings	${rule["input_interface"]}	any
	Should Be Equal As Strings	${rule["fragments"]}	all
	Should Be Equal As Strings	${rule["protocol"]}	numeric
	Should Be Equal As Strings	${rule["protocol_number"]}	3
	Should Be Equal As Strings	${rule["source_port"]}	${EMPTY}
	Should Be Equal As Strings	${rule["destination_port"]}	${EMPTY}
	Should Be Equal As Strings	${rule["tcp_flag_syn"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_ack"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_fin"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_rst"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_urg"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_psh"]}	any
	Should Be Equal As Strings	${rule["source_udp_port"]}	${EMPTY}
	Should Be Equal As Strings	${rule["destination_udp_port"]}	${EMPTY}
	Should Be Equal As Strings	${rule["icmp_type"]}	any
	Should Be Equal As Strings	${rule["log_level"]}	debug
	Should Be Equal As Strings	${rule["log_prefix"]}	ZPE_PREFIX
	Should Not Be True	${rule["reverse_match_for_source_ip/mask"]}
	Should Not Be True	${rule["reverse_match_for_destination_ip/mask"]}
	Should Not Be True	${rule["reverse_match_for_input_interface"]}
	Should Not Be True	${rule["enable_state_match"]}
	Should Not Be True	${rule["reverse_state_match"]}
	Should Not Be True	${rule["reverse_match_for_tcp_flags"]}
	Should Not Be True	${rule["reverse_match_for_icmp_type"]}
	Should Not Be True	${rule["reverse_match_for_protocol"]}
	Should Not Be True	${rule["reverse_match_for_source_port"]}
	Should Not Be True	${rule["reverse_match_for_destination_port"]}
	Should Not Be True	${rule["log_tcp_sequence_numbers"]}
	Should Not Be True	${rule["log_options_from_the_tcp_packet_header"]}
	Should Not Be True	${rule["log_options_from_the_ip_packet_header"]}

IPv6: Delete Chain Rule
	${rules}=	API::Send Get Request	security/nat/PREROUTING:${IPv6}/rules
	${count}=	Get Length	${rules}

	${payload}=	Create Dictionary
	${string}=	convert to string  ${count-1}
	${list}= 	Create List 	${string}
	Set To Dictionary	${payload}	rules=${list}
	${headers}=	API::Get Ticket Header
	${response}=	Delete Request	api	/api/${API_VERSION}/security/nat/PREROUTING:${IPv6}/rules	${payload}	headers=${headers}
	Should Be Equal As Strings	${response.status_code}	200

	${rules}=	API::Send Get Request	security/nat/PREROUTING:${IPv6}/rules
	Length Should Be	${rules}	${count-1}