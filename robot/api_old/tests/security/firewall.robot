*** Settings ***
Documentation	Test /api/<version>/security/firewall endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Variable ***
${TYPE}=	IPv6
${CHAIN}=	ZPE

*** Test Cases ***

Add Firewall Chain
	${chains}=	API::Get Security Firewall Chain
	${count}=	Get Length	${chains}

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	type=${TYPE}
	Set To Dictionary	${payload}	chain=${CHAIN}

	API::Add Security Firewall Chain	${payload}

	${chains}=	API::Get Security Firewall Chain
	Length Should Be	${chains}	${count+1}


Get Firewall Chains
	${chains}=	API::Get Security Firewall Chain
	:FOR	${fw}	IN	@{chains}
	\	${keys}=	Get Dictionary Keys	${fw}
	\	Length Should Be	${keys}	6
	\	Dictionary Should Contain Key	${fw}	id
	\	Dictionary Should Contain Key	${fw}	bytes
	\	Dictionary Should Contain Key	${fw}	chain
	\	Dictionary Should Contain Key	${fw}	packets
	\	Dictionary Should Contain Key	${fw}	policy
	\	Dictionary Should Contain Key	${fw}	type

Add Chain Rule
	${rules}=	API::Get Security Firewall Chain Rules	${CHAIN}:${TYPE}
	${count}=	Get Length	${rules}

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	target=LOG
	Set To Dictionary	${payload}	reject_with=icmp6-no-route
	Set To Dictionary	${payload}	source_net6=${EMPTY}
	Set To Dictionary	${payload}	destination_net6=${EMPTY}
	Set To Dictionary	${payload}	input_interface=any
	Set To Dictionary	${payload}	output_interface=any
	Set To Dictionary	${payload}	state_match=NEW
	Set To Dictionary	${payload}	fragments=all
	Set To Dictionary	${payload}	protocol=numeric
	Set To Dictionary	${payload}	protocol_number=7
	Set To Dictionary	${payload}	source_port=${EMPTY}
	Set To Dictionary	${payload}	destination_port=${EMPTY}
	Set To Dictionary	${payload}	tcp_flag_syn=any
	Set To Dictionary	${payload}	tcp_flag_ack=any
	Set To Dictionary	${payload}	tcp_flag_fin=any
	Set To Dictionary	${payload}	tcp_flag_rst=any
	Set To Dictionary	${payload}	tcp_flag_urg=any
	Set To Dictionary	${payload}	tcp_flag_psh=any
	Set To Dictionary	${payload}	source_udp_port=${EMPTY}
	Set To Dictionary	${payload}	destination_udp_port=${EMPTY}
	Set To Dictionary	${payload}	icmp_type=any
	Set To Dictionary	${payload}	log_level=debug
	Set To Dictionary	${payload}	log_prefix=ZPE_PREFIX
	Set To Dictionary	${payload}	reverse_match_for_source_ip/mask=${FALSE}}
	Set To Dictionary	${payload}	reverse_match_for_destination_ip/mask=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_input_interface=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_output_interface=${FALSE}
	Set To Dictionary	${payload}	enable_state_match=${FALSE}
	Set To Dictionary	${payload}	reverse_state_match=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_tcp_flags=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_icmp_type=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_protocol=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_source_port=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_destination_port=${FALSE}
	Set To Dictionary	${payload}	log_tcp_sequence_numbers=${FALSE}
	Set To Dictionary	${payload}	log_options_from_the_tcp_packet_header=${FALSE}
	Set To Dictionary	${payload}	log_options_from_the_ip_packet_header=${FALSE}

	API::Add Security Firewall Chain Rule	${CHAIN}:${TYPE}	${payload}

	${rules}=	API::Get Security Firewall Chain Rules	${CHAIN}:${TYPE}
	Length Should Be	${rules}	${count+1}

Get Chain Rules
	${rules}=	API::Get Security Firewall Chain Rules	${CHAIN}:${TYPE}
	:FOR	${rule}	IN	@{rules}
	\	Dictionary Should Contain Key	${rule}	bytes
	\	Dictionary Should Contain Key	${rule}	destination_net6
	\	Dictionary Should Contain Key	${rule}	id
	\	Dictionary Should Contain Key	${rule}	input_interface
	\	Dictionary Should Contain Key	${rule}	output_interface
	\	Dictionary Should Contain Key	${rule}	packets
	\	Dictionary Should Contain Key	${rule}	protocol
	\	Dictionary Should Contain Key	${rule}	rules
	\	Dictionary Should Contain Key	${rule}	source_net6
	\	Dictionary Should Contain Key	${rule}	target
	\	Length Should Be	${rule}	10

Get Chain Rule
	${rules}=	API::Get Security Firewall Chain Rules	${CHAIN}:${TYPE}
	${count}=	Get Length	${rules}
	${rule}=	API::Get Security Firewall Chain Rule	${CHAIN}:${TYPE}	${count-1}

	Should Be Equal As Strings	${rule["target"]}	LOG
	Should Be Equal As Strings	${rule["reject_with"]}	icmp6-no-route
	Should Be Equal As Strings	${rule["source_net6"]}	${EMPTY}
	Should Be Equal As Strings	${rule["destination_net6"]}	${EMPTY}
	Should Be Equal As Strings	${rule["input_interface"]}	any
	Should Be Equal As Strings	${rule["output_interface"]}	any
	Should Be Equal As Strings	${rule["state_match"]}	NEW
	Should Be Equal As Strings	${rule["fragments"]}	all
	Should Be Equal As Strings	${rule["protocol"]}	numeric
	Should Be Equal As Strings	${rule["protocol_number"]}	7
	Should Be Equal As Strings	${rule["source_port"]}	${EMPTY}
	Should Be Equal As Strings	${rule["destination_port"]}	${EMPTY}
	Should Be Equal As Strings	${rule["tcp_flag_syn"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_ack"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_fin"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_rst"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_urg"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_psh"]}	any
	Should Be Equal As Strings	${rule["source_udp_port"]}	${EMPTY}
	Should Be Equal As Strings	${rule["destination_udp_port"]}	${EMPTY}
	Should Be Equal As Strings	${rule["icmp_type"]}	any
	Should Be Equal As Strings	${rule["log_level"]}	debug
	Should Be Equal As Strings	${rule["log_prefix"]}	ZPE_PREFIX
	Should Not Be True	${rule["reverse_match_for_source_ip/mask"]}
	Should Not Be True	${rule["reverse_match_for_destination_ip/mask"]}
	Should Not Be True	${rule["reverse_match_for_input_interface"]}
	Should Not Be True	${rule["reverse_match_for_output_interface"]}
	Should Not Be True	${rule["enable_state_match"]}
	Should Not Be True	${rule["reverse_state_match"]}
	Should Not Be True	${rule["reverse_match_for_tcp_flags"]}
	Should Not Be True	${rule["reverse_match_for_icmp_type"]}
	Should Not Be True	${rule["reverse_match_for_protocol"]}
	Should Not Be True	${rule["reverse_match_for_source_port"]}
	Should Not Be True	${rule["reverse_match_for_destination_port"]}
	Should Not Be True	${rule["log_tcp_sequence_numbers"]}
	Should Not Be True	${rule["log_options_from_the_tcp_packet_header"]}
	Should Not Be True	${rule["log_options_from_the_ip_packet_header"]}

Update Chain Rule
	${rules}=	API::Get Security Firewall Chain Rules	${CHAIN}:${TYPE}
	${count}=	Get Length	${rules}

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	target=LOG
	Set To Dictionary	${payload}	reject_with=icmp6-no-route
	Set To Dictionary	${payload}	source_net6=${EMPTY}
	Set To Dictionary	${payload}	destination_net6=${EMPTY}
	Set To Dictionary	${payload}	input_interface=any
	Set To Dictionary	${payload}	output_interface=any
	Set To Dictionary	${payload}	state_match=NEW
	Set To Dictionary	${payload}	fragments=all
	Set To Dictionary	${payload}	protocol=numeric
	Set To Dictionary	${payload}	protocol_number=${EMPTY}
	Set To Dictionary	${payload}	source_port=${EMPTY}
	Set To Dictionary	${payload}	destination_port=${EMPTY}
	Set To Dictionary	${payload}	tcp_flag_syn=any
	Set To Dictionary	${payload}	tcp_flag_ack=any
	Set To Dictionary	${payload}	tcp_flag_fin=any
	Set To Dictionary	${payload}	tcp_flag_rst=any
	Set To Dictionary	${payload}	tcp_flag_urg=any
	Set To Dictionary	${payload}	tcp_flag_psh=any
	Set To Dictionary	${payload}	source_udp_port=${EMPTY}
	Set To Dictionary	${payload}	destination_udp_port=${EMPTY}
	Set To Dictionary	${payload}	icmp_type=any
	Set To Dictionary	${payload}	log_level=debug
	Set To Dictionary	${payload}	log_prefix=ZPE_PREFIX2
	Set To Dictionary	${payload}	reverse_match_for_source_ip/mask=${FALSE}}
	Set To Dictionary	${payload}	reverse_match_for_destination_ip/mask=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_input_interface=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_output_interface=${FALSE}
	Set To Dictionary	${payload}	enable_state_match=${FALSE}
	Set To Dictionary	${payload}	reverse_state_match=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_tcp_flags=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_icmp_type=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_protocol=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_source_port=${FALSE}
	Set To Dictionary	${payload}	reverse_match_for_destination_port=${FALSE}
	Set To Dictionary	${payload}	log_tcp_sequence_numbers=${FALSE}
	Set To Dictionary	${payload}	log_options_from_the_tcp_packet_header=${FALSE}
	Set To Dictionary	${payload}	log_options_from_the_ip_packet_header=${FALSE}

	API::Update Security Firewall Chain Rule	${CHAIN}:${TYPE}	${count-1}	${payload}

Get Chain Rule After Update
	${rules}=	API::Get Security Firewall Chain Rules	${CHAIN}:${TYPE}
	${count}=	Get Length	${rules}
	${rule}=	API::Get Security Firewall Chain Rule	${CHAIN}:${TYPE}	${count-1}

	Should Be Equal As Strings	${rule["target"]}	LOG
	Should Be Equal As Strings	${rule["reject_with"]}	icmp6-no-route
	Should Be Equal As Strings	${rule["source_net6"]}	${EMPTY}
	Should Be Equal As Strings	${rule["destination_net6"]}	${EMPTY}
	Should Be Equal As Strings	${rule["input_interface"]}	any
	Should Be Equal As Strings	${rule["output_interface"]}	any
	Should Be Equal As Strings	${rule["state_match"]}	NEW
	Should Be Equal As Strings	${rule["fragments"]}	all
	Should Be Equal As Strings	${rule["protocol"]}	numeric
	Should Be Equal As Strings	${rule["protocol_number"]}	${EMPTY}
	Should Be Equal As Strings	${rule["source_port"]}	${EMPTY}
	Should Be Equal As Strings	${rule["destination_port"]}	${EMPTY}
	Should Be Equal As Strings	${rule["tcp_flag_syn"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_ack"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_fin"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_rst"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_urg"]}	any
	Should Be Equal As Strings	${rule["tcp_flag_psh"]}	any
	Should Be Equal As Strings	${rule["source_udp_port"]}	${EMPTY}
	Should Be Equal As Strings	${rule["destination_udp_port"]}	${EMPTY}
	Should Be Equal As Strings	${rule["icmp_type"]}	any
	Should Be Equal As Strings	${rule["log_level"]}	debug
	Should Be Equal As Strings	${rule["log_prefix"]}	ZPE_PREFIX2
	Should Not Be True	${rule["reverse_match_for_source_ip/mask"]}
	Should Not Be True	${rule["reverse_match_for_destination_ip/mask"]}
	Should Not Be True	${rule["reverse_match_for_input_interface"]}
	Should Not Be True	${rule["reverse_match_for_output_interface"]}
	Should Not Be True	${rule["enable_state_match"]}
	Should Not Be True	${rule["reverse_state_match"]}
	Should Not Be True	${rule["reverse_match_for_tcp_flags"]}
	Should Not Be True	${rule["reverse_match_for_icmp_type"]}
	Should Not Be True	${rule["reverse_match_for_protocol"]}
	Should Not Be True	${rule["reverse_match_for_source_port"]}
	Should Not Be True	${rule["reverse_match_for_destination_port"]}
	Should Not Be True	${rule["log_tcp_sequence_numbers"]}
	Should Not Be True	${rule["log_options_from_the_tcp_packet_header"]}
	Should Not Be True	${rule["log_options_from_the_ip_packet_header"]}


Delete Chain Rule
	${rules}=	API::Get Security Firewall Chain Rules	${CHAIN}:${TYPE}
	${count}=	Get Length	${rules}

	API::Delete Security Firewall Chain Rule	${CHAIN}:${TYPE}	${count-1}

	${rules}=	API::Get Security Firewall Chain Rules	${CHAIN}:${TYPE}
	Length Should Be	${rules}	${count-1}


Delete Firewall Chain
	${chains}=	API::Get Security Firewall Chain
	${count}=	Get Length	${chains}

	API::Delete Security Firewall Chain	${CHAIN}:${TYPE}

	${chains}=	API::Get Security Firewall Chain
	Length Should Be	${chains}	${count-1}


*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin

Suite Teardown
	#API::Delete Security Firewall Chain	${CHAIN}:${TYPE}
	API::Delete::Session
