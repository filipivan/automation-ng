*** Settings ***
Documentation	Test /api/<version>/security/services endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Get Security Services
	${info}=	API::Get Security Services
	Check Required Fields	${info}
	Should Not Be True	${info["ssh_allow_root_access"]}
	Should Not Be True	${info["block_host_with_multiple_authentication_fails"]}
	Should Not Be True	${info["device_access_per_user_group_authorization"]}
	Should Be True	${info["auto_cloud_enroll"]}
	Should Be Equal	${info["period_host_will_stay_blocked"]}	10
	Should Be Equal	${info["cloud_tcp_port"]}	9966
	Should Be True	${info["enable_autodiscovery"]}
	Should Not Be True	${info["dhcp_lease_per_autodiscovery_rules"]}
	Should Be Equal	${info["search_engine_tcp_port"]}	9300
	Should Not Be True	${info["enable_search_engine_high_level_cipher_suite"]}
	Should Be Equal	${info["timeframe_to_monitor_authentication_fails"]}	10
	Should Not Be True	${info["enable_ftp_service"]}
	Should Be True	${info["enable_http_access"]}
	Should Be Equal	${info["http_port"]}	80
	Should Be True	${info["enable_https_access"]}
	Should Be Equal	${info["cipher_suite_level"]}	medium
	Should Be Equal	${info["custom_cipher_suite"]}	${EMPTY}
	Should Not Be True	${info["tlsv1"]}
	Should Be True	${info["tlsv1.1"]}
	Should Be True	${info["tlsv1.2"]}
	Should Be Equal	${info["https_port"]}	443
	Should Be True	${info["enable_icmp_echo_reply"]}
	Should Be Equal	${info["number_of_authentication_fails_to_block_host"]}	5
	Should Be True	${info["enable_pxe"]}
	Should Be True	${info["redirect_http_to_https"]}
	Should Not Be True	${info["rescue_mode_require_authentication"]}
	Should Not Be True	${info["enable_rpc"]}
	Should Be Equal	${info["enable_snmp_service"]}	${TRUE}
	Should Be Equal	${info["ssh_ciphers"]}	${EMPTY}
	Should Be Equal	${info["ssh_kexalgorithms"]}	${EMPTY}
	Should Be Equal	${info["ssh_macs"]}	${EMPTY}
	Should Be Equal	${info["ssh_tcp_port"]}	22
	Should Not Be True	${info["enable_telnet_service_to_nodegrid"]}
	Should Not Be True	${info["enable_telnet_service_to_managed_devices"]}
	Should Be Equal	${info["telnet_tcp_port"]}	23
	Should Be Equal	${info["enable_detection_of_usb_devices"]}	${TRUE}
	Should Not Be True	${info["enable_usb_over_ip"]}
	Should Be Equal	${info["vmotion_timeout"]}	300
	Should Be True	${info["enable_vm_serial_access"]}
	Should Be Equal	${info["vm_serial_port"]}	9977
	Should Be True	${info["enable_zero_touch_provisioning"]}

Update Security Services
	${random}=	Generate Random String
	Set Suite Variable	${random}
	${payload}=	Get Default Payload
	Set To Dictionary	${payload}	auto_cloud_enroll=${FALSE}
	Set To Dictionary	${payload}	enable_search_engine_high_level_cipher_suite=${TRUE}
	Set To Dictionary	${payload}	enable_ftp_service=${TRUE}
	Set To Dictionary	${payload}	enable_icmp_echo_reply=${FALSE}
	Set To Dictionary	${payload}	enable_pxe=${FALSE}
	Set To Dictionary	${payload}	enable_rpc=${TRUE}
	Set To Dictionary	${payload}	enable_snmp_service=${FALSE}
	Set To Dictionary	${payload}	enable_telnet_service_to_nodegrid=${TRUE}
	Set To Dictionary	${payload}	enable_telnet_service_to_managed_devices=${TRUE}
	Set To Dictionary	${payload}	enable_detection_of_usb_devices=${FALSE}
	Set To Dictionary	${payload}	enable_usb_over_ip=${TRUE}
	Set To Dictionary	${payload}	enable_vm_serial_access=${FALSE}
	Set To Dictionary	${payload}	enable_zero_touch_provisioning=${FALSE}
	API::Update Security Services	${payload}

Get Security Services After Update
	${info}=	API::Get Security Services
	Check Required Fields	${info}
	Should Not Be True	${info["auto_cloud_enroll"]}
	Should Be True	${info["enable_search_engine_high_level_cipher_suite"]}
	Should Be True	${info["enable_ftp_service"]}
	Should Not Be True	${info["enable_icmp_echo_reply"]}
	Should Not Be True	${info["enable_pxe"]}
	Should Be True	${info["enable_rpc"]}
	Should Not Be True	${info["enable_snmp_service"]}
	Should Be True	${info["enable_telnet_service_to_nodegrid"]}
	Should Be True	${info["enable_telnet_service_to_managed_devices"]}
	Should Not Be True	${info["enable_detection_of_usb_devices"]}
	Should Be True	${info["enable_usb_over_ip"]}
	Should Not Be True	${info["enable_vm_serial_access"]}
	Should Not Be True	${info["enable_zero_touch_provisioning"]}

*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin
	Reset Values

Suite Teardown
	Reset Values
	API::Delete::Session

Reset Values
	${payload}=	Get Default Payload
	API::Update Security Services	${payload}


Get Default Payload
	${info}=	Create Dictionary
	Set To Dictionary	${info}	ssh_allow_root_access=${FALSE}
	Set To Dictionary	${info}	block_host_with_multiple_authentication_fails=${FALSE}
	Set To Dictionary	${info}	device_access_per_user_group_authorization=${FALSE}
	Set To Dictionary	${info}	auto_cloud_enroll=${TRUE}
	Set To Dictionary	${info}	period_host_will_stay_blocked=10
	Set To Dictionary	${info}	cloud_tcp_port=9966
	Set To Dictionary	${info}	enable_autodiscovery=${TRUE}
	Set To Dictionary	${info}	dhcp_lease_per_autodiscovery_rules=${FALSE}
	Set To Dictionary	${info}	search_engine_tcp_port=9300
	Set To Dictionary	${info}	enable_search_engine_high_level_cipher_suite=${FALSE}
	Set To Dictionary	${info}	timeframe_to_monitor_authentication_fails=10
	Set To Dictionary	${info}	enable_ftp_service=${FALSE}
	Set To Dictionary	${info}	enable_http_access=${TRUE}
	Set To Dictionary	${info}	http_port=80
	Set To Dictionary	${info}	enable_https_access=${TRUE}
	Set To Dictionary	${info}	cipher_suite_level=medium
	Set To Dictionary	${info}	custom_cipher_suite=${EMPTY}
	Set To Dictionary	${info}	tlsv1=${FALSE}
	Set To Dictionary	${info}	tlsv1.1=${TRUE}
	Set To Dictionary	${info}	tlsv1.2=${TRUE}
	Set To Dictionary	${info}	https_port=443
	Set To Dictionary	${info}	enable_icmp_echo_reply=${TRUE}
	Set To Dictionary	${info}	number_of_authentication_fails_to_block_host=5
	Set To Dictionary	${info}	enable_pxe=${TRUE}
	Set To Dictionary	${info}	redirect_http_to_https=${TRUE}
	Set To Dictionary	${info}	rescue_mode_require_authentication=${FALSE}
	Set To Dictionary	${info}	enable_rpc=${FALSE}
	Set To Dictionary	${info}	enable_snmp_service=${TRUE}
	Set To Dictionary	${info}	ssh_ciphers=${EMPTY}
	Set To Dictionary	${info}	ssh_kexalgorithms=${EMPTY}
	Set To Dictionary	${info}	ssh_macs=${EMPTY}
	Set To Dictionary	${info}	ssh_tcp_port=22
	Set To Dictionary	${info}	enable_telnet_service_to_nodegrid=${FALSE}
	Set To Dictionary	${info}	enable_telnet_service_to_managed_devices=${FALSE}
	Set To Dictionary	${info}	telnet_tcp_port=23
	Set To Dictionary	${info}	enable_detection_of_usb_devices=${TRUE}
	Set To Dictionary	${info}	enable_usb_over_ip=${FALSE}
	Set To Dictionary	${info}	vmotion_timeout=300
	Set To Dictionary	${info}	enable_vm_serial_access=${TRUE}
	Set To Dictionary	${info}	vm_serial_port=9977
	Set To Dictionary	${info}	enable_zero_touch_provisioning=${TRUE}
	[return]	${info}

Check Required Fields
	[Arguments]	${info}
	${keys}=	Get Dictionary Keys	${info}
	Length Should Be	${keys}	41
	Dictionary Should Contain Key	${info}	ssh_allow_root_access
	Dictionary Should Contain Key	${info}	block_host_with_multiple_authentication_fails
	Dictionary Should Contain Key	${info}	device_access_per_user_group_authorization
	Dictionary Should Contain Key	${info}	auto_cloud_enroll
	Dictionary Should Contain Key	${info}	period_host_will_stay_blocked
	Dictionary Should Contain Key	${info}	cloud_tcp_port
	Dictionary Should Contain Key	${info}	enable_autodiscovery
	Dictionary Should Contain Key	${info}	dhcp_lease_per_autodiscovery_rules
	Dictionary Should Contain Key	${info}	search_engine_tcp_port
	Dictionary Should Contain Key	${info}	enable_search_engine_high_level_cipher_suite
	Dictionary Should Contain Key	${info}	timeframe_to_monitor_authentication_fails
	Dictionary Should Contain Key	${info}	enable_ftp_service
	Dictionary Should Contain Key	${info}	enable_http_access
	Dictionary Should Contain Key	${info}	http_port
	Dictionary Should Contain Key	${info}	enable_https_access
	Dictionary Should Contain Key	${info}	cipher_suite_level
	Dictionary Should Contain Key	${info}	custom_cipher_suite
	Dictionary Should Contain Key	${info}	tlsv1
	Dictionary Should Contain Key	${info}	tlsv1.1
	Dictionary Should Contain Key	${info}	tlsv1.2
	Dictionary Should Contain Key	${info}	https_port
	Dictionary Should Contain Key	${info}	enable_icmp_echo_reply
	Dictionary Should Contain Key	${info}	number_of_authentication_fails_to_block_host
	Dictionary Should Contain Key	${info}	enable_pxe
	Dictionary Should Contain Key	${info}	redirect_http_to_https
	Dictionary Should Contain Key	${info}	rescue_mode_require_authentication
	Dictionary Should Contain Key	${info}	enable_rpc
	Dictionary Should Contain Key	${info}	enable_snmp_service
	Dictionary Should Contain Key	${info}	ssh_ciphers
	Dictionary Should Contain Key	${info}	ssh_kexalgorithms
	Dictionary Should Contain Key	${info}	ssh_macs
	Dictionary Should Contain Key	${info}	ssh_tcp_port
	Dictionary Should Contain Key	${info}	enable_telnet_service_to_nodegrid
	Dictionary Should Contain Key	${info}	enable_telnet_service_to_managed_devices
	Dictionary Should Contain Key	${info}	telnet_tcp_port
	Dictionary Should Contain Key	${info}	enable_detection_of_usb_devices
	Dictionary Should Contain Key	${info}	enable_usb_over_ip
	Dictionary Should Contain Key	${info}	vmotion_timeout
	Dictionary Should Contain Key	${info}	enable_vm_serial_access
	Dictionary Should Contain Key	${info}	vm_serial_port
	Dictionary Should Contain Key	${info}	enable_zero_touch_provisioning
