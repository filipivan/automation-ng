*** Settings ***
Documentation	Test /api/<version>/security/localaccounts endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***


Add Local Account
	${payload}=	Create Dictionary
	Set Suite Variable	${uName}	dummyuser
	Set To Dictionary	${payload}	username=${uName}
	Set To Dictionary	${payload}	password=${uName}
	Set To Dictionary	${payload}	confirm_password=${uName}
	${expire_date}=	Get Current Date
	${expire_date}=	Add Time To Date	${expire_date}	1 days	result_format=%Y-%m-%d
	Set To Dictionary	${payload}	account_expiration-date=${expire_date}
	Set To Dictionary	${payload}	hash_format_password=${FALSE}
	Set To Dictionary	${payload}	password_change_at_login=${FALSE}
	${groups}=	Create List	admin	user
	Set To Dictionary	${payload}	user_group=${groups}
	API::Add Local Account	${payload}
	${accounts}=	API::Get Security Local Accounts
	Length Should Be	${accounts}	${accounts_count+1}	New local account not found

Get Local Accounts Information
	${accounts}=	API::Get Security Local Accounts
	:FOR	${account}	IN	@{accounts}
	\	${keys}=	Get Dictionary Keys	${account}
	\	Length Should Be	${keys}	4
	\	Dictionary Should Contain Key	${account}	id
	\	Dictionary Should Contain Key	${account}	user_group
	\	Dictionary Should Contain Key	${account}	username
	\	Dictionary Should Contain Key	${account}	state

Get Local Account
	${account}=	API::Get Security Local Account	${uName}
	Should Not Be True	${account["password_change_at_login"]}
	${groups}=	Create List	admin	user
	Lists Should Be Equal	${groups}	${account["user_group"]["value"]}

Update Local Account
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	password_change_at_login=${TRUE}
	${groups}=	Create List	admin
	Set To Dictionary	${payload}	user_group=${groups}
	API::Update Security Local Account	${uName}	${payload}

Get Local Account After Update
	${account}=	API::Get Security Local Account	${uName}
	Should Be True	${account["password_change_at_login"]}
	${groups}=	Create List	admin
	Lists Should Be Equal	${groups}	${account["user_group"]["value"]}

Delete Local Account
	API::Delete Local Account	${uName}
	${accounts}=	API::Get Security Local Accounts
	Length Should Be	${accounts}	${accounts_count}

*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin

	${accounts}=	API::Get Security Local Accounts
	${accounts_count}=	Get Length	${accounts}
	Set Suite Variable	${accounts_count}

Suite Teardown
	#Reset Values
	API::Delete::Session

Reset Values
	API::Delete Local Account	${uName}
