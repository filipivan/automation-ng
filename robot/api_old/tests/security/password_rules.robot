*** Settings ***
Documentation	Test /api/<version>/security/passwordrules endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Get Password Rules Information
	${info}=	API::Get Password Rules
	${keys}=	Get Dictionary Keys	${info}
	Dictionary Should Contain Key	${info}	check_password_complexity
	Dictionary Should Contain Key	${info}	pwd_expiration_max_days
	Dictionary Should Contain Key	${info}	pwd_expiration_min_days
	Dictionary Should Contain Key	${info}	min_digits
	Dictionary Should Contain Key	${info}	minimum_size
	Dictionary Should Contain Key	${info}	min_special_characters
	Dictionary Should Contain Key	${info}	min_upper_case_characters
	Dictionary Should Contain Key	${info}	passwords_in_history
	Dictionary Should Contain Key	${info}	pwd_expiration_warning_days
	Length Should Be	${keys}	9

	Should Be Equal As Strings	${info["pwd_expiration_max_days"]}	99999
	Should Be Equal As Strings	${info["pwd_expiration_min_days"]}	0
	Should Be Equal As Strings	${info["min_digits"]}	0
	Should Be Equal As Strings	${info["minimum_size"]}	8
	Should Be Equal As Strings	${info["min_special_characters"]}	0
	Should Be Equal As Strings	${info["min_upper_case_characters"]}	0
	Should Be Equal As Strings	${info["passwords_in_history"]}	1
	Should Be Equal As Strings	${info["pwd_expiration_warning_days"]}	7

Update Password Rules Information
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	check_password_complexity=${TRUE}
	Set To Dictionary	${payload}	pwd_expiration_max_days=88888
	Set To Dictionary	${payload}	pwd_expiration_min_days=2
	Set To Dictionary	${payload}	min_digits=2
	Set To Dictionary	${payload}	minimum_size=6
	Set To Dictionary	${payload}	min_special_characters=2
	Set To Dictionary	${payload}	min_upper_case_characters=2
	Set To Dictionary	${payload}	passwords_in_history=2
	Set To Dictionary	${payload}	pwd_expiration_warning_days=2
	API::Update Password Rules	${payload}


Get Password Rules Information After Updated
	${info}=	API::Get Password Rules
	${keys}=	Get Dictionary Keys	${info}
	Length Should Be	${keys}	9
	Should Be True	${info["check_password_complexity"]}
	Should Be Equal As Strings	${info["pwd_expiration_max_days"]}	88888
	Should Be Equal As Strings	${info["pwd_expiration_min_days"]}	2
	Should Be Equal As Strings	${info["min_digits"]}	2
	Should Be Equal As Strings	${info["minimum_size"]}	6
	Should Be Equal As Strings	${info["min_special_characters"]}	2
	Should Be Equal As Strings	${info["min_upper_case_characters"]}	2
	Should Be Equal As Strings	${info["passwords_in_history"]}	2
	Should Be Equal As Strings	${info["pwd_expiration_warning_days"]}	2

*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin
	Reset Values

Suite Teardown
	Reset Values
	API::Delete::Session

Reset Values
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	check_password_complexity=${TRUE}
	Set To Dictionary	${payload}	pwd_expiration_max_days=99999
	Set To Dictionary	${payload}	pwd_expiration_min_days=0
	Set To Dictionary	${payload}	min_digits=0
	Set To Dictionary	${payload}	minimum_size=8
	Set To Dictionary	${payload}	min_special_characters=0
	Set To Dictionary	${payload}	min_upper_case_characters=0
	Set To Dictionary	${payload}	passwords_in_history=1
	Set To Dictionary	${payload}	pwd_expiration_warning_days=7
	API::Update Password Rules	${payload}
