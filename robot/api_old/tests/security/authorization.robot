*** Settings ***
Documentation	Test /api/<version>/security/authorization endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Suite Setup
Suite Teardown	Teardown Test Suite

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Variables ***
${dummy_device}=	self
${dummy_pdu}=	${PDU["name"]}
${uGroup}=	test
${dummy_user}=	dummy

*** Test Cases ***

Adds Security Authorization
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	group=${uGroup}
	API::Add Authorization	${payload}

	${authorization}=	API::Get Security Authorization
	Length Should Be	${authorization}	${ORIGINAL_COUNT+1}

Get Authorization Information
	${authorization}=	API::Get Security Authorization
	:FOR	${row}	IN	@{authorization}
	\	${keys}=	Get Dictionary Keys	${row}
	\	Length Should Be	${keys}	2
	\	Dictionary Should Contain Key	${row}	id
	\	Dictionary Should Contain Key	${row}	name

Get Security Authorization Members
	${members}=	API::Get Security Authorization Members	admin
	${MEMBERS_COUNT}=	Get Length	${members}
	Set Suite Variable	${MEMBERS_COUNT}
	:FOR	${member}	IN	@{members}
	\	Dictionary Should Contain Key	${member}	members
	\	Dictionary Should Contain Key	${member}	id
	\	Length Should Be	${member}	2

Add Security Authorization Member
	[Setup]	Add Dummy Local Account
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	remote_users=remote
	Set To Dictionary	${payload}	local_users=${dummy_user}
	API::Add Security Authorization Member	admin	${payload}

	${members}=	API::Get Security Authorization Members	admin
	Length Should Be	${members}	${MEMBERS_COUNT+2}

Delete Security Authorization Member
	API::Delete Security Authorization Member	admin	${dummy_user}
	API::Delete Security Authorization Member	admin	remote

	${members}=	API::Get Security Authorization Members	admin
	${count}=	Get Length	${members}
	Should Be Equal As Integers	${count}	${MEMBERS_COUNT}

Get Security Authorization Profile
	${profile}=	API::Get Security Authorization Profile	admin
	Dictionary Should Contain Key	${profile}	custom_session_timeout
	Dictionary Should Contain Key	${profile}	timeout
	Dictionary Should Contain Key	${profile}	menu-driven_access_to_devices
	Dictionary Should Contain Key	${profile}	email_events_to
	Dictionary Should Contain Key	${profile}	permissions
	Dictionary Should Contain Key	${profile}	restrict_configure_system_permission_to_read_only
	Length Should Be	${profile}	6

Get Security Authorization Remote Groups
	${remote_group}=	API::Get Security Authorization Remote Groups	admin
	Length Should Be	${remote_group}	1
	Dictionary Should Contain Key	${remote_group}	remote_groups


Add Security Authorization Devices
	[Setup]	Add Dummy Device
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	door=DoorStatus
	Set To Dictionary	${payload}	power=PowerStatus
	Set To Dictionary	${payload}	session=ReadOnly
	Set To Dictionary	${payload}	access_log_audit=${TRUE}
	Set To Dictionary	${payload}	access_log_clear=${TRUE}
	Set To Dictionary	${payload}	event_log_audit=${TRUE}
	Set To Dictionary	${payload}	event_log_clear=${TRUE}
	Set To Dictionary	${payload}	kvm=${TRUE}
	Set To Dictionary	${payload}	mks=${TRUE}
	Set To Dictionary	${payload}	monitoring=${TRUE}
	Set To Dictionary	${payload}	reset_device=${TRUE}
	Set To Dictionary	${payload}	sp_console=${TRUE}
	Set To Dictionary	${payload}	tunnel=${TRUE}
	Set To Dictionary	${payload}	sensors_data=${TRUE}
	Set To Dictionary	${payload}	virtual_media=${TRUE}
	${devices}=	Create List	${dummy_device}
	Set To Dictionary	${payload}	outlets=${devices}

	${devices}=	API::Get Security Authorization Devices	${uGroup}
	${DEVICES_COUNT}=	Get Length	${devices}
	Set Suite Variable	${DEVICES_COUNT}
	API::Add Security Authorization Devices	${uGroup}	${payload}

	${devices}=	API::Get Security Authorization Devices	${uGroup}
	Length Should Be	${devices}	${DEVICES_COUNT+1}

Get Security Authorization Devices
	${devices}=	API::Get Security Authorization Devices	admin
	:FOR	${device}	IN	@{devices}
	\	Length Should Be	${device}	14
	\	Dictionary Should Contain Key	${device}	id
	\	Dictionary Should Contain Key	${device}	access_log
	\	Dictionary Should Contain Key	${device}	door_mode
	\	Dictionary Should Contain Key	${device}	event_log
	\	Dictionary Should Contain Key	${device}	kvm
	\	Dictionary Should Contain Key	${device}	mks
	\	Dictionary Should Contain Key	${device}	monitoring
	\	Dictionary Should Contain Key	${device}	targets
	\	Dictionary Should Contain Key	${device}	power_mode
	\	Dictionary Should Contain Key	${device}	session_mode
	\	Dictionary Should Contain Key	${device}	reset_device
	\	Dictionary Should Contain Key	${device}	sp_console
	\	Dictionary Should Contain Key	${device}	sensors_data
	\	Dictionary Should Contain Key	${device}	virtual_media

Get Security Authorization Device
	Sleep	5 seconds
	${device}=	API::Get Security Authorization Device	${uGroup}	${dummy_device}
	Should Be Equal As Strings	${device["door"]}	NoAccess
	Should Be Equal As Strings	${device["power"]}	NoAccess
	Should Be Equal As Strings	${device["session"]}	ReadOnly
	Should Be Equal As Strings	${device["targets"]}	${dummy_device}
	Should Be True	${device["access_log_audit"]}
	Should Be True	${device["access_log_clear"]}
	Should Be True	${device["tunnel"]}
	Should Be True	${device["monitoring"]}
	Should Not Be True	${device["event_log_audit"]}
	Should Not Be True	${device["event_log_clear"]}
	Should Not Be True	${device["kvm"]}
	Should Not Be True	${device["mks"]}
	Should Not Be True	${device["reset_device"]}
	Should Not Be True	${device["sp_console"]}
	Should Not Be True	${device["sensors_data"]}
	Should Not Be True	${device["virtual_media"]}
	Length Should Be	${device}	17


Update Security Authorization Device
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	session=NoAccess
	Set To Dictionary	${payload}	power=NoAccess
	Set To Dictionary	${payload}	door=NoAccess
	Set To Dictionary	${payload}	mks=${FALSE}
	Set To Dictionary	${payload}	kvm=${FALSE}
	Set To Dictionary	${payload}	tunnel=${FALSE}
	Set To Dictionary	${payload}	sp_console=${FALSE}
	Set To Dictionary	${payload}	reset_device=${FALSE}
	Set To Dictionary	${payload}	virtual_media=${FALSE}
	Set To Dictionary	${payload}	access_log_audit=${FALSE}
	Set To Dictionary	${payload}	access_log_clear=${FALSE}
	Set To Dictionary	${payload}	event_log_audit=${FALSE}
	Set To Dictionary	${payload}	event_log_clear=${FALSE}
	Set To Dictionary	${payload}	sensors_data=${FALSE}
	Set To Dictionary	${payload}	monitoring=${FALSE}

	API::Update Security Authorization Device	${uGroup}	${dummy_device}	${payload}

Get Security Authorization Device After Update
	${device}=	API::Get Security Authorization Device	${uGroup}	${dummy_device}
	Should Be Equal As Strings	${device["door"]}	NoAccess
	Should Be Equal As Strings	${device["power"]}	NoAccess
	Should Be Equal As Strings	${device["session"]}	NoAccess
	Should Be Equal As Strings	${device["targets"]}	${dummy_device}
	Should Not Be True	${device["access_log_audit"]}
	Should Not Be True	${device["access_log_clear"]}
	Should Not Be True	${device["event_log_audit"]}
	Should Not Be True	${device["event_log_clear"]}
	Should Not Be True	${device["kvm"]}
	Should Not Be True	${device["mks"]}
	Should Not Be True	${device["monitoring"]}
	Should Not Be True	${device["reset_device"]}
	Should Not Be True	${device["sp_console"]}
	Should Not Be True	${device["tunnel"]}
	Should Not Be True	${device["sensors_data"]}
	Should Not Be True	${device["virtual_media"]}

Delete Security Authorization Devices
	API::Delete Security Authorization Devices	${uGroup}	${dummy_device}

	${devices}=	API::Get Security Authorization Devices	${uGroup}
	Length Should Be	${devices}	${DEVICES_COUNT}

Add Security Authorization Outlet
	[Setup]	Add Dummy PDU
	${payload}=	Create Dictionary
	${outlets}=	Create List	${dummy_outlet}
	Set To Dictionary	${payload}	outlets=${outlets}
	Set To Dictionary	${payload}	power=PowerStatus

	${outlets}=	API::Get Security Authorization Outlets	${uGroup}
	${OUTLETS_COUNT}=	Get Length	${outlets}
	Set Suite Variable	${OUTLETS_COUNT}
	API::Add Security Authorization Outlet	${uGroup}	${payload}

	Wait Until ${uGroup} User Group Outlet Is ${OUTLETS_COUNT+1}

	#${outlets}=	API::Get Security Authorization Outlets	${uGroup}
	#Length Should Be	${outlets}	${OUTLETS_COUNT+1}

Get Security Authorization Outlets
	${outlets}=	API::Get Security Authorization Outlets	admin
	:FOR	${outlet}	IN	@{outlets}
	\	Dictionary Should Contain Key	${outlet}	device
	\	Dictionary Should Contain Key	${outlet}	power_mode
	\	Dictionary Should Contain Key	${outlet}	outlet_id
	\	Dictionary Should Contain Key	${outlet}	id
	\	Dictionary Should Contain Key	${outlet}	pdu_id
	\	Length Should Be	${outlet}	5


Get Security Authorization Outlet
	${outlet}=	API::Get Security Authorization Outlet	${uGroup}	${dummy_outlet}
	Should Be Equal As Strings	${outlet["power"]}	PowerStatus
	Should Be Equal As Strings	${outlet["targets"]}	${dummy_outlet}

Update Security Authorization Outlet

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	power=NoAccess
	API::Update Security Authorization Outlet	${uGroup}	${dummy_outlet}	${payload}

Get Security Authorization Outlet After Update
	${outlet}=	API::Get Security Authorization Outlet	${uGroup}	${dummy_outlet}
	Should Be Equal As Strings	${outlet["power"]}	NoAccess
	Should Be Equal As Strings	${outlet["targets"]}	${dummy_outlet}

Delete Security Authorization Outlet
	API::Delete Security Authorization Outlet	${uGroup}	${dummy_outlet}

	${outlets}=	API::Get Security Authorization Devices	${uGroup}
	Length Should Be	${outlets}	${OUTLETS_COUNT}


Delete Security Authorization
	API::Delete Authorization	${uGroup}

	${authorization}=	API::Get Security Authorization
	Length Should Be	${authorization}	${ORIGINAL_COUNT}


*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin
	Reset Values
	${authorization}=	API::Get Security Authorization
	${ORIGINAL_COUNT}=	Get Length	${authorization}
	Set Suite Variable	${ORIGINAL_COUNT}

Teardown Test Suite
	#Reset Values
	API::Delete::Session

Reset Values
	API::Delete Security Authorization Member	admin	${dummy_user}
	API::Delete Security Authorization Member	admin	remote
	API::Delete Authorization	${uGroup}
	API::Delete Local Account	${dummy_user}
	API::Delete::Devices::Table	${dummy_device}
	API::Delete::Devices::Table	${dummy_pdu}

Add Dummy Local Account
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	dummy_user=${dummy_user}
	Set To Dictionary	${payload}	uPasswd=${dummy_user}
	Set To Dictionary	${payload}	cPasswd=${dummy_user}
	${expire_date}=	Get Current Date
	${expire_date}=	Add Time To Date	${expire_date}	1 days	result_format=%Y-%m-%d
	Set To Dictionary	${payload}	accExpireDate=${expire_date}
	Set To Dictionary	${payload}	hashPwd=${FALSE}
	Set To Dictionary	${payload}	pwForceChange=${FALSE}
	${groups}=	Create List	admin	user
	Set To Dictionary	${payload}	uGroup=${groups}
	API::Add Local Account	${payload}

Add Dummy Device
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	spm_name=${dummy_device}
	Set To Dictionary	${payload}	type=device_console
	Set To Dictionary	${payload}	phys_addr=${HOST}
	Set To Dictionary	${payload}	tcpport=22
	Set To Dictionary	${payload}	chassis=${EMPTY}
	Set To Dictionary	${payload}	blade=${EMPTY}
	Set To Dictionary	${payload}	username=admin
	Set To Dictionary	${payload}	askpassword=no
	Set To Dictionary	${payload}	passwordfirst=admin
	Set To Dictionary	${payload}	passwordconf=admin
	Set To Dictionary	${payload}	currentstatus=enabled
	API::Post::Devices::Table	${payload}

Add Dummy PDU
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	spm_name=${PDU["name"]}
	Set To Dictionary	${payload}	type=${PDU["type"]}
	Set To Dictionary	${payload}	phys_addr=${PDU["ip"]}
	Set To Dictionary	${payload}	tcpport=22
	Set To Dictionary	${payload}	chassis=${EMPTY}
	Set To Dictionary	${payload}	blade=${EMPTY}
	Set To Dictionary	${payload}	username=${PDU["username"]}
	Set To Dictionary	${payload}	askpassword=no
	Set To Dictionary	${payload}	passwordfirst=${PDU["password"]}
	Set To Dictionary	${payload}	passwordconf=${PDU["password"]}
	Set To Dictionary	${payload}	currentstatus=enabled
	API::Post::Devices::Table	${payload}
	Set Suite Variable	${dummy_outlet}	servertech:A:AA10
	Wait Until Keyword Succeeds	5 times	10 sec	PDU Should Have Outlets

PDU Should Have Outlets
	${outlets}=	API::Get::Devices::Table::${NAME} Outlets	${dummy_pdu}
	Should Not Be Empty	${outlets}
	:FOR	${outlet}	IN	@{outlets}
	\	 Dictionary Should Not Contain Key	${outlet}	error

Wait Until ${GROUP} User Group Outlet Is ${COUNT}
	Wait Until Keyword Succeeds	3 times	10 seconds	${GROUP} User Group Outlet Should Be ${COUNT}

${GROUP} User Group Outlet Should Be ${COUNT}
	${outlets}=	API::Get Security Authorization Outlets	${GROUP}
	Length Should Be	${outlets}	${COUNT}
