*** Settings ***
Documentation	Test /api/<version>/security/authentication endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Add authentication method
	${payload}=	Create Dictionary

	Set To Dictionary	${payload}	fallback_if_denied_access=${TRUE}
	Set To Dictionary	${payload}	method=radius
	Set To Dictionary	${payload}	remote_server=${HOST}
	Set To Dictionary	${payload}	status=enabled
	Set To Dictionary	${payload}	radius_accounting_server=server
	Set To Dictionary	${payload}	radius_retries=1
	Set To Dictionary	${payload}	radius_confirm_secret=secret
	Set To Dictionary	${payload}	radius_secret=secret
	Set To Dictionary	${payload}	radius_enable_servicetype=${FALSE}
	Set To Dictionary	${payload}	radius_timeout=1

	API::Add Authentication Method	${payload}

	${authorization}=	API::Get Security Authentication
	Check Authentication Fields	${authorization}
	Length Should Be	${authorization}	${ORIGINAL_COUNT+1}

Get Authentication Information
	${authorization}=	API::Get Security Authentication
	Check Authentication Fields	${authorization}

Get authentication method info
	${auth}=	API::Get Authentication Method Info	1
	Dictionary Should Contain Key	${auth}	radius_secret
	Dictionary Should Contain Key	${auth}	radius_confirm_secret
	Should Be Equal As Strings	${auth["method"]}	radius
	Should Be Equal As Strings	${auth["remote_server"]}	${HOST}
	Should Be Equal As Strings	${auth["status"]}	enabled
	Should Be Equal As Strings	${auth["radius_accounting_server"]}	server
	Should Be Equal As Strings	${auth["radius_retries"]}	1
	Should Be Equal As Strings	${auth["radius_timeout"]}	1
	Should Be True	${auth["fallback_if_denied_access"]}	${TRUE}
	Should Not Be True	${auth["radius_enable_servicetype"]}	${FALSE}

Update authentication method info
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	fallback_if_denied_access=${TRUE}
	Set To Dictionary	${payload}	method=radius
	Set To Dictionary	${payload}	remote_server=${HOST}
	Set To Dictionary	${payload}	status=enabled
	Set To Dictionary	${payload}	radius_accounting_server=serverr
	Set To Dictionary	${payload}	radius_retries=2
	Set To Dictionary	${payload}	radius_confirm_secret=secret
	Set To Dictionary	${payload}	radius_secret=secret
	Set To Dictionary	${payload}	radius_enable_servicetype=${FALSE}
	Set To Dictionary	${payload}	radius_timeout=2
	API::Update Authentication Method Info	1	${payload}

Get authentication method info After Update
	${auth}=	API::Get Authentication Method Info	1
	Dictionary Should Contain Key	${auth}	radius_secret
	Dictionary Should Contain Key	${auth}	radius_confirm_secret
	Should Be Equal As Strings	${auth["method"]}	radius
	Should Be Equal As Strings	${auth["remote_server"]}	${HOST}
	Should Be Equal As Strings	${auth["status"]}	enabled
	Should Be Equal As Strings	${auth["radius_accounting_server"]}	serverr
	Should Be Equal As Strings	${auth["radius_retries"]}	2
	Should Be Equal As Strings	${auth["radius_timeout"]}	2
	Should Be True	${auth["fallback_if_denied_access"]}	${TRUE}
	Should Not Be True	${auth["radius_enable_servicetype"]}	${FALSE}


Move authentication method down
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	fallback_if_denied_access=${TRUE}
	Set To Dictionary	${payload}	method=kerberos
	Set To Dictionary	${payload}	remote_server=${HOST}
	Set To Dictionary	${payload}	status=enabled
	Set To Dictionary	${payload}	kerberos_realm_domain_name=real
	Set To Dictionary	${payload}	kerberos_domain_name=domain
	API::Add Authentication Method	${payload}

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	fallback_if_denied_access=${TRUE}
	Set To Dictionary	${payload}	method=kerberos
	Set To Dictionary	${payload}	remote_server=127.0.0.1
	Set To Dictionary	${payload}	status=enabled
	Set To Dictionary	${payload}	kerberos_realm_domain_name=real
	Set To Dictionary	${payload}	kerberos_domain_name=domain
	API::Add Authentication Method	${payload}

	${authorization}=	API::Get Security Authentication
	Length Should Be	${authorization}	${ORIGINAL_COUNT+3}
	Check Authentication Fields	${authorization}
	Should Be Equal As Strings	${authorization[1]["remote_server"]}	${HOST}
	Should Be Equal As Strings	${authorization[2]["remote_server"]}	127.0.0.1

	API::Move Authentication Method Down	${authorization[1]["id"]}
	${authorization}=	API::Get Security Authentication
	Should Be Equal As Strings	${authorization[1]["remote_server"]}	127.0.0.1
	Should Be Equal As Strings	${authorization[2]["remote_server"]}	${HOST}

Move authentication method up
	${authorization}=	API::Get Security Authentication
	API::Move Authentication Method Up	${authorization[2]["id"]}
	${authorization}=	API::Get Security Authentication
	Check Authentication Fields	${authorization}
	Should Be Equal As Strings	${authorization[1]["remote_server"]}	${HOST}
	Should Be Equal As Strings	${authorization[2]["remote_server"]}	127.0.0.1

Delete authentication method
	API::Delete Authentication Method	1
	API::Delete Authentication Method	1
	API::Delete Authentication Method	1
	${authorization}=	API::Get Security Authentication
	Length Should Be	${authorization}	${ORIGINAL_COUNT}

*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin
	${authorization}=	API::Get Security Authentication
	${count}=	Get Length	${authorization}
	Set Suite Variable	${ORIGINAL_COUNT}	${count}

Suite Teardown
	API::Delete::Session

Check Authentication Fields
	[Arguments]	${authorization}
	:FOR	${row}	IN	@{authorization}
	\	${keys}=	Get Dictionary Keys	${row}
	\	Length Should Be	${keys}	6
	\	Dictionary Should Contain Key	${row}	id
	\	Dictionary Should Contain Key	${row}	fallback
	\	Dictionary Should Contain Key	${row}	method
	\	Dictionary Should Contain Key	${row}	index
	\	Dictionary Should Contain Key	${row}	remote_server
	\	Dictionary Should Contain Key	${row}	status

