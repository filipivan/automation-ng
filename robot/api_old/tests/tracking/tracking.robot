*** Settings ***
Documentation	Test /api/<version>/tracking endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot
Force Tags	API	${VERSION}
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${INVALID_SESSION}	1|admin|http|1.1.1.1|Sunday 2019|api|||
${INVALID_EVENT}	0
${INVALID_INTERFACE}	interface0|1|up|1100|0|10000|0|0|0|
${INVALID_USB}	2:4|1-2|1:1|modem|wdm0|Wirelles|Ch|
${SCAN_ID}	test_scan

*** Test Cases ***
Get Open Sessions
	${sessions}=	API::Get::Tracking::Open Sessions
	${sessions}=	Set Variable	${sessions.json()}
	:FOR	${session}	IN	@{sessions}
	\	Dictionary Should Contain Key	${session}	id
	\	Dictionary Should Contain Key	${session}	user
	\	Dictionary Should Contain Key	${session}	mode
	\	Dictionary Should Contain Key	${session}	source_ip
	\	Dictionary Should Contain Key	${session}	type
	\	Dictionary Should Contain Key	${session}	device_name
	\	Dictionary Should Contain Key	${session}	ref
	\	Dictionary Should Contain Key	${session}	session_start
	\	Length Should Be	${session}	8

Terminate One Valid Open Session
	[Setup]	API::Post::Session	MODE=no
	${sessions}=	API::Get::Tracking::Open Sessions
	${last_session}=	Get From List	${sessions.json()}	-1
	${id}=	Get From Dictionary	${last_session}	id

	${payload}=	API::JSON::Delete	sessions	${id}
	${response}=	API::Post::Tracking::Open Sessions::Terminate	${payload}
	${STATUS}=	Evaluate	200
	Should Be Equal	${response.status_code}	${STATUS}

	${sessions}=	API::Get::Tracking::Open Sessions
	${last_session}=	Get From List	${sessions.json()}	-1
	Dictionary Should Not Contain Value	${last_session}	${id}
	[Teardown]	Run Keyword If Test Failed	API::Delete::Session	-1	yes

Terminate Two Valid Open Session
	[Setup]	Run Keywords	API::Post::Session	MODE=no	AND	API::Post::Session	MODE=no
	${sessions}=	API::Get::Tracking::Open Sessions
	${first_session}=	Get From List	${sessions.json()}	-2
	${last_session}=	Get From List	${sessions.json()}	-1
	${first_id}=	Get From Dictionary	${first_session}	id
	${last_id}=	Get From Dictionary	${first_session}	id

	${payload}=	API::JSON::Delete	sessions	${first_id}	${last_id}
	${response}=	API::Post::Tracking::Open Sessions::Terminate	${payload}
	${STATUS}=	Evaluate	200
	Should Be Equal	${response.status_code}	${STATUS}

	${sessions}=	API::Get::Tracking::Open Sessions
	${first_session}=	Get From List	${sessions.json()}	-2
	${last_session}=	Get From List	${sessions.json()}	-1
	Dictionary Should Not Contain Value	${first_session}	${first_id}
	Dictionary Should Not Contain Value	${last_session}	${last_id}

Terminate Invalid Open Session
	[Tags]	NON-CRITICAL	BUG_API_05
	${payload}=	API::JSON::Delete	sessions	${INVALID_SESSION}
	${response}=	API::Post::Tracking::Open Sessions::Terminate	${payload}
	${status}=	Evaluate	200
#	Should Not Be Equal	${response.status_code}	${status}
	Dictionary Should Contain Value	${response.json()}	Error

Get Empty Devices Sessions
	${devices_sessions}=	API::Get::Tracking::Devices Sessions
	${empty_list}=	Create List
	Should Be Equal	${devices_sessions.json()}	${empty_list}
	Length Should Be	${devices_sessions.json()}	0

#Get Devices Sessions
#	[Setup]	SUITE:Create Device::Table and its Session
#	${devices_sessions}=	API::Get::Tracking::Devices Sessions
#	${devices_sessions}=	Set Variable	${devices_sessions.json()}
#	:FOR	${devices_sessions}	IN	@{devices_sessions}
#	\	${keys}=	Get Dictionary Keys	${devices_sessions}
#	\	Dictionary Should Contain Key	${devices_session}	id
#	\	Dictionary Should Contain Key	${devices_session}	users
#	\	Dictionary Should Contain Key	${devices_session}	device_name
#	\	Dictionary Should Contain Key	${devices_session}	number_of_sessions
#	\	Length Should Be	${keys}	4
#	[Teardown]	SUITE:Delete Device::Table and its Session
#
#Terminate One Device Session
#	[Setup]	SUITE:Create Device::Table and its Session
#	${devices_sessions}=	API::Get::Tracking::Devices Sessions
#	${last_session}=	Get From List	${devices_sessions.json()}	-1
#	${id}=	Get From Dictionary	${last_session}	id
#
#	${payload}=	API::JSON::Delete	devices_sessions	${id}
#	${response}=	API::Post::Tracking::Devices Sessions::Terminate	${payload}
#	${STATUS}=	Evaluate	200
#	Should Be Equal	${response.status_code}	${STATUS}
#	${all_sessions}=	API::Get::Tracking::Devices Sessions
#	${last_session}=	Get From List	${all_sessions.json()}	-1
#	Dictionary Should Not Contain Value	${last_session}	${id}
#	[Teardown]	SUITE:Delete Device::Table and its Session
#
# FIX IT
#Terminate Two Device Session
#	[Setup]	Run Keywords	SUITE:Create Device::Table and its Session	AND	SUITE:Create Device::Table and its Session
#	${devices_sessions}=	API::Get::Tracking::Devices Sessions
#	${first_session}=	Get From List	${devices_sessions.json()}	-2
#	${last_session}=	Get From List	${devices_sessions.json()}	-1
#	${first_id}=	Get From Dictionary	${first_session}	id
#	${last_id}=	Get From Dictionary	${last_session}	id
#
#	${payload}=	API::JSON::Delete	devices_sessions	${first_id}	${last_id}
#	${response}=	API::Post::Tracking::Devices Sessions::Terminate	${payload}
#	${STATUS}=	Evaluate	200
#	Should Be Equal	${response.status_code}	${STATUS}
#	${all_sessions}=	API::Get::Tracking::Devices Sessions
#	${first_session}=	Get From List	${devices_sessions.json()}	-2
#	${last_session}=	Get From List	${devices_sessions.json()}	-1
#	Dictionary Should Not Contain Value	${first_session}	${first_id}
#	Dictionary Should Not Contain Value	${last_session}	${last_id}
#	[Teardown]	Run Keywords	SUITE:Delete Device::Table and its Session	AND	SUITE:Delete Device::Table and its Session
#
#Terminate Invalid Device Session
#	[Setup]	SUITE:Create Device::Table and its Session
#	${devices_sessions}=	API::Get::Tracking::Devices Sessions
#	${last_session}=	Get From List	${devices_sessions.json()}	-1
#	${id}=	Get From Dictionary	${last_session}	id
#
#	${payload}=	API::JSON::Delete	devices_sessions	${id}
#	${response}=	API::Post::Tracking::Devices Sessions::Terminate	${payload}
#	${STATUS}=	Evaluate	200
#	Should Be Equal	${response.status_code}	${STATUS}
#	${all_sessions}=	API::Get::Tracking::Devices Sessions
#	${last_session}=	Get From List	${all_sessions.json()}	-1
#	Dictionary Should Not Contain Value	${last_session}	${id}
#	[Teardown]	SUITE:Delete Device::Table and its Session

Get Events
	${events}=	API::Get::Tracking::Events
	${events}=	Set Variable	${events.json()}
	:FOR	${event}	IN	@{events}
	\	Dictionary Should Contain Key	${event}	id
	\	Dictionary Should Contain Key	${event}	category
	\	Dictionary Should Contain Key	${event}	event_number
	\	Dictionary Should Contain Key	${event}	description
	\	Dictionary Should Contain Key	${event}	occurrences
	\	Length Should Be	${event}	5

Reset Counters One Event
	[Setup]	SUITE:Create and Delete Session
	${events}=	API::Get::Tracking::Events
	${events}=	Set Variable	${events.json()}
	${reset_events}=	Create List
	:FOR	${event}	IN	@{events}
#	Obs: 200 and 201 are login and logout events
	\	${id_event}=	Get From Dictionary	${event}	id
	\	${event_number}=	Get From Dictionary	${event}	event_number
	\	${occurrences}=	Run Keyword If	${event_number} == 200	Get From Dictionary	${event}	occurrences
	\	Run Keyword If	${event_number} == 200	Should Not Be Equal	${occurrences}	0
	\	Run Keyword If	${event_number} == 200	Append to List	${reset_events}	${id_event}
	\	Exit For Loop If	${event_number} == 200

	${PAYLOAD}=	API::JSON::Delete	events	@{reset_events}
	API::Post::Tracking::Events::Reset Counters	${PAYLOAD}

	${events}=	API::Get::Tracking::Events
	${events}=	Set Variable	${events.json()}
	:FOR	${event}	IN	@{events}
#	Obs: 200 and 201 are login and logout events
	\	${event_number}=	Get From Dictionary	${event}	event_number
	\	${occurrences}=	Run Keyword If	${event_number} == 200	Get From Dictionary	${event}	occurrences
	\	Run Keyword If	${event_number} == 200	Should Be Equal	${occurrences}	0
	\	Exit For Loop If	${event_number} == 201

Reset Counters Two Event
	[Setup]	SUITE:Create and Delete Session
	${events}=	API::Get::Tracking::Events
	${events}=	Set Variable	${events.json()}
	${reset_events}=	Create List
	:FOR	${event}	IN	@{events}
#	Obs: 200 and 201 are login and logout events
	\	${id_event}=	Get From Dictionary	${event}	id
	\	${event_number}=	Get From Dictionary	${event}	event_number
	\	${occurrences}=	Run Keyword If	${event_number} == 200 or ${event_number} == 201	Get From Dictionary	${event}	occurrences
	\	Run Keyword If	${event_number} == 200 or ${event_number} == 201	Should Not Be Equal	${occurrences}	0
	\	Run Keyword If	${event_number} == 200 or ${event_number} == 201	Append to List	${reset_events}	${id_event}
	\	Exit For Loop If	${event_number} == 201

	${PAYLOAD}=	API::JSON::Delete	events	@{reset_events}
	API::Post::Tracking::Events::Reset Counters	${PAYLOAD}

	${events}=	API::Get::Tracking::Events
	${events}=	Set Variable	${events.json()}
	:FOR	${event}	IN	@{events}
#	Obs: 200 and 201 are login and logout events
	\	${event_number}=	Get From Dictionary	${event}	event_number
	\	${occurrences}=	Run Keyword If	${event_number} == 200 or ${event_number} == 201	Get From Dictionary	${event}	occurrences
	\	Run Keyword If	${event_number} == 200 or ${event_number} == 201	Should Be Equal	${occurrences}	0
	\	Exit For Loop If	${event_number} == 201

Reset Counters Invalid Event
	[Tags]	NON-CRITICAL	BUG_API_6
	${PAYLOAD}=	API::JSON::Delete	events	${INVALID_EVENT}
	${response}=	API::Post::Tracking::Events::Reset Counters	${PAYLOAD}
	${STATUS}=	Evaluate	200
	Should Not Be Equal	${response.status_code}	${STATUS}
	Dictionary Should Contain Value	${response.json()}	Error

Get System Memory Usage
	${memory}=	API::Get::Tracking::System::Memory
	${memory}=	Set Variable	${memory.json()}
	:FOR	${mem}	IN	@{memory}
	\	Dictionary Should Contain Key	${mem}	id
	\	Dictionary Should Contain Key	${mem}	memory_type
	\	Dictionary Should Contain Key	${mem}	total_(kb)
	\	Dictionary Should Contain Key	${mem}	used_(kb)
	\	Dictionary Should Contain Key	${mem}	free_(kb)
	\	Length Should Be	${mem}	5

Get System CPU Usage
	${cpu}=	API::Get::Tracking::System::CPU
	${cpu}=	Set Variable	${cpu.json()}
	:FOR	${cpu}	IN	@{cpu}
	\	Dictionary Should Contain Key	${cpu}	id
	\	Dictionary Should Contain Key	${cpu}	user_%
	\	Dictionary Should Contain Key	${cpu}	system_%
	\	Dictionary Should Contain Key	${cpu}	idle_%
	\	Dictionary Should Contain Key	${cpu}	waiting_i/o_%
	\	Length Should Be	${cpu}	5

Get System Disk Usage
	${disk}=	API::Get::Tracking::System::Disk
	${disk}=	Set Variable	${disk.json()}
	:FOR	${disk}	IN	@{disk}
	\	Dictionary Should Contain Key	${disk}	id
	\	Dictionary Should Contain Key	${disk}	description
	\	Dictionary Should Contain Key	${disk}	partition
	\	Dictionary Should Contain Key	${disk}	size_(kb)
	\	Dictionary Should Contain Key	${disk}	used_(kb)
	\	Dictionary Should Contain Key	${disk}	available_(kb)
	\	Dictionary Should Contain Key	${disk}	use_%
	\	Length Should Be	${disk}	7

Get Empty Discovery Logs
	[Tags]	THINKING	NON-CRITICAL	DISCOVERY
	${logs}=	API::Get::Tracking::Discovery Logs
	${EMPTY_LIST}=	Create List
	Should Be Equal	${logs.json()}	${EMPTY_LIST}
	Length Should Be	${logs.json()}	0

Get Discovery Logs
	[Tags]	DISCOVERY
	[Setup]	SUITE:Create and Delete Devices::Discovery::Network Scan
	${logs}=	API::Get::Tracking::Discovery Logs
	${logs}=	Set Variable	${logs.json()}
	:FOR	${log}	IN	@{logs}
	\	Dictionary Should Contain Key	${log}	id
	\	Dictionary Should Contain Key	${log}	date
	\	Dictionary Should Contain Key	${log}	ip_address
	\	Dictionary Should Contain Key	${log}	device_name
	\	Dictionary Should Contain Key	${log}	discovery_method
	\	Dictionary Should Contain Key	${log}	action
	\	Length Should Be	${log}	6
	[Teardown]	API::Post::Tracking::Discovery Logs::Reset Logs

Reset Logs Discovery Logs
	[Tags]	DISCOVERY
	[Setup]	SUITE:Create and Delete Devices::Discovery::Network Scan
	${logs}=	API::Get::Tracking::Discovery Logs
	${logs}=	Set Variable	${logs.json()}
	Should Not Be Empty	${logs}

	API::Post::Tracking::Discovery Logs::Reset Logs
	${logs}=	API::Get::Tracking::Discovery Logs
	${logs}=	Set Variable	${logs.json()}
	Should Be Empty	${logs}

Get Network Interfaces
	${interfaces}=	API::Get::Tracking::Network::Interfaces
	${interfaces}=	Set Variable	${interfaces.json()}
	:FOR	${interface}	IN	@{interfaces}
	\	Dictionary Should Contain Key	${interface}	id
	\	Dictionary Should Contain Key	${interface}	ifname
	\	Dictionary Should Contain Key	${interface}	ifindex
	\	Dictionary Should Contain Key	${interface}	state
	\	Dictionary Should Contain Key	${interface}	rx_packets
	\	Dictionary Should Contain Key	${interface}	tx_packets
	\	Dictionary Should Contain Key	${interface}	collisions
	\	Dictionary Should Contain Key	${interface}	dropped
	\	Length Should Be	${interface}	9

Get Network Interfaces::Name
	${interfaces}=	API::Get::Tracking::Network::Interfaces
	${interfaces}=	Set Variable	${interfaces.json()}
	:FOR	${interface}	IN	@{interfaces}
	\	${id}=	Get From Dictionary	${interface}	id
	\	${one_interface}=	API::Get::Tracking::Network::Interfaces::Name	${id}
	\	${one_interface}=	Set Variable	${one_interface.json()}
#	\	Dictionary Should Contain Key	${one_interface}	id
	\	Dictionary Should Contain Key	${one_interface}	ifname
	\	Dictionary Should Contain Key	${one_interface}	speed(mb/s)
	\	Dictionary Should Contain Key	${one_interface}	collisions
	\	Dictionary Should Contain Key	${one_interface}	rx_packets
	\	Dictionary Should Contain Key	${one_interface}	rx_bytes
	\	Dictionary Should Contain Key	${one_interface}	rx_errors
	\	Dictionary Should Contain Key	${one_interface}	rx_crc_errors
	\	Dictionary Should Contain Key	${one_interface}	rx_dropped
	\	Dictionary Should Contain Key	${one_interface}	rx_fifo_errors
	\	Dictionary Should Contain Key	${one_interface}	rx_compressed
	\	Dictionary Should Contain Key	${one_interface}	rx_frame_errors
	\	Dictionary Should Contain Key	${one_interface}	rx_length_errors
	\	Dictionary Should Contain Key	${one_interface}	rx_missed_errors
	\	Dictionary Should Contain Key	${one_interface}	rx_over_errors
	\	Dictionary Should Contain Key	${one_interface}	tx_packets
	\	Dictionary Should Contain Key	${one_interface}	tx_bytes
	\	Dictionary Should Contain Key	${one_interface}	tx_errors
	\	Dictionary Should Contain Key	${one_interface}	tx_carrier_errors
	\	Dictionary Should Contain Key	${one_interface}	tx_dropped
	\	Dictionary Should Contain Key	${one_interface}	tx_fifo_errors
	\	Dictionary Should Contain Key	${one_interface}	tx_compressed
	\	Dictionary Should Contain Key	${one_interface}	tx_aborted_errors
	\	Dictionary Should Contain Key	${one_interface}	tx_heartbeat_errors
	\	Dictionary Should Contain Key	${one_interface}	tx_window_errors
#		Length is 24 but are considering 'status' and 'errors' fields
	\	Length Should Be	${one_interface}	24

Get Network Invalid Interface::Name
	[Tags]	NON-CRITICAL	BUG_API_7
	${response}=	API::Get::Tracking::Network::Interfaces::Name	${INVALID_INTERFACE}
	${STATUS}=	Evaluate	200
	Should Not Be Equal	${response.status_code}	${STATUS}
	Dictionary Should Contain Value	${response.json()}	Error

Get Network LLDP
	${lldps}=	API::Get::Tracking::Network::LLDP
	${lldps}=	Set Variable	${lldps.json()}
	:FOR	${lldp}	IN	@{lldps}
	\	Dictionary Should Contain Key	${lldp}	id
	\	Dictionary Should Contain Key	${lldp}	type
	\	Dictionary Should Contain Key	${lldp}	connection
	\	Dictionary Should Contain Key	${lldp}	chassis_id
	\	Dictionary Should Contain Key	${lldp}	port_id
	\	Dictionary Should Contain Key	${lldp}	age
	\	Dictionary Should Contain Key	${lldp}	system_name
	\	Dictionary Should Contain Key	${lldp}	ipv4_mgmt_addr
	\	Dictionary Should Contain Key	${lldp}	ipv6_mgmt_addr
	\	Dictionary Should Contain Key	${lldp}	system_description
	\	Length Should Be	${lldp}	10

Get Network Routing Table
	${routes}=	API::Get::Tracking::Network::Routing Table
	${routes}=	Set Variable	${routes.json()}
	:FOR	${route}	IN	@{routes}
	\	${keys}=	Get Dictionary Keys	${route}
	\	Dictionary Should Contain Key	${route}	id
	\	Dictionary Should Contain Key	${route}	destination
	\	Dictionary Should Contain Key	${route}	gateway
	\	Dictionary Should Contain Key	${route}	metric
	\	Dictionary Should Contain Key	${route}	from
	\	Dictionary Should Contain Key	${route}	table
	\	Dictionary Should Contain Key	${route}	interface
	\	Length Should Be	${route}	7

Get Devices USB Devices
	${usb_devices}=	API::Get::Tracking::Devices::USB Devices
	${usb_devices}=	Set Variable	${usb_devices.json()}
	:FOR	${usb_device}	IN	@{usb_devices}
	\	${keys}=	Get Dictionary Keys	${usb_device}
	\	Dictionary Should Contain Key	${usb_device}	id
	\	Dictionary Should Contain Key	${usb_device}	usb_port
	\	Dictionary Should Contain Key	${usb_device}	usb_path
	\	Dictionary Should Contain Key	${usb_device}	usb_id
	\	Dictionary Should Contain Key	${usb_device}	detected_type
	\	Dictionary Should Contain Key	${usb_device}	kernel_device
	\	Dictionary Should Contain Key	${usb_device}	description
	\	Length Should Be	${usb_device}	7

Get Devices USB Devices::Name
	${usb_devices}=	API::Get::Tracking::Devices::USB Devices
	${usb_devices}=	Set Variable	${usb_devices.json()}
	:FOR	${usb_device}	IN	@{usb_devices}
	\	${id}=	Get From Dictionary	${usb_device}	id
	\	${one_usb}=	API::Get::Tracking::Devices::USB Devices::${id}
	\	${one_usb}=	Set Variable	${one_usb.json()}
#	\	Dictionary Should Contain Key	${one_usb}	id
	\	Dictionary Should Contain Key	${one_usb}	usb_port
	\	Dictionary Should Contain Key	${one_usb}	bus:dev
	\	Dictionary Should Contain Key	${one_usb}	usb_path
	\	Dictionary Should Contain Key	${one_usb}	vendorid:productid
	\	Dictionary Should Contain Key	${one_usb}	detected_type
	\	Dictionary Should Contain Key	${one_usb}	kernel_device
	\	Dictionary Should Contain Key	${one_usb}	manufacturer
	\	Dictionary Should Contain Key	${one_usb}	description
	\	Dictionary Should Contain Key	${one_usb}	number_of_interfaces
	\	Dictionary Should Contain Key	${one_usb}	driver(s)
#		Length is 12 but are considering 'status' and 'errors' fields
	\	Length Should Be	${one_usb}	12

Get Devices Invalid USB Devices::Name
	[Tags]	NON-CRITICAL	BUG_API_8
	${response}=	API::Get::Tracking::Devices::USB Devices::Name	${INVALID_USB}
	${STATUS}=	Evaluate	200
	Should Not Be Equal	${response.status_code}	${STATUS}
	Dictionary Should Contain Value	${response.json()}	Error

Get Devices Serial Statistics
	${serial_stats}=	API::Get::Tracking::Devices::Serial Statistics
	${serial_stats}=	Set Variable	${serial_stats.json()}
	:FOR	${serial_stat}	IN	@{serial_stats}
	\	${keys}=	Get Dictionary Keys	${serial_stat}
	\	Dictionary Should Contain Key	${keys}	id
	\	Dictionary Should Contain Key	${keys}	speed
	\	Dictionary Should Contain Key	${keys}	port
	\	Dictionary Should Contain Key	${keys}	device_name
	\	Dictionary Should Contain Key	${keys}	rx_bytes
	\	Dictionary Should Contain Key	${keys}	tx_bytes
	\	Dictionary Should Contain Key	${keys}	rs-232_signals
	\	Dictionary Should Contain Key	${keys}	cts_shift
	\	Dictionary Should Contain Key	${keys}	dcd_shift
	\	Dictionary Should Contain Key	${keys}	frame_error
	\	Dictionary Should Contain Key	${keys}	overrun
	\	Dictionary Should Contain Key	${keys}	parity_error
	\	Dictionary Should Contain Key	${keys}	break
	\	Dictionary Should Contain Key	${keys}	buffer_overrun
	\	Length Should Be	${serial_stat}	14

Reset Statistics Devices Serial Statistics
	[Setup]	SUITE:Enable Serial Ports and Access First
	${all_serial}=	API::Get::Tracking::Devices::Serial Statistics
	${last_serial}=	Get From List	${all_serial.json()}	0
	${id}=	Get From Dictionary	${last_serial}	id
	${payload}=	API::JSON::Delete	devices	${id}
	${response}=	API::Post::Tracking::Devices::Serial Statistics::Reset Statistics	${payload}
	${serial}=	API::Get::Tracking::Devices::Serial Statistics

Get Schedules
	[Setup]	SUITE:Create and Delete System::Schedule
	${schedules}=	API::Get::Tracking::Schedule
	${schedules}=	Set Variable	${schedules.json()}
	:FOR	${schedule}	IN	@{schedules}
	\	Dictionary Should Contain Key	${schedule}	id
	\	Dictionary Should Contain Key	${schedule}	date
	\	Dictionary Should Contain Key	${schedule}	pid
	\	Dictionary Should Contain Key	${schedule}	event
	\	Dictionary Should Contain Key	${schedule}	task_name
	\	Dictionary Should Contain Key	${schedule}	user
	\	Dictionary Should Contain Key	${schedule}	error
	\	Length Should Be	${schedule}	7

Reset Logs Schedule
	[Setup]	SUITE:Create and Delete System::Schedule
	${devices}=	API::Get::Tracking::Schedule
	${response}=	API::Post::Tracking::Schedule::Reset Logs
	Should Be Empty	${response.json()}

*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	${RANGE}=	Get Length	${SESSION_TICKET}
	${RANGE}=	Evaluate	${RANGE}-1
	API::Delete::Session
#	:FOR	${INDEX}	IN RANGE	0	${RANGE}
#	\	API::Delete::Session	INDEX_TICKET=${INDEX}

#SUITE:Create Device::Table and its Session
#	API::Delete If Exists Devices::Tables	idrac
#	CLI:Open
##	TODO ADD DEVICE USING API
#	CLI:Add Device	idrac	idrac6	192.168.2.28	root	NodeGridD3m0Passw0rd	enabled
##	API doesn't have how to connect to a device
#	CLI:Connect Device	idrac	cli

SUITE:Delete Device::Table and its Session
	CLI:Close Current Connection	Yes
	API::Delete Devices::Table	idrac

SUITE:Create and Delete Session
	API::Post::Session
	API::Delete::Session	INDEX_TICKET=-1

SUITE:Create and Delete Devices::Discovery::Network Scan
	API::Delete If Exists Devices::Discovery::Networks	${SCAN_ID}
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	scan_id=${SCAN_ID}
	Set To Dictionary	${payload}	ip_range_start=1.1.1.1
	Set To Dictionary	${payload}	ip_range_end=1.1.1.5
	Set To Dictionary	${payload}	enable_scanning=yes
	Set To Dictionary	${payload}	similar_devices=no
	Set To Dictionary	${payload}	port_scan=yes
	Set To Dictionary	${payload}	port_list=22-23,623
	API::Post::Devices::Discovery::Network	${payload}
	API::Delete Devices::Discovery::Network	${SCAN_ID}
	Wait Until Keyword Succeeds	2m	2s	API::Aux::Check IP Devices::Discovery::Log	1.1.1.5

SUITE:Delete Devices::Discovery::Network Scan
	API::Delete If Exists Devices::Discovery::Networks	${SCAN_ID}
	Sleep	10s
	API::Post::Devices::Discovery::Logs::Reset Logs

SUITE:Create and Delete System::Schedule
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	task_name=test_schedule
	Set To Dictionary	${payload}	user=daemon
	Set To Dictionary	${payload}	command=test();
	API::Post::System::Schedule	${payload}

SUITE:Enable Serial Ports and Access First
	Sleep	1
#	API::Put::Devices::Table::Name::Enable	ttyS1
