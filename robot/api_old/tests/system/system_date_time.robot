*** Settings ***
Documentation	Test /api/<version>/system/datetime endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot
Force Tags		API	${VERSION}
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Test Setup	SUITE:Reset Default Values System::Date Time

*** Variables ***
${DATE_AND_TIME}	manual
${SERVER}	pool.ntp.org
${DAY}	29
${MONTH}	02
${YEAR}	2018
${HOUR}	00
${MINUTE}	30
${SECOND}	15
${ZONE}	GMT

*** Test Cases ***
Get System Date Time
	${response}=	API::Get::System::Date Time
	${response}=	Set Variable	${response.json()}
	SUITE:Should Contain Fields System::Date Time	${response}

Update System Date Time to Manual
	[Tags]	NON-CRITICAL	FIX_TEST
	${payload}=	SUITE:Dictionary Update System::Date Time
	${response}=	API::Put::System::Date Time	${payload}
	${response}=	Set Variable	${response.json()}

	SUITE:Should Contain Fields System::Date Time	${response}
	SUITE:Should Be Equal Update System::Date Time	${response}
#	Remove to fields be the same
	Remove From Dictionary	${response}	status	errors	system_time	last_update
	Dictionaries Should Be Equal	${response}	${payload}

Update System Date Time to Auto
	[Tags]	NON-CRITICAL	FIX_TEST
	${payload}=	SUITE:Dictionary Update System::Date Time
	Set To Dictionary	${payload}	date_and_time=auto
	${response}=	API::Put::System::Date Time	${payload}
	${response}=	Set Variable	${response.json()}

	SUITE:Should Contain Fields System::Date Time	${response}
	SUITE:Should Be Equal Update System::Date Time	${response}
#	Remove to fields be the same
	Remove From Dictionary	${response}	status	errors	system_time	last_update
	Dictionaries Should Be Equal	${response}	${payload}

#	@todo tests with wrong fields
#	@todo tests with wrong values to all fields
#	@todo test changing server
#	@todo test changing all data (day, month, hour, ...) with date_and_time=auto
#	@todo test checking system_time and last_update fields
#	@todo test changing server to a wrong site
#	@todo test of funcionality

*** Keywords ***
SUITE:Setup
	API::Post::Session	MODE=no
	#Start: Getting default values
	${date_time}=	API::Get::System::Date Time	MODE=no
	${date_time}=	Set Variable	${date_time.json()}
	Remove From Dictionary	${date_time}	status	errors
	Set Suite Variable	${DEFAULT_DATETIME}	${date_time}
	#End: Getting default values

SUITE:Teardown
	SUITE:Reset Default Values System::Date Time
	API::Delete::Session

SUITE:Reset Default Values System::Date Time
	API::Put::System::Preferences	${DEFAULT_DATETIME}	MODE=no

SUITE:Should Contain Fields System::Date Time
	[Arguments]	${date_time}
	Dictionary Should Contain Key	${date_time}	server
	Dictionary Should Contain Key	${date_time}	system_time
	Dictionary Should Contain Key	${date_time}	last_update
	Dictionary Should Contain Key	${date_time}	date_and_time
	Dictionary Should Contain Key	${date_time}	day
	Dictionary Should Contain Key	${date_time}	month
	Dictionary Should Contain Key	${date_time}	year
	Dictionary Should Contain Key	${date_time}	hour
	Dictionary Should Contain Key	${date_time}	minute
	Dictionary Should Contain Key	${date_time}	second
	Dictionary Should Contain Key	${date_time}	zone

SUITE:Should Be Equal Update System::Date Time
	[Arguments]	${date_time}
	${update_keys}=	Get Dictionary Keys	${date_time}
	${expected}=	SUITE:Dictionary Update System::Date Time
	:FOR	${key}	IN	@{update_keys}
	\	Continue For Loop If	'${key}' == 'system_time' or '${key}' == 'last_update'
	\	Should Be Equal As Strings	${date_time["${key}"]}	${expected["${key}"]}
	${expected_keys}=	Get Dictionary Keys	${expected}
	Remove Values From List	${update_keys}	status	errors	system_time	last_update
	Lists Should Be Equal	${update_keys}	${expected_keys}

SUITE:Dictionary Update System::Date Time
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	server=${SERVER}
	Set To Dictionary	${payload}	month=${MONTH}
	Set To Dictionary	${payload}	day=${DAY}
	Set To Dictionary	${payload}	year=${YEAR}
	Set To Dictionary	${payload}	hour=${HOUR}
	Set To Dictionary	${payload}	minute=${MINUTE}
	Set To Dictionary	${payload}	second=${SECOND}
	Set To Dictionary	${payload}	zone=${ZONE}
	Set To Dictionary	${payload}	date_and_time=${DATE_AND_TIME}
	[Return]	${payload}