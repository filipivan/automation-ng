*** Settings ***
Documentation	Test /api/<version>/system/sms... endpoints
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot
Force Tags		API	${VERSION}
Default Tags	EXCLUDEIN3_2	EXCLUDEIN4_0

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${NAME}	ricardo_number
${PHONE_NUMBER}	+5547997834946

${NAME2}	josias_number
${PHONE_NUMBER2}	+5555996888733

*** Test Cases ***
Get System::Sms
	${sms}=	API::Get::System::Sms
	${sms}=	Set Variable	${sms.json()}
	SUITE:Should Contain Fields System::Sms	${sms}

Update System::Sms per field
#	How to put as setup a var??
	${whitelist_fields}=	Create List	enable_actions_via_incoming_sms	allow_apn	allow_simswap	allow_connect_and_disconnect
	...	allow_mstatus	allow_reset	allow_info	allow_factorydefault	allow_reboot	password
	${original}=	API::Get::System::Sms
	${original}=	Set Variable	${original.json()}
	Remove From Dictionary	${original}	password
	:FOR	${element}	IN	@{whitelist_fields}
	\	Continue For Loop If	'${element}' == 'password'
	\	${payload}=	Create Dictionary

	\	Set To Dictionary	${payload}	${element}=${false}
	\	${updated}=	API::Put::System::Sms	${payload}
	\	${updated}=	Set Variable	${updated.json()}
	\	SUITE:Should Contain Fields System::Sms	${updated}
	\	SUITE:Should Be Equal	${updated}	${element}	${false}

	\	Set To Dictionary	${payload}	${element}=${true}
	\	${updated}=	API::Put::System::Sms	${payload}
	\	${updated}=	Set Variable	${updated.json()}
	\	SUITE:Should Contain Fields System::Sms	${updated}
	\	SUITE:Should Be Equal	${updated}	${element}	${true}

Get Empty System::Sms::Whitelist
	${whitelist_list}=	API::Get::System::Sms::Whitelist
	${whitelist_list}=	Set Variable	${whitelist_list.json()}
	Length Should Be	${whitelist_list}	0
	${empty_list}=	Create List
	Should Be Equal	${whitelist_list}	${empty_list}

Get System::Sms::Whitelist
	[Setup]	API::Add System::Sms::Whitelist	${NAME}	${PHONE_NUMBER}
	${whitelist_list}=	API::Get::System::Sms::Whitelist
	${whitelist_list}=	Set Variable	${whitelist_list.json()}
	:FOR	${whitelist_dict}	IN	@{whitelist_list}
	\	SUITE:Should Contain Fields System::Sms::Whitelist	${whitelist_dict}
	[Teardown]	API::Delete System::Sms::Whitelist	${NAME}

Add One System::Sms::Whitelist
	[Setup]	API::Delete If Exists System::Sms::Whitelists	${NAME}
	API::Add System::Sms::Whitelist	${NAME}	${PHONE_NUMBER}
	API::Aux::Should Exist System::Sms::Whitelist	${NAME}
	[Teardown]	API::Delete System::Sms::Whitelist	${NAME}

Add Two System::Sms::Whitelist
	[Setup]	API::Delete If Exists System::Sms::Whitelists	${NAME}	${NAME2}
	API::Add System::Sms::Whitelist	${NAME}	${PHONE_NUMBER}
	API::Add System::Sms::Whitelist	${NAME2}	${PHONE_NUMBER2}
	API::Aux::Should Exist System::Sms::Whitelists	${NAME}	${NAME2}
	[Teardown]	API::Delete System::Sms::Whitelists	${NAME}	${NAME2}

Add Duplicate System::Sms::Whitelist
	[Setup]	API::Delete If Exists System::Sms::Whitelists	${NAME}
	API::Add System::Sms::Whitelist	${NAME}	${PHONE_NUMBER}
	API::Aux::Should Exist System::Sms::Whitelist	${NAME}
	${response}=	API::Add System::Sms::Whitelist	${NAME}	${PHONE_NUMBER}	Raw
	${response}=	Set Variable	${response.json()}
#	Comparing response of request with expected message created
	API::Msg::Compare Simple Message	${response}	name	Entry already exists.
	[Teardown]	API::Delete System::Sms::Whitelists	${NAME}

Add Invalid System::Sms::Whitelist | name, phone_number | EMPTY
	[Tags]	NON-CRITICAL	BUG_API_8
#	Understand name and phone_number values as EMPTY by Default
	${response}=	API::Add System::Sms::Whitelist	RAW_MODE=Raw
	${response}=	Set Variable	${response.json()}
#	Comparing response of request with expected message created
	${fields}=	Create List	name	phone_number
	${values}=	Create List	This field is empty or it contains invalid characters.	This field should be in +<country code><phone number> format (phone number - only digits).
	API::Msg::Compare Complex Message	${response}	${fields}	${values}

Add Invalid System::Sms::Whitelist | name | EMPTY
	${response}=	API::Add System::Sms::Whitelist	${EMPTY}	${PHONE_NUMBER}	Raw
	${response}=	Set Variable	${response.json()}
#	Comparing response of request with expected message created
	API::Msg::Compare Simple Message	${response}	name	This field is empty or it contains invalid characters.	#Validation error.

Add Invalid System::Sms::Whitelist | phone_number | EMPTY
	${response}=	API::Add System::Sms::Whitelist	${NAME}	${EMPTY}	RAW_MODE=Raw
	${response}=	Set Variable	${response.json()}
#	Comparing response of request with expected message created
	API::Msg::Compare Simple Message	${response}	phone_number	This field should be in +<country code><phone number> format (phone number - only digits).	#Validation error.
	[Teardown]	Run Keyword If Test Failed	API::Delete If Exists System::Sms::Whitelists	${NAME}

Add Invalid System::Sms::Whitelist | name, phone_number | EXCEEDED
	[Tags]	NON-CRITICAL	BUG_API_9
#	Understand field_name and field_value values as EMPTY by Default
	${response}=	API::Add System::Sms::Whitelist	${EXCEEDED}	${EXCEEDED}	Raw
	${response}=	Set Variable	${response.json()}
#	Comparing response of request with expected message created
	${fields}=	Create List	name	phone_number
	${values}=	Create List	Validation error.	Validation error.	#Validation error.
	API::Msg::Compare Complex Message	${response}	${fields}	${values}
	[Teardown]	Run Keyword If Test Failed	API::Delete If Exists System::Sms::Whitelists	${EXCEEDED}

Add Invalid System::Sms::Whitelist| name | EXCEEDED
	[Tags]	NON-CRITICAL	BUG_API_9
	${response}=	API::Add System::Sms::Whitelist	${EXCEEDED}	${PHONE_NUMBER}	Raw
	${response}=	Set Variable	${response.json()}
#	Comparing response of request with expected message created
	API::Msg::Compare Simple Message	${response}	name	Validation error.	#Validation error.
	[Teardown]	Run Keyword If Test Failed	API::Delete If Exists System::Sms::Whitelists	${EXCEEDED}

Add Invalid System::Sms::Whitelist | phone_number | EXCEEDED
	[Tags]	NON-CRITICAL	BUG_API_9
	${response}=	API::Add System::Sms::Whitelist	${NAME}	${EXCEEDED}	Raw
	${response}=	Set Variable	${response.json()}
#	Comparing response of request with expected message created
	API::Msg::Compare Simple Message	${response}	phone_number	Validation error.	#Validation error.
	[Teardown]	Run Keyword If Test Failed	API::Delete If Exists System::Sms::Whitelists	${NAME}

Delete One System::Sms::Whitelist
	[Setup]	API::Add System::Sms::Whitelist	${NAME}	${PHONE_NUMBER}
	API::Delete System::Sms::Whitelist	${NAME}
	API::Aux::Should Not Exist System::Sms::Whitelist	${NAME}
	[Teardown]	Run Keyword If Test Failed	API::Delete If Exists System::Sms::Whitelists	${NAME}

Delete Two System::Sms::Whitelist
	[Setup]	SUITE:Add Two System::Sms::Whitelist
	API::Delete System::Sms::Whitelists	${NAME}	${NAME2}
	API::Aux::Should Not Exist System::Sms::Whitelists	${NAME}	${NAME2}
	[Teardown]	Run Keyword If Test Failed	API::Delete If Exists System::Sms::Whitelists	${NAME}	${NAME2}

#	@todo tests of funcionality
#	@todo tests with wrong fields

Update System::Sms::Whitelist
	[Setup]	API::Add System::Sms::Whitelist	${NAME}	${PHONE_NUMBER}
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	phone_number=${PHONE_NUMBER2}
	API::Put::System::Sms::Whitelist::Name	${NAME}	${payload}

	${whitelist_list}=	API::Get::System::Sms::Whitelist
	${whitelist_list}=	Set Variable	${whitelist_list.json()}
	Should Not Be Empty	${whitelist_list}
	${expected_dict}=	Create Dictionary
	Set To Dictionary	${expected_dict}	id=${NAME}
	Set To Dictionary	${expected_dict}	name=${NAME}
	Set To Dictionary	${expected_dict}	phone_number=${PHONE_NUMBER2}
	List Should Contain Value	${whitelist_list}	${expected_dict}
	[Teardown]	API::Delete System::Sms::Whitelists	${NAME}

*** Keywords ***
SUITE:Setup
	API::Post::Session	admin	ZPE.Cl0ud.2019
	API::Delete If Exists System::Sms::Whitelists	${NAME}	${NAME2}

SUITE:Teardown
	API::Delete If Exists System::Sms::Whitelists	${NAME}	${NAME2}
	API::Delete::Session

SUITE:Should Be Equal
	[Arguments]	${dict}	${field}	${value}
	${get_from_field}=	Get From Dictionary	${dict}	${field}
	Should Be Equal	${get_from_field}	${value}

SUITE:Add Two System::Sms::Whitelist
	API::Add System::Sms::Whitelist	${NAME}	${PHONE_NUMBER}
	API::Add System::Sms::Whitelist	${NAME2}	${PHONE_NUMBER2}

SUITE:Should Contain Fields System::Sms
	[Arguments]	${sms_dict}
	${whitelist_fields}=	Create List	enable_actions_via_incoming_sms	allow_apn	allow_simswap	allow_connect_and_disconnect
	...	allow_mstatus	allow_reset	allow_info	allow_factorydefault	allow_reboot	password
	:FOR	${element}	IN	@{whitelist_fields}
	\	Dictionary Should Contain Key	${sms_dict}	${element}
	Length Should Be	${sms_dict}	10

SUITE:Should Contain Fields System::Sms::Whitelist
	[Arguments]	${sms_dict}
	${whitelist_fields}=	Create List	id	name	phone_number
	:FOR	${element}	IN	@{whitelist_fields}
	\	Dictionary Should Contain Key	${sms_dict}	${element}
	Length Should Be	${sms_dict}	3
