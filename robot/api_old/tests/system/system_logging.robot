
*** Settings ***
Documentation	Test /api/<version>/logging endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot
Force Tags		API	${VERSION}
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Test Setup	SUITE:Reset Default Values System::Logging

*** Variables ***
${ENABLE_SESSION_LOGGING}	${TRUE}
${ENABLE_SESSION_LOGGING_ALERTS}	${TRUE}
${SESSION_STRING_1}	string_1
${SESSION_STRING_2}	string_2
${SESSION_STRING_3}	string_3
${SESSION_STRING_4}	string_4
${SESSION_STRING_5}	string_5

*** Test Cases ***
Get System Logging
	${response}=	API::Get::System::Logging
	${response}=	Set Variable	${response.json()}
	SUITE:Should Contain Fields System::Logging	${response}

Update System Logging
	${payload}=	SUITE:Dictionary Update System::Logging
	${response}=	API::Put::System::Logging	${payload}
	${response}=	Set Variable	${response.json()}

#	Remove to fields be the same
	Remove From Dictionary	${response}	status	errors
	SUITE:Should Contain Fields System::Logging	${response}
	SUITE:Should Be Equal Update System::Logging	${response}
	Dictionaries Should Be Equal	${response}	${payload}

#	@todo test disabling enable_session_logging_alerts and changing values
#	@todo test disabling enable_session_logging and changing values
#	@todo tests with wrong fields
#	@todo tests with wrong values to all fields
#	@todo test of funcionality

*** Keywords ***
SUITE:Setup
	API::Post::Session	MODE=no
	#Start: Getting default values
	${logging}=	API::Get::System::Logging	MODE=no
	${logging}=	Set Variable	${logging.json()}
	Remove From Dictionary	${logging}	status	errors
	Set Suite Variable	${DEFAULT_LOGGING}	${logging}
	#End: Getting default values

SUITE:Teardown
	SUITE:Reset Default Values System::Logging
	API::Delete::Session

SUITE:Reset Default Values System::Logging
	API::Put::System::Logging	${DEFAULT_LOGGING}	MODE=no

SUITE:Should Contain Fields System::Logging
	[Arguments]	${logging}
	Dictionary Should Contain Key	${logging}	enable_session_logging
	Dictionary Should Contain Key	${logging}	enable_session_logging_alerts
	Dictionary Should Contain Key	${logging}	session_string_1
	Dictionary Should Contain Key	${logging}	session_string_2
	Dictionary Should Contain Key	${logging}	session_string_3
	Dictionary Should Contain Key	${logging}	session_string_4
	Dictionary Should Contain Key	${logging}	session_string_5

SUITE:Should Be Equal Update System::Logging
	[Arguments]	${logging}
	${update_keys}=	Get Dictionary Keys	${logging}
	${expected}=	SUITE:Dictionary Update System::Logging
	:FOR	${key}	IN	@{update_keys}
	\	Should Be Equal As Strings	${logging["${key}"]}	${expected["${key}"]}
	${expected_keys}=	Get Dictionary Keys	${expected}
	Remove Values From List	${update_keys}	status	errors
	Lists Should Be Equal	${update_keys}	${expected_keys}

SUITE:Dictionary Update System::Logging
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	enable_session_logging=${ENABLE_SESSION_LOGGING}
	Set To Dictionary	${payload}	enable_session_logging_alerts=${ENABLE_SESSION_LOGGING_ALERTS}
	Set To Dictionary	${payload}	session_string_1=${SESSION_STRING_1}
	Set To Dictionary	${payload}	session_string_2=${SESSION_STRING_2}
	Set To Dictionary	${payload}	session_string_3=${SESSION_STRING_3}
	Set To Dictionary	${payload}	session_string_4=${SESSION_STRING_4}
	Set To Dictionary	${payload}	session_string_5=${SESSION_STRING_5}
	[Return]	${payload}