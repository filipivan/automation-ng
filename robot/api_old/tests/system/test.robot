*** Settings ***
Documentation	Test /api/<version>/system/licenses endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot
Force Tags		API	${VERSION}
Default Tags	EXCLUDEIN3_2

Suite Setup	CLI:Open	root	root
Suite Teardown	CLI:Close Connection

*** Variables ***
${NAME}	grpc
${VERSION}	1.19.0
*** Test Cases ***
Test title

	${elements}=	Create List	${NAME}
	:FOR	${element}	IN	@{elements}
    \	${response}=	CLI:Write	npm search ${element}

*** Keywords ***