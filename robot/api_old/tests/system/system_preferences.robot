*** Settings ***
Documentation	Test /api/<version>/system/preferences endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot
Force Tags		API	${VERSION}
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown
Test Setup	SUITE:Reset Default Values System::Preferences

*** Variables ***
${ADDRESS_LOCATION}	Antartida
${COORDINATES}	90,90
${HELP_URL}	https:help.com
${IDLE_TIMEOUT}	90
${URL}	${EMPTY}
${USERNAME}	${EMPTY}
${PASSWORD}	${EMPTY}
${PERCENTAGE_TO_TRIGGER_EVENTS}	90
${UNIT_IPV4_ADDRESS}	1.1.1.1
${UNIT_NETMASK}	255.255.255.0
${ISO_URL}	https://site.com/file.name
${CONSOLE_PORT_SPEED}	9600
${UNIT_INTERFACE}	eth1
${LOGO_IMAGE_SELECTION}	${FALSE}
${PATH_ABSOLUTE}	${FALSE}
${ENABLE_BANNER}	${TRUE}
${ENABLE_SERIAL_PORTS_UTILIZATION}	${TRUE}
${ENABLE_LICENSE_UTILIZATION}	${TRUE}
${BANNER}	Test ZPE Systems Banner
${LOGO_IMAGE}	applylogo-restore
${ENABLE_ALARM_ON_FAILURE}	${TRUE}
${ENABLE_ALARM_WHEN_POWERED_OFF}	${TRUE}

*** Test Cases ***
Get System Preferences
	${response}=	API::Get::System::Preferences
	${response}=	Set Variable	${response.json()}
	SUITE:Should Contain Fields System::Preferences	${response}

Update System Preferences
	[Tags]	NON-CRITICAL	FIX_KEYWORD_COMPARATION	FIX_TEXT_AREA
	${payload}=	SUITE:Dictionary Update System::Preferences
	${response}=	API::Put::System::Preferences	${payload}
	${response}=	Set Variable	${response.json()}

#	Remove to fields be the same
	Remove From Dictionary	${response}	status	errors
	SUITE:Should Contain Fields System::Preferences	${response}
	SUITE:Should Be Equal Update System::Preferences	${response}
#	Banner always add a \n in end of string
	Set To Dictionary	${payload}	banner	${BANNER}\\n
	Dictionaries Should Be Equal	${response}	${payload}

#	@todo tests to 2_enable banner
#	@todo tests with wrong fields
#	@todo tests with wrong values to all fields
#	@todo test of funcionality

*** Keywords ***
SUITE:Setup
	API::Post::Session	MODE=no
	#Start: Getting default values
	${preferences}=	API::Get::System::Preferences	MODE=no
	${preferences}=	Set Variable	${preferences.json()}
	Remove From Dictionary	${preferences}	status	errors
	${banner}=	Get From Dictionary	${preferences}	banner
	${banner}=	Get Substring	${banner}	0	-1
	Log to Console	\n-- ${banner} --\n
	Set To Dictionary	${preferences}	banner=${banner}
	Set Suite Variable	${DEFAULT_PREFERENCES}	${preferences}
	#End: Getting default values

SUITE:Teardown
	SUITE:Reset Default Values System::Preferences
	API::Delete::Session

SUITE:Reset Default Values System::Preferences
	${banner_status}=	Get From Dictionary	${DEFAULT_PREFERENCES}	enable_banner
	Run Keyword If	${banner_status} == False	Set To Dictionary	${DEFAULT_PREFERENCES}	enable_banner	${TRUE}
	API::Put::System::Preferences	${DEFAULT_PREFERENCES}	MODE=no
 	Run Keyword If	${banner_status} == False	Set To Dictionary	${DEFAULT_PREFERENCES}	enable_banner	${FALSE}
	API::Put::System::Preferences	${DEFAULT_PREFERENCES}	MODE=no

SUITE:Should Contain Fields System::Preferences
	[Arguments]	${preferences}
	Dictionary Should Contain Key	${preferences}	address_location
	Dictionary Should Contain Key	${preferences}	coordinates
	Dictionary Should Contain Key	${preferences}	help_url
	Dictionary Should Contain Key	${preferences}	idle_timeout
	Dictionary Should Contain Key	${preferences}	logo_image_selection
	Dictionary Should Contain Key	${preferences}	logo_image
	Dictionary Should Contain Key	${preferences}	url
	Dictionary Should Contain Key	${preferences}	username
	Dictionary Should Contain Key	${preferences}	password
	Dictionary Should Contain Key	${preferences}	the_path_in_url_to_be_used_as_absolute_path_name
	Dictionary Should Contain Key	${preferences}	enable_banner
	Dictionary Should Contain Key	${preferences}	banner
	Dictionary Should Contain Key	${preferences}	enable_license_utilization_rate
	Dictionary Should Contain Key	${preferences}	percentage_to_trigger_events
	Dictionary Should Contain Key	${preferences}	unit_ipv4_address
	Dictionary Should Contain Key	${preferences}	unit_netmask
	Dictionary Should Contain Key	${preferences}	unit_interface
	Dictionary Should Contain Key	${preferences}	iso_url
	Run Keyword If	${IS_CONSOLE_SERVER} or ${IS_BOLD_SR}	Run Keywords
	#	Speed field is console_port_speed when a console port
	...	Dictionary Should Contain Key	${preferences}	console_port_speed
	...	AND	Dictionary Should Contain Key	${preferences}	enable_local_serial_ports_utilization_rate
	Run Keyword If	${IS_CONSOLE_SERVER}	Run Keywords
	...	Dictionary Should Contain Key	${preferences}	state_of_power_supply_1
	...	AND	Dictionary Should Contain Key	${preferences}	state_of_power_supply_2
	...	AND	Dictionary Should Contain Key	${preferences}	enable_alarm_sound_when_one_power_supply_is_powered_off
	...	AND	Dictionary Should Contain Key	${preferences}	state_of_fan_1
	...	AND	Dictionary Should Contain Key	${preferences}	state_of_fan_2
	...	AND	Dictionary Should Contain Key	${preferences}	enable_alarm_sound_on_fan_failure

SUITE:Should Be Equal Update System::Preferences
	[Arguments]	${preferences}
	${update_keys}=	Get Dictionary Keys	${preferences}
	${expected}=	SUITE:Dictionary Update System::Preferences
	:FOR	${key}	IN	@{update_keys}
#	FIX THE BANNER FIELD COMPARATION
#	IT HAS A PROBLEM WITH \n
	\	Run Keyword If	'${key}' != 'banner'	Should Be Equal As Strings	${preferences["${key}"]}	${expected["${key}"]}
	\	Run Keyword If	'${key}' == 'banner'	Should Be Equal As Strings	${preferences["${key}"]}	${expected["${key}"]}\\n
	${expected_keys}=	Get Dictionary Keys	${expected}
	Remove Values From List	${update_keys}	status	errors
	Lists Should Be Equal	${update_keys}	${expected_keys}

SUITE:Dictionary Update System::Preferences
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	address_location=${ADDRESS_LOCATION}
	Set To Dictionary	${payload}	coordinates=${COORDINATES}
	Set To Dictionary	${payload}	help_url=${HELP_URL}
	Set To Dictionary	${payload}	idle_timeout=${IDLE_TIMEOUT}
	Set To Dictionary	${payload}	url=${EMPTY}
	Set To Dictionary	${payload}	username=${EMPTY}
	Set To Dictionary	${payload}	password=${EMPTY}
#	Can't test properly fields: logo_image_selection, logo_image=, url, username, the_path_in_url_to_be_used_as_absolute_path_name
	Set To Dictionary	${payload}	percentage_to_trigger_events=${PERCENTAGE_TO_TRIGGER_EVENTS}
	Set To Dictionary	${payload}	unit_ipv4_address=${UNIT_IPV4_ADDRESS}
	Set To Dictionary	${payload}	unit_netmask=${UNIT_NETMASK}
	Set To Dictionary	${payload}	iso_url=${ISO_URL}
	Run Keyword If	${IS_CONSOLE_SERVER} or ${IS_BOLD_SR}
	...	Set To Dictionary	${payload}	console_port_speed=${CONSOLE_PORT_SPEED}
	Set To Dictionary	${payload}	unit_interface=${UNIT_INTERFACE}
#	2 next change to false after update
	Set To Dictionary	${payload}	logo_image_selection=${LOGO_IMAGE_SELECTION}
	Set To Dictionary	${payload}	the_path_in_url_to_be_used_as_absolute_path_name=${PATH_ABSOLUTE}
	Set To Dictionary	${payload}	enable_banner=${ENABLE_BANNER}
	Run Keyword If	${IS_CONSOLE_SERVER} or ${IS_BOLD_SR}
	...	Set To Dictionary	${payload}	enable_local_serial_ports_utilization_rate=${ENABLE_SERIAL_PORTS_UTILIZATION}
	Set To Dictionary	${payload}	enable_license_utilization_rate=${ENABLE_LICENSE_UTILIZATION}
	Set To Dictionary	${payload}	banner=${BANNER}
#	change after update
	Set To Dictionary	${payload}	logo_image=${LOGO_IMAGE}
#	Run Keyword If	${IS_CONSOLE_SERVER}	Run Keywords
#	...	AND	Set To Dictionary	${payload}	enable_alarm_sound_on_fan_failure=${TRUE}
#	...	AND	Set To Dictionary	${payload}	enable_alarm_sound_when_one_power_supply_is_powered_off=${TRUE}
	[Return]	${payload}