*** Settings ***
Documentation	Test /api/<version>/system/customfields endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot
Force Tags		API	${VERSION}
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variable ***
${NAME}	name1
${VALUE}	value1
${NAME2}	name2

*** Test Cases ***
Get Empty System Custom Fields
	${customfields_list}=	API::Get::System::Custom Fields
	${customfields_list}=	Set Variable	${customfields_list.json()}
	Length Should Be	${customfields_list}	0
	${empty_list}=	Create List
	Should Be Equal	${customfields_list}	${empty_list}

Get System Custom Fields
	[Setup]	API::Add System::Custom Field	${NAME}	${VALUE}
	${customfields_list}=	API::Get::System::Custom Fields
	${customfields_list}=	Set Variable	${customfields_list.json()}
	:FOR	${customfields_dict}	IN	@{customfields_list}
	\	SUITE:Should Contain Fields System::Custom Fields	${customfields_dict}
	[Teardown]	API::Delete System::Custom Field	${NAME}

Add One System Custom Fields
	[Setup]	API::Delete If Exists System::Custom Fields	${NAME}
	API::Add System::Custom Field	${NAME}	${VALUE}
	API::Aux::Should Exist System::Custom Field	${NAME}
	[Teardown]	API::Delete System::Custom Fields	${NAME}

Add Two System Custom Fields
	[Setup]	API::Delete If Exists System::Custom Fields	${NAME}	${NAME2}
	API::Add System::Custom Field	${NAME}	${VALUE}
	API::Add System::Custom Field	${NAME2}	${VALUE}
	API::Aux::Should Exist System::Custom Fields	${NAME}	${NAME2}
	[Teardown]	API::Delete System::Custom Fields	${NAME}	${NAME2}

Add Duplicate System Custom Fields
	[Setup]	API::Delete If Exists System::Custom Fields	${NAME}
	API::Add System::Custom Field	${NAME}	${VALUE}
	API::Aux::Should Exist System::Custom Field	${NAME}
	${response}=	API::Add System::Custom Field	${NAME}	${VALUE}	MODE=yes
	${response}=	Set Variable	${response.json()}
#	Comparing response of request with expected message created
	API::Msg::Compare Simple Message	${response}	field_name	Entry already exists.
	[Teardown]	API::Delete System::Custom Field	${NAME}

# Has no especific invalid value for it
#Add Invalid System Custom Fields

Add Invalid System Custom Field | field_name, field_value | EMPTY
#	Understand field_name and field_value values as EMPTY by Default
	${response}=	API::Add System::Custom Field
	${response}=	Set Variable	${response.json()}
#	Comparing response of request with expected message created
	${fields}=	Create List	field_name	field_value
	${values}=	Create List	Validation error.	Validation error.
	API::Msg::Compare Complex Message	${response}	${fields}	${values}

Add Invalid System Custom Field | field_name | EMPTY
	${response}=	API::Add System::Custom Field	${EMPTY}	${VALUE}
	${response}=	Set Variable	${response.json()}
#	Comparing response of request with expected message created
	API::Msg::Compare Simple Message	${response}	field_name	Validation error.	#Validation error.

Add Invalid System Custom Field | field_value | EMPTY
	${response}=	API::Add System::Custom Field	${NAME}	${EMPTY}
	${response}=	Set Variable	${response.json()}
#	Comparing response of request with expected message created
	API::Msg::Compare Simple Message	${response}	field_value	Validation error.	#Validation error.

Add Invalid System Custom Field | field_name, field_value | EXCEEDED
#	Understand field_name and field_value values as EMPTY by Default
	${response}=	API::Add System::Custom Field	${EXCEEDED}	${EXCEEDED}
	${response}=	Set Variable	${response.json()}
#	Comparing response of request with expected message created
	${fields}=	Create List	field_name	field_value
	${values}=	Create List	Validation error.	Validation error.	#Validation error.
	API::Msg::Compare Complex Message	${response}	${fields}	${values}

Add Invalid System Custom Field | field_name | EXCEEDED
	${response}=	API::Add System::Custom Field	${EXCEEDED}	${VALUE}
	${response}=	Set Variable	${response.json()}
#	Comparing response of request with expected message created
	API::Msg::Compare Simple Message	${response}	field_name	Validation error.	#Validation error.

Add Invalid System Custom Field | field_value | EXCEEDED
	${response}=	API::Add System::Custom Field	${NAME}	${EXCEEDED}
	${response}=	Set Variable	${response.json()}
#	Comparing response of request with expected message created
	API::Msg::Compare Simple Message	${response}	field_value	Validation error.	#Validation error.

Delete One System Custom Fields
	[Setup]	API::Add System::Custom Field	${NAME}	${VALUE}
	API::Delete System::Custom Field	${NAME}
	API::Aux::Should Not Exist System::Custom Field	${NAME}
	[Teardown]	Run Keyword If Test Failed	API::Delete If Exists System::Custom Fields	${NAME}

Delete Two System Custom Fields
	[Setup]	SUITE:Add Two System::Custom Fields
	API::Delete System::Custom Fields	${NAME}	${NAME2}
	API::Aux::Should Not Exist System::Custom Fields	${NAME}	${NAME2}
	[Teardown]	Run Keyword If Test Failed	API::Delete If Exists System::Custom Fields	${NAME}	${NAME2}

#	@todo tests of funcionality
#	@todo tests with wrong fields
#
#Update custom field
#	API::Put::System::Custom Fields	custom1	value1.1
#	${custom_fields}=	API::Get::System::Custom Fields
#	Should Not Be Empty	${custom_fields}
#	${exp_dict}=	Create Dictionary
#	Set To Dictionary	${exp_dict}	id=custom1
#	Set To Dictionary	${exp_dict}	field_name=custom1
#	Set To Dictionary	${exp_dict}	field_value=value1.1
#	List Should Contain Value	${custom_fields}	${exp_dict}

*** Keywords ***
SUITE:Setup
	API::Post::Session
	API::Delete If Exists System::Custom Fields	${NAME}	${NAME2}

SUITE:Teardown
	API::Delete If Exists System::Custom Fields	${NAME}	${NAME2}
	API::Delete::Session

SUITE:Should Contain Fields System::Custom Fields
	[Arguments]	${license_dict}
	Dictionary Should Contain Key	${license_dict}	id
	Dictionary Should Contain Key	${license_dict}	field_name
	Dictionary Should Contain Key	${license_dict}	field_value

SUITE:Add Two System::Custom Fields
	${fields}=	Create List	${NAME}	${NAME2}
	${values}=	Create List	${VALUE}	${VALUE}
	API::Add System::Custom Fields	${fields}	${values}