*** Settings ***
Documentation	Test /api/<version>/system/licenses endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot
Force Tags		API	${VERSION}
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	SUITE:Teardown

*** Variables ***
${INVALID_LICENSE}	aaaa-aaaa-aaaa-aaaa
${FIFTY_DEVICES_ACCESS_LICENSE}	${FIFTY_DEVICES_ACCESS_LICENSE}
${FIVE_THOUSAND_DEVICES_ACCESS_LICENSE}	${FIVE_THOUSAND_DEVICES_ACCESS_LICENSE}

*** Test Cases ***
Get Empty System Licenses
	${licenses_list}=	API::Get::System::Licenses
	${licenses_list}=	Set Variable	${licenses_list.json()}
	Length Should Be	${licenses_list}	0
	${empty_list}=	Create List
	Should Be Equal	${licenses_list}	${empty_list}

Get System Licenses
	[Setup]	API::Add System::Licenses	${FIFTY_DEVICES_ACCESS_LICENSE}
	${licenses_list}=	API::Get::System::Licenses
	${licenses_list}=	Set Variable	${licenses_list.json()}
	:FOR	${license_dict}	IN	@{licenses_list}
	\	SUITE:Should Contain Fields System::Licenses	${license_dict}
	[Teardown]	API::Delete System::Licenses	${FIFTY_DEVICES_ACCESS_LICENSE}

Add One System License
	[Setup]	API::Delete If Exists System::Licenses	${FIFTY_DEVICES_ACCESS_LICENSE}
	API::Add System::License	${FIFTY_DEVICES_ACCESS_LICENSE}
	API::Aux::Should Exist System::License	${FIFTY_DEVICES_ACCESS_LICENSE}
	[Teardown]	API::Delete System::Licenses	${FIFTY_DEVICES_ACCESS_LICENSE}

Add Two System License
	[Setup]	API::Delete If Exists System::Licenses	${FIFTY_DEVICES_ACCESS_LICENSE}	${FIVE_THOUSAND_DEVICES_ACCESS_LICENSE}
	API::Add System::Licenses	${FIFTY_DEVICES_ACCESS_LICENSE}	${FIVE_THOUSAND_DEVICES_ACCESS_LICENSE}
	API::Aux::Should Exist System::Licenses	${FIFTY_DEVICES_ACCESS_LICENSE}	${FIVE_THOUSAND_DEVICES_ACCESS_LICENSE}
	[Teardown]	API::Delete System::Licenses	${FIFTY_DEVICES_ACCESS_LICENSE}	${FIVE_THOUSAND_DEVICES_ACCESS_LICENSE}

Add Duplicate System License
	[Setup]	API::Delete If Exists System::Licenses	${FIFTY_DEVICES_ACCESS_LICENSE}
	API::Add System::License	${FIFTY_DEVICES_ACCESS_LICENSE}
	API::Aux::Should Exist System::License	${FIFTY_DEVICES_ACCESS_LICENSE}
	${response}=	API::Add System::License	${FIFTY_DEVICES_ACCESS_LICENSE}
	${response}=	Set Variable	${response.json()}
	API::Msg::Compare Simple Message	${response}	license_key	License Already Installed
	[Teardown]	API::Delete System::Licenses	${FIFTY_DEVICES_ACCESS_LICENSE}

Add Invalid System License | license_key | EMPTY
	${response}=	API::Add System::License	${EMPTY}
	${response}=	Set Variable	${response.json()}
	API::Msg::Compare Simple Message	${response}	license_key	Field must not be empty.

Add Invalid System License | license_key | WORD, NUMBER, POINTS, EXCEEDED, INVALID_LICENSE
	[Setup]	API::Delete If Exists System::Licenses	${WORD}	${NUMBER}	${POINTS}	${EXCEEDED}	${INVALID_LICENSE}
	${OPTIONS}=	Create List	${WORD}	${NUMBER}	${POINTS}	${EXCEEDED}	${INVALID_LICENSE}
	:FOR	${OPTION}	IN	@{OPTIONS}
	\	${response}=	API::Add System::License	${OPTION}
	\	${response}=	Set Variable	${response.json()}
	\	API::Msg::Compare Simple Message	${response}	license_key	Not a Valid License Key
	[Teardown]	Run Keyword If Test Failed	API::Delete If Exists System::Licenses	@{OPTIONS}

Delete One System License
	[Setup]	API::Add System::Licenses	${FIFTY_DEVICES_ACCESS_LICENSE}
	API::Delete System::Licenses	${FIFTY_DEVICES_ACCESS_LICENSE}
	API::Aux::Should Not Exist System::Licenses	${FIFTY_DEVICES_ACCESS_LICENSE}
	[Teardown]	Run Keyword If Test Failed	API::Delete If Exists System::Licenses	${FIFTY_DEVICES_ACCESS_LICENSE}

Delete Two System License
	[Setup]	API::Add System::Licenses	${FIFTY_DEVICES_ACCESS_LICENSE}	${FIVE_THOUSAND_DEVICES_ACCESS_LICENSE}
	API::Delete System::Licenses	${FIFTY_DEVICES_ACCESS_LICENSE}	${FIVE_THOUSAND_DEVICES_ACCESS_LICENSE}
	API::Aux::Should Not Exist System::Licenses	${FIFTY_DEVICES_ACCESS_LICENSE}	${FIVE_THOUSAND_DEVICES_ACCESS_LICENSE}
	[Teardown]	Run Keyword If Test Failed	API::Delete If Exists System::Licenses	${FIFTY_DEVICES_ACCESS_LICENSE}	${FIVE_THOUSAND_DEVICES_ACCESS_LICENSE}

#	@todo tests of funcionality
#	@todo tests with wrong fields
*** Keywords ***
SUITE:Setup
	API::Post::Session

SUITE:Teardown
	API::Delete If Exists System::Licenses	${FIFTY_DEVICES_ACCESS_LICENSE}	${FIVE_THOUSAND_DEVICES_ACCESS_LICENSE}	${INVALID_LICENSE}
	API::Delete::Session

SUITE:Should Contain Fields System::Licenses
	[Arguments]	${license_dict}
	Dictionary Should Contain Key	${license_dict}	id
	Dictionary Should Contain Key	${license_dict}	serial_number
	Dictionary Should Contain Key	${license_dict}	license_key
	Dictionary Should Contain Key	${license_dict}	application
	Dictionary Should Contain Key	${license_dict}	number_of_licenses
	Dictionary Should Contain Key	${license_dict}	type
	Dictionary Should Contain Key	${license_dict}	peer_address
	Dictionary Should Contain Key	${license_dict}	expiration_date
	Dictionary Should Contain Key	${license_dict}	details