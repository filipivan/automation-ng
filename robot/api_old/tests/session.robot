*** Settings ***
Documentation	Test the authentication mechanism available in the API
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	./init.robot
Force Tags	API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***
Test session creation
	${response}=	API::Post::Session
	${session}=	Get From Dictionary	${response.json()}	session

	${EXPECTED}=	Create Dictionary
	Set To Dictionary	${EXPECTED}	session=${session}
	Set To Dictionary	${EXPECTED}	status=success
	Should Be Equal	${response.json()}	${EXPECTED}

Test destroy session
	${response}=	API::Delete::Session
	${EXPECTED}=	Create Dictionary	status=success
	Should Be Equal	${response.json()}	${EXPECTED}