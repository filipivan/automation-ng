*** Settings ***
Documentation	Test /api/<version>/preferences endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	./init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown
Test setup	Reset Default Values

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Get system preferences
	${preferences}=	API::Get System Preferences
	Should Contain All Required System Preferences Fields	${preferences}

Update system preferences
	${payload}=	Get Update Preferences Payload
	API::Update System Preferences	${payload}
	${preferences}=	API::Get System Preferences
	Should Contain All Required System Preferences Fields	${preferences}

	Should Not Be True	${preferences["logo_image_selection"]}
	Should Be Equal As Strings	${preferences["address_location"]}	antartica
	Should Be Equal As Strings	${preferences["logo_image"]}	${EMPTY}
	Should Be Equal As Strings	${preferences["help_url"]}	https://myhelpserver.com
	Should Be Equal As Strings	${preferences["idle_timeout"]}	9999
	Should Contain	${preferences["banner"]}	A dummy banner message
	Should Be True	${preferences["enable_banner"]}
	Should Be Equal As Strings	${preferences["unit_interface"]}	eth1
	Should Be Equal As Strings	${preferences["unit_ipv4_address"]}	127.0.0.1
	Should Be Equal As Strings	${preferences["unit_netmask"]}	255.255.0.0
	Should Be Equal As Strings	${preferences["iso_url"]}	https://myserver/netboot
	Should Be Equal As Strings	${preferences["password"]}	${EMPTY}
	Should Be Equal As Strings	${preferences["url"]}	${EMPTY}
	Should Be Equal As Strings	${preferences["username"]}	${EMPTY}
	Should Be True	${preferences["enable_license_utilization_rate"]}
	Should Be Equal As Strings	${preferences["percentage_to_trigger_events"]}	50
	Run Keyword If	${IS_CONSOLE_SERVER}	Run Keywords	Should Be Equal As Strings	${preferences["console_port_speed"]}	9600
	...	AND	Should Be True	${preferences["enable_alarm_sound_on_fan_failure"]}
	...	AND	Should Be True	${preferences["enable_alarm_sound_when_one_power_supply_is_powered_off"]}
	...	AND	Should Be True	${preferences["enable_local_serial_ports_utilization_rate"]}

*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Authentication	admin	admin
	Apply Factory Default Values

Suite Teardown
	Apply Factory Default Values
	API::Destroy Session

Apply Factory Default Values
	#restore banner message
	Reset Default Values
	#3_disable banner message
	${payload}=	Get Default Values Payload
	Set To Dictionary	${payload}	enable_banner=${FALSE}
	API::Update System Preferences	${payload}

Should Contain All Required System Preferences Fields
	[Arguments]	${preferences}
	Dictionary Should Contain Key	${preferences}	logo_image_selection
	Dictionary Should Contain Key	${preferences}	address_location
	Dictionary Should Contain Key	${preferences}	logo_image
	Dictionary Should Contain Key	${preferences}	coordinates
	Dictionary Should Contain Key	${preferences}	help_url
	Dictionary Should Contain Key	${preferences}	idle_timeout
	Dictionary Should Contain Key	${preferences}	banner
	Dictionary Should Contain Key	${preferences}	enable_banner
	Dictionary Should Contain Key	${preferences}	unit_interface
	Dictionary Should Contain Key	${preferences}	unit_ipv4_address
	Dictionary Should Contain Key	${preferences}	unit_netmask
	Dictionary Should Contain Key	${preferences}	iso_url
	Dictionary Should Contain Key	${preferences}	password
	Dictionary Should Contain Key	${preferences}	url
	Dictionary Should Contain Key	${preferences}	username
	Dictionary Should Contain Key	${preferences}	enable_license_utilization_rate
	Dictionary Should Contain Key	${preferences}	percentage_to_trigger_events
	Run Keyword If	${IS_CONSOLE_SERVER}	Run Keywords	Dictionary Should Contain Key	${preferences}	console_port_speed
	...	AND	Dictionary Should Contain Key	${preferences}	state_of_power_supply_1
	...	AND	Dictionary Should Contain Key	${preferences}	state_of_power_supply_2
	...	AND	Dictionary Should Contain Key	${preferences}	enable_alarm_sound_when_one_power_supply_is_powered_off
	...	AND	Dictionary Should Contain Key	${preferences}	state_of_fan_1
	...	AND	Dictionary Should Contain Key	${preferences}	state_of_fan_2
	...	AND	Dictionary Should Contain Key	${preferences}	enable_alarm_sound_on_fan_failure
	...	AND	Dictionary Should Contain Key	${preferences}	enable_local_serial_ports_utilization_rate

Get Update Preferences Payload
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	logo_image_selection=${TRUE}
	Set To Dictionary	${payload}	address_location=antartica
	Set To Dictionary	${payload}	help_url=https://myhelpserver.com
	Set To Dictionary	${payload}	idle_timeout=9999
	Set To Dictionary	${payload}	banner=A dummy banner message
	Set To Dictionary	${payload}	enable_banner=${TRUE}
	Set To Dictionary	${payload}	unit_interface=eth1
	Set To Dictionary	${payload}	unit_ipv4_address=127.0.0.1
	Set To Dictionary	${payload}	unit_netmask=255.255.0.0
	Set To Dictionary	${payload}	iso_url=https://myserver/netboot
	Set To Dictionary	${payload}	password=${EMPTY}
	Set To Dictionary	${payload}	username=${EMPTY}
	Set To Dictionary	${payload}	enable_license_utilization_rate=${TRUE}
	Set To Dictionary	${payload}	percentage_to_trigger_events=50
	Run Keyword If	${IS_CONSOLE_SERVER}	Run Keywords	Set To Dictionary	${payload}	console_port_speed=9600
	...	AND	Set To Dictionary	${payload}	enable_local_serial_ports_utilization_rate=${TRUE}
	Set To Dictionary	${payload}	enable_alarm_sound_on_fan_failure=${TRUE}
	Set To Dictionary	${payload}	enable_alarm_sound_when_one_power_supply_is_powered_off=${TRUE}
	[return]	${payload}

Get Default Values Payload
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	address_location=${EMPTY}
	Set To Dictionary	${payload}	coordinates=${EMPTY}
	Set To Dictionary	${payload}	help_url=http://www.zpesystems.com/ng/v3_0/NodeGrid-UserGuide-v3_0.pdf
	Set To Dictionary	${payload}	idle_timeout=300
	Set To Dictionary	${payload}	logo_image_selection=${TRUE}
	Set To Dictionary	${payload}	applytypeselection=applylogo-restore
	Set To Dictionary	${payload}	banner=WARNING: This private system is provided for authorized use only and it may be monitored for all lawful purposes to ensure its use. All information including personal information, placed on or sent over this system may be monitored and recorded. Use of this system, authorized or unauthorized, constitutes consent to monitoring your session. Unauthorized use may subject you to criminal prosecution. Evidence of any such unauthorized use may be used for administrative, criminal and/or legal actions.
	Set To Dictionary	${payload}	enable_banner=${TRUE}
	Set To Dictionary	${payload}	unit_interface=eth0
	Set To Dictionary	${payload}	unit_ipv4_address=192.168.160.1
	Set To Dictionary	${payload}	unit_netmask=255.255.255.0
	Set To Dictionary	${payload}	iso_url=http://ServerIPAddress/PATH/FILENAME.ISO
	Set To Dictionary	${payload}	password=${EMPTY}
	Set To Dictionary	${payload}	url=${EMPTY}
	Set To Dictionary	${payload}	username=${EMPTY}
	Set To Dictionary	${payload}	enable_license_utilization_rate=${TRUE}
	Set To Dictionary	${payload}	percentage_to_trigger_events=90
	Run Keyword If	${IS_CONSOLE_SERVER}	Run Keywords	Set To Dictionary	${payload}	console_port_speed=115200
	...	AND	Set To Dictionary	${payload}	enable_alarm_sound_on_fan_failure=${TRUE
	...	AND	Set To Dictionary	${payload}	enable_alarm_sound_when_one_power_supply_is_powered_off=${TRUE}
	...	AND	Set To Dictionary	${payload}	enable_local_serial_ports_utilization_rate=${FALSE}
	[return]	${payload}

Reset Default Values
	${payload}=	Get Default Values Payload
	API::Update System Preferences	${payload}
