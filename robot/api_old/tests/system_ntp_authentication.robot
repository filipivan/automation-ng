*** Settings ***
Documentation	Test /api/<version>/system/datetime/ntp endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	./init.robot

Suite Setup	Suite Setup
Suite Teardown	API::Destroy Session

Force Tags		CLI	API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2	EXCLUDEIN4_0

*** Variable ***
${KEY}		5
${HASH}		SHA1
${PASS}		password
${CPASS}	password

*** Keywords ***
Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Authentication	admin	admin

*** Test Cases ***
Get NTP Authentications
	${ntps}=	API::Send Get Request	system/datetime/ntp
	:FOR	${fw}	IN	@{ntps}
	\	${keys}=	Get Dictionary Keys	${fw}
	\	Length Should Be	${keys}	3
	\	Dictionary Should Contain Key	${fw}	id
	\	Dictionary Should Contain Key	${fw}	key_number
	\	Dictionary Should Contain Key	${fw}	hash_algorithm

Add NTP
	${ntps}=	API::Send Get Request	system/datetime/ntp
	${count}=	Get Length	${ntps}
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	key_number=${KEY}
	Set To Dictionary	${payload}	hash_algorithm=${HASH}
	Set To Dictionary	${payload}	password=${PASS}
	Set To Dictionary	${payload}	confirm-password=${CPASS}
	API::Send Post Request	system/datetime/ntp	${payload}
	${ntps}=	API::Send Get Request	system/datetime/ntp
	Length Should Be 	${ntps}	${count+1}

Get Added NTP
	${field}=	API::Send Get Request	system/datetime/ntp/${KEY}
	Dictionary Should Contain Key	${field}	id
	Dictionary Should Contain Key	${field}	label
	Dictionary Should Contain Key	${field}	hash_algorithm
	Dictionary Should Contain Key	${field}	password
	Dictionary Should Contain Key	${field}	confirm_password
	Dictionary Should Contain Key	${field}	key_number
	Should Be Equal As Strings	${field["id"]}	ntpAdd
	Should Be Equal As Strings	${field["label"]}	add
	Should Be Equal As Strings	${field["hash_algorithm"]}	${HASH}
	Should Be Equal As Strings	${field["password"]}	${EMPTY}
	Should Be Equal As Strings	${field["key_number"]}	${KEY}

Edit Added NTP
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	hash_algorithm=SHA256
	API::Send Put Request	system/datetime/ntp/${KEY}	${payload}
	${ntp}=	API::Send Get Request	system/datetime/ntp/${KEY}
	Should Be Equal As Strings	${ntp["hash_algorithm"]}	SHA256

Delete Added NTP
	${ntps}=	API::Send Get Request	system/datetime/ntp
	${count}=	Get Length	${ntps}

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	key_number=${KEY}
	${headers}=	API::Get Ticket Header
	${response}=	Delete Request	api	/api/${API_VERSION}/system/datetime/ntp	${payload}	headers=${headers}
	Log To Console 	${response}
	Should Be Equal As Strings	${response.status_code}	200

	${ntps}=	API::Send Get Request	system/datetime/ntp
	Length Should Be	${ntps}	${count-1}