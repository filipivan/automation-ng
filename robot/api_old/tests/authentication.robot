*** Settings ***
Documentation	Test the authentication mechanism available in the API
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	./init.robot

Suite Setup	Suite Setup

Force Tags	API	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Test session creation
	API::Post::Session	admin	ZPE.Cl0ud.2019

Test destroy session
	API::Delete::Session

*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}

