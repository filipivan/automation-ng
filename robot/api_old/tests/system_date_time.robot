*** Settings ***
Documentation	Test /api/<version>/system/datetime endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	./init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown
Test setup	Set Default Values

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Get date time
	${date_time}=	API::Get System Date Time
	API:Json Should Contains Required Date Time Fields	${date_time}
	Should Be Equal As Strings	${date_time["date_and_time"]}	auto
	Should Be Equal As Strings	${date_time["server"]}	pool.ntp.org
	Should Be Equal As Strings	${date_time["zone"]}	UTC

Update system date time to auto
	${FIELDS}=	Create Dictionary
	Set To Dictionary	${FIELDS}	date_and_time=auto
	Set To Dictionary	${FIELDS}	server=google.com
	Set To Dictionary	${FIELDS}	zone=GMT-3
	API::Update Date Time	${FIELDS}
	${date_time}=	API::Get System Date Time
	API:Json Should Contains Required Date Time Fields	${date_time}
	Should Be Equal As Strings	${date_time["date_and_time"]}	auto
	Should Be Equal As Strings	${date_time["server"]}	google.com
	Should Be Equal As Strings	${date_time["zone"]}	GMT-3

Update system date time to manual
	${FIELDS}=	Create Dictionary
	Set To Dictionary	${FIELDS}	date_and_time=manual
	Set To Dictionary	${FIELDS}	year=2018
	Set To Dictionary	${FIELDS}	month=01
	Set To Dictionary	${FIELDS}	day=01
	Set To Dictionary	${FIELDS}	hour=10
	Set To Dictionary	${FIELDS}	minute=10
	Set To Dictionary	${FIELDS}	second=00
	API::Update Date Time	${FIELDS}
	${date_time}=	API::Get System Date Time
	API:Json Should Contains Required Date Time Fields	${date_time}
	Should Be Equal As Strings	${date_time["date_and_time"]}	manual
	Should Be Equal As Strings	${date_time["day"]}	01
	Should Be Equal As Strings	${date_time["month"]}	01
	Should Be Equal As Strings	${date_time["year"]}	2018
	Should Be Equal As Strings	${date_time["hour"]}	10
	Should Be Equal As Strings	${date_time["minute"]}	10
	Should Be Equal As Strings	${date_time["zone"]}	UTC

*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Authentication	admin	admin

Suite Teardown
	Set Default Values
	API::Destroy Session

Set Default Values
	${FIELDS}=	Create Dictionary
	Set To Dictionary	${FIELDS}	date_and_time=auto
	Set To Dictionary	${FIELDS}	server=pool.ntp.org
	Set To Dictionary	${FIELDS}	zone=UTC
	API::Update Date Time	${FIELDS}
	${date_time}=	API::Get System Date Time
	API:Json Should Contains Required Date Time Fields	${date_time}
	Should Be Equal As Strings	${date_time["date_and_time"]}	auto
	Should Be Equal As Strings	${date_time["server"]}	pool.ntp.org
	Should Be Equal As Strings	${date_time["zone"]}	UTC

API:Json Should Contains Required Date Time Fields
	[Arguments]	${data}
	[Documentation]	Check if the given data contains all required date and time fields
	[Tags]	API
	Should Contain	${data}	system_time
	Should Contain	${data}	date_and_time
	Should Contain	${data}	day
	Should Contain	${data}	month
	Should Contain	${data}	year
	Should Contain	${data}	last_update
	Should Contain	${data}	server
	Should Contain	${data}	zone
	Should Contain	${data}	hour
	Should Contain	${data}	minute
	Should Contain	${data}	second

