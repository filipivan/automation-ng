*** Settings ***
Documentation	Test /api/<version>/system/licenses endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	./init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Variables ***
${FIFTY_DEVICES_ACCESS_LICENSE_EXPIRED}	${FIFTY_DEVICES_ACCESS_LICENSE_EXPIRED}

*** Test Cases ***
Add license
	API::Add License	${FIFTY_DEVICES_ACCESS_LICENSE_EXPIRED}
	${licenses}=	API::Get Licenses
	:FOR	${license}	IN	@{licenses}
	\	${id}=	Set Variable	${license["license_key"]}
	\	Pass Execution If	'${EXLICENSEID}' == '${id}'	License found
	Fail	License not found

Get licenses
	${licenses}=	API::Get Licenses
	:FOR	${license}	IN	@{licenses}
	\	Check Licenses Fields	${license}

Delete license
	${licenses}=	API::Get Licenses
	:FOR	${license}	IN	@{licenses}
	\	${id}=	Set Variable	${license["license_key"]}
	\	Run Keyword If	'${EXLICENSEID}' == '${id}'	API::Delete License	${license["id"]}
	${licenses}=	API::Get Licenses
	:FOR	${license}	IN	@{licenses}
	\	${id}=	Set Variable	${license["license_key"]}
	\	Run Keyword If	'${EXLICENSEID}' == '${id}'	Fail	License found


*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Authentication	admin	admin

Suite Teardown
	API::Destroy Session

Check Licenses Fields
	[Arguments]	${LICENSE}
	Dictionary Should Contain Key	${LICENSE}	application
	Dictionary Should Contain Key	${LICENSE}	details
	Dictionary Should Contain Key	${LICENSE}	expiration_date
	Dictionary Should Contain Key	${LICENSE}	id
	Dictionary Should Contain Key	${LICENSE}	license_key
	Dictionary Should Contain Key	${LICENSE}	number_of_licenses
	Dictionary Should Contain Key	${LICENSE}	peer_address
	Dictionary Should Contain Key	${LICENSE}	serial_number
	Dictionary Should Contain Key	${LICENSE}	type
