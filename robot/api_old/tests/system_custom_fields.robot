
*** Settings ***
Documentation	Test /api/<version>/system/customfields endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	./init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Add custom field
	API::Add Custom Field	custom1	value1

Get custom fields
	${custom_fields}=	API::Get System Custom Fields
	Should Not Be Empty	${custom_fields}
	${exp_dict}=	Create Dictionary
	Set To Dictionary	${exp_dict}	id=custom1
	Set To Dictionary	${exp_dict}	field_name=custom1
	Set To Dictionary	${exp_dict}	field_value=value1
	List Should Contain Value	${custom_fields}	${exp_dict}

Update custom field
	API::Update Custom Field	custom1	value1.1
	${custom_fields}=	API::Get System Custom Fields
	Should Not Be Empty	${custom_fields}
	${exp_dict}=	Create Dictionary
	Set To Dictionary	${exp_dict}	id=custom1
	Set To Dictionary	${exp_dict}	field_name=custom1
	Set To Dictionary	${exp_dict}	field_value=value1.1
	List Should Contain Value	${custom_fields}	${exp_dict}


Delete custom field
	API::Delete Custom Field	custom1
	${custom_fields}=	API::Get System Custom Fields
	${exp_dict}=	Create Dictionary
	Set To Dictionary	${exp_dict}	id=custom1
	Set To Dictionary	${exp_dict}	field_name=custom1
	Set To Dictionary	${exp_dict}	field_value=value1.1
	List Should Not Contain Value	${custom_fields}	${exp_dict}

*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Authentication	admin	admin
	API::Delete Custom Field	custom1

Suite Teardown
	API::Delete Custom Field	custom1
	API::Delete Custom Field	custom1
	API::Destroy Session
