*** Settings ***
Documentation	Test /api/<version>/tracking endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	./init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Get Open Session
	${sessions}=	API::Get Open Session
	:FOR	${session}	IN	@{sessions}
	\	${keys}=	Get Dictionary Keys	${session}
	\	Dictionary Should Contain Key	${session}	source_ip
	\	Dictionary Should Contain Key	${session}	device_name
	\	Dictionary Should Contain Key	${session}	type
	\	Dictionary Should Contain Key	${session}	session_start
	\	Dictionary Should Contain Key	${session}	id
	\	Dictionary Should Contain Key	${session}	ref
	\	Dictionary Should Contain Key	${session}	mode
	\	Dictionary Should Contain Key	${session}	user
	\	Length Should Be	${keys}	8

Get Devices Session
	${sessions}=	API::Get Devices Session
	:FOR	${session}	IN	@{sessions}
	\	${keys}=	Get Dictionary Keys	${session}
	\	Dictionary Should Contain Key	${session}	id
	\	Dictionary Should Contain Key	${session}	device_name
	\	Dictionary Should Contain Key	${session}	number_of_sessions
	\	Dictionary Should Contain Key	${session}	users
	\	Length Should Be	${keys}	4

Get Events
	${events}=	API::Get Events
	:FOR	${event}	IN	@{events}
	\	${keys}=	Get Dictionary Keys	${event}
	\	Dictionary Should Contain Key	${event}	id
	\	Dictionary Should Contain Key	${event}	category
	\	Dictionary Should Contain Key	${event}	description
	\	Dictionary Should Contain Key	${event}	event_number
	\	Dictionary Should Contain Key	${event}	occurrences
	\	Length Should Be	${keys}	5

Get Discovery Logs
	${logs}=	API::Get Discovery Logs
	:FOR	${log}	IN	@{logs}
	\	${keys}=	Get Dictionary Keys	${log}
	\	Dictionary Should Contain Key	${log}	action
	\	Dictionary Should Contain Key	${log}	date
	\	Dictionary Should Contain Key	${log}	device_name
	\	Dictionary Should Contain Key	${log}	discovery_method
	\	Dictionary Should Contain Key	${log}	id
	\	Dictionary Should Contain Key	${log}	ip_address
	\	Length Should Be	${keys}	6

Get CPU Usage
	${usage}=	API::Get CPU Usage
	:FOR	${cpu}	IN	@{usage}
	\	${keys}=	Get Dictionary Keys	${cpu}
	\	Dictionary Should Contain Key	${cpu}	id
	\	Dictionary Should Contain Key	${cpu}	idle_%
	\	Dictionary Should Contain Key	${cpu}	system_%
	\	Dictionary Should Contain Key	${cpu}	user_%
	\	Dictionary Should Contain Key	${cpu}	waiting_i/o_%
	\	Length Should Be	${cpu}	5
Get Memory Usage
	${usage}=	API::Get Memory Usage
	:FOR	${mem}	IN	@{usage}
	\	${keys}=	Get Dictionary Keys	${mem}
	\	Dictionary Should Contain Key	${mem}	id
	\	Dictionary Should Contain Key	${mem}	free_(kb)
	\	Dictionary Should Contain Key	${mem}	total_(kb)
	\	Dictionary Should Contain Key	${mem}	memory_type
	\	Dictionary Should Contain Key	${mem}	used_(kb)
	\	Length Should Be	${mem}	5

Get Disk Usage
	${usage}=	API::Get Disk Usage
	:FOR	${disk}	IN	@{usage}
	\	${keys}=	Get Dictionary Keys	${disk}
	\	Dictionary Should Contain Key	${disk}	id
	\	Dictionary Should Contain Key	${disk}	available_(kb)
	\	Dictionary Should Contain Key	${disk}	description
	\	Dictionary Should Contain Key	${disk}	partition
	\	Dictionary Should Contain Key	${disk}	size_(kb)
	\	Dictionary Should Contain Key	${disk}	use_%
	\	Dictionary Should Contain Key	${disk}	used_(kb)
	\	Length Should Be	${disk}	7


Get Network Interfaces
	${interfaces}=	API::Get Network Interfaces
	:FOR	${interface}	IN	@{interfaces}
	\	${keys}=	Get Dictionary Keys	${interface}
	\	Dictionary Should Contain Key	${interface}	id
	\	Dictionary Should Contain Key	${interface}	collisions
	\	Dictionary Should Contain Key	${interface}	errors
	\	Dictionary Should Contain Key	${interface}	ifindex
	\	Dictionary Should Contain Key	${interface}	ifname
	\	Dictionary Should Contain Key	${interface}	state
	\	Dictionary Should Contain Key	${interface}	rx_packets
	\	Dictionary Should Contain Key	${interface}	tx_packets
	\	Length Should Be	${interface}	8

Get Network LLDP
	${lldps}=	API::Get Network LLDP
	:FOR	${lldp}	IN	@{lldps}
	\	${keys}=	Get Dictionary Keys	${lldp}
	\	Dictionary Should Contain Key	${lldp}	id
	\	Dictionary Should Contain Key	${lldp}	chassis_id
	\	Dictionary Should Contain Key	${lldp}	connection
	\	Dictionary Should Contain Key	${lldp}	ipv4_mgmt_addr
	\	Dictionary Should Contain Key	${lldp}	ipv6_mgmt_addr
	\	Dictionary Should Contain Key	${lldp}	port_id
	\	Dictionary Should Contain Key	${lldp}	system_description
	\	Dictionary Should Contain Key	${lldp}	system_name
	\	Dictionary Should Contain Key	${lldp}	age
	\	Dictionary Should Contain Key	${lldp}	type
	\	Length Should Be	${lldp}	10

Get Network Routing Table
	${routes}=	API::Get Network Routing Table
	:FOR	${route}	IN	@{routes}
	\	${keys}=	Get Dictionary Keys	${route}
	\	Dictionary Should Contain Key	${route}	id
	\	Dictionary Should Contain Key	${route}	destination
	\	Dictionary Should Contain Key	${route}	from
	\	Dictionary Should Contain Key	${route}	gateway
	\	Dictionary Should Contain Key	${route}	interface
	\	Dictionary Should Contain Key	${route}	metric
	\	Dictionary Should Contain Key	${route}	table
	\	Length Should Be	${route}	7

Get USB Devices
	${devices}=	API::Get USB Devices
	:FOR	${device}	IN	@{devices}
	\	${keys}=	Get Dictionary Keys	${device}
	\	Dictionary Should Contain Key	${device}	description
	\	Dictionary Should Contain Key	${device}	detected_type
	\	Dictionary Should Contain Key	${device}	id
	\	Dictionary Should Contain Key	${device}	kernel_device
	\	Dictionary Should Contain Key	${device}	usb_id
	\	Dictionary Should Contain Key	${device}	usb_path
	\	Dictionary Should Contain Key	${device}	usb_port
	\	Length Should Be	${device}	7

*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Authentication	admin	admin

Suite Teardown
	API::Destroy Session

