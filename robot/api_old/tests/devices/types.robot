*** Settings ***
Documentation	Test /api/<version>/devices/types endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Setup Test Suite
Suite Teardown	Teardown Test Suite

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Variables ***
${CLONE_NAME}	clone

*** Test Cases ***

Get Devices Types
	${types}=	API::Get::Devices::Types
	${count}=	Get Length	${types}
	Set Suite Variable	${ORIGINAL_COUNT}	${count}
	:FOR	${type}	IN	@{types}
	\	Length Should Be	${type}	4
	\	Dictionary Should Contain Key	${type}	id
	\	Dictionary Should Contain Key	${type}	family
	\	Dictionary Should Contain Key	${type}	protocol
	\	Dictionary Should Contain Key	${type}	device_type_name

Clone Device Type
	API::Put::Devices::Types::${NAME}::Clone	device_console	${CLONE_NAME}
	${types}=	API::Get::Devices::Types
	Length Should Be	${types}	${ORIGINAL_COUNT+1}

Get Device Type
	${info}=	API::Get::Devices::Types::${NAME}	${CLONE_NAME}
	Should Be Equal As Strings	${info["console_escape_sequence"]}	 ${EMPTY}
	Should Be Equal As Strings	${info["family"]}	 device console
	Should Be Equal As Strings	${info["login_prompt"]}	 (ogin:|sername:)
	Should Be Equal As Strings	${info["password_prompt"]}	 sword:
	Should Be Equal As Strings	${info["protocol"]}	 ssh
	Should Be Equal As Strings	${info["command_prompt"]}	 [^@:>]+(@?)([^@:>]?)+(:?)([^@:>]?)+[#\\$>]
	Should Be Equal As Strings	${info["device_type_name"]}	 ${CLONE_NAME}
	Should Be Equal As Strings	${info["ssh_options"]}	 ${EMPTY}

Update Device Type
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	device_type_name=${CLONE_NAME}
	Set To Dictionary	${payload}	family=Device Console
	Set To Dictionary	${payload}	protocol=ssh
	Set To Dictionary	${payload}	ssh_options=-v
	Set To Dictionary	${payload}	login_prompt=(ogin:|sernamee:)
	Set To Dictionary	${payload}	password_prompt=ssword:
	Set To Dictionary	${payload}	command_prompt=[^@:>]+(@?)([^@:>]?)+(:?)([^@:>]?)+[#\\%>]
	Set To Dictionary	${payload}	console_escape_sequence=${EMPTY}
	API::Put::Devices::Types::${NAME}	${CLONE_NAME}	${payload}

Get Device Type After Update
	${info}=	API::Get::Devices::Types::${NAME}	${CLONE_NAME}
	Should Be Equal As Strings	${info["console_escape_sequence"]}	 ${EMPTY}
	Should Be Equal As Strings	${info["family"]}	 device console
	Should Be Equal As Strings	${info["login_prompt"]}	 (ogin:|sernamee:)
	Should Be Equal As Strings	${info["password_prompt"]}	 ssword:
	Should Be Equal As Strings	${info["protocol"]}	 ssh
	Should Be Equal As Strings	${info["command_prompt"]}	 [^@:>]+(@?)([^@:>]?)+(:?)([^@:>]?)+[#\\%>]
	Should Be Equal As Strings	${info["device_type_name"]}	 ${CLONE_NAME}
	Should Be Equal As Strings	${info["ssh_options"]}	 -v

Delete Device Type
	API::Delete::Devices::Types	${CLONE_NAME}
	${types}=	API::Get::Devices::Types
	Length Should Be	${types}	${ORIGINAL_COUNT}

*** Keywords ***

Setup Test Suite
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin

Teardown Test Suite
	API::Delete::Devices::Types	clone
	API::Delete::Session

