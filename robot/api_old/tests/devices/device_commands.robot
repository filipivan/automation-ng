*** Settings ***
Documentation	Test /api/<version>/devices/<name>/customfields endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Variables ***
${DEVICE}=	device

*** Test Cases ***

Get Device Commands
	${fields}=	API::Get::Devices::Table::${NAME}::Commands	${DEVICE}
	Check Commands Fields	${fields}
	${length}=	Get Length	${fields}
	Set Suite Variable	${ORIGINAL_COMMANDS}	${length}

Add Device KVM Command
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	command=kvm
	Set To Dictionary	${payload}	enabled=${FALSE}
	API::Post::Devices::Table::${NAME}::Command	${DEVICE}	${payload}

	${fields}=	API::Get::Devices::Table::${NAME}::Commands	${DEVICE}
	Check Commands Fields	${fields}
	Length Should Be	${fields}	${ORIGINAL_COMMANDS+1}

Add Device Outlet Command
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	command=outlet
	Set To Dictionary	${payload}	enabled=${FALSE}
	API::Post::Devices::Table::${NAME}::Command	${DEVICE}	${payload}

	${fields}=	API::Get::Devices::Table::${NAME}::Commands	${DEVICE}
	Check Commands Fields	${fields}
	Length Should Be	${fields}	${ORIGINAL_COMMANDS+2}

Get Device Command
	${info}=	API::Get::Devices::Table::${NAME}::Command	${DEVICE}	kvm
	Dictionary Should Contain Key	${info}	command
	Dictionary Should Contain Key	${info}	enabled
	Dictionary Should Contain Key	${info}	protocol
	Dictionary Should Contain Key	${info}	type_extension
	Length Should Be	${info}	4
	Should Be Equal As Strings	${info["command"]}	KVM
	Should Be Equal As Strings	${info["protocol"]}	SSH
	Should Be Equal As Strings	${info["type_extension"]}	${EMPTY}
	Should Not Be True	${info["enabled"]}

	${info}=	API::Get::Devices::Table::${NAME}::Command	${DEVICE}	web
	${info}=	API::Get::Devices::Table::${NAME}::Command	${DEVICE}	datalog
	${info}=	API::Get::Devices::Table::${NAME}::Command	${DEVICE}	eventlog
	${info}=	API::Get::Devices::Table::${NAME}::Command	${DEVICE}	power
	${info}=	API::Get::Devices::Table::${NAME}::Command	${DEVICE}	console
	${info}=	API::Get::Devices::Table::${NAME}::Command	${DEVICE}	outlet

Update Device Command
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	enabled=${TRUE}
	Set To Dictionary	${payload}	type_extension=ilo.py
	API::Put::Devices::Table::${NAME} Command	${DEVICE}	kvm	${payload}

Get Device Command After Update
	${info}=	API::Get::Devices::Table::${NAME}::Command	${DEVICE}	kvm
	Length Should Be	${info}	4
	Dictionary Should Contain Key	${info}	command
	Dictionary Should Contain Key	${info}	enabled
	Dictionary Should Contain Key	${info}	protocol
	Dictionary Should Contain Key	${info}	type_extension
	Should Be Equal As Strings	${info["command"]}	KVM
	Should Be Equal As Strings	${info["protocol"]}	SSH
	Should Be Equal As Strings	${info["type_extension"]}	ilo.py
	Should Be True	${info["enabled"]}

Delete Command
	API::Delete::Devices::Table::${NAME}::Command	${DEVICE}	kvm
	API::Delete::Devices::Table::${NAME}::Command	${DEVICE}	outlet

	${fields}=	API::Get::Devices::Table::${NAME}::Commands	${DEVICE}
	Check Commands Fields	${fields}
	Length Should Be	${fields}	${ORIGINAL_COMMANDS}

*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin
	API::Delete::Devices::Table	${DEVICE}
	Add Dummy Device Console Device

Suite Teardown
	API::Delete::Devices::Table	${DEVICE}
	API::Delete::Session

Add Dummy Device Console Device
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	spm_name=${DEVICE}
	Set To Dictionary	${payload}	type=ilom
	Set To Dictionary	${payload}	phys_addr=${HOST}
	Set To Dictionary	${payload}	tcpport=22
	Set To Dictionary	${payload}	chassis=${EMPTY}
	Set To Dictionary	${payload}	blade=${EMPTY}
	Set To Dictionary	${payload}	addr_location=antartica
	Set To Dictionary	${payload}	coordinates=
	Set To Dictionary	${payload}	url=http://%IP
	Set To Dictionary	${payload}	webhtml5=${TRUE}
	Set To Dictionary	${payload}	username=admin
	Set To Dictionary	${payload}	askpassword=no
	Set To Dictionary	${payload}	passwordfirst=admin
	Set To Dictionary	${payload}	passwordconf=admin
	Set To Dictionary	${payload}	vmmanager=${EMPTY}
	Set To Dictionary	${payload}	discname=${FALSE}
	Set To Dictionary	${payload}	multisession=${TRUE}
	Set To Dictionary	${payload}	break-sequence=~break
	Set To Dictionary	${payload}	icon_app_options=linux_color.png
	Set To Dictionary	${payload}	icon_file_path=undefined
	Set To Dictionary	${payload}	status=enabled
	Set To Dictionary	${payload}	currentstatus=enabled
	Set To Dictionary	${payload}	ttlType=never
	Set To Dictionary	${payload}	ttlDate=${EMPTY}
	Set To Dictionary	${payload}	ttlDays=${EMPTY}
	Set To Dictionary	${payload}	childidserial=${EMPTY}
	Set To Dictionary	${payload}	childidkvm=${EMPTY}
	Set To Dictionary	${payload}	inbhotkey=^Ec
	Set To Dictionary	${payload}	inbpowerctlkey=^O
	Set To Dictionary	${payload}	inbshowinfo=${TRUE}
	Set To Dictionary	${payload}	inbipAddr=${EMPTY}
	Set To Dictionary	${payload}	inbInterf=eth0
	Set To Dictionary	${payload}	inbipTeln=${TRUE}
	Set To Dictionary	${payload}	inbipTelnPort=23
	Set To Dictionary	${payload}	inbipRawPort=${EMPTY}
	Set To Dictionary	${payload}	inbSecipAddr=${EMPTY}
	Set To Dictionary	${payload}	inbSecInterf=eth0
	Set To Dictionary	${payload}	inbSecipTeln=${TRUE}
	Set To Dictionary	${payload}	inbSecipTelnPort=23
	Set To Dictionary	${payload}	inbSecipRawPort=${EMPTY}
	Set To Dictionary	${payload}	inbssh=${TRUE}
	Set To Dictionary	${payload}	inbsshTCP=${EMPTY}
	Set To Dictionary	${payload}	inbtelnTCP=${EMPTY}
	Set To Dictionary	${payload}	inbrawTCP=${EMPTY}
	Set To Dictionary	${payload}	multisessRW=${FALSE}
	Set To Dictionary	${payload}	enable-break=${FALSE}
	Set To Dictionary	${payload}	inbauth=${FALSE}
	Set To Dictionary	${payload}	inbipEnable=${FALSE}
	Set To Dictionary	${payload}	inbipRaw=${FALSE}
	Set To Dictionary	${payload}	inbSecipEnable=${FALSE}
	Set To Dictionary	${payload}	inbSecipRaw=${FALSE}
	Set To Dictionary	${payload}	inbteln=${FALSE}
	Set To Dictionary	${payload}	inbraw=${FALSE}
	API::Post::Devices::Table	${payload}

Check Commands Fields
	[Arguments]	${COMMANDS}
	:FOR	${command}	IN	@{COMMANDS}
	\	Dictionary Should Contain Key	${command}	command
	\	Dictionary Should Contain Key	${command}	command_status
	\	Dictionary Should Contain Key	${command}	id
	\	Dictionary Should Contain Key	${command}	protocol_status
	\	Dictionary Should Contain Key	${command}	protocol
	\	Length Should Be	${command}	5
