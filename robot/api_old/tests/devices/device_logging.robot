*** Settings ***
Documentation	Test /api/<version>/devices endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Variables ***
${DEVICE}=	device

*** Test Cases ***

Get Device Logging
	${info}=	API::Get::Devices::Table::${NAME}::Logging	${DEVICE}
	Dictionary Should Contain Key	${info}	name
	Dictionary Should Contain Key	${info}	enable_data_logging_alerts
	Dictionary Should Contain Key	${info}	data_logging
	Dictionary Should Contain Key	${info}	data_script_1
	Dictionary Should Contain Key	${info}	data_script_2
	Dictionary Should Contain Key	${info}	data_script_3
	Dictionary Should Contain Key	${info}	data_script_4
	Dictionary Should Contain Key	${info}	data_script_5
	Dictionary Should Contain Key	${info}	data_string_1
	Dictionary Should Contain Key	${info}	data_string_2
	Dictionary Should Contain Key	${info}	data_string_3
	Dictionary Should Contain Key	${info}	data_string_4
	Dictionary Should Contain Key	${info}	data_string_5
	#Dictionary Should Contain Key	${info}	event_logging
	#Dictionary Should Contain Key	${info}	enable_event_logging_alerts
	#Dictionary Should Contain Key	${info}	event_log_frequecy
	#Dictionary Should Contain Key	${info}	event_log_unit
	#Dictionary Should Contain Key	${info}	event_script_1
	#Dictionary Should Contain Key	${info}	event_script_2
	#Dictionary Should Contain Key	${info}	event_script_3
	#Dictionary Should Contain Key	${info}	event_script_4
	#Dictionary Should Contain Key	${info}	event_script_5
	#Dictionary Should Contain Key	${info}	event_string_1
	#Dictionary Should Contain Key	${info}	event_string_2
	#Dictionary Should Contain Key	${info}	event_string_3
	#Dictionary Should Contain Key	${info}	event_string_4
	#Dictionary Should Contain Key	${info}	event_string_5
	Length Should Be	${info}	13

Update Device Logging
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}
	Set To Dictionary	${payload}	data_logging=${TRUE}
	Set To Dictionary	${payload}	enable_data_logging_alerts=${TRUE}
	Set To Dictionary	${payload}	data_string_1=sdfsdf
	Set To Dictionary	${payload}	data_script_1=PowerCycleDevice_sample.sh
	Set To Dictionary	${payload}	data_string_2=sdfsdf
	Set To Dictionary	${payload}	data_script_2=ShutdownDevice_sample.sh
	Set To Dictionary	${payload}	data_string_3=sdfsdf
	Set To Dictionary	${payload}	data_script_3=PowerCycleDevice_sample.sh
	Set To Dictionary	${payload}	data_string_4=sfsdf
	Set To Dictionary	${payload}	data_script_4=ShutdownDevice_sample.sh
	Set To Dictionary	${payload}	data_string_5=sdfsdf
	Set To Dictionary	${payload}	data_script_5=PowerCycleDevice_sample.sh
	API::Put::Devices::Table::${NAME}::Logging	${DEVICE}	${PAYLOAD}

Get Device Logging After Update
	Sleep	30 seconds
	${info}=	API::Get::Devices::Table::${NAME}::Logging	${DEVICE}
	Should Be True	${info["data_logging"]}
	Should Be True	${info["enable_data_logging_alerts"]}
	Should Be Equal As Strings	${info["data_string_1"]}	sdfsdf
	Should Be Equal As Strings	${info["data_script_1"]}	PowerCycleDevice_sample.sh
	Should Be Equal As Strings	${info["data_string_2"]}	sdfsdf
	Should Be Equal As Strings	${info["data_script_2"]}	ShutdownDevice_sample.sh
	Should Be Equal As Strings	${info["data_string_3"]}	sdfsdf
	Should Be Equal As Strings	${info["data_script_3"]}	PowerCycleDevice_sample.sh
	Should Be Equal As Strings	${info["data_string_4"]}	sfsdf
	Should Be Equal As Strings	${info["data_script_4"]}	ShutdownDevice_sample.sh
	Should Be Equal As Strings	${info["data_string_5"]}	sdfsdf
	Should Be Equal As Strings	${info["data_script_5"]}	PowerCycleDevice_sample.sh
	Length Should Be	${info}	13

*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin
	API::Delete::Devices::Table	${DEVICE}
	Add Dummy Device Console Device

Suite Teardown
	API::Delete::Devices::Table	${DEVICE}
	API::Delete::Session

Add Dummy Device Console Device
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	spm_name=${DEVICE}
	Set To Dictionary	${payload}	type=device_console
	Set To Dictionary	${payload}	phys_addr=${HOST}
	Set To Dictionary	${payload}	tcpport=22
	Set To Dictionary	${payload}	chassis=${EMPTY}
	Set To Dictionary	${payload}	blade=${EMPTY}
	Set To Dictionary	${payload}	addr_location=antartica
	Set To Dictionary	${payload}	coordinates=
	Set To Dictionary	${payload}	url=http://%IP
	Set To Dictionary	${payload}	webhtml5=${TRUE}
	Set To Dictionary	${payload}	username=admin
	Set To Dictionary	${payload}	askpassword=no
	Set To Dictionary	${payload}	passwordfirst=admin
	Set To Dictionary	${payload}	passwordconf=admin
	Set To Dictionary	${payload}	vmmanager=${EMPTY}
	Set To Dictionary	${payload}	discname=${TRUE}
	Set To Dictionary	${payload}	multisession=${TRUE}
	Set To Dictionary	${payload}	break-sequence=~break
	Set To Dictionary	${payload}	icon_app_options=linux_color.png
	Set To Dictionary	${payload}	icon_file_path=undefined
	Set To Dictionary	${payload}	status=enabled
	Set To Dictionary	${payload}	currentstatus=enabled
	Set To Dictionary	${payload}	ttlType=never
	Set To Dictionary	${payload}	ttlDate=${EMPTY}
	Set To Dictionary	${payload}	ttlDays=${EMPTY}
	Set To Dictionary	${payload}	childidserial=${EMPTY}
	Set To Dictionary	${payload}	childidkvm=${EMPTY}
	Set To Dictionary	${payload}	inbhotkey=^Ec
	Set To Dictionary	${payload}	inbpowerctlkey=^O
	Set To Dictionary	${payload}	inbshowinfo=${TRUE}
	Set To Dictionary	${payload}	inbipAddr=${EMPTY}
	Set To Dictionary	${payload}	inbInterf=eth0
	Set To Dictionary	${payload}	inbipTeln=${TRUE}
	Set To Dictionary	${payload}	inbipTelnPort=23
	Set To Dictionary	${payload}	inbipRawPort=${EMPTY}
	Set To Dictionary	${payload}	inbSecipAddr=${EMPTY}
	Set To Dictionary	${payload}	inbSecInterf=eth0
	Set To Dictionary	${payload}	inbSecipTeln=${TRUE}
	Set To Dictionary	${payload}	inbSecipTelnPort=23
	Set To Dictionary	${payload}	inbSecipRawPort=${EMPTY}
	Set To Dictionary	${payload}	inbssh=${TRUE}
	Set To Dictionary	${payload}	inbsshTCP=${EMPTY}
	Set To Dictionary	${payload}	inbtelnTCP=${EMPTY}
	Set To Dictionary	${payload}	inbrawTCP=${EMPTY}
	Set To Dictionary	${payload}	multisessRW=${FALSE}
	Set To Dictionary	${payload}	enable-break=${FALSE}
	Set To Dictionary	${payload}	inbauth=${FALSE}
	Set To Dictionary	${payload}	inbipEnable=${FALSE}
	Set To Dictionary	${payload}	inbipRaw=${FALSE}
	Set To Dictionary	${payload}	inbSecipEnable=${FALSE}
	Set To Dictionary	${payload}	inbSecipRaw=${FALSE}
	Set To Dictionary	${payload}	inbteln=${FALSE}
	Set To Dictionary	${payload}	inbraw=${FALSE}
	API::Post::Devices::Table	${payload}
	Sleep	10 seconds

