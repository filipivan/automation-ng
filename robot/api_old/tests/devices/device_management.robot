*** Settings ***
Documentation	Test /api/<version>/devices/<name>/management endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Variables ***
${DEVICE}=	device

*** Test Cases ***

Get Device Management
	${info}=	API::Get::Devices::Table::${NAME}::Management	${DEVICE}
	Dictionary Should Contain Key	${info}	name
	Dictionary Should Contain Key	${info}	snmp
	Dictionary Should Contain Key	${info}	snmp_commmunity
	Dictionary Should Contain Key	${info}	snmp_version
	Dictionary Should Contain Key	${info}	snmp_commmunity
	Dictionary Should Contain Key	${info}	snmpv3_username
	Dictionary Should Contain Key	${info}	snmpv3_security_level
	Dictionary Should Contain Key	${info}	snmpv3_authentication_algorithm
	Dictionary Should Contain Key	${info}	snmpv3_authentication_password
	Dictionary Should Contain Key	${info}	snmpv3_privacy_algorithm
	Dictionary Should Contain Key	${info}	snmpv3_privacy_password
	Dictionary Should Contain Key	${info}	monitoring_snmp_template
	Dictionary Should Contain Key	${info}	monitoring_snmp_interval
	Dictionary Should Contain Key	${info}	monitoring_nominal_name
	Dictionary Should Contain Key	${info}	monitoring_nominal_type
	Dictionary Should Contain Key	${info}	monitoring_nominal_value
	Dictionary Should Contain Key	${info}	monitoring_nominal_interval
	Dictionary Should Contain Key	${info}	on_session_start
	Dictionary Should Contain Key	${info}	on_session_stop
	Dictionary Should Contain Key	${info}	on_device_up
	Dictionary Should Contain Key	${info}	on_device_down
	Dictionary Should Contain Key	${info}	monitoring_snmp
	Dictionary Should Contain Key	${info}	monitoring_nominal
	Length Should Be 	${info}	22

Update Device Management
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	snmp=${TRUE}
	Set To Dictionary	${payload}	snmp_version=2
	Set To Dictionary	${payload}	snmp_commmunity=public2
	Set To Dictionary	${payload}	snmpv3_username=${EMPTY}
	Set To Dictionary	${payload}	snmpv3_security_level=noauth
	Set To Dictionary	${payload}	snmpv3_authentication_algorithm=SHA
	Set To Dictionary	${payload}	snmpv3_authentication_password=${EMPTY}
	Set To Dictionary	${payload}	snmpv3_privacy_algorithm=AES
	Set To Dictionary	${payload}	snmpv3_privacy_password=${EMPTY}
	Set To Dictionary	${payload}	monitoring_snmp_template=pdu_apc_rpdu
	Set To Dictionary	${payload}	monitoring_snmp_interval=120
	Set To Dictionary	${payload}	monitoring_nominal_name=name
	Set To Dictionary	${payload}	monitoring_nominal_type=power
	Set To Dictionary	${payload}	monitoring_nominal_value=10
	Set To Dictionary	${payload}	monitoring_nominal_interval=120
	Set To Dictionary	${payload}	on_session_start=${EMPTY}
	Set To Dictionary	${payload}	on_session_stop=${EMPTY}
	Set To Dictionary	${payload}	on_device_up=${EMPTY}
	Set To Dictionary	${payload}	on_device_down=${EMPTY}
	Set To Dictionary	${payload}	monitoring_snmp=${TRUE}
	Set To Dictionary	${payload}	monitoring_nominal=${TRUE}
	API::Put::Devices::Table::${NAME}::Management	${DEVICE}	${payload}

Get Device Management After Update
	${info}=	API::Get::Devices::Table::${NAME}::Management	${DEVICE}
	Length Should Be 	${info}	22
	Should Be True	${info["snmp"]}
	Should Be Equal As Strings	${info["snmp_version"]}	2
	Should Be Equal As Strings	${info["snmp_commmunity"]}	public2
	Should Be Equal As Strings	${info["snmpv3_username"]}	${EMPTY}
	Should Be Equal As Strings	${info["snmpv3_security_level"]}	noauth
	Should Be Equal As Strings	${info["snmpv3_authentication_algorithm"]}	SHA
	Should Be Equal As Strings	${info["snmpv3_privacy_algorithm"]}	AES
	Should Be Equal As Strings	${info["monitoring_snmp_template"]}	pdu_apc_rpdu
	Should Be Equal As Strings	${info["monitoring_snmp_interval"]}	120
	Should Be Equal As Strings	${info["monitoring_nominal_name"]}	name
	Should Be Equal As Strings	${info["monitoring_nominal_type"]}	power
	Should Be Equal As Strings	${info["monitoring_nominal_value"]}	10
	Should Be Equal As Strings	${info["monitoring_nominal_interval"]}	120
	Should Be Equal As Strings	${info["on_session_start"]}	${EMPTY}
	Should Be Equal As Strings	${info["on_session_stop"]}	${EMPTY}
	Should Be Equal As Strings	${info["on_device_up"]}	${EMPTY}
	Should Be Equal As Strings	${info["on_device_down"]}	${EMPTY}
	Should Be True	${info["monitoring_snmp"]}
	Should Be True	${info["monitoring_nominal"]}

*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin
	API::Delete::Devices::Table	${DEVICE}
	Add Dummy Device Console Device

Suite Teardown
	API::Delete::Devices::Table	${DEVICE}
	API::Delete::Session

Add Dummy Device Console Device
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	spm_name=${DEVICE}
	Set To Dictionary	${payload}	type=device_console
	Set To Dictionary	${payload}	phys_addr=${HOST}
	Set To Dictionary	${payload}	tcpport=22
	Set To Dictionary	${payload}	chassis=${EMPTY}
	Set To Dictionary	${payload}	blade=${EMPTY}
	Set To Dictionary	${payload}	addr_location=antartica
	Set To Dictionary	${payload}	coordinates=
	Set To Dictionary	${payload}	url=http://%IP
	Set To Dictionary	${payload}	webhtml5=${TRUE}
	Set To Dictionary	${payload}	username=admin
	Set To Dictionary	${payload}	askpassword=no
	Set To Dictionary	${payload}	passwordfirst=admin
	Set To Dictionary	${payload}	passwordconf=admin
	Set To Dictionary	${payload}	vmmanager=${EMPTY}
	Set To Dictionary	${payload}	discname=${TRUE}
	Set To Dictionary	${payload}	multisession=${TRUE}
	Set To Dictionary	${payload}	break-sequence=~break
	Set To Dictionary	${payload}	icon_app_options=linux_color.png
	Set To Dictionary	${payload}	icon_file_path=undefined
	Set To Dictionary	${payload}	status=enabled
	Set To Dictionary	${payload}	currentstatus=enabled
	Set To Dictionary	${payload}	ttlType=never
	Set To Dictionary	${payload}	ttlDate=${EMPTY}
	Set To Dictionary	${payload}	ttlDays=${EMPTY}
	Set To Dictionary	${payload}	childidserial=${EMPTY}
	Set To Dictionary	${payload}	childidkvm=${EMPTY}
	Set To Dictionary	${payload}	inbhotkey=^Ec
	Set To Dictionary	${payload}	inbpowerctlkey=^O
	Set To Dictionary	${payload}	inbshowinfo=${TRUE}
	Set To Dictionary	${payload}	inbipAddr=${EMPTY}
	Set To Dictionary	${payload}	inbInterf=eth0
	Set To Dictionary	${payload}	inbipTeln=${TRUE}
	Set To Dictionary	${payload}	inbipTelnPort=23
	Set To Dictionary	${payload}	inbipRawPort=${EMPTY}
	Set To Dictionary	${payload}	inbSecipAddr=${EMPTY}
	Set To Dictionary	${payload}	inbSecInterf=eth0
	Set To Dictionary	${payload}	inbSecipTeln=${TRUE}
	Set To Dictionary	${payload}	inbSecipTelnPort=23
	Set To Dictionary	${payload}	inbSecipRawPort=${EMPTY}
	Set To Dictionary	${payload}	inbssh=${TRUE}
	Set To Dictionary	${payload}	inbsshTCP=${EMPTY}
	Set To Dictionary	${payload}	inbtelnTCP=${EMPTY}
	Set To Dictionary	${payload}	inbrawTCP=${EMPTY}
	Set To Dictionary	${payload}	multisessRW=${FALSE}
	Set To Dictionary	${payload}	enable-break=${FALSE}
	Set To Dictionary	${payload}	inbauth=${FALSE}
	Set To Dictionary	${payload}	inbipEnable=${FALSE}
	Set To Dictionary	${payload}	inbipRaw=${FALSE}
	Set To Dictionary	${payload}	inbSecipEnable=${FALSE}
	Set To Dictionary	${payload}	inbSecipRaw=${FALSE}
	Set To Dictionary	${payload}	inbteln=${FALSE}
	Set To Dictionary	${payload}	inbraw=${FALSE}
	API::Post::Devices::Table	${payload}

