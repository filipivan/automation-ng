*** Settings ***
Documentation	Test /api/<version>/devices/settings endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Get Devices Power Settings
	${fields}=	API::Get::Devices::Preference::Power
	Should Be Equal As Strings	${fields["cycle_label"]}	 Cycle
	Should Be Equal As Strings	${fields["cycle_option"]}	 5
	Should Be Equal As Strings	${fields["exit_label"]}	 Exit
	Should Be Equal As Strings	${fields["exit_option"]}	 1
	Should Be Equal As Strings	${fields["off_label"]}	 Off
	Should Be Equal As Strings	${fields["off_option"]}	 4
	Should Be Equal As Strings	${fields["on_label"]}	 On
	Should Be Equal As Strings	${fields["on_option"]}	 3
	Should Be Equal As Strings	${fields["status_label"]}	 Status
	Should Be Equal As Strings	${fields["status_option"]}	 2

Update Devices Power Settings
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	cycle_label=Cyclee
	Set To Dictionary	${payload}	cycle_option=1
	Set To Dictionary	${payload}	exit_label=Exitt
	Set To Dictionary	${payload}	exit_option=2
	Set To Dictionary	${payload}	off_label=Offf
	Set To Dictionary	${payload}	off_option=3
	Set To Dictionary	${payload}	on_label=Onn
	Set To Dictionary	${payload}	on_option=4
	Set To Dictionary	${payload}	status_label=Statuss
	Set To Dictionary	${payload}	status_option=5
	API::Put::Devices::Preference::Power	${payload}

Get Devices Power Settings After Update
	${fields}=	API::Get::Devices::Preference::Power
	Should Be Equal As Strings	${fields["cycle_label"]}	 Cyclee
	Should Be Equal As Strings	${fields["cycle_option"]}	 1
	Should Be Equal As Strings	${fields["exit_label"]}	 Exitt
	Should Be Equal As Strings	${fields["exit_option"]}	 2
	Should Be Equal As Strings	${fields["off_label"]}	 Offf
	Should Be Equal As Strings	${fields["off_option"]}	 3
	Should Be Equal As Strings	${fields["on_label"]}	 Onn
	Should Be Equal As Strings	${fields["on_option"]}	 4
	Should Be Equal As Strings	${fields["status_label"]}	 Statuss
	Should Be Equal As Strings	${fields["status_option"]}	 5

Get Devices Session Settings
	${fields}=	API::Get::Devices::Preference::Session
	Should Be Equal As Strings	${fields["disconnect_hotkey"]}	${EMPTY}

Update Devices Session Settings
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	disconnect_hotkey=^p
	API::Put::Devices::Preference::Session	${payload}

Get Devices Session Settings After Update
	${fields}=	API::Get::Devices::Preference::Session
	Should Be Equal As Strings	${fields["disconnect_hotkey"]}	^p

*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin
	Reset Values

Suite Teardown
	Reset Values
	API::Delete::Session

Reset Values
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	cycle_label=Cycle
	Set To Dictionary	${payload}	cycle_option=5
	Set To Dictionary	${payload}	exit_label=Exit
	Set To Dictionary	${payload}	exit_option=1
	Set To Dictionary	${payload}	off_label=Off
	Set To Dictionary	${payload}	off_option=4
	Set To Dictionary	${payload}	on_label=On
	Set To Dictionary	${payload}	on_option=3
	Set To Dictionary	${payload}	status_label=Status
	Set To Dictionary	${payload}	status_option=2
	API::Put::Devices::Preference::Power	${payload}

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	disconnect_hotkey=${EMPTY}
	API::Put::Devices::Preference::Session	${payload}

