*** Settings ***
Documentation	Test /api/<version>/devices endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Get devices
	${devices}=	API::Get::Devices::Table
	${ORIGINAL_DEVICES_COUNT}=	Get Length	${devices}
	Set Suite Variable	${ORIGINAL_DEVICES_COUNT}
	Device List Should Be Valid	${devices}

Add dummy device
	Add Dummy Device Console Device
	${devices}=	API::Get::Devices::Table
	${devices_count}=	Get Length	${devices}
	Device List Should Be Valid	${devices}
	Should Be Equal As Integers	${devices_count}	${ORIGINAL_DEVICES_COUNT+1}

Get Device Info
	${device}=	API::Get::Devices::Table::${NAME}	device
	Should Be Equal As Integers	${device["ip_alias_telnet_port"]}	23
	Should Be Equal As Integers	${device["port"]}	22
	Should Be Equal As Integers	${device["sec_ip_alias_telnet_port"]}	23
	Should Be Equal As Strings	${device["address_location"]}	antartica
	Should Be Equal As Strings	${device["break_sequence"]}	~break
	Should Be Equal As Strings	${device["coordinates"]}	${EMPTY}
	Should Be Equal As Strings	${device["credential"]}	no
	Should Be Equal As Strings	${device["duration"]}	${EMPTY}
	Should Be Equal As Strings	${device["end_point"]}	${EMPTY}
	Should Be Equal As Strings	${device["escape_sequence"]}	^Ec
	Should Be Equal As Strings	${device["expiration"]}	never
	Should Be Equal As Strings	${device["expiration_date"]}	${EMPTY}
	Should Be Equal As Strings	${device["icon"]}	linux_color.png
	Should Be Equal As Strings	${device["interface"]}	eth0
	Should Be Equal As Strings	${device["ip_address"]}	192.168.15.20
	Should Be Equal As Strings	${device["ip_alias"]}	${EMPTY}
	Should Be Equal As Strings	${device["ip_alias_binary_port"]}	${EMPTY}
	Should Be Equal As Strings	${device["mode"]}	enabled
	Should Be Equal As Strings	${device["name"]}	device
	Should Be Equal As Strings	${device["port_number"]}	${EMPTY}
	Should Be Equal As Strings	${device["port_number"]}	${EMPTY}    #there are two fields with the same label
	Should Be Equal As Strings	${device["power_control_key"]}	^O
	Should Be Equal As Strings	${device["sec_interface"]}	eth0
	Should Be Equal As Strings	${device["sec_ip_alias"]}	${EMPTY}
	Should Be Equal As Strings	${device["sec_ip_alias_binary_port"]}	${EMPTY}
	Should Be Equal As Strings	${device["ssh_port"]}	${EMPTY}
	Should Be Equal As Strings	${device["tcp_socket_port"]}	${EMPTY}
	Should Be Equal As Strings	${device["telnet_port"]}	${EMPTY}
	Should Be Equal As Strings	${device["type"]}	device_console
	Should Be Equal As Strings	${device["username"]}	yocto
	Should Be Equal As Strings	${device["web_url"]}	http://%IP
	Should Be True	${device["allow_ssh_protocol"]}
	Should Be True	${device["enable_hostname_detection"]}
	Should Be True	${device["ip_alias_telnet"]}
	Should Be True	${device["launch_url_via_html5"]}
	Should Be True	${device["multisession"]}
	Should Be True	${device["sec_ip_alias_telnet"]}
	Should Be True	${device["show_text_information"]}
	Should Not Be True	${device["allow_binary_socket"]}
	Should Not Be True	${device["allow_telnet_protocol"]}
	Should Not Be True	${device["enable_ip_alias"]}
	Should Not Be True	${device["enable_second_ip_alias"]}
	Should Not Be True	${device["enable_send_break"]}	${FALSE}
	Should Not Be True	${device["ip_alias_binary"]}
	Should Not Be True	${device["read-write_multisession"]}
	Should Not Be True	${device["sec_ip_alias_binary"]}
	Should Not Be True	${device["skip_authentication_to_access_device"]}

Update Device
	${payload}=	Get Dummy Device Payload
	Set To Dictionary	${payload}	ip_address=192.168.15.99
	API::Put::Devices::Table::${NAME}	device	${payload}

	${device}=	API::Get::Devices::Table::${NAME}	device
	${keys}=	Get Dictionary Keys	${device}
	${len}=	Get Length	${keys}
	Should Be Equal As Strings	${device["ip_address"]}	192.168.15.99
	${devices}=	API::Get::Devices::Table
	${devices_count}=	Get Length	${devices}
	Device List Should Be Valid	${devices}
	${payload}=	Get Clone Payload
	API::Put::Devices::Table::${NAME}::Clone	device	${payload}
	Wait Until Keyword Succeeds	3	30 sec	Should Have Cloned Devices

Rename device
	API::Put::Devices::Table::${NAME}::Rename	device	renamed_device
	${device}=	API::Get::Devices::Table::${NAME}	renamed_device
	Should Be Equal As Strings	${device["name"]}	renamed_device
	API::Put::Devices::Table::${NAME}::Rename	renamed_device	device
	${device}=	API::Get::Devices::Table::${NAME}	device
	Should Be Equal As Strings	${device["name"]}	device

Enable Devices
	API::Put::Devices::Table::${NAME}::Enable	device
	${device}=	API::Get::Devices::Table::${NAME}	device
	Should Be Equal As Strings	${device["mode"]}	enabled

On-demand Devices
	API::Put::Devices::Table::${NAME}::Ondemand	device
	${device}=	API::Get::Devices::Table::${NAME}	device
	Should Be Equal As Strings	${device["mode"]}	ondemand

Disable Devices
	API::Put::Devices::Table::${NAME}::Disable	device
	${device}=	API::Get::Devices::Table::${NAME}	device
	Should Be Equal As Strings	${device["mode"]}	disabled

Delete Dummy Device
	API::Delete::Devices::Table	device
	API::Delete::Devices::Table	clone1
	API::Delete::Devices::Table	clone2
	API::Delete::Devices::Table	clone3
	${devices}=	API::Get::Devices::Table
	API::Delete::Devices::Table	device
	${devices_count}=	Get Length	${devices}
	Device List Should Be Valid	${devices}
	Should Be Equal As Integers	${devices_count}	${ORIGINAL_DEVICES_COUNT}

Default serial devices
	[Timeout]	10 minutes
	${devices}=	API::Get::Devices::Table
	Device List Should Be Valid	${devices}
	:FOR	${device}	IN	@{devices}
	\	Run Keyword If	'${device["type"]}' == 'local_serial' or '${device["type"]}' == 'usb_serialB'	API::Put::Devices::Table::${NAME}::Default	${device["name"]}

Bounce DTR serial devices
	[Timeout]	10 minutes
	${devices}=	API::Get::Devices::Table
	Device List Should Be Valid	${devices}
	:FOR	${device}	IN	@{devices}
	\	Run Keyword If	'${device["type"]}' == 'local_serial' or '${device["type"]}' == 'usb_serialB'	API::Put::Devices::Table::${NAME}::Bounce DTR	${device["name"]}

Get Device Outlets
	[Teardown]	API::Delete::Devices::Table	${PDU["name"]}
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	name=${PDU["name"]}
	Set To Dictionary	${payload}	type=${PDU["type"]}
	Set To Dictionary	${payload}	ip_address=${PDU["ip"]}
	Set To Dictionary	${payload}	port=22
	Set To Dictionary	${payload}	chassis=${EMPTY}
	Set To Dictionary	${payload}	blade=${EMPTY}
	Set To Dictionary	${payload}	username=${PDU["username"]}
	Set To Dictionary	${payload}	credential=no
	Set To Dictionary	${payload}	passwordfirst=${PDU["password"]}
	Set To Dictionary	${payload}	passwordconf=${PDU["password"]}
	Set To Dictionary	${payload}	currentstatus=enabled
	API::Post::Devices::Table	${payload}
	Set Suite Variable	${dummy_outlet}	servertech:A:AA10
	Wait Until Keyword Succeeds	5 times	10 sec	PDU Should Have Outlets




*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin
	Cleanup

Suite Teardown
	Cleanup
	API::Delete::Session

Cleanup
	API::Delete::Devices::Table	device
	API::Delete::Devices::Table	servertech
	API::Delete::Devices::Table	clone1
	API::Delete::Devices::Table	clone2
	API::Delete::Devices::Table	clone3

Device Info Should Contain All Required Fields
	[Arguments]	${DEVICE}
	${keys}=	Get Dictionary Keys	${DEVICE}
	${len}=	Get Length	${keys}
	Dictionary Should Contain Key	${DEVICE}	access
	Dictionary Should Contain Key	${DEVICE}	connected_through
	Dictionary Should Contain Key	${DEVICE}	id
	Dictionary Should Contain Key	${DEVICE}	monitoring
	Dictionary Should Contain Key	${DEVICE}	name
	Dictionary Should Contain Key	${DEVICE}	type
	Should Be Equal As Integers	${len}	6

Device List Should Be Valid
	[Arguments]	${DEVICES}
	:FOR	${device}	IN	@{DEVICES}
	\	Device Info Should Contain All Required Fields	${device}

Get Dummy Device Payload
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	name=device
	Set To Dictionary	${payload}	type=device_console
	Set To Dictionary	${payload}	ip_address=192.168.15.20
	Set To Dictionary	${payload}	port=22
	Set To Dictionary	${payload}	chassis=${EMPTY}
	Set To Dictionary	${payload}	blade=${EMPTY}
	Set To Dictionary	${payload}	address_location=antartica
	Set To Dictionary	${payload}	coordinates=
	Set To Dictionary	${payload}	web_url=http://%IP
	Set To Dictionary	${payload}	launch_url_via_html5=${TRUE}
	Set To Dictionary	${payload}	username=yocto
	Set To Dictionary	${payload}	credential=no
	Set To Dictionary	${payload}	passwordfirst=yocto
	Set To Dictionary	${payload}	passwordconf=yocto
	Set To Dictionary	${payload}	vmmanager=${EMPTY}
	Set To Dictionary	${payload}	enable_hostname_detection=${TRUE}
	Set To Dictionary	${payload}	multisession=${TRUE}
	Set To Dictionary	${payload}	break_sequence=~break
	Set To Dictionary	${payload}	icon_app_options=linux_color.png
	Set To Dictionary	${payload}	icon_file_path=undefined
	Set To Dictionary	${payload}	mode=enabled
	Set To Dictionary	${payload}	currentstatus=enabled
	Set To Dictionary	${payload}	expiration=never
	Set To Dictionary	${payload}	expiration_date=${EMPTY}
	Set To Dictionary	${payload}	duration=${EMPTY}
	Set To Dictionary	${payload}	port_number=${EMPTY}
	Set To Dictionary	${payload}	port_number=${EMPTY}
	Set To Dictionary	${payload}	escape_sequence=^Ec
	Set To Dictionary	${payload}	power_control_key=^O
	Set To Dictionary	${payload}	show_text_information=${TRUE}
	Set To Dictionary	${payload}	ip_alias=${EMPTY}
	Set To Dictionary	${payload}	interface=eth0
	Set To Dictionary	${payload}	ip_alias_telnet=${TRUE}
	Set To Dictionary	${payload}	ip_alias_telnet_port=23
	Set To Dictionary	${payload}	ip_alias_binary_port=${EMPTY}
	Set To Dictionary	${payload}	sec_ip_alias=${EMPTY}
	Set To Dictionary	${payload}	sec_interface=eth0
	Set To Dictionary	${payload}	sec_ip_alias_telnet=${TRUE}
	Set To Dictionary	${payload}	sec_ip_alias_telnet_port=23
	Set To Dictionary	${payload}	sec_ip_alias_binary_port=${EMPTY}
	Set To Dictionary	${payload}	allow_ssh_protocol=${TRUE}
	Set To Dictionary	${payload}	ssh_port=${EMPTY}
	Set To Dictionary	${payload}	telnet_port=${EMPTY}
	Set To Dictionary	${payload}	tcp_socket_port=${EMPTY}
	Set To Dictionary	${payload}	read-write_multisession=${FALSE}
	Set To Dictionary	${payload}	enable_send_break=${FALSE}
	Set To Dictionary	${payload}	skip_authentication_to_access_device=${FALSE}
	Set To Dictionary	${payload}	enable_ip_alias=${FALSE}
	Set To Dictionary	${payload}	ip_alias_binary=${FALSE}
	Set To Dictionary	${payload}	enable_second_ip_alias=${FALSE}
	Set To Dictionary	${payload}	sec_ip_alias_binary=${FALSE}
	Set To Dictionary	${payload}	allow_telnet_protocol=${FALSE}
	Set To Dictionary	${payload}	allow_binary_socket=${FALSE}
	[return]	${payload}

Add Dummy Device Console Device
	${payload}=	Get Dummy Device Payload
	API::Post::Devices::Table	${payload}

Get Clone Payload
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	name=clone
	Set To Dictionary	${payload}	num_clones=3
	Set To Dictionary	${payload}	inc_addr=${TRUE}
	Set To Dictionary	${payload}	mode=ondemand
	Set To Dictionary	${payload}	ip_address=127.0.0.1
	[return]	${payload}

Should Have Cloned Devices
	${devices}=	API::Get::Devices::Table
	${devices_count}=	Get Length	${devices}
	Device List Should Be Valid	${devices}
	Should Be Equal As Integers	${devices_count}	${ORIGINAL_DEVICES_COUNT+4}

PDU Should Have Outlets
	${outlets}=	API::Get::Devices::Table::${NAME} Outlets	${PDU["name"]}
	Should Not Be Empty	${outlets}
	:FOR	${outlet}	IN	@{outlets}
	\	Dictionary Should Contain Key	${outlet}	chain_position
	\	Dictionary Should Contain Key	${outlet}	device
	\	Dictionary Should Contain Key	${outlet}	id
	\	Dictionary Should Contain Key	${outlet}	outlet_id
	\	Dictionary Should Contain Key	${outlet}	outlet_name
	\	Dictionary Should Contain Key	${outlet}	pdu_id
	\	Length Should Be	${outlet}	6
