*** Settings ***
Documentation	Test /api/<version>/devices/<name>/customfields endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Variables ***
${DEVICE}=	device
${FIELD_NAME}=	${EMPTY}

*** Test Cases ***

Get Device Custom Fields
	${fields}=	API::Get::Devices::Table::${NAME}::Custom Field::${NAME2}s	${DEVICE}
	Check Custom Fields Fields	${fields}

Add Device Custom Field
	${payload}=	Create Dictionary
	${field_name}=	Generate Random String
	Set Suite Variable	${FIELD_NAME}	${field_name}
	${field_value}=	Generate Random String
	Set Suite Variable	${FIELD_VALUE}	${field_value}
	Set To Dictionary	${payload}	field_name=${FIELD_NAME}
	Set To Dictionary	${payload}	field_value=${field_value}
	API::Post::Devices::Table::${NAME}::Custom Field	${DEVICE}	${payload}

	Wait until ${DEVICE} has ${ORIGINAL_FIELDS_COUNT+1} custom fields
	#${fields}=	API::Get::Devices::Table::${NAME}ORIGINAL_FIELDS_COUNT+1 Custom Fields	${DEVICE}
	#Length Should Be	${fields}	ORIGINAL_FIELDS_COUNT+1${ORIGINAL_FIELDS_COUNT+1}

Get Device Custom Field
	${info}=	API::Get::Devices::Table::${NAME}::Custom Field::${NAME2}	${DEVICE}	${FIELD_NAME}
	Length Should Be	${info}	1
	Should Be Equal As Strings	${info["field_value"]}	${FIELD_VALUE}

Update Device Custom Field
	${payload}=	Create Dictionary
	${field_value}=	Generate Random String
	Set Suite Variable	${FIELD_VALUE}	${field_value}
	Set To Dictionary	${payload}	field_value=${field_value}
	API::Put::Devices::Table::${NAME}::Custom Field::${NAME2}	${DEVICE}	${FIELD_NAME}	${payload}

Get Device Custom Field After Update
	${info}=	API::Get::Devices::Table::${NAME}::Custom Field::${NAME2}	${DEVICE}	${FIELD_NAME}
	Length Should Be	${info}	1
	Should Be Equal As Strings	${info["field_value"]}	${FIELD_VALUE}

Delete Custom Field
	API::Delete::Devices::Table::${NAME}::Custom Fields	${DEVICE}	${FIELD_NAME}

	${fields}=	API::Get::Devices::Table::${NAME}::Custom Field::${NAME2}s	${DEVICE}
	Check Custom Fields Fields	${fields}
	Length Should Be	${fields}	${ORIGINAL_FIELDS_COUNT}

*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin
	API::Delete::Devices::Table	${DEVICE}
	Add Dummy Device Console Device

	${fields}=	API::Get::Devices::Table::${NAME}::Custom Field::${NAME2}s	${DEVICE}
	${length}=	Get Length	${fields}
	Set Suite Variable	${ORIGINAL_FIELDS_COUNT}	${length}

Suite Teardown
	#API::Delete::Devices::Table	${DEVICE}
	API::Delete::Session

Add Dummy Device Console Device
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	spm_name=${DEVICE}
	Set To Dictionary	${payload}	type=device_console
	Set To Dictionary	${payload}	phys_addr=${HOST}
	Set To Dictionary	${payload}	tcpport=22
	Set To Dictionary	${payload}	chassis=${EMPTY}
	Set To Dictionary	${payload}	blade=${EMPTY}
	Set To Dictionary	${payload}	addr_location=antartica
	Set To Dictionary	${payload}	coordinates=
	Set To Dictionary	${payload}	url=http://%IP
	Set To Dictionary	${payload}	webhtml5=${TRUE}
	Set To Dictionary	${payload}	username=admin
	Set To Dictionary	${payload}	askpassword=no
	Set To Dictionary	${payload}	passwordfirst=admin
	Set To Dictionary	${payload}	passwordconf=admin
	Set To Dictionary	${payload}	vmmanager=${EMPTY}
	Set To Dictionary	${payload}	discname=${TRUE}
	Set To Dictionary	${payload}	multisession=${TRUE}
	Set To Dictionary	${payload}	break-sequence=~break
	Set To Dictionary	${payload}	icon_app_options=linux_color.png
	Set To Dictionary	${payload}	icon_file_path=undefined
	Set To Dictionary	${payload}	status=enabled
	Set To Dictionary	${payload}	currentstatus=enabled
	Set To Dictionary	${payload}	ttlType=never
	Set To Dictionary	${payload}	ttlDate=${EMPTY}
	Set To Dictionary	${payload}	ttlDays=${EMPTY}
	Set To Dictionary	${payload}	childidserial=${EMPTY}
	Set To Dictionary	${payload}	childidkvm=${EMPTY}
	Set To Dictionary	${payload}	inbhotkey=^Ec
	Set To Dictionary	${payload}	inbpowerctlkey=^O
	Set To Dictionary	${payload}	inbshowinfo=${TRUE}
	Set To Dictionary	${payload}	inbipAddr=${EMPTY}
	Set To Dictionary	${payload}	inbInterf=eth0
	Set To Dictionary	${payload}	inbipTeln=${TRUE}
	Set To Dictionary	${payload}	inbipTelnPort=23
	Set To Dictionary	${payload}	inbipRawPort=${EMPTY}
	Set To Dictionary	${payload}	inbSecipAddr=${EMPTY}
	Set To Dictionary	${payload}	inbSecInterf=eth0
	Set To Dictionary	${payload}	inbSecipTeln=${TRUE}
	Set To Dictionary	${payload}	inbSecipTelnPort=23
	Set To Dictionary	${payload}	inbSecipRawPort=${EMPTY}
	Set To Dictionary	${payload}	inbssh=${TRUE}
	Set To Dictionary	${payload}	inbsshTCP=${EMPTY}
	Set To Dictionary	${payload}	inbtelnTCP=${EMPTY}
	Set To Dictionary	${payload}	inbrawTCP=${EMPTY}
	Set To Dictionary	${payload}	multisessRW=${FALSE}
	Set To Dictionary	${payload}	enable-break=${FALSE}
	Set To Dictionary	${payload}	inbauth=${FALSE}
	Set To Dictionary	${payload}	inbipEnable=${FALSE}
	Set To Dictionary	${payload}	inbipRaw=${FALSE}
	Set To Dictionary	${payload}	inbSecipEnable=${FALSE}
	Set To Dictionary	${payload}	inbSecipRaw=${FALSE}
	Set To Dictionary	${payload}	inbteln=${FALSE}
	Set To Dictionary	${payload}	inbraw=${FALSE}
	API::Post::Devices::Table	${payload}

Check Custom Fields Fields
	[Arguments]	${FIELDS}
	:FOR	${field}	IN	@{FIELDS}
	\	Dictionary Should Contain Key	${field}	field_name
	\	Dictionary Should Contain Key	${field}	field_value

Wait until ${DEVICE} has ${COUNT} custom fields
	Wait Until Keyword Succeeds	3 times	10 seconds	Device ${DEVICE} should has ${COUNT} custom fields count

Device ${DEVICE} should has ${COUNT} custom fields count
	${fields}=	API::Get::Devices::Table::${NAME}::Custom Field::${NAME2}s	${DEVICE}
	Check Custom Fields Fields	${fields}
	Length Should Be	${fields}	${COUNT}
