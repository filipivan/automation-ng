*** Settings ***
Documentation	Test /api/<version>/devices/discovery/logs endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Setup Test Suite
Suite Teardown	Teardown Test Suite

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Get Auto Discovery Discovery Logs
	Wait Until Keyword Succeeds	3x	30 seconds	Has Discovery Logs

Reset Discovery Logs
	API::Post::Devices::Discovery::Logs::Reset Logs
	${logs}=	API::Get::Devices::Discovery::Logs
	Length Should Be	${logs}	0

*** Keywords ***

Setup Test Suite
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	netdiscover_id=test
	Set To Dictionary	${payload}	ipfirst=127.0.0.1
	Set To Dictionary	${payload}	iplast=127.0.0.10
	Set To Dictionary	${payload}	enabled=${TRUE}
	Set To Dictionary	${payload}	seed=ttyS1
	Set To Dictionary	${payload}	portscan=${TRUE}
	Set To Dictionary	${payload}	portlist=22-23,623
	Set To Dictionary	${payload}	ping=${TRUE}
	Set To Dictionary	${payload}	interval=60
	Set To Dictionary	${payload}	morelikethis=${FALSE}
	API::Post::Devices::Discovery::Network	${payload}

Teardown Test Suite
	API::Delete::Devices::Discovery::Network	test

Has Discovery Logs
	${logs}=	API::Get::Devices::Discovery::Logs
	Should Not Be Empty	${logs}
