*** Settings ***
Documentation	Test /api/<version>/devices/ endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Setup Test Suite
Suite Teardown	Teardown Test Suite

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Variables ***

*** Test Cases ***

Add Auto Discovery Network Scan
	${scans}=	API::Get::Devices::Discovery::Network
	${count}=	Get Length	${scans}
	Set Suite Variable	${ORIGINAL_COUNT}	${count}

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	scan_id=test
	Set To Dictionary	${payload}	ip_range_start=127.0.0.1
	Set To Dictionary	${payload}	ip_range_end=127.0.0.10
	Set To Dictionary	${payload}	enable_scanning=${TRUE}
	Set To Dictionary	${payload}	device=ttyS1
	Set To Dictionary	${payload}	port_scan=${TRUE}
	Set To Dictionary	${payload}	port_list=22-23,623
	Set To Dictionary	${payload}	ping=${TRUE}
	Set To Dictionary	${payload}	scan_interval=60
	Set To Dictionary	${payload}	similar_devices=${FALSE}
	API::Post::Devices::Discovery::Network	${payload}

	${scans}=	API::Get::Devices::Discovery::Network
	Length Should Be	${scans}	${ORIGINAL_COUNT+1}

Get Auto Discovery Discovery Now
	${discoveries}=	API::Get::Devices::Discovery::Now
	:FOR	${discovery}	IN	@{discoveries}
	\	Dictionary Should Contain Key	${discovery}	id
	\	Dictionary Should Contain Key	${discovery}	name
	\	Dictionary Should Contain Key	${discovery}	type
	\	Length Should Be	${discovery}	3


Get Auto Discovery Network Scan
	${scans}=	API::Get::Devices::Discovery::Network
	:FOR	${scan}	IN	@{scans}
	\	Length Should Be	${scan}	8
	\	Dictionary Should Contain Key	${scan}	id
	\	Dictionary Should Contain Key	${scan}	interval
	\	Dictionary Should Contain Key	${scan}	similar_devices
	\	Dictionary Should Contain Key	${scan}	scan_id
	\	Dictionary Should Contain Key	${scan}	ping
	\	Dictionary Should Contain Key	${scan}	port_scan
	\	Dictionary Should Contain Key	${scan}	ip_range
	\	Dictionary Should Contain Key	${scan}	status

Get Network Scan
	${scan}=	API::Get::Devices::Discovery::Network::${NAME}	test
	Should Be Equal As Strings	${scan["scan_id"]}	test
	Should Be Equal As Strings	${scan["ip_range_start"]}	127.0.0.1
	Should Be Equal As Strings	${scan["ip_range_end"]}	127.0.0.10
	Should Be Equal As Strings	${scan["device"]}	${EMPTY}
	Should Be Equal As Strings	${scan["port_list"]}	22-23,623
	Should Be Equal As Strings	${scan["scan_interval"]}	60
	Should Not Be True	${scan["similar_devices"]}
	Should Be True	${scan["enable_scanning"]}
	Should Be True	${scan["port_scan"]}
	Should Be True	${scan["ping"]}

Update Auto Discovery Network Scan
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	scan_id=test
	Set To Dictionary	${payload}	ip_range_start=127.0.0.1
	Set To Dictionary	${payload}	device=ttyS1
	Set To Dictionary	${payload}	port_scan=${TRUE}
	Set To Dictionary	${payload}	port_list=22-23,623
	Set To Dictionary	${payload}	ping=${TRUE}
	Set To Dictionary	${payload}	scan_interval=60
	Set To Dictionary	${payload}	similar_devices=${FALSE}
	Set To Dictionary	${payload}	ip_range_end=127.0.0.11
	Set To Dictionary	${payload}	enable_scanning=${TRUE}
	API::Put::Devices::Discovery::Network::${NAME}	test	${payload}

Get Network Scan After Update
	${scan}=	API::Get::Devices::Discovery::Network::${NAME}	test
	Should Be Equal As Strings	${scan["scan_id"]}	test
	Should Be Equal As Strings	${scan["ip_range_start"]}	127.0.0.1
	Should Be Equal As Strings	${scan["ip_range_end"]}	127.0.0.11
	Should Be Equal As Strings	${scan["device"]}	${EMPTY}
	Should Be Equal As Strings	${scan["port_list"]}	22-23,623
	Should Be Equal As Strings	${scan["scan_interval"]}	60
	Should Not Be True	${scan["similar_devices"]}
	Should Be True	${scan["enable_scanning"]}
	Should Be True	${scan["port_scan"]}
	Should Be True	${scan["ping"]}

Delete Auto Discovery Network Scan
	# lldel can take some time sometimes ;)
	Wait Until Keyword Succeeds	3 times	10 seconds	Delete network scan

Add Auto Discovery VM Manager
	${managers}=	API::Get::Devices::Discovery::VM Managers
	${count}=	Get Length	${managers}
	Set Suite Variable	${ORIGINAL_COUNT}	${count}

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	vm_server=${HOST}
	Set To Dictionary	${payload}	username=username
	Set To Dictionary	${payload}	password=pass
	Set To Dictionary	${payload}	confirm_password=pass
	Set To Dictionary	${payload}	type=VMware
	Set To Dictionary	${payload}	html_console_port=7331,7343
	API::Post::Devices::Discovery::VM Managers	${payload}

	${managers}=	API::Get::Devices::Discovery::VM Managers
	Length Should Be	${managers}	${ORIGINAL_COUNT+1}

Get Auto Discovery VM Managers
	${managers}=	API::Get::Devices::Discovery::VM Managers
	:FOR	${manager}	IN	@{managers}
	\	Dictionary Should Contain Key	${manager}	id
	\	Dictionary Should Contain Key	${manager}	interval_in_minutes
	\	Dictionary Should Contain Key	${manager}	type
	\	Dictionary Should Contain Key	${manager}	discover_virtual_machines
	\	Dictionary Should Contain Key	${manager}	vm_server
	\	Length Should Be	${manager}	5

Get VM Manager
	${manager}=	API::Get::Devices::Discovery::VM Managers::${NAME}	${HOST}
	Should Be Equal As Strings	${manager["vm_server"]}	${HOST}
	Should Be Equal As Strings	${manager["username"]}	username
	Should Be Equal As Strings	${manager["type"]}	VMware
	Should Be Equal As Strings	${manager["html_console_port"]}	7331,7343

Update Auto Discovery VM Manager
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	vm_server=${HOST}
	Set To Dictionary	${payload}	username=username
	Set To Dictionary	${payload}	password=pass
	Set To Dictionary	${payload}	confirm_password=pass
	Set To Dictionary	${payload}	type=VMware
	Set To Dictionary	${payload}	html_console_port=7331,7345
	API::Put::Devices::Discovery::VM Managers::${NAME}	${HOST}	${payload}

Get VM Manager After Update
	${manager}=	API::Get::Devices::Discovery::VM Managers::${NAME}	${HOST}
	Should Be Equal As Strings	${manager["vm_server"]}	${HOST}
	Should Be Equal As Strings	${manager["username"]}	username
	Should Be Equal As Strings	${manager["type"]}	VMware
	Should Be Equal As Strings	${manager["html_console_port"]}	7331,7345

Delete Auto Discovery VM Manager
	API::Delete::Devices::Discovery::VM Managers	${HOST}
	${managers}=	API::Get::Devices::Discovery::VM Managers
	Length Should Be	${managers}	${ORIGINAL_COUNT}

Add Auto Discovery Rule
	${rules}=	API::Get::Devices::Discovery::Rules
	${count}=	Get Length	${rules}
	Set Suite Variable	${ORIGINAL_COUNT}	${count}

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	discovery_name=rule
	Set To Dictionary	${payload}	status=disabled
	Set To Dictionary	${payload}	source=dhcp
	Set To Dictionary	${payload}	macaddr=00:00:00:00:00:00
	Set To Dictionary	${payload}	porturi=${EMPTY}
	Set To Dictionary	${payload}	datacenter=${EMPTY}
	Set To Dictionary	${payload}	cluster=${EMPTY}
	Set To Dictionary	${payload}	portlistcas=${EMPTY}
	Set To Dictionary	${payload}	portlistkvm=${EMPTY}
	Set To Dictionary	${payload}	netdiscover=${EMPTY}
	Set To Dictionary	${payload}	hostname=${EMPTY}
	Set To Dictionary	${payload}	operation=discard
	Set To Dictionary	${payload}	device=ttyS1
	API::Post::Devices::Discovery::Rules	${payload}
	Wait until has ${ORIGINAL_COUNT+1} discovery rules

	Add Network Scan
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	discovery_name=rule2
	Set To Dictionary	${payload}	status=disabled
	Set To Dictionary	${payload}	source=network
	Set To Dictionary	${payload}	porturi=${EMPTY}
	Set To Dictionary	${payload}	datacenter=${EMPTY}
	Set To Dictionary	${payload}	cluster=${EMPTY}
	Set To Dictionary	${payload}	portlistcas=${EMPTY}
	Set To Dictionary	${payload}	portlistkvm=${EMPTY}
	Set To Dictionary	${payload}	netdiscover=test
	Set To Dictionary	${payload}	hostname=${EMPTY}
	Set To Dictionary	${payload}	operation=discard
	Set To Dictionary	${payload}	device=ttyS1
	API::Post::Devices::Discovery::Rules	${payload}
	Wait until has ${ORIGINAL_COUNT+2} discovery rules

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	discovery_name=rule3
	Set To Dictionary	${payload}	status=disabled
	Set To Dictionary	${payload}	source=network
	Set To Dictionary	${payload}	porturi=${EMPTY}
	Set To Dictionary	${payload}	datacenter=${EMPTY}
	Set To Dictionary	${payload}	cluster=${EMPTY}
	Set To Dictionary	${payload}	portlistcas=${EMPTY}
	Set To Dictionary	${payload}	portlistkvm=${EMPTY}
	Set To Dictionary	${payload}	netdiscover=test
	Set To Dictionary	${payload}	hostname=${EMPTY}
	Set To Dictionary	${payload}	operation=discard
	Set To Dictionary	${payload}	device=ttyS1
	API::Post::Devices::Discovery::Rules	${payload}
	Wait until has ${ORIGINAL_COUNT+3} discovery rules

Get Auto Discovery Discovery Rules
	${rules}=	API::Get::Devices::Discovery::Rules
	:FOR	${rule}	IN	@{rules}
	\	Dictionary Should Contain Key	${rule}	id
	\	Dictionary Should Contain Key	${rule}	action
	\	Dictionary Should Contain Key	${rule}	clone_from
	\	Dictionary Should Contain Key	${rule}	host_identifier
	\	Dictionary Should Contain Key	${rule}	lookup_pattern
	\	Dictionary Should Contain Key	${rule}	method
	\	Dictionary Should Contain Key	${rule}	rule_name
	\	Dictionary Should Contain Key	${rule}	status
	\	Length Should Be	${rule}	9

Move Discovery Rule Up
	${rules}=	API::Get::Devices::Discovery::Rules
	Should Be Equal As Strings	${rules[0]["id"]}	rule
	Should Be Equal As Strings	${rules[1]["id"]}	rule2
	Should Be Equal As Strings	${rules[2]["id"]}	rule3
	API::Put::Devices::Discovery::Rules::${NAME}::Up	rule3
	${rules}=	API::Get::Devices::Discovery::Rules
	Should Be Equal As Strings	${rules[0]["id"]}	rule
	Should Be Equal As Strings	${rules[1]["id"]}	rule3
	Should Be Equal As Strings	${rules[2]["id"]}	rule2

Move Discovery Rule Down
	${rules}=	API::Get::Devices::Discovery::Rules
	Should Be Equal As Strings	${rules[0]["id"]}	rule
	Should Be Equal As Strings	${rules[1]["id"]}	rule3
	Should Be Equal As Strings	${rules[2]["id"]}	rule2
	API::Put::Devices::Discovery::Rules::${NAME}::Down	rule3
	${rules}=	API::Get::Devices::Discovery::Rules
	Should Be Equal As Strings	${rules[0]["id"]}	rule
	Should Be Equal As Strings	${rules[1]["id"]}	rule2
	Should Be Equal As Strings	${rules[2]["id"]}	rule3

Get Discovery Rule
	${rule}=	API::Get::Devices::Discovery::Rules::${NAME}	rule
	Should Be Equal As Strings	${rule["rule_name"]}	rule
	Should Be Equal As Strings	${rule["status"]}	disabled
	Should Be Equal As Strings	${rule["method"]}	dhcp
	Should Be Equal As Strings	${rule["mac_address"]}	00:00:00:00:00:00
	Should Be Equal As Strings	${rule["port_uri"]}	${EMPTY}
	Should Be Equal As Strings	${rule["datacenter"]}	${EMPTY}
	Should Be Equal As Strings	${rule["cluster"]}	${EMPTY}
	Should Be Equal As Strings	${rule["port_list"]}	${EMPTY}
	Should Be Equal As Strings	${rule["port_list"]}	${EMPTY}
	Should Be Equal As Strings	${rule["scan_id"]}	${EMPTY}
	Should Be Equal As Strings	${rule["host_identifier"]}	${EMPTY}
	Should Be Equal As Strings	${rule["action"]}	discard
	Should Be Equal As Strings	${rule["clone_from"]}	${EMPTY}

Update Auto Discovery Rule
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	rule_name=rule
	Set To Dictionary	${payload}	status=enabled
	Set To Dictionary	${payload}	method=dhcp
	Set To Dictionary	${payload}	mac_address=${EMPTY}
	Set To Dictionary	${payload}	port_uri=${EMPTY}
	Set To Dictionary	${payload}	datacenter=${EMPTY}
	Set To Dictionary	${payload}	cluster=${EMPTY}
	Set To Dictionary	${payload}	port_list=${EMPTY}
	Set To Dictionary	${payload}	port_list=${EMPTY}
	Set To Dictionary	${payload}	scan_id=${EMPTY}
	Set To Dictionary	${payload}	host_identifier=${EMPTY}
	Set To Dictionary	${payload}	action=discard
	Set To Dictionary	${payload}	clone_from=ttyS1
	API::Update::Devices::Discovery::Rules::${NAME}	rule	${payload}

Get Discovery Rule After Update
	${rule}=	API::Get::Devices::Discovery::Rules::${NAME}	rule
	Should Be Equal As Strings	${rule["rule_name"]}	rule
	Should Be Equal As Strings	${rule["status"]}	enabled
	Should Be Equal As Strings	${rule["method"]}	dhcp
	Should Be Equal As Strings	${rule["mac_address"]}	${EMPTY}
	Should Be Equal As Strings	${rule["port_uri"]}	${EMPTY}
	Should Be Equal As Strings	${rule["datacenter"]}	${EMPTY}
	Should Be Equal As Strings	${rule["cluster"]}	${EMPTY}
	Should Be Equal As Strings	${rule["port_list"]}	${EMPTY}
	Should Be Equal As Strings	${rule["port_list"]}	${EMPTY}
	Should Be Equal As Strings	${rule["scan_id"]}	${EMPTY}
	Should Be Equal As Strings	${rule["host_identifier"]}	${EMPTY}
	Should Be Equal As Strings	${rule["action"]}	discard
	Should Be Equal As Strings	${rule["clone_from"]}	${EMPTY}

Delete Auto Discovery Rule
	API::Delete::Devices::Discovery::Rules	rule
	Wait until has ${ORIGINAL_COUNT+2} discovery rules
	API::Delete::Devices::Discovery::Rules	rule2
	Wait until has ${ORIGINAL_COUNT+1} discovery rules
	API::Delete::Devices::Discovery::Rules	rule3
	Wait until has ${ORIGINAL_COUNT} discovery rules

Add Hostname Detection Rules
	${rules}=	API::Get::Devices::Discovery::Hostname
	${count}=	Get Length	${rules}
	Set Suite Variable	${ORIGINAL_COUNT}	${count}

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	string_type=match
	Set To Dictionary	${payload}	string=blah
	API::Post::Devices::Discovery::Hostname	${payload}

	Wait until has ${ORIGINAL_COUNT+1} hostname detection rules
	Set Suite Variable	${MATCH_COUNT}	${MATCH_COUNT + 1}

Get Auto Discovery Hostname Detection
	${hosts}=	API::Get::Devices::Discovery::Hostname
	:FOR	${host}	IN	@{hosts}
	\	Length Should Be	${host}	4
	\	Dictionary Should Contain Key	${host}	id
	\	Dictionary Should Contain Key	${host}	index
	\	Dictionary Should Contain Key	${host}	string
	\	Dictionary Should Contain Key	${host}	string_type

Get Hostname Detection
	${hostname}=	API::Get::Devices::Discovery::Hostname::${NAME}	match.${MATCH_COUNT}
	Should Be Equal As Strings	${hostname["string_type"]}	Match
	Should Be Equal As Strings	${hostname["string"]}	blah

Update Hostname Detection Rules
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	string_type=match
	Set To Dictionary	${payload}	string=bloh
	API::Put::Devices::Discovery::Hostname::${NAME}	match.6	${payload}

Get Hostname Detection After Update
	${hostname}=	API::Get::Devices::Discovery::Hostname::${NAME}	match.6
	Should Be Equal As Strings	${hostname["string_type"]}	Match
	Should Be Equal As Strings	${hostname["string"]}	bloh

Delete Hostname Detection Rules
	API::Delete::Devices::Discovery::Hostname	match.6
	Wait until has ${ORIGINAL_COUNT} hostname detection rules

Get Hostname Global Settings
	${settings}=	API::Get::Devices::Discovery::Hostname Global Settings
	Dictionary Should Contain Key	${settings}	new_discovered_device_receives_the_name_during_conflict
	Dictionary Should Contain Key	${settings}	number_of_retries
	Dictionary Should Contain Key	${settings}	probe_timeout
	Dictionary Should Contain Key	${settings}	update_device_name
	Length Should Be	${settings}	4

Update Hostname Global Settings
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	new_discovered_device_receives_the_name_during_conflict=${FALSE}
	Set To Dictionary	${payload}	update_device_name=${TRUE}
	Set To Dictionary	${payload}	number_of_retries=11
	Set To Dictionary	${payload}	probe_timeout=10

	API::Put::Devices::Discovery::Hostname Global Settings	${payload}

Get Hostname Global Settings After Update
	${settings}=	API::Get::Devices::Discovery::Hostname Global Settings
	Should Not Be True	${settings["new_discovered_device_receives_the_name_during_conflict"]}
	Should Be True	${settings["update_device_name"]}
	Should Be Equal As Strings	${settings["number_of_retries"]}	11
	Should Be Equal As Strings	${settings["probe_timeout"]}	10
	Length Should Be	${settings}	4

Get Auto Discovery Discovery Logs
	${logs}=	API::Get::Devices::Discovery::Logs
	:FOR	${log}	IN	@{logs}
	\	Dictionary Should Contain Key	${log}	action
	\	Dictionary Should Contain Key	${log}	date
	\	Dictionary Should Contain Key	${log}	device_name
	\	Dictionary Should Contain Key	${log}	discovery_method
	\	Dictionary Should Contain Key	${log}	id
	\	Dictionary Should Contain Key	${log}	ip_address
	\	Length Should Be	${log}	6

*** Keywords ***

Setup Test Suite
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin
	Reset Values
	Count Match Type

Teardown Test Suite
	Reset Values
	API::Delete::Session

Reset Values
	API::Delete::Devices::Discovery::Network	test
	API::Delete::Devices::Discovery::Hostname	match.6
	API::Delete::Devices::Discovery::VM Managers	${HOST}
	API::Delete::Devices::Discovery::Rules	rule
	API::Delete::Devices::Types	clone

Count Match Type
	${rules}=	API::Get::Devices::Discovery::Hostname
	${types}=	Create List
	:FOR	${rule}	IN	@{rules}
	\	Append To List	${types}	${rule["string_type"]}
	${count}=	Count Values In List	${types}	match
	Set Suite Variable	${MATCH_COUNT}	${count}

Add Network Scan
	${scans}=	API::Get::Devices::Discovery::Network
	${count}=	Get Length	${scans}
	Set Suite Variable	${ORIGINAL_COUNT}	${count}

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	scan_id=test
	Set To Dictionary	${payload}	ip_range_start=127.0.0.1
	Set To Dictionary	${payload}	ip_range_end=127.0.0.10
	Set To Dictionary	${payload}	enable_scanning=${TRUE}
	Set To Dictionary	${payload}	device=ttyS1
	Set To Dictionary	${payload}	port_scan=${TRUE}
	Set To Dictionary	${payload}	port_list=22-23,623
	Set To Dictionary	${payload}	ping=${TRUE}
	Set To Dictionary	${payload}	scan_interval=60
	Set To Dictionary	${payload}	similar_devices=${FALSE}
	API::Post::Devices::Discovery::Network	${payload}

	${scans}=	API::Get::Devices::Discovery::Network
	Length Should Be	${scans}	${ORIGINAL_COUNT+1}

Delete network scan
	API::Delete::Devices::Discovery::Network	test
	${scans}=	API::Get::Devices::Discovery::Network
	Length Should Be	${scans}	${ORIGINAL_COUNT}

Wait until has ${COUNT} discovery rules
	Wait Until Keyword Succeeds	3 times	10 seconds	Discovery rules should be ${COUNT}

Discovery rules should be ${COUNT}
	${rules}	API::Get::Devices::Discovery::Rules
	Length Should Be	${rules}	${COUNT}

Wait until has ${COUNT} hostname detection rules
	Wait Until Keyword Succeeds	3 times	10 seconds	Hostname detection rules should be ${COUNT}

Hostname detection rules should be ${COUNT}
	${rules}=	API::Get::Devices::Discovery::Hostname
	Length Should Be	${rules}	${COUNT}
