*** Settings ***
Documentation	Test /api/<version>/auditing/destination endpoint
Metadata	Version	1.0
Metadata	Executed At		${HOST}
Resource	../init.robot
Force Tags	API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

Suite Setup	SUITE:Setup
Suite Teardown	Suite Teardown

*** Test Cases ***
Get auditing destination file
	${fields}=	API::Get::Auditing::Destination::File
	Should Be Equal As Strings	${fields["destination"]}	local
	Should Be Equal As Strings	${fields["archive_by_time"]}	${EMPTY}
	Should Be Equal As Strings	${fields["number_of_archives"]}	1
	Should Be Equal As Strings	${fields["file_size"]}	256
	Should Be Equal As Strings	${fields["nfs_archive_by_time"]}	${EMPTY}
	Should Be Equal As Strings	${fields["nfs_path"]}	${EMPTY}
	Should Be Equal As Strings	${fields["number_of_archives_in_nfs"]}	10
	Should Be Equal As Strings	${fields["nfs_server"]}	${EMPTY}
	Should Be Equal As Strings	${fields["nfs_file_size"]}	1024

Get auditing destination email
	${fields}=	API::Get::Auditing::Destination::Email
	Should Be Equal As Strings	${fields["destination_email"]}	${EMPTY}
	Should Be Equal As Strings	${fields["password"]}	********
	Should Be Equal As Strings	${fields["confirm_password"]}	********
	Should Be Equal As Strings	${fields["email_port"]}	25
	Should Be Equal As Strings	${fields["email_server"]}	${EMPTY}
	Should Be Equal As Strings	${fields["username"]}	${EMPTY}
	Should Be True	${fields["start_tls"]}

Get auditing destination SNMP Trap
	${fields}=	API::Get::Auditing::Destination::SNMP Trap
	Should Be Equal As Strings	${fields["snmptrap_authentication_password"]}	********
	Should Be Equal As Strings	${fields["snmptrap_authentication"]}	SHA
	Should Be Equal As Strings	${fields["snmptrap_community"]}	public
	Should Contain	${fields["snmp_engine_id"]}	0x8000a6160
	Should Be Equal As Strings	${fields["snmptrap_port"]}	161
	Should Be Equal As Strings	${fields["snmptrap_privacy_passphrase"]}	********
	Should Be Equal As Strings	${fields["snmptrap_privacy_algo"]}	AES
	Should Be Equal As Strings	${fields["snmptrap_transport_protocol"]}	udp
	Should Be Equal As Strings	${fields["snmptrap_user"]}	secname
	${fields["snmptrap_server"]}=	Strip String	${fields["snmptrap_server"]}
	Should Be Equal As Strings	${fields["snmptrap_server"]}	127.0.0.1
	Should Be Equal As Strings	${fields["snmptrap_version"]}	2c

Get auditing destination Syslog
	${fields}=	API::Get::Auditing::Destination::Syslog
	Should Be Equal As Strings	${fields["datalog_facility"]}	LOG_LOCAL0
	Should Be Equal As Strings	${fields["event_facility"]}	LOG_LOCAL0
	Should Be Equal As Strings	${fields["ipv4_address"]}	${EMPTY}
	Should Be Equal As Strings	${fields["ipv6_address"]}	${EMPTY}
	Should Be True	${fields["system_console"]}
	Should Not Be True	${fields["ipv4_remote_server"]}
	Should Not Be True	${fields["ipv6_remote_server"]}
	Should Not Be True	${fields["admin_session"]}

Update auditing destination file
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	destination=nfs
	Set To Dictionary	${payload}	nfs_server=127.0.0.1
	Set To Dictionary	${payload}	nfs_path=/test
	Set To Dictionary	${payload}	archive_by_time=${EMPTY}
	Set To Dictionary	${payload}	number_of_archives=1
	Set To Dictionary	${payload}	file_size=256
	Set To Dictionary	${payload}	nfs_archive_by_time=${EMPTY}
	Set To Dictionary	${payload}	number_of_archives_in_nfs=10
	Set To Dictionary	${payload}	nfs_file_size=1024
	API::Put::Auditing::Destination::File	${payload}

Update auditing destination email
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	email_server=smtp.blah.com
	Set To Dictionary	${payload}	destination_email=dummy@dummy.com
	Set To Dictionary	${payload}	password=pass
	Set To Dictionary	${payload}	confirm_password=pass
	Set To Dictionary	${payload}	username=username
	API::Put::Auditing::Destination::Email	${payload}

Update auditing destination SNMP Trap
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	snmptrap_transport_protocol=tcpipv6
	Set To Dictionary	${payload}	snmptrap_server=127.0.0.2
	API::Put::Auditing::Destination::SNMP Trap	${payload}

Update auditing destination Syslog
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	ipv4_address=localhost
	Set To Dictionary	${payload}	ipv6_address=::
	Set To Dictionary	${payload}	system_console=${TRUE}
	Set To Dictionary	${payload}	ipv4_remote_server=${TRUE}
	Set To Dictionary	${payload}	ipv6_remote_server=${TRUE}
	Set To Dictionary	${payload}	admin_session=${TRUE}
	API::Put::Auditing::Destination::Syslog	${payload}

Get auditing destination file after update
	${fields}=	API::Get::Auditing::Destination::File
	Should Be Equal As Strings	${fields["destination"]}	nfs
	Should Be Equal As Strings	${fields["archive_by_time"]}	${EMPTY}
	Should Be Equal As Strings	${fields["number_of_archives"]}	1
	Should Be Equal As Strings	${fields["file_size"]}	256
	Should Be Equal As Strings	${fields["nfs_archive_by_time"]}	${EMPTY}
	Should Be Equal As Strings	${fields["nfs_path"]}	/test
	Should Be Equal As Strings	${fields["number_of_archives_in_nfs"]}	10
	Should Be Equal As Strings	${fields["nfs_server"]}	127.0.0.1
	Should Be Equal As Strings	${fields["nfs_file_size"]}	1024

Get auditing destination email after update
	${fields}=	API::Get::Auditing::Destination::Email
	Should Be Equal As Strings	${fields["destination_email"]}	dummy@dummy.com
	Should Be Equal As Strings	${fields["password"]}	********
	Should Be Equal As Strings	${fields["confirm_password"]}	********
	Should Be Equal As Strings	${fields["email_server"]}	smtp.blah.com
	Should Be Equal As Strings	${fields["username"]}	username
	Should Be True	${fields["start_tls"]}

Get auditing destination SNMP Trap after update
	${fields}=	API::Get::Auditing::Destination::SNMP Trap
	Should Be Equal As Strings	${fields["snmptrap_authentication_password"]}	********
	Should Be Equal As Strings	${fields["snmptrap_authentication"]}	SHA
	Should Be Equal As Strings	${fields["snmptrap_community"]}	public
	Should Contain	${fields["snmp_engine_id"]}	0x8000a6160
	Should Be Equal As Strings	${fields["snmptrap_port"]}	161
	Should Be Equal As Strings	${fields["snmptrap_privacy_passphrase"]}	********
	Should Be Equal As Strings	${fields["snmptrap_privacy_algo"]}	AES
	Should Be Equal As Strings	${fields["snmptrap_transport_protocol"]}	tcpipv6
	Should Be Equal As Strings	${fields["snmptrap_user"]}	secname
	Should Be Equal As Strings	${fields["snmptrap_server"]}	127.0.0.2
	Should Be Equal As Strings	${fields["snmptrap_version"]}	2c

Get auditing destination Syslog after update
	${fields}=	API::Get::Auditing::Destination::Syslog
	Should Be Equal As Strings	${fields["datalog_facility"]}	LOG_LOCAL0
	Should Be Equal As Strings	${fields["event_facility"]}	LOG_LOCAL0
	Should Be Equal As Strings	${fields["ipv4_address"]}	localhost
	Should Be Equal As Strings	${fields["ipv6_address"]}	::
	Should Be True	${fields["system_console"]}
	Should Be True	${fields["ipv4_remote_server"]}
	Should Be True	${fields["ipv6_remote_server"]}
	Should Be True	${fields["admin_session"]}

*** Keywords ***

SUITE:Setup
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin
	Reset Values

SUITE:Teardown
	Reset Values
	API::Delete::Session


Reset Values
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	destination=nfs
	Set To Dictionary	${payload}	nfs_server=${EMPTY}
	Set To Dictionary	${payload}	nfs_path=${EMPTY}
	Set To Dictionary	${payload}	nfs_archive_by_time=${EMPTY}
	Set To Dictionary	${payload}	number_of_archives_in_nfs=10
	Set To Dictionary	${payload}	nfs_file_size=1024
	Set To Dictionary	${payload}	archive_by_time=${EMPTY}
	Set To Dictionary	${payload}	number_of_archives=1
	Set To Dictionary	${payload}	file_size=256
	API::Put::Auditing::Destination::File	${payload}
	Set To Dictionary	${payload}	destination=local
	API::Put::Auditing::Destination::File	${payload}

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	email_server=${EMPTY}
	Set To Dictionary	${payload}	destination_email=${EMPTY}
	Set To Dictionary	${payload}	password=${EMPTY}
	Set To Dictionary	${payload}	confirm_password=${EMPTY}
	Set To Dictionary	${payload}	username=${EMPTY}
	API::Put::Auditing::Destination::Email	${payload}

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	snmptrap_transport_protocol=udp
	Set To Dictionary	${payload}	snmptrap_server=127.0.0.1
	API::Put::Auditing::Destination::SNMP Trap	${payload}

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	ipv4_address=${EMPTY}
	Set To Dictionary	${payload}	ipv6_address=${EMPTY}
	Set To Dictionary	${payload}	system_console=${TRUE}
	Set To Dictionary	${payload}	ipv4_remote_server=${TRUE}
	Set To Dictionary	${payload}	ipv6_remote_server=${TRUE}
	Set To Dictionary	${payload}	admin_session=${TRUE}
	API::Put::Auditing::Destination::Syslog	${payload}
	Set To Dictionary	${payload}	ipv4_remote_server=${FALSE}
	Set To Dictionary	${payload}	ipv6_remote_server=${FALSE}
	Set To Dictionary	${payload}	admin_session=${FALSE}
	API::Put::Auditing::Destination::Syslog	${payload}
