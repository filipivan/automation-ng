*** Settings ***
Documentation	Test /api/<version>/auditing/settings endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Get auditing event file
	${fields}=	API::Get::Auditing::Events::File
	Length Should Be	${fields}	4
	Should Be True	${fields["system_events"]}
	Should Be True	${fields["aaa_events"]}
	Should Be True	${fields["device_events"]}
	Should Be True	${fields["logging_events"]}

Get auditing event email
	${fields}=	API::Get::Auditing::Events::Email
	Length Should Be	${fields}	4
	Should Not Be True	${fields["system_events"]}
	Should Not Be True	${fields["aaa_events"]}
	Should Not Be True	${fields["device_events"]}
	Should Not Be True	${fields["logging_events"]}

Get auditing event SNMP Trap
	${fields}=	API::Get::Auditing::Events::SNMP Trap
	Length Should Be	${fields}	4
	Should Not Be True	${fields["system_events"]}
	Should Not Be True	${fields["aaa_events"]}
	Should Not Be True	${fields["device_events"]}
	Should Not Be True	${fields["logging_events"]}

Get auditing event Syslog
	${fields}=	API::Get::Auditing::Events::Syslog
	Length Should Be	${fields}	4
	Should Be True	${fields["system_events"]}
	Should Be True	${fields["aaa_events"]}
	Should Be True	${fields["device_events"]}
	Should Be True	${fields["logging_events"]}

Update auditing event file
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	system_events=${FALSE}
	Set To Dictionary	${payload}	aaa_events=${FALSE}
	Set To Dictionary	${payload}	device_events=${FALSE}
	Set To Dictionary	${payload}	logging_events=${FALSE}
	API::Put::Auditing::Events::File	${payload}

Update auditing event email
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	system_events=${TRUE}
	Set To Dictionary	${payload}	aaa_events=${TRUE}
	Set To Dictionary	${payload}	device_events=${TRUE}
	Set To Dictionary	${payload}	logging_events=${TRUE}
	API::Put::Auditing::Events::Email	${payload}

Update auditing event SNMP Trap
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	system_events=${TRUE}
	Set To Dictionary	${payload}	aaa_events=${TRUE}
	Set To Dictionary	${payload}	device_events=${TRUE}
	Set To Dictionary	${payload}	logging_events=${TRUE}
	API::Put::Auditing::Events::SNMP Trap	${payload}

Update auditing event Syslog
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	system_events=${FALSE}
	Set To Dictionary	${payload}	aaa_events=${FALSE}
	Set To Dictionary	${payload}	device_events=${FALSE}
	Set To Dictionary	${payload}	logging_events=${FALSE}
	API::Put::Auditing::Events::Syslog	${payload}

Get auditing event file After update
	${fields}=	API::Get::Auditing::Events::File
	Length Should Be	${fields}	4
	Should Not Be True	${fields["system_events"]}
	Should Not Be True	${fields["aaa_events"]}
	Should Not Be True	${fields["device_events"]}
	Should Not Be True	${fields["logging_events"]}

Get auditing event email After update
	${fields}=	API::Get::Auditing::Events::Email
	Length Should Be	${fields}	4
	Should Be True	${fields["system_events"]}
	Should Be True	${fields["aaa_events"]}
	Should Be True	${fields["device_events"]}
	Should Be True	${fields["logging_events"]}

Get auditing event SNMP Trap After update
	${fields}=	API::Get::Auditing::Events::SNMP Trap
	Length Should Be	${fields}	4
	Should Be True	${fields["system_events"]}
	Should Be True	${fields["aaa_events"]}
	Should Be True	${fields["device_events"]}
	Should Be True	${fields["logging_events"]}

Get auditing event Syslog After update
	${fields}=	API::Get::Auditing::Events::Syslog
	Length Should Be	${fields}	4
	Should Not Be True	${fields["system_events"]}
	Should Not Be True	${fields["aaa_events"]}
	Should Not Be True	${fields["device_events"]}
	Should Not Be True	${fields["logging_events"]}

*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin
	Reset Value

Suite Teardown
	Reset Value
	API::Delete::Session

Reset Value
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	system_events=${TRUE}
	Set To Dictionary	${payload}	aaa_events=${TRUE}
	Set To Dictionary	${payload}	device_events=${TRUE}
	Set To Dictionary	${payload}	logging_events=${TRUE}
	API::Put::Auditing::Events::File	${payload}

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	system_events=${FALSE}
	Set To Dictionary	${payload}	aaa_events=${FALSE}
	Set To Dictionary	${payload}	device_events=${FALSE}
	Set To Dictionary	${payload}	logging_events=${FALSE}
	API::Put::Auditing::Events::Email	${payload}

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	system_events=${FALSE}
	Set To Dictionary	${payload}	aaa_events=${FALSE}
	Set To Dictionary	${payload}	device_events=${FALSE}
	Set To Dictionary	${payload}	logging_events=${FALSE}
	API::Put::Auditing::Events::SNMP Trap	${payload}

	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	system_events=${TRUE}
	Set To Dictionary	${payload}	aaa_events=${TRUE}
	Set To Dictionary	${payload}	device_events=${TRUE}
	Set To Dictionary	${payload}	logging_events=${TRUE}
	API::Put::Auditing::Events::Syslog	${payload}
