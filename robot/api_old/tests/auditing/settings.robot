*** Settings ***
Documentation	Test /api/<version>/auditing/settings endpoint
Metadata	Version			1.0
Metadata	Executed At		${HOST}
Resource	../init.robot

Suite Setup	Suite Setup
Suite Teardown	Suite Teardown

Force Tags		API	${BROWSER}	${SYSTEM}	${MODEL}	${VERSION}
Default Tags	EXCLUDEIN3_2

*** Test Cases ***

Get auditing settings
	${settings}=	API::Get::Auditing::Settings
	Should Be True	${settings["enable_file_destination"]}
	Should Not Be True	${settings["enable_syslog_destination"]}
	Should Not Be True	${settings["datalog_add_timestamp"]}
	Should Be Equal As Strings	${settings["datalog_timestamp_format"]}	utc
	Should Be Equal As Strings	${settings["event_timestamp_format"]}	utc
	Length Should Be	${settings}	5

Update auditing settings
	${payload}=	Get Payload
	API::Put::Auditing::Settings	${payload}
	${settings}=	API::Get::Auditing::Settings
	Should Not Be True	${settings["enable_file_destination"]}
	Should Be True	${settings["enable_syslog_destination"]}
	Should Be True	${settings["datalog_add_timestamp"]}
	Should Be Equal As Strings	${settings["datalog_timestamp_format"]}	local
	Should Be Equal As Strings	${settings["event_timestamp_format"]}	local
	Length Should Be	${settings}	5

*** Keywords ***

Suite Setup
	API::Setup HTTP Context	${HOST}
	API::Post::Session	admin	admin
	Reset Value

Suite Teardown
	Reset Value
	API::Delete::Session

Get Payload
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	event_timestamp_format=local
	Set To Dictionary	${payload}	datalog_timestamp_format=local
	Set To Dictionary	${payload}	enable_file_destination=${FALSE}
	Set To Dictionary	${payload}	enable_syslog_destination=${TRUE}
	Set To Dictionary	${payload}	datalog_add_timestamp=${TRUE}
	[return]	${payload}

Reset Value
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	event_timestamp_format=utc
	Set To Dictionary	${payload}	datalog_timestamp_format=utc
	Set To Dictionary	${payload}	enable_file_destination=${TRUE}
	Set To Dictionary	${payload}	enable_syslog_destination=${FALSE}
	Set To Dictionary	${payload}	datalog_add_timestamp=${FALSE}
	API::Put::Auditing::Settings	${payload}
