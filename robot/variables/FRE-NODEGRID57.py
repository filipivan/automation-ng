#!/usr/bin/python
# -*- coding: utf-8 -*-

HOMEPAGE = "https://192.168.2.57"
HOST = "192.168.2.57"
BROWSER = "Chrome"
TESTSYSTEMIP = "192.168.2.57"
GATEWAY = "192.168.2.254"
NETWORK = "192.168.2.0/24"
NGVERSION = "4.1"   #branch version
NFSPATH = "/nfs/nodegrid_57"
