#!/usr/bin/python
# -*- coding: utf-8 -*-

HOMEPAGE = "https://192.168.2.87"
HOST = "192.168.2.87"
BROWSER = "Chrome"
#BROWSER = "Firefox"
TESTSYSTEMIP = "192.168.2.87"
GATEWAY = "192.168.2.254"
NETWORK = "192.168.2.0/24"
#CLIENTIP = "192.168.2.151"
CLIENTIP = "192.168.2.68"
SESSION_TYPES = ["cli", "web", "none"]
SESSION_MODE = ["https", "ssh", "telnet", "http", "console"]
NGVERSION = "4.0"
VALIDATE_LDAP_USER_AUTHENTICATION = True
VALIDATE_AD_USER_AUTHENTICATION = True
VALIDATE_IPV6GWD = True
LICENSE =     "FS0SE-5FGGO-9UZ5V-33MGM"
EXPLICENSE =   "NE0BD-W741B-9Z1RD-7K2DR"
INTERFACES =  ["eth0","eth1","loopback","loopback0","main","virbr0"]
METRIC = ["0","90","256","1024"]
EVENTCATEGOREY  =   ["system event","aaa event","device event","logging event"]
SCREENSHOTDIR   =   "screenshots"
DISCOVERY_LOG_FORMAT = "[A-Z]{1}[a-z]{2}\s+[A-Z]{1}[a-z]{2}\s+\d{1,2}\s+\d{2}[:]\d{2}[:]\d{2}\s+\d{4}\s+\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\s+.{1,64}\s+.{1,16}\s+.{1,16}"
# the pdu variable is used to add a pdu device and test the outlet table in
PDU = { "name" : "servertech", "ip": "66.214.208.191", "type" : "pdu_servertech","username" : "admn", "password" : "admn"}
DUMMY_DEVICE_CONSOLE_NAME = "dummy_dev-ice.console"
DUMMY_DEVICE_CONSOLE_NAME_ACCESS = "dummy_dev-ice.access"
#NFSSERVERPRESENT = "Yes"
#NFSSERVERUSER = "root"
#NFSSERVERPASSWORD = "root"
#NFSSERVER = "192.168.2.88"
#NFSPATH = "/nfs/nodegrid_183"
#SNMPSERVERPRESENT = "Yes"
#SNMPSERVER = "192.168.2.88"
#SNMPSERVERUSER = "snmpuser"
#SNMPSERVERPASSWORD = "snmpuser"
#SNMPUDPPORT = "162"
#SNMPTCPPORT = "161"
#SNMPCOMMUNITY = "public"
#SNMPPATH = "/var/log"
#SNMPFILE = "nodegrid-snmp.log"
#SNMPMD5DESUSER = "mymd5desuser"
#SNMPMD5AESUSER = "mymd5aesuser"
#SNMPSHADESUSER = "myshadesuser"
#SNMPSHAAESUSER = "mysheaesuser"
#SNMPAUTHONLYUSER = "myauthonlyuser"
#SNMPNOAUTHNOPRIVUSER = "mynoauthnoprivuser"
#SNMPMD5PASSWORD = "mymd5password"
#SNMPDESPASSWORD = "mydespassword"
#SNMPSHAPASSWORD = "myshapassword"
#SNMPAESPASSWORD = "myaespassword"
#SYSLOGSERVERPRESENT = "Yes"
#SYSLOGSERVER = "192.168.2.88"
#SYSLOGSERVERUSER = "root"
#SYSLOGSERVERPASSWORD = "root"
#EVENTFACILITY = "log_local_3"
#DATALOGFACILITY = "log_local_4"
#SYSLOGPATH = "/var/log"
#SYSLOGEVENTSFILE = "nodegrid-events.log"
#SYSLOGDATALOGFILE = "nodegrid-datatlog.log"
DUMMY_PDU_SERVERTECH_NAME = "DUMMY_SERVERTECH"



EXECUTE_ILO_TESTS = False   #change from False to True
ILO_NAME = ""
ILO_IP = ""
ILO_USERNAME = ""
ILO_PASSWORD = ""
ILO_AWAITED_STRING_ON_CONNECT_CMD = ""

EXECUTE_IDRAC_TESTS = False   #change from False to True
IDRAC_NAME = ""
IDRAC_IP = ""
IDRAC_USERNAME = ""
IDRAC_PASSWORD = ""
IDRAC_AWAITED_STRING_ON_CONNECT_CMD = ""

EXECUTE_IMM_TESTS = False   #change from False to True
IMM_NAME = ""
IMM_IP = ""
IMM_USERNAME = ""
IMM_PASSWORD = ""
IMM_AWAITED_STRING_ON_CONNECT_CMD = ""  #need to know what is the correct string to put in the line


#ILO_NAME = "HPiLO4_2.35"
#ILO_IP = "192.168.2.35"
#ILO_USERNAME = "Administrator"
#ILO_PASSWORD = "Administrator"
#ILO_AWAITED_STRING_ON_CONNECT_CMD = ""     #need to know what is the correct string to put in the line


EXECUTE_IDRAC_TESTS = True   #change from False to True
IDRAC_NAME = "iDRAC_R330_170.178.141.146"
IDRAC_IP = "170.178.141.146"
IDRAC_USERNAME = "root"
IDRAC_PASSWORD = "NodeGridD3m0Passw0rd"
IDRAC_AWAITED_STRING_ON_CONNECT_CMD = "zpe-PowerEdge-R330 login:"   #need to know what is the correct string to put in the line


#EXECUTE_IMM_TESTS = True   #change from False to True
#IMM_NAME = "IMM"
#IMM_IP = "170.178.141.147"
#IMM_USERNAME = "USERID"
#IMM_PASSWORD = "PASSW0RD"
#IMM_AWAITED_STRING_ON_CONNECT_CMD = "system>"     #need to know what is the correct string to put in the line
