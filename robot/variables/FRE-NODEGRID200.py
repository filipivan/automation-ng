#!/usr/bin/python
# -*- coding: utf-8 -*-

HOMEPAGE = "https://192.168.2.200"
HOST = "192.168.2.200"
TESTSYSTEMIP = "192.168.2.200"
GATEWAY = "192.168.2.254"
NETWORK = "192.168.2.0/24"
BROWSER = "Chrome"
NGVERSION = "3.2"
NFSPATH = "/nfs/nodegrid_200"