#!/usr/bin/python
# -*- coding: utf-8 -*-

VALIDATE_IPV6GWD = True  # Where is it used?

"""
    LIST OF VARIABLES RELATED TO ROBOT
"""
# ROBOT:DIRECTORY TO STORE SCREENSHOTS
SCREENSHOTDIR = "screenshots"

"""
    LIST OF VARIABLES RELATED TO NETWORK AND CONNECTIONS
"""
FRE_NETWORK = "192.168.0.0/20"
CLI_DEFAULT_TIMEOUT = "30s"
GUI_DEFAULT_TIMEOUT = "30s"
API_DEFAULT_TIMEOUT = "30s"

"""
    LIST OF VARIABLES RELATED TO NGM
"""
# NGM:DEFAULT USERNAME
USERNAME = "admin"
FACTORY_PASSWORD = "admin"
DEFAULT_USERNAME = "admin"
DEFAULT_PASSWORD = FACTORY_PASSWORD
ROOT_DEFAULT_USERNAME = "root"
ROOT_FACTORY_PASSWORD = "root"
ROOT_PASSWORD = "root"
# NGM:QA PASSWORD
CONSOLE_PASSWORD = ".ZPE5ystems!2020"
QA_PASSWORD = ".ZPE5ystems!2022"
API_VERSION = "v1"

# NGM:DEFAULT HOSTNAME
HOSTNAME_NODEGRID = "nodegrid"
DOMAINAME_NODEGRID = "localdomain"
# NGM:INTERFACES
SESSION_TYPES = ["cli", "web", "none"]
# NGM:PROTOCOLS TO ACCESS NODEGRID
SESSION_MODE = ["https", "ssh", "telnet", "http", "console"]
# NGM:NETWORK INTERFACES (When without GSM, Wifi...)
INTERFACES = ["eth0", "eth1", "loopback", "loopback0", "main", "virbr0"]
# NGM:METRIC
METRIC = ["0", "90", "256", "1024"]
# NGM:TYPE OF EVENTS
EVENTCATEGOREY = ["system event", "aaa event", "device event", "logging event"]
# NGM:FORMAT FOR LOGS
DISCOVERY_LOG_FORMAT = "[A-Z]{1}[a-z]{2}\s+[A-Z]{1}[a-z]{2}\s+\d{1,2}\s+\d{2}[:]\d{2}[:]\d{2}\s+\d{4}\s+[a-z0-9\.\(\)]{1,16}\s+.{1,64}\s+.{1,16}\s+.{1,16}"
# Origial
# DISCOVERY_LOG_FORMAT = "[A-Z]{1}[a-z]{2}\s+[A-Z]{1}[a-z]{2}\s+\d{1,2}\s+\d{2}[:]\d{2}[:]\d{2}\s+\d{4}\s+\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\s+.{1,64}\s+.{1,16}\s+.{1,16}"

"""
    LIST OF VARIABLES RELATED TO DEVICES
"""
# GSM
GSM_WMODEM_CARRIERS_APN = {"AT&T": "i2gold", "Verizon": "VZWINTERNET", "VERIZON": "vzwinternet", "Verizon Wireless": "vzwinternet"}
GSM_CELLULAR_CONNECTION = "test-automation-gsm-connection"
GSM_TYPE = "mobile_broadband_gsm"
GSM_TYPE_GUI = "Mobile Broadband GSM"
GSM_RADIO_MODE = "LTE"
GSM_4G_MODEM_MODEL = "EM7565"
GSM_5G_MODEM_MODEL = "EM9191"

# GENERIC NAMES
DUMMY_DEVICE_CONSOLE_NAME = "dummy_dev-ice.console"
DUMMY_DEVICE_CONSOLE_NAME_ACCESS = "dummy_dev-ice.access"
DUMMY_PDU_SERVERTECH_NAME = "DUMMY_SERVERTECH"
PDU_SERVERTECH_CLONE = "pdu_servertech_ssh"
MKS_DEVICE = "Centos-Guacamole"

# DEVICE:PDU
PDU_NAME = "servertech"
PDU_IP = "192.168.6.111"    #no longer 2.111
PDU_TYPE = "pdu_servertech"
PDU_USERNAME = "admn"
PDU_PASSWORD = "admn"
SSH_OPTIONS_PDU_SERVERTECH = "\"-o HostKeyAlgorithms=ssh-rsa,ssh-dss -o KexAlgorithms=diffie-hellman-group1-sha1 -o Ciphers=aes128-cbc,3des-cbc,aes256-ctr,aes192-ctr,aes128-ctr,aes192-cbc,aes256-cbc\""

# --------------------------------    PDU's   --------------------------------
# PDU APC
PDU_APC				= {"name": "pdu_apc", "execute": "True", "ip": "192.168.7.239", "type": "pdu_apc", "username": "apc", "password": "apc"}
# PDU CPI | Is not used in automation...
PDU_CPI				= {"name": "pdu_cpi", "execute": "False", "ip": "192.168.2.243", "type": "pdu_cpi", "username": "", "password": ""}
# PDU EATON
PDU_EATON			= {"name": "pdu_eaton", "execute": "True", "ip": "192.168.7.219", "type": "pdu_eaton", "username": "admin", "password": "admin"}
# PDU DIGITAL LOGGERS | Is loaner from SE
PDU_DIGITAL_LOGGERS	= {"name": "pdu_digital_loggers", "execute": "False", "ip": "192.168.7.156", "type": "pdu_digital_loggers", "username": "admin", "password": "123456789"}
# PDU CYBERPOWER
PDU_CYBERPOWER		= {"name": "pdu_cyberpower", "execute": "True", "ip": "192.168.3.160", "type": "pdu_cyberpower", "username": "admin", "password": "CyberPower"}
# PDU GEIST | Device Dead
PDU_GEIST			= {"name": "pdu_geist", "execute": "False", "ip": "76.79.48.119", "type": "pdu_geist", "username": "GeistDemo", "password": "powerup"}
# PDU SERVERTECH
PDU_SERVERTECH 		= {"name": "pdu_servertech", "execute": "True", "ip": "192.168.6.111", "ip6": "FE80::20A:9CFF:FE51:3320", "fqdn": "automation-fqdn-test.com", "type": "pdu_servertech", "username": "admn", "password": "admn"}
# PDU RARITAN | Is used with Dev team...
PDU_RARITAN 		= {"name": "pdu_raritan", "execute": "True", "ip": "192.168.3.158", "type": "pdu_raritan", "username": "admin", "password": "Admin"}
# PDU RARITAN METERED
PDU_RARITAN_METERED = {"name": "pdu_raritan_metered","execute": "False", "ip": "192.168.3.55", "type": "pdu_raritan", "username": "admin", "password": "raritan123"}
# PDU RARITAN PX3
PDU_RARITAN_PX3 	= {"name": "pdu_raritan_px3", "execute": "True", "ip": "192.168.3.7", "type": "pdu_raritan", "username": "admin", "password": "admin"}
# PDU RARITAN PM3000 | Device Production PDU. Do not turn off any of the outlets
PDU_PM3000 			= {"name": "pdu_raritan_pm3000", "execute": "False", "ip": "192.168.3.XX", "type": "pdu_pm3000", "username": "admin", "password": "avocent"}
# PDU MPH2
PDU_MPH2 			= {"name": "pdu_mph2", "execute": "True", "ip": "192.168.7.232", "type": "pdu_mph2", "username": "admin", "password": "admin"}
# PDU CPI METERED
PDU_CPI_METERED 	= {"name": "pdu_cpi_metered", "execute": "True", "ip": "192.168.7.235", "type": "pdu_cpi", "username": "admin", "password": "admin"}
# PDU TRIPPLITE ATS | Device Dead
PDU_TRIPPLITE_ATS 	= {"name": "pdu_tripplite_ats", "execute": "False", "ip": "192.168.3.20", "type": "pdu_tripplite", "username": "localadmin", "password": "localadmin"}
# PDU DIGI Console TS
PDU_DIGI_CONSOLE_TS = {"name": "pdu_digi_console_ts", "execute": "True", "ip": "192.168.3.183", "type": "console_server_digicp", "username": "root", "password": "dbps"}
# PDU DIGI PASSPORT
PDU_DIGI_PASSPORT 	= {"name": "pdu_digi_passport", "execute": "True", "ip": "192.168.3.187", "type": "console_server_digicp", "username": "root", "password": "dbps"}
# PDU DIGI CM48
PDU_DIGI_CM48 		= {"name": "pdu_digi_cm48", "execute": "True", "ip": "192.168.3.242", "type": "console_server_digicp", "username": "root", "password": "dbps"}
# PDU Dell iDRAC
PDU_IDRAC 			= {"name": "pdu_idrac", "execute": "False", "ip": "192.168.2.233", "type": "drac", "username": "root", "password": "calvin"}
# PDU Dell iDRAC9
PDU_IDRAC9 			= {"name": "pdu_idrac9", "execute": "False", "ip": "192.168.3.23", "type": "idrac6", "username": "root", "password": "root"}
# PDU ENCONNEX
PDU_ENCONNEX 		= {"name": "pdu_enconnex", "execute": "True", "ip": "192.168.3.80", "type": "pdu_enconnex", "username": "admin", "password": "123456789"}
# PDU iLO4
PDU_ENCONNEX 		= {"name": "pdu_ilo4", "execute": "False", "ip": "192.168.2.232", "type": "ilo", "username": "Administrator", "password": "Adm1n1strat0r"}
# PDU iLO5
PDU_ENCONNEX 		= {"name": "pdu_ilo5", "execute": "False", "ip": "192.168.2.15", "type": "ilo5", "username": "Administrator", "password": "KWH6WFQ7"}
# -----------------------------------------------------------------------------

# PDU ENCONNEX
ENCONNEX_IP = "192.168.3.80"
ENCONNEX_USER = "admin"
ENCONNEX_PASSWORD = "123456789"
ENCONNEX_TYPE = "pdu_enconnex"

# DEVICE:CONSOLE SERVER (NG - NSC)
EXECUTE_CSN_TESTS = True
CONSOLE_SERVER_NG_NAME = "console_server_ng"
CONSOLE_SERVER_NG_IP = "192.168.6.41"   #no longer available NSC2.53, use unit NSC6.41 as for now
CONSOLE_SERVER_NG_USERNAME = "admin"
CONSOLE_SERVER_NG_PASSWORD = "admin"
CONSOLE_SERVER_NG_AWAITED_STRING_ON_CONNECT_CMD = "ogin"

# DEVICE:HP iLO
EXECUTE_ILO_TESTS = False
ILO_NAME = ""
ILO_IP = ""
ILO_USERNAME = ""
ILO_PASSWORD = ""
ILO_AWAITED_STRING_ON_CONNECT_CMD = ""

# DEVICE:iLO4 v2.70
ILO4_IP = "192.168.2.232"
ILO_TYPE = "ilo"
ILO4_USERNAME = "Administrator"
ILO4_PASSWORD = "Adm1n1strat0r"

# DEVICE:iLO5
ILO5_IP = "192.168.2.15"
ILO5_USERNAME = "Administrator"
ILO5_PASSWORD = "KWH6WFQ7"

#DEVICE: Intel BMC
INTEL_BMC_IP = "192.168.2.174"
INTEL_BMC_TYPE = "intel_bmc"
INTEL_BMC_USERNAME = "admin"
INTEL_BMC_PASSWORD = "inteladmin"

# DEVICE:Dell iDRAC
EXECUTE_IDRAC_TESTS = False
IDRAC_NAME = "iDRAC_R330_192.168.2.233" #old IP 2.28
IDRAC_IP = "192.168.2.233"
IDRAC_USERNAME = "root"
IDRAC_PASSWORD = "calvin"
# IDRAC_PASSWORD = "NodeGridD3m0Passw0rd"
IDRAC_AWAITED_STRING_ON_CONNECT_CMD = "zpe-PowerEdge-R330 login:"

# DEVICE:Dell iDRAC9
# EXECUTE_IDRAC9_TESTS = False
IDRAC9_NAME = "iDRAC9_192.168.3.23"
IDRAC9_IP = "192.168.3.23"
IDRAC9_USERNAME = "root"
IDRAC9_PASSWORD = "Calv1n9"
IDRAC9_AWAITED_STRING_ON_CONNECT_CMD = "racadm>>"

# DEVICE:IBM IMM
EXECUTE_IMM_TESTS = False
IMM_NAME = "IMM_192.168.2.157"
IMM_IP = "192.168.2.157"  # IMM IP is nolonger 192.168.2.147 from 192.168.2.203
IMM_USERNAME = "USERID"
IMM_PASSWORD = "PASSW0RD"
IMM_AWAITED_STRING_ON_CONNECT_CMD = ""  # No OS, prompt will shows imm prompt "system>"

# DEVICE:SUPERMICRO 2.236; nolonger 2.36 - No working
#KVM_DEVICE = {"name": "supermicro", "ip": "192.168.2.236", "username": "ADMIN", "password": ".Sup3rM1cr0!", "type": "ipmi_2.0", "pymod": "supermicro.py"}

# DEVICE:SUPERMICRO New
KVM_DEVICE = {"name": "supermicro", "ip": "192.168.3.244", "username": "ADMIN", "password": "Newuser1234", "type": "ipmi_2.0", "pymod": "supermicro.py"}

# DEVICE:RARITAN DOMINION KVM
RARITAN_KVM_PRESENT = True
RARITAN_KVM_IP = "192.168.2.23"
RARITAN_KVM_USERNAME = "admin"
RARITAN_KVM_PASSWORD = "D0mini0n"
RARITAN_KVM_TYPE = "kvm_raritan"
RARITAN_KVM_AVAILABLE_PORTS = ["1", "2"]

# WINDOWS_KVM
WINDOWS_KVM_DEVICE = "windows_kvm"
WINDOWS_KVM_IP = "192.168.3.96"
WINDOWS_KVM_USER = "QAusers"
WINDOWS_KVM_PASSWORD = "Passw0rd!"
WINDOWS_KVM_TYPE = "device_console"
WINDOWS_KVM_MODE = "On-demand"
WINDOWS_KVM_COMMAND = "KVM"
WINDOWS_KVM_TYPE_EXTENSION = "rdp.py"

# DEVICE:ACS6000
ACS6000_IP = "192.168.2.151"
ACS6000_USERNAME = "admin"
ACS6000_PASSWORD = "avocent"
ACS6000_IP_RANGE_START = "192.168.2.150"
ACS6000_IP_RANGE_END = "192.168.2.155"

"""
    LIST OF VARIABLES RELATED TO VIRTUALIZATION
"""
VMMANAGER = {
	"ip": "192.168.2.160",
	"username": "administrator@vsphere.local",
	"password": ".ZPE5ystems!",
	"hosts": ["192.168.2.161"],
	"datacenter": "Datacenter"
}
# was "hosts": ["192.168.2.224", "192.168.2.234", "192.168.2.238"], but 2.234 is temporaraly unaccessible
VMMANAGER_ESXI = {"ip": "192.168.2.161", "username": "root", "password": ".ZPE5ystems!"}
# VM:Windows (VM available in the VMMANAGER to launch MKS - Template Device used to discover VMs, 2.122)
MKS_TEMPLATE = {"name": "Centos-Guacamole", "ip": "192.168.2.161", "username": "root", "password": ".ZPE5ystems!", "type": "virtual_console_vmware"}

"""
    LIST OF VARIABLES RELATED TO SERVERS
"""
# SERVER:FTP
FTPSERVER_PRESENT = True
FTPSERVER_IP = "192.168.2.201"
FTPSERVER_PORT = "21"
FTPSERVER_URL = "ftp://" + FTPSERVER_IP + ":" + FTPSERVER_PORT
FTPSERVER_USER = "anonymous"
FTPSERVER_PASSWORD = "anonymous"

#SERVER:VNC
VNC_VM_NAME = "QA-UbuntuDesktop-VNC-3.232"
VNC_IP = "192.168.2.222"
VNC_USER = "qausers"
VNC_PASSWORD = "QAusers!"
VNC_TCP_PORT = "5900"

#SERVER:RDP
RDP_VM_NAME = "QA-Windows10-3.227"
RDP_IP = "192.168.3.227"
RDP_USER = "QAusers"
RDP_PASSWORD = "Passw0rd!"
RDP_TCP_PORT = "3389"

# SERVER:FTP VMRC
FTPSERVER2_PRESENT = True
FTPSERVER2_IP = "192.168.2.88"
FTPSERVER2_PORT = "21"
FTPSERVER2_URL = "ftp://" + FTPSERVER2_IP + ":" + FTPSERVER2_PORT
FTPSERVER2_USER = "ftpuser"
FTPSERVER2_PASSWORD = "ftpuser"
#FTPSERVER2_VMRC10_PATH = "/test_files/VMware-Remote-Console-10.0.4-11818843.x86_64.bundle"
FTPSERVER2_VMRC10_PATH = "/files/VMware-Remote-Console-10.0.4-11818843.x86_64.bundle"
#FTPSERVER2_VMRC_PATH = "/test_files/VMware-Remote-Console.bundle"
FTPSERVER2_VMRC_NAME = "VMware-Remote-Console.bundle"
FTPSERVER2_VMRC_PATH = "/files/VMware-Remote-Console.bundle"

# SERVER:FTP 192.168.3.8
FTPSERVER3_PRESENT = False
FTPSERVER3_IP = "192.168.3.8"
FTPSERVER3_PORT = "21"
FTPSERVER3_URL = "ftp://" + FTPSERVER3_IP + ":" + FTPSERVER3_PORT
FTPSERVER3_USER = "ftpuser"
FTPSERVER3_PASSWORD = "ftpuser"
#FTPSERVER3_VMRC10_PATH = "/test_files/VMware-Remote-Console-10.0.4-11818843.x86_64.bundle"
FTPSERVER3_VMRC10_PATH = "/files/VMware-Remote-Console-10.0.4-11818843.x86_64.bundle"
FTPSERVER3_VMRC_PATH = "/VMware-Remote-Console.bundle"

# SERVER:NFS /nfs is nfs path directory
NFSSERVERPRESENT = "Yes"
NFSSERVER = "192.168.2.88"
NFSSERVERUSER = "root"
NFSSERVERPASSWORD = "Passw0rd!"

# SERVER2:NFS 192.168.3.8 - /nfs is nfs path directory
NFSSERVER2_PRESENT = "Yes"
NFSSERVER2 = "192.168.3.8"
NFSSERVER2USER = "root"
NFSSERVER2PASSWORD = "Passw0rd!"

#qaserver:NETFLOW COLLECTOR
NETFLOW_COLLECTOR_SERVER = "192.168.2.223"
NETFLOW_USERNAME = "qausers"
NETFLOW_PASSWD = "QAusers!"
NETFLOW_PORT = "2055"
SFLOW_PORT = "6363"


# SERVER:SNMP/SNMPTRAP
SNMPSERVERPRESENT = True
SNMPSERVER = "192.168.2.88"
SNMPSERVERUSER = "snmpuser"
SNMPSERVERPASSWORD = "snmpuser"
SNMPUDPPORT = "162"
SNMPTCPPORT = "161"
SNMPCOMMUNITY = "public"
SNMPPATH = "/var/log"
SNMPFILE = "nodegrid-snmp.log"
SNMPMD5DESUSER = "mymd5desuser"
SNMPMD5AESUSER = "mymd5aesuser"
SNMPSHADESUSER = "myshadesuser"
SNMPSHAAESUSER = "myshaaesuser"
SNMPAUTHONLYUSER = "myauthonlyuser"
SNMPNOAUTHNOPRIVUSER = "mynoauthnoprivuser"
SNMPMD5PASSWORD = "mymd5password"
SNMPDESPASSWORD = "mydespassword"
SNMPSHAPASSWORD = "myshapassword"
SNMPAESPASSWORD = "myaespassword"

# SERVER2:SNMP/SNMPTRAP 192.168.3.8
SNMPSERVER2PRESENT = True
SNMPSERVER2 = "192.168.3.8"
SNMPSERVER2USER = "snmpuser"
SNMPSERVER2PASSWORD2 = "snmpuser"
SNMPUDPPORT2 = "162"
SNMPTCPPORT2 = "161"
SNMPCOMMUNITY2 = "public"
SNMPPATH2 = "/var/log"
SNMPFILE2 = "nodegrid-snmp.log"
SNMPMD5DESUSER2 = "mymd5desuser"
SNMPMD5AESUSER2 = "mymd5aesuser"
SNMPSHADESUSER2 = "myshadesuser"
SNMPSHAAESUSER2 = "myshaaesuser"
SNMPAUTHONLYUSER2 = "myauthonlyuser"
SNMPNOAUTHNOPRIVUSER2 = "mynoauthnoprivuser"
SNMPMD5PASSWORD2 = "mymd5password"
SNMPDESPASSWORD2 = "mydespassword"
SNMPSHAPASSWORD2 = "myshapassword"
SNMPAESPASSWORD2 = "myaespassword"

# SERVER:SYSLOG
SYSLOGSERVERPRESENT = True
SYSLOGSERVER = "192.168.2.88"
SYSLOGSERVERUSER = "root"
SYSLOGSERVERPASSWORD = "Passw0rd!"
EVENTFACILITY = "log_local_3"
DATALOGFACILITY = "log_local_4"
SYSLOGPATH = "/var/log"
SYSLOGEVENTSFILE = "nodegrid-events.log"
SYSLOGDATALOGFILE = "nodegrid-datatlog.log"

# SERVER2:SYSLOG 192.168.3.8
SYSLOGSERVER2PRESENT = True
SYSLOGSERVER2 = "192.168.3.8"
SYSLOGSERVER2USER = "root"
SYSLOGSERVER2PASSWORD2 = "Passw0rd!"
EVENTFACILITY2 = "log_local_3"
DATALOGFACILITY2 = "log_local_4"
SYSLOGPATH2 = "/var/log"
SYSLOGEVENTSFILE2 = "nodegrid-events.log"
SYSLOGDATALOGFILE2 = "nodegrid-datatlog.log"

#ACS SERVER
ACS_IP = "192.168.6.176"	# Acs6004 has no static ip (Check console access on 6.150 or ask Jamie if its change ip).
ACS_USER = "admin"
ACS_PASSWORD = "avocent"

# SERVER:PERLE SCS
PERLE_SCS_PRESENT = True
PERLE_SCS_SERVER_IP = "192.168.6.11"  #no long 2.251
PERLE_SCS_PORT = "21"
PERLE_SCS_TYPE = "console_server_perle"
PERLE_SCS_SERVER_HOMEPAGE = "http://" + PERLE_SCS_SERVER_IP + "/manage.cgi/login"
PERLE_SCS_SERVER_USERNAME = "admin"
PERLE_SCS_SERVER_PASSWORD = "superuser"
PERLE_SCS_NEW_LINE = "\r\n"
PERLE_SCS_PROMPT = "SCS16#"
PERLE_SCS_AVAILABLE_PORTS = ["1"]

# SERVER:PERLE SCR
PERLE_SCR_PRESENT = True
PERLE_SCR_SERVER_IP = "192.168.2.82"
PERLE_SCR_TYPE = "console_server_perle"
PERLE_SCR_SERVER_HOMEPAGE = "http://" + PERLE_SCR_SERVER_IP + "/manage.cgi/login"
PERLE_SCR_SERVER_USERNAME = "admin"
PERLE_SCR_SERVER_PASSWORD = "P3rl3SCR"
PERLE_SCR_AVAILABLE_PORTS = ["3"]

# SERVER:RARITAN
RARITAN_SERVER_PRESENT = True
RARITAN_SERVER_IP = "192.168.6.163"	# Raritan console server has no static ip (Check console access on 6.150 or ask Jamie if its change ip).
RARITAN_PORT = "21"
RARITAN_SERVER_HOMEPAGE = "https://" + RARITAN_SERVER_IP + "/auth.asp"
RARITAN_SERVER_USERNAME = "admin"
RARITAN_SERVER_PASSWORD = ".D0m1n10n"
RARITAN_TYPE = "console_server_raritan"
RARITAN_NEW_LINE = "\r"
RARITAN_SERIAL1_NAME = "Serial_Port_1-opengear1"
RARITAN_SERIAL2_NAME = "Serial_Port_2-opengear2"
RARITAN_SERIAL_LOGIN = "imx4248 login:"
RARITAN_SERIAL_PROMPT = "#"


# SERVER:AD (Authentication) Active Directory Details
VALIDATE_AD_USER_AUTHENTICATION = True
VALIDATE_LDAP_USER_AUTHENTICATION = True
AD_SERVER = "192.168.2.227"
AD_BASE_DOMAIN = "dc=zpesystems,dc=local"
AD_BROWSE_USER_DN = "zpe@zpesystems.local"
AD_BROWSE_USER = "zpe"
AD_BROWSE_PASSWORD = "Passw0rd"
AD_ADMIN_GROUP = "testgroup"
AD_ADMIN_USER = "testuser3"
AD_ADMIN_PASSWORD = "Passw0rd"
AD_INVALID_USER = "InvalidUser"
AD_GROUP_ATRRIB = "memberOf"
AD_USER_ATRRIB = "sAMAccountName"
AD_GROUP_BASE = "CN=users,DC=ZPESYSTEMS,DC=local"

# SERVER:Nested (Authentication)
NESTED_SERVER = "192.168.2.227"
NESTED_BASE_DOMAIN = "dc=zpesystems,dc=local"
NESTED_DATA_USER_DN = "testuser@zpesystems.local"
NESTED_BROWSE_PASSWORD = "Passw0rd"
NESTED_BROWSE_CONFIRM_PASSWORD = "Passw0rd"
NESTED_GROUP_ATRRIB = "memberOf"
NESTED_USER_ATRRIB = "sAMAccountName"
NESTED_GROUP_BASE = "CN=users,DC=ZPESYSTEMS,DC=local"
NESTED_USER1 = "testuser"
NESTED_PASS1 = "Passw0rd"


# SERVER:LDAP
LDAPSERVER_PRESENT = True
LDAPSERVER = "192.168.2.88"
LDAPSERVER_USER = "sAMAccountName"
LDAPSERVER_SECURE = "off"
LDAPSERVER_BASE = "dc=zpe,dc=net"
LDAPSERVER_SECRET = "secret"
LDAPSERVER_USR = "cn=admin,dc=zpe,dc=net"
LDAPSERVER_PASS = "administrator"
LDAPSERVER_CONFRM_PASS = "administrator"
LDAPSERVER_GRP_ATTR = "memberUid"
LDAPSERVER_USER1 = "jane"
LDAPSERVER_PASS1 = "jane"

# SERVER2:LDAP 192.168.3.8
LDAPSERVER2_PRESENT = True
LDAPSERVER2 = "192.168.3.8"
LDAPSERVER2_USER = "sAMAccountName"
LDAPSERVER2_SECURE = "off"

# SERVER:TACACS (Authentication)
TACACS_SERVER = "192.168.2.88"
TACACSSERVER_IP = "192.168.2.88"
TACACSSERVER_URL = "https://" + TACACSSERVER_IP
TACACSSERVER_USER1 = "tacacs1"
TACACSSERVER_PASSWORD1 = "tacacs1"
TACACSSERVER_SECRET = "secret"
TACACSSERVER_CONFIRM_SECRET = "secret"


# SERVER2:TACACS (Authentication) 192.168.3.8
TACACS_SERVER2 = "192.168.3.8"
TACACSSERVER2_IP = "192.168.3.8"
TACACSSERVER2_URL = "https://" + TACACSSERVER_IP
TACACSSERVER2_USER1 = "tacacs1"
TACACSSERVER2_PASSWORD1 = "tacacs1"
TACACS_USER1 = "tac-grp1"
TACACS_USER2 = "tac-grp2"
TACACS_USER3 = "tac-grp3"
TACACS_SERVICES = "raccess"
TACACS_VERSION = "V0_V1"

#SERVER ESXI
ESXI_SERVER_IP = "192.168.2.234"
ESXI_SERVER_USERNAME = "root"
ESXI_SERVER_PASSWORD = ".VMware!123"
ESXI_SERVER_HOMEPAGE = "https://" + ESXI_SERVER_IP + "/ui/#/login"
VM1 = "Nodegrid_4.1.6"
VM2 = "NGM_Prajakta"
VM3 = "NGM_Ana_1"
VM4 = "NGM_Raquel_1"

# SERVER:RADIUS (Authentication)
RADIUSSERVER_PRESENT = True
RADIUSSERVER = "192.168.2.88"
RADIUSSERVER_SECRET = "secret"
RADIUSSERVER_SERVICE_TYPE = "vsa-grp1"
RADIUSSERVER_SERVICE_FRAMED = "radUserVsa"
RADIUSSERVER_SERVICE_CALL_BACK_LOGIN = "radUserVsa"
RADIUSSERVER_SERVICE_CALL_BACK_FRAMED = "radUserVsa"
RADIUSSERVER_USER1 = "radUserVsa"
RADIUSSERVER_PASS1 = "radUserVsa"


# SERVER2:RADIUS (Authentication) 192.168.3.8
RADIUSSERVER2_PRESENT = True
RADIUSSERVER2 = "192.168.3.8"
RADIUSSERVER2_SECRET = "secret"
RADIUSSERVER2_PORT = "1812"
RADIUSSERVER2_USER_TEST1 = "test1"
RADIUSSERVER2_USER_TEST1_PASSWORD = "test1"
RADIUSSERVER2_USER_TESTMD5 = "TESTMD5"
RADIUSSERVER2_USER_TESTPEAPMD5 = "TESTPEAPMD5"
RADIUSSERVER2_USER_TESTPEAPMSCH2 = "TESTPEAPMSCH2"
RADIUSSERVER2_USER_TESTTLS = "TESTTLS"
RADIUSSERVER2_USER_TESTTTLSMD5 = "TESTTTLSMD5"
RADIUSSERVER2_USER_TESTTTLSPAP = "TESTTTLSPAP"
RADIUSSERVER2_USER_TESTTTLSCHAP = "TESTTTLSCHAP"
RADIUSSERVER2_USER_TESTTTLSMSCH = "TESTTTLSMSCH"
RADIUSSERVER2_USER_TESTTTLSMSCH2 = "TESTTTLSMSCH2"
RADIUSSERVER2_USERS_PASSWORD = "NGAut0m!"

# SERVER:RADIUS3 (Authentication)
RADIUSSERVER3_PRESENT = True
RADIUSSERVER3 = "192.168.2.115"
RADIUSSERVER3_SECRET = "secret"
RADIUSSERVER3_SERVICE_TYPE = "vsa-grp1"
RADIUSSERVER3_SERVICE_FRAMED = "radUserVsa"
RADIUSSERVER3_SERVICE_CALL_BACK_LOGIN = "radUserVsa"
RADIUSSERVER3_SERVICE_CALL_BACK_FRAMED = "radUserVsa"
RADIUSSERVER3_USER1 = "radUserVsa"
RADIUSSERVER3_PASS1 = "radUserVsa"

# SERVER:RSA
RSASERVER_PRESENT = True
RSASERVER_NAME = "rsa_method"
RSASERVER = "rsa.zpesystems.com,192.168.2.229"
RSASERVER_ADDRESS = "192.168.2.229"
RSASERVER_ALIAS = "rsa-am.zpesystems.com"
RSASERVER_REST_URL = "https://rsa-am.zpesystems.com:5555/mfa/v1_1/authn"
RSASERVER_CLIENT_KEY = "08wd3zndor551a518i65taze0fgh6o0t9q3mc5dn6xz5mqur9xw54826vi8xa962"
RSASERVER_CLIENT_ID = "rsa.zpesystems.com"
RSASERVER_READ_TIMEOUT = "120"	#	30 <= read_timeout <= 240
RSASERVER_CONNECT_TIMEOUT = "20"	#	10 <= connect_timeout <= 120
RSASERVER_MAX_RETRIES = "3"	#	1 <= max_retries <= 5
RSASERVER_REPLICAS = "rsa.zpesystems.com,192.168.2.229"
RSASERVER_REPLICAS_PORTS = "rsa.zpesystems.com:4444,rsa.zpesystems.com:3333"
RSASERVER_POLICY_ID = "low-mfa-policy"
RSASERVER_TENANT_ID = "rsaready"
#RSASERVER_ROOTCA = "/test_files/RootCA.pem"  # ftp 88
RSASERVER_ROOTCA = "/files/RootCA.pem"  # ftp 88

# SERVER:SSO - DUE
SSOSERVER_PRESENT = True
SSOSERVER = "192.168.2.8"   #IP changed from 3.175 to 3.62
SSOSERVER_URL = "https://" + SSOSERVER   #IP URL changed from 3.175 to 3.62
SSOSERVER_USER = "zpe"
SSOSERVER_PASSWORD = "Passw0rd"
SSOSERVER_DAG_PATH = "/dag/"
SSOSERVER_SERVICE_PATH = "/dag/saml2/idp/SSOService.php"
SSOSERVER_ISSUER_PATH = "/dag/saml2/idp/metadata.php"
#SSOSERVER_CRT_PATH = "/test_files/saml.crt"  # ftp 88
SSOSERVER_CRT_PATH = "/files/saml.crt"  # ftp 88
#SSOSERVER_XML_PATH = "/test_files/saml.xml"  # ftp 88
SSOSERVER_XML_PATH = "/files/saml.xml"  # ftp 88
SSOSERVER_FILENAME = "dag.crt"
SSOSERVER_FILEPATH = "C:\\VMRC\\${SSOSERVER_FILENAME}"
SSOSERVER_LOGOUT_URL = "${SSOSERVER_URL}/dag/saml2/idp/SingleLogoutService.php?ReturnTo=${SSOSERVER_URL}/dag/module.php/duosecurity/logout.php"

# CLOUD SSO
SSO_CLOUD_USER = "cloudssotests@gmail.com"
SSO_CLOUD_PASSWORD = "$Q6FY>ub3XuN9WHH"

# SERVER:MAIL
EMAIL_SERVER = "192.168.2.88"
EMAIL_ACCOUNT = "jamie"
EMAIL_PASSWD = "Passw0rd!"
EMAIL_PORT = 25
EMAIL_DESTINATION = "jamie@AuthenticationServer"

# SERVER2:MAIL 192.168.3.8
EMAIL_SERVER2 = "192.168.3.8"
EMAIL_ACCOUNT2 = "jamie"
EMAIL_PASSWD2 = "Passw0rd!"
EMAIL_PORT2 = 25
EMAIL_DESTINATION2 = "jamie@AuthenticationServer2"

# SERVER:iPerf3
IPERF3_SERVER_IP = "iperf.astra.in.ua"	#You can get another server info if needed from https://iperf.fr/iperf-servers.php
IPERF3_SERVER_IPV4ORIPV6 = "-4"	#-4=ipv4
IPERF3_SERVER_PORT = "5204"

# Allow company to configure custom email service
"""
SMTP MAIL SERVER
"""
SMTP_SERVER =	"smtp.gmail.com"
SMTP_PORT =	"587"
SMTP_USER =	"autobuild.zpesystems"
SMTP_USER_PASSWORD	=	"ZPE5ystems!"
wrong_SMTP_USER =	"testing@zpecloud.com"
wrong_SMTP_USER_PASSWORD =	"password"

# SERVER:SSH KEYS AUTHENTICATION
SSHKEYS_SERVER = "192.168.3.8"
SSHKEYS_SERVER_USER = "root"
SSHKEYS_SERVER_PASSWORD = "Passw0rd!"

"""
    LIST OF VARIABLES RELATED TO VALIDATION
"""
WORD = "abcdefghij"
ONE_WORD = "a"
TWO_WORD = "ab"
THREE_WORD = "abc"
SEVEN_WORD = "abcdefg"
EIGHT_WORD = "abcdefgh"
ELEVEN_WORD = "abcdefghijk"

NEGATIVE_FLOAT_NUMBER = "-1.5"
NUMBER = 1234567890
ONE_NUMBER = 1
TWO_NUMBER = 12
THREE_NUMBER = 123
SEVEN_NUMBER = 1234567
EIGHT_NUMBER = 12345678
ELEVEN_NUMBER = 12345678901

POINTS = "!@-$%^*#()"
ONE_POINTS = "@"
TWO_POINTS = "!@"
THREE_POINTS = "!@-"
SEVEN_POINTS = "!@-$%^*"
EIGHT_POINTS = "!@-$%^*#"
NINE_POINTS = "!@-$%^*#("
ELEVEN_POINTS = "!@-$%^*#()+"
EXCEEDED = 12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
VALID_SECRET_SIZE = 1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456

WORD_AND_NUMBER = "abcde12345"
WORD_AND_POINTS = "abcde!@($-"
NUMBER_AND_WORD = "12345abcde"
NUMBER_AND_POINTS = "12345!@#$-"
POINTS_AND_NUMBER = "!@($-12345"
POINTS_AND_WORD = "!@($-abcde"

WORDS_SPACES = "this string has spaces"
WORDS_TWO_SPACES = " this string has spaces "
WORDS_WITH_BARS = "this is a str//ing"
WORDS_UNDERSCORE = "this_string_has_UNDERSCORE"
"""
    LIST OF VARIABLES RELATED TO LICENSE
"""
# LICENSE:ACCESS  (DON'T USE FOR MANUAL TEST)
ONE_HUNDRED_DEVICES_ACCESS_LICENSE = "NE0BD-W741B-9Z1RD-7K2DR"  # 2020-02-21
FIFTY_DEVICES_ACCESS_LICENSE = "M3P5W-8F58F-GGXVK-BTXIM"  # 2025-01-01
FIFTY_DEVICES_ACCESS_LICENSE_EXPIRED = "FS0SE-5FGGO-9UZ5V-33MGM"  # License Expired 2018-09-01
ONE_THOUSAND_DEVICES_ACCESS_LICENSE = "100ZJ-7N38D-4DKIX-ID13R"  # 2020-02-21
FIVE_THOUSAND_DEVICES_ACCESS_LICENSE = "FP1N0-E7U73-PODMA-9GX9R"  # 2020-02-20

# NGM:CLUSTER  (DON'T USE FOR MANUAL TEST)
FIVE_DEVICES_CLUSTERING_LICENSE_1_EXPIRED = "U23G3-RFDB3-ARPT2-F7SF3"  # License Expired 2020-01-01
FIVE_DEVICES_CLUSTERING_LICENSE_2 = "BVOX9-0O1DW-B51O9-0TXIM"  # 2025-01-01
FIVE_DEVICES_CLUSTERING_LICENSE_3_EXPIRED = "ANR14-E9UEZ-0XSAS-BKBWG"  # License Expired 2019-12-13
ONE_DEVICE_CLUSTERING_LICENSE = "8O5K5-DTA4Z-R1AX9-7WMOM"  # 2025-01-01
TEN_DEVICES_CLUSTERING_LICENSE = "XV8RP-AXRMR-22D51-5J48R"  # 2023-08-01
TEN_DEVICES_CLUSTERING_LICENSE_2 = "3ZHAI-AGA0S-8MG2G-SSSSR"  # 2023-08-01
TEN_DEVICES_CLUSTERING_LICENSE_GUI = "4TAF8-0A39E-TUETM-IMUER"  # 2024-03-01
TEN_DEVICES_CLUSTERING_LICENSE_GUI_2 = "EM3Z2-9TDX9-JSI73-XT1IR"  # 2024-03-01

# LICENSE RENEWAL BANNER  (DON'T USE FOR MANUAL TEST)
LICENSE_RENEWAL_REGEX = "Warning: A license key is expiring\\. Type: (Access|Clustering)\\. Serial Number: \\d+\\. Number of Licenses: \\d+\\. Expiration: \\d{4}-\\d{2}-\\d{2}\\. Remaining Days: \\d+\\."

# LICENSE DOCKER  (DON'T USE FOR MANUAL TEST)
LICENSE_VM_AND_DOCKER = "DZEKI-NUTG8-W5DT0-JX21R"  # Licence was created on 4/8/2021. This license is for 5 VMs/docker each. Valid for 90 days. You can reuse after 90 days if factory default.

"""
    LIST OF VARIABLES RELATED TO IPv4 AND IPv6
"""
#IPv6
IPV6_PREFIX = "2601:641:100:c400:caca::"

# IP:ALIASES
IP_ALIAS_IPV4 = "192.168.2.246"
IP_ALIAS_IPV6 = "2601:641:100:c400:cafe::0001"
IP_ALIAS_IPV6_2 = "2620:10D:C010:31::3:1"
IP_ALIAS_IPV6_3 = "2601:641:100:c400:caca::100"
IP_ALIAS_IPV6_4 = "2601:641:100:c400:caca::200"
# IP:NETFLOWD
EXECUTE_NETFLOWD_TESTS = False

# IP:NETFLOWD IPV4
EXECUTE_NETFLOWD_IPV4_TESTS = False
IP_NETFLOWD_IPV4_VALID = "192.168.2.201"
IP_NETFLOWD_IPV4_INVALID = "192.168.1.201"
IP_NETFLOWD_IPV4_DOMAIN = "192.168.6.1"
IP_NETFLOWD_IPV4_DOMAIN_INVALID = "192.168.1.202"
IP_NETFLOWD_DOMAIN_IPV4 = "192.168.6.1"  # Should remove and change test
IP_NETFLOWD_DOMAIN_INVALID = "192.168.1.202"  # Should remove and change test

# IP:NETFLOWD IPV6
EXECUTE_NETFLOWD_IPV6_TESTS = False
IP_NETFLOWD_IPV6_VALID = "2601:641:100:c400::1"
IP_NETFLOWD_IPV6_INVALID = "2601:641:100:c400::9"
IP_NETFLOWD_IPV6_DOMAIN = "2601:641:100:c400::2"
IP_NETFLOWD_IPV6_DOMAIN_INVALID = "2601:641:100:t400::2"
IP_NETFLOWD_DOMAIN_IPV6 = "2601:641:100:c400::2"  # Should remove and change test

"""
    LIST OF VARIABLES RELATED TO PLUGINS
"""
WEBSESSION_PLUGIN_FILEPATH = "/home/qausers/Documents/web_forwarder/extension_1_2_0_0.crx"

"""
    LIST OF VARIABLES RELATED TO DEVICES
"""
PROTECTED_DEVICE_TYPES = "local_serial,usb_serialB,usb_sensor,usb_kvm,usb_device"

"""
    LIST OF VARIABLES SYSTEM VALUES SNMP OIDS
"""
SYSTEM_VALUES_OIDS = [
	".1.3.6.1.4.1.42518.4.2.1.1.1.1.0",
	".1.3.6.1.4.1.42518.4.2.1.1.1.2.0",
	".1.3.6.1.4.1.42518.4.2.1.1.1.3.0",
	".1.3.6.1.4.1.42518.4.2.1.1.1.4.0",
	".1.3.6.1.4.1.42518.4.2.1.1.1.5.0",
	".1.3.6.1.4.1.42518.4.2.1.1.1.6.0",
	".1.3.6.1.4.1.42518.4.2.1.1.1.7.0",
	".1.3.6.1.4.1.42518.4.2.1.1.1.8.0",
	".1.3.6.1.4.1.42518.4.2.1.1.1.9.0",
	".1.3.6.1.4.1.42518.4.2.1.1.1.10.0",
	".1.3.6.1.4.1.42518.4.2.1.1.1.11.0",
	".1.3.6.1.4.1.42518.4.2.1.1.1.12.0",
	".1.3.6.1.4.1.42518.4.2.1.1.1.13.0",
	".1.3.6.1.4.1.42518.4.2.1.1.1.14.0",
	".1.3.6.1.4.1.42518.4.2.1.1.1.15.0",
	".1.3.6.1.4.1.42518.4.2.1.1.1.16.0",
	".1.3.6.1.4.1.42518.4.2.1.1.1.17.0",
	".1.3.6.1.4.1.42518.4.2.1.1.1.18.0",
	".1.3.6.1.4.1.42518.4.2.1.1.1.19.0"
]
SYSTEM_VALUES_OID_TREE = ".1.3.6.1.4.1.42518.4.2.1.1.1"

"""
    LIST OF VARIABLES RELATED TO JENKINS VMs
"""

#Jenkins VMs credentials
CLI_JENKINS_VM_USERNAME= "jenkins"
CLI_JENKINS_VM_PASSWORD= "Passw0rd"
GUI_JENKINS_VM_USERNAME= "qausers"
GUI_JENKINS_VM_PASSWORD= "QAusers!"
CLOUD_JENKINS_VM_USERNAME= "qausers"
CLOUD_JENKINS_VM_PASSWORD= "QAusers!"