#!/usr/bin/python
# -*- coding: utf-8 -*-

#5.6 CLI Nightly
HOMEPAGE = "https://192.168.6.47"
HOST = "192.168.6.47"
BROWSER = "Chrome"
TESTSYSTEMIP = "192.168.6.47"
GATEWAY = "192.168.6.1"
NETWORK = "192.168.6.0/24"
NGVERSION = "5.6"
NFSPATH = "/nfs/nodegrid_47"
SSO_ENTITY_ID = "NodegridAutomationNightly5.6"

CONSOLE_PORT_TTYS = "ttyS16"
CONSOLE_PORT_NAME = "NSR6.47_v5.6_CLI_Nightly_ttyS16"
CONSOLE_ACCESS_IP = "192.168.6.150"

'''
	PHYSICAL CONFIGURATION
'''
HAS_HOSTSHARED = True
SWITCH_NETPORT_HOST_INTERFACE1 = "netS2-1"
SWITCH_NETPORT_SHARED_INTERFACE1 = "netS1"
SWITCH_NETPORT_HOST_INTERFACE2 = "netS2-2"
SWITCH_NETPORT_SHARED_INTERFACE2 = "netS2"
SWITCH_SFP_HOST_INTERFACE1 = "sfp0"
SWITCH_SFP_SHARED_INTERFACE1 = "netS3"

# /31 network
SLASH31_AVAILABLE = True
SLASH31_USES_SFP = True
SLASH31_HOST_INTERFACE = SWITCH_SFP_HOST_INTERFACE1
SLASH31_SHARED_INTERFACE = SWITCH_SFP_SHARED_INTERFACE1

# ACL
ACL_AVAILABLE = True

# 802.1x
STD802_1x_AVAILABLE = True

# VLAN
VLAN_AVAILABLE = True

#IP PASSTHROUGH
IP_PASSTHROUGH_AVAILABLE = True
IP_PASSTHROUGH_SHARED_USES_SWITCH = True
IP_PASSTHROUGH_HOST_INTERFACE = SWITCH_NETPORT_HOST_INTERFACE1
IP_PASSTHROUGH_SHARED_INTERFACE = SWITCH_NETPORT_SHARED_INTERFACE1

#SNMP ON SWITCH INTERFACES
SWITCH_SNMP_SFP_AVAILABLE = True
SWITCH_SNMP_NETPORT_AVAILABLE = True