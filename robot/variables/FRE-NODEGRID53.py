#!/usr/bin/python
# -*- coding: utf-8 -*-
#CHANGE THE IP TO THE VM IP
HOMEPAGE = "https://192.168.2.53"
HOST = "192.168.2.53"
BROWSER = "Chrome"
TESTSYSTEMIP = "192.168.2.53"
GATEWAY = "192.168.2.254"
NETWORK = "192.168.2.0/24"
NGVERSION = "4.1"
NFSPATH = "/nfs/nodegrid_53"
