#!/usr/bin/python
# -*- coding: utf-8 -*-

HOMEPAGE = "https://192.168.3.38"
HOST = "192.168.3.38"
TESTSYSTEMIP = "192.168.3.38"
GATEWAY = "192.168.2.254"
NETWORK = "192.168.2.0/24"
BROWSER = "Chrome"
NGVERSION = "5.0"