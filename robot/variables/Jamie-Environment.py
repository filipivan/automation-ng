#!/usr/bin/python
# -*- coding: utf-8 -*-

VALIDATE_IPV6GWD = True  # Where is it used?

"""
    LIST OF VARIABLES RELATED TO ROBOT
"""
# ROBOT:DIRECTORY TO STORE SCREENSHOTS
SCREENSHOTDIR = "screenshots"

"""
    LIST OF VARIABLES RELATED TO NGM
"""
# NGM:DEFAULT USERNAME
USERNAME = "admin"
DEFAULT_USERNAME = "admin"
DEFAULT_PASSWORD = "admin"
# NGM:QA PASSWORD
QA_PASSWORD = ".ZPE5ystems!2020"
# NGM:DEFAULT HOSTNAME
HOSTNAME_NODEGRID = "nodegrid"
# NGM:INTERFACES
SESSION_TYPES = ["cli", "web", "none"]
# NGM:PROTOCOLS TO ACCESS NODEGRID
SESSION_MODE = ["https", "ssh", "telnet", "http", "console"]
# NGM:NETWORK INTERFACES (When without GSM, Wifi...)
INTERFACES = ["eth0", "eth1", "loopback", "loopback0", "main", "virbr0"]
# NGM:METRIC
METRIC = ["0", "90", "256", "1024"]
# NGM:TYPE OF EVENTS
EVENTCATEGOREY = ["system event", "aaa event", "device event", "logging event"]
# NGM:FORMAT FOR LOGS
DISCOVERY_LOG_FORMAT = "[A-Z]{1}[a-z]{2}\s+[A-Z]{1}[a-z]{2}\s+\d{1,2}\s+\d{2}[:]\d{2}[:]\d{2}\s+\d{4}\s+[a-z0-9\.\(\)]{1,16}\s+.{1,64}\s+.{1,16}\s+.{1,16}"
# Origial
# DISCOVERY_LOG_FORMAT = "[A-Z]{1}[a-z]{2}\s+[A-Z]{1}[a-z]{2}\s+\d{1,2}\s+\d{2}[:]\d{2}[:]\d{2}\s+\d{4}\s+\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\s+.{1,64}\s+.{1,16}\s+.{1,16}"

"""
    LIST OF VARIABLES RELATED TO DEVICES
"""
# GENERIC NAMES
DUMMY_DEVICE_CONSOLE_NAME = "dummy_dev-ice.console"
DUMMY_DEVICE_CONSOLE_NAME_ACCESS = "dummy_dev-ice.access"
DUMMY_PDU_SERVERTECH_NAME = "DUMMY_SERVERTECH"
PDU_SERVERTECH_CLONE = "pdu_servertech_ssh"
MKS_DEVICE = "Win10-Automation"

# DEVICE:PDU
# PDU CPI is not in this IP...
PDU_CPI = {"name": "pdu_cpi", "type": "pdu_cpi", "ip": "192.168.2.243", "username": "", "password": ""}
PDU_GEIST = {"name": "pdu_geist", "ip": "76.79.48.119", "type": "pdu_geist", "username": "GeistDemo", "password": "powerup"}
PDU = {"name": "servertech", "ip": "192.168.2.111", "type": "pdu_servertech", "username": "admn", "password": "admn"}
PDU_NAME = "servertech"
PDU_IP = "192.168.2.111"
PDU_TYPE = "pdu_servertech"
PDU_USERNAME = "admn"
PDU_PASSWORD = "admn"
SSH_OPTIONS_PDU_SERVERTECH = "\"-o HostKeyAlgorithms=ssh-rsa,ssh-dss -o KexAlgorithms=diffie-hellman-group1-sha1 -o Ciphers=aes128-cbc,3des-cbc,aes256-ctr,aes192-ctr,aes128-ctr,aes192-cbc,aes256-cbc\""

# DEVICE:CONSOLE SERVER (NG - NSC)
EXECUTE_CSN_TESTS = True
CONSOLE_SERVER_NG_NAME = "console_server_ng"
CONSOLE_SERVER_NG_IP = "192.168.2.53"
CONSOLE_SERVER_NG_USERNAME = "admin"
CONSOLE_SERVER_NG_PASSWORD = "admin"
CONSOLE_SERVER_NG_AWAITED_STRING_ON_CONNECT_CMD = "ogin"

# DEVICE:HP iLO
EXECUTE_ILO_TESTS = False
ILO_NAME = ""
ILO_IP = ""
ILO_USERNAME = ""
ILO_PASSWORD = ""
ILO_AWAITED_STRING_ON_CONNECT_CMD = ""

# DEVICE:Dell iDRAC
EXECUTE_IDRAC_TESTS = False
IDRAC_NAME = "iDRAC_R330_192.168.2.28"
IDRAC_IP = "192.168.2.28"
IDRAC_USERNAME = "root"
IDRAC_PASSWORD = "calvin"
# IDRAC_PASSWORD = "NodeGridD3m0Passw0rd"
IDRAC_AWAITED_STRING_ON_CONNECT_CMD = "zpe-PowerEdge-R330 login:"

# DEVICE:Dell iDRAC9
# EXECUTE_IDRAC9_TESTS = False
IDRAC9_NAME = "iDRAC9_192.168.2.226"
IDRAC9_IP = "192.168.2.226"
IDRAC9_USERNAME = "root"
IDRAC9_PASSWORD = "root"
IDRAC9_AWAITED_STRING_ON_CONNECT_CMD = "->"

# DEVICE:IBM IMM
EXECUTE_IMM_TESTS = False
IMM_NAME = "IMM_192.168.2.147"
IMM_IP = "192.168.2.147"  # IMM IP has changed back to 192.168.2.147 from 192.168.2.203
IMM_USERNAME = "USERID"
IMM_PASSWORD = "PASSW0RD"
IMM_AWAITED_STRING_ON_CONNECT_CMD = ""  # No OS, prompt will shows imm prompt "system>"

# DEVICE:SUPERMICRO 2.99 is not working
#KVM_DEVICE = {"name": "supermicro", "ip": "192.168.2.99", "username": "ADMIN", "password": ".Sup3rM1cr0!", "type": "ipmi_2.0", "pymod": "supermicro.py"}
# DEVICE:SUPERMICRO 2.36 old supermicro
KVM_DEVICE = {"name": "supermicro", "ip": "192.168.2.36", "username": "ADMIN", "password": ".Sup3rM1cr0!", "type": "ipmi_2.0", "pymod": "supermicro.py"}

# DEVICE:ACS6000
ACS6000_IP = "192.168.2.151"
ACS6000_USERNAME = "admin"
ACS6000_PASSWORD = "avocent"
ACS6000_IP_RANGE_START = "192.168.2.150"
ACS6000_IP_RANGE_END = "192.168.2.155"

"""
    LIST OF VARIABLES RELATED TO CONNECTIONS
"""
GSM_CELLULAR_CONNECTION = "automation_gsm_connection"
GSM_ETHERNET_INTERFACE = "cdc-wdm0"
GSM_TYPE = "mobile_broadband_gsm"
GSM_ACCESS_POINT_NAME = "i2gold"

"""
    LIST OF VARIABLES RELATED TO VIRTUALIZATION
"""

# VM MANAGER:ESXI (VM manager with VMs to be discover)
VMMANAGER = {"ip": "192.168.2.225", "username": "Administrator@zpesystems.com", "password": ".VMware!123"}

# VM MANAGER:ESXI not working
#VMMANAGER = {"ip": "192.168.2.213", "username": "root", "password": ".vmware!"}
VMMANAGER_ESXI = {"ip": "192.168.2.224", "username": "root", "password": ".vmware!"}

# VM:Windows (VM available in the VMMANAGER to launch MKS - Template Device used to discover VMs)
MKS_TEMPLATE = {"name": "mks", "ip": "192.168.2.122", "username": "dummy", "password": "dummy", "type": "virtual_console_vmware"}

"""
    LIST OF VARIABLES RELATED TO SERVERS
"""
# SERVER:FTP
FTPSERVER_PRESENT = True
FTPSERVER_IP = "192.168.2.201"
FTPSERVER_PORT = "21"
FTPSERVER_URL = "ftp://" + FTPSERVER_IP + ":" + FTPSERVER_PORT
FTPSERVER_USER = "anonymous"
FTPSERVER_PASSWORD = "anonymous"

# SERVER:FTP VMRC
FTPSERVER2_PRESENT = True
FTPSERVER2_IP = "192.168.2.88"
FTPSERVER2_PORT = "21"
FTPSERVER2_URL = "ftp://" + FTPSERVER2_IP + ":" + FTPSERVER2_PORT
FTPSERVER2_USER = "ftpuser"
FTPSERVER2_PASSWORD = "ftpuser"
#FTPSERVER2_VMRC10_PATH = "/test_files/VMware-Remote-Console-10.0.4-11818843.x86_64.bundle"
FTPSERVER2_VMRC10_PATH = "/files/VMware-Remote-Console-10.0.4-11818843.x86_64.bundle"
#FTPSERVER2_VMRC_PATH = "/test_files/VMware-Remote-Console.bundle"
FTPSERVER2_VMRC_PATH = "/files/VMware-Remote-Console.bundle"

# SERVER:FTP DEAD
FTPSERVER3_PRESENT = False
FTPSERVER3_IP = "192.168.2.166"
FTPSERVER3_PORT = "21"
FTPSERVER3_URL = "ftp://" + FTPSERVER3_IP + ":" + FTPSERVER3_PORT
FTPSERVER3_USER = "ftpuser"
FTPSERVER3_PASSWORD = "ftpuser"
#FTPSERVER3_VMRC10_PATH = "/test_files/VMware-Remote-Console-10.0.4-11818843.x86_64.bundle"
FTPSERVER3_VMRC10_PATH = "/files/VMware-Remote-Console-10.0.4-11818843.x86_64.bundle"
FTPSERVER3_VMRC_PATH = "/VMware-Remote-Console.bundle"

# SERVER:NFS /nfs is the nfs path directory
NFSSERVERPRESENT = "Yes"
NFSSERVER = "192.168.2.88"
NFSSERVERUSER = "root"
NFSSERVERPASSWORD = "root"

# SERVER:SNMP/SNMPTRAP
SNMPSERVERPRESENT = True
SNMPSERVER = "192.168.2.88"
SNMPSERVERUSER = "snmpuser"
SNMPSERVERPASSWORD = "snmpuser"
SNMPUDPPORT = "162"
SNMPTCPPORT = "161"
SNMPCOMMUNITY = "public"
SNMPPATH = "/var/log"
SNMPFILE = "nodegrid-snmp.log"
SNMPMD5DESUSER = "mymd5desuser"
SNMPMD5AESUSER = "mymd5aesuser"
SNMPSHADESUSER = "myshadesuser"
SNMPSHAAESUSER = "mysheaesuser"
SNMPAUTHONLYUSER = "myauthonlyuser"
SNMPNOAUTHNOPRIVUSER = "mynoauthnoprivuser"
SNMPMD5PASSWORD = "mymd5password"
SNMPDESPASSWORD = "mydespassword"
SNMPSHAPASSWORD = "myshapassword"
SNMPAESPASSWORD = "myaespassword"

# SERVER:SYSLOG
SYSLOGSERVERPRESENT = True
SYSLOGSERVER = "192.168.2.88"
SYSLOGSERVERUSER = "root"
SYSLOGSERVERPASSWORD = "root"
EVENTFACILITY = "log_local_3"
DATALOGFACILITY = "log_local_4"
SYSLOGPATH = "/var/log"
SYSLOGEVENTSFILE = "nodegrid-events.log"
SYSLOGDATALOGFILE = "nodegrid-datatlog.log"

# SERVER:PERLE
PERLE_SCS_PRESENT = True
PERLE_SCS_SERVER_IP = "192.168.2.251"
PERLE_SCS_PORT = "21"
PERLE_SCS_SERVER_HOMEPAGE = "http://" + PERLE_SCS_SERVER_IP + "/manage.cgi/login"
PERLE_SCS_SERVER_USERNAME = "admin"
PERLE_SCS_SERVER_PASSWORD = "superuser"

# SERVER:AD (Authentication) Active Directory Details
VALIDATE_AD_USER_AUTHENTICATION = True
VALIDATE_LDAP_USER_AUTHENTICATION = True
AD_SERVER = "192.168.2.227"
AD_BASE_DOMAIN = "dc=zpesystems,dc=local"
AD_BROWSE_USER_DN = "zpe@zpesystems.local"
AD_BROWSE_USER = "zpe"
AD_BROWSE_PASSWORD = "Passw0rd"
AD_ADMIN_GROUP = "testgroup"
AD_ADMIN_USER = "testuser3"
AD_ADMIN_PASSWORD = "Passw0rd"
AD_INVALID_USER = "InvalidUser"
AD_GROUP_ATRRIB = "memberOf"
AD_USER_ATRRIB = "sAMAccountName"
AD_GROUP_BASE = "CN=users,DC=ZPESYSTEMS,DC=local"

# SERVER:LDAP
LDAPSERVER_PRESENT = True
LDAPSERVER = "192.168.2.88"
LDAPSERVER_USER = "sAMAccountName"
LDAPSERVER_SECURE = "off"

# SERVER:TACACS (Authentication)
TACACS_SERVER = "192.168.2.88"
TACACSSERVER_IP = "192.168.2.88"
TACACSSERVER_URL = "https://" + TACACSSERVER_IP
TACACSSERVER_USER1 = "tacacs1"
TACACSSERVER_PASSWORD1 = "tacacs1"

# SERVER:RADIUS (Authentication)
RADIUSSERVER_PRESENT = True
RADIUSSERVER = "192.168.2.88"
RADIUSSERVER_SECRET = "secret"

# SERVER:RSA
RSASERVER_PRESENT = True
RSASERVER_NAME = "rsa_method"
RSASERVER = "rsa.zpesystems.com,192.168.2.229"
RSASERVER_REST_URL = "https://rsa-am.zpesystems.com:5555/mfa/v1_1/authn"
RSASERVER_CLIENT_KEY = "08wd3zndor551a518i65taze0fgh6o0t9q3mc5dn6xz5mqur9xw54826vi8xa962"
RSASERVER_CLIENT_ID = "rsa.zpesystems.com"
RSASERVER_READ_TIMEOUT = "120"	#	30 <= read_timeout <= 240
RSASERVER_CONNECT_TIMEOUT = "20"	#	10 <= connect_timeout <= 120
RSASERVER_MAX_RETRIES = "3"	#	1 <= max_retries <= 5
RSASERVER_REPLICAS = "rsa.zpesystems.com,192.168.2.229"
RSASERVER_REPLICAS_PORTS = "rsa.zpesystems.com:4444,rsa.zpesystems.com:3333"
RSASERVER_POLICY_ID = "low-mfa-policy"
RSASERVER_TENANT_ID = "rsaready"
RSASERVER_ROOTCA = "/test_files/RootCA.pem"  # ftp 88

# SERVER:SSO - DUE
SSOSERVER_PRESENT = True
SSOSERVER = "192.168.2.190"
SSOSERVER_URL = "https://192.168.2.190"
SSOSERVER_USER = "zpe"
SSOSERVER_PASSWORD = "Passw0rd"
SSOSERVER_DAG_PATH = "/dag/"
SSOSERVER_SERVICE_PATH = "/dag/saml2/idp/SSOService.php"
SSOSERVER_ISSUER_PATH = "/dag/saml2/idp/metadata.php"
#SSOSERVER_CRT_PATH = "/test_files/saml.crt"  # ftp 88
SSOSERVER_CRT_PATH = "/files/saml.crt"  # ftp 88
#SSOSERVER_XML_PATH = "/test_files/saml.xml"  # ftp 88
SSOSERVER_XML_PATH = "/files/saml.xml"  # ftp 88
SSOSERVER_FILENAME = "dag.crt"
SSOSERVER_FILEPATH = "C:\\VMRC\\${SSOSERVER_FILENAME}"
SSOSERVER_LOGOUT_URL = "${SSOSERVER_URL}/dag/saml2/idp/SingleLogoutService.php?ReturnTo=${SSOSERVER_URL}/dag/module.php/duosecurity/logout.php"

# SERVER:MAIL
EMAIL_SERVER = "192.168.2.88"
EMAIL_ACCOUNT = "jamie"
EMAIL_PASSWD = "jamie"
EMAIL_PORT = 25
EMAIL_DESTINATION = "jamie@AuthenticationServer"

"""
    LIST OF VARIABLES RELATED TO VALIDATION
"""
WORD = "abcdefghij"
ONE_WORD = "a"
TWO_WORD = "ab"
THREE_WORD = "abc"
SEVEN_WORD = "abcdefg"
EIGHT_WORD = "abcdefgh"
ELEVEN_WORD = "abcdefghijk"

NUMBER = 1234567890
ONE_NUMBER = 1
TWO_NUMBER = 12
THREE_NUMBER = 123
SEVEN_NUMBER = 1234567
EIGHT_NUMBER = 12345678
ELEVEN_NUMBER = 12345678901

POINTS = "!@-$%^*#()"
ONE_POINTS = "@"
TWO_POINTS = "!@"
THREE_POINTS = "!@-"
SEVEN_POINTS = "!@-$%^*"
EIGHT_POINTS = "!@-$%^*#"
NINE_POINTS = "!@-$%^*#("
ELEVEN_POINTS = "!@-$%^*#()+"
EXCEEDED = 12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890

WORD_AND_NUMBER = "abcde12345"
WORD_AND_POINTS = "abcde!@($-"
NUMBER_AND_WORD = "12345abcde"
NUMBER_AND_POINTS = "12345!@#$-"
POINTS_AND_NUMBER = "!@($-12345"
POINTS_AND_WORD = "!@($-abcde"

"""
    LIST OF VARIABLES RELATED TO LICENSE
"""
# LICENSE:ACCESS
ONE_HUNDRED_DEVICES_ACCESS_LICENSE = "NE0BD-W741B-9Z1RD-7K2DR"  # 2020-02-21
FIFTY_DEVICES_ACCESS_LICENSE = "GM0A5-455B0-HRWB5-XW5VR"  # 2022-01-01
FIFTY_DEVICES_ACCESS_LICENSE_EXPIRED = "FS0SE-5FGGO-9UZ5V-33MGM"  # License Expired 2018-09-01
ONE_THOUSAND_DEVICES_ACCESS_LICENSE = "100ZJ-7N38D-4DKIX-ID13R"  # 2020-02-21
FIVE_THOUSAND_DEVICES_ACCESS_LICENSE = "FP1N0-E7U73-PODMA-9GX9R"  # 2020-02-20

# NGM:CLUSTER
FIVE_DEVICES_CLUSTERING_LICENSE_1_EXPIRED = "U23G3-RFDB3-ARPT2-F7SF3"  # License Expired 2020-01-01
FIVE_DEVICES_CLUSTERING_LICENSE_2 = "F2FK4-990II-KU84K-0W5VR"  # 2022-01-01
FIVE_DEVICES_CLUSTERING_LICENSE_3_EXPIRED = "ANR14-E9UEZ-0XSAS-BKBWG"  # License Expired 2019-12-13
ONE_DEVICE_CLUSTERING_LICENSE = "4XU03-DJSPG-B74J7-ETZBR"  # 2022-01-01

"""
    LIST OF VARIABLES RELATED TO IPv4 AND IPv6
"""
#IPv6
IPV6_PREFIX = "2601:641:100:c400:caca::"

# IP:ALIASES
IP_ALIAS_IPV4 = "192.168.2.246"
IP_ALIAS_IPV6 = "2601:641:100:c400:cafe::0001"
IP_ALIAS_IPV6_2 = "2620:10D:C010:31::3:1"
IP_ALIAS_IPV6_3 = "2601:641:100:c400:caca::100"
IP_ALIAS_IPV6_4 = "2601:641:100:c400:caca::200"
# IP:NETFLOWD
EXECUTE_NETFLOWD_TESTS = False

# IP:NETFLOWD IPV4
EXECUTE_NETFLOWD_IPV4_TESTS = False
IP_NETFLOWD_IPV4_VALID = "192.168.2.201"
IP_NETFLOWD_IPV4_INVALID = "192.168.1.201"
IP_NETFLOWD_IPV4_DOMAIN = "192.168.6.1"
IP_NETFLOWD_IPV4_DOMAIN_INVALID = "192.168.1.202"
IP_NETFLOWD_DOMAIN_IPV4 = "192.168.6.1"  # Should remove and change test
IP_NETFLOWD_DOMAIN_INVALID = "192.168.1.202"  # Should remove and change test

# IP:NETFLOWD IPV6
EXECUTE_NETFLOWD_IPV6_TESTS = False
IP_NETFLOWD_IPV6_VALID = "2601:641:100:c400::1"
IP_NETFLOWD_IPV6_INVALID = "2601:641:100:c400::9"
IP_NETFLOWD_IPV6_DOMAIN = "2601:641:100:c400::2"
IP_NETFLOWD_IPV6_DOMAIN_INVALID = "2601:641:100:t400::2"
IP_NETFLOWD_DOMAIN_IPV6 = "2601:641:100:c400::2"  # Should remove and change test

"""
    LIST OF VARIABLES RELATED TO PLUGINS
"""
WEBSESSION_PLUGIN_FILEPATH = "C:/Users/QAUsers/Documents/web_forwarder/extension_1_2_0_0.crx"