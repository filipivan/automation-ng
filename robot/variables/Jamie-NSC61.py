#!/usr/bin/python
# -*- coding: utf-8 -*-

HOMEPAGE = "https://192.168.2.176"
HOST = "192.168.2.176"
TESTSYSTEMIP = "192.168.2.176"
GATEWAY = "192.168.2.254"
NETWORK = "192.168.2.0/24"
BROWSER = "Chrome"
NGVERSION = "4.2"