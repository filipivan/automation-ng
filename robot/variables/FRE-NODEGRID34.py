#!/usr/bin/python
# -*- coding: utf-8 -*-

HOMEPAGE = "https://192.168.2.34"
HOST = "192.168.2.34"
BROWSER = "Chrome"
TESTSYSTEMIP = "192.168.2.34"
GATEWAY = "192.168.2.254"
NETWORK = "192.168.2.0/24"
NGVERSION = "3.2"
NFSPATH = "/nfs/nodegrid_34"