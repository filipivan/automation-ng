#!/usr/bin/python
# -*- coding: utf-8 -*-

#HOMEPAGE = "https://lkngm04.emea.zpesystems.local"
#HOST = "lkngm02.emea.zpesystems.local"
HOMEPAGE = "https://192.168.56.101"
HOST = "192.168.56.101"
BROWSER = "Chrome"
TESTSYSTEMIP = "192.168.56.101"
GATEWAY = "192.168.56.254"
NETWORK = "192.168.56.0/24"
CLIENTIP = "192.168.56.1"
SESSION_TYPES = ["cli", "web", "none"]
SESSION_MODE = ["https", "ssh", "telnet", "http", "console"]
NGVERSION = "4.0"
VALIDATE_LDAP_USER_AUTHENTICATION = False
VALIDATE_AD_USER_AUTHENTICATION = False
VALIDATE_IPV6GWD = True
INTERFACES =  ["eth0","eth1","loopback","loopback0","main","virbr0"]
METRIC = ["0","90","256","1024"]
EVENTCATEGOREY  =   ["system event","aaa event","device event","logging event"]
SCREENSHOTDIR   =   ".\results\screenshots"
DISCOVERY_LOG_FORMAT = "[A-Z]{1}[a-z]{2}\s+[A-Z]{1}[a-z]{2}\s+\d{1,2}\s+\d{2}[:]\d{2}[:]\d{2}\s+\d{4}\s+\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\s+.{1,64}\s+.{1,16}\s+.{1,16}"
DUMMY_PDU_SERVERTECH_NAME = "DUMMY_SERVERTECH"
LICENSE =     "FS0SE-5FGGO-9UZ5V-33MGM"
EXPLICENSE =   "NE0BD-W741B-9Z1RD-7K2DR"

EXECUTE_ILO_TESTS = False
ILO_NAME = ""
ILO_IP = ""
ILO_USERNAME = ""
ILO_PASSWORD = ""
ILO_AWAITED_STRING_ON_CONNECT_CMD = ""

EXECUTE_IDRAC_TESTS = False
IDRAC_NAME = ""
IDRAC_IP = ""
IDRAC_USERNAME = ""
IDRAC_PASSWORD = ""
IDRAC_AWAITED_STRING_ON_CONNECT_CMD = ""
