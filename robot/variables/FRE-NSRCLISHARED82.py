#!/usr/bin/python
# -*- coding: utf-8 -*-

#5.2 CLI SHARED Network
HOMEPAGESHARED = "https://192.168.6.82"
HOSTSHARED = "192.168.6.82"

CONSOLE_PORT_TTYS = "ttyS12"
CONSOLE_PORT_NAME = "NSR6.82_v5.2-CLI-GUI-Shared_Automation_ttyS12"
CONSOLE_ACCESS_IP = "192.168.6.150"