#!/usr/bin/python
# -*- coding: utf-8 -*-

SESSION_TYPES = ["cli", "web", "none"]
SESSION_MODE = ["https", "ssh", "telnet", "http", "console"]
VALIDATE_LDAP_USER_AUTHENTICATION = True
VALIDATE_AD_USER_AUTHENTICATION = True
VALIDATE_IPV6GWD = True
INTERFACES =  ["eth0","eth1","loopback","loopback0","main","virbr0"]
METRIC = ["0","90","256","1024"]
EVENTCATEGOREY  =   ["system event","aaa event","device event","logging event"]
SCREENSHOTDIR   =   "screenshots"
DISCOVERY_LOG_FORMAT = "[A-Z]{1}[a-z]{2}\s+[A-Z]{1}[a-z]{2}\s+\d{1,2}\s+\d{2}[:]\d{2}[:]\d{2}\s+\d{4}\s+[a-z0-9\.\(\)]{1,16}\s+.{1,64}\s+.{1,16}\s+.{1,16}"
#Origial
# DISCOVERY_LOG_FORMAT = "[A-Z]{1}[a-z]{2}\s+[A-Z]{1}[a-z]{2}\s+\d{1,2}\s+\d{2}[:]\d{2}[:]\d{2}\s+\d{4}\s+\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\s+.{1,64}\s+.{1,16}\s+.{1,16}"
# the pdu variable is used to add a pdu device and test the outlet table in
PDU = { "name" : "servertech", "ip": "192.168.2.111", "type" : "pdu_servertech","username" : "admn", "password" : "admn"}
SSH_OPTIONS_PDU_SERVERTECH = "\"-o HostKeyAlgorithms=ssh-rsa,ssh-dss -o KexAlgorithms=diffie-hellman-group1-sha1 -o Ciphers=aes128-cbc,3des-cbc,aes256-ctr,aes192-ctr,aes128-ctr,aes192-cbc,aes256-cbc\""
DUMMY_DEVICE_CONSOLE_NAME = "dummy_dev-ice.console"
DUMMY_DEVICE_CONSOLE_NAME_ACCESS = "dummy_dev-ice.access"
NFSSERVERPRESENT = "Yes"
NFSSERVERUSER = "root"
NFSSERVERPASSWORD = "root"
NFSSERVER = "192.168.2.88"
SNMPSERVERPRESENT = "Yes"
SNMPSERVER = "192.168.2.88"
SNMPSERVERUSER = "snmpuser"
SNMPSERVERPASSWORD = "snmpuser"
SNMPUDPPORT = "162"
SNMPTCPPORT = "161"
SNMPCOMMUNITY = "public"
SNMPPATH = "/var/log"
SNMPFILE = "nodegrid-snmp.log"
SNMPMD5DESUSER = "mymd5desuser"
SNMPMD5AESUSER = "mymd5aesuser"
SNMPSHADESUSER = "myshadesuser"
SNMPSHAAESUSER = "mysheaesuser"
SNMPAUTHONLYUSER = "myauthonlyuser"
SNMPNOAUTHNOPRIVUSER = "mynoauthnoprivuser"
SNMPMD5PASSWORD = "mymd5password"
SNMPDESPASSWORD = "mydespassword"
SNMPSHAPASSWORD = "myshapassword"
SNMPAESPASSWORD = "myaespassword"
SYSLOGSERVERPRESENT = "Yes"
SYSLOGSERVER = "192.168.2.88"
SYSLOGSERVERUSER = "root"
SYSLOGSERVERPASSWORD = "root"
EVENTFACILITY = "log_local_3"
DATALOGFACILITY = "log_local_4"
SYSLOGPATH = "/var/log"
SYSLOGEVENTSFILE = "nodegrid-events.log"
SYSLOGDATALOGFILE = "nodegrid-datatlog.log"
DUMMY_PDU_SERVERTECH_NAME = "DUMMY_SERVERTECH"

EXECUTE_ILO_TESTS = False
ILO_NAME = ""
ILO_IP = ""
ILO_USERNAME = ""
ILO_PASSWORD = ""
ILO_AWAITED_STRING_ON_CONNECT_CMD = ""

#EXECUTE_IDRAC_TESTS = False
EXECUTE_IDRAC_TESTS = False
IDRAC_NAME = "iDRAC_R330_192.168.2.28"
IDRAC_IP = "192.168.2.28"
IDRAC_USERNAME = "root"
IDRAC_PASSWORD = "calvin"
#IDRAC_PASSWORD = "NodeGridD3m0Passw0rd"
IDRAC_AWAITED_STRING_ON_CONNECT_CMD = "zpe-PowerEdge-R330 login:"

EXECUTE_IMM_TESTS = False
IMM_NAME = "IMM_192.168.2.147"
IMM_IP = "192.168.2.147"        #IMM IP has changed back to 192.168.2.147 from 192.168.2.203
IMM_USERNAME = "USERID"
IMM_PASSWORD = "PASSW0RD"
IMM_AWAITED_STRING_ON_CONNECT_CMD = ""   #No OS, prompt will shows imm prompt "system>"

# device used in access table tests
KVM_DEVICE = { "name" : "supermicro", "ip": "192.168.2.99", "username" : "ADMIN", "password" : ".Sup3rM1cr0!", "type" : "ipmi_2.0", "pymod": "supermicro.py"}
# VM manager with VMs to be discover
#VMMANAGER = {"ip" : "192.168.2.213", "username": "root", "password" : ".vmware!"}

#New VM Managers
VMMANAGER = {"ip" : "192.168.2.225", "username": "Administrator@zpesystems.com", "password" : ".VMware!123"}
# VM available in the VMMANAGER to launch MKS
MKS_DEVICE = "Win10-Automation"
MKS_TEMPLATE = { "name" : "mks", "ip": "192.168.2.122", "username" : "dummy", "password" : "dummy", "type" : "virtual_console_vmware"} # Template Device used to discover VMs

#Active Directory Details
AD_SERVER = "192.168.2.227"
AD_BASE_DOMAIN = "dc=zpesystems,dc=local"
AD_BROWSE_USER_DN = "zpe@zpesystems.local"
AD_BROWSE_USER = "zpe"
AD_BROWSE_PASSWORD = "Passw0rd"
AD_ADMIN_GROUP = "testgroup"
AD_ADMIN_USER = "testuser3"
AD_ADMIN_PASSWORD = "Passw0rd"
AD_INVALID_USER = "InvalidUser"
AD_GROUP_ATRRIB = "memberOf"
AD_USER_ATRRIB = "sAMAccountName"
AD_GROUP_BASE = "CN=users,DC=ZPESYSTEMS,DC=local"

EXECUTE_NETFLOWD_TESTS = False
IP_NETFLOWD_IPV4_VALID = "192.168.2.201"
IP_NETFLOWD_IPV6_VALID = "2601:641:100:c400::1"

IP_NETFLOWD_IPV4_INVALID = "192.168.1.201"
IP_NETFLOWD_IPV6_INVALID = "2601:641:100:c400::9"

IP_NETFLOWD_DOMAIN_IPV4 = "192.168.6.1"
IP_NETFLOWD_DOMAIN_IPV6 = "2601:641:100:c400::2"
IP_NETFLOWD_DOMAIN_INVALID = "192.168.1.202"

# device used in access table tests
KVM_DEVICE = { "name" : "supermicro", "ip": "192.168.2.99", "username" : "ADMIN", "password" : ".Sup3rM1cr0!", "type" : "ipmi_2.0", "pymod": "supermicro.py"}
# VM manager with VMs to be discover
VMMANAGER = {"ip" : "192.168.2.213", "username": "root", "password" : ".vmware!"}
# VM available in the VMMANAGER to launch MKS
MKS_DEVICE = "Win10-Automation"
MKS_TEMPLATE = { "name" : "mks", "ip": "192.168.2.122", "username" : "dummy", "password" : "dummy", "type" : "virtual_console_vmware"} # Template Device used to discover VMs

USERNAME = "admin"
#5000 devices license
HUNDRED_DEVICES_LICENSE = "FP1N0-E7U73-PODMA-9GX9R"
FIVE_THOUSAND_DEVICES_LICENSE = "FP1N0-E7U73-PODMA-9GX9R"

# Checking Values Tests #
USERNAME = "admin"
HOSTNAME_NODEGRID = "nodegrid"
PDU_SERVERTECH_CLONE = "pdu_servertech_ssh"

WORD = "abcdefghij"
ONE_WORD = "a"
TWO_WORD = "ab"
THREE_WORD = "abc"
SEVEN_WORD = "abcdefg"
EIGHT_WORD = "abcdefgh"
ELEVEN_WORD = "abcdefghijk"

NUMBER = 1234567890
ONE_NUMBER = 1
TWO_NUMBER = 12
THREE_NUMBER = 123
SEVEN_NUMBER = 1234567
EIGHT_NUMBER = 12345678
ELEVEN_NUMBER = 12345678901

POINTS = "!@-$%^*#()"
ONE_POINTS = "@"
TWO_POINTS = "!@"
THREE_POINTS = "!@-"
SEVEN_POINTS = "!@-$%^*"
EIGHT_POINTS = "!@-$%^*#"
NINE_POINTS = "!@-$%^*#("
ELEVEN_POINTS = "!@-$%^*#()+"
EXCEEDED = 12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890

WORD_AND_NUMBER = "abcde12345"
WORD_AND_POINTS = "abcde!@($-"
NUMBER_AND_WORD = "12345abcde"
NUMBER_AND_POINTS = "12345!@#$-"
POINTS_AND_NUMBER = "!@($-12345"
POINTS_AND_WORD = "!@($-abcde"

"""
    LIST OF VARIABLES RELATED TO LICENSES
"""
LICENSE = "GM0A5-455B0-HRWB5-XW5VR"       #new license
EXPLICENSE = "FS0SE-5FGGO-9UZ5V-33MGM"    #license expired, need to ask Livio for a new license
#EXPLICENSE =   "NE0BD-W741B-9Z1RD-7K2DR"
# Clustering
FIVE_DEVICES_CLUSTERING_LICENSE = "U23G3-RFDB3-ARPT2-F7SF3"      # 2020-01-01
FIVE_DEVICES_CLUSTERING_LICENSE_2 = "F2FK4-990II-KU84K-0W5VR"    # 2022-01-01
FIVE_DEVICES_CLUSTERING_LICENSE_3 = "ANR14-E9UEZ-0XSAS-BKBWG"    # 2019-12-13
ONE_DEVICE_CLUSTERING_LICENSE = "4XU03-DJSPG-B74J7-ETZBR"        # 2022-01-01

"""
    LIST OF VARIABLES RELATED TO IPv4 AND IPv6
"""
# IP Alias
IP_ALIAS = "192.168.2.246"
#IP Alias IPv6
IP_ALIAS_IPv6 = "2601:641:100:c400:cafe:"