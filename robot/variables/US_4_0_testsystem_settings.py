#!/usr/bin/python
# -*- coding: utf-8 -*-

VALIDATE_LDAP_USER_AUTHENTICATION = True
VALIDATE_AD_USER_AUTHENTICATION = True
VALIDATE_IPV6GWD = True
LICENSE =     "FS0SE-5FGGO-9UZ5V-33MGM"
EXPLICENSE =   "NE0BD-W741B-9Z1RD-7K2DR"
INTERFACES =  ["eth0","eth1","loopback","loopback0","main","virbr0"]
METRIC = ["0","90","256","1024"]
EVENTCATEGOREY  =   ["system event","aaa event","device event","logging event"]
SCREENSHOTDIR   =   "screenshots"
DISCOVERY_LOG_FORMAT = "[A-Z]{1}[a-z]{2}\s+[A-Z]{1}[a-z]{2}\s+\d{1,2}\s+\d{2}[:]\d{2}[:]\d{2}\s+\d{4}\s+[a-z0-9\.\(\)]{1,16}\s+.{1,64}\s+.{1,16}\s+.{1,16}"
PDU = { "name" : "servertech", "ip": "66.214.208.191", "type" : "pdu_servertech","username" : "admn", "password" : "admn"}
DUMMY_DEVICE_CONSOLE_NAME = "dummy_dev-ice.console"
DUMMY_DEVICE_CONSOLE_NAME_ACCESS = "dummy_dev-ice.access"
NFSSERVERPRESENT = "Yes"
NFSSERVERUSER = "root"
NFSSERVERPASSWORD = "root"
NFSSERVER = "192.168.2.88"
NFSPATH = "/nfs/nodegrid_53"
SNMPSERVERPRESENT = "Yes"
SNMPSERVER = "192.168.2.88"
SNMPSERVERUSER = "snmpuser"
SNMPSERVERPASSWORD = "snmpuser"
SNMPUDPPORT = "162"
SNMPTCPPORT = "161"
SNMPCOMMUNITY = "public"
SNMPPATH = "/var/log"
SNMPFILE = "nodegrid-snmp.log"
SNMPMD5DESUSER = "mymd5desuser"
SNMPMD5AESUSER = "mymd5aesuser"
SNMPSHADESUSER = "myshadesuser"
SNMPSHAAESUSER = "mysheaesuser"
SNMPAUTHONLYUSER = "myauthonlyuser"
SNMPNOAUTHNOPRIVUSER = "mynoauthnoprivuser"
SNMPMD5PASSWORD = "mymd5password"
SNMPDESPASSWORD = "mydespassword"
SNMPSHAPASSWORD = "myshapassword"
SNMPAESPASSWORD = "myaespassword"
SYSLOGSERVERPRESENT = "Yes"
SYSLOGSERVER = "192.168.2.88"
SYSLOGSERVERUSER = "root"
SYSLOGSERVERPASSWORD = "root"
EVENTFACILITY = "log_local_3"
DATALOGFACILITY = "log_local_4"
SYSLOGPATH = "/var/log"
SYSLOGEVENTSFILE = "nodegrid-events.log"
SYSLOGDATALOGFILE = "nodegrid-datatlog.log"
DUMMY_PDU_SERVERTECH_NAME = "DUMMY_SERVERTECH"

EXECUTE_ILO_TESTS = False
ILO_NAME = ""
ILO_IP = ""
ILO_USERNAME = ""
ILO_PASSWORD = ""
ILO_AWAITED_STRING_ON_CONNECT_CMD = ""

#EXECUTE_IDRAC_TESTS = False
EXECUTE_IDRAC_TESTS = False
IDRAC_NAME = "iDRAC_R330_170.178.141.146"
IDRAC_IP = "170.178.141.146"
IDRAC_USERNAME = "root"
IDRAC_PASSWORD = "NodeGridD3m0Passw0rd"
IDRAC_AWAITED_STRING_ON_CONNECT_CMD = "zpe-PowerEdge-R330 login:"

EXECUTE_IMM_TESTS = False
IMM_NAME = "IMM_170.178.141.147"
IMM_IP = "170.178.141.147"
IMM_USERNAME = "USERID"
IMM_PASSWORD = "PASSW0RD"
IMM_AWAITED_STRING_ON_CONNECT_CMD = ""   #No OS, prompt will shows imm prompt "system>"

EXECUTE_NETFLOWD_TESTS = False
HOST = "192.168.15.91"
IP_NETFLOWD_IPV4_VALID = "192.168.2.201"
IP_NETFLOWD_IPV6_VALID = "2601:641:100:c400::1"

IP_NETFLOWD_IPV4_INVALID = "192.168.1.201"
IP_NETFLOWD_IPV6_INVALID = "2601:641:100:c400::9"

IP_NETFLOWD_DOMAIN_IPV4 = "192.168.6.1"
IP_NETFLOWD_DOMAIN_IPV6 = "2601:641:100:c400::2"
IP_NETFLOWD_DOMAIN_INVALID = "192.168.1.202"

# device used in access table tests
KVM_DEVICE = { "name" : "supermicro", "ip": "192.168.2.99", "username" : "ADMIN", "password" : ".Sup3rM1cr0!", "type" : "ipmi_2.0", "pymod": "supermicro.py"}
# VM manager with VMs to be discover
VMMANAGER = {"ip" : "192.168.2.213", "username": "root", "password" : ".vmware!"}
# VM available in the VMMANAGER to launch MKS
MKS_DEVICE = "Win10-Automation"
MKS_TEMPLATE = { "name" : "mks", "ip": "192.168.2.122", "username" : "dummy", "password" : "dummy", "type" : "virtual_console_vmware"} # Template Device used to discover VMs
