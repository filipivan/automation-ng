#!/usr/bin/python
# -*- coding: utf-8 -*-

#5.2 GUI Nightly
HOMEPAGE = "https://192.168.2.103"
HOST = "192.168.2.103"
BROWSER = "Chrome"
TESTSYSTEMIP = "192.168.2.103"
GATEWAY = "192.168.2.1"
NETWORK = "192.168.2.0/24"
NGVERSION = "4.2"
NFSPATH = "/nfs/nodegrid_103"
SSO_ENTITY_ID = "NodegridAutomationNightly4.x"

HOMEPAGEPEER = "https://192.168.2.105"
HOSTPEER = "192.168.2.105"
NGPEERVERSION = "4.2"

HOMEPAGESHARED = "https://192.168.2.105"
HOSTSHARED = "192.168.2.105"

CLIENTIP = "192.168.3.38"

'''
	PHYSICAL CONFIGURATION
'''
HAS_HOSTSHARED = False

SWITCH_NETPORT_HOST_INTERFACE1 = "netS2-1"
SWITCH_NETPORT_SHARED_INTERFACE1 = "netS3-15"
SWITCH_NETPORT_HOST_INTERFACE2 = "netS2-2"
SWITCH_NETPORT_SHARED_INTERFACE2 = "netS3-16"
SWITCH_SFP_HOST_INTERFACE1 = "sfp0"
SWITCH_SFP_SHARED_INTERFACE1 = "sfp0"

# /31 network
SLASH31_AVAILABLE = False
SLASH31_USES_SFP = False
SLASH31_HOST_INTERFACE = SWITCH_SFP_HOST_INTERFACE1
SLASH31_SHARED_INTERFACE = SWITCH_SFP_SHARED_INTERFACE1

# ACL
ACL_AVAILABLE = False

# 802.1x
STD802_1x_AVAILABLE = False

# VLAN
VLAN_AVAILABLE = False

# IP PASSTHROUGH
IP_PASSTHROUGH_AVAILABLE = False
IP_PASSTHROUGH_SHARED_USES_SWITCH = False
IP_PASSTHROUGH_HOST_INTERFACE = SWITCH_NETPORT_HOST_INTERFACE1
IP_PASSTHROUGH_SHARED_INTERFACE = SWITCH_NETPORT_SHARED_INTERFACE1

#SNMP ON SWITCH INTERFACES
SWITCH_SNMP_SFP_AVAILABLE = False
SWITCH_SNMP_NETPORT_AVAILABLE = False