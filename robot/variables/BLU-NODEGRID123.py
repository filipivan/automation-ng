#!/usr/bin/python
# -*- coding: utf-8 -*-

HOMEPAGE = "https://192.168.16.123"
HOST = "192.168.16.123"
BROWSER = "Firefox"
TESTSYSTEMIP = "192.168.16.161"
GATEWAY = "192.168.16.1"
NETWORK = "192.168.16.0/14"
NGVERSION = "4.1"
APIVERSION = "v1"
