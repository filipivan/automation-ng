#!/usr/bin/python
# -*- coding: utf-8 -*-

#3.2.52 CLI Official
HOMEPAGE = "https://192.168.6.31"
HOST = "192.168.6.31"
BROWSER = "Chrome"
TESTSYSTEMIP = "192.168.6.31"
#GATEWAY = "192.168.2.254"
GATEWAY = "192.168.6.1" #192.168.6.1-GW-routerIP or 192.168.2.254
#NETWORK = "192.168.2.0/24"
NETWORK = "192.168.6.0/24"
NGVERSION = "3.2"
NFSPATH = "/nfs/nodegrid_31"

CONSOLE_PORT_TTYS = "ttyS8"
CONSOLE_PORT_NAME = "NSC6.31_v3.2.52-CLI-Official_Automation_ttyS8"
CONSOLE_ACCESS_IP = "192.168.6.150"

'''
	PHYSICAL CONFIGURATION
'''
HAS_HOSTSHARED = False