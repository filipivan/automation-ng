#!/usr/bin/python
# -*- coding: utf-8 -*-

HOMEPAGE = "https://192.168.2.83"
HOST = "192.168.2.83"
BROWSER = "Firefox"
TESTSYSTEMIP = "192.168.2.83"
GATEWAY = "192.168.2.254"
NETWORK = "192.168.2.0/24"
NGVERSION = "4.2"   #branch version
NFSPATH = "/nfs/nodegrid_11"
CHIPSETA_A = "aldrin"
CHIPSETA_X = "xcat"
INTERFACEA3_3 = "netS3-3"
INTERFACEA3_4 = "netS3-4"


HOMEPAGEB = "https://192.168.2.230"
HOSTB = "192.168.2.230"
BROWSERB = "Firefox"
TESTSYSTEMIPB = "192.168.2.230"
GATEWAYB = "192.168.2.254"
NETWORKB = "192.168.2.0/24"
NGVERSIONB = "4.2"   #branch version
NFSPATHB = "/nfs/nodegrid_11"
CHIPSETB_A = "aldrin"
CHIPSETB_X = "xcat"
INTERFACEB3_3 = "netS3-3"
INTERFACEB3_4 = "netS3-4"
