#!/usr/bin/python
# -*- coding: utf-8 -*-

HOMEPAGE = "https://192.168.2.90"
HOST = "192.168.2.90"
BROWSER = "Firefox"
# TESTSYSTEMIP = "192.168.16.173"
# GATEWAY = "192.168.16.1"
# NETWORK = "192.168.16.0/14"
# CLIENTIP = "192.168.16.155"
NGVERSION = "5.8"

HOSTPEER = "192.168.2.93"
HOMEPAGEPEER = "https://192.168.2.93"
HOSTNAME_NODEGRID = "nodegrid-test"


# HOMEPAGE = "https://192.168.16.162"
# HOST = "192.168.16.162"
# BROWSER = "Firefox"
# TESTSYSTEMIP = "192.168.16.162"
# GATEWAY = "192.168.16.1"
# NETWORK = "192.168.16.0/14"
# CLIENTIP = "192.168.16.162"
# NGVERSION = "3.2"

# '''
# NETWORK = "192.168.16.0/14"
# CLIENTIP = "192.168.16.168"
# SESSION_TYPES = ["cli", "web", "none"]
# SESSION_MODE = ["https", "ssh", "telnet", "http", "console"]
# VALIDATE_LDAP_USER_AUTHENTICATION = True
# VALIDATE_AD_USER_AUTHENTICATION = True
# VALIDATE_IPV6GWD = True
# LICENSE = "NE0BD-W741B-9Z1RD-7K2DR"
# INTERFACES =  ["eth0","eth1","loopback","loopback0","main","virbr0"]
# METRIC = ["0","90","256","1024"]
# EVENTCATEGOREY = ["system event","aaa event","device event","logging event"]
# SCREENSHOTDIR = "C:\\Reports\\GUI\\Windows\\Firefox\\Screenshots"
# DISCOVERY_LOG_FORMAT = "[A-Z]{1}[a-z]{2}\s[A-Z]{1}[a-z]{2}\s\d{2}\s\d{2}[:]\d{2}[:]\d{2}\s\d{4}\s\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\s+.{1,64}\s+.{1,16}\s+.{1,16}"
# # the pdu variable is used to add a pdu device and test the outlet table in
# PDU = { "name" : "pdu_servertech", "ip": "66.214.208.190", "type" : "pdu_servertech", "username" : "admn", "password" : "admn"}
# DUMMY_DEVICE_CONSOLE_NAME = "dummy_dev-ice.console"
# DUMMY_DEVICE_CONSOLE_NAME_ACCESS = "dummy_dev-ice.access"
# NFSSERVERPRESENT = "No"
# SNMPSERVERPRESENT = "No"
# SYSLOGSERVERPRESENT = "No"
# API_VERSION="v1"
#
# EXECUTE_ILO_TESTS = False
# ILO_NAME = "ilo_remote"
# ILO_IP = "192.168.2.150"
# ILO_USERNAME = "Administrator"
# ILO_PASSWORD = "Adm1n1strat0r"
# ILO_AWAITED_STRING_ON_CONNECT_CMD = "mother login:"
#
# EXECUTE_IDRAC_TESTS = False
# IDRAC_NAME = "idrac6_remote"
# IDRAC_IP = "104.36.2.28"
# IDRAC_USERNAME = "root"
# IDRAC_PASSWORD = "NodeGridD3m0Passw0rd"
# IDRAC_AWAITED_STRING_ON_CONNECT_CMD = "zpe-PowerEdge-R330 login:"
#
# EXECUTE_IMM_TESTS = False
# IMM_NAME="imm_remote"
# IMM_IP = "170.178.2.146"
# IMM_USERNAME = "USERID"
# IMM_PASSWORD = "PASSW0RD"
# IMM_AWAITED_STRING_ON_CONNECT_CMD = "system>"
# '''
