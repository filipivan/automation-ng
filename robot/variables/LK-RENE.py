#!/usr/bin/python
# -*- coding: utf-8 -*-

HOMEPAGE = "https://192.168.2.183"
HOST = "192.168.2.183"
TESTSYSTEMIP = "192.168.2.183"
GATEWAY = "192.168.2.254"
NETWORK = "192.168.2.0/24"
BROWSER = "Chrome"
NGVERSION = "3.2"