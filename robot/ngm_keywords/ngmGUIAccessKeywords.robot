*** Keywords ***

GUI::Access::Open
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Access tab
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Access Tab is open and all elements are accessible
	Set Selenium Timeout    15s
	Wait Until Element Is Visible	//*[@id='main_menu']/li[1]/a
	GUI::Basic::Click Element	//*[@id='main_menu']/li[1]/a
	GUI::Basic::Spinner Should Be Invisible

GUI:Access::Open Table tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Access::Table tab
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Table Tab is open and all elements are accessible
	GUI::Access::Open
	Wait Until Element Is Visible	//*[contains(@class, 'submenu')]/ul/li[1]/a
	GUI::Basic::Click Element	//*[contains(@class, 'submenu')]/ul/li[1]/a
	Wait Until Element Is Visible	//*[contains(@class, 'widget-title')]/h4
	${WIDGET_TILE}=	Get Text	jquery=.widget-title h4
	${WIDGET_TILE}=	Get Text	//*[contains(@class, 'widget-title')]/h4
	Should Be Equal	${WIDGET_TILE}	Access :: Table

GUI:Access::Open Tree tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Access::Tree tab
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Tree Tab is open and all elements are accessible
	GUI::Access::Open
	Wait Until Element Is Visible	jquery=.submenu ul li:nth-child(2) a
	GUI::Basic::Click Element	//*[contains(@class, 'submenu')]/ul/li[2]/a
	Wait Until Element Is Visible	jquery=.widget-title h4
	${WIDGET_TILE}=	Get Text	jquery=.widget-title h4
	Should Be Equal	${WIDGET_TILE}	Access :: Tree

GUI:Access::Open Node tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Access::Node tab
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Node Tab is open and all elements are accessible
	GUI::Access::Open
	Wait Until Element Is Visible	jquery=.submenu ul li:nth-child(3) a
	GUI::Basic::Click Element	//*[contains(@class, 'submenu')]/ul/li[3]/a
	Wait Until Element Is Visible	jquery=.widget-title h4
	${WIDGET_TILE}=	Get Text	jquery=.widget-title h4
	Should Be Equal	${WIDGET_TILE}	Access :: Node

GUI:Access::Open Map tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Access::Map tab
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Map Tab is open and all elements are accessible
	GUI::Access::Open
	Wait Until Element Is Visible	jquery=.submenu ul li:nth-child(4) a
	GUI::Basic::Click Element	//*[contains(@class, 'submenu')]/ul/li[4]/a
	Wait Until Element Is Visible	jquery=.widget-title h4
	${WIDGET_TILE}=	Get Text	jquery=.widget-title h4
	Should Be Equal	${WIDGET_TILE}	Access :: Map

GUI:Access::Open Image tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Access::Image tab
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Image Tab is open and all elements are accessible
	GUI::Access::Open
	Wait Until Element Is Visible	jquery=.submenu ul li:nth-child(5) a
	GUI::Basic::Click Element	//*[contains(@class, 'submenu')]/ul/li[5]/a
	Wait Until Element Is Visible	jquery=.widget-title h4
	${WIDGET_TILE}=	Get Text	jquery=.widget-title h4
	Should Be Equal	${WIDGET_TILE}	Access :: Image

GUI::Access::Direct URL
    [Documentation]   Open a new browser and direct access the device
    ...   == ARGUMENTS ==
    ...   -   DEVICE_NAME = If the argument is not empty, than a managed device console will be accessed
    [Arguments]   ${USERNAME}=${DEFAULT_USERNAME}   ${PASSWORD}=${DEFAULT_PASSWORD}   ${DEVICE_NAME}=${EMPTY}   ${PAGE}=${HOMEPAGE}
    Run Keyword If   "${DEVICE_NAME}" == "${EMPTY}"
    ...    GUI::Basic::Open Nodegrid   PAGE=${HOMEPAGE}/direct
    ...    ELSE   GUI::Basic::Open Nodegrid   PAGE=${HOMEPAGE}/direct/${DEVICE_NAME}/console
    GUI::Basic::Direct Login  ${USERNAME}   ${PASSWORD}
#    GUI::Basic::Wait Until Window Handle Exists    NEW
#    GUI::Basic::Handle Browser Certificate
    Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists    ${DEVICE_NAME} - Console
    ...	ELSE	GUI:Basic::Wait Until Window Exists    ${DEVICE_NAME}

    ${FOUND_ALERT}=    Run Keyword And Return Status    Alert Should Be Present
    Run Keyword If    ${FOUND_ALERT}  Switch Window    title=ZPE Systems®, Inc.
    Run Keyword If    ${FOUND_ALERT}    Fail    No alert should be visible after ${DEVICE_NAME} window launch
#    TTYD terminal elements
    Wait Until Page Contains Element    xpath=//*[@id='termwindow']
    Select Frame    xpath=//*[@id='termwindow']
    Wait Until Keyword Succeeds    1m    1s     Execute JavaScript  term.selectAll();
    Current Frame Should Contain    terminal-container
    Current Frame Should Contain    xterm-viewport
    Current Frame Should Contain    xterm-screen
#    TTYD xterm.js elements
    Wait Until Page Contains Element    css=.xterm-helpers
    Wait Until Page Contains Element    css=.xterm-helper-textarea
    Wait Until Page Contains Element    css=.xterm-text-layer
    Wait Until Page Contains Element    css=.xterm-selection-layer
    Wait Until Page Contains Element    css=.xterm-link-layer
    Wait Until Page Contains Element    css=.xterm-cursor-layer
    Press Keys  None    RETURN

GUI::Access::Get Device Type in Table Page
	[Arguments]	${DEVICE_NAME}
	[Documentation]	Go to Access > Table and return the type of the ${DEVICE_NAME} by clicking in device name
	...	and getting the device TYPE from the modal table
	...	== ARGUMENTS ==
	...	-   DEVICE_NAME = Target device name
	...	== EXPECTED RESULT ==
	...	Returns the target device TYPE
	GUI:Access::Open Table tab
	Click Element	xpath=//span[.="${DEVICE_NAME}"]
	Wait Until Page Contains Element	xpath=//tbody[@id='tbody']
	${DEVICE_TYPE}=	Get Text	xpath=//tbody[@id='tbody']/tr[3]/td[2]
	Press Keys	None	ESC
	[Return]	${DEVICE_TYPE}

GUI:Access::Device
	[Arguments]	${JQUERY}	${DEVICE_NAME}	${BUTTON}
	[Timeout]	2 minutes
	${DEVICE_TYPE}=	GUI::Access::Get Device Type in Table Page	${DEVICE_NAME}

	GUI:Access::Open Table tab
	${MAIN_WINDOW_TITLE}=	Get Title
	Click Element	jquery=${JQUERY}
	Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists	${DEVICE_NAME} - ${BUTTON}
	...	ELSE	GUI:Basic::Wait Until Window Exists	${DEVICE_NAME}
	${FOUND_ALERT}=	Run Keyword And Return Status	Alert Should Be Present
	Run Keyword If	${FOUND_ALERT}	Fail	No alert should be visible after ${DEVICE_NAME} window launch
	Wait Until Page Contains Element	xpath=//*[@id='targetParameters']
#	buttons: text input and clipboard removed from console window button - ttyd_template.html
	#Run Keyword If	'${DEVICE_TYPE}' != 'device_console'	Wait Until Page Contains Element	xpath=//*[@id='txt']
	#Run Keyword If	'${DEVICE_TYPE}' != 'device_console'	Wait Until Page Contains Element	xpath=//*[@id='cpb']
	Wait Until Page Contains Element	xpath=//*[@id='inf']
	Wait Until Page Contains Element	xpath=//*[@id='fs']
	Wait Until Page Contains Element	xpath=//*[@id='cw']
	Wait Until Page Contains Element	xpath=//*[@id='expand_button']
	${POWER_BUTTON_DEVICES}=	Create List	ilo	imm	drac	idrac6	ipmi_1.5	ipmi_2.0	ilom	cimc_ucs
	...	netapp	virtual_console_kvm	kvm_aten	kvm_raritan	usb	cimc_ucs2
	${SHOULD_CONTAIN_POWER_BUTTONS}=	Run Keyword And Return Status	Should Contain	${POWER_BUTTON_DEVICES}	${DEVICE_TYPE}
	Run Keyword If	${SHOULD_CONTAIN_POWER_BUTTONS}
	...	Run Keywords
	...	Wait Until Page Contains Element	xpath=//*[@id='po']
	...	AND	Wait Until Page Contains Element	xpath=//*[@id='pon']
	...	AND	Wait Until Page Contains Element	xpath=//*[@id='cyc']
	...	AND	Wait Until Page Contains Element	xpath=//*[@id='shut']
	...	AND	Wait Until Page Contains Element	xpath=//*[@id='pst']
	Close Window
	Switch Window	title=${MAIN_WINDOW_TITLE}

GUI:Access::Device Check Terminal
	[Arguments]	${JQUERY}	${DEVICE_NAME}
	[Documentation]	Goes to the Access::Table page and opens the given device console
	...	== ARGUMENTS ==
	...	-   JQUERY = Console button of the given target device
	...	-   DEVICE_NAME = Target device name
	...	== EXPECTED RESULT ==
	...	Access Tab is open and click the console button of the given target device validating the TERMINAL page elements
	GUI:Access::Open Table tab
	${MAIN_WINDOW_TITLE}=	Get Title
	Click Element	jquery=${JQUERY}
	Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists	${DEVICE_NAME} - Console
	...	ELSE	GUI:Basic::Wait Until Window Exists	${DEVICE_NAME}
	${FOUND_ALERT}=	Run Keyword And Return Status	Alert Should Be Present
	Run Keyword If	${FOUND_ALERT} and '${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists	${DEVICE_NAME} - Console
	...	ELSE	Run Keyword If	${FOUND_ALERT}	GUI:Basic::Wait Until Window Exists	${DEVICE_NAME}
	Run Keyword If	${FOUND_ALERT}	Fail	No alert should be visible after ${DEVICE_NAME} window launch
#	TTYD terminal elements
	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
	Select Frame	xpath=//*[@id='termwindow']
	Wait Until Keyword Succeeds    1m    1s     Execute JavaScript  term.selectAll();
	Current Frame Should Contain	terminal-container
	Current Frame Should Contain	xterm-viewport
	Current Frame Should Contain	xterm-screen
#	TTYD xterm.js elements
	Wait Until Page Contains Element	css=.xterm-helpers
	Wait Until Page Contains Element	css=.xterm-helper-textarea
	Wait Until Page Contains Element	css=.xterm-text-layer
	Wait Until Page Contains Element	css=.xterm-selection-layer
	Wait Until Page Contains Element	css=.xterm-link-layer
	Wait Until Page Contains Element	css=.xterm-cursor-layer
#	Resizing terminal window
	${PAGE_WIDTH}	${PAGE_HEIGHT}=	Get Window Size
	Set Window Size	800	600
	Sleep	1
	Set Window Size	${PAGE_WIDTH}	${PAGE_HEIGHT}

	Close Window
	Switch Window	title=${MAIN_WINDOW_TITLE}

GUI:Access::Device Access Console
	[Arguments]	${JQUERY}	${DEVICE_NAME}
	[Documentation]	Goes to the Access::Table page and opens the given device console and keeps
	...	the console window open
	...	== ARGUMENTS ==
	...	-   JQUERY = Console button of the given target device
	...	-   DEVICE_NAME = Target device name
	...	== EXPECTED RESULT ==
	...	Access Tab is open and click the console button of the given target device validating the CONSOLE and then,
	...	funcionalities and keeps the console window open

	GUI:Access::Open Table tab
	Click Element	jquery=${JQUERY}
	Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists	${DEVICE_NAME} - Console
	...	ELSE	GUI:Basic::Wait Until Window Exists	${DEVICE_NAME}
	${FOUND_ALERT}=	Run Keyword And Return Status	Alert Should Be Present
#	Run Keyword If	${FOUND_ALERT}  Switch Window	title=ZPE Systems®, Inc.
	Run Keyword If	${FOUND_ALERT} and '${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists	${DEVICE_NAME} - Console
	...	ELSE	Run Keyword If	${FOUND_ALERT}	GUI:Basic::Wait Until Window Exists	${DEVICE_NAME}
	Run Keyword If	${FOUND_ALERT}	Fail	No alert should be visible after ${DEVICE_NAME} window launch
#	TTYD terminal elements
	Wait Until Page Contains Element	xpath=//*[@id='termwindow']
	Select Frame	xpath=//*[@id='termwindow']
	Wait Until Keyword Succeeds    1m    1s     Execute JavaScript  term.selectAll();
	Current Frame Should Contain	terminal-container
	Current Frame Should Contain	xterm-viewport
	Current Frame Should Contain	xterm-screen
#	TTYD xterm.js elements
	Wait Until Page Contains Element	css=.xterm-helpers
	Wait Until Page Contains Element	css=.xterm-helper-textarea
	Wait Until Page Contains Element	css=.xterm-text-layer
	Wait Until Page Contains Element	css=.xterm-selection-layer
	Wait Until Page Contains Element	css=.xterm-link-layer
	Wait Until Page Contains Element	css=.xterm-cursor-layer
	Press Keys 	None	RETURN

GUI:Access::Table::Access Device Console
    [Arguments]   ${DEVICE_NAME}   ${HOSTNAME}=${HOSTNAME_NODEGRID}
    [Documentation]   Goes to the Access::Table page and opens the given device console and keeps
    ...               the console window opened
    ...    == ARGUMENTS ==
    ...    -   DEVICE_NAME = Name of the device to access the console
    ...    -   HOSTNAME = Hostname (Works for clustering) that manages the device (default is 'nodegrid')
    GUI:Access::Open Table tab

    ${EXPANDADED}=   Get Element Attribute   //div[@id[contains(., "|${HOSTNAME}")]]  class
    Run Keyword If  "${EXPANDADED}" == "peer"    Run Keywords
    ...   Click Element    //div[@id[contains(., "|${HOSTNAME}")]]//img[@id="expandstate"]   AND
    ...   Wait Until Page Contains Element    //div[@id[contains(., "|${HOSTNAME}")]][@class="peer expanded"]

    Wait Until Element Is Visible   //div[@id[contains(.,"|${HOSTNAME}")]]//tr[@id[contains(.,"|${DEVICE_NAME}")]]//a[text()[contains(.,"Console")]]
    Click Element    //div[@id[contains(.,"|${HOSTNAME}")]]//tr[@id[contains(.,"|${DEVICE_NAME}")]]//a[text()[contains(.,"Console")]]
#    GUI::Basic::Wait Until Window Handle Exists    NEW
#    GUI::Basic::Handle Browser Certificate
    Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists    ${DEVICE_NAME} - Console
    ...	ELSE	GUI:Basic::Wait Until Window Exists    ${DEVICE_NAME}

    ${FOUND_ALERT}=    Run Keyword And Return Status    Alert Should Be Present
    Run Keyword If    ${FOUND_ALERT} and '${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists    ${DEVICE_NAME} - Console
    ...	ELSE	Run Keyword If	${FOUND_ALERT}	GUI:Basic::Wait Until Window Exists    ${DEVICE_NAME}
    Run Keyword If    ${FOUND_ALERT}    Fail    No alert should be visible after ${DEVICE_NAME} window launch
#    TTYD terminal elements
    Wait Until Page Contains Element    xpath=//*[@id='termwindow']
    Select Frame    xpath=//*[@id='termwindow']
    Wait Until Keyword Succeeds    1m    1s     Execute JavaScript  term.selectAll();
    Current Frame Should Contain    terminal-container
    Current Frame Should Contain    xterm-viewport
    Current Frame Should Contain    xterm-screen
#    TTYD xterm.js elements
    Wait Until Page Contains Element    css=.xterm-helpers
    Wait Until Page Contains Element    css=.xterm-helper-textarea
    Wait Until Page Contains Element    css=.xterm-text-layer
    Wait Until Page Contains Element    css=.xterm-selection-layer
    Wait Until Page Contains Element    css=.xterm-link-layer
    Wait Until Page Contains Element    css=.xterm-cursor-layer
    Press Keys  None    RETURN

GUI:Access::Tree::Access Device Console
    [Arguments]   ${DEVICE_NAME}   ${HOSTNAME}=${HOSTNAME_NODEGRID}
    [Documentation]   Goes to the Access::Table page and opens the given device console and keeps
    ...               the console window opened
    ...    == ARGUMENTS ==
    ...    -   DEVICE_NAME = Name of the device to access the console
    ...    -   HOSTNAME = Hostname (Works for clustering) that manages the device (default is 'nodegrid')
    GUI:Access::Open Tree tab

    ${EXPANDADED}=   Get Element Attribute   //div[@rowid[contains(., "|${HOSTNAME}")]]/label/img[@id="view"]  state
    Run Keyword If  "${EXPANDADED}" == "off"    Run Keywords
    ...   Click Element    //div[@rowid[contains(., "|${HOSTNAME}")]]/label/img[@id="view"]   AND
    ...   Wait Until Page Contains Element    //div[@rowid[contains(., "|${HOSTNAME}")]]/label/img[@id="view"][@state="on"]

    Wait Until Element Is Visible   //div[@parent[contains(.,"|${HOSTNAME}")]]//span[@id[contains(.,"|${DEVICE_NAME}")]]//a[text()="Console"]
    Click Element    //div[@parent[contains(.,"|${HOSTNAME}")]]//span[@id[contains(.,"|${DEVICE_NAME}")]]//a[text()="Console"]
#    GUI::Basic::Wait Until Window Handle Exists    NEW
#    GUI::Basic::Handle Browser Certificate
    Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists    ${DEVICE_NAME} - Console
    ...	ELSE	GUI:Basic::Wait Until Window Exists    ${DEVICE_NAME}

    ${FOUND_ALERT}=    Run Keyword And Return Status    Alert Should Be Present
    Run Keyword If    ${FOUND_ALERT}  Switch Window    title=ZPE Systems®, Inc.
    Run Keyword If    ${FOUND_ALERT}    Fail    No alert should be visible after ${DEVICE_NAME} window launch
#    TTYD terminal elements
    Wait Until Page Contains Element    xpath=//*[@id='termwindow']
    Select Frame    xpath=//*[@id='termwindow']
    Wait Until Keyword Succeeds    1m    1s     Execute JavaScript  term.selectAll();
    Current Frame Should Contain    terminal-container
    Current Frame Should Contain    xterm-viewport
    Current Frame Should Contain    xterm-screen

    Wait Until Page Contains Element    css=.xterm-helpers
    Wait Until Page Contains Element    css=.xterm-helper-textarea
    Wait Until Page Contains Element    css=.xterm-text-layer
    Wait Until Page Contains Element    css=.xterm-selection-layer
    Wait Until Page Contains Element    css=.xterm-link-layer
    Wait Until Page Contains Element    css=.xterm-cursor-layer
    Press Keys  None    RETURN

GUI:Access::Console Command Output
	[Arguments]	${COMMAND}
	[Documentation]	Input the given command into the ttyd terminal and retrieve the output after prompt lines
	...	== REQUIREMENTS ==
	...	Console should be on cli mode only. It does not work on shell mode or as root user
	...	== ARGUMENTS ==
	...	-   COMMAND = Command to be executed
	...	== EXPECTED RESULT ==
	...	Input the command into the ttyd terminal and returns the OUTPUT between the command and the last prompt

	${CHECK_TAB}=	Run Keyword And Return Status	Should Not Contain	${COMMAND}	TAB
	Run Keyword If	${CHECK_TAB}	Press Keys 	None	${COMMAND}	RETURN
	...	ELSE	Press Keys 	None	${COMMAND}
	Sleep	1
#	Since ttyd(xterm) draw everything in canvases, text cannot be easily extracted, and so far
#	the best approach I've found is by calling term.getSelection().trim() to get a copy of all text in the canvas.
#	Create selection, then get selection:
    Wait Until Keyword Succeeds    1m    1s     Execute JavaScript  term.selectAll();
	${SELECTION}=    Wait Until Keyword Succeeds    1m    1s     Execute Javascript   return term.getSelection().trim();
	Should Contain  ${SELECTION}	[Enter '^Ec?' for help]
	Should Not Contain  ${SELECTION}	[error.connection.failure] Could not establish a connection to device

	${LINES}=	Split String	${SELECTION}	\n
	${INDEXES_OCCURRED}=	Create List
	${LENGTH}=	Get Length	${LINES}
	FOR	${INDEX}	IN RANGE	0	${LENGTH}
		${LINE}=	Get From List	${LINES}	${INDEX}
		${CHECK}=	Run Keyword And Return Status	Should Contain	${LINE}	]#
		Run Keyword If	${CHECK}	Append to List	${INDEXES_OCCURRED}	${INDEX}
	END

	${STRING}=	Set Variable
	${END}=	Get From List	${INDEXES_OCCURRED}	-1
	${PREVIOUS}=	Get From List	${INDEXES_OCCURRED}	-2
	FOR	${INDEX}	IN RANGE	${PREVIOUS}	${END}
		${LINE}=	Get From List	${LINES}	${INDEX}
		${STRING}=	Set Variable	${STRING}\n${LINE}
	END
	${STRING}=	Get Substring	${STRING}	1
	[Return]	${STRING}

GUI:Access::Generic Console Command Output
    [Arguments]    ${COMMAND}    ${CONTAINS_HELP}=no
    [Documentation]     Input the given command into the ttyd terminal and retrieve the output after prompt lines
    ...    == REQUIREMENTS ==
    ...    The console type needs to be one of those:
    ...    -    CLI -> ends with "]#"
    ...    -    Shell -> ends with "$"
    ...    -    Root -> ends with "#"
    ...    == ARGUMENTS ==
    ...    -    COMMAND = Command to be executed
    ...    -    CONTAINS_HELP = [yes/no] Console contains "[Enter '^Ec?' for help]" message
    ${CONSOLE_CONTENT}=    Wait Until Keyword Succeeds    3 times    3 seconds   GUI:Access::Generic Console Content    COMMAND=${COMMAND}    CONTAINS_HELP=${CONTAINS_HELP}
    Log	\n\n++++++++++++++++++++++++++++ Console Content: ++++++++++++++++++++++++++++\n${CONSOLE_CONTENT}    INFO    console=yes
    Log	\n\n+++++++++++++++++++++++++ End of Console Content. ++++++++++++++++++++++++    INFO    console=yes
    Should Not Contain  ${CONSOLE_CONTENT}  [error.connection.failure] Could not establish a connection to device
    Run Keyword If   "${CONTAINS_HELP}" == "yes"   Should Contain   ${CONSOLE_CONTENT}   [Enter '^Ec?' for help]
    [Return]    ${CONSOLE_CONTENT}

GUI:Access::Generic Console Content
    [Arguments]    ${COMMAND}    ${CONTAINS_HELP}=no
    ${STATUS}    Run Keyword And Return Status    Select Frame    xpath=//*[@id='termwindow']
    Run Keyword If    ${STATUS}    Sleep    15s
    Run Keyword If    ${STATUS}    Press Keys      //body      RETURN
    ${CHECK_TAB}=   Run Keyword And Return Status   Should Not Contain  ${COMMAND}  TAB
    Run Keyword If  ${CHECK_TAB}    Press Keys 	None    ${COMMAND}  RETURN
    ...  ELSE    Press Keys     None    ${COMMAND}
    Sleep   1
    Wait Until Keyword Succeeds    3 times    3 seconds     Execute JavaScript  term.selectAll();
    ${CONSOLE_CONTENT}=    Wait Until Keyword Succeeds    3 times    3 seconds     Execute Javascript   return term.getSelection().trim();
    [Return]    ${CONSOLE_CONTENT}

GUI::Access::Table::Open Console
    [Arguments]    ${HOSTNAME}=${HOSTNAME_NODEGRID}
    [Documentation]     Open the console for the current user
    GUI:Access::Open Table tab
    Click Element   //div[@class="peer_header"]/div[text()="${HOSTNAME}"]/../div/a[text()="Console"]

GUI::Access::Table::Access Console
    [Arguments]    ${HOSTNAME}=${HOSTNAME_NODEGRID}
    [Documentation]     Open the console for the host from Access :: Table and wait until it is ready to be used
    ...    == ARGUMENTS ==
    ...    -    HOSTNAME = Hostname of the console to be accessed
    GUI:Access::Open Table tab
    Wait Until Element Is Visible   //div[@class="peer_header"]/div[text()="${HOSTNAME}"]/../div/a[text()="Console"]
    Click Element   //div[@class="peer_header"]/div[text()="${HOSTNAME}"]/../div/a[text()="Console"]
    Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists    ${HOSTNAME} - Console
    ...	ELSE	GUI:Basic::Wait Until Window Exists    ${HOSTNAME}

    Wait Until Page Contains Element    xpath=//*[@id='termwindow']
    Select Frame    xpath=//*[@id='termwindow']
    Wait Until Keyword Succeeds    1m    1s     Execute JavaScript  term.selectAll();

    Wait Until Page Contains Element    terminal-container
    Current Frame Should Contain    xterm-viewport
    Current Frame Should Contain    xterm-screen

    Wait Until Page Contains Element    css=.xterm-helpers
    Wait Until Page Contains Element    css=.xterm-helper-textarea
    Wait Until Page Contains Element    css=.xterm-text-layer
    Wait Until Page Contains Element    css=.xterm-selection-layer
    Wait Until Page Contains Element    css=.xterm-link-layer
    Wait Until Page Contains Element    css=.xterm-cursor-layer
    Press Keys     None    RETURN
    [Return]    ${HOSTNAME}

GUI::Access::Tree::Access Console
    [Arguments]    ${HOSTNAME}=${HOSTNAME_NODEGRID}   ${DOMAIN_NAME}=${EMPTY}
    [Documentation]     Open the console for the host from Access :: Tree and wait until it is ready to be used
    ...    == ARGUMENTS ==
    ...    -    HOSTNAME = Hostname of the console to be accessed
    ...    -    DOMAIN_NAME = This variable is only set if the console is being accessed via cluster (i.e., it's a peer)
    GUI:Access::Open Tree Tab
    Wait Until Element Is Visible    //ol[@id="tree_content"]//a[text()="${HOSTNAME}"]/../../span/a[text()="Console"]
    Click Element   //ol[@id="tree_content"]//a[text()="${HOSTNAME}"]/../../span/a[text()="Console"]

    ${WINDOW_TITLE}=   Set Variable   ${HOSTNAME}
    ${WINDOW_TITLE}=   Set Variable If   "${DOMAIN_NAME}" == "${EMPTY}"
    ...   ${HOSTNAME}    ${HOSTNAME}.${DOMAIN_NAME}
#    GUI::Basic::Wait Until Window Handle Exists    NEW
#    GUI::Basic::Handle Browser Certificate
    Run Keyword If	'${NGVERSION}' >= '5.6'	GUI:Basic::Wait Until Window Exists    ${WINDOW_TITLE} - Console
    ...	ELSE	GUI:Basic::Wait Until Window Exists    ${WINDOW_TITLE}

    Wait Until Page Contains Element    xpath=//*[@id='termwindow']
    Select Frame    xpath=//*[@id='termwindow']
    Wait Until Keyword Succeeds    1m    1s     Execute JavaScript  term.selectAll();

    Wait Until Page Contains Element    terminal-container
    Current Frame Should Contain    xterm-viewport
    Current Frame Should Contain    xterm-screen

    Wait Until Page Contains Element    css=.xterm-helpers
    Wait Until Page Contains Element    css=.xterm-helper-textarea
    Wait Until Page Contains Element    css=.xterm-text-layer
    Wait Until Page Contains Element    css=.xterm-selection-layer
    Wait Until Page Contains Element    css=.xterm-link-layer
    Wait Until Page Contains Element    css=.xterm-cursor-layer
    Press Keys     None    RETURN
    [Return]    ${WINDOW_TITLE}


GUI::Access::Table::Open Device "${DEVICE_NAME}" Websession
    [Documentation]	Open the Websession for the device with name ${DEVICE_NAME}
    GUI:Access::Open Table tab
    Run Keyword If	'${NGVERSION}' <= '5.6'	Click Element    //tbody//tr[@id="|${DEVICE_NAME}"]//a[text()="Web"]
    ...	ELSE	Click Element    //tbody//tr[@id="|${DEVICE_NAME}|"]//a[text()="Web"]
    Wait Until Keyword Succeeds	5 times	1 seconds	Switch Window	NEW

GUI::Access::Table::Open Info Window
    [Documentation]	Opens the Info window for the system
    GUI:Access::Open Table tab
    Click Element   //div[@class="peer_header"]//a[text()="Info"]
    Wait Until Element Is Visible   //div[@class="modal-body"]//div[@class="modal_buttons"]

GUI::Access::Table::Device Name
	[Documentation]	Opens Device Info on Access table Page
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' <= '5.6'		Click Element	//*[@id="|${DEV_NAME}"]/td[1]/div/a/span
	...	ELSE	Click Element		//*[@id="|${DEV_NAME}|"]/td[1]/div/a/span

GUI::Access::Table::Open Event Log PDF
    GUI::Access::Table::Open Info Window
    ${HANDLES}=   Get Window Handles
    Click Element   //div[@class="modal-body"]//div[@class="modal_buttons"]//input[@id="eventlog"]
    Wait Until Keyword Succeeds    5 times   1 seconds   Switch Window   ${HANDLES}
    Wait Until Element Is Visible   //body
    Sleep  2

GUI::Access::Table::Open Data Log PDF
    GUI::Access::Table::Open Info Window
    ${HANDLES}=   Get Window Handles
    Click Element   //div[@class="modal-body"]//div[@class="modal_buttons"]//input[@id="datalog"]
    Wait Until Keyword Succeeds    5 times   1 seconds   Switch Window   ${HANDLES}
    Wait Until Element Is Visible   //body
    Sleep  2

GUI::Access::Table::Close Info Window
    Click Element   //*[@id="modal"]/div/div/div[1]/button
    Wait Until Element Is Not Visible   //div[@class="modal-body"]//div[@class="modal_buttons"]

GUI::Access::Table::Open Device ${DEVICE_NAME} Info Window
    [Documentation]	Opens the Info window for the device ${DEVICE_NAME}
    GUI:Access::Open Table tab
    Run Keyword If	'${NGVERSION}' <= '5.6'		Click Element   //tbody//tr[@id="|${DEVICE_NAME}"]//a//span[text()="${DEVICE_NAME}"]
    ...	ELSE	Click Element   //tbody//tr[@id="|${DEVICE_NAME}|"]//a//span[text()="${DEVICE_NAME}"]
    Wait Until Element Is Visible   //div[@class="modal-body"]//div[@class="modal_buttons"]

GUI::Access::Table::Open Device "${DEVICE_NAME}" Data Log PDF
    GUI::Access::Table::Open Device ${DEVICE_NAME} Info Window
    ${HANDLES}=   Get Window Handles
    Click Element   //div[@class="modal-body"]//div[@class="modal_buttons"]//input[@id="datalog"]
    Wait Until Keyword Succeeds    5 times   1 seconds   Switch Window   ${HANDLES}
    Wait Until Element Is Visible   //body
    Sleep  2

GUI::Access::Table::Open Device "${DEVICE_NAME}" Event Log PDF
    GUI::Access::Table::Open Device ${DEVICE_NAME} Info Window
    ${HANDLES}=   Get Window Handles
    Click Element   //div[@class="modal-body"]//div[@class="modal_buttons"]//input[@id="eventlog"]
    Wait Until Keyword Succeeds    5 times   1 seconds   Switch Window   ${HANDLES}
    Wait Until Element Is Visible   //body
    Sleep  2

GUI::Access::Table::Get Hostname
    [Documentation]	Returns the hostname
    GUI:Access::Open Table tab
    ${HOSTNAME}=    Get Text    //div[@class="peer_header"]/div[@class="ClusterTitle"]
    [Return]  ${HOSTNAME}

GUI::Access::Get Userline Length
	[Documentation]	Retrieves the current userline char length
	...	eg.: 'admin@nodegrid.localdomain' turns to '[admin@nodegrid /]# ' and will return 20
	...	== EXPECTED RESULT ==
	...	Returns the length of the userline to use as a reference in the console window to simulate copy/paste

	${NAME}=	Get Text	pwl
	${NAME}=	Fetch From Left	${NAME}	.
	${NAME}=	Get Substring	${NAME}	1
	${NAME}=	Set Variable	[${NAME} /]#${SPACE}
	${LENGTH}=	Get Length	${NAME}
	[Return]	${LENGTH}

GUI:Access::Inspect Device In Row
	[Arguments]	${ROW}	${BUTTON}	${TERMINAL}=${FALSE}	${ALIVE}=False
	${ID_DEVICE}=	Get Element Attribute	${ROW}	id
	${ID_DEVICE}=	Escape jQuery Selector	${ID_DEVICE}
	${JQUERY}=	Set Variable	div:visible > table > tbody > tr\#${ID_DEVICE} > td:nth-child(2) > div > a
	${BUTTONS}=	Get Element Count	jquery=${JQUERY}
	FOR	${INDEX}	IN RANGE	${BUTTONS}
		${TEXT}=	Get Text	jquery=${JQUERY}:nth-child(${INDEX+1})
		${IS_EXPECTED_BUTTON}=	Run Keyword And Return Status	Should Be Equal	${TEXT}	${BUTTON}
		${DEVICE_NAME}=	Get Text	jquery=div:visible > table > tbody > tr\#${ID_DEVICE} > td:first-child() > div > a > span
		Run Keyword If	${IS_EXPECTED_BUTTON} and '${TERMINAL}' == 'False' and '${ALIVE}' == 'False'	GUI:Access::Device	${JQUERY}:nth-child(${INDEX+1})	${DEVICE_NAME}	${BUTTON}
		Run Keyword If	${IS_EXPECTED_BUTTON} and ${TERMINAL} and '${BUTTON}' == 'Console'	GUI:Access::Device Check Terminal	${JQUERY}:nth-child(${INDEX+1})	${DEVICE_NAME}
		Run Keyword If	${IS_EXPECTED_BUTTON} and '${TERMINAL}' == 'False' and '${ALIVE}' == 'True'	GUI:Access::Device Access Console	${JQUERY}:nth-child(${INDEX+1})	${DEVICE_NAME}
		Exit for Loop If	${IS_EXPECTED_BUTTON}
	END

GUI:Access::Access Devices With Button
	[Arguments]	${BUTTON}	${IGNORE}=${EMPTY}	${MAX}=5
	${ROWS}=	Get WebElements	jquery=div:visible > table.SPMTable > tbody > tr
	${COUNT}=	Set Variable	0
	FOR	${ROW}	IN	@{ROWS}
		Run Keyword If	${COUNT} >= ${MAX}	Exit For Loop
		${ID_DEVICE}=	Get Element Attribute	${ROW}	id
		Run Keyword If	'${IGNORE}' == '${ID_DEVICE}'	Continue For Loop
		${COUNT}=	Set Variable	'${COUNT}' + '1'
		GUI:Access::Inspect Device In Row	${ROW}	${BUTTON}
	END

GUI::Access::Access ${DEVICE} Device With ${BUTTON}
	[Documentation]	Launch the given action in the given device in the access page
	
    IF  '${NGVERSION}' <= '5.6'	
	    ${ID_DEVICE}=  Escape jQuery Selector	|${DEVICE}
    ELSE   
	    ${ID_DEVICE}=	Escape jQuery Selector	|${DEVICE}|
	END

	${ROW}=	Get WebElement	jquery=div:visible > table.SPMTable > tbody > tr\#${ID_DEVICE}
	GUI:Access::Inspect Device In Row	${ROW}	${BUTTON}
	GUI:Access::Open Table tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	${BUTTON}

GUI::Access::Access ${DEVICE} Device and Check Terminal ${TERMINAL}
	[Documentation]	Launch the given action in the given device in the access page
	${ID_DEVICE}=	Escape jQuery Selector	|${DEVICE}
	${ROW}=	Get WebElement	jquery=div:visible > table.SPMTable > tbody > tr\#${ID_DEVICE}
	GUI:Access::Inspect Device In Row	${ROW}	Console	${TERMINAL}

GUI::Access::Access ${DEVICE} Device and Console Window Open ${ALIVE}
	[Documentation]	Launch the given action in the given device in the access page
	${ID_DEVICE}=	Escape jQuery Selector	|${DEVICE}
	${ROW}=	Get WebElement	jquery=div:visible > table.SPMTable > tbody > tr\#${ID_DEVICE}
	GUI:Access::Inspect Device In Row	${ROW}	Console	ALIVE=${ALIVE}

GUI::Access::Table::Wait Until Cluster Node Is Visible
    [Arguments]     ${NAME}
    Wait Until Keyword Succeeds  2m  3s   GUI::Access::Table::Check Cluster Node Visibility    ${NAME}

GUI::Access::Table::Check Cluster Node Visibility
    [Arguments]    ${NAME}
    GUI:Access::Open Table tab
    GUI::Basic::Spinner Should Be Invisible
    wait until page contains element		xpath=//img[@id='expandstate']
    Click Element    xpath=//img[@id='expandstate']
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain Element   //div[@class="peer_header"]/div[text()="${NAME}"]/../div/a[text()="Console"]
    Page Should Contain Element   //div[@class="peer_header"]/div[text()="${NAME}"]/../div/a[text()="Web"]

GUI::Access::Tree::Wait Until Cluster Node Is Visible
    [Arguments]     ${NAME}
    Wait Until Keyword Succeeds  2m  3s   GUI::Access::Tree::Check Cluster Node Visibility    ${NAME}

GUI::Access::Tree::Check Cluster Node Visibility
    [Arguments]    ${NAME}
    GUI:Access::Open Tree tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element    (//img[@id='view'])[1]
    GUI::Basic::Spinner Should Be Invisible
    Click Element    (//img[@id='view'])[2]
    GUI::Basic::Spinner Should Be Invisible
    Page Should Contain Element   //ol[@id="tree_content"]//a[text()="${NAME}"]/../../span/a[text()="Console"]
    Page Should Contain Element   //ol[@id="tree_content"]//a[text()="${NAME}"]/../../span/a[text()="Web"]

GUI::Console Command Output
	[Arguments]	${COMMAND}
	${CHECK_TAB}=	Run Keyword And Return Status	Should Not Contain	${COMMAND}	TAB
	Run Keyword If	${CHECK_TAB}	Press Keys 	None	${COMMAND}	RETURN
	...	ELSE	Press Keys 	None	${COMMAND}
	Sleep	1

Enter to Shell Mode
	[Arguments]	${SCREENSHOT}=TRUE
	[Documentation]	Opens the NG Access tab
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is true
	...	== EXPECTED RESULT ==
	...	Access Tab is Opened and all elements are accessible
	Wait Until Element Is Visible		xpath=//th[contains(.,'Name')]
	GUI::Access::Table::Access Console
	${OUTPUT}=	GUI:Access::Generic Console Command Output	shell sudo su -
	Sleep	15s
	Should Contain	${OUTPUT}	root@nodegrid:~#

GUI::Access::Overview::Open Tab
	GUI::Access::Open
	Click Element		xpath=(//a[contains(text(),'Overview')])[2]
	Wait Until Element Is Visible		css=#title_overallStatus
	Wait Until Element Is Visible		xpath=//h4[contains(.,'Connections')]
	Wait Until Element Is Visible		xpath=//h4[contains(.,'DHCP Leases')]
	Wait Until Element Is Visible		xpath=//h4[contains(.,'ZPE Cloud')]