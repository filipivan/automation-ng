*** Keywords ***

GUI::Cloud::Open
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid cloud tab
	...     == ARGUMENTS ==
	...     -	SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Cloud Tab is open and all elements are accessable
	GUI::Basic::Wait Until Element Is Accessible	jquery=#main_menu > li:nth-child(6) > a
	Execute Javascript	window.document.evaluate("//*[@id='main_menu']", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element	jquery=#main_menu > li:nth-child(6) > a
	GUI::Basic::Spinner Should Be Invisible

GUI::Cloud::Open Settings Tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Cloud::Settings tab
	...     == ARGUMENTS ==
	...     -	SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Settings Tab is open and all elements are accessible
	[Tags]      GUI     CLOUD      LICENSE
	GUI::Cloud::Open
	Run Keyword If	'${NGVERSION}' <= '5.6'		Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(2) > a
	...	ELSE	click element		xpath=(//a[contains(text(),'Settings')])[4]
	GUI::Cloud::Wait For Settings

GUI::Cloud::Open Peers Tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Cloud::Settings tab
	...     == ARGUMENTS ==
	...     -	SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Settings Tab is open and all elements are accessible
	[Tags]      GUI     CLOUD      LICENSE
	GUI::Cloud::Open
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(1) > a
	GUI::Cloud::Wait For Peers

GUI::Cloud::Wait For Settings
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' <= '5.6'	Wait Until Element Contains	jquery=body > div.main_menu > div > ul > li.active > a	Settings
	...	ELSE	Wait Until Element Is Visible		xpath=(//a[contains(text(),'Settings')])[4]
	Wait Until Element Is Visible	jquery=#saveButton
	Wait Until Element Is Visible	id=autoEnroll
	Wait Until Element Is Visible	id=enabled
	Wait Until Element Is Visible	id=management_enabled
	Run Keyword If	'${NGVERSION}' >= '4.0'	Wait Until Element Is Visible	id=lps_enabled

GUI::Cloud::Wait For Peers
	GUI::Basic::Spinner Should Be Invisible
	Table Header Should Contain	id=peerTable	Name
	Table Header Should Contain	id=peerTable	Address
	Table Header Should Contain	id=peerTable	Type
	Table Header Should Contain	id=peerTable	Status
	Table Header Should Contain	id=peerTable	Peer Status
