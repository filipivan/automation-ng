from clipboard import paste

class ngmGuiBasicKeywords(object):
    def paste_from_clipboard(self):
        return paste()

    paste_from_clipboard.robot_name = "GUI::Basic::Paste From Clipboard"