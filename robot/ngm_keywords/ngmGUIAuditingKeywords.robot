*** Keywords ***

GUI::Auditing::Open
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Auditing tab
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Managed devices Tab is open and all elements are accessable
	Click Element	jquery=#main_menu > li:nth-child(8) > a
	GUI::Basic::Spinner Should Be Invisible

GUI::Auditing::Open Settings tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Auditing::Settings tab
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Managed devices Tab is open and all elements are accessable
	GUI::Auditing::Open
	Wait Until Element Is Visible	xpath=(//a[contains(text(),'Settings')])[4]
	Click Element	xpath=(//a[contains(text(),'Settings')])[4]
	GUI::Basic::Spinner Should Be Invisible

GUI::Auditing::Open Events tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Auditing::Events tab
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Managed devices Tab is open and all elements are accessable
	GUI::Auditing::Open
	Wait Until Element Is Visible	xpath=(//a[contains(text(),'Events')])[2]
	Click Element	xpath=(//a[contains(text(),'Events')])[2]
	GUI::Basic::Spinner Should Be Invisible

GUI::Auditing::Events::Open "${NAME}" Row
    [Documentation]	Opens the event category selection for the row given by "${NAME}"
    GUI::Auditing::Open Events tab
    Click Element   //table//tr//a[@id="${NAME}"]
    GUI::Basic::Spinner Should Be Invisible

GUI::Auditing::Open Destinations tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Auditing::Destinations tab
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Managed devices Tab is open and all elements are accessable
	GUI::Auditing::Open
	Wait Until Element Is Visible	xpath=(//a[contains(text(),'Destinations')])[2]
	Click Element	xpath=(//a[contains(text(),'Destinations')])[2]
	GUI::Basic::Spinner Should Be Invisible

GUI::Auditing::Events::Open Event List tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Auditing::Events::Event List tab
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Managed devices Tab is open and all elements are accessable
	GUI::Basic::Auditing::Events::Open Tab
	Click Element	css=#auditing_event_list > .title_b
	GUI::Basic::Spinner Should Be Invisible

GUI::Auditing::Open File Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Auditing::Destinations::File
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	VM Auditing Tab is open and all elements are accessible
	[Tags]	GUI	AUDITING	FILE
	GUI::Auditing::Open Destinations tab
	Click Element	jquery=#pod_menu > a:nth-child(1)
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#refresh-icon

GUI::Auditing::Open Syslog Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Auditing::Destinations::Syslog
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	VM Auditing Tab is open and all elements are accessible
	[Tags]	GUI	AUDITING	SYSLOG
	GUI::Auditing::Open Destinations tab
	Click Element	jquery=#pod_menu > a:nth-child(2)
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#refresh-icon

GUI::Auditing::Open SNMPTrap Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Auditing::Destinations::SNMPTrap
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	VM Auditing Tab is open and all elements are accessible
	[Tags]	GUI	AUDITING	SNMPTRAP
	GUI::Auditing::Open Destinations tab
	Click Element	jquery=#pod_menu > a:nth-child(3)
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#refresh-icon

GUI::Auditing::Open Email Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Auditing::Destinations::Email
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	VM Auditing Tab is open and all elements are accessible
	[Tags]	GUI	AUDITING	EMAIL
	GUI::Auditing::Open Destinations tab
	Click Element	jquery=#pod_menu > a:nth-child(4)
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#refresh-icon

GUI::Auditing::Settings::Go To Events And Back to Settings
	GUI::Auditing::Open Events tab
	GUI::Auditing::Open Settings tab

GUI::Auditing::Settings::Switch EvttimeType
	${UTC_SELECTED}=	Run Keyword And Return Status	Radio Button Should Be Set To	evttimeType	utc
	${EVT_TIME_TYPE}=	Set Variable If	${UTC_SELECTED}	local	utc
	Select Radio Button	evttimeType	${EVT_TIME_TYPE}
	[Return]	${EVT_TIME_TYPE}

GUI::Auditing::Settings::Set And Validate EvttimeType
	${EVT_TIME_TYPE}=	GUI::Auditing::Settings::Switch EvttimeType
	GUI::Basic::Save
	GUI::Auditing::Settings::Go To Events And Back to Settings
	Radio Button Should Be Set To	evttimeType	${EVT_TIME_TYPE}

GUI::Auditing::Settings::Switch DatatimeType
	${UTC_SELECTED}=	Run Keyword And Return Status	Radio Button Should Be Set To	datatimeType	utc
	${DATA_TIME_TYPE}=	Set Variable If	${UTC_SELECTED}	local	utc
	Select Radio Button	datatimeType	${DATA_TIME_TYPE}
	[Return]	${DATA_TIME_TYPE}

GUI::Auditing::Settings::Set And Validate DatatimeType
	${TIMESTAMP}=	Run Keyword And Return Status	Checkbox Should Be Selected	jquery=#timestamp
	Click Element	jquery=#timestamp
	GUI::Basic::Save
	GUI::Auditing::Settings::Go To Events And Back to Settings
	Run Keyword If	${TIMESTAMP}	Checkbox Should Not Be Selected	jquery=#timestamp
	Run Keyword Unless	${TIMESTAMP}	Checkbox Should Be Selected	jquery=#timestamp

GUI::Auditing::Settings::Set And Validate Timestamp Format
	${DATA_TIME_TYPE}=	GUI::Auditing::Settings::Switch DatatimeType
	GUI::Basic::Save
	GUI::Auditing::Settings::Go To Events And Back to Settings
	Radio Button Should Be Set To	datatimeType	${DATA_TIME_TYPE}


GUI::Auditing::Auto Input Tests
    [Arguments]  ${LOCATOR}     @{TESTS}	${FAILBACK}=no
    [Documentation]	locator gets the id of the element to test. You must be on the proper page
    ...                 tests is the texts to be tested. All must be wrong except for the last input
    ${LEN_T}=     Get Length      ${TESTS}
    FOR    ${I}    IN RANGE    0   ${LEN_T}-1
           GUI::Auditing::Text Input Error Tests  ${LOCATOR}  ${TRUE}     ${TESTS}[${I}]	FAILBACK=${FAILBACK}
    END
    ${LEN_T}=       Evaluate    ${LEN_T} - 1
    GUI::Auditing::Text Input Error Tests  ${LOCATOR}  ${FALSE}    ${TESTS}[${LEN_T}]	FAILBACK=${FAILBACK}

GUI::Auditing::Text Input Error Tests
    [Arguments]  ${LOCATOR}     ${EXPECTS}      ${TEXT_ENT}	${FAILBACK}=no
    [Documentation]	locator wants the id of the element
    ...                 expects wants a t/f of whether an error should be thrown
    ...                 text_ent wants the input
    Execute Javascript  document.getElementById("${LOCATOR}").scrollIntoView(true);
    Input Text          jquery=#${LOCATOR}      ${empty}
    Input Text          jquery=#${LOCATOR}      ${TEXT_ENT}
    Execute Javascript	window.scrollTo(0,0);
    Wait Until Element Is Not Visible     xpath=//*[@id='loading']    timeout=20s
    Run Keyword If	'${FAILBACK}' == 'Yes'	Select Checkbox	//input[@id='mauthFallback']
    Click Element       jquery=#saveButton
    Wait Until Element Is Not Visible     xpath=//*[@id='loading']    timeout=20s
    GUI::Basic::Spinner Should Be Invisible
    #Element Should Be Visible               jquery=#refresh-icon
    #Need to uncomment the above line after Bug NG-7284
    ${TST}=     Execute Javascript  var ro=document.getElementById('${LOCATOR}');if(ro==null){return false;}var er=ro.parentNode.nextSibling;if(er==null){return false;}return true;
    Log     ${TST}
    Run Keyword If      ${EXPECTS}==${TRUE}     Should Be True     ${TST}   msg=${TEXT_ENT} IN ${LOCATOR} does not throw an error
    ...     ELSE    Should Not Be True     ${TST}   msg=${TEXT_ENT} IN ${LOCATOR} should not throw an error
    #TESTING
    #Run Keyword if      ${EXPECTS}==${TRUE}     Page Should Contain Element     xpath=//*[@class='form-group row rwtext has-error']//input[@id='${LOCATOR}']
    #Page Should Not Contain Element       Page Should Not Contain Element       xpath=//*[@class='form-group row rwtext has-error']//input[@id='${LOCATOR}']

GUI::Auditing::Text Global Error Tests
    [Arguments]  ${LOCATOR}     ${EXPECTS}      ${TEXT_ENT}		${ERROR}
    [Documentation]	locator wants the id of the element
    ...                 expects wants a t/f of whether an error should be thrown
    ...                 text_ent wants the input
    Execute Javascript  document.getElementById("${LOCATOR}").scrollIntoView(true);
    Input Text          jquery=#${LOCATOR}      ${empty}
    Input Text          jquery=#${LOCATOR}      ${TEXT_ENT}
    Execute Javascript	window.scrollTo(0,0);
    Wait Until Element Is Not Visible     xpath=//*[@id='loading']    timeout=20s
    Click Element       jquery=#saveButton
    Wait Until Element Is Not Visible     xpath=//*[@id='loading']    timeout=20s
    Element Should Be Visible               jquery=#refresh-icon
    #TESTING
    ${TST}=     Execute Javascript  var ro=document.getElementById('globalerr');if(ro==null){return false;}if(ro.getElementsByTagName('p')[0].innerHTML==='${ERROR}'){return true;}return false;
    Log     ${TST}
    Run Keyword If      ${EXPECTS}==${TRUE}     Should Be True     ${TST}   msg=${TEXT_ENT} IN ${LOCATOR} does not throw an error
    ...     ELSE    Should Not Be True     ${TST}   msg=${TEXT_ENT} IN ${LOCATOR} should not throw an error