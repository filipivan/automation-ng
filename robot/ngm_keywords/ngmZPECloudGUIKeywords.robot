*** Settings ***
Resource	./rootSettings.robot

*** Keywords ***
# First keyword
#	[Tags]	GUI	BROWSER
#	[Documentation]	Opens a browser instance and navigates to the provided URL
#	...	== REQUIREMENTS ==
#	...	== ARGUMENTS ==
#	...	== EXPECTED RESULT ==
#	[Arguments]	${HOMEPAGE}	${BROWSER}
#	[Return]	${RETURN}

GUI::ZPECloud::Basic::Open ZPE Cloud
	[Tags]	GUI	ZPECLOUD	BROWSER
	[Documentation]	Opens a browser instance and navigates to the provided URL
	...	== Steps Resume ==
	...	Open Browser; Wait appears Login page;
	...	== REQUIREMENTS ==
	...	None
	...	== ARGUMENTS ==
	...	- HOMEPAGE -	URL to navigate i.e. https://localhost
	...	- BROWSER -	Browser to be used. Obs: SeleniumLibrary should support and driver should be installed
	...	- SCREENSHOT -	boolean to capture or not a screenshot from page
	...	== EXPECTED RESULT ==
	...	Browser opened URL and login elements appage appears
	[Arguments]	${PAGE}=${HOMEPAGE}	${BROWSER}=${BROWSER}	${SCREENSHOT}=${FALSE}	${ALIAS}=default_alias
	...	${ACCEPT_COOKIES}=//*[text()='This website uses cookies to ensure you get the best experience on our website. ']/*[text()='Learn more']/../../div/button[text()='I accept cookies from this website']
	${BROWSER}	Convert To Lower Case	${BROWSER}
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nOpening Browser: ${BROWSER}	INFO	console=yes
	Run Keyword If	'${BROWSER}' == 'chrome' or '${BROWSER}' == 'headlesschrome'	Wait Until Keyword Succeeds	3x	3s	Open Browser	${PAGE}	${BROWSER}	alias=${ALIAS}	options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors")
	Run Keyword If	'${BROWSER}' == 'firefox' or '${BROWSER}' == 'headlessfirefox'	Wait Until Keyword Succeeds	3x	3s	Open Browser	${PAGE}	${BROWSER}	alias=${ALIAS}	options=set_preference("moz:dom.disable_beforeunload", "true")
	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\nBrowser ${BROWSER} opened	INFO	console=yes
	Set Window Size	1920	1080
	GUI::ZPECloud::Basic::Wait Elements Login
	Run Keyword If	${SCREENSHOT} != ${FALSE}	Capture Page Screenshot	filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png
	Wait Until Keyword Succeeds	6x	3s	GUI::ZPECloud::Basic::Check if cookies message exist	${ACCEPT_COOKIES}

GUI::ZPECloud::Basic::Check if cookies message exist
	[Arguments]	${ACCEPT_COOKIES}
	Reload Page
	Set Window Size	1920	1080
	GUI::ZPECloud::Basic::Wait Elements Login
	${COOKIES}=	Run Keyword And return Status	Page Should Contain Button	${ACCEPT_COOKIES}
	${COOKIES_SCROLL}=	Run Keyword And return Status	Scroll Element Into View	${ACCEPT_COOKIES}
	Run Keyword If	${COOKIES} and ${COOKIES_SCROLL}	Click Element	${ACCEPT_COOKIES}
	Run Keyword If	${COOKIES} and ${COOKIES_SCROLL}	Wait Until Element Is Not Visible	${ACCEPT_COOKIES}	timeout=30s

GUI::ZPECloud::Basic::Wait Elements Login
	Wait Until Keyword Succeeds	1m	1s	GUI::ZPECloud::Basic::Pass if has login page and if has dashboard page click on logout
	${url}=	Log Location
	${login_location}=	Split String	${url}	/
	${login_location}=	Get From List	${login_location}	-1
	Should Be Equal As Strings	${login_location}	login
	Wait Until Keyword Succeeds	30s	10ms	Wait Until Page Contains Element	id=filled-adornment-password

GUI::ZPECloud::Basic::Pass if has login page and if has dashboard page click on logout
	${HAS_DASHBOARD_PAGE}	Run Keyword And Return Status	Page Should Contain Element	//*[contains(@id,'logout-btn')]
	Run Keyword If	${HAS_DASHBOARD_PAGE}	Wait Until Keyword Succeeds	30s	1s	Click Element	//*[contains(@id,'logout-btn')]
	Page Should Contain Element	id=oauth-signin-form-email-input

GUI::ZPECloud::Basic::Open And Login
	[Documentation]	Open Browser, Homepage, Insert credentials, click Login
	...	== Steps Resume ==
	...	Open Browser and Homepage; Wait appears credential fields; insert credentials; click to Login; wait appears Failure;
	...	== REQUIREMENTS ==
	...	ZPE Cloud Login page opened. Can use: GUI::ZPECloud::Basic::Open ZPE Cloud
	...	== ARGUMENTS ==
	...	- USERNAME -	email or username to login
	...	- PASSWORD -	password to login
	...	- SCREENSHOT -	boolean to capture or not a screenshot from page
	...	== EXPECTED RESULT ==
	...	Open Browser and login at homepage
	[Arguments]	${USERNAME}=admin	${PASSWORD}=admin	${SCREENSHOT}=${FALSE}
	GUI::ZPECloud::Basic::Open ZPE Cloud	${ZPECLOUD_HOMEPAGE}	${BROWSER}
	Wait Until Keyword Succeeds	2x	1s	GUI::ZPECloud::Basic::Login	${USERNAME}	${PASSWORD}

GUI::ZPECloud::Basic::Login
	[Documentation]	Insert creadentials and click button to login
	...	== Steps Resume ==
	...	Wait appears credential fields; insert credentials; click to Login; wait appears Main page;
	...	== REQUIREMENTS ==
	...	ZPE Cloud Login page opened. Can use: GUI::ZPECloud::Basic::Open ZPE Cloud
	...	== ARGUMENTS ==
	...	- USERNAME -	email or username to login
	...	- PASSWORD -	password to login
	...	- SCREENSHOT -	boolean to capture or not a screenshot from page
	...	== EXPECTED RESULT ==
	...	Login and open main menu
	[Arguments]	${USERNAME}=admin	${PASSWORD}=admin	${SCREENSHOT}=${FALSE}
	GUI::ZPECloud::Basic::Wait Elements Login
#	${JQUERY}=	Escape jQuery Selector	\div[id="root"] > div > div > div > div:nth-child(1) > main > div > div:nth-child(1) > div > form > div:nth-child(1) > input
	Input Text	xpath=//form//input[1]	${USERNAME}
	Input Text	css=input[id=filled-adornment-password]	${PASSWORD}
	Wait Until Page Contains Element	//*[@id="oauth-signin-form-login-btn"]/span[1]	15s
	Wait Until Keyword Succeeds	2x	1s	GUI::ZPECLOUD::SIGNIN
	GUI::ZPECloud::Basic::Wait Until Menu Appears
	Run Keyword If	${SCREENSHOT} != ${FALSE}	Capture Page Screenshot	filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI::ZPECLOUD::SIGNIN
	Click Element	//*[@id="oauth-signin-form-login-btn"]/span[1]
	GUI::ZPECloud::Basic::Wait Until Menu Appears

GUI::ZPECloud::Basic::LoginWithEnter
	[Documentation]	Insert credentials and press ENTER to login
	...	== Steps Resume ==
	...	Wait appears credential fields; insert credentials; press ENTER to Login; wait appears Main page;
	...	== REQUIREMENTS ==
	...	ZPE Cloud Login page opened. Can use: GUI::ZPECloud::Basic::Open ZPE Cloud
	...	== ARGUMENTS ==
	...	- USERNAME -	email or username to login
	...	- PASSWORD -	password to login
	...	- SCREENSHOT -	boolean to capture or not a screenshot from page
	...	== EXPECTED RESULT ==
	...	Login and open main menu containing Dashboard
	[Arguments]	${USERNAME}=admin	${PASSWORD}=admin	${SCREENSHOT}=${FALSE}
	GUI::ZPECloud::Basic::Wait Elements Login
#	${JQUERY}=	Escape jQuery Selector	\div[id="root"] > div > div > div > div:nth-child(1) > main > div > div:nth-child(1) > div > form > div:nth-child(1) > input
	Input Text	xpath=//form//input[1]	${USERNAME}
	Input Text	css=input[id=filled-adornment-password]	${PASSWORD}
	Click Element	filled-adornment-password
	Press Keys	filled-adornment-password	RETURN
	GUI::ZPECloud::Basic::Wait Until Menu Appears
	Run Keyword If	${SCREENSHOT} != ${FALSE}	Capture Page Screenshot	filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI::ZPECloud::Basic::Login Failure
	[Documentation]	Insert credentials, click Login and wait Failure
	...	== Steps Resume ==
	...	Wait appears credential fields; insert credentials; click to Login; wait appears Failure;
	...	== REQUIREMENTS ==
	...	ZPE Cloud Login page opened. Can use: GUI::ZPECloud::Basic::Open ZPE Cloud
	...	== ARGUMENTS ==
	...	- USERNAME -	email or username to login
	...	- PASSWORD -	password to login
	...	- SCREENSHOT -	boolean to capture or not a screenshot from page
	...	== EXPECTED RESULT ==
	...	Login failure and return to login page
	[Arguments]	${USERNAME}=admin	${PASSWORD}=bla	${SCREENSHOT}=${FALSE}
	GUI::ZPECloud::Basic::Wait Elements Login
#	${JQUERY}=	Escape jQuery Selector	\div[id="root"] > div > div > div > div:nth-child(1) > main > div > div:nth-child(1) > div > form > div:nth-child(1) > input
	Input Text	xpath=//form//input[1]	${USERNAME}
	Input Text	css=input[id=filled-adornment-password]	${PASSWORD}
	Wait Until Page Contains Element	//*[@id="oauth-signin-form-login-btn"]/span[1]	15s
	Click Element	//*[@id="oauth-signin-form-login-btn"]/span[1]
	GUI::ZPECloud::Basic::Wait Elements Login
	Page Should Contain	Login failed
	Run Keyword If	${SCREENSHOT} != ${FALSE}	Capture Page Screenshot	filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI::ZPECloud::Basic::Logout and Close All
	[Arguments]	${CLOSEALL}=True
	GUI::ZPECloud::Basic::Logout
	Run Keyword If	'${CLOSEALL}'=='True'	Close All Browsers
	...	ELSE	Close Browser

GUI::Cloud::Enroll Device on Cloud
	[Arguments]	${EMAIL}=${EMAIL_ADDRESS}	${PWD}=${PASSWORD}	${DIFF_SERIALNUMBER}=${SERIALNUMBER}	${USERNG}=${USERNAMENG}	${PWDNG}=${PASSWORDNG}	${CUSCODE}=${CUSTOMERCODE}	${ENROLLKEY}=${ENROLLMENTKEY}	${ZPECLOUDPAGE}=${ZPECLOUD_HOMEPAGE}
	[Documentation]	Enroll the Device on cloud
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Settings Tab is open and all elements are accessible
	[Tags]	GUI	CLOUD	Enroll
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Security::Services::open tab
	GUI::Basic::Spinner Should Be Invisible
	${Is_Checkbox_Selected}=	Run Keyword And Return Status	Checkbox Should Be Selected	//*[@id="cloud_enable"]
	Run Keyword If	'${Is_Checkbox_Selected}'== 'False'	Run Keywords	GUI::Cloud::Enable cloud on NG
	GUI::Open System tab
	Sleep	2s
	Click Element	xpath=(//a[contains(text(),'Toolkit')])[2]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	icon_cloudEnrollment	30s
	Click Element	icon_cloudEnrollment
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	url	30s
	Clear Element Text	url
	Press Keys	url	CTRL+a+BACKSPACE
	Input Text	url	${ZPECLOUDPAGE}
	Input Text	customer_code	${CUSTOMERCODE}
	Input Text	enrollment_key	${ENROLLMENTKEY}
	Click Element	//*[@id="saveButton"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	15x	1s	Wait Until Page Contains	uccessful	#Enrollment process successful! or Susscessfully transfered device ownership.
	Sleep	5s
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	Click Element	//*[@id="ca-header-tab-1-3"]
	GUI::Basic::Spinner Should Be Invisible
	Click Button	//*[@id="ca-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	search-input	${DIFF_SERIALNUMBER}
	GUI::Basic::Spinner Should Be Invisible
	Sleep	5s
	Wait until Page Contains	${DIFF_SERIALNUMBER}	30s
	Wait Until Page Contains Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	30s
	Click Element	//*[@id="ca-devices-available-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Click Element	//*[@id="function-button-0"]
	Wait Until Keyword Succeeds	15x	1s	Wait Until Page Contains	uccessful	#Device Enrolled Successfully

GUI::Cloud::Enable cloud on NG
	Select Checkbox	jquery=#cloud_enable
	Select Checkbox	jquery=#remote_access
	Click Element	jquery=#saveButton
	GUI::Basic::Spinner Should Be Invisible	SPINNER_TIMEOUT=2m

GUI::Cloud::Get Customercode and Enrollmentkey
	[Arguments]	${EMAIL}=${EMAIL_ADDRESS}	${PWD}=${PASSWORD}
	[Documentation]	Opens the NodeGrid Cloud::Settings tab and fetches Customer code and Enrollment key
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Settings Tab is open and all elements are accessible
	[Tags]	GUI	CLOUD	Enroll
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	Click ELement	//*[@id="ca-header-tab-1-7"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	${Is_Checkbox_Selected}=	Run Keyword And Return Status	Checkbox Should Be Selected	//*[@id="ca-setgen-enable-device-enrollment-checkbox"]
	Select Checkbox	//*[@id="ca-setgen-enable-device-enrollment-checkbox"]
	${CUSTOMERCODE_Temp}=	Get Value	//*[@id="ca-setgen-customer-code-input"]
	${ENROLLMENTKEY_Temp}=	Get Value	//*[@id="filled-adornment-password"]
	Set Suite Variable	${CUSTOMERCODE}	${CUSTOMERCODE_Temp}
	Set Suite Variable	${ENROLLMENTKEY}	${ENROLLMENTKEY_Temp}
	Sleep	2s
	Run Keyword If	'${Is_Checkbox_Selected}'== 'False'	Run Keyword	Click Element	//*[@id="ca-setgen-set-company-key-btn"]/span[1]
	GUI::Basic::Spinner Should Be Invisible	SPINNER_TIMEOUT=2m
	Sleep	2s
	[Return]	${CUSTOMERCODE}	${ENROLLMENTKEY}

GUI::Cloud::UNEnroll Device on Cloud
	[Arguments]	${EMAIL}=${EMAIL_ADDRESS}	${PWD}=${PASSWORD}	${USERNG}=${USERNAMENG}	${PWDNG}=${PASSWORDNG}	${CUSCODE}=${CUSTOMERCODE}	${ENROLLKEY}=${ENROLLMENTKEY}	${ZPECLOUDPAGE}=${ZPECLOUD_HOMEPAGE}
	[Documentation]	UnEnroll the Device on cloud
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Settings Tab is open and all elements are accessible
	[Tags]	GUI	CLOUD	Enroll
	GUI::Cloud::Get Customercode and Enrollmentkey
	CLI:Connect As Root	${HOST}	${PWDNG}
	${UNENROLL}=	Set Variable	zpe_cloud_enroll -c ${CUSCODE} -k ${ENROLLKEY} -u ${ZPECLOUDPAGE} -s # processo de unenroll
	Write	${UNENROLL}
	Read Until	(yes, no):
	Write	yes
	sleep	5s
	${OUTPUT}=	Read Until Prompt
	Should Contain Any	${OUTPUT}	Unenrollment process successful!
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	Click ELement	//*[@id="ca-header-tab-1-7"]
	Sleep	2s
	Unselect Checkbox	//*[@id="ca-setgen-enable-device-enrollment-checkbox"]
	Sleep	2s
	Click Element	//*[@id="ca-setgen-set-company-key-btn"]/span[1]
	GUI::Basic::Spinner Should Be Invisible	SPINNER_TIMEOUT=2m

GUI::Cloud::Verify if device is enrolled and Online
	[Arguments]	${EMAIL}=${EMAIL_ADDRESS}	${PWD}=${PASSWORD}
	GUI::ZPECloud::Check and update the zpecloud serial number variable if needed	${USERNAMENG}	${PASSWORDNG}
	Wait Until Keyword Succeeds	3x	3s	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Input Text	search-input	${SERIALNUMBER}
	GUI::Basic::Spinner Should Be Invisible
	${HAS_DEVICE_ENROLLED}=	Run Keyword And Return Status	Wait Until Page Contains Element
	...	//div[contains(text(),'${SERIALNUMBER}')]	1m
	IF	not ${HAS_DEVICE_ENROLLED}
		${HAS_AVAILABLE_PAGE}	Run Keyword And Return Status	GUI::ZPECloud::Devices::Available::Open Tab
		IF	${HAS_AVAILABLE_PAGE}
			${HAS_DEVICE_AVAILABLE}=	Run Keyword And Return Status	GUI::Cloud::Verify if device is available
			Run Keyword If	${HAS_DEVICE_AVAILABLE}	GUI::Cloud::Move from available to enrolled
			Run Keyword If	not ${HAS_DEVICE_AVAILABLE}	Fail	NG is not enrolled.
		ELSE
		Wait Until Keyword Succeeds	3x	3s	GUI::Cloud::Login And Check If Device is Enrolled
		END
	END
	${HAS_DEVICE_ONLINE_STATUS}=	Run Keyword and Return Status	GUI::ZPECloud::Devices::Enrolled::Page Should Contain Device With Online Status
	${STATUS}	Set Variable	${HAS_DEVICE_ONLINE_STATUS}
	IF	not ${HAS_DEVICE_ONLINE_STATUS}
		CLI::ZPECloud::Restart agent process via root
		${STATUS}	Run Keyword And Return Status	Wait Until Keyword Succeeds	6m	5s	GUI::ZPECloud::Devices::Enrolled::Page Should Contain Device With Online Status
		${DEVICE_STATUS}=	GUI::ZPECloud::Devices::Enrolled::Get Device Status
		IF	'${DEVICE_STATUS}' == 'Offline'
			Log	\n\nDevice on ${DEVICE_STATUS} status. Need to check if it was a network issue or if not need to understant the issue and raise a bug.\n\n	WARN
		ELSE IF	'${DEVICE_STATUS}' == 'Failover'
			Log	\n\nDevice on ${DEVICE_STATUS} status. After 6m the device was Online:"${STATUS}". If "False" need to check why device didn't go back to Online status.\n\n	WARN
		ELSE
			Log	\n\nDevice on ${DEVICE_STATUS} status. This is an unknown status being warned to check and if needed please update this keyword.	WARN
		END
	END
	[Return]	${STATUS}

GUI::ZPECloud::Devices::Enrolled::Get Device Status
	GUI::ZPECloud::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Input Text	search-input	${SERIALNUMBER}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//div[contains(text(), '${SERIALNUMBER}')]	30s
	GUI::Basic::Spinner Should Be Invisible
	${STATUS}	Get Text	(//div[contains(text(), '${SERIALNUMBER}')]/../../td/div/div/span)[1]
	[Return]	${STATUS}

GUI::Cloud::Login And Check If Device is Enrolled
	Wait Until Keyword Succeeds	3x	3s	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Input Text	search-input	${SERIALNUMBER}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//div[contains(text(),'${SERIALNUMBER}')]	1m

GUI::Cloud::Verify if device is available
	[Arguments]	${EMAIL}=${EMAIL_ADDRESS}	${PWD}=${PASSWORD}	${DIFF_SERIALNUMBER}=${SERIALNUMBER}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	Click Element	//*[@id="ca-header-tab-1-3"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="ca-header-tab-2-1"]
	Click Element	//*[@id="ca-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	${STATUS}=	Run Keyword And Return Status	Page Should Contain	${DIFF_SERIALNUMBER}
	[Return]	${STATUS}

GUI::Cloud::Move from available to enrolled
	[Arguments]	${EMAIL}=${EMAIL_ADDRESS}	${PWD}=${PASSWORD}	${DIFF_SERIALNUMBER}=${SERIALNUMBER}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	Click Element	//*[@id="ca-header-tab-1-3"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="ca-header-tab-2-1"]
	Click Element	//*[@id="ca-header-tab-2-1"]
	GUI::Basic::Spinner Should Be Invisible
	Sleep	2s
	Click Element	//td[contains(.,'${DIFF_SERIALNUMBER}')]/../td[1]
	Click Element	//button[contains(.,'Enroll')]
	Wait Until Page Contains	Device Enrolled Successfully

### Menu | Submenu ###

GUI::ZPECloud::Basic::Get Menu Elements
	[Arguments]	${SCREENSHOT}=${FALSE}
	Wait Until Page Contains Element	xpath=//header/*//img
	Wait Until Page Contains Element	xpath=//header[1]/*//button
	${buttons}=	Get WebElement	xpath=//header[1]/*//button
	${text}=	Get Text	${buttons}
	${get}=	Get WebElement	xpath=//header/*//img
#	${elements}=	Get Element Attribute	${get}	src

	${up_menu}=	Get WebElement	xpath=//header
	${menu_text}=	Get Text	${up_menu}
	${menutext_list}=	Split String	${menu_text}	\n

	${logout}=	Get From List	${menutext_list}	-1
	${logout}=	Convert to Lowercase	${logout}
	Should Be Equal As Strings	${logout}	logout

	${help}=	Get From List	${menutext_list}	-3
	${help}=	Convert to Lowercase	${help}
	Should Be Equal As Strings	${help}	help
#	Wait Until Page Contains Element	xpath=//main/*//header/div/p[1]
	Remove From List	${menutext_list}	-2
	Remove From List	${menutext_list}	-4
	Run Keyword If	${SCREENSHOT} != ${FALSE}	Capture Page Screenshot	filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png
	[Return]	${menutext_list}

GUI::ZPECloud::Basic::Wait Until Menu Appears
	Wait Until Keyword Succeeds	2x	1s	GUI::ZPECloud::Basic::Get Menu Elements

GUI::ZPECloud::Basic::Get Main Menu
	[Documentation]	Get only buttons from Principal Menu
	${buttons}=	Get WebElements	xpath=//header[1]/*//button
	${return}=	Create List
	FOR	${button}	IN	@{buttons}
#	\	${id}=	Get Element Attribute	${button}	id
		${text}=	Get Text	${button}
		${text_list}=	Split String	${text}	\n
		${text}=	Get From List	${text_list}	-1
		${id}=	Replace String	${text}	${SPACE}	_
		${id}=	Convert To Lowercase	${id}
		Assign Id To Element	${button}	${id}
		Run Keyword If	'${id}' != '${EMPTY}'	Append To List	${return}	${id}
		Run Keyword If	'${id}' != '${EMPTY}'	Log	get/set ID: menu>buttons>${id}	INFO	console=yes
	END
	[Return]	${return}

GUI::ZPECloud::Basic::Get Sub Menu
	[Documentation]	Get only buttons from Sub Menu
	${buttons}=	Get WebElements	xpath=//header[2]/*//button
	${return}=	Create List
	FOR	${button}	IN	@{buttons}
#	\	${id}=	Get Element Attribute	${button}	id
		${text}=	Get Text	${button}
		${text_list}=	Split String	${text}	\n
		${text}=	Get From List	${text_list}	-1
		${id}=	Replace String	${text}	${SPACE}	_
		${id}=	Convert To Lowercase	${id}
		Assign Id To Element	${button}	${id}
		Run Keyword If	'${id}' != '${EMPTY}'	Append To List	${return}	${id}
		Run Keyword If	'${id}' != '${EMPTY}'	Log	get/set ID: menu>submenu>button>${id}	INFO	console=yes
	END
	[Return]	${return}

GUI::ZPECloud::Basic::Help
	[Tags]	GUI	ZPECLOUD	HELP
	[Documentation]	Open help button
	...	== Steps Resume ==
	...	Wait appears help button; click it;
	...	== REQUIREMENTS ==
	...	ZPE Cloud Menu appearing. Can just Login: GUI::ZPECloud::Basic::Login
	...	== ARGUMENTS ==
	...	- SCREENSHOT -	boolean to capture or not a screenshot from page
	...	== EXPECTED RESULT ==
	...	Open help page
	[Arguments]	${SCREENSHOT}=${FALSE}
	${menutext_list}=	GUI::ZPECloud::Basic::Wait Until Menu Appears
	${text}=	Get From List	${menutext_list}	-2
	${text}=	Convert to Lowercase	${text}
	Should Be Equal As Strings	${text}	help
	Run Keyword If	${SCREENSHOT} != ${FALSE}	Capture Page Screenshot	filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI::ZPECloud::Basic::Logout
	[Tags]	GUI	ZPECLOUD	BROWSER	LOGOUT
	[Documentation]	Open help button
	...	== Steps Resume ==
	...	Wait appears logout button; click it; wait appears Login page
	...	== REQUIREMENTS ==
	...	ZPE Cloud Menu appearing. Can just Login: GUI::ZPECloud::Basic::Login
	...	== ARGUMENTS ==
	...	- SCREENSHOT -	boolean to capture or not a screenshot from page
	...	== EXPECTED RESULT ==
	...	Logout and return to Login page
	[Arguments]	${SCREENSHOT}=${FALSE}
	${menutext_list}=	GUI::ZPECloud::Basic::Get Menu Elements
	${text}=	Get From List	${menutext_list}	-1
	${text}=	Convert to Lowercase	${text}
	Should Be Equal As Strings	${text}	logout
	Wait Until Keyword Succeeds	30s	10ms	Click Element	xpath=//header/*//p[3]
	Run Keyword If	${SCREENSHOT} != ${FALSE}	Capture Page Screenshot	filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI::ZPECloud::Basic::Change Password
	[Tags]	GUI	ZPECLOUD	BROWSER	LOGOUT
	[Documentation]	Change password
	...	== Steps Resume ==
	...	Click username in header; click change password; insert older and new passwords; click to confirm change
	...	== REQUIREMENTS ==
	...	ZPE Cloud Menu appearing. Can just Login: GUI::ZPECloud::Basic::Login
	...	== ARGUMENTS ==
	...	- OLD -	older password
	...	- NEW -	new password to change
	...	- CONFIRM -	repeat new password to change
	...	- SCREENSHOT -	boolean to capture or not a screenshot from page
	...	== EXPECTED RESULT ==
	...	Logout and return to Login page
	[Arguments]	${OLD}	${NEW}	${CONFIRM}	${SCREENSHOT}=${FALSE}
	${user}=	Get WebElement	xpath=//header/*//p[1]
	${text}=	Get Text	${user}
	${text}=	Convert to Lowercase	${text}
	Should Not Be Equal As Strings	${user}	logout
	Should Not Be Equal As Strings	${user}	help
	Click Element	xpath=//header/*//p[1]

	${change}=	Get WebElement	xpath=//header/*//div[@id=menu-list-grow]/div/ul/li[1]
	${text}=	Get Text	${change}
	${text}=	Convert to Lowercase	${text}
	Should Be Equal As Strings	${text}	change password
	Click Element	xpath=//header/*//div[@id=menu-list-grow]/div/ul/li[1]
	Input Text	id=old_password	${OLD}
	Input Text	id=new_password	${NEW}
	Input Text	id=confirm_new_password	${CONFIRM}
	GUI::ZPECloud::Basic::Get Form Function Buttons
	Click Element	change_password
	Run Keyword If	${SCREENSHOT} != ${FALSE}	Capture Page Screenshot	filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png


### Buttons ###


GUI::ZPECloud::Basic::Set ID to All Buttons
	[Documentation]	Get all buttons (including menu); Get text from element; Set it as ID (lowercase and space as underscore)
	${buttons}=	Get WebElements	xpath=//button
#	${SPANS_ICON}=	Get WebElements	xpath=//button//span[1]
	FOR	${button}	IN	@{buttons}
		${text}=	Get Text	${button}
		${text_list}=	Split String	${text}	\n
		${text}=	Get From List	${text_list}	-1
		${id}=	Replace String	${text}	${SPACE}	_
		${id}=	Convert To Lowercase	${id}
		Assign Id To Element	${button}	${ID}
		Log	set ID: */button>${id}	INFO	console=yes
	END

GUI::ZPECloud::Basic::Get All Function Buttons
	[Documentation]	Get only buttons with ZPE Cloud Functionalities
	${buttons}=	Get WebElements	xpath=//main/*//main[1]/*//button[not(@aria-label)]
	${return}=	Create List
	FOR	${button}	IN	@{buttons}
#	\	${id}=	Get Element Attribute	${button}	id
		${text}=	Get Text	${button}
		${text_list}=	Split String	${text}	\n
		${text}=	Get From List	${text_list}	-1
		${id}=	Replace String	${text}	${SPACE}	_
		${id}=	Convert To Lowercase	${id}
		Assign Id To Element	${button}	${id}
		Run Keyword If	'${id}' != '${EMPTY}'	Append To List	${return}	${id}
		Run Keyword If	'${id}' != '${EMPTY}'	Log	get/set ID: main>button>${id}	INFO	console=yes
	END
	[Return]	${return}


### Form ###

GUI::ZPECloud::Basic::Form::Get Function Buttons
	[Documentation]	Get only buttons from forms with ZPE Cloud Functionalities
	GUI::ZPECloud::Basic::Form::Buttons::Wait Until Appears
	${buttons}=	Get WebElements	xpath=//form/*//button
	${return}=	Create List
	FOR	${button}	IN	@{buttons}
#	\	${id}=	Get Element Attribute	${button}	id
		${text}=	Get Text	${button}
		${text_list}=	Split String	${text}	\n
		${text}=	Get From List	${text_list}	-1
		${id}=	Replace String	${text}	${SPACE}	_
		${id}=	Convert To Lowercase	${id}
		Assign Id To Element	${button}	${id}
		Run Keyword If	'${id}' != '${EMPTY}'	Append To List	${return}	${id}
		Run Keyword If	'${id}' != '${EMPTY}'	Log	get/set ID: form>button>${id}	INFO	console=yes
	END
	[Return]	${return}

GUI::ZPECloud::Basic::Form::Buttons::Wait Until Appears
	Wait Until Keyword Succeeds	30s	10ms	GUI::ZPECloud::Basic::Form::Check If Exists

GUI::ZPECloud::Basic::Form::Buttons::Check If Exists
	${count}=	Get Element Count	xpath=//form
	Should Be True	${count} > 0

GUI::ZPECloud::Basic::Form::Wait Until Appears
	Wait Until Keyword Succeeds	30s	10ms	GUI::ZPECloud::Basic::Form::Check If Exists

GUI::ZPECloud::Basic::Form::Check If Exists
	${count}=	Get Element Count	xpath=//form
	Should Be True	${count} > 0


### Modal ###


GUI::ZPECloud::Basic::Modal::Get Function Buttons
	[Documentation]	Get only buttons from modals with ZPE Cloud Functionalities
	GUI::ZPECloud::Basic::Modal::Wait Until Appears
	${buttons}=	Get WebElements	xpath=//div[@role="dialog"]/*//button
	${return}=	Create List
	FOR	${button}	IN	@{buttons}
#	\	${id}=	Get Element Attribute	${button}	id
		${text}=	Get Text	${button}
		${text_list}=	Split String	${text}	\n
		${text}=	Get From List	${text_list}	-1
		${id}=	Replace String	${text}	${SPACE}	_
		${id}=	Convert To Lowercase	${id}
		Assign Id To Element	${button}	${id}
		Run Keyword If	'${id}' != '${EMPTY}'	Append To List	${return}	${id}
		Run Keyword If	'${id}' != '${EMPTY}'	Log	get/set ID: div[@role="dialog"]>button>${id}	INFO	console=yes
	END
	[Return]	${return}

GUI::ZPECloud::Basic::Modal::Wait Until Appears
	Wait Until Keyword Succeeds	30s	10ms	GUI::ZPECloud::Basic::Modal::Should Exists

GUI::ZPECloud::Basic::Modal::Wait Until Disappears
	Wait Until Keyword Succeeds	30s	10ms	GUI::ZPECloud::Basic::Modal::Should Not Exists

GUI::ZPECloud::Basic::Modal::Return If Exists
	${return}=	Run Keyword And Return Status	GUI::ZPECloud::Basic::Modal::Should Exists
	[Return]	${return}

GUI::ZPECloud::Basic::Modal::Should Exists
	${count}=	Get Element Count	xpath=//div[@role="dialog"]
	Should Be True	${count} > 0

GUI::ZPECloud::Basic::Modal::Should Not Exists
	Capture Page Screenshot	filename=${SCREENSHOTDIR}{index:06}.png
	${count}=	Get Element Count	xpath=//div[@role="dialog"]
	Should Be True	${count} == 0


### Table ###


GUI::ZPECloud::Basic::Set Tables ID
	[Documentation]	Set Table ID using name of submenu
	${table_list}=	Get WebElements	xpath=//table
	${submenu_selected}=	Get WebElement	xpath=//header[2]/*//button[@aria-selected='true']
	${text}=	Get Text	${submenu_selected}
	${text_list}=	Split String	${text}	\n
	${text}=	Get From List	${text_list}	-1
	${id}=	Replace String	${text}	${SPACE}	_
	${id}=	Convert To Lowercase	${id}
	${count}=	Evaluate	0
	FOR	${table}	IN	@{table_list}
		Assign Id To Element	${table}	${id}_${count}
		Log	set ID: table>${id}_${count}	INFO	console=yes
		${count}=	Evaluate	${count}+1
	END
	[Return]	${table_list}

GUI::ZPECloud::Basic::Set Table Rows ID
	[Documentation]	Set rows' table ID using first field
	[Arguments]	${TABLEID}	${SET_TABLESID}=${FALSE}
	Run Keyword If	${SET_TABLESID}	GUI::ZPECloud::Basic::Set Tables ID
	GUI::ZPECloud::Basic::Wait Until Loaded Table	${TABLEID}
	${rows_list}=	Get WebElements	xpath=//table[@id='${TABLEID}']/tbody/tr
	FOR	${row}	IN	@{rows_list}
		${text_row}=	Get Text	${row}
		${text_list}=	Split String	${text_row}	\n
		${text}=	Get From List	${text_list}	0
		${id}=	Replace String	${text}	${SPACE}	_
		${id}=	Convert To Lowercase	${id}
		Assign Id To Element	${row}	${id}
		Log	set ID: table>tbody>tr: ${id}	INFO	console=yes
	END

GUI::ZPECloud::Basic::Get First Line Text
	[Arguments]	${TABLEID}	${SET_TABLESID}=${FALSE}
	Run Keyword If	${SET_TABLESID}	GUI::ZPECloud::Basic::Set Tables ID
	${first_row}=	Get WebElement	xpath=//table[@id='${TABLEID}']/tbody/tr[1]
	${text}=	Get Text	${first_row}
	[Return]	${text}

GUI::ZPECloud::Basic::Should Be Loaded Table
	[Arguments]	${TABLEID}	${SET_TABLESID}=${FALSE}
	${text}=	GUI::ZPECloud::Basic::Get First Line Text	${TABLEID}	${SET_TABLESID}
	${should}=	Should Not Be Equal As Strings	${text}	Loading table data...
	[Return]	${should}

GUI::ZPECloud::Basic::Wait Until Loaded Table
	[Arguments]	${TABLEID}	${SET_TABLESID}=${FALSE}
	Wait Until Keyword Succeeds	30s	10ms	GUI::ZPECloud::Basic::Should Be Loaded Table	${TABLEID}	${SET_TABLESID}

GUI::ZPECloud::Basic::Select Table Row
	[Arguments]	${TABLEID}	${ID}	${SET_ROWSID}=${FALSE}	${SET_TABLESID}=${FALSE}
	${selected}=	Set Variable	${FALSE}
	${id_lower}=	Replace String	${ID}	${SPACE}	_
	${id_lower}=	Convert To Lowercase	${id_lower}
	FOR	${index}	IN RANGE	0	999
		Run Keyword If	${SET_ROWSID}	GUI::ZPECloud::Basic::Set Table Rows ID	${TABLEID}	${SET_TABLESID}
		${count}=	Get Element Count	xpath=//table[@id="${TABLEID}"]/tbody/tr[@id="${id_lower}"]
		Run Keyword If	'${count}' != '0'	Select Checkbox	//*[@id="${TABLE_ID}"]/tbody/tr[@id="${id_lower}"]//input[@type="checkbox"]
		Return From Keyword If	'${count}' != '0'	${TRUE}
		${is_last_page}=	GUI::ZPECloud::Basic::Return If Is Disabled	xpath=//table[@id="${TABLEID}"]//tfoot//button[@aria-label="Next Page"]
		Exit For Loop If	${is_last_page} == ${TRUE} and '${count}' == '0'
		Click Element	xpath=//table[@id="${TABLEID}"]//tfoot//button[@aria-label="Next Page"]
	END
	[Return]	${selected}

###################################
GUI::ZPECloud::Basic::Select Table Rows
	[Arguments]	${TABLEID}	${IDS}	${SET_ROWSID}=${FALSE}	${SET_TABLESID}=${FALSE}

	FOR	${ID}	IN	@{IDS}
	${id_lower}=	Replace String	${ID}	${SPACE}	_
	${id_lower}=	Convert To Lowercase	${id_lower}
		Run Keyword If	${SET_ROWSID}	GUI::ZPECloud::Basic::Set Table Rows ID	${TABLEID}	${SET_TABLESID}
		${count}=	Get Element Count	xpath=//table[@id="${TABLEID}"]/tbody/tr[@id="${id_lower}"]
		Run Keyword If	'${count}' != '0'	Select Checkbox	//*[@id="${TABLE_ID}"]/tbody/tr[@id="${id_lower}"]//input[@type="checkbox"]
		${is_last_page}=	GUI::ZPECloud::Basic::Return If Is Disabled	xpath=//table[@id="${TABLEID}"]//tfoot//button[@aria-label="Next Page"]
		Click Element	xpath=//table[@id="${TABLEID}"]//tfoot//button[@aria-label="Next Page"]
	END

GUI::ZPECloud::Basic::Table::Get Count Rows
	[Arguments]	${TABLEID}	${SET_TABLESID}=${FALSE}
	${count}=	Set Variable	0
	Run Keyword If	${SET_TABLESID} == ${TRUE}	GUI::ZPECloud::Basic::Set Tables ID
	${th_exists}=	Run Keyword And Return Status	Get Webelement	xpath=//table[@id="${TABLEID}"]//tbody//tr[1]//th
	${text}=	Run Keyword If	${th_exists}	Get Text	xpath=//table[@id="${TABLEID}"]//tbody//tr[1]//th
	${text}=	Run Keyword If	${th_exists}	Convert To Lowercase	${text}
	Return From Keyword If	'${text}' == 'no result found'	${count}
	FOR	${index}	IN RANGE	0	999
		${rowcount}=	Get Element Count	xpath=//table[@id="${TABLEID}"]//tbody//tr	# JUST TO KNOW LINES
		${count}=	Evaluate	${count} + ${rowcount}
		${go_next}=	GUI::ZPECloud::Basic::Return If Is Enabled	xpath=//table[@id="${TABLEID}"]//tfoot//button[@aria-label="Next Page"]
		Run Keyword If	${go_next}	Click Element	xpath=//table[@id="${TABLEID}"]//tfoot//button[@aria-label="Next Page"]
		Exit For Loop If	${go_next} == ${FALSE}
	END
	${go_first}=	GUI::ZPECloud::Basic::Return If Is Enabled	xpath=//table[@id="${TABLEID}"]//tfoot//button[@aria-label="First Page"]
	Run Keyword If	${go_first}	Click Element	xpath=//table[@id="${TABLEID}"]//tfoot//button[@aria-label="First Page"]
	[Return]	${count}

GUI::ZPECloud::Basic::Table::Get Count Rows Element ID
	[Arguments]	${TABLEID}	${ID}	${SET_ROWSID}=${FALSE}	${SET_TABLESID}=${FALSE}
	${count}=	Set Variable	0
	${id_lower}=	Replace String	${ID}	${SPACE}	_
	${id_lower}=	Convert To Lowercase	${id_lower}
	FOR	${index}	IN RANGE	0	999
		Run Keyword If	${SET_ROWSID}	GUI::ZPECloud::Basic::Set Table Rows ID	${TABLEID}	${SET_TABLESID}
		${rowcount}=	Get Element Count	xpath=//table[@id="${TABLEID}"]//tbody//tr	# JUST TO KNOW LINES
		${page_count}=	Get Element Count	xpath=//table[@id="${TABLEID}"]//tbody//tr[@id="${id_lower}"]
		${count}=	Evaluate	${count} + ${page_count}
		${is_last_page}=	GUI::ZPECloud::Basic::Return If Is Disabled	xpath=//table[@id="${TABLEID}"]//tfoot//button[@aria-label="Next Page"]
		Exit For Loop If	${is_last_page} == ${TRUE}
		Click Element	xpath=//table[@id="${TABLEID}"]//tfoot//button[@aria-label="Next Page"]
	END
	${goto_first_page}=	GUI::ZPECloud::Basic::Return If Is Enabled	xpath=//table[@id="${TABLEID}"]//tfoot//button[@aria-label="First Page"]
	Run Keyword If	${goto_first_page}	Click Element	xpath=//table[@id="${TABLEID}"]//tfoot//button[@aria-label="First Page"]
	[Return]	${count}

GUI::ZPECloud::Basic::Table::Delete
	[Arguments]	${TABLEID}	${ID}	${SET_ROWSID}=${FALSE}	${SET_TABLESID}=${FALSE}	${RAW_MODE}=${FALSE}
	GUI::ZPECloud::Basic::Select Table Row	${TABLEID}	${ID}	${TRUE}
	GUI::ZPECloud::Basic::Should Be Enabled	delete
	GUI::ZPECloud::Basic::Get All Function Buttons
	GUI::ZPECloud::Basic::Click Button	delete
	GUI::ZPECloud::Basic::Modal::Get Function Buttons
	GUI::ZPECloud::Basic::Modal::Click Button	delete
	GUI::ZPECloud::Basic::Modal::Wait Until Disappears
	GUI::ZPECloud::Basic::Wait Until Loaded Table	${TABLEID}
	Reload Page
	Run Keyword If	${RAW_MODE} == ${FALSE}	GUI::ZPECloud::Basic::Table::Should Not Exist Row	${TABLEID}	${ID}

GUI::ZPECloud::Basic::Table::Delete If Exists
	[Arguments]	${TABLEID}	${ID}	${SET_ROWSID}=${FALSE}	${SET_TABLESID}=${FALSE}	${RAW_MODE}=${FALSE}
	${is_selected}=	GUI::ZPECloud::Basic::Select Table Row	${TABLEID}	${ID}	${TRUE}
	Return From Keyword If	${is_selected} == ${FALSE}	${is_selected}
	GUI::ZPECloud::Basic::Get All Function Buttons
	${isnot_delete}=	GUI::ZPECloud::Basic::Return If Is Disabled	delete
	Return From Keyword If	${isnot_delete}	${isnot_delete}
	GUI::ZPECloud::Basic::Get All Function Buttons
	GUI::ZPECloud::Basic::Click Button	delete
	GUI::ZPECloud::Basic::Modal::Get Function Buttons
	GUI::ZPECloud::Basic::Modal::Click Button	delete
	GUI::ZPECloud::Basic::Wait Until Loaded Table	${TABLEID}
	Run Keyword If	${RAW_MODE} == ${FALSE}	wait until keyword succeeds	30	2	GUI::ZPECloud::Basic::Table::Should Not Exist Row	${TABLEID}	${ID}


GUI::ZPECloud::Basic::Table::Should Exists Row
	[Arguments]	${TABLEID}	${ID}
	Reload Page
	${count}=	GUI::ZPECloud::Basic::Table::Get Count Rows Element ID	${TABLEID}	${ID}	${TRUE}	${TRUE}
	Should Be True	${count} > 0

GUI::ZPECloud::Basic::Table::Should Not Exist Row
	[Arguments]	${TABLEID}	${ID}
	${count}=	GUI::ZPECloud::Basic::Table::Get Count Rows Element ID	${TABLEID}	${ID}	${TRUE}	${TRUE}
	Should Be True	${count} == 0

GUI::ZPECloud::Basic::Table::Row::Should Be Loaded
	[Arguments]	${TABLEID}	${ID}
	GUI::ZPECloud::Basic::Set Table Rows ID	${TABLEID}
	${id_lower}=	Replace String	${ID}	${SPACE}	_
	${id_lower}=	Convert To Lowercase	${id_lower}
	${count}=	Get Element Count	xpath=//table[@id="${TABLEID}"]//tbody//tr[@id="${id_lower}"]
	${should}=	Should Be True	${count} == 1
	[Return]	${should}

GUI::ZPECloud::Basic::Table::Row::Wait Until Loaded
	[Arguments]	${TABLEID}	${ID}
	Wait Until Keyword Succeeds	30s	10ms	GUI::ZPECloud::Basic::Table::Row::Should Be Loaded	${TABLEID}	${ID}

GUI::ZPECloud::Basic::Table::Search::Should Not Exist
	[Arguments]	${TABLEID}
	${should}=	Run Keyword And Return Status	GUI::ZPECloud::Basic::Table::Search::Return If Not Exist	${TABLEID}
	Should Be True	${should}
	[Return]	${should}

GUI::ZPECloud::Basic::Table::Search::Return If Not Exist
	[Arguments]	${TABLEID}
	GUI::ZPECloud::Basic::Set Table Rows ID	${TABLEID}
	${first_line}=	Get WebElement	xpath=//table[@id="${TABLEID}"]//tbody//tr[1]
	${text}=	Get Text	${first_line}
	${should}=	Should Be Equal As Strings	${text}	No result found
	[Return]	${should}

GUI::ZPECloud::Basic::Table::Select If Exist First Position
	[Arguments]	${TABLEID}	${ID}	${SET_ROWSID}=${FALSE}	${SET_TABLESID}=${FALSE}	${RAW_MODE}=${FALSE}
	${is_selected}=	GUI::ZPECloud::Basic::Select Table Row	${TABLEID}	${ID}	${TRUE}
	Return From Keyword If	${is_selected} == ${FALSE}	${is_selected}


GUI::ZPECloud::Basic::Table::Select If Exist item
	[Arguments]	${TABLEID}	${ID}	${SET_ROWSID}=${FALSE}	${SET_TABLESID}=${FALSE}	${RAW_MODE}=${FALSE}
	${is_selected}=	GUI::ZPECloud::Basic::Table::Select Table Row item	${TABLEID}	${ID}	${TRUE}
	Return From Keyword If	${is_selected} == ${FALSE}	${is_selected}

GUI::ZPECloud::Basic::Table::Search item
	[Arguments]	${TABLEID}	${ID}	${SET_ROWSID}=${FALSE}	${SET_TABLESID}=${FALSE}	${RAW_MODE}=${FALSE}
	${is_selected}=	GUI::ZPECloud::Basic::Table::Select Table Row item	${TABLEID}	${ID}	${TRUE}
	[Return]	${is_selected}

GUI::ZPECloud::Basic::Table::Select Table Row item
	[Arguments]	${TABLEID}	${ID}	${SET_ROWSID}=${FALSE}	${SET_TABLESID}=${FALSE}
	${selected}=	Set Variable	${FALSE}
	${id_lower}=	Replace String	${ID}	${SPACE}	_
	${id_lower}=	Convert To Lowercase	${id_lower}
	FOR	${index}	IN RANGE	0	999
		Run Keyword If	${SET_ROWSID}	GUI::ZPECloud::Basic::Table::Set Table Rows ID item	${TABLEID}	${SET_TABLESID}
		${count}=	Get Element Count	xpath=//table[@id="${TABLEID}"]/tbody/tr[@id="${id_lower}"]
		Run Keyword If	'${count}' != '0'	Select Checkbox	//*[@id="${TABLE_ID}"]/tbody/tr[@id="${id_lower}"]//input[@type="checkbox"]
		Return From Keyword If	'${count}' != '0'	${TRUE}
		${is_last_page}=	GUI::ZPECloud::Basic::Return If Is Disabled	xpath=//table[@id="${TABLEID}"]//tfoot//button[@aria-label="Next Page"]
		Exit For Loop If	${is_last_page} == ${TRUE} and '${count}' == '0'
		Click Element	xpath=//table[@id="${TABLEID}"]//tfoot//button[@aria-label="Next Page"]
	END
	${goto_first_page}=	GUI::ZPECloud::Basic::Return If Is Enabled	xpath=//table[@id="${TABLEID}"]//tfoot//button[@aria-label="First Page"]
	Run Keyword If	${goto_first_page}	Click Element	xpath=//table[@id="${TABLEID}"]//tfoot//button[@aria-label="First Page"]
	[Return]	${selected}

GUI::ZPECloud::Basic::Table::Set Table Rows ID item
	[Documentation]	Set rows' table ID using second field
	[Arguments]	${TABLEID}	${SET_TABLESID}=${FALSE}
	Run Keyword If	${SET_TABLESID}	GUI::ZPECloud::Basic::Set Tables ID
	GUI::ZPECloud::Basic::Wait Until Loaded Table	${TABLEID}
	${rows_list}=	Get WebElements	xpath=//table[@id='${TABLEID}']/tbody/tr
	${text_row}=	Get Text	${rows_list}[0]
	${text_row}=	Remove String	${text_row}	\n
	Return From Keyword If	'${text_row}' == 'No result found'
	FOR	${row}	IN	@{rows_list}
		${text_row}=	Get Text	${row}
		${text_list}=	Split String	${text_row}	\n
		${text}=	Get From List	${text_list}	1
		${id}=	Replace String	${text}	${SPACE}	_
		${id}=	Convert To Lowercase	${id}
		Assign Id To Element	${row}	${id}
		Log	set ID: table>tbody>tr: ${id}	INFO	console=yes
	END


### Should ###


GUI::ZPECloud::Basic::Element Status Should Be Disabled 
	[Arguments]	${element_text}
	Run Keyword And Return Status	Element Should Be Visible	//*[text()='${element_text}' and contains(@class, 'disabled')]

GUI::ZPECloud::Basic::Return If Is Enabled
	[Documentation]	If disabled attribute is visible returns true
	[Arguments]	${locator}
	${element}=	Get WebElement	${locator}
	${class}=	Get Element Attribute	${locator}	class
	${is_nodisabled_class}=	Run Keyword And Return Status	Should Not Contain	${class}	Mui-disabled
	${return}=	Run Keyword And Return Status	Should Be True	${is_nodisabled_class}
	[Return]	${return}

GUI::ZPECloud::Basic::Return If Is Disabled
	[Documentation]	If disabled attribute is invisible returns true
	...	INVESTIGATING FAILURE
	[Arguments]	${locator}
	${disabled}=	Get Element Attribute	${locator}	disabled
	${class}=	Get Element Attribute	${locator}	class
	${is_disabled_class}=	Run Keyword And Return Status	Should Contain	${class}	Mui-disabled
	${return}=	Run Keyword And Return Status	Should Be True	'${disabled}' == 'true' or ${is_disabled_class}
	[Return]	${return}

GUI::ZPECloud::Basic::Should Be Enabled
	[Documentation]	If exists returns true
	[Arguments]	${locator}
	${is_visible}=	GUI::ZPECloud::Basic::Return If Is Enabled	${locator}
	${should}=	Should Be True	${is_visible}
	[Return]	${should}

GUI::ZPECloud::Basic::Should Not Be Enabled
	[Documentation]	If not exists returns true
	[Arguments]	${locator}
	${is_invisible}=	GUI::ZPECloud::Basic::Return If Is Disabled	${locator}
	${should}=	Should Be True	${is_invisible}
	[Return]	${should}

GUI::ZPECloud::Basic::Should Be Disabled
	[Documentation]	If not exists returns true
	[Arguments]	${locator}
	${is_invisible}=	GUI::ZPECloud::Basic::Return If Is Disabled	${locator}
	${should}=	Should Be True	${is_invisible}
	[Return]	${should}

GUI::ZPECloud::Basic::Should Not Be Disabled
	[Documentation]	If exists returns true
	[Arguments]	${locator}
	${is_visible}=	GUI::ZPECloud::Basic::Return If Is Enabled	${locator}
	${should}=	Should Be True	${is_visible}
	[Return]	${should}



### Open Tab ###
GUI::ZPECloud::Devices::Open Tab
	[Documentation]	Click in Devices Menu Tab
	${menu}=	GUI::ZPECloud::Basic::Get Main Menu
	${return}=	GUI::ZPECloud::Basic::Click Button	devices
	GUI::Basic::Spinner Should Be Invisible
	[Return]	${return}

GUI::ZPECloud::Settings::Open Tab
	[Documentation]	Click in Devices Menu Tab
	${menu}=	GUI::ZPECloud::Basic::Get Main Menu
	${return}=	GUI::ZPECloud::Basic::Click Button	settings
	GUI::Basic::Spinner Should Be Invisible
	[Return]	${return}

GUI::ZPECloud::Schedules::Open Tab
	[Documentation]	Click in Schedules Menu Tab
	${menu}=	GUI::ZPECloud::Basic::Get Main Menu
	${return}=	GUI::ZPECloud::Basic::Click Button	schedules
	GUI::Basic::Spinner Should Be Invisible
	[Return]	${return}

GUI::ZPECloud::${MenuOption}::${SubmenuOption}::Open Tab
	[Documentation]	Click in Menu::Submenu Tab
	${menu}=	GUI::ZPECloud::Basic::Get Main Menu
	${MenuOption}=	Convert To Lowercase	${MenuOption}
	GUI::ZPECloud::Basic::Click Button	${MenuOption}
	GUI::ZPECloud::Basic::Should Be Enabled	${MenuOption}

	${menu}=	GUI::ZPECloud::Basic::Get Sub Menu
	${SubmenuOption}=	Convert To Lowercase	${SubmenuOption}
	${return}=	GUI::ZPECloud::Basic::Click Button	${SubmenuOption}
#	GUI::ZPECloud::Basic::Should Be Enabled	${SubmenuOption}
	GUI::ZPECloud::Basic::Get All Function Buttons
	Wait Until Keyword Succeeds	10	1	GUI::ZPECloud::Basic::Set Tables ID
	GUI::Basic::Spinner Should Be Invisible
	[Return]	${return}

#GUI::ZPECloud::Companies::General::Open Tab
#	[Documentation]	Click in Companies::General Menu Tab
#	Click Element	companies
#	${submenu}=	GUI::ZPECloud::Basic::Get Sub Menu
#	${return}	Click Element	general
#	[Return]	${return}

GUI::ZPECloud::Companies::Open Tab
	[Documentation]	Click in Companies Menu Tab
	${menu}=	GUI::ZPECloud::Basic::Get Main Menu
	${return}=	GUI::ZPECloud::Basic::Click Button	companies
	[Return]	${return}

GUI::ZPECloud::Profiles::Open Tab
	[Documentation]	Click in Profile Menu Tab
	${menu}=	GUI::ZPECloud::Basic::Get Main Menu
	${return}=	GUI::ZPECloud::Basic::Click Button	profiles
	[Return]	${return}

GUI::ZPECloud::Apps::Open Tab
	[Documentation]	Click in Apps Menu Tab
	${menu}=	GUI::ZPECloud::Basic::Get Main Menu
	${return}=	GUI::ZPECloud::Basic::Click Button	apps
	[Return]	${return}

GUI::ZPECloud::Users::Open Tab
	[Documentation]	Click in Users Menu Tab
	${menu}=	GUI::ZPECloud::Basic::Get Main Menu
	${return}=	GUI::ZPECloud::Basic::Click Button	users
	[Return]	${return}

#GUI::ZPECloud::Profiles::Configuration::Open Tab
#	[Documentation]	Click in Profile::Configuration Menu Tab
#	Click Element	devices
#	${submenu}=	GUI::ZPECloud::Basic Get Sub Menu
#	${return}	Click Element	general
#	[Return]	${return}
#
#GUI::ZPECloud::Profiles::Script::Open Tab
#	[Documentation]	Click in Profile::Scripts Menu Tab
#	Click Element	devices
#	${submenu}=	GUI::ZPECloud::Basic Get Sub Menu
#	${return}	Click Element	general
#	[Return]	${return}
#
#GUI::ZPECloud::Profiles::Software::Open Table
#	[Documentation]	Click in Profile::Software Menu Tab
#	Click Element	devices
#	${submenu}=	GUI::ZPECloud::Basic Get Sub Menu
#	${return}	Click Element	general
#	[Return]	${return}


### Users ###

GUI::ZPECloud::Users::General::Add
	[Arguments]	${CUSTOMER_USERNAME}	${SUPER_PASSWORD}	${FIRST_NAME}	${LAST_NAME}	${CUSTOMER_EMAIL}	${CUSTOMER_NUMBER}	${COMPANY_NAME}
	GUI::ZPECloud::Users::General::Open Tab
	GUI::ZPECloud::Basic::Table::Delete If Exists	general_0	${CUSTOMER_USERNAME}
	GUI::ZPECloud::Basic::Click Button	add
	Input Text	xpath=//input[contains(@name, 'username')]	${CUSTOMER_USERNAME}
	Input Text	xpath=//input[@id='filled-adornment-password']	${SUPER_PASSWORD}
	Input Text	xpath=//input[contains(@name, 'first_name')]	${FIRST_NAME}
	Input Text	xpath=//input[contains(@name, 'last_name')]	${LAST_NAME}
	Input Text	xpath=//input[contains(@name, 'email')]	${CUSTOMER_EMAIL}
	Input Text	xpath=//input[contains(@name, 'phone_number')]	${CUSTOMER_NUMBER}

	Input Text	xpath=//input[contains(@placeholder, 'Search company')]	${COMPANY_NAME}
	Sleep	3
	Click Element	xpath=//span[contains(@style, 'font-weight: 300')]
	Sleep	3

	Click Element	xpath=//div[contains(@aria-labelledby, 'mui-component-select-type')]
	Sleep	3
	Click Element	xpath=//li[@data-value='CA']
	Sleep	3
	GUI::ZPECloud::Basic::Modal::Click Button	add_user
	Wait Until Keyword Succeeds	15x	500ms	Wait Until Element Is Visible	xpath=//span[@id='client-snackbar']
	Wait Until Keyword Succeeds	15x	500ms	Element Text Should Be	xpath=//span[@id='client-snackbar']	User added successfully


GUI::ZPECloud::Users::General::Edit
	[Arguments]	${FIRST_NAME}	${LAST_NAME}	${CONTACT}	${CUSTOMER_USERNAME}
	GUI::ZPECloud::Users::General::Open Tab
	GUI::ZPECloud::Basic::Table::Delete If Exists	general_0	${CUSTOMER_USERNAME}
	GUI::ZPECloud::Basic::Click Button	edit
	Input Text	xpath=//input[contains(@name, 'first_name')]	${FIRST_NAME}
	Input Text	xpath=//input[contains(@name, 'last_name')]	${LAST_NAME}
	Input Text	xpath=//input[contains(@name, 'phone_number')]	${CONTACT}
	GUI::ZPECloud::Basic::Modal::Click Button	edit_user
	Wait Until Keyword Succeeds	15x	500ms	Wait Until Element Is Visible	xpath=//span[@id='client-snackbar']
	Element Text Should Be	xpath=//span[@id='client-snackbar']	User details edited successfully

### Sites ###

GUI::ZPECloud::Sites::Open Tab
	[Documentation]	Click in SITE Menu Tab
	${menu}=	GUI::ZPECloud::Basic::Get Main Menu
	${return}	GUI::ZPECloud::Basic::Click Button	sites
	[Return]	${return}

GUI::ZPECloud::Sites::Devices::Open Tab
	[Documentation]	Click in SITE::DEVICES Menu Tab
	${menu}=	GUI::ZPECloud::Basic::Get Main Menu
	GUI::ZPECloud::Basic::Click Button	sites
	${return}	GUI::ZPECloud::Basic::Click Button	xpath=//button[@id='ca-header-tab-2-1']
	[Return]	${return}

### Groups ###

GUI::ZPECloud::Groups::Open Tab
	[Documentation]	Click in Group Menu Tab
	${menu}=	GUI::ZPECloud::Basic::Get Main Menu
	${return}	GUI::ZPECloud::Basic::Click Button	groups
	GUI::Basic::Spinner Should Be Invisible
	[Return]	${return}

GUI::ZPECloud::Groups::Devices::Open Tab
	[Documentation]	Click in GROUPS::DEVICES Menu Tab
	${menu}=	GUI::ZPECloud::Basic::Get Main Menu
	GUI::ZPECloud::Basic::Click Button	groups
	${return}	GUI::ZPECloud::Basic::Click Button	xpath=//button[@id='ca-header-tab-2-1']
	GUI::Basic::Spinner Should Be Invisible
	[Return]	${return}

GUI::ZPECloud::Groups::Users::Open Tab
	[Documentation]	Click in GROUPS::USERS Menu Tab
	${menu}=	GUI::ZPECloud::Basic::Get Main Menu
	GUI::ZPECloud::Basic::Click Button	groups
	${return}	GUI::ZPECloud::Basic::Click Button	xpath=//button[@id='ca-header-tab-2-2']
	GUI::Basic::Spinner Should Be Invisible
	[Return]	${return}

### Profiles ###

GUI::ZPECloud::Profiles::Configuration::Add
	[Arguments]	${NAME}	${DESCRIPTION}	${MYPATH}	${CONFIGURATIONADDED}=Configuration added successfully
	GUI::ZPECloud::Profiles::Open Tab
	sleep	2s
	Click Element	xpath=//div[2]/div/button[2]
	sleep    2s
	Wait Until Page Contains Element	//input[contains(@name,'name')]	15s
	Input Text	//input[contains(@name,'name')]	${NAME}
	Input Text	//input[contains(@name,'description')]	${DESCRIPTION}
	sleep	2s
	Click Element	mui-component-select-type
	Click Element	opt-CONFIGURATION
	Choose File	fileinput-file	${MYPATH}
	Wait Until Keyword Succeeds	30	6	GUI::ZPECloud::Profiles::Configuration::Save	${NAME}

GUI::ZPECloud::Profiles::Configuration::Save
	[Arguments]	${NAME}	${ADDEDLIST}=//*[@id="ca-profiles-config-table-1"]	${CONFIGURATIONADDED}=Configuration added successfully
	Click Element	//button[contains(.,'Save')]
	Wait Until Page Contains	${CONFIGURATIONADDED}
	Sleep	5
	Wait Until Page Contains Element	${ADDEDLIST}
	Input Text	search-input	${NAME}
	Sleep	5
	Table Should Contain	${ADDEDLIST}	${NAME}

GUI::ZPECloud::Profiles::Script::Add
	[Arguments]	${NAME}	${DESCRIPTION}	${MYPATH}
	GUI::ZPECloud::Profiles::Open Tab
	sleep	2s
	Click Element	xpath=//div[2]/div/button[2]
	Wait Until Page Contains Element	//input[contains(@name,'name')]	15s
	sleep     2s
	#Input Text	name=name	${NAME}
	#Input Text	name=description	${DESCRIPTION}
	Input Text	//input[contains(@name,'name')]	${NAME}
	Input Text	//input[contains(@name,'description')]	${DESCRIPTION}
	sleep	2s
	Click Element	mui-component-select-type
	Click Element	opt-SCRIPT
	Choose File	fileinput-file	${MYPATH}
	Wait Until Keyword Succeeds	30	6	GUI::ZPECloud::Profiles::Configuration::Save	${NAME}

GUI::ZPECloud::Profiles::Template::Add
	[Arguments]	${CODE}	${NAME}	${DESCRIPTION}	${TYPESELECTION}=//*[@id="template-type-option-0"]	${CONFIGURATIONADDED}=Template added successfully
	GUI::ZPECloud::Profiles::Template::Open Tab
	Sleep	5
	GUI::ZPECloud::Basic::Click Button	add
	Sleep	5
	Input Text	template-code-input	${CODE}
	Input Text	template-name-input	${NAME}
	Input Text	template-description-select	${DESCRIPTION}
	Click Element	template-type-select
	Click Element	${TYPESELECTION}
	Wait Until Element Is Enabled	//*[@id="function-button-1"]
	Wait Until Keyword Succeeds	30	6	Click Element	//*[@id="function-button-1"]
	Wait Until Page Contains	${CONFIGURATIONADDED}
	Sleep	5
	Wait Until Page Contains Element	//*[@id="customer-admin-layout"]/main/div[1]/div[2]/div[3]/div[1]/div/table
	Table Should Contain	//*[@id="customer-admin-layout"]/main/div[1]/div[2]/div[3]/div[1]/div/table	${NAME}

### Companies ###
GUI::ZPECloud::Companies::General::Add
	[Arguments]	${BUSINNESS_NAME}	${ADDRESS}	${CONTACT_INFO}
	GUI::ZPECloud::Companies::General::Open Tab
	GUI::ZPECloud::Basic::Table::Delete If Exists	general_0	${BUSINNESS_NAME}
	GUI::ZPECloud::Basic::Click Button	add
	Input Text	name=business_name	${BUSINNESS_NAME}
	Input Text	name=address	${ADDRESS}
	Input Text	name=contact_info	${CONTACT_INFO}
	GUI::ZPECloud::Basic::Modal::Click Button	add_company
	Wait Until Keyword Succeeds	15x	500ms	Wait Until Element Is Visible	xpath=//span[@id='client-snackbar']
	Wait Until Keyword Succeeds	15x	500ms	Element Text Should Be	xpath=//span[@id='client-snackbar']	Company added successfully
	Reload Page
	GUI::ZPECloud::Basic::Table::Should Exists Row	general_0	${BUSINNESS_NAME}

GUI::ZPECloud::Form::Cancel
	GUI::ZPECloud::Basic::Form::Get Function Buttons
	Click Element	cancel

GUI::ZPECloud::Cancel
	GUI::ZPECloud::Basic::Set ID to All Buttons
	Click Element	cancel

GUI::ZPECloud::Form::Delete
	GUI::ZPECloud::Basic::Form::Get Function Buttons
	Click Element	delete

GUI::ZPECloud::Modal::Delete
	GUI::ZPECloud::Basic::Modal::Get Function Buttons
	Click Element	div[@role="dialog"]/*//button[@id="delete"]

GUI::ZPECloud::Delete
	GUI::ZPECloud::Basic::Set ID to All Buttons
	Click Element	delete

GUI::ZPECloud::Basic::Search::Clear
	Input Text	xpath=//input[@type='text']	${EMPTY}

GUI::ZPECloud::Basic::Click Button
	[Arguments]	${ELEMENT}
	GUI::ZPECloud::Basic::Wait Until Menu Appears
	${buttons}=	GUI::ZPECloud::Basic::Get All Function Buttons
	Log	----->>>${ELEMENT}<<<-----	console=yes
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	${ELEMENT}	60s
	Wait Until Keyword Succeeds	30s	3s	Click Element	${ELEMENT}

GUI::ZPECloud::Basic::Modal::Click Button
	[Arguments]	${ELEMENT}
	${buttons}=	GUI::ZPECloud::Basic::Modal::Get Function Buttons
	GUI::ZPECloud::Basic::Modal::Wait Until Appears
	Log	----->>>${ELEMENT}<<<-----	console=yes
	Wait Until Keyword Succeeds	30s	10ms	Click Element	xpath=//div[@role="dialog"]/*//button[@id="${ELEMENT}"]
	GUI::ZPECloud::Basic::Modal::Wait Until Disappears
#BACKSPACEn


### Dashboard ###

#GUI::ZPECloud::Dashboard::Get Marked Offline Places
#	${OUTPUT}=	Get Webelements	xpath=//smsmkds//img[]
#	${COUNT}=	Get Count //fvsdffd//img[src='offline.png']
#
#GUI::ZPECloud::Dashboard::Get Marked Online Places
#	${OUTPUT}=	Get Webelements	xpath=//smsmkds//img[]
#	${COUNT}=	Get Count //fvsdffd//img[src='online.png']


### Sites ###

GUI::ZPECloud::Sites::General::Add
	[Arguments]	${NAME}	${ADDRESS}	${LATITUDE}	${LONGITUDE}
	GUI::ZPECloud::Sites::General::Open Tab
	GUI::ZPECloud::Basic::Table::Delete If Exists	general_0	${NAME}
	GUI::ZPECloud::Basic::Click Button	add
	Input Text	name=name	${NAME}
	Input Text	name=address	${ADDRESS}
	Input Text	name=latitude	${LATITUDE}
	Input Text	name=longitude	${LONGITUDE}
	GUI::ZPECloud::Basic::Modal::Click Button	add_site
	GUI::ZPECloud::Basic::Table::Should Exists Row	general_0	${NAME}

### Devices ###
GUI::ZPECloud::Devices::Associate devices with a company
	[Arguments]	${SUPER_USERNAME}	${SUPER_PASSWORD}	${COMPANY_NAME}	${DEVICE_SERIAL}
	GUI::ZPECloud::Basic::Open And Login	${SUPER_USERNAME}	${SUPER_PASSWORD}
	GUI::ZPECloud::Devices::General::Open Tab
	${elements}=	Get WebElements	xpath=//span[contains(@style, 'cursor: pointer; white-space: nowrap;')]
	${element}=	Get From List	${elements}	0
	Click Element	${element}
	GUI::ZPECloud::Basic::Wait Until Loaded Table	general_0
	Click Element	xpath=//input[contains(@placeholder, 'Search hostname, serial no, sitename')]
	Press Keys	xpath=//input[contains(@placeholder, 'Search hostname, serial no, sitename')]	${DEVICE_SERIAL}
	Sleep	2
	GUI::ZPECloud::Basic::Table::Select If Exist First Position	general_0	${DEVICE_SERIAL}
	Sleep	2
	Click Element	xpath=//button[contains(@style, 'min-width: 100px')]
	Input Text	xpath=//input[contains(@placeholder, 'Search company')]	${COMPANY_NAME}
	Sleep	2
	Click Element	xpath=//li[contains(@id, 'react-autowhatever-1--item-0')]
	Click Element	xpath=//button[contains(@type, 'submit')]
	Wait Until Keyword Succeeds	15x	500ms	Wait Until Element Is Visible	xpath=//span[@id='client-snackbar']
	Wait Until Keyword Succeeds	15x	500ms	Element Text Should Be	xpath=//span[@id='client-snackbar']	Device associated
#	GUI::ZPECLoud::Basic::Logout


GUI::ZPECloud::Transfer Ownership
	[Arguments]	${CUSTOMER_USERNAME_2}	${CUSTOMER_PASSWORD_2}	${LOGIN_DEVICE}	${PASSWORD_DEVICE}
	GUI::ZPECloud::Basic::Login	${CUSTOMER_USERNAME_2}	${CUSTOMER_PASSWORD_2}
	GUI::ZPECloud::Settings::Enrollment::Open Tab
	Select Checkbox	xpath=//input[contains(@type, 'checkbox')]
	Click Element	xpath=//span[text()='Save']
	${customer_code}=	Get Value	xpath=//input[contains(@type, 'text')]
	${enrollment_key}=	Get Value	xpath=//input[@id='filled-adornment-password']
	CLI:Open	${LOGIN_DEVICE}	${PASSWORD_DEVICE}
	CLI:Enter Path	/settings/zpe_cloud/
	CLI:Set Field	enable_zpe_cloud	yes
	CLI:Commit
	CLI:Connect As Root	PASSWORD=NodeGridD3m0Passw0rd!
	${OUTPUT}=	CLI:Write	zpe_cloud_enroll -c ${customer_code} -k ${enrollment_key}
	Should Contain	${OUTPUT}	Successfully transferred device ownership.
#	CLI:Write	cd /var/zpe-cloud/
#	${enroll}=	CLI:Write	tail -10 zpe_cloud_enroll.log | grep "Device Serial Number: " | cut -d '"' -f 2	user=Yes
	CLI:Close Connection
#	GUI::ZPECloud::Devices::Available::Open Tab
#	GUI::ZPECloud::Basic::Table::Select If Exist item	available_0	${enroll}
#	GUI::ZPECLoud::Basic::Logout

GUI::ZPECloud::Check and update the zpecloud serial number variable if needed
	[Arguments]	${USERNAMENG}	${PASSWORDNG}	${IS_HOSTPEER}=${FALSE}
	IF	'${IS_HOSTPEER}' == '${FALSE}'
		Wait Until Keyword Succeeds	3x	3s	CLI:Open	${USERNAMENG}	${PASSWORDNG}
		${IS_VM}	CLI:Is VM System
		${CURRENT_SERIAL_NUMBER}	CLI:Get Serial Number
		${HAS_CORRECT_SERIAL_NUMBER}	Run Keyword and Return Status	Should Be Equal	${CURRENT_SERIAL_NUMBER}	${SERIALNUMBER}
			IF	not ${HAS_CORRECT_SERIAL_NUMBER}
				${STATUS}	Evaluate NG Versions: ${NGVERSION} <= 5.2
				IF	${STATUS}
					${SERIALNUMBER}=	Set Variable	${CURRENT_SERIAL_NUMBER}
					Set Global Variable	${SERIALNUMBER}
					Log	The VSR Device has a new SERIAL NUMBER. Check NG-11381 and CLOUD-8132 for more information.	WARN	console=yes
				END
				Log	The NG's SERIAL NUMBER changed. Need to check if it's a bug.	WARN	console=yes
			END
	ELSE IF	'${IS_HOSTPEER}' == '${TRUE}'
		Wait Until Keyword Succeeds	3x	3s	CLI:Open	${USERNAMENG}	${PASSWORDNG}	HOST_DIFF=${HOSTPEER}
		${IS_VM}	CLI:Is VM System
		${CURRENT_SERIAL_NUMBER}	CLI:Get Serial Number
		${HAS_CORRECT_SERIAL_NUMBER}	Run Keyword and Return Status	Should Be Equal	${CURRENT_SERIAL_NUMBER}	${SERIALNUMBERPEER}
			IF	not ${HAS_CORRECT_SERIAL_NUMBER}
				${STATUS}	Evaluate NG Versions: ${NGVERSION} <= 5.2
				IF	${STATUS}
					${SERIALNUMBERPEER}=	Set Variable	${CURRENT_SERIAL_NUMBER}
					Set Global Variable	${SERIALNUMBERPEER}
					Log	The VSR Device has a new SERIAL NUMBER. Check NG-11381 and CLOUD-8132 for more information.	WARN	console=yes
				END
				Log	The NG's SERIAL NUMBER changed. Need to check if it's a bug.	WARN	console=yes
			END
	END
	[Teardown]	CLI:Close Connection

GUI::ZPECloud::Devices::Enrolled::Page Should Contain Device With Online Status
	GUI::ZPECloud::Devices::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Input Text	search-input	${SERIALNUMBER}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//div[contains(text(), '${SERIALNUMBER}')]/../..//span[contains(text(), 'Online')]	30s
	GUI::Basic::Spinner Should Be Invisible

CLI::ZPECloud::Restart agent process via root
	CLI:Connect As Root	${HOST}	${PASSWORDNG}
	${OUTPUT}=	CLI:Write	/etc/init.d/zpe_cloud_agent restart	user=Yes
	Should Contain	${OUTPUT}	Starting zpe_cloud_agent
	Close All Connections

GUI::ZPECloud::Profiles::Operation::Get Previous ID
	GUI::ZPECloud::Profiles::Operation::Reset columns order
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Element Is Visible	//*/tr/td	30s
	Input Text	search-input	${SERIALNUMBER}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	//*/tr/td	30s
	${HAS_PREVIOUS_ID}=	Run Keyword And return Status	Page Should Contain Element	(//*/tr/td/span/a)[1]
	Set Suite Variable	${HAS_PREVIOUS_ID}
	IF	not ${HAS_PREVIOUS_ID}
		${PREVIOUS_ID}	Set Variable	${NULL}
		Set Suite Variable	${PREVIOUS_ID}
		Return From Keyword	${FALSE}
	END
	${PREVIOUS_ID}=	Get Text	(//*/tr/td/span/a)[1]
	Set Suite Variable	${PREVIOUS_ID}
	${HAS_CORRECT_ID_FORMAT}	Run Keyword And Return Status	Should Match Regexp	${PREVIOUS_ID}	........-....-....-....-............
	IF	not ${HAS_CORRECT_ID_FORMAT}
		Capture Page Screenshot
		Fail	Has incorrect ID format.
	END

GUI::ZPECloud::Profiles::Operation::Reset columns order
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Wait Until Element Is Visible	//button[@aria-label='Manage columns']	30s
	Click Element	//button[@aria-label='Manage columns']
	Wait Until Element Is Visible	//button[normalize-space()='Reset order']	30s
	Click Element	//button[normalize-space()='Reset order']
	Reload Page
	Wait Until Element Is Visible	//div[normalize-space()='Reload']	30s
	Click Element	//div[normalize-space()='Reload']
	Wait Until Element Is Not Visible	//button[normalize-space()='Reset order']	30s

GUI::ZPECloud::Devices::Backup Device Configuration
	GUI::ZPECloud::Devices::Open Tab
	Input Text	search-input	${SERIALNUMBER}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	(//*/tr/td/div)[1]	${SERIALNUMBER}	timeout=30s
	Wait Until Page Contains Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input	15s
	Click Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Wait Until Element Is Enabled	//*[@id="function-button-2"]
	Click Element	//*[@id="function-button-2"]
	Wait Until Page Contains Element	//*[@id="file-protection-radio"]

GUI::ZPECloud::Devices::Backup::Schedule Backup
	Wait Until Page Contains Element	//*[@id="opt-schedule"]	15s
	Click Element	//*[@id="opt-schedule"]
	Wait Until Page Contains Element	//*[@id="schedule-status-date"]	30s
#	${START_DATE}=	Get Current Date	result_format=%Y-%m-%d %H:%M:%S
#	Log	\nGet Current Date: ${START_DATE}\n	INFO	console=yes
#	${START_DATE}=	Convert Date	${START_DATE}	result_format=%Y-%m-%d, %H:%M
#	Log	\nConvert Date : ${START_DATE}\n	INFO	console=yes
	Click Element	//*[@id="schedule-status-date"]
	Wait Until Element Is Visible	//*[@id="ui-datepicker-selector"]/div[2]/div[2]/div/div/button	timeout=30s
#	Input Text	//*[@id="schedule-status-date"]	${START_DATE}
	Wait Until Page Contains Element	//*[@id="recurrent-checkbox"]	30s
	Click Element	recurrent-checkbox
	Wait Until Page Contains Element	//*[@id="mui-component-select-recurrent-type"]	30s
	Click Element	//*[@id="mui-component-select-recurrent-type"]
	Click Element	xpath=//li[contains(.,'Hourly')]
	Wait Until Element Is Visible	//*[@id="expiry-date"]	30s
	${EXPIRE_DATE}=	Get Current Date	result_format=%Y-%m-%d %H:%M:%S
	Log	\nGet Current Date: ${EXPIRE_DATE}\n	INFO	console=yes
	${EXPIRE_DATE}=	Add Time To Date	${EXPIRE_DATE}	7 days
	Log	\nAdd Time To Date: ${EXPIRE_DATE}\n	INFO	console=yes
	${EXPIRE_DATE}=	Convert Date	${EXPIRE_DATE}	result_format=%Y-%m-%d, %H:%M
	Log	\nConvert Date : ${EXPIRE_DATE}\n	INFO	console=yes
	Wait Until Element Is Not Visible	//*[@id="menu-recurrent-type"]	timeout=30s
	Click Element	expiry-date
	Wait Until Element Is Visible	//*[@id="ui-datepicker-selector"]/div[2]/div[2]/div/div/button	timeout=30s
	Input Text	expiry-date	${EXPIRE_DATE}
	Click Element	//*[@id="opt-schedule"]
	Capture Page Screenshot

GUI::ZPECloud::Devices::Backup::Apply Configuration
	[Arguments]	${GROUPS}=No
	Scroll Element Into View	submit-btn
	Wait Until Element is Enabled	submit-btn
	Click Element	submit-btn
	Run Keyword If	'${GROUPS}' == 'No'	Wait Until Page Contains	Backup in the selected devices was requested	timeout=1m
	Run Keyword If	'${GROUPS}' == 'Yes'	Wait Until Page Contains	Backup in the selected groups was requested	timeout=1m
	Capture Page Screenshot

GUI::ZPECloud::Devices::Backup::Check if backup has been started
	[Arguments]	${EMAIL}	${PWD}	${BACKUP_RESTORE}=No
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Input Text	search-input	${SERIALNUMBER}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	(//tr)[2]//div[contains(text(), '${SERIALNUMBER}')]	timeout=30s
	IF	${HAS_PREVIOUS_ID}
		${LAST_ID_IS_PREVIOUS_ID}	Run Keyword And Return Status	Page Should Contain Element	(//tr)[2]//div[contains(text(), '${PREVIOUS_ID}')]
		IF	${LAST_ID_IS_PREVIOUS_ID}
			Capture Page Screenshot
			GUI::ZPECloud::Profiles::Operation::Open Tab
			GUI::Basic::Click Element    //button[normalize-space()='Schedules']
			GUI::Basic::Spinner Should Be Invisible
			${HAS_SCHEDULED_BACKUP}	Run Keyword And Return Status	Wait Until Page Contains Element	(//tr)[2]//div[contains(text(), '${SERIALNUMBER}')]	timeout=30s
			IF	not ${HAS_SCHEDULED_BACKUP}
				Capture Page Screenshot
				Page Should Contain	No result found
				GUI::ZPECloud::Profiles::Backup::Open Tab
				Capture Page Screenshot
				Fail	The backup was scheduled successfully but didn't started. See opened BUG: CLOUD-9740 for more details.
			END
			Fail	Last ID on PROFILES::JOBS page is Previous Backup ID. The test must fail until PROFILES::JOBS page has a new ID.
		END
	END
	Run Keyword If	'${BACKUP_RESTORE}' == 'No'	Element Should Contain	(//*/tr/td/span/div)[5]	Backup configuration
	Run Keyword If	'${BACKUP_RESTORE}' == 'Yes'	Element Should Contain	(//*/tr/td/span/div)[5]	Restore backup configuration
	[Teardown]	Close All Browsers

GUI::ZPECloud::Profiles::Operation::Check if backup is Successful
	[Arguments]	${STORAGE_TYPE}
	${ITERATION_VALUE}	Set Variable	30
	${ITERATION_VALUE-1}	Evaluate	${ITERATION_VALUE} - 1
	FOR	${INDEX}	IN RANGE	${ITERATION_VALUE}	#It will be checking for 5 min
		Close All Browsers
		Sleep	10s
		Wait Until Keyword Succeeds	3x	3s	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
		GUI::ZPECloud::Profiles::Operation::Open Tab
		Input Text	search-input	${SERIALNUMBER}
		GUI::Basic::Spinner Should Be Invisible
		Wait Until Page Contains Element	(//tr)[2]//div[contains(text(), '${SERIALNUMBER}')]	timeout=30s
		${HAS_PREVIOUS_ID}	Run Keyword And Return Status	Page Should Contain Element	(//tr)[2]//div[contains(text(), '${PREVIOUS_ID}')]
		Run Keyword If	${HAS_PREVIOUS_ID}	Fail	The current backup has previous ID.
		Run Keyword If	${HAS_PREVIOUS_ID}	Exit For Loop
		${HAS_BACKUP_FAILED_STATUS}	Run Keyword And Return Status	Page Should Contain Element	(//tr)[2]//div[contains(text(), 'Failed')]
		${HAS_BACKUP_CANCELLED_STATUS}	Run Keyword And Return Status	Page Should Contain Element	(//tr)[2]//div[contains(text(), 'Cancelled')]
		${HAS_BACKUP_TIMEOUT_STATUS}	Run Keyword And Return Status	Page Should Contain Element	(//tr)[2]//div[contains(text(), 'Timeout')]
		${HAS_BACKUP_SCHEDULED_STATUS}	Run Keyword And Return Status	Page Should Contain Element	(//tr)[2]//div[contains(text(), 'Scheduled')]
		${HAS_BACKUP_SENDING_STATUS}	Run Keyword And Return Status	Page Should Contain Element	(//tr)[2]//div[contains(text(), 'Sending')]
		Run Keyword If	${HAS_BACKUP_FAILED_STATUS}	Fail	The backup status Must not contain FAIL status.
		Run Keyword If	${HAS_BACKUP_CANCELLED_STATUS}	Fail	The backup status Must not contain Cancelled status.
		Run Keyword If	${HAS_BACKUP_TIMEOUT_STATUS}	Fail	The backup status Must not contain Timeout status.
		Run Keyword If	${HAS_BACKUP_SCHEDULED_STATUS}	Fail	The backup status Must not contain Scheduled status.
		Run Keyword If	${HAS_BACKUP_SENDING_STATUS}	Fail	The backup status Must not contain Sending status.
		Run Keyword If	${HAS_BACKUP_FAILED_STATUS} or ${HAS_BACKUP_CANCELLED_STATUS} or ${HAS_BACKUP_TIMEOUT_STATUS} or ${HAS_BACKUP_SCHEDULED_STATUS} or ${HAS_BACKUP_SENDING_STATUS}	Exit For Loop
		${HAS_BACKUP_SUCCESSFUL_STATUS}	Run Keyword And Return Status	Page Should Contain Element	(//tr)[2]//div[contains(text(), 'Successful')]
		Run Keyword If	not ${HAS_BACKUP_SUCCESSFUL_STATUS}	Continue For Loop
		GUI::ZPECloud::Profiles::Backup::Open Tab
		Input Text	search-input	${SERIALNUMBER}
		GUI::Basic::Spinner Should Be Invisible
		Wait Until Page Contains Element	(//tr)[2]//td[contains(text(), 'No result found')] | (//tr)[2]//div[contains(text(), '${SERIALNUMBER}')]	timeout=30s
		${HAS_NO_RESULT_FOUND}	Run Keyword And Return Status	Element Should Contain	(//*/tr/td)[1]	No result found
		Run Keyword If	not ${HAS_NO_RESULT_FOUND} and '${INDEX}' == '${ITERATION_VALUE-1}'	Capture Page Screenshot
		IF	${HAS_NO_RESULT_FOUND}
			GUI::ZPECloud::Profiles::Operation::Open Tab
			GUI::ZPECloud::Schedules::Open Tab
			Wait Until Page Contains Element	search-input	timeout=30s
			Input Text	search-input	${SERIALNUMBER}
			GUI::Basic::Spinner Should Be Invisible
			Wait Until Page Contains Element	(//tr)[2]//td[contains(text(), 'No result found')] | (//tr)[2]//div[contains(text(), '${SERIALNUMBER}')]	timeout=30s
			${HAS_SCHEDULED_BACKUP}	Run Keyword And Return Status	Element Should Not Contain	(//tr)[2]//td	No result found
			Run Keyword If	not ${HAS_SCHEDULED_BACKUP}	Log	Has not scheduled backup	WARN	console=yes
			Run Keyword If	not ${HAS_SCHEDULED_BACKUP}	Capture Page Screenshot
			Continue For Loop
		END
		${HAS_CORRECT_STORAGE_TYPE}	Run Keyword And Return Status	Page Should Contain Element	(//tr)[2]//div[contains(text(), '${STORAGE_TYPE}')]
		Run Keyword If	not ${HAS_PREVIOUS_ID} and ${HAS_BACKUP_SUCCESSFUL_STATUS} and ${HAS_CORRECT_STORAGE_TYPE}	Exit For Loop
		Run Keyword If	'${INDEX}' == '${ITERATION_VALUE-1}'	Fail	The Backup generation failed. There is no backup available on Profiles::Backup page. Check the screenshots above.
	END

GUI::ZPECloud::Profiles::Operation::Schedule::Delete Schedule
	[Arguments]	${EMAIL}	${PWD}
	Wait Until Keyword Succeeds	3x	3s	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Open Tab
	GUI::ZPECloud::Schedules::Open Tab
	Wait Until Page Contains Element	search-input	timeout=30s
	Input Text	search-input	${SERIALNUMBER}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	(//*/tr/td)[1]	60s
	Click Element	(//*/tr/td/span/input[@type='checkbox'])[1]
	GUI::ZPECloud::Basic::Click Button	delete
	Wait Until Page Contains Element	//*[@id="confirm-btn"]	15s
	Click Element	//*[@id="confirm-btn"]
	Wait Until Page Contains Element	//span[@id='client-snackbar'][contains(.,'Schedules deleted successfully')]	timeout=1m
	Wait Until Page Contains Element	(//tr)[2]//td[contains(text(), 'No result found')] | (//tr)[2]//div[contains(text(), '${SERIALNUMBER}')]	timeout=30s
	[Teardown]	Close All Browsers

GUI::ZPECloud::Profiles::Backup::Delete Backup
	[Arguments]	${EMAIL}	${PWD}
	Wait Until Keyword Succeeds	3x	3s	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Backup::Open Tab
	Wait Until Page Contains Element	search-input	timeout=30s
	Input Text	search-input	${SERIALNUMBER}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="ca-profiles-bkp-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="ca-profiles-bkp-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	GUI::ZPECloud::Basic::Click Button	delete
	Wait Until Page Contains Element	//*[@id="confirm-btn"]	15s
	Click Element	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	Backups are deleted successfully	timeout=1m
	Wait Until Page Contains Element	(//tr)[2]//td[contains(text(), 'No result found')] | (//tr)[2]//div[contains(text(), '${SERIALNUMBER}')]	timeout=30s
	[Teardown]	Close All Browsers

GUI::ZPECloud::Profiles::Backup::Restore
	GUI::ZPECloud::Profiles::Backup::Open Tab
	Wait Until Page Contains Element	//*[@id="ca-profiles-bkp-table-1"]/div/div/table/tbody/tr[1]/td[1]/span	timeout=1m
	Click Element	//*[@id="ca-profiles-bkp-table-1"]/div/div/table/tbody/tr[1]/td[1]/span
	GUI::Basic::Spinner Should Be Invisible
	GUI::ZPECloud::Basic::Click Button	restore
	Wait Until Page Contains Element	xpath=//div[@id='ca-sd-search-1']/div/div/input
	Input Text	xpath=//div[@id='ca-sd-search-1']/div/div/input	${SERIALNUMBER}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	//*[@id="ca-sd-table-1"]/div/div/table/tbody/tr[1]/td[3]/div	${SERIALNUMBER}	timeout=30s
	Wait Until Page Contains Element	//*[@id="ca-sd-table-1"]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="ca-sd-table-1"]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	60s	1s	Checkbox Should Be Selected	//*[@id="ca-sd-table-1"]/div[1]/div/table/tbody/tr[1]/td[1]/span/input
	Wait Until Page Contains Element	//*[@id="submit-btn"]
	Click Element	//*[@id="submit-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="confirm-btn"]	15s
	Click Element	//*[@id="confirm-btn"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	30	2	Wait Until Page Contains	Backup restore scheduled on devices. Check operations for details

GUI::ZPECloud::Security::Services::Enable File Protection on NG Device
	[Arguments]	${USERNG}	${PWDNG}
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Security::Open Services tab
	Wait Until Page Contains Element	//*[@id="pass_prot"]
	Click Element	//*[@id="pass_prot"]
	Wait Until Page Contains Element	//*[@id="file_pass_conf"]
	Wait Until Element Is Visible	//*[@id="file_pass_conf"]
	Input Text	//*[@id="file_pass"]	${PWDNG}
	Input Text	//*[@id="file_pass_conf"]	${PWDNG}
	GUI::Basic::Save If Configuration Changed
	GUI::Basic::Spinner Should Be Invisible
	Close All Browsers

GUI::ZPECloud::Security::Services::Disable file protection on NG device if needed
	[Arguments]	${USERNG}	${PWDNG}
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Security::Open Services tab
	${HAS_CLOUD_FILE_PROTECTION_ENABLED}	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='pass_prot']
	Run Keyword If	${HAS_CLOUD_FILE_PROTECTION_ENABLED}	Unselect Checkbox	//input[@id='pass_prot']
	GUI::Basic::Save If Configuration Changed
	[Teardown]	Close All Browsers

GUI::ZPECloud::Groups::Find Group and Select it
	[Arguments]	${GROUP_NAME}
	GUI::ZPECloud::Groups::Open Tab
	Wait Until Page Contains Element	search-input	timeout=30s
	Input Text	search-input	${GROUP_NAME}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr[1]/td[1]/span/input
	Click Element	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr[1]/td[1]/span

GUI::ZPECloud::Groups::Delete Group
	[Arguments]	${GROUP_NAME}
	GUI::ZPECloud::Profiles::Backup::Open Tab
	GUI::ZPECloud::Groups::Find Group and Select it	${GROUP_NAME}
	Click Element	//button[contains(text(), 'Delete')]
	Wait Until Page Contains Element	//*[contains(text(),'Are you sure you want to delete it?')]	timeout=30s
	Click Element	//button[@id='confirm-btn']
	Wait Until Page Contains	Group deleted successfully	timeout=30s
	Reload Page
	Wait Until Page Contains Element	search-input	timeout=30s
	Input Text	search-input	${GROUP_NAME}
	GUI::Basic::Spinner Should Be Invisible
	Page Should Not Contain Element	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr/td[2]/div

GUI::ZPECloud::Groups::Delete group If needed
	[Arguments]	${GROUP_NAME}
	${HAS_GROUP}	Run Keyword And Return Status	GUI::ZPECloud::Groups::Find Group and Select it	${GROUP_NAME}
	Run Keyword If	${HAS_GROUP}	GUI::ZPECloud::Groups::Delete Group	${GROUP_NAME}

GUI::ZPECloud::Groups::Create Group
	[Arguments]	${GROUP_NAME}
	GUI::ZPECloud::Groups::Open Tab
	Wait Until Page Contains Element	//button/span[contains(text(),'add')]	timeout=30s
	Click Element	//button/span[contains(text(),'add')]
	GUI::Basic::Spinner Should Be Invisible
	Input Text	xpath=//input[@name='name']	${GROUP_NAME}
	Click Element	//*[@id="ca-group-permission-management-radio"]/label[1]/span[1]/input
	Wait Until Element Is Enabled	submit-btn	timeout=30s
	Click Element	submit-btn
	Wait Until Page Contains	Group added successfully	30s
	Reload Page
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	search-input	30s
	Input Text	search-input	${GROUP_NAME}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr/td[2]/div
	Element Should Contain	//*[@id="ca-groups-general-table-1"]/div/div/table/tbody/tr/td[2]/div	${GROUP_NAME}

GUI::ZPECloud::Groups::Devices::Add device to group
	[Arguments]	${GROUP_NAME}	${DIFF_SERIALNUMBER}
	GUI::ZPECloud::Groups::Devices::Open Tab
	Wait Until Page Contains Element	search-input	timeout=30s
	Input Text	search-input	${DIFF_SERIALNUMBER}
	Wait Until Page Does Not Contain Element	//div[normalize-space()='${DIFF_SERIALNUMBER}']/../../td/span/input[@type='checkbox']	timeout=30s
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//div[normalize-space()='${DIFF_SERIALNUMBER}']/../../td/span/input[@type='checkbox']	timeout=30s
	Select Checkbox	//tr/td/span/input[@type='checkbox']
	${HAS_CHECKBOX_SELECTED}	Run Keyword And Return Status	Wait Until Keyword Succeeds	30s	1s	Checkbox Should Be Selected	//tr/td/span/input[@type='checkbox']
	Run Keyword If	not ${HAS_CHECKBOX_SELECTED}	Click Element	//tr/td/span/input[@type='checkbox']
	Wait Until Page Contains Element	//button/span[contains(text(),'add')]	timeout=30s
	Wait Until Element Is Enabled	//button/span[contains(text(),'add')]	timeout=30s
	Click Element	//button/span[contains(text(),'add')]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains	${GROUP_NAME}	timeout=30s
	Wait Until Page Contains Element	search-input	timeout=30s
	Input Text	search-input	${GROUP_NAME}
	GUI::Basic::Spinner Should Be Invisible
#	Wait Until Element Contains	(//*/tr/td/div)[1]	${GROUP_NAME}	timeout=30s	#BUG_CLOUD_9029
	Wait Until Page Contains Element	//div[normalize-space()='${GROUP_NAME}']/../../td/span/input[@type='checkbox']	timeout=30s
	Select Checkbox	//div[normalize-space()='${GROUP_NAME}']/../../td/span/input[@type='checkbox']
	${HAS_CHECKBOX_SELECTED}	Run Keyword And Return Status	Wait Until Keyword Succeeds	30s	1s	Checkbox Should Be Selected	//div[normalize-space()='${GROUP_NAME}']/../../td/span/input[@type='checkbox']
	Run Keyword If	not ${HAS_CHECKBOX_SELECTED}	Click Element	//div[normalize-space()='${GROUP_NAME}']/../../td/span/input[@type='checkbox']
	Wait Until Page Contains Element	//button[contains(text(),'Add on Group')]	timeout=30s
	Wait Until Element Is Enabled	//button[contains(text(),'Add on Group')]	timeout=30s
	Click Element	//button[contains(text(),'Add on Group')]
	Wait Until Page Contains	Device Added to Group	timeout=30s

GUI::ZPECloud::Groups::Devices::Remove device from group
	[Arguments]	${GROUP_NAME}	${DIFF_SERIALNUMBER}
	GUI::ZPECloud::Groups::Devices::Open Tab
	Wait Until Page Contains Element	search-input	timeout=30s
	Input Text	search-input	${DIFF_SERIALNUMBER}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//div[@id='mui-component-select-group-assigned']	timeout=30s
	Wait Until Element Is Enabled	//div[@id='mui-component-select-group-assigned']	timeout=30s
	Click Element	//div[@id='mui-component-select-group-assigned']
	${HAS_GROUP_LIST_AVAILABLE}	Run Keyword And Return Status	Wait Until Page Contains Element	//li[normalize-space()='${GROUP_NAME}']	timeout=30s
	Run Keyword If	not ${HAS_GROUP_LIST_AVAILABLE}	Click Element	//div[@id='mui-component-select-group-assigned']
	Wait Until Element Is Enabled	//li[normalize-space()='${GROUP_NAME}']	timeout=30s
	Click Element	//li[normalize-space()='${GROUP_NAME}']
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	search-input	timeout=30s
	Clear Element Text	search-input
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	search-input	timeout=30s
	Input Text	search-input	${DIFF_SERIALNUMBER}
	Wait Until Page Does Not Contain Element	//div[normalize-space()='${DIFF_SERIALNUMBER}']/../../td/span/input[@type='checkbox']	timeout=30s
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//div[normalize-space()='${DIFF_SERIALNUMBER}']/../../td/span/input[@type='checkbox']	timeout=30s
	Select Checkbox	//tr/td/span/input[@type='checkbox']
	${HAS_CHECKBOX_SELECTED}	Run Keyword And Return Status	Wait Until Keyword Succeeds	30s	1s	Checkbox Should Be Selected	//tr/td/span/input[@type='checkbox']
	Run Keyword If	not ${HAS_CHECKBOX_SELECTED}	Click Element	//tr/td/span/input[@type='checkbox']
	Wait Until Element Is Enabled	(//button[normalize-space()='Remove From Group'])[1]	timeout=30s
	Click Element	(//button[normalize-space()='Remove From Group'])[1]
	Wait Until Page Contains	Devices Removed from Group	timeout=2m
	GUI::ZPECloud::Groups::Open Tab
	Wait Until Page Contains Element	search-input	timeout=30s
	Input Text	search-input	${GROUP_NAME}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	(//tr/td/div)[2]	timeout=30s
	Element Should Contain	(//tr/td/div)[2]	-

GUI::ZPECloud::Groups::Go to backup page on groups
	[Arguments]	${GROUP_NAME}
	GUI::ZPECloud::Groups::Find Group and Select it	${GROUP_NAME}
	Wait Until Page Contains Element	function-button-6
	Click Element	function-button-6
	Wait Until Page Contains Element	//*[@id="file-protection-radio"]

GUI::ZPECloud::Settings::Disable On-Premises Enrollment and save
	GUI::ZPECloud::Settings::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	(//button[normalize-space()='On-Premises'])[1]	timeout=1m
	Click Element	(//button[normalize-space()='On-Premises'])[1]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	(//input[@id='ca-setop-enable-device-enrollment-checkbox'])[1]
	Click Element	(//input[@id='ca-setop-enable-device-enrollment-checkbox'])[1]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Enabled	//span[contains(.,'SAVE')]
	Click Element	//span[contains(.,'SAVE')]

GUI::ZPECloud::Devices::Jobs::Check if job has been started
	[Arguments]	${EMAIL}	${PWD}	${TYPE}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
	GUI::ZPECloud::Profiles::Operation::Open Tab
	Input Text	search-input	${SERIALNUMBER}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	(//tr)[2]//div[contains(text(), '${SERIALNUMBER}')]	timeout=30s
	Run Keyword If	${HAS_PREVIOUS_ID}	Page Should Not Contain Element	(//tr)[2]//div[contains(text(), '${PREVIOUS_ID}')]
	Element Should Contain	(//*/tr/td/span/div)[3]	${TYPE}
	[Teardown]	Close All Browsers

GUI::ZPECloud::Profiles::Operation::Check if job is Successful
	[Arguments]	${EMAIL}	${PWD}	${TYPE}
	${ITERATION_VALUE}	Set Variable	30
	${ITERATION_VALUE-1}	Evaluate	${ITERATION_VALUE} - 1
	FOR	${INDEX}	IN RANGE	${ITERATION_VALUE}	#It will be checking for 5 min
		Close All Browsers
		Sleep	10s
		GUI::ZPECloud::Basic::Open And Login	${EMAIL}	${PWD}
		GUI::ZPECloud::Profiles::Operation::Open Tab
		Input Text	search-input	${SERIALNUMBER}
		GUI::Basic::Spinner Should Be Invisible
		Wait Until Page Contains Element	(//tr)[2]//div[contains(text(), '${SERIALNUMBER}')]	timeout=30s
		${HAS_PREVIOUS_ID}	Run Keyword And Return Status	Page Should Contain Element	(//tr)[2]//div[contains(text(), '${PREVIOUS_ID}')]
		Run Keyword If	${HAS_PREVIOUS_ID}	Fail	The current backup has previous ID.
		Run Keyword If	${HAS_PREVIOUS_ID}	Exit For Loop
		${HAS_JOB_FAILED_STATUS}	Run Keyword And Return Status	Page Should Contain Element	(//tr)[2]//div[contains(text(), 'Failed')]
		Run Keyword If	${HAS_JOB_FAILED_STATUS}	Fail	The current backup test has FAIL status.
		Run Keyword If	${HAS_JOB_FAILED_STATUS}	Exit For Loop
		${HAS_JOB_SUCCESSFUL_STATUS}	Run Keyword And Return Status	Page Should Contain Element	(//tr)[2]//div[contains(text(), 'Successful')]
		Run Keyword If	not ${HAS_JOB_SUCCESSFUL_STATUS}	Continue For Loop
		${HAS_CORRECT_TYPE}	Run Keyword And Return Status	Page Should Contain Element	(//tr)[2]//div[contains(text(), '${TYPE}')]
		Run Keyword If	not ${HAS_PREVIOUS_ID} and ${HAS_JOB_SUCCESSFUL_STATUS} and ${HAS_CORRECT_TYPE}	Exit For Loop
		Run Keyword If	'${INDEX}' == '${ITERATION_VALUE-1}'	Fail	The Job generation failed. There is no job available on Profiles::Operation page. Check the screenshots above.
	END

GUI::ZPECloud::Add connection
	[Arguments]	${USERNG}	${PWDNG}	${CONNECTION_NAME}
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Network::Open Connections Tab
	${HAS_CONNECTION}	Run Keyword And Return Status	Page Should Contain	${CONNECTION_NAME}
	Run Keyword If	${HAS_CONNECTION}	Return From Keyword
	Click Button	addButton
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	connType
	GUI::Basic::Wait Until Element Is Accessible	connType
	Select From List By Value	connType	ethernet
	Input Text	connName	${CONNECTION_NAME}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	[Teardown]	Run Keywords	GUI::Basic::Cancel or return if exist	AND	Close Browser

GUI::ZPECloud::Configure Failover
	[Arguments]	${USERNG}	${PWDNG}	${CONNECTION_NAME}	${FAILOVER_IP}
	GUI::Basic::Open And Login Nodegrid	${USERNG}	${PWDNG}
	GUI::Network::Open Settings Tab
	Wait Until Page Contains	failover	timeout=30s
	${HAS_FAILOVER_ENABLED}=	Run Keyword And return Status	Checkbox Should Be Selected	failover
	Run Keyword If	${HAS_FAILOVER_ENABLED}	Return From Keyword
	Click Element	failover
	Select From List By Value	secondaryConn	${CONNECTION_NAME}
	Select Radio Button	trigger	ipaddress
	Input Text	address	${FAILOVER_IP}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save
	GUI::Security::Open Firewall tab
	Click Element	//*[@id='OUTPUT:IPv4']/td[2]/a
	GUI::Basic::Spinner Should Be Invisible
	Click Element	addButton
	GUI::Basic::Spinner Should Be Invisible
	Click Element	target
	Click Element	//option[@value='DROP']
	Input Text	dest	${FAILOVER_IP}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="outIface"]//option[@value='eth0']
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	[Teardown]	Run Keywords	GUI::Basic::Cancel or return if exist	AND	Close Browser

GUI::ZPECloud::Move Device to Available
	[Arguments]	${DIFF_SERIALNUMBER}=${SERIALNUMBER}
	GUI::ZPECloud::Basic::Open And Login	${EMAIL_ADDRESS}	${PASSWORD}
	GUI::ZPECloud::Devices::Open Tab
	Wait Until Page Contains Element	search-input	15s
	Input Text	search-input	${DIFF_SERIALNUMBER}
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element	//tr/td/div[contains(text(), '${DIFF_SERIALNUMBER}')]	30s
	Wait Until Keyword Succeeds	10	1	Click Element	//*[@id="ca-devices-enrolled-table-1"]/div[1]/div/table/tbody/tr/td[1]/span/input
	Click Element	//div[contains(text(),'Menu')]
	Wait Until Page Contains Element	//button[normalize-space()='Move to available']	20s
	Wait Until Keyword Succeeds	10	1	Click Element	//button[normalize-space()='Move to available']
	Wait Until Page Contains	Device moved to available tab	30s
	[Teardown]	Close Browser

GUI::ZPECloud::Security::Services::Disable file protection on NG device if needed on current browser window
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Open Services tab
	${HAS_CLOUD_FILE_PROTECTION_ENABLED}	Run Keyword And Return Status	Checkbox Should Be Selected	//input[@id='pass_prot']
	Run Keyword If	${HAS_CLOUD_FILE_PROTECTION_ENABLED}	Unselect Checkbox	//input[@id='pass_prot']
	[Teardown]	Wait Until Keyword Succeeds	3x	3s	Run Keywords	GUI::Basic::Save If Configuration Changed	AND	GUI::Basic::Cancel or return if exist

GUI::ZPECloud::Security::Firewall::Delete firewall rule if needed
	[Arguments]	${FIREWALL_IP}=1.1.1.1	${FIREWALL_TARGET}=DROP
	GUI::Security::Open Firewall tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	//tr[@id='OUTPUT:IPv6']//a[@id='OUTPUT']	1m
	${HAS_OPPENED_OUTPUT_FIREWALL}	Run Keyword And Return Status	Wait Until Keyword Succeeds	60s	3s	Click Element	//*[@id='OUTPUT:IPv4']//*[@id='OUTPUT']
	Run Keyword If	not ${HAS_OPPENED_OUTPUT_FIREWALL}	Wait Until Keyword Succeeds	60s	3s	Click Element	(//a[contains(.,'OUTPUT')])[1]
	GUI::Basic::Spinner Should Be Invisible
	${HAS_FIREWALL_RULE}	Run Keyword And Return Status
	...	Page Should Contain Element	//*[text()='${FIREWALL_IP}']/../*[text()='${FIREWALL_TARGET}']/../*/input
	IF	${HAS_FIREWALL_RULE}
		Click Element	//*[text()='${FIREWALL_IP}']/../*[text()='${FIREWALL_TARGET}']/../*/input
		Wait Until Element is Enabled	delButton	30s
		Click Element	delButton
		GUI::Basic::Spinner Should Be Invisible
		Wait Until Page Does Not Contain Element	//*[text()='${FIREWALL_IP}']/../*[text()='${FIREWALL_TARGET}']/../*/input	30s
	END

GUI::ZPECloud::Network::Settings::Disable failover if needed
	GUI::Network::Open Settings Tab
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	failover
	${IS_SELECTED}=	Run Keyword And return Status	Checkbox Should Be Selected	failover
	Run Keyword If	${IS_SELECTED}	Unselect Checkbox	failover
	Run Keyword If	${IS_SELECTED}	GUI::Basic::Save
	[Teardown]	Wait Until Keyword Succeeds	3x	3s	Run Keywords	GUI::Basic::Save If Configuration Changed	AND	GUI::Basic::Cancel or return if exist

GUI::ZPECloud::Network::Connection::Delete connection if needed
	[Arguments]	${CONNECTION_NAME}	${CONNECTION_CHECKBOX_LOCATOR}
	GUI::Network::Open Connections Tab
	GUI::Basic::Spinner Should Be Invisible
	${CONNECTIONEXISTS}=	Run Keyword And return Status	Page Should Contain	${CONNECTION_NAME}
	Run Keyword If	${CONNECTIONEXISTS}	Scroll Element Into View	${CONNECTION_CHECKBOX_LOCATOR}
	Run Keyword If	${CONNECTIONEXISTS}	Click Element	${CONNECTION_CHECKBOX_LOCATOR}
	Run Keyword If	${CONNECTIONEXISTS}	GUI::Basic::Delete