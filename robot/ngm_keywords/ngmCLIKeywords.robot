*** Settings ***
Documentation
...    = Table of Content =
...    - Overview
...    - Useage
...    - Supported Nodegrid Versions
...    == Overview ==
...    This Library supports spefic keywords relating to the ZPE Systems Nodegrid CLI interface. These keywords are only intended to be used with the CLI interface and not with the SHELL interface
...    == Useage ==
...    This file needs to be added as a resource library to the robot test files. In teh inhouse test automation system this is perfformed through the init.robot file which combines all required resources to run the actual test cases
...    == Supported Nodegrid Versions ==
...    - 3.2
...    - 4.0
Resource	../ngm_tests/init.robot
*** Keywords ***
CLI:Open
    [Documentation]
		...    Opens a SSH session to a Nodegrid host and connects to the CLI. The keyword sets up the ssh enviorment
        ...    to enable the test cases to run with minimla interruptions due to the enviorment settings.
        ...
        ...    username and password arguments are both optional. When not defined the Nodegrid default values for the
        ...    admin user are used for the session
        ...
        ...    Session alias is assigned to session and can be used to switch between sessions see `CLI:Switch Connection`
        ...
        ...    The output from the ssh session is loged to the about.log file.
        ...
        ...    For more details see: : http://robotframework.org/SSHLibrary/latest/SSHLibrary.html
        ...
        ...    Examples:
        ...
        ...    | CLI:Open          |              |          |          |          |
        ...    | CLI:Open          | username     | password |          |          |
        ...    | CLI:Open          | username     | password | Session1 | Open Session1 |
        ...    | CLI:Open          | username     | password | Session2 | Open Session2 |
        ...    | CLI:Switch Connection | Session1     |          |          | Switch to Session with the alias Session1 |
        ...
    [Arguments]    ${USERNAME}=${DEFAULT_USERNAME}    ${PASSWORD}=${DEFAULT_PASSWORD}    ${session_alias}=default    ${timeout}=${CLI_DEFAULT_TIMEOUT}
    ...    ${startping}=Yes    ${RAW_MODE}=No	${TYPE}=cli	${HOST_DIFF}=${HOST}    ${CHECK_MEMORY_CPU}=Yes
    ${PING_VALUE}	${PING_MSG}=	Run Keyword If	'${startping}' == 'Yes'	Run Keyword And Ignore Error	CLIENT:Start Target Ping	${HOST_DIFF}
    Log	\nPing value from ${CLIENTIP} to ${HOST_DIFF}: ${PING_VALUE}\n	INFO	console=yes
    Run Keyword If	'${PING_VALUE}' != 'PASS'	Log	\nPing error message: ${PING_MSG}\n	INFO	console=yes
    ${DATE}=	Get Current Date	UTC	exclude_millis=${TRUE}
    ${CONVERTED_DATE}=	Convert Date	${DATE}	date_format=%Y-%m-%d %H:%M:%S	result_format=%Y-%m-%dT%H-%M-%S
    ${SSH_LOG_FILE}=	Set Variable	Results/logs/${CONVERTED_DATE}_sshlog.log
    Create File	${SSH_LOG_FILE}
    Enable Ssh Logging	${SSH_LOG_FILE}
    Set Default Configuration    loglevel=INFO
    Run Keyword If	'${USERNAME}' == 'root'	Set Default Configuration	prompt=#
    ...	ELSE	Run Keyword If	'${TYPE}' == 'cli'	Set Default Configuration	prompt=]#
    ...	ELSE	Run Keyword If	'${TYPE}' == 'root'	Set Default Configuration	prompt=#
    ...	ELSE	Run Keyword If	'${TYPE}' == 'shell'	Set Default Configuration	prompt=$
    ...	ELSE	Run Keyword If	'${TYPE}' == 'menudriven'	Set Default Configuration	prompt=.
    ...	ELSE	Run Keyword If	'${TYPE}' == 'menudrivencluster'	Set Default Configuration	prompt=Enter an action:
    ...	ELSE	Run Keyword If	'${TYPE}' == 'connect_device'	Set Default Configuration	prompt=]
    ...	ELSE	Run Keyword If	'${TYPE}' == 'nodegrid_device'	Set Default Configuration	prompt=to cli ]
    ...	ELSE	Run Keyword If	'${TYPE}' == 'windows'	Set Default Configuration	prompt=\>
    ...	ELSE	Run Keyword If	'${USERNAME}' == 'admin'	Set Default Configuration	prompt=]#
    Set Default Configuration    newline=\n
    Set Default Configuration    escape_ansi=${TRUE}
    Run Keyword If	'${TYPE}' == 'windows'    Set Default Configuration    newline=\r\n
    Set Default Configuration    width=400
    Set Default Configuration    height=600
    Set Default Configuration    timeout=${timeout}
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nOpen Connection ${HOST_DIFF} - alias ${session_alias}	INFO	console=yes
    Open Connection    ${HOST_DIFF}    alias=${session_alias}
	Log	Login ${USERNAME} ${PASSWORD}	INFO	console=yes
    ${LOGIN_VALUE}	${LOGIN_MSG}=	Run Keyword If	'${RAW_MODE}'=='No'	Run Keyword And Ignore Error	Login	${USERNAME}	${PASSWORD}	loglevel=INFO
    ${LOGIN_STATUS}	Run Keyword And Return Status	Should Be Equal	${LOGIN_VALUE}	PASS
	Run Keyword If	'${RAW_MODE}'=='No'	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\nConnected	INFO	console=yes
    Run Keyword If	'${RAW_MODE}'!='No'	Run Keyword And Ignore Error	Login	${USERNAME}	${PASSWORD}	loglevel=INFO
    Run Keyword If	'${RAW_MODE}'!='No'	CLI:Switch Connection	default
	Run Keyword If	'${RAW_MODE}'!='No'	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\nIgnored Error to Connect	INFO	console=yes
	Run Keyword If	'${HOST_DIFF}' == '${HOST}' and '${TYPE}' == 'cli' and '${USERNAME}' == '${DEFAULT_USERNAME}' and '${LOGIN_STATUS}' == '${FALSE}'
	...	Run Keyword And Ignore Error	CLI:Access via console and get logs information for debug
	Run Keyword If	not ${LOGIN_STATUS} and '${RAW_MODE}'=='No'	Fail	${LOGIN_MSG}
	Run Keyword If	'${HOST_DIFF}' == '${HOST}' and '${TYPE}' == 'cli' and '${USERNAME}' == '${DEFAULT_USERNAME}' and '${CHECK_MEMORY_CPU}' == 'Yes'
	...	CLI:Check memory and CPU usages

CLI:Close Connection
    [Arguments]	${CURRENT_ONLY}=No
    [Documentation]
		...	This keyword can only be called on teardown due to the use of
        ...	Run Keyword If Any Tests Failed
        ...
    Run Keyword If      '${CURRENT_ONLY}' == 'Yes'    Close Connection
    Run Keyword If      '${CURRENT_ONLY}' == 'No'    Close All Connections
    Run Keyword and Ignore error        CLIENT:End Target Ping
    #Run Keyword If Any Tests Failed  CLI:GET TROUBLESHOOTING INFORMATION   TAIL=20

CLI:Switch Connection
	[Documentation]
	...	Switch to the specified connection which is already opened
	[Arguments]	${ALIAS}
	Log To Console	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nSwitch to connection ${ALIAS}
	Switch Connection	${ALIAS}
	Log To Console	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\nSwitched to ${ALIAS} connection

CLI:General Open
    [Documentation]
		...    Opens a SSH session to any device. For opening a session to a Nodegrid system, use CLI:Open
    [Arguments]    ${HOST_DIFF}	${USERNAME}    ${PASSWORD}	${session_alias}    ${timeout}=${CLI_DEFAULT_TIMEOUT}
    ...    ${startping}=Yes    ${RAW_MODE}=No	${prompt}=#
    Run Keyword If    '${startping}' == 'Yes'    CLIENT:Start Target Ping	${HOST_DIFF}
    Enable Ssh Logging    about.log
    Set Default Configuration    loglevel=INFO
    Set Default Configuration	prompt=${prompt}
    Set Default Configuration    newline=\n
    Set Default Configuration    width=400
    Set Default Configuration    height=600
    Set Default Configuration    timeout=${timeout}
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nOpen Connection ${HOST_DIFF} - alias ${session_alias}	INFO	console=yes
    Open Connection    ${HOST_DIFF}    alias=${session_alias}
	Log	Login ${USERNAME} ${PASSWORD}	INFO	console=yes
    Run Keyword If	'${RAW_MODE}'=='No'	Login    ${USERNAME}    ${PASSWORD}    loglevel=INFO
	Run Keyword If	'${RAW_MODE}'=='No'	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\nConnected	INFO	console=yes
    Run Keyword If	'${RAW_MODE}'!='No'	Run Keyword And Ignore Error	Login    ${USERNAME}    ${PASSWORD}    loglevel=INFO
    Run Keyword If	'${RAW_MODE}'!='No'	CLI:Switch Connection	default
	Run Keyword If	'${RAW_MODE}'!='No'	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\nIgnored Error to Connect	INFO	console=yes


CLI:Set Current Connection Configuration
	[Documentation]
	...	Set configuration for the current connection
	[Arguments]	${CONFIGURATION}
	Log To Console	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nSet current connection configuration ${CONFIGURATION}
	Set Client Configuration	${CONFIGURATION}
	Log To Console	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\nCurrent connection configuration was set: ${CONFIGURATION}

CLI:Connect As Root
	[Documentation]
		...    Keyword opens a new SSH session as root user
        ...
        ...    The Keyword returns no output
        ...
        ...    Examples:
        ...
        ...    | CLI:Coonnect As Root |  |
        ...    | CLI:Get User Whoami |
        ...    | Output: |
        ...    | | root |
        ...    | |
	[Arguments]	${HOST_DIFF}=${HOST}	${PASSWORD}=${ROOT_PASSWORD}	${session_alias}=root_session
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nOpen Connection ${HOST_DIFF} - alias ${session_alias}	INFO	console=yes
	Open Connection     ${HOST_DIFF}     alias=${session_alias}
	Set Client Configuration    prompt=#
	Set Client Configuration    newline=\n
	Set Client Configuration    width=400
	Set Client Configuration    height=600
	Set Client Configuration    timeout=300
	Log	Login root ${PASSWORD}	INFO	console=yes
	Login   root     ${PASSWORD}
	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\nConnected	INFO	console=yes

CLI:Reconnect As Root
	[Documentation]
		...    Keyword exits from  the current session and opens a new SSH session as root user
        ...
        ...    The Keyword returns no output
        ...
        ...    Examples:
        ...
        ...    | CLI:Get User Whoami |
        ...    | Output: |
        ...    | | admin |
        ...    | CLI:Reconnect As Root |  |
        ...    | CLI:Get User Whoami |
        ...    | Output: |
        ...    | | root |
        ...    | |
	Write   exit
	${output}=  Read    delay=3s
	CLI:Connect As Root

CLI:Open Device
	[Documentation]
		...	Opens a SSH session using direct access feature to connects to the device from Nodegrid host.
	[Arguments]	${DEVICE_NAME}	${USERNAME}=${DEFAULT_USERNAME}	${PASSWORD}=${DEFAULT_PASSWORD}	${session_alias}=device_session	${timeout}=${CLI_DEFAULT_TIMEOUT}
	...	${RAW_MODE}=No	${TYPE}=cli	${HOST_DIFF}=${HOST}
	Enable Ssh Logging	device.log
	Set Default Configuration	loglevel=INFO
	Set Default Configuration	newline=\n
	Set Default Configuration	width=400
	Set Default Configuration	height=600
	Set Default Configuration	prompt=${SPACE}]
	Set Default Configuration	timeout=${timeout}
	Open Connection	${HOST_DIFF}	alias=${session_alias}
	${OUTPUT}=	Run Keyword If	'${RAW_MODE}'=='No'	Login	${USERNAME}:${DEVICE_NAME}	${PASSWORD}	loglevel=INFO
	Log To Console	${OUTPUT}
	Run Keyword If	'${RAW_MODE}'!='No'	Run Keyword And Ignore Error	Login	${USERNAME}	${PASSWORD}	loglevel=INFO
	Run Keyword If	'${TYPE}' == 'cli'	Set Client Configuration	prompt=]#
	...	ELSE	Run Keyword If	'${TYPE}' == 'root'	Set Client Configuration	prompt=#
	...	ELSE	Run Keyword If	'${TYPE}' == 'shell'	Set Client Configuration	prompt=$
	...	ELSE	Run Keyword If	'${TYPE}' == 'menudriven'	Set Client Configuration	prompt=.
	Run Keyword If	'${RAW_MODE}'!='No'	CLI:Switch Connection	default

CLI:Close Current Connection
    [Documentation]
		...
    [Arguments]    ${SWITCH_DEFAULT}=No
    Close Connection
    Run Keyword If    '${SWITCH_DEFAULT}' == 'Yes'    CLI:Switch Connection    default

CLI:Create File Remotely
        [Documentation]
		...    Opens a SSH session to a Nodegrid host and creates a file
        ...
        ...    ${FILEPATH} waits for the path and filename
        ...
        ...    Example:
        ...
        ...    | CLI:Create File Remotely | /etc/scripts/custom_commands/test.py
        ...
    [Arguments]    ${FILEPATH}    ${CONTENT}=""
    CLI:Connect as Root
    CLI:Write    > ${FILEPATH}
    Run Keyword If    '${CONTENT}' != ''    CLI:Write    /bin/echo ${CONTENT} > ${FILEPATH}
    CLI:Close Current Connection    Yes

CLI:Append To File Remotely
        [Documentation]
		...    Opens a SSH session to a Nodegrid host and append content to file
        ...
        ...    ${FILEPATH} waits for the path and filename, while ${CONTENT} states for the file content
        ...
        ...    Example:
        ...
        ...    | CLI:Append To File Remotely | /etc/scripts/custom_commands/test.py | "def Test():\n	print('hello')"
    [Arguments]    ${FILEPATH}    ${CONTENT}
    CLI:Connect as Root
    CLI:Write    /bin/echo ${CONTENT} >> ${FILEPATH}
    CLI:Close Current Connection    Yes

CLI:Remove File Remotely
        [Arguments]	${FILEPATH}
        [Documentation]
		...    Opens a SSH session to a Nodegrid host and remove file if it exists
        ...
        ...    ${FILEPATH} waits for the path and filename
        ...
        ...     Example:
        ...
        ...     | CLI:Remove File Remotely | /etc/scripts/custom_commands/test.py
	CLI:Connect as Root
	${OUTPUT}=	CLI:Write   cat ${FILEPATH}
	${RESULT}=	Run Keyword And Return Status	Should Not Contain	${OUTPUT}	No such file or directory
	Run Keyword If	${RESULT}	CLI:Write	rm ${FILEPATH}
	CLI:Close Current Connection	Yes

CLI:Number of Network Interfaces
    [Documentation]
		...     Returns the amount of network interfaces which are available on a Nodegrid device
        ...
        ...    The result is returned as a interger
        ...
        ...    Examples:
        ...
        ...    | ${interfaces}=                   | CLI:Number of Network Interfaces     |
        ...    | Output                           | Amount of Network Interfaces are: 10 |
        ...    | Log to Console                   | ${interfaces}                        |
        ...    | Output                           | 10                                   |
        ...    |                                  |                                      |
        ...    | CLI:Number of Network Interfaces |                                      |
        ...    | Output                           | Amount of Network Interfaces are: 10 |
    ${OUTPUT}=    CLI:Enter Path    /system/network_statistics/
    ${OUTPUT}=    CLI:Ls
    @{INTERFACES}=    Split To Lines    ${OUTPUT}    0    -3
    ${NUMOFINTERFACES}=    Get Length    ${INTERFACES}
    ${NUMOFINTERFACES}=    Evaluate    ${NUMOFINTERFACES} - 2
    Log    \nAmount of Network Interfaces are: ${NUMOFINTERFACES}    INFO    console=yes
    [Return]    ${NUMOFINTERFACES}

CLI:Get Builtin Network Connections
	[Documentation]
	...	Returns a list of all the builting interfaces/connections available on
	...	system. ETH0 will always be returned first on the list
	[Arguments]	${INTERFACES_NAMES}=No
	${SYSTEM_NAME}=	CLI:Get System Name
	${IS_VM}=	CLI:Is VM System	${SYSTEM_NAME}
	${IS_NSC}=	CLI:Is Serial Console	${SYSTEM_NAME}
	${IS_NSCP}=	CLI:Is Serial Console Plus	${SYSTEM_NAME}
	${IS_BSR}=	CLI:Is Bold SR	${SYSTEM_NAME}
	${IS_GSR}=	CLI:Is Gate SR	${SYSTEM_NAME}
	${IS_NSR}=	CLI:Is Net SR	${SYSTEM_NAME}
	${INTERFACES}=	Create List
	Run Keyword If	${IS_NSR} and '${INTERFACES_NAMES}' == 'No'	Append To List	${INTERFACES}	ETH0	ETH1	BACKPLANE0	BACKPLANE1
	...	ELSE	Run Keyword If	${IS_BSR} and '${INTERFACES_NAMES}' == 'No'	Append To List	${INTERFACES}	ETH0	BACKPLANE0
	...	ELSE	Run Keyword If	${IS_GSR} and '${INTERFACES_NAMES}' == 'No'	Append To List	${INTERFACES}	ETH0	BACKPLANE0	BACKPLANE1	SFP0	SFP1
	...	ELSE	Run Keyword If	${IS_NSC} and '${INTERFACES_NAMES}' == 'No'	Append To List	${INTERFACES}	ETH0	ETH1
	...	ELSE	Run Keyword If	${IS_NSCP} and '${INTERFACES_NAMES}' == 'No'	Append To List	${INTERFACES}	ETH0	ETH1	SFP0	SFP1
	...	ELSE	Run Keyword If	${IS_VM} and '${INTERFACES_NAMES}' == 'No'	Append To List	${INTERFACES}	ETH0	ETH1
	...	ELSE	Run Keyword If	${IS_NSR} and '${INTERFACES_NAMES}' != 'No'	Append To List	${INTERFACES}	eth0	eth1	backplane0	backplane1
	...	ELSE	Run Keyword If	${IS_BSR} and '${INTERFACES_NAMES}' != 'No'	Append To List	${INTERFACES}	eth0	backplane0
	...	ELSE	Run Keyword If	${IS_GSR} and '${INTERFACES_NAMES}' != 'No'	Append To List	${INTERFACES}	eth0	backplane0	backplane1	sfp0	sfp1
	...	ELSE	Run Keyword If	${IS_NSC} and '${INTERFACES_NAMES}' != 'No'	Append To List	${INTERFACES}	eth0	eth1
	...	ELSE	Run Keyword If	${IS_NSCP} and '${INTERFACES_NAMES}' != 'No'	Append To List	${INTERFACES}	eth0	eth1	sfp0	sfp1
	...	ELSE	Run Keyword If	${IS_VM} and '${INTERFACES_NAMES}' != 'No'	Append To List	${INTERFACES}	eth0	eth1
	[Return]	${INTERFACES}

CLI:Has Interface
	[Arguments]	${INTERFACE}
	${INTERFACES}=	CLI:Get Builtin Network Connections	Yes
	${HAS_INTERFACE}=	Run Keyword And Return Status	List Should Contain Value	${INTERFACES}	${INTERFACE}
	[Return]	${HAS_INTERFACE}

CLI:Get Interface IP Address
	[Arguments]	${INTERFACE}	${IPV6}=No
	Set Client Configuration	prompt=#
	CLI:Write	shell sudo su -
	${IPV4_ADDRESS}=	Run Keyword If	'${IPV6}' == 'Yes'
	...	CLI:Write	ifconfig ${INTERFACE} | grep -Eo 'inet6 ([0-9]+\.*)+' | cut -d ' ' -f 2	user=Yes
	...	ELSE	CLI:Write	ifconfig ${INTERFACE} | grep -Eo 'inet( addr:| )([0-9]+\.*)+' | cut -d ' ' -f 2	user=Yes
	${IS_3_2}	Run Keyword And Return Status	Should Contain	${IPV4_ADDRESS}	addr:	#ifconfig in v3.2 produces different output
	${IPV4_ADDR_3_2}	Run Keyword If	${IS_3_2}	Remove String Using Regexp	${IPV4_ADDRESS}	addr:
	${IPV4_ADDRESS}	Set Variable If	${IS_3_2}	${IPV4_ADDR_3_2}	${IPV4_ADDRESS}
	Set Client Configuration	prompt=]#
	CLI:Write	exit
	[Return]	${IPV4_ADDRESS}

CLI:Get IPv4Addresses
    [Documentation]
		...    Returns the configured IPv4 network Addresses from the Settings::Network Connections section which are
        ...    available on a Nodegrid device and logs each result to the console
        ...
        ...    The result is returned as a list object
        ...
        ...    Examples:
        ...
        ...    | @{IPv4Address}=                   | CLI:Get IPv4Addresses           |
        ...    | Output                            | IPv4 Address is: 192.168.56.101 |
        ...    | Log to Console                    | ${IPv4Address}                  |
        ...    | Output                            |  [u'192.168.56.101']            |
    ${RETURN}=   CLI:Show    /settings/network_connections/
    @{LINES}=    Split To Lines    ${RETURN}    2    -3
    @{IPS}=     Create List
    FOR    ${LINE}    IN    @{LINES}
		Run Keyword Unless    'connected' in '${LINE}'    Continue For Loop
		${LINE}=    Replace String Using Regexp    ${LINE}    \\s\\s*    --
		@{ADDRESSLINE}=    Split String    ${LINE}    --
		${ADDRESSLINE}=    Get From List    ${ADDRESSLINE}    6
		@{IP}=    Split String    ${ADDRESSLINE}    /
		${IP}=    Get From List    ${IP}  0
		Log to Console    \nIPv4 Address is: ${IP}
		Append To List    ${IPS}    ${IP}
	END
    Log    \nIPv4 Addresses are: ${IPS}    INFO    console=yes
    [Return]    @{IPS}

CLI:Get IPv6Addresses
    [Documentation]
		...    Returns the configured IPv6 network Addresses  from the Settings::Network Connections section which are
        ...    available on a Nodegrid device and logs each result to the console
        ...
        ...    The result is returned as a list object
        ...
        ...    Examples:
        ...
        ...    | @{IPv6Address}=                   | CLI:Get IPv6Addresses                     |
        ...    | Output                            | IPv6 Address is: fe80::a00:27ff:fe0f:e9b2 |
        ...    | Log to Console                    | ${IPv6Address}                            |
        ...    | Output                            |  [u'fe80::a00:27ff:fe0f:e9b2']            |
    ${RETURN}=    CLI:Show    /settings/network_connections/
    @{LINES}=    Split To Lines    ${RETURN}    2    -3
    @{IPS}=    Create List
    FOR    ${LINE}    IN    @{LINES}
		Run Keyword Unless    'connected' in '${LINE}'    Continue For Loop
		${LINE}=    Replace String Using Regexp    ${LINE}    \\s\\s*    --
		@{ADDRESSLINE}=    Split String    ${LINE}    --
		${ADDRESSLINE}=    Get From List   ${ADDRESSLINE}    7
		@{IP}=    Split String    ${ADDRESSLINE}    /
		${IP}=    Get From List    ${IP}    0
		Log to Console    \nIPv6 Address is: ${IP}
		Append To List    ${IPS}    ${IP}
    END
    Log    \nIPv6 Addresses are: ${IPS}    INFO    console=yes
    [Return]    @{IPS}

CLI:Get MACAddresses
    [Documentation]
		...    Returns all the MAC Addresses from the Settings::Network Connections section of the NodeGrid device and
        ...    Logs the results to the Console
        ...
        ...    The result is returned as a list object
        ...
        ...    Examples:
        ...
        ...    | @{MACAddress}=                    | CLI:Get MACAddresses              |
        ...    | Output                            | MAC Address is: 08:00:27:0f:e9:b2 |
        ...    | Log to Console                    | ${MACAddress}                     |
        ...    | Output                            |  [u'08:00:27:0f:e9:b2']           |
    ${RETURN}=    CLI:Show    /settings/network_connections/
    @{LINES}=    Split To Lines    ${RETURN}    2    -3
    @{MACS}=    Create List
    FOR    ${LINE}    IN    @{LINES}
		Run Keyword Unless    'connected' in '${LINE}'    Continue For Loop
		${LINE}=    Replace String Using Regexp    ${LINE}    \\s\\s*    --
		@{ADDRESSLINE}=    Split String    ${LINE}  --
		${MAC}=    Run Keyword If    '${NGVERSION}' >= '4.0'    Get From List    ${ADDRESSLINE}    10
		...    ELSE    Get From List    ${ADDRESSLINE}    8
		Append To List    ${MACS}    ${MAC}
    END
    Log    \nMAC Addresses are: ${MACS}    INFO    console=yes
    [Return]  @{MACS}

CLI:Get Network System Name
    [Documentation]
		...    Returns the the Nodegrid system name from the Settings::Network_Settings section and Logs the results
        ...    to the Console
        ...
        ...    The result is returned as a string object
        ...
        ...    Examples:
        ...
        ...    | ${SYSTEMNAME}=                    | CLI:Get Network System Name               |
        ...    | Output                            | System Name: nodegrid.localdomain |
        ...    | Log to Console                    | ${SYSTEMNAME}                     |
        ...    | Output                            |  nodegrid.localdomain             |
    ${RETURN}=    CLI:Show    /settings/network_settings/
    @{LINES}=    Split To Lines    ${RETURN}    0    2
    ${SYSTEMNAME}=    Set Variable    ${EMPTY}
    FOR    ${LINE}    IN    @{LINES}
		@{Values}=    Split String    ${LINE}     =
		${Value}=    Get From List   ${Values}    1
		${Value}=    Strip String    ${Value}
		${SYSTEMNAME}=    Catenate    SEPARATOR=    ${SYSTEMNAME}.${Value}
    END
    ${SYSTEMNAME}=     Get Substring    ${SYSTEMNAME}   1
    Log  \nSystem Name: ${SYSTEMNAME}    INFO       console=yes
    [Return]    ${SYSTEMNAME}

CLI:Get System Description
    [Documentation]
		...    Returns the the Nodegrid system description from the System::About section and Logs the results to the
        ...    Console
        ...
        ...    The result is returned as a string object
        ...
        ...    Examples:
        ...
        ...    | ${SYSTEMDESCRIPTION}=             | CLI:Get System Description    |
        ...    | Output                            | System Description: NodeGrid Manager - 3.2.40 |
        ...    | Log to Console                    | ${SYSTEMDESCRIPTION} |
        ...    | Output                            | NodeGrid Manager - 3.2.40   |
    ${RETURN}=  CLI:Show	/system/about/
    @{lines_list}=     Split To Lines      ${RETURN}      0   -1
    &{dict}=    Create Dictionary
    FOR     ${ELEMENT}      IN      @{lines_list}
		Log  Start Loop for ${ELEMENT}   INFO
		${len}=  Get Length      ${ELEMENT}
		Log  Lenghth of element ${len}   INFO
		${v1}	${v2}=	Run Keyword If   ${len} > 0     Split String	${ELEMENT}	:	1
		${v1}=      Run Keyword If	${len} > 0	Strip String    ${v1}
		${v2}=      Run Keyword If	${len} > 0	  Strip String    ${v2}
		Run Keyword If	${len} > 0	Set To Dictionary	${dict}	    ${v1}	${v2}
    END
    ${SYSTEM}=    Get From Dictionary    ${dict}    system
    ${MANAGER}=    Run Keyword If  '${NGVERSION}' < '4.1'    Run Keyword And Return Status    Should Contain    ${SYSTEM}    NodeGrid Manager
    ...    ELSE    Run Keyword And Return Status    Should Contain    ${SYSTEM}    Nodegrid Manager
    ${PART_NUM}=    Run Keyword If    ${MANAGER} == False    Get From Dictionary    ${dict}    part_number
    ${VERSION}=    Get From Dictionary    ${dict}    software
    ${VERSION_NUMBER}    ${REST}=    Split String	${VERSION}    (    max_split=1
    ${VERSION_NUMBER}=    Fetch From Right    ${VERSION_NUMBER}    v
    ${VERSION_NUMBER}=    Strip String    ${VERSION_NUMBER}    mode=both
    ${SYSTEMDESCRIPTION}=    Run Keyword If    ${MANAGER}    Catenate    SEPARATOR=    ${SYSTEM} - ${VERSION_NUMBER}
    ...    ELSE    Catenate    SEPARATOR=    ${PART_NUM} - ${VERSION_NUMBER}
    Log    \nSystem Description: ${SYSTEMDESCRIPTION}    INFO    console=yes
    [Return]    ${SYSTEMDESCRIPTION}

CLI:Get System Name
	[Documentation]
		...    Returns the system name of the target device
		...
		...    The result is returned as a string object with the name of the system
		...
		...    Examples:
		...
		...    | ${Console}=             | CLI:Get System Name    |
		...    | Output                            | System Description: NodeGrid Manager |
		...    | Log to Console                    | ${Console} |
		...    |  |    |
		...    | Output                            | Nodegrid Manager   |
		...    | ${Console}=             | CLI:Get System Name    |
		...    | Output                            | System Description: NodeGrid Serial Console |
		...    | Log to Console                    | ${Console} |
		...    | Output                            | NodeGrid Serial Console   |
	${RETURN}=	CLI:Show	/system/about/ system	user=yes
	${SYSTEM}=	Fetch From Right	${RETURN}	system:${SPACE}
	${SYSTEM}=	Fetch From Left	${SYSTEM}	\n
	Log	\nSystem Description: ${SYSTEM}	INFO	console=yes
	Return From Keyword	${SYSTEM}

CLI:Get System Model
	[Documentation]
		...    Returns the system model of the target device
		...
		...    The result is returned as a string object with the model of the system
		...
		...    Examples:
		...
		...    | ${Console}=             | CLI:Get System Model    |
		...    | Output                            | System Description: Nodegrid Manager |
		...    | Log to Console                    | ${Console} |
		...    |  |    |
		...    | Output                            | Nodegrid Manager   |
		...    | ${Console}=             | CLI:Get System Model    |
		...    | Output                            | System Description: Nodegrid Serial Console |
		...    | Log to Console                    | ${Console} |
		...    | Output                            | Nodegrid Serial Console   |
	${RETURN}=	CLI:Show	/system/about/
	${SYSTEM}=	Fetch From Right	${RETURN}	model:${SPACE}
	${SYSTEM}=	Fetch From Left	${SYSTEM}	${\n}
	Log	\nSystem Description: ${SYSTEM}	INFO	console=yes
	[Return]	${SYSTEM}

CLI:Get System Version
	[Documentation]
	...	Get the system full version in format X.Y.Z, ex. 2.2.10
	...
	...	Must be logged as admin
	${VERSION}=	CLI:Write	shell cat /software | grep ^VERSION= | cut -d '=' -f 2	user=Yes
	[Return]	${VERSION}

CLI:Get Serial Number
	[Documentation]
	...	Get the serial number from /system/about/
	...
	...	Must be logged as admin
	${OUTPUT}=	CLI:Show	/system/about/
	${SERIAL_NUMBER_AND_AFTER}=	Fetch From Right	${OUTPUT}	serial_number:${SPACE}
	${SERIAL_NUMBER}=	Get Line	${SERIAL_NUMBER_AND_AFTER}	0
	Log	\nSystem Serial Number: ${SERIAL_NUMBER}	INFO	console=yes
	[Return]	${SERIAL_NUMBER}

CLI:Get System Major And Minor Version
	[Documentation]
	...	Get the system version in format X.Y (major and minor), ex. 2.2
	...
	...	Must be logged as admin
	${VERSION}=	CLI:Write	shell cat /software | grep ^VERSION= | cut -d '=' -f 2 | grep -Eo ^[0-9]+\.[0-9]+	user=Yes
	[Return]	${VERSION}

CLI:Is VM System
	[Documentation]
		...	Returns True if System is a Virtual Machine
	[Arguments]	${SYSTEM_NAME}=${EMPTY}
	${SYSTEM_NAME}=	Run Keyword If	'${SYSTEM_NAME}' == '${EMPTY}'
	...	CLI:Get System Name
	...	ELSE	Set Variable	${SYSTEM_NAME}
	Return From Keyword If	'${SYSTEM_NAME}' == 'Nodegrid Manager'	${TRUE}
	Return From Keyword If	'${SYSTEM_NAME}' == 'NodeGrid Manager'	${TRUE}
	Return From Keyword If	'${SYSTEM_NAME}' == 'Virtual Machine'	${TRUE}
	[Return]	${FALSE}

CLI:Is Serial Console
	[Documentation]
		...    Returns True if the unit is a Serial Console based on CLI:Get System Name keyword
		...
		...    Result is either 1 (True) if System Description contains "NodeGrid Serial Console" or 0 (False) if not
		...
		...    The result is returned as a string object
		...
		...    Examples:
		...
		...    | ${Console}=             | CLI:Is Serial Console    |
		...    | Output                            | System Description: NodeGrid Manager |
		...    | Log to Console                    | ${Console} |
		...    |  |    |
		...    | Output                            | 0   |
		...    | ${Console}=             | CLI:Is Serial Console    |
		...    | Output                            | System Description: NodeGrid Serial Console |
		...    | Log to Console                    | ${Console} |
		...    | Output                            | 1   |
	[Arguments]	${SYSTEM_NAME}=${EMPTY}
	${SYSTEM_NAME}=	Run Keyword If	'${SYSTEM_NAME}' == '${EMPTY}'
	...	CLI:Get System Name
	...	ELSE	Set Variable	${SYSTEM_NAME}
	Return From Keyword If	'${SYSTEM_NAME}' == 'Nodegrid Serial Console'	${TRUE}
	Return From Keyword If	'${SYSTEM_NAME}' == 'NodeGrid Serial Console'	${TRUE}
	[Return]	${FALSE}

CLI:Is Serial Console Plus
	[Documentation]
		...    Returns True if the unit is a Serial Console Plus based on CLI:Get System Name keyword
		...
		...    Result is either 1 (True) if System Description contains "NodeGrid Serial Console Plus" or 0 (False) if not
		...
		...    The result is returned as a string object
		...
		...    Examples:
		...
		...    | ${Console}=             | CLI:Is Serial Console Plus    |
		...    | Output                            | System Description: NodeGrid Manager |
		...    | Log to Console                    | ${Console} |
		...    |  |    |
		...    | Output                            | 0   |
		...    | ${Console}=             | CLI:Is Serial Console Plus    |
		...    | Output                            | System Description: NodeGrid Serial Console Plus |
		...    | Log to Console                    | ${Console} |
		...    | Output                            | 1   |
	[Arguments]	${SYSTEM_NAME}=${EMPTY}
	${SYSTEM_NAME}=	Run Keyword If	'${SYSTEM_NAME}' == '${EMPTY}'
	...	CLI:Get System Name
	...	ELSE	Set Variable	${SYSTEM_NAME}
	Return From Keyword If	'${SYSTEM_NAME}' == 'Nodegrid Serial Console Plus'	${TRUE}
	[Return]	${FALSE}

CLI:Is 48 Ports Serial Console
	[Documentation]
		...    Returns True if the unit is a Serial Console based on CLI:Get System Name keyword with 48 ports
		...
		...    Result is either 1 (True) if System Description contains "NodeGrid Serial Console" with 48 ports
		...    or 0 (False) if not
		...
		...    The result is returned as a string object
	${SYSTEM_MODEL}=	CLI:Get System Model
	${IS_NSC48}=	Run Keyword And Return Status 	Should Contain	${SYSTEM_MODEL}	model: NSC-T48
	Return From Keyword If	${IS_NSC48}	${TRUE}
	[Return]	${FALSE}

CLI:Is 96 Ports Serial Console
	[Documentation]
		...    Returns True if the unit is a Serial Console based on CLI:Get System Name keyword with 96 ports
		...
		...    Result is either 1 (True) if System Description contains "NodeGrid Serial Console" with 96 ports
		...    or 0 (False) if not
		...
		...    The result is returned as a string object
	${SYSTEM_MODEL}=	CLI:Get System Model
	${IS_NSC96}= 	Run Keyword And Return Status 	Should Contain	${SYSTEM_MODEL}	model: NSC-T96
	Return From Keyword If	${IS_NSC96}	${TRUE}
	[Return]	${FALSE}

CLI:Is Bold SR
	[Documentation]
		...	Returns True if System is a Nodegrid Bold SR
	[Arguments]	${SYSTEM_NAME}=${EMPTY}
	${SYSTEM_NAME}=	Run Keyword If	'${SYSTEM_NAME}' == '${EMPTY}'
	...	CLI:Get System Name
	...	ELSE	Set Variable	${SYSTEM_NAME}
	Return From Keyword If	'${SYSTEM_NAME}' == 'Nodegrid Bold SR'	${TRUE}
	[Return]	${FALSE}

CLI:Is Link SR
	[Documentation]
		...	Returns True if System is a Nodegrid Link SR
	[Arguments]	${SYSTEM_NAME}=${EMPTY}
	${SYSTEM_NAME}=	Run Keyword If	'${SYSTEM_NAME}' == '${EMPTY}'
	...	CLI:Get System Name
	...	ELSE	Set Variable	${SYSTEM_NAME}
	Return From Keyword If	'${SYSTEM_NAME}' == 'Nodegrid Link SR'	${TRUE}
	Return From Keyword If	'${SYSTEM_NAME}' == 'NodeGrid Link SR'	${TRUE}
	[Return]	${FALSE}

CLI:Is Gate SR
	[Documentation]
		...	Returns True if System is a Nodegrid Gate SR
	[Arguments]	${SYSTEM_NAME}=${EMPTY}
	${SYSTEM_NAME}=	Run Keyword If	'${SYSTEM_NAME}' == '${EMPTY}'
	...	CLI:Get System Name
	...	ELSE	Set Variable	${SYSTEM_NAME}
	Return From Keyword If	'${SYSTEM_NAME}' == 'Nodegrid Gate SR'	${TRUE}
	Return From Keyword If	'${SYSTEM_NAME}' == 'NodeGrid Gate SR'	${TRUE}
	[Return]	${FALSE}

CLI:Is Net SR
	[Documentation]
		...	Returns True if System is a Nodegrid Bold SR
	[Arguments]	${SYSTEM_NAME}=${EMPTY}
	${SYSTEM_NAME}=	Run Keyword If	'${SYSTEM_NAME}' == '${EMPTY}'
	...	CLI:Get System Name
	...	ELSE	Set Variable	${SYSTEM_NAME}
	Return From Keyword If	'${SYSTEM_NAME}' == 'Nodegrid Services Router'	${TRUE}
	Return From Keyword If	'${SYSTEM_NAME}' == 'Nodegrid Service Router'	${TRUE}
	Return From Keyword If	'${SYSTEM_NAME}' == 'NodeGrid Services Router'	${TRUE}
	Return From Keyword If	'${SYSTEM_NAME}' == 'NodeGrid Service Router'	${TRUE}
	Return From Keyword If	'${SYSTEM_NAME}' == 'Nodegrid Net SR'	${TRUE}
	[Return]	${FALSE}

CLI:Is Hive SR
	[Documentation]
		...	Returns True if System is a Nodegrid Hive SR
	[Arguments]	${SYSTEM_NAME}=${EMPTY}
	${SYSTEM_NAME}=	Run Keyword If	'${SYSTEM_NAME}' == '${EMPTY}'
	...	CLI:Get System Name
	...	ELSE	Set Variable	${SYSTEM_NAME}
	Return From Keyword If	'${SYSTEM_NAME}' == 'Nodegrid Hive SR'	${TRUE}
	Return From Keyword If	'${SYSTEM_NAME}' == 'NodeGrid Hive SR'	${TRUE}
	[Return]	${FALSE}

CLI:Is SR System
	[Arguments]	${SYSTEM_NAME}=${EMPTY}
	${SYSTEM_NAME}=	Run Keyword If	'${SYSTEM_NAME}' == '${EMPTY}'
	...	CLI:Get System Name
	...	ELSE	Set Variable	${SYSTEM_NAME}
	${IS_BSR}=	CLI:Is Bold SR	${SYSTEM_NAME}
	${IS_GSR}=	CLI:Is Gate SR	${SYSTEM_NAME}
	${IS_NSR}=	CLI:Is Net SR	${SYSTEM_NAME}
	${IS_LSR}=	CLI:Is Link SR	${SYSTEM_NAME}
	${IS_HSR}=	CLI:Is Hive SR	${SYSTEM_NAME}
	Return From Keyword If	${IS_BSR} or ${IS_GSR} or ${IS_NSR} or ${IS_LSR} or ${IS_HSR}	${TRUE}
	[Return]	${FALSE}

CLI:Is Serial Console System
	[Arguments]	${SYSTEM_NAME}=${EMPTY}
	${SYSTEM_NAME}=	Run Keyword If	'${SYSTEM_NAME}' == '${EMPTY}'
	...	CLI:Get System Name
	...	ELSE	Set Variable	${SYSTEM_NAME}
	${IS_NSC}=	CLI:Is Serial Console	${SYSTEM_NAME}
	${IS_NSCP}=	CLI:Is Serial Console Plus	${SYSTEM_NAME}

	Return From Keyword If	${IS_NSC} or ${IS_NSCP}	${TRUE}
	[Return]	${FALSE}

CLI:Has Serial Ports Support
	[Documentation]
		...    Returns True if the unit have serial based on the CLI:Get System Name keyword
		...
		...    Result is either 1 (True) if System Description contains "NodeGrid Serial Console" or 0 (False) if not
		...
		...    The result is returned as a boolean object
		...
		...    Examples:
		...
		...    | ${Console}=             | CLI:Has Serial Ports Support    |
		...    | Output                            | System Description: NodeGrid Manager |
		...    | Log to Console                    | ${Console} |
		...    |  |    |
		...    | Output                            | 0   |
		...    | ${Console}=             | CLI:Has Serial Ports Support    |
		...    | Output                            | System Description: NodeGrid Serial Console |
		...    | Log to Console                    | ${Console} |
		...    | Output                            | 1   |
	${SYSTEM_NAME}=	CLI:Get System Name
	${IS_VM}=	CLI:Is VM System	${SYSTEM_NAME}
	${IS_SERIAL_CONSOLE}=	CLI:Is Serial Console System	${SYSTEM_NAME}
	${IS_SR}=	CLI:Is SR System	${SYSTEM_NAME}

	Return From Keyword If	${IS_SERIAL_CONSOLE} or ${IS_SR}	${TRUE}
	[Return]	${FALSE}

CLI:Has Serial Ports
	[Documentation]
	...	Check if device has serial
	${SERIAL_PORTS}=	CLI:Get Serial Devices List
	${HAS_SERIAL_PORTS}=	Run Keyword And Return Status	Should Not Be Empty	${SERIAL_PORTS}
	[Return]	${HAS_SERIAL_PORTS}

CLI:Has USB Ports
	[Documentation]
	...	Check if device has usb ports
	${USB_PORTS}=	CLI:Get USB Devices List
	${HAS_USB_PORTS}=	Run Keyword And Return Status	Should Not Be Empty	${USB_PORTS}
	[Return]	${HAS_USB_PORTS}

CLI:Has USB Sensor
	[Documentation]
	...	Check If device has any USB sensor connected
	CLI:Enter Path	/system/usb_devices/
	${HAS_USB_SENSOR}=	Run Keyword And Return Status	CLI:Test Show Command	Sensor Device
	[Return]	${HAS_USB_SENSOR}

CLI:Has ETH1
	${SYSTEM_NAME}=	CLI:Get System Name
	${IS_BSR}=	CLI:Is Bold SR	${SYSTEM_NAME}
	${IS_GSR}=	CLI:Is Gate SR	${SYSTEM_NAME}

	Return From Keyword If	${IS_BSR} or ${IS_GSR}	${FALSE}
	[Return]	${TRUE}

CLI:Has Wireless Modem Support
	${SYSTEM_NAME}=	CLI:Get System Name
	${IS_SR}=	CLI:Is SR System	${SYSTEM_NAME}
	${IS_NSCP}=	CLI:Is Serial Console Plus	${SYSTEM_NAME}

	Return From Keyword If	${IS_SR} or ${IS_NSCP}	${TRUE}
	[Return]	${FALSE}

CLI:Has Wireless Modem With SIM Card
	[Documentation]
	...	Check If device has any wireless modem with SIM card
	CLI:Enter Path	/system/wireless_modem/
	${OUTPUT}=	CLI:Show
	${HAS_WMODEM_WITH_SIM_CARD}=	Run Keyword And Return Status
	...	Should Contain Any	${OUTPUT}	@{GSM_WMODEM_CARRIERS_APN.keys()}
	[Return]	${HAS_WMODEM_WITH_SIM_CARD}

CLI:Get Wireless Modems SIM Card Info
	[Documentation]
	...	Get devices wireless modem with SIM card information
	...
	...	Position argument indicates a postion (0, 1) to return a single wireless
	...	modem slot information
	[Arguments]	${POSITION}=${EMPTY}
	${HAS_WMODEM_WITH_SIM_CARD}=	CLI:Has Wireless Modem With SIM Card
	Return From Keyword If	not ${HAS_WMODEM_WITH_SIM_CARD}	${FALSE}	${NONE}

	CLI:Enter Path	/system/wireless_modem/
	${OUTPUT}=	CLI:Show
	${WMODEMS}=	Split To Lines	${OUTPUT}	2
	${INTERFACES_INFO}=	Create List

	FOR	${WMODEM}	IN	@{WMODEMS}
		${HAS_SIM_CARD}=	Run Keyword And Return Status
		...	Should Contain Any	${WMODEM}	@{GSM_WMODEM_CARRIERS_APN.keys()}
		${WMODEM_INFO}=	Run Keyword If	${HAS_SIM_CARD}
		...	CLI:Get Wireless Modem Info	${WMODEM}
		Run Keyword If	${HAS_SIM_CARD}	Append To List	${INTERFACES_INFO}	${WMODEM_INFO}
	END
	${MODEM_INFO}=	Run Keyword If	'${POSITION}' != '${EMPTY}'	Get From List	${INTERFACES_INFO}	${POSITION}
	Run Keyword If	'${POSITION}' != '${EMPTY}'	Return From Keyword	${TRUE}	${MODEM_INFO}
	[Return]	${TRUE}	${INTERFACES}

CLI:Get Wireless Modem Info
	[Documentation]
	...	Keyword for internal use of CLI:Get Wireless Modems SIM Card Info
	[Arguments]	${WMODEM}
	${GSM_INTERFACE}=	Get Regexp Matches	${WMODEM}	(cdc-wdm\\d+)	1
	${GSM_INTERFACE}=	Get From List	${GSM_INTERFACE}	0
	${GSM_SHELL_INTERFACE_NUMBER}=	Get Regexp Matches	${WMODEM}	cdc-wdm(\\d+)	1
	${GSM_SHELL_INTERFACE_NUMBER}=	Get From List	${GSM_SHELL_INTERFACE_NUMBER}	0
	FOR	${CARRIER}	IN	@{GSM_WMODEM_CARRIERS_APN.keys()}
		${HAS_CARRIER}=	Run Keyword And Return Status	Should Contain	${WMODEM}	${CARRIER}
		&{WMODEM_INFO}=	Run Keyword If	${HAS_CARRIER}
		...	Create Dictionary	carrier=${CARRIER}	interface=${GSM_INTERFACE}	shell interface=wwan${GSM_SHELL_INTERFACE_NUMBER}	apn=${GSM_WMODEM_CARRIERS_APN["${CARRIER}"]}
		Return From Keyword If	${HAS_CARRIER}	${WMODEM_INFO}
	END

CLI:Get Wireless Modem Model
	[Documentation]
	...	Get the currently installed wireless modem model
	...
	...	Must be connected as admin
	[Arguments]	${MODEM_NUMBER}=0
	Set Client Configuration	prompt=#
	CLI:Write	shell sudo su -
	${MODEM_MODEL}=	CLI:Write	mmcli -m ${MODEM_NUMBER} | grep "model:" | cut -d "'" -f 2	user=Yes
	Set Client Configuration	prompt=]#
	CLI:Write	exit
	[Return]	${MODEM_MODEL}

CLI:Read Until Prompt Removing User Line
    [Documentation]
		...    Basically the CLI:Read Until Keyword, but removing line with user [user@...]
    ...
    [Arguments]    ${RAW_Mode}=No    ${lines}=Yes
    ${OUTPUT}=    Read Until Prompt
    ${OUTPUT}=    Remove String Using Regexp    ${OUTPUT}    ['\r']
    ${REVERT_STATUS}=	Run Keyword And Return Status	Should Contain	${OUTPUT}	Please revert
    ${REVERT_IS_VALID}=	Run Keyword And Return Status	Should Match Regexp	${OUTPUT}	\\[\\+.*@
    Run Keyword If    ${REVERT_STATUS} and ${REVERT_IS_VALID} and '${RAW_Mode}' == 'No'    CLI:Revert
    Run Keyword If    ${REVERT_STATUS} and '${NGVERSION}' == '3.2'    Set Tags    NON-CRITICAL
    Run Keyword If    ${REVERT_STATUS} and '${RAW_Mode}' == 'No'    Fail    ${OUTPUT}
    Run Keyword If    not ${REVERT_STATUS} and '${RAW_Mode}' == 'No'    Should Not Contain    ${OUTPUT}    Error:    ignore_case=True
    Log    \n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${OUTPUT}    INFO    console=yes
    ${OUTPUT}=    Fetch From Left    ${OUTPUT}    [${USERNAME}
    ${OUTPUT}=    Fetch From Left    ${OUTPUT}    [+${USERNAME}
    ${OUTPUT}=    Fetch From Left    ${OUTPUT}    root@
    ${OUTPUT}=    Fetch From Left    ${OUTPUT}    ${\n}Try
    ${OUT}=    Remove String    ${OUTPUT}    ${\n}
    ${OUT}=    Remove String    ${OUTPUT}    \n
    Return From Keyword If    '${lines}' == 'Yes'    ${OUT}
    [Return]    ${OUTPUT}

CLI:Read Until Prompt
        [Documentation]
		...    Reads the Output until the Prompt is presented. The Keyword returns the Output which was returned and
        ...    strips it of any "return" charaters
        ...    to improve parsebility of the output
        ...
        ...    The returned output is checked to ensure that no Error has occured. The Argument ${RAW_Mode} accepts the
        ...    value 'RAW' or 'Yes' In which case the output
        ...    will be returned without the error checking
        ...
        ...    The result is returned as a string object
        ...
        ...    Examples:
        ...
        ...    | Write | ls |
        ...    | ${Console}=             | CLI:Read Until Prompt   |
        ...    | Output                            | access  |
        ...    |  |  system  |
        ...    |  |  settings  |
        ...    |  |  [admin@nodegrid /]#  |
        ...    |  |    |
        ...    | Write | cancel |
        ...    | ${Console}=             | CLI:Read Until Prompt    |
        ...    | Output                            | Error: Invalid command: cancel | |  Test Failed |
        ...    |  |  [admin@nodegrid /]#  |
        ...    |  |    |
        ...    | Write | cancel |
        ...    | ${Console}=             | CLI:Read Until Prompt    | Raw |
        ...    | Output                            | Error: Invalid command: cancel | |  Test will Passes |
        ...    |  |  [admin@nodegrid /]#  |
    [Arguments]    ${RAW_Mode}=No
    ${OUTPUT}=    Read Until Prompt
    ${OUTPUT}=    Remove String Using Regexp    ${OUTPUT}    ['\r']
    Log    \n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${OUTPUT}    INFO    console=yes

    ${REVERT_STATUS}=	Run Keyword And Return Status	Should Contain	${OUTPUT}	Please revert
    ${REVERT_IS_VALID}=	Run Keyword And Return Status	Should Match Regexp	${OUTPUT}	\\[\\+.*@
    #${IS_NOT_TEARDOWN}=	Run Keyword And Return Status	Set Tags	NON-CRITICAL	#this keyword only fails on Teardown
    Run Keyword If    ${REVERT_STATUS} and ${REVERT_IS_VALID} and '${RAW_Mode}' == 'No'    CLI:Revert
    # If please revert happens during a testcase or during a setup, the test/setup is skipped
    #Run Keyword If    ${REVERT_STATUS} and '${RAW_Mode}' == 'No' and ${IS_NOT_TEARDOWN}     	Skip    ${OUTPUT} happened during a testcase
    # If please revert happens during teardown, a warning is set, but all the other keywords coming after will be executed
    #Run Keyword If    ${REVERT_STATUS} and '${RAW_Mode}' == 'No' and not ${IS_NOT_TEARDOWN}
    #...	Run Keyword And Warn On Failure	Fail	${OUTPUT} happened on ${SUITE_NAME} Teardown
    Run Keyword If    not ${REVERT_STATUS} and '${RAW_Mode}' == 'No'    Should Not Contain    ${OUTPUT}    Error:    ignore_case=True
    [Return]    ${OUTPUT}

CLI:Write
    [Documentation]
		...    Will Write the provided argument to the CLI.
        ...
        ...    The Keyword returns the Output which was returned and strips it of any "return" charaters
        ...    to improve parsebility of the output
        ...
        ...    The returned output is checked to ensure that no Error has occured. The Argument ${RAW_Mode} accepts
        ...    the value 'RAW' In which case the output
        ...    will be returned without the error checking
        ...
        ...    The result is returned as a string object
        ...
        ...    Examples:
        ...
        ...    | ${Console}= | CLI:Write | ls |
        ...    | Output      | access  | |
        ...    |  |  system  | |
        ...    |  |  settings  | |
        ...    |  |  [admin@nodegrid /]#  | |
        ...    |  |    |
        ...    | ${Console}=             | CLI:Write    | cancel |
        ...    | Output                            | Error: Invalid command: cancel | |  Test Failed |
        ...    |  |  [admin@nodegrid /]#  |
        ...    |  |    |
        ...    | ${Console}=             | CLI:Write    | cancel | Raw |
        ...    | Output                            | Error: Invalid command: cancel | |  Test will Passes |
        ...    |  |  [admin@nodegrid /]#  |
        ...
    [Arguments]    ${FIELD}    ${RAW_Mode}=No    ${user}=No	${lines}=Yes
    Log    \n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\n${FIELD}    INFO    console=yes
    Run Keyword If	'${RAW_Mode}' != 'No'	Log To Console	\n________________\nInput Mode: Raw |	no_newline=true
    Write    ${FIELD}
    ${OUTPUT}=    Run Keyword If    '${user}' == 'No'    CLI:Read Until Prompt    ${RAW_Mode}
    ...    ELSE    CLI:Read Until Prompt Removing User Line    ${RAW_Mode}	${lines}
    [Return]    ${OUTPUT}

CLI:Write Bare
    [Documentation]
		...    Will Write Bare the provided argument to the CLI.
        ...
        ...    The Keyword returns the Output which was returned and strips it of any "return" charaters
        ...    to improve parsebility of the output
        ...
        ...    The returned output is checked to ensure that no Error has occured. The Argument ${RAW_Mode} accepts the
        ...    value 'RAW' In which case the output
        ...    will be returned without the error checking
        ...
        ...    The result is returned as a string object
        ...
        ...    Examples:
        ...
        ...    | ${Console}= | CLI:Write | ls |
        ...    | Output      | access  | |
        ...    |  |  system  | |
        ...    |  |  settings  | |
        ...    |  |  [admin@nodegrid /]#  | |
        ...    |  |    |
        ...    | ${Console}=             | CLI:Write    | cancel |
        ...    | Output                            | Error: Invalid command: cancel | |  Test Failed |
        ...    |  |  [admin@nodegrid /]#  |
        ...    |  |    |
        ...    | ${Console}=             | CLI:Write    | cancel | Raw |
        ...    | Output                            | Error: Invalid command: cancel | |  Test will Passes |
        ...    |  |  [admin@nodegrid /]#  |
        ...
    [Arguments]    ${ARG}    ${user}=No
    Log    \n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\n${ARG}    INFO    console=yes
    Write Bare    ${ARG}
    ${OUTPUT}=    Run Keyword If    '${user}' == 'No'    CLI:Read Until Prompt
    ...    ELSE    CLI:Read Until Prompt Removing User Line
    [Return]    ${OUTPUT}

CLI:Enter Path
	[Documentation]
		...    Will navigate to provided PATH.
        ...
        ...    The Keyword returns the Output which was returned and strips it of any "return" charaters
        ...    to improve parsebility of the output
        ...
        ...    The returned output is checked to ensure that no Error has occured. The Argument ${RAW_Mode} accepts the
        ...    value 'RAW' In which case the output
        ...    will be returned without the error checking
        ...
        ...    The result is returned as a string object
        ...
        ...    Examples:
        ...
        ...    | ${Console}= | CLI:Enter Path | /access |
        ...    | Output      |   | |
        ...    |  |  [admin@nodegrid /]#  | |
        ...    |  |    |
        ...    | ${Console}=             | CLI:Enter Path    | /test/ |
        ...    | Output                            | Error: Invalid path: test | | |  Test Failed |
        ...    |  |  [admin@nodegrid /]#  |
        ...    |  |    |
        ...    | ${Console}=             | CLI:Enter Path    | /test/ | Raw |
        ...    | Output                            | Error: Invalid path: test | | | Test will Pass |
        ...    |  |  [admin@nodegrid /]#  |
    [Arguments]    ${PATH}    ${RAW_Mode}=No
    ${OUTPUT}=    CLI:Write    cd ${PATH}    ${RAW_Mode}
    [Return]    ${OUTPUT}

CLI:Enter Cloud Settings Path
    [Documentation]
		...    Will navigate to correct cloud path depending on Nodegrid Version provided in the ${NGVERSION} variable
        ...
        ...    The Keyword returns the Output which was returned and strips it of any "return" charaters
        ...    to improve parsebility of the output
        ...
        ...    The returned output is checked to ensure that no Error has occured.
        ...
        ...    The result is returned as a string object
        ...
        ...    Examples:
        ...
        ...    | ${Console}= | CLI:Enter Cloud Settings Path | Nodegrid Version 3.2 |
        ...    | Output      |   | |
        ...    |  |  [admin@nodegrid cloud]#  | |
        ...    |  |    |
        ...    | ${Console}= | CLI:Enter Cloud Settings Path |  Nodegrid Version 4.0 |
        ...    | Output                            |  | |
        ...    |  |  [admin@nodegrid settings]#  |
        ...    |  |    |
        ...
    ${OUTPUT}=    Run Keyword If  '${NGVERSION}' < '4.1'	CLI:Enter Path    /settings/cloud
    ...		ELSE 	CLI:Enter Path    /settings/cluster
    Run Keyword If    '${NGVERSION}' > '3.2'    CLI:Enter Path    settings
    [Return]    ${OUTPUT}

CLI:Add
    [Documentation]
		...    Executes an add command
        ...
        ...    The Keyword returns the Output which was returned and strips it of any "return" charaters
        ...    to improve parsebility of the output
        ...
        ...    The returned output is checked to ensure that no Error has occured. The Argument ${RAW_Mode} accepts
        ...    the value 'RAW' In which case the output
        ...    will be returned without the error checking
        ...
        ...    The result is returned as a string object
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | ${Console}= | CLI:Add |  |
        ...    | Output      |   | |
        ...    |  |  [admin@nodegrid devices]#  | |
        ...    |  |  [admin@nodegrid {devices}]#  | |
        ...    |  |    |
        ...    | CLI:Enter Path | /access |  |
        ...    | ${Console}=             | CLI:Add    |  |
        ...    | Output                            | Error: Invalid command: add | |  Test Failed |
        ...    |  |  [admin@nodegrid access]#' contains 'Error'  |
        ...    |  |    |
        ...    | CLI:Enter Path | /access |  |
        ...    | ${Console}=             | CLI:Add    | Raw |
        ...    | Output                            | Error: Invalid command: add | |  Test will Pass |
        ...    |  |  [admin@nodegrid access]#' contains 'Error'  |
        ...
    [Arguments]    ${RAW_Mode}=No
    ${OUTPUT}=    CLI:Write    add    ${RAW_Mode}
    [Return]    ${OUTPUT}

CLI:Edit
    [Documentation]
		...    Executes an edit command for the passed arguments. If no arguments are passed then just the edit
        ...    command will be executed.
        ...
        ...    The Keyword returns the Output which was returned and strips it of any "return" charaters
        ...    to improve parsebility of the output
        ...
        ...    The returned output is checked to ensure that no Error has occured. The Argument ${RAW_Mode} accepts the
        ...    value 'RAW' In which case the output
        ...    will be returned without the error checking
        ...
        ...    The result is returned as a string object
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | ${Console} | CLI:Edit | ttyS1 |
        ...    | Output      |   | |
        ...    |  |  [admin@nodegrid devices]#  | |
        ...    |  |  [admin@nodegrid {devices}]#  | |
        ...    |  |    | |
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | ${Console} | CLI:Edit | ${RAW_Mode}=Raw |  |
        ...    | Output      |   | |
        ...    |  |  [admin@nodegrid devices]#  | |
        ...    |  |  Error: Not enough arguments  | | Test will Pass |
    [Arguments]    ${ARGUMENTS}=${EMPTY}    ${RAW_Mode}=No
    ${OUTPUT}=    CLI:Write    edit ${ARGUMENTS}    ${RAW_Mode}
    [Return]    ${OUTPUT}

CLI:Cancel
    [Documentation]
		...    Executes a cancel command
        ...
        ...    The Keyword returns the Output which was returned and strips it of any "return" charaters
        ...    to improve parsebility of the output
        ...
        ...    The returned output is checked to ensure that no Error has occured. The Argument ${RAW_Mode} accepts the
        ...    value 'RAW' In which case the output
        ...    will be returned without the error checking
        ...
        ...    The result is returned as a string object
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | CLI:Add |  |  |
        ...    | ${Console}= | CLI:Cancel |  |
        ...    | Output      |   | |
        ...    |  |  [admin@nodegrid devices]#  | |
        ...    |  |  [admin@nodegrid {devices}]#  | |
        ...    |  |  [admin@nodegrid devices]#  | |
        ...    |  |    |
        ...    | CLI:Enter Path | /access |  |
        ...    | ${Console}=             | CLI:Cancel    |  |
        ...    | Output                            | Error: Invalid command: add | |  Test Failed |
        ...    |  |  [admin@nodegrid access]#' contains 'Error'  |
        ...    |  |    |
        ...    | CLI:Enter Path | /access |  |
        ...    | ${Console}=             | CLI:Cancel    | Raw |
        ...    | Output                            | Error: Invalid command: add | |  Test will Pass |
        ...    |  |  [admin@nodegrid access]#' contains 'Error'  |
    [Arguments]    ${RAW_Mode}=No
    ${OUTPUT}=    CLI:Write    cancel    ${RAW_Mode}
    [Return]    ${OUTPUT}

CLI:Revert
    [Documentation]
		...    Execute a revert command
        ...
        ...    The Keyword returns the Output which was returned and strips it of any "return" charaters
        ...    to improve parsebility of the output
        ...
        ...    The returned output is checked to ensure that no Error has occured. The Argument ${RAW_Mode} accepts the
        ...    value 'RAW' In which case the output
        ...    will be returned without the error checking
        ...
        ...    The result is returned as a string object
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path | /settings/devices |
        ...    | ${Console}= | CLI:Revert |
        ...    | Output      |   |
        ...    |  |  [admin@nodegrid devices]#  |
        ...    |  |  |
    [Arguments]    ${RAW_Mode}=No
    ${OUTPUT}=    CLI:Write    revert    ${RAW_Mode}
    [Return]    ${OUTPUT}

CLI:Commit
    [Documentation]
		...    Execute a commit command
        ...
        ...    The Keyword returns the Output which was returned and strips it of any "return" charaters
        ...    to improve parsebility of the output
        ...
        ...    The returned output is checked to ensure that no Error has occured. The Argument ${RAW_Mode} accepts the
        ...    value 'RAW' In which case the output
        ...    will be returned without the error checking
        ...
        ...    The result is returned as a string object
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path | /settings/devices |
        ...    | CLI:Edit | ttyS1 |
        ...    | CLI:Write | set icon=dell.png |
        ...    | ${Console}= | CLI:Commit |
        ...    | Output      |   |
        ...    |  |  [admin@nodegrid devices]#  |
        ...    |  |
    [Arguments]    ${RAW_Mode}=No
    ${OUTPUT}=    CLI:Write    commit    ${RAW_Mode}
    [Return]    ${OUTPUT}

CLI:Ls
    [Documentation]
		...    Execute a ls command at current location or at provided path without changing the current location
        ...
        ...    The Keyword returns the Output.
        ...
        ...    The returned output is checked to ensure that no Error has occured. The Argument ${RAW_Mode} accepts the
        ...    value 'RAW' In which case the output
        ...    will be returned without the error checking
        ...
        ...    The result is returned as a string object
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | ${Console}= | CLI:ls | |
        ...    | Output      |   | |
        ...    |  |  ttyS1/  | |
        ...    |  |  ttyS2/  | |
        ...    |  |  ....  | |
        ...    |  |  ttyS95/  | |
        ...    |  |  ttyS96/  | |
        ...    |  |  Try show command instead...  | |
        ...    |  |  [admin@nodegrid devices]#  |
        ...    |  |  |
        ...    | CLI:Write |  pwd  |
        ...    | Output |    |
        ...    |  |  /\  |
        ...    | ${Console}= | CLI:ls | /settings/devices |
        ...    | Output      |   | |
        ...    |  |  ttyS1/  | |
        ...    |  |  ttyS2/  | |
        ...    |  |  ....  | |
        ...    |  |  ttyS95/  | |
        ...    |  |  ttyS96/  | |
        ...    |  |  Try show command instead...  | |
        ...    |  |  [admin@nodegrid devices]#  |
        ...    | CLI:Write |  pwd  |
        ...    | Output |    |
        ...    |  |  /\  |
    [Arguments]    ${PATH}=${EMPTY}    ${RAW_Mode}=No	${user}=No
    ${OUTPUT}=    CLI:Write    ls ${PATH}    ${RAW_Mode}	user=${user}
    [Return]    ${OUTPUT}

CLI:Show
    [Documentation]
		...    Execute a show command
        ...    Execute a show command at current location or at provided path without changing the current location
        ...
        ...    The Keyword returns the Output.
        ...
        ...    The returned output is checked to ensure that no Error has occured. The Argument ${RAW_Mode} accepts the
        ...    value 'RAW' In which case the output
        ...    will be returned without the error checking
        ...
        ...    The result is returned as a string object
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | ${Console}= | CLI:Show | |
        ...    | Output      |   | |
        ...    |  |  name  | connected through | type | access | monitoring |
        ...    |  |  =====================  | ================= | ============== | ========= | ============= |
        ...    |  |  ttyS1  | ttyS1 | local_serial | on-demand | not supported |
        ...    |  |  ttyS2  | ttyS2 | local_serial | on-demand | not supported |
        ...    |  |  ..... |
        ...    |  |  ttyS96  | ttyS96 | local_serial | on-demand | not supported |
        ...    |  |  Try show command instead...  | |
        ...    |  |  [admin@nodegrid devices]#  |
        ...    |  |  |
        ...    | CLI:Write |  pwd  |
        ...    | Output |    |
        ...    |  |  /\  |
        ...    | ${Console}= | CLI:ls | /settings/devices |
        ...    | Output      |   | |
        ...    |  |  name  | connected through | type | access | monitoring |
        ...    |  |  =====================  | ================= | ============== | ========= | ============= |
        ...    |  |  ttyS1 | ttyS1 | local_serial | on-demand | not supported |
        ...    |  |  ttyS2  | ttyS2 | local_serial | on-demand | not supported |
        ...    |  |  ..... |
        ...    |  |  ttyS96  | ttyS96 | local_serial | on-demand | not supported |
        ...    |  |  Try show command instead...  | |
        ...    |  |  [admin@nodegrid devices]#  |
        ...    | CLI:Write |  pwd  |
        ...    | Output |    |
        ...    |  |  /\  |
    [Arguments]    ${PATH}=${EMPTY}    ${RAW_Mode}=No	${user}=No
    ${OUTPUT}=    CLI:Write    show ${PATH}    ${RAW_Mode}	${user}
    [Return]    ${OUTPUT}

CLI:Save
    [Documentation]
		...    Executes an save command
        ...
        ...    The Keyword returns the Output which was returned and strips it of any "return" charaters
        ...    to improve parsebility of the output
        ...
        ...    The returned output is checked to ensure that no Error has occured. The Argument ${RAW_Mode} accepts the
        ...    value 'RAW' In which case the output
        ...    will be returned without the error checking
        ...
        ...    The result is returned as a string object
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | ${Console}= | CLI:Save |  |
        ...    | Output      |   | |
        ...
    [Arguments]    ${RAW_Mode}=No
    ${OUTPUT}=    CLI:Write    save    ${RAW_Mode}
    [Return]    ${OUTPUT}

CLI:Show Settings
	[Documentation]
		...	Writes show_settings and wait some time to appears every setting
	[Arguments]	${PLAIN_PASSWORD}=${FALSE}
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nshow_settings	INFO	console=yes
	IF	${PLAIN_PASSWORD}
		Write	show_settings --plain-password
	ELSE
		Write	show_settings
	END
	Set Client Configuration	timeout=5s
	${OUTPUT}=	Wait Until Keyword Succeeds	10x	5s	CLI:Read Until Prompt
	Set Client Configuration	timeout=30s
	[Return]	${OUTPUT}

CLI:Test Search Command
    [Documentation]
		...    This test is for search anyhing
        ...    ${OBJECT} is any thing that you are search, considering that exist a field to do a search for him
        ...
    [Arguments]    @{VALUES}
    FOR    ${VALUE}    IN    @{VALUES}
		${OUTPUT}=    CLI:Write    search ${VALUE}
		Should Not Contain  ${OUTPUT}    search: ${VALUE}\nresults: 0 result\npage: 1 of 0
	END
    [Return]    ${OUTPUT}

CLI:Test Show Command
    [Documentation]
		...    Tests that output of show command in the current location to ensure that it includes *all* the @{FIELDS}
        ...
        ...    Keyword returns the Output of show command. Test will pass when all arguments where found in the output.
        ...
        ...    The returned output is checked to ensure that no Error has occured.
        ...
        ...    The result is returned as a string object
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | ${Console}= | CLI:Test Show Command | name | connected through | type |
        ...    | Output      |   | |
        ...    |  |  name  | connected through | type | access | monitoring |
        ...    |  |  =====================  | ================= | ============== | ========= | ============= |
        ...    |  |  ttyS1  | ttyS1 | local_serial | on-demand | not supported |
        ...    |  |  ttyS2  | ttyS2 | local_serial | on-demand | not supported |
        ...    |  |  ..... |
        ...    |  |  ttyS96  | ttyS96 | local_serial | on-demand | not supported |
        ...    |  |  Try show command instead...  | |
        ...    |  |  [admin@nodegrid devices]#  |  |  |  |  | Pass |
        ...    |  |  |
        ...    |  @{Fields}= | Create List | name | connected through | type |  |
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | ${Console}= | CLI:Test Show Command | @{Fields} |
        ...    | Output      |   | |
        ...    |  |  name  | connected through | type | access | monitoring |
        ...    |  |  =====================  | ================= | ============== | ========= | ============= |
        ...    |  |  ttyS1  | ttyS1 | local_serial | on-demand | not supported |
        ...    |  |  ttyS2  | ttyS2 | local_serial | on-demand | not supported |
        ...    |  |  ..... |
        ...    |  |  ttyS96  | ttyS96 | local_serial | on-demand | not supported |
        ...    |  |  Try show command instead...  | |
        ...    |  |  [admin@nodegrid devices]#  |  |  |  |  | Pass |
        ...    |  |
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | ${Console}= | CLI:Test Show Command | name | connected through | type | NOT-IN-THE-LIST |
        ...    | Output      |   | |
        ...    |  |  name  | connected through | type | access | monitoring |
        ...    |  |  =====================  | ================= | ============== | ========= | ============= |
        ...    |  |  ttyS1  | ttyS1 | local_serial | on-demand | not supported |
        ...    |  |  ttyS2  | ttyS2 | local_serial | on-demand | not supported |
        ...    |  |  ..... |
        ...    |  |  ttyS96  | ttyS96 | local_serial | on-demand | not supported |
        ...    |  |  Try show command instead...  | |
        ...    |  |  [admin@nodegrid devices]#  | does not contain 'NOT-IN-THE-LIST' |  |  |  | Fail |
    [Arguments]    @{FIELDS}
    ${OUTPUT}=    CLI:Show
    FOR    ${FIELD}    IN    @{FIELDS}
		Should Contain    ${OUTPUT}     ${FIELD}
	END
    [Return]    ${OUTPUT}

CLI:Test Show Command Any
    [Documentation]
		...    Tests that output of show command in the current location to ensure that it includes *any* the @{FIELDS}.
        ...    The command ingnores the first 2 lines (header) of the output for the test.
        ...
        ...    Keyword returns the Output of show command. Test will pass when all arguments where found in the output.
        ...
        ...    The returned output is checked to ensure that no Error has occured.
        ...
        ...    The result is returned as a string object
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | ${Console}= | CLI:Test Show Command | name | connected through | type | NOT-IN-THE-LIST |
        ...    | Output      |   | |
        ...    |  |  name  | connected through | type | access | monitoring |
        ...    |  |  =====================  | ================= | ============== | ========= | ============= |
        ...    |  |  ttyS1  | ttyS1 | local_serial | on-demand | not supported |
        ...    |  |  ttyS2  | ttyS2 | local_serial | on-demand | not supported |
        ...    |  |  ..... |
        ...    |  |  ttyS96  | ttyS96 | local_serial | on-demand | not supported |
        ...    |  |  Try show command instead...  | |
        ...    |  |  [admin@nodegrid devices]#  | does not contain 'NOT-IN-THE-LIST' |  |  |  | Fail |
        ...    |  |
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | ${Console}= | CLI:Test Show Command Any| name | connected through | type | NOT-IN-THE-LIST |
        ...    | Output      |   | |
        ...    |  |  name  | connected through | type | access | monitoring |
        ...    |  |  =====================  | ================= | ============== | ========= | ============= |
        ...    |  |  ttyS1  | ttyS1 | local_serial | on-demand | not supported |
        ...    |  |  ttyS2  | ttyS2 | local_serial | on-demand | not supported |
        ...    |  |  ..... |
        ...    |  |  ttyS96  | ttyS96 | local_serial | on-demand | not supported |
        ...    |  |  Try show command instead...  | |
        ...    |  |  [admin@nodegrid devices]#  |  |  |  |  | Pass |
        ...    |  |
        ...    |  @{Fields}= | Create List | name | connected through | type | NOT-IN-THE-LIST |
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | ${Console}= | CLI:Test Show Command Any| @{Fields} |
        ...    | Output      |   | |
        ...    |  |  name  | connected through | type | access | monitoring |
        ...    |  |  =====================  | ================= | ============== | ========= | ============= |
        ...    |  |  ttyS1  | ttyS1 | local_serial | on-demand | not supported |
        ...    |  |  ttyS2  | ttyS2 | local_serial | on-demand | not supported |
        ...    |  |  ..... |
        ...    |  |  ttyS96  | ttyS96 | local_serial | on-demand | not supported |
        ...    |  |  Try show command instead...  | |
        ...    |  |  [admin@nodegrid devices]#  |  |  |  |  | Pass |
    [Arguments]    @{FIELDS}
    ${OUTPUT}=    CLI:Show
    ${OUTPUT}=    Remove String Using Regexp    ${OUTPUT}    ['\r']
#    Log    ${OUTPUT}
    @{LINES}=    Split To Lines    ${OUTPUT}    2    1
    FOR    ${LINE}    IN    @{LINES}
		Should Contain Any	${LINE}	@{FIELDS}
	END
    [Return]    ${OUTPUT}

CLI:Test Access Show Command
    [Documentation]
		...    NEEDS TO BE REVIEWED - Only works for ilo devices
    [Arguments]    ${TYPE}
    ${OUTPUT}=    CLI:Show
    @{FIELDS}    Create List
    Run Keyword If    '${TYPE}' == 'ilo'    Append To List    ${FIELDS}    name    type    ip_address    address_location    coordinates    web_url    launch_url_via_html5    username    credential    password    enable_hostname_detection    multisession    read-write_multisession    enable_send_break    icon    mode    expiration    skip_authentication_to_access_device    escape_sequence	power_control_key    show_text_information    enable_ip_alias	enable_second_ip_alias    allow_ssh_protocol    ssh_port    allow_telnet_protocol    allow_binary_socket
    FOR    ${FIELD}    IN    @{FIELDS}
		Should Contain    ${OUTPUT}    ${FIELD}
    END
    [Return]    ${OUTPUT}

CLI:Test Not Show Command
    [Documentation]
		...    Tests that output of show command in current location to ensure that it includes *none* the @{FIELDS}
        ...
        ...    Keyword returns the Output of show command. Test will pass when all arguments where not found in output.
        ...
        ...    The result is returned as a string object
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | ${Console}= | CLI:Test Not Show Command | name | connected through | type |
        ...    | Output      |   | |
        ...    |  |  name  | connected through | type | access | monitoring |
        ...    |  |  =====================  | ================= | ============== | ========= | ============= |
        ...    |  |  ttyS1  | ttyS1 | local_serial | on-demand | not supported |
        ...    |  |  ttyS2  | ttyS2 | local_serial | on-demand | not supported |
        ...    |  |  ..... |
        ...    |  |  ttyS96  | ttyS96 | local_serial | on-demand | not supported |
        ...    |  |  Try show command instead...  | |
        ...    |  |  [admin@nodegrid devices]#  | contains 'name' |  |  |  | Fail |
        ...    |  |  |
        ...    |  @{Fields}= | Create List | NOT-IN-THE-LIST |  |  |  |
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | ${Console}= | CLI:Test Not Show Command  | @{Fields} |
        ...    | Output      |   | |
        ...    |  |  name  | connected through | type | access | monitoring |
        ...    |  |  =====================  | ================= | ============== | ========= | ============= |
        ...    |  |  ttyS1  | ttyS1 | local_serial | on-demand | not supported |
        ...    |  |  ttyS2  | ttyS2 | local_serial | on-demand | not supported |
        ...    |  |  ..... |
        ...    |  |  ttyS96  | ttyS96 | local_serial | on-demand | not supported |
        ...    |  |  Try show command instead...  | |
        ...    |  |  [admin@nodegrid devices]#  |  |  |  |  | Pass |
        ...    |  |
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | ${Console}= | CLI:Test Show Command | NOT-IN-THE-LIST |
        ...    | Output      |   | |
        ...    |  |  name  | connected through | type | access | monitoring |
        ...    |  |  =====================  | ================= | ============== | ========= | ============= |
        ...    |  |  ttyS1  | ttyS1 | local_serial | on-demand | not supported |
        ...    |  |  ttyS2  | ttyS2 | local_serial | on-demand | not supported |
        ...    |  |  ..... |
        ...    |  |  ttyS96  | ttyS96 | local_serial | on-demand | not supported |
        ...    |  |  Try show command instead...  | |
        ...    |  |  [admin@nodegrid devices]#  |  |  |  |  | Pass |
    [Arguments]    	@{FIELDS}
    ${OUTPUT}=    CLI:Show
    FOR    ${FIELD}    IN    @{FIELDS}
		Should Not Contain    ${OUTPUT}    ${FIELD}
    END
    [Return]    ${OUTPUT}

CLI:Test Show Command Regexp
    [Documentation]
		...    NEEDS TO BE REVIEWED - 2 keywords with the same feature implemented
        ...
        ...    Tests that output of the show command in the current location to ensure that it matches *any* of the
        ...    Regular Expressions passed as the @{FIELDS}
        ...    The command ingnores the first 2 lines (header) of the output for the test.
        ...
        ...    Keyword returns the Output of show command. Test will pass when all arguments where found in the output.
        ...
        ...    The returned output is checked to ensure that no Error has occured.
        ...
        ...    The result is returned as a string object
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | ${Console}= | CLI:Test Show Command Regexp| [A-Za-z]{1-4} | d{1-8} |
        ...    | Output      |   | |
        ...    |  |  name  | connected through | type | access | monitoring |
        ...    |  |  =====================  | ================= | ============== | ========= | ============= |
        ...    |  |  ttyS1  | ttyS1 | local_serial | on-demand | not supported |
        ...    |  |  ttyS2  | ttyS2 | local_serial | on-demand | not supported |
        ...    |  |  ..... |
        ...    |  |  ttyS96  | ttyS96 | local_serial | on-demand | not supported |
        ...    |  |  Try show command instead...  | |
        ...    |  |  [admin@nodegrid devices]#  |  |  |  |  | Pass |
    [Arguments]    @{REGEXPS}
    ${OUTPUT}=    CLI:Show
    FOR    ${REGEXP}    IN    @{REGEXPS}
		Should Match Regexp    ${OUTPUT}    ${REGEXP}
	END

CLI:Test Not Show Command Regexp
    [Documentation]
		...    NEEDS TO BE REVIEWED - 2 keywords with the same feature implemented
        ...
        ...    Tests that output of the show command in the current location to ensure that it matches *any* of the
        ...    Regular Expressions passed as the @{FIELDS}
        ...    The command ingnores the first 2 lines (header) of the output for the test.
        ...
        ...    Keyword returns the Output of show command. Test will pass when all arguments where found in the output.
        ...
        ...    The returned output is checked to ensure that no Error has occured.
        ...
        ...    The result is returned as a string object
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | ${Console}= | CLI:Test Show Command Regexp| [A-Za-z]{1-4} | d{1-8} |
        ...    | Output      |   | |
        ...    |  |  name  | connected through | type | access | monitoring |
        ...    |  |  =====================  | ================= | ============== | ========= | ============= |
        ...    |  |  ttyS1  | ttyS1 | local_serial | on-demand | not supported |
        ...    |  |  ttyS2  | ttyS2 | local_serial | on-demand | not supported |
        ...    |  |  ..... |
        ...    |  |  ttyS96  | ttyS96 | local_serial | on-demand | not supported |
        ...    |  |  Try show command instead...  | |
        ...    |  |  [admin@nodegrid devices]#  |  |  |  |  | Pass |
    [Arguments]    @{REGEXPS}
    ${OUTPUT}=    CLI:Show
    FOR    ${REGEXP}    IN    @{REGEXPS}
		Should Not Match Regexp    ${OUTPUT}    ${REGEXP}
	END

CLI:Test Ls Command
    [Documentation]
		...    Tests if the output of thge ls command contains *all* of the provided @{FIELDS}
        ...
        ...    For Version 4 tests it will remove the string 'Try show command instead...'
        ...
        ...    Keyword returns the Output of ls command. Test will pass when all arguments where found in the output.
        ...
        ...    The returned output is checked to ensure that no Error has occured.
        ...
        ...    The result is returned as a string object
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | ${Console}= | CLI:Test Ls Command| ttyS1 | ttyS2 | ttyS96 |
        ...    | Output      |   | |
        ...    |  |  ttyS1/  | |
        ...    |  |  ttyS2/  | |
        ...    |  |  ....  | |
        ...    |  |  ttyS95/  | |
        ...    |  |  ttyS96/  | |
        ...    |  |  [admin@nodegrid devices]#  |  |  |  |  | Pass |
        ...    | |
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | ${Console}= | CLI:Test Ls Command| ttyS1 | ttyS2 | ttyS97 |
        ...    | Output      |   | |
        ...    |  |  ttyS1/  | |
        ...    |  |  ttyS2/  | |
        ...    |  |  ....  | |
        ...    |  |  ttyS95/  | |
        ...    |  |  ttyS96/  | |
        ...    |  |  [admin@nodegrid devices]#  | does not contain 'ttyS97' |  |  |  | Fail |
	[Arguments]	@{FIELDS}
	Run Keyword If	${NGVERSION} > 3.2	Remove Values From List	${FIELDS}	Try show command instead...
	${OUTPUT}=	 CLI:Ls
	FOR	${FIELD}	IN	@{FIELDS}
		Should Contain	${OUTPUT}	${FIELD}
	END
	[Return]	${OUTPUT}

CLI:Start Configuration transaction
    ${OUTPUT}=  CLI:Write  config_start
    Should Not Contain   ${OUTPUT}   Error: Another configuration transaction is underway.

CLI:Revert Configuration transaction
    ${OUTPUT}=  CLI:Write  config_revert
    Should Not Contain   ${OUTPUT}   Error: There is no active configuration transaction.

CLI:Confirm Configuration transaction
    ${OUTPUT}=  CLI:Write   config_confirm
    Should Not Contain   ${OUTPUT}  Error: There is no active configuration transaction.

CLI:Test Ls Command Any
    [Documentation]
		...    Tests if the output of thge ls command contains *any* of the provided @{FIELDS}
        ...
        ...    For Version 4 tests it will remove the string 'Try show command instead...'
        ...
        ...    Keyword returns the Output of ls command. Test will pass when any arguments where found in the output.
        ...    The first 2 lines will be ignored for the comparision
        ...
        ...    The returned output is checked to ensure that no Error has occured.
        ...
        ...    The result is returned as a string object
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | ${Console}= | CLI:Test Ls Command| ttyS1 | ttyS2 | ttyS96 |
        ...    | Output      |   | |
        ...    |  |  ttyS1/  | |
        ...    |  |  ttyS2/  | |
        ...    |  |  ....  | |
        ...    |  |  ttyS95/  | |
        ...    |  |  ttyS96/  | |
        ...    |  |  [admin@nodegrid devices]#  |  |  |  |  | Pass |
        ...    | |
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | ${Console}= | CLI:Test Ls Command| ttyS1 | ttyS2 | ttyS97 |
        ...    | Output      |   | |
        ...    |  |  ttyS1/  | |
        ...    |  |  ttyS2/  | |
        ...    |  |  ....  | |
        ...    |  |  ttyS95/  | |
        ...    |  |  ttyS96/  | |
        ...    |  |  [admin@nodegrid devices]#  | does not contain 'ttyS97' |  |  |  | Fail |
    [Arguments]    @{FIELDS}
    ${OUTPUT}=    CLI:Ls
    ${OUTPUT}=    Remove String Using Regexp	${OUTPUT}	['\r']
    Log    ${OUTPUT}
    @{LINES}=    Split To Lines    ${OUTPUT}    2    1
    FOR    ${LINE}    IN    @{LINES}
		Should Contain Any    ${LINE}    @{FIELDS}
	END

CLI:Test Available Commands
    [Documentation]
		...    Tests if the available commands which can be identified through a <TAB><TAB> at the current location
        ...    contain *all* of the commands provided
        ...
        ...    Should the command show_settings be provided in the list, will it be removed from the list in version 4.0
        ...
        ...    The Keyword returns no output. The command is checked for errors and will fail if errors are displayed.
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | CLI:Test Available Commands | add | bounce_dtr | cd |  |
        ...    | Output      |   | |
        ...    |  |  [admin@nodegrid devices]#  |  |  |  |  | Pass |
        ...    | |
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | CLI:Test Available Commands | add | bounce_dtr | cd | ttyS96 |
        ...    | Output      |   | |
        ...    |  |  [admin@nodegrid devices]#  | does not contain 'ttyS96' |  |  |  | Fail |
    [Arguments]    @{COMMANDS}
    Write Bare    \n\n
    CLI:Read Until Prompt
    CLI:Read Until Prompt
    Write Bare    \t\t
    ${OUTPUT}=    CLI:Read Until Prompt
#    Run Keyword If    '${NGVERSION}' >= '4.0'    Remove Values From List    ${COMMANDS}    show_settings
    FOR    ${COMMAND}    IN    @{COMMANDS}
		Should Contain    ${OUTPUT}    ${COMMAND}
	END

CLI:Test Not Available Commands
    [Documentation]
		...    Tests if the available commands which can be identified through a <TAB><TAB> at the current location,
        ...    do not contain *any* of provided commands
        ...
        ...    The keywork show_settings will be added to the list in version 4.0 and above.
        ...
        ...    The Keyword returns no output. The command is checked for errors and will fail if errors are displayed.
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | CLI:Test Not Available Commands | add | bounce_dtr | cd |  |
        ...    | Output      |   | |
        ...    |  |  [admin@nodegrid devices]#  | does contain 'add' |  |  |  | Fail |
        ...    | |
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | CLI:Test Available Commands | ttyS96 |  |  |  |
        ...    | Output      |   | |
        ...    |  |  [admin@nodegrid devices]#  |  |  |  |  | Pass |
    [Arguments]    @{COMMANDS}
    Write Bare    \n\n
    CLI:Read Until Prompt
    CLI:Read Until Prompt
    Write Bare    \t\t
    ${OUTPUT}=    CLI:Read Until Prompt
#    Run Keyword If    ${NGVERSION} >= 4.0    Append To List    ${COMMANDS}	show_settings
    FOR    ${COMMAND}    IN    @{COMMANDS}
		Should Not Contain    ${OUTPUT}    ${COMMAND}
	END

CLI:Test Unavailable Commands
    [Documentation]
		...    Needs to be reviewed duplication with CLI:Test Not Available Commands
    [Arguments]    @{COMMANDS}
    Write Bare    \n\n
    CLI:Read Until Prompt
    CLI:Read Until Prompt
    Write Bare    \t\t
    ${OUTPUT}=    CLI:Read Until Prompt
    Run Keyword If    '${NGVERSION}' >= '4.0'    Remove Values From List    ${COMMANDS}    show_settings
    FOR    ${COMMAND}    IN    @{COMMANDS}
		Should Not Contain    ${OUTPUT}    ${COMMAND}
	END

CLI:Test Set Available Field
    [Documentation]
		...    Needs to be reviewed duplication with CLI:Test Set Available Fields
    [Arguments]    ${FIELD}
    Write Bare    set ${FIELD}=\n
    ${OUTPUT}=    CLI:Read Until Prompt    Raw
    Should Not Contain    ${OUTPUT}    Invalid parameter name: ${FIELD}

CLI:TabTab Fill Set
	[Arguments]	${FIELD}	${DEFAULT_OPTION}	@{REGEX_DEFAULT_OPTIONS}
	${INVALID_VALUE}=	Set Variable	\\_\\)\\(\\*&¨%

	#Test Invalid entries
	Write Bare	set ${FIELD}=${INVALID_VALUE}\n
	${OUTPUT}=	Read Until Prompt
	${OUTPUT}=	Remove String Using Regexp	${OUTPUT}	['\r']
	Log to Console	\n${OUTPUT}
	@{LINES}=	Split to Lines	${OUTPUT}
	Run Keyword If	'${NGVERSION}' >= '4.2'	Should Match Regexp	${LINES}[2]	Error: Invalid value: ${INVALID_VALUE} for parameter: ${FIELD}
	Run Keyword If	'${NGVERSION}' == "3.2"	Should Match Regexp	${LINES}[2]	Error: Invalid value: ${INVALID_VALUE}

	#Test autofill options
	Write Bare	set ${FIELD}=\t\t
	${OUTPUT}=	Read Until Prompt
	${OUTPUT}=	Remove String Using Regexp	${OUTPUT}	['\r']
	Log to Console	\n${OUTPUT}
	@{LINES}=	Split to Lines	${OUTPUT}
	FOR	${OPTION}	IN	@{REGEX_DEFAULT_OPTIONS}
		Should Match Regexp	${LINES}[1]	(.)*(${OPTION})(.)*
	END
	#Fill with one of the options
	Write Bare	${DEFAULT_OPTION}\n
	${OUTPUT}=	Read Until Prompt
	${OUTPUT}=	Remove String Using Regexp	${OUTPUT}	['\r']
	Log to Console	\n${OUTPUT}

CLI:Test Set Available Fields
    [Documentation]
		...    Tests if the available options for the set command which can be identified through set <TAB><TAB> at the
        ...    current location, contains the provided options
        ...
        ...    The Keyword returns no output. The command is checked for errors and will fail if errors are displayed.
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | CLI:Test Set Available Fields | ttyS1 | ttyS12 | ttyS96 |  |
        ...    | Output      |   | |
        ...    |  |  [admin@nodegrid devices]#  |  |  |  |  | Pass |
        ...    | |
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | CLI:Test Set Available Fields | ttyS1 | ttyS12 | ttyS97 |  |
        ...    | Output      |   | |
        ...    |  |  [admin@nodegrid devices]#  | does not contain 'ttyS97' |  |  |  | Fail |
    [Arguments]    @{FIELDS}
    FOR    ${FIELD}    IN    @{FIELDS}
		Write Bare    set ${FIELD}=\n
		${OUTPUT}=    CLI:Read Until Prompt    Raw
		Should Not Contain    ${OUTPUT}    Invalid parameter name: ${FIELD}
	END

CLI:Test Set Available Access Fields
    [Documentation]
		...    Needs to be checked as keyword is only valid for ilo's
        ...
    [Arguments]    ${TYPE}
    Write Bare    set \t\t\n
    ${OUTPUT}=    CLI:Read Until Prompt
    CLI:Read Until Prompt
    @{FIELDS}    Create List
    Run Keyword If    '${TYPE}' == 'ilo'    Append To List    ${FIELDS}    name    type    ip_address    address_location    coordinates    web_url    launch_url_via_html5    username    credential    password    enable_hostname_detection    multisession    read-write_multisession    enable_send_break    icon    mode    expiration    skip_authentication_to_access_device    escape_sequence    power_control_key    show_text_information    enable_ip_alias    enable_second_ip_alias    allow_ssh_protocol    ssh_port    allow_telnet_protocol    allow_binary_socket
    FOR    ${FIELD}    IN    @{FIELDS}
		Should Contain    ${OUTPUT}    ${FIELD}
	END

CLI:Test Set Available Equal Fields
    [Documentation]
		...    Needs to be reviewed duplication with CLI:Test Set Available Fields
        ...
    [Arguments]    @{FIELDS}
    Write Bare    set \t\t\t
    ${OUTPUT}=    CLI:Read Until Prompt
    Write Bare    \n
    CLI:Read Until Prompt    Raw
    FOR    ${FIELD}    IN    @{FIELDS}
		Should Contain    ${OUTPUT}    ${FIELD}
	END

CLI:Test Set Unavailable Fields
    [Documentation]
		...    Tests if the available options for the set command which can be identified through set <TAB><TAB> at the
        ...    current location, do not contain any of the provided options
        ...
        ...    The Keyword returns no output. The command is checked for errors and will fail if errors are displayed.
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | CLI:Test Set Unavailable Fields | ttyS1 | ttyS12 | ttyS96 |  |
        ...    | Output      |   | |
        ...    |  |  [admin@nodegrid devices]#  | does contain 'ttyS1' |  |  |  | Fail |
        ...    | |
        ...    | CLI:Enter Path | /settings/devices |  |
        ...    | CLI:Test Set Unavailable Fields | cancel |  |  |  |
        ...    | Output      |   | |
        ...    |  |  [admin@nodegrid devices]#  |  |  |  |  | Pass |
    [Arguments]    @{FIELDS}
    FOR    ${FIELD}    IN    @{FIELDS}
		Write Bare    set ${FIELD}=\n
		${OUTPUT}=    CLI:Read Until Prompt    Raw
		Run Keyword If  '${NGVERSION}' >= '4.0'    Should Contain    ${OUTPUT}    Invalid parameter name: ${FIELD}
		Run Keyword If  '${NGVERSION}' < '4.0'     Should Contain    ${OUTPUT}    Invalid parameter: ${FIELD}
	END

CLI:Test Field Options
    [Documentation]
		...    Tests if the available options for the set= command which can be identified through set= <TAB><TAB> at
        ...    the current location, contains all of the provided options
        ...
        ...    The Keyword returns no output.
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path | /settings/date_and_time/ |  |
        ...    | CLI:Test Set Field Options | zone | gmt | us|eastern | us|pacific |
        ...    | Output      |   | |
        ...    |  |  [admin@nodegrid devices]#  |  |  |  |  | Pass |
        ...    | |
        ...    | CLI:Enter Path | /settings/date_and_time/ |  |
        ...    | CLI:Test Set Field Options | zone | gmt | us|eastern | emea|dublin |
        ...    | Output      |   | |
        ...    |  |  [admin@nodegrid devices]#  | does not contain 'emea|dublin' |  |  |  | Fail |
    [Arguments]    ${FIELD}    @{OPTIONS}
    Log    Options:@{OPTIONS}
    Write Bare    set ${FIELD}=\t\t\t\n
    ${RESULT}=    CLI:Read Until Prompt    Raw
    Log     Confirm if Options are part of the available output
    FOR    ${OPTION}    IN    @{OPTIONS}            #Confirm if Options are part of the available output
		Should Contain    ${RESULT}    ${OPTION}
		Log    Option: ${OPTION}\n    Info    console=yes
	END
    Log    Clean up prompt after tab-tab
    Read Until    Missing
    Wait Until Keyword Succeeds    5s    1s    CLI:Read Until Prompt

CLI:Test Set Field Options
    [Documentation]
		...    Tests if the available options for the set= command which can be identified through set= <TAB><TAB> at
        ...    the current location, contains all of the provided options
        ...
        ...    The Keyword returns no output. The command is checked for errors and will fail if errors are displayed.
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path | /settings/date_and_time/ |  |
        ...    | CLI:Test Set Field Options | zone | gmt | us|eastern | us|pacific |
        ...    | Output      |   | |
        ...    |  |  [admin@nodegrid devices]#  |  |  |  |  | Pass |
        ...    | |
        ...    | CLI:Enter Path | /settings/date_and_time/ |  |
        ...    | CLI:Test Set Field Options | zone | gmt | us|eastern | emea|dublin |
        ...    | Output      |   | |
        ...    |  |  [admin@nodegrid devices]#  | does not contain 'emea|dublin' |  |  |  | Fail |
    [Arguments]    ${FIELD}    @{OPTIONS}
    Log    Options:@{OPTIONS}
    Write Bare    set ${FIELD}=\t\t\t\n
    ${RESULT}=    CLI:Read Until Prompt       Raw

    ${FIELD_EXIST}=	Run Keyword And Return Status
    ...	Run Keyword If	${NGVERSION} == 3.2
    ...		Should Not Contain	${RESULT}	Error: Invalid parameter: ${FIELD}
    ...	ELSE
    ...		Should Not Contain	${RESULT}	Error: Invalid parameter name: ${FIELD}
    Run Keyword If	not ${FIELD_EXIST}	Run Keywords
    ...	Write Bare	\n	AND
    ...	Read	delay=1s	AND
    ...	Fail	Field ${FIELD} does not exist

    Log    Confirm if Options are part of the available output
    FOR    ${OPTION}    IN    @{OPTIONS}                  #Confirm if Options are part of the available output
		${CONTAINS}=	Run Keyword And Return Status	Should Contain	${RESULT}    ${OPTION}
		Run Keyword If	not ${CONTAINS}	Run Keywords
		...	Read    delay=1s	AND
		...	Write Bare    \n	AND
		...	Wait Until Keyword Succeeds    5s    1s    CLI:Read Until Prompt	AND
		...	Fail	Field ${FIELD} does not have option ${OPTION}
		Log    Option: ${OPTION}\n    Info    console=yes
	END
    Log    Clean up prompt after tab-tab
    Read Until    Error
    Read    delay=1s
    Write Bare    \n
    Wait Until Keyword Succeeds    5s    1s    CLI:Read Until Prompt
    Log    Confirm if Options are accepted as valid values
    FOR    ${OPTION}    IN    @{OPTIONS}
		CLI:Write    set ${FIELD}=${OPTION}
		${SHOWOUTPUT}=    CLI:Write    show
		Should Contain    ${SHOWOUTPUT}    ${FIELD} = ${OPTION}
	END
    ${USECANCEL}=    Run Keyword And Return Status    CLI:Test Available Commands    cancel
    ${USEREVERT}=    Run Keyword And Return Status    CLI:Test Available Commands    revert
    Run Keyword If    not ${USECANCEL} and ${USEREVERT}    CLI:Write    revert

CLI:Test Set Field Options Raw
    [Documentation]
		...    Tests if the available options for the set= command which can be identified through set= <TAB><TAB> at
        ...    the current location, contains all of the provided options. It does not expect for error
        ...    when setting an empty field
        ...
        ...    The Keyword returns no output. The command is checked for errors and will fail if errors are displayed.
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path | /settings/date_and_time/ |  |
        ...    | CLI:Test Set Field Options | zone | gmt | us|eastern | us|pacific |
        ...    | Output      |   | |
        ...    |  |  [admin@nodegrid devices]#  |  |  |  |  | Pass |
        ...    | |
        ...    | CLI:Enter Path | /settings/date_and_time/ |  |
        ...    | CLI:Test Set Field Options | zone | gmt | us|eastern | emea|dublin |
        ...    | Output      |   | |
        ...    |  |  [admin@nodegrid devices]#  | does not contain 'emea|dublin' |  |  |  | Fail |
    [Arguments]    ${FIELD}    @{OPTIONS}
    Log    Options:@{OPTIONS}
    Write Bare    set ${FIELD}=\t\t\t\n
    ${RESULT}=    CLI:Read Until Prompt       Raw
    Log    Confirm if Options are part of the available output
    FOR    ${OPTION}    IN    @{OPTIONS}                  #Confirm if Options are part of the available output
		Should Contain	${RESULT}    ${OPTION}
		Log    Option: ${OPTION}\n    Info    console=yes
	END
    Log    Clean up prompt after tab-tab
    Read    delay=1s
    Write Bare    \n
    Wait Until Keyword Succeeds    5s    1s    CLI:Read Until Prompt
    Log    Confirm if Options are accepted as valid values
    FOR    ${OPTION}    IN    @{OPTIONS}
		CLI:Write    set ${FIELD}=${OPTION}
		${SHOWOUTPUT}=    CLI:Write    show
		Should Contain    ${SHOWOUTPUT}    ${FIELD} = ${OPTION}
	END
    ${USECANCEL}=    Run Keyword And Return Status    CLI:Test Available Commands    cancel
    Run Keyword If    ${USECANCEL} == False    CLI:Write    revert

CLI:Set
    [Documentation]
		...    Needs to be reviewed possiable duplication with CLI:Set Field
        ...
    [Arguments]    ${TEXT}
    ${OUTPUT}=    CLI:Write    set ${TEXT}
    [Return]    ${OUTPUT}

CLI:Set Field
    [Documentation]
		...    Sets field with provided value equal to set <FIELD>=<VALUE>. Keyword dose not issue a commit after set.
        ...
        ...    The Keyword returns the output. The command is checked for errors and will fail if errors are displayed.
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path | /settings/date_and_time/ |  |
        ...    | ${OUTPUT}= | CLI:Set Field | zone | gmt | | | | |
        ...    | Output      |   | |
        ...    | | set zone=gmt |
        ...    |  |  [+admin@nodegrid date_and_time]#  |  |  |  |  | Pass |
        ...    | |
        ...
    [Arguments]    ${FIELD}    ${VALUE}=${EMPTY}
    Log    set ${FIELD}=${VALUE}    INFO    console=yes
    ${OUTPUT}=    CLI:Write    set ${FIELD}=${VALUE}
    [Return]    ${OUTPUT}

CLI:Test Set Field Invalid Options
    [Documentation]
		...    Tests invalid options for the set= command at the current location and confirms the error message.
    ...
    ...    Keyword returns no output. Command is checked for errors and will fail if not correct errors are displayed.
    ...
    ...    Examples:
    ...
    ...    | CLI:Enter Path | /settings/date_and_time/ |  |
    ...    | CLI:Test Set Field Invalid Options | zone | met | Invalid value: 'met' for parameter: 'zone' |
    ...    | Output      |   | |
    ...    |  |  [admin@nodegrid devices]#  |  |  |  |  | Pass |
    ...
    [Arguments]    ${FIELD}    ${VALUE}=${EMPTY}    ${ERROR}=${EMPTY}    ${save}=no    ${revert}=no
    ${SETOUTPUT}=    CLI:Write    set ${FIELD}=${VALUE}    Raw    Yes
    Run Keyword If    '${save}' == 'no'    Should Contain  ${SETOUTPUT}    ${ERROR}
    ${SHOWOUTPUT}=    Run Keyword If    '${save}' == 'yes'    CLI:Write    save    Raw    Yes
    Run Keyword If    '${save}' == 'yes'    Should Contain    ${SHOWOUTPUT}    ${ERROR}

    ${SHOWOUTPUT}=    Run Keyword If    '${save}' == 'commit'    CLI:Write    commit    Raw    Yes
    Run Keyword If    '${save}' == 'commit'    Should Contain    ${SHOWOUTPUT}    ${ERROR}
    Run Keyword If    '${revert}' == 'commit'    CLI:Revert

CLI:Test Set Validate Invalid Options
    [Documentation]
		...    Tests invalid options for the set= command at the current location and confirms the error message.
    ...
    ...    Keyword returns no output. Command is checked for errors and will fail if not correct errors are displayed.
    ...
    ...    Examples:
    ...
    ...    | CLI:Test Set Validate Invalid Options | /settings/date_and_time/ | zone | met | Invalid value: 'met' for parameter: 'zone' |
    ...    | Output      |   | |
    ...    |  |  [admin@nodegrid devices]#  |  |  |  |  | Pass |
    ...
    [Arguments]    ${FIELD}    ${VALUE}=${EMPTY}    ${ERROR}=${EMPTY}    ${revert}=no
    ${SETOUTPUT}=    CLI:Write    set . ${FIELD}=${VALUE}    Raw    Yes
    Should Be Equal    ${SETOUTPUT}    ${ERROR}
    Run Keyword If    '${revert}' == 'yes'    CLI:Revert

CLI:Test Set Validate Invalid Options Multiple Fields
    [Documentation]
		...    Tests invalid options for the set= command at the current location and confirms the error message.
    ...
    ...    Keyword returns no output. Command is checked for errors and will fail if not correct errors are displayed.
    ...
    ...    Examples:
    ...
    ...    | CLI:Test Set Validate Invalid Options | /settings/date_and_time/ | zone | met | Invalid value: 'met' for parameter: 'zone' |
    ...    | Output      |   | |
    ...    |  |  [admin@nodegrid devices]#  |  |  |  |  | Pass |
    ...
    [Arguments]    ${TEXT}    ${ERROR}=${EMPTY}    ${revert}=no
    ${SETOUTPUT}=    CLI:Write    set . ${TEXT}    Raw    Yes
    Should Be Equal    ${SETOUTPUT}    ${ERROR}
    Run Keyword If    '${revert}' == 'yes'    CLI:Revert

CLI:Test Set Validate Field
    [Documentation]
		...
    ...    Tests valid option for set command at current location and confirms it worked using show ${FIELD} command.
    ...    Note: This keyword is used when output must be different of input
    ...
    ...    Examples:
    ...
    ...    |    CLI:Test Set Validate Field    |    server/    |    status    |    enabled    |
    ...
    ...    |    ${CONSOLE}=    |    set server/ status=enabled    |
    ...    |    ${CONSOLE}=    |    show server/ status        |
    ...    |    Output    |                |
    ...    |    |    status =    |    Enabled    |
    ...
    [Arguments]    ${FIELD}    ${VALUE}=${EMPTY}    ${VALUEOUT}=${VALUE}
    CLI:Write    set . ${FIELD}=${VALUE}    user=Yes
    ${SHOWOUTPUT}=    CLI:Show    . ${FIELD}
    Run Keyword If    '${FIELD}: ${VALUEOUT}' in '${SHOWOUTPUT}'    Should Contain    ${SHOWOUTPUT}    ${FIELD}: ${VALUEOUT}
    Run Keyword If    '${FIELD} = ${VALUEOUT}' in '${SHOWOUTPUT}'    Should Contain    ${SHOWOUTPUT}    ${FIELD} = ${VALUEOUT}

CLI:Test Set Validate Normal Fields
    [Documentation]
		...    Tests valid options for set command at current location and confirms it worked using show ${FIELD} command.
    ...
    ...    Examples:
    ...
    ...    |    CLI:Test Set Validate Normal Fields | server/ | status | enabled | disabled |
    ...
    ...    |    ${CONSOLE}=    |    set server/ status=enabled    |
    ...    |    ${CONSOLE}=    |    show server/ status        |
    ...    |    Output    |                |
    ...    |    |    status =    |    enabled    |
    ...
    ...    |    ${CONSOLE}=    |    set server/ status=disabled    |
    ...    |    ${CONSOLE}=    |    show server/ status        |
    ...    |    Output    |                |
    ...    |    |    status =    |    disabled    |
    ...
    [Arguments]    ${FIELD}    @{VALUES}
    FOR    ${VALUE}    IN    @{VALUES}
		CLI:Write    set . ${FIELD}=${VALUE}
		${SHOWOUTPUT}=    CLI:Write    show . ${FIELD}    user=Yes
		Run Keyword If    '${FIELD}: ${VALUE}' in '${SHOWOUTPUT}'    Should Contain    ${SHOWOUTPUT}    ${FIELD}: ${VALUE}
		...	ELSE	Should Contain	${SHOWOUTPUT}	${FIELD} = ${VALUE}
	END

CLI:Test Validate Unmodifiable Field
    [Documentation]
		...
    ...    Tests the value stored at the current location and confirms it by using show ${FIELD} command.
    ...    Note: This keyword is used to unmodifiables fields.
    ...
    [Arguments]    ${FIELD}    ${VALUE}=${EMPTY}    ${VALUEOUT}=${VALUE}
    ${SHOWOUTPUT}=    CLI:Write    show . ${FIELD}
    Should Contain    ${SHOWOUTPUT}    ${VALUEOUT}

CLI:Test Set Field Valid Options
    [Documentation]
		...    Tests invalid options for the set= command at the current location and confirms the error message.
    ...
    ...    Keyword returns no output. Command is checked for errors and will fail if not correct errors are displayed.
    ...
    ...    Examples:
    ...
    ...    | CLI:Enter Path | /settings/date_and_time/ |  |
    ...    | CLI:Test Set Field Invalid Options | zone | met | Invalid value: 'met' for parameter: 'zone' |
    ...    | Output      |   | |
    ...    |  |  [admin@nodegrid devices]#  |  |  |  |  | Pass |
    ...
    [Arguments]    ${FIELD}    ${VALUE}    ${revert}=yes    ${passwordfield}=no   ${QUOTATION_MARKS}=No
    Run Keyword If    '${QUOTATION_MARKS}' == 'No'   CLI:Write    set ${FIELD}=${VALUE}
    Run Keyword If    '${QUOTATION_MARKS}' == 'Yes'   CLI:Write    set ${FIELD}="${VALUE}"
    ${SHOWOUTPUT}=    CLI:Show
    ${TESTVALUE}=    Set Variable If    '${passwordfield}' == 'no'    ${VALUE}    ********
    Should Contain    ${SHOWOUTPUT}    ${FIELD} = ${TESTVALUE}
    Run Keyword If    '${revert}' == 'yes'    CLI:Write    revert

CLI:Test Message After Commit
    [Documentation]
		...    Test message returned after commit.
    [Arguments]    ${MESSAGE}
    ${OUTPUT}=    CLI:Write    commit    Raw    Yes
    Log to Console    --${OUTPUT}--
    Should be Equal    ${OUTPUT}    ${MESSAGE}
    [return]    ${OUTPUT}

CLI:Commands After Set Error
    [Documentation]
		...    REVIEW: Test name should be changed to CLI:Test Commands After Set Error
        ...    Test available commands after a invalid set command was issued using the 'CLI:Test Available Commands'
        ...
        ...    Keyword returns no output. 'CLI:Test Available Commands' command is checked for errors and will fail if
        ...    errors are displayed.
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path | /settings/date_and_time/ |  |
        ...    | CLI:Commands After Set Error | zone | emea|dublin | revert | | | |
        ...    | Output      |   | |
        ...    | | set zone=emea|dublin |
        ...    | | set zone=emea|dublin |
        ...    | | Error: Invalid value: emea|dublin for parameter: zone |
        ...    | | cd    change_password    commit    event_system_audit    event_system_clear    exit    hostname
        ...         ls    pwd    quit    reboot    revert    set |
        ...    |  |  [+admin@nodegrid date_and_time]#  |  |  |  |  | Pass |
        ...    | |
	[Arguments]	${FIELD}	${INVALID_VALUE}	@{COMMANDS}
	Log     Command issued:\nset ${FIELD}=${INVALID_VALUE}     INFO    console=yes
	Write Bare	set ${FIELD}=${INVALID_VALUE}\n
	${OUTPUT}=	CLI:Read Until Prompt   Raw
	Should Contain	${OUTPUT}	Error
	CLI:Test Available Commands	@{COMMANDS}

CLI:Get User Whoami
    [Documentation]
		...    Returns the current user with the command whoami
        ...
        ...    The Keyword returns as output only the username.
        ...
        ...    Examples:
        ...
        ...    | ${OUTPUT}= | CLI:Get User Whoami |
        ...    | Log to Console |  ${OUTPUT} |
        ...    | Output      |
        ...    | | admin |
        ...    | |
	${OUTPUT}=  CLI:Write	whoami
	@{LINES}=	Split To Lines	${OUTPUT}	0	1
	${USER_WHOAMI}=	Get From List	${LINES}	0
	${USER_WHOAMI}=	Strip String	${USER_WHOAMI}
	[Return]	${USER_WHOAMI}

CLI:Get User Prompt
    [Documentation]
		...    Review, where is this used
	Write Bare	\n
	${OUTPUT}=	CLI:Read Until Prompt
	${OUTPUT}=	Remove String Using Regexp	${OUTPUT}	['\r']
	@{LINES}=	Split To Lines	${OUTPUT}	0	2
	${USER_PROMPT}=	Get From List	${LINES}	1
	@{USER_PROMPT}=	Split String	${USER_PROMPT}	@
	${USER_PROMPT}=	Get From List	${USER_PROMPT}	0
	${USER_PROMPT}=	Get Substring	${USER_PROMPT}	1
	[Return]	${USER_PROMPT}

CLI:Configure INTERFACE with IPv4 Address
    [Documentation]
		...    Configures a given network interface with a static IPv4 address. The command is checked for any errors
        ...    and confirmes that the values where set.
        ...
        ...    The Keyword returns no output
        ...
        ...    Examples:
        ...
        ...    | CLI:Configure INTERFACE with IPv4 Address | 192.168.1.1 | 255.255.255.0 | ETH1 | 192.168.1.255 |
        ...    | |
	[Arguments]     ${IPAddress}  ${BITMASK}  ${INTERFACE}=ETH1  ${GATEWAY}=${EMPTY}
	CLI:Enter Path	/settings/network_connections/${INTERFACE}
	Run Keyword If	'${GATEWAY}' != '${EMPTY}'	CLI:Write	set ipv4_mode=static ipv4_address=${IPAddress} ipv4_bitmask=${BITMASK} ipv4_gateway=${GATEWAY}
	Run Keyword Unless	'${GATEWAY}' != '${EMPTY}'	CLI:Write	set ipv4_mode=static ipv4_address=${IPAddress} ipv4_bitmask=${BITMASK}
	${OUTPUT}=	CLI:Commit
	Should Not Match Regexp     ${OUTPUT}       Error
	${OUTPUT}=	CLI:Test Show Command	ipv4_mode = static	ipv4_address = ${IPAddress}	ipv4_bitmask = ${BITMASK}	ipv4_gateway = ${GATEWAY}

CLI:Configure INTERFACE with IPv6 Address
    [Documentation]
		...    Configures a given network interface with a static IPv6 address. The command is checked for any errors
        ...    and confirmes that the values where set.
        ...
        ...    The Keyword returns no output
        ...
        ...    Examples:
        ...
        ...    | CLI:Configure INTERFACE with IPv6 Address | fe80::20c:29ff:fef5:bbf4 | 64 | ETH1 |
        ...    | |
	[Arguments]     ${IPAddress}  ${PREFIX}  ${INTERFACE}=ETH1  ${GATEWAY}=${EMPTY}
	CLI:Enter Path	    /settings/network_connections/${INTERFACE}
	CLI:Write	set ipv6_mode=static ipv6_address=${IPAddress} ipv6_prefix_length=${PREFIX}
	Run Keyword If  '${GATEWAY}' != '${EMPTY}'	CLI:Write	set ipv6_gateway=${GATEWAY}
	${OUTPUT}=	CLI:Commit
	Should Not Match Regexp     ${OUTPUT}       Error
	${OUTPUT}=	CLI:Test Show Command	ipv6_mode = static	ipv6_address = ${IPAddress}	ipv6_prefix_length = ${PREFIX}
	...	ipv6_gateway = ${GATEWAY}

CLI:Delete Device
    [Documentation]
		...    Deletes a specific device from the Nodegrid configuration. The keyword confirms that the device was
        ...    removed and that no errors happened during the process. The provided device name has to be valid and
        ...    match an existing device
        ...
        ...    The Keyword returns no output
        ...
        ...    Examples:
        ...
        ...    | CLI:Delete Device | test.device |  |  |
        ...    | |
	[Arguments]	${DEVICE_NAME}
	CLI:Enter Path	/settings/devices/
	Log	++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\ndelete ${DEVICE_NAME}	INFO	console=yes
	Write	delete ${DEVICE_NAME}
	${OUTPUT}=	Read Until	:
	Log	++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${OUTPUT}	INFO	console=yes
	Log	++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nyes	INFO	console=yes
	Write	yes
	${OUTPUT}=	CLI:Read Until Prompt
	Should Not Contain	${OUTPUT}	Error
	${OUTPUT}=	CLI:Test Show Command
	Should Not Contain	${OUTPUT}	${DEVICE_NAME}
	${OUTPUT}=	CLI:Commit

CLI:Add Device
    [Documentation]
		...    Adds a specific device to the Nodegrid configuration. Keyword confirms that no errors where
        ...    displayed during operation and if device is in the access table. Only the Device Name is required
        ...
        ...    The Keyword returns no output
        ...
        ...    Examples:
        ...
        ...    | CLI:Add Device | SuperMicro_Server |  |  |
        ...    | CLI:Add Device | SuperMicro_Server | ilo |  |
        ...    | CLI:Add Device | SuperMicro_Server | ilo | 192.168.1.1 |
        ...    | `Device | SuperMicro_Server | ilo | 192.168.1.1 | admin | .ZPE5ystems!2020 | on-demand |
        ...    | |
	[Arguments]	${DEVICE_NAME}	${DEVICE_TYPE}=${EMPTY}	${IP_ADDRESS}=${EMPTY}	${USERNAME}=${EMPTY}	${PASSWORD}=${EMPTY}	${MODE}=${EMPTY}
	...	${ADDITIONAL_FIELDS}=${EMPTY}
	CLI:Enter Path	/settings/devices/
	CLI:Add
	CLI:Set	name=${DEVICE_NAME}
	Run Keyword If	'${DEVICE_TYPE}' != '${EMPTY}'	CLI:Set	type=${DEVICE_TYPE}
	Run Keyword If	'${IP_ADDRESS}' != '${EMPTY}'	CLI:Set	ip_address=${IP_ADDRESS}
	Run Keyword If	'${USERNAME}' != '${EMPTY}'	CLI:Set	username=${USERNAME}
	Run Keyword If	'${PASSWORD}' != '${EMPTY}'	CLI:Set	password=${PASSWORD}
	Run Keyword If	'${MODE}' != '${EMPTY}'	CLI:Set	mode=${MODE}
	Run Keyword If	'${ADDITIONAL_FIELDS}' != '${EMPTY}'	CLI:Set	${ADDITIONAL_FIELDS}

	${OUTPUT}=	CLI:Commit
	Should Not Contain	${OUTPUT}	Error
	${OUTPUT}=	CLI:Ls
	Should Contain	${OUTPUT}	${DEVICE_NAME}\
	Wait Until Keyword Succeeds	10sec	1sec	CLI:Can Access Device With Name	${DEVICE_NAME}

CLI:Table Should Have Headers
        [Documentation]
		...    Keyword confirmes if a given table header (first 2 lines) contains all of the provided strings
        ...
        ...    The Keyword returns no output
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path  | /settings/devices |
        ...    | CLI:Table Should Have Headers | name | connected | through | type | access | monitoring |
        ...    | |
	[Arguments]	@{HEADERS}
	${OUTPUT}=	CLI:Show
	${LINE}=	Get Line	${OUTPUT}	0
	@{WORDS}=	Split String In Words	${LINE}
	${HEADERS_COUNT}=	Get Length	${HEADERS}
	${WORDS_COUNT}=	Get Length	${WORDS}
	Should Be Equal As Integers	${HEADERS_COUNT}	${WORDS_COUNT}
	FOR	${HEADER}	${WORD}	IN ZIP	${HEADERS}	${WORDS}
		Should Be Equal	${HEADER}	${WORD}
	END

CLI:Get Local Devices Identifiers
    [Documentation]
		...    Keyword returns the device table row's IDs from the /settings/devices table (using ls command) as List
        ...    Object. All Devices will be returned regadless of the mode and status.
        ...
        ...    The Keyword returns a list object.
        ...
        ...    Examples:
        ...
        ...    | ${LIST}=  | CLI:Get Local Devices Identifiers |
        ...    | Log to Console | ${LIST} |
        ...    | Output| |
        ...    | | [u'SuperMicro_Server', u'SuperMicro_ServerA', u'SuperMicro_ServerB'] |
        ...    | |
	CLI:Enter Path	/settings/devices
	@{IDS}=	CLI:Get Available Paths
	Log List    ${IDS}     INFO
#	Log	\n++++++++++++++++++++++++++++ Device IDs: ++++++++++++++++++++++++++++\n${IDS}     INFO    console=yes
	[Return]	@{IDS}

CLI:Get Topology Table Identifiers
	[Documentation]
		...    Keyword returns the topology table row's IDs from the /access table (using ls command) as List Object.
        ...
        ...    The Keyword returns a list object.
        ...
        ...    Examples:
        ...
        ...    | ${LIST}=  | CLI:Get Topology Table Identifiers |
        ...    | Log to Console | ${LIST} |
        ...    | Output| |
        ...    | | [u'DC-Paris.localdomain', u'DC-Dublin', u'SuperMicro_Server', u'SuperMicro_ServerB'] |
        ...    | |
	CLI:Enter Path	/access
	@{IDS}=	CLI:Get Available Paths
	Log List    ${IDS}     INFO
#	Log	\n++++++++++++++++++++++++++++ Device IDs: ++++++++++++++++++++++++++++\n${IDS}     INFO    console=yes
	[Return]	@{IDS}

CLI:Get Available Paths
	[Documentation]
		...    Keyword returns the available paths as List Object.
        ...
        ...    The Keyword returns a list object.
        ...
        ...    Examples:
        ...
        ...    | ${LIST}=  | CLI:Get Available Paths |
        ...    | Log to Console | ${LIST} |
        ...    | Output| |
        ...    | | [u'access', u'system', u'settings'] |
        ...    | |
	${OUTPUT}=	CLI:Ls
	${CLINES}=	Get Lines Matching Regexp	${OUTPUT}	.+/
	${CLINES}=	Remove String	${CLINES}	/
	${CLINES}=	Strip String	${CLINES}
	@{LINES}=	Split To Lines	${CLINES}
	Log	\n++++++++++++++++++++++++++++ Paths: ++++++++++++++++++++++++++++\n${LINES}     INFO    console=yes
	[Return]	@{LINES}

CLI:Available Folders Should Be
	[Documentation]
		...    *REVIEW REQUIRED* Keyword should be changed to Test Available Folders. Review implementation
	    ...    Keyword tests if the list of provided folders names matches the return folders at teh current location
        ...
        ...    The Keyword returns no output.
        ...
        ...    Examples:
        ...
        ...    | CLI:Available Folders Should Be | access | system | settings |
        ...    | | | | | Pass |
        ...    | CLI:Available Folders Should Be | access | system |  |
        ...    | | | | | Fail |
        ...    | |
	[Arguments]	@{FOLDERS}
	@{PATHS}=	CLI:Get Available Paths
	Sort List	@{FOLDERS}
	Sort List	@{PATHS}
	List Should Be Equal	@{FOLDERS}	@{PATHS}

CLI:String Contains
	[Documentation]
		...    Keyword tests if a given String conatins a specific substring
        ...
        ...    The Keyword returns either True or False
        ...
        ...    Examples:
        ...
        ...    | CLI:String Contains | Nodegrid | grid |
        ...    | | | | Pass |
        ...    | |
	[Arguments]	${STRING}	${SUBSTRING}
	${RESULT}=	Run Keyword And Return Status	Should Contain	${STRING}	${SUBSTRING}
	[Return]	${RESULT}

CLI:Test Get Devices
    [Documentation]
		...
    ${LS}=    CLI:Write    ls
    ${LS}=    Remove String    ${LS}    ${SPACE}    \t    \r    \n
    ${DEVICES}=    Split String    ${LS}    /
    [Return]    ${DEVICES}

CLI:Test Get Elements
    [Documentation]
		...
    ${LS}=    CLI:Ls
    ${LS}=    Remove String    ${LS}    ${SPACE}    \t    \r    \n
    ${DEVICES}=    Split String    ${LS}    /
    [Return]    ${DEVICES}

CLI:Test Count Elements
    [Documentation]
		...
    ${OUTPUT}=    CLI:Ls
    ${OUTPUT}=    Remove String    ${OUTPUT}    \n    \t    \r    ${SPACE}
    ${OUTPUT}=    Split String    ${OUTPUT}    /
    ${COUNT}=    Get Length    ${OUTPUT}
    ${COUNT}=    Evaluate    ${COUNT}-1
    [Return]    ${COUNT}

CLI:Check If Exists
    [Documentation]
		...    Keyword check if exist a value at its current location by ls command.
        ...
        ...    The keyword return pass if exist.
        ...    The keyword return fail if not exist.
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path    | /settings/authorization/ |
        ...    | CLI:Check If Exists |  another_group |
        ...    | |
        ...
    [Arguments]    ${VALUE}=${EMPTY}
    ${OUTPUT}=    CLI:Ls
    ${OUTPUT}=    Replace String    ${OUTPUT}    \n    ${SPACE}
    ${RESULT}=    Run Keyword And Return Status    Should Contain    ${OUTPUT}    ${SPACE}${VALUE}/
    [Return]    ${RESULT}

CLI:Test Device With Name Exist
    [Documentation]
		...    *REVIEW REQUIRED* CHANGED(2017-11-03) Tests only if part of the device name is true will not check for a
        ...    specific device name
        ...    Keyword tests if a given Device Name exist based on the /settings/devices table and returns the results
        ...
        ...    The Keyword returns either True or False
        ...
        ...    Examples:
        ...
        ...    |  |
        ...    | |
        ...    | |
	[Arguments]	${DEVICE}
	CLI:Enter Path	/settings/devices
	${OUTPUT}=	CLI:Ls
	${RESULT}=	CLI:String Contains	${OUTPUT}	${DEVICE}/
	[Return]	${RESULT}

CLI:Can Access Device With Name
	[Documentation]
		...    *REVIEW REQUIRED* Tests only if part of device name is true will not check for a specific device name
	    ...    Keyword tests if a Device Name exist and is accessable based on /access table and returns the results
        ...
        ...    The Keyword returns either True or False
        ...
        ...    Examples:
        ...
        ...    | |
        ...    | |
        ...    | |
	[Arguments]	${DEVICE}
	CLI:Enter Path	/access/
	${OUTPUT}=	CLI:Show
	${RESULT}=	CLI:String Contains	${OUTPUT}	${DEVICE}
	[Return]	${RESULT}

CLI:Check Devices
	[Documentation]
		...    | |
	[Arguments]	@{DEVICES}
	CLI:Enter Path	/access/
	CLI:Test Show Command	@{DEVICES}

CLI:Check WireGuard
	[Documentation]
		...    | |
	[Arguments]	@{WIREGUARDS}
	CLI:Enter Path	/settings/wireguard/
	CLI:Test Show Command	@{WIREGUARDS}

CLI:Change Password
	[Documentation]
		...    Keyword changes the password for the current user and confirms that the password change was successful.
        ...
        ...    The Keyword returns no output
        ...
        ...    Examples:
        ...
        ...    | CLI:Change Password | Current_PWD | New_PWD |
        ...    | |
	[Arguments]	${CURRENT_PASSWORD}	${NEW_PASSWORD}
	Write	change_password
	${CURRENT_PASSWORD_LABEL}=	Run Keyword If	'${NGVERSION}'>='3.2'	Set Variable	Current password:
	${CURRENT_PASSWORD_LABEL}=	Run Keyword If	'${NGVERSION}'=='3.2'	Set Variable	Old password:
        ...	ELSE
        ...	Set Variable	${CURRENT_PASSWORD_LABEL}
	${ALL_AFTER_CHANGE_PASSWORD}=	Read Until	${CURRENT_PASSWORD_LABEL}
	Write	${CURRENT_PASSWORD}
	${ALL_AFTER_CURRENT_PASSWORD}=	Read Until	New password:
	Write	${NEW_PASSWORD}
	${ALL_AFTER_NEW_PASSWORD}=	Read Until	Retype new password:
	Write	${NEW_PASSWORD}
	${ALL_AFTER_RETYPE_NEW_PASSWORD}=	Read Until Prompt
	Should Contain	${ALL_AFTER_RETYPE_NEW_PASSWORD}	passwd: password updated successfully

CLI:Change User Password From Shell
	[Documentation]
		...    Changes the password of an existing user through the shell.
	    ...
	    ...    It uses by default the root user to connect to the NG and is then chaning the password of the user
        ...
        ...    The Keyword returns no output
        ...
        ...    Examples:
        ...
        ...    | CLI:Change User Password From Shell | admin | .ZPE5ystems!2020 |
        ...    | Output: |
        ...    | |  |
	[Arguments]	${USER}	${NEW_PASSWORD}	${CURRENT_ROOT_PASSWORD}=${ROOT_PASSWORD}	${HOST_DIFF}=${HOST}
	CLI:Connect As Root	${HOST_DIFF}	${CURRENT_ROOT_PASSWORD}
	Write	passwd ${USER}
	${OUTPUT}=	Read Until	New password:
	Log To Console	\n${OUTPUT}
	Write	${NEW_PASSWORD}
	${OUTPUT}=	Read Until	Retype new password:
	Log To Console	\n${OUTPUT}
	${OUTPUT}=	CLI:Write	${NEW_PASSWORD}
	Log To Console	\n${OUTPUT}\n
	CLI:Close Connection	Yes
	Should Contain	${OUTPUT}	passwd: password updated successfully

CLI:Delete
    [Documentation]
	...    Keyword deletes a set of provide values from the Nodegrid at its current location
    ...
    ...    Values can be passed as a list or as a comma separated values string
    ...
    ...    The keyword is not returning a output.
    ...
    ...    Examples:
    ...
    ...    | CLI:Delete |  remote1 |
    ...
    [Arguments]    @{VALUES}
	${EMPTY_LIST}=	Run Keyword And Return Status	Should Be EMPTY	${VALUES}
	Return From Keyword If	${EMPTY_LIST}
	${FIRST_VALUE}=	Get From List	${VALUES}	0
	Return From Keyword If	'${FIRST_VALUE}' == '${EMPTY}'

    FOR    ${VALUE}    IN    @{VALUES}
		Continue For Loop If	'${VALUE}' == '${EMPTY}'
		CLI:Write    delete ${VALUE}
		CLI:Commit
	END

CLI:Delete Confirm
	[Documentation]
		...    Keyword deletes a set of provide values from the Nodegrid at its current location.
	...
	...    Keyword will answer the confirmation question with "Yes"
	...
	...    Values can be passed as a list or as a comma separated values string
	...
	...    The keyword is not returning a output.
	...
	...    Examples:
	...
	...    | CLI:Enter Path    | /settings/authorization/ |
	...    | CLI:Delete Confirm |  another_group |
	...    | |
	...
	[Arguments]	@{VALUES}
	${EMPTY_LIST}=	Run Keyword And Return Status	Should Be EMPTY	${VALUES}
	Return From Keyword If	${EMPTY_LIST}
	${FIRST_VALUE}=	Get From List	${VALUES}	0
	Return From Keyword If	'${FIRST_VALUE}' == '${EMPTY}'

	FOR	${VALUE}	IN	@{VALUES}
		Write	delete ${VALUE}
		Log To Console	Deleting ${VALUE}
		${DELETE_QUESTION}=	Read Until	:
		CLI:Write	yes
		CLI:Commit
	END

CLI:Delete If Exists
	[Documentation]
		...    Keyword deletes provides values from Nodegrid at its current location only if the values currently exist.
		...
		...    The keyword is not returning a output.
		...
		...    Examples:
		...
		...    | CLI:Enter Path    | /settings/authorization/ |
		...    | CLI:Delete If Exists |  another_group |
		...    | |
		...
	[Arguments]	@{VALUES}
	${EMPTY_LIST}=	Run Keyword And Return Status	Should Be EMPTY	${VALUES}
	Return From Keyword If	${EMPTY_LIST}

	${OUTPUT}=	CLI:Write	ls	user=Yes
	${OUTPUT}=	Replace String	${OUTPUT}	/	${SPACE}
	FOR	${VALUE}	IN	@{VALUES}
		${RESULT}=	Run Keyword And Return Status	Should Contain	${SPACE}${OUTPUT}${SPACE}	${SPACE}${VALUE}${SPACE}
		Run Keyword If	${RESULT}	CLI:Delete	${VALUE}
	END

CLI:Delete All
	[Documentation]
		...	Delete all entries
	${OUTPUT}=	CLI:Write	ls	user=Yes
	${IS_EMPTY}=	Run Keyword And Return Status	Should Be EMPTY	${OUTPUT}
	Return From Keyword If	${IS_EMPTY}
	${LIST}=	Split String	${OUTPUT}	/
	${JOINED}=	Catenate	SEPARATOR=,	@{LIST}
	CLI:Delete	${JOINED}

CLI:Delete All Confirm
	[Documentation]
		...	Delete confirm all entries
	${OUTPUT}=	CLI:Write	ls	user=Yes
	${IS_EMPTY}=	Run Keyword And Return Status	Should Be EMPTY	${OUTPUT}
	Return From Keyword If	${IS_EMPTY}
	${LIST}=	Split String	${OUTPUT}	/
	${JOINED}=	Catenate	SEPARATOR=,	@{LIST}
	CLI:Delete Confirm	${JOINED}

CLI:Should Be Equal
    [Documentation]
		...     Keyword helper for CLI:Delete If Exists that compares one string with a list of string to see if the first
    ...     string is in the list.
    [Arguments]  ${VALUE1}  @{LIST}
    ${STATUS}=  Set Variable  False
    FOR    ${VALUE}   IN  @{LIST}
		${VALUE}=   Strip String     ${VALUE}
		${STATUS}=	Run Keyword And Return Status   should be equal as strings      ${VALUE}   ${VALUE1}
		exit for loop if    ${STATUS}
	END
    [Return]    ${STATUS}

CLI:Delete If Exists Confirm
    [Documentation]
		...    Keyword deletes provide values from Nodegrid at its current location only if the values currently exist.
        ...
        ...    Keyword will answer the confirmation question with "Yes"
        ...
        ...    The keyword is not returning a output.
        ...
        ...    Examples:
        ...
        ...    | CLI:Enter Path    | /settings/authorization/ |
        ...    | CLI:Delete If Exists Confirm |  another_group |
        ...    | |
        ...
	[Arguments]	@{VALUES}
	${OUTPUT}=	CLI:Write	ls	user=Yes
	${OUTPUT}=	Remove String	${OUTPUT}	${\n}
	${LIST}=	Split String	${OUTPUT}	/
	FOR	${VALUE}	IN	@{VALUES}
		${RESULT}=	Run Keyword And Return Status	List Should Contain Value	${LIST}	${VALUE}
		Run Keyword If	${RESULT}	CLI:Delete Confirm	${VALUE}
	END

CLI:Delete Devices
    [Documentation]
		...    Keyword deletes a set of provide devices from the Nodegrid only if the devices currently exist.
        ...
        ...    The keyword is not returning a output.
        ...
        ...    Examples:
        ...
        ...    | CLI:Delete Devices |  test_device |
        ...    | |
        ...
    [Arguments]    @{DEVICES}
    CLI:Enter Path    /settings/devices
    ${DEVICES_LIST}=	Set Variable
    FOR    ${DEVICE}    IN    @{DEVICES}
		Wait Until Keyword Succeeds    5x    500ms    CLI:Delete If Exists Confirm    ${DEVICE}
	END

CLI:Delete All Devices
    [Documentation]
		...    Keyword deletes a set of provide devices from the Nodegrid only if the devices currently exist.
        ...
        ...    The keyword is not returning a output.
        ...
        ...    Examples:
        ...
        ...    | CLI:Delete Devices |  test_device |
        ...    | |
        ...
	IF	'${NGVERSION}'>'3.2'
		CLI:Enter Path  /settings/devices/
		Write	delete -
		Read Until	:
		Write	yes
		${PROMPT_MESSAGE}	Read Until Prompt
		${CONTAIN_PROMPT_MESSAGE}	Run Keyword And Return Status	Should Contain	${PROMPT_MESSAGE}	Error: Device is protected. Fail to delete.
		Set Client Configuration	timeout=60s
		CLI:Commit
		IF	not ${CONTAIN_PROMPT_MESSAGE}
			${IS_VM}	CLI:Is VM System
			Should Be True	${IS_VM}
			Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
		END
	ELSE
		@{DEVICES_LIST}=	CLI:Get Devices List	all	${PROTECTED_DEVICE_TYPES}
		${JOINED_DEVICES_LIST}=	Catenate	SEPARATOR=,	@{DEVICES_LIST}
		Set Client Configuration	timeout=60s
		CLI:Delete Confirm	${JOINED_DEVICES_LIST}
		Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	END

CLI:Delete All Wireguards 
    [Documentation]
		...    Keyword deletes a set of provide wireguards from the Nodegrid only if the wireguard currently exist.
        ...
        ...    The keyword is not returning a output.
        ...
        ...    Examples:
        ...
        ...    | CLI:Delete all Wireguards
        ...    | |
        ...
    
	CLI:Enter Path  /settings/wireguard/
	Write	delete -
	${PROMPT_MESSAGE}	Read Until Prompt
	${CONTAIN_PROMPT_MESSAGE}	Run Keyword And Return Status	Should Contain	${PROMPT_MESSAGE}	Error: Device is protected. Fail to delete.
	CLI:Commit
	
CLI:Remove From List Devices From Box
    [Documentation]
		...    Keyword removes tty and usb devices from list because in Nodegrid box it is protected devices
        ...
        ...    The keyword is not returning a output.
        ...
        ...    Examples:
        ...
        ...    | CLI:Remove From List Devices From Box | ['device1', 'device2', 'ttyS1', 'ttyS2'] |
        ...    | |
        ...
    [Arguments]	${DEVICES}
	${ISNOT_BOX}=	CLI:Is VM System
	Return From Keyword If	${ISNOT_BOX}
	FOR	${DEVICE}	IN	@{DEVICES}
		Continue For Loop If	'${DEVICE}' == '${EMPTY}'
		${ISNOT_PROTECTED}=	Run Keyword And Return Status	Should Not Match Regexp	${DEVICE}	^(ttyS|usbS)\\d?\\d
		Continue For Loop If	${ISNOT_PROTECTED}
		Remove Values From List	${DEVICES}	${DEVICE}
	END

CLI:Test All Cloud Fields
    [Documentation]
		...    Keyword tests all Cloud Fields in Version 4.0
    ...
    ...    The keyword is not returning a output.
    ...
    ...    Examples:
    ...
    ...    | CLI:Test All Cloud Fields |   |
    ...    | |
    ...
	@{list}=	Create List	auto_enroll	enable_cloud	enable_peer_management
	Run Keyword If	'${NGVERSION}' > '3.2'
        ...	Run Keywords
        ...	Append To List	${list}	auto_psk	enable_license_pool	lps_type
        ...	AND	CLI:Edit

	CLI:Test Set Available Fields	@{list}
	Run Keyword If	'${NGVERSION}' > '3.2'	CLI:Cancel    #2017-11-07 Changed to CLI:Cancel from CLI:Revert
        ...	ELSE	CLI:Revert

CLI:Connect
    [Documentation]
		...    Keyword creates a connection to a device and confirms if the connection was opened.
    ...
    ...    After the keyword was run is the output now connected to the end device and not the NG
    ...
    ...    Examples:
    ...
    ...    | CLI:Connect |  test_device |
    ...    | |
    ...
	[Arguments]	${DEVICE}
	CLI:Enter Path	/access
	CLI:Show
	CLI:Enter Path	${Device}
	Write	connect
	${CONNECTION}=	CLI:Read Until Regexp	((\(yes/no\))|(#))
	@{FIELDS}=      Create List     Enter       Ec.     to      help        cli
	FOR	${FIELD}	IN	@{FIELDS}
		Should Contain	${CONNECTION}	${FIELD}
	END
	${REMOTE_HOST_ID_CHANGED}=	Run Keyword And Return Status	Should Contain	${CONNECTION}	REMOTE HOST IDENTIFICATION HAS CHANGED
	Run Keyword If	${REMOTE_HOST_ID_CHANGED}	CLI:Write	yes
	Should not contain      ${CONNECTION}       Connection refused

CLI:Get Hostname
    [Documentation]
		...    Returns the the Nodegrid hostname
        ...
        ...    The result is returned as a string object
        ...
        ...    Examples:
        ...
        ...    | ${HOSTNAME}=                   | CLI:Get Hostname        |
        ...    | Output                            |  Host Name: nodegrid |
        ...    | Log to Console                    | ${HOSTNAME}          |
        ...    | Output                            |  nodegrid            |
        ${OUTPUT}=    CLI:Write   hostname
        @{LINES}=	Split To Lines	${OUTPUT}	0	1
	    ${HOSTNAME}=	Get From List	${LINES}	0
	    ${HOSTNAME}=	Strip String	${HOSTNAME}
        Log  \nHost Name: ${HOSTNAME}    INFO       console=yes
        [Return]    ${HOSTNAME}

CLI:Change Hostname
	[Arguments]	${HOSTNAME}
	CLI:Enter Path	/settings/network_settings
	CLI:Set	hostname=${HOSTNAME}
	CLI:Commit

CLI:Change Domain Name
	[Arguments]	${DOMAIN}
	CLI:Enter Path	/settings/network_settings
	CLI:Set	domain_name=${DOMAIN}
	CLI:Commit

CLI:Test Get Nodegrid Version
	[Documentation]
		...
	CLI:Enter Path	/system/
	${VERSION}=	CLI:Write	show about/ software	user=Yes
#	${VERSION}=	Remove String	${VERSION}	software	${SPACE}	:	=	v
	${VERSION}=	Fetch From Left	${VERSION}	(
	${VERSION}=	Fetch From Right	${VERSION}	software: v
	[Return]	${VERSION}

CLI:Test Get Connections
    [Documentation]
		...
    CLI:Enter Path    /settings/network_connections/
    ${RETURN}=    CLI:Ls
    ${RETURN}=    Remove String    ${RETURN}    \r    \n    \t
    ${CONNECTIONS}=    Create List
    FOR    ${INDEX}    IN RANGE    999999
		${CONN}=    Fetch From Left    ${RETURN}    /
		${RETURN}=    Remove String    ${RETURN}    ${CONN}/
		${CONN}=   Strip String     ${CONN}
		${LASTCHAR}=    Get Substring     ${CONN}     -1
		Run Keyword If  '${LASTCHAR}' == '#'    Exit For Loop
		Append to List    ${CONNECTIONS}    ${CONN}
	END
    [Return]    ${CONNECTIONS}

CLI:Should Contain All
	[Documentation]
		...		Check if the string(first argument) constains all the strings in the list(second argument)
	[Arguments]	${STRING}	${LIST}
	FOR	${ITEM}	IN	@{LIST}
		Should Contain	${STRING}	${ITEM}
	END

CLI:Should Not Contain All
	[Documentation]
		...		Check if the string(first argument) constains all the strings in the list(second argument)
	[Arguments]	${STRING}	${LIST}
	FOR	${ITEM}	IN	@{LIST}
		Should Not Contain	${STRING}	${ITEM}
	END

CLI:Outlet On
	[Documentation]
		...		Executes the outlet_on command
	[Arguments]	${DEVICE}=${EMPTY}	${OUTLET}=${EMPTY}	${MODE}=${EMPTY}
	Run Keyword If	'${DEVICE}' != '${EMPTY}'	CLI:Enter Path	/access/${DEVICE}
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\noutlet_on ${OUTLET}	INFO	console=yes
	Set Client Configuration	timeout=5s
	${OUT}=	Write	outlet_on ${OUTLET}
	${OUTPUT}=	Wait Until Keyword Succeeds	2m	5s	CLI:Read Until Prompt
	Run Keyword If	'${MODE}' == '${EMPTY}'	Should Not Match Regexp	${OUTPUT}	error.\\w+.failure	ignore_case=True
	[Teardown]	Set Client Configuration	timeout=30s

CLI:Outlet Off
	[Documentation]
		...		Executes the outlet_off command
	[Arguments]	${DEVICE}=${EMPTY}	${OUTLET}=${EMPTY}	${MODE}=${EMPTY}
	Run Keyword If	'${DEVICE}' != '${EMPTY}'	CLI:Enter Path	/access/${DEVICE}
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\noutlet_off ${OUTLET}	INFO	console=yes
	Set Client Configuration	timeout=5s
	Write	outlet_off ${OUTLET}
	${OUTPUT}=	Wait Until Keyword Succeeds	2m	5s	CLI:Read Until Prompt
	Run Keyword If	'${MODE}' == '${EMPTY}'	Should Not Match Regexp	${OUTPUT}	error.\\w+.failure	ignore_case=True
	[Teardown]	Set Client Configuration	timeout=30s

CLI:Outlet Cycle
	[Documentation]
		...	Executes the outlet_cycle command
	[Arguments]	${DEVICE}=${EMPTY}	${OUTLET}=${EMPTY}	${MODE}=${EMPTY}
	Run Keyword If	'${DEVICE}' != '${EMPTY}'	CLI:Enter Path	/access/${DEVICE}
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\noutlet_cycle ${OUTLET}	INFO	console=yes
	Set Client Configuration	timeout=5s
	Write	outlet_cycle ${OUTLET}
	${OUTPUT}=	Wait Until Keyword Succeeds	2m	5s	CLI:Read Until Prompt
	Run Keyword If	'${MODE}' == '${EMPTY}'	Should Not Match Regexp	${OUTPUT}	error.\\w+.failure	ignore_case=True
	[Teardown]		Set Client Configuration	timeout=30s

CLI:Outlet Status
	[Documentation]
		...	Executes the outlet_status command
	[Arguments]	${DEVICE}=${EMPTY}	${OUTLET}=${EMPTY}	${MODE}=${EMPTY}
	Run Keyword If	'${DEVICE}' != '${EMPTY}'	CLI:Enter Path	/access/${DEVICE}
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\noutlet_status ${OUTLET}	INFO	console=yes
	Write	outlet_status ${OUTLET}
	Set Client Configuration	timeout=5s
	${OUTPUT}=	Wait Until Keyword Succeeds	2m	5s	CLI:Read Until Prompt
	Run Keyword If	'${MODE}' == '${EMPTY}'	Should Not Match Regexp	${OUTPUT}	error.\\w+.failure	ignore_case=True
	[Teardown]	Set Client Configuration	timeout=30s
	[Return]	${OUTPUT}

CLI:Check Command All Outlets
	[Arguments]	${DEVICE_NAME}=${EMPTY}	${VALUE}=${EMPTY}	${OUTLET}=${EMPTY}
	${NO_EXPECTED}=	Run Keyword If	'${VALUE}' == 'on'	Set Variable	off
	...	ELSE	Set Variable	on
	Run Keyword If	'${DEVICE_NAME}' != '${EMPTY}'	CLI:Enter Path	/access/${DEVICE_NAME}
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\noutlet_status ${OUTLET}	INFO	console=yes
	${STATUS}=	Wait Until Keyword Succeeds	120s	5s	CLI:Outlet Status	DEVICE=${DEVICE_NAME}	OUTLET=${OUTLET}
	Should Not Contain	${STATUS}	${NO_EXPECTED}

CLI:Discover Now
	[Arguments]	${NAME}	${TYPE}=${EMPTY}
	CLI:Enter Path	/settings/auto_discovery/discover_now/
	${DISCOVER_3_2}=	Set Variable If	'${TYPE}' != '${EMPTY}'
	...	${TYPE}|${NAME}
	...	${NAME}
	Run Keyword If	'${NGVERSION}' == '3.2'	CLI:Write	discover_now ${DISCOVER_3_2}
	Run Keyword If	'${NGVERSION}' >= '4.2'	CLI:Write	discover_now ${NAME}

CLI:Reset Discovery Logs
	[Documentation]
		...	Resets discovery logs
	CLI:Enter Path	/settings/auto_discovery/discovery_logs
	CLI:Show
	CLI:Write	reset_logs
	CLI:Show

CLI:Get Current System Time In Seconds
	${TIME_SECONDS}=	CLI:Write	shell sudo date +'%s'	user=Yes
	[Return]	${TIME_SECONDS}

CLI:Get Discovery Logs After Time
	[Arguments]	${TIME_SECONDS}
	CLI:Enter Path	/settings/auto_discovery/discovery_logs
	${OUTPUT}=	CLI:Show
	${OUTPUT}=	Replace String Using Regexp	${OUTPUT}	\\s+Jan\\s+	${SPACE}01${SPACE}
	${OUTPUT}=	Replace String Using Regexp	${OUTPUT}	\\s+Feb\\s+	${SPACE}02${SPACE}
	${OUTPUT}=	Replace String Using Regexp	${OUTPUT}	\\s+Mar\\s+	${SPACE}03${SPACE}
	${OUTPUT}=	Replace String Using Regexp	${OUTPUT}	\\s+Apr\\s+	${SPACE}04${SPACE}
	${OUTPUT}=	Replace String Using Regexp	${OUTPUT}	\\s+May\\s+	${SPACE}05${SPACE}
	${OUTPUT}=	Replace String Using Regexp	${OUTPUT}	\\s+Jun\\s+	${SPACE}06${SPACE}
	${OUTPUT}=	Replace String Using Regexp	${OUTPUT}	\\s+Jul\\s+	${SPACE}07${SPACE}
	${OUTPUT}=	Replace String Using Regexp	${OUTPUT}	\\s+Aug\\s+	${SPACE}08${SPACE}
	${OUTPUT}=	Replace String Using Regexp	${OUTPUT}	\\s+Sep\\s+	${SPACE}09${SPACE}
	${OUTPUT}=	Replace String Using Regexp	${OUTPUT}	\\s+Oct\\s+	${SPACE}10${SPACE}
	${OUTPUT}=	Replace String Using Regexp	${OUTPUT}	\\s+Nov\\s+	${SPACE}11${SPACE}
	${OUTPUT}=	Replace String Using Regexp	${OUTPUT}	\\s+Dec\\s+	${SPACE}12${SPACE}
	@{ALL_DISCOVERY_LOGS}=	Split To Lines	${OUTPUT}	2
	@{DISCOVERY_LOGS}=	Create List

	FOR	${DISCOVERY_LOG}	IN	@{ALL_DISCOVERY_LOGS}
		${MATCHES}=	Get Regexp Matches	${DISCOVERY_LOG}	(\\d+\\s+\\d+\\s+\\d+:\\d+:\\d+\\s+\\d+).*	1
		${NO_MATCHES}=	Run Keyword And Return Status	Should Be EMPTY	${MATCHES}
		Continue For Loop If	${NO_MATCHES}
		${TIMESTAMP}=	Get From List	${MATCHES}	0
		${LOG_TIME_SECONDS}=	Convert Date	${TIMESTAMP}	date_format=%m %d %H:%M:%S %Y	result_format=epoch
		${TIME_SECONDS_INT}=	Convert To Integer	${TIME_SECONDS}
		${LOG_TIME_SECONDS_INT}=	Convert To Integer	${LOG_TIME_SECONDS}
		${IS_NEW_LOG}=	Run Keyword And Return Status	Should Be True	${TIME_SECONDS_INT} < ${LOG_TIME_SECONDS_INT}
		Run Keyword If	${IS_NEW_LOG}	Append To List	${DISCOVERY_LOGS}	${DISCOVERY_LOG}
	END

	${DISCOVERY_LOGS}=	Catenate	SEPARATOR=\n	@{DISCOVERY_LOGS}
	[Return]	${DISCOVERY_LOGS}

CLI:Discovery Logs Should Match Regexp And Time
	[Arguments]	${REGEXP}	${TIME_SECONDS}
	${DISCOVERY_LOGS}=	CLI:Get Discovery Logs After Time	${TIME_SECONDS}
	Should Match Regexp	${DISCOVERY_LOGS}	${REGEXP}

CLI:Add Rule
	[Documentation]
		...    The keyword adds a specific rule to the Nodegrid configuration. Keyword confirms that no errors were
		...    display during the operation and if the rule is on discovery rules path.
		...    The keyword does not return an output.
		...
	[Arguments]	${RULE_NAME}	${METHOD}	${CLONE_FROM}	${STATUS}=enabled
	...	${ACTION}=clone_mode_enabled	${ADDITIONAL_FIELDS}=${EMPTY}
	CLI:Enter Path	/settings/auto_discovery/discovery_rules/
	CLI:Add
	CLI:Set	rule_name=${RULE_NAME} method=${METHOD} clone_from=${CLONE_FROM} status=${STATUS} action=${ACTION} ${ADDITIONAL_FIELDS}
	CLI:Commit
	CLI:Test Show Command	${RULE_NAME}

CLI:Add Wireguard
	[Documentation]
		...    The keyword adds a specific rule to the Nodegrid configuration. Keyword confirms that no errors were
		...    display during the operation and if the rule is on discovery rules path.
		...    The keyword does not return an output.
		
	[Arguments]	${WIREGUARD_NAME}  ${WIREGUARD_INTERNAL_IP}=127.0.0.1  ${WIREGUARD_LISTENING_PORT}=6000
	CLI:Enter Path  /settings/wireguard/
	CLI:Add
	CLI:Set  interface_name=${WIREGUARD_NAME} internal_address=${WIREGUARD_INTERNAL_IP} listening_port=${WIREGUARD_LISTENING_PORT}
	CLI:Commit
    CLI:Show  /settings/wireguard/${WIREGUARD_NAME}/interfaces/

CLI:Add Network Scan
	[Documentation]
		...    The keyword adds a specific network scan to the Nodegrid configuration. Keyword confirms that no errors were
		...    display during the operation and if the network scan is on network scan path.
		...    The keyword does not return an output.
		...
	[Arguments]	${SCAN_ID}	${IP_RANGE_START}	${IP_RANGE_END}	${SIMILAR_DEVICES}=yes	${DEVICE}=${EMPTY}	${PORT_SCAN}=yes	${PING}=yes
	CLI:Enter Path	/settings/auto_discovery/network_scan/
	CLI:Add
	CLI:Set	scan_id=${SCAN_ID} ip_range_start=${IP_RANGE_START} ip_range_end=${IP_RANGE_END} similar_devices=${SIMILAR_DEVICES} device=${DEVICE} port_scan=${PORT_SCAN} ping=${PING}
	CLI:Commit
	CLI:Test Show Command	${SCAN_ID}

CLI:Delete Rules
    [Documentation]
		...    Keyword deletes a set of provide rules from the Nodegrid only if the rule currently exist.
        ...    The keyword does not return an output.
        ...
        ...    Examples:
        ...
        ...    | CLI:Delete Rules |  test_rule |
        ...    | |
        ...
    [Arguments]    @{RULES}
    CLI:Enter Path    /settings/auto_discovery/discovery_rules/
    FOR    ${RULE}    IN    @{RULES}
		Wait Until Keyword Succeeds    5x    500ms    CLI:Delete If Exists Confirm    ${RULE}
	END

CLI:Delete Network Scan
	 [Documentation]
		...    Keyword deletes a set of provide network scans from the Nodegrid only if the network scan currently exist.
        ...    The keyword does not return an output.
        ...
        ...    Examples:
        ...
        ...    | CLI:Delete Network Scan |  test_network_scan |
        ...    | |
        ...
    [Arguments]	@{SCAN_IDS}
	CLI:Enter Path	/settings/auto_discovery/network_scan
	 FOR    ${SCAN_ID}    IN    @{SCAN_IDS}
		Wait Until Keyword Succeeds    5x    500ms    CLI:Delete If Exists Confirm    ${SCAN_ID}
	END

CLI:GET TROUBLESHOOTING INFORMATION
    [Documentation]
		...    Get the DMESG, var/log/messages and uptime information from the Shell and returns the output. The keyword
    ...    opens a new session as root user and retieves the information this way.
    [Arguments]     ${USERNAME}=root     ${PASSWORD}=${ROOT_PASSWORD}   ${TAIL}=999     ${session_alias}=dmsgsession	${timeout}=${CLI_DEFAULT_TIMEOUT}
        Enable Ssh Logging  about.log
        Set Default Configuration	loglevel=INFO
        Set Default Configuration    prompt=#
        Set Default Configuration    newline=\n
        Set Default Configuration    width=400
        Set Default Configuration    height=600
        Set Default Configuration    timeout=${timeout}

        Open Connection     ${HOST}     alias=${session_alias}
        Login   ${USERNAME}     ${PASSWORD}     loglevel=INFO
        Write   dmesg -T --nopager | tail -${TAIL}
        ${OUTPUT}=      Read Until  :~#
        Log  DMESG Results:\n ${OUTPUT}    INFO       console=yes
        Write   tail -${TAIL} '/var/log/messages'
        ${OUTPUT}=      Read Until  :~#
        Log  messages Results:\n ${OUTPUT}    INFO       console=yes
        Write   uptime
        ${OUTPUT}=      Read Until  :~#
        Log  uptime:\n ${OUTPUT}    INFO       console=yes
        Close Connection

CLI:Get Devices List
    [Arguments]     ${DEVICE_TYPE}=all  ${EXCLUDE_DEVICE_TYPES}=${EMPTY}
    CLI:Enter Path  /settings/devices
    Log  DEVICE_TYPE:\n ${DEVICE_TYPE}    INFO       console=yes
    ${EXCLUDED_DEVICE_TYPES}=	Run Keyword If	'${EXCLUDE_DEVICE_TYPES}' != '${EMPTY}'
    ...	Split String    ${EXCLUDE_DEVICE_TYPES}  ,
    ${OUTPUT}=      CLI:Show
    @{DEVICELIST}=  Create List
    @{DEVICES}=    Run Keyword If  '${NGVERSION}' == '3.2'    Split to Lines    ${OUTPUT}    2    -2
    ...    ELSE    Split to Lines    ${OUTPUT}    2    -1

    FOR	${DEVICE}	IN	@{DEVICES}
		${LINE}=        Replace String Using Regexp    ${DEVICE}     \\s\\s*       --
		@{FIELDS}=      Split String    ${LINE}  --
		${LENGTH}=      Get Length      ${FIELDS}
		${STATUS}=	Run Keyword If	'${EXCLUDE_DEVICE_TYPES}' != '${EMPTY}'
		...	CLI:String Do Not Contain List Items	${DEVICE}	${EXCLUDED_DEVICE_TYPES}
		...	ELSE	Set Variable	${TRUE}
		Run Keyword If   ('${DEVICE_TYPE}' == 'all' and ${STATUS} and ${LENGTH} > 1)          Append to List      ${DEVICELIST}     ${FIELDS}[1]
		Run Keyword If   ('${DEVICE_TYPE}' in '${LINE}' and ${STATUS} and ${LENGTH} > 1)      Append to List      ${DEVICELIST}     ${FIELDS}[1]
		Log  Lines:\n ${LINE}    INFO       console=yes
	END
    [Return]   @{DEVICELIST}

CLI:Get Serial Devices List
	${SERIAL_DEVICES}=	CLI:Get Devices List	local_serial
	[Return]	${SERIAL_DEVICES}

CLI:Get USB Devices List
	${SERIAL_DEVICES}=	CLI:Get Devices List	usb_serialB
	[Return]	${SERIAL_DEVICES}

CLI:String Contains List Items
	[Arguments]	${STRING}	${LINES}
	FOR	${LINE}	IN	@{LINES}
		${CONTAINS}=	Run Keyword And Return Status	Should Contain	${STRING}	${LINE}
		Return From Keyword If	${CONTAINS} is ${TRUE}	${TRUE}
	END
	[Return]  ${FALSE}

CLI:String Do Not Contain List Items
	[Arguments]	${STRING}	${LINES}
	FOR	${LINE}	IN	@{LINES}
		${CONTAINS}=	Run Keyword And Return Status	Should Contain	${STRING}	${LINE}
		Return From Keyword If	${CONTAINS} is ${TRUE}	${FALSE}
	END
	[Return]  ${TRUE}

CLI:Has Dual Power or Fans
	[Documentation]
		...    Tests if the device is NSC or NSR, and if it has dual power or fans
	CLI:Enter Path	/settings/system_preferences
	${SHOWOUTPUT}=	CLI:Show
	${ret}=	Run Keyword and Return Status	Should Contain	${SHOWOUTPUT}	state of
	Return From Keyword	${ret}

CLI:Get Power Status
	${OUTPUT}=	CLI:Write	power_status
	Should Match Regexp	${OUTPUT}	Powerstatus: ((on)|(off))
	[Return]	${OUTPUT}

CLI:Test Device Connection
	[Arguments]	${DEVICE_NAME}	${STATE}=Connected
	${LOWER_STATE}=	Convert To Lowercase	${STATE}
	${STATE}=	Set Variable If	'${NGVERSION}' < '4.0'	${LOWER_STATE}	${STATE}
	CLI:Enter Path	/access
	CLI:Test Show Command Regexp	\\s+${DEVICE_NAME}\\s+${STATE}

CLI:Device Is Powered On
	${STATUS}=	CLI:Get Power Status
	${IS_ON}=	Run Keyword And Return Status	Should Be Equal	${STATUS}	Powerstatus: on
	[Return]	${IS_ON}

CLI:Connect Device
	[Documentation]
		...    Keyword creates a connection to a device and confirms if the connection was opened.
        ...
        ...    After the keyword was run is the output now connected to the end device and not the NG
        ...
        ...    Examples:
        ...
        ...    | CLI:Connect |  test_device |
        ...    | |
        ...
	[Arguments]	${DEVICE}	${PROMPT}=${EMPTY}	${FORCE_ATTACH}=No
	CLI:Enter Path	/access
	CLI:Show
	CLI:Enter Path	${Device}
	Write	connect
	${CONNECTION}=	CLI:Read Until Regexp	\\[.*\\](\\n\\r|\\r|\\n)+\\[.*\\]
	Log To Console	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${CONNECTION}\n

	Run Keyword If	'${FORCE_ATTACH}' == 'Yes'	Write	\x05cf
	${PROMPT}=	Read Until	${PROMPT}
	Log To Console	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${PROMPT}\n

	Should not contain	${CONNECTION}	Connection refused
	@{FIELDS}=	Create List     Enter       Ec.     to      help        cli
	FOR	${FIELD}	IN	@{FIELDS}
		Should Contain	${CONNECTION}	${FIELD}
	END

CLI:Disconnect Device
	[Arguments]	${RAW_MODE}=No
	${OUTPUT}=	CLI:Write	\x05c.	${RAW_MODE}

CLI:Test Device State
	[Arguments]	${STATE}=on
	${STATUS}=	CLI:Get Power Status
	[Return]	Should Be Equal	${STATUS}	Powerstatus: ${STATE}

CLI:Device Power On
	[Arguments]	${CHECK_MESSAGE}="Yes"
	${IS_ON}=	CLI:Device Is Powered On
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nopower_on	INFO	console=yes
	Write	power_on
	Set Client Configuration	timeout=5s
	${OUTPUT}=	Wait Until Keyword Succeeds	10x	5s	CLI:Read Until Prompt
	Set Client Configuration	timeout=30s
	${OUTPUT}=	Split To Lines	${OUTPUT}	0	1
	Run Keyword If	'${CHECK_MESSAGE}'=='Yes'	CLI:Check Return Power On	${IS_ON}	${OUTPUT}[0]
	Wait Until Keyword Succeeds	20x	1s	CLI:Test Device State	on

CLI:Check Return Power On
	[Arguments]	${IS_ON}	${OUTPUT}
	Run Keyword If	${IS_ON}	Should Be Equal	${OUTPUT}	Set Chassis Power Control to Up/On failed: Command not supported in present state
    ...	ELSE	Should Be Equal	${OUTPUT}	Chassis Power Control: Up/On

CLI:Device Power Off
	[Arguments]	${CHECK_MESSAGE}="Yes"
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\npower_off	INFO	console=yes
	Write	power_off
	Set Client Configuration	timeout=5s
	${OUTPUT}=	Wait Until Keyword Succeeds	10x	5s	CLI:Read Until Prompt
	Set Client Configuration	timeout=30s
	${STATUS}=	Split To Lines	${OUTPUT}	0	1
	Run Keyword If	'${CHECK_MESSAGE}'=='Yes'	Should Be Equal	${STATUS}[0]	Chassis Power Control: Down/Off
	Wait Until Keyword Succeeds	20x	1s	CLI:Test Device State	off

CLI:Device Power Cycle
	[Arguments]	${CHECK_MESSAGE}="Yes"
	${IS_ON}=	CLI:Device Is Powered On
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\npower_cycle	INFO	console=yes
	Write	power_cycle
	Set Client Configuration	timeout=5s
	${OUTPUT}=	Wait Until Keyword Succeeds	10x	5s	CLI:Read Until Prompt
	Set Client Configuration	timeout=30s
	${OUTPUT}=	Split To Lines	${OUTPUT}	0	1
	Run Keyword If	'${CHECK_MESSAGE}'=='Yes'	CLI:Check Return Power Cycle	${IS_ON}	${OUTPUT}[0]
	Wait Until Keyword Succeeds	40x	1s	CLI:Test Device State	on

CLI:Check Return Power Cycle
	[Arguments]	${IS_ON}	${OUTPUT}
	Run Keyword If	${IS_ON}	Should Be Equal	${OUTPUT}	Chassis Power Control: Cycle
    ...	ELSE	Should Be Equal	${OUTPUT}	Set Chassis Power Control to Cycle failed: Command not supported in present state

CLI:Power Status
	[Documentation]
		...		Executes the power_status command
	[Arguments]	${DEVICE}=${EMPTY}
	Run Keyword If	'${DEVICE}' != '${EMPTY}'	CLI:Enter Path	/access/${DEVICE}
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\npower_status	INFO	console=yes
	Write	power_status
	Set Client Configuration	timeout=5s
	${OUTPUT}=	Wait Until Keyword Succeeds	10x	5s	CLI:Read Until Prompt
	Set Client Configuration	timeout=30s
	Should Not Contain	${OUTPUT}	error.connection.failure
	[Return]	${OUTPUT}

CLI:Test power_status command
	# The arguments ${CHECK_MESSAGE} is necessary because imm don't return messages
	[Arguments]	${DEVICE}=${EMPTY}	${CHECK_MESSAGE}="Yes"
	CLI:Enter Path	/access
	CLI:Show
	CLI:Enter Path	${Device}
	${IS_ON}=	CLI:Device Is Powered On
	Run Keyword If	${IS_ON}
        ...	Run Keywords
        ...		CLI:Device Power Off	${CHECK_MESSAGE}
        ...		AND	CLI:Device Power On	${CHECK_MESSAGE}
        ...		AND	CLI:Device Power Cycle	${CHECK_MESSAGE}
        ...	ELSE
        ...	Run Keywords
        ...		CLI:Device Power On	${CHECK_MESSAGE}
        ...		AND	CLI:Device Power Cycle	${CHECK_MESSAGE}
        ...		AND	CLI:Device Power Off	${CHECK_MESSAGE}
        ...		AND	CLI:Device Power On	${CHECK_MESSAGE}

CLI:Test connect command
	[Arguments]	${DEVICE}	${PROMPT}
#	Sleep	240s
	CLI:Connect Device	${DEVICE}	${PROMPT}
	CLI:Disconnect Device
	Write	pwd
	${OUTPUT}=	CLI:Read Until Prompt
	Should Contain	${OUTPUT}	/

CLI:Get Active Sessions
    CLI:Enter Path	/system/device_sessions/
    ${OUTPUT}=     CLI:Show
    @{CONNECTIONLIST}=  Create List
    @{DEVICES}=     Run Keyword If  '${NGVERSION}' == '3.2'       Split to Lines  ${OUTPUT}   2   -2
                ...	ELSE                                        Split to Lines  ${OUTPUT}   2   -1
    FOR	${DEVICE}	IN	@{DEVICES}
		${LINE}=     Replace String Using Regexp    ${DEVICE}     \\s\\s*       --
		@{FIELDS}=     Split String    ${LINE}  --
		${LENGTH}=      Get Length      ${FIELDS}
		Run Keyword If  ${LENGTH} > 1   Append to List      ${CONNECTIONLIST}     ${FIELDS}[1]
		Log  Lines:\n ${LINE}    INFO       console=yes
	END
    [Return]   @{CONNECTIONLIST}

CLI:Terminate Session
	[Arguments]	${VALUE}	${RAW_MODE}=No
	CLI:Enter Path	/system/device_sessions/
	CLI:Write	terminate_device_sessions ${VALUE}	${RAW_MODE}

CLI:Terminate Session If Active
	[Arguments]	${VALUE}
	CLI:Enter Path	/system/device_sessions/
	${OUTPUT}=	CLI:Ls
	${RESULT}=	Run Keyword And Return Status	Should Contain	${OUTPUT}	${VALUE}
	Run Keyword If	${RESULT}	CLI:Terminate Session	${VALUE}

CLI:Terminate Active Sessions
    [Documentation]
		...    Keyword terminates a set of provide active sessions from the Nodegrid only if the sessions currently exist.
    ...
    ...    The keyword is not returning a output.
    ...
    ...    Examples:
    ...
    ...    | CLI:Terminate Active Sessions |  test_device |
    ...    | |
    ...
	[Arguments]	@{SESSIONS}
	CLI:Enter Path	/system/device_sessions/
	FOR	${SESSION}	IN	@{SESSIONS}
		Wait Until Keyword Succeeds     3x      200ms   CLI:Terminate Session If Active	    ${SESSION}
	END

CLI:Rename Device
    [Documentation]
		...    Keyword renames an existing Device.
    ...
    ...    The keyword will check if the device names was scucessfully changed
    ...
    ...    Examples:
    ...
    ...    | CLI:Rename Device |  test_device |    new_test_device
    ...    | |
    ...
    [Arguments]	${DEVICE_NAME}  ${NEW_DEVICE_NAME}
    CLI:Test Device With Name Exist     ${DEVICE_NAME}
    CLI:Enter Path	/settings/devices/
    CLI:Write  rename ${DEVICE_NAME}
    CLI:Set Field     new_name    ${NEW_DEVICE_NAME}
    CLI:Commit
    CLI:Test Device With Name Exist     ${NEW_DEVICE_NAME}

CLI:Clone Device
    [Documentation]
		...    Keyword clones an existing Device.
    ...
    ...    The keyword will check if the device was scucessfully cloned
    ...
    ...    Examples:
    ...
    ...    | CLI:Clone Device |  test_device |    new_test_device
    ...    | |
    ...
    [Arguments]	${DEVICE_NAME}  ${NEW_DEVICE_NAME}     &{PARAMETERS}
    CLI:Test Device With Name Exist     ${DEVICE_NAME}
    CLI:Enter Path	/settings/devices/
    CLI:Write  clone ${DEVICE_NAME}
    CLI:Set Field     name    ${NEW_DEVICE_NAME}
    @{KEYS}=   Get Dictionary Keys    ${PARAMETERS}
	FOR	${KEY}	IN	@{KEYS}
		${VALUE}=   Get From Dictionary     ${PARAMETERS}   ${KEY}
		CLI:Set Field   ${KEY}      ${VALUE}
	END
    CLI:Commit
    CLI:Test Device With Name Exist     ${NEW_DEVICE_NAME}

CLI:Clone Type
	[Documentation]
		...    Keyword clones an existing Type.
    ...
    ...    The keyword will check if the type was scucessfully cloned
    ...
    ...	   The keyword will add options to cloned type
    ...    Examples:
    ...
    ...    | CLI:Clone Type |  test_type |    new_test_type
    ...    | |
    ...
    [Arguments]	${TYPE_NAME}  ${NEW_TYPE_NAME}     @{PARAMETERS}
    CLI:Enter Path  /settings/types
	CLI:Delete If Exists Confirm	${NEW_TYPE_NAME}
	CLI:Write  clone_type ${TYPE_NAME}
	CLI:Set  device_type_name=${NEW_TYPE_NAME}
	CLI:Commit
	CLI:Test Show Command	${NEW_TYPE_NAME}
	CLI:Enter Path  ${NEW_TYPE_NAME}
	FOR 	${FIELD} 	IN 		@{PARAMETERS}
		CLI:Write  	set ${FIELD}
	END
	CLI:Commit	Raw

CLI:Add User
    [Arguments]	${USERNAME}	${PASSWORD}	${GROUP}=user
    ${RESULT}=	CLI:Check If User Exists	${USERNAME}
    Return From Keyword If	${RESULT}
    CLI:Add
    CLI:Set	username=${USERNAME} password=${PASSWORD}
    CLI:Set	user_group=${GROUP}
    CLI:Commit

CLI:Delete Users
	[Arguments]	@{USERNAMES}
	CLI:Enter Path	/settings/local_accounts/
	CLI:Delete If Exists Confirm	@{USERNAMES}

CLI:Check If User Exists
    [Arguments]	@{USERNAME}
    CLI:Enter Path	/settings/local_accounts/
    ${OUTPUT}=	CLI:Ls
    ${OUTPUT}=	Replace String	${OUTPUT}	${\n}	${SPACE}
    FOR	${USERNAME}	IN	@{USERNAME}
		${RESULT}=	Run Keyword And Return Status	Should Contain	${SPACE}${OUTPUT}	${SPACE}${USERNAME}/
	END
    [Return]	${RESULT}

CLI:Add User To Group
    [Arguments]	${USERNAME}     ${GROUP}
    CLI:Enter Path  /settings/local_accounts/${USERNAME}
    ${USERGROUPS}=  CLI:Get User Group Membership   ${USERNAME}
    @{GROUPS}=      Split String     ${USERGROUPS}  ,
    ${x}=   Get Match Count     ${GROUPS}   ${GROUP}
    Run Keyword If  ${x} == 0    Append to List  ${GROUPS}   ${GROUP}
    ${GROUP_STRING}=    catenate  SEPARATOR=,   @{GROUPS}
    CLI:Set Field  user_group   ${GROUP_STRING}
    CLI:Commit

CLI:Get User Group Membership
    [Arguments]	${USERNAME}
    CLI:Enter Path  /settings/local_accounts/${USERNAME}
    ${RETURN}=  CLI:Show    /settings/local_accounts/${USERNAME}
	@{lines_list}=     Split To Lines      ${RETURN}      0   -1
	&{dict}=    Create Dictionary
    FOR     ${ELEMENT}      IN      @{lines_list}
		Log  Start Loop for ${ELEMENT}   INFO
		${len}=  Get Length      ${ELEMENT}
		Log  Lenghth of element ${len}   INFO
		${ELEMENT}=     Replace String      ${ELEMENT}      :       =       count=1
		${v1}	${v2}=	Run Keyword If   ${len} > 0     Split String	${ELEMENT}	=	1
		${v1}=      Run Keyword If	${len} > 0	Strip String    ${v1}
		${v2}=      Run Keyword If	${len} > 0	  Strip String    ${v2}
		Run Keyword If	${len} > 0	Set To Dictionary	${dict}	    ${v1}	${v2}
	END
	${USERGROUPS}=      Get From Dictionary     ${dict}     user_group
    [Return]  ${USERGROUPS}

CLI:Get All User Groups
    CLI:Enter Path  /settings/authorization/
    ${RETURN}=  CLI:Show    /settings/authorization/
    @{lines_list}=      Run Keyword If	'${NGVERSION}' <= '3.2'    Split To Lines      ${RETURN}      2   -3
	                    ...	ELSE                                     Split To Lines      ${RETURN}      2   -1
	@{list}=    Create List
    FOR     ${ELEMENT}      IN      @{lines_list}
		${len}=  Get Length      ${ELEMENT}
		${v1}=      Run Keyword If	${len} > 0	Strip String    ${ELEMENT}
		Run Keyword If	${len} > 0	Append to List	${list}	    ${v1}
	END
    [Return]  @{list}

CLI:Add License Key
	[Arguments]	${LICENSE_KEY}
	CLI:Enter Path	/settings/license/
	CLI:Add
	CLI:Set	license_key=${LICENSE_KEY}
	${OUTPUT}=	CLI:Commit
	[Return]	${OUTPUT}

CLI:Delete License If Exists
	[Arguments]	${LICENSE_KEY}
	CLI:Enter Path	/settings/license/
	${LS}=	CLI:Write	ls	user=Yes
	${LS}=	Remove String	${LS}	${SPACE}	\t	\r	\n
	@{LICENSES}=	Split String	${LS}	/
	${EXIST}=	Run Keyword And Return Status	List Should Contain Value	${LICENSES}	${LICENSE_KEY}
	Return From Keyword If	${EXIST} == ${FALSE}
	CLI:Delete	${LICENSE_KEY}

CLI:Delete All License Keys Installed
	CLI:Enter Path	/settings/license/
	${LS}=	CLI:Write	ls	user=Yes
	${LS}=	Remove String	${LS}	${SPACE}	\t	\r	\n
	@{LICENSES}=	Split String	${LS}	/
	Run Keyword If	'${NGVERSION}' == '3.2'	Remove From List	${LICENSES}	-1
	FOR	${LICENSE}	IN	@{LICENSES}
		Run Keyword Unless	'${LICENSE}' == '${EMPTY}'	CLI:Write	delete ${LICENSE}
	END

CLI:Get Number Of Available Access License Keys Installed
	${OUTPUT}=	CLI:Show	/system/about licenses
	${FULL_MATCH}	${TOTAL_NUMBER_OF_LICENSES}=	Should Match Regexp	${OUTPUT}	licenses: (\\d+)
	${DEVICES_LIST}=	CLI:Get Devices List
	${NUMBER_OF_DEVICES}=	Get Length	${DEVICES_LIST}
	[Return]	${${TOTAL_NUMBER_OF_LICENSES} - ${NUMBER_OF_DEVICES}}

CLI:Enable Auditing Destination To Events
	[Documentation]
		...	in syslog admin_session=no
	[Arguments]	${DESTINATION}=local
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Set Field	destination	${DESTINATION}
	CLI:Enter Path	/settings/auditing/settings/
	CLI:Set Field	datalog_destination	file
	CLI:Enter Path	/settings/auditing/destinations/syslog
	CLI:Set Field	admin_session	no
#	CLI:Enter Path	/settings/auditing/events/file/
#	CLI:Set	aaa_events=yes device_events=yes logging_events=yes system_events=yes
	CLI:Commit

CLI:Enable All Events
	[Arguments]	${VALUE}=yes	${DESTINATION}=file
	CLI:Enter Path	/settings/auditing/events/${DESTINATION}/
	CLI:Set	aaa_events=${VALUE} device_events=${VALUE} logging_events=${VALUE} system_events=${VALUE}
	CLI:Commit

CLI:Enable System Events
	[Arguments]	${VALUE}=yes	${DESTINATION}=file
	CLI:Enter Path	/settings/auditing/events/${DESTINATION}/
	CLI:Set	system_events=${VALUE}
	CLI:Commit

CLI:Enable AAA Events
	[Arguments]	${VALUE}=yes	${DESTINATION}=file
	CLI:Enter Path	/settings/auditing/events/${DESTINATION}/
	CLI:Set Field	aaa_events	${VALUE}
	CLI:Commit

CLI:Enable Device Events
	[Arguments]	${VALUE}=yes	${DESTINATION}=file
	CLI:Enter Path	/settings/auditing/events/${DESTINATION}/
	CLI:Set Field	device_events	${VALUE}
	CLI:Commit

CLI:Enable Logging Events
	[Arguments]	${VALUE}=yes	${DESTINATION}=file
	CLI:Enter Path	/settings/auditing/events/${DESTINATION}/
	CLI:Set Field	logging_events	${VALUE}
	CLI:Commit

CLI:Clear Event Logfile
	[Documentation]
		...	Must have 2 opened connections (alias).
		...	By default called default (CLI) and root_session (ROOT), which you can change an pass as argument
	[Arguments]	${HOSTNAME}	${CLI_ALIAS}=default	${ROOT_ALIAS}=root_session
	CLI:Switch Connection	${CLI_ALIAS}
	Run Keyword If	${NGVERSION} == 3.2	CLI:Enter Path	/
	#event_system_clear does not work in all paths in 3.2
	CLI:Write	event_system_clear
	CLI:Switch Connection	${ROOT_ALIAS}
	${OUTPUT}=	CLI:Write	cat /var/local/EVT/${HOSTNAME}
	Should Not Contain	${OUTPUT}	Event ID

CLI:Test Event ID Logged
	[Documentation]
		...	Must be as root
	[Arguments]	${HOSTNAME}	${EVENTID}	${EVENTDESCRIPTION}
	${OUTPUT}	CLI:Write	cat /var/local/EVT/${HOSTNAME}
	Should Contain	${OUTPUT}	Event ID ${EVENTID}: ${EVENTDESCRIPTION}

CLI:Test Event ID Not Logged
	[Documentation]
		...	Must be as root
	[Arguments]	${HOSTNAME}	${EVENTID}
	${OUTPUT}=	CLI:Write	cat /var/local/EVT/${HOSTNAME}
	Should Not Contain	${OUTPUT}	${EVENTID}:

CLI:Test NFS Event ID Logged
	[Documentation]
		...	Must be in nfs
	[Arguments]	${PATH}	${HOSTNAME}	${EVENTID}	${EVENTDESCRIPTION}
	${OUTPUT}=	CLI:Write	cat ${PATH}/EVT/${HOSTNAME}
	Should Contain	${OUTPUT}	Event ID ${EVENTID}: ${EVENTDESCRIPTION}

CLI:Test NFS Event ID Not Logged
	[Documentation]
		...	Must be in nfs
	[Arguments]	${PATH}	${HOSTNAME}	${EVENTID}
	${OUTPUT}=	CLI:Write	cat ${PATH}/EVT/${HOSTNAME}
	Should Not Contain	${OUTPUT}	${EVENTID}:

CLI:Enable Session Logging
	[Arguments]	${LOGGING}=yes	${ALERT}=yes	@{STRINGS}
	CLI:Enter Path	/settings/system_logging
	CLI:Set	enable_session_logging=${LOGGING}
	Run Keyword If	'${LOGGING}' == 'yes'	CLI:Set	enable_session_logging_alerts=${ALERT}
	${INDEX}=	Evaluate	1
	FOR	${STRING}	IN	@{STRINGS}
		Exit For Loop If	'${LOGGING}' != 'yes'
		Exit For Loop If	'${ALERT}' != 'yes'
		CLI:Set	session_string_${INDEX}=${STRING}
		${INDEX}=	Evaluate	${INDEX}+1
	END
	CLI:Commit

CLI:Enable Data Logging
	[Arguments]	${DEVICE_NAME}	${LOGGING}=yes	${ALERT}=yes	@{STRINGS}
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/logging
	CLI:Set	data_logging=${LOGGING}
	Run Keyword If	'${LOGGING}' == 'yes'	CLI:Set	enable_data_logging_alerts=${ALERT}
	${INDEX}=	Evaluate	1
	FOR	${STRING}	IN	@{STRINGS}
		Exit For Loop If	'${LOGGING}' != 'yes'
		Exit For Loop If	'${ALERT}' != 'yes'
		CLI:Set	data_string_${INDEX}=${STRING}
		${INDEX}=	Evaluate	${INDEX}+1
	END
	CLI:Commit

CLI:Enable Event Logging
	[Arguments]	${DEVICE_NAME}	${LOGGING}=yes	${FREQUENCY}=1	${UNIT}=hours	${ALERT}=yes	@{STRINGS}
	CLI:Enter Path	/settings/devices/${DEVICE_NAME}/logging
	CLI:Set	event_logging=${LOGGING}
	Run Keyword If	'${LOGGING}' == 'yes'	CLI:Set	event_log_frequency=${FREQUENCY} event_log_unit=${UNIT}
	...	enable_event_logging_alerts=${ALERT}
	${INDEX}=	Evaluate	1
	FOR	${STRING}	IN	@{STRINGS}
		Exit For Loop If	'${LOGGING}' != 'yes'
		Exit For Loop If	'${ALERT}' != 'yes'
		CLI:Set	data_string_${INDEX}=${STRING}
		${INDEX}=	Evaluate	${INDEX}+1
	END
	CLI:Commit

CLI:Enable Auditing Destination As NFS
	[Arguments]	${NFS_HOST}	${NFS_PATH}	${FILE_SIZE}=1024	${NUMBER_ARCHIVES}=10	${TIME_ARCHIVE}=${EMPTY}
	CLI:Enter Path	/settings/auditing/destinations/file
	CLI:Set	destination=nfs nfs_server=${NFS_HOST} nfs_path=${NFS_PATH} nfs_file_size=${FILE_SIZE} number_of_archives_in_nfs=${NUMBER_ARCHIVES} nfs_archive_by_time=${TIME_ARCHIVE}
	CLI:Commit

CLI:Check Nodegrid Resources Status
	[Documentation]	Must be as root
	CLI:Write	ps -ef
	CLI:Write	netstat -nopt
	CLI:Write	df -m
	CLI:Write	free -m
	CLI:Write	cat /proc/meminfo

CLI:Reset
	[Arguments]	${ELEMENT}
	Log	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nreset ${ELEMENT}	INFO	console=yes
	Set Client Configuration	timeout=5s
	Wait Until Keyword Succeeds	2m	1s	CLI:Read Until Prompt
	Set Client Configuration	timeout=30s

CLI:Delete All Devices Catenate
	[Arguments]	${REGEX}=^\\s+\\w+(|-\\d)\\s+(ttyS|usbS)\\d?(|-)\\d
	CLI:Enter Path	/settings/devices/
	${OUTPUT}=	CLI:Show
	${PROTECTED_LINES}=	Get Lines Matching Regexp	${OUTPUT}	${REGEX}	True
	${COUNT}=	Get Line Count	${PROTECTED_LINES}
	${OUTPUT}=	CLI:Write	ls	user=Yes	lines=No
	${OUTPUT}=	Remove String	${OUTPUT}	/
	${UNPROTECTED_DEVICES}=	Split To Lines	${OUTPUT}	${COUNT}
	${JOINED}=	Catenate	SEPARATOR=,	@{UNPROTECTED_DEVICES}
	Run Keyword If	'${JOINED}' != '${EMPTY}'	CLI:Delete Confirm	${JOINED}

CLI:Apply Factory Settings
	[Documentation]
	...	Apply factory settings to the system. It does not change any configuration
	...	after applying it. The connection is closed after running the keyword.
	...
	...	There is an optional argument to specify to clear the system logs
	[Arguments]	${CLEAR_LOGS}=${EMPTY}	${PROFILE}=${EMPTY}
	Set Client Configuration	timeout=60s
	CLI:Write	factory_settings
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Run Keyword If	'${CLEAR_LOGS}' == 'Yes'	CLI:Write	set clear_all_log_files=yes
	Run Keyword If	'${PROFILE}' != '${EMPTY}'	CLI:Set	system_profile=${PROFILE}
	${OUTPUT}=	Write	restore
	Log To Console	${OUTPUT}
	${OUTPUT}=	Read Until	you want to proceed? (yes, no)
	Log To Console	${OUTPUT}
	${OUTPUT}=	Write	yes
	Log To Console	${OUTPUT}
	Log	${OUTPUT}
#	${STATUS}=	CLI:Read Until Regexp	(]#|The system is going down for reboot NOW\\!)	TIMEOUT=60s
	${STATUS}=	CLI:Read Until Regexp	(]#|\\$|y)	TIMEOUT=60s	#Added y to regexp because we catch a problem that sometimes appears Y on console (Just on automation).
#	Should Contain	${STATUS}	The system is going down for reboot NOW!
	Log To Console	${STATUS}
	${IS_ERROR}=	Run Keyword And Return Status	Should Contain	${STATUS}	Error: Transaction error: no response
	Run Keyword If	${IS_ERROR} == ${FALSE}	Should Not Contain	${STATUS}	Error:
	CLI:Close Connection
	Sleep	240s
	Run Keyword If	'${NGVERSION}' == '4.2'	Sleep	180s

CLI:QA Device First Settings
	[Documentation]
	...	Fast changing configuration in 5.0+ version after device having done
	...	factory settings or upgrade/downgrade software
	...
	...	On version 4.2- it will only try to reconnect to the device
	...
	...	The new configuration will match the QA defaults
	...
	...	The device should not be the peer
	[Arguments]	${HOST_DIFF}=${HOST}	${VERSION}=${NGVERSION}
	Run Keyword If	'${VERSION}' >= '5.0'	CLI:QA Device First Settings 5.0+	${HOST_DIFF}	${VERSION}
	Run Keyword If	'${VERSION}' <= '4.2'	CLI:Wait Nodegrid Webserver To Be Up

CLI:QA Device First Settings 5.0+
	[Arguments]	${HOST_DIFF}	${VERSION}
	CLI:Change Password At Login Time	HOST_DIFF=${HOST_DIFF}
	CLI:Wait Nodegrid Webserver To Be Up	${HOST_DIFF}	${QA_PASSWORD}	4m
	CLI:Open	${DEFAULT_USERNAME}	${QA_PASSWORD}	HOST_DIFF=${HOST_DIFF}	session_alias=factory_session_admin
	CLI:Enable Ssh And Console Access As Root
	CLI:Close Connection	Yes
	CLI:Change Password At Login Time	root	root	prompt=~#   HOST_DIFF=${HOST_DIFF}
	CLI:Change Root And Admin Password To QA Default	HOST_DIFF=${HOST_DIFF}

CLI:Wait Nodegrid Webserver To Be Up
	[Arguments]	${HOST_DIFF}=${HOST}	${PASSWORD}=${DEFAULT_PASSWORD}	${LOGIN_TIMEOUT}=4m
	Wait Until Keyword Succeeds	${LOGIN_TIMEOUT}	1s
	...	CLI:Open	${DEFAULT_USERNAME}	${PASSWORD}	HOST_DIFF=${HOST_DIFF}	session_alias=factory_session_admin
	CLI:Close Connection	Yes

CLI:Enable Ssh And Console Access As Root
	CLI:Enter Path	/settings/services
	CLI:Set	ssh_allow_root_access=yes
	CLI:Set	allow_root_console_access=yes
	Set Client Configuration	timeout=120s
	CLI:Commit
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}

CLI:Change Password At Login Time
	[Arguments]	${USERNAME}=${DEFAULT_USERNAME}	${CURR_PASSWORD}=${FACTORY_PASSWORD}
	...	${NEW_PASSWORD}=${QA_PASSWORD}	${prompt}=]#   ${HOST_DIFF}=${HOST}
	...	${CONNECT_TIMEOUT}=2m

	Run Keyword If	"${HOST_DIFF}" == "${HOST}"   CLI:Connect As Root	${HOSTPEER}
	Run Keyword If	"${HOST_DIFF}" == "${HOSTPEER}"   CLI:Connect As Root	${HOST}
	Run Keyword If	${HAS_HOSTSHARED}	Run Keyword If	"${HOST_DIFF}" == "${HOSTSHARED}"   CLI:Connect As Root	${HOST}

	Set Client Configuration	prompt=~#
	CLI:Write	ssh-keygen -f "/home/root/.ssh/known_hosts" -R "${HOST_DIFF}"

	${OUTPUT}=	Wait Until Keyword Succeeds	${CONNECT_TIMEOUT}	10s
	...	CLI:Open SSH Connection From Peer  ${USERNAME}   ${HOST_DIFF}

	${HAS_TO_CONFIRM}=	Run Keyword And Return Status
	...	Should Match Regexp	${OUTPUT}	Are you sure you want to continue connecting \\(.*\\)\\?
	Run Keyword If	${HAS_TO_CONFIRM}	Write	yes
	${OUTPUT}=	Run Keyword If	${HAS_TO_CONFIRM}
	...	Read Until	Password:
	Run Keyword If	${HAS_TO_CONFIRM}	Log To Console	\n${OUTPUT}\n

	Write	${CURR_PASSWORD}
	${OUTPUT}=	Read Until	Current password:
	Log To Console	\n${OUTPUT}\n
	Should Contain	${OUTPUT}	You are required to change your password immediately (administrator enforced)
	Should Contain	${OUTPUT}	Changing password for ${USERNAME}.

	Write	${CURR_PASSWORD}
	${OUTPUT}=	Read Until	New password:
	Log To Console	\n${OUTPUT}\n

	Write	${NEW_PASSWORD}
	${OUTPUT}=	Read Until	Retype new password:
	Log To Console	\n${OUTPUT}\n

	Write	${NEW_PASSWORD}
	${OUTPUT}=	CLI:Read Until Regexp	(~#|${prompt})
	${WEBSERVER_CONNECTED}=	Run Keyword And Return Status
	...	Should Not Contain	${OUTPUT}	Error: Failed to connect to WEBSERVER
	Run Keyword If	${WEBSERVER_CONNECTED}	CLI:Write	exit
	CLI:Close Connection	Yes

CLI:Connect To Host On Peer Shell
	[Documentation]
	...	Connects to the specified host using a peer's root ssh connection
	[Arguments]	${HOST_IP_ADDRESS}=${HOST}	${PEER_IP_ADDRESS}=${HOSTPEER}
	...	${USERNAME}=${DEFAULT_USERNAME}	${PASSWORD}=${DEFAULT_PASSWORD}	${PROMPT}=]#
	...	${PEER_ROOT_PASSWORD}=${ROOT_PASSWORD}
	CLI:Connect As Root	${PEER_IP_ADDRESS}	${PEER_ROOT_PASSWORD}
	Set Client Configuration	prompt=~#
	CLI:Write	ssh-keygen -f "/home/root/.ssh/known_hosts" -R "${HOST_IP_ADDRESS}"
	Log To Console	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nConnect to ${HOST_IP_ADDRESS} on ${PEER_IP_ADDRESS} shell
	Log To Console	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n
	${OUTPUT}=	CLI:Open SSH Connection From Peer  ${USERNAME}   ${HOST_IP_ADDRESS}
	CLI:Login From Peer	${OUTPUT}	${PASSWORD}	${PROMPT}
	Log To Console  \nConnected to ${HOST_IP_ADDRESS} from ${PEER_IP_ADDRESS}\n

CLI:Disconnect From Host On Peer Shell
	[Documentation]
	...	Disconnects from the connection opened using CLI:Connect To Host From Peer Shell
	Log To Console	\n++++++++++++++++++++++++++++ Input: ++++++++++++++++++++++++++++\nDisconnect from HOST on PEER shell
	Log To Console	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++
	Set Client Configuration	prompt=#
	Write	exit
	${OUTPUT}=	Read Until prompt
	Log To Console	\n${OUTPUT}\n
	CLI:Close Connection	Yes
	Log To Console	\nDisconnected from HOST on PEER shell\n

CLI:Open SSH Connection From Peer
	[Documentation]
	...	Examples:
	...
	...	CLI:Open SSH Connection From Peer	CLI:Open SSH Connection From Peer	${USERNAME}	${HOST}
	...
	...	or	CLI:Open SSH Connection From Peer	CLI:Open SSH Connection From Peer	${USERNAME}	${HOST_IPV6}	IPV6=Yes
	[Arguments]	${USERNAME}	${HOST_DIFF}	${IPV6}=No	${INTERFACE}=eth0
	Run Keyword If	'${IPV6}' == 'No'	Write	ssh ${USERNAME}@${HOST_DIFF}
	Run Keyword If	'${IPV6}' == 'Yes'	Write	ssh -6 ${USERNAME}@${HOST_DIFF}%${INTERFACE}
	${OUTPUT}=	CLI:Read Until Regexp	(~#|Password:|Are you sure you want to continue connecting \\(.*\\)\\?)
	Should Not Contain	${OUTPUT}	No route to host
	Should Not Contain	${OUTPUT}	Connection refused
	[Return]	${OUTPUT}

CLI:Login From Peer
	[Documentation]
	...	Performs a Login from peer after calling CLI:Open SSH Connection From Peer
	[Arguments]	${CONNECTION_OUTPUT}	${PASSWORD}	${PROMPT}
	${HAS_TO_CONFIRM}=	Run Keyword And Return Status
	...	Should Match Regexp	${CONNECTION_OUTPUT}	Are you sure you want to continue connecting \\(.*\\)\\?
	Run Keyword If	${HAS_TO_CONFIRM}	Write	yes
	${OUTPUT}=	Run Keyword If	${HAS_TO_CONFIRM}
	...	Read Until	Password:
	Run Keyword If	${HAS_TO_CONFIRM}	Log To Console	\n${OUTPUT}

	Set Client Configuration	prompt=${PROMPT}
	${OUTPUT}=	CLI:Write	${PASSWORD}
	Log To Console	\n${OUTPUT}

CLI:Change Root And Admin Password To QA Default
	[Arguments]	${CURR_ROOT_PASSWORD}=${QA_PASSWORD}   ${HOST_DIFF}=${HOST}
	CLI:Connect As Root	${HOST_DIFF}	${CURR_ROOT_PASSWORD}
	Write	passwd root
	${OUTPUT}=	Read Until	New password:
	Write	${ROOT_PASSWORD}
	${OUTPUT}=	Read Until	Retype new password:
	Write	${ROOT_PASSWORD}
	${OUTPUT}=	CLI:Read Until Prompt
	Should Contain	${OUTPUT}	passwd: password updated successfully

	Write	passwd ${DEFAULT_USERNAME}
	${OUTPUT}=	Read Until	New password:
	Write	${DEFAULT_PASSWORD}
	${OUTPUT}=	Read Until	Retype new password:
	Write	${DEFAULT_PASSWORD}
	${OUTPUT}=	CLI:Read Until Prompt
	Should Contain	${OUTPUT}	passwd: password updated successfully

	CLI:Close Connection	Yes

CLI:Change Root And Admin Passwords from admin user
	[Arguments]	${CURRENT_PASSWORD}=${QA_PASSWORD}	${HOST_DIFF}=${HOST}	${NEW_PASSWORD}=${DEFAULT_PASSWORD}
	CLI:Open	PASSWORD=${CURRENT_PASSWORD}	HOST_DIFF=${HOST_DIFF}
	Set Client Configuration	prompt=#
	CLI:Write	shell sudo su -
	Set Client Configuration	timeout=120s
	Write	passwd root
	${OUTPUT}=	Read Until	New password:
	Write	${ROOT_PASSWORD}
	${OUTPUT}=	Read Until	Retype new password:
	Write	${ROOT_PASSWORD}
	${OUTPUT}=	CLI:Read Until Prompt
	Should Contain	${OUTPUT}	passwd: password updated successfully
	Write	passwd ${DEFAULT_USERNAME}
	${OUTPUT}=	Read Until	New password:
	Write	${NEW_PASSWORD}
	${OUTPUT}=	Read Until	Retype new password:
	Write	${NEW_PASSWORD}
	${OUTPUT}=	CLI:Read Until Prompt
	Should Contain	${OUTPUT}	passwd: password updated successfully
	Set Client Configuration	prompt=]#
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Write	exit
	CLI:Close Connection

CLI:Generate System Checksum
	[Arguments]	${CHECKSUM}=md5sum	${ACTION}=view
	CLI:Write	system_config_check
	CLI:Set	checksum=${CHECKSUM} action=${ACTION}
	CLI:Commit
	Set Client Configuration	timeout=120s
	Write	show
	${CHECKSUM}=	CLI:Read Until Regexp	(-- more --:)|(\\]\\#)
	${MORE_ITEMS}	Run Keyword And Return Status	Should Contain	${CHECKSUM}	-- more --:
	Run Keyword If	${MORE_ITEMS}	Write	q
	Run Keyword If	${MORE_ITEMS}	CLI:Read Until Prompt
	CLI:Write	finish
	[Return]	${CHECKSUM}

CLI:Test Ping
	[Documentation]  Test ping other ip address from current nodegrid (should be
	...	connected as admin)
	[Arguments]	${IP_ADDRESS}	${NUM_PACKETS}=10	${TIMEOUT}=60
	...	${SOURCE_INTERFACE}=${EMPTY}	${IPV6}=No	${NO_STICKY}=No	${INSTABLE_MODE}=No

	${PING_COMMAND}=	Set Variable	ping ${IP_ADDRESS} -W ${TIMEOUT} -c ${NUM_PACKETS} -B
	IF	'${NO_STICKY}' != 'No'
		${PING_COMMAND}=	Set Variable	ping ${IP_ADDRESS} -W ${TIMEOUT} -c ${NUM_PACKETS}
	END
	${PING_COMMAND}=	Run Keyword If	'${SOURCE_INTERFACE}' != '${EMPTY}'
	...	Catenate	${PING_COMMAND}	${SPACE}-I ${SOURCE_INTERFACE}
	...	ELSE	Set Variable	${PING_COMMAND}
	${PING_COMMAND}=	Run Keyword If	'${IPV6}' != 'No'
	...	Catenate	${PING_COMMAND}	${SPACE}-6
	...	ELSE	Set Variable	${PING_COMMAND}

	Set Client Configuration	prompt=#
	CLI:Write	shell sudo su -
	Set Client Configuration	timeout=120s
	${OUTPUT}=	CLI:Write	${PING_COMMAND}
	Set Client Configuration	prompt=]#
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Write	exit

	IF	'${INSTABLE_MODE}' == 'No'
		Should Contain	${OUTPUT}	${NUM_PACKETS} received
	ELSE
		Should Not Contain	${OUTPUT}	0 received
	END

CLI:Add VLAN
	[Documentation]	Add a VLAN using the desired ports
		...	Tagged ports is a comma separated string containing the port names
		...
		...	Untagged ports is a comma separated string containing the port names
	[Arguments]	${VLAN}	${TAGGED_PORTS}=${EMPTY}	${UNTAGGED_PORTS}=${EMPTY}
	CLI:Enter Path	/settings/switch_vlan
	CLI:Show
	CLI:Add
	CLI:Set	vlan=${VLAN}
	CLI:Set	tagged_ports=${TAGGED_PORTS}
	CLI:Set	untagged_ports=${UNTAGGED_PORTS}
	CLI:Commit
	CLI:Test Show Command	${VLAN}

CLI:Edit VLAN
	[Documentation]	Edit and existing VLAN using the desired ports
		...	Tagged ports is a comma separated string containing the port names
		...
		...	Untagged ports is a comma separated string containing the port names
	[Arguments]	${VLAN}	${TAGGED_PORTS}=${EMPTY}	${UNTAGGED_PORTS}=${EMPTY}
	CLI:Enter Path	/settings/switch_vlan/
	CLI:Show
	CLI:Edit	${VLAN}
	CLI:Set	tagged_ports=${TAGGED_PORTS}
	CLI:Set	untagged_ports=${UNTAGGED_PORTS}
	CLI:Commit
	CLI:Test Show Command	${VLAN}

CLI:Add Static IPv4 Network Connection
	[Arguments]	${CONNECTION}	${IP_ADDRESS}	${TYPE}=ethernet
	...	${INTERFACE}=backplane0	${BITMASK_SIZE}=24	${VLAN_ID}=${EMPTY}
	CLI:Enter Path	/settings/network_connections/
	CLI:Show
	CLI:Add
	CLI:Set	name=${CONNECTION} type=${TYPE} ethernet_interface=${INTERFACE}
	CLI:Set	ipv4_mode=static ipv4_address=${IP_ADDRESS} ipv4_bitmask=${BITMASK_SIZE}
	CLI:Set	vlan_id=${VLAN_ID}
	CLI:Commit
	CLI:Test Show Command	${CONNECTION}
	CLI:Write	up_connection ${CONNECTION}

CLI:Delete Network Connections
	[Arguments]	@{CONNECTIONS}
	CLI:Enter Path	/settings/network_connections/
	CLI:Show
	CLI:Delete If Exists	@{CONNECTIONS}
	CLI:Test Not Show Command	@{CONNECTIONS}

CLI:Enable Switch Interface
	[Arguments]	${INTERFACE}	${SPEED}=auto	${VLAN_ID}=${EMPTY}	${AUTO_NEGOTIATION}=disabled
	CLI:Enter Path	/settings/switch_interfaces/${INTERFACE}
	CLI:Set	status=enabled speed=${SPEED}
	Run Keyword If	'${AUTO_NEGOTIATION}' == 'enabled'	CLI:Set	auto-negotiation=${AUTO_NEGOTIATION}
	Run Keyword If	'${VLAN_ID}' != '${EMPTY}'	CLI:Set	port_vlan_id=${VLAN_ID}
	CLI:Commit

CLI:Disable Switch Interface
	[Arguments]	${INTERFACE}
	CLI:Enter Path	/settings/switch_interfaces/${INTERFACE}
	CLI:Set	status=disabled
	CLI:Commit

CLI:Delete VLAN
	[Arguments]	${VLAN}
	CLI:Enter Path	/settings/switch_vlan
	CLI:Show
	CLI:Delete If Exists	${VLAN}

CLI:Add Flow Exporter
	[Documentation]	Add a flow exporter (Netflow) inside path /settings/flow_exporter
	...
	...				aggreagation is a comma separated string of aggregation values
	[Arguments]	${NAME}	${COLLECTOR_ADDRESS}	${PORT}	${AGGREGATION}	${PROTOCOL}
	...	${SAMPLING_RATE}=1		${ENABLED}=yes	${INTERFACE}=eth0
	CLI:Enter Path	/settings/flow_exporter
	CLI:Add
	CLI:Set	name=${NAME} collector_address=${COLLECTOR_ADDRESS}
	CLI:Set	enabled=${ENABLED} protocol=${PROTOCOL}
	CLI:Set	collector_port=${PORT} interface=${INTERFACE}
	CLI:Set	aggregation=${AGGREGATION} sampling_rate=${SAMPLING_RATE}
	CLI:Commit

CLI:Delete All Netflow Exporters
	[Documentation]	Delete all flow exporters (Netflow) inside path
	...				/settings/flow_exporter
	CLI:Enter Path	/settings/flow_exporter
	FOR	${I}	IN RANGE	999
		${CONTAINS}=	Run Keyword And Return Status	CLI:Test Ls Command	1
		Exit For Loop If	'${CONTAINS}' == 'False'
		${SHOW_OUTPUT}=	CLI:Show
		${DEVICE_LINE}=	Split To Lines	${SHOW_OUTPUT}	2	3
		${DEVICE_LINE}=	Get From List	${DEVICE_LINE}	0
		${MATCH}	${EXPORTER_NAME}=	Should Match Regexp	${DEVICE_LINE}	(\\w+)
		CLI:Delete If Exists	${EXPORTER_NAME}
		CLI:Delete If Exists	1
	END

CLI:Get System About Dictionary
    [Documentation]     Return a dictionary with the informations on /system/about
    CLI:Enter Path	/system/about
	${ABOUT_OUTPUT}=	CLI:Show
    @{lines_list}=	Split To Lines	${ABOUT_OUTPUT}	0	-1
	&{dict}=	Create Dictionary	a	a
	FOR	${ELEMENT}	IN	@{lines_list}
		Log	Start Loop for ${ELEMENT}	INFO
		${len}=	Get Length	${ELEMENT}
		Log	Lenghth of element ${len}	INFO
		${v1}	${v2}=	Run Keyword If	${len} > 0	Split String	${ELEMENT}	:	1
		Run Keyword If	${len} > 0	Log	Length of values 2	INFO
		Run Keyword If	${len} > 0	Set To Dictionary	${dict}	'${v1}'	'${v2}'
	END
	Log Dictionary	${dict}	INFO
	[Return]    ${dict}

CLI:Test Pwd Command
    [Documentation]     Tests if the output of the pwd command contains the ${FIELD}
    [Arguments]	        ${FIELD}
    ${OUTPUT}=          CLI:Write  pwd
    Should Contain      ${OUTPUT}   ${FIELD}
    [Return]            ${OUTPUT}

CLI:Get Supported Ethernet Link Modes
	[Documentation]	Get the supported ethernet link modes for field
	...	ethernet_link_mode under /settings/network_connections/<ethernet connection>
	Set Client Configuration	prompt=#
	CLI:Write	shell sudo su -
	${OUTPUT}=	CLI:Write	ethtool eth0 | tr '\\n' ' ' | grep -oP 'Supported link modes:.*Supported pause frame use' | sed -e "s/Supported link modes://" | sed -e "s/Supported pause frame use//"	user=Yes
	Set Client Configuration	prompt=]#
	CLI:Write	exit

	${OUTPUT}=	Replace String	${OUTPUT}	Half	half
	${OUTPUT}=	Replace String	${OUTPUT}	Full	full
	${OUTPUT}=	Replace String	${OUTPUT}	/	|
	${OUTPUT}=	Replace String	${OUTPUT}	10baseT	10m
	${OUTPUT}=	Replace String	${OUTPUT}	100baseT	100m
	${OUTPUT}=	Replace String	${OUTPUT}	1000baseT	1g
	@{MODES}=	Split String	${OUTPUT}
	Append To List	${MODES}	auto
	[Return]	${MODES}

CLI:Is Interface Carrier State Up
	[Arguments]	${INTERFACE}
	Set Client Configuration	prompt=#
	CLI:Write	shell sudo su -
	${OUTPUT}=	CLI:Write	ip addr show dev ${INTERFACE} | grep -Eo 'state (UP|DOWN)' | grep -Eo '(UP|DOWN)'	user=Yes
	Set Client Configuration	prompt=]#
	CLI:Write	exit
	Return From Keyword If	'${OUTPUT}' == 'UP'	${TRUE}
	[Return]	${FALSE}

CLI:Remove
    [Documentation]
		...    Keyword remove a set of provide values from the Nodegrid at its current location
    ...    Examples:
    ...    | CLI:Remove |  nodegrid.peer |
    ...
    [Arguments]    @{VALUES}
    FOR    ${VALUE}    IN    @{VALUES}
		Continue For Loop If	'${VALUE}' == '${EMPTY}'
		CLI:Write    remove ${VALUE}
		CLI:Commit
	END

CLI:Remove If Exists
	[Documentation]
		...    Keyword Remove provides values from Nodegrid at its current location only if the values currently exist.
		...    Examples:
		...    | CLI:Enter Path    | /settings/cluster/cluster_peers |
		...    | CLI:Remove If Exists |  nodegrid.localdomain |
	[Arguments]	@{VALUES}
	${OUTPUT}=	CLI:Write	ls	user=Yes
	${OUTPUT}=	Replace String	${OUTPUT}	/	${SPACE}
	FOR	${VALUE}	IN	@{VALUES}
		${RESULT}=	Run Keyword And Return Status	Should Contain	${SPACE}${OUTPUT}${SPACE}	${SPACE}${VALUE}${SPACE}
		Run Keyword If	${RESULT}	CLI:Remove	${VALUE}
	END

CLI:Get Interface MAC Address
	[Arguments]	${INTERFACE}
	${OUTPUT}=	CLI:Show	/settings/network_connections/
	@{GET_LINES}=	Split To Lines	${OUTPUT}
	FOR	${INDEX}	${LINE}	IN ENUMERATE	@{GET_LINES}
		${STATUS}=	Run Keyword And Return Status	Should Contain	${LINE}	${INTERFACE}
		${NUMBER_LINE}=	Run Keyword If	'${STATUS}' == 'True'	Set Variable	${INDEX}
		Run Keyword If	'${STATUS}' == 'True'	Exit For Loop
	END
	${LINE}=	Get From List	${GET_LINES}	${NUMBER_LINE}
	${MAC}   @{MATCHES}=	Should Match Regexp	${LINE}	([0-9a-fA-F]{2}[:]){5}([0-9a-fA-F]{2})
	[Return]	${MAC}

CLI:Disable All Switch Interfaces
	CLI:Enter Path	/settings/switch_interfaces/
	@{INTERFACES}=	CLI:Get Switch Interfaces
	${LENGTH}=	Get Length	${INTERFACES}
	${END}=	Set Variable	${LENGTH - 2}
	FOR	${INDEX}	${INTERFACE}	IN ENUMERATE	@{INTERFACES}
		Wait Until Keyword Succeeds	3x	500ms	CLI:Disable Switch Interface	${INTERFACE}
		Run Keyword If	'${INDEX}' == '${END}'	Exit For Loop
	END

CLI:Get Switch Interfaces
	CLI:Enter Path	/settings/switch_interfaces/
	${LS}=	CLI:Write	ls	user=Yes
	${LS}=	Remove String	${LS}	${SPACE}	\t	\r	\n
	${INTERFACES}=	Split String	${LS}	/
	[Return]	${INTERFACES}

CLI:Get Interface Ipv6 Address
	[Arguments]	${INTERFACE}
	${OUTPUT}=	CLI:Show	/settings/network_connections/
	@{GET_LINES}=	Split To Lines	${OUTPUT}
	FOR	${INDEX}	${LINE}	IN ENUMERATE	@{GET_LINES}
		${STATUS}=	Run Keyword And Return Status	Should Contain	${LINE}	${INTERFACE}
		${NUMBER_LINE}=	Run Keyword If	'${STATUS}' == 'True'	Set Variable	${INDEX}
		Run Keyword If	'${STATUS}' == 'True'	Exit For Loop
	END
	${LINE}=	Get From List	${GET_LINES}	${NUMBER_LINE}
	${IPV6}   @{MATCHES}=	Should Match Regexp	${LINE}	(([0-9a-fA-F]{0,4}:){1,7}[0-9a-fA-F]{0,4})
	[Return]	${IPV6}

CLI:Edit To Default Interface Configuration
	[Arguments]	${INTERFACE}
	CLI:Enter Path	/settings/network_connections/${INTERFACE}/
	CLI:Set	set_as_primary_connection=yes connect_automatically=yes ipv4_mode=dhcp ipv6_mode=address_auto_configuration
	CLI:Commit
	[Teardown]	CLI:Cancel	Raw

CLI:Check If FTP Server Is Alive And If Exists File
	[Arguments]	${FILE}
	${STATUS}=	Run Keyword And Return Status	CLI:Open	USERNAME=root	PASSWORD=${NFSSERVERPASSWORD}	session_alias=ftpuser
	...	TYPE=root	HOST_DIFF=${FTPSERVER2_IP}
	Return From Keyword If	'${STATUS}' == '${FALSE}'	${STATUS}
	CLI:Enter Path	/home/ftpuser/ftp/files/
	${OUTPUT}=	CLI:Ls
	CLI:Close Connection
	${STATUS}=	Run Keyword And Return Status	Should Contain	${OUTPUT}	${FILE}
	[Return]	${STATUS}

CLI:Check memory and CPU usages
	Log	\n++++++++++++++++++++++++++++ Check Memory and CPU usages: ++++++++++++++++++++++++++++\n  INFO     console=yes
	Set Client Configuration	prompt=#
	CLI:Write	shell sudo su -
	# Sort by memory usage
	${OUTPUT}	CLI:Write	ps aux | sort -g -k 4 -r | head -n 1	user=Yes
	${MATCH}	${MEMORY}=	Should Match Regexp	${OUTPUT}	(?:.*?[0-9.]+){2}.*?([0-9.]+)
	${DATE}	Get Current Date	UTC
	${MEMORY_RESULT}	Set Variable	UTC Date: ${DATE}. Memory usage: ${MEMORY}%. Process details: ${OUTPUT}.\n
	Log	\n++++++++++++++++++++++++++++ Menory usage: ++++++++++++++++++++++++++++\n${MEMORY_RESULT}	INFO	console=yes
	CLI:Write	mkdir /var/jenkins_logs/	Raw
	CLI:Write	echo "${MEMORY_RESULT}" >> /var/jenkins_logs/jenkins_build_memory_usage.log
	# Sort by cpu usage
	${OUTPUT}	CLI:Write	ps aux | sort -g -k 3 -r | head -n 1	user=Yes
	${MATCH}	${CPU}=	Should Match Regexp	${OUTPUT}	(?:.*?[0-9.]+){1}.*?([0-9.]+)
	${DATE}	Get Current Date	UTC
	${CPU_RESULT}	Set Variable	UTC Date: ${DATE}. CPU usage: ${CPU}%. Process details: ${OUTPUT}.\n
	Log	\n++++++++++++++++++++++++++++ CPU usage: ++++++++++++++++++++++++++++\n${CPU_RESULT}	INFO	console=yes
	CLI:Write	echo "${CPU_RESULT}" >> /var/jenkins_logs/jenkins_build_cpu_usage.log
	Set Client Configuration	prompt=]#
	CLI:Write	exit
#	Should Be True    ${MEMORY} < 60
#	Should Be True    ${CPU} < 60

CLI:Start iPerf3 Server
	[Documentation]  Test iPerf3 server receive connection from the iperf3 client (should be connected as admin)
	...
	...	Example:	~# iperf3 -s -p 5201 -D
	...
	[Arguments]	${PORT}=5201	${DAEMON}=-D
	${IPERF3_SERVER_COMMAND}	Set Variable	iperf3 -s -p ${PORT} ${DAEMON}
	Set Client Configuration	prompt=#
	CLI:Write	shell sudo su -
	Set Client Configuration	timeout=120s
	CLI:Write	${IPERF3_SERVER_COMMAND}
	Set Client Configuration	prompt=]#
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Write	exit

CLI:Stop iPerf3 Server
	[Documentation]  Test stop iPerf3 server (should be connected as admin)
	...
	Set Client Configuration	prompt=#
	CLI:Write	shell sudo su -
	Set Client Configuration	timeout=120s
	CLI:Write	killall iperf3
	Set Client Configuration	prompt=]#
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Write	exit

CLI:Test iPerf3 Client
	[Documentation]  Test iPerf3 cliente to connect with iperf3 server from current nodegrid (should be
	...	connected as admin)
	...
	...	Example:	~# iperf3 -c iperf.par2.as49434.net -4 -p 9210 -t 5 -i 1 --bind 100.104.126.155
	...
	[Arguments]	${SERVER_IP_ADDRESS}	${IPV4ORIPV6}=-4	${PORT}=5201	${TIME}=5	${INTERVAL}=1
	...	${SOURCE_INTERFACE_IP}=${EMPTY}
	${IPERF3_CLIENT_COMMAND}=	Set Variable	iperf3 -c ${SERVER_IP_ADDRESS} ${IPV4ORIPV6} -p ${PORT} -t ${TIME} -i ${INTERVAL}
	${IPERF3_CLIENT_COMMAND}=	Run Keyword If	'${SOURCE_INTERFACE_IP}' != '${EMPTY}'
	...	Catenate	${IPERF3_CLIENT_COMMAND}	${SPACE} --bind ${SOURCE_INTERFACE_IP}	ELSE	Set Variable	${IPERF3_CLIENT_COMMAND}
	Set Client Configuration	prompt=#
	CLI:Write	shell sudo su -
	Set Client Configuration	timeout=120s
	${OUTPUT}=	CLI:Write	${IPERF3_CLIENT_COMMAND}
	Set Client Configuration	prompt=]#
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	CLI:Write	exit
	Log	\niPerf3 Client Command output: \n${OUTPUT}	INFO	console=yes
	[Return]	${OUTPUT}

CLI:Access via console and get logs information for debug
	Run Keyword If	'${CONSOLE_PORT_TTYS}' == 'FALSE' or '${CONSOLE_PORT_NAME}' == 'FALSE' or '${CONSOLE_ACCESS_IP}' == 'FALSE'
	...	Fail	NG has no access via console.
	${STATUS}	Run Keyword And Return Status	CLI:Open	USERNAME=${DEFAULT_USERNAME}:${CONSOLE_PORT_TTYS}	PASSWORD=${CONSOLE_PASSWORD}
	...	TYPE=nodegrid_device	HOST_DIFF=${CONSOLE_ACCESS_IP}
	Run Keyword If	not ${STATUS}	CLI:Open	USERNAME=${DEFAULT_USERNAME}:${CONSOLE_PORT_NAME}	PASSWORD=${CONSOLE_PASSWORD}
	...	TYPE=nodegrid_device	HOST_DIFF=${CONSOLE_ACCESS_IP}
	CLI:Wait Until Success Press Enter And Read Until Regexp And Press Enter	login:
	Write	${ROOT_PASSWORD}
	CLI:Read Until Regexp	Password:
	Write	${ROOT_PASSWORD}
	${LOGIN_OUTPUT}=	CLI:Read Until Regexp	(~#|login:)
	${LOGIN}=	Run Keyword And Return Status	Should Not Contain	${LOGIN_OUTPUT}	Login incorrect
	IF	not ${LOGIN}
		CLI:Wait Until Success Press Enter And Read Until Regexp And Press Enter	login:
		Write	${ROOT_PASSWORD}
		CLI:Read Until Regexp	Password:
		Write	${QA_PASSWORD}
		${LOGIN_OUTPUT2}=	CLI:Read Until Regexp	(~#|login:)
		${LOGIN2}=	Run Keyword And Return Status	Should Not Contain	${LOGIN_OUTPUT2}	Login incorrect
		Run Keyword If	not ${LOGIN2}	Fail	Without access via console using root account using default password also strong password.
	END
	Write	cat /var/log/dlog.log
	${DLOG_OUTPUT}=	CLI:Read Until Regexp	~#
	Log	\ndlog.log output looking for matches of date and time with some test case:\n${DLOG_OUTPUT}	INFO	console=yes
	Write	ls -la /var/coredump/
	${COREDUMP_OUTPUT}=	CLI:Read Until Regexp	~#
	Log	\ncoredump output looking for matches of date and time with some test case:\n${COREDUMP_OUTPUT}	INFO	console=yes
	CLI:Wait Until Success Press Enter And Read Until Regexp And Press Enter	~#
	Write	exit
	CLI:Wait Until Success Press Enter And Read Until Regexp And Press Enter	login:
	Write	\x05c.

CLI:Wait Until Success Press Enter And Read Until Regexp And Press Enter
	[Arguments]	${PROMPT}
	${OUTPUT}=	Wait Until Keyword Succeeds	12x	1s	CLI:Press Enter And Read Until Regexp And Press Enter	${PROMPT}
	[Return]	${OUTPUT}

CLI:Press Enter And Read Until Regexp And Press Enter
	[Arguments]	${PROMPT}
	Set Client Configuration	timeout=5s
	Write Bare	\n
	${OUTPUT}=	Read Until Regexp	${PROMPT}	loglevel=INFO
	Write Bare	\n
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Log To Console	${OUTPUT}
	[Return]	${OUTPUT}

CLI:Read Until Regexp
	[Arguments]	${PROMPT}	${TIMEOUT}=120s
	Set Client Configuration	timeout=${TIMEOUT}
	${OUTPUT}=	Read Until Regexp	${PROMPT}	loglevel=INFO
	Set Client Configuration	timeout=${CLI_DEFAULT_TIMEOUT}
	Log To Console	${OUTPUT}
	[Return]	${OUTPUT}

CLI:Is Nodegrid Version Higher
	[Documentation]	Keyword receives two nodegrid version and compare them. These version could be full versions
	...	MAJOR.MINOR.RELEASE or only MAJOR.MINOR. First it will verify which ones are full versions and split them.
	...	then some nested IF structures will compare Major version, Minor version and then Release versions in this
	...	triority. The keyword returns ${TRUE} in case ${VERSION1} is higher and ${FALSE} in case ${VERSION2} is higher.
	...	This keyword will only fail in case MAJORS and MINORS are the same in different formats (e.g. 5.8 and 5.8.3).
	...	This keyword will return EQUAL as result if both version are exactly equal (e.gs. 5.6 and 5.6 or 5.6.1 and 5.6.1)
	...	Examples:
			...	5.2 and 4.2.10 - true
			...	3.2.56 and 5.10 - false
			...	5.4 and 5.2.3 - true
			...	5.6.9 and 5.8 - false
			...	5.4.12 and 5.4.13  false
			...	5.8.2 and 5.8 - fail
	[Arguments]	${VERSION1}	${VERSION2}
	${VERSION1_IS_FULL}	Run Keyword And Return Status	Should Contain X Times	${VERSION1}	.	2
	@{VERSION_PARTS1}	Split String	${VERSION1}	.
	${MAJOR1}	Set Variable	${VERSION_PARTS1}[0]
	${MINOR1}	Set Variable	${VERSION_PARTS1}[1]
	${RELEASE1}	Set Variable If	${VERSION1_IS_FULL}	${VERSION_PARTS1}[2]	-1

	${VERSION2_IS_FULL}	Run Keyword And Return Status	Should Contain X Times	${VERSION2}	.	2
	@{VERSION_PARTS2}	Split String	${VERSION2}	.
	${MAJOR2}	Set Variable	${VERSION_PARTS2}[0]
	${MINOR2}	Set Variable	${VERSION_PARTS2}[1]
	${RELEASE2}	Set Variable If	${VERSION2_IS_FULL}	${VERSION_PARTS2}[2]	-1

	${SAME_FORMAT}	Set Variable If	${VERSION1_IS_FULL} == ${VERSION2_IS_FULL}	${TRUE}	${FALSE}

	IF	${MAJOR1} > ${MAJOR2}
		Return From Keyword	${TRUE}
	ELSE IF	${MAJOR2} > ${MAJOR1}
		Return From Keyword	${FALSE}
	ELSE
		IF	${MINOR1} > ${MINOR2}
			Return From Keyword	${TRUE}
		ELSE IF	${MINOR2} > ${MINOR1}
			Return From Keyword	${FALSE}
		ELSE
			IF	${SAME_FORMAT} and ${RELEASE1} > ${RELEASE2}
				Return From Keyword	${TRUE}
			ELSE IF	${SAME_FORMAT} and ${RELEASE2} > ${RELEASE1}
				Return From Keyword	${FALSE}
			ELSE IF	not ${SAME_FORMAT}
				Fail	Is not possible to determine which version is higher due to different formats
			ELSE
				Log	Versions are exactly the same
				Return From Keyword	EQUAL
			END
		END
	END

CLI:Which Nodegrid Version Is Higher
	[Documentation]	Keyword returns which of the two versions are higher using CLI:Is Nodegrid Verision Higher
	[Arguments]	${VERSION1}	${VERSION2}
	${FISRT_HIGHER}	Run Keyword And Return Status	CLI:Is Nodegrid Version Higher	${VERSION1}	${VERSION2}
	Return From Keyword If	${FIRST_HIGHER}	${VERSION1}	${VERSION2}