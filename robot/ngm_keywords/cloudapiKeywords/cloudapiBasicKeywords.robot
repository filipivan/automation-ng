*** Settings ***
Resource	../../ngm_tests/init.robot

*** Keywords ***

CLOUD::API::Setup HTTP Context
	[Arguments]	${API_PATH_DIFF}=${API_PATH}
	[Documentation]
	...	Setup http library used to perform the http request (You MUST use it on requests)
	...	== REQUIREMENTS ==
	...	None
	...	== ARGUMENTS ==
	...	-	API_PATH API_PATH which the request will be send
	Should Not Be Empty	${API_PATH_DIFF}	Invalid API_PATH
	#robotframework-requests
	${headers}=	CLOUD::API::Get Headers
	Create Session	api	https://${API_PATH_DIFF}	headers=${headers}	disable_warnings=1

CLOUD::API::Setup HTTP Context Formdata
	[Arguments]	${API_PATH_DIFF}=${API_PATH}
	[Documentation]
	...	Setup http library used to perform the http request (You MUST use it on requests)
	...	== REQUIREMENTS ==
	...	None
	...	== ARGUMENTS ==
	...	-	API_PATH API_PATH which the request will be send
	Should Not Be Empty	${API_PATH_DIFF}	Invalid API_PATH
	#robotframework-requests
	${headers}=	CLOUD::API::Get Headers Formdata
	Create Session	api	https://${API_PATH_DIFF}	headers=${headers}	disable_warnings=1

CLOUD::API::Get Headers
	[Arguments]	${API_PATH_DIFF}=${API_PATH}
	[Documentation]
	...	Generates a dictionary to be used as header when executing a http request
	...	== REQUIREMENTS ==
	...	None
	...	== ARGUMENTS ==
	...	-	API_PATH -	API_PATH which the request will be send
	${headers}=	Create Dictionary
	Set To Dictionary	${headers}	Origin=https://${API_PATH_DIFF}
	Set To Dictionary	${headers}	Referer=https://${API_PATH_DIFF}/
	Set To Dictionary	${headers}	Accept-Language=en-GB,en;q=0.8,en-US;q=0.6
	Set To Dictionary	${headers}	Accept-Encoding=gzip, deflate, br
	Set To Dictionary	${headers}	Accept=application/json
	Set To Dictionary	${headers}	Cache-control=no-cache
	Set To Dictionary	${headers}	Pragma=no-cache
	Set To Dictionary	${headers}	Content-Type=application/json
	[Return]	${headers}

CLOUD::API::Get Headers Formdata
	[Arguments]	${API_PATH_DIFF}=${API_PATH}
	[Documentation]
	...	Generates a dictionary to be used as header when executing a http request
	...	== REQUIREMENTS ==
	...	None
	...	== ARGUMENTS ==
	...	-	API_PATH -	API_PATH which the request will be send
	${headers}=	Create Dictionary
	Set To Dictionary	${headers}	Origin=https://${API_PATH_DIFF}
	Set To Dictionary	${headers}	Referer=https://${API_PATH_DIFF}/
	Set To Dictionary	${headers}	Accept-Language=en-GB,en;q=0.8,en-US;q=0.6
	Set To Dictionary	${headers}	Accept-Encoding=gzip, deflate, br
	Set To Dictionary	${headers}	Accept=application/json
	Set To Dictionary	${headers}	Cache-control=no-cache
	Set To Dictionary	${headers}	Pragma=no-cache
	Set To Dictionary	${headers}	Content-Type=multipart/form-data
	[Return]	${headers}

CLOUD::API::Get Ticket Header
	[Arguments]	${INDEX_TICKET}=0
	[Documentation]
	...	Generates header and adds the ticket from Suite variable ${SESSION_TICKET} on it
	...	== REQUIREMENTS ==
	...	Must be created a Session before with CLOUD::API::Post::Session
	...	== ARGUMENTS ==
	...	-	INDEX_TICKET - index to get ticket from ${SESSION_TICKET} list
	${headers}=	CLOUD::API::Get Headers
	FOR	${S}	IN	${SESSION_TICKET}
		LOG	${S}
	END
	${session}=	Get From List	${SESSION_TICKET}	${INDEX_TICKET}
	${ticket}=	Get From Dictionary	${session}	cookie
	Set To Dictionary	${headers}	cookie=${ticket}
	[Return]	${headers}

CLOUD::API::Get Ticket Header Formdata
	[Arguments]	${INDEX_TICKET}=0
	[Documentation]
	...	Generates header and adds the ticket from Suite variable ${SESSION_TICKET} on it
	...	== REQUIREMENTS ==
	...	Must be created a Session before with CLOUD::API::Post::Session
	...	== ARGUMENTS ==
	...	-	INDEX_TICKET - index to get ticket from ${SESSION_TICKET} list
	${headers}=	CLOUD::API::Get Headers Formdata
	FOR	${S}	IN	${SESSION_TICKET}
		LOG	${S}
	END
	${session}=	Get From List	${SESSION_TICKET}	${INDEX_TICKET}
	${ticket}=	Get From Dictionary	${session}	cookie
	Set To Dictionary	${headers}	cookie=${ticket}
	[Return]	${headers}

CLOUD::API::Get Ticket Header For Error
	[Arguments]	${INDEX_TICKET}=0
	[Documentation]
	...	Generates header and adds the ticket from Suite variable ${SESSION_TICKET} on it
	...	== REQUIREMENTS ==
	...	Must be created a Session before with CLOUD::API::Post::Session
	...	== ARGUMENTS ==
	...	-	INDEX_TICKET - index to get ticket from ${SESSION_TICKET} list
	${headers}=	CLOUD::API::Get Headers
	FOR	${S}	IN	${SESSION_TICKET}
		LOG	${S}
	END
	${session}=	Get From List	${SESSION_TICKET}	${INDEX_TICKET}
	${ticket}=	Get From Dictionary	${session}	cookie
	Set To Dictionary	${headers}	cookie=WRONGCREDENTIAL TEST
	[Return]	${headers}

CLOUD::API::Log Ticket List
	Log	Tickets: ${SESSION_TICKET}\n	console=yes
	[Return]	${SESSION_TICKET}

CLOUD::API::Get Ticket By Index
	[Arguments]	${INDEX_TICKET}=0
	${session}=	Get From List	${SESSION_TICKET}	${INDEX_TICKET}
	${ticket}=	Get From Dictionary	${session}	ticket
#	Set To Dictionary	${headers}	ticket=${ticket}
	[Return]	${ticket}

CLOUD::API::Get Session By Ticket
	[Arguments]	${TICKET}
	${length}=	Get Length	${SESSION_TICKET}
	:FOR	${index}	IN RANGE 0	${length}
	\	${dict}=	Get From List	${SESSION_TICKET}	${index}
	\	${ticket_from_list}=	Get From Dictionary	${dict}	ticket
	\	${session_from_list}=	Get From Dictionary	${dict}	session
	\	Exit Loop If	${ticket_from_list} == ${TICKET}
	[Return]	${session_from_list}

CLOUD::API::Send Post Request
	[Documentation]
	...	Executes a POST On Session into Cloud's API
	...	== REQUIREMENTS ==
	...	When not creating a Session -> must contain an valid ticket in ${SESSION_TICKET}, except if waiting error
	...	== ARGUMENTS ==
	...	-	PATH -	PATH from API with post support to execute request
	...	-	PAYLOAD - dictionary with payload data to send to API
	...	-	INDEX_TICKET - index to get ticket from ${SESSION_TICKET} list
	[Arguments]	${PATH}	${PAYLOAD}=${EMPTY}	${INDEX_TICKET}=0	${RAW_MODE}=no	${MODE}=yes	${ERROR_CONTROL}=${TRUE}	${EXPECT_ERROR}=False
	CLOUD::API::Setup HTTP Context
	${headers}=	Run Keyword If	'${PATH}' != '/user/auth'	CLOUD::API::Get Ticket Header	${INDEX_TICKET}
	...	ELSE	CLOUD::API::Get Headers
	Run Keyword If	'${MODE}' == 'yes'	Log	\n++++++++++++++++++++++++++++ POST - ++++++++++++++++++++++++++++\n${headers}\n${PAYLOAD}\n	INFO	console=yes
	IF	'${EXPECT_ERROR}'=='False'
		${response}=	POST on Session	api	${PATH}	json=${PAYLOAD}	headers=${headers}
	ELSE
		${response}=	Run Keyword And Expect Error	${EXPECT_ERROR}	POST on Session	api	${PATH}	json=${PAYLOAD}	headers=${headers}
	END
	Run Keyword If	'${MODE}' == 'yes' and '${EXPECT_ERROR}'=='False'	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${response.text}\n	INFO	console=yes
	#Run Keyword If	${ERROR_CONTROL}	CLOUD::API::Error Control	${response}	${RAW_MODE}
	[Return]	${response}

CLOUD::API::Send Post Request Formdata
	[Documentation]
	...	Executes a POST On Session into Cloud's API
	...	== REQUIREMENTS ==
	...	When not creating a Session -> must contain an valid ticket in ${SESSION_TICKET}, except if waiting error
	...	== ARGUMENTS ==
	...	-	PATH -	PATH from API with post support to execute request
	...	-	PAYLOAD - dictionary with payload data to send to API
	...	-	INDEX_TICKET - index to get ticket from ${SESSION_TICKET} list
	[Arguments]	${PATH}	${PAYLOAD}=${EMPTY}	${INDEX_TICKET}=0	${RAW_MODE}=no	${MODE}=yes	${ERROR_CONTROL}=${TRUE}	${EXPECT_ERROR}=False
	CLOUD::API::Setup HTTP Context Formdata
	${headers}=	Run Keyword If	'${PATH}' != '/user/auth'	CLOUD::API::Get Ticket Header Formdata	${INDEX_TICKET}
	...	ELSE	CLOUD::API::Get Headers Formdata
	Run Keyword If	'${MODE}' == 'yes'	Log	\n++++++++++++++++++++++++++++ POST - ++++++++++++++++++++++++++++\n${headers}\n${PAYLOAD}\n	INFO	console=yes
	IF	'${EXPECT_ERROR}'=='False'
		${response}=	POST on Session	api	${PATH}	json=${PAYLOAD}	headers=${headers}
	ELSE
		${response}=	Run Keyword And Expect Error	${EXPECT_ERROR}	POST on Session	api	${PATH}	json=${PAYLOAD}	headers=${headers}
	END
	Run Keyword If	'${MODE}' == 'yes' and '${EXPECT_ERROR}'=='False'	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${response.text}\n	INFO	console=yes
	#Run Keyword If	${ERROR_CONTROL}	CLOUD::API::Error Control	${response}	${RAW_MODE}
	[Return]	${response}

CLOUD::API::Send Post Request Error 400
	[Documentation]
	...	Executes a POST On Session into Cloud's API  for invalid body
	...	== REQUIREMENTS ==
	...	When not creating a Session -> must contain an valid ticket in ${SESSION_TICKET}, except if waiting error
	...	== ARGUMENTS ==
	...	-	PATH -	PATH from API with post support to execute request
	...	-	PAYLOAD - dictionary with payload data to send to API
	...	-	INDEX_TICKET - index to get ticket from ${SESSION_TICKET} list
	[Arguments]	${PATH}	${PAYLOAD}=${EMPTY}	${INDEX_TICKET}=0	${RAW_MODE}=no	${MODE}=yes	${ERROR_CONTROL}=${TRUE}	${EXPECT_ERROR}=False
	CLOUD::API::Setup HTTP Context
	${headers}=	Run Keyword If	'${PATH}' != '/user/auth'	CLOUD::API::Get Ticket Header	${INDEX_TICKET}
	...	ELSE	CLOUD::API::Get Headers
	Run Keyword If	'${MODE}' == 'yes'	Log	\n++++++++++++++++++++++++++++ POST - ++++++++++++++++++++++++++++\n${headers}\n${PAYLOAD}\n	INFO	console=yes
	IF	'${EXPECT_ERROR}'=='False'
		${response}=	POST on Session	api	${PATH}	json=${PAYLOAD}	headers=${headers}   expected_status=400
	ELSE
		${response}=	Run Keyword And Expect Error	${EXPECT_ERROR}	POST on Session	api	${PATH}	json=${PAYLOAD}	headers=${headers}   expected_status=400
	END
	Run Keyword If	'${MODE}' == 'yes' and '${EXPECT_ERROR}'=='True'	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${response.text}\n	INFO	console=yes
	[Return]	${response}

CLOUD::API::Send Post Request Error 403
	[Documentation]
	...	Executes a POST On Session into Cloud's API  by giving wrong credentials
	...	== REQUIREMENTS ==
	...	When not creating a Session -> must contain an valid ticket in ${SESSION_TICKET}, except if waiting error
	...	== ARGUMENTS ==
	...	-	PATH -	PATH from API with post support to execute request
	...	-	PAYLOAD - dictionary with payload data to send to API
	...	-	INDEX_TICKET - index to get ticket from ${SESSION_TICKET} list
	[Arguments]	${PATH}	${PAYLOAD}=${EMPTY}	${INDEX_TICKET}=0	${RAW_MODE}=no	${MODE}=yes	${ERROR_CONTROL}=${TRUE}	${EXPECT_ERROR}=False
	CLOUD::API::Setup HTTP Context
	${headers}=	CLOUD::API::Get Ticket Header For Error 	${INDEX_TICKET}
	Run Keyword If	'${MODE}' == 'yes'	Log	\n++++++++++++++++++++++++++++ POST - ++++++++++++++++++++++++++++\n${headers}\n${PAYLOAD}\n	INFO	console=yes
	IF	'${EXPECT_ERROR}'=='False'
		${response}=	POST on Session	api	${PATH}	json=${PAYLOAD}	headers=${headers}  expected_status=403
	ELSE
		${response}=	Run Keyword And Expect Error	${EXPECT_ERROR}	POST on Session	api	${PATH}	json=${PAYLOAD}	headers=${headers}  expected_status=403
	END
	Run Keyword If	'${MODE}' == 'yes' and '${EXPECT_ERROR}'=='True'	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${response.text}\n	INFO	console=yes
	[Return]	${response}

CLOUD::API::Send Post Request Error 404
	[Documentation]
	...	Executes a POST On Session into Cloud's API  for wrong endpoint
	...	== REQUIREMENTS ==
	...	When not creating a Session -> must contain an valid ticket in ${SESSION_TICKET}, except if waiting error
	...	== ARGUMENTS ==
	...	-	PATH -	PATH from API with post support to execute request
	...	-	PAYLOAD - dictionary with payload data to send to API
	...	-	INDEX_TICKET - index to get ticket from ${SESSION_TICKET} list
	[Arguments]	${PATH}	${PAYLOAD}=${EMPTY}	${INDEX_TICKET}=0	${RAW_MODE}=no	${MODE}=yes	${ERROR_CONTROL}=${TRUE}	${EXPECT_ERROR}=False
	CLOUD::API::Setup HTTP Context
	${headers}=	Run Keyword If	'${PATH}' != '/user/auth'	CLOUD::API::Get Ticket Header	${INDEX_TICKET}
	...	ELSE	CLOUD::API::Get Headers
	Run Keyword If	'${MODE}' == 'yes'	Log	\n++++++++++++++++++++++++++++ POST - ++++++++++++++++++++++++++++\n${headers}\n${PAYLOAD}\n	INFO	console=yes
	IF	'${EXPECT_ERROR}'=='False'
		${response}=	POST on Session	api	${PATH}	json=${PAYLOAD}	headers=${headers}   expected_status=404
	ELSE
		${response}=	Run Keyword And Expect Error	${EXPECT_ERROR}	POST on Session	api	${PATH}	json=${PAYLOAD}	headers=${headers}   expected_status=404
	END
	Run Keyword If	'${MODE}' == 'yes' and '${EXPECT_ERROR}'=='True'	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${response.text}\n	INFO	console=yes
	[Return]	${response}

CLOUD::API::Send Get Request
	[Arguments]	${PATH}	${INDEX_TICKET}=0	${RAW_MODE}=no	${MODE}=yes	${PAYLOAD}=${EMPTY}	${ERROR_CONTROL}=${TRUE}	${EXPECT_ERROR}=False
	[Documentation]
	...	Executes a GET request into Cloud's API
	...	== REQUIREMENTS ==
	...	Must contain an valid ticket in ${SESSION_TICKET}, except if waiting error
	...	== ARGUMENTS ==
	...	-	PATH -	PATH from API with get support to execute request
	...	-	INDEX_TICKET - index to get ticket from ${SESSION_TICKET} list
	CLOUD::API::Setup HTTP Context
	${headers}=	CLOUD::API::Get Ticket Header	${INDEX_TICKET}
	Run Keyword If	'${MODE}' == 'yes' and '${EXPECT_ERROR}'=='False'	Log	\n++++++++++++++++++++++++++++ GET - ${PATH} ++++++++++++++++++++++++++++\n${headers}\n	INFO	console=yes
	IF	"${PAYLOAD}" == "${EMPTY}"
		IF	'${EXPECT_ERROR}'=='False'
			${response}=	GET on Session	api	/${PATH}	headers=${headers}
		ELSE
			${response}=	Run Keyword And Expect Error	${EXPECT_ERROR}	GET on Session	api	/${PATH}	headers=${headers}
		END
	ELSE
		IF	'${EXPECT_ERROR}'=='False'
			${response}=	GET on Session	api	/${PATH}	headers=${headers}	json=${PAYLOAD}
		ELSE
			${response}=	Run Keyword And Expect Error	${EXPECT_ERROR}	GET on Session	api	/${PATH}	headers=${headers}	json=${PAYLOAD}	headers=${headers}
		END
	END
	Run Keyword If	'${MODE}' == 'yes' and '${EXPECT_ERROR}'=='False'	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${response.text}\n	INFO	console=yes
	#Run Keyword If	${ERROR_CONTROL}	CLOUD::API::Error Control	${response}	${RAW_MODE}
	[Return]	${response}

CLOUD::API::Send Get Request Error 404
	[Arguments]	${PATH}	${INDEX_TICKET}=0	${RAW_MODE}=no	${MODE}=yes	${PAYLOAD}=${EMPTY}	${ERROR_CONTROL}=${TRUE}	${EXPECT_ERROR}=False
	[Documentation]
	...	Executes a GET Error request 404 into Cloud's API by giving wrong Endpoint
	...	== REQUIREMENTS ==
	...	Must contain an valid ticket in ${SESSION_TICKET}, except if waiting error
	...	== ARGUMENTS ==
	...	-	PATH -	PATH from API with get support to execute request
	...	-	INDEX_TICKET - index to get ticket from ${SESSION_TICKET} list
	CLOUD::API::Setup HTTP Context
	${headers}=	CLOUD::API::Get Ticket Header	${INDEX_TICKET}
	Run Keyword If	'${MODE}' == 'yes' and '${EXPECT_ERROR}'=='False'	Log	\n++++++++++++++++++++++++++++ GET - ${PATH} ++++++++++++++++++++++++++++\n${headers}\n	INFO	console=yes
	IF	"${PAYLOAD}" == "${EMPTY}"
		${response}=	GET on Session	api	/${PATH}	headers=${headers}      expected_status=404

	ELSE
		${response}=	GET on Session	api	/${PATH}	headers=${headers}	json=${PAYLOAD}     expected_status=404

	END
	Run Keyword If	'${MODE}' == 'yes' and '${EXPECT_ERROR}'=='True'	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${response.text}\n	INFO	console=yes
	#Run Keyword If	${ERROR_CONTROL}	CLOUD::API::Error Control	${response}	${RAW_MODE}
	[Return]	${response}

CLOUD::API::Send Get Request Error 403
	[Arguments]	${PATH}	${INDEX_TICKET}=0	${RAW_MODE}=no	${MODE}=yes	${PAYLOAD}=${EMPTY}	${ERROR_CONTROL}=${TRUE}	${EXPECT_ERROR}=False
	[Documentation]
	...	Executes a GET Error request into Cloud's API by giving wrong credentials
	...	== REQUIREMENTS ==
	...	Must contain an valid ticket in ${SESSION_TICKET}, except if waiting error
	...	== ARGUMENTS ==
	...	-	PATH -	PATH from API with get support to execute request
	...	-	INDEX_TICKET - index to get ticket from ${SESSION_TICKET} list
	CLOUD::API::Setup HTTP Context
	${headers}=	CLOUD::API::Get Ticket Header For Error	${INDEX_TICKET}
	Run Keyword If	'${MODE}' == 'yes' and '${EXPECT_ERROR}'=='False'	Log	\n++++++++++++++++++++++++++++ GET - ${PATH} ++++++++++++++++++++++++++++\n${headers}\n	INFO	console=yes
	IF	"${PAYLOAD}" == "${EMPTY}"
		${response}=	GET on Session	api	/${PATH}	headers=${headers}      expected_status=403

	ELSE
		${response}=	GET on Session	api	/${PATH}	headers=${headers}	json=${PAYLOAD}     expected_status=403

	END
	Run Keyword If	'${MODE}' == 'yes' and '${EXPECT_ERROR}'=='True'	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${response.text}\n	INFO	console=yes
	[Return]	${response}

CLOUD::API::Send Put Request
	[Arguments]	${PATH}	${PAYLOAD}=${EMPTY}	${INDEX_TICKET}=0	${RAW_MODE}=no	${MODE}=yes	${ERROR_CONTROL}=${TRUE}	${EXPECT_ERROR}=False
	[Documentation]
	...	Executes a PUT request into Cloud's API
	...	== REQUIREMENTS ==
	...	Must contain an valid ticket in ${SESSION_TICKET}, except if waiting error
	...	== ARGUMENTS ==
	...	-	PATH -	PATH from API with put support to execute request
	...	-	PAYLOAD - dictionary with payload data to send to API
	...	-	INDEX_TICKET - index to get ticket from ${SESSION_TICKET} list

	CLOUD::API::Setup HTTP Context
	${headers}=	CLOUD::API::Get Ticket Header	${INDEX_TICKET}
	Run Keyword If	'${MODE}' == 'yes' and '${EXPECT_ERROR}'=='False'	Log	\n++++++++++++++++++++++++++++ PUT - ${PATH} ++++++++++++++++++++++++++++\n${headers}\n${PAYLOAD}\n	INFO	console=yes
	IF	'${EXPECT_ERROR}'=='False'
		${response}=	PUT on Session	api	${PATH}	json=${PAYLOAD}	headers=${headers}
	ELSE
		${response}=	Run Keyword And Expect Error	${EXPECT_ERROR}	PUT on Session	api	${PATH}	json=${PAYLOAD}	headers=${headers}
	END
	Run Keyword If	'${MODE}' == 'yes' and '${EXPECT_ERROR}'=='False'	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${response.text}\n	INFO	console=yes
	#Run Keyword If	${ERROR_CONTROL}	CLOUD::API::Error Control	${response}	${RAW_MODE}
	[Return]	${response}

CLOUD::API::Send Put Request Error 400
	[Arguments]	${PATH}	${PAYLOAD}=${EMPTY}	${INDEX_TICKET}=0	${RAW_MODE}=no	${MODE}=yes	${ERROR_CONTROL}=False	${EXPECT_ERROR}=False
	[Documentation]
	...	Executes a PUT request into Cloud's API with Error 400 for invalid body
	...	== REQUIREMENTS ==
	...	Must contain an valid ticket in ${SESSION_TICKET}, except if waiting error
	...	== ARGUMENTS ==
	...	-	PATH -	PATH from API with put support to execute request
	...	-	PAYLOAD - dictionary with payload data to send to API with malformed info
	...	-	INDEX_TICKET - index to get ticket from ${SESSION_TICKET} list

	CLOUD::API::Setup HTTP Context
	${headers}=	CLOUD::API::Get Ticket Header	${INDEX_TICKET}
	Run Keyword If	'${MODE}' == 'yes' and '${EXPECT_ERROR}'=='True'	Log	\n++++++++++++++++++++++++++++ PUT - ${PATH} ++++++++++++++++++++++++++++\n${headers}\n${PAYLOAD}\n	INFO	console=yes
	IF	'${EXPECT_ERROR}'=='False'
		${response}=	PUT on Session	api	${PATH}	json=${PAYLOAD}	headers=${headers}  expected_status=400
	ELSE
		${response}=	Run Keyword And Expect Error	${EXPECT_ERROR}	PUT on Session	api	${PATH}	json=${PAYLOAD}	headers=${headers}  expected_status=400
	END
	Run Keyword If	'${MODE}' == 'yes' and '${EXPECT_ERROR}'=='True'	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${response.text}\n	INFO	console=yes
	#Run Keyword If	${ERROR_CONTROL}	CLOUD::API::Error Control	${response}	${RAW_MODE}
	[Return]	${response}

CLOUD::API::Send Put Request Error 403
	[Arguments]	${PATH}	${PAYLOAD}=${EMPTY}	${INDEX_TICKET}=0	${RAW_MODE}=no	${MODE}=yes	${ERROR_CONTROL}=False	${EXPECT_ERROR}=False
	[Documentation]
	...	Executes a PUT request into Cloud's API with Error 403 by giving wrong credentials
	...	== REQUIREMENTS ==
	...	Must contain an valid ticket in ${SESSION_TICKET}, except if waiting error
	...	== ARGUMENTS ==
	...	-	PATH -	PATH from API with put support to execute request
	...	-	PAYLOAD - dictionary with payload data to send to API with malformed info
	...	-	INDEX_TICKET - index to get ticket from ${SESSION_TICKET} list

	CLOUD::API::Setup HTTP Context
	${headers}=	CLOUD::API::Get Ticket Header For Error 	${INDEX_TICKET}
	Run Keyword If	'${MODE}' == 'yes' and '${EXPECT_ERROR}'=='True'	Log	\n++++++++++++++++++++++++++++ PUT - ${PATH} ++++++++++++++++++++++++++++\n${headers}\n${PAYLOAD}\n	INFO	console=yes
	IF	'${EXPECT_ERROR}'=='False'
		${response}=	PUT on Session	api	${PATH}	json=${PAYLOAD}	headers=${headers}  expected_status=403
	ELSE
		${response}=	Run Keyword And Expect Error	${EXPECT_ERROR}	PUT on Session	api	${PATH}	json=${PAYLOAD}	headers=${headers}  expected_status=403
	END
	Run Keyword If	'${MODE}' == 'yes' and '${EXPECT_ERROR}'=='True'	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${response.text}\n	INFO	console=yes
	#Run Keyword If	${ERROR_CONTROL}	CLOUD::API::Error Control	${response}	${RAW_MODE}
	[Return]	${response}

CLOUD::API::Send Put Request Error 404
	[Arguments]	${PATH}	${PAYLOAD}=${EMPTY}	${INDEX_TICKET}=0	${RAW_MODE}=no	${MODE}=yes	${ERROR_CONTROL}=False	${EXPECT_ERROR}=False
	[Documentation]
	...	Executes a PUT request into Cloud's API with Error 404 by giving wrong Endpoint
	...	== REQUIREMENTS ==
	...	Must contain an valid ticket in ${SESSION_TICKET}, except if waiting error
	...	== ARGUMENTS ==
	...	-	PATH -	PATH from API with put support to execute request
	...	-	PAYLOAD - dictionary with payload data to send to API with malformed info
	...	-	INDEX_TICKET - index to get ticket from ${SESSION_TICKET} list

	CLOUD::API::Setup HTTP Context
	${headers}=	CLOUD::API::Get Ticket Header For Error 	${INDEX_TICKET}
	Run Keyword If	'${MODE}' == 'yes' and '${EXPECT_ERROR}'=='True'	Log	\n++++++++++++++++++++++++++++ PUT - ${PATH} ++++++++++++++++++++++++++++\n${headers}\n${PAYLOAD}\n	INFO	console=yes
	IF	'${EXPECT_ERROR}'=='False'
		${response}=	PUT on Session	api	${PATH}	json=${PAYLOAD}	headers=${headers}  expected_status=404
	ELSE
		${response}=	Run Keyword And Expect Error	${EXPECT_ERROR}	PUT on Session	api	${PATH}	json=${PAYLOAD}	headers=${headers}  expected_status=404
	END
	Run Keyword If	'${MODE}' == 'yes' and '${EXPECT_ERROR}'=='True'	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${response.text}\n	INFO	console=yes
	[Return]	${response}

CLOUD::API::Send Delete Request
	[Arguments]	${PATH}	${PAYLOAD}=${EMPTY}	${INDEX_TICKET}=0	${RAW_MODE}=no	${MODE}=yes
	[Documentation]
	...	Executes a POST request into Cloud's API
	...	== REQUIREMENTS ==
	...	Must contain an valid ticket in ${SESSION_TICKET}, except if waiting error
	...	== ARGUMENTS ==
	...	-	PATH -	PATH from API with delete support to execute request
	...	-	PAYLOAD - dictionary with payload data to send to API
	...	-	INDEX_TICKET - index to get ticket from ${SESSION_TICKET} list
	CLOUD::API::Setup HTTP Context
	${headers}=	CLOUD::API::Get Ticket Header	${INDEX_TICKET}
	Run Keyword If	'${MODE}' == 'yes'	Log	\n+++++++++++++++++++++++++++ DELETE - ${PATH} +++++++++++++++++++++++++++\n${headers}\n${PAYLOAD}\n	INFO	console=yes
	${response}=	DELETE on Session	api	${PATH}	json=${PAYLOAD}	headers=${headers}
	Run Keyword If	'${MODE}' == 'yes'	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${response.text}\n	INFO	console=yes
	#CLOUD::API::Error Control	${response}	${RAW_MODE}
	[Return]	${response}

CLOUD::API::Send Delete Request session
	[Arguments]	${PATH}	${PAYLOAD}=${EMPTY}	${INDEX_TICKET}=0	${RAW_MODE}=no	${MODE}=yes
	[Documentation]
	...	Executes a POST request into Cloud's API
	...	== REQUIREMENTS ==
	...	Must contain an valid ticket in ${SESSION_TICKET}, except if waiting error
	...	== ARGUMENTS ==
	...	-	PATH -	PATH from API with delete support to execute request
	...	-	PAYLOAD - dictionary with payload data to send to API
	...	-	INDEX_TICKET - index to get ticket from ${SESSION_TICKET} list
	CLOUD::API::Setup HTTP Context
	${headers}=	CLOUD::API::Get Ticket Header	${INDEX_TICKET}
	Run Keyword If	'${MODE}' == 'yes'	Log	\n+++++++++++++++++++++++++++ POST - ${PATH} +++++++++++++++++++++++++++\n${headers}\n${PAYLOAD}\n	INFO	console=yes
	${response}=	POST on Session	api	${PATH}	data=${PAYLOAD}	headers=${headers}
	Run Keyword If	'${MODE}' == 'yes'	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${response.text}\n	INFO	console=yes
	CLOUD::API::Error Control	${response}	${RAW_MODE}
	[Return]	${response}

CLOUD::API::Check Body Request
	[Documentation]
	...	Executes a Get On Session into Cloud's API
	...	== ARGUMENTS ==
	...	-	PATH -	PATH from API with post support to execute request
	...	-	BODY_EXPECTED - a json script with the expected result
	...	== CALL EXAMPLE  ==
	...	{BODY}=	Evaluate   str({'id': '{SUBNET}/${NETMASK}', 'leasetime': '{LEASETIME}''})
	...	CLOUD::API::Check Body Request  PATH=/network/dhcp	BODY_EXPECTED={BODY}
	[Arguments]	${PATH}	${BODY_EXPECTED}	${CONTAIN}=true

 	${response}=   CLOUD::API::Send Get Request  ${PATH}
  ${BODY}=   Evaluate   str(${response.json()})

	IF    '${CONTAIN}' == 'true'
			Should Contain	${BODY}    ${BODY_EXPECTED}
	ELSE
			Should Not Contain    ${BODY}    ${BODY_EXPECTED}
	END

CLOUD::API::Set IDLE Timeout
	[Arguments]	${TIMEOUT_VALUE}=300
	${PAYLOAD}=	Set variable	{"idle_timeout":"${TIMEOUT_VALUE}"}

	${response}=    CLOUD::API::Send Put Request    PAYLOAD=${PAYLOAD}    PATH=/system/preferences
	Should Be Equal   ${response.status_code}   ${200}

CLOUD::API::Error Control
	[Arguments]	${RESPONSE}	${RAW_MODE}=no	${ERROR}=Error on page. Please check.
	[Documentation]
	...	Checks requests' response and fails tests when returns error messages, except for RAW_MODE=yes
	...	== REQUIREMENTS ==
	...	Must contain an valid ticket in ${SESSION_TICKET}, except if waiting error
	...	== ARGUMENTS ==
	...	-	PATH -	PATH from API with delete support to execute request
	...	-	PAYLOAD - dictionary with payload data to send to API
	...	-	INDEX_TICKET - index to get ticket from ${SESSION_TICKET} list
	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${response.text}\n	INFO	console=no
	#	CASE RETURN IS NOT JSON
	#		Example: ERROR IN CONNECTION
	#	response = <html>...503 Service Unavailable...<html>\n
	${CONTAIN_JSON}=	Run Keyword and Return Status	Set Variable	${RESPONSE.json()}
	Run Keyword If	${CONTAIN_JSON} == ${FALSE}	 Fatal Error	${RESPONSE}

	#	CASE RETURN IS AN EMPTY ARRAY
	#		Example: AFTER RESET DISCOVERY LOGS
	#	response = []
	${length}=	Get Length	${RESPONSE.json()}
	${is_array_empty}=	Run Keyword And Return Status	Should Be Equal as Integers	${length}	0

	#	CASE RETURN CONTAIN MESSAGE DIRECT IN DICT
	#		EXAMPLE: AFTER PASSING AN INVALID TICKET
	#			OR
	#		TRYING TO LOGIN WITH WRONG CREDENTIALS
	#	response = { ... , message: "xxx" }
	${contain_dict_message}=	Run Keyword and Return Status	Get From Dictionary	${RESPONSE.json()}	message

	#	CASE RETURN CONTAIN MESSAGE IN LIST
	#		EXAMPLE: AFTER PASSING AN INVALID VALUE TO FIELDS
	#	response = [ { ... , message: "xxx" } ] OR [ {... , message: "xxx" }, {...}, ...]
	${contain_list}=	Run Keyword and Return Status	Get From List	${RESPONSE.json()}	0
	${dict_list_message}=	Run Keyword If	${contain_list}	Get From List	${RESPONSE.json()}	0
	${contain_list_message}=	Run Keyword and Return Status	Get From Dictionary	${dict_list_message}	message

	${message}=	Run Keyword If	${contain_dict_message}	Get From Dictionary	${RESPONSE.json()}	message
	...	ELSE	Run Keyword If	${contain_list_message}	Get From Dictionary	${dict_list_message}	message

	Run Keyword If	'${RAW_MODE}'=='no'	Should Not Be Equal	'${ERROR}'	'${message}'
	[Return]	${RESPONSE}
