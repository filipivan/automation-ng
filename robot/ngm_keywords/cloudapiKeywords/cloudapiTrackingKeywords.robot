*** Keywords ***
CLOUD::API::Get::Tracking::Open Sessions
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get open sessions info using the cloud API
	...	== REQUIREMENTS ==
	...	A valid session create by CLOUD::API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from cloud
	${response}=	CLOUD::API::Send Get Request	sessions/cloud	MODE=${MODE}
	[Return]	${response}

CLOUD::API::Post::Tracking::Open Sessions::Terminate
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Terminate open sessions using the cloud API
	...	== REQUIREMENTS ==
	...	A valid session create by CLOUD::API::Authenticate
	...	== EXPECTED RESULT ==
	...	Terminate open session on ${PAYLOAD} from NG
	${response}=	CLOUD::API::Send Post Request	sessions/cloud/terminate	${PAYLOAD}	MODE=${MODE}
	${LENGTH}=	Get Length	${SESSION_TICKET}
	${LIST}=	Create List
	:FOR	${INDEX}	IN RANGE	${LENGTH}
	\	${SESSION}=	Get From List	${SESSION_TICKET}	${INDEX}
	\	${ID}=	Get From Dictionary	${SESSION}	id
	\	Append to List	${LIST}	${ID}
	:FOR	${session}	IN	@{PAYLOAD["sessions"]}
	\	${ID}=	Get Index From List	${LIST}	${session}
	\	Run Keyword If	${ID} != -1	Remove From List	${SESSION_TICKET}	${ID}
	[Return]	${response}
