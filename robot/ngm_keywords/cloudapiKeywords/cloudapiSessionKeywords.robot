*** Keywords ***
CLOUD::API::Post::Session
	[Arguments]	${email}=${EMAIL}	${PASSWORD}=${PASSWORD}	${RAW_MODE}=no	${MODE}=yes	${SESSION}=False
	[Documentation]
	...	Authenticate using credentials into Cloud's API and add ticket to ${SESSION_TICKET}
	...	== REQUIREMENTS ==
	...	None
	...	== ARGUMENTS ==
	...	-	Email Email I'd used in the authentication
	...	-	PASSWORD Username's password used in the authentication
	...	== EXPECTED RESULT ==
	...	Login successfully in the API
	${payload}=	Create Dictionary
	IF	${SESSION}!=False
		${payload}	Set Variable	${SESSION}
	END
	Set To Dictionary	${payload}	email=${EMAIL}
	Set To Dictionary	${payload}	password=${PASSWORD}
	${response}=	CLOUD::API::Send Post Request	/user/auth	${payload}	RAW_MODE=${RAW_MODE}	MODE=${MODE}
	Should Be Equal   ${response.status_code}   ${200}

    #Fetching session id and xsrf-token from the response header
    ${contain_session}=	Run Keyword And Return Status	Get From Dictionary	${response.headers}	set-cookie
	${seperated_session}=	Run Keyword If	${contain_session}	Get From Dictionary	${response.headers}	set-cookie
    @{split_session} =  Split String     ${seperated_session}
    ${session_xsrf} =    Get From List    ${split_session}  0
    ${session_sess} =    Get From List    ${split_session}  12
    ${session} =      Catenate     ${session_xsrf}  ${session_sess}
    #${session} =    Get Lines Matching Regexp  ${session_1}  (?<==).*?(?=;)
    ${session}=  Set Suite Variable     ${session}

    #Fetch company id from the response
    ${session_body}=	Run Keyword If	${contain_session}	Get From Dictionary	${response.json()}    company
    ${company_id}=	    Get From Dictionary  	${session_body}    id
    ${company_id}=  Set Suite Variable     ${company_id}


	Return From Keyword If	'${RAW_MODE}'!='no'	${response}
	CLOUD::API::Set Suite Ticket	${response.json()}

	[Return]	${response}

CLOUD::API::Post::Session::Formdata
	[Arguments]	${email}=${EMAIL}	${PASSWORD}=${PASSWORD}	${RAW_MODE}=no	${MODE}=yes	${SESSION}=False
	[Documentation]
	...	Authenticate using credentials into Cloud's API and add ticket to ${SESSION_TICKET}
	...	== REQUIREMENTS ==
	...	None
	...	== ARGUMENTS ==
	...	-	Email Email I'd used in the authentication
	...	-	PASSWORD Username's password used in the authentication
	...	== EXPECTED RESULT ==
	...	Login successfully in the API
	${payload}=	Create Dictionary
	IF	${SESSION}!=False
		${payload}	Set Variable	${SESSION}
	END
	Set To Dictionary	${payload}	email=${EMAIL}
	Set To Dictionary	${payload}	password=${PASSWORD}
	${response}=	CLOUD::API::Send Post Request Formdata	/user/auth	${payload}	RAW_MODE=${RAW_MODE}	MODE=${MODE}
	Should Be Equal   ${response.status_code}   ${200}

    #Fetching session id and xsrf-token from the response header
    ${contain_session}=	Run Keyword And Return Status	Get From Dictionary	${response.headers}	set-cookie
	${seperated_session}=	Run Keyword If	${contain_session}	Get From Dictionary	${response.headers}	set-cookie
    @{split_session} =  Split String     ${seperated_session}
    ${session_xsrf} =    Get From List    ${split_session}  0
    ${session_sess} =    Get From List    ${split_session}  12
    ${session} =      Catenate     ${session_xsrf}  ${session_sess}
    #${session} =    Get Lines Matching Regexp  ${session_1}  (?<==).*?(?=;)
    ${session}=  Set Suite Variable     ${session}

	Return From Keyword If	'${RAW_MODE}'!='no'	${response}
	CLOUD::API::Set Suite Ticket	${response.json()}

	[Return]	${response}

CLOUD::API::Set Suite Ticket
	[Arguments]	${response.headers}

	#	ONLY CREATING DICT BEFORE TO USE {TICKET} IN CLOUD::API::Get::Tracking::Open Sessions AND GET ID
	${dict}=	Create Dictionary
	Set To Dictionary	${dict}	cookie=${session}
	${session_list}=	Create List	${dict}

	${status}=	Run Keyword And Return Status	Variable Should Exist	${SESSION_TICKET}
	Set Suite Variable	${SESSION_TICKET}	${session_list}

	#	USING TICKET HERE AND GETTING ID
	${all_sessions}=	CLOUD::API::Get::Tracking::Open Sessions	MODE=no
	#${last_session}=	Get From List	${all_sessions.json()}	-1
	#${last_session_id}=	Get From Dictionary	${last_session}	id

	#Run Keyword If	${status} == ${FALSE}	Remove From List	${SESSION_TICKET}	0
	#Set To Dictionary	${dict}	ticket=${session}
	#Set To Dictionary	${dict}	id=${last_session_id}
	#Append to List	${SESSION_TICKET}	${dict}


CLOUD::API::Delete::Session
	[Arguments]	${INDEX_TICKET}=0	${RAW_MODE}=no	${MODE}=yes
	[Documentation]
	...	Destroy the session previously created by CLOUD::API::Authenticate
	...	== REQUIREMENTS ==
	...	A valid session create by CLOUD::API::Authenticate
	...	== EXPECTED RESULT ==
	...	Destroy the session set in the "ticket" header
	${response}=	CLOUD::API::Send Delete Request session	/user/logout	INDEX_TICKET=${INDEX_TICKET}	RAW_MODE=${RAW_MODE}	MODE=${MODE}
	Remove From List	${SESSION_TICKET}	${INDEX_TICKET}
	[Return]	${response}