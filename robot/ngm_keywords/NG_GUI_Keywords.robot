*** Keywords ***

GUI::Network::Connection::Create an Connection
	[Arguments]		${CONNECTION_NAME}		${CONNECTION_TYPE}
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible		//*[@id="connName"]		20s
	Input Text	//*[@id="connName"]	${CONNECTION_NAME}
	Select From List By value		//*[@id="connType"]		${CONNECTION_TYPE}
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible
	Sleep	3s
	Page Should Contain		${CONNECTION_NAME}


To Delete Network Connection If Exists
	[Arguments]	${CONNECTION_NAME}
	[Documentation]	Delete table rows which contains the given value
	${EXISTS}=  Run Keyword And Return Status  Page Should Contain Element  xpath=//tr[@id='${CONNECTION_NAME}']/td/input

	IF  ${EXISTS} == ${TRUE}
	    Select Checkbox		xpath=//tr[@id='${CONNECTION_NAME}']/td/input
	    Wait Until Element Is Enabled		xpath=//input[@id='delButton']
        Click Element		xpath=//input[@id='delButton']
        GUI::Basic::Spinner Should Be Invisible
        sleep		5s
        Page Should Not Contain		${CONNECTION_NAME}
	END
