
class ngmGUIAccessKeywords(object):
    def __init__(self):
        self.console_types = {
            ']#': 'cli',
            '#': 'root',
            '$': 'shell'
        }

    def output_from_last_command(self, console_content):
        lines = console_content.split('\n')
        last_command_line_index = self._last_command_line_index(lines)
        output = ''
        for i in range(last_command_line_index + 1, len(lines)):
            line = lines[i]
            output += line + '\n'
            if self._is_command(line):  # Line after output
                break
        return output

    def _last_command_line_index(self, lines):
        for i, line in enumerate(reversed(lines), 1):
            for console_type in self.console_types.keys():
                if console_type in line and not line.endswith(console_type):
                    return len(lines) - i

    def _is_command(self, line):
        for console_type in self.console_types.keys():
            if console_type in line:
                return True
        return False