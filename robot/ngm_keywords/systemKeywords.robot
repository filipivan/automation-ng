*** Keywords ***
CLIENT:Get Operating System
	${OS}=	Run Keyword	CLIENT:Get Windows Version
	${OSLEN}=	Get Length	${OS}
	Run Keyword If	${OSLEN} > 0
	...	Return From Keyword	${OS}

	${OS}=	Run Keyword And Ignore Error	CLIENT:Get MacOS Version
	${OSVALUE}=	Get From List	${OS}	1
	${OSLEN}=	Get Length	${OSVALUE}
	Run Keyword If	${OSLEN} > 0
	...	Return From Keyword	${OS}

	${OS}=	Run Keyword And Ignore Error	CLIENT:Get Linux Version
	${OSVALUE}=	Get From List	${OS}	1
	${OSLEN}=	Get Length	${OSVALUE}
	Run Keyword If	${OSLEN} > 0
	...	Return From Keyword	${OS}

	Log	Client OS:\n ${OS}	INFO	console=yes
	[return]	${OS}

CLIENT:Get Operating System No Version
	${OS}=	CLIENT:Get Operating System
	${OS}=	Get From List	${OS}	1
	${OS}=	Get From List	${OS}	0
	${OS_LINES}=	Split String	${OS}	${SPACE}
	${OS}=	Get From List	${OS_LINES}	0
	[Return]	${OS}

CLIENT:Get MacOS Version
	${MACDetails}=	run	system_profiler SPSoftwareDataType
	Log	Client OS Details are :\n: ${MACDetails}	TRACE
	${MACOS}=	Get Regexp Matches	${MACDetails}	macOS.\\d{2}.\\d{2}.\\d{1}
	[return]	${MACOS}

CLIENT:Get Windows Version
	${WindowsDetails}=	run	systeminfo | findstr /B /C:"OS Name" /C:"OS Version"
	Log	Client OS Details are :\n: ${WindowsDetails}	TRACE
	${WINOS}=	Get Regexp Matches	${WindowsDetails}	Microsoft.*
	[return]	${WINOS}

CLIENT:Get Linux Version
	${LinuxDetails}=	run	cat /proc/version
	Log	Linux Client OS Details are :\n: ${LinuxDetails}	TRACE
	${LINUXOS}=	Get Regexp Matches	${LinuxDetails}	Linux*
	[return]	${LINUXOS}

CLIENT:Get IPv4 Address
	${RETURN}=	CLIENT:Get Operating System
	Log to Console	Client IPv4 Addresses are:\n
	@{localnetwork}=	Run Keyword If	'macOS' in "${RETURN}"	CLIENT:Get MacOS IPv4 Address
	...	ELSE IF	'Microsoft' in "${RETURN}"	CLIENT:Get Windows IPv4 Address
	[return]	@{localnetwork}


CLIENT:Get MacOS IPv4 Address
	${RETURN}=	run	ifconfig -a|grep 'inet '
	@{LINES}=	Split To Lines	${RETURN}
	@{IPS}=	Create List
	FOR	${LINE}	IN	@{LINES}
		@{ADDRESSLINE}=	Split String	${LINE} ${SPACE}
		${IP}=	Get From List	${ADDRESSLINE}	1
		Log to Console	IPv4 Address is: ${IP}
		Append To List	${IPS}	${IP}
	END
	[return]	@{IPS}

CLIENT:Get Windows IPv4 Address
	${RETURN}=	run	ipconfig /all | findstr /C:"IPv4 Address"
	@{LINES}=	Split To Lines	${RETURN}
	@{IPS}=	Create List
	FOR	${LINE}	IN	@{LINES}
		${LINE}=	Replace String	${LINE}	(	:
		@{ADDRESSLINE}=	Split String	${LINE}	:
		${IP}=	Get From List	${ADDRESSLINE}	1
		Log to Console	IPv4 Address is: ${IP}
		Append To List	${IPS}	${IP}
	END
	[return]	@{IPS}

CLIENT:Start Target Ping
	[Arguments]	${HOST2PING}
	${OS}=	CLIENT:Get Operating System
	Set Suite Variable	${PID_SET}	${FALSE}
	Log	Client OS:\n ${OS}	INFO	console=yes
	Run Keyword If	('macOS' in '''${OS}''' or 'Linux' in '''${OS}''' or 'Microsoft' in '''${OS}''')
	...	CLIENT:Ping	${HOST2PING}
	...	ELSE	Set Suite Variable	${PID_SET}	${FALSE}

CLIENT:Ping
	[Arguments]	${HOST2PING}
	${IS_IPV4}=	Run Keyword and Return Status	Should Match Regexp	${HOST2PING}	^(?:[0-9]{1,3}\\.){3}[0-9]{1,3}$
	${VALUE}=	Set Variable If	${IS_IPV4}	${EMPTY}	6
	${DATE}=	Get Current Date	UTC	exclude_millis=${TRUE}
	${CONVERTED_DATE}=	Convert Date	${DATE}	date_format=%Y-%m-%d %H:%M:%S	result_format=%Y-%m-%dT%H-%M-%S
	${PING_LOG_FILE}=	Set Variable	Results/logs/${CONVERTED_DATE}_ping.log
	Create File	${PING_LOG_FILE}
	${PID}=	Start Process	ping${VALUE}	${HOST2PING}	stdout=${PING_LOG_FILE}
	Set Suite Variable	${PING_LOG_FILE}
	Set Suite Variable	${PID}	${PID}
	Set Suite Variable	${PID_SET}	${TRUE}
	Process should be running	${PID}

CLIENT:End Target Ping
 #	Log	pid_set: ${PID_SET}	INFO	console=yes
	Run Keyword If	${PID_SET}==${TRUE}	Terminate Process	handle=${PID}	kill=true
	Terminate All Processes
	${result} =	Run Keyword If	${PID_SET}==${TRUE}	OperatingSystem.Get File	${PING_LOG_FILE}
	Run Keyword If	${PID_SET}==${TRUE}	Log	Ping Results:\n ${result}	INFO	console=yes
	Run Keyword If	${PID_SET}==${FALSE}	Log	Ping Results:\n No Ping Process was started.\n	INFO	console=yes