*** Settings ***
Documentation
...    Keywords related to Network tabs to be used on GUI.
Resource	../ngm_tests/init.robot

*** Keywords ***
GUI::Network::Open
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Network tab
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Network Tab is open and all elements are accessable
	Click Element	jquery=#main_menu > li:nth-child(4) > a
	GUI::Basic::Spinner Should Be Invisible

GUI::Network::Open Settings Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Network::Settings
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Settings Tab is open and all elements are accessable
	[Tags]	GUI	NETWORK	SETTINGS
	GUI::Network::Open
	click Element		xpath=(//a[contains(text(),'Settings')])[4]
	wait until page contains element		jquery=#hostname

GUI::Network::Open Connections Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Network::Connections
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Connections Tab is open and all elements are accessable
	[Tags]	GUI	NETWORK     CONNECTIONS
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible

GUI::Network::Open Static Routes Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Network::Static Routes
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Static Routes Tab is open and all elements are accessable
	[Tags]	GUI	NETWORK     STATIC_ROUTES
	${ret}=	GUI::Basic::Get NodeGrid System
	Run Keyword If	'${ret}' == 'Nodegrid Services Router'		GUI::Network::Get Tab       4
	...	ELSE 	GUI::Network::Get Tab       3
	Element Should Be Visible       jquery=#netmnt_StroutTable
	GUI::Basic::Spinner Should Be Invisible

GUI::Network::Open Hosts Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Network::Hosts
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Hosts Tab is open and all elements are accessable
	[Tags]	GUI	NETWORK     HOSTS
	${ret}=	GUI::Basic::Get NodeGrid System
	Run Keyword If	'${ret}' == 'Nodegrid Services Router'		GUI::Network::Get Tab       5
	...	ELSE 	GUI::Network::Get Tab       4
	Element Should Be Visible       jquery=#hostTable

GUI::Network::Open SNMP Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Network::SNMP
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	SNMP Tab is open and all elements are accessable
	[Tags]	GUI	NETWORK     SNMP
	${ret}=	GUI::Basic::Get NodeGrid System
	Run Keyword If	'${ret}' == 'Nodegrid Services Router'		GUI::Network::Get Tab       6
	...	ELSE 	GUI::Network::Get Tab       5
	Element Should Be Visible       jquery=#snmpTable

GUI::Network::Open DHCP Server Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Network::DHCP Server
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	DHCP Server Tab is open and all elements are accessable
	[Tags]	GUI	NETWORK     DHCP_SERVER
	${ret}=	GUI::Basic::Get NodeGrid System
	Run Keyword If	'${ret}' == 'Nodegrid Services Router'		GUI::Network::Get Tab       7
	...	ELSE 	GUI::Network::Get Tab       6
	Element Should Be Visible       jquery=#hostTable

GUI::Network::Open SSL VPN Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Network::SSL VPN
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	SSL VPN Tab is open and all elements are accessable
	[Tags]	GUI	NETWORK     STATIC_ROUTES
	${ret}=	GUI::Basic::Get NodeGrid System
	Run Keyword If	'${ret}' == 'Nodegrid Services Router'		GUI::Network::Get Tab       8
	...	ELSE 	GUI::Network::Get Tab       7
	Element Should Be Visible       jquery=#sslvpnClientTable

GUI::Network::SSL VPN::Open Client Tab
    [Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Network::SSL VPN::Client
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Client Tab is open and Logging menu is activated
	[Tags]	GUI	NETWORK     SSL_VPN     CLIENT
	Execute Javascript	window.document.evaluate("//*[@id='main_menu']", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element	jquery=#pod_menu > a:nth-child(1)
	GUI::Basic::Spinner Should Be Invisible

GUI::Network::SSL VPN::Open Server Tab
    [Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Network::SSL VPN::Server
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Server Tab is open and Logging menu is activated
	[Tags]	GUI	NETWORK     SSL_VPN     CLIENT
	Execute Javascript	window.document.evaluate("//*[@id='main_menu']", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element	jquery=#pod_menu > a:nth-child(2)
	GUI::Basic::Spinner Should Be Invisible

GUI::Network::SSL VPN::Open Server Status Tab
    [Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Network::SSL VPN::Server Status
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Server Status Tab is open and Logging menu is activated
	[Tags]	GUI	NETWORK     SSL_VPN     SERVER_STATUS
	Execute Javascript	window.document.evaluate("//*[@id='main_menu']", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element	jquery=#pod_menu > a:nth-child(3)
	GUI::Basic::Spinner Should Be Invisible

GUI::Network::Open Wireless Modem Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Network::Wireless Modem
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	SSL VPN Tab is open and all elements are accessable
	[Tags]	GUI	NETWORK
	${SYSTEM}=	GUI4.x::Basic::Get System
	Run Keyword If 	'${SYSTEM}' == 'Nodegrid Services Router' or '${SYSTEM}' == 'Nodegrid Gate SR'		GUI::Network::Get Tab       9
	Run Keyword If 	'${SYSTEM}' == 'Nodegrid Bold SR' or '${SYSTEM}' == 'Nodegrid Link SR'		GUI::Network::Get Tab       8
	Element Should Be Visible       jquery=#wmodemGlobalTable

GUI::Network::Get Tab
    [Arguments]  ${TAB_NUM}
    Execute Javascript	window.document.evaluate("//*[@id='main_menu']", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	GUI::Network::Open
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(${TAB_NUM})
	GUI::Basic::Spinner Should Be Invisible

GUI::Network::Check If Has Connection
	[Arguments]	${CONNECTION}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections \n${TABLE_CONTENTS}	INFO	console=yes
	${HAS_CONNECTION}	Run Keyword And Return Status	Should Match Regexp	${TABLE_CONTENTS}	${CONNECTION}
#	...	${CONNECTION}+.*Up+.*\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}(?:\\/\\d{2})
	[Return]	${HAS_CONNECTION}

GUI::Network::Get Connection Ipv4
	[Arguments]	${CONNECTION}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections \n${TABLE_CONTENTS}	INFO	console=yes
	${TABLE_CONTENTS}=	Convert To String	${TABLE_CONTENTS}
	@{GET_LINES}=	Split To Lines	${TABLE_CONTENTS}
	FOR	${INDEX}	${LINE}	IN ENUMERATE	@{GET_LINES}
		${STATUS}=	Run Keyword And Return Status	Should Contain	${LINE}	${CONNECTION}
		IF	'${STATUS}' == 'True'
			${NUMBER_LINE}=	Set Variable	${INDEX}
			Exit For Loop
		END
	END
	${LINE}=	Get From List	${GET_LINES}	${NUMBER_LINE}
	${IPV4}=	Should Match Regexp	${LINE}	\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}(?=\\/\\d{2})
	[Return]	${IPV4}

GUI::Network::Get Connection Ipv6
	[Arguments]	${CONNECTION}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections \n${TABLE_CONTENTS}	INFO	console=yes
	${TABLE_CONTENTS}=	Convert To String	${TABLE_CONTENTS}
	@{GET_LINES}=	Split To Lines	${TABLE_CONTENTS}
	FOR	${INDEX}	${LINE}	IN ENUMERATE	@{GET_LINES}
		${STATUS}=	Run Keyword And Return Status	Should Contain	${LINE}	${CONNECTION}
		IF	'${STATUS}' == 'True'
			${NUMBER_LINE}=	Set Variable	${INDEX}
			Exit For Loop
		END
	END
	${LINE}=	Get From List	${GET_LINES}	${NUMBER_LINE}
	${IPV6}   @{MATCHES}=	Should Match Regexp	${LINE}	(([0-9a-fA-F]{0,4}:){1,7}[0-9a-fA-F]{0,4})
	[Return]	${IPV6}

GUI::Network::Setup Untagged VLAN to default with sfp0 and sfp1 on switch
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Click Element	//span[normalize-space()='VLAN']
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete All Rows In Table If Exists	\#switchVlanTable	False
	GUI::Basic::Click Element	//a[@id='1']
	GUI::Basic::Click Element	//div[@id='memberUntagged']//select[@class='selectbox groupFrom']
	Select All From List	//div[@id='memberUntagged']//select[@class='selectbox groupFrom']
	GUI::Basic::Click Element	//div[@id='memberUntagged']//button[@class='listbuilder_btn'][contains(text(),'Add ►')]
	GUI::Basic::Save If Configuration Changed
	GUI::Basic::Click Element	//span[normalize-space()='VLAN']
	GUI::Basic::Click Element	//a[@id='1']
	GUI::Basic::Click Element	//option[@value='backplane1']
	GUI::Basic::Click Element	//div[@id='memberUntagged']//button[@class='listbuilder_btn'][contains(text(),'◄ Remove')]
	GUI::Basic::Click Element	//option[@value='sfp1']
	GUI::Basic::Click Element	//div[@id='memberUntagged']//button[@class='listbuilder_btn'][contains(text(),'◄ Remove')]
	GUI::Basic::Save If Configuration Changed
	GUI::Basic::Click Element	//span[normalize-space()='VLAN']
	GUI::Basic::Add
	GUI::Basic::Click Element	//input[@id='vlan']
	GUI::Basic::Input Text	//input[@id='vlan']	2
	GUI::Basic::Click Element	//div[@id='memberUntagged']//option[@value='backplane1'][normalize-space()='backplane1']
	GUI::Basic::Click Element	//div[@id='memberUntagged']//button[@class='listbuilder_btn'][contains(text(),'Add ►')]
	GUI::Basic::Click Element	//div[@id='memberUntagged']//option[@value='sfp1'][normalize-space()='sfp1']
	GUI::Basic::Click Element	//div[@id='memberUntagged']//button[@class='listbuilder_btn'][contains(text(),'Add ►')]
	GUI::Basic::Save

GUI::Network::Setup Untagged VLAN to default without sfp0 and sfp1 on switch
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Click Element	//span[normalize-space()='VLAN']
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete All Rows In Table If Exists	\#switchVlanTable	False
	GUI::Basic::Click Element	//a[@id='1']
	GUI::Basic::Click Element	//div[@id='memberUntagged']//select[@class='selectbox groupFrom']
	Select All From List	//div[@id='memberUntagged']//select[@class='selectbox groupFrom']
	GUI::Basic::Click Element	//div[@id='memberUntagged']//button[@class='listbuilder_btn'][contains(text(),'Add ►')]
	GUI::Basic::Save If Configuration Changed
	GUI::Basic::Click Element	//span[normalize-space()='VLAN']
	GUI::Basic::Click Element	//a[@id='1']
	GUI::Basic::Click Element	//option[@value='backplane1']
	GUI::Basic::Click Element	//div[@id='memberUntagged']//button[@class='listbuilder_btn'][contains(text(),'◄ Remove')]
	GUI::Basic::Save If Configuration Changed

GUI::Network::Setup BACKPLANE Connection to default
	[Arguments]	${BACKPLANE}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Click Element	//a[@id='${BACKPLANE}']
	GUI::Basic::Click Element	//label[normalize-space()='DHCP']//input[@id='method']
	GUI::Basic::Save If Configuration Changed

GUI::Network::Switch::Add Backplane1 and sfp1 to first Untagged VLAN
	[Arguments]	${HAS_SFP1}=Yes
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Click Element	//span[normalize-space()='VLAN']
	GUI::Basic::Click Element	//a[@id='1']
	GUI::Basic::Click Element	//div[@id='memberUntagged']//option[@value='backplane1'][normalize-space()='backplane1']
	GUI::Basic::Click Element	//div[@id='memberUntagged']//button[@class='listbuilder_btn'][contains(text(),'Add ►')]
	IF	'${HAS_SFP1}' == 'Yes'
		GUI::Basic::Click Element	//div[@id='memberUntagged']//option[@value='sfp1'][normalize-space()='sfp1']
		GUI::Basic::Click Element	//div[@id='memberUntagged']//button[@class='listbuilder_btn'][contains(text(),'Add ►')]
	END
	GUI::Basic::Save

GUI::Network::Connections::Setup Backplane with static IPv4 and check if it's Up
	[Arguments]	${HOSTDIFF}	${BACKPLANE}	${TEST_CONNECTION_BACK}	${TEST_QOS_INTERFACE_BACK}	${ALIAS}
	GUI::Basic::Switch Browser	ALIAS=${ALIAS}
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Click Element	//a[@id='${BACKPLANE}']
	GUI::Basic::Click Element	//label[normalize-space()='Static']//input[@id='method']
	GUI::Basic::Click Element	//input[@id='address']
	${LAST_IPV4_NUMBER}	Fetch From Right	${HOSTDIFF}	.
	IF	'${BACKPLANE}' == 'BACKPLANE0'
		${HOSTDIFF_BACK01_IP}	Set Variable	10.0.0.${LAST_IPV4_NUMBER}
	ELSE
		${HOSTDIFF_BACK01_IP}	Set Variable	10.0.1.${LAST_IPV4_NUMBER}
	END
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Input Text	//input[@id='address']	${HOSTDIFF_BACK01_IP}
	Select Checkbox	//input[@id='connAuto']
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save If Configuration Changed
	Wait Until Keyword Succeeds	3x	3s	GUI::Network::Connections::Check If connection Has IPv4 and Is Up
	...	${TEST_CONNECTION_BACK}	${TEST_QOS_INTERFACE_BACK}	${HOSTDIFF_BACK01_IP}
	[Return]	${HOSTDIFF_BACK01_IP}

GUI::Network::Switch::Switch Interfaces::Setup Switch Interfaces to default with sfp0 and sfp1 on switch
	${ALL_STATUS}	Create List	Disabled	Enabled	Disabled
	FOR	${STATUS}	IN	@{ALL_STATUS}
		GUI::Basic::Click Element	//span[normalize-space()='Switch Interfaces']
		GUI::Basic::Click Element	//th[@class='sorting_disabled selectable']//input[@type='checkbox']
		GUI::Basic::Click Element	//input[@id='editButton']
		GUI::Basic::Click Element	//select[@id='status']
		Select From List By Label	//select[@id='status']	${STATUS}
		GUI::Basic::Save If Configuration Changed
	END
	GUI::Basic::Click Element	//span[normalize-space()='Switch Interfaces']
	Select Checkbox	//tr[@id='sfp0']//input[@type='checkbox']
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	//tr[@id='sfp1']//input[@type='checkbox']
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//input[@id='editButton']
	GUI::Basic::Click Element	//select[@id='status']
	Select From List By Label	//select[@id='status']	Enabled
	GUI::Basic::Save

GUI::Network::Switch::Switch Interfaces::Setup Switch Interfaces to default without sfp0 and sfp1 on switch
	${ALL_STATUS}	Create List	Enabled	Disabled	Enabled
	GUI::Basic::Network::Switch::open tab
	FOR	${STATUS}	IN	@{ALL_STATUS}
		GUI::Basic::Click Element	//span[normalize-space()='Switch Interfaces']
		GUI::Basic::Click Element	//th[@class='sorting_disabled selectable']//input[@type='checkbox']
		GUI::Basic::Click Element	//input[@id='editButton']
		GUI::Basic::Click Element	//select[@id='status']
		Select From List By Label	//select[@id='status']	${STATUS}
		GUI::Basic::Save If Configuration Changed
	END

GUI::Network::Switch::Switch Interfaces::Enable switch interface
	[Arguments]	${SWITCH_INTERFACE}
	${ALL_STATUS}	Create List	Disabled	Enabled	Disabled
	GUI::Basic::Network::Switch::open tab
	FOR	${STATUS}	IN	@{ALL_STATUS}
		GUI::Basic::Click Element	//span[normalize-space()='Switch Interfaces']
		GUI::Basic::Click Element	//th[@class='sorting_disabled selectable']//input[@type='checkbox']
		GUI::Basic::Click Element	//input[@id='editButton']
		GUI::Basic::Click Element	//select[@id='status']
		Select From List By Label	//select[@id='status']	${STATUS}
		GUI::Basic::Save If Configuration Changed
	END
	GUI::Basic::Network::Switch::open tab
	GUI::Basic::Click Element	//span[normalize-space()='Switch Interfaces']
	Select Checkbox	//tr[@id='${SWITCH_INTERFACE}']//input[@type='checkbox']
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//input[@id='editButton']
	GUI::Basic::Click Element	//select[@id='status']
	Select From List By Label	//select[@id='status']	Enabled
	GUI::Basic::Save
	
GUI::Network::802.1x::Open Profiles Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Network::802.1x::Profiles Tab
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Profiles Tab is open and all elements are accessable
	[Tags]	GUI  NETWORK  802.1x  Profiles
	GUI::Basic::Network::802.1x::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' >= '5.8'	Click Element    xpath=//*[@id="netDot1x"]/span
	Run Keyword If	'${NGVERSION}' < '5.8'	Click Element    xpath=/html/body/div[7]/div[1]/a[1]/span
	GUI::Basic::Spinner Should Be Invisible

GUI::Network::802.1x::Open Credentials Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Network::802.1x::Credentials Tab
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Credentials Tab is open and all elements are accessable
	[Tags]	GUI  NETWORK  802.1x  Credentials
	GUI::Basic::Network::802.1x::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${NGVERSION}' >= '5.8'	Click Element    xpath=//*[@id="netswitchDot1xUsers"]/span
	Run Keyword If	'${NGVERSION}' < '5.8'	Click Element    xpath=/html/body/div[7]/div[1]/a[2]/span
	GUI::Basic::Spinner Should Be Invisible

GUI::Network::802.1x::Delete Credentials If Exists
	[Arguments]	${CRED_NAME}=""	${HAS_ALERT}=True
	[Documentation]	Delete table rows which contains the given value
	${EXISTS}=  Run Keyword And Return Status  Page Should Contain Element  //tr[@id='${CRED_NAME}']
    
	IF  ${EXISTS} == ${TRUE}
	    GUI::Basic::Select Checkbox  //tr[@id='${CRED_NAME}']/td/input
	    Wait Until Element Is Enabled  id=delButton
        Click Element    id=delButton
        GUI::Basic::Spinner Should Be Invisible
	END

GUI::Network::802.1x::Delete Profiles If Exists
	[Arguments]	${PROF_NAME}=""	${HAS_ALERT}=True
	[Documentation]	Delete table rows which contains the given value
	${EXISTS}=  Run Keyword And Return Status  Page Should Contain Element  //tr[@id='${PROF_NAME}']
    
	IF  ${EXISTS} == ${TRUE}
	    GUI::Basic::Select Checkbox  //tr[@id='${PROF_NAME}']/td/input
	    Wait Until Element Is Enabled  id=delButton
        Click Element    id=delButton
        GUI::Basic::Spinner Should Be Invisible
	END

GUI::Network::802.1x::Create An Credentials Certificate
	[Arguments]  ${CERT_COUNTRY}=${EMPTY}  ${CERT_STATE}=${EMPTY}  ${CERT_LOCALITY}=${EMPTY}  ${CERT_ORGANIZATION}=${EMPTY}
	...  ${CERT_EMAIL}=${EMPTY}  ${CERT_INPUT_PASSWORD}=${EMPTY}  ${CERT_OUTPUT_PASSWORD}=${EMPTY}  ${CLICK_ON_SAVE}=${True}
    
	Wait Until Element Is Visible		id=dot1xCertCountry

	Run Keyword If	'${CERT_COUNTRY}' != '${EMPTY}'  Input Text		xpath=//*[@id="dot1xCertCountry"]		${CERT_COUNTRY}
	Run Keyword If	'${CERT_STATE}' != '${EMPTY}'  Input Text		xpath=//*[@id="dot1xCertState"]			${CERT_STATE}
	Run Keyword If	'${CERT_LOCALITY}' != '${EMPTY}'  Input Text		xpath=//*[@id="dot1xCertLocality"]	${CERT_LOCALITY}
	Run Keyword If	'${CERT_ORGANIZATION}' != '${EMPTY}'  Input Text		xpath=//*[@id="dot1xCertOrg"]				${CERT_ORGANIZATION}
	Run Keyword If	'${CERT_EMAIL}' != '${EMPTY}'  Input Text		xpath=//*[@id="dot1xCertEmail"]			${CERT_EMAIL}
	Run Keyword If	'${CERT_INPUT_PASSWORD}' != '${EMPTY}'  Input Text		xpath=//*[@id="dot1xCertInPass"]		${CERT_INPUT_PASSWORD}
	Run Keyword If	'${CERT_OUTPUT_PASSWORD}' != '${EMPTY}'  Input Text		xpath=//*[@id="dot1xCertOutPass"]		${CERT_OUTPUT_PASSWORD}

  IF  '${CLICK_ON_SAVE}'=='${True}'
		GUI::Basic::Save
	END

GUI::Network::802.1x::Create An Credentials
  [Arguments]  ${CRED_NAME}=admin  ${CRED_PASSWORD}=admin  ${CRED_AUTH_METHOD}=MD5  ${CRED_ANONYMOUS}=${EMPTY}
  ...  ${CRED_INNER_AUTH}=${EMPTY}  ${CLICK_ON_SAVE}=${True}
    
	Wait Until Element Is Visible    id=dot1xUsersName
	Input Text	                xpath=//*[@id="dot1xUsersName"]						${CRED_NAME}
	Input Text	                xpath=//*[@id="dot1xUsersPassword"]				${CRED_PASSWORD}
	Input Text	                xpath=//*[@id="dot1xUsersPasswordAgain"]	${CRED_PASSWORD}
	Select From List By Value		xpath=//*[@id="dot1xUsersAuthMethod"]			${CRED_AUTH_METHOD}
	 
	IF  '${CRED_AUTH_METHOD}' == 'PEAP'  
		Wait Until Element Is Visible	id=dot1xUsersAuthMethod2
		Input Text										id=dot1xUsersAnonId				${CRED_ANONYMOUS}
		Select From List By Value			id=dot1xUsersAuthMethod2	${CRED_INNER_AUTH}
	END

	IF	'${CRED_AUTH_METHOD}' == 'TTLS'
		Wait Until Element Is Visible	id=dot1xUsersAuthMethod2
		Input Text										id=dot1xUsersAnonId				${CRED_ANONYMOUS}
		Select From List By Value			id=dot1xUsersAuthMethod2	${CRED_INNER_AUTH}
	END

  IF  '${CLICK_ON_SAVE}'=='${True}'
		GUI::Basic::Save
	END

GUI::Network::802.1x::Create An Profile
	[Arguments]  ${PROF_NAME}=admin  ${PROF_TYPE}=internal_eap_server  ${PROF_USERS}=${EMPTY} 
	...  ${PROF_TRANSMIT_INTERVAL}=${EMPTY}  ${PROF_IP_ADDRESS}=${EMPTY}  ${PROF_PORT_NUMBER}=${EMPTY}
	...  ${PROF_SHARED_SECRET}=${EMPTY}  ${PROF_RETRANSMIT_INTERVAL}=${EMPTY}
    
	Wait Until Element Is Visible    id=dot1xName  20s
    Input Text	id=dot1xName	${PROF_NAME}
    
	IF	'${PROF_TYPE}' == 'internal_eap_server'
	    GUI::Basic::Select Radio Button    dot1xType    internal
	    FOR    ${PROF_USER}	 IN   @{PROF_USERS}
            Double Click Element  xpath=//select/option[@value="${PROF_USER}"]
		END

	    Input Text    xpath=//*[@id="dot1xIntPeriod"]    ${PROF_TRANSMIT_INTERVAL}
    END

	IF  '${PROF_TYPE}' == 'radius_server'
	    GUI::Basic::Select Radio Button    dot1xType    radius
	    Input Text	xpath=//*[@id="dot1xIp"]	    ${PROF_IP_ADDRESS}
	    Input Text	xpath=//*[@id="dot1xPort"]	    ${PROF_PORT_NUMBER}
	    Input Text	xpath=//*[@id="dot1xSecret"]	${PROF_SHARED_SECRET}
	    Input Text	xpath=//*[@id="dot1xPeriod"]	${PROF_RETRANSMIT_INTERVAL}
    END

	IF  '${PROF_TYPE}' == 'supplicant'
	    GUI::Basic::Select Radio Button    dot1xType    supplicant
	    Select From List By Label	xpath=//*[@id="dot1xSuppUsers"]	@{PROF_USERS}
	END

	GUI::Basic::Save

GUI::Network::Connections::Check If connection Has IPv4 and Is Up
	[Arguments]	${TEST_CONNECTION_NAME}	${TEST_INTERFACE_NAME}	${HOSTDIFF_IPV4}
	GUI::Basic::Network::Connections::open tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible  peerTable
	${TABLE_CONTENTS}=	Get Text	peerTable
	Log	\nTable contents in Network::Connections \n${TABLE_CONTENTS}	INFO	console=yes
	Should Match Regexp	${TABLE_CONTENTS}	${TEST_CONNECTION_NAME}+\\s+Connected+\\s+Ethernet+\\s+${TEST_INTERFACE_NAME}+\\s+Up+\\s+${HOSTDIFF_IPV4}

GUI::Network::VPN::Open IPsec
	Run Keyword If	'${NGVERSION}' <= '5.2'	GUI::Basic::Network::IPsec::open tab
	Run Keyword If	'${NGVERSION}' >= '5.4'	Run Keywords	GUI::Basic::Network::VPN::Open Tab	AND	Click Element	//span[normalize-space()='IPsec']
	GUI::Basic::Spinner Should Be Invisible

GUI::Network::VPN::Open IPsec::Global
	Run Keyword If	'${NGVERSION}' <= '5.2'	GUI::Basic::Network::IPsec::open tab
	Run Keyword If	'${NGVERSION}' >= '5.4'	Run Keywords	GUI::Basic::Network::VPN::Open Tab	AND	Click Element	//span[normalize-space()='IPsec']
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//span[normalize-space()='Global']
	GUI::Basic::Spinner Should Be Invisible

GUI::Network::VPN::Open IPsec::IKE Profile
	Run Keyword If	'${NGVERSION}' <= '5.2'	GUI::Basic::Network::IPsec::open tab
	Run Keyword If	'${NGVERSION}' >= '5.4'	Run Keywords	GUI::Basic::Network::VPN::Open Tab	AND	Click Element	//span[normalize-space()='IPsec']
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//span[normalize-space()='IKE Profile']
	GUI::Basic::Spinner Should Be Invisible

GUI::Network::DHCP Server::Network Range::Open Tab
	Run Keyword If	'${NGVERSION}' >= '5.8'  Wait Until Page Contains Element	xpath=//*[@id="range"]
	Run Keyword If	'${NGVERSION}' <= '5.6'  Wait Until Page Contains Element	xpath=/html/body/div[7]/div[1]/a[2]/span
	
	Run Keyword If	'${NGVERSION}' >= '5.8'	Click Element	xpath=//*[@id="range"]
	Run Keyword If	'${NGVERSION}' <= '5.6'	Click Element	xpath=/html/body/div[7]/div[1]/a[2]/span
	GUI::Basic::Spinner Should Be Invisible

GUI::Network::DHCP Server::Hosts::Open Tab
	Run Keyword If	'${NGVERSION}' >= '5.8'  Wait Until Page Contains Element	xpath=//*[@id="dhcphost"]
	Run Keyword If	'${NGVERSION}' <= '5.6'  Wait Until Page Contains Element	xpath=/html/body/div[7]/div[1]/a[3]/span
	
	Run Keyword If	'${NGVERSION}' >= '5.8'	Click Element	xpath=//*[@id="dhcphost"]
	Run Keyword If	'${NGVERSION}' <= '5.6'	Click Element	xpath=/html/body/div[7]/div[1]/a[3]/span
	GUI::Basic::Spinner Should Be Invisible