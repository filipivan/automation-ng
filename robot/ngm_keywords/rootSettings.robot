*** Settings ***
Resource    ngmGuiBasicKeywords.robot
Resource    ngmGUIAccessKeywords.robot
Resource    ngmGUISystemKeywords.robot
Resource    ngmGUIManagedDevicesKeywords.robot
Resource    ngmGUISecurityKeywords.robot
Resource    ngmGUIAuditingKeywords.robot
Resource    ngmGUIDashboardKeywords.robot
Resource    ngmCLIKeywords.robot
Resource    systemKeywords.robot
Resource    commonKeywords.robot
Resource    ngmGUICloudKeywords.robot
Resource    ngmGUIClusterKeywords.robot
Resource    ngmGUINetworkKeywords.robot
Resource    ngmGUITrackingKeywords.robot
Resource    ngmZPECloudGUIKeywords.robot
Resource    ./rootApiSettings.robot
Resource    ./rootCloudApiSettings.robot



Library     SeleniumLibrary    run_on_failure=Capture Page Screenshot      screenshot_root_directory=Results/Screenshots
Library     SSHLibrary
Library     String
Library     Collections
Library     OperatingSystem
Library     Process
Library     DateTime
Library     RequestsLibrary
Library     ngmGUIAccessKeywords.py
Library     ngmGUIBasicKeywords.py
Library     ngmZPECloudCellularDataCollection.py

*** Variables ***
${SCREENSHOTDIR}    Results/Screenshots