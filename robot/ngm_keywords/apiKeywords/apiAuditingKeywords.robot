*** Keywords ***
API::Get::Auditing::Settings
	[Documentation]
	...	Get the auditing settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the auditing settings
	${response}=	API::Send Get Request	auditing/settings
	[Return]	${response}

API::Put::Auditing::Settings
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update the auditing settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update auditing settings
	${response}=	API::Send Put Request	auditing/settings	${PAYLOAD}
	[Return]	${response}

API::Get::Auditing::Events::Email
	[Documentation]
	...	Get the auditing events email info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the auditing events email info
	${response}=	API::Send Get Request	auditing/events/email
	[Return]	${response}

API::Put::Auditing::Events::Email
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update the auditing events email info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the auditing events email info
	${response}=	API::Send Put Request	auditing/events/email	${PAYLOAD}
	[Return]	${response}

API::Get::Auditing::Events::File
	[Documentation]
	...	Get the auditing events file info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the auditing events file info
	${response}=	API::Send Get Request	auditing/events/file
	[Return]	${response}

API::Put::Auditing::Events::File
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update the auditing events file info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the auditing events file info
	${response}=	API::Send Put Request	auditing/events/file	${PAYLOAD}
	[Return]	${response}

API::Get::Auditing::Events::SNMP Trap
	[Documentation]
	...	Get the auditing events SNMP trap info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the auditing events SNMP trap info
	${response}=	API::Send Get Request	auditing/events/snmptrap
	[Return]	${response}

API::Put::Auditing::Events::SNMP Trap
	[Arguments]	${PAYLOAD}
	[Documentation]
	...	Update the auditing events SNMP trap info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the auditing events SNMP trap info
	${response}=	API::Send Put Request	auditing/events/snmptrap	${PAYLOAD}
	[Return]	${response}

API::Get::Auditing::Events::Syslog
	[Documentation]
	...	Get the auditing events syslog info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the auditing events syslog info
	${response}=	API::Send Get Request	auditing/events/syslog
	[Return]	${response}

API::Put::Auditing::Events::Syslog
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update the auditing events syslog info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the auditing events syslog info
	${response}=	API::Send Put Request	auditing/events/syslog	${PAYLOAD}
	[Return]	${response}

API::Get::Auditing::Destination::File
	[Documentation]
	...	Get the::Auditing::Destination::File using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the::Auditing::Destination::File
	${response}=	API::Send Get Request	auditing/destination/file
	[Return]	${response}

API::Put::Auditing::Destination::File
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update the::Auditing::Destination::File info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the::Auditing::Destination::File info
	${response}=	API::Send Put Request	auditing/destination/file	${PAYLOAD}
	[Return]	${response}

API::Get::Auditing::Destination::Syslog
	[Documentation]
	...	Get the::Auditing::Destination::Syslog using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the::Auditing::Destination::Syslog
	${response}=	API::Send Get Request	auditing/destination/syslog
	[Return]	${response}

API::Put::Auditing::Destination::Syslog
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update the::Auditing::Destination::Syslog info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the::Auditing::Destination::Syslog info
	${response}=	API::Send Put Request	auditing/destination/syslog	${PAYLOAD}
	[Return]	${response}

API::Get::Auditing::Destination::SNMP Trap
	[Documentation]
	...	Get the::Auditing::Destination::SNMP Trap using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the::Auditing::Destination::SNMP Trap
	${response}=	API::Send Get Request	auditing/destination/snmptrap
	[Return]	${response}

API::Put::Auditing::Destination::SNMP Trap
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update the::Auditing::Destination::SNMP Trap info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the::Auditing::Destination::SNMP Trap info
	${response}=	API::Send Put Request	auditing/destination/snmptrap	${PAYLOAD}
	[Return]	${response}

API::Get::Auditing::Destination::Email
	[Documentation]
	...	Get the::Auditing::Destination::Email using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the::Auditing::Destination::Email
	${response}=	API::Send Get Request	auditing/destination/email
	[Return]	${response}

API::Put::Auditing::Destination::Email
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update the::Auditing::Destination::Email info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the::Auditing::Destination::Email info
	${response}=	API::Send Put Request	auditing/destination/email	${PAYLOAD}
	[Return]	${response}