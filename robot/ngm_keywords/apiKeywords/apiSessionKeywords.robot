*** Settings ***
Resource	../apiKeywords/apiBasicKeywords.robot

*** Keywords ***
API::Post::Session
	[Arguments]	${USERNAME}=admin	${PASSWORD}=${DEFAULT_PASSWORD}	${RAW_MODE}=no	${MODE}=yes	${SESSION}=False
	[Documentation]
	...	Authenticate using credentials into Nodegrid's API and add ticket to ${SESSION_TICKET}
	...	== REQUIREMENTS ==
	...	None
	...	== ARGUMENTS ==
	...	-	USERNAME Username used in the authentication
	...	-	PASSWORD Username's password used in the authentication
	...	== EXPECTED RESULT ==
	...	Login successfully in the API
	${payload}=	Create Dictionary
	IF	${SESSION}!=False
		${payload}	Set Variable	${SESSION}
	END
	Set To Dictionary	${payload}	username=${USERNAME}
	Set To Dictionary	${payload}	password=${PASSWORD}
	${response}=	API::Send Post Request	/Session	${payload}	RAW_MODE=${RAW_MODE}	MODE=${MODE}
	Should Be Equal   ${response.status_code}   ${200}
	
	Return From Keyword If	'${RAW_MODE}'!='no'	${response}
	API::Set Suite Ticket	${response.json()}

	[Return]	${response}

API::Set Suite Ticket
	[Arguments]	${response}
	${contain_session}=	Run Keyword And Return Status	Get From Dictionary	${response}	session
	${session}=	Run Keyword If	${contain_session}	Get From Dictionary	${response}	session
	Should Not Be Empty	${session}

	#	ONLY CREATING DICT BEFORE TO USE {TICKET} IN API::Get::Tracking::Open Sessions AND GET ID
	${dict}=	Create Dictionary
	Set To Dictionary	${dict}	ticket=${session}
	Set To Dictionary	${dict}	id=0
	${session_list}=	Create List	${dict}

	${status}=	Run Keyword And Return Status	Variable Should Exist	${SESSION_TICKET}
	Set Suite Variable	${SESSION_TICKET}	${session_list}

	#	USING TICKET HERE AND GETTING ID
	${all_sessions}=	API::Get::Tracking::Open Sessions	MODE=no
	${last_session}=	Get From List	${all_sessions.json()}	-1
	${last_session_id}=	Get From Dictionary	${last_session}	id

	Run Keyword If	${status} == ${FALSE}	Remove From List	${SESSION_TICKET}	0
	Set To Dictionary	${dict}	ticket=${session}
	Set To Dictionary	${dict}	id=${last_session_id}
	Append to List	${SESSION_TICKET}	${dict}

API::Delete::Session
	[Arguments]	${INDEX_TICKET}=0	${RAW_MODE}=no	${MODE}=yes
	[Documentation]
	...	Destroy the session previously created by API::Authenticate
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Destroy the session set in the "ticket" header
	${response}=	API::Send Delete Request	/Session	INDEX_TICKET=${INDEX_TICKET}	RAW_MODE=${RAW_MODE}	MODE=${MODE}
	Remove From List	${SESSION_TICKET}	${INDEX_TICKET}
	[Return]	${response}