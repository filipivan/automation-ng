*** Settings ***
Resource	apiBasicKeywords.robot

*** Keywords ***
API::Get::System::Licenses
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get the licenses using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the licenses
	${response}=	API::Send Get Request	system/licenses	MODE=${MODE}
	[Return]	${response}

API::Post::System::Licenses
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds licenses using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds the given license in the NG
	${response}=	API::Send Post Request	system/licenses	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Delete::System::Licenses
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Delete license using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Remove license from NG
	${response}=	API::Send Delete Request	system/licenses	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::System::Preferences
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get the system preferences using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the system preferences
	${response}=	API::Send Get Request	system/preferences	MODE=${MODE}
	[Return]	${response}

API::Put::System::Preferences
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Updates the system preferences using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the system preferences
	${response}=	API::Send Put Request	system/preferences	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::System::Date Time
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get the system date time using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the system date time
	${response}=	API::Send Get Request	system/datetime	MODE=${MODE}
	[Return]	${response}

API::Put::System::Date Time
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update the system date time using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the system date time fields
	${response}=	API::Send Put Request	system/datetime	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Post::System::Toolkit::Shutdown
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Adds licenses using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds the given license in the NG
	${response}=	API::Send Post Request	system/toolkit/shutdown	MODE=${MODE}
	[Return]	${response}

API::Post::System::Toolkit::Reboot
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Adds licenses using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds the given license in the NG
	${response}=	API::Send Post Request	system/toolkit/reboot	MODE=${MODE}
	[Return]	${response}

API::Post::System::Toolkit::Upgrade
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds licenses using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds the given license in the NG
	${response}=	API::Send Post Request	system/toolkit/upgrade	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Post::System::Toolkit::Save Settings
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds licenses using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds the given license in the NG
	${response}=	API::Send Post Request	system/toolkit/savesettings	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Post::System::Toolkit::Apply Settings
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds licenses using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds the given license in the NG
	${response}=	API::Send Post Request	system/toolkit/applysettings	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Post::System::Toolkit::Factory
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds licenses using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds the given license in the NG
	${response}=	API::Send Post Request	system/toolkit/factory	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Post::System::Toolkit::Certificate
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds licenses using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds the given license in the NG
	${response}=	API::Send Post Request	system/toolkit/factory	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::System::Toolkit::Checksum
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Adds licenses using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds the given license in the NG
	${response}=	API::Send Get Request	system/toolkit/checksum	MODE=${MODE}
	[Return]	${response}

API::Post::System::Toolkit::Checksum
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds licenses using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds the given license in the NG
	${response}=	API::Send Post Request	system/toolkit/checksum	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Post::System::Toolkit::Ping
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds licenses using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds the given license in the NG
	${response}=	API::Send Post Request	system/toolkit/ping	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Post::System::Toolkit::Traceroute
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds licenses using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds the given license in the NG
	${response}=	API::Send Post Request	system/toolkit/traceroute	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Post::System::Toolkit::DNS Lookup
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds licenses using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds the given license in the NG
	${response}=	API::Send Post Request	system/toolkit/dnslookup	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::System::Logging
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get the system logging using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the system logging data
	${response}=	API::Send Get Request	system/logging	MODE=${MODE}	MODE=${MODE}
	[Return]	${response}

API::Put::System::Logging
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update the system logging using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the system logging fields
	${response}=	API::Send Put Request	system/logging	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::System::Custom Fields
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get the system custom fields using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the system custom fields data
	${response}=	API::Send Get Request	system/customfields	MODE=${MODE}
	[Return]	${response}

API::Post::System::Custom Fields
	[Arguments]	${PAYLOAD}	${RAW_MODE}=no	${MODE}=yes
	[Documentation]
	...	Adds new custom field using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds new custom field
	${response}=	API::Send Post Request	system/customfields	${PAYLOAD}	RAW_MODE=${RAW_MODE}	MODE=${MODE}
	[Return]	${response}

API::Delete::System::Custom Fields
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Updates custom field value using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update custom field value
	${response}=	API::Send Delete Request	system/customfields	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Put::System::Custom Fields::Name
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Updates custom field value using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update custom field value
	${response}=	API::Send Put Request	system/customfields/${NAME}	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::System::Dial Up
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get the dialup configuration using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the dialup configuration
	${response}=	API::Send Get Request	system/dialup	MODE=${MODE}
	[Return]	${response}

API::Put::System::Dial Up
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Get the dialup configuration using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the dialup configuration
	${response}=	API::Send Put Request	system/dialup	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::System::Dial Up::Callback Users
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get the dialup callback users using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the dialup callback users data
	${response}=	API::Send Get Request	system/dialup/callbackusers	MODE=${MODE}
	[Return]	${response}

API::Post::System::Dial Up::Callback Users
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds dialup callback user using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add new dialup callback user in NG
	${response}=	API::Send Post Request	system/dialup/callbackusers	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Delete::System::Dial Up::Callback Users
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Deletes dialup callback user using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	License removed from NG
	${response}=	API::Send Delete Request	system/dialup/callbackusers	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::System::Dial Up::Callback Users::Name
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get dialup callback user using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Returns the dialup callback users
	${response}=	API::Send Get Request	system/dialup/callbackusers/${NAME}	MODE=${MODE}
	[Return]	${response}

API::Put::System::Dial Up::Callback Users::Name
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds dialup callback user using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add new dialup callback user in NG
	${response}=	API::Send Put Request	system/dialup/callbackusers/${NAME}	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::System::Schedule
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get the dialup callback users using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the dialup callback users data
	${response}=	API::Send Get Request	system/schedule	MODE=${MODE}
	[Return]	${response}

API::Post::System::Schedule
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds dialup callback user using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add new dialup callback user in NG
	${response}=	API::Send Post Request	system/schedule	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Delete::System::Schedule
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Deletes dialup callback user using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	License removed from NG
	${response}=	API::Send Delete Request	system/schedule	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::System::Schedule::Name
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get dialup callback user using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Returns the dialup callback users
	${response}=	API::Send Get Request	system/schedule/${NAME}	MODE=${MODE}
	[Return]	${response}

API::Put::System::Schedule::Name
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds dialup callback user using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add new dialup callback user in NG
	${response}=	API::Send Put Request	system/schedule/${NAME}	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::System::Sms
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Gets the sms values using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the system custom fields data
	${response}=	API::Send Get Request	system/sms	MODE=${MODE}
	[Return]	${response}

API::Put::System::Sms
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Updates sms values using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update custom field value
	${response}=	API::Send Put Request	system/sms	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::System::Sms::Whitelist
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get the system sms whitelist using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the system custom fields data
	${response}=	API::Send Get Request	system/sms/whitelist	MODE=${MODE}
	[Return]	${response}

API::Post::System::Sms::Whitelist
	[Arguments]	${PAYLOAD}	${RAW_MODE}=no	${MODE}=yes
	[Documentation]
	...	Adds new sms whitelist using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds new custom field
	${response}=	API::Send Post Request	system/sms/whitelist	${PAYLOAD}	RAW_MODE=${RAW_MODE}	MODE=${MODE}
	[Return]	${response}

API::Delete::System::Sms::Whitelist
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Updates sms whitelistvalue using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update custom field value
	${response}=	API::Send Delete Request	system/sms/whitelist	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Put::System::Sms::Whitelist::Name
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Updates sms whitelist value using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update custom field value
	${response}=	API::Send Put Request	system/sms/whitelist/${NAME}	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}