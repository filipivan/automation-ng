*** Keywords ***
API::Get::Tracking::Open Sessions
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	${response}=	API::Send Get Request	tracking/opensessions	MODE=${MODE}
	[Return]	${response}

API::Post::Tracking::Open Sessions::Terminate
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Terminate open sessions using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Terminate open session on ${PAYLOAD} from NG
	${response}=	API::Send Post Request	tracking/opensessions/terminate	${PAYLOAD}	MODE=${MODE}
	${LENGTH}=	Get Length	${SESSION_TICKET}
	${LIST}=	Create List
	:FOR	${INDEX}	IN RANGE	${LENGTH}
	\	${SESSION}=	Get From List	${SESSION_TICKET}	${INDEX}
	\	${ID}=	Get From Dictionary	${SESSION}	id
	\	Append to List	${LIST}	${ID}
	:FOR	${session}	IN	@{PAYLOAD["sessions"]}
	\	${ID}=	Get Index From List	${LIST}	${session}
	\	Run Keyword If	${ID} != -1	Remove From List	${SESSION_TICKET}	${ID}
	[Return]	${response}

API::Get::Tracking::Devices Sessions
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	${response}=	API::Send Get Request	tracking/devicessessions	MODE=${MODE}
	[Return]	${response}

API::Post::Tracking::Devices Sessions::Terminate
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Terminate open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	${response}=	API::Send Post Request	tracking/devicessessions/terminate	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Tracking::Events
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	${response}=	API::Send Get Request	tracking/events	MODE=${MODE}
	[Return]	${response}

API::Post::Tracking::Events::Reset Counters
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Terminate open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	${response}=	API::Send Post Request	tracking/events/resetcounters	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Tracking::System::CPU
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	${response}=	API::Send Get Request	tracking/system/cpu	MODE=${MODE}
	[Return]	${response}

API::Get::Tracking::System::Memory
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	${response}=	API::Send Get Request	tracking/system/memory	MODE=${MODE}
	[Return]	${response}

API::Get::Tracking::System::Disk
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	${response}=	API::Send Get Request	tracking/system/disk	MODE=${MODE}
	[Return]	${response}

API::Get::Tracking::Discovery Logs
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	${response}=	API::Send Get Request	tracking/discoverylogs	MODE=${MODE}
	[Return]	${response}

API::Post::Tracking::Discovery Logs::Reset Logs
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	${response}=	API::Send Post Request	tracking/discoverylogs/resetlogs	MODE=${MODE}
	[Return]	${response}

API::Get::Tracking::Network::Interfaces
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	${response}=	API::Send Get Request	tracking/network/interfaces	MODE=${MODE}
	[Return]	${response}

API::Get::Tracking::Network::Interfaces::Name
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	${response}=	API::Send Get Request	tracking/network/interfaces/${NAME}	MODE=${MODE}
	[Return]	${response}

API::Get::Tracking::Network::LLDP
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	${response}=	API::Send Get Request	tracking/network/lldp	MODE=${MODE}
	[Return]	${response}

API::Get::Tracking::Network::Routing Table
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	${response}=	API::Send Get Request	tracking/network/routingtable	MODE=${MODE}
	[Return]	${response}

API::Get::Tracking::Devices::USB Devices
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	${response}=	API::Send Get Request	tracking/devices/usbdevices	MODE=${MODE}
	[Return]	${response}

API::Get::Tracking::Devices::USB Devices::Name
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	${response}=	API::Send Get Request	tracking/devices/usbdevices/${NAME}	MODE=${MODE}
	[Return]	${response}

API::Get::Tracking::Devices::Serial Statistics
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	${response}=	API::Send Get Request	tracking/devices/serialstats	MODE=${MODE}
	[Return]	${response}

API::Post::Tracking::Devices::Serial Statistics::Reset Statistics
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	${response}=	API::Send Post Request	tracking/devices/serialstats/resetstats	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Tracking::Schedule
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	${response}=	API::Send Get Request	tracking/schedule	MODE=${MODE}
	[Return]	${response}

API::Post::Tracking::Schedule::Reset Logs
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get open sessions info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return open session from NG
	${response}=	API::Send Post Request	tracking/schedule/resetlogs	MODE=${MODE}
	[Return]	${response}

API::Get Network Interfaces
	${response}=	API::Send Get Request	tracking/network/interfaces
	Should Be Equal   ${response.status_code}   ${200}
	${INTERFACE}=	evaluate	str(${response.json()})
	${INTERFACE}=	Remove String	${INTERFACE}	[
	${INTERFACE}=	Remove String	${INTERFACE}	]
	${INTERFACE}	split string	${INTERFACE}	},
	@{INTERFACES}	Create List
	FOR	${I}	IN	@{INTERFACE}
		${COLUMNS}=	Split String	${I}	',
		FOR	${COLUMN}	IN	@{COLUMNS}
			${CONTAINS}	Run Keyword And Return Status	Should Contain	${COLUMN}	'id':
			IF	${CONTAINS}
				${ID}=	Replace String Using Regexp	${COLUMN}	\\s*\{\'id\'\:\\s|\'|\:	${EMPTY}
				Append to List	${INTERFACES}	${ID}
			END
			Exit For Loop If	${CONTAINS}
		END
	END
	[Return]	@{INTERFACES}