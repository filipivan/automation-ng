*** Settings ***
Resource	../../ngm_tests/init.robot

*** Keywords ***

API::Setup HTTP Context
	[Arguments]	${HOST_DIFF}=${HOST}
	[Documentation]
	...	Setup http library used to perform the http request (You MUST use it on requests)
	...	== REQUIREMENTS ==
	...	None
	...	== ARGUMENTS ==
	...	-	HOST Host which the request will be send
	Should Not Be Empty	${HOST_DIFF}	Invalid host
	#robotframework-requests
	${headers}=	API::Get Headers
	Create Session	api	https://${HOST_DIFF}	headers=${headers}	disable_warnings=1

API::Get Headers
	[Arguments]	${HOST_DIFF}=${HOST}	${AUTH_TYPE}=${EMPTY}	${AUTH_KEY}=${EMPTY}	${AUTH_USERNAME}=${EMPTY}	${DEBUG}=False
	[Documentation]
	...	Generates a dictionary to be used as header when executing a http request
	...	== REQUIREMENTS ==
	...	None
	...	== ARGUMENTS ==
	...	-	HOST -	HOST which the request will be send

	IF	'${DEBUG}'=='True'
		Log	HOST_DIFF:${HOST_DIFF} \nAUTH_KEY:${AUTH_KEY} \nAUTH_USERNAME:${AUTH_USERNAME} \nAUTH_TYPE:${AUTH_TYPE}
	END	

	${headers}=	Create Dictionary
	Set To Dictionary	${headers}	Origin=https://${HOST_DIFF}
	Set To Dictionary	${headers}	Referer=https://${HOST_DIFF}/
	Set To Dictionary	${headers}	Accept-Language=en-GB,en;q=0.8,en-US;q=0.6
	Set To Dictionary	${headers}	Accept-Encoding=gzip, deflate, br
	Set To Dictionary	${headers}	Accept=application/json
	Set To Dictionary	${headers}	Cache-control=no-cache
	Set To Dictionary	${headers}	Pragma=no-cache
	Set To Dictionary	${headers}	Content-Type=application/json
	
	IF	'${AUTH_TYPE}'!='${EMPTY}'
		Set To Dictionary	${headers}	${AUTH_TYPE}=${AUTH_KEY}
		Set To Dictionary	${headers}	username=${AUTH_USERNAME}
	END

	IF	'${DEBUG}'=='True'
		Log	HEADER:${headers}
	END
	
	[Return]	${headers}

API::Get Ticket Header
	[Arguments]	${INDEX_TICKET}=0
	[Documentation]
	...	Generates header and adds the ticket from Suite variable ${SESSION_TICKET} on it
	...	== REQUIREMENTS ==
	...	Must be created a Session before with API::Post::Session
	...	== ARGUMENTS ==
	...	-	INDEX_TICKET - index to get ticket from ${SESSION_TICKET} list
	${headers}=	API::Get Headers
	FOR	${S}	IN	${SESSION_TICKET}
		LOG	${S}
	END
	${session}=	Get From List	${SESSION_TICKET}	${INDEX_TICKET}
	${ticket}=	Get From Dictionary	${session}	ticket
	Set To Dictionary	${headers}	ticket=${ticket}
	[Return]	${headers}

API::Log Ticket List
	Log	Tickets: ${SESSION_TICKET}\n	console=yes
	[Return]	${SESSION_TICKET}

API::Get Ticket By Index
	[Arguments]	${INDEX_TICKET}=0
	${session}=	Get From List	${SESSION_TICKET}	${INDEX_TICKET}
	${ticket}=	Get From Dictionary	${session}	ticket
#	Set To Dictionary	${headers}	ticket=${ticket}
	[Return]	${ticket}

API::Get Session By Ticket
	[Arguments]	${TICKET}
	${length}=	Get Length	${SESSION_TICKET}
	:FOR	${index}	IN RANGE 0	${length}
	\	${dict}=	Get From List	${SESSION_TICKET}	${index}
	\	${ticket_from_list}=	Get From Dictionary	${dict}	ticket
	\	${session_from_list}=	Get From Dictionary	${dict}	session
	\	Exit Loop If	${ticket_from_list} == ${TICKET}
	[Return]	${session_from_list}

API::Send Post Request
	[Documentation]
	...	Executes a POST On Session into Nodegrid's API
	...	== REQUIREMENTS ==
	...	When not creating a Session -> must contain an valid ticket in ${SESSION_TICKET}, except if waiting error
	...	== ARGUMENTS ==
	...	-	PATH -	PATH from API with post support to execute request
	...	-	PAYLOAD - dictionary with payload data to send to API
	...	-	INDEX_TICKET - index to get ticket from ${SESSION_TICKET} list
	[Arguments]	${PATH}	${PAYLOAD}=${EMPTY}	${INDEX_TICKET}=0	${RAW_MODE}=no	${MODE}=yes	${ERROR_CONTROL}=${TRUE}	${EXPECT_ERROR}=False
	API::Setup HTTP Context
	${headers}=	Run Keyword If	'${PATH}' != '/Session'	API::Get Ticket Header	${INDEX_TICKET}
	...	ELSE	API::Get Headers
	Run Keyword If	'${MODE}' == 'yes'	Log	\n++++++++++++++++++++++++++++ POST - ${API_VERSION}/${PATH} ++++++++++++++++++++++++++++\n${headers}\n${PAYLOAD}\n	INFO	console=yes
	IF	'${EXPECT_ERROR}'=='False'
		${response}=	POST on Session	api	/api/${API_VERSION}${PATH}	json=${PAYLOAD}	headers=${headers}
	ELSE
		${response}=	Run Keyword And Expect Error	${EXPECT_ERROR}	POST on Session	api	/api/${API_VERSION}${PATH}	json=${PAYLOAD}	headers=${headers}
	END
	Run Keyword If	'${MODE}' == 'yes' and '${EXPECT_ERROR}'=='False'	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${response.text}\n	INFO	console=yes
	Run Keyword If	${ERROR_CONTROL}	API::Error Control	${response}	${RAW_MODE}
	[Return]	${response}

API::Send Get Request
	[Arguments]	${PATH}	${INDEX_TICKET}=0	${RAW_MODE}=no	${MODE}=yes	${PAYLOAD}=${EMPTY}	${ERROR_CONTROL}=${TRUE}	${EXPECT_ERROR}=False
	[Documentation]
	...	Executes a GET request into Nodegrid's API
	...	== REQUIREMENTS ==
	...	Must contain an valid ticket in ${SESSION_TICKET}, except if waiting error
	...	== ARGUMENTS ==
	...	-	PATH -	PATH from API with get support to execute request
	...	-	INDEX_TICKET - index to get ticket from ${SESSION_TICKET} list
	API::Setup HTTP Context
	${headers}=	API::Get Ticket Header	${INDEX_TICKET}
	Run Keyword If	'${MODE}' == 'yes' and '${EXPECT_ERROR}'=='False'	Log	\n++++++++++++++++++++++++++++ GET - ${API_VERSION}/${PATH} ++++++++++++++++++++++++++++\n${headers}\n	INFO	console=yes
	IF	"${PAYLOAD}" == "${EMPTY}"
		IF	'${EXPECT_ERROR}'=='False'
			${response}=	GET on Session	api	/api/${API_VERSION}/${PATH}	headers=${headers}
		ELSE
			${response}=	Run Keyword And Expect Error	${EXPECT_ERROR}	GET on Session	api	/api/${API_VERSION}/${PATH}	headers=${headers}
		END
	ELSE
		IF	'${EXPECT_ERROR}'=='False'
			${response}=	GET on Session	api	/api/${API_VERSION}/${PATH}	headers=${headers}	json=${PAYLOAD}
		ELSE
			${response}=	Run Keyword And Expect Error	${EXPECT_ERROR}	GET on Session	api	/api/${API_VERSION}/${PATH}	headers=${headers}	json=${PAYLOAD}	headers=${headers}
		END
	END
	Run Keyword If	'${MODE}' == 'yes' and '${EXPECT_ERROR}'=='False'	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${response.text}\n	INFO	console=yes
	Run Keyword If	${ERROR_CONTROL}	API::Error Control	${response}	${RAW_MODE}
	[Return]	${response}

API::Send Put Request
	[Arguments]	${PATH}	${PAYLOAD}=${EMPTY}	${INDEX_TICKET}=0	${RAW_MODE}=no	${MODE}=yes	${ERROR_CONTROL}=${TRUE}	${EXPECT_ERROR}=False
	[Documentation]
	...	Executes a PUT request into Nodegrid's API
	...	== REQUIREMENTS ==
	...	Must contain an valid ticket in ${SESSION_TICKET}, except if waiting error
	...	== ARGUMENTS ==
	...	-	PATH -	PATH from API with put support to execute request
	...	-	PAYLOAD - dictionary with payload data to send to API
	...	-	INDEX_TICKET - index to get ticket from ${SESSION_TICKET} list

	API::Setup HTTP Context
	${headers}=	API::Get Ticket Header	${INDEX_TICKET}
	Run Keyword If	'${MODE}' == 'yes' and '${EXPECT_ERROR}'=='False'	Log	\n++++++++++++++++++++++++++++ PUT - ${API_VERSION}/${PATH} ++++++++++++++++++++++++++++\n${headers}\n${PAYLOAD}\n	INFO	console=yes
	IF	'${EXPECT_ERROR}'=='False'
		${response}=	PUT on Session	api	/api/${API_VERSION}${PATH}	data=${PAYLOAD}	headers=${headers}
	ELSE
		${response}=	Run Keyword And Expect Error	${EXPECT_ERROR}	PUT on Session	api	/api/${API_VERSION}${PATH}	data=${PAYLOAD}	headers=${headers}
	END
	Run Keyword If	'${MODE}' == 'yes' and '${EXPECT_ERROR}'=='False'	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${response.text}\n	INFO	console=yes
	Run Keyword If	${ERROR_CONTROL}	API::Error Control	${response}	${RAW_MODE}
	[Return]	${response}

API::Send Delete Request
	[Arguments]	${PATH}	${PAYLOAD}=${EMPTY}	${INDEX_TICKET}=0	${RAW_MODE}=no	${MODE}=yes
	[Documentation]
	...	Executes a DELETE request into Nodegrid's API
	...	== REQUIREMENTS ==
	...	Must contain an valid ticket in ${SESSION_TICKET}, except if waiting error
	...	== ARGUMENTS ==
	...	-	PATH -	PATH from API with delete support to execute request
	...	-	PAYLOAD - dictionary with payload data to send to API
	...	-	INDEX_TICKET - index to get ticket from ${SESSION_TICKET} list
	API::Setup HTTP Context
	${headers}=	API::Get Ticket Header	${INDEX_TICKET}
	Run Keyword If	'${MODE}' == 'yes'	Log	\n+++++++++++++++++++++++++++ DELETE - ${API_VERSION}/${PATH} +++++++++++++++++++++++++++\n${headers}\n${PAYLOAD}\n	INFO	console=yes
	${response}=	DELETE on Session	api	/api/${API_VERSION}${PATH}	data=${PAYLOAD}	headers=${headers}
	Run Keyword If	'${MODE}' == 'yes'	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${response.text}\n	INFO	console=yes
	API::Error Control	${response}	${RAW_MODE}
	[Return]	${response}

API::Check Body Request
	[Documentation]
	...	Executes a Get On Session into Nodegrid's API
	...	== ARGUMENTS ==
	...	-	PATH -	PATH from API with post support to execute request
	...	-	BODY_EXPECTED - a json script with the expected result
	...	== CALL EXAMPLE  ==
	...	{BODY}=	Evaluate   str({'id': '{SUBNET}/${NETMASK}', 'leasetime': '{LEASETIME}''})
	...	API::Check Body Request  PATH=/network/dhcp	BODY_EXPECTED={BODY}
	[Arguments]	${PATH}	${BODY_EXPECTED}	${CONTAIN}=true

 	${response}=   API::Send Get Request  ${PATH}
  ${BODY}=   Evaluate   str(${response.json()})

	IF    '${CONTAIN}' == 'true'
			Should Contain	${BODY}    ${BODY_EXPECTED}
	ELSE
			Should Not Contain    ${BODY}    ${BODY_EXPECTED}
	END
  

API::Get System Name
	${response}=    API::Send Get Request    /system/about
	Should Be Equal   ${response.status_code}   ${200}
	${NAME}=	evaluate	str(${response.json()})
	${NAME}=	Remove String	${NAME}	[
	${NAME}=	Remove String	${NAME}	]
	${COLUMNS}=	Split String	${NAME}	',
	FOR	${COLUMN}	IN	@{COLUMNS}
		${CONTAINS}	Run Keyword And Return Status	Should Contain	${COLUMN}	'system':
		IF	${CONTAINS}
			${NAME}=	Replace String Using Regexp	${COLUMN}	(\\s*\{|\'|system|\:\\s\')	${EMPTY}
			Exit For Loop If	${TRUE}
		END
	END
	LOG	${NAME}
	[Return]	${NAME}

API::Set IDLE Timeout
	[Arguments]	${TIMEOUT_VALUE}=300
	${PAYLOAD}=	Set variable	{"idle_timeout":"${TIMEOUT_VALUE}"}

	${response}=    API::Send Put Request    PAYLOAD=${PAYLOAD}    PATH=/system/preferences
	Should Be Equal   ${response.status_code}   ${200}

API::Is GSR
	${SYSTEM_NAME}	API::Get System Name
	Return From Keyword If	'${SYSTEM_NAME}' == 'Nodegrid Gate SR'	${TRUE}
	[Return]	${FALSE}

API::Is NSR
	${SYSTEM_NAME}	API::Get System Name
	Return From Keyword If	'${SYSTEM_NAME}' == 'Nodegrid Services Router'	${TRUE}
	Return From Keyword If	'${SYSTEM_NAME}' == 'Nodegrid Service Router'	${TRUE}
	Return From Keyword If	'${SYSTEM_NAME}' == 'Nodegrid Net SR'	${TRUE}
	[Return]	${FALSE}

API::Is NGM
	${SYSTEM_NAME}	API::Get System Name
	Return From Keyword If	'${SYSTEM_NAME}' == 'Nodegrid Manager'	${TRUE}
	Return From Keyword If	'${SYSTEM_NAME}' == 'NodeGrid Manager'	${TRUE}
	Return From Keyword If	'${SYSTEM_NAME}' == 'Virtual Machine'	${TRUE}
	[Return]	${FALSE}

API::Has Interface
	[Arguments]	${INTERFACE}
	${response}=	API::Send Get Request	/network/connections
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	${CONTAINS}	Run Keyword and Return Status	Should Contain	${resp}	${INTERFACE}
	[Return]	${CONTAINS}

API::Interface Has IP_Address
	[Arguments]	${INTERFACE}
	${UPPER_CASE}=	Convert To Upper Case	${INTERFACE}
	${LOWER_CASE}=	Convert To Lower Case	${INTERFACE}
	${response}=	API::Send Get Request	/network/connections
	Should Be Equal   ${response.status_code}   ${200}
	${resp}=    Evaluate   str(${response.json()})
	Log    ${resp}
	${CONTAINS}	Run Keyword and Return Status	Should Contain	${resp}	{'id': '${UPPER_CASE}', 'description': '', 'autoConn': 'yes', 'name': '${UPPER_CASE}', 'status': 'connected', 'type': 'ethernet', 'interface': '${INTERFACE}', 'carrier_state': 'up', 'ipv4_address': '192.168.
	[Return]	${CONTAINS}

API::Error Control
	[Arguments]	${RESPONSE}	${RAW_MODE}=no	${ERROR}=Error on page. Please check.
	[Documentation]
	...	Checks requests' response and fails tests when returns error messages, except for RAW_MODE=yes
	...	== REQUIREMENTS ==
	...	Must contain an valid ticket in ${SESSION_TICKET}, except if waiting error
	...	== ARGUMENTS ==
	...	-	PATH -	PATH from API with delete support to execute request
	...	-	PAYLOAD - dictionary with payload data to send to API
	...	-	INDEX_TICKET - index to get ticket from ${SESSION_TICKET} list
	Log	\n++++++++++++++++++++++++++++ Output: ++++++++++++++++++++++++++++\n${response.text}\n	INFO	console=no
	#	CASE RETURN IS NOT JSON
	#		Example: ERROR IN CONNECTION
	#	response = <html>...503 Service Unavailable...<html>\n
	${CONTAIN_JSON}=	Run Keyword and Return Status	Set Variable	${RESPONSE.json()}
	Run Keyword If	${CONTAIN_JSON} == ${FALSE}	 Fatal Error	${RESPONSE}

	#	CASE RETURN IS AN EMPTY ARRAY
	#		Example: AFTER RESET DISCOVERY LOGS
	#	response = []
	${length}=	Get Length	${RESPONSE.json()}
	${is_array_empty}=	Run Keyword And Return Status	Should Be Equal as Integers	${length}	0

	#	CASE RETURN CONTAIN MESSAGE DIRECT IN DICT
	#		EXAMPLE: AFTER PASSING AN INVALID TICKET
	#			OR
	#		TRYING TO LOGIN WITH WRONG CREDENTIALS
	#	response = { ... , message: "xxx" }
	${contain_dict_message}=	Run Keyword and Return Status	Get From Dictionary	${RESPONSE.json()}	message

	#	CASE RETURN CONTAIN MESSAGE IN LIST
	#		EXAMPLE: AFTER PASSING AN INVALID VALUE TO FIELDS
	#	response = [ { ... , message: "xxx" } ] OR [ {... , message: "xxx" }, {...}, ...]
	${contain_list}=	Run Keyword and Return Status	Get From List	${RESPONSE.json()}	0
	${dict_list_message}=	Run Keyword If	${contain_list}	Get From List	${RESPONSE.json()}	0
	${contain_list_message}=	Run Keyword and Return Status	Get From Dictionary	${dict_list_message}	message

	${message}=	Run Keyword If	${contain_dict_message}	Get From Dictionary	${RESPONSE.json()}	message
	...	ELSE	Run Keyword If	${contain_list_message}	Get From Dictionary	${dict_list_message}	message

	Run Keyword If	'${RAW_MODE}'=='no'	Should Not Be Equal	'${ERROR}'	'${message}'
	[Return]	${RESPONSE}

#
#	GLOBAL HELPERS
#
API::Global::Get ID
	[Arguments]	${PATH}	${ELEMENT}	${MODE}=no
	${elements_list}=	API::Get::${PATH}	MODE=${MODE}
	${elements_list}=	Set Variable	${elements_list.json()}
	${id}=	Set Variable	${EMPTY}
	:FOR	${elements_dict}	IN	@{elements_list}
	\	${id_dict}=	Get From Dictionary	${elements_dict}	id
	\	${id}=	Set Variable If	'${ELEMENT}' == '${id_dict}'	${id_dict}	${EMPTY}
	\	Exit For Loop If	'${ELEMENT}' == '${id_dict}'
	[Return]	${id}

API::Global::Check
	[Arguments]	${PATH}	${ELEMENT}
	${id}=	API::Global::Get ID	${PATH}	${ELEMENT}
	${result}=	Set Variable If	'${id}' == 'False'	${FALSE}	${TRUE}
	[Return]	${result}

API::Global::Should Not Exist
	[Arguments]	${PATH}	${ELEMENT}
	${exist}=	API::Global::Check	${PATH}	${ELEMENT}
	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	[Return]	${result}

API::Global::Should Not Exists
	[Arguments]	${PATH}	@{ELEMENTS}
	${response}=	Create List
	:FOR	${ELEMENT}	IN	@{ELEMENTS}
	\	${exist}=	API::Global::Check	${PATH}	${ELEMENT}
	\	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}

API::Global::Should Exist
	[Arguments]	${PATH}	${ELEMENT}
	${exist}=	API::Global::Check	${PATH}	${ELEMENT}
	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	[Return]	${result}

API::Global::Should Exists
	[Arguments]	${PATH}	@{ELEMENTS}
	${response}=	Create List
	:FOR	${ELEMENT}	IN	@{ELEMENTS}
	\	${exist}=	API::Global::Check	${PATH}	${ELEMENT}
	\	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}

API::Global::Delete If Exists
	[Arguments]	${PATH}	${NAME}	@{ELEMENTS}
	${elements_id}=	Create List
	:FOR	${ELEMENT}	IN	@{ELEMENTS}
	\	${id}=	API::Aux::Get ID	${PATH}	${ELEMENT}
#
# TESSSTTTT
#
	\	Run keyword If	${id}	Append to List	${elements_id}	${id}
	${payload}=	API::JSON::Delete	${NAME}	@{elements_id}
	${length}=	Get Length	${elements_id}
	${response}=	Run Keyword If	'${length}' > '0'	API::Delete::${PATH}	${payload}
	[Return]	${response}

#
#	AUXILIATIVE KEYWORDS
#
API::JSON::Delete
	[Arguments]	${NAME}	@{ELEMENTS}
	${JSON}=	Create Dictionary
	Set To Dictionary	${JSON}	${NAME}=${ELEMENTS}
	[Return]	${JSON}

#
#	System::Licenses
#
API::Add System::License
	[Arguments]	${LICENSE_KEY}=${EMPTY}
	${payload}=	Create Dictionary	license_key=${LICENSE_KEY}
	${response}=	API::Post::System::Licenses	${payload}
	[Return]	${response}

API::Add System::Licenses
	[Arguments]	@{LICENSES_KEYS}
	${response}=	Create List
	:FOR	${LICENSE_KEY}	IN	@{LICENSES_KEYS}
	\	${payload}=	Create Dictionary	license_key=${LICENSE_KEY}
	\	${result}=	API::Post::System::Licenses	${payload}
	\	Append to List	${response}	${result}
	[Return]	${response}

API::Delete System::License
	[Arguments]	${LICENSE_KEY}
	${id}=	API::Aux::Get ID System::License	${LICENSE_KEY}
	${payload}=	API::JSON::Delete	licenses	${id}
	${response}=	API::Delete::System::Licenses	${payload}
	[Return]	${response}

API::Delete System::Licenses
	[Arguments]	@{LICENSES_KEYS}
	${licenses_id}=	Create List
	:FOR	${LICENSE_KEY}	IN	@{LICENSES_KEYS}
	\	${id}=	API::Aux::Get ID System::License	${LICENSE_KEY}
	\	Append to List	${LICENSES_ID}	${id}
	${payload}=	API::JSON::Delete	licenses	@{licenses_id}
	${response}=	API::Delete::System::Licenses	${payload}
	[Return]	${response}

API::Delete If Exists System::Licenses
	[Arguments]	@{LICENSES_KEYS}
	${licenses_id}=	Create List
	:FOR	${LICENSE_KEY}	IN	@{LICENSES_KEYS}
	\	${id}=	API::Aux::Get ID System::License	${LICENSE_KEY}
#
# TESSSTTTT
#
	\	Run keyword If	${id}	Append to List	${licenses_id}	${id}
	${payload}=	API::JSON::Delete	licenses	@{licenses_id}
	${length}=	Get Length	${licenses_id}
	${response}=	Run Keyword If	'${length}' > '0'	API::Delete::System::Licenses	${payload}
	[Return]	${response}

#
#	Auxiliary::System::Licenses
#
API::Aux::Get ID System::License
	[Arguments]	${LICENSE_KEY}	${MODE}=no
	Return From Keyword If	'-' not in '${LICENSE_KEY}'	${FALSE}
	${license_part}=	Fetch from Right	${LICENSE_KEY}	-
	${license_part}=	Set Variable	xxxxx-xxxxx-xxxxx-${license_part}
	${licenses_list}=	API::Get::System::Licenses	MODE=${MODE}
	${licenses_list}=	Set Variable	${licenses_list.json()}
	${id}=	Set Variable	${FALSE}
	:FOR	${license_dict}	IN	@{licenses_list}
	\	${id}=	Get From Dictionary	${license_dict}	id
	\	Exit For Loop If	'${license_part}' in '${id}'
	[Return]	${id}

API::Aux::Check System::License
	[Arguments]	${LICENSE_KEY}
	${license_id}=	API::Aux::Get ID System::License	${LICENSE_KEY}
	${result}=	Set Variable If	'${license_id}' == 'False'	${FALSE}	${TRUE}
	[Return]	${result}

API::Aux::Should Not Exist System::License
	[Arguments]	${LICENSE_KEY}
	${exist}=	API::Aux::Check System::License	${LICENSE_KEY}
	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	[Return]	${result}

API::Aux::Should Not Exist System::Licenses
	[Arguments]	@{LICENSE_KEYS}
	${response}=	Create List
	:FOR	${LICENSE_KEY}	IN	@{LICENSE_KEYS}
	\	${exist}=	API::Aux::Check System::License	${LICENSE_KEY}
	\	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}

API::Aux::Should Exist System::License
	[Arguments]	${LICENSE_KEY}
	${exist}=	API::Aux::Check System::License	${LICENSE_KEY}
	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	[Return]	${result}

API::Aux::Should Exist System::Licenses
	[Arguments]	@{LICENSE_KEYS}
	${response}=	Create List
	:FOR	${LICENSE_KEY}	IN	@{LICENSE_KEYS}
	\	${exist}=	API::Aux::Check System::License	${LICENSE_KEY}
	\	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}

#
#	System::Custom Fields
#
API::Add System::Custom Field
	[Arguments]	${FIELD_NAME}=${EMPTY}	${FIELD_VALUE}=${EMPTY}	${RAW_MODE}=no
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	field_name=${FIELD_NAME}
	Set To Dictionary	${payload}	field_value=${FIELD_VALUE}
	${response}=	API::Post::System::Custom Fields	${payload}	RAW_MODE=${RAW_MODE}
	[Return]	${response}

#	TODO CHECK STRUCTURE
API::Add System::Custom Fields
	[Arguments]	${FIELD_NAMES}	${FIELD_VALUES}
	${length_names}=	Get Length	${FIELD_NAMES}
	${length_values}=	Get Length	${FIELD_VALUES}
	Should Be Equal	${length_names}	${length_values}

	${response}=	Create List
	:FOR	${index}	IN RANGE	@{length_names}
	\	${payload}=	Create Dictionary
	\	${field_name}=	Get From List	${FIELD_NAMES}	${index}
	\	${field_value}=	Get From List	${FIELD_VALUES}	${index}
	\	Set To Dictionary	${payload}	field_name=${field_name}
	\	Set To Dictionary	${payload}	field_value=${field_value}
	\	${result}=	API::Post::System::Custom Fields	${payload}
	\	Append to List	${response}	${result}
	[Return]	${response}

#Does the same the keyword bellow, but I wanna maintain the structure one-many
API::Delete System::Custom Field
	[Arguments]	${CUSTOM_FIELD}
	${payload}=	API::JSON::Delete	custom_fields	${CUSTOM_FIELD}
	${response}=	API::Delete::System::Custom Fields	${payload}
	[Return]	${response}

#Does the same the keyword 1_up, but I wanna maintain the structure one-many
API::Delete System::Custom Fields
	[Arguments]	@{CUSTOM_FIELDS}
	${payload}=	API::JSON::Delete	custom_fields	@{CUSTOM_FIELDS}
	${response}=	API::Delete::System::Custom Fields	${payload}
	[Return]	${response}

API::Delete If Exists System::Custom Fields
	[Arguments]	@{CUSTOM_FIELDS}
	${customfields_id}=	Create List
	:FOR	${CUSTOM_FIELD}	IN	@{CUSTOM_FIELDS}
	\	${id}=	API::Aux::Get ID System::Custom Field	${CUSTOM_FIELD}
#
# TESSSTTT
#
	\	Run keyword If	${id}	Append to List	${customfields_id}	${id}
	${payload}=	API::JSON::Delete	custom_fields	@{customfields_id}
	${length}=	Get Length	${customfields_id}
	${response}=	Run Keyword If	'${length}' > '0'	API::Delete::System::Custom Fields	${payload}
	[Return]	${response}

#
#	Auxiliary::System::Custom Fields
#
API::Aux::Get ID System::Custom Field
	[Arguments]	${CUSTOM_FIELD}	${MODE}=no
	${customfields_list}=	API::Get::System::Custom Fields	MODE=${MODE}
	${customfields_list}=	Set Variable	${customfields_list.json()}
	${id}=	Set Variable	${FALSE}
	:FOR	${customfields_dict}	IN	@{customfields_list}
	\	${id_dict}=	Get From Dictionary	${customfields_dict}	id
	\	${id}=	Set Variable If	'${CUSTOM_FIELD}' == '${id_dict}'	${id_dict}	${FALSE}
	\	Exit For Loop If	'${CUSTOM_FIELD}' == '${id_dict}'
	[Return]	${id}

API::Aux::Check System::Custom Field
	[Arguments]	${CUSTOM_FIELD}
	${id}=	API::Aux::Get ID System::Custom Field	${CUSTOM_FIELD}
	${result}=	Set Variable If	'${id}' == 'False'	${FALSE}	${TRUE}
	[Return]	${result}

API::Aux::Should Not Exist System::Custom Field
	[Arguments]	${CUSTOM_FIELD}
	${exist}=	API::Aux::Check System::Custom Field	${CUSTOM_FIELD}
	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	[Return]	${result}

API::Aux::Should Not Exist System::Custom Fields
	[Arguments]	@{CUSTOM_FIELDS}
	${response}=	Create List
	:FOR	${CUSTOM_FIELD}	IN	@{CUSTOM_FIELDS}
	\	${exist}=	API::Aux::Check System::Custom Field	${CUSTOM_FIELD}
	\	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}

API::Aux::Should Exist System::Custom Field
	[Arguments]	${CUSTOM_FIELD}
	${exist}=	API::Aux::Check System::Custom Field	${CUSTOM_FIELD}
	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	[Return]	${result}

API::Aux::Should Exist System::Custom Fields
	[Arguments]	@{CUSTOM_FIELDS}
	${response}=	Create List
	:FOR	${CUSTOM_FIELD}	IN	@{CUSTOM_FIELDS}
	\	${exist}=	API::Aux::Check System::Custom Field	${CUSTOM_FIELD}
	\	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}

#
#	System::Dial Up::Callback Users
#
API::Add System::Dial Up::Callback User
	[Arguments]	${CALLBACK_USER}=${EMPTY}	${CALLBACK_NUMBER}=${EMPTY}	${MODE}=no
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	callback_user=${CALLBACK_USER}
	Set To Dictionary	${payload}	callback_number=${CALLBACK_NUMBER}
	${response}=	API::Post::System::Dial Up::Callback Users	${payload}	MODE=${MODE}
	[Return]	${response}

#	TODO CHECK STRUCTURE
API::Add System::Dial Up::Callback Users
	[Arguments]	${CALLBACK_USERS}	${CALLBACK_NUMBERS}	${MODE}=no
	${length_callbackusers}=	Get Length	${CALLBACK_USERS}
	${length_callbacknumber}=	Get Length	${CALLBACK_NUMBERS}
	Should Be Equal	${length_callbackusers}	${length_callbacknumber}

	${response}=	Create List
	:FOR	${index}	IN RANGE	@{length_callbackusers}
	\	${payload}=	Create Dictionary
	\	${callback_user}=	Get From List	${CALLBACK_USERS}	${index}
	\	${callback_number}=	Get From List	${CALLBACK_NUMBERS}	${index}
	\	Set To Dictionary	${payload}	callback_user=${callback_user}
	\	Set To Dictionary	${payload}	callback_number=${callback_number}
	\	${result}=	API::Post::System::Dial Up::Callback Users	${payload}	MODE=${MODE}
	\	Append to List	${response}	${result}
	[Return]	${response}

#Does the same the keyword bellow, but I wanna maintain the structure one-many
API::Delete System::Dial Up::Callback User
	[Arguments]	${CALLBACK_USER}	${MODE}=no
	${payload}=	API::JSON::Delete	callback_users	${CALLBACK_USER}
	${response}=	API::Delete::System::Dial Up::Callback Users	${payload}	MODE=${MODE}
	[Return]	${response}

#Does the same the keyword 1_up, but I wanna maintain the structure one-many
API::Delete System::Dial Up::Callback Users
	[Arguments]	@{CALLBACK_USERS}
	${payload}=	API::JSON::Delete	callback_users	@{CALLBACK_USERS}
	${response}=	API::Delete::System::Dial Up::Callback Users	${payload}
	[Return]	${response}

API::Delete If Exists System::Dial Up::Callback Users
	[Arguments]	@{CALLBACK_USERS}
	${callbackusers_id}=	Create List
	:FOR	${CALLBACK_USER}	IN	@{CALLBACK_USERS}
	\	${id}=	API::Aux::Get ID System::Dial Up::Callback User	${CALLBACK_USER}
	\	Run keyword If	'${id}'!='${FALSE}'	Append to List	${callbackusers_id}	${id}
	${payload}=	API::JSON::Delete	callback_users	@{callbackusers_id}
	${length}=	Get Length	${callbackusers_id}
	${response}=	Run Keyword If	'${length}' > '0'	API::Delete::System::Dial Up::Callback Users	${payload}
	[Return]	${response}

#
#	Auxiliary::System::Dial Up::Callback Users
#
API::Aux::Get ID System::Dial Up::Callback User
	[Arguments]	${CALLBACK_USER}	${MODE}=no
	${callbackusers_list}=	API::Get::System::Dial Up::Callback Users	MODE=${MODE}
	${callbackusers_list}=	Set Variable	${callbackusers_list.json()}
	${id}=	Set Variable	${FALSE}
	:FOR	${callbackusers_dict}	IN	@{callbackusers_list}
	\	${id_dict}=	Get From Dictionary	${callbackusers_dict}	id
	\	${id}=	Set Variable If	'${CALLBACK_USER}' == '${id_dict}'	${id_dict}	${FALSE}
	\	Exit For Loop If	'${CALLBACK_USER}' == '${id_dict}'
	[Return]	${id}

API::Aux::Check System::Dial Up::Callback User
	[Arguments]	${CALLBACK_USER}
	${id}=	API::Aux::Get ID System::Dial Up::Callback User	${CALLBACK_USER}
	${result}=	Set Variable If	'${id}' == 'False'	${FALSE}	${TRUE}
	[Return]	${result}

API::Aux::Should Not Exist System::Dial Up::Callback User
	[Arguments]	${CALLBACK_USER}
	${exist}=	API::Aux::Check System::Dial Up::Callback User	${CALLBACK_USER}
	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	[Return]	${result}

API::Aux::Should Not Exist System::Dial Up::Callback Users
	[Arguments]	@{CALLBACK_USERS}
	${response}=	Create List
	:FOR	${CUSTOM_FIELD}	IN	@{CALLBACK_USERS}
	\	${exist}=	API::Aux::Check System::Dial Up::Callback User	${CALLBACK_USER}
	\	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}

API::Aux::Should Exist System::Dial Up::Callback User
	[Arguments]	${CALLBACK_USER}
	${exist}=	API::Aux::Check System::Dial Up::Callback User	${CALLBACK_USER}
	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	[Return]	${result}

API::Aux::Should Exist System::Dial Up::Callback Users
	[Arguments]	@{CALLBACK_USERS}
	${response}=	Create List
	:FOR	${CALLBACK_USER}	IN	@{CALLBACK_USERS}
	\	${exist}=	API::Aux::Check System::Dial Up::Callback User	${CALLBACK_USER}
	\	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}

#
#	System::Schedule
#
#	TODO CHECK STRUCTURE
API::Add System::Schedule
	[Arguments]	${SCHEDULE_FIELDS}	${SCHEDULE_VALUES}	${MODE}=no
	${length_schedulefields}=	Get Length	${SCHEDULE_FIELDS}
	${length_schedulevalues}=	Get Length	${SCHEDULE_VALUES}
	Should Be Equal	${length_schedulefields}	${length_schedulevalues}

	${payload}=	Create Dictionary
	:FOR	${index}	IN RANGE	@{length_schedulefields}
	\	${schedule_field}=	Get From List	${SCHEDULE_FIELDS}	${index}
	\	${schedule_value}=	Get From List	${SCHEDULE_VALUES}	${index}
	\	Set To Dictionary	${payload}	${schedule_field}=${schedule_value}
	${response}=	API::Post::System::Schedule	${payload}	MODE=${MODE}
	[Return]	${response}

#Does the same the keyword bellow, but I wanna maintain the structure one-many
API::Delete System::Schedule
	[Arguments]	${SCHEDULE}	${MODE}=no
	${payload}=	API::JSON::Delete	schedules	${SCHEDULE}
	${response}=	API::Delete::System::Schedule	${payload}	MODE=${MODE}
	[Return]	${response}

#Does the same the keyword 1_up, but I wanna maintain the structure one-many
API::Delete System::Schedules
	[Arguments]	@{SCHEDULES}
	${payload}=	API::JSON::Delete	schedules	@{SCHEDULES}
	${response}=	API::Delete::System::Schedule	${payload}
	[Return]	${response}

API::Delete If Exists System::Schedules
	[Arguments]	@{SCHEDULES}
	${schedules_id}=	Create List
	:FOR	${SCHEDULE}	IN	@{SCHEDULES}
	\	${id}=	API::Aux::Get ID System::Schedule	${SCHEDULE}
	\	Run keyword If	'${id}'!='${FALSE}'	Append to List	${schedules_id}	${id}
	${payload}=	API::JSON::Delete	schedules	@{schedules_id}
	${length}=	Get Length	${schedules_id}
	${response}=	Run Keyword If	'${length}' > '0'	API::Delete::System::Schedule	${payload}
	[Return]	${response}

#
#	Auxiliary::System::Schedule
#
API::Aux::Get ID System::Schedule
	[Arguments]	${SCHEDULE}	${MODE}=no
	${schedules_list}=	API::Get::System::Schedule	MODE=${MODE}
	${schedules_list}=	Set Variable	${schedules_list.json()}
	${id}=	Set Variable	${FALSE}
	:FOR	${schedules_dict}	IN	@{schedules_list}
	\	${id_dict}=	Get From Dictionary	${schedules_dict}	id
	\	${id}=	Set Variable If	'${SCHEDULE}' == '${id_dict}'	${id_dict}	${FALSE}
	\	Exit For Loop If	'${SCHEDULE}' == '${id_dict}'
	[Return]	${id}

API::Aux::Check System::Schedule
	[Arguments]	${SCHEDULE}
	${id}=	API::Aux::Get ID System::Schedule	${SCHEDULE}
	${result}=	Set Variable If	'${id}' == 'False'	${FALSE}	${TRUE}
	[Return]	${result}

API::Aux::Should Not Exist System::Schedule
	[Arguments]	${SCHEDULE}
	${exist}=	API::Aux::Check System::Schedule	${SCHEDULE}
	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	[Return]	${result}

API::Aux::Should Not Exist System::Schedules
	[Arguments]	@{SCHEDULES}
	${response}=	Create List
	:FOR	${SCHEDULE}	IN	@{SCHEDULES}
	\	${exist}=	API::Aux::Check System::Schedule	${SCHEDULE}
	\	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}

API::Aux::Should Exist System::Schedule
	[Arguments]	${SCHEDULE}
	${exist}=	API::Aux::Check System::Schedule	${SCHEDULE}
	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	[Return]	${result}

API::Aux::Should Exist System::Schedules
	[Arguments]	@{SCHEDULES}
	${response}=	Create List
	:FOR	${SCHEDULE}	IN	@{SCHEDULES}
	\	${exist}=	API::Aux::Check System::Schedule	${SCHEDULE}
	\	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}

#
#	System::Sms::Whitelists
#
API::Add System::Sms::Whitelist
	[Arguments]	${NAME}=${EMPTY}	${NUMBER}=${EMPTY}	${RAW_MODE}=no
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	name=${NAME}
	Set To Dictionary	${payload}	phone_number=${NUMBER}
	${response}=	API::Post::System::Sms::Whitelist	${payload}	${RAW_MODE}
	[Return]	${response}

#	TODO CHECK STRUCTURE
API::Add System::Sms::Whitelists
	[Arguments]	${FIELD_NAMES}	${FIELD_VALUES}
	${length_names}=	Get Length	${FIELD_NAMES}
	${length_values}=	Get Length	${FIELD_VALUES}
	Should Be Equal	${length_names}	${length_values}

	${response}=	Create List
	:FOR	${index}	IN RANGE	@{length_names}
	\	${payload}=	Create Dictionary
	\	${name}=	Get From List	${FIELD_NAMES}	${index}
	\	${phone_number}=	Get From List	${FIELD_VALUES}	${index}
	\	Set To Dictionary	${payload}	name=${name}
	\	Set To Dictionary	${payload}	phone_number=${phone_number}
	\	${result}=	API::Post::System::Sms::Whitelist	${payload}
	\	Append to List	${response}	${result}
	[Return]	${response}

#Does the same the keyword bellow, but I wanna maintain the structure one-many
API::Delete System::Sms::Whitelist
	[Arguments]	${WHITELIST}	${MODE}=yes
	${payload}=	API::JSON::Delete	whitelists	${WHITELIST}
	${response}=	API::Delete::System::Sms::Whitelist	${payload}	MODE=${MODE}
	[Return]	${response}

#Does the same the keyword 1_up, but I wanna maintain the structure one-many
API::Delete System::Sms::Whitelists
	[Arguments]	@{WHITELISTS}
	${payload}=	API::JSON::Delete	whitelists	@{WHITELISTS}
	${response}=	API::Delete::System::Sms::Whitelist	${payload}
	[Return]	${response}

API::Delete If Exists System::Sms::Whitelists
	[Arguments]	@{WHITELISTS}
	${whitelist_id}=	Create List
	:FOR	${WHITELIST}	IN	@{WHITELISTS}
	\	${id}=	API::Aux::Get ID System::Sms::Whitelist	${WHITELIST}
	\	Run keyword If	'${id}' != 'False'	Append to List	${whitelist_id}	${id}
	${payload}=	API::JSON::Delete	whitelists	@{whitelist_id}
	${length}=	Get Length	${whitelist_id}
	${response}=	Run Keyword If	'${length}' > '0'	API::Delete::System::Sms::Whitelist	${payload}
	[Return]	${response}

#
#	Auxiliary::System::Sms::Whitelists
#
API::Aux::Get ID System::Sms::Whitelist
	[Arguments]	${WHITELIST}	${MODE}=no
	${whitelist_list}=	API::Get::System::Sms::Whitelist	MODE=${MODE}
	${whitelist_list}=	Set Variable	${whitelist_list.json()}
	${id}=	Set Variable	${FALSE}
	:FOR	${whitelist_dict}	IN	@{whitelist_list}
	\	${id_dict}=	Get From Dictionary	${whitelist_dict}	id
	\	${id}=	Set Variable If	'${WHITELIST}' == '${id_dict}'	${id_dict}	${FALSE}
	\	Exit For Loop If	'${WHITELIST}' == '${id_dict}'
	[Return]	${id}

API::Aux::Check System::Sms::Whitelist
	[Arguments]	${WHITELIST}
	${id}=	API::Aux::Get ID System::Sms::Whitelist	${WHITELIST}
	${result}=	Set Variable If	'${id}' == 'False'	${FALSE}	${TRUE}
	[Return]	${result}

API::Aux::Should Not Exist System::Sms::Whitelist
	[Arguments]	${WHITELIST}
	${exist}=	API::Aux::Check System::Sms::Whitelist	${WHITELIST}
	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	[Return]	${result}

API::Aux::Should Not Exist System::Sms::Whitelists
	[Arguments]	@{WHITELISTS}
	${response}=	Create List
	:FOR	${WHITELIST}	IN	@{WHITELISTS}
	\	${exist}=	API::Aux::Check System::Sms::Whitelist	${WHITELIST}
	\	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}

API::Aux::Should Exist System::Sms::Whitelist
	[Arguments]	${WHITELIST}
	${exist}=	API::Aux::Check System::Sms::Whitelist	${WHITELIST}
	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	[Return]	${result}

API::Aux::Should Exist System::Sms::Whitelists
	[Arguments]	@{WHITELISTS}
	${response}=	Create List
	:FOR	${WHITELIST}	IN	@{WHITELISTS}
	\	${exist}=	API::Aux::Check System::Sms::Whitelist	${WHITELIST}
	\	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}





#
#	Network::Connections
#
#	TODO CHECK STRUCTURE
API::Add Network::Connections
	[Arguments]	${CONNECTION_FIELDS}	${CONNECTION_VALUES}	${MODE}=no
	${length_connectionfields}=	Get Length	${CONNECTION_FIELDS}
	${length_connectionvalues}=	Get Length	${CONNECTION_VALUES}
	Should Be Equal	${length_connectionfields}	${length_connectionvalues}

	${payload}=	Create Dictionary
	:FOR	${index}	IN RANGE	@{length_connectionfields}
	\	${connection_field}=	Get From List	${CONNECTION_FIELDS}	${index}
	\	${connection_value}=	Get From List	${CONNECTION_VALUES}	${index}
	\	Set To Dictionary	${payload}	${connection_field}=${connection_value}
	${response}=	API::Post::Network::Connections	${payload}	MODE=${MODE}
	[Return]	${response}

#Does the same the keyword bellow, but I wanna maintain the structure one-many
API::Delete Network::Connection
	[Arguments]	${CONNECTION}	${MODE}=no
	${payload}=	API::JSON::Delete	connections	${CONNECTION}
	${response}=	API::Delete::Network::Connections	${payload}	MODE=${MODE}
	[Return]	${response}

#Does the same the keyword 1_up, but I wanna maintain the structure one-many
API::Delete Network::Connections
	[Arguments]	@{CONNECTIONS}
	${payload}=	API::JSON::Delete	connections	@{CONNECTIONS}
	${response}=	API::Delete::Network::Connections	${payload}
	[Return]	${response}

API::Delete If Exists Network::Connections
	[Arguments]	@{CONNECTIONS}
	${connections_id}=	Create List
	:FOR	${CONNECTION}	IN	@{CONNECTIONS}
	\	${id}=	API::Aux::Get ID Network::Connection	${CONNECTION}
	\	Run keyword If	'${id}'!='${FALSE}'	Append to List	${connections_id}	${id}
	${payload}=	API::JSON::Delete	connections	@{connections_id}
	${length}=	Get Length	${connections_id}
	${response}=	Run Keyword If	'${length}' > '0'	API::Delete::Network::Connections	${payload}
	[Return]	${response}

#
#	Auxiliary::Network::Connections
#
API::Aux::Get ID Network::Connection
	[Arguments]	${CONNECTION}	${MODE}=no
	${connections_list}=	API::Get::Network::Connections	MODE=${MODE}
	${connections_list}=	Set Variable	${connections_list.json()}
	${id}=	Set Variable	${FALSE}
	:FOR	${connections_dict}	IN	@{connections_list}
	\	${id_dict}=	Get From Dictionary	${connections_dict}	id
	\	${id}=	Set Variable If	'${CONNECTION}' == '${id_dict}'	${id_dict}	${FALSE}
	\	Exit For Loop If	'${CONNECTION}' == '${id_dict}'
	[Return]	${id}

API::Aux::Check Network::Connection
	[Arguments]	${CONNECTION}
	${id}=	API::Aux::Get ID Network::Connection	${CONNECTION}
	${result}=	Set Variable If	'${id}' == 'False'	${FALSE}	${TRUE}
	[Return]	${result}

API::Aux::Should Not Exist Network::Connection
	[Arguments]	${CONNECTION}
	${exist}=	API::Aux::Check Network::Connection	${CONNECTION}
	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	[Return]	${result}

API::Aux::Should Not Exist Network::Connections
	[Arguments]	@{CONNECTIONS}
	${response}=	Create List
	:FOR	${CONNECTION}	IN	@{CONNECTIONS}
	\	${exist}=	API::Aux::Check Network::Connection	${CONNECTION}
	\	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}

API::Aux::Should Exist Network::Connection
	[Arguments]	${CONNECTION}
	${exist}=	API::Aux::Check Network::Connection	${CONNECTION}
	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	[Return]	${result}

API::Aux::Should Exist Network::Connections
	[Arguments]	@{CONNECTIONS}
	${response}=	Create List
	:FOR	${CONNECTION}	IN	@{CONNECTIONS}
	\	${exist}=	API::Aux::Check Network::Connection	${CONNECTION}
	\	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}





#
#	Network::Switch::Vlans
#
API::Add Network::Switch::Vlan
	[Arguments]	${NAME}=${EMPTY}	${UNTAGGED}=${EMPTY}	${TAGGED}=${EMPTY}	${RAW_MODE}=no
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	vlan=${NAME}
	Run Keyword If	'${NAME}'!='${EMPTY}'	Set To Dictionary	${payload}	untagged_ports=${UNTAGGED}
	Run Keyword If	'${NAME}'!='${EMPTY}'	Set To Dictionary	${payload}	tagged_ports=${TAGGED}
	${response}=	API::Post::Network::Switch::Vlan	${payload}	${RAW_MODE}
	[Return]	${response}

#Does the same the keyword bellow, but I wanna maintain the structure one-many
API::Delete Network::Switch::Vlan
	[Arguments]	${VLAN}	${MODE}=yes
	${payload}=	API::JSON::Delete	vlans	${VLAN}
	${response}=	API::Delete::Network::Switch::Vlan	${payload}	MODE=${MODE}
	[Return]	${response}

#Does the same the keyword 1_up, but I wanna maintain the structure one-many
API::Delete Network::Switch::Vlans
	[Arguments]	@{VLANS}
	${payload}=	API::JSON::Delete	vlans	@{VLANS}
	${response}=	API::Delete::Network::Switch::Vlan	${payload}
	[Return]	${response}

API::Delete If Exists Network::Switch::Vlans
	[Arguments]	@{VLANS}
	${vlan_id}=	Create List
	:FOR	${VLAN}	IN	@{VLANS}
	\	${id}=	API::Aux::Get ID Network::Switch::Vlan	${VLAN}
	\	Run keyword If	'${id}' != 'False'	Append to List	${vlan_id}	${id}
	${payload}=	API::JSON::Delete	vlans	@{vlan_id}
	${length}=	Get Length	${vlan_id}
	${response}=	Run Keyword If	'${length}' > '0'	API::Delete::Network::Switch::Vlan	${payload}
	[Return]	${response}

#
#	Auxiliary::Network::Switch::Vlans
#
API::Aux::Get ID Network::Switch::Vlan
	[Arguments]	${VLAN}	${MODE}=no
	${vlan_list}=	API::Get::Network::Switch::Vlan	MODE=${MODE}
	${vlan_list}=	Set Variable	${vlan_list.json()}
	${id}=	Set Variable	${FALSE}
	:FOR	${vlan_dict}	IN	@{vlan_list}
	\	${id_dict}=	Get From Dictionary	${vlan_dict}	id
	\	${id}=	Set Variable If	'${VLAN}' == '${id_dict}'	${id_dict}	${FALSE}
	\	Exit For Loop If	'${VLAN}' == '${id_dict}'
	[Return]	${id}

API::Aux::Check Network::Switch::Vlan
	[Arguments]	${VLAN}
	${id}=	API::Aux::Get ID Network::Switch::Vlan	${VLAN}
	${result}=	Set Variable If	'${id}' == 'False'	${FALSE}	${TRUE}
	[Return]	${result}

API::Aux::Should Not Exist Network::Switch::Vlan
	[Arguments]	${VLAN}
	${exist}=	API::Aux::Check Network::Switch::Vlan	${VLAN}
	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	[Return]	${result}

API::Aux::Should Not Exist Network::Switch::Vlans
	[Arguments]	@{VLANS}
	${response}=	Create List
	:FOR	${VLAN}	IN	@{VLANS}
	\	${exist}=	API::Aux::Check Network::Switch::Vlan	${VLAN}
	\	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}

API::Aux::Should Exist Network::Switch::Vlan
	[Arguments]	${VLAN}
	${exist}=	API::Aux::Check Network::Switch::Vlan	${VLAN}
	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	[Return]	${result}

API::Aux::Should Exist Network::Switch::Vlans
	[Arguments]	@{VLANS}
	${response}=	Create List
	:FOR	${VLAN}	IN	@{VLANS}
	\	${exist}=	API::Aux::Check Network::Switch::Vlan	${VLAN}
	\	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}





#
#	Network::Static Routes
#
#	TODO CHECK STRUCTURE
API::Add Network::Static Routes
	[Arguments]	${STATICROUTE_FIELDS}	${STATICROUTE_VALUES}	${MODE}=no
	${length_staticroutefields}=	Get Length	${STATICROUTE_FIELDS}
	${length_staticroutevalues}=	Get Length	${STATICROUTE_VALUES}
	Should Be Equal	${length_staticroutefields}	${length_staticroutevalues}

	${payload}=	Create Dictionary
	:FOR	${index}	IN RANGE	@{length_staticroutefields}
	\	${staticroute_field}=	Get From List	${STATICROUTE_FIELDS}	${index}
	\	${staticroute_value}=	Get From List	${STATICROUTE_VALUES}	${index}
	\	Set To Dictionary	${payload}	${staticroute_field}=${staticroute_value}
	${response}=	API::Post::Network::Static Routes	${payload}	MODE=${MODE}
	[Return]	${response}

#Does the same the keyword bellow, but I wanna maintain the structure one-many
API::Delete Network::Static Route
	[Arguments]	${STATIC_ROUTE}	${MODE}=no
	${payload}=	API::JSON::Delete	static_routes	${STATIC_ROUTE}
	${response}=	API::Delete::Network::Static Routes	${payload}	MODE=${MODE}
	[Return]	${response}

#Does the same the keyword 1_up, but I wanna maintain the structure one-many
API::Delete Network::Static Routes
	[Arguments]	@{STATIC_ROUTES}
	${payload}=	API::JSON::Delete	static_routes	@{STATIC_ROUTES}
	${response}=	API::Delete::Network::Static Routes	${payload}
	[Return]	${response}

API::Delete If Exists Network::Static Routes
	[Arguments]	@{STATIC_ROUTE}
	${staticroutes_id}=	Create List
	:FOR	${STATIC_ROUTE}	IN	@{STATIC_ROUTES}
	\	${id}=	API::Aux::Get ID Network::Static Route	${STATIC_ROUTE}
	\	Run keyword If	'${id}'!='${FALSE}'	Append to List	${staticroutes_id}	${id}
	${payload}=	API::JSON::Delete	static_routes	@{staticroutes_id}
	${length}=	Get Length	${staticroutes_id}
	${response}=	Run Keyword If	'${length}' > '0'	API::Delete::Network::Static Routes	${payload}
	[Return]	${response}

#
#	Auxiliary::Network::Static Routes
#
API::Aux::Get ID Network::Static Route
	[Arguments]	${STATIC_ROUTE}	${MODE}=no
	${staticroutes_list}=	API::Get::Network::Static Routes	MODE=${MODE}
	${staticroutes_list}=	Set Variable	${staticroutes_list.json()}
	${id}=	Set Variable	${FALSE}
	:FOR	${staticroutes_dict}	IN	@{staticroutes_list}
	\	${id_dict}=	Get From Dictionary	${staticroutes_dict}	id
	\	${id}=	Set Variable If	'${STATIC_ROUTE}' == '${id_dict}'	${id_dict}	${FALSE}
	\	Exit For Loop If	'${STATIC_ROUTE}' == '${id_dict}'
	[Return]	${id}

API::Aux::Check Network::Static Route
	[Arguments]	${STATIC_ROUTE}
	${id}=	API::Aux::Get ID Network::Static Route	${STATIC_ROUTE}
	${result}=	Set Variable If	'${id}' == 'False'	${FALSE}	${TRUE}
	[Return]	${result}

API::Aux::Should Not Exist Network::Static Route
	[Arguments]	${STATIC_ROUTE}
	${exist}=	API::Aux::Check Network::Static Route	${STATIC_ROUTE}
	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	[Return]	${result}

API::Aux::Should Not Exist Network::Static Routes
	[Arguments]	@{STATIC_ROUTES}
	${response}=	Create List
	:FOR	${STATIC_ROUTE}	IN	@{STATIC_ROUTE}
	\	${exist}=	API::Aux::Check Network::Static Route	${STATIC_ROUTE}
	\	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}

API::Aux::Should Exist Network::Static Route
	[Arguments]	${STATIC_ROUTE}
	${exist}=	API::Aux::Check Network::Static Route	${STATIC_ROUTE}
	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	[Return]	${result}

API::Aux::Should Exist Network::Static Routes
	[Arguments]	@{STATIC_ROUTES}
	${response}=	Create List
	:FOR	${STATIC_ROUTE}	IN	@{STATIC_ROUTES}
	\	${exist}=	API::Aux::Check Network::Static Route	${STATIC_ROUTE}
	\	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}





#
#	Network::Hosts
#
#	TODO CHECK STRUCTURE
API::Add Network::Hosts
	[Arguments]	${HOST_FIELDS}	${HOST_VALUES}	${MODE}=no
	${length_hostfields}=	Get Length	${HOST_FIELDS}
	${length_hostvalues}=	Get Length	${HOST_VALUES}
	Should Be Equal	${length_hostfields}	${length_hostvalues}

	${payload}=	Create Dictionary
	:FOR	${index}	IN RANGE	@{length_hostfields}
	\	${host_field}=	Get From List	${HOST_FIELDS}	${index}
	\	${host_value}=	Get From List	${HOST_VALUES}	${index}
	\	Set To Dictionary	${payload}	${host_field}=${host_value}
	${response}=	API::Post::Network::Hosts	${payload}	MODE=${MODE}
	[Return]	${response}

#Does the same the keyword bellow, but I wanna maintain the structure one-many
API::Delete Network::Host
	[Arguments]	${HOST}	${MODE}=no
	${payload}=	API::JSON::Delete	hosts	${HOST}
	${response}=	API::Delete::Network::Hosts	${payload}	MODE=${MODE}
	[Return]	${response}

#Does the same the keyword 1_up, but I wanna maintain the structure one-many
API::Delete Network::Hosts
	[Arguments]	@{HOSTS}
	${payload}=	API::JSON::Delete	hosts	@{HOSTS}
	${response}=	API::Delete::Network::Hosts	${payload}
	[Return]	${response}

API::Delete If Exists Network::Hosts
	[Arguments]	@{HOSTS}
	${hosts_id}=	Create List
	:FOR	${HOST}	IN	@{HOSTS}
	\	${id}=	API::Aux::Get ID Network::Host	${HOST}
	\	Run keyword If	'${id}'!='${FALSE}'	Append to List	${hosts_id}	${id}
	${payload}=	API::JSON::Delete	hosts	@{hosts_id}
	${length}=	Get Length	${hosts_id}
	${response}=	Run Keyword If	'${length}' > '0'	API::Delete::Network::Hosts	${payload}
	[Return]	${response}

#
#	Auxiliary::Network::Hosts
#
API::Aux::Get ID Network::Host
	[Arguments]	${HOST}	${MODE}=no
	${hosts_list}=	API::Get::Network::Hosts	MODE=${MODE}
	${hosts_list}=	Set Variable	${hosts_list.json()}
	${id}=	Set Variable	${FALSE}
	:FOR	${hosts_dict}	IN	@{hosts_list}
	\	${id_dict}=	Get From Dictionary	${hosts_dict}	id
	\	${id}=	Set Variable If	'${HOST}' == '${id_dict}'	${id_dict}	${FALSE}
	\	Exit For Loop If	'${HOST}' == '${id_dict}'
	[Return]	${id}

API::Aux::Check Network::Host
	[Arguments]	${HOST}
	${id}=	API::Aux::Get ID Network::Host	${HOST}
	${result}=	Set Variable If	'${id}' == 'False'	${FALSE}	${TRUE}
	[Return]	${result}

API::Aux::Should Not Exist Network::Host
	[Arguments]	${HOST}
	${exist}=	API::Aux::Check Network::Host	${HOST}
	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	[Return]	${result}

API::Aux::Should Not Exist Network::Hosts
	[Arguments]	@{HOSTS}
	${response}=	Create List
	:FOR	${HOST}	IN	@{HOSTS}
	\	${exist}=	API::Aux::Check Network::Host	${HOST}
	\	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}

API::Aux::Should Exist Network::Host
	[Arguments]	${HOST}
	${exist}=	API::Aux::Check Network::Host	${HOST}
	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	[Return]	${result}

API::Aux::Should Exist Network::Hosts
	[Arguments]	@{HOSTS}
	${response}=	Create List
	:FOR	${HOST}	IN	@{HOSTS}
	\	${exist}=	API::Aux::Check Network::Host	${HOST}
	\	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}





#
#	Network::SNMP
#
#	TODO CHECK STRUCTURE
API::Add Network::SNMP
	[Arguments]	${SNMP_FIELDS}	${SNMP_VALUES}	${MODE}=no
	${length_snmpfields}=	Get Length	${SNMP_FIELDS}
	${length_snmpvalues}=	Get Length	${SNMP_VALUES}
	Should Be Equal	${length_snmpfields}	${length_snmpvalues}

	${payload}=	Create Dictionary
	:FOR	${index}	IN RANGE	@{length_snmpfields}
	\	${snmp_field}=	Get From List	${SNMP_FIELDS}	${index}
	\	${snmp_value}=	Get From List	${SNMP_VALUES}	${index}
	\	Set To Dictionary	${payload}	${snmp_field}=${snmp_value}
	${response}=	API::Post::Network::SNMP	${payload}	MODE=${MODE}
	[Return]	${response}

#Does the same the keyword bellow, but I wanna maintain the structure one-many
API::Delete Network::SNMP
	[Arguments]	${SNMP}	${MODE}=no
	${payload}=	API::JSON::Delete	snmps	${SNMP}
	${response}=	API::Delete::Network::SNMP	${payload}	MODE=${MODE}
	[Return]	${response}

#Does the same the keyword 1_up, but I wanna maintain the structure one-many
API::Delete Network::SNMPs
	[Arguments]	@{SNMPS}
	${payload}=	API::JSON::Delete	snmps	@{SNMPS}
	${response}=	API::Delete::Network::SNMP	${payload}
	[Return]	${response}

API::Delete If Exists Network::SNMPs
	[Arguments]	@{SNMPS}
	${snmps_id}=	Create List
	:FOR	${SNMP}	IN	@{SNMPS}
	\	${id}=	API::Aux::Get ID Network::SNMP	${SNMP}
	\	Run keyword If	'${id}'!='${FALSE}'	Append to List	${snmps_id}	${id}
	${payload}=	API::JSON::Delete	snmps	@{snmps_id}
	${length}=	Get Length	${snmps_id}
	${response}=	Run Keyword If	'${length}' > '0'	API::Delete::Network::SNMP	${payload}
	[Return]	${response}

#
#	Auxiliary::Network::SNMP
#
API::Aux::Get ID Network::SNMP
	[Arguments]	${SNMP}	${MODE}=no
	${snmps_list}=	API::Get::Network::SNMP	MODE=${MODE}
	${snmps_list}=	Set Variable	${snmps_list.json()}
	${id}=	Set Variable	${FALSE}
	:FOR	${snmps_dict}	IN	@{snmps_list}
	\	${id_dict}=	Get From Dictionary	${snmps_dict}	id
	\	${id}=	Set Variable If	'${SNMP}' == '${id_dict}'	${id_dict}	${FALSE}
	\	Exit For Loop If	'${SNMP}' == '${id_dict}'
	[Return]	${id}

API::Aux::Check Network::SNMP
	[Arguments]	${SNMP}
	${id}=	API::Aux::Get ID Network::SNMP	${SNMP}
	${result}=	Set Variable If	'${id}' == 'False'	${FALSE}	${TRUE}
	[Return]	${result}

API::Aux::Should Not Exist Network::SNMP
	[Arguments]	${SNMP}
	${exist}=	API::Aux::Check Network::SNMP	${SNMP}
	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	[Return]	${result}

API::Aux::Should Not Exist Network::SNMPs
	[Arguments]	@{SNMPS}
	${response}=	Create List
	:FOR	${SNMP}	IN	@{SNMPS}
	\	${exist}=	API::Aux::Check Network::SNMP	${SNMP}
	\	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}

API::Aux::Should Exist Network::SNMP
	[Arguments]	${SNMP}
	${exist}=	API::Aux::Check Network::SNMP	${SNMP}
	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	[Return]	${result}

API::Aux::Should Exist Network::SNMPs
	[Arguments]	@{SNMPS}
	${response}=	Create List
	:FOR	${SNMP}	IN	@{SNMPS}
	\	${exist}=	API::Aux::Check Network::SNMP	${SNMP}
	\	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}





#
#	Network::DHCP Servers
#
#	TODO CHECK STRUCTURE
API::Add Network::DHCP Servers
	[Arguments]	${DHCP_FIELDS}	${DHCP_VALUES}	${MODE}=no
	${length_dhcpfields}=	Get Length	${DHCP_FIELDS}
	${length_dhcpvalues}=	Get Length	${DHCP_VALUES}
	Should Be Equal	${length_dhcpfields}	${length_dhcpvalues}

	${payload}=	Create Dictionary
	:FOR	${index}	IN RANGE	@{length_dhcpfields}
	\	${dhcp_field}=	Get From List	${DHCP_FIELDS}	${index}
	\	${dhcp_value}=	Get From List	${DHCP_VALUES}	${index}
	\	Set To Dictionary	${payload}	${dhcp_field}=${dhcp_value}
	${response}=	API::Post::Network::DHCP Servers	${payload}	MODE=${MODE}
	[Return]	${response}

#Does the same the keyword bellow, but I wanna maintain the structure one-many
API::Delete Network::DHCP Server
	[Arguments]	${DHCPS}	${MODE}=no
	${payload}=	API::JSON::Delete	dhcp_servers	${DHCPS}
	${response}=	API::Delete::Network::DHCP Servers	${payload}	MODE=${MODE}
	[Return]	${response}

#Does the same the keyword 1_up, but I wanna maintain the structure one-many
API::Delete Network::DHCP Servers
	[Arguments]	@{DHCPS}
	${payload}=	API::JSON::Delete	dhcp_servers	@{DHCPS}
	${response}=	API::Delete::Network::DHCP Servers	${payload}
	[Return]	${response}

API::Delete If Exists Network::DHCP Servers
	[Arguments]	@{DHCPS}
	${dhcps_id}=	Create List
	:FOR	${DHCP}	IN	@{DHCPS}
	\	${id}=	API::Aux::Get ID Network::DHCP Server	${DHCP}
	\	Run keyword If	'${id}'!='${FALSE}'	Append to List	${dhcps_id}	${id}
	${payload}=	API::JSON::Delete	dhcp_servers	@{dhcps_id}
	${length}=	Get Length	${dhcps_id}
	${response}=	Run Keyword If	'${length}' > '0'	API::Delete::Network::DHCP Server	${payload}
	[Return]	${response}

#
#	Auxiliary::Network::DHCP Servers
#
API::Aux::Get ID Network::DHCP Server
	[Arguments]	${DHCP}	${MODE}=no
	${dhcps_list}=	API::Get::Network::DHCP Servers	MODE=${MODE}
	${dhcps_list}=	Set Variable	${dhcps_list.json()}
	${id}=	Set Variable	${FALSE}
	:FOR	${dhcps_dict}	IN	@{dhcps_list}
	\	${id_dict}=	Get From Dictionary	${dhcps_dict}	id
	\	${id}=	Set Variable If	'${DHCP}' == '${id_dict}'	${id_dict}	${FALSE}
	\	Exit For Loop If	'${DHCP}' == '${id_dict}'
	[Return]	${id}

API::Aux::Check Network::DHCP Server
	[Arguments]	${DHCP}
	${id}=	API::Aux::Get ID Network::DHCP Server	${DHCP}
	${result}=	Set Variable If	'${id}' == 'False'	${FALSE}	${TRUE}
	[Return]	${result}

API::Aux::Should Not Exist Network::DHCP Server
	[Arguments]	${DHCP}
	${exist}=	API::Aux::Check Network::DHCP Server	${DHCP}
	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	[Return]	${result}

API::Aux::Should Not Exist Network::DHCP Servers
	[Arguments]	@{DHCPS}
	${response}=	Create List
	:FOR	${DHCP}	IN	@{DHCPS}
	\	${exist}=	API::Aux::Check Network::DHCP Server	${DHCP}
	\	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}

API::Aux::Should Exist Network::DHCP Server
	[Arguments]	${DHCP}
	${exist}=	API::Aux::Check Network::DHCP Server	${DHCP}
	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	[Return]	${result}

API::Aux::Should Exist Network::DHCP Servers
	[Arguments]	@{DHCPS}
	${response}=	Create List
	:FOR	${DHCP}	IN	@{DHCPS}
	\	${exist}=	API::Aux::Check Network::DHCP Server	${DHCP}
	\	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}





#
#	Network::SSL VPN::Clients
#
#	TODO CHECK STRUCTURE
API::Add Network::SSL VPN::Client
	[Arguments]	${VPN_FIELDS}	${VPN_VALUES}	${MODE}=no
	${length_vpnfields}=	Get Length	${VPN_FIELDS}
	${length_vpnvalues}=	Get Length	${VPN_VALUES}
	Should Be Equal	${length_vpnfields}	${length_vpnvalues}

	${payload}=	Create Dictionary
	:FOR	${index}	IN RANGE	@{length_vpnfields}
	\	${vpn_field}=	Get From List	${VPN_FIELDS}	${index}
	\	${vpn_value}=	Get From List	${VPN_VALUES}	${index}
	\	Set To Dictionary	${payload}	${vpn_field}=${vpn_value}
	${response}=	API::Post::Network::SSL VPN::Clients	${payload}	MODE=${MODE}
	[Return]	${response}

#Does the same the keyword bellow, but I wanna maintain the structure one-many
API::Delete Network::SSL VPN::Client
	[Arguments]	${VPN}	${MODE}=no
	${payload}=	API::JSON::Delete	vpns	${VPN}
	${response}=	API::Delete::Network::SSL VPN::Clients	${payload}	MODE=${MODE}
	[Return]	${response}

#Does the same the keyword 1_up, but I wanna maintain the structure one-many
API::Delete Network::SSL VPN::Clients
	[Arguments]	@{VPNS}
	${payload}=	API::JSON::Delete	vpns	@{VPNS}
	${response}=	API::Delete::Network::SSL VPN::Clients	${payload}
	[Return]	${response}

API::Delete If Exists Network::SSL VPN::Clients
	[Arguments]	@{VPNS}
	${vpns_id}=	Create List
	:FOR	${VPN}	IN	@{VPNS}
	\	${id}=	API::Aux::Get ID Network::SSL VPN::Client	${VPN}
	\	Run keyword If	'${id}'!='${FALSE}'	Append to List	${vpns_id}	${id}
	${payload}=	API::JSON::Delete	vpns	@{vpns_id}
	${length}=	Get Length	${vpns_id}
	${response}=	Run Keyword If	'${length}' > '0'	API::Delete::Network::SSL VPN::Clients	${payload}
	[Return]	${response}

#
#	Auxiliary::Network::SSL VPN::Client
#
API::Aux::Get ID Network::SSL VPN::Client
	[Arguments]	${VPN}	${MODE}=no
	${vpns_list}=	API::Get::Network::SSL VPN::Clients	MODE=${MODE}
	${vpns_list}=	Set Variable	${vpns_list.json()}
	${id}=	Set Variable	${FALSE}
	:FOR	${vpns_dict}	IN	@{vpns_list}
	\	${id_dict}=	Get From Dictionary	${vpns_dict}	id
	\	${id}=	Set Variable If	'${VPN}' == '${id_dict}'	${id_dict}	${FALSE}
	\	Exit For Loop If	'${VPN}' == '${id_dict}'
	[Return]	${id}

API::Aux::Check Network::SSL VPN::Client
	[Arguments]	${VPN}
	${id}=	API::Aux::Get ID Network::SSL VPN::Client	${VPN}
	${result}=	Set Variable If	'${id}' == 'False'	${FALSE}	${TRUE}
	[Return]	${result}

API::Aux::Should Not Exist Network::SSL VPN::Client
	[Arguments]	${VPN}
	${exist}=	API::Aux::Check Network::SSL VPN::Client	${VPN}
	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	[Return]	${result}

API::Aux::Should Not Exist Network::SSL VPN::Clients
	[Arguments]	@{VPNS}
	${response}=	Create List
	:FOR	${VPN}	IN	@{VPNS}
	\	${exist}=	API::Aux::Check Network::SSL VPN::Client	${VPN}
	\	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}

API::Aux::Should Exist Network::SSL VPN::Client
	[Arguments]	${VPN}
	${exist}=	API::Aux::Check Network::SSL VPN::Client	${VPN}
	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	[Return]	${result}

API::Aux::Should Exist Network::SSL VPN::Clients
	[Arguments]	@{VPNS}
	${response}=	Create List
	:FOR	${VPN}	IN	@{VPNS}
	\	${exist}=	API::Aux::Check Network::SSL VPN::Client	${VPN}
	\	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}





























#
#	Devices::Table
#
API::Add Devices::Table
	[Arguments]	${NAME}	${TYPE}=${EMPTY}	${IP_ADDRESS}=${EMPTY}	${USERNAME}=${EMPTY}	${PASSWORD}=${EMPTY}	${DEVICE_MODE}=${EMPTY}	${CPASSWORD}=${PASSWORD}	${MODE}=yes
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	name=${NAME}
	Set To Dictionary	${payload}	type=${TYPE}
	Set To Dictionary	${payload}	ip_address=${IP_ADDRESS}
	Run Keyword If	'${USERNAME}'!='${EMPTY}'	Set To Dictionary	${payload}	username=${USERNAME}
	Run Keyword If	'${PASSWORD}'!='${EMPTY}'	Set To Dictionary	${payload}	password=${PASSWORD}
	Run Keyword If	'${CPASSWORD}'!='${EMPTY}'	Set To Dictionary	${payload}	confirm_password=${CPASSWORD}
	Run Keyword If	'${DEVICE_MODE}'!='${EMPTY}'	Set To Dictionary	${payload}	mode=${DEVICE_MODE}
	${response}=	API::Post::Devices::Table	${payload}	MODE=${MODE}
	[Return]	${response}

# WON'T DO, IT USES A LOT OF DIFFERENTLY FIELDS TO EACH DEVICE TYPE
#API::Add Devices::Table
#	[Arguments]	${FIELD_NAMES}	${FIELD_VALUES}	${MODE}=no
#	${length_fields}=	Get Length	${FIELD_NAMES}
#	${length_messages}=	Get Length	${FIELD_VALUES}
#	Should Be Equal	${length_fields}	${length_messages}
#	${response}=	Create List
#	:FOR	${index}	IN RANGE	@{length_fields}
#	\	${payload}=	Create Dictionary
#	\	${field_name}=	Get From List	${FIELD_NAMES}	${index}
#	\	${field_value}=	Get From List	${FIELD_VALUES}	${index}
#	\	Set To Dictionary	${payload}	field_name=${field_name}
#	\	Set To Dictionary	${payload}	field_value=${field_value}
#	\	${result}=	API::Post::System::Custom Fields	${payload}	MODE=${MODE}
#	\	Append to List	${response}	${result}
#	[Return]	${response}

#Does the same the keyword bellow, but I wanna maintain the structure one-many
API::Delete Devices::Table
	[Arguments]	${FIELD_NAME}	${MODE}=no
	${payload}=	Set Variable	{"devices":["${FIELD_NAME}"]}
	${response}=	API::Delete::Devices::Table	${payload}	MODE=${MODE}
	[Return]	${response}

#Does the same the keyword 1_up, but I wanna maintain the structure one-many
API::Delete Devices::Tables
	[Arguments]	@{FIELD_NAMES}
	${payload}=	API::JSON::Delete	devices	@{FIELD_NAMES}
	${response}=	API::Delete::Devices::Table	${payload}
	[Return]	${response}

API::Delete If Exists Devices::Tables
	[Arguments]	@{DEVICE_NAMES}
	${devices_id}=	Create List
	:FOR	${DEVICE_NAME}	IN	@{DEVICE_NAMES}
	\	${id}=	API::Aux::Get ID Devices::Table	${DEVICE_NAME}
	\	Run keyword If	'${id}'!='${FALSE}'	Append to List	${devices_id}	${id}
	${payload}=	API::JSON::Delete	devices	@{devices_id}
	${length}=	Get Length	${devices_id}
	${response}=	Run Keyword If	'${length}' > '0'	API::Delete::Devices::Table	${payload}
	[Return]	${response}

#
#	Auxiliary::Devices::Table
#
API::Aux::Get ID Devices::Table
	[Arguments]	${DEVICE}
	${devices_list}=	API::Get::Devices::Table	MODE=no
	${devices_list}=	Set Variable	${devices_list.json()}
	${id}=	Set Variable	${FALSE}
	:FOR	${devices_dict}	IN	@{devices_list}
	\	${id_dict}=	Get From Dictionary	${devices_dict}	id
	\	${id}=	Set Variable If	'${DEVICE}' == '${id_dict}'	${id_dict}	${FALSE}
	\	Exit For Loop If	'${DEVICE}' == '${id_dict}'
	[Return]	${id}

API::Aux::Check Devices::Table
	[Arguments]	${DEVICE}
	${id}=	API::Aux::Get ID Devices::Tables	${DEVICE}
	${result}=	Set Variable If	'${id}' == 'False'	${FALSE}	${TRUE}
	[Return]	${result}

API::Aux::Should Not Exist Devices::Table
	[Arguments]	${DEVICE}
	${exist}=	API::Aux::Check Devices::Table	${DEVICE}
	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	[Return]	${result}

API::Aux::Should Not Exist Devices::Tables
	[Arguments]	@{DEVICES}
	${response}=	Create List
	:FOR	${DEVICE}	IN	@{DEVICES}
	\	${exist}=	API::Aux::Check Devices::Table	${DEVICE}
	\	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}

API::Aux::Should Exist Devices::Table
	[Arguments]	${DEVICE}
	${exist}=	API::Aux::Check Devices::Table	${DEVICE}
	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	[Return]	${result}

API::Aux::Should Exist Devices::Tables
	[Arguments]	@{DEVICES}
	${response}=	Create List
	:FOR	${DEVICE}	IN	@{DEVICES}
	\	${exist}=	API::Aux::Check Devices::Table	${DEVICE}
	\	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}





















#Does the same the keyword bellow, but I wanna maintain the structure one-many
API::Delete Devices::Discovery::Network
	[Arguments]	${NETWORK_SCAN}	${MODE}=no
	${payload}=	API::JSON::Delete	scans	${NETWORK_SCAN}
	${response}=	API::Delete::Devices::Discovery::Network	${payload}	MODE=${MODE}
	[Return]	${response}

#Does the same the keyword 1_up, but I wanna maintain the structure one-many
API::Delete Devices::Discovery::Networks
	[Arguments]	@{NETWORK_SCANS}
	${payload}=	API::JSON::Delete	scans	@{NETWORK_SCANS}
	${response}=	API::Delete::Devices::Discovery::Network	${payload}
	[Return]	${response}

API::Delete If Exists Devices::Discovery::Networks
	[Arguments]	@{NETWORKSCAN_NAMES}
	${networkscan_id}=	Create List
	:FOR	${NETWORKSCAN_NAME}	IN	@{NETWORKSCAN_NAMES}
	\	${id}=	API::Aux::Get ID Devices::Discovery::Network	${NETWORKSCAN_NAME}
	\	Run keyword If	'${id}'!='${FALSE}'	Append to List	${networkscan_id}	${id}
	${payload}=	API::JSON::Delete	scans	@{networkscan_id}
	${length}=	Get Length	${networkscan_id}
	${response}=	Run Keyword If	'${length}' > '0'	API::Delete::Devices::Discovery::Network	${payload}
	[Return]	${response}

#
#	Auxiliary::Devices::Discovery::Network
#
API::Aux::Get ID Devices::Discovery::Network
	[Arguments]	${NETWORK_SCAN}
	${networkscan_list}=	API::Get::Devices::Discovery::Network	MODE=no
	${networkscan_list}=	Set Variable	${networkscan_list.json()}
	${id}=	Set Variable	${FALSE}
	:FOR	${networkscan_dict}	IN	@{networkscan_list}
	\	${id_dict}=	Get From Dictionary	${networkscan_dict}	id
	\	${id}=	Set Variable If	'${NETWORK_SCAN}' == '${id_dict}'	${id_dict}	${FALSE}
	\	Exit For Loop If	'${NETWORK_SCAN}' == '${id_dict}'
	[Return]	${id}

API::Aux::Check Devices::Discovery::Network
	[Arguments]	${NETWORK_SCAN}
	${id}=	API::Aux::Get ID Devices::Discovery::Network	${NETWORK_SCAN}
	${result}=	Set Variable If	'${id}' == 'False'	${FALSE}	${TRUE}
	[Return]	${result}

API::Aux::Should Not Exist Devices::Discovery::Network
	[Arguments]	${NETWORK_SCAN}
	${exist}=	API::Aux::Check Devices::Discovery::Network	${NETWORK_SCAN}
	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	[Return]	${result}

API::Aux::Should Not Exist Devices::Discovery::Networks
	[Arguments]	@{NETWORK_SCANS}
	${response}=	Create List
	:FOR	${NETWORK_SCAN}	IN	@{NETWORK_SCANS}
	\	${exist}=	API::Aux::Check Devices::Discovery::Network	${NETWORK_SCAN}
	\	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}

API::Aux::Should Exist Devices::Discovery::Network
	[Arguments]	${NETWORK_SCAN}
	${exist}=	API::Aux::Check Devices::Discovery::Network	${NETWORK_SCAN}
	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	[Return]	${result}

API::Aux::Should Exist Devices::Discovery::Networks
	[Arguments]	@{NETWORK_SCANS}
	${response}=	Create List
	:FOR	${NETWORK_SCAN}	IN	@{NETWORK_SCANS}
	\	${exist}=	API::Aux::Check Devices::Discovery::Network	${NETWORK_SCAN}
	\	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}





#
#	Auxiliary::Devices::Discovery::Log
#
API::Aux::Get ID Devices::Discovery::Log
	[Arguments]	${NETWORK_LOG}
	${networklog_list}=	API::Get::Devices::Discovery::Logs	MODE=no
	${networklog_list}=	Set Variable	${networklog_list.json()}
	${id}=	Set Variable	${FALSE}
	:FOR	${networklog_dict}	IN	@{networklog_list}
	\	${id_dict}=	Get From Dictionary	${networklog_dict}	id
	\	${id}=	Set Variable If	'${NETWORK_LOG}' == '${id_dict}'	${id_dict}	${FALSE}
	\	Exit For Loop If	'${NETWORK_LOG}' == '${id_dict}'
	[Return]	${id}

API::Aux::Check Devices::Discovery::Log
	[Arguments]	${NETWORK_LOG}
	${id}=	API::Aux::Get ID Devices::Discovery::Log	${NETWORK_LOG}
	${result}=	Set Variable If	'${id}' == 'False'	${FALSE}	${TRUE}
	[Return]	${result}

API::Aux::Check IP Devices::Discovery::Log
	[Arguments]	${VALUE}
	${networklog_list}=	API::Get::Devices::Discovery::Logs	MODE=no
	${networklog_list}=	Set Variable	${networklog_list.json()}
	${result}=	Set Variable
	:FOR	${networklog_dict}	IN	@{networklog_list}
	\	${ip_dict}=	Get From Dictionary	${networklog_dict}	ip_address
	\	${result}=	Run Keyword And Return Status	Should Be Equal	${ip_dict}	${VALUE}
	\	Exit For Loop If	${result}
	[Return]	${result}

API::Aux::Should Not Exist Devices::Discovery::Log
	[Arguments]	${NETWORK_LOG}
	${exist}=	API::Aux::Check Devices::Discovery::Log	${NETWORK_LOG}
	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	[Return]	${result}

API::Aux::Should Not Exist Devices::Discovery::Logs
	[Arguments]	@{NETWORK_LOGS}
	${response}=	Create List
	:FOR	${NETWORK_LOG}	IN	@{NETWORK_LOGS}
	\	${exist}=	API::Aux::Check Devices::Discovery::Log	${NETWORK_LOG}
	\	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}

API::Aux::Should Exist Devices::Discovery::Log
	[Arguments]	${NETWORK_LOG}
	${exist}=	API::Aux::Check Devices::Discovery::Log	${NETWORK_LOG}
	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	[Return]	${result}

API::Aux::Should Exist Devices::Discovery::Logs
	[Arguments]	@{NETWORK_LOGS}
	${response}=	Create List
	:FOR	${NETWORK_LOG}	IN	@{NETWORK_LOGS}
	\	${exist}=	API::Aux::Check Devices::Discovery::Log	${NETWORK_LOG}
	\	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}





#
#	Cloud::Peer
#
#Does the same the keyword bellow, but I wanna maintain the structure one-many
API::Delete Cloud::Peer
	[Arguments]	${PEER}	${MODE}=no
	${payload}=	API::JSON::Delete	peers	${PEER}
	${response}=	API::Delete::Cloud::Peers	${payload}	MODE=${MODE}
	[Return]	${response}

#Does the same the keyword 1_up, but I wanna maintain the structure one-many
API::Delete Cloud::Peers
	[Arguments]	@{PEERS}
	${payload}=	API::JSON::Delete	peers	@{PEERS}
	${response}=	API::Delete::Cloud::Peers	${payload}
	[Return]	${response}

API::Delete If Exists Cloud::Peers
	[Arguments]	@{PEERS}
	${peers_id}=	Create List
	:FOR	${PEER}	IN	@{PEERS}
	\	${id}=	API::Aux::Get ID Cloud::Peer	${PEER}
	\	Run keyword If	'${id}'!='${FALSE}'	Append to List	${peers_id}	${id}
	${payload}=	API::JSON::Delete	peers	@{peers_id}
	${length}=	Get Length	${peers_id}
	${response}=	Run Keyword If	'${length}' > '0'	API::Delete::Cloud::Peers	${payload}
	[Return]	${response}

#
#	Auxiliary::Cloud::Peer
#
API::Aux::Get ID Cloud::Peer
	[Arguments]	${PEER}	${MODE}=no
	${peers_list}=	API::Get::Cloud::Peers	MODE=${MODE}
	${peers_list}=	Set Variable	${peers_list.json()}
	${id}=	Set Variable	${FALSE}
	:FOR	${peers_dict}	IN	@{peers_list}
	\	${id_dict}=	Get From Dictionary	${peers_dict}	id
	\	${id}=	Set Variable If	'${PEER}' == '${id_dict}'	${id_dict}	${FALSE}
	\	Exit For Loop If	'${PEER}' == '${id_dict}'
	[Return]	${id}

API::Aux::Check Cloud::Peer
	[Arguments]	${PEER}
	${id}=	API::Aux::Get ID Cloud::Peer	${PEER}
	${result}=	Set Variable If	'${id}' == 'False'	${FALSE}	${TRUE}
	[Return]	${result}

API::Aux::Should Not Exist Cloud::Peer
	[Arguments]	${PEER}
	${exist}=	API::Aux::Check Cloud::Peer	${PEER}
	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	[Return]	${result}

API::Aux::Should Not Exist Cloud::Peers
	[Arguments]	@{PEERS}
	${response}=	Create List
	:FOR	${PEER}	IN	@{PEERS}
	\	${exist}=	API::Aux::Check Cloud::Peer	${PEER}
	\	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}

API::Aux::Should Exist Cloud::Peer
	[Arguments]	${PEER}
	${exist}=	API::Aux::Check Cloud::Peer	${PEER}
	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	[Return]	${result}

API::Aux::Should Exist Cloud::Peers
	[Arguments]	@{PEERS}
	${response}=	Create List
	:FOR	${PEER}	IN	@{PEERS}
	\	${exist}=	API::Aux::Check Cloud::Peer	${PEER}
	\	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}





#
#	Cloud::Settings::Range
#
API::Add Cloud::Settings::Range
	[Arguments]	${IP_RANGE_START}=${EMPTY}	${IP_RANGE_END}=${EMPTY}	${MODE}=no
	${payload}=	Create Dictionary
	Set To Dictionary	${payload}	ip_range_start=${IP_RANGE_START}
	Set To Dictionary	${payload}	ip_range_end=${IP_RANGE_END}
	${response}=	API::Post::Cloud::Settings::Range	${payload}	MODE=${MODE}
	[Return]	${response}

#	TODO CHECK STRUCTURE
API::Add Cloud::Settings::Ranges
	[Arguments]	${IP_RANGE_STARTS}	${IP_RANGE_ENDS}	${MODE}=no
	${length_iprangestart}=	Get Length	${IP_RANGE_STARTS}
	${length_iprangeend}=	Get Length	${IP_RANGE_ENDS}
	Should Be Equal	${length_iprangestart}	${length_iprangeend}

	${response}=	Create List
	:FOR	${index}	IN RANGE	@{length_iprangestart}
	\	${payload}=	Create Dictionary
	\	${ip_range_start}=	Get From List	${IP_RANGE_STARTS}	${index}
	\	${ip_range_end}=	Get From List	${IP_RANGE_ENDS}	${index}
	\	Set To Dictionary	${payload}	ip_range_start=${ip_range_start}
	\	Set To Dictionary	${payload}	ip_range_end=${ip_range_end}
	\	${result}=	API::Post::Cloud::Settings::Range	${payload}	MODE=${MODE}
	\	Append to List	${response}	${result}
	[Return]	${response}

#Does the same the keyword bellow, but I wanna maintain the structure one-many
API::Delete Cloud::Settings::Range
	[Arguments]	${RANGE}	${MODE}=no
	${payload}=	API::JSON::Delete	ranges	${RANGE}
	${response}=	API::Delete::Cloud::Settings::Range	${payload}	MODE=${MODE}
	[Return]	${response}

#Does the same the keyword 1_up, but I wanna maintain the structure one-many
API::Delete Cloud::Settings::Ranges
	[Arguments]	@{RANGES}
	${payload}=	API::JSON::Delete	ranges	@{RANGES}
	${response}=	API::Delete::Cloud::Settings::Range	${payload}
	[Return]	${response}

API::Delete If Exists Cloud::Settings::Ranges
	[Arguments]	@{RANGES}
	${ranges_id}=	Create List
	:FOR	${RANGE}	IN	@{RANGES}
	\	${id}=	API::Aux::Get ID Cloud::Settings::Range	${RANGE}
	\	Run keyword If	'${id}'!='${FALSE}'	Append to List	${ranges_id}	${id}
	${payload}=	API::JSON::Delete	ranges	@{ranges_id}
	${length}=	Get Length	${ranges_id}
	${response}=	Run Keyword If	'${length}' > '0'	API::Delete::Cloud::Settings::Range	${payload}
	[Return]	${response}

#
#	Auxiliary::Cloud::Settings::Range
#
API::Aux::Get ID Cloud::Settings::Range
	[Arguments]	${RANGE}	${MODE}=no
	${ranges_list}=	API::Get::Cloud::Settings::Range	MODE=${MODE}
	${ranges_list}=	Set Variable	${ranges_list.json()}
	${id}=	Set Variable	${FALSE}
	:FOR	${ranges_dict}	IN	@{ranges_list}
	\	${id_dict}=	Get From Dictionary	${ranges_dict}	id
	\	${id}=	Set Variable If	'${RANGE}' == '${id_dict}'	${id_dict}	${FALSE}
	\	Exit For Loop If	'${RANGE}' == '${id_dict}'
	[Return]	${id}

API::Aux::Check Cloud::Settings::Range
	[Arguments]	${RANGE}
	${id}=	API::Aux::Get ID Cloud::Settings::Range	${RANGE}
	${result}=	Set Variable If	'${id}' == 'False'	${FALSE}	${TRUE}
	[Return]	${result}

API::Aux::Should Not Exist Cloud::Settings::Range
	[Arguments]	${RANGE}
	${exist}=	API::Aux::Check Cloud::Settings::Range	${RANGE}
	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	[Return]	${result}

API::Aux::Should Not Exist Cloud::Settings::Ranges
	[Arguments]	@{RANGES}
	${response}=	Create List
	:FOR	${RANGE}	IN	@{RANGES}
	\	${exist}=	API::Aux::Check Cloud::Settings::Range	${RANGE}
	\	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}

API::Aux::Should Exist Cloud::Settings::Range
	[Arguments]	${RANGE}
	${exist}=	API::Aux::Check Cloud::Settings::Range	${RANGE}
	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	[Return]	${result}

API::Aux::Should Exist Cloud::Settings::Ranges
	[Arguments]	@{RANGES}
	${response}=	Create List
	:FOR	${RANGE}	IN	@{RANGES}
	\	${exist}=	API::Aux::Check Cloud::Settings::Range	${RANGE}
	\	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}





#
#	Cloud::Management
#
#Does the same the keyword bellow, but I wanna maintain the structure one-many
API::Upgrade Cloud::Management
	[Arguments]	${MANAGEMENT}	${MODE}=no
	${payload}=	API::JSON::Delete	multi_peers	${MANAGEMENT}
	${response}=	API::Post::Cloud::Management::Upgrade	${payload}	MODE=${MODE}
	[Return]	${response}

#Does the same the keyword 1_up, but I wanna maintain the structure one-many
API::Upgrade Cloud::Managements
	[Arguments]	@{MANAGEMENTS}
	${payload}=	API::JSON::Delete	multi_peers	@{MANAGEMENTS}
	${response}=	API::Post::Cloud::Management::Upgrade	${payload}
	[Return]	${response}

API::Upgrade If Exists Cloud::Managements
	[Arguments]	@{MANAGEMENTS}
	${managements_id}=	Create List
	:FOR	${MANAGEMENT}	IN	@{MANAGEMENTS}
	\	${id}=	API::Aux::Get ID Cloud::Management	${MANAGEMENT}
	\	Run keyword If	'${id}'!='${FALSE}'	Append to List	${managements_id}	${id}
	${payload}=	API::JSON::Delete	multi_peers	@{managements_id}
	${length}=	Get Length	${managements_id}
	${response}=	Run Keyword If	'${length}' > '0'	API::Post::Cloud::Management::Upgrade	${payload}
	[Return]	${response}

#
#	Auxiliary::Cloud::Management
#
API::Aux::Get ID Cloud::Management
	[Arguments]	${MANAGEMENT}	${MODE}=no
	${managements_list}=	API::Get::Cloud::Management	MODE=${MODE}
	${managements_list}=	Set Variable	${managements_list.json()}
	${id}=	Set Variable	${FALSE}
	:FOR	${managements_dict}	IN	@{managements_list}
	\	${id_dict}=	Get From Dictionary	${managements_dict}	id
	\	${id}=	Set Variable If	'${MANAGEMENT}' == '${id_dict}'	${id_dict}	${FALSE}
	\	Exit For Loop If	'${MANAGEMENT}' == '${id_dict}'
	[Return]	${id}

API::Aux::Check Cloud::Management
	[Arguments]	${MANAGEMENT}
	${id}=	API::Aux::Get ID Cloud::Management	${MANAGEMENT}
	${result}=	Set Variable If	'${id}' == 'False'	${FALSE}	${TRUE}
	[Return]	${result}

API::Aux::Should Not Exist Cloud::Management
	[Arguments]	${MANAGEMENT}
	${exist}=	API::Aux::Check Cloud::Management	${MANAGEMENT}
	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	[Return]	${result}

API::Aux::Should Not Exist Cloud::Managements
	[Arguments]	@{MANAGEMENTS}
	${response}=	Create List
	:FOR	${MANAGEMENT}	IN	@{MANAGEMENTS}
	\	${exist}=	API::Aux::Check Cloud::Management	${MANAGEMENT}
	\	${result}=	Run Keyword And Return Status	Should Not Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}

API::Aux::Should Exist Cloud::Management
	[Arguments]	${MANAGEMENT}
	${exist}=	API::Aux::Check Cloud::Management	${MANAGEMENT}
	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	[Return]	${result}

API::Aux::Should Exist Cloud::Managements
	[Arguments]	@{MANAGEMENTS}
	${response}=	Create List
	:FOR	${MANAGEMENT}	IN	@{MANAGEMENTS}
	\	${exist}=	API::Aux::Check Cloud::Management	${MANAGEMENT}
	\	${result}=	Run Keyword And Return Status	Should Be True	${exist}
	\	Append to List	${response}	${result}
	[Return]	${response}




#
#	Auxiliative to Messages
#
API::Msg::Create Simple Message
	[Arguments]	${field}	${message}
	${globalerr}=	Create Dictionary
	Set To Dictionary	${globalerr}	message=Error on page. Please check.
	Set To Dictionary	${globalerr}	field=globalerr
	${list}=	Create List	${globalerr}

	${dictionary}=	Create Dictionary
	Set To Dictionary	${dictionary}	message=${message}
	Set To Dictionary	${dictionary}	field=${field}
	Append to List	${list}	${dictionary}
	[Return]	${list}

API::Msg::Create Complex Message
	[Arguments]	${FIELDS}	${MESSAGES}
#	Just checking has the same length of fields and messages
	${length_fields}=	Get Length	${FIELDS}
	${length_messages}=	Get Length	${MESSAGES}
	Should Be Equal	${length_fields}	${length_messages}

#	When occurs an error, dictionary "globalerr" always appears
	${globalerr}=	Create Dictionary
	Set To Dictionary	${globalerr}	message=Error on page. Please check.
	Set To Dictionary	${globalerr}	field=globalerr
	${list}=	Create List	${globalerr}

#	Creating all dictionaries to the list of response
	:FOR	${index}	IN RANGE	${length_fields}
	\	${dictionary}=	Create Dictionary
	\	${field}=	Get From List	${FIELDS}	${index}
	\	${message}=	Get From List	${MESSAGES}	${index}
	\	Set To Dictionary	${dictionary}	field=${field}
	\	Set To Dictionary	${dictionary}	message=${message}
	\	Append to List	${list}	${dictionary}
	[Return]	${list}

#	Would I create a mechanism to when no passing arg, understands values as empty?
API::Msg::Compare Simple Message
	[Arguments]	${RESPONSE_MESSAGE}	${EXPECTED_FIELD}=${EMPTY}	${EXPECTED_MESSAGE}=${EMPTY}
	${result}=	API::Msg::Create Simple Message	${EXPECTED_FIELD}	${EXPECTED_MESSAGE}
	${globaller}=	Get From List	${result}	0
	Run Keyword If	'${EXPECTED_FIELD}' != '${EMPTY}'	Should Be Equal	${RESPONSE_MESSAGE}	${result}
	...	ELSE	Should Not Contain	${RESPONSE_MESSAGE}	${globaller}
#	Run Keyword If	'${EXPECTED_FIELD}' != '${EMPTY}'	Lists Should Be Equal	${RESPONSE_MESSAGE}	${result}
#	...	ELSE	List Should Not Contain Value	${RESPONSE_MESSAGE}	${globaller}

API::Msg::Compare Complex Message
	[Arguments]	${RESPONSE_MESSAGE}	${EXPECTED_FIELD}	${EXPECTED_MESSAGE}
	${result}=	API::Msg::Create Complex Message	${EXPECTED_FIELD}	${EXPECTED_MESSAGE}
	Should Be Equal	${RESPONSE_MESSAGE}	${result}
#	Lists Should Be Equal	${RESPONSE_MESSAGE}	${result}