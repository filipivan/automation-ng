*** Keywords ***
API::Get::Security::Local Accounts
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get security services info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return security services from NG
	${response}=	API::Send Get Request	security/localaccounts
	[Return]	${response}

API::Post::Security::Local Accounts
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds local accounts using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add new local account
	${response}=	API::Send Post Request	security/localaccounts	${PAYLOAD}
	[Return]	${response}

API::Delete::Security::Local Accounts
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Deletes local accounts using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Delete given local account
	${response}=	API::Send Delete Request	security/localaccounts	${PAYLOAD}
	[Return]	${response}

API::Get::Security::Local Accounts::Name
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get local account info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return local account from NG
	${response}=	API::Send Get Request	security/localaccounts/${NAME}
	[Return]	${response}

API::Put::Security::Local Account
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update local account info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update local account from NG
	${response}=	API::Send Put Request	security/localaccounts/${NAME}	${PAYLOAD}
	[Return]	${response}

API::Get::Security::Password Rules
	[Documentation]
	...	Get password rules info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return password rules from NG
	${response}=	API::Send Get Request	security/passwordrules
	[Return]	${response}

API::Put::Security::Password Rules
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update password rules info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update password rules from NG
	${response}=	API::Send Put Request	security/passwordrules	${PAYLOAD}
	[Return]	${response}

API::Get::Security::Authorization
	[Documentation]
	...	Get security authorization using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get new security authorization group
	${response}=	API::Send Get Request	security/authorization
	[Return]	${response}

API::Post::Security::Authorization
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds security authorization using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Added new security authorization group
	${response}=	API::Send Post Request	security/authorization	${PAYLOAD}
	[Return]	${response}

API::Delete::Security::Authorization
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Deletes security authorization using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Delete given security authorization
	${response}=	API::Send Delete Request	security/authorization	${PAYLOAD}

API::Get::Security::Authorization::Name::Members
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get security authorization members info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return security authorization members from NG
	${response}=	API::Send Get Request	security/authorization/${NAME}/members
	[Return]	${response}

API::Post::Security::Authorization::Name::Members
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds security authorization member info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Security authorization member added
	${response}=	API::Send Post Request	security/authorization/${NAME}/members	${PAYLOAD}
	[Return]	${response}

API::Delete::Security::Authorization::Name::Members
	[Arguments]	${NAME}	${PAYLAOD}	${MODE}=yes
	[Documentation]
	...	Deletes security authorization member info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Security authorization member deleted
	${response}=	API::Send Delete Request	security/authorization/${NAME}/members	${PAYLOAD}
	[Return]	${response}

API::Get::Security::Authorization::Name::Profile
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get security authorization profile info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return security authorization profile from NG
	${response}=	API::Send Get Request	security/authorization/${NAME}/profile
	[Return]	${response}

API::Put::Security::Authorization::Name::Profile
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Get security authorization profile info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return security authorization profile from NG
	${response}=	API::Send Put Request	security/authorization/${NAME}/profile	${PAYLOAD}
	[Return]	${response}

API::Get::Security::Authorization::Name::Remote Groups
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get security authorization remote groups info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return security authorization remote groups from NG
	${response}=	API::Send Get Request	security/authorization/${NAME}/remotegroups
	[Return]	${response}

API::Put::Security::Authorization::Name::Remote Groups
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get security authorization remote groups info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return security authorization remote groups from NG
	${response}=	API::Send Put Request	security/authorization/${NAME}/remotegroups
	[Return]	${response}

API::Get::Security::Authorization::Name::Devices
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get security authorization devices info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return security authorization devices from NG
	${response}=	API::Send Get Request	security/authorization/${NAME}/devices
	[Return]	${response}

API::Post::Security::Authorization::Name::Devices
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds security authorization device info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	New security authorization member added
	${response}=	API::Send Post Request	security/authorization/${NAME}/devices	${PAYLOAD}
	[Return]	${response}

API::Delete::Security::Authorization::Name::Devices
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds security authorization device info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	New security authorization device added
	${response}=	API::Send Delete Request	security/authorization/${NAME}/devices	${PAYLOAD}
	[Return]	${response}

API::Get::Security::Authorization::Name::Devices::Name2
	[Arguments]	${NAME}	${NAME2}	${MODE}=yes
	[Documentation]
	...	Gets security authorization device info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return security authorization device info
	${response}=	API::Send Get Request	security/authorization/${NAME}/devices/${NAME2}
	[Return]	${response}

API::Put::Security::Authorization::Name::Sevices::Name
	[Arguments]	${NAME}	${NAME2}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Updates security authorization device info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Security authorization devices updated
	${response}=	API::Send Put Request	security/authorization/${NAME}/devices/${NAME2}	${PAYLOAD}
	[Return]	${response}

API::Get::Security::Authorization::Name::Outlets
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get security authorization outlets info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return security authorization outlets from NG
	${response}=	API::Send Get Request	security/authorization/${NAME}/outlets
	[Return]	${response}

API::Post::Security::Authorization::Name::Outlets
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds security authorization outlet info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Security authorization outlet added
	${response}=	API::Send Post Request	security/authorization/${NAME}/outlets	${PAYLOAD}
	[Return]	${response}

API::Delete::Security::Authorization::Name::Outlets
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Deletes security authorization outlet info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Security authorization outlet removed
	${response}=	API::Send Delete Request	security/authorization/${NAME}/outlets	${PAYLOAD}
	[Return]	${response}

API::Get::Security::Authorization::Name::Outlet::Name
	[Arguments]	${NAME}	${NAME2}	${MODE}=yes
	[Documentation]
	...	Gets security authorization outlet info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return security authorization outlet info
	${response}=	API::Send Get Request	security/authorization/${NAME}/outlets/${NAME2}
	[Return]	${response}

API::Put::Security::Authorization::Name::Outlet::Name2
	[Arguments]	${NAME}	${NAME2}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Updates security authorization outlet info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Security authorization outlet updated
	${response}=	API::Send Put Request	security/authorization/${NAME}/outlets/${NAME2}	${PAYLOAD}
	[Return]	${response}

API::Get::Security::Authentication
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get security authentication info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return security authentication from NG
	${response}=	API::Send Get Request	security/authentication
	[Return]	${response}

API::Post::Security::Authentication
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Add security authentication info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add security authentication from NG
	${response}=	API::Send Post Request	security/authentication	${PAYLOAD}
	[Return]	${response}

API::Delete::Security::Authentication
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Delete security authentication info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Delete security authentication from NG
	${response}=	API::Send Delete Request	security/authentication	${PAYLOAD}
	[Return]	${response}

API::Get::Security::Authentication::Name
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get security authentication info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get security authentication from NG
	${response}=	API::Send Get Request	security/authentication/${NAME}
	[Return]	${response}

API::Put::Security::Authentication::Name
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update security authentication info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update security authentication from NG
	${response}=	API::Send Put Request	security/authentication/${NAME}	${PAYLOAD}
	[Return]	${response}

API::Put::Security::Authentication::Name::Up
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Move security authentication method up using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Move security authentication up from NG
	${response}=	API::Send Put Request	security/authentication/${NAME}/up
	[Return]	${response}

API::Put::Security::Authentication::Name::Down
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Move security authentication method down using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Move security authentication down from NG
	${response}=	API::Send Put Request	security/authentication/${NAME}/down
	[Return]	${response}

API::Get::Security::Authentication Console
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get security authentication info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get security authentication from NG
	${response}=	API::Send Get Request	security/authentication_console
	[Return]	${response}

API::Post::Security::Authentication Console
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update security authentication info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update security authentication from NG
	${response}=	API::Send Post Request	security/authentication_console	${PAYLOAD}
	[Return]	${response}

API::Get::Security::Authentication Default Group
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get security authentication info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get security authentication from NG
	${response}=	API::Send Get Request	security/authentication_defaultgroup
	[Return]	${response}

API::Post::Security::Authentication Default Group
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update security authentication info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update security authentication from NG
	${response}=	API::Send Post Request	security/authentication_defaultgroup	${PAYLOAD}
	[Return]	${response}

API::Get::Security::Firewall
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get security firewall info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return security firewall from NG
	${response}=	API::Send Get Request	security/firewall
	[Return]	${response}

API::Post::Security::Firewall
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Add new firewall chain info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add new firewall chain from NG
	${response}=	API::Send Post Request	security/firewall	${PAYLOAD}
	[Return]	${response}

API::Delete::Security::Firewall
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Add new firewall chain info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add new firewall chain from NG
	${response}=	API::Send Delete Request	security/firewall/	${PAYLOAD}
	[Return]	${response}

API::Get::Security::Firewall::Name::Rules
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get chain's rules info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return chain rules
	${response}=	API::Send Get Request	security/firewall/${NAME}/rules
	[Return]	${response}

API::Post::Security::Firewall::Name::Rules
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Add new firewall chain rule info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add new firewall chain rule from NG
	${response}=	API::Send Post Request	security/firewall/${NAME}/rules	${PAYLOAD}
	[Return]	${response}

API::Delete::Security::Firewall::Name::Rules
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Add new firewall chain info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add new firewall chain from NG
	${response}=	API::Send Delete Request	security/firewall/${NAME}/rules	${PAYLOAD}
	[Return]	${response}

API::Get::Security::Firewall::Name::Rules::Name2
	[Arguments]	${NAME}	${NAME2}	${MODE}=yes
	[Documentation]
	...	Get firewall chain rule info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get firewall chain rule from NG
	${response}=	API::Send Get Request	security/firewall/${NAME}/rules/${NAME2}
	[Return]	${response}

API::Put::Security::Firewall::Name::Rules::Name2
	[Arguments]	${NAME}	${NAME2}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update firewall chain rule info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update firewall chain rule from NG
	${response}=	API::Send Put Request	security/firewall/${NAME}/rules/${NAME2}	${PAYLOAD}
	[Return]	${response}

API::Put::Security::Firewall::Name::Rules::Name2::Up
	[Arguments]	${NAME}	${NAME2}	${MODE}=yes
	[Documentation]
	...	Update firewall chain rule info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update firewall chain rule from NG
	${response}=	API::Send Put Request	security/firewall/${NAME}/rules/${NAME2}/up
	[Return]	${response}

API::Put::Security::Firewall::Name::Rules::Name2::Down
	[Arguments]	${NAME}	${NAME2}	${MODE}=yes
	[Documentation]
	...	Update firewall chain rule info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update firewall chain rule from NG
	${response}=	API::Send Put Request	security/firewall/${NAME}/rules/${NAME2}/down
	[Return]	${response}

API::Get::Security::Services
	[Documentation]
	...	Get security services info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return security services from NG
	${response}=	API::Send Get Request	security/services
	[Return]	${response}

API::Put::Security::Services
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update security services info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update security services from NG
	${response}=	API::Send Put Request	security/services	${PAYLOAD}
	[Return]	${response}