*** Keywords ***
API::Get::Network::Settings
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get network settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Returns network settings info
	${response}=	API::Send Get Request	network/settings	MODE=${MODE}
	[Return]	${response}

API::Put::Network::Settings
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Updates network settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update network settings from NG
	${response}=	API::Send Put Request	network/settings	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Network::Connections
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get network connections using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Network connections from NG
	${response}=	API::Send Get Request	network/connections	MODE=${MODE}
	[Return]	${response}

API::Post::Network::Connections
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds network settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds network connection from NG
	${response}=	API::Send Post Request	network/connections	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Delete::Network::Connections
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Deletes network settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Delete network connection from NG
	${response}=	API::Send Delete Request	network/connections	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Network::Connections::Name
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get network connection using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Returns network connection info
	${response}=	API::Send Get Request	network/connections/${NAME}	MODE=${MODE}
	[Return]	${response}

API::Put::Network::Connections::Name
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Updates network settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update network connection from NG
	${response}=	API::Send Put Request	network/connections/${NAME}	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Put::Network::Connections::Name::Up
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Up network connection using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Up network connection from NG
	${response}=	API::Send Put Request	network/connections/${NAME}/up	${EMPTY}	MODE=${MODE}
	[Return]	${response}

API::Put::Network::Connections::Name::Down
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Down network connection using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Down network connection from NG
	${response}=	API::Send Put Request	network/connections/${NAME}/down	MODE=${MODE}
	[Return]	${response}

API::Get::Network::Switch::Vlan
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Gets vlan entries using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return the system custom fields data
	${response}=	API::Send Get Request	network/switch/vlan	MODE=${MODE}
	[Return]	${response}

API::Post::Network::Switch::Vlan
	[Arguments]	${PAYLOAD}	${RAW_MODE}=no	${MODE}=yes
	[Documentation]
	...	Adds vlan entry using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds new custom field
	${response}=	API::Send Post Request	network/switch/vlan	${PAYLOAD}	RAW_MODE=${RAW_MODE}	MODE=${MODE}
	[Return]	${response}

API::Delete::Network::Switch::Vlan
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Deletes vlan entries using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update custom field value
	${response}=	API::Send Delete Request	network/switch/vlan	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Put::Network::Switch::Vlan::Name
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Updates vlan entry values using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update custom field value
	${response}=	API::Send Put Request	network/switch/vlan/${NAME}	${PAYLOAD}	MODE=${MODE}
	[Return]	${response

API::Get::Network::Static Routes
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get static routes using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return static routes from NG
	${response}=	API::Send Get Request	network/staticroutes	MODE=${MODE}
	[Return]	${response}

API::Post::Network::Static Routes
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds static route using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds static route from NG
	${response}=	API::Send Post Request	network/staticroutes	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Delete::Network::Static Routes
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Deletes static route using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Delete static route from NG
	${response}=	API::Send Delete Request	network/staticroutes	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Network::Static Routes::Name
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get static route using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Returns static route info
	${response}=	API::Send Get Request	network/staticroutes/${NAME}	MODE=${MODE}
	[Return]	${response}

API::Put::Network::Static Routes::Name
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Updates static route using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update static route from NG
	${response}=	API::Send Put Request	network/staticroutes/${NAME}	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Network::Hosts
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get hosts using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return hosts from NG
	${response}=	API::Send Get Request	network/hosts	MODE=${MODE}
	[Return]	${response}

API::Post::Network::Hosts
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds host using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds host from NG
	${response}=	API::Send Post Request	network/hosts	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Delete::Network::Hosts
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Deletes host using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Delete host from NG
	${response}=	API::Send Delete Request	network/hosts	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Network::Hosts::Name
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get host using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Returns host info
	${response}=	API::Send Get Request	network/hosts/${NAME}	MODE=${MODE}
	[Return]	${response}

API::Put::Network::Hosts::Name
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Updates host using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update host from NG
	${response}=	API::Send Put Request	network/hosts/${NAME}	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Network::SNMP
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get SNMP info using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return SNMP info data from NG
	${response}=	API::Send Get Request	network/snmp	MODE=${MODE}
	[Return]	${response}

API::Post::Network::SNMP
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds SNMP entry using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add SNMP entry from NG
	${response}=	API::Send Post Request	network/snmp	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Delete::Network::SNMP
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Deletes SNMP entry using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Deletes SNMP entry from NG
	${response}=	API::Send Delete Request	network/snmp	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Network::SNMP::Name
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get SNMP entry using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get SNMP entry from NG
	${response}=	API::Send Get Request	network/snmp/${NAME}	MODE=${MODE}
	[Return]	${response}

API::Put::Network::SNMP::Name
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update SNMP entry using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update SNMP entry from NG
	${response}=	API::Send Put Request	network/snmp/${NAME}	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Network::SNMP System
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get SNMP entry using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get SNMP entry from NG
	${response}=	API::Send Get Request	network/snmp_system	MODE=${MODE}
	[Return]	${response}

API::Put::Network::SNMP System
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update SNMP entry using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update SNMP entry from NG
	${response}=	API::Send Put Request	network/snmp_system	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Network::DHCP Servers
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get DHCP servers using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return DHCP servers from NG
	${response}=	API::Send Get Request	network/dhcp	MODE=${MODE}
	[Return]	${response}

API::Post::Network::DHCP Servers
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds DHCP server using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds DHCP from NG
	${response}=	API::Send Post Request	network/dhcp	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Delete::Network::DHCP Servers
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Deletes DHCP server using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Delete DHCP server from NG
	${response}=	API::Send Delete Request	network/dhcp	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Network::DHCP Servers::Name
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get DHCP server using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Returns DHCP server info
	${response}=	API::Send Get Request	network/dhcp/${NAME}	MODE=${MODE}
	[Return]	${response}

API::Put::Network::DHCP Servers::Name
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Updates DHCP server using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update DHCP server from NG
	${response}=	API::Send Put Request	network/dhcp/${NAME}	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Network::SSL VPN::Clients
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get SSL VPN clients using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return SSL VPN clients from NG
	${response}=	API::Send Get Request	network/sslvpn/clients	MODE=${MODE}
	[Return]	${response}

API::Post::Network::SSL VPN::Clients
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds SSL VPN client using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add SSL VPN clients from NG
	${response}=	API::Send Post Request	network/sslvpn/clients	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Delete::Network::SSL VPN::Clients
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Deletes SSL VPN client using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Deletes SSL VPN clients from NG
	${response}=	API::Send Delete Request	network/sslvpn/clients	${PAYLOAD}	MODE=${MODE}

API::Get::Network::SSL VPN::Clients::Name
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get SSL VPN client using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return SSL VPN client from NG
	${response}=	API::Send Get Request	network/sslvpn/clients/${NAME}	MODE=${MODE}
	[Return]	${response}

API::Put::Network::SSL VPN::Clients::Name
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update SSL VPN client using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update SSL VPN clients from NG
	${response}=	API::Send Put Request	network/sslvpn/clients/${NAME}	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Put::Network::SSL VPN::Clients::Name::Start
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Start SSL VPN client using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Start SSL VPN clients from NG
	${response}=	API::Send Post Request	network/sslvpn/clients/${NAME}/start	MODE=${MODE}
	[Return]	${response}

API::Put::Network::SSL VPN::Clients::Name::Stop
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Stop SSL VPN client using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Stop SSL VPN clients from NG
	${response}=	API::Send Post Request	network/sslvpn/clients/${NAME}/stop	MODE=${MODE}
	[Return]	${response}

API::Get::Network::SSL VPN::Server
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get SSL VPN server using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return SSL VPN server info from NG
	${response}=	API::Send Get Request	network/sslvpn/server	MODE=${MODE}
	[Return]	${response}

API::Put::Network::SSL VPN::Server
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update SSL VPN server using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Updated SSL VPN server info from NG
	${response}=	API::Send Put Request	network/sslvpn/server	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Network::SSL VPN::Server Status
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get SSL VPN server status using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return SSL VPN server status info from NG
	${response}=	API::Send Get Request	network/sslvpn/server/status	MODE=${MODE}
	[Return]	${response}

API::Get Switch Interfaces
	${response}=	API::Send Get Request	/network/switch/interfaces
	Should Be Equal   ${response.status_code}   ${200}
	${INTERFACE}=	evaluate	str(${response.json()})
	${INTERFACE}=	Remove String	${INTERFACE}	[
	${INTERFACE}=	Remove String	${INTERFACE}	]
	${INTERFACE}	split string	${INTERFACE}	},
	@{INTERFACES}	Create List
	FOR	${I}	IN	@{INTERFACE}
		${COLUMNS}=	Split String	${I}	',
		FOR	${COLUMN}	IN	@{COLUMNS}
			${CONTAINS}	Run Keyword And Return Status	Should Contain	${COLUMN}	'id':
			IF	${CONTAINS}
				${ID}=	Replace String Using Regexp	${COLUMN}	\\s*\{\'id\'\:\\s|\'|\:	${EMPTY}
				Append to List	${INTERFACES}	${ID}
			END
			Exit For Loop If	${CONTAINS}
		END
	END
	[Return]	@{INTERFACES}