*** Keywords ***
API::Get::Devices::Table
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get managed devices using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return managed devices
	${response}=	API::Send Get Request	devices/table	MODE=${MODE}
	[Return]	${response}

API::Post::Devices::Table
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds device using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Adds new device in NG
	${response}=	API::Send Post Request	/devices/table	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Delete::Devices::Table
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Delete managed device using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Remove device from NG
	${response}=	API::Send Delete Request	/devices/table	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Devices::Table::Name
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get managed device using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Returns the managed device info
	${response}=	API::Send Get Request	devices/table/${NAME}	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Table::Name
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Updates managed device using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update device from NG
	${response}=	API::Send Put Request	devices/table/${NAME}	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Table::Name::Enable
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Disable device using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Disable managed device
	${response}=	API::Send Put Request	devices/table/${NAME}/enable	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Table::Name::Disable
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Disable device using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Disable managed device
	${response}=	API::Send Put Request	devices/table/${NAME}/disable	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Table::Name::Ondemand
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Updates the device mode configuration to On-demand using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update device mode configuration
	${response}=	API::Send Put Request	devices/table/${NAME}/ondemand	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Table::Name::Rename
	[Arguments]	${NAME}	${NEW_NAME}	${MODE}=yes
	[Documentation]
	...	Updates device name using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Rename device name
	${response}=	API::Send Put Request	devices/table/${NAME}/rename	${NEW_NAME}	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Table::Name::Default
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Updates the local_serial device to its default configs using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Reset local_serial device configuration
	${response}=	API::Send Put Request	devices/table/${NAME}/default	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Table::Name::Bounce DTR
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Updates the local_serial device to its default configs using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Reset local_serial device configuration
	${response}=	API::Send Put Request	devices/table/${NAME}/bouncedtr	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Table::Name::Clone
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Updates the local_serial device to its default configs using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Reset local_serial device configuration
	${response}=	API::Send Put Request	devices/table/${NAME}/clone	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Devices::Table::Name::Management
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get device management configuration using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's management info from NG
	${response}=	API::Send Get Request	devices/table/${NAME}/management	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Table::Name::Management
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Updates device management configuration using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the given device's management info from NG
	${response}=	API::Send Put Request	devices/table/${NAME}/management	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Devices::Table::Name::Logging
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get device logging configuration using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's logging info from NG
	${response}=	API::Send Get Request	devices/table/${NAME}/logging	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Table::Name::Logging
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Updates device logging configuration using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the given device's logging info from NG
	${response}=	API::Send Put Request	devices/${NAME}/logging	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Devices::Table::Name::Custom Fields
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get device custom fields using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's custom fields info from NG
	${response}=	API::Send Get Request	devices/table/${NAME}/customfields	MODE=${MODE}
	[Return]	${response}

API::Post::Devices::Table::Name::Custom Fields
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds device custom fields using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add the given device's custom fields info from NG
	${response}=	API::Send Post Request	devices/table/${NAME}/customfields	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Delete::Devices::Table::Name::Custom Fields
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Delete device custom fields using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Delete the given device's custom fields info from NG
	${response}=	API::Send Delete Request	devices/table/${NAME}/customfields	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Devices::Table::Name::Custom Field::Name2
	[Arguments]	${NAME}	${NAME2}	${MODE}=yes
	[Documentation]
	...	Get device custom fields using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's custom fields info from NG
	${response}=	API::Send Get Request	devices/table/${NAME}/customfields/${NAME2}	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Table::Name::Custom Field::Name2
	[Arguments]	${NAME}	${NAME2}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update device custom fields using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the given device's custom fields info from NG
	${response}=	API::Send Put Request	devices/table/${NAME}/customfields/${NAME2}	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Devices::Table::Name::Commands
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get device commands using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's commands info from NG
	${response}=	API::Send Get Request	devices/table/${NAME}/commands	MODE=${MODE}
	[Return]	${response}

API::Post::Devices::Table::Name::Commands
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Adds device command using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Add the given device's command info from NG
	${response}=	API::Send Post Request	devices/${NAME}/commands	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Delete::Devices::Table::Name::Commands
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Delete device command using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Delete the given device's command info from NG
	${response}=	API::Send Delete Request	devices/table/${NAME}/commands	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Devices::Table::Name::Commands::Name2
	[Arguments]	${NAME}	${NAME2}	${MODE}=yes
	[Documentation]
	...	Get device command using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's command info from NG
	${response}=	API::Send Get Request	devices/table/${NAME}/commands/${NAME2}	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Table::Name::Commands::Name2
	[Arguments]	${NAME}	${NAME2}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update device command using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update the given device's command info from NG
	${response}=	API::Send Put Request	devices/table/${NAME}/commands/${NAME2}	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Devices::Table::Name::Outlets
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Get Request	devices/table/${NAME}/outlets	MODE=${MODE}
	[Return]	${response}

API::Get::Devices::Table::Name::Outlets::List
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Get Request	devices/table/${NAME}/outlets/list	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Table::Name::Outlets::Status
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Get Request	devices/table/${NAME}/outlets/status	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Table::Name::Outlets::On
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Get Request	devices/table/${NAME}/outlets/on	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Table::Name::Outlets::Off
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Get Request	devices/table/${NAME}/outlets/off	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Devices::Table::Name::Outlets::Cycle
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Get Request	devices/table/${NAME}/outlets/cycle	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

#	TODO	POR NA API????
#API::Get::Devices::Views
#	[Documentation]
#	...	Get device outlet list using the NG API
#	...	== REQUIREMENTS ==
#	...	A valid session create by API::Authenticate
#	...	== EXPECTED RESULT ==
#	...	Get the given device's outlet list info from NG
#	${response}=	API::Send Get Request	devices/views	MODE=${MODE}
#	[Return]	${response}
#
#API::Post::Devices::Views
#	[Arguments]	${PAYLOAD}
#	[Documentation]
#	...	Get device outlet list using the NG API
#	...	== REQUIREMENTS ==
#	...	A valid session create by API::Authenticate
#	...	== EXPECTED RESULT ==
#	...	Get the given device's outlet list info from NG
#	${response}=	API::Send Post Request	devices/views	${PAYLOAD}	MODE=${MODE}
#	[Return]	${response}
#
#API::Delete::Devices::Views
#	[Arguments]	${PAYLOAD}
#	[Documentation]
#	...	Get device outlet list using the NG API
#	...	== REQUIREMENTS ==
#	...	A valid session create by API::Authenticate
#	...	== EXPECTED RESULT ==
#	...	Get the given device's outlet list info from NG
#	${response}=	API::Send Delete Request	devices/views	${PAYLOAD}	MODE=${MODE}
#	[Return]	${response}
#
#API::Get::Devices::Views::${NAME}
#	[Documentation]
#	...	Get device outlet list using the NG API
#	...	== REQUIREMENTS ==
#	...	A valid session create by API::Authenticate
#	...	== EXPECTED RESULT ==
#	...	Get the given device's outlet list info from NG
#	${response}=	API::Send Get Request	devices/views/${NAME}	MODE=${MODE}
#	[Return]	${response}
#
#API::Put::Devices::Views::Name
#	[Arguments]	${NAME}	${PAYLOAD}
#	[Documentation]
#	...	Get device outlet list using the NG API
#	...	== REQUIREMENTS ==
#	...	A valid session create by API::Authenticate
#	...	== EXPECTED RESULT ==
#	...	Get the given device's outlet list info from NG
#	${response}=	API::Send Put Request	devices/views/${NAME}	${PAYLOAD}	MODE=${MODE}
#	[Return]	${response}
#
#API::Get::Devices::Views::Image
#	[Documentation]
#	...	Get device outlet list using the NG API
#	...	== REQUIREMENTS ==
#	...	A valid session create by API::Authenticate
#	...	== EXPECTED RESULT ==
#	...	Get the given device's outlet list info from NG
#	${response}=	API::Send Get Request	devices/views/image	MODE=${MODE}
#	[Return]	${response}
#
#API::Post::Devices::Views::Image
#	[Arguments]	${PAYLOAD}
#	[Documentation]
#	...	Get device outlet list using the NG API
#	...	== REQUIREMENTS ==
#	...	A valid session create by API::Authenticate
#	...	== EXPECTED RESULT ==
#	...	Get the given device's outlet list info from NG
#	${response}=	API::Send Post Request	devices/views/image	${PAYLOAD}	MODE=${MODE}
#	[Return]	${response}
#
#API::Delete::Devices::Views::Image
#	[Arguments]	${PAYLOAD}
#	[Documentation]
#	...	Get device outlet list using the NG API
#	...	== REQUIREMENTS ==
#	...	A valid session create by API::Authenticate
#	...	== EXPECTED RESULT ==
#	...	Get the given device's outlet list info from NG
#	${response}=	API::Send Delete Request	devices/views/image	${PAYLOAD}	MODE=${MODE}
#	[Return]	${response}
#
#API::Get::Devices::Views::Image::${NAME}
#	[Documentation]
#	...	Get device outlet list using the NG API
#	...	== REQUIREMENTS ==
#	...	A valid session create by API::Authenticate
#	...	== EXPECTED RESULT ==
#	...	Get the given device's outlet list info from NG
#	${response}=	API::Send Get Request	devices/views/image/${NAME}	MODE=${MODE}
#	[Return]	${response}
#
#API::Put::Devices::Views::Image::Name
#	[Arguments]	${NAME}	${PAYLOAD}
#	[Documentation]
#	...	Get device outlet list using the NG API
#	...	== REQUIREMENTS ==
#	...	A valid session create by API::Authenticate
#	...	== EXPECTED RESULT ==
#	...	Get the given device's outlet list info from NG
#	${response}=	API::Send Put Request	devices/views/image/${NAME}	${PAYLOAD}	MODE=${MODE}
#	[Return]	${response}

API::Get::Devices::Types
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get devices types using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return devices types info
	${response}=	API::Send Get Request	devices/types	MODE=${MODE}
	[Return]	${response}

API::Delete::Devices::Types
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Deletes device type using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Delete the given device type from NG
	${response}=	API::Send Delete Request	devices/types	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Devices::Types::Name
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get device type using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device type info from NG
	${response}=	API::Send Get Request	devices/types/${NAME}	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Types::Name
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update device type using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update device type
	${response}=	API::Send Put Request	devices/types/${NAME}	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Types::Name::Clone
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Clones device type using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Create a new device type cloning another one
	${payload}=	Create Dictionary
	${response}=	API::Send Post Request	devices/types/clone/${NAME}	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Devices::Discovery::Network
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Update device type using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update device type
	${response}=	API::Send Get Request	devices/discovery/network	MODE=${MODE}
	[Return]	${response}

API::Post::Devices::Discovery::Network
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update device type using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update device type
	${response}=	API::Send Post Request	devices/discovery/network	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Delete::Devices::Discovery::Network
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update device type using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update device type
	${response}=	API::Send Delete Request	devices/discovery/network	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Devices::Discovery::Network::Name
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Update device type using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update device type
	${response}=	API::Send Get Request	devices/discovery/network/${NAME}	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Discovery::Network::Name
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update device type using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update device type
	${response}=	API::Send Put Request	devices/discovery/network/${NAME}	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Devices::Discovery::VM Managers
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Get Request	devices/discovery/vmmanager	MODE=${MODE}
	[Return]	${response}

API::Post::Devices::Discovery::VM Managers
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Post Request	devices/discovery/vmmanager	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Delete::Devices::Discovery::VM Managers
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Delete Request	devices/discovery/vmmanager	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Devices::Discovery::VM Managers::Name
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Get Request	devices/discovery/vmmanager/${NAME}	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Discovery::VM Managers::Name
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Put Request	devices/discovery/vmmanager/${NAME}	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Devices::Discovery::Rules
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Get Request	devices/discovery/rules	MODE=${MODE}
	[Return]	${response}

API::Post::Devices::Discovery::Rules
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Post Request	devices/discovery/rules	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Delete::Devices::Discovery::Rules
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Delete Request	devices/discovery/rules	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Devices::Discovery::Rules::Name
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Get Request	devices/discovery/rules/${NAME}	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Discovery::Rules::Name
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Put Request	devices/discovery/rules/${NAME}	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Discovery::Rules::Name::Up
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Move the discovery rule up using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Move the discovery rule up using the NG API
	${response}=	API::Send Put Request	devices/discovery/rules/${NAME}/up	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Discovery::Rules::Name::Down
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Move the discovery rule down using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Move the discovery rule down using the NG API
	${response}=	API::Send Put Request	devices/discovery/rules/${NAME}/down	MODE=${MODE}
	[Return]	${response}

API::Get::Devices::Discovery::Hostname
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Get Request	devices/discovery/hostname	MODE=${MODE}
	[Return]	${response}

API::Post::Devices::Discovery::Hostname
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Post Request	devices/discovery/hostname	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Delete::Devices::Discovery::Hostname
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Delete Request	devices/discovery/hostname	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Devices::Discovery::Hostname::Name
	[Arguments]	${NAME}	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Get Request	devices/discovery/hostname/${NAME}	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Discovery::Hostname::Name
	[Arguments]	${NAME}	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Put Request	devices/discovery/hostname/${NAME}	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Devices::Discovery::Hostname Global Settings
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get hostname detection global settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return hostname global settings
	${response}=	API::Send Get Request	devices/discovery/hostname_globalsettings	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Discovery::Hostname Global Settings
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Update hostname detection global settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Update hostname global settings
	${response}=	API::Send Put Request	devices/discovery/hostname_globalsettings	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Devices::Discovery::Logs
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Get Request	devices/discovery/logs	MODE=${MODE}
	[Return]	${response}

API::Post::Devices::Discovery::Logs::Reset Logs
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Delete Request	devices/discovery/logs/resetlogs	MODE=${MODE}
	[Return]	${response}

API::Get::Devices::Discovery::Now
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Get Request	devices/discovery/now	MODE=${MODE}
	[Return]	${response}

API::Post::Devices::Discovery::Now
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Get device outlet list using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Get the given device's outlet list info from NG
	${response}=	API::Send Post Request	devices/discovery/now	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Devices::Preference::Power
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get devices power settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return devices power settings info
	${response}=	API::Send Get Request	devices/preference/power	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Preference::Power
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Get devices power settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return devices power settings info
	${response}=	API::Send Put Request	devices/preference/power	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}

API::Get::Devices::Preference::Session
	[Arguments]	${MODE}=yes
	[Documentation]
	...	Get devices power settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return devices power settings info
	${response}=	API::Send Get Request	devices/preference/session	MODE=${MODE}
	[Return]	${response}

API::Put::Devices::Preference::Session
	[Arguments]	${PAYLOAD}	${MODE}=yes
	[Documentation]
	...	Get devices session settings using the NG API
	...	== REQUIREMENTS ==
	...	A valid session create by API::Authenticate
	...	== EXPECTED RESULT ==
	...	Return devices session settings info
	${response}=	API::Send Put Request	devices/preference/session	${PAYLOAD}	MODE=${MODE}
	[Return]	${response}