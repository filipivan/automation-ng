*** Keywords ***
# First keyword
#	[Tags]	GUI	BROWSER
#	[Arguments]	${HOMEPAGE}	${BROWSER}
#	[Documentation]	Opens a browser instance and navigates to the provided URL
#	...	== REQUIREMENTS ==
#	...	== ARGUMENTS ==
#	...	== EXPECTED RESULT ==

*** Keywords ***
GUI::Basic::Open NodeGrid
	[Tags]	GUI	BROWSER
	[Arguments]	${PAGE}=${HOMEPAGE}	${MYBROWSER}=${BROWSER}	${VERSION}=${NGVERSION}	${ALIAS}=defaultOpen	${DESIRED_CAPABILITIES}=${EMPTY}	${SCREENSHOT}=${FALSE}
	[Documentation]	Opens a browser instance and navigates to the provided URL
	...	== REQUIREMENTS ==
	...	None
	...	== ARGUMENTS ==
	...	- HOMEPAGE -	URL to which should be navidated i.e. https://localhost
	...	- BROWSER	-	Which bowser should be used. It has to be a Browser which is supported by the Selenium Framnework and the driver has to be installed on the local system
	...	== EXPECTED RESULT ==
	...	Browser session was opened and the Login window of the NGM is presented
	${MYBROWSER}	Convert To Lower Case	${MYBROWSER}
	Log	${MYBROWSER}	console=yes
	IF	'${DESIRED_CAPABILITIES}' == '${EMPTY}'
		Run Keyword If	'${MYBROWSER}' == 'chrome' or '${MYBROWSER}' == 'headlesschrome'	Wait Until Keyword Succeeds	3x	3s	Open Browser	${PAGE}	${MYBROWSER}	alias=${ALIAS}	options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors")
		Run Keyword If	'${MYBROWSER}' == 'firefox' or '${MYBROWSER}' == 'headlessfirefox'	Wait Until Keyword Succeeds	3x	3s	Open Browser	${PAGE}	${MYBROWSER}	alias=${ALIAS}	options=set_preference("moz:dom.disable_beforeunload", "true")
	ELSE
		Run Keyword If	'${MYBROWSER}' == 'chrome' or '${MYBROWSER}' == 'headlesschrome'	Wait Until Keyword Succeeds	3x	3s	Open Browser	${PAGE}	${MYBROWSER}	alias=${ALIAS}	desired_capabilities=${DESIRED_CAPABILITIES}	alias=${ALIAS}	options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors")
		Run Keyword If	'${MYBROWSER}' == 'firefox' or '${MYBROWSER}' == 'headlessfirefox'	Wait Until Keyword Succeeds	3x	3s	Open Browser	${PAGE}	${MYBROWSER}	alias=${ALIAS}	desired_capabilities=${DESIRED_CAPABILITIES}	alias=${ALIAS}	options=set_preference("moz:dom.disable_beforeunload", "true")
	END
	Maximize Browser Window
	Run Keyword If	'${VERSION}' == '3.2'	GUI3.x::Basic::Open NodeGrid
	Run Keyword If	'${VERSION}' >= '4.0'	GUI4.x::Basic::Open NodeGrid
	Run Keyword If	'${SCREENSHOT}' != '${FALSE}'	Capture Page Screenshot	filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI3.x::Basic::Open NodeGrid
	Wait Until Page Contains Element	id=username	30s
	Wait Until Element Is Visible	css=input[id=password]	30s

GUI4.x::Basic::Open NodeGrid
#	Wait Until Page Contains Element	id=username
#	Wait Until Element Is Visible	css=input[id=password]
	Wait Until Page Contains Element	username
	Wait Until Element Is Visible	password

GUI::Basic::Login
	[Tags]	GUI	BASIC	LOGIN
	[Arguments]	${VERSION}=${NGVERSION}	${USERNAME}=admin	${PASSWORD}=admin	${SCREENSHOT}=${FALSE}
	[Documentation]	A user will be authenticated agains the NodeGrid Manager with he provided USERNAME and PASSWORD
	...	== REQUIREMENTS ==
	...	NGM GUI is open, User will be logod out if a current active session exist
	...	== ARGUMENTS ==
	...	-	USERNAME -	USERNAME which should be used. Default: admin
	...	-	PASSWORD -	PASSWORD which should be used. Default: admin
	...	== EXPECTED RESULT ==
	...	User is successfuly authenticated and the NodeGrid access page is displayed
	Run Keyword If	'${VERSION}' >= '4.0'	GUI4.x::Basic::Login	${USERNAME}	${PASSWORD}	${SCREENSHOT}
	Run Keyword If	'${VERSION}' == '3.2'	GUI3.x::Basic::Login	${USERNAME}	${PASSWORD}	${SCREENSHOT}

GUI3.x::Basic::Login
	[Tags]	GUI	BASIC	LOGIN
	[Arguments]	${USERNAME}=admin	${PASSWORD}=admin	${SCREENSHOT}=${FALSE}
	GUI3.x::Basic::Open NodeGrid
	Input Text	css=#username	${USERNAME}
	Input Text	css=input[id=password]	${PASSWORD}
	Click Element	id=login-btn
	Wait Until Page Contains Element	id=main_doc
	Wait Until Element Is Visible	id=pwl
	Wait Until Element Is Visible	//*[@id="action"]/div/div/div[1]/a
	Run Keyword If	'${SCREENSHOT}' != '${FALSE}'	Capture Page Screenshot	filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI4.x::Basic::Login
	[Tags]	GUI	BASIC	LOGIN
	[Arguments]	${USERNAME}=admin	${PASSWORD}=admin	${SCREENSHOT}=${FALSE}
	GUI4.x::Basic::Open NodeGrid
	Input Text	username	${USERNAME}
	Input Text	password	${PASSWORD}
	Click Element	id=login-btn
	Wait Until Keyword Succeeds	1m	2s	Wait Until Element Is Visible	id=main_menu
	Wait Until Element Is Visible	id=pwl
	Wait Until Element Is Visible	jquery=div.widget-title > h4
	GUI::Basic::Spinner Should Be Invisible
	Run Keyword If	'${SCREENSHOT}' != '${FALSE}'	Capture Page Screenshot	filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI3.x::Basic::LoginWithEnter
	[Tags]	GUI	BASIC	LOGIN
	[Arguments]	${USERNAME}=admin	${PASSWORD}=admin	${SCREENSHOT}=${FALSE}
	GUI3.x::Basic::Open NodeGrid
	Input Text	css=#username	${USERNAME}
	Input Text	css=input[id=password]	${PASSWORD}
	Press Keys	password	\\13
	Wait Until Page Contains Element	id=main_doc
	Wait Until Element Is Visible	id=pwl
	Wait Until Element Is Visible	//*[@id="action"]/div/div/div[1]/a
	Run Keyword If	'${SCREENSHOT}' != '${FALSE}'	Capture Page Screenshot	filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI::Basic::Login Failure
	[Tags]	GUI	BASIC	LOGIN
	[Arguments]	${NGVERSION}	${USERNAME}	${PASSWORD}	${SCREENSHOT}=${FALSE}
	[Documentation]	A user will be authenticated agains the NodeGrid Manager with he provided USERNAME and PASSWORD. It is expected that the authentication fails
	...	== REQUIREMENTS ==
	...	NGM GUI is open, User will be logod out if a current active session exist
	...	== ARGUMENTS ==
	...	-	USERNAME -	USERNAME which should be used
	...	-	PASSWORD -	PASSWORD which should be used
	...	-	SCREENSHOT - Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	User authenticatin is expected to fail and the NodeGrid access page is *not* displayed
	Run Keyword If	'${NGVERSION}' == '4.0'	GUI4.x::Basic::Login Failure	${USERNAME}	${PASSWORD}	${SCREENSHOT}
	Run Keyword If	'${NGVERSION}' == '3.2'	GUI3.x::Basic::Login Failure	${USERNAME}	${PASSWORD}	${SCREENSHOT}

GUI3.x::Basic::Login Failure
	[Arguments]	${USERNAME}	${PASSWORD}	${SCREENSHOT}=${FALSE}
	GUI3.x::Basic::Open NodeGrid
	Input Text	css=#username	${USERNAME}
	Input Text	css=input[id=password]	${PASSWORD}
	Click Element	id=login-btn
	Wait Until Element Is Visible	css=input[id=password]
	Page Should Contain	Login failed
	Run Keyword If	'${SCREENSHOT}' != '${FALSE}'	Capture Page Screenshot	filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI4.x::Basic::Login Failure
	[Arguments]	${USERNAME}	${PASSWORD}	${SCREENSHOT}=${FALSE}
	GUI4.x::Basic::Open NodeGrid
	Input Text	css=#username	${USERNAME}
	Input Text	password	${PASSWORD}
	Click Element	id=login-btn
	Wait Until Element Is Visible	password
	Wait Until Element Is Visible	jquery=div.alert-danger	error=Error message not found
	Run Keyword If	'${SCREENSHOT}' != '${FALSE}'	Capture Page Screenshot	filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI::Basic::Open And Login Nodegrid
	[Documentation]	A wrapper of the GUI::Basic::Open Nodegrid and GUI::Basic::Login keywords.
	...	Hence, the tests can open and log in the system in one keyword.
	...	The current implementation uses default credentials (admin/admin)
	[Tags]	GUI	BASIC	BROWSER	LOGIN
	[Arguments]	${USERNAME}=admin	${PASSWORD}=admin	${PAGE}=${HOMEPAGE}	${MYBROWSER}=${BROWSER}	${VERSION}=${NGVERSION}	${ALIAS}=defaultOpenLogin	${SCREENSHOT}=${FALSE}
	GUI::Basic::Open NodeGrid	${PAGE}	${MYBROWSER}	VERSION=${VERSION}	ALIAS=${ALIAS}
	GUI::Basic::Login	${VERSION}	${USERNAME}	${PASSWORD}

GUI::Basic::Direct Login
	[Tags]	GUI	BASIC	DIRECT_LOGIN
	[Arguments]	${USERNAME}=admin	${PASSWORD}=admin	${SCREENSHOT}=${FALSE}	${DEVICE}=DEVICE	${DIRECT_LOGIN}=TRUE
	[Documentation]	A user will login the NodeGrid console /or other console with the provided username and password
	Input Text	username	${USERNAME}
	Input Text	password	${PASSWORD}
	Click Element	login-btn
	Wait Until Page Contains Element	BannerMessage

GUI::Basic::Logout
	[Tags]	GUI	BROWSER	LOGOUT
	[Arguments]	${SCREENSHOT}=${FALSE}
	[Documentation]	Logs the current user out and returns back to the NodeGrid Manager Login page
	...	== REQUIREMENTS ==
	...	NONE
	...	== ARGUMENTS ==
	...	NONE
	...	== EXPECTED RESULT ==
	...	Logs the current user out and returns back to the NodeGrid Manager Login page
	Wait Until Element Is Visible	lo
	GUI::Basic::Click Element	//*[@id='lo']
	Wait Until Element Is Visible	id=username
	Run Keyword If	'${SCREENSHOT}' != '${FALSE}'	Capture Page Screenshot	filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI::Basic::Logout and Close Nodegrid
	[Arguments]	${MODE}=${EMPTY}
	GUI::Basic::Logout
	Run Keyword If	'${MODE}' == 'Raw'	Close Browser
	Close All Browsers

GUI::Basic::Help
	[Tags]	GUI	BASIC	HELP
	[Arguments]	${SCREENSHOT}=${FALSE}
	[Documentation]	Open thes the NodeGrid Managers Help File
	...	== REQUIREMENTS ==
	...	Valid session to the NodeGride Manger exist
	...	== ARGUMENTS ==
	...	NONE
	...	== EXPECTED RESULT ==
	...	Help File is opened in a new tab/window
	Wait Until Element Is Visible	id=hel
	Click Element	id=hel
	Sleep	5s
	${SPLIT_VERSION}=	Split String	${NGVERSION}	.
	${PATH_VERSION}=	Set Variable	${SPLIT_VERSION}[0]_${SPLIT_VERSION}[1]

	Run Keyword If	'${NGVERSION}' < '4.0'	Switch Window	url=http://www.zpesystems.com/ng/v3_0/NodeGrid-UserGuide-v3_0.pdf
	Run Keyword If	'${NGVERSION}' >= '4.0'	Switch Window	url=https://www.zpesystems.com/ng/v${PATH_VERSION}/NodegridManual${NGVERSION}.html
	Run Keyword If	'${SCREENSHOT}' != '${FALSE}'	Sleep	5s
	Run Keyword If	'${SCREENSHOT}' != '${FALSE}'	Capture Page Screenshot	filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png
	Switch Window

GUI::Basic::ZPE Link
	[Tags]	GUI	BROWSER	ZPE WEBPAGE
	[Arguments]	${SCREENSHOT}=${FALSE}
	[Documentation]	Clicks on the ZPE Web Side Link and confirms that the ZPE Web page is displayed
	...	== REQUIREMENTS ==
	...	Valid session to the NodeGride Manger exist
	...	== ARGUMENTS ==
	...	NONE
	...	== EXPECTED RESULT ==
	...	The ZPE web page is opened in a new tab/window
	Click Element	id=pwl
	Wait Until Page Contains Element	id=abt
	Click Element	//*[@id="header_inbox_bar"]/ul/li[1]/a
	Sleep	2s
	${TITLE}=	Set Variable	Home - ZPE Systems - Rethink the Way Networks are Built and Managed
	Switch Window	${TITLE}
	Title Should Be	${TITLE}
#	Switch Window	Home - ZPE Systems - Helping You Reduce Downtime
#	Title Should Be	Home - ZPE Systems - Helping You Reduce Downtime
	Run Keyword If	'${SCREENSHOT}' != '${FALSE}'	Sleep	5s
	Run Keyword If	'${SCREENSHOT}' != '${FALSE}'	Capture Page Screenshot	filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png
	Switch Window

GUI::Basic::About::${OPTION}
	[Tags]	GUI	BROWSER
	[Documentation]	Return the System setting from About page
	Run Keyword and Return If	'${NGVERSION}' >= '4.0'	GUI4.x::Basic::About::${OPTION}
#	Return From Keyword	GUI3.x::Basic::About::${OPTION}

GUI4.x::Basic::About::${OPTION}
	[Tags]	GUI	BROWSER	ABOUT
	[Documentation]	Login in NG and check the cpu cores
	...	== REQUIREMENTS ==
	...	Valid session to the NodeGride Manger exist
	...	== ARGUMENTS ==
	...	NONE
	...	== EXPECTED RESULT ==
	...	Get information about NG Manager
	Click Element	pwl
	Wait Until Page Contains Element	abt
	Click Element	abt
	Wait Until Page Contains	System
	${TEXT}=	Get Text	${OPTION}
	[Return]	${TEXT}

GUI::Basic::About
	[Arguments]	${NGVERSION}	${SCREENSHOT}=FALSE
	[Documentation]	Opens the About information page and confirms it details
	...	== REQUIREMENTS ==
	...	Requires a valid bowser session see keyword:	login ngm
	...	== ARGUMENTS ==
	...	None
	...	== EXPECTED RESULT ==
	...	The About page is displayed and contains the following Text Elements
	...	- System:
	...	- Version
	...	- Licenses:
	...	- CPU:
	...	- CPU Cores:
	...	- Bogomips per core:
	...	- Serial Number:
	[Tags]	GUI	BROWSER
	Run Keyword If	'${NGVERSION}' == '4.0'	GUI4.x::Basic::About	${SCREENSHOT}
	Run Keyword If	'${NGVERSION}' == '3.2'	GUI3.x::Basic::About	${SCREENSHOT}

GUI3.x::Basic::About
	[Arguments]	${SCREENSHOT}=FALSE
	Wait Until Element Is Visible	//*[@id="action"]/div/div/div[1]/a
	Wait Until Element Is Enabled	id=pwl
	Click Element	id=pwl
	Wait Until Page Contains Element	id=abt
	Click Element	id=abt
	Wait Until Page Contains	Serial Number
	${Text1}=	Get Text	//table/tbody/tr[2]/td/table/tbody/tr[1]/td[2]
	Element Text Should Be	//table/tbody/tr[2]/td/table/tbody/tr[1]/td[1]	System:
	Element Text Should Be	//table/tbody/tr[2]/td/table/tbody/tr[2]/td[1]	Version:
	Element Text Should Be	//table/tbody/tr[2]/td/table/tbody/tr[3]/td[1]	Licenses:
	Element Text Should Be	//table/tbody/tr[2]/td/table/tbody/tr[4]/td[1]	CPU:
	Element Text Should Be	//table/tbody/tr[2]/td/table/tbody/tr[5]/td[1]	CPU Cores:
	Element Text Should Be	//table/tbody/tr[2]/td/table/tbody/tr[6]/td[1]	Bogomips per core:
	Element Text Should Be	//table/tbody/tr[2]/td/table/tbody/tr[7]/td[1]	Serial Number:

	RUN KEYWORD IF	'${Text1}' == 'NodeGrid Serial Console'	Element Text Should Be	//table/tbody/tr[2]/td/table/tbody/tr[8]/td[1]	Model:
	RUN KEYWORD IF	'${Text1}' == 'NodeGrid Serial Console'	Element Text Should Be	//table/tbody/tr[2]/td/table/tbody/tr[9]/td[1]	Part Number:
	RUN KEYWORD IF	'${Text1}' == 'NodeGrid Serial Console'	Element Text Should Be	//table/tbody/tr[2]/td/table/tbody/tr[10]/td[1]	BIOS Version:
	RUN KEYWORD IF	'${Text1}' == 'NodeGrid Serial Console'	Element Text Should Be	//table/tbody/tr[2]/td/table/tbody/tr[11]/td[1]	PSU:


	RUN KEYWORD IF	'${Text1}' == 'NodeGrid Serial Console'	Element Text Should Be	//table/tbody/tr[2]/td/table/tbody/tr[1]/td[2]	NodeGrid Serial Console
	...	ELSE IF	'${Text1}' == 'NodeGrid Manager'	Element Text Should Be	//table/tbody/tr[2]/td/table/tbody/tr[1]/td[2]	NodeGrid Manager
	...	ELSE	FAIL	Not a known About page
	Run Keyword If	'${SCREENSHOT}' != 'FALSE'	Capture Page Screenshot	filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png
	Click Element	//*[@id="modal_b"]/button

GUI3.x::Basic::Get System
	[Arguments]	${SCREENSHOT}=FALSE
	Wait Until Element Is Visible	//*[@id="action"]/div/div/div[1]/a
	Wait Until Element Is Enabled	id=pwl
	Click Element	id=pwl
	Wait Until Page Contains Element	id=abt
	Click Element	id=abt
	Wait Until Page Contains	Serial Number
	${ret}=	Get Text	//table/tbody/tr[2]/td/table/tbody/tr[1]/td[2]
	Click Element	//*[@id="modal_b"]/button
	[Return]	${ret}

GUI4.x::Basic::About
	[Arguments]	${SCREENSHOT}=FALSE
	Wait Until Element Is Visible	jquery=#main_doc
	Wait Until Element Is Enabled	id=pwl
	Click Element	id=pwl
	Wait Until Page Contains Element	id=abt
	Click Element	id=abt
	Wait Until Page Contains	Serial Number
	${Text1}=	Get Text	jquery=#system
	Element Text Should Be	jquery=body > div.modal.in > div > div > div.modal-body > div:nth-child(2) > div.col-xs-3 > span	System:
	Element Text Should Be	jquery=body > div.modal.in > div > div > div.modal-body > div:nth-child(3) > div.col-xs-3 > span	Version:
	Element Text Should Be	jquery=body > div.modal.in > div > div > div.modal-body > div:nth-child(4) > div.col-xs-3 > span	Licenses:
	Element Text Should Be	jquery=body > div.modal.in > div > div > div.modal-body > div:nth-child(5) > div.col-xs-3 > span	CPU:
	Element Text Should Be	jquery=body > div.modal.in > div > div > div.modal-body > div:nth-child(6) > div.col-xs-3 > span	CPU Cores:
	Element Text Should Be	jquery=body > div.modal.in > div > div > div.modal-body > div:nth-child(7) > div.col-xs-3 > span	Bogomips per core:
	Element Text Should Be	jquery=body > div.modal.in > div > div > div.modal-body > div:nth-child(8) > div.col-xs-3 > span	Serial Number:
	Element Text Should Be	jquery=body > div.modal.in > div > div > div.modal-body > div:nth-child(9) > div.col-xs-3 > span	Uptime:

	RUN KEYWORD IF	'${Text1}' == 'NodeGrid Serial Console'	Element Text Should Be	jquery=body > div.modal.in > div > div > div.modal-body > div:nth-child(10) > div.col-xs-3 > span	Model:
	RUN KEYWORD IF	'${Text1}' == 'NodeGrid Serial Console'	Element Text Should Be	jquery=body > div.modal.in > div > div > div.modal-body > div:nth-child(11) > div.col-xs-3 > span	Part Number:
	RUN KEYWORD IF	'${Text1}' == 'NodeGrid Serial Console'	Element Text Should Be	jquery=body > div.modal.in > div > div > div.modal-body > div:nth-child(12) > div.col-xs-3 > span	BIOS Version:
	RUN KEYWORD IF	'${Text1}' == 'NodeGrid Serial Console'	Element Text Should Be	jquery=body > div.modal.in > div > div > div.modal-body > div:nth-child(13) > div.col-xs-3 > span	PSU:


	RUN KEYWORD IF	'${Text1}' == 'NodeGrid Serial Console'	Element Text Should Be	id=system	NodeGrid Serial Console
	...	ELSE IF	'${Text1}' == 'NodeGrid Manager'	Element Text Should Be	id=system	NodeGrid Manager
	...	ELSE	FAIL	Not a known About page
	Run Keyword If	'${SCREENSHOT}'!= 'FALSE'	Capture Page Screenshot	filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png
	Click Button	jquery=div.modal button

GUI::${PATH}::${SUBPATH}::Open Tab
	[Documentation]	Opens the NodeGrid Manager path and subpath
	[Tags]	GUI	${PATH}	${SUBPATH}
	${MYPATH}=	Convert to Lowercase 	${PATH}
	${MYPATH}=	Replace String	${MYPATH}	${SPACE}	_
	GUI::Basic::Get Menu
	Click Element	${MYPATH}
	GUI::Basic::Spinner Should Be Invisible

	${MYSUBPATH}=	Convert to Lowercase 	${SUBPATH}
	${MYSUBPATH}=	Replace String	${MYSUBPATH}	${SPACE}	_
	GUI::Basic::Get Sub Menu
	Click Element	${MYSUBPATH}
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Check Rows In Table
	[Arguments]	${LOCATOR}	${LINES}
	[Documentation]	Count table rows
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//table[@id='${LOCATOR}']/tbody/tr	limit=${LINES}

GUI::Basic::Count Table Rows With Id
	[Arguments]	${LOCATOR}	${ID}
	[Documentation]	Count table rows and return it
	${TABLE_ID}=	Get Element Attribute	${LOCATOR}	id
	${count}=	Get Element Count	//table[@id='${TABLE_ID}']/tbody/tr[@id='${ID}']
	${return}=	Convert To Integer	${count}
	[Return]	${return}

GUI::Basic::Delete All Rows In Table If Exists
	[Arguments]	${LOCATOR}	${HAS_ALERT}=True
	[Documentation]	Delete all table rows if there are some
	GUI::Basic::Spinner Should Be Invisible
	${count}=	Get Element Count	jquery=${LOCATOR} > tbody > tr
	Run Keyword If	${count} > 0	GUI::Basic::Delete All Rows In Table	${LOCATOR}	${HAS_ALERT}

GUI::Basic::Delete All Rows In Table
	[Arguments]	${LOCATOR}	${HAS_ALERT}=True
	[Documentation]	Delete all table rows
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	jquery=${LOCATOR} >thead input[type=checkbox]
	GUI::Basic::Delete	${HAS_ALERT}

GUI::Basic::Select Rows In Table Containing Value
	[Arguments]	${TABLE_ID}=table	${VALUE}=""
	[Documentation]	Select table rows which contains the given value

	${count}=	Get Element Count	xpath://table[@id="${TABLE_ID}"]/tbody/tr
	${COUNT_SELECTIONS}=	Set Variable	0

	FOR	${index}	IN RANGE	1	${count}+1
		${has_value} =	Run Keyword And Return Status	Table Row Should Contain	id=${TABLE_ID}	${index}	${VALUE}
		Run Keyword If	${has_value}	Select Checkbox	xpath://table[@id="${TABLE_ID}"]/tbody/tr[${index}]/td[1]/input
	END

GUI::Basic::Delete Rows In Table Containing Value
	[Arguments]	${TABLE_ID}=table	${VALUE}=""	${HAS_ALERT}=True
	[Documentation]	Delete table rows which contains the given value
	GUI::Basic::Select Rows In Table Containing Value	${TABLE_ID}	${VALUE}
	GUI::Basic::Delete	${HAS_ALERT}

GUI::Basic::Delete Rows In Table
	[Arguments]	${LOCATOR}	@{IDS}
	[Documentation]	Delete table rows
	FOR		${ID}	IN	@{IDS}
		Select Checkbox	jquery=${LOCATOR} tr#${ID} input[type=checkbox]
	END
	GUI::Basic::Delete With Alert

GUI::Basic::Delete Rows In Table Without Alert
	[Arguments]	${LOCATOR}	@{IDS}
	[Documentation]	Delete table rows
	FOR		${ID}	IN	@{IDS}
		Select Checkbox	jquery=${LOCATOR} tr#${ID} input[type=checkbox]
	END
	GUI::Basic::Delete

GUI::Basic::Delete Rows In Table If Exists
	[Arguments]	${LOCATOR}=jquery=table	${IDS}=[]	${HAS_ALERT}=${TRUE}
	[Documentation]	Delete table rows when ID exists
	${COUNT_SELECTIONS}=	Evaluate	0
	${TABLE_ID}=	Get Element Attribute	${LOCATOR}	id
	FOR		${ID}	IN	@{IDS}
		${count}=	GUI::Basic::Count Table Rows With Id	${LOCATOR}	${ID}
		${COUNT_SELECTIONS}=	Evaluate	${COUNT_SELECTIONS} + ${count}
		Run Keyword If	'${count}' > '0'	GUI::Basic::Select Table Row By Id	${LOCATOR}	${ID}
	END
	Return From Keyword If	'${COUNT_SELECTIONS}' == '0'
	Run Keyword If	'${HAS_ALERT}' == '${TRUE}'	GUI::Basic::Delete With Alert
	Run Keyword If	'${HAS_ALERT}' == '${FALSE}'	GUI::Basic::Delete

GUI::Basic::Edit Rows In Table
	[Arguments]	${LOCATOR}	@{IDS}
	[Documentation]	Edit table rows
	FOR		${ID}	IN	@{IDS}
		Select Checkbox	jquery=${LOCATOR} tr[id='${ID}'] input[type=checkbox]
	END
	Click Element	jquery=#editButton
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Table Should Not Contain
	[Arguments]	${LOCATOR}	${CONTENT}
	[Documentation]	Check if table does not contain the parameter's element
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	//table[@id='${LOCATOR}']/tbody/tr[td='${CONTENT}']	limit=0

GUI::Basic::Drilldown table row
	[Arguments]	${LOCATOR}	${ID}
	[Documentation]	Fires a drilldown action in a table row
	Click Element	jquery=${LOCATOR} tr#${ID} a
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Table Should Has Row With Id
	[Arguments]	${LOCATOR}	${ID}	${SCREENSHOT}=${FALSE}
	[Documentation]	Check if the given table has a row with the given id
	${count}=	Get Element Count	//table[@id='${LOCATOR}']/tbody/tr[@id='${ID}']
	Should Be True	${count} > 0	There is a row with the given ID

GUI::Basic::Table Should Not Have Row With Id
	[Arguments]	${LOCATOR}	${ID}	${SCREENSHOT}=${FALSE}
	[Documentation]	Check if the given table has a row with the given id
	${count}=	Get Element Count	//table[@id='${LOCATOR}']/tbody/tr[@id='${ID}']
	Should Be True	${count} == 0	There is no row with the given ID

GUI::Basic::Select Table Rows
	[Arguments]	${LOCATOR}	${IDS}	${SCREENSHOT}=${FALSE}
	[Documentation]	Select table rows
	...	== ARGUMENTS ==
	...	-	LOCATOR = Table locator in jquery format
	...	-	IDS = Rows ids which should be selected
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Select the table row of the given ids
	[Tags]	GUI	BASIC	TABLE
	${TABLE_ID}=	Get Element Attribute	${LOCATOR}	id
	FOR		${ID}	IN	@{IDS}
		Select Checkbox	//*[@id="${TABLE_ID}"]//tbody//tr[@id="${ID}"]//input[@type="checkbox"]
	END

GUI::Basic::Select Table Row By Id
	[Arguments]	${LOCATOR}	${ID}	${SCREENSHOT}=${FALSE}
	[Documentation]	Select table rows
	...	== ARGUMENTS ==
	...	-	LOCATOR = Table locator
	...	-	IDS = Row id which should be selected
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Select the table row of the given id
	[Tags]	GUI	BASIC	TABLE
	${TABLE_ID}=	Get Element Attribute	${LOCATOR}	id
	Select Checkbox	//*[@id="${TABLE_ID}"]//tbody//tr[@id="${ID}"]//input[@type="checkbox"]

GUI::Basic::Count Table Rows
	[Arguments]	${LOCATOR}	${SCREENSHOT}=${FALSE}
	[Documentation]	Count the number of the rows (tr) for the given table
	...	== ARGUMENTS ==
	...	-	LOCATOR = Table id
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Returns the number table's rows count
	[Tags]	GUI	BASIC	TABLE
	GUI::Basic::Spinner Should Be Invisible
	${count}=	Get Element Count	//table[@id='${LOCATOR}']/tbody/tr
	${count}=	Convert To Integer	${count}
	[Return]	${count}

GUI::Basic::Table Rows Count Should Be Greater
	[Arguments]	${LOCATOR}	${EXPECTED}	${SCREENSHOT}=${FALSE}
	[Documentation]	Check if the total table rows count is greater then the given value
	GUI::Basic::Spinner Should Be Invisible
	${count}=	GUI::Basic::Count Table Rows	${LOCATOR}
	Should Be True	${count} > ${EXPECTED}

GUI::Basic::Get Table Values
	[Arguments]	${LOCATOR}	${SCREENSHOT}=${FALSE}
	[Documentation]	Get the values displayed in the given table
	...	== ARGUMENTS ==
	...	-	LOCATOR = Table id
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Returns the number table's rows count
	[Tags]	GUI	BASIC	TABLE
	${count} =	GUI::Basic::Count Table Rows	${LOCATOR}
	${rows} =	Create List
	FOR		${i}	IN RANGE	2	${count}+2
		${row} =	GUI::Basic::Get Table Row Values By Index	${LOCATOR}	${i}
		Append To List	${rows}	${row}
	END
	[Return]	${rows}

GUI::Basic::Table Values Should Be Equal
	[Arguments]	${T1}	${T2}	${SCREENSHOT}=${FALSE}
	[Documentation]	Compare two table values generated by	GUI::Basic::Get Table Values
	...	== ARGUMENTS ==
	...	-	T1 = First table values
	...	-	T2 = First table values
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	[Tags]	GUI	BASIC	TABLE
	Lists Should Be Equal	${T1}	${T2}

GUI::Basic::Get Table Row Values By Index
	[Arguments]	${LOCATOR}	${ROW}	${SCREENSHOT}=${FALSE}
	[Documentation]	Get the values from the given table row
	...	== ARGUMENTS ==
	...	-	LOCATOR = Table id
	...	-	ROW = Row index
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Returns the number table rows content
	[Tags]	GUI	BASIC	TABLE
	${headers} =	GUI::Basic::Get Table Headers	${LOCATOR}
	${values} =	Create Dictionary
	FOR		${header}	IN	@{headers}
		${col} =	GUI::Basic::Get Table Header Index	${LOCATOR}	${header}
		${value} =	Get Table Cell	jquery=table\#${LOCATOR}	${ROW}	${col}
		Set To Dictionary	${values}	${header}	${value}
	END
	[Return]	${values}

GUI::Basic::Get Table Header Index
	[Arguments]	${LOCATOR}	${HEADER}	${SCREENSHOT}=${FALSE}
	[Documentation]	Get the header index
	...	== ARGUMENTS ==
	...	-	LOCATOR = Table id
	...	-	HEADER = Row index
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Returns the index of the header inside its table
	[Tags]	GUI	BASIC	TABLE
	${count}=	GUI::Basic:: Get Table Columns Count	${LOCATOR}
	FOR		${I}	IN RANGE	1	${count}+1
		${is_selectable} =	GUI::Basic::Is Table Column Selectable	${LOCATOR}	${I}
		Continue For Loop If	${is_selectable}
		${h} =	GUI::Basic::Get Table Header By Index	${LOCATOR}	${I}
		Return From Keyword If	'${h}' == '${HEADER}'	${I}
	END
	${index} =	Set Variable	-1
	[Return]	${index}

GUI::Basic::Get Table Header By Index
	[Arguments]	${LOCATOR}	${INDEX}	${SCREENSHOT}=${FALSE}
	[Documentation]	Get the table header by index
	...	== ARGUMENTS ==
	...	-	LOCATOR = Table locator
	...	-	HEADER = Header index
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Returns the table header value
	[Tags]	GUI	BASIC	TABLE
	${is_selectable} =	GUI::Basic::Is Table Column Selectable	${LOCATOR}	${INDEX}
	Run Keyword If	${is_selectable}	Fail	Invalid header column
	${text}=	Get Table Cell	${LOCATOR}	1	${INDEX}
	[Return]	${text}

GUI::Basic::Get Table Headers
	[Arguments]	${LOCATOR}	${SCREENSHOT}=${FALSE}
	[Documentation]	Get table headers
	...	== ARGUMENTS ==
	...	-	LOCATOR = Table id
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Returns a list with the table headers
	${headers} =	Create List
	${count}=	GUI::Basic:: Get Table Columns Count	${LOCATOR}
	FOR		${I}	IN RANGE	1	${count}+1
		${is_selectable} =	GUI::Basic::Is Table Column Selectable	${LOCATOR}	${I}
		Continue For Loop If	${is_selectable}
		${TEXT}=	Get Text	//table[@id='${LOCATOR}']/thead/tr/th[${I}]
		Append To List	${headers}	${TEXT}
	END
	[Return]	${headers}

GUI::Basic::Is Table Column Selectable
	[Arguments]	${LOCATOR}	${INDEX}	${SCREENSHOT}=${FALSE}
	[Documentation]	Check if the table column is selectable or not
	...	== ARGUMENTS ==
	...	-	LOCATOR = Table id
	...	-	INDEX = Column index
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Return a boolean value showing if the columns is selectable
	${count}=	Get Element Count	//table[@id='${LOCATOR}']/thead/tr/th[${index}][contains(@class, 'selectable')]
	${is_selectable} =	Evaluate	${count} > 0
	[Return]	${is_selectable}

GUI::Basic::Get Table Columns Count
	[Arguments]	${LOCATOR}	${SCREENSHOT}=${FALSE}
	[Documentation]	Get table columns count
	...	== ARGUMENTS ==
	...	-	LOCATOR = Table id
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Returns the table columns count
	${count}=	Get Element Count	//table[@id='${LOCATOR}']/thead/tr/th
	[Return]	${count}

GUI::Basic::Get Table Rows Count
	[Arguments]	${LOCATOR}	${SCREENSHOT}=${FALSE}
	[Documentation]	Get table rows count
	...	== ARGUMENTS ==
	...	-	LOCATOR = Table id
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Returns the table rows count
	${count}=	Get Element Count	//table[@id='${LOCATOR}']/tbody/tr
	[Return]	${count}

GUI::Basic::Scope Should Be
	[Arguments]	${EXPECTED}
	[Documentation]	Test if scope is equal the expected
	${SCOPE} =	Execute Javascript	return document.getElementById("scope").innerHTML;
	Should Be Equal	${SCOPE}	${EXPECTED}

GUI::Basic::Return Button
	GUI::Basic::Click Element	//*[@id='returnButton']
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Get Page Title
	[Documentation]	Returns the title that is below the menus like 'Access :: Table'
	Wait Until Element Is Visible	jquery=.widget-title h4
	${WIDGET_TILE}=	Get Text	jquery=.widget-title h4
	[Return]	${WIDGET_TILE}

GUI::Basic::Title Should Contain
	[Arguments]	${EXPECTED}
	[Documentation]	Check if page title has the expected string
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	jquery=div.widget-title > h4
	Element Should Contain	jquery=div.widget-title > h4	${EXPECTED}

GUI::Basic::Page Title Should Be
	[Documentation]	Check if the title that is below the menus is the equal the expected
	[Arguments]	${EXPECTED_TITLE}
	${WIDGET_TILE}=	GUI::Basic::Get Page Title
	Should Be Equal	${WIDGET_TILE}	${EXPECTED_TITLE}

GUI::Basic::Spinner Should Be Invisible
	[Documentation]	Verify if spinner is invisible
	${EXISTS}=	Run Keyword And Return Status	GUI::Element Exists	//*[@id='loading']
	Run Keyword If	${EXISTS}	Wait Until Element Is Not Visible	jquery=#loading	timeout=${GUI_DEFAULT_TIMEOUT}
	${EXISTS}=	Run Keyword And Return Status	GUI::Element Exists	//*[@id='back_ground']
	Run Keyword If	${EXISTS}	Wait Until Element Is Not Visible	jquery=#back_ground	timeout=${GUI_DEFAULT_TIMEOUT}

GUI::Basic::Wait Until Elements Are Visible
	[Arguments]	@{ELEMENTS}
	[Documentation]	Waits until all elements are visible
	GUI::Basic::Spinner Should Be Invisible
	FOR		${ELEMENT}	IN	@{ELEMENTS}
		Wait Until Element Is Visible	${ELEMENT}
	END

GUI::Basic::Wait Until Elements Are Not Visible
	[Arguments]	@{ELEMENTS}
	[Documentation]	Waits until all elements are not visible
	GUI::Basic::Spinner Should Be Invisible
	FOR		${ELEMENT}	IN	@{ELEMENTS}
		Wait Until Element Is Not Visible	${ELEMENT}
	END

GUI::Basic::Elements Should Be Visible
	[Arguments]	@{ELEMENTS}
	[Documentation]	Verify if elements are visible
	FOR		${ELEMENT}	IN	@{ELEMENTS}
		Element Should Be Visible	${ELEMENT}
	END

GUI::Basic::Elements Should Not Be Visible
	[Arguments]	@{ELEMENTS}
	[Documentation]	Verify if elements are not visible
	FOR		${ELEMENT}	IN	@{ELEMENTS}
		Element Should Not Be Visible	${ELEMENT}
	END

GUI::Basic::Guarantees The Field Is Optically Visible
	[Arguments]	${LOCATOR}
	[Documentation]	Await field become accessible and scrolls until its optically visible
	...	== ARGUMENTS ==
	...	LOCATOR = field path using XPATH format
	...	== EXPECTED RESULT ==
	...	Await until the element is visible
	GUI::Basic::Wait Until Element Is Accessible	${LOCATOR}\
	Execute Javascript	window.document.evaluate("${LOCATOR}", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);

GUI::Basic::Wait Until Element Is Accessible
	[Arguments]	${ELEMENT}	${TIME}=15s
	[Documentation]	Verify if spinner is invisible and Element is visible and enabled
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	${ELEMENT}	timeout=${TIME}
	Wait Until Element Is Enabled	${ELEMENT}	timeout=${TIME}

GUI::Basic::Wait Until Elements Are Accessible
	[Arguments]	@{ELEMENTS}
	[Documentation]	Waits until all elements are accessible
	GUI::Basic::Spinner Should Be Invisible
	FOR		${ELEMENT}	IN	@{ELEMENTS}
		Wait Until Element Is Visible	${ELEMENT}
		Wait Until Element Is Enabled	${ELEMENT}
	END

GUI::Element Exists
	[Arguments]	${xpath}
	[Documentation]	Returns true or false whether the element exists or not
	${COUNT}=	Get Element Count	${xpath}
	${EXISTS}=	Evaluate	${COUNT} > 0
	[Return]	${EXISTS}

GUI::Basic::Click Link
	[Arguments]	${LOCATOR}
	[Documentation]	Await field become accessible, scrolls until its optically visible and then click
	GUI::Basic::Guarantees The Field Is Optically Visible	${LOCATOR}
	Click Link	${LOCATOR}
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Click Element
	[Documentation]	Await field become accessible, scrolls until its optically visible and then click
	[Arguments]	${LOCATOR}	${WAIT_SPINNER}=${TRUE}
	GUI::Basic::Guarantees The Field Is Optically visible	${LOCATOR}
	Click Element	${LOCATOR}
	Run Keyword If	${WAIT_SPINNER}	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Click Element Accept Alert
	[Documentation]	Await field become accessible, scrolls until its optically visible and then click
	[Arguments]	${LOCATOR}	${WAIT_SPINNER}=${TRUE}
	GUI::Basic::Guarantees The Field Is Optically visible	${LOCATOR}
	Click Element	${LOCATOR}
	Run Keyword And Return Status	Handle Alert	timeout=10s
	Run Keyword If	${WAIT_SPINNER}	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Button::${BUTTON}
	[Documentation]	Click in the button and wait for the Spinner figure to disappear
	${MYBUTTON}=	Convert To Lowercase	${BUTTON}
	GUI::Basic::Click Element	//*[@id='${MYBUTTON}Button']
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Input Text
	[Arguments]	${LOCATOR}	${TEXT}
	[Documentation]	Await field become accessible, scrolls until its optically visible and the input the text
	GUI::Basic::Guarantees The Field Is Optically visible	${LOCATOR}
	Input Text	${LOCATOR}	${TEXT}

GUI::Basic::Select Checkbox
	[Arguments]	${LOCATOR}
	[Documentation]	Await field become accessible, scrolls until its optically visible and select the checkbox
	GUI::Basic::Guarantees The Field Is Optically visible	${LOCATOR}
	Select Checkbox	${LOCATOR}

GUI::Basic::Unselect Checkbox
	[Arguments]	${LOCATOR}
	[Documentation]	Await field become accessible, scrolls until its optically visible and unselect the checkbox
	GUI::Basic::Guarantees The Field Is Optically visible	${LOCATOR}
	Unselect Checkbox	${LOCATOR}

GUI::Basic::Select Radio Button
	[Arguments]	${RADIO_BUTTON_ID}	${VALUE}
	[Documentation]	Await field become accessible, scrolls until its optically visible and select the radio item
	GUI::Basic::Guarantees The Field Is Optically visible	//*[@id='${RADIO_BUTTON_ID}']
	Select Radio Button	${RADIO_BUTTON_ID}	${VALUE}

GUI::Basic::${BUTTON} Should Be Visible
	[Documentation]	Check if the button is in the current page
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#${BUTTON}

GUI::Basic::Listbuilder Should Be Visible
	[Documentation]	Check if there is some listbuild in the current page
	Element Should Be Visible	jquery=.listbuilder	There is no listbuilder

GUI::Basic::Select Listbuilder Values By Index
	[Arguments]	${LOCATOR}	${INDEXES}	${SCREENSHOT}=${FALSE}
	[Documentation]	Select listerbuilder values
	GUI::Basic::Listbuilder Should Be Visible
	FOR		${element}	IN	@{INDEXES}
		Select From List By Index	jquery=${LOCATOR} select.groupFrom	${element}
	END
	Click Element	jquery=${LOCATOR} > div > div > div:nth-child(2) > div > div:first-child button

GUI::Basic::Get Selected Listbuilder Items
	[Arguments]	${LOCATOR}	${SCREENSHOT}=${FALSE}
	[Documentation]	Get listerbuilder selected values
	GUI::Basic::Listbuilder Should Be Visible
	@{items}=	Get Selected List Values	jquery=${LOCATOR} select.groupTo
	[Return]	@{items}

GUI::Basic::Discovery Now Button Should Be Visible
	[Documentation]	Check if there is some discovery now button in the current page
	Wait Until Element Is Visible	jquery=#discovernow

GUI::Basic::Discovery Now
	[Documentation]	Click in the Discovery now button and wait for the Spinner figure to disappear
	GUI::Basic::Click Element	//*[@id='discovernow']
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Up Button Should Be Visible
	[Documentation]	Check if there is some up button in the current page
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#upButton

GUI::Basic::Down Button Should Be Visible
	[Documentation]	Check if there is some add button in the current page
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#downButton

GUI::Basic::Console Button Should Be Visible
	[Documentation]	Check if there is some add button in the current page
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#consoleButton

GUI::Basic::Save Button Should Be Visible
	[Documentation]	Check if there is some save button in the current page
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#saveButton

GUI::Basic::Cancel Button Should Be Visible
	[Documentation]	Check if there is some cancel button in the current page
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#cancelButton

GUI::Basic::Drilldown Table Row By Index
	[Arguments]	${LOCATOR}	${INDEX}
	[Documentation]	Fires a drilldown action in a table row by index
	Click Element	jquery=${LOCATOR} tr:nth-child(${INDEX}) a
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Save
	[Documentation]	Click in the Save button and wait for the Spinner figure to disappear
	[Arguments]	${HAS_ALERT}=${FALSE}	${WAIT_SPINNER}=${TRUE}
	GUI::Basic::Wait Until Element Is Accessible	jquery=#saveButton
	Run Keyword If	${HAS_ALERT}	GUI::Basic::Click Element Accept Alert	//*[@id='saveButton']	${WAIT_SPINNER}
	...     ELSE    GUI::Basic::Click Element	//*[@id='saveButton']	${WAIT_SPINNER}

#GUI::Basic::Console
#	[Documentation]	Click in the console button and wait for the Spinner figure to disappear
#	GUI::Basic::Click Element	//*[@id='consoleButton']
#	GUI::Basic::Spinner Should Be Invisible
#
#GUI::Basic::Cancel
#	[Documentation]	Click in the cancel button and wait for the Spinner figure to disappear
#	GUI::Basic::Click Element	//*[@id='cancelButton']
#	GUI::Basic::Spinner Should Be Invisible
#
GUI::Basic::Delete
	[Arguments]	${HAS_ALERT}=${FALSE}
	[Documentation]	Click in the delete button and wait for the Spinner figure to disappear
	Wait Until Element Is Enabled	id=delButton	error=Cannot delete because delete button is disable
	Click Element	//*[@id='delButton']
	Run Keyword If	${HAS_ALERT}	Handle Alert	ACCEPT
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Delete With Alert
	[Documentation]	Click in the delete button, dismiss the alert message and wait for the Spinner figure to disappear
	GUI::Basic::Delete	True

GUI::${PATH}::${SUBPATH}::Delete If Exists
	[Arguments]	${TABLE}	${DEVICES}	${SCREENSHOT}=FALSE
	[Documentation]	Delete devices if they exists in the devices table
	...	== ARGUMENTS ==
	...	-   DEVICES = Device to be deleted
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Delete all devices in the given list that exists in the devices table
	GUI::Basic::${PATH}::${SUB}::Open Tab
	GUI::Basic::Delete Rows In Table If Exists	jquery=${TABLE}	${DEVICES}

GUI:Basic::Exist Window With Title
	[Arguments]	${TITLE}
	${TITLES}=	Get Window Names
	FOR		${T}	IN	@{TITLES}
		Run Keyword If	${TITLE} == ${T}	Return From Keyword	${TRUE}
	END
	[Return]	${FALSE}

GUI::Basic::Page Should Not Contain Error
	[Arguments]	${SCREENSHOT}=${FALSE}
	[Documentation]	Current web page should not contain error
	GUI::Basic::Spinner Should Be Invisible
	${ret1}=	Run Keyword And Return Status	GUI::Basic::Error Count Should Be	0
	${ret2}=	Run Keyword And Return Status	GUI::Basic::Field Error Count Should Be	0
	Should Be True	${ret1}
	Should Be True	${ret2}

GUI::Basic::Error Count Should Be
	[Arguments]	${ERROR_COUNT}	${SCREENSHOT}=${FALSE}
	[Documentation]	Current web page should contain the given error count
	Page Should Contain Element	jquery=div#globalerr p.text-danger	limit=${ERROR_COUNT}	message=There are more global errors than expected

GUI::Basic::Field Error Count Should Be
	[Arguments]	${ERROR_COUNT}	${SCREENSHOT}=${FALSE}
	[Documentation]	Current web page should contain the given error count
	Page Should Contain Element	jquery=span#errormsg.text-danger	limit=${ERROR_COUNT}	message=There are more field errors than expected

GUI::Basic::Select All Multilist Values
	[Documentation]	Selects from a multilist widget
	Select All From List	jquery=.multilist > div:nth-child(2) > div:first-child() > select
	Click Element	jquery=.multilist > div:nth-child(2) > div:nth-child(3) > button:first-child()

GUI::Basic::Input Text::Validation
	[Arguments]	${LOCATOR}	${VALIDS}	${INVALIDS}
	[Documentation]	locator gets the id of the element to test. You must be on the proper page
	...	tests is the texts to be tested. All must be wrong except for the first input which will be set on the end
	FOR		${INVALID}	IN	@{INVALIDS}
		Run Keyword And Continue On Failure	GUI::Basic::Input Text::Error	${LOCATOR}	${TRUE}	${INVALID}
	END
	FOR		${VALID}	IN	@{VALIDS}
		Run Keyword And Continue On Failure	GUI::Basic::Input Text::Error	${LOCATOR}	${FALSE}	${VALID}
	END

GUI::Basic::Input Text::Error
	[Arguments]	${LOCATOR}	${EXPECTS}	${TEXT}
	[Documentation]	locator wants the id of the element
	...	expects wants a t/f of whether an error should be thrown
	...	text_ent wants the input
	Input Text	jquery=#${LOCATOR}	${TEXT}
	Click Element	saveButton
	Wait Until Element Is Not Visible	xpath=//*[@id='loading']	timeout=120s
	GUI::Basic::Spinner Should Be Invisible
#Commenting the below line for bug NG-7284
#	Element Should Be Visible	jquery=#refresh-icon
#	Element Should Be Visible	jquery=#globalerr
	${ERR_MSG}=	Execute Javascript	var ro=document.getElementById('${LOCATOR}');if(ro==null){return false;}var er=ro.parentNode.nextSibling;if(er==null){return false;}return true;
	Log	Has error: ${ERR_MSG}
	Run Keyword If	${EXPECTS}	Should Be True	${ERR_MSG}	msg=${TEXT} IN ${LOCATOR} does not throw an error
	...	ELSE	Should Not Be True	${ERR_MSG}	msg=${TEXT} IN ${LOCATOR} should not throw an error

GUI::Basic::GUI4.0::Get Model
	Wait Until Element Is Enabled	pwl
	Click Element	pwl
	Wait Until Element Is Visible	abt
	Click Element	abt
	Wait Until Page Contains	Serial Number
	${MODEL}=	Get Text	jquery=div.modal \#system
	Click Button	jquery=div.modal button
	Wait Until Element Is Not Visible	jquery=div.modal
	[Return]	${MODEL}

GUI::Basic::Check Menu
	[Tags]	GUI BASIC MENU
	[Documentation]	Check main menu options
	GUI::Basic::Wait Until Element Is Accessible	//*[@id="main_menu"]
	@{OPTIONS}	Create List	Access	Tracking	System	Network	Managed Devices	Cluster	Security	Auditing	Dashboard
	@{MENU}=	Get Webelements	xpath=//*//*[@id='main_menu']//a
	FOR		${ELEMENT}	IN	@{MENU}
		${TEXT}=	Get Text	${ELEMENT}
		Should Contain Any	${TEXT}	@{OPTIONS}
	END

GUI::Basic::Get Menu
	[Tags]	GUI BASIC MENU
	[Documentation]	Get main menu options
	${OPTIONS}=	Get WebElements	//*//*[@id='main_menu']//a
	${RETURN}=	Create List
	FOR		${OPTION}	IN	@{OPTIONS}
		${TEXT}=	Get Text	${OPTION}
		${TEXT_LIST}=	Split String	${TEXT}	${\n}
		${TEXT}=	Get From List	${TEXT_LIST}	-1
		${ID}=	Replace String	${TEXT}	${SPACE}	_
		${ID}=	Convert To Lowercase	${ID}
		Assign Id To Element	${OPTION}	${ID}
		Run Keyword If	'${ID}' != '${EMPTY}'	Append To List	${RETURN}	${ID}
	END

GUI::Basic::Get Sub Menu
	[Tags]	GUI BASIC MENU
	[Documentation]	Get Sub Menu options
	${OPTIONS}=	Get WebElements	//*[@class="main_menu"]//div[contains(@class, 'submenu')]//li
	${RETURN}=	Create List
	FOR		${OPTION}	IN	@{OPTIONS}
		${TEXT}=	Get Text	${OPTION}
		${TEXT_LIST}=	Split String	${TEXT}	${\n}
		${TEXT}=	Get From List	${TEXT_LIST}	-1
		${ID}=	Replace String	${TEXT}	${SPACE}	_
		${ID}=	Convert To Lowercase	${ID}
		Assign Id To Element	${OPTION}	${ID}
		Run Keyword If	'${ID}' != '${EMPTY}'	Append To List	${RETURN}	${ID}
	END

GUI::Basic::Open ${PATH} Tab
	[Tags]	GUI BASIC OPEN TAB
	[Documentation]	Open tab for the given path of main menu
	${PATH}=	Convert to Lowercase	${PATH}
	${PATH}=	Replace String	${PATH}	${SPACE}	_
	GUI::Basic::Get Menu
	Click Element	${PATH}
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::${PATH}::${SUB}::Open Tab
	[Tags]	GUI BASIC OPEN TAB
	[Documentation]	Open tab for the given path on sub menu
	${PATH}=	Convert to Lowercase	${PATH}
	${PATH}=	Replace String	${PATH}	${SPACE}	_
	GUI::Basic::Get Menu
	Click Element	${PATH}
	GUI::Basic::Spinner Should Be Invisible

	${SUB}=	Convert to Lowercase	${SUB}
	${SUB}=	Replace String	${SUB}	${SPACE}	_
	GUI::Basic::Get Sub Menu
	Click Element	${SUB}
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Access::Table::Open NodeGrid console
	[Tags]	GUI BASIC OPEN CONSOLE
	[Arguments]	${PATH}=access	${SUB}=table
	[Documentation]	Open NG console under Access>>Table
	${PATH}=	Convert to Lowercase	${PATH}
	${PATH}=	Replace String	${PATH}	${SPACE}	_
	GUI::Basic::Get Menu
	Click Element	${PATH}
	GUI::Basic::Spinner Should Be Invisible

	${SUB}=	Convert to Lowercase	${SUB}
	${SUB}=	Replace String	${SUB}	${SPACE}	_
	GUI::Basic::Get Sub Menu
	Click Element	${SUB}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="|nodegrid"]/div[1]/div[2]
	Switch Window	nodegrid

GUI::Basic::Access::Table::Open NodeGrid info
	[Tags]	GUI BASIC INFO
	[Arguments]	${PATH}=access	${SUB}=table
	[Documentation]	Open NG info under Access::Table
	${PATH}=	Convert to Lowercase	${PATH}
	${PATH}=	Replace String	${PATH}	${SPACE}	_
	GUI::Basic::Get Menu
	Click Element	${PATH}
	GUI::Basic::Spinner Should Be Invisible

	${SUB}=	Convert to Lowercase	${SUB}
	${SUB}=	Replace String	${SUB}	${SPACE}	_
	GUI::Basic::Get Sub Menu
	Click Element	${SUB}
	GUI::Basic::Spinner Should Be Invisible

	Click Element	//*[@id="|nodegrid"]/div[1]/div[3]/a
	Page Should Contain Element	//*[@id="modal"]/div/div

GUI::Basic::Access::Tree::Open NodeGrid console
	[Tags]	GUI BASIC OPEN CONSOLE
	[Arguments]	${PATH}=access	${SUB}=tree
	[Documentation]	Open NG info under Access::Tree_
	GUI::Basic::Get Menu
	Click Element	${PATH}
	GUI::Basic::Spinner Should Be Invisible

	GUI::Basic::Get Sub Menu
	Click Element	${SUB}
	GUI::Basic::Spinner Should Be Invisible

	Click Element	//*[@id="003b29f6-60bf-4182-96de-59b657f21971|nodegrid"]/a
	Switch Window	nodegrid

GUI::Basic::${PATH}::${SUB}::Search::${SEARCH_TEXT}
	[Tags]	GUI BASIC SEARCH
	[Documentation]	Search under ${PATH}::${SUB}
	GUI::Basic::${PATH}::${SUB}::Open Tab

	Page Should Contain Element	search_expr1
	Input Text	search_expr1	${SEARCH_TEXT}
	Press Keys	search_expr1	ENTER
	GUI::Basic::Spinner Should Be Invisible

GUI::Basic::Add License
	[Tags]	GUI BASIC LICENSE
	[Arguments]	${LICENSE}	${PATH}=system	${SUB}=license
	[Documentation]	Search under ${PATH}::${SUB}

	${PATH}=	Convert to Lowercase	${PATH}
	${PATH}=	Replace String	${PATH}	${SPACE}	_
	GUI::Basic::Get Menu
	Click Element	${PATH}
	GUI::Basic::Spinner Should Be Invisible

	${SUB}=	Convert to Lowercase	${SUB}
	${SUB}=	Replace String	${SUB}	${SPACE}	_
	GUI::Basic::Get Sub Menu
	Click Element	${SUB}
	GUI::Basic::Spinner Should Be Invisible

	Page Should Contain Element	addButton
	Click Element	addButton
	Page Should Contain Element	licensevalue
	Input Text	licensevalue	${LICENSE}
	Click Element	saveButton
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain Element	license_table_wrapper

GUI::Basic::Delete License
	[Tags]	GUI BASIC	DELETE	LICENSE
	[Arguments]	${LICENSE}	${PATH}=system	${SUB}=license
	[Documentation]	Search under ${PATH}::${SUB}

	${PATH}=	Convert to Lowercase	${PATH}
	${PATH}=	Replace String	${PATH}	${SPACE}	_
	GUI::Basic::Get Menu
	Click Element	${PATH}
	GUI::Basic::Spinner Should Be Invisible

	${SUB}=	Convert to Lowercase	${SUB}
	${SUB}=	Replace String	${SUB}	${SPACE}	_
	GUI::Basic::Get Sub Menu
	Click Element	${SUB}
	GUI::Basic::Spinner Should Be Invisible

	Page Should Contain Element	license_table_wrapper
	${KEY}=	Fetch From Right	${LICENSE}	-
	${LICENSEKEY}=	Catenate	SEPARATOR=	xxxxx-xxxxx-xxxxx-	${KEY}
	Log	${LICENSEKEY}
	GUI::Basic::Select Rows In Table Containing Value	license_table	${LICENSEKEY}
	GUI::Basic::Delete
	Log	Exited
	Page Should Not Contain	${LICENSEKEY}

GUI::Basic::Add Device
	[Tags]	GUI BASIC	ADD DEVICE
	[Arguments]	${DEVICE}	${DEVICE_TYPE}	${DEVICE_IP}	${USERNAME}	${PASSWORD}
	[Documentation]	Add device

	GUI::Basic::${PATH}::${SUB}::Open Tab

	Click Element	addButton
	Input Text	spm_name	${DEVICE}

	${DEVICE_TYPE}=	Convert to Lowercase	${DEVICE_TYPE}
	${DEVICE_TYPE}=	Replace String	${DEVICE_TYPE}	${SPACE}	_
	Select From List By Value	type	${DEVICE_TYPE}

	Input Text	phys_addr	${DEVICE_IP}
	Input Text	username	${USERNAME}
	Input Text	passwordfirst	${PASSWORD}
	Input Text	passwordconf	${PASSWORD}
	Click Button	saveButton

	GUI::Basic::Spinner Should Be Invisible
	Table Should Contain	SPMTable	${DEVICE}

GUI::Basic::Delete Device
	[Tags]	GUI BASIC	ADD DEVICE
	[Arguments]	${DEVICE}
	[Documentation]	Delete device

	GUI::Basic::${PATH}::${SUB}::Open Tab

	Page Should Contain Element	SPMTable
	GUI::Basic::Select Rows In Table Containing Value	SPMTable	${DEVICE}
	Select Checkbox	//*[@id="${DEVICE}"]/td[1]/input
	GUI::Basic::Delete With Alert
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Table Should Not Contain	SPMTable	${DEVICE}

GUI::Basic::Add Local Accounts
	[Tags]	GUI BASIC	ADD LOCAL ACCOUNTS
	[Arguments]	${NAME}	${PASSWORD}	${PATH}=Security	${SUB}=Local Accounts
	[Documentation]	Add local account

	GUI::Basic::${PATH}::${SUB}::Open Tab

	GUI::Basic::Add
	Input Text	uName	${NAME}
	Input Text	uPasswd	${PASSWORD}
	Input Text	cPasswd	${PASSWORD}
	GUI::Basic::Save

	GUI::Basic::Spinner Should Be Invisible
	Table Should Contain	user_namesTable_wrapper	${NAME}

GUI::Basic::Delete Local Accounts
	[Tags]	GUI BASIC	DELETE LOCAL ACCOUNTS
	[Arguments]	${NAME}	${PATH}=Security	${SUB}=Local Accounts
	[Documentation]	Delete local account

	GUI::Basic::${PATH}::${SUB}::Open Tab

	Page Should Contain Element	user_namesTable_wrapper
	GUI::Basic::Select Rows In Table Containing Value	user_namesTable_wrapper	${NAME}
	Select Checkbox	//*[@id="${NAME}"]/td[1]/input
	GUI::Basic::Delete With Alert
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Table Should Not Contain	user_namesTable_wrapper	${NAME}
