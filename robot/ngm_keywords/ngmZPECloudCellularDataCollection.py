from requests import request
from datetime import datetime

date_format = "%Y-%m-%dT%H:%M:%S%Z"

def get_data():
	now = datetime.utcnow()
	current_dt = now.strftime(date_format)
	initial_dt = now.replace(month=now.month - 1 if now.month > 1 else 12).strftime(date_format)

	url = "https://api.qa-zpecloud.com/user/auth"
	payload = "{\"email\":\"${EMAIL_ADDRESS}\",\"password\": \"${PASSWORD}\"}"
	headers = {
		'Accept': 'application/json, text/plain, */*',
		'Authorization': '',
		'Content-Type': 'application/json'
	}

	login_response = request("POST", url, headers=headers, data=payload)
	try:
		json_response = login_response.json()
		token = json_response.get('token')
	except ValueError:
		print("Failed to parse json")
		return ""

	# url = "https://api.qa-zpecloud.com/device/12/statistics?since={since}Z&to={to}Z&downsample_key=accumBytes&downsample_to=500".format(since=initial_dt, to=current_dt)
	url = "https://api.qa-zpecloud.com/device/statistics/sim?month={month}&year={year}&device=895&timezone_offset=180".format(month=now.month,year=now.year)
	payload = {}
	headers = {
		'Accept': 'application/json',
		'Authorization': 'Bearer {token}'.format(token=token),
	}
	data = None
	try:
		cellular_data_response = request("GET", url, headers=headers, data=payload)
		cellular_data = cellular_data_response.json()
		data = cellular_data["modems"][0]["sim1"]["data"]
		data = (data[len(data)-1]["accumBytes"])
	except (ValueError, IndexError, AttributeError) as error:
		print("Failed to parse json", error)
		return "0"
	return data