*** Settings ***
Documentation
...    Keywords related to Security tabs to be used on GUI.
Resource	../ngm_tests/init.robot

*** Keywords ***

GUI::Security::Open Security
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]
	...	Opens the NodeGrid security tab
	...     == ARGUMENTS ==
	...     -	SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Managed devices Tab is open and all elements are accessable
	GUI::Basic::Wait Until Element Is Accessible	jquery=#main_menu > li:nth-child(7) > a
	Execute Javascript	window.document.evaluate("//*[@id='main_menu']", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	GUI::Basic::Spinner Should Be Invisible

	Run Keyword If 	'${NGVERSION}' >= '5.8'    Click Element  xpath=//*[@id="menu_main_security"]
	Run Keyword If 	'${NGVERSION}' < '5.8'     Click Element	jquery=#main_menu > li:nth-child(7) > a
	
	GUI::Basic::Spinner Should Be Invisible

GUI::Security::Open Firewall tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]
	...	Opens the NodeGrid Security::Firewall tab
	...     == ARGUMENTS ==
	...     -	SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Firewall Tab is open and all elements are accessible
	[Tags]      GUI     SYSTEM      LICENSE
	GUI::Security::Open Security
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(5) > a
	GUI::Security::Wait For Firewall Table

GUI::Security::Wait For Firewall Table
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Page Contains Element		//*[@id="tbody"]
	Wait Until Element Is Visible	jquery=#addButton
	Wait Until Element Is Visible	jquery=#policyButton
	Wait Until Element Is Visible	jquery=#thead > tr > th:nth-child(1) > input[type="checkbox"]
	${FIRST_COL_NAME}=	Get Text	jquery=#thead > tr > th:nth-child(2)
	Should Be Equal	${FIRST_COL_NAME}	Chain

GUI::Security::Add Firewall Chain
	[Arguments]	${NAME}	${TYPE}
	Wait Until Element Is Visible	jquery=#addButton
	Click Element	jquery=#addButton
	Wait Until Element Is Visible	jquery=#saveButton
	Input Text	jquery=#chain	${NAME}
	Select Radio Button	type	${TYPE}
	Click Element	jquery=#saveButton
	GUI::Security::Wait For Firewall Table
	Table Should Contain	jquery=#filterChains	${NAME}

GUI::Security::Open Local Accounts tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]
	...	Opens the NodeGrid Security::Local Accounts tab
	...     == ARGUMENTS ==
	...     -	SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Local Accounts Tab is open and all elements are accessible
	[Tags]      GUI     SYSTEM      LICENSE
	GUI::Security::Open Security
	Run Keyword If	'${NGVERSION}' <= '5.6'		Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(1) > a
	...	ELSE	Click Element		xpath=(//a[contains(text(),'Local Accounts')])[2]
	GUI::Basic::Spinner Should Be Invisible

GUI::Security::Local Accounts::Add
	[Arguments]	${NAME}	${SCREENSHOT}=FALSE
	[Documentation]
	...	Opens the NodeGrid Security::Local Accounts tab and add a new user
	...     == ARGUMENTS ==
	...     -   NAME = The username of the new to be created user (the username will be the password too)
	...     -	SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     A new users will appear in the users table
	[Tags]      GUI     SECURITY	LOCAL ACCOUNTS
	GUI::Security::Open Local Accounts tab
	GUI::Security::Local Accounts::Delete	${NAME}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=#addButton
	Wait Until Element Is Visible	jquery=#uName
	Input Text	jquery=#uName	${NAME}
	Wait Until Element Is Visible	jquery=#uPasswd
	Input Text	jquery=#uPasswd	${NAME}
	Wait Until Element Is Visible	jquery=#cPasswd
	Input Text	jquery=#cPasswd	${NAME}
	GUI::Basic::Save

GUI::Security::Local Accounts::Add on Admin Group
	[Arguments]	${NAME}	${PASSWORD}	${SCREENSHOT}=FALSE
	[Documentation]     Opens the NodeGrid Security::Local Accounts tab and add a new user
	...     == ARGUMENTS ==
	...     -   NAME = The username of the new to be created user (the username will be the password too)
	...     -   SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     A new users will appear in the users table
	[Tags]      GUI     SECURITY	LOCAL ACCOUNTS
	GUI::Security::Open Local Accounts tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Security::Local Accounts::Delete	${NAME}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	jquery=#addButton
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#uName
	Input Text	jquery=#uName	${NAME}
	Wait Until Element Is Visible	jquery=#uPasswd
	Input Text	jquery=#uPasswd	${PASSWORD}
	Wait Until Element Is Visible	jquery=#cPasswd
	Input Text	jquery=#cPasswd	${PASSWORD}
	Click Element	//*[@id="uGroup"]/div/div[1]/div[1]/select/option
	Click Element	//*[@id="uGroup"]/div/div[1]/div[2]/div/div[1]/button
	Element Should Contain	//*[@id="uGroup"]/div/div[1]/div[3]/select	admin
	GUI::Basic::Save
	GUI::Basic::Spinner Should Be Invisible

GUI::Security::Local Accounts::Wait For Local Accounts Table
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#addButton
	Wait Until Element Is Visible	jquery=#thead > tr > th:nth-child(1) > input[type="checkbox"]

GUI::Security::Local Accounts::Delete
	[Arguments]	${USERNAME}	${SCREENSHOT}=FALSE
	[Documentation]
	...	Opens the NodeGrid Security::Local Accounts tab and delete an user
	...     == ARGUMENTS ==
	...     -   NAME = The username of the new to be deleted user (the username will be the password too)
	...     -	SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     An user will be deleted/disappears in the users table
	[Tags]      GUI     SECURITY	LOCAL ACCOUNTS
	${USERNAMES}=	Create List	${USERNAME}
	GUI::Security::Open Local Accounts tab
	GUI::Basic::Delete Rows In Table If Exists	jquery=table\#user_namesTable	${USERNAMES}

GUI::Security::Open Password Rules tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]
	...	Opens the NodeGrid Security::Password Rules tab
	...     == ARGUMENTS ==
	...     -	SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Local Accounts Tab is open and all elements are accessible
	[Tags]      GUI     SYSTEM      LICENSE
	GUI::Security::Open Security
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(2) > a
	GUI::Basic::Spinner Should Be Invisible

GUI::Security::Open Authorization tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]
	...	Opens the NodeGrid Security::Authorization page
	...     == ARGUMENTS ==
	...     -	SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Authorization page is open and all elements are accessible
	[Tags]	GUI	SECURITY	AUTHORIZATION
	GUI::Security::Open Security
	Run Keyword If 	'${NGVERSION}' >= '5.8'  Click Element		xpath=/html/body/div[7]/div[2]/ul/li[3]/a
	Run Keyword If 	'${NGVERSION}' < '5.8'  Click Element			jquery=body > div.main_menu > div > ul > li:nth-child(3) > a
	GUI::Security::Wait For Authorization Table

GUI::Security::Wait For Authorization Table
	[Documentation]  Wait for the authorization page under security menu be ready for use
	GUI::Basic::Spinner Should Be Invisible
	
	Run Keyword If 	'${NGVERSION}' < '5.8'  Wait Until Element Contains	jquery=body > div.main_menu > div > ul > li.active > a	Authorization
	
	Wait Until Element Is Visible	jquery=#addButton
	Wait Until Element Is Visible	jquery=#delButton
	Table Header Should Contain	jquery=#author_groupsTable	Group

GUI::Security::Check Default User Groups
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]
	...	Checks if there are the default user groups
	...     == ARGUMENTS ==
	...     -	SCREENSHOT = Should a Screenshot be taken Default is false
	[Tags]      GUI     SECURITY      AUTHORIZATION
	GUI::Security::Wait For Authorization Table

GUI::Security::Authorization::Add User Group
	[Arguments]     ${GROUP}
	[Documentation]
	...	Create user group
    GUI::Security::Open Authorization tab
    GUI::Basic::Add
    Input Text     uGroup   ${GROUP}
    GUI::Basic::Save
    GUI::Basic::Spinner Should Be Invisible
    Table Should Contain    author_groupsTable  ${GROUP}

GUI::Security::Authorization::Delete User Group If Exists
	[Arguments]     ${GROUP}
	[Documentation]
	...	Delete user group
	${GROUPS}=   Create List     ${GROUP}
    GUI::Security::Open Authorization tab
    ${LOCATOR}=     Set Variable    author_groupsTable
	${COUNT_SELECTIONS}=	GUI::Basic::Count Table Rows With Id	${LOCATOR}	${GROUP}
    Run Keyword If	${COUNT_SELECTIONS} > 0    GUI::Basic::Select Table Row By Id	${LOCATOR}	${GROUP}
    Run Keyword If	${COUNT_SELECTIONS} > 0    GUI::Basic::Delete With Alert

GUI::Security::Authorization::Add User To User Group
	[Arguments]     ${USER}     ${GROUP}
	[Documentation]
	...	Add a member (user) to an authorization group
	GUI::Security::Authorization::Delete User From User Group If Exists  ${USER}     ${GROUP}
    GUI::Security::Open Authorization tab
    Click Element   //tr[@id='${GROUP}']//a
    GUI::Basic::Spinner Should Be Invisible
    GUI::Basic::Add
    Double Click Element   //select[@class='selectbox groupFrom']//option[@value="${USER}"]
    GUI::Basic::Save

GUI::Security::Authorization::Delete User From User Group If Exists
	[Arguments]     ${USER}     ${GROUP}
	[Documentation]
	...	Remove a member (user) from an authorization group
    GUI::Security::Open Authorization tab
    Click Element   //tr[@id='${GROUP}']//a
    GUI::Basic::Spinner Should Be Invisible
    ${LOCATOR}=     Set Variable    agMember_nav
	${COUNT_SELECTIONS}=	GUI::Basic::Count Table Rows With Id	${LOCATOR}	${USER}
    Run Keyword If	${COUNT_SELECTIONS} > 0    GUI::Basic::Select Table Row By Id	${LOCATOR}	${USER}
    Run Keyword If	${COUNT_SELECTIONS} > 0    GUI::Basic::Delete

GUI::Security::Authorization::Open User Group Profile Tab
	[Arguments]     ${GROUP}
	[Documentation]
	...	Open group profile tab
    GUI::Security::Open Authorization tab
    GUI::Basic::Click Element   //tr[@id='${GROUP}']//a
    GUI::Basic::Click Element   //a[@id='profile']


GUI::Security::Drilldown User Group
	[Arguments]	${GROUP}	${SCREENSHOT}=FALSE
	[Documentation]
	...	Open the page to edit the given user group
	GUI::Security::Open Authorization tab
	GUI::Basic::Drilldown table row	\#author_groupsTable	${GROUP}
	GUI::Basic::Add Button Should Be Visible
	GUI::Basic::Delete Button Should Be Visible
	GUI::Basic::Return Button Should Be Visible

GUI::Security::Open User Group Outlet Tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]
	...	Open the user group outlet page be
	Click Element	jquery=div#pod_menu > a:nth-child(5)
	GUI::Security::Wait For User Group Outlet Table

GUI::Security::Wait For User Group Outlet Table
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]
	...	Wait for the user group outlet page be ready for use
	GUI::Basic::Return Button Should Be Visible
	Wait Until Element Is Visible	jquery=#returnButton
	Table Header Should Contain	jquery=#agasOutlet_Table	Device
	Table Header Should Contain	jquery=#agasOutlet_Table	PDU ID
	Table Header Should Contain	jquery=#agasOutlet_Table	Outlet ID
	Table Header Should Contain	jquery=#agasOutlet_Table	Power Mode

GUI::Security::Wait For Authentication Table
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]
	...	Wait until the Authentication table is ready to be used
	GUI::Basic::Add Button Should Be Visible
	GUI::Basic::Delete Button Should Be Visible
	GUI::Basic::Up Button Should Be Visible
	GUI::Basic::Down Button Should Be Visible
	GUI::Basic::Console Button Should Be Visible
	Table Header Should Contain	jquery=#multiAuthTable	Index
	Table Header Should Contain	jquery=#multiAuthTable	Method
	Table Header Should Contain	jquery=#multiAuthTable	Remote Server
	Table Header Should Contain	jquery=#multiAuthTable	Status
	Table Header Should Contain	jquery=#multiAuthTable	Fallback


GUI::Security::Open Authentication Tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]
	...	Open the Authentication page under Security menu
	GUI::Security::Open Security
	Run Keyword If	'${NGVERSION}' <= '5.6'		Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(4) > a
	...	ELSE	Click Element		xpath=(//a[contains(text(),'Authentication')])[2]
	GUI::Security::Wait For Authentication Table
	GUI::Basic::Spinner Should Be Invisible

GUI::Security::Authentication Add TACACS+ Server
	[Arguments]	${IP}	${secret}=secret
	[Documentation]
	...	Opens the NodeGrid Security::Authorization page and add a TACACS+ server
	GUI::Security::Open Authentication Tab
	GUI::Basic::Add
	GUI::Basic::Spinner Should Be Invisible
	Select From List By Value	mauthMethod	tacplus
	Select Checkbox	mauthFallback
	Input Text	mauthServer	${IP}
	Input Text	tacAccServer	${IP}
	Input Text	tacsecret	${secret}
	Input Text	tacsecret2	${secret}
	Select Checkbox	mauthFallback
	GUI::Basic::Save

GUI::Security::Authentication Delete TACACS+ Server If Exists
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]
	...	Delete tacacs+ server if they exists
	GUI::Security::Open Authentication Tab
	${COUNT}=	Get Element Count	xpath=//table//tbody//tr
	FOR	${INDEX}	IN RANGE	1	99999
		Exit For Loop If	'${COUNT}'=='${INDEX}'
		${text}=	Get Text	xpath=//tbody//tr[@id="${INDEX}"]/td[3]
		Run Keyword If	'${text}'=='TACACS+'	Select Checkbox	//tr[@id="${INDEX}"]/td[1]/input
		Run Keyword If	'${text}'=='TACACS+'	GUI::Basic::Delete
	END

GUI::Security::Authentication Delete Servers If Exists
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]
	...	Delete servers if they exists (letting only local server)
	GUI::Security::Open Authentication Tab
	${COUNT}=	Get Element Count	xpath=//table//tbody//tr
	FOR	${INDEX}	IN RANGE	1	99999
		Exit For Loop If	'${COUNT}'=='${INDEX}'
		${text}=	Get Text	xpath=//tbody//tr[@id="${INDEX}"]/td[3]
		Run Keyword If	'${text}'!='Local'	Continue For Loop
		Select Checkbox	//tr[@id="${INDEX}"]/td[1]/input
		GUI::Basic::Delete
	END

GUI::Security::Open Services tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]
	...	Opens the NodeGrid Security::Authorization page
	...     == ARGUMENTS ==
	...     -	SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     Services page is open and all elements are accessible
	[Tags]	GUI	SECURITY
	GUI::Security::Open Security
	Click Element	(//a[text()='Services'])[2]
	Wait Until Element Is Visible	jquery=#webService
	Wait Until Element Is Visible	jquery=#services
	Wait Until Element Is Visible	jquery=#sshServer
	Wait Until Element Is Visible	jquery=#crypto
	Wait Until Element Is Visible	jquery=#managedDevices
#	Wait Until Element Is Visible	jquery=#fail2ban

GUI::Security::Open NAT tab
	[Arguments]     ${SCREENSHOT}=FALSE
	[Documentation]
	...	Opens the NodeGrid Security::NAT page
	...     == ARGUMENTS ==
	...     -	SCREENSHOT = Should a Screenshot be taken Default is false
	...     == EXPECTED RESULT ==
	...     NAT page is open and all elements are accessible
	[Tags]	GUI	    NAT
	GUI::Security::Open Security
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(6) > a
	Wait Until Element Is Visible	jquery=#nat_natChains

GUI::Security::Authentication::Open Sso Tab
	GUI::Security::Open Authentication Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element		xpath=//span[contains(.,'SSO')]
	GUI::Basic::Spinner Should Be Invisible

GUI::Security::Authorization::User::Open User tab
	GUI::Basic::Security::Authorization::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element	xpath=//a[contains(text(),'user')]

GUI::Security::Authorization::User::Open Members tab
	GUI::Basic::Security::Authorization::Open Tab
    GUI::Basic::Spinner Should Be Invisible
    GUI::Security::Authorization::User::Open User tab
    GUI::Basic::Spinner Should Be Invisible
    Click Element		xpath=//span[contains(.,'Members')]

GUI::Security::Authorization::User::Open Profile tab
	GUI::Basic::Security::Authorization::Open Tab
  	GUI::Basic::Spinner Should Be Invisible
  	GUI::Security::Authorization::User::Open User tab
  	GUI::Basic::Spinner Should Be Invisible
  	Click Element		xpath=//span[contains(.,'Profile')]

GUI::Security::Authorization::User::Open Remote Groups tab
	GUI::Basic::Security::Authorization::Open Tab
  	GUI::Basic::Spinner Should Be Invisible
  	GUI::Security::Authorization::User::Open User tab
  	GUI::Basic::Spinner Should Be Invisible
  	Click Element		xpath=//span[contains(.,'Remote Groups')]

GUI::Security::Authorization::User::Open Devices tab
	GUI::Basic::Security::Authorization::Open Tab
 	GUI::Basic::Spinner Should Be Invisible
  	GUI::Security::Authorization::User::Open User tab
  	GUI::Basic::Spinner Should Be Invisible
  	Click Element		xpath=//span[contains(.,'Devices')]

GUI::Security::Authorization::User::Open Outlets tab
	GUI::Basic::Security::Authorization::Open Tab
 	GUI::Basic::Spinner Should Be Invisible
 	GUI::Security::Authorization::User::Open User tab
 	GUI::Basic::Spinner Should Be Invisible
 	Click Element		xpath=//span[contains(.,'Outlets')]

GUI::Security::Authorization::admin::Open Device Tab
	GUI::Basic::Security::Authorization::Open Tab
 	GUI::Basic::Spinner Should Be Invisible
	Click Element   xpath=(//a[contains(text(),'admin')])[2]
	GUI::Basic::Spinner Should Be Invisible
	Click Element		xpath=//a[4]/span

GUI::Security::Authorization::admin::Open Outlets tab
	GUI::Basic::Security::Authorization::Open Tab
 	GUI::Basic::Spinner Should Be Invisible
 	Click Element   xpath=(//a[contains(text(),'admin')])[2]
 	GUI::Basic::Spinner Should Be Invisible
 	Click Element		xpath=//span[contains(.,'Outlets')]