*** Settings ***
Resource	rootSettings.robot

*** Keywords ***
GUI::Cluster::Open
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid cloud tab
	...	== ARGUMENTS ==
	...	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Cluster Tab is open and all elements are accessable
	GUI::Basic::Wait Until Element Is Accessible	jquery=#main_menu > li:nth-child(6) > a
	Execute Javascript	window.document.evaluate("//*[@id='main_menu']", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);

	Run Keyword If	'${NGVERSION}' <= '5.6'		Click Element   jquery=#main_menu > li:nth-child(6) > a
	...	ELSE	Click Element		//*[@id="menu_main_cluster"]

	GUI::Basic::Spinner Should Be Invisible

GUI::Cluster::Open Peers Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Cluster::Settings tab
	...	== ARGUMENTS ==
	...	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Settings Tab is open and all elements are accessible
	[Tags]	GUI	CLUSTER	LICENSE
	GUI::Cluster::Open

		Run Keyword If	'${NGVERSION}' <= '5.6'		Click Element   jquery=body > div.main_menu > div > ul > li:nth-child(1) > a
	...	ELSE	Click Element		xpath=/html/body/div[7]/div[2]/ul/li[1]/a

	GUI::Basic::Spinner Should Be Invisible

GUI::Cluster::Open Settings Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Cluster::Settings tab
	...	== ARGUMENTS ==
	...	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Settings Tab is open and all elements are accessible
	[Tags]	GUI	CLUSTER	LICENSE
	GUI::Cluster::Open
	Run Keyword If 	'${NGVERSION}' == '4.0'	Click Element	document.querySelector("body > div.main_menu > div > ul > li.active > a > span")
	...	ELSE IF	'${NGVERSION}' <= '5.6'	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(2) > a
	...	ELSE	click element		xpath=(//a[contains(text(),'Settings')])[4]
	GUI::Basic::Spinner Should Be Invisible

GUI::Cluster::Open Management Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Cluster::Settings tab
	...	== ARGUMENTS ==
	...	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Settings Tab is open and all elements are accessible
	[Tags]	GUI	CLUSTER	LICENSE
	GUI::Cluster::Open
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(3) > a
	GUI::Basic::Spinner Should Be Invisible

GUI::Cluster::Open Automatic Enrollment Range
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]	Opens the NodeGrid Manager Cluster::Settings::Automatic Enrollment Range
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	-	A.E.R. Tab is open and all elements are accessible
	[Tags]	GUI	CLUSTER	AUTOMATIC_ENROLLMENT_RANGE
	GUI::Basic::Cluster::Settings::open tab
	Click Element	jquery=#pod_menu > a:nth-child(2)
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=table#peerTable

GUI::Cluster::Wait For Peers
	GUI::Basic::Spinner Should Be Invisible
	Table Header Should Contain		id=peerTable	Name
	Table Header Should Contain		id=peerTable	Address
	Table Header Should Contain		id=peerTable	Type
	Table Header Should Contain		id=peerTable	Status
	Table Header Should Contain		id=peerTable	Peer Status

GUI::Cluster::Wait For Settings
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Contains	jquery=body > div.main_menu > div > ul > li.active > a	Settings
	Wait Until Element Is Visible	jquery=#saveButton
	Wait Until Element Is Visible	id=autoEnroll
	Wait Until Element Is Visible	id=enabled
	Wait Until Element Is Visible	id=management_enabled
	Wait Until Element Is Visible	id=lps_enabled

GUI::Cluster::Wait For Management
	GUI::Basic::Spinner Should Be Invisible
	Table Header Should Contain	    id=peerMngtTable	Name
	Table Header Should Contain	    id=peerMngtTable	Address
	Table Header Should Contain	    id=peerMngtTable    Status
	Table Header Should Contain	    id=peerMngtTable	SW Version
	Table Header Should Contain	    id=peerMngtTable	Management Status

GUI::Cluster::Add Coordinator
	[Arguments]	${DEVCOORD_NAME}	${DEVCOORD_IPADDR}	${SHAREDKEY}	${IS_STAR_MODE}	${DEVCON_TYPE}=device_console	${DEVCON_USERNAME}=${DEFAULT_USERNAME}	${DEVCON_ASKPASS}=no	${DEVCON_PASSWORD}=${DEFAULT_PASSWORD}	${DEVCON_STATUS}=enabled	${DEVCON_URL}=http://%IP
	[Documentation]	Add a device and set it's cluster configuration as coordinator
	GUI::ManagedDevices::Delete Device If Exists	${DEVCOORD_NAME}
	GUI::ManagedDevices::Add Device	 ${DEVCOORD_NAME}	 ${DEVCON_TYPE}	${DEVCOORD_IPADDR}	${DEVCON_USERNAME}	${DEVCON_ASKPASS}	${DEVCON_PASSWORD}	${DEVCON_STATUS}	${DEVCON_URL}
	GUI::Cluster::Open Settings Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	enabled
	Select Radio Button	nodeType	master
	Input Text	preSharedKey	${SHAREDKEY}
	Select Radio Button	starMode	${IS_STAR_MODE}
	Select Checkbox	cluster_enabled
	GUI::Basic::Wait Until Element Is Accessible 	lps_enabled
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	lps_enabled
	Select Checkbox	lps_enabled
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	lps_type
	Select Radio Button	lps_type	server
	Select Checkbox	management_enabled
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Save If Configuration Changed	SPINNER_TIMEOUT=60s

GUI::Cluster::Add Peer
	[Arguments]	${DEVPEER_NAME}	${DEVPEER_IPADDR}	${DEVCOOR_IPADDR}	${SHAREDKEY}
	...	${DEVCON_TYPE}=device_console	${DEVCON_USERNAME}=${DEFAULT_USERNAME}	${DEVCON_ASKPASS}=no	${DEVCON_PASSWORD}=${DEFAULT_PASSWORD}	${DEVCON_STATUS}=enabled	${DEVCON_URL}=http://%IP
	[Documentation]	Add a device and set it's cluster configuration as peer
	GUI::ManagedDevices::Delete Device If Exists	${DEVPEER_NAME}
	GUI::ManagedDevices::Add Device	 ${DEVPEER_NAME}	${DEVCON_TYPE}	${DEVPEER_IPADDR}	${DEVCON_USERNAME}	${DEVCON_ASKPASS}	${DEVCON_PASSWORD}	${DEVCON_STATUS}	${DEVCON_URL}
	GUI::Cluster::Open Settings Tab
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	autoEnroll
	Select Checkbox	autoEnroll
	Textfield Should Contain	autoPreSharedKey	nodegrid-key
	Select Checkbox	enabled
	Select Radio Button	nodeType	peer
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	masterAddr
	Input Text	masterAddr	${DEVCOOR_IPADDR}
	Input Text	preSharedKey2	${SHAREDKEY}
	Select Checkbox	cluster_enabled
	Select Checkbox	management_enabled
	GUI::Basic::Wait Until Element Is Accessible 	lps_enabled
	Select Checkbox	lps_enabled
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	lps_type
	GUI::Basic::Wait Until Element Is Accessible 	lps_type
	GUI::Basic::Select Radio Button	lps_type	client
	GUI::Basic::Save If Configuration Changed	SPINNER_TIMEOUT=90s

GUI::Cluster::Add License
	[Arguments]	${LICENSE}
	GUI::Basic::System::License::Open Tab
	GUI::Basic::Add
	GUI::Basic::Save Button Should Be Visible
	GUI::Basic::Cancel Button Should Be Visible
	Wait Until Element Is Visible	//*[@id="licensevalue"]
	Input Text	//*[@id="licensevalue"]	${LICENSE}
	GUI::Basic::Save If Configuration Changed	SPINNER_TIMEOUT=60s
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	90	3	Page Should Contain Element	license_table

GUI::Cluster::Delete Licenses
	GUI::Basic::System::License::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Delete All Rows In Table If Exists	\#license_table	False

GUI::Cluster::Remove Peer From Coordinator If Exist
	[Arguments]	${PEER_IP}=${HOSTPEER}
	GUI::Cluster::Open Peers Tab
	GUI::Basic::Spinner Should Be Invisible
	${STATUS}=	Run Keyword And Return Status	Page Should Contain	${PEER_IP}
	Run Keyword If	${STATUS}	GUI::Cluster::Remove Peer From Coordinator

GUI::Cluster::Remove Peer From Coordinator
	GUI::Cluster::Open Peers Tab
	GUI::Basic::Spinner Should Be Invisible
	Select Checkbox	(//*[@type="checkbox"])[3]
	GUI::Basic::Spinner Should Be Invisible
	Click Element	//*[@id="delButton"]
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Keyword Succeeds	30	3	GUI::Cluster::Should Not Contain Peer
	GUI::Basic::Spinner Should Be Invisible

GUI::Cluster::Should Not Contain Peer
	GUI::Cluster::Open Peers Tab
	Element Should Not Contain	peerTable	${HOSTPEER}
	Click Element	//*[@id="main_menu"]/li[6]

GUI::Cluster::Disable Cluster
    [Documentation]   Disable the Cluster option for the system
    GUI::Cluster::Open Settings Tab
    Unselect Checkbox   //*[@id="enabled"]
    Select Checkbox   lps_enabled
    Click Element    //label[normalize-space()='Client']//input[@id='lps_type']
    Unselect Checkbox   management_enabled
    GUI::Basic::Save If Configuration Changed	SPINNER_TIMEOUT=60s

GUI::Cluster::Save
	[Documentation]	Save configurations and changes takes more time under Cluster
	GUI::Basic::Wait Until Element Is Accessible	jquery=#saveButton
	Click Element	//*[@id='saveButton']
	GUI::Basic::Spinner Should Be Invisible	SPINNER_TIMEOUT=60s