*** Settings ***
Documentation
...    Tracking Keywords to be used on GUI.
Resource	../ngm_tests/init.robot

*** Keywords ***
GUI::Tracking::Open
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]
	...	Opens the NodeGrid Manager Tracking tab
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Tracking Tab is open and all elements are accessable

	GUI::Basic::Wait Until Element Is Accessible	jquery=#main_menu > li:nth-child(2) > a
	GUI::Basic::Spinner Should Be Invisible

	Run Keyword If 	'${NGVERSION}' >= '5.8'    Click Element  xpath=//*[@id="menu_main_tracking"]
	Run Keyword If 	'${NGVERSION}' < '5.8'     Click Element	xpath=//span[contains(.,'Tracking')]
	
	GUI::Basic::Spinner Should Be Invisible

GUI::Tracking::Open Open Sessions Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]
	...	Opens the NodeGrid Manager Tracking::Open Sessions
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Open Sessions Tab is open and all elements are accessable
	[Tags]	GUI
	GUI::Tracking::Open
	GUI::Basic::Wait Until Element Is Accessible	xpath=(//a[contains(text(),'Open Sessions')])[2]
	Click Element	xpath=(//a[contains(text(),'Open Sessions')])[2]
	GUI::Basic::Spinner Should Be Invisible


GUI::Tracking::Open Event List Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]
	...	Opens the NodeGrid Manager Tracking::Event List
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Event List Tab is open and all elements are accessable
	[Tags]	GUI
	GUI::Tracking::Open
	GUI::Basic::Wait Until Element Is Accessible	css=.submenu li:nth-child(2) > a
	Click Element	css=.submenu li:nth-child(2) > a
	GUI::Basic::Spinner Should Be Invisible


GUI::Tracking::Open System Usage Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]
	...	Opens the NodeGrid Manager Tracking::System Usage
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	System Usage Tab is open and all elements are accessable
	[Tags]	GUI
	GUI::Tracking::Open
	GUI::Basic::Wait Until Element Is Accessible	xpath=(//a[contains(text(),'System Usage')])[2]
	Click Element	xpath=(//a[contains(text(),'System Usage')])[2]
	GUI::Basic::Spinner Should Be Invisible


GUI::Tracking::Open Discovery Logs Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]
	...	Opens the NodeGrid Manager Tracking::Discovery Logs
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Discovery Logs Tab is open and all elements are accessable
	[Tags]	GUI
	GUI::Tracking::Open
	GUI::Basic::Wait Until Element Is Accessible	css=.submenu li:nth-child(4) > a
	Click Element	css=.submenu li:nth-child(4) > a
	GUI::Basic::Spinner Should Be Invisible


GUI::Tracking::Open Network Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]
	...	Opens the NodeGrid Manager Tracking::Network
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Network Tab is open and all elements are accessable
	[Tags]	GUI
	GUI::Tracking::Open
	GUI::Basic::Wait Until Element Is Accessible	xpath=(//a[contains(text(),'Network')])[2]
	Click Element		xpath=(//a[contains(text(),'Network')])[2]
	GUI::Basic::Spinner Should Be Invisible


GUI::Tracking::Open Devices Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]
	...	Opens the NodeGrid Manager Tracking::Devices
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Devices Tab is open and all elements are accessable
	[Tags]	GUI
	GUI::Tracking::Open
	GUI::Basic::Wait Until Element Is Accessible	jquery=body > div.main_menu > div > ul > li:nth-child(6)
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(6)
	GUI::Basic::Spinner Should Be Invisible


GUI::Tracking::Open Scheduler Tab
	[Arguments]	${SCREENSHOT}=FALSE
	[Documentation]
	...	Opens the NodeGrid Manager Tracking::Scheduler
	...	== ARGUMENTS ==
	...	-	SCREENSHOT = Should a Screenshot be taken Default is false
	...	== EXPECTED RESULT ==
	...	Scheduler Tab is open and all elements are accessable
	[Tags]	GUI
	GUI::Tracking::Open

	Run Keyword If 	'${NGVERSION}' >= '5.8'    Click Element  xpath=/html/body/div[7]/div[2]/ul/li[7]/a
	Run Keyword If 	'${NGVERSION}' < '5.8'     Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(7)

	GUI::Basic::Spinner Should Be Invisible


GUI::Tracking::Check Nodegrid Events
	[Arguments]	${CURRENT_DATE_BEFORE_LOGIN}	${TABLE_ROWS_ROW}=1	${EVENT_ID}=200	${TABLE_CELL_ROW}=2
	...	${TABLE_CELL_COLUMN}=1	${GENERATE_EVENTS_LOG_FILE}=No
	Log	\n++++++++++++++++++++++++++++ Check Nodegrid Events: ++++++++++++++++++++++++++++\n  INFO     console=yes
	Log	\nCURRENT_DATE_BEFORE_LOGIN: \n${CURRENT_DATE_BEFORE_LOGIN}	INFO	console=yes
	${CURRENT_DATE_AFTER_LOGIN}	Get Current Date	time_zone=UTC
	Log	\nCURRENT_DATE_AFTER_LOGIN: \n${CURRENT_DATE_AFTER_LOGIN}	INFO	console=yes
	GUI::Basic::Spinner Should Be Invisible
	GUI::Tracking::Open Event List Tab
	${RESULT}	Run Keyword And Return Status	Wait Until Keyword Succeeds	4x	5s
	...	GUI::Tracking::Check For Event ID	${CURRENT_DATE_BEFORE_LOGIN}	TABLE_ROWS_ROW=${TABLE_ROWS_ROW}
	...	EVENT_ID=${EVENT_ID}	TABLE_CELL_ROW=${TABLE_CELL_ROW}	TABLE_CELL_COLUMN=${TABLE_CELL_COLUMN}
	IF	'${RESULT}' == 'True'
		${LOG_MESSAGE}	Set Variable	UTC Date: ${CURRENT_DATE_AFTER_LOGIN}. Event ${EVENT_ID} generated successfully.\n
	ELSE
		${LOG_MESSAGE}	Set Variable	UTC Date: ${CURRENT_DATE_AFTER_LOGIN}. Event ${EVENT_ID} NOT generated.\n
	END
	Log	\n++++++++++++++++++++++ + Result of check Nodegrid Events: ++++++++++++++++++++++\n${LOG_MESSAGE}  INFO     console=yes
	Run Keyword If	'${GENERATE_EVENTS_LOG_FILE}' == 'Yes'	GUI::Basic::Generate Jenkins Build Events Log file	${LOG_MESSAGE}	LOG_FILE_NAME=events
	[Return]	${RESULT}

GUI::Tracking::Check For Event ID
	[Arguments]	${CURRENT_DATE_BEFORE_LOGIN}	${TABLE_ROWS_ROW}	${EVENT_ID}	${TABLE_CELL_ROW}	${TABLE_CELL_COLUMN}
	GUI::Basic::Spinner Should Be Invisible
	Click Element	xpath=//a[@id='listEvents']/span
	GUI::Basic::Spinner Should Be Invisible
	Input Text	id=filter_field	${EMPTY}
	GUI::Basic::Spinner Should Be Invisible
	Click Button	id=button_search_date
	GUI::Basic::Spinner Should Be Invisible
	Table Row Should Contain	id=eventListTable	${TABLE_ROWS_ROW}	${EVENT_ID}
	${TABLE_CELL}	Get Table Cell	id=eventListTable	${TABLE_CELL_ROW}	${TABLE_CELL_COLUMN}
	Log	\nTABLE_CELL: \n${TABLE_CELL}	INFO	console=yes
	${DATE_FROM_TABLE}	Replace String Using Regexp	${TABLE_CELL}	([A-Z]|[a-z])	${SPACE}	count=2
	${CONVERTED_DATE_FROM_TABLE}	Convert Date	${DATE_FROM_TABLE}	date_format=%Y-%m-%d %H:%M:%S${SPACE}	#2022-03-02T17:31:57Z
	Log	\nCONVERTED_DATE_FROM_TABLE: \n${CONVERTED_DATE_FROM_TABLE}	INFO	console=yes
	${TABLE_CONTENTS}=	Get Text	id=eventListTable
	Log	\nTable contents in Tracking::Event List::Events: \n${TABLE_CONTENTS}	INFO	console=yes
	${SUBTRACTION}	Subtract Date From Date	${CONVERTED_DATE_FROM_TABLE}	${CURRENT_DATE_BEFORE_LOGIN}	result_format=number
	Log	\nSUBTRACTION (${CONVERTED_DATE_FROM_TABLE} - ${CURRENT_DATE_BEFORE_LOGIN}): \n${SUBTRACTION}	INFO	console=yes
	Should Be True	${SUBTRACTION} > 0

GUI::Tracking::Check If Has GSM Module and Sim Card with Signal
	GUI::Basic::Spinner Should Be Invisible
	GUI::Tracking::Open Devices Tab
	${HAS_GSM}	Run Keyword And Return Status	Page Should Contain Element	//span[normalize-space()='Wireless Modem']
	Run Keyword If	not ${HAS_GSM}	Return From Keyword	${HAS_GSM}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Click Element	//span[normalize-space()='Wireless Modem']
	${TABLE_CONTENTS}=	Get Text	wmodemTable
	Log	\nTable contents in Tracking :: Devices :: Wireless Modem \n${TABLE_CONTENTS}	INFO	console=yes
	${TABLE_CONTENTS}=	Convert To String	${TABLE_CONTENTS}
	@{GET_LINES}=	Split To Lines	${TABLE_CONTENTS}
	FOR	${INDEX}	${LINE}	IN ENUMERATE	@{GET_LINES}
		${HAS_OPERATOR}=	Run Keyword And Return Status	Should Contain Any	${LINE}	@{GSM_WMODEM_CARRIERS_APN.keys()}
		${HAS_RADIO}=	Run Keyword And Return Status	Should Contain	${LINE}	${GSM_RADIO_MODE}
		${HAS_SIGNAL}=	Run Keyword And Return Status	Should Match Regexp	${LINE}	((?:\\b|-)([1-9]{1,2}[0]?|100)\\b)%	#regexp to match only numbers between 1-100%
		IF	${HAS_OPERATOR} and ${HAS_RADIO} and ${HAS_SIGNAL}
			${HAS_GSM}=	Set Variable	${TRUE}
			Return From Keyword	${HAS_GSM}
		ELSE
			${HAS_GSM}=	Set Variable	${FALSE}
		END
	END
	[Return]	${HAS_GSM}

GUI::Tracking::Rewrite to root GSM interface format
	[Arguments]	${GSM_CDC_WDMX}
	${GSM_INTERFACE}	Fetch From Right	${GSM_CDC_WDMX}	cdc-wdm
	${ROOT_GSM_INTERFACE}	Set Variable	wwan${GSM_INTERFACE}
	[Return]	${ROOT_GSM_INTERFACE}

GUI::Tracking::Get Tracking QoS Table Text
	GUI::Basic::Spinner Should Be Invisible
	GUI::Tracking::Open Network Tab
	GUI::Basic::Spinner Should Be Invisible
	wait until page contains element		xpath=//span[contains(.,'QoS')]
	click element		xpath=//span[contains(.,'QoS')]
	GUI::Basic::Spinner Should Be Invisible
	${TABLE_CONTENTS}=	Get Text	//table[@id='trackingQosTable']
	Log	\nTable contents in Tracking :: Network :: QoS \n${TABLE_CONTENTS}	INFO	console=yes
	[Return]	${TABLE_CONTENTS}

GUI::Tracking::Get Tracking Wireless Modem Table Cell
	[Arguments]	${ROW}	${COLUMN}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::Tracking::Devices::Open Tab
	GUI::Basic::Click Element	//span[normalize-space()='Wireless Modem']
	${TABLE_CELL}	Get Table Cell	id=wmodemTable	${ROW}	${COLUMN}
	Log	\nTABLE_CELL: \n${TABLE_CELL}	INFO	console=yes
	[Return]	${TABLE_CELL}

GUITracking::Network::Open IPsec
	GUI::Basic::Spinner Should Be Invisible
	Click Element	id=menu_main_tracking
	GUI::Basic::Spinner Should Be Invisible
	#relative xpath to Tracking::"Network"
	Click Element	//li[@role='presentation']//a[contains(text(),'Network')]
	GUI::Basic::Spinner Should Be Invisible
	#relative xpath to Tracking::Network::"IPsec"
	Run Keyword If	'${NGVERSION}' == '5.0'	Click Element	//span[normalize-space()='IPsec Table']
	...	ELSE	Click Element	//span[normalize-space()='IPsec']
	GUI::Basic::Spinner Should Be Invisible
	Page Should Contain	Reload

GUI::Tracking::Get index number with 5G sim card Up otherwise 4G Up
	${IS_4G_MODEM}=	Set Variable	${FALSE}
	${IS_5G_MODEM}=	Set Variable	${FALSE}
	${4G_INDEX}=	Set Variable	${NULL}
	${5G_INDEX}=	Set Variable	${NULL}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Tracking::Open Devices Tab
	GUI::Basic::Click Element	//span[normalize-space()='Wireless Modem']
	${TABLE_CONTENTS}=	Get Text	wmodemTable
	Log	\nTable contents in Tracking :: Devices :: Wireless Modem \n${TABLE_CONTENTS}	INFO	console=yes
	${TABLE_CONTENTS}=	Convert To String	${TABLE_CONTENTS}
	@{GET_LINES}=	Split To Lines	${TABLE_CONTENTS}
	FOR	${INDEX}	${LINE}	IN ENUMERATE	@{GET_LINES}
		${HAS_OPERATOR}=	Run Keyword And Return Status	Should Contain Any	${LINE}	@{GSM_WMODEM_CARRIERS_APN.keys()}
		${HAS_RADIO}=	Run Keyword And Return Status	Should Contain	${LINE}	${GSM_RADIO_MODE}
		${HAS_SIGNAL}=	Run Keyword And Return Status	Should Match Regexp	${LINE}	((?:\\b|-)([1-9]{1,2}[0]?|100)\\b)%	#regexp to match only numbers between 1-100%
		IF	${HAS_OPERATOR} and ${HAS_RADIO} and ${HAS_SIGNAL}
			GUI::Basic::Click Element	//tr[${INDEX}]//td[1]//a
			Wait Until Page Contains Element	//input[@id='wmodModel']
			${IS_4G_MODEM}=	Run Keyword And Return Status	Textfield Should Contain	//*[@id='wmodModel']	${GSM_4G_MODEM_MODEL}
			${IS_5G_MODEM}=	Run Keyword And Return Status	Textfield Should Contain	//*[@id='wmodModel']	${GSM_5G_MODEM_MODEL}
			GUI::Basic::Click Element	//input[@id='returnButton']
			${4G_INDEX}=	Run Keyword If	${IS_4G_MODEM}	Set Variable	${INDEX}
			...	ELSE	Set Variable	${NULL}
			${5G_INDEX}=	Run Keyword If	${IS_5G_MODEM}	Set Variable	${INDEX}
			...	ELSE	Set Variable	${NULL}
			Run Keyword If	not ${IS_4G_MODEM} and not ${IS_5G_MODEM}	Fail	The sim card must be 4G or 5G.
		END
	END
	${4G_OR_5G_INDEX}=	Run Keyword If	${IS_5G_MODEM}	Set Variable	${5G_INDEX}
	...	ELSE	Set Variable	${4G_INDEX}
	Set Suite Variable	${IS_4G_MODEM}
	Set Suite Variable	${IS_5G_MODEM}
	[Return]	${4G_OR_5G_INDEX}