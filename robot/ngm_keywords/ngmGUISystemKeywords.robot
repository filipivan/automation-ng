*** Settings ***
Documentation
...    Keywords related to System tabs to be used on GUI.
Resource	../ngm_tests/init.robot

*** Keywords ***
#
# First keyword
#        [Arguments]     ${HOMEPAGE}     ${BROWSER}
#        [Documentation]	Opens a browser instance and navigates to the provided URL
#        ...     == REQUIREMENTS ==
#        ...     == ARGUMENTS ==
#        ...     == EXPECTED RESULT ==
#        [Tags]      GUI     BROWSER

GUI::Open System tab
        [Arguments]     ${SCREENSHOT}=${FALSE}
        [Documentation]	Opens the NodeGrid Manager System tab
        ...     == REQUIREMENTS ==
        ...     None
        ...     == ARGUMENTS ==
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...     System Tab is open and all elements are accessable
        [Tags]      GUI     SYSTEM      LICENSE
		Run Keyword If	'${NGVERSION}'=='3.2'
		...	Run Keywords
		...	Wait Until Element Is Visible       //*[@id="main.system"]/div/img
		...	AND	Click Element       //*[@id="menu_1"]/li[3]
		...	AND	Wait Until Element Is Visible     //*[@id="main.system.SystemPreference"]/span[2]
		...	ELSE IF	'${NGVERSION}'>='4.0'
		...	Run Keywords
		...	GUI::Basic::Spinner Should Be Invisible
		...	AND	Click Element	jquery=#main_menu > li:nth-child(3) > a
		...	AND	GUI::Basic::Spinner Should Be Invisible
		Run Keyword If	${SCREENSHOT} != ${FALSE}	Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

#System License Tab Keywords

GUI::System::Open License tab
        [Arguments]     ${SCREENSHOT}=${FALSE}
        [Documentation]	Opens the NodeGrid Manager System::License Tab.
        ...     == ARGUMENTS ==
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...     License Tab is open and all elements are accessable
        [Tags]      GUI     SYSTEM      LICENSE
	${on_license} =	Run Keyword And Return Status	Page Should Contain Element	id=license_table
	Run Keyword Unless	${on_license}	Run Keywords	
	...	GUI::Open System tab	${SCREENSHOT}
	...	AND	Wait Until Element Is Visible     class:submenu
        ...	AND	Click Element       jquery:.submenu > ul:nth-child(1) > li:nth-child(1)
        ...	AND	Wait Until Element Is Visible       id=license_table
        ...	AND	Run Keyword If	${SCREENSHOT} != ${FALSE}	Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI::System::License::Add License
        [Arguments]     ${LICENSE}      ${SCREENSHOT}=${FALSE}
        [Documentation]	Adds a License key to the NodeGrid Manager and validates that the key was added *NOT FULLY IMPLEMENTED*
        ...     == REQUIREMENTS ==
        ...     Expects that the License tab is open
        ...     == ARGUMENTS ==
        ...     -   LICENSE = License Key which should be added
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...     License Key is add to the NodeGride Manager
        [Tags]      GUI     SYSTEM      LICENSE
	GUI::System::Open License tab
	GUI::Basic::Add
	GUI::Basic::Save Button Should Be Visible
	GUI::Basic::Cancel Button Should Be Visible
        Wait Until Element Is Visible       //*[@id="licensevalue"]
        Input Text          //*[@id="licensevalue"]    ${LICENSE}
	GUI::Basic::Save
        Run Keyword If	${SCREENSHOT} != ${FALSE}	Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI::System::License::Add::Save
        Wait Until Element Is Enabled   //*[@id="saveButton"]
        Click Element       //*[@id="saveButton"]
        Wait Until Element Is Visible       name=-
GUI::System::License::Add::Cancel
        Wait Until Element Is Visible       //*[@id="licensevalue"]
        Click Element       //*[@id="form_commands"]/div[2]/div[3]/a
        Wait Until Element Is Visible       name=-

GUI::System::License::Delete License
        [Arguments]     ${LICENSE}      ${SCREENSHOT}=${FALSE}
        [Documentation]	Delets a License key from the NodeGrid Manager and validates that the key was deleted *NOT FULLY IMPLEMENTED*
        ...     == REQUIREMENTS ==
        ...     None
        ...     == ARGUMENTS ==
        ...     -   LICENSE = License Key which should be deleted
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...     License Key is deleted from the NodeGride Manager
        [Tags]      GUI     SYSTEM      LICENSE
	GUI::System::Open License tab
        ${key}=     Fetch From Right    ${LICENSE}      -
        ${Licensekey}=      Catenate    SEPARATOR=      xxxxx-xxxxx-xxxxx-  ${key}
        Log      ${Licensekey}
        GUI::Basic::Select Rows In Table Containing Value   license_table   ${Licensekey}
        GUI::Basic::Delete
        Log     Exited
        Page Should Not Contain           ${Licensekey}

GUI::System::License::Delete All Licenses
	GUI::System::Open License tab
	GUI::Basic::Delete All Rows In Table If Exists	\#license_table	${FALSE}

#System_Preferences Tab Keywords

GUI::System::Open Preferences tab
        [Arguments]     ${SCREENSHOT}=${FALSE}
        [Documentation]	Opens the NodeGrid Manager System::Preferences Tab, it expects the System tab to be open
        ...     == REQUIREMENTS ==
        ...     System tab is open
        ...     == ARGUMENTS ==
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...     LicPreferences Tab is open and all elements are accessable
        [Tags]      GUI     SYSTEM      LICENSE
        Run Keyword If	'${NGVERSION}'=='3.2'
		...	Run Keywords
		...	Wait Until Element Is Visible     //*[@id="main.system.SystemPreference"]/span[2]
		...	AND	Click Element       //*[@id="menu_1"]/li[3]/div/ul/li[2]
        ...	AND	Wait Until Element Is Visible       //*[@id="netboot_url"]
		...	ELSE IF	'${NGVERSION}'>='4.0'
		...	Run Keywords
		...	GUI::Open System tab
		...	AND	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(2)
		...	AND	GUI::Basic::Spinner Should Be Invisible
		...	AND	Wait Until Element Is Visible	jquery=#saveButton
		...	AND	Wait Until Element Is Visible	jquery=#addr_location
        Run Keyword If      ${SCREENSHOT} != ${FALSE}      Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI::System::Preferences::Save
        [Arguments]
        [Documentation]	Opens the NodeGrid Manager System::Preferences Tab, it expects the System tab to be open
        ...     == REQUIREMENTS ==
        ...     System tab is open
        ...     == ARGUMENTS ==
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...     LicPreferences Tab is open and all elements are accessable
        [Tags]      GUI     SYSTEM      LICENSE
        Wait Until Element Is Enabled   //*[@id="saveButton"]
        Click Element       //*[@id="saveButton"]
        Wait Until Element Is Visible       //*[@id="netboot_url"]

GUI::System::Preferences::Address Location
        [Arguments]     ${ADDRESSS}=Castletroy, Limerick, Ireland      ${COORDINATE}=52.6654318,-8.5712428      ${SCREENSHOT}=${FALSE}
        [Documentation]	Clears the current Address and Coordinate details and sets the location details to a know good values and confirms that the returned Coordinates match the expected result
        ...     == REQUIREMENTS ==
        ...     None
        ...     == ARGUMENTS ==
        ...     -   ADDRESS = The Address which should be used Default: Castletroy, Limerick, Ireland
        ...     -   COORDINATE = Coordinates which should match the Address. Default: 52.6654318,-8.5712427
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...     License Key is add to the NodeGride Manager
        [Tags]      GUI     SYSTEM      PREFERENCES
        Wait Until Element Is Visible       //*[@id="coordinates"]
        Clear Element Text      //*[@id="coordinates"]
        Clear Element Text      //*[@id="addr_location"]
        Run Keyword If      ${SCREENSHOT} != ${FALSE}      Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png
        Input Text              //*[@id="addr_location"]        ${ADDRESSS}
        Click Element           //*[@id="getcoordinates_btn"]
        Sleep                   3
        Textfield Should Contain         //*[@id="coordinates"]        ${COORDINATE}
        Run Keyword If      ${SCREENSHOT} != ${FALSE}      Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

#52.6654318,-8.5712427

GUI::System::Preferences::Address Coordinates
        [Arguments]     ${SCREENSHOT}=${FALSE}
        [Documentation]	*NOT IMPLEMENTED*
        ...     == REQUIREMENTS ==
        ...
        ...     == ARGUMENTS ==
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...
        [Tags]      GUI     SYSTEM      PREFERENCES

GUI::System::Preferences::Help Location
        [Arguments]    ${HELP}=https://www.zpesystems.com/ng/v4_0/NodegridManual4.0.html        ${SCREENSHOT}=${FALSE}
        [Documentation]	Confirms that the default setting for the help file points to the correct location
        ...     == REQUIREMENTS ==
        ...     None
        ...     == ARGUMENTS ==
        ...     -   HELP = URL of the Current correct Help file. Default: https://www.zpesystems.com/ng/v4_0/NodegridManual4.0.html
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...     Help file location should match ${HELP}
        [Tags]      GUI     SYSTEM      PREFERENCES
        Wait Until Element Is Visible       //*[@id="help_url"]
        Textfield Value Should Be      id=help_url    ${HELP}
        Run Keyword If      ${SCREENSHOT} != ${FALSE}      Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI::System::Preferences::Session Idle Timeout
        [Arguments]     ${LICENSE}      ${SCREENSHOT}=${FALSE}
        [Documentation]	*NOT FULLY IMPLEMENTED*
        ...     == REQUIREMENTS ==
        ...     Nene
        ...     == ARGUMENTS ==
        ...     -   LICENSE = License Key which should be deleted
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...     License Key is add to the NodeGride Manager
        [Tags]      GUI     SYSTEM      PREFERENCES


GUI::System::Preferences::Logo Image selection
        [Arguments]     ${LICENSE}      ${SCREENSHOT}=${FALSE}
        [Documentation]	*NOT FULLY IMPLEMENTED*
        ...     == REQUIREMENTS ==
        ...     Nene
        ...     == ARGUMENTS ==
        ...     -   LICENSE = License Key which should be deleted
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...     License Key is add to the NodeGride Manager
        [Tags]      GUI     SYSTEM      PREFERENCES

GUI::System::Preferences::Enable Banner Message
        [Arguments]     ${LICENSE}      ${SCREENSHOT}=${FALSE}
        [Documentation]	*NOT FULLY IMPLEMENTED*
        ...     == REQUIREMENTS ==
        ...     Nene
        ...     == ARGUMENTS ==
        ...     -   LICENSE = License Key which should be deleted
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...     License Key is add to the NodeGride Manager
        [Tags]      GUI     SYSTEM      PREFERENCES

GUI::System::Preferences::Enable License Utilization Rate
        [Arguments]     ${LICENSE}      ${SCREENSHOT}=${FALSE}
        [Documentation]	*NOT FULLY IMPLEMENTED*
        ...     == REQUIREMENTS ==
        ...     Nene
        ...     == ARGUMENTS ==
        ...     -   LICENSE = License Key which should be deleted
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...     License Key is add to the NodeGride Manager
        [Tags]      GUI     SYSTEM      PREFERENCES


GUI::System::Preferences::Percentage to trigger events
        [Arguments]     ${LICENSE}      ${SCREENSHOT}=${FALSE}
        [Documentation]	*NOT FULLY IMPLEMENTED*
        ...     == REQUIREMENTS ==
        ...     Nene
        ...     == ARGUMENTS ==
        ...     -   LICENSE = License Key which should be deleted
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...     License Key is add to the NodeGride Manager
        [Tags]      GUI     SYSTEM      PREFERENCES

GUI::System::Preferences::Network Boot:Unit IPv4 Address
        [Arguments]     ${LICENSE}      ${SCREENSHOT}=${FALSE}
        [Documentation]	*NOT FULLY IMPLEMENTED*
        ...     == REQUIREMENTS ==
        ...     Nene
        ...     == ARGUMENTS ==
        ...     -   LICENSE = License Key which should be deleted
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...     License Key is add to the NodeGride Manager
        [Tags]      GUI     SYSTEM      PREFERENCES

GUI::System::Preferences::Network Boot:Unit Netmask
        [Arguments]     ${LICENSE}      ${SCREENSHOT}=${FALSE}
        [Documentation]	*NOT FULLY IMPLEMENTED*
        ...     == REQUIREMENTS ==
        ...     Nene
        ...     == ARGUMENTS ==
        ...     -   LICENSE = License Key which should be deleted
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...     License Key is add to the NodeGride Manager
        [Tags]      GUI     SYSTEM      PREFERENCES

GUI::System::Preferences::Network Boot:Unit Interface
        [Arguments]     ${LICENSE}      ${SCREENSHOT}=${FALSE}
        [Documentation]	*NOT FULLY IMPLEMENTED*
        ...     == REQUIREMENTS ==
        ...     Nene
        ...     == ARGUMENTS ==
        ...     -   LICENSE = License Key which should be deleted
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...     License Key is add to the NodeGride Manager
        [Tags]      GUI     SYSTEM      PREFERENCES

GUI::System::Preferences::Network Boot:ISO URL
        [Arguments]     ${LICENSE}      ${SCREENSHOT}=${FALSE}
        [Documentation]	*NOT FULLY IMPLEMENTED*
        ...     == REQUIREMENTS ==
        ...     Nene
        ...     == ARGUMENTS ==
        ...     -   LICENSE = License Key which should be deleted
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...     License Key is add to the NodeGride Manager
        [Tags]      GUI     SYSTEM      PREFERENCES

# System Date and Time keywords

GUI::System::Open Date and Time tab
        [Arguments]     ${SCREENSHOT}=${FALSE}
        [Documentation]	Opens the NodeGrid Manager System::Date and Time Tab, it expects the System tab to be open
        ...     == REQUIREMENTS ==
        ...     System tab is open
        ...     == ARGUMENTS ==
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...     Date and Time Tab is open and all elements are accessable
        [Tags]      GUI     SYSTEM      LICENSE
        ${ret}=	GUI::Basic::Get NodeGrid System
		Run Keyword If	'${ret}' == 'Nodegrid Services Router'		Set Suite Variable	${TAB_NUM}       4
		...	ELSE 	Set Suite Variable   ${TAB_NUM}    3
        Wait Until Element Is Visible     jquery=body > div.main_menu > div > ul > li:nth-child(${TAB_NUM})
        Click Element       jquery=body > div.main_menu > div > ul > li:nth-child(${TAB_NUM})
        Wait Until Element Is Visible       //*[@id="server"]
        Run Keyword If      ${SCREENSHOT} != ${FALSE}      Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI::System::Open Toolkit tab
        [Arguments]     ${SCREENSHOT}=${FALSE}
        [Documentation]	Opens the NodeGrid Manager System::Toolkit Tab, it expects the System tab to be open
        ...     == REQUIREMENTS ==
        ...     System tab is open
        ...     == ARGUMENTS ==
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...     Toolkit Tab is open and all elements are accessable
        [Tags]      GUI     SYSTEM      LICENSE
        ${ret}=	GUI::Basic::Get NodeGrid System
		Run Keyword If	'${ret}' == 'Nodegrid Services Router'		Set Suite Variable	${TAB_NUM}       5
		...	ELSE 	Set Suite Variable   ${TAB_NUM}    4
        Wait Until Element Is Visible     jquery=body > div.main_menu > div > ul > li:nth-child(${TAB_NUM})
        Click Element       jquery=body > div.main_menu > div > ul > li:nth-child(${TAB_NUM})
        Wait Until Element Is Visible       //*[@id="icon_systemCfgChk"]
        Run Keyword If      ${SCREENSHOT} != ${FALSE}      Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI::System::Open Logging tab
        [Arguments]     ${SCREENSHOT}=${FALSE}
        [Documentation]	Opens the NodeGrid Manager System::Logging Tab, it expects the System tab to be open
        ...     == REQUIREMENTS ==
        ...     System tab is open
        ...     == ARGUMENTS ==
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...     Logging Tab is open and all elements are accessable
        [Tags]      GUI     SYSTEM      LICENSE
        ${ret}=	GUI::Basic::Get NodeGrid System
		Run Keyword If	'${ret}' == 'Nodegrid Services Router'		Set Suite Variable	${TAB_NUM}       6
		...	ELSE 	Set Suite Variable   ${TAB_NUM}    5
        Wait Until Element Is Visible     jquery=body > div.main_menu > div > ul > li:nth-child(${TAB_NUM})
        Click Element       jquery=body > div.main_menu > div > ul > li:nth-child(${TAB_NUM})
        Wait Until Element Is Visible       //*[@id="sessionlogenable"]
        Run Keyword If      ${SCREENSHOT} != ${FALSE}      Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI::System::Open Custom Fields tab
        [Arguments]     ${SCREENSHOT}=${FALSE}
        [Documentation]	Opens the NodeGrid Manager System::Custom Fields Tab, it expects the System tab to be open
        ...     == REQUIREMENTS ==
        ...     System tab is open
        ...     == ARGUMENTS ==
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...     Custom Fields Tab is open and all elements are accessable
        [Tags]      GUI     SYSTEM      CUSTOM LICENSES
        ${ret}=	GUI::Basic::Get NodeGrid System
		Run Keyword If	'${ret}' == 'Nodegrid Services Router'		Set Suite Variable	${TAB_NUM}       7
		...	ELSE 	Set Suite Variable   ${TAB_NUM}    6
        Wait Until Element Is Visible	jquery=body > div.main_menu > div > ul > li:nth-child(${TAB_NUM})
	Click Element	jquery=body > div.main_menu > div > ul > li:nth-child(${TAB_NUM})
	GUI::Basic::Spinner Should Be Invisible
        Run Keyword If      ${SCREENSHOT} != ${FALSE}      Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI::System::Custom Fields::Add Custom Field
        [Arguments]     ${LABEL}	${VALUE}      ${SCREENSHOT}=${FALSE}
        [Documentation]	Adds a Custom Field to the NodeGrid Manager
        ...     == REQUIREMENTS ==
        ...     Expects that the License tab is open
        ...     == ARGUMENTS ==
        ...     -   LABEL = Label of the custom field
        ...     -   VALUE = Value of the custom field
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...     License Key is add to the NodeGride Manager
        [Tags]      GUI     SYSTEM      CUSTOM FIELD
	GUI::Basic::Spinner Should Be Invisible
        Wait Until Element Is Enabled   //*[@id="addButton"]
        Click Element       //*[@id="addButton"]
        Wait Until Element Is Visible       //*[@id="custom_name"]
        Input Text          //*[@id="custom_name"]    ${LABEL}
        Wait Until Element Is Visible       //*[@id="custom_value"]
        Input Text          //*[@id="custom_value"]    ${VALUE}
        Wait Until Element Is Enabled   //*[@id="saveButton"]
        Run Keyword If      ${SCREENSHOT} != ${FALSE}      Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI::System::Custom Fields::Add::Save
        Wait Until Element Is Enabled   //*[@id="saveButton"]
        Click Element       //*[@id="saveButton"]
        Wait Until Element Is Visible       //*[@id="addButton"]

GUI::System::Custom Fields::Add::Cancel
        Wait Until Element Is Visible       //*[@id="custom_name"]
        Click Element       //*[@id="cancelButton"]
        Wait Until Element Is Visible       name=-

GUI::System::Open Dial Up tab
        [Arguments]     ${SCREENSHOT}=${FALSE}
        [Documentation]	Opens the NodeGrid Manager System::Dial Up Tab, it expects the System tab to be open
        ...     == REQUIREMENTS ==
        ...     System tab is open
        ...     == ARGUMENTS ==
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...     Dial Up Tab is open and all elements are accessable
        [Tags]      GUI     SYSTEM      LICENSE
        ${ret}=	GUI::Basic::Get NodeGrid System
		Run Keyword If	'${ret}' == 'Nodegrid Services Router'		Set Suite Variable	${TAB_NUM}       8
		...	ELSE 	Set Suite Variable   ${TAB_NUM}    7
        Wait Until Element Is Visible     jquery=body > div.main_menu > div > ul > li:nth-child(${TAB_NUM})
        Click Element       jquery=body > div.main_menu > div > ul > li:nth-child(${TAB_NUM})
        Wait Until Element Is Visible       //*[@id="dialupSetPPP"]
        Run Keyword If      ${SCREENSHOT} != ${FALSE}      Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI::System::Set Default Help Url And Close Browser
		GUI::System::Open Preferences tab
		Input Text	jquery=#help_url	https://www.zpesystems.com/ng/v4_0/NodegridManual4.0.html
		Click Element	jquery=#saveButton
		GUI::Basic::Spinner Should Be Invisible
		Close all Browsers

GUI::System::Add Dummy Dialup Callback User
	[Arguments]	${DU_NAME}	${DU_NUMBER}
	GUI::Basic::System::Dial Up::Open Tab
	GUI::Basic::Spinner Should Be Invisible
	Click Element   jquery=#dialupCallback
	Wait Until Element Is Visible	jquery=#addButton
	Click Element	jquery=#addButton
	GUI::Basic::Spinner Should Be Invisible
	Wait Until Element Is Visible	jquery=#saveButton
	Input Text	jquery=#dialupCbUser	${DU_NAME}
	Input Text	jquery=#dialupCbNumber	${DU_NUMBER}
	GUI::Basic::Click Element	//*[@id='saveButton']
	Wait Until Element Is Visible   jquery=#delButton
	Table Should Contain	jquery=#dialupCallbackTable 	${DU_NAME}


GUI::System::Open Scheduler tab
        [Arguments]     ${SCREENSHOT}=${FALSE}
        [Documentation]	Opens the NodeGrid Manager System::Scheduler, it expects the System tab to be open
        ...     == REQUIREMENTS ==
        ...     System tab is open
        ...     == ARGUMENTS ==
        ...     -	SCREENSHOT = Should a Screenshot be taken Default is false
        ...     == EXPECTED RESULT ==
        ...     Scheduler Tab is open and all elements are accessable
        [Tags]      GUI     SYSTEM      LICENSE
        ${ret}=	GUI::Basic::Get NodeGrid System
		Run Keyword If	'${ret}' == 'Nodegrid Services Router'		Set Suite Variable	${TAB_NUM}       9
		...	ELSE 	Set Suite Variable   ${TAB_NUM}    8
        Wait Until Element Is Visible     jquery=body > div.main_menu > div > ul > li:nth-child(${TAB_NUM})
        Click Element       jquery=body > div.main_menu > div > ul > li:nth-child(${TAB_NUM})
        GUI::Basic::Spinner Should be Invisible
        Run Keyword If      ${SCREENSHOT} != ${FALSE}      Capture Page Screenshot     filename=${SCREENSHOTDIR}${TEST NAME}{index:06}.png

GUI::System::Toolkit::Network Tools::Ping target through interface
	[Arguments]	${INTERFACE}	${TARGET}	${SOURCE_IP_ADDRESS}=${EMPTY}
	GUI::Basic::Spinner Should Be Invisible
	GUI::Basic::System::Toolkit::open tab
	Click Element	id=icon_NetworkTools
	GUI::Basic::Spinner Should Be Invisible
	Element Should Be Visible	//*[@id="ipaddress"]
	Element Should Be Visible	//*[@id="interface"]
	Input Text	//*[@id="ipaddress"]	${TARGET}
	Click Element	id=interface
	IF	'${SOURCE_IP_ADDRESS}' == '${EMPTY}'
		Select From List By Label	//*[@id="interface"]	${INTERFACE}
	ELSE
	Select From List By Label	//*[@id="interface"]	Source IP Address
	Input Text	//*[@id="interfaceIp"]	${SOURCE_IP_ADDRESS}
	END
	Click Element	//*[@id="networktools"]/div[2]/div[1]/div/input[1]
	GUI::Basic::Spinner Should Be Invisible
	${COMMAND_OUTPUT}=	Wait Until Keyword Succeeds	1m	3s	GUI::System::Toolkit::Network Tools::Get text in command output
	Should Match Regexp	${COMMAND_OUTPUT}	5 packets transmitted, 5 received, 0% packet loss
	[Teardown]	GUI::Basic::Cancel

GUI::System::Toolkit::Network Tools::Get text in command output
	${COMMAND_OUTPUT}=	Get Text	//*[@id="log"]
	Log	\nCommand Output contents in System::Toolkit::Network Tools: \n${COMMAND_OUTPUT}	INFO	console=yes
	Should Not Match Regexp	${COMMAND_OUTPUT}	Running...wait.
	[Return]	${COMMAND_OUTPUT}

GUI::System::Preferences::Add cordinates and save if needed
	GUI::System::Open Preferences tab
	GUI::Basic::Spinner Should Be Invisible
	GUI::System::Preferences::Address Location
	GUI::Basic::Save If Configuration Changed